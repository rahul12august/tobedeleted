# ms-transfer

Transfer service 

## Version specification
- node: v14.9.0
- TypeScript v3.5

## IDE recommendations and setup

- VSCode IDE
- Eslint vscode plugin
- Prettier vscode plugin for linting warnings (auto fix on save)
- Add the following setting in vscode settings.json 
```json
"eslint.autoFixOnSave": true
```

## Dev setup
- Install all the dependencies using `npm install`
- To run the server with watch use `npm run dev-watch`
- To run the test cases in watch mode use `npm run test-watch`
- To run the test cases without watch mode use `npm run test`
- To run docker compose `docker-compose -f docker-compose.dev.yml up -d`
- To build docker image only:
`
 npm tsc &&
 docker build -t ${your image name} .
`

## Test

- Unit Test: We are using Jest for assertion and mocking

## Git Hooks
The seed uses `husky` to enable commit hook.

### Pre commit
Whenever there is a commit, there will be check on lint, on failure commit fails.

### Pre push
Whenever there is a push, there will be check on test.

## Internal Module dependencies
The following are module dependecies:
- @dk/module-common: shared common error handling, dictionaries
- @dk/module-config: shared common logic of configuration management
- @dk/module-httpclient: untility for working with http client
- @dk/module-logger: shared common logic for log management

## Working with Mongo migration script

**Limitation**: can not reuse model from typescript because migration scripts are not supported in in **Typescript** yet

Steps to create migration script

Create migration script
```bash
npm run migrate:create <Migration class name>
```

Run migration script up
```bash
npm run migrate:up
```

Run migration script down
```bash
npm run migrate:down
```

## ENV variables

### Optional
- LOG_FILE: path to log file, default console log

## Misc

Swagger API is at http://localhost:8080/documentation
