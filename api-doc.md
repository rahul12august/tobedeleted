Welcome to Bank Jago API for transfer.

## Overview
This document is to describe the detail design of ms-transfer service to support the following business requirement:
- Transfer from an internal account to another internal account
- Transfer from an internal account to an external bank account
- Top-up (in-app or from external such as switching)
- Blocking transaction. The transaction amount is blocked, adjusted or canceled and executed when it’s confirmed

### Transfer transaction
- This is to describe the flow of transfer transaction between internal accounts
- The service will reject the request if any of the validation failed
- Below diagram Ably part is not included with ~ 1 second average performance 


<img src="https://dev-assets.jago.com/assets/transfer-flow/transfer-submit-transfer-transaction.png"
     alt="Transfer transaction flow"
     style="float: center;" />

| No   | From                  | To               | Description                     | 
| ---- | --------------------- | ---------------- | ------------------------------- | 
| `1`  | `ms-transfer`         | `transaction DB` | Execute payment service rules and store transaction into database `ms-transfer` executes the transaction and store it into `transaction db` | 
| `1.1`  | `ms-payment-instruction` | `ms-transfer` | Submit transfer transaction `ms-payment-instruction` submits the transfer order to `ms-transfer` | 
| `1.2`  | `ms-transfer` | `ms-account` | Inquiry account balance to validate transaction amount `ms-transfer` submits a request to `ms-account` to get get balance and validate it with the transaction amount | 
| `1.3`  | `ms-account` | `Core Banking` | Get account balance `ms-account` connects to  `Core Banking` to get the available balance of the account | 
| `1.4`  | `ms-transfer` | `ms-award` | Get transaction usage counter to calculate transaction fee `ms-transfer` connects to `ms-award` to check the available bonus for free quota transfer/withdrawal in regard to calculate the transaction fee | 
| `1.5`  | `ms-transfer` | `ms-configuration` | Get payment service rules, bank code mapping, category code `ms-transfer` connects to `ms-configuration` to get the correct payment configuration and validate the destination bank. | 
| `1.6`  | `ms-transfer` | `transaction db` | Execute payment service rules and store transaction into database `ms-transfer` executes the transaction and store it into `transaction db`. | 
| `1.7`  | `ms-transfer` | `Core Banking` | Submit withdraw and fee transaction to source account `ms-transfer` instructs `Core Banking` to settle the transaction and also the fee. | 
| `1.8`  | `ms-transfer` | `Switching` | Submit deposit transactions to outgoing external account If the destination account is on another bank, `ms-transfer`notifies the `Switching` to process the transaction to the destination account. | 
| `1.9`  | `ms-transfer` | `ms-award` | Update usage counter Once the transaction is processed, `ms-transfer` updates the available bonus for free quota transfer/withdrawal. | 
| `2.0`  | `ms-transfer` | `ms-notification` | Send transaction succeed or failed `ms-transfer` connects to `ms-notification` in regard to notify the customer about the transaction status. | 

### Outgoing transfer transaction
This is to describe the flow of transfer transaction from an internal account to an external bank account
<img src="https://dev-assets.jago.com/assets/transfer-flow/transfer-submit-outgoing-transfer-transaction.png"
     alt="Outgoing transfer transaction flow"
     style="float: center;" />



### Blocking Transaction
Blocking transaction will block for the about. The customer can adjust, cancel and confirm the blocking later

<img src="https://dev-assets.jago.com/assets/transfer-flow/transfer-submit-blocking-transfer-transaction.png"
     alt="Blocking Transaction flow"
     style="float: center;" />

### Transaction Reversal
A linked pocket can be authorized by the customer to the partner application to make a payment with one of their pockets.

<img src="https://dev-assets.jago.com/assets/transfer-flow/reverse_tx_flow.png"
     alt="Pocket transaction flow"
     style="float: center;" />

## Top-up transaction from external
This describes the flow of top-up transaction come from external parties
<img src="https://dev-assets.jago.com/assets/transfer-flow/transfer-submit-external-top-up-transaction.png"
     alt="Top-up transaction flow"
     style="float: center;" />

## Error codes