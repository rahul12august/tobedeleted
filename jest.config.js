module.exports = {
  roots: ['<rootDir>/src'],
  setupFilesAfterEnv: ['<rootDir>/src/__jest__/setup.ts'],
  transform: {
    '^.+\\.ts$': 'ts-jest'
  },
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: ['**/?(*.)(spec|test).ts'],
  moduleFileExtensions: ['ts', 'js', 'json', 'node'],
  globals: {
    'ts-jest': {
      diagnostics: false
    }
  },
  coverageThreshold: {
    global: {
      branches: 85,
      functions: 90,
      lines: 90
    }
  },
  collectCoverageFrom: [
    'src/**/*.{ts,js}',
    '!src/**/*.(interface|constant|type|validator).{ts,js}',
    '!src/proxy.ts',
    '!**/__mocks__/**',
    '!**/node_modules/**',
    '!src/common/contextProgressTracker.ts',
    '!src/entitlement/__tests__/contract.constants.ts',
    '!src/entitlement/__tests__/contract.helper.ts',
    '!src/entitlement/__tests__/common.headers.contract.ts'
  ]
};
