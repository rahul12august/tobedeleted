"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_model_1 = require("../src/transaction/transaction.model");
const logger_1 = __importDefault(require("../src/logger"));
module.exports = {
    up(db) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield db.collection(transaction_model_1.collectionName).updateMany({ paymentServiceCode: 'IRIS_JAGO_TO_OTH', fees: null }, {
                    $set: {
                        fees: []
                    }
                });
            }
            catch (e) {
                logger_1.default.error(`Error when migrating up script 20210409082140-update-fees-to-array-IRIS-transactions ${e}`);
            }
        });
    },
    down(db) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield db.collection(transaction_model_1.collectionName).updateMany({ paymentServiceCode: 'IRIS_JAGO_TO_OTH', fees: [] }, {
                    $set: {
                        fees: null
                    }
                });
            }
            catch (e) {
                logger_1.default.error(`Error when migrating down script 20210409082140-update-fees-to-array-IRIS-transactions ${e}`);
            }
        });
    }
};
//# sourceMappingURL=20210409082140-update-fees-to-array-IRIS-transactions.js.map