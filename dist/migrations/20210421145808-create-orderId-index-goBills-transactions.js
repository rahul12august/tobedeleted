"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const billPaymentTransaction_model_1 = require("../src/billPayment/billPaymentTransaction/billPaymentTransaction.model");
module.exports = {
    up(db) {
        return db
            .collection(billPaymentTransaction_model_1.collectionName)
            .createIndex({ orderId: 1 }, { name: 'orderId_Idx_1', background: true })
            .catch(e => e);
    },
    down(db) {
        return db
            .collection(billPaymentTransaction_model_1.collectionName)
            .dropIndex('orderId_Idx_1')
            .catch(e => e);
    }
};
//# sourceMappingURL=20210421145808-create-orderId-index-goBills-transactions.js.map