"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_model_1 = require("../src/transaction/transaction.model");
module.exports = {
    up(db) {
        db.collection(transaction_model_1.collectionName)
            .dropIndex('externalIdx_1')
            .catch(e => e);
        return db
            .collection(transaction_model_1.collectionName)
            .createIndex({ externalId: 1, paymentServiceType: -1 }, { name: 'externalIdx_paymentServiceTypex', unique: true, sparse: true });
    },
    down(db) {
        return db
            .collection(transaction_model_1.collectionName)
            .dropIndex('externalIdx_paymentServiceTypex');
    }
};
//# sourceMappingURL=20200924165935-externalId.js.map