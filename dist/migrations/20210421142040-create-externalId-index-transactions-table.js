"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_model_1 = require("../src/transaction/transaction.model");
module.exports = {
    up(db) {
        return db
            .collection(transaction_model_1.collectionName)
            .createIndex({ externalId: 1 }, { name: 'externalId_Idx_1', background: true })
            .catch(e => e);
    },
    down(db) {
        return db
            .collection(transaction_model_1.collectionName)
            .dropIndex('externalId_Idx_1')
            .catch(e => e);
    }
};
//# sourceMappingURL=20210421142040-create-externalId-index-transactions-table.js.map