"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importDefault(require("../src/logger"));
const gobillsPollingCollection = 'bill_payment_status_polling_configs';
const goBillsTransactionsCollection = 'bill_payment_transactions';
const gobillsPollingCollectionNewName = 'gobills_polling_states';
const goBillsTransactionsCollectionNewName = 'gobills_transactions';
module.exports = {
    up(db) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield Promise.all([
                    db.renameCollection(gobillsPollingCollection, gobillsPollingCollectionNewName),
                    db.renameCollection(goBillsTransactionsCollection, goBillsTransactionsCollectionNewName)
                ]);
            }
            catch (e) {
                logger_1.default.error(`Migration: Error while renaming ${gobillsPollingCollection} and ${goBillsTransactionsCollection}`);
            }
        });
    },
    down(db) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield Promise.all([
                    db.renameCollection(gobillsPollingCollectionNewName, gobillsPollingCollection),
                    db.renameCollection(goBillsTransactionsCollectionNewName, goBillsTransactionsCollection)
                ]);
            }
            catch (e) {
                logger_1.default.error(`Migration: Error while renaming ${gobillsPollingCollectionNewName} and ${goBillsTransactionsCollection}`);
            }
        });
    }
};
//# sourceMappingURL=20201022054336-rename-bill-payment-collections.js.map