"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_model_1 = require("../src/transaction/transaction.model");
module.exports = {
    up(db) {
        db.collection(transaction_model_1.collectionName)
            .dropIndex('secondaryExternalIdx_1')
            .catch(e => e);
        return db.collection(transaction_model_1.collectionName).createIndex({ externalId: 1, secondaryExternalId: 1, paymentServiceType: -1 }, {
            name: 'secondaryExternalIdx_paymentServiceTypex',
            unique: true,
            sparse: true
        });
    },
    down(db) {
        return db
            .collection(transaction_model_1.collectionName)
            .dropIndex('secondaryExternalIdx_paymentServiceTypex');
    }
};
//# sourceMappingURL=20200819165945-secondaryExternalId.js.map