"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_model_1 = require("../src/transaction/transaction.model");
module.exports = {
    up(db) {
        return db
            .collection(transaction_model_1.collectionName)
            .createIndex({ thirdPartyOutgoingId: 1 }, { name: 'thirdPartyOutgoingId_Idx_1', background: true })
            .catch(e => e);
    },
    down(db) {
        return db
            .collection(transaction_model_1.collectionName)
            .dropIndex('thirdPartyOutgoingId_Idx_1')
            .catch(e => e);
    }
};
//# sourceMappingURL=20210520110219-create-thirdPartyOutgoingId-index-transactions-table.js.map