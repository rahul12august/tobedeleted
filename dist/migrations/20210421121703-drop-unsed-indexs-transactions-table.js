"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_model_1 = require("../src/transaction/transaction.model");
module.exports = {
    up(db) {
        db.collection(transaction_model_1.collectionName)
            .dropIndex('offerClaimId_1')
            .catch(e => e);
        db.collection(transaction_model_1.collectionName)
            .dropIndex('secondaryExternalIdx_paymentServiceTypex')
            .catch(e => e);
        return db
            .collection(transaction_model_1.collectionName)
            .dropIndex('externalIdx_paymentServiceTypex')
            .catch(e => e);
    },
    down(db) {
        db.collection(transaction_model_1.collectionName)
            .createIndex({ offerClaimId: 1 }, { name: 'offerClaimId_1' })
            .catch(e => e);
        db.collection(transaction_model_1.collectionName)
            .createIndex({ externalId: 1, secondaryExternalId: 1, paymentServiceType: -1 }, { name: 'secondaryExternalIdx_paymentServiceTypex', unique: true })
            .catch(e => e);
        return db
            .collection(transaction_model_1.collectionName)
            .createIndex({ externalId: 1, paymentServiceType: -1 }, { name: 'externalIdx_paymentServiceTypex', unique: true })
            .catch(e => e);
    }
};
//# sourceMappingURL=20210421121703-drop-unsed-indexs-transactions-table.js.map