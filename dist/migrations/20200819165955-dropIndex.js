"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_model_1 = require("../src/transaction/transaction.model");
module.exports = {
    up(db) {
        db.collection(transaction_model_1.collectionName)
            .dropIndex('externalId_1')
            .catch(e => e);
        return db
            .collection(transaction_model_1.collectionName)
            .dropIndex('secondaryExternalId_1')
            .catch(e => e);
    },
    down(db) {
        db.collection(transaction_model_1.collectionName)
            .createIndex({ externalId: 1 }, { name: 'externalId_1', unique: true, sparse: true })
            .catch(e => e);
        return db
            .collection(transaction_model_1.collectionName)
            .createIndex({ secondaryExternalId: 1 }, { name: 'secondaryExternalId_1', unique: true, sparse: true })
            .catch(e => e);
    }
};
//# sourceMappingURL=20200819165955-dropIndex.js.map