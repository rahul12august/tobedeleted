"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_logger_1 = require("@dk/module-logger");
const constant_1 = require("../src/common/constant");
const module_common_1 = require("@dk/module-common");
const config_1 = require("./config");
const logger = module_logger_1.createLogger({
    defaultMeta: {
        service: config_1.config.get('serviceName')
    },
    tracing: {
        tracerSessionName: constant_1.Tracing.TRACER_SESSION,
        requestId: constant_1.Tracing.TRANSACTION_ID
    }
});
exports.wrapLogs = new module_common_1.Logger.BaseLogWrapper(logger).getWrapper();
exports.default = logger;
//# sourceMappingURL=logger.js.map