"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("./config");
const logger_1 = __importDefault(require("../src/logger"));
const module_common_1 = require("@dk/module-common");
const context_1 = require("../src/context");
const getAccessToken = () => process.env.RUNDECK_API_ACCESS_TOKEN || config_1.rundeck.apiAccessToken;
const rundeckConfig = {
    apiURL: config_1.rundeck.apiURL,
    apiVersion: config_1.rundeck.apiVersion,
    token: getAccessToken()
};
const rundeckClient = new module_common_1.Rundeck(rundeckConfig, logger_1.default, context_1.context);
logger_1.default.info(`Setting up rundeck-client: ${config_1.rundeck.apiURL}`);
exports.default = rundeckClient;
//# sourceMappingURL=rundeck.client.js.map