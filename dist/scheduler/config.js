"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const module_config_1 = require("@dk/module-config");
exports.config = module_config_1.config;
const defaultConf = require('../configs/default.json');
const env = process.env.NODE_ENV;
const envConf = env ? require(`../configs/${env}.json`) : {};
module_config_1.loadConfig(lodash_1.merge(defaultConf, envConf));
const rundeck = module_config_1.config.get('rundeck');
exports.rundeck = rundeck;
//# sourceMappingURL=config.js.map