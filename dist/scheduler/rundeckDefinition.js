"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("./config");
const ms = config_1.config.get('ms');
const autoUpdateMambuBlockingCounter = {
    filepath: `${__dirname}/jobDefinitions/put.yaml`,
    options: {
        jobId: 'update-mambu-blocking-counter',
        jobDescription: 'This job is to update mambu blocking counter with days code',
        group: 'ms-transfer',
        name: 'Update mambu blocking counter',
        data: '',
        api: `${ms.transfer}/private/jobs-execution/update-mambu-blocking-days`,
        scheduleDay: '"*"',
        scheduleMonth: '"*"',
        scheduleHour: '"0"',
        scheduleMinute: '"0"',
        scheduleSeconds: '"0"',
        scheduleYear: '"*"',
        slackWebhookURL: config_1.rundeck.slackWebhookURL
    }
};
const runDeckDefinitions = [
    {
        project: {
            name: 'ms-transfer',
            configs: {}
        },
        jobs: [
            {
                filepath: `${__dirname}/jobDefinitions/put.yaml`,
                options: {
                    jobId: 'update-status-gobills',
                    jobDescription: 'This job is to update all pending transaction status from gobills',
                    group: 'ms-transfer',
                    name: 'Update Status from gobills',
                    data: '',
                    api: `${ms.transfer}/private/jobs/bill-payment/status-update`,
                    scheduleDay: '"*"',
                    scheduleMonth: '"*"',
                    scheduleHour: '"*"',
                    scheduleMinute: '"*/5"',
                    scheduleSeconds: '"0"',
                    scheduleYear: '"*"',
                    slackWebhookURL: config_1.rundeck.slackWebhookURL
                }
            },
            autoUpdateMambuBlockingCounter
        ]
    }
];
exports.default = runDeckDefinitions;
//# sourceMappingURL=rundeckDefinition.js.map