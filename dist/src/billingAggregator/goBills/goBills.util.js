"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const contextHandler_1 = require("../../common/contextHandler");
const config_1 = require("../../config");
exports.DEBOUNCE_MAX_ATTEMPTS = 3;
exports.getInitialDelayForUpdate = () => config_1.config.get('goBills').firstUpdateTimeInSeconds;
exports.RetryConfig = () => ({
    shouldRetry: (error) => {
        return error.status == 408 || (error.status > 499 && error.status < 600);
    },
    numberOfTries: contextHandler_1.getRetryCount() || config_1.config.get('goBills').retryPolicy.maxRetries,
    delayInMS: config_1.config.get('goBills').retryPolicy.delayTimeInSeconds
});
exports.goBillsErrorTranslator = (errorResponse) => {
    let error;
    if (errorResponse.name !== 'AppError' && errorResponse.error) {
        error = JSON.stringify(errorResponse.error.errors);
    }
    return error;
};
exports.debounceWhile = (predicate, maxAttempts, initialDelay, functionToExecute, ...args) => __awaiter(void 0, void 0, void 0, function* () {
    const delay = (seconds) => new Promise(resolve => setTimeout(resolve, seconds * 1000));
    const backOff = (offset) => Math.round((Math.pow(2, offset) + offset) * initialDelay);
    for (let i = 0; i < maxAttempts; i++) {
        const shouldContinue = yield predicate(...args);
        if (!shouldContinue) {
            break;
        }
        yield delay(backOff(i));
    }
    return yield functionToExecute(...args);
});
//# sourceMappingURL=goBills.util.js.map