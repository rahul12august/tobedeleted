"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const UrlConstants = {
    GET_PAYMENT_STATUS: '/order',
    POST_PAYMENT: '/payment'
};
exports.UrlConstants = UrlConstants;
//# sourceMappingURL=goBills.constant.js.map