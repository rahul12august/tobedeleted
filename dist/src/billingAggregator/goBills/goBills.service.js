"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const goBills_repository_1 = __importDefault(require("./goBills.repository"));
const billPaymentTransaction_service_1 = __importDefault(require("../../billPayment/billPaymentTransaction/billPaymentTransaction.service"));
const goBills_payload_1 = require("./goBills.payload");
const billPaymentTransaction_enum_1 = require("../../billPayment/billPaymentTransaction/billPaymentTransaction.enum");
const logger_1 = __importStar(require("../../logger"));
const goBills_util_1 = require("./goBills.util");
const transactionConfirmation_service_1 = __importDefault(require("../../transaction/transactionConfirmation.service"));
const paymentStatusPolling_service_1 = __importDefault(require("../../billPayment/paymentStatusPolling/paymentStatusPolling.service"));
const AppError_1 = require("../../errors/AppError");
const module_common_1 = require("@dk/module-common");
const transaction_util_1 = require("../../transaction/transaction.util");
const transaction_enum_1 = require("../../transaction/transaction.enum");
const billDetail_service_1 = __importDefault(require("../../billPayment/billDetail/billDetail.service"));
const errors_1 = require("../../common/errors");
const updateCoreBanking = (inquiryId, orderId, status, refNumber, voucherCode) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c, _d, _e;
    logger_1.default.info(`Bill Payment : Updating Core Banking for inquiryId : ${inquiryId}, status:${status}, refNumber:${refNumber}, voucherCode:${voucherCode}`);
    const transaction = yield billPaymentTransaction_service_1.default.getTransactionStatus(orderId);
    const payload = {
        externalId: inquiryId,
        accountNo: (_a = transaction) === null || _a === void 0 ? void 0 : _a.accountNo,
        transactionDate: ((_b = transaction) === null || _b === void 0 ? void 0 : _b.createdAt) ? transaction_util_1.formatTransactionDate((_c = transaction) === null || _c === void 0 ? void 0 : _c.createdAt)
            : '',
        responseCode: status
            ? transaction_enum_1.ConfirmTransactionStatus.SUCCESS
            : transaction_enum_1.ConfirmTransactionStatus.ERROR_UNEXPECTED,
        additionalInformation1: refNumber,
        additionalInformation2: voucherCode,
        amount: (_d = transaction) === null || _d === void 0 ? void 0 : _d.amount
    };
    logger_1.default.info(`Bill Payment: Transaction Confirmation for transactionId : ${(_e = transaction) === null || _e === void 0 ? void 0 : _e.transactionId} with inquiry id : ${inquiryId}`);
    yield transactionConfirmation_service_1.default.confirmTransaction(payload);
});
const updateTransactionDB = (status, orderId, error) => __awaiter(void 0, void 0, void 0, function* () {
    return billPaymentTransaction_service_1.default.updateStatus({ status: status, reason: error, capturedAt: new Date() }, orderId);
});
const getStatusFromGoBills = (orderId) => goBills_repository_1.default.getBillPaymentStatus(orderId);
const updateTransactionWithResult = (res, orderId, updateBillDetails = true) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Updating ${res.status} for orderId: ${orderId}`);
    const result = yield updateTransactionDB(res.status, orderId, res.failureReason);
    if (!result) {
        let detail = `Error while updating bill payment transaction for orderId: ${orderId}!`;
        logger_1.default.error(`Bill Payment : ${detail} for payload : ${JSON.stringify(res)}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.INVALID_ORDER_ID);
    }
    if ((res.status == billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.SUCCESS ||
        res.status == billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.FAILURE) &&
        updateBillDetails) {
        yield billDetail_service_1.default.updateOrderDetails(result.referenceId, res);
        yield updateCoreBanking(result.referenceId, orderId, res.status == billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.SUCCESS, res.refNumber, res.voucherCode);
    }
    return result;
});
const retryUpdate = (orderId) => __awaiter(void 0, void 0, void 0, function* () { return yield module_common_1.Util.retry(goBills_util_1.RetryConfig(), getStatusFromGoBills, orderId); });
const updateBillPaymentStatus = (orderId) => __awaiter(void 0, void 0, void 0, function* () {
    return retryUpdate(orderId)
        .then((res) => __awaiter(void 0, void 0, void 0, function* () {
        logger_1.default.info(`Bill Payment : Updating bill payment status for orderId : ${orderId}`);
        const paymentConfig = yield paymentStatusPolling_service_1.default.updatePollingConfig(orderId, res.status);
        if (!paymentConfig &&
            res.status === billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.WAITING) {
            logger_1.default.error(`Bill Payment : Updating bill payment status for orderId : ${orderId} to ${billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.MANUAL_INTERVENTION}`);
            res.status = billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.MANUAL_INTERVENTION;
        }
        return updateTransactionWithResult(res, orderId);
    }))
        .catch(errorResponse => {
        logger_1.default.error(`Bill Payment : Retries exceeded for error in getting bill payment status : ${JSON.stringify(errorResponse)} for orderId : ${orderId}`);
        return updateTransactionDB(billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.WAITING, orderId, goBills_util_1.goBillsErrorTranslator(errorResponse));
    });
});
const createBillPaymentTransaction = (input, result = null, error) => billPaymentTransaction_service_1.default.createTransaction(goBills_payload_1.getTransactionPayload(input, result, error));
const submitPaymentRequest = (input) => goBills_repository_1.default.submitPayment(input.orderId, input.inquiryId);
const retrySubmit = (input) => __awaiter(void 0, void 0, void 0, function* () { return yield module_common_1.Util.retry(goBills_util_1.RetryConfig(), submitPaymentRequest, input); });
const isPaymentPending = (orderId) => __awaiter(void 0, void 0, void 0, function* () {
    const billPaymentStatus = yield getStatusFromGoBills(orderId);
    return billPaymentStatus.status === billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.WAITING;
});
const submitPayment = (input) => __awaiter(void 0, void 0, void 0, function* () {
    return retrySubmit(input)
        .then(result => {
        logger_1.default.info(`Bill Payment : Creating bill payment record for referenceId : ${input.inquiryId}`);
        createBillPaymentTransaction(input, result);
        goBills_util_1.debounceWhile(isPaymentPending, goBills_util_1.DEBOUNCE_MAX_ATTEMPTS, goBills_util_1.getInitialDelayForUpdate(), updateBillPaymentStatus, input.orderId);
    })
        .catch((errorResponse) => __awaiter(void 0, void 0, void 0, function* () {
        logger_1.default.error(`Bill Payment : Retries exceeded for error submitting bill payment status : ${JSON.stringify(errorResponse)} with payload : ${JSON.stringify(input)}`);
        yield createBillPaymentTransaction(input, null, goBills_util_1.goBillsErrorTranslator(errorResponse));
        updateCoreBanking(input.inquiryId, input.orderId, false);
    }));
});
const gobillsService = {
    submitPayment,
    updateBillPaymentStatus,
    updateTransactionWithResult,
    isPaymentPending
};
exports.default = logger_1.wrapLogs(gobillsService);
//# sourceMappingURL=goBills.service.js.map