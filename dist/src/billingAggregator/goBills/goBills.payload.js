"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const billPaymentTransaction_enum_1 = require("../../billPayment/billPaymentTransaction/billPaymentTransaction.enum");
exports.getTransactionPayload = (input, result, error) => ({
    orderId: input.orderId,
    transactionId: input.transactionId,
    referenceId: input.inquiryId,
    status: [
        {
            status: result
                ? billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.TRANSACTION_SUBMITTED
                : billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.TRANSACTION_SUBMIT_FAILED,
            reason: error,
            capturedAt: new Date()
        }
    ],
    accountNo: input.accountNo,
    createdAt: input.createdAt || new Date(),
    amount: input.amount
});
//# sourceMappingURL=goBills.payload.js.map