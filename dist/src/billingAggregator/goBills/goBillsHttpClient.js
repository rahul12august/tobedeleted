"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_httpclient_1 = require("@dk/module-httpclient");
const config_1 = require("../../config");
const logger_1 = __importDefault(require("../../logger"));
const context_1 = require("../../context");
const token = process.env.GOBILLS_BEARER_TOKEN;
const baseURL = config_1.config.get('billingAggregator').goBills;
const httpClient = module_httpclient_1.createHttpClient({
    baseURL,
    headers: {
        Authorization: `Bearer ${token}`
    },
    timeout: config_1.config.get('goBills').timeout,
    context: context_1.context,
    logger: logger_1.default
});
logger_1.default.info(`Setting up http-client with billing aggregator on address: ${baseURL}`);
exports.default = httpClient;
//# sourceMappingURL=goBillsHttpClient.js.map