"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    submitPayment: jest.fn(),
    updateBillPaymentStatus: jest.fn(),
    updateTransactionWithResult: jest.fn()
};
//# sourceMappingURL=goBills.service.js.map