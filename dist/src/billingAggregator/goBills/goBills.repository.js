"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../../logger"));
const lodash_1 = require("lodash");
const goBillsHttpClient_1 = __importDefault(require("./goBillsHttpClient"));
const goBills_constant_1 = require("./goBills.constant");
const circuitBreaker_1 = __importDefault(require("./circuitBreaker"));
const contextHandler_1 = require("../../common/contextHandler");
const log = (httpMethod, response) => {
    logger_1.default.debug(`Response from goBills ${httpMethod} payment ${response.status} and body ${JSON.stringify(response.data)}`);
};
const doSubmitPayment = (orderId, inquiryId) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Bill Payment : Executing goBills POST payment for orderId : ${orderId}, inquiryId : ${inquiryId}`);
    try {
        contextHandler_1.updateRequestIdInRedis(orderId);
        const result = yield goBillsHttpClient_1.default.post(goBills_constant_1.UrlConstants.POST_PAYMENT, {
            orderId: orderId,
            inquiryId: inquiryId
        });
        log('POST', result);
        const response = lodash_1.get(result, ['data', 'data']);
        logger_1.default.info(`doSubmitPayment: Response from goBills
        orderId: ${response.orderId},
        skuTag: ${response.skuTag}`);
        return response;
    }
    catch (err) {
        logger_1.default.error(`Bill Payment : Error in submitting bill payment status : ${JSON.stringify(err)} with orderId : ${orderId}, inquiryId : ${inquiryId}`);
        throw err;
    }
});
const doGetBillPaymentStatus = (orderId) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Bill Payment : Executing goBills GET payment status API : ${orderId}`);
    try {
        const response = yield goBillsHttpClient_1.default.get(`${goBills_constant_1.UrlConstants.GET_PAYMENT_STATUS}/${orderId}`);
        log('GET', response);
        const result = lodash_1.get(response, ['data', 'data'], []);
        logger_1.default.info(`doGetBillPaymentStatus: Response from goBills with
        orderId: ${result.orderId},
        skuTag: ${result.skuTag},
        status: ${result.status},
        refNumber: ${result.refNumber},
        failureReason: ${result.failureReason}`);
        return result;
    }
    catch (error) {
        logger_1.default.error(`Bill Payment : Error in goBills GET payment status API : ${error.message} and ${error.code} with orderId : ${orderId}`);
        throw error;
    }
});
const goBillsRepository = {
    submitPayment: circuitBreaker_1.default.create(doSubmitPayment),
    getBillPaymentStatus: circuitBreaker_1.default.create(doGetBillPaymentStatus)
};
exports.default = logger_1.wrapLogs(goBillsRepository);
//# sourceMappingURL=goBills.repository.js.map