"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = require("../logger");
const billingAggregator_service_1 = __importDefault(require("./billingAggregator.service"));
const module_common_1 = require("@dk/module-common");
const transaction_repository_1 = __importDefault(require("../transaction/transaction.repository"));
const module_common_2 = require("@dk/module-common");
const submitTransaction = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    const payload = {
        orderId: transactionModel.id,
        inquiryId: transactionModel.externalId,
        accountNo: transactionModel.beneficiaryAccountNo,
        transactionId: transactionModel.id,
        createdAt: transactionModel.createdAt || new Date(),
        amount: transactionModel.transactionAmount
    };
    yield transaction_repository_1.default.update(transactionModel.id, {
        thirdPartyOutgoingId: transactionModel.externalId
    });
    yield billingAggregator_service_1.default.submitPayment(payload);
    return transactionModel;
});
const isEligible = (model) => module_common_1.BankChannelEnum.GOBILLS === model.beneficiaryBankCodeChannel;
const shouldSendNotification = () => true;
const getRail = () => module_common_2.Rail.GOBILLS;
const submitPaymentTemplate = {
    submitTransaction,
    isEligible,
    getRail,
    shouldSendNotification
};
exports.default = logger_1.wrapLogs(submitPaymentTemplate);
//# sourceMappingURL=billingAggregatorSubmitPaymentTemplate.service.js.map