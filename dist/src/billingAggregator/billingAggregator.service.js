"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const goBills_service_1 = __importDefault(require("./goBills/goBills.service"));
const logger_1 = require("../logger");
const updateBillPaymentStatus = (orderId) => __awaiter(void 0, void 0, void 0, function* () { return yield goBills_service_1.default.updateBillPaymentStatus(orderId); });
const submitPayment = (input) => __awaiter(void 0, void 0, void 0, function* () { return yield goBills_service_1.default.submitPayment(input); });
const billingAggregatorService = { updateBillPaymentStatus, submitPayment };
exports.default = logger_1.wrapLogs(billingAggregatorService);
//# sourceMappingURL=billingAggregator.service.js.map