"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IrisPayoutStatusEnum;
(function (IrisPayoutStatusEnum) {
    IrisPayoutStatusEnum["QUEUED"] = "queued";
    IrisPayoutStatusEnum["PROCESSED"] = "processed";
    IrisPayoutStatusEnum["COMPLETED"] = "completed";
    IrisPayoutStatusEnum["FAILED"] = "failed";
})(IrisPayoutStatusEnum = exports.IrisPayoutStatusEnum || (exports.IrisPayoutStatusEnum = {}));
//# sourceMappingURL=iris.type.js.map