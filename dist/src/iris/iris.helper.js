"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const iris_constant_1 = require("./iris.constant");
const logger_1 = require("../logger");
exports.mapToIrisTransaction = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    return {
        /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
        beneficiary_name: transaction.beneficiaryAccountName,
        beneficiary_account: transaction.beneficiaryAccountNo,
        beneficiary_bank: transaction.beneficiaryIrisCode,
        amount: transaction.transactionAmount,
        notes: transaction.beneficiaryBankName == iris_constant_1.GOPAY_BENEFICIARY_BANK_NAME
            ? iris_constant_1.GOPAY_NOTES + transaction.thirdPartyOutgoingId
            : transaction.thirdPartyOutgoingId
    };
});
exports.default = logger_1.wrapLogs(exports.mapToIrisTransaction);
//# sourceMappingURL=iris.helper.js.map