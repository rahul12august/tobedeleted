"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const iris_validator_1 = require("./iris.validator");
const iris_service_1 = __importDefault(require("./iris.service"));
const contextHandler_1 = require("../common/contextHandler");
const confirmTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/iris/transaction-confirmation',
    options: {
        description: 'Confirm transaction from IRIS',
        notes: 'Private api called from IRIS to confirm transaction',
        tags: ['api', 'transactions'],
        auth: false,
        validate: {
            payload: iris_validator_1.confirmTransactionRequestValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { payload } = hapiRequest;
            yield contextHandler_1.updateRequestIdInContext(payload.reference_no);
            yield iris_service_1.default.confirmTransaction(payload);
            return hapiResponse.response().code(module_common_1.Http.StatusCode.NO_CONTENT);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'Ok'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const irisController = [confirmTransaction];
exports.default = irisController;
//# sourceMappingURL=iris.controller.js.map