"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const constant_1 = require("../common/constant");
const iris_constant_1 = require("./iris.constant");
const context_1 = require("../context");
const logger_1 = __importDefault(require("../logger"));
const module_common_1 = require("@dk/module-common");
const constant_2 = require("../common/constant");
const { baseUrl, timeout } = config_1.config.get('iris');
const apiKey = process.env.IRIS_API_KEY || '';
const httpClient = module_common_1.Http.createHttpClientForward({
    baseURL: baseUrl,
    headers: {
        [constant_1.HttpHeaders.CONTENT_TYPE]: 'application/json',
        [constant_1.HttpHeaders.ACCEPT]: 'application/json',
        [constant_1.HttpHeaders.AUTH]: `${iris_constant_1.BASIC_PREFIX} ${Buffer.from(apiKey).toString('base64')}:`
    },
    timeout: timeout,
    context: context_1.context,
    logger: logger_1.default,
    retryConfig: Object.assign(Object.assign({}, constant_2.retryConfig), { networkErrorCodesToRetry: ['ENOENT', 'ECONNREFUSED'] })
});
exports.default = httpClient;
//# sourceMappingURL=httpClient.js.map