"use strict";
/**
 * @module irisSubmissionTemplate handle outgoing transaction to switching
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const logger_1 = __importStar(require("../logger"));
const iris_repository_1 = __importDefault(require("./iris.repository"));
const iris_helper_1 = require("./iris.helper");
const transaction_util_1 = __importDefault(require("../transaction/transaction.util"));
const transaction_repository_1 = __importDefault(require("../transaction/transaction.repository"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const transaction_enum_1 = require("../transaction/transaction.enum");
const iris_type_1 = require("./iris.type");
const contextHandler_1 = require("../common/contextHandler");
const iris_service_1 = __importDefault(require("./iris.service"));
const submitTransaction = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const irisTransactionPayload = yield iris_helper_1.mapToIrisTransaction(transactionModel);
        const irisTransaction = yield transaction_util_1.default.submitTransactionWithRetry(iris_repository_1.default.submitTransaction, irisTransactionPayload, transactionModel.externalId);
        const status = irisTransaction.status === iris_type_1.IrisPayoutStatusEnum.FAILED
            ? transaction_enum_1.TransactionStatus.DECLINED
            : transaction_enum_1.TransactionStatus.SUBMITTED;
        if (irisTransaction.error) {
            const detail = 'Failed submission to iris!';
            logger_1.default.error(detail, irisTransaction.error);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.ERROR_FROM_IRIS);
        }
        if (irisTransaction.reference_no)
            contextHandler_1.updateRequestIdInRedis(irisTransaction.reference_no);
        if (irisTransaction.status === iris_type_1.IrisPayoutStatusEnum.FAILED) {
            yield iris_service_1.default.declineTransaction(transactionModel, new AppError_1.TransferAppError('Decline transaction because iris transaction status is FAILED', transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.INVALID_TRANSACTION_STATUS));
        }
        const updatedTransactionModel = yield transaction_repository_1.default.update(transactionModel.id, {
            thirdPartyOutgoingId: irisTransaction.reference_no,
            status
        });
        if (!updatedTransactionModel) {
            const detail = `Failed to update transaction info for txnId: ${transactionModel.id}!`;
            logger_1.default.error(`submitTransaction: ${detail}`);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION);
        }
        return updatedTransactionModel;
    }
    catch (error) {
        logger_1.default.error('failed submission to iris after all retry or non-retriable condition', error);
        throw error;
    }
});
const isEligible = (model) => {
    if (module_common_1.BankChannelEnum.IRIS === model.beneficiaryBankCodeChannel &&
        model.beneficiaryAccountNo) {
        return true;
    }
    return false;
};
const shouldSendNotification = () => true;
const getRail = () => module_common_1.Rail.IRIS;
const irisSubmissionTemplate = {
    submitTransaction,
    isEligible,
    getRail,
    shouldSendNotification
};
exports.default = logger_1.wrapLogs(irisSubmissionTemplate);
//# sourceMappingURL=irisSubmissionTemplate.service.js.map