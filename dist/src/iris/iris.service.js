"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const AppError_1 = require("../errors/AppError");
const logger_1 = __importStar(require("../logger"));
const transaction_enum_1 = require("../transaction/transaction.enum");
const iris_enum_1 = require("./iris.enum");
const transactionConfirmation_service_1 = __importDefault(require("../transaction/transactionConfirmation.service"));
const errors_1 = require("../common/errors");
const transactionBlocking_helper_1 = __importDefault(require("../transaction/transactionBlocking.helper"));
const transactionExecution_service_1 = __importStar(require("../transaction/transactionExecution.service"));
const confirmTransaction = (input) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`IRIS: Transaction Confirmation with external id: ${input.reference_no} status : ${input.status}`);
    let responseCode;
    switch (input.status) {
        case iris_enum_1.ConfirmTransactionStatusEnum.FAILED:
        case iris_enum_1.ConfirmTransactionStatusEnum.REJECTED:
            responseCode = transaction_enum_1.ConfirmTransactionStatus.ERROR_EXPECTED;
            break;
        case iris_enum_1.ConfirmTransactionStatusEnum.COMPLETED:
            responseCode = transaction_enum_1.ConfirmTransactionStatus.SUCCESS;
            break;
        case iris_enum_1.ConfirmTransactionStatusEnum.APPROVED:
        case iris_enum_1.ConfirmTransactionStatusEnum.PROCESSED:
        case iris_enum_1.ConfirmTransactionStatusEnum.QUEUED:
            return;
        default:
            const detail = `Unsupported confirm transaction status: ${input.status}!`;
            logger_1.default.error(`confirmTransaction: ${detail}`);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.IRIS, errors_1.ERROR_CODE.UNSUPPORTED_CONFIRM_TRANSACTION_STATUS);
    }
    const transactionInput = {
        externalId: input.reference_no,
        amount: Number(input.amount),
        responseCode,
        responseMessage: input.error_message
    };
    yield transactionConfirmation_service_1.default.confirmTransaction(transactionInput);
});
const declineTransaction = (updatedTransactionModel, error) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`declineTransaction: transaction with status: ${updatedTransactionModel.status}, 
                thirdpartyOutgoingId: ${updatedTransactionModel.thirdPartyOutgoingId}`);
    yield transactionBlocking_helper_1.default.cancelBlockingTransaction(updatedTransactionModel);
    yield transactionExecution_service_1.default.onFailedTransaction(updatedTransactionModel, transactionExecution_service_1.defaultExecutionHooks, error);
    return;
});
const irisService = logger_1.wrapLogs({
    confirmTransaction,
    declineTransaction
});
exports.default = irisService;
//# sourceMappingURL=iris.service.js.map