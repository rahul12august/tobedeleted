"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ConfirmTransactionStatusEnum;
(function (ConfirmTransactionStatusEnum) {
    ConfirmTransactionStatusEnum["PROCESSED"] = "processed";
    ConfirmTransactionStatusEnum["COMPLETED"] = "completed";
    ConfirmTransactionStatusEnum["FAILED"] = "failed";
    ConfirmTransactionStatusEnum["APPROVED"] = "approved";
    ConfirmTransactionStatusEnum["REJECTED"] = "rejected";
    ConfirmTransactionStatusEnum["QUEUED"] = "queued";
})(ConfirmTransactionStatusEnum = exports.ConfirmTransactionStatusEnum || (exports.ConfirmTransactionStatusEnum = {}));
//# sourceMappingURL=iris.enum.js.map