"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const httpClient_1 = __importDefault(require("./httpClient"));
const logger_1 = __importStar(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const constant_1 = require("../common/constant");
const v4_1 = __importDefault(require("uuid/v4"));
const transaction_enum_1 = require("../transaction/transaction.enum");
const submitTransaction = (payload, idempotencyKey) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const payouts = {
            payouts: [payload]
        };
        const externalKey = idempotencyKey || v4_1.default();
        logger_1.default.info(`Submitting IRIS transaction ExternalKey : ${externalKey}, 
        beneficiary_account: ${payload.beneficiary_account},
        beneficiary_bank: ${payload.beneficiary_bank}`);
        const options = {
            headers: {
                [constant_1.HttpHeaders.X_IDEMPOTENCY_KEY]: externalKey
            }
        };
        const result = yield httpClient_1.default.post(`/api/v1/payouts`, payouts, options);
        if (!result.data || !result.data.payouts) {
            let detail = 'Error from IRIS while transferring funds!';
            logger_1.default.error(` submitTransaction: ${detail}`);
            throw new AppError_1.TransferAppError(detail, result.status == 500
                ? transaction_enum_1.TransferFailureReasonActor.IRIS
                : transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.ERROR_FROM_IRIS);
        }
        logger_1.default.info(`Submit IRIS transaction response : ${JSON.stringify(result.data.payouts[0])}`);
        return result.data.payouts[0];
    }
    catch (error) {
        const detail = 'Error from IRIS while transferring funds!';
        logger_1.default.error(`submitTransaction catch: ${detail}`, error);
        if (error instanceof AppError_1.TransferAppError)
            throw error;
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.ERROR_FROM_IRIS);
    }
});
const irisRepository = logger_1.wrapLogs({
    submitTransaction
});
exports.default = irisRepository;
//# sourceMappingURL=iris.repository.js.map