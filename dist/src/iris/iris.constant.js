"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BASIC_PREFIX = 'Basic';
exports.default = {
    MAX_LENGTH_TRANSACTION_RESPONSE_ID: 12,
    MAX_LENGTH_IRIS_REFERENCE_NO: 40,
    MAX_LENGTH_IRIS_ERROR_MESSAGE: 100,
    MAX_LENGTH_IRIS_ERROR_CODE: 3
};
exports.GOPAY_NOTES = 'TopUp via Jago ';
exports.GOPAY_BENEFICIARY_BANK_NAME = 'GoPay';
//# sourceMappingURL=iris.constant.js.map