"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("@hapi/joi"));
const iris_constant_1 = __importDefault(require("./iris.constant"));
const module_common_1 = require("@dk/module-common");
const iris_enum_1 = require("./iris.enum");
/* eslint-disable @typescript-eslint/camelcase */
exports.confirmTransactionRequestValidator = joi_1.default.object({
    reference_no: joi_1.default.string()
        .required()
        .max(iris_constant_1.default.MAX_LENGTH_IRIS_REFERENCE_NO)
        .description('Transaction reference no from IRIS'),
    amount: module_common_1.JoiValidator.requiredString().description('Transaction amount'),
    status: module_common_1.JoiValidator.requiredString().valid(Object.values(iris_enum_1.ConfirmTransactionStatusEnum)).description(`Trasnaction status 
      * PROCESSED
      * COMPLETED
      * FAILED
      * APPROVED
      * REJECTED
      * QUEUED`),
    updated_at: joi_1.default.date().description('Transaction updated date'),
    error_code: module_common_1.JoiValidator.optionalString()
        .max(iris_constant_1.default.MAX_LENGTH_IRIS_ERROR_CODE)
        .description('Error code from IRIS'),
    error_message: module_common_1.JoiValidator.optionalString()
        .max(iris_constant_1.default.MAX_LENGTH_IRIS_ERROR_MESSAGE)
        .description('Error message from IRIS')
}).label('confirmTransactionRequestValidator');
//# sourceMappingURL=iris.validator.js.map