"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const faker_1 = __importDefault(require("faker"));
const iris_type_1 = require("../iris.type");
const iris_enum_1 = require("../iris.enum");
exports.getIrisTransactionData = () => ({
    status: iris_type_1.IrisPayoutStatusEnum.QUEUED,
    reference_no: faker_1.default.random.uuid()
});
exports.getIrisTransactionPayload = () => ({
    /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
    beneficiary_name: 'beneficiary_name',
    beneficiary_account: 'beneficiary_account',
    beneficiary_bank: 'beneficiary_bank',
    amount: 1000,
    notes: 'notes'
});
exports.mockConfirmTransactionRequest = () => {
    return {
        reference_no: '0093384848',
        amount: '1000.0',
        status: iris_enum_1.ConfirmTransactionStatusEnum.COMPLETED,
        error_code: undefined,
        error_message: undefined
    };
};
//# sourceMappingURL=iris.data.js.map