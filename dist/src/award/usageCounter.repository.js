"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const httpClient_1 = __importDefault(require("./httpClient"));
const pathUsageCounters = '/usage-counters';
const getUsageCounters = (customerId, limitGroupCodes) => __awaiter(void 0, void 0, void 0, function* () {
    const result = yield httpClient_1.default.get(`/customers/${customerId}/usage-counters`, {
        params: {
            limitGroupCodes: JSON.stringify(limitGroupCodes)
        }
    });
    return lodash_1.get(result, ['data', 'data'], []);
});
const updateUsageCounters = (customerId, data) => __awaiter(void 0, void 0, void 0, function* () {
    const { limitGroupCode, adjustmentLimitAmount } = data;
    yield httpClient_1.default.post(`${pathUsageCounters}`, {
        limitGroupCode,
        adjustmentPoint: 1,
        adjustmentLimitAmount
    }, {
        params: {
            customerId
        }
    });
});
const usageCounterRepository = {
    getUsageCounters,
    updateUsageCounters
};
exports.default = usageCounterRepository;
//# sourceMappingURL=usageCounter.repository.js.map