"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const httpClient_1 = __importDefault(require("./httpClient"));
const logger_1 = __importStar(require("../logger"));
const pathUsageCounters = '/private/usage-counters';
const getUsageCounters = (customerId, limitGroupCodes) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getUsageCounters repository: get usage counter for customerId: ${customerId}.`);
    const result = yield httpClient_1.default.get(`/private/customers/${customerId}/usage-counters`, limitGroupCodes
        ? {
            params: {
                limitGroupCodes: JSON.stringify(limitGroupCodes)
            }
        }
        : {});
    return lodash_1.get(result, ['data', 'data'], []);
});
const getThresholdCounter = (customerId, groupType) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getThresholdCounter repository: get threshold counter for customerId: ${customerId} and groupType: ${groupType}.`);
    const result = yield httpClient_1.default.get(`/private/customers/${customerId}/threshold-counter/${groupType}`);
    return lodash_1.get(result, ['data', 'data', 'thresholdCounter'], {});
});
const updateUsageCounters = (customerId, data) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`updateUsageCounters repository: update usage counter for customerId: ${customerId}.`);
    const payload = data.map(updateCounterPayload => ({
        limitGroupCode: updateCounterPayload.limitGroupCode,
        adjustmentPoint: 1,
        adjustmentLimitAmount: updateCounterPayload.adjustmentLimitAmount
    }));
    yield httpClient_1.default.put(`${pathUsageCounters}`, payload, {
        params: {
            customerId
        }
    });
});
const awardRepository = logger_1.wrapLogs({
    getUsageCounters,
    getThresholdCounter,
    updateUsageCounters
});
exports.default = awardRepository;
//# sourceMappingURL=award.repository.js.map