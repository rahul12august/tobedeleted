"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const faker_1 = __importDefault(require("faker"));
exports.getUsageCountersData = () => {
    return [
        {
            limitGroupCode: 'L001',
            dailyAccumulationAmount: 2322,
            monthlyAccumulationAmount: 2322,
            totalAccumulationAmount: 2322,
            dailyAccumulationTransaction: 100,
            monthlyAccumulationTransaction: 100,
            totalAccumulationTransaction: 100
        }
    ];
};
exports.anyCustomerId = () => {
    return faker_1.default.random.alphaNumeric(20);
};
exports.anyCode = () => {
    return faker_1.default.random.alphaNumeric(8);
};
exports.anyGroupType = () => {
    return faker_1.default.random.arrayElement([
        'Bonus_Transfer',
        'Bonus_Overseas_CashWithdrawal',
        'Bonus_Local_CashWithdrawal'
    ]);
};
exports.usageCountersInput = () => {
    return {
        limitGroupCode: 'L001',
        adjustmentPoint: 1,
        adjustmentLimitAmount: 10000
    };
};
exports.getThresholdCountersData = () => {
    return { thresholdCounter: 'AWARD_TRANSFER_001' };
};
//# sourceMappingURL=award.data.js.map