"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const award_repository_1 = __importDefault(require("./award.repository"));
const logger_1 = __importStar(require("../logger"));
const getUsageCounter = (customerId, limitGroupCode) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getUsageCounter service: get usage counter for customerId: ${customerId}.`);
    const usageCounters = yield award_repository_1.default.getUsageCounters(customerId, [
        limitGroupCode
    ]);
    return (usageCounters && usageCounters[0]) || null;
});
const getThresholdCounter = (customerId, groupType) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getThresholdCounter service: get threshold counter for customerId: ${customerId} groupType: ${groupType}.`);
    const thresholdCounters = yield award_repository_1.default.getThresholdCounter(customerId, groupType);
    return thresholdCounters;
});
const awardService = logger_1.wrapLogs({
    getUsageCounter,
    getThresholdCounter
});
exports.default = awardService;
//# sourceMappingURL=award.service.js.map