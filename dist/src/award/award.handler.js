"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const errors_1 = require("../common/errors");
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const AppError_1 = require("../errors/AppError");
const logger_1 = __importDefault(require("../logger"));
const transaction_enum_1 = require("../transaction/transaction.enum");
class AwardHandler {
    generateMissingUsageCounterBasedOnTransactionCodes(params) {
        return __awaiter(this, void 0, void 0, function* () {
            //return as is
            return params.usageCounters;
        });
    }
    validateFeeRuleObject(feeRuleObject) {
        return __awaiter(this, void 0, void 0, function* () {
            logger_1.default.info(`[AwardHandler] validateFeeRuleObject: 
      monthlyNoTransaction: ${feeRuleObject.monthlyNoTransaction}
      counterCode: ${feeRuleObject.counterCode}
    `);
            if (typeof feeRuleObject.monthlyNoTransaction !== 'number' ||
                lodash_1.isUndefined(feeRuleObject.counterCode)) {
                const detail = `monthlyNoTransaction or counter code not found!`;
                logger_1.default.error(`[AwardHandler] validateFeeRuleObject: ${detail}`);
                throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_ENTITLEMENT, errors_1.ERROR_CODE.TRANSFER_FEE_RULE_INVALID_MANDATORY_FIELDS);
            }
            logger_1.default.info(` [AwardHandler] validateFeeRuleObject: Counter code: ${feeRuleObject.counterCode}`);
            const counter = yield configuration_repository_1.default.getCounterByCode(feeRuleObject.counterCode);
            if (lodash_1.isEmpty(counter) || (counter && lodash_1.isUndefined(counter.value))) {
                const detail = `Counter code ${feeRuleObject.counterCode} not found!`;
                logger_1.default.error(`[AwardHandler] validateFeeRuleObject: ${detail}`);
                throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_ENTITLEMENT, errors_1.ERROR_CODE.TRANSFER_FEE_RULE_INVALID_REGISTER_PARAMETER);
            }
            logger_1.default.info(`[AwardHandler] validateFeeRuleObject: monthlyNoTransaction: ${feeRuleObject.monthlyNoTransaction}`);
            //in most cases
            //basicFeeCode1 is for free transaction
            //basicFeeCode2 is for paid transaction
            return {
                usedQuota: feeRuleObject.monthlyNoTransaction,
                quota: counter ? counter.value : 0,
                freeFeeCode: feeRuleObject.basicFeeCode1,
                paidFeeCode: feeRuleObject.basicFeeCode2
            };
        });
    }
    determineBasicFeeMapping(feeRuleObject) {
        return __awaiter(this, void 0, void 0, function* () {
            const { usedQuota, quota, freeFeeCode, paidFeeCode } = yield this.validateFeeRuleObject(feeRuleObject);
            logger_1.default.info(`[AwardHandler] determineBasicFeeMapping: 
        quota: [${quota}]
        usedQuota: [${usedQuota}]
      `);
            const basicFeeMapping = usedQuota <= quota ? freeFeeCode : paidFeeCode;
            return basicFeeMapping;
        });
    }
    preProcessCustomerEntitlementBasedOnTransactionCodes() {
        return __awaiter(this, void 0, void 0, function* () {
            logger_1.default.info(`[AwardHandler] preProcessCustomerEntitlementBasedOnTransactionCodes - no preprocessing
      `);
            //no preprocessing for awards
            return null;
        });
    }
    mapEntitlementUsageCounterByLimitGroupCode(usageCounters) {
        logger_1.default.info(`[AwardHandler] mapEntitlementUsageCounterByLimitGroupCode - no mapping required [return as is]
      `);
        //just return as it is
        return usageCounters;
    }
    processFeeRuleInfo(currentFeeRuleInfo) {
        logger_1.default.info(`[AwardHandler] processFeeRuleInfo - return as is
      `);
        //return as is
        return currentFeeRuleInfo;
    }
}
exports.AwardHandler = AwardHandler;
//# sourceMappingURL=award.handler.js.map