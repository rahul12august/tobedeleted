"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const award_handler_1 = require("../award/award.handler");
const entitlement_handler_1 = require("../entitlement/entitlement.handler");
const logger_1 = __importDefault(require("../logger"));
const customerEntitlement_manager_1 = require("./customerEntitlement.manager");
const lodash_1 = require("lodash");
class EntitlementManager extends customerEntitlement_manager_1.CustomerEntitlementManager {
    constructor(isEntitlementEnabled, customerEntitlementHandlerInstance = new entitlement_handler_1.EntitlementHandler(), customerEntitlementHandlerFallbackInstance = new award_handler_1.AwardHandler()) {
        super();
        super.isEntitlementEnabled = isEntitlementEnabled;
        super.customerEntitlementHandlerInstance = customerEntitlementHandlerInstance;
        //this is the default
        super.customerEntitlementHandlerFallbackInstance = customerEntitlementHandlerFallbackInstance;
    }
    getHandlerInstanceProcessFeeRuleInfo(_, feeRuleType, awardGroupCounter, usageCounters) {
        const isWithEntitlement = !lodash_1.isUndefined(awardGroupCounter) &&
            !lodash_1.isUndefined(feeRuleType) &&
            !lodash_1.isUndefined(usageCounters);
        logger_1.default.info(`[EntitlementManager] handlerInstanceValidationProcessFeeRuleInfo isWithEntitlement: [${isWithEntitlement}]`);
        if (isWithEntitlement) {
            return this.customerEntitlementHandlerInstance;
        }
        else {
            return this.customerEntitlementHandlerFallbackInstance;
        }
    }
    getHandlerInstanceDetermineBasicFeeMapping(currentFeeRuleInfo) {
        const { quota, isQuotaExceeded, monthlyNoTransaction: usedQuota } = currentFeeRuleInfo;
        const isWithEntitlement = !lodash_1.isUndefined(usedQuota) &&
            !lodash_1.isUndefined(quota) &&
            !lodash_1.isUndefined(isQuotaExceeded);
        logger_1.default.info(`[EntitlementManager]: handlerInstanceValidationDetermineBasicFeeMapping isWithEntitlement: [${isWithEntitlement}]`);
        if (isWithEntitlement) {
            return this.customerEntitlementHandlerInstance;
        }
        else {
            return this.customerEntitlementHandlerFallbackInstance;
        }
    }
}
exports.EntitlementManager = EntitlementManager;
class AwardManager extends customerEntitlement_manager_1.CustomerEntitlementManager {
    toUseHandlerFallbackInstance() {
        throw new Error('Method not implemented.');
    }
    constructor(isEntitlementEnabled, customerEntitlementHandlerInstance = new award_handler_1.AwardHandler()) {
        super();
        super.isEntitlementEnabled = isEntitlementEnabled;
        super.customerEntitlementHandlerInstance = customerEntitlementHandlerInstance;
        super.customerEntitlementHandlerFallbackInstance = null;
    }
    getHandlerInstanceProcessFeeRuleInfo() {
        return this.customerEntitlementHandlerInstance;
    }
    getHandlerInstanceDetermineBasicFeeMapping() {
        return this.customerEntitlementHandlerInstance;
    }
}
exports.AwardManager = AwardManager;
//# sourceMappingURL=customerEntitlement.manager.impl.js.map