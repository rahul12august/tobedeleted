"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const entitlementFlag_1 = __importDefault(require("../common/entitlementFlag"));
const customerEntitlement_manager_impl_1 = require("./customerEntitlement.manager.impl");
class CustomerEntitlementManagerFactory {
    static getCustomerEntitlementManagerByFlag() {
        return __awaiter(this, void 0, void 0, function* () {
            const isEntitlementEnabled = yield entitlementFlag_1.default.isEnabled();
            if (this.customerEntitlementManager == null) {
                if (isEntitlementEnabled) {
                    this.customerEntitlementManager = new customerEntitlement_manager_impl_1.EntitlementManager(isEntitlementEnabled);
                }
                else {
                    this.customerEntitlementManager = new customerEntitlement_manager_impl_1.AwardManager(isEntitlementEnabled);
                }
            }
            return this.customerEntitlementManager;
        });
    }
    //for jest to reset singleton instance
    static resetCustomerEntitlementManager() {
        this.customerEntitlementManager = null;
    }
}
exports.CustomerEntitlementManagerFactory = CustomerEntitlementManagerFactory;
CustomerEntitlementManagerFactory.customerEntitlementManager = null;
//# sourceMappingURL=customerEntitlement.manager.factory.js.map