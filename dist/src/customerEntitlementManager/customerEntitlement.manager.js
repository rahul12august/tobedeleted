"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const award_handler_1 = require("../award/award.handler");
const entitlement_handler_1 = require("../entitlement/entitlement.handler");
class CustomerEntitlementManager {
    constructor() {
        this.isEntitlementEnabled = false;
        this.customerEntitlementHandlerInstance = null;
        this.customerEntitlementHandlerFallbackInstance = null;
    }
    preProcessCustomerEntitlementBasedOnTransactionCodes(params) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.customerEntitlementHandlerInstance !== null) {
                return this.customerEntitlementHandlerInstance.preProcessCustomerEntitlementBasedOnTransactionCodes(params);
            }
            return null;
        });
    }
    mapEntitlementUsageCounterByLimitGroupCode(usageCounters, entitlementResultResponse) {
        if (this.customerEntitlementHandlerInstance !== null) {
            return this.customerEntitlementHandlerInstance.mapEntitlementUsageCounterByLimitGroupCode(usageCounters, entitlementResultResponse);
        }
        //return as is
        return usageCounters;
    }
    determineBasicFeeMapping(feeRuleObject) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.customerEntitlementHandlerInstance !== null) {
                const handler = this.getHandlerInstanceDetermineBasicFeeMapping(feeRuleObject);
                if (handler !== null) {
                    return yield handler.determineBasicFeeMapping(feeRuleObject);
                }
            }
            return Promise.resolve(undefined);
        });
    }
    processFeeRuleInfo(currentFeeRuleInfo, feeRuleType, awardGroupCounter, usageCounters) {
        if (this.customerEntitlementHandlerInstance !== null) {
            const handler = this.getHandlerInstanceProcessFeeRuleInfo(currentFeeRuleInfo, feeRuleType, awardGroupCounter, usageCounters);
            if (handler !== null) {
                return handler.processFeeRuleInfo(currentFeeRuleInfo, feeRuleType, awardGroupCounter, usageCounters);
            }
        }
        //return as is
        return currentFeeRuleInfo;
    }
    generateMissingUsageCounterBasedOnTransactionCodes(params) {
        if (this.customerEntitlementHandlerInstance !== null) {
            return this.customerEntitlementHandlerInstance.generateMissingUsageCounterBasedOnTransactionCodes(params);
        }
        //return as is
        return params.usageCounters;
    }
    getCurrentInstanceType() {
        return this.constructor.name;
    }
    getCurrentInstanceHandlerTypeDesc() {
        if (this.customerEntitlementHandlerInstance != null) {
            if (this.customerEntitlementHandlerInstance instanceof entitlement_handler_1.EntitlementHandler) {
                return 'CustomerEntitlementManagerFactory: using EntitlementManager - EntitlementHandler';
            }
            else if (this.customerEntitlementHandlerInstance instanceof award_handler_1.AwardHandler) {
                return 'CustomerEntitlementManagerFactory: using AwardManager - AwardHandler';
            }
            return 'CustomerEntitlementManagerFactory: no instance type detected';
        }
        return 'CustomerEntitlementManagerFactory: handler is null';
    }
}
exports.CustomerEntitlementManager = CustomerEntitlementManager;
//# sourceMappingURL=customerEntitlement.manager.js.map