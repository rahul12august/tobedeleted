"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const fee_constant_1 = require("./fee.constant");
const module_common_1 = require("@dk/module-common");
const transaction_enum_1 = require("../transaction/transaction.enum");
exports.fixedSubsidiary = logger_1.wrapLogs({
    isApplicable: (input) => typeof input.basicFee.subsidiaryAmount === 'number',
    getFee: (input) => {
        const amount = input.basicFee.subsidiaryAmount;
        if (amount === null || amount === undefined) {
            const detail = 'Invalid subsidiary amount in fee rule!';
            logger_1.default.error(detail);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_SUBSIDIARY_VALUE);
        }
        return amount;
    }
});
exports.percentageSubsidiary = logger_1.wrapLogs({
    isApplicable: (input) => typeof input.basicFee.subsidiaryAmountPercentage === 'number',
    getFee: (input) => {
        const percentage = input.basicFee.subsidiaryAmountPercentage;
        if (percentage === null || percentage === undefined) {
            const detail = 'Invalid subsidiary percentage in fee rule!';
            logger_1.default.error(detail);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_SUBSIDIARY_VALUE);
        }
        const grossAmount = module_common_1.Formater.roundUp(input.transactionAmount / (1 - percentage / fee_constant_1.PERCENTAGE));
        return grossAmount - input.transactionAmount;
    }
});
exports.subsidiaryTemplates = () => [
    exports.fixedSubsidiary,
    exports.percentageSubsidiary
];
//# sourceMappingURL=subsidiary.template.js.map