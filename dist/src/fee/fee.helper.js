"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const decisionRules_1 = __importDefault(require("./decisionRules"));
const logger_1 = __importStar(require("../logger"));
const AppError_1 = require("../errors/AppError");
const transaction_enum_1 = require("../transaction/transaction.enum");
const errors_1 = require("../common/errors");
const fee_template_1 = require("./fee.template");
const subsidiary_template_1 = require("./subsidiary.template");
const getFeeRuleTemplatePayload = (input, basicFeeMap) => {
    return {
        basicFee: basicFeeMap.basicFee,
        transactionAmount: input.transactionAmount,
        feeAmount: input.feeAmount,
        externalId: input.externalId,
        beneficiaryBankCodeChannel: input.beneficiaryBankCodeChannel,
        refund: input.refund
    };
};
const getFeeMappings = (input) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const feeRule = yield decisionRules_1.default.getFeeRule(input.feeRuleCode, input.transactionAmount, input.monthlyNoTransaction, input.interchange, input.targetBankCode, input.thresholdCounter, input.customerId, input.awardGroupCounter, input.usageCounters);
    if (!feeRule) {
        const detail = `No fee rule found for feeRuleCode: ${input.feeRuleCode}!`;
        logger_1.default.error(detail);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.INVALID_REGISTER_PARAMETER);
    }
    return _a = feeRule.basicFeeMapping, (_a !== null && _a !== void 0 ? _a : []);
});
const getApplicableTemplate = (feeRuleTemplatePayload) => {
    const template = fee_template_1.feeTemplates().find(tmpl => tmpl.isApplicable(feeRuleTemplatePayload));
    if (!template) {
        const detail = 'Fee template could not be found for fee rule!';
        logger_1.default.error(detail, feeRuleTemplatePayload);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.MISSING_FEE_RULE);
    }
    return template;
};
const getSubsidiaryTemplate = (feeRuleTemplatePayload) => {
    const template = subsidiary_template_1.subsidiaryTemplates().find(tmpl => tmpl.isApplicable(feeRuleTemplatePayload));
    if (!template) {
        const detail = 'Subsidiary template could not be found for fee rule!';
        logger_1.default.error(detail, feeRuleTemplatePayload);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.MISSING_SUBSIDIARY_RULE);
    }
    return template;
};
const calculateFeeAmount = (feePayload) => __awaiter(void 0, void 0, void 0, function* () { return getApplicableTemplate(feePayload).getFee(feePayload); });
const calculateSubsidiaryAmount = (feePayload) => __awaiter(void 0, void 0, void 0, function* () {
    return feePayload.basicFee.subsidiary
        ? getSubsidiaryTemplate(feePayload).getFee(feePayload)
        : 0;
});
const getFeeAmount = (input, basicFeeMapping) => __awaiter(void 0, void 0, void 0, function* () {
    {
        const { basicFee, basicFeeCode } = basicFeeMapping;
        const feePayload = getFeeRuleTemplatePayload(input, basicFeeMapping);
        const fee = yield calculateFeeAmount(feePayload);
        const subsidiaryAmount = yield calculateSubsidiaryAmount(feePayload);
        return {
            feeCode: basicFeeCode,
            interchange: basicFeeMapping.interchange,
            feeAmount: fee,
            customerTc: basicFee.customerTc,
            debitGlNumber: basicFee.debitGlNumber || '',
            creditGlNumber: basicFee.creditGlNumber || '',
            subsidiary: basicFee.subsidiary,
            subsidiaryAmount: subsidiaryAmount,
            customerTcChannel: basicFee.customerTcInfo.channel
        };
    }
});
const feeHelper = {
    getFeeRuleTemplatePayload,
    getFeeMappings,
    getFeeAmount
};
exports.default = logger_1.wrapLogs(feeHelper);
//# sourceMappingURL=fee.helper.js.map