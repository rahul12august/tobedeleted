"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PERCENTAGE = 100;
exports.BIFAST_FEE_THRESHOULD_AMOUNT = 50000000;
var COUNTER;
(function (COUNTER) {
    COUNTER["COUNTER_01"] = "COUNTER_01";
    COUNTER["COUNTER_07"] = "COUNTER_07";
    COUNTER["COUNTER_08"] = "COUNTER_08";
    COUNTER["COUNTER_09"] = "COUNTER_09";
    COUNTER["COUNTER_MFS_01"] = "COUNTER_MFS_01";
})(COUNTER = exports.COUNTER || (exports.COUNTER = {}));
var FeeRuleType;
(function (FeeRuleType) {
    FeeRuleType["ZERO_FEE_RULE"] = "ZERO_FEE_RULE";
    FeeRuleType["RTOL_TRANSFER_FEE_RULE"] = "RTOL_TRANSFER_FEE_RULE";
    FeeRuleType["LOCAL_ATM_WITHDRAWAL_RULE"] = "LOCAL_ATM_WITHDRAWAL_RULE";
    FeeRuleType["GOPAY_TOP_UP_RULE"] = "GOPAY_TOP_UP_RULE";
    FeeRuleType["RTOL_TRANSFER_ON_OTHER_ATM_FEE_RULE"] = "RTOL_TRANSFER_ON_OTHER_ATM_FEE_RULE";
    FeeRuleType["OVERSEAS_ATM_WITHDRAWAL_RULE"] = "OVERSEAS_ATM_WITHDRAWAL_RULE";
    FeeRuleType["LOCAL_BALANCE_INQUIRY_FEE"] = "LOCAL_BALANCE_INQUIRY_FEE";
    FeeRuleType["OVERSEAS_BALANCE_INQUIRY_FEE"] = "OVERSEAS_BALANCE_INQUIRY_FEE";
    FeeRuleType["PENALTY_FEE"] = "PENALTY_FEE";
    FeeRuleType["BILL_PAYMENT_FEE"] = "BILL_PAYMENT_FEE";
    FeeRuleType["WALLET_FEE_RULE"] = "WALLET_FEE_RULE";
    FeeRuleType["IRIS_TRANSFER_RULE"] = "IRIS_TRANSFER_RULE";
    FeeRuleType["GENERAL_REFUND_FEE_RULE"] = "GENERAL_REFUND_FEE_RULE";
    FeeRuleType["OFFER_TAX_RULE"] = "OFFER_TAX_RULE";
    FeeRuleType["CASHBACK_TAX_RULE"] = "CASHBACK_TAX_RULE";
    FeeRuleType["SKN_TRANSFER_FEE_RULE"] = "SKN_TRANSFER_FEE_RULE";
    FeeRuleType["RTGS_TRANSFER_FEE_RULE"] = "RTGS_TRANSFER_FEE_RULE";
    FeeRuleType["BRANCH_SKN_TRANSFER_FEE_RULE"] = "BRANCH_SKN_TRANSFER_FEE_RULE";
    FeeRuleType["BRANCH_RTGS_TRANSFER_FEE_RULE"] = "BRANCH_RTGS_TRANSFER_FEE_RULE";
    FeeRuleType["SKN_SHARIA_FEE_RULE"] = "SKN_SHARIA_FEE_RULE";
    FeeRuleType["RTGS_SHARIA_FEE_RULE"] = "RTGS_SHARIA_FEE_RULE";
    FeeRuleType["DIGITAL_GOODS_FEE"] = "DIGITAL_GOODS_FEE";
    FeeRuleType["RTOL_TRANSFER_FOR_BUSINESS_FEE_RULE"] = "RTOL_TRANSFER_FOR_BUSINESS_FEE_RULE";
    FeeRuleType["SKN_TRANSFER_FOR_BUSINESS_FEE_RULE"] = "SKN_TRANSFER_FOR_BUSINESS_FEE_RULE";
    FeeRuleType["RTGS_TRANSFER_FOR_BUSINESS_FEE_RULE"] = "RTGS_TRANSFER_FOR_BUSINESS_FEE_RULE";
    FeeRuleType["BIFAST_FEE_RULE"] = "BIFAST_FEE_RULE";
    FeeRuleType["BIFAST_SHARIA_FEE_RULE"] = "BIFAST_SHARIA_FEE_RULE";
    FeeRuleType["INTEREST_TAX_RULE"] = "INTEREST_TAX_RULE";
    FeeRuleType["BIFAST_TRANSFER_FOR_BUSINESS_FEE_RULE"] = "BIFAST_TRANSFER_FOR_BUSINESS_FEE_RULE";
})(FeeRuleType = exports.FeeRuleType || (exports.FeeRuleType = {}));
exports.feeRuleMapping = {
    [FeeRuleType.ZERO_FEE_RULE]: {
        description: 'Zero Fee Rule'
    },
    [FeeRuleType.RTOL_TRANSFER_FEE_RULE]: {
        description: 'RTOL Transfer Fee Rule'
    },
    [FeeRuleType.LOCAL_ATM_WITHDRAWAL_RULE]: {
        description: 'Local ATM Withdrawal Rule'
    },
    [FeeRuleType.GOPAY_TOP_UP_RULE]: {
        description: 'Gopay Top up Rule',
        basicFeeMapping: [
            {
                basicFeeCode: 'BF001'
            }
        ]
    },
    [FeeRuleType.RTOL_TRANSFER_ON_OTHER_ATM_FEE_RULE]: {
        description: 'RTOL Transfer on other ATM Fee Rule'
    },
    [FeeRuleType.OVERSEAS_ATM_WITHDRAWAL_RULE]: {
        description: 'Overseas ATM Withdrawal Rule',
        basicFeeMapping: [
            {
                basicFeeCode: 'CW003'
            }
        ]
    },
    [FeeRuleType.LOCAL_BALANCE_INQUIRY_FEE]: {
        description: 'Local Balance Inquiry Fee'
    },
    [FeeRuleType.OVERSEAS_BALANCE_INQUIRY_FEE]: {
        description: 'Overseas Balance Inquiry Fee',
        basicFeeMapping: [
            {
                basicFeeCode: 'BL003'
            }
        ]
    },
    [FeeRuleType.PENALTY_FEE]: {
        description: 'TD Penalty',
        basicFeeMapping: [
            {
                basicFeeCode: 'PF001'
            }
        ]
    },
    [FeeRuleType.BILL_PAYMENT_FEE]: { description: 'Bill payment fee' },
    [FeeRuleType.WALLET_FEE_RULE]: {
        description: 'Wallet Top up Rule'
    },
    [FeeRuleType.IRIS_TRANSFER_RULE]: {
        description: 'IRIS Transfer Rule'
    },
    [FeeRuleType.GENERAL_REFUND_FEE_RULE]: {
        description: 'General Refund Fee Rule',
        basicFeeMapping: [
            {
                basicFeeCode: 'RGC01'
            }
        ]
    },
    [FeeRuleType.OFFER_TAX_RULE]: {
        description: 'Offer Tax Rule'
    },
    [FeeRuleType.CASHBACK_TAX_RULE]: {
        description: 'Tax on Cashback'
    },
    [FeeRuleType.SKN_TRANSFER_FEE_RULE]: {
        description: 'SKN transfer fee rule'
    },
    [FeeRuleType.RTGS_TRANSFER_FEE_RULE]: {
        description: 'RTGS transfer fee rule'
    },
    [FeeRuleType.BRANCH_SKN_TRANSFER_FEE_RULE]: {
        description: 'Branch SKN transfer fee rule',
        basicFeeMapping: [
            {
                basicFeeCode: 'TF005'
            }
        ]
    },
    [FeeRuleType.BRANCH_RTGS_TRANSFER_FEE_RULE]: {
        description: 'Branch RTGS transfer fee rule',
        basicFeeMapping: [
            {
                basicFeeCode: 'TF006'
            }
        ]
    },
    [FeeRuleType.SKN_SHARIA_FEE_RULE]: {
        description: 'Fee Rules for Sharia SKN Transfer'
    },
    [FeeRuleType.RTGS_SHARIA_FEE_RULE]: {
        description: 'Fee Rules for Sharia RTGS Transfer',
        basicFeeMapping: [
            {
                basicFeeCode: 'TF019'
            }
        ]
    },
    [FeeRuleType.DIGITAL_GOODS_FEE]: {
        description: 'Fee Rules for Digital Goods',
        basicFeeMapping: [
            {
                basicFeeCode: 'DG00'
            }
        ]
    },
    [FeeRuleType.RTOL_TRANSFER_FOR_BUSINESS_FEE_RULE]: {
        description: 'Fee rules for BFS RTOL transfer'
    },
    [FeeRuleType.SKN_TRANSFER_FOR_BUSINESS_FEE_RULE]: {
        description: 'Fee rules for BFS SKN transfer',
        basicFeeMapping: [
            {
                basicFeeCode: 'TF005'
            }
        ]
    },
    [FeeRuleType.RTGS_TRANSFER_FOR_BUSINESS_FEE_RULE]: {
        description: 'Fee rules for BFS RTGS transfer',
        basicFeeMapping: [
            {
                basicFeeCode: 'TF006'
            }
        ]
    },
    [FeeRuleType.BIFAST_FEE_RULE]: {
        description: 'Fee Rules for BI Fast Transfer'
    },
    [FeeRuleType.BIFAST_SHARIA_FEE_RULE]: {
        description: 'Fee Rules for BI Fast Sharia Transfer'
    },
    [FeeRuleType.INTEREST_TAX_RULE]: {
        description: 'Tax on interest'
    },
    [FeeRuleType.BIFAST_TRANSFER_FOR_BUSINESS_FEE_RULE]: {
        description: 'Fee rules for BFS BIFAST Transfer',
        basicFeeMapping: [
            {
                basicFeeCode: 'TF022'
            }
        ]
    }
};
//# sourceMappingURL=fee.constant.js.map