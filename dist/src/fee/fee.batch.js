"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fee_helper_1 = __importDefault(require("./fee.helper"));
const fee_override_1 = __importDefault(require("./fee.override"));
const logger_1 = __importStar(require("../logger"));
const computeRecordedFeeIfAbsent = (basicFeeRecord, input, basicFeeMapping) => __awaiter(void 0, void 0, void 0, function* () {
    const recordKey = {
        basicFeeCode: basicFeeMapping.basicFeeCode,
        customerId: input.customerId
    };
    const keyString = JSON.stringify(recordKey);
    const cachedFee = basicFeeRecord.get(keyString);
    if (cachedFee) {
        return cachedFee;
    }
    const feePayload = fee_helper_1.default.getFeeRuleTemplatePayload(input, basicFeeMapping);
    yield fee_override_1.default.applyCustomerSpecificFeeIfAvailable(input.customerId, feePayload);
    const recordedFee = basicFeeMapping.basicFee;
    basicFeeRecord.set(keyString, recordedFee);
    logger_1.default.info(`Recorded the following fee for customer ID: '${input.customerId}': ${recordedFee}`);
    return recordedFee;
});
const applyCustomerSpecificFeeIfAvailable = (feeQueries) => __awaiter(void 0, void 0, void 0, function* () {
    const basicFeeRecord = new Map();
    for (const query of feeQueries) {
        const { input, feeMappings } = query;
        for (const basicFeeMapping of feeMappings) {
            basicFeeMapping.basicFee = yield computeRecordedFeeIfAbsent(basicFeeRecord, input, basicFeeMapping);
        }
    }
});
const feeBatchHelper = {
    applyCustomerSpecificFeeIfAvailable
};
exports.default = logger_1.wrapLogs(feeBatchHelper);
//# sourceMappingURL=fee.batch.js.map