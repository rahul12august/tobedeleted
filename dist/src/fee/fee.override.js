"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fee_template_1 = require("./fee.template");
const entitlement_repository_1 = __importDefault(require("../entitlement/entitlement.repository"));
const logger_1 = __importStar(require("../logger"));
const lodash_1 = __importDefault(require("lodash"));
// Regex for percentage or fixed fees
const overrideFeeRegex = new RegExp('^[0-9]+(\\.[0-9]+)?%?$');
const tryGetCustomFeeFromEntitlement = (customerId, feeEntitlementCode) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield entitlement_repository_1.default.getEntitlements(customerId, [
            feeEntitlementCode
        ]);
        if (!lodash_1.default.isEmpty(response)) {
            // Should only have 1 item, as we only have a single request
            return String(response[0].quota);
        }
        else {
            return null;
        }
    }
    catch (error) {
        // We ignore any error. This just means no override is available from ms-entitlement.
        // Error already logged by entitlementRepository
        return null;
    }
});
const applyCustomerSpecificFeeIfAvailable = (customerId, feePayload) => __awaiter(void 0, void 0, void 0, function* () {
    if (!customerId ||
        !(fee_template_1.fixedFee.isApplicable(feePayload) ||
            fee_template_1.percentageFee.isApplicable(feePayload))) {
        // We currently only allow custom fee overrides for fixed or percentage fees
        return;
    }
    // Try to get custom fee from ms-entitlement
    const basicFee = feePayload.basicFee;
    const feeCode = basicFee.code;
    const feeEntitlementCode = `value.tx.${feeCode}.fee`;
    const overrideFee = yield tryGetCustomFeeFromEntitlement(customerId, feeEntitlementCode);
    if (!overrideFee)
        return;
    if (!overrideFeeRegex.test(overrideFee)) {
        const detail = `Could not parse fee amount from entitlement: '${feeEntitlementCode}', for customer ID: '${customerId}'! ` +
            `Value was '${overrideFee}'`;
        logger_1.default.error(`applyCustomerSpecificFeeIfAvailable: ${detail}`);
        return;
    }
    // Replace the existing fee values
    const overrideFeeAmount = parseFloat(overrideFee);
    if (overrideFee.includes('%')) {
        basicFee.feeAmountPercentage = overrideFeeAmount;
        basicFee.feeAmountFixed = undefined;
    }
    else {
        basicFee.feeAmountFixed = overrideFeeAmount;
        basicFee.feeAmountPercentage = undefined;
    }
    logger_1.default.info(`The fee amount for fee code: '${feeCode}' has been changed to '${overrideFee}', for customer ID: '${customerId}'`);
});
const feeOverride = {
    applyCustomerSpecificFeeIfAvailable
};
exports.default = logger_1.wrapLogs(feeOverride);
//# sourceMappingURL=fee.override.js.map