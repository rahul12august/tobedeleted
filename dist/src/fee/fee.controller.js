"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const fee_validator_1 = require("./fee.validator");
const fee_service_1 = __importDefault(require("./fee.service"));
const getFeeAmount = {
    method: module_common_1.Http.Method.POST,
    path: '/fees',
    options: {
        description: 'Calculate fee for transaction',
        notes: 'All information must be valid',
        tags: ['api', 'fees'],
        validate: {
            payload: fee_validator_1.feeAmountRequestValidator
        },
        response: {
            schema: fee_validator_1.feeAmountResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { feeRuleCode, transactionAmount, monthlyNoTransaction, customerId } = hapiRequest.payload;
            const feeData = yield fee_service_1.default.getFeeAmount({
                feeRuleCode: feeRuleCode,
                transactionAmount: transactionAmount,
                monthlyNoTransaction: monthlyNoTransaction,
                customerId: customerId
            });
            const defaultFeeDataResponse = {
                feeAmount: 0
            };
            const feeDataResponse = (feeData && feeData[0]) || defaultFeeDataResponse; // get the first found regardless interchange
            delete feeDataResponse.interchange;
            delete feeDataResponse.feeCode;
            return hapiResponse.response(feeDataResponse).code(module_common_1.Http.StatusCode.OK);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'OK'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const privateGetFeeAmount = Object.assign(Object.assign({}, getFeeAmount), { path: '/private/fees' });
const feeController = [getFeeAmount, privateGetFeeAmount];
exports.default = feeController;
//# sourceMappingURL=fee.controller.js.map