"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Joi = __importStar(require("@hapi/joi"));
const module_common_1 = require("@dk/module-common");
const MAX_NO_TRANSACTION = 999;
const ZERO_AMOUNT = 0;
const noTractionValidator = Joi.number()
    .min(0)
    .max(MAX_NO_TRANSACTION)
    .required();
exports.feeAmountRequestValidator = Joi.object({
    feeRuleCode: Joi.string()
        .max(40)
        .required()
        .description('Fee rule code'),
    transactionAmount: module_common_1.JoiValidator.currency()
        .required()
        .description('Transaction AMount'),
    todayAccumulationAmount: module_common_1.JoiValidator.currency()
        .allow(ZERO_AMOUNT)
        .required()
        .description('Total amount of transactions done against particular limit group code for a day'),
    monthlyAccumulationAmount: module_common_1.JoiValidator.currency()
        .allow(ZERO_AMOUNT)
        .required()
        .description('Total amount of transactions done against particular limit group code for a month'),
    totalAccumulationAmount: module_common_1.JoiValidator.currency()
        .allow(ZERO_AMOUNT)
        .required()
        .description('Total amount of transactions done against particular limit group code till date'),
    todayAccumulationNoTransaction: noTractionValidator.description('Total number of transactions done for a day'),
    monthlyNoTransaction: noTractionValidator.description('Total number of transactions done for a month'),
    totalAccumulationNoTransaction: noTractionValidator.description('Total number of transactions done till date'),
    customerId: module_common_1.JoiValidator.optionalString().description('Customer ID')
}).label('FeePayload');
exports.feeAmountResponseValidator = Joi.object({
    feeAmount: Joi.number()
        .required()
        .description('Fee amount'),
    customerTc: Joi.string().description('Fee transaction code'),
    customerTcChannel: Joi.string().description('Fee Transaction code'),
    subsidiaryAmount: module_common_1.JoiValidator.currency().description('Amount paid by Jago Bank'),
    debitGlNumber: module_common_1.JoiValidator.optionalString().description('General ledger debit account number'),
    creditGlNumber: module_common_1.JoiValidator.optionalString().description('General ledger credit account number'),
    subsidiary: Joi.boolean().description(`Subsidiary 
      * True
      * False`)
}).label('Fee');
//# sourceMappingURL=fee.validator.js.map