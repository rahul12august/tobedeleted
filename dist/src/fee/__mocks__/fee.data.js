"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const faker_1 = __importDefault(require("faker"));
const module_common_1 = require("@dk/module-common");
const configuration_enum_1 = require("../../configuration/configuration.enum");
exports.getFeeAmountData = () => {
    return {
        feeCode: 'CW003',
        feeAmount: 25000,
        customerTc: 'FCD05',
        customerTcChannel: 'FCD05_channel',
        subsidiary: true,
        subsidiaryAmount: 7500,
        debitGlNumber: '01.01.00.00.00.00',
        creditGlNumber: '01.02.00.00.00.00'
    };
};
exports.getFeeAmountDataWithoutGlNumber = () => {
    return {
        feeCode: 'CW003',
        feeAmount: 25000,
        customerTc: 'FCD05',
        customerTcChannel: 'FCD05_channel',
        subsidiary: false,
        subsidiaryAmount: 0,
        debitGlNumber: '',
        creditGlNumber: ''
    };
};
exports.feeRuleTemplatePayloadForThirdPartyFee = () => ({
    basicFee: {
        thirdPartyFee: true,
        code: 'BC144',
        customerTc: 'dummy',
        customerTcInfo: {
            channel: 'dummy'
        },
        subsidiary: false
    },
    transactionAmount: 1000,
    externalId: 'dummy123',
    beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.GOBILLS
});
exports.feeRuleTemplatePayloadForThirdPartyFeeWithAdminFee = () => ({
    basicFee: {
        thirdPartyFee: true,
        code: 'BC144',
        customerTc: 'dummy',
        customerTcInfo: {
            channel: 'dummy'
        },
        subsidiary: false
    },
    transactionAmount: 1000,
    externalId: 'dummy123',
    beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.GOBILLS,
    feeAmount: 2500
});
exports.feeRuleTemplatePayloadForTokopedia = () => ({
    basicFee: {
        thirdPartyFee: true,
        code: 'BC144',
        customerTc: 'dummy',
        customerTcInfo: {
            channel: 'dummy'
        },
        subsidiary: false
    },
    transactionAmount: 1000,
    externalId: 'dummy123',
    beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.TOKOPEDIA
});
exports.feeRuleTemplatePayloadForFixedFee = () => ({
    basicFee: {
        feeAmountFixed: 100,
        code: 'BC144',
        customerTc: 'dummy',
        customerTcInfo: {
            channel: 'dummy'
        },
        subsidiary: true,
        subsidiaryAmount: 7500
    },
    transactionAmount: 1000,
    externalId: 'dummy123'
});
exports.feeRuleTemplatePayloadForPercentageFee = () => ({
    basicFee: {
        feeAmountPercentage: 2,
        code: 'BC144',
        customerTc: 'dummy',
        customerTcInfo: {
            channel: 'dummy'
        },
        subsidiary: true,
        subsidiaryAmountPercentage: 20
    },
    transactionAmount: 1000,
    externalId: 'dummy123'
});
exports.getBasicFeeData = () => ({
    code: faker_1.default.random.word().substr(0, 40),
    feeAmountPercentage: faker_1.default.random.number({ min: 1, max: 100 }),
    customerTc: faker_1.default.random.word().substr(0, 40),
    subsidiary: true,
    subsidiaryAmount: 35000,
    debitGlNumber: '7170232320.00.00',
    creditGlNumber: '71702323202.00.00',
    thirdPartyFee: false,
    customerTcInfo: {
        channel: faker_1.default.random.word().substr(0, 40)
    }
});
exports.getTransactionCode = () => {
    return {
        code: 'SAD01',
        ownershipCode: 'MA',
        transactionType: configuration_enum_1.TransactionCodeType.DEBIT,
        defaultCategoryCode: 'C011',
        splitBill: false,
        isExternal: false,
        downloadReceipt: false,
        allowedToDispute: false,
        currency: 'Any',
        minAmountPerTx: 1,
        maxAmountPerTx: 99999999999,
        limitGroupCode: 'L001',
        feeRules: 'ZERO_FEE_RULE',
        allowedToRepeat: false,
        addToContact: false,
        allowedToInstallment: false,
        loyaltyPoint: false,
        cashBack: false,
        image: '/picture/Transaction_code/SAD01.png',
        notificationTemplate: 'Notif_SAD01',
        showInConsolidatedTrxHistory: false,
        neutralizeSignage: false,
        reversalCodeReference: 'IVC01',
        maxReversalDay: 1,
        channel: 'SAD01'
    };
};
//# sourceMappingURL=fee.data.js.map