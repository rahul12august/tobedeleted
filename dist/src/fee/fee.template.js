"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const fee_constant_1 = require("./fee.constant");
const logger_1 = __importStar(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const lodash_1 = require("lodash");
const transaction_repository_1 = __importDefault(require("../transaction/transaction.repository"));
const billDetail_service_1 = __importDefault(require("../billPayment/billDetail/billDetail.service"));
const transaction_enum_1 = require("../transaction/transaction.enum");
exports.fixedFee = logger_1.wrapLogs({
    isApplicable: (input) => typeof input.basicFee.feeAmountFixed === 'number',
    getFee: (input) => {
        const { feeAmountFixed } = input.basicFee;
        if (feeAmountFixed === null || feeAmountFixed === undefined) {
            let detail = "Can't continue feeAmountFixed is absent in basic fee!";
            logger_1.default.error(detail, input);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.MISSING_FIXED_FEE_CONFIGURATION);
        }
        return feeAmountFixed;
    }
});
exports.percentageFee = logger_1.wrapLogs({
    isApplicable: (input) => typeof input.basicFee.feeAmountPercentage === 'number',
    getFee: (input) => {
        const { feeAmountPercentage } = input.basicFee;
        if (feeAmountPercentage === null || feeAmountPercentage === undefined) {
            const detail = 'Fee amount percentage is absent in basic fee!';
            logger_1.default.error(detail, input);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.MISSING_PERCENTAGE_FEE_CONFIGURATION);
        }
        return module_common_1.Formater.roundUp((input.transactionAmount * feeAmountPercentage) / fee_constant_1.PERCENTAGE);
    }
});
exports.refundFee = logger_1.wrapLogs({
    isApplicable: (input) => !lodash_1.isUndefined(input.refund) &&
        !lodash_1.isUndefined(input.refund.originalTransactionId),
    getFee: (input) => __awaiter(void 0, void 0, void 0, function* () {
        let feeAmount = 0;
        if (input.refund && input.refund.originalTransactionId) {
            const originalTransaction = yield transaction_repository_1.default.getByKey(input.refund.originalTransactionId);
            if (!originalTransaction) {
                const detail = `Original transaction not found for transactionId: ${input.refund.originalTransactionId}`;
                logger_1.default.error(`General refund getFee: ${detail}`);
                throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.TRANSACTION_NOT_FOUND);
            }
            if (originalTransaction.fees) {
                feeAmount = lodash_1.sumBy(originalTransaction.fees, 'feeAmount');
            }
        }
        return feeAmount;
    })
});
exports.billPaymentAdminFee = logger_1.wrapLogs({
    isApplicable: (input) => input.basicFee.thirdPartyFee === true &&
        (input.beneficiaryBankCodeChannel === module_common_1.BankChannelEnum.GOBILLS ||
            input.beneficiaryBankCodeChannel === module_common_1.BankChannelEnum.TOKOPEDIA),
    getFee: (input) => __awaiter(void 0, void 0, void 0, function* () {
        if (input.feeAmount) {
            logger_1.default.info(`billPaymentAdminFee : getFee : Using feeAmount provided in transfer input.`);
            return input.feeAmount;
        }
        if (!input.externalId) {
            const detail = 'externalId is not provided for getFee';
            logger_1.default.error(`billPaymentAdminFee : ${detail}, payload: `, JSON.stringify(input));
            throw new AppError_1.TransferAppError(detail + '!', transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.MISSING_INQUIRY_ID);
        }
        return billDetail_service_1.default.getAdminFee(input.externalId);
    })
});
exports.feeTemplates = () => [
    exports.refundFee,
    exports.billPaymentAdminFee,
    exports.fixedFee,
    exports.percentageFee
];
//# sourceMappingURL=fee.template.js.map