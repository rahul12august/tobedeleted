"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const module_common_1 = require("@dk/module-common");
const fee_constant_1 = require("./fee.constant");
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const logger_1 = __importStar(require("../logger"));
const customer_cache_1 = __importDefault(require("../customer/customer.cache"));
const customerEntitlement_manager_factory_1 = require("../customerEntitlementManager/customerEntitlement.manager.factory");
const transaction_enum_1 = require("../transaction/transaction.enum");
const getBasicFee = (fees) => __awaiter(void 0, void 0, void 0, function* () {
    if (fees.basicFeeMapping) {
        yield Promise.all(fees.basicFeeMapping.map((basicFeeMapping) => __awaiter(void 0, void 0, void 0, function* () {
            const basicFeeCode = yield configuration_repository_1.default.getBasicFees(basicFeeMapping.basicFeeCode);
            if (!basicFeeCode) {
                const detail = `Basic fee code: ${basicFeeMapping.basicFeeCode} not found!`;
                logger_1.default.error(`getBasicFee: ${detail}`);
                throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.INVALID_REGISTER_PARAMETER);
            }
            basicFeeMapping.basicFee = basicFeeCode;
        })));
        yield Promise.all(fees.basicFeeMapping.map((basicFeeMapping) => __awaiter(void 0, void 0, void 0, function* () {
            if (basicFeeMapping.basicFee &&
                !basicFeeMapping.basicFee.customerTcInfo) {
                const transctionCode = yield configuration_repository_1.default.getTransactionCode(basicFeeMapping.basicFee.customerTc);
                if (!transctionCode) {
                    const detail = `Transaction code: ${basicFeeMapping.basicFee.customerTc} not found!`;
                    logger_1.default.error(`getBasicFee: ${detail}`);
                    throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.INVALID_REGISTER_PARAMETER);
                }
                basicFeeMapping.basicFee.customerTcInfo = {
                    channel: transctionCode.channel
                };
            }
        })));
    }
    return fees;
});
const getFeeRuleBase = (feeRuleObject, basicFeeMapping) => __awaiter(void 0, void 0, void 0, function* () {
    // request all available
    if (!feeRuleObject.interchange ||
        !basicFeeMapping ||
        !basicFeeMapping.length) {
        return getBasicFee(Object.assign(Object.assign({}, fee_constant_1.feeRuleMapping[feeRuleObject.code]), { basicFeeMapping }));
    }
    // get by specific interchange
    let returnBasicFeeMapping = basicFeeMapping.filter(mappingInfo => feeRuleObject.interchange === mappingInfo.interchange);
    // fall back to default if interchange not found
    if (!returnBasicFeeMapping.length) {
        returnBasicFeeMapping = basicFeeMapping.filter(mappingInfo => !mappingInfo.interchange);
    }
    if (!returnBasicFeeMapping.length) {
        throw new AppError_1.TransferAppError(`Transfer interchange fee rule not configured!`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.TRANSFER_INTERCHANGE_FEE_RULE_NOT_CONFIGURED);
    }
    return getBasicFee(Object.assign(Object.assign({}, fee_constant_1.feeRuleMapping[feeRuleObject.code]), { basicFeeMapping: returnBasicFeeMapping }));
});
const getFeeRuleBasedOnMonthlyNoTransaction = (feeRuleObject) => __awaiter(void 0, void 0, void 0, function* () {
    const custEntitlementMgr = yield customerEntitlement_manager_factory_1.CustomerEntitlementManagerFactory.getCustomerEntitlementManagerByFlag();
    const basicFeeMapping = yield custEntitlementMgr.determineBasicFeeMapping(feeRuleObject);
    return getFeeRuleBase(feeRuleObject, basicFeeMapping);
});
const getRtolAndWalletCounterCode = (thresholdCounter, customerId) => __awaiter(void 0, void 0, void 0, function* () {
    if (customerId &&
        (yield customer_cache_1.default.getCustomerType(customerId)) ===
            "BUSINESS_INDIVIDUAL" /* BUSINESS_INDIVIDUAL */) {
        return fee_constant_1.COUNTER.COUNTER_MFS_01;
    }
    return thresholdCounter || fee_constant_1.COUNTER.COUNTER_01;
});
const calculateFeeRule = (feeRuleInput, customerId, awardGroupCounter, usageCounters) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`calculateFeeRule: getting fee rule`);
    //based on the feature flag, get the appropriate customerEntitlementManager
    const custEntitlementManager = yield customerEntitlement_manager_factory_1.CustomerEntitlementManagerFactory.getCustomerEntitlementManagerByFlag();
    const { code, monthlyNoTransaction, interchange, targetBankCode, thresholdCounter } = feeRuleInput;
    switch (code) {
        case fee_constant_1.FeeRuleType.LOCAL_BALANCE_INQUIRY_FEE:
            let localBalanceInquiryFeeObject = {
                code,
                basicFeeCode2: [
                    {
                        interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                        basicFeeCode: 'BL001'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.ALTO,
                        basicFeeCode: 'BL002'
                    }
                ],
                interchange
            };
            return yield getFeeRuleBase(localBalanceInquiryFeeObject, localBalanceInquiryFeeObject.basicFeeCode2);
        case fee_constant_1.FeeRuleType.RTOL_TRANSFER_ON_OTHER_ATM_FEE_RULE:
            let rtolTransferOnOtherAtmFeeRuleObject = {
                code,
                basicFeeCode2: [
                    {
                        interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                        basicFeeCode: 'TF003'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.ALTO,
                        basicFeeCode: 'TF004'
                    }
                ],
                interchange
            };
            return yield getFeeRuleBase(rtolTransferOnOtherAtmFeeRuleObject, rtolTransferOnOtherAtmFeeRuleObject.basicFeeCode2);
        case fee_constant_1.FeeRuleType.BILL_PAYMENT_FEE:
            if (!targetBankCode) {
                const detail = `Calculating fee rule targetBankCode not provided!`;
                logger_1.default.error(`calculateFeeRule: ${detail}`);
                throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.TRANSFER_FEE_RULE_INVALID_MANDATORY_FIELDS);
            }
            return getBasicFee(Object.assign(Object.assign({}, fee_constant_1.feeRuleMapping[code]), { basicFeeMapping: [
                    {
                        basicFeeCode: targetBankCode
                    }
                ] }));
        case fee_constant_1.FeeRuleType.RTOL_TRANSFER_FEE_RULE:
            let rtolTransferFeeRuleObject = {
                code,
                counterCode: yield getRtolAndWalletCounterCode(thresholdCounter, customerId),
                basicFeeCode1: [
                    {
                        interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                        basicFeeCode: 'TF015'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.ALTO,
                        basicFeeCode: 'TF008'
                    }
                ],
                basicFeeCode2: [
                    {
                        interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                        basicFeeCode: 'TF010'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.ALTO,
                        basicFeeCode: 'TF009'
                    }
                ],
                monthlyNoTransaction,
                interchange
            };
            rtolTransferFeeRuleObject = custEntitlementManager.processFeeRuleInfo(rtolTransferFeeRuleObject, code, awardGroupCounter, usageCounters);
            return yield getFeeRuleBasedOnMonthlyNoTransaction(rtolTransferFeeRuleObject);
        case fee_constant_1.FeeRuleType.LOCAL_ATM_WITHDRAWAL_RULE:
            let localAtmFeeRuleInfo = {
                code,
                counterCode: thresholdCounter || fee_constant_1.COUNTER.COUNTER_07,
                basicFeeCode1: [
                    {
                        interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                        basicFeeCode: 'CW007'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.ALTO,
                        basicFeeCode: 'CW005'
                    }
                ],
                basicFeeCode2: [
                    {
                        interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                        basicFeeCode: 'CW001'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.ALTO,
                        basicFeeCode: 'CW002'
                    }
                ],
                monthlyNoTransaction,
                interchange
            };
            localAtmFeeRuleInfo = custEntitlementManager.processFeeRuleInfo(localAtmFeeRuleInfo, code, awardGroupCounter, usageCounters);
            return yield getFeeRuleBasedOnMonthlyNoTransaction(localAtmFeeRuleInfo);
        case fee_constant_1.FeeRuleType.OVERSEAS_ATM_WITHDRAWAL_RULE:
            let overseasAtmFeeRuleInfo = {
                code,
                counterCode: thresholdCounter,
                basicFeeCode1: [
                    {
                        basicFeeCode: 'CW004'
                    }
                ],
                basicFeeCode2: [
                    {
                        basicFeeCode: 'CW003'
                    }
                ],
                monthlyNoTransaction,
                interchange
            };
            return thresholdCounter
                ? getFeeRuleBasedOnMonthlyNoTransaction(overseasAtmFeeRuleInfo)
                : getFeeRuleBase(overseasAtmFeeRuleInfo, [
                    {
                        basicFeeCode: 'CW003'
                    }
                ]);
        case fee_constant_1.FeeRuleType.WALLET_FEE_RULE:
            let walletTransferFeeRuleObject = {
                code,
                counterCode: yield getRtolAndWalletCounterCode(thresholdCounter, customerId),
                basicFeeCode1: [
                    {
                        interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                        basicFeeCode: 'BF011'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.ALTO,
                        basicFeeCode: 'BF004'
                    }
                ],
                basicFeeCode2: [
                    {
                        interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                        basicFeeCode: 'BF005'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.ALTO,
                        basicFeeCode: 'BF003'
                    }
                ],
                monthlyNoTransaction,
                interchange
            };
            walletTransferFeeRuleObject = custEntitlementManager.processFeeRuleInfo(walletTransferFeeRuleObject, code, awardGroupCounter, usageCounters);
            return yield getFeeRuleBasedOnMonthlyNoTransaction(walletTransferFeeRuleObject);
        case fee_constant_1.FeeRuleType.IRIS_TRANSFER_RULE:
            let irisTransferFeeRuleObject = {
                code,
                counterCode: thresholdCounter || fee_constant_1.COUNTER.COUNTER_01,
                basicFeeCode1: [
                    {
                        basicFeeCode: 'TF011'
                    }
                ],
                basicFeeCode2: [
                    {
                        basicFeeCode: 'TF012'
                    }
                ],
                monthlyNoTransaction
            };
            return yield getFeeRuleBasedOnMonthlyNoTransaction(irisTransferFeeRuleObject);
        case fee_constant_1.FeeRuleType.SKN_TRANSFER_FEE_RULE:
            let sknFeeRuleInfo = {
                code,
                counterCode: thresholdCounter || fee_constant_1.COUNTER.COUNTER_08,
                basicFeeCode1: [
                    {
                        basicFeeCode: 'TF013'
                    }
                ],
                basicFeeCode2: [
                    {
                        basicFeeCode: 'TF005'
                    }
                ],
                monthlyNoTransaction,
                interchange
            };
            return yield getFeeRuleBasedOnMonthlyNoTransaction(sknFeeRuleInfo);
        case fee_constant_1.FeeRuleType.RTGS_TRANSFER_FEE_RULE:
            let rtgsFeeRuleInfo = {
                code,
                counterCode: thresholdCounter || fee_constant_1.COUNTER.COUNTER_09,
                basicFeeCode1: [
                    {
                        basicFeeCode: 'TF014'
                    }
                ],
                basicFeeCode2: [
                    {
                        basicFeeCode: 'TF006'
                    }
                ],
                monthlyNoTransaction,
                interchange
            };
            return yield getFeeRuleBasedOnMonthlyNoTransaction(rtgsFeeRuleInfo);
        case fee_constant_1.FeeRuleType.CASHBACK_TAX_RULE:
            let cashbackFeeRuleObject = {
                code,
                basicFeeCode2: [
                    {
                        interchange: module_common_1.BankNetworkEnum.JAGO,
                        basicFeeCode: 'CBC01'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.PARTNER,
                        basicFeeCode: 'CBC02'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.VISA,
                        basicFeeCode: 'CBC03'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.GIVEAWAY,
                        basicFeeCode: 'CBC04'
                    }
                ],
                interchange
            };
            return yield getFeeRuleBase(cashbackFeeRuleObject, cashbackFeeRuleObject.basicFeeCode2);
        case fee_constant_1.FeeRuleType.OFFER_TAX_RULE:
            let offerFeeRuleObject = {
                code,
                basicFeeCode2: [
                    {
                        interchange: module_common_1.BankNetworkEnum.TAX,
                        basicFeeCode: 'OFC01'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.NOTAX,
                        basicFeeCode: 'OFC02'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.PARTNER,
                        basicFeeCode: 'OFC03'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.OPERATION,
                        basicFeeCode: 'OFC04'
                    }
                ],
                interchange
            };
            return yield getFeeRuleBase(offerFeeRuleObject, offerFeeRuleObject.basicFeeCode2);
        case fee_constant_1.FeeRuleType.SKN_SHARIA_FEE_RULE:
            let sknShariaFeeRuleInfo = {
                code,
                counterCode: thresholdCounter || fee_constant_1.COUNTER.COUNTER_08,
                basicFeeCode1: [
                    {
                        basicFeeCode: 'TF017'
                    }
                ],
                basicFeeCode2: [
                    {
                        basicFeeCode: 'TF018'
                    }
                ],
                monthlyNoTransaction,
                interchange
            };
            return yield getFeeRuleBasedOnMonthlyNoTransaction(sknShariaFeeRuleInfo);
        case fee_constant_1.FeeRuleType.RTOL_TRANSFER_FOR_BUSINESS_FEE_RULE:
            let rtolTransferForBusinessFeeRuleObject = {
                code,
                counterCode: thresholdCounter || fee_constant_1.COUNTER.COUNTER_01,
                basicFeeCode2: [
                    {
                        interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                        basicFeeCode: 'TF010'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.ALTO,
                        basicFeeCode: 'TF009'
                    }
                ],
                monthlyNoTransaction,
                interchange
            };
            return yield getFeeRuleBase(rtolTransferForBusinessFeeRuleObject, rtolTransferForBusinessFeeRuleObject.basicFeeCode2);
        case fee_constant_1.FeeRuleType.BIFAST_FEE_RULE:
            let biFastFeeRuleObject = {
                code,
                counterCode: yield getRtolAndWalletCounterCode(thresholdCounter, customerId),
                basicFeeCode1: [
                    {
                        basicFeeCode: 'TF022'
                    }
                ],
                basicFeeCode2: [
                    {
                        basicFeeCode: 'TF020'
                    }
                ],
                monthlyNoTransaction,
                interchange
            };
            biFastFeeRuleObject = custEntitlementManager.processFeeRuleInfo(biFastFeeRuleObject, code, awardGroupCounter, usageCounters);
            return yield getFeeRuleBasedOnMonthlyNoTransaction(biFastFeeRuleObject);
        case fee_constant_1.FeeRuleType.BIFAST_SHARIA_FEE_RULE:
            let biFastShariaFeeRuleObject = {
                code,
                counterCode: yield getRtolAndWalletCounterCode(thresholdCounter, customerId),
                basicFeeCode1: [
                    {
                        basicFeeCode: 'TF023'
                    }
                ],
                basicFeeCode2: [
                    {
                        basicFeeCode: 'TF021'
                    }
                ],
                monthlyNoTransaction,
                interchange
            };
            biFastShariaFeeRuleObject = custEntitlementManager.processFeeRuleInfo(biFastShariaFeeRuleObject, code, awardGroupCounter, usageCounters);
            return yield getFeeRuleBasedOnMonthlyNoTransaction(biFastShariaFeeRuleObject);
        case fee_constant_1.FeeRuleType.INTEREST_TAX_RULE:
            let interestTaxFeeRuleObject = {
                code,
                basicFeeCode2: [
                    {
                        interchange: module_common_1.BankNetworkEnum.LFS,
                        basicFeeCode: 'IBC01'
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.MFS,
                        basicFeeCode: 'IBC02'
                    }
                ],
                monthlyNoTransaction,
                interchange
            };
            return yield getFeeRuleBase(interestTaxFeeRuleObject, interestTaxFeeRuleObject.basicFeeCode2);
    }
    return getBasicFee(fee_constant_1.feeRuleMapping[code]);
});
const getFeeRule = (feeRuleCode, transactionAmount, monthlyNoTransaction, interchange, targetBankCode, thresholdCounter, customerId, awardGroupCounter, usageCounters) => __awaiter(void 0, void 0, void 0, function* () {
    const code = feeRuleCode;
    let transactionCount = monthlyNoTransaction || 0;
    ++transactionCount;
    return calculateFeeRule({
        code,
        monthlyNoTransaction: transactionCount,
        interchange,
        targetBankCode,
        thresholdCounter,
        transactionAmount
    }, customerId, awardGroupCounter, usageCounters);
});
const decisionRules = {
    getFeeRule
};
exports.default = logger_1.wrapLogs(decisionRules);
//# sourceMappingURL=decisionRules.js.map