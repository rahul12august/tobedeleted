"use strict";
/*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_constant_1 = require("./transaction.constant");
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const v1_1 = __importDefault(require("uuid/v1"));
const moment_1 = __importDefault(require("moment"));
const module_common_1 = require("@dk/module-common");
const logger_1 = __importStar(require("../logger"));
const encodeString_util_1 = require("../common/encodeString.util");
const lodash_1 = require("lodash");
const contextHandler_1 = require("../common/contextHandler");
const dateUtils_1 = require("../common/dateUtils");
const transaction_enum_1 = require("./transaction.enum");
const error_util_1 = require("../errors/error.util");
const fee_constant_1 = require("../fee/fee.constant");
const constant_1 = require("../common/constant");
const coreBanking_constant_1 = __importDefault(require("../coreBanking/coreBanking.constant"));
const loan_enum_1 = require("../coreBanking/loan.enum");
const account_enum_1 = require("../account/account.enum");
const common_util_1 = require("../common/common.util");
const alto_constant_1 = require("../alto/alto.constant");
const alto_utils_1 = __importDefault(require("../alto/alto.utils"));
const alto_enum_1 = require("../alto/alto.enum");
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const bifast_constant_1 = require("../bifast/bifast.constant");
const transaction_repository_1 = __importDefault(require("./transaction.repository"));
const transaction_helper_1 = __importDefault(require("./transaction.helper"));
const async_hooks_1 = require("async_hooks");
exports.RetryConfig = () => ({
    shouldRetry: (error) => {
        return error.status >= 500;
    },
    numberOfTries: 1
});
exports.IdempotentRetryConfig = () => ({
    shouldRetry: (error) => {
        return error.status >= 500;
    },
    numberOfTries: contextHandler_1.getRetryCount() || 1
});
exports.isBIFast = (paymentServiceCode) => { var _a; return ((_a = paymentServiceCode) === null || _a === void 0 ? void 0 : _a.indexOf(module_common_1.BankChannel.BIFAST)) !== -1; };
const isRefundBifast = (paymentServiceCode) => { var _a; return ((_a = paymentServiceCode) === null || _a === void 0 ? void 0 : _a.indexOf(bifast_constant_1.BIFAST_VOID_PAYMENT_SERVICE_CODE)) !== -1; };
const getTargetAccountNo = (transactionModel) => exports.isBIFast(transactionModel.paymentServiceCode) &&
    transactionModel.additionalInformation4 &&
    !isRefundBifast(transactionModel.paymentServiceCode)
    ? transactionModel.additionalInformation4
    : transactionModel.beneficiaryAccountNo;
const isBiFastProxy = (transactionModel) => exports.isBIFast(transactionModel.paymentServiceCode) &&
    !lodash_1.isEmpty(transactionModel.additionalInformation3) &&
    bifast_constant_1.BIFAST_PROXY_TYPES.includes(transactionModel.additionalInformation3);
const mapToCoreBankingTransactionCustomFields = (transactionModel, transactionCode) => {
    return {
        _Custom_Transaction_Details: {
            Category_Code: transactionModel.categoryCode || undefined,
            Payment_Service_Code: transactionModel.paymentServiceCode || '',
            Transaction_Id: transactionModel.id,
            Payment_Instruction_ID: transactionModel.id,
            PI_Id: transactionModel.paymentInstructionId,
            Reference_ID: transactionModel.referenceId,
            Third_Party_Incoming_Id: transactionModel.externalId,
            Transaction_Code: transactionCode,
            Notes: transactionModel.note
                ? encodeString_util_1.punycodeUtil.encodeString(transactionModel.note)
                : undefined,
            Currency: transactionModel.sourceTransactionCurrency || undefined,
            Original_Currency: transaction_constant_1.DEFAULT_ORIGINAL_CURRENCY,
            Original_Amount: transactionModel.transactionAmount,
            Exchange_Rate: transaction_constant_1.DEFAULT_EXCHANGE_RATE,
            Additional_Information_1: transactionModel.additionalInformation1 || undefined,
            Additional_Information_2: transactionModel.additionalInformation2 || undefined,
            Additional_Information_3: transactionModel.additionalInformation3 || undefined,
            Additional_Information_4: transactionModel.additionalInformation4 || undefined,
            Third_Party_Outgoing_Id: transactionModel.thirdPartyOutgoingId || undefined,
            Request_Id: contextHandler_1.getRequestId()
        },
        _Custom_Source_Information: {
            Source_Account_Type: transactionModel.sourceAccountType,
            Source_Account_No: !lodash_1.isEmpty(transactionModel.sourceAccountNo)
                ? transactionModel.sourceAccountNo
                : undefined,
            Source_Account_Name: !lodash_1.isEmpty(transactionModel.sourceAccountName)
                ? transactionModel.sourceAccountName
                : undefined,
            Executor_CIF: transactionModel.executorCIF,
            Source_CIF: transactionModel.sourceCIF,
            Source_Bank_Code: transactionModel.sourceBankCode
        },
        _Custom_Target_Information: {
            Institutional_Bank_Code: isBiFastProxy(transactionModel)
                ? bifast_constant_1.BIFAST_PROXY_BANK_CODE
                : transactionModel.beneficiaryBankCode,
            Real_Target_Bank_Code: transactionModel.beneficiaryRealBankCode,
            Target_Account_Type: transactionModel.beneficiaryAccountType,
            Target_Account_No: getTargetAccountNo(transactionModel),
            Target_Account_Name: !lodash_1.isEmpty(transactionModel.beneficiaryAccountName)
                ? transactionModel.beneficiaryAccountName
                : undefined,
            Target_Bank_Name: isBiFastProxy(transactionModel)
                ? undefined
                : transactionModel.beneficiaryBankName,
            Target_CIF: transactionModel.beneficiaryCIF
        }
    };
};
exports.mapToCoreBankingCardTransaction = (transactionModel, transactionCode, transactionChannel, blockingId) => {
    const customFields = mapToCoreBankingTransactionCustomFields(transactionModel, transactionCode);
    return Object.assign(Object.assign({}, customFields), { amount: transactionModel.transactionAmount, externalReferenceId: blockingId, externalAuthorizationReferenceId: blockingId, transactionChannelId: transactionChannel });
};
exports.mapToCoreBankingTransaction = (transactionModel, transactionCode, transactionChannel) => {
    const customFields = mapToCoreBankingTransactionCustomFields(transactionModel, transactionCode);
    return Object.assign(Object.assign({}, customFields), { transactionDetails: {
            transactionChannelId: transactionChannel
        }, amount: transactionModel.transactionAmount, notes: customFields._Custom_Transaction_Details.Notes });
};
exports.getErrorMessage = (col) => ({
    message: errors_1.ErrorList[errors_1.ERROR_CODE.INVALID_REGISTER_PARAMETER].message,
    code: errors_1.ERROR_CODE.INVALID_REGISTER_PARAMETER,
    key: col
});
exports.GenerateAppError = (detail, actor, errorCode, key) => {
    return new AppError_1.TransferAppError(detail, actor, errorCode, error_util_1.getErrorDetails(errorCode, key));
};
exports.mapDebitTransactionLimitAmount = (transactionCodeInfo) => ({
    transactionCode: transactionCodeInfo.code,
    transactionMinAmount: transactionCodeInfo.minAmountPerTx,
    transactionMaxAmount: transactionCodeInfo.maxAmountPerTx
});
exports.generateUniqueId = () => {
    return v1_1.default();
};
exports.toTransactionDetail = (transaction) => {
    const fees = transaction.fees &&
        transaction.fees.map(fee => {
            return {
                feeAmount: fee.feeAmount
            };
        });
    const refund = transaction.refund && {
        remainingAmount: transaction.refund.remainingAmount,
        originalTransactionId: transaction.refund.originalTransactionId,
        transactions: transaction.refund.transactions,
        revertPaymentServiceCode: transaction.refund.revertPaymentServiceCode
    };
    const reason = transaction.reason &&
        transaction.reason.map(reason => {
            return {
                responseCode: reason.responseCode,
                responseMessage: reason.responseMessage,
                transactionDate: reason.transactionDate
            };
        });
    const journey = transaction.journey &&
        transaction.journey.map(data => {
            return {
                status: data.status,
                updatedAt: data.updatedAt
            };
        });
    return {
        paymentRequestID: transaction.paymentRequestID,
        sourceCIF: transaction.sourceCIF,
        sourceAccountType: transaction.sourceAccountType,
        sourceAccountNo: transaction.sourceAccountNo,
        sourceAccountName: transaction.sourceAccountName,
        sourceBankCode: transaction.sourceBankCode,
        sourceTransactionCurrency: transaction.sourceTransactionCurrency,
        sourceBankCodeChannel: transaction.sourceBankCodeChannel,
        transactionAmount: transaction.transactionAmount,
        beneficiaryCIF: transaction.beneficiaryCIF,
        beneficiaryBankCode: transaction.beneficiaryBankCode,
        beneficiaryAccountType: transaction.beneficiaryAccountType,
        beneficiaryAccountNo: transaction.beneficiaryAccountNo,
        beneficiaryAccountName: transaction.beneficiaryAccountName,
        beneficiaryBankName: transaction.beneficiaryBankName,
        beneficiaryBankCodeChannel: transaction.beneficiaryBankCodeChannel,
        creditPriority: transaction.creditPriority,
        debitPriority: transaction.debitPriority,
        institutionalType: transaction.institutionalType,
        note: transaction.note,
        paymentServiceType: transaction.paymentServiceType,
        extendedMessage: transaction.extendedMessage,
        status: transaction.status,
        fees,
        externalId: transaction.externalId,
        referenceId: transaction.referenceId,
        additionalInformation1: transaction.additionalInformation1,
        additionalInformation2: transaction.additionalInformation2,
        additionalInformation3: transaction.additionalInformation3,
        additionalInformation4: transaction.additionalInformation4,
        categoryCode: transaction.categoryCode,
        promotionCode: transaction.promotionCode,
        sourceCustomerId: transaction.sourceCustomerId,
        refund,
        blockingId: transaction.blockingId,
        cardId: transaction.cardId,
        transactionId: transaction.id,
        confirmationCode: transaction.confirmationCode,
        reason,
        paymentServiceCode: transaction.paymentServiceCode,
        createdAt: transaction.createdAt,
        journey: journey,
        ownerCustomerId: transaction.ownerCustomerId
    };
};
const shouldMaskSensitiveTransaction = (transaction, maskData) => {
    const isSensitiveCreditTransaction = transaction.creditTransactionCode &&
        constant_1.SENSITIVE_CREDIT_TRANSACTION_CODES.includes(transaction.creditTransactionCode);
    const isSensitiveDebitTransaction = transaction.debitTransactionCode &&
        constant_1.SENSITIVE_DEBIT_TRANSACTION_CODES.includes(transaction.debitTransactionCode);
    return (maskData && (isSensitiveCreditTransaction || isSensitiveDebitTransaction));
};
exports.toTransactionList = (transactionList, adminRoles, refundTransactions, fetchRefund) => {
    const hasEmployeeReadRole = adminRoles.includes(module_common_1.AdminPortalUserPermissionCode.EMPLOYEE_INFO_READ);
    const refundTransactionsMap = new Map(refundTransactions.map(trx => [trx.id, trx]));
    return lodash_1.map(transactionList, transaction => {
        const transactionData = exports.toTransactionDetail(transaction);
        const applyMasking = shouldMaskSensitiveTransaction(transaction, !hasEmployeeReadRole);
        transactionData.fees =
            transactionData.fees &&
                transactionData.fees.map(fee => {
                    return {
                        feeAmount: applyMasking ? constant_1.DEFAULT_MASKING_PLACEHOLDER : fee.feeAmount
                    };
                });
        if (transactionData.refund) {
            transactionData.refund.remainingAmount = applyMasking
                ? constant_1.DEFAULT_MASKING_PLACEHOLDER
                : transactionData.refund.remainingAmount;
            if (fetchRefund && transactionData.refund.transactions) {
                for (const transactionId of transactionData.refund.transactions) {
                    let refundTransaction = refundTransactionsMap.get(transactionId);
                    if (!refundTransaction)
                        continue;
                    if (refundTransaction.status == transaction_enum_1.TransactionStatus.SUCCEED) {
                        transactionData.refund.latestTransaction = refundTransaction;
                        break;
                    }
                    else if (!transactionData.refund.latestTransaction ||
                        transactionData.refund.latestTransaction.updatedAt <
                            refundTransaction.updatedAt) {
                        transactionData.refund.latestTransaction = refundTransaction;
                    }
                }
            }
        }
        transactionData.transactionAmount = applyMasking
            ? constant_1.DEFAULT_MASKING_PLACEHOLDER
            : transactionData.transactionAmount;
        return transactionData;
    });
};
exports.isDepositTransactionNotReverted = (depositTransaction) => {
    var _a, _b;
    logger_1.default.info(`isDepositTransactionNotReverted: 
    coreBankingId: ${depositTransaction.id}, 
    adjustmentTransactionKey: ${depositTransaction.adjustmentTransactionKey}
    cardToken: ${depositTransaction.cardTransaction && ((_a = depositTransaction.cardTransaction) === null || _a === void 0 ? void 0 : _a.cardToken)},
    externalReferenceId: ${depositTransaction.cardTransaction && ((_b = depositTransaction.cardTransaction) === null || _b === void 0 ? void 0 : _b.externalReferenceId)}`);
    return !depositTransaction.adjustmentTransactionKey;
};
exports.mapTransactionResultToResponse = (result) => ({
    id: result.id,
    externalId: result.externalId,
    referenceId: result.referenceId,
    transactionIds: result.coreBankingTransactionIds || [],
    transactionDate: result.createdAt
});
exports.mapTransactionResultToExternalResponse = (result, includeCoreBankingTransactions) => {
    const responseWithoutCoreBankingIds = {
        id: result.id,
        externalId: result.externalId,
        availableBalance: result.availableBalance
    };
    if (includeCoreBankingTransactions) {
        return Object.assign(Object.assign({}, responseWithoutCoreBankingIds), { coreBankingTransactions: result.coreBankingTransactions || [] });
    }
    return responseWithoutCoreBankingIds;
};
const notExistOrFalsy = (value) => {
    return value !== true;
};
exports.mapConfirmTransactionResultToExternalResponse = (result, query) => {
    if (notExistOrFalsy(query.includeTransactionId)) {
        delete result.id;
    }
    if (notExistOrFalsy(query.includeExternalId)) {
        delete result.externalId;
    }
    if (notExistOrFalsy(query.includeCoreBankingTransactions)) {
        delete result.coreBankingTransactions;
    }
    return result;
};
exports.formatBeneficiaryAccountNoForWallet = (walletBankCode, identityNumber) => {
    if (walletBankCode.prefix) {
        return `${walletBankCode.prefix}${identityNumber}`;
    }
    return identityNumber;
};
exports.formatTransactionDate = (date) => {
    return moment_1.default(date)
        .utc()
        .format(transaction_constant_1.TRANSACTION_DATE_FORMAT_STRING);
};
exports.formatString2Date = (dateString, format) => {
    return moment_1.default(dateString, format).toDate();
};
exports.formatDate2String = (date, format) => {
    return moment_1.default(date)
        .utc()
        .format(format);
};
exports.formatToUtcTime = (dateString, currentFormat, expectedFormat) => {
    const date = exports.formatString2Date(dateString, currentFormat);
    return exports.formatDate2String(date, expectedFormat);
};
exports.createIncomingTransactionExternalId = (externalId, transactionDate, accountNumber = '') => {
    // externalId=${externalId}_${YYMMDDhhmmss}_${accountNo}
    const generateTransactionDate = exports.formatToUtcTime(transactionDate, transaction_constant_1.TRANSACTION_DATE_FORMAT_STRING, transaction_constant_1.TRANSACTION_DATE_FORMAT_STRING);
    const accountNumberStr = accountNumber ? `_${accountNumber}` : '';
    const extId = `${externalId}_${generateTransactionDate}` + accountNumberStr;
    logger_1.default.info(`createIncomingTransactionExternalId : External Id : ${extId}`);
    return extId;
};
exports.mapDataJournalEntriesGL = (fee, transactionID) => {
    logger_1.default.info(`mapDataJournalEntriesGL: TransactionId ${transactionID} journalEntries debitGlNumber ${fee.debitGlNumber} 
  and creditGlNumber ${fee.creditGlNumber}`);
    return {
        date: moment_1.default().format('YYYY-MM-DD'),
        debitAccount1: fee.debitGlNumber || '',
        debitAmount1: fee.subsidiaryAmount || 0,
        creditAccount1: fee.creditGlNumber || '',
        creditAmount1: fee.subsidiaryAmount || 0,
        transactionID
    };
};
exports.submitCoreBankingTransactionWithRetry = (fn, ...args) => __awaiter(void 0, void 0, void 0, function* () {
    const idempotencyKey = module_common_1.Http.generateIdempotencyKey();
    args.push(idempotencyKey);
    return yield module_common_1.Util.retry(exports.IdempotentRetryConfig(), fn, ...args);
});
const isFeeApplicable = (creditTc, debitTc) => {
    var _a, _b;
    // Identify fee from credit transaction code
    let creditTcHasReversalSuffix = false;
    if (!lodash_1.isUndefined(creditTc) && !lodash_1.isEmpty(creditTc.code)) {
        creditTcHasReversalSuffix = creditTc.code.endsWith(transaction_constant_1.REVERSAL_TC_SUFFIX);
    }
    const creditTcHasFee = ((_a = creditTc) === null || _a === void 0 ? void 0 : _a.feeRules) !== fee_constant_1.FeeRuleType.ZERO_FEE_RULE &&
        !creditTcHasReversalSuffix;
    // Identify fee from debit transaction code
    let debitTcHasReversalSuffix = false;
    if (!lodash_1.isUndefined(debitTc) && !lodash_1.isEmpty(debitTc.code)) {
        debitTcHasReversalSuffix = debitTc.code.endsWith(transaction_constant_1.REVERSAL_TC_SUFFIX);
    }
    const debitTcHasFee = ((_b = debitTc) === null || _b === void 0 ? void 0 : _b.feeRules) !== fee_constant_1.FeeRuleType.ZERO_FEE_RULE &&
        !debitTcHasReversalSuffix;
    let isZeroFeeRuleApplicable = false;
    if (!lodash_1.isUndefined(creditTc) && !lodash_1.isEmpty(creditTc.feeRules)) {
        isZeroFeeRuleApplicable = creditTc.feeRules === fee_constant_1.FeeRuleType.ZERO_FEE_RULE;
    }
    if (!lodash_1.isUndefined(debitTc) && !lodash_1.isEmpty(debitTc.feeRules)) {
        isZeroFeeRuleApplicable = debitTc.feeRules === fee_constant_1.FeeRuleType.ZERO_FEE_RULE;
    }
    return (creditTcHasFee || debitTcHasFee) && !isZeroFeeRuleApplicable;
};
exports.extractTransactionInfoByInterchange = (transactionModel, interchange, mustMatch) => {
    var _a, _b, _c, _d, _e, _f, _g;
    let debitTc;
    if (transactionModel.debitTransactionCodeMapping &&
        transactionModel.debitTransactionCodeMapping.length) {
        if (!interchange && !mustMatch) {
            debitTc = transactionModel.debitTransactionCodeMapping.find(tc => tc.interchange === transactionModel.debitPriority);
            // when create transaction without interchange, just get the first value
            if (lodash_1.isEmpty(debitTc))
                debitTc = transactionModel.debitTransactionCodeMapping[0];
        }
        else {
            debitTc = transactionModel.debitTransactionCodeMapping.find(tc => tc.interchange === interchange);
            debitTc =
                debitTc ||
                    transactionModel.debitTransactionCodeMapping.find(tc => !tc.interchange // fall back to default if interchange not match
                    );
            if (!debitTc) {
                // debit configuration is not empty but none matches interchange
                logger_1.default.error('extractTransactionInfoByInterchange: debit configuration for interchange not found');
                throw new AppError_1.TransferAppError('Debit configuration for interchange not found when extracting transaction info by interchange!', transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.DEBIT_INTERCHANGE_NOT_FOUND);
            }
        }
    }
    let creditTc;
    if (transactionModel.creditTransactionCodeMapping &&
        transactionModel.creditTransactionCodeMapping.length) {
        if (!interchange && !mustMatch) {
            creditTc = transactionModel.creditTransactionCodeMapping.find(tc => tc.interchange === transactionModel.creditPriority);
            // when create transaction without interchange, just get the first value
            if (lodash_1.isEmpty(creditTc))
                creditTc = transactionModel.creditTransactionCodeMapping[0];
        }
        else {
            creditTc = transactionModel.creditTransactionCodeMapping.find(tc => tc.interchange === interchange);
            creditTc =
                creditTc ||
                    transactionModel.creditTransactionCodeMapping.find(tc => !tc.interchange // fall back to default if interchange not match
                    );
            if (!creditTc) {
                // credit configuration is not empty but none matches interchange
                logger_1.default.error('extractTransactionInfoByInterchange: credit configuration for interchange not found');
                throw new AppError_1.TransferAppError('Credit configuration for interchange not found when extracting transaction info by interchange!', transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.CREDIT_INTERCHANGE_NOT_FOUND);
            }
        }
    }
    let feeCodeInfo;
    if (isFeeApplicable(creditTc, debitTc) &&
        transactionModel.feeMapping &&
        transactionModel.feeMapping.length) {
        if (!interchange && !mustMatch) {
            feeCodeInfo = transactionModel.feeMapping.find(feeMapping => feeMapping.interchange === transactionModel.debitPriority);
            // when create transaction without interchange, just get the first value
            if (lodash_1.isEmpty(feeCodeInfo))
                feeCodeInfo = transactionModel.feeMapping[0];
        }
        else {
            feeCodeInfo = transactionModel.feeMapping.find(fc => fc.interchange === interchange);
            feeCodeInfo =
                feeCodeInfo ||
                    transactionModel.feeMapping.find(fc => !fc.interchange // fall back to default if interchange not match
                    );
            if (!feeCodeInfo) {
                // fee configuration is not empty but none matches interchange
                logger_1.default.error('extractTransactionInfoByInterchange: fee configuration for interchange not found');
                throw new AppError_1.TransferAppError('Fee configuration for interchange not found when extracting transaction info by interchange!', transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.FEE_INTERCHANGE_NOT_FOUND);
            }
        }
    }
    return {
        debitTransactionCode: (_a = debitTc) === null || _a === void 0 ? void 0 : _a.code,
        debitTransactionChannel: (_b = debitTc) === null || _b === void 0 ? void 0 : _b.channel,
        debitLimitGroupCode: (_c = debitTc) === null || _c === void 0 ? void 0 : _c.limitGroupCode,
        debitAwardGroupCounter: (_d = debitTc) === null || _d === void 0 ? void 0 : _d.awardGroupCounter,
        creditTransactionCode: (_e = creditTc) === null || _e === void 0 ? void 0 : _e.code,
        creditTransactionChannel: (_f = creditTc) === null || _f === void 0 ? void 0 : _f.channel,
        fees: feeCodeInfo && [feeCodeInfo],
        categoryCode: transactionModel.userSelectedCategoryCode || ((_g = debitTc) === null || _g === void 0 ? void 0 : _g.defaultCategoryCode)
    };
};
exports.validateManualAdjustmentInterchangeAndChannel = (bankCodeChannel, interchange) => {
    if (!lodash_1.includes([
        module_common_1.BankChannelEnum.EXTERNAL,
        module_common_1.BankChannelEnum.GOBILLS,
        module_common_1.BankChannelEnum.IRIS,
        module_common_1.BankChannelEnum.WINCOR,
        module_common_1.BankChannelEnum.TOKOPEDIA,
        module_common_1.BankChannelEnum.QRIS
    ], bankCodeChannel)) {
        logger_1.default.error(`validateManualAdjustmentInterchangeAndChannel: invalid transaction bank channel ${bankCodeChannel} for manual adjustment`);
        throw new AppError_1.TransferAppError(`Manual adjustment validation failed. Invalid transaction bank channel ${bankCodeChannel} for manual adjustment!`, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_BANK_CHANNEL);
    }
    else if (lodash_1.includes([
        module_common_1.BankChannelEnum.GOBILLS,
        module_common_1.BankChannelEnum.IRIS,
        module_common_1.BankChannelEnum.TOKOPEDIA,
        module_common_1.BankChannelEnum.QRIS
    ], bankCodeChannel) &&
        interchange) {
        logger_1.default.error(`validateManualAdjustmentInterchangeAndChannel: interchange not expected ${bankCodeChannel}  and ${interchange} for manual adjustment`);
        throw new AppError_1.TransferAppError(`Manual adjustment validation failed. Interchange not expected ${bankCodeChannel} and ${interchange} for manual adjustment!`, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INTERCHANGE_NOT_EXPECTED);
    }
};
exports.validateTransactionForManualAdjustment = (transaction, reqStatus, interchange) => {
    if (reqStatus === transaction.status) {
        logger_1.default.error(`validateTransactionForManualAdjustment: Invalid transaction status to manually adjusted for txnId: ${transaction.id}`);
        throw new AppError_1.TransferAppError(`Manual adjustment validation failed. Invalid transaction status to manually adjusted for txnId: ${transaction.id}!`, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_TRANSACTION_STATUS);
    }
    if (!dateUtils_1.isDateWithinYear(transaction.updatedAt)) {
        logger_1.default.error(`validateTransactionForManualAdjustment: Transaction ${transaction.id} with external Id ${transaction.externalId} is not eligible for Manual adjustment as transaction date is not withing limits.`);
        throw new AppError_1.TransferAppError(`Manual adjustment validation failed. Transaction ${transaction.id} with external Id ${transaction.externalId} is not eligible for Manual adjustment as transaction date is not withing limits!`, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.TRANSACTION_DATE_NOT_ALLOWED);
    }
    exports.validateManualAdjustmentInterchangeAndChannel(transaction.beneficiaryBankCodeChannel, interchange);
};
exports.updateExtraFields = (payload) => ({
    additionalInformation1: payload.additionalInformation1,
    additionalInformation2: payload.additionalInformation2,
    additionalInformation3: payload.additionalInformation3,
    additionalInformation4: payload.additionalInformation4,
    sourceCIF: payload.sourceCIF
});
exports.submitTransactionWithRetry = (fn, ...args) => __awaiter(void 0, void 0, void 0, function* () {
    return yield module_common_1.Util.retry(exports.RetryConfig(), fn, ...args);
});
exports.submitIdempotentTransactionWithRetry = (fn, ...args) => __awaiter(void 0, void 0, void 0, function* () {
    return yield module_common_1.Util.retry(exports.IdempotentRetryConfig(), fn, ...args);
});
/**
 * Persist the latest transaction data and load the latest transaction data to/from DB before/after calling thirdPartySubmissionFunction,
 * to prevent data loss in case of race condition caused by out-of-order confirmation from third party.
 *
 * Retry the submission, on Http connection error.
 *
 * @param thirdPartySubmissionFunction third party submission function
 * @param thirdPartySubmissionPayload payload to submit to third party
 * @param transactionModel transaction data
 * @returns updated transaction data
 */
exports.submitThirdPartyTransactionWithRetryAndUpdate = (thirdPartySubmissionFunction, thirdPartySubmissionPayload, transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    yield transaction_repository_1.default.update(transactionModel.id, Object.assign(Object.assign({}, transactionModel), { status: transaction_enum_1.TransactionStatus.SUBMITTING, journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.SUBMITTING_THIRD_PARTY, transactionModel) }));
    const thirdPartySubmissionResponse = yield module_common_1.Util.retry(exports.RetryConfig(), thirdPartySubmissionFunction, thirdPartySubmissionPayload);
    transactionModel =
        (yield transaction_repository_1.default.findOneByQuery({
            _id: transactionModel.id
        })) || transactionModel;
    return {
        submissionResponse: thirdPartySubmissionResponse,
        updatedTransaction: transactionModel
    };
});
exports.checkTransferSameCifForMigratingAccounts = (sourceAccount, beneficiaryAccount) => {
    return !((sourceAccount.status == account_enum_1.AccountStatusEnum.MIGRATING ||
        beneficiaryAccount.status == account_enum_1.AccountStatusEnum.MIGRATING) &&
        sourceAccount.cif !== beneficiaryAccount.cif);
};
/**
 * whether bank code is external bank
 *
 * @param bankCode the bank code
 */
exports.isExternalBank = (bankCode) => {
    return bankCode.channel === module_common_1.BankChannelEnum.EXTERNAL;
};
/**
 * whether bank code is BIFast bank
 *
 * @param bankCodeId the bank code id
 */
exports.isBIFastBank = (bankCodeId) => {
    return bankCodeId === bifast_constant_1.BIFAST_PROXY_BANK_CODE;
};
exports.mapToDisbursementTransaction = (transactionModel, transactionChannel) => {
    var _a, _b;
    /**
     * If defined always in UTC timezone
     */
    const loanTransactionDate = (_a = transactionModel.additionalPayload) === null || _a === void 0 ? void 0 : _a.loanTransactionDate;
    const currentDate = moment_1.default().utcOffset(constant_1.UTC_OFFSET_WESTERN_INDONESIAN);
    const disbursementPayload = {
        transactionDetails: {
            transactionChannelId: transactionChannel
        },
        externalId: lodash_1.get(transactionModel, 'additionalPayload.coreBankingExternalId')
    };
    if (loanTransactionDate) {
        const disbursementDate = moment_1.default
            .utc(loanTransactionDate)
            .utcOffset(constant_1.UTC_OFFSET_WESTERN_INDONESIAN);
        if (moment_1.default(disbursementDate).isBefore(currentDate, 'day')) {
            disbursementPayload.valueDate = moment_1.default(disbursementDate)
                .utcOffset(constant_1.UTC_OFFSET_WESTERN_INDONESIAN)
                .format(coreBanking_constant_1.default.VALUE_DATE_MAMBU_FORMAT);
            disbursementPayload.bookingDate = currentDate.format(coreBanking_constant_1.default.VALUE_DATE_MAMBU_FORMAT);
        }
    }
    if ((_b = transactionModel.additionalPayload) === null || _b === void 0 ? void 0 : _b.shouldDisburseWithAmount) {
        disbursementPayload.amount = transactionModel.transactionAmount;
    }
    return disbursementPayload;
};
exports.rounding = (amount) => {
    return +(Math.round(parseFloat(amount + `e+2`)) + 'e-2');
};
exports.mapToRepaymentTransaction = (transactionModel, transactionChannel, bankIncomeAmount) => {
    var _a, _b;
    const repaymentDate = (_a = transactionModel.additionalPayload) === null || _a === void 0 ? void 0 : _a.loanTransactionDate;
    const allowance = lodash_1.get(transactionModel.additionalPayload, 'allowance', 0);
    const repaymentAmountForPayOffInterestSettlement = lodash_1.get(transactionModel.additionalPayload, 'repaymentAmountForPayOffInterestSettlement');
    const repaymentAmount = repaymentAmountForPayOffInterestSettlement ||
        transactionModel.transactionAmount - bankIncomeAmount - allowance;
    const repaymentPayload = {
        externalId: lodash_1.get(transactionModel, 'additionalPayload.coreBankingExternalId'),
        amount: exports.rounding(repaymentAmount),
        transactionDetails: {
            transactionChannelId: transactionChannel
        }
    };
    if (repaymentDate) {
        repaymentPayload.valueDate = moment_1.default(repaymentDate)
            .utcOffset(constant_1.UTC_OFFSET_WESTERN_INDONESIAN)
            .format(coreBanking_constant_1.default.VALUE_DATE_MAMBU_FORMAT);
    }
    if ((_b = transactionModel.additionalPayload) === null || _b === void 0 ? void 0 : _b.loanRepaymentAllocation) {
        repaymentPayload.customPaymentAmounts = transactionModel.additionalPayload
            .loanRepaymentAllocation
            .filter(repaymentAllocation => repaymentAllocation.type in loan_enum_1.AmountTypeEnum)
            .map(amountDetail => {
            return {
                amount: amountDetail.amount,
                customPaymentAmountType: amountDetail.type
            };
        });
    }
    if (repaymentPayload.externalId) {
        repaymentPayload.externalId = `${repaymentPayload.externalId}-rp`;
    }
    return repaymentPayload;
};
exports.mapToPayOffTransaction = (transactionModel, transactionChannel) => {
    var _a;
    const payOffPayload = {
        externalId: lodash_1.get(transactionModel, 'additionalPayload.coreBankingExternalId'),
        transactionDetails: {
            transactionChannelId: transactionChannel
        },
        notes: transactionModel.note
    };
    const allocations = (_a = transactionModel.additionalPayload) === null || _a === void 0 ? void 0 : _a.loanRepaymentAllocation;
    if (allocations) {
        const interestPaid = (allocations
            .filter(allocation => allocation.type === loan_enum_1.AmountTypeEnum.INTEREST)
            .map(allocation => allocation.amount)[0] || 0);
        const penaltyPaid = (allocations
            .filter(allocation => allocation.type === loan_enum_1.AmountTypeEnum.PENALTY)
            .map(allocation => allocation.amount)[0] || 0);
        payOffPayload.payOffAdjustableAmounts = {
            interestPaid,
            penaltyPaid
        };
    }
    if (payOffPayload.externalId) {
        payOffPayload.externalId = `${payOffPayload.externalId}-po`;
    }
    return payOffPayload;
};
const composeGlAccountAmount = (glTransactionDetail, type) => {
    return glTransactionDetail.reduce((result, item) => {
        if (item.type === type) {
            result.push({
                glAccount: item.accountNumber,
                amount: item.amount
            });
        }
        return result;
    }, []);
};
exports.mapToGlIncomeTransaction = (transactionModel, isReversal = false, transactionId) => {
    var _a;
    const transactionDate = ((_a = transactionModel.additionalPayload) === null || _a === void 0 ? void 0 : _a.loanTransactionDate) ? transactionModel.additionalPayload.loanTransactionDate
        : new Date();
    const date = moment_1.default(transactionDate)
        .utcOffset(constant_1.UTC_OFFSET_WESTERN_INDONESIAN)
        .format(coreBanking_constant_1.default.VALUE_DATE_MAMBU_FORMAT);
    const glIncomePayload = {
        date,
        branchId: lodash_1.get(transactionModel, 'additionalPayload.branchCode'),
        notes: transactionModel.note
    };
    if (transactionId) {
        glIncomePayload.transactionId = transactionId;
    }
    if (transactionModel.additionalPayload &&
        transactionModel.additionalPayload.glTransactionDetail) {
        const glTransactionDetail = transactionModel.additionalPayload
            .glTransactionDetail;
        const creditTransactions = composeGlAccountAmount(glTransactionDetail, transaction_enum_1.GlTransactionTypes.CREDIT);
        const debitTransactions = composeGlAccountAmount(glTransactionDetail, transaction_enum_1.GlTransactionTypes.DEBIT);
        glIncomePayload.credits = isReversal
            ? debitTransactions
            : creditTransactions;
        glIncomePayload.debits = isReversal
            ? creditTransactions
            : debitTransactions;
    }
    return glIncomePayload;
};
const validateRequiredField = (payload, field, errorList) => {
    if (!payload[field]) {
        errorList.push({
            code: errors_1.ERROR_CODE.INVALID_ADDITIONAL_PAYLOAD,
            key: field,
            message: 'is required'
        });
    }
};
const validateGlTransactionDetail = (payload, generalLedgers, errorList) => {
    const glTransactionDetail = payload.glTransactionDetail;
    glTransactionDetail.forEach(element => {
        validateRequiredField(element, 'glAccountCode', errorList);
        validateRequiredField(element, 'amount', errorList);
        validateRequiredField(element, 'type', errorList);
        common_util_1.enforceStringFormat(element, 'glAccountCode');
        common_util_1.enforceStringFormat(element, 'type');
        common_util_1.enforceNumberFormat(element, 'amount');
        if (!Object.values(transaction_enum_1.GlTransactionTypes).includes(element.type)) {
            errorList.push({
                code: errors_1.ERROR_CODE.INCORRECT_FIELD,
                key: 'type',
                message: 'invalid value'
            });
        }
        const generalLedger = generalLedgers.find(glItem => {
            return element.glAccountCode === glItem.code;
        });
        if (!generalLedger) {
            errorList.push({
                code: errors_1.ERROR_CODE.INCORRECT_FIELD,
                key: 'glAccountCode',
                message: 'invalid value'
            });
            return;
        }
        element.accountNumber = generalLedger.accountNumber;
    });
};
exports.loanAdditionalPayloadValidator = (transactionModel, generalLedgers = []) => {
    const payload = transactionModel.additionalPayload;
    const errorList = [];
    if (payload) {
        common_util_1.enforceBooleanFormat(payload, 'shouldDisburseWithAmount');
        if (payload.glTransactionDetail)
            validateGlTransactionDetail(payload, generalLedgers, errorList);
        if (payload.loanRepaymentAllocation) {
            payload.loanRepaymentAllocation.forEach(element => {
                validateRequiredField(element, 'amount', errorList);
                validateRequiredField(element, 'type', errorList);
                common_util_1.enforceNumberFormat(element, 'amount');
                common_util_1.enforceStringFormat(element, 'type');
            });
        }
        if (payload.repaymentAmountForPayOffInterestSettlement)
            common_util_1.enforceNumberFormat(payload, 'repaymentAmountForPayOffInterestSettlement');
        if (!lodash_1.isEmpty(payload.allowance)) {
            common_util_1.enforceNumberFormat(payload, 'allowance');
        }
    }
    if (errorList.length > 0) {
        logger_1.default.error(`Invalid additional payload: ${JSON.stringify(errorList)}`);
        throw new AppError_1.TransferAppError(`Invalid additional payload!`, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_ADDITIONAL_PAYLOAD, errorList);
    }
};
exports.isGlTransactionDetailFieldExists = (transactionModel) => {
    return !!(transactionModel.additionalPayload &&
        transactionModel.additionalPayload.glTransactionDetail);
};
exports.qrisAdditionalPayloadValidator = (payload) => {
    const errorList = [];
    if (lodash_1.isEmpty(payload)) {
        throw new AppError_1.TransferAppError(`Additional payload is empty!`, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.ADDITIONAL_PAYLOAD_NOT_EXIST);
    }
    // TODO: extend ITransactionModel to make this many validation as a mandatory
    validateRequiredField(payload, 'currencyCode', errorList);
    validateRequiredField(payload, 'amount', errorList);
    validateRequiredField(payload, 'nationalMid', errorList);
    validateRequiredField(payload.merchant, 'pan', errorList);
    validateRequiredField(payload.merchant, 'id', errorList);
    validateRequiredField(payload.merchant, 'criteria', errorList);
    validateRequiredField(payload.merchant, 'name', errorList);
    validateRequiredField(payload.merchant, 'city', errorList);
    validateRequiredField(payload.merchant, 'mcc', errorList);
    validateRequiredField(payload.merchant, 'postalCode', errorList);
    validateRequiredField(payload.merchant, 'countryCode', errorList);
    validateRequiredField(payload.customer, 'pan', errorList);
    validateRequiredField(payload.customer, 'name', errorList);
    common_util_1.enforceStringFormat(payload, 'currencyCode');
    common_util_1.enforceNumberFormat(payload, 'amount');
    common_util_1.enforceNumberFormat(payload, 'fee');
    common_util_1.enforceStringFormat(payload, 'nationalMid');
    common_util_1.enforceStringFormat(payload, 'terminalLabel');
    common_util_1.enforceStringFormat(payload.merchant, 'merchant.pan');
    common_util_1.enforceStringFormat(payload.merchant, 'merchant.id');
    common_util_1.enforceStringFormat(payload.merchant, 'merchant.criteria');
    common_util_1.enforceStringFormat(payload.merchant, 'merchant.name');
    common_util_1.enforceStringFormat(payload.merchant, 'merchant.city');
    common_util_1.enforceStringFormat(payload.merchant, 'merchant.mcc');
    common_util_1.enforceStringFormat(payload.merchant, 'merchant.postalCode');
    common_util_1.enforceStringFormat(payload.merchant, 'merchant.countryCode');
    common_util_1.enforceStringFormat(payload.customer, 'customer.pan');
    common_util_1.enforceStringFormat(payload.customer, 'customer.name');
    if (errorList.length > 0) {
        logger_1.default.error(`qrisAdditionalPayloadValidator: ${JSON.stringify(errorList)}`);
        throw new AppError_1.TransferAppError(`Qris additional payload validation failed!`, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_ADDITIONAL_PAYLOAD, errorList);
    }
};
exports.mapToQrisTransaction = (transactionModel, additionalPayload) => {
    return {
        date_time: moment_1.default().format('YYYY-MM-DD hh:mm:ss.SSS[Z]'),
        authorization_id: alto_utils_1.default.generateAuthorizationId(),
        customer_reference_number: transactionModel.externalId,
        currency_code: additionalPayload.currencyCode || transaction_constant_1.DEFAULT_ORIGINAL_CURRENCY,
        amount: additionalPayload.amount,
        fee: additionalPayload.fee,
        issuer_nns: alto_constant_1.ISSUER_NNS,
        national_mid: additionalPayload.nationalMid,
        additional_data: additionalPayload.additionalData,
        terminal_label: additionalPayload.terminalLabel,
        merchant: {
            pan: transactionModel.beneficiaryAccountNo || '',
            id: additionalPayload.merchant.id,
            criteria: additionalPayload.merchant.criteria,
            name: additionalPayload.merchant.name,
            city: additionalPayload.merchant.city,
            mcc: additionalPayload.merchant.mcc,
            postal_code: additionalPayload.merchant.postalCode,
            country_code: additionalPayload.merchant.countryCode
        },
        customer: {
            pan: alto_utils_1.default.generateCustomerPan(additionalPayload.customer.pan),
            name: additionalPayload.customer.name,
            account_type: alto_enum_1.AltoAccountTypeEnum.SAVING
        }
    };
};
exports.mapToQrisTransferDb = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    const additionalPayload = transactionModel.additionalPayload;
    const nnsPrefix = additionalPayload.merchant.pan.substring(0, 8);
    const nnsData = yield configuration_repository_1.default.getNNSMapping(nnsPrefix);
    if (nnsData === undefined) {
        throw new AppError_1.TransferAppError(`Qris NNS mapping data is undefined!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.QRIS_NNS_MAPPING_NOT_FOUND);
    }
    const beneficiaryAccountNo = alto_utils_1.default.generateBeneficiaryAccountNumberWithLundCheckDigit(additionalPayload.merchant.pan);
    const customerPan = alto_utils_1.default.generateCustomerPan(additionalPayload.customer.pan);
    return {
        beneficiaryAccountName: additionalPayload.merchant.name,
        beneficiaryAccountNo: beneficiaryAccountNo,
        beneficiaryAccountType: additionalPayload.merchant.mcc,
        beneficiaryBankName: nnsPrefix,
        thirdPartyOutgoingId: transactionModel.externalId,
        additionalInformation1: nnsData.brandName,
        additionalInformation2: customerPan,
        additionalInformation3: additionalPayload.merchant.city
    };
});
const isBIFastTransaction = (transaction) => { var _a; return ((_a = transaction.paymentServiceCode) === null || _a === void 0 ? void 0 : _a.indexOf(module_common_1.BankChannelEnum.BIFAST)) !== -1; };
exports.getRefundServiceType = (originalTransaction) => {
    return alto_utils_1.default.isQrisTransaction(originalTransaction)
        ? module_common_1.PaymentServiceTypeEnum.VOID_QRIS
        : isBIFastTransaction(originalTransaction)
            ? module_common_1.PaymentServiceTypeEnum.VOID_BIFAST
            : module_common_1.PaymentServiceTypeEnum.GENERAL_REFUND;
};
exports.mapTransactionModelToRefundTransaction = (transactions) => transactions.map(transaction => {
    return {
        id: transaction.id,
        status: transaction.status,
        amount: transaction.transactionAmount,
        createdAt: transaction.createdAt,
        updatedAt: transaction.updatedAt
    };
});
const readOnlyModeStorage = new async_hooks_1.AsyncLocalStorage();
exports.withReadOnlyMode = (f) => {
    return readOnlyModeStorage.run(true, f);
};
exports.isReadOnlyMode = () => {
    var _a;
    return _a = readOnlyModeStorage.getStore(), (_a !== null && _a !== void 0 ? _a : false);
};
const transactionUtility = {
    submitTransactionWithRetry: exports.submitTransactionWithRetry,
    submitThirdPartyTransactionWithRetryAndUpdate: exports.submitThirdPartyTransactionWithRetryAndUpdate
};
exports.default = logger_1.wrapLogs(transactionUtility);
//# sourceMappingURL=transaction.util.js.map