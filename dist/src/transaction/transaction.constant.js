"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const alto_constant_1 = require("../alto/alto.constant");
const bifast_constant_1 = require("../bifast/bifast.constant");
const billPaymentTransaction_enum_1 = require("../billPayment/billPaymentTransaction/billPaymentTransaction.enum");
const transaction_enum_1 = require("./transaction.enum");
exports.RTOL_LIMIT_GROUP_CODE = 'L002';
exports.SKN_LIMIT_GROUP_CODE = 'L003';
exports.RTGS_LIMIT_GROUP_CODE = 'L004';
exports.BILL_LIMIT_GROUP_CODE = 'L009';
exports.BIFAST_LIMIT_GROUP_CODE = 'L019';
exports.CONFIG_KEY_BLOCKING_TIMEOUT_DAYS = 'blockingTransactionTimeoutInDays';
exports.MOMENT_NOW_DAY_FORMAT_STRING = 'd';
exports.TRANSACTION_DATE_FORMAT_STRING = 'YYMMDDhhmmss';
exports.GENERATE_TRANSACTION_DATE_FORMAT = 'YYYYMMDD';
exports.DEFAULT_EXCHANGE_RATE = 1;
exports.DEFAULT_ORIGINAL_CURRENCY = 'IDR';
exports.FIELD_RRN = 'RRN';
exports.FIELD_EXTERNALID = 'EXTERNALID';
exports.FIELD_ACCOUNT_NO = 'ACCOUNT_NO';
exports.FIELD_AMOUNT = 'AMOUNT';
exports.FIELD_TRANSACTION_DATE = 'TRANSACTION_DATE';
exports.TIMEOUT_ERROR_MESSAGE_FROM_SWITCHING = 'TIMEOUT';
exports.REVERSED_REASON_ERROR_FROM_SWITCHING = 'ERROR_FROM_SWITCHING';
exports.FIELD_THIRDPARTY_INCOMING_ID = 'thirdPartyIncomingId';
exports.TRANSACTION_RESPONSE_TOPIC_FIELD = 'Transaction-Response-Topic';
exports.JAGO_BANK_CODE = 'BC002';
exports.JAGO_SHARIA_BANK_CODE = 'BC777';
exports.default = {
    MAX_LENGTH_EXTERNAL_ID: 100,
    MAX_LENGTH_PAYMENT_REQUEST_ID: 40,
    MAX_LENGTH_REFERENCE_ID: 40,
    MAX_LENGTH_SOURCE_CIF: 40,
    MAX_LENGTH_SOURCE_ACCOUNT_TYPE: 10,
    MAX_LENGTH_SOURCE_ACCOUNT_NO: 40,
    MAX_LENGTH_SOURCE_BANK_CODE: 10,
    MAX_LENGTH_SOURCE_TRANSACTION_CURRENCY: 10,
    MAX_LENGTH_BENEFICIARY_CIF: 40,
    MAX_LENGTH_BENEFICIARY_BANK_CODE: 10,
    MAX_LENGTH_BENEFICIARY_ACCOUNT_TYPE: 10,
    MAX_LENGTH_BENEFICIARY_ACCOUNT_NO: 40,
    MAX_LENGTH_BENEFICIARY_ACCOUNT_NAME: 100,
    MAX_LENGTH_CHANNEL_TARGET: 20,
    MAX_LENGTH_NETWORK_PRIORITY: 20,
    MAX_LENGTH_INSTITUTIONAL_TYPE: 10,
    MAX_LENGTH_NOTE: 100,
    MAX_LENGTH_CATEGORY_CODE: 10,
    MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION: 250,
    MAX_LENGTH_WORKFLOW_ID: 100,
    LENGTH_TRANSACTION_DATE_CONFIRMATION: 12,
    OBJECT_PROPERTY_BANK_CODE: 'bankCode',
    BENEFICIARY_BANK_CODE_COL: 'beneficiaryBankCode',
    SOURCE_BANK_CODE_COL: 'sourceBankCode',
    CHANNEL_TARGET_COL: 'channelTarget',
    INSTITUTIONAL_TYPE_COL: 'institutionalType',
    CATEGORY_CODE_COL: 'categoryCode',
    AMOUNT_IS_LIMITED: 'Transaction amount is limited',
    MESSAGE_HEADER_SERVICE_NAME: 'service-name',
    SOURCE_CIF_COL: 'sourceCIF',
    BENEFICIARY_ACCOUNT_NO_COL: 'beneficiaryAccountNo',
    MAX_LENGTH_CUSTOMER_ID: 40,
    MAX_LENGTH_TRANSACTION_RESPONSE_MESSAGE: 100,
    MAX_LENGTH_TRANSACTION_RESPONSE_ID: 12,
    MAX_LENGTH_INTERCHANGE_FIELD: 30,
    MAX_LENGTH_PROMOTION_CODE: 40,
    MAX_LENGTH_CURRENCY_ISO_NO: 5,
    DEFAULT_RECORD_PER_PAGE: 20,
    MAX_RECORD_PER_PAGE: 10000
};
exports.BI_FAST_MAX_TRANSACTION_AMOUNT = 250000000;
exports.EURONET_REFUND_EXTERNAL_ID_LENGTH = 12;
exports.PAYMENT_SERVICE_CODE_REFUND_GENERAL = 'REFUND_GENERAL';
exports.MAP_TXN_STATUS_TO_GOBILL = {
    [transaction_enum_1.TransactionStatus.SUCCEED]: billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.SUCCESS,
    [transaction_enum_1.TransactionStatus.DECLINED]: billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.FAILURE
};
exports.MAP_GOBILL_STATUS_TO_TXN = {
    [billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.SUCCESS]: transaction_enum_1.TransactionStatus.SUCCEED,
    [billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.FAILURE]: transaction_enum_1.TransactionStatus.DECLINED
};
exports.TMZ = 'Asia/Jakarta';
exports.FORMAT_DATE = 'DD-MM-YYYY';
exports.FORMAT_YYYY_MM_DD = 'YYYY-MM-DD';
exports.FORMAT_DATETIME_EMAIL_DETAIL = 'DD MMMM YYYY, HH:mm';
exports.FORMAT_DAY_NAME = 'dddd';
exports.FORMAT_HOUR_MINUS_24H = 'HH:mm';
exports.SWITCHING_ACCOUNT_NOT_FOUND = 'SWITCHING_ACCOUNT_NOT_FOUND';
exports.ACCOUNT_NUMBER_NOT_FOUND = 'ACCOUNT_NUMBER_NOT_FOUND';
exports.DAYS_IN_YEAR = 365;
exports.BANK_KSEI = 'BC1000';
exports.BANK_INCOME = 'BANK_INCOME';
exports.IGNORE_RDN_EXCEPTIONS = ['ENOENT', 'ECONNRESET', 'ETIMEDOUT'];
exports.LOAN_PROCESSOR_PAYMENT_SERVICE_TYPE_LIST = [
    module_common_1.PaymentServiceTypeEnum.LOAN_DIRECT_DISBURSEMENT,
    module_common_1.PaymentServiceTypeEnum.LOAN_DIRECT_REPAYMENT,
    module_common_1.PaymentServiceTypeEnum.LOAN_REPAYMENT
];
exports.RTOL_BIFAST_LIMIT_GROUPS = ['L002', 'L013', 'L019'];
exports.RTOL_BIFAST_FEE = 'RTOL_BIFAST_FEE';
exports.SCHEDULED_RESOLVE_STATUS_TRANSACTION_CODES = [
    alto_constant_1.QRIS_PAYMENT_SERVICE_CODE,
    bifast_constant_1.BIFAST_OUTGOING_PAYMENT_SERVICE_CODE
];
exports.SCHEDULED_RESOLVE_STATUS_MIN_TRANSACTION_AGE_IN_MINUTES = 30;
exports.SCHEDULED_RESOLVE_STATUS_MAX_TRANSACTION_AGE_IN_MINUTES = 4320;
exports.SCHEDULED_RESOLVE_STATUS_FETCH_RECORD_LIMIT = 100;
var ExecutionMethodEnum;
(function (ExecutionMethodEnum) {
    ExecutionMethodEnum["ONE_BY_ONE"] = "ONE_BY_ONE";
    ExecutionMethodEnum["SCHEDULED"] = "SCHEDULED";
    ExecutionMethodEnum["BULK"] = "BULK";
})(ExecutionMethodEnum = exports.ExecutionMethodEnum || (exports.ExecutionMethodEnum = {}));
exports.REVERSAL_TC_SUFFIX = '-R';
exports.IDR_CURRENCY_PREFIX = 'Rp';
//# sourceMappingURL=transaction.constant.js.map