"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const lodash_1 = __importDefault(require("lodash"));
const billPayment_service_1 = __importDefault(require("../billPayment/billPayment.service"));
const logger_1 = __importDefault(require("../logger"));
const generalRefund_service_1 = __importDefault(require("./refund/generalRefund.service"));
const transaction_service_1 = __importDefault(require("./transaction.service"));
const transaction_util_1 = require("./transaction.util");
const transaction_validator_1 = require("./transaction.validator");
const transactionBlocking_service_1 = __importDefault(require("./transactionBlocking.service"));
const transactionConfirmation_service_1 = __importDefault(require("./transactionConfirmation.service"));
const transactionExternal_service_1 = __importDefault(require("./transactionExternal.service"));
const transactionManualAdjustment_service_1 = __importDefault(require("./transactionManualAdjustment.service"));
const transactionReversal_service_1 = __importDefault(require("./transactionReversal.service"));
const contextHandler_1 = require("../common/contextHandler");
const transaction_enum_1 = require("./transaction.enum");
const common_util_1 = require("../common/common.util");
const authentication_util_1 = require("../common/authentication.util");
const entitlement_service_1 = __importDefault(require("../entitlement/entitlement.service"));
const entitlementFlag_1 = __importDefault(require("../common/entitlementFlag"));
const transactionUsage_service_1 = __importDefault(require("../transactionUsage/transactionUsage.service"));
const constant_1 = require("../common/constant");
const recommendPaymentServices = {
    method: module_common_1.Http.Method.POST,
    path: '/recommend-payment-services',
    options: {
        description: 'Recommend payment service code',
        notes: 'The api is to calculate the transaction fee and to acquire a payment service code which is used to submit a transaction later',
        tags: ['api', 'transactions', 'admin'],
        validate: {
            payload: transaction_validator_1.recommendPaymentServiceRequestValidator
        },
        response: {
            schema: transaction_validator_1.recommendPaymentServiceResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const startTime = new Date().getTime();
            logger_1.default.debug(`recommendPaymentServices: before calling recommendPaymentService : ${new Date().getTime() -
                startTime}ms to complete`);
            const serviceRecommendations = yield transaction_service_1.default.recommendPaymentService(hapiRequest.payload);
            logger_1.default.debug(`recommendPaymentServices: after calling recommendPaymentService : ${new Date().getTime() -
                startTime}ms to complete`);
            const response = serviceRecommendations.map(serviceRecommendation => {
                const feeData = serviceRecommendation.feeData;
                return {
                    paymentServiceCode: serviceRecommendation.paymentServiceCode,
                    debitTransactionCode: lodash_1.default.get(serviceRecommendation, ['debitTransactionCode', 0, 'transactionCode'], null),
                    creditTransactionCode: lodash_1.default.get(serviceRecommendation, ['creditTransactionCode', 0, 'transactionCode'], null),
                    realBeneficiaryBankCode: lodash_1.default.get(serviceRecommendation, 'beneficiaryBankCode', null),
                    feeAmount: lodash_1.default.get(feeData, [0, 'feeAmount'], null),
                    customerTc: lodash_1.default.get(feeData, [0, 'customerTc'], null),
                    authenticationRequired: serviceRecommendation.authenticationType
                };
            });
            logger_1.default.debug(`recommendPaymentServices: takes  ${new Date().getTime() -
                startTime}ms to complete`);
            return hapiResponse.response(response).code(module_common_1.Http.StatusCode.OK);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'Success'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const recommendPaymentServicesForAdminPortal = Object.assign(Object.assign({}, recommendPaymentServices), { path: '/admin/recommend-payment-services' });
const recommendPaymentServicesWithoutAuth = Object.assign(Object.assign({}, recommendPaymentServices), { path: '/private/recommend-payment-services', options: Object.assign(Object.assign({}, recommendPaymentServices.options), { description: 'Recommend payment service code', notes: 'Private api to calculate the transaction fee and to acquire a payment service code which is used to submit a transaction later', tags: ['api', 'private', 'transactions'], auth: false }) });
const createTransferTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/transfer-transactions',
    options: {
        description: 'Create a transfer transaction',
        notes: 'Private Api to do Transfer from sourceAccountNo to beneficiaryAccountNo',
        tags: ['api', 'transactions', 'private'],
        auth: false,
        validate: {
            query: transaction_validator_1.customerIdValidator,
            payload: transaction_validator_1.createTransferTransactionRequestValidator
        },
        response: {
            schema: transaction_validator_1.transactionResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { payload, query } = hapiRequest;
            common_util_1.trimStringValuesFromObject(payload);
            const transaction = yield transaction_service_1.default.createTransferTransaction(Object.assign(Object.assign({}, payload), { sourceCustomerId: query.customerId }));
            let response = transaction_util_1.mapTransactionResultToResponse(transaction);
            if (transaction.executionType === transaction_enum_1.ExecutionTypeEnum.NONE_BLOCKING) {
                response = Object.assign(Object.assign({}, response), { transactionStatus: transaction.status });
            }
            return hapiResponse.response(response).code(module_common_1.Http.StatusCode.CREATED);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.CREATED]: {
                        description: 'Transfer transaction is created'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const privateCreateTransferTransaction = Object.assign(Object.assign({}, createTransferTransaction), { path: '/private/transfer-transactions' });
const createInternalTopupTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/top-up-internal-transactions',
    options: {
        description: 'Create internal top up transaction',
        notes: `Private api where internal topup will topup money from main account of customer to partner (GoPay) and none partner (Ovo) wallets`,
        tags: ['api', 'transactions', 'private'],
        auth: false,
        validate: {
            query: transaction_validator_1.customerIdValidator,
            payload: transaction_validator_1.topupInternalTransactionRequestPayload
        },
        response: {
            schema: transaction_validator_1.transactionResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const transaction = yield transaction_service_1.default.createInternalTopupTransaction(Object.assign(Object.assign({}, hapiRequest.payload), { sourceCustomerId: hapiRequest.query.customerId }));
            const response = transaction_util_1.mapTransactionResultToResponse(transaction);
            return hapiResponse.response(response).code(module_common_1.Http.StatusCode.CREATED);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.CREATED]: {
                        description: 'Created'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const privateCreateInternalTopupTransaction = Object.assign(Object.assign({}, createInternalTopupTransaction), { path: '/private/top-up-internal-transactions' });
/**
 * @dikshitthakral
 * To be depreciated as not being used.
 */
const createExternalTopupTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/top-up-external-transactions',
    options: {
        description: 'Create top up transaction triggered from external/switching',
        notes: `An external topup will topup money to partner (GoPay) only`,
        tags: ['api', 'transactions', 'private'],
        auth: false,
        validate: {
            payload: transaction_validator_1.topupExternalTransactionRequestPayload
        },
        response: {
            schema: transaction_validator_1.transactionResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const transaction = yield transactionExternal_service_1.default.createExternalTopupTransaction(hapiRequest.payload);
            const response = transaction_util_1.mapTransactionResultToResponse(transaction);
            return hapiResponse.response(response).code(module_common_1.Http.StatusCode.CREATED);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.CREATED]: {
                        description: 'Created'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const privateCreateExternalTopupTransaction = Object.assign(Object.assign({}, createExternalTopupTransaction), { path: '/private/top-up-external-transactions' });
const createDepositTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/deposit-transactions',
    options: {
        description: 'Create deposit transaction',
        notes: 'Private api to deposit a transaction amount to a given beneficiaryAccountNo',
        tags: ['api', 'transactions', 'private'],
        auth: false,
        validate: {
            query: transaction_validator_1.customerIdValidator,
            payload: transaction_validator_1.createDepositTransactionRequestValidator
        },
        response: {
            schema: transaction_validator_1.transactionResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const transaction = yield transaction_service_1.default.createDepositTransaction(Object.assign(Object.assign({}, hapiRequest.payload), { sourceCustomerId: hapiRequest.query.customerId }));
            const response = transaction_util_1.mapTransactionResultToResponse(transaction);
            return hapiResponse.response(response).code(module_common_1.Http.StatusCode.CREATED);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.CREATED]: {
                        description: 'Created'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const privateCreateDepositTransaction = Object.assign(Object.assign({}, createDepositTransaction), { path: '/private/deposit-transactions' });
const createWithdrawTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/withdraw-transactions',
    options: {
        description: 'Create withdraw transaction',
        notes: 'Private Api to withdraw a transaction amount from a given sourceAccountNo',
        tags: ['api', 'transactions', 'private'],
        auth: false,
        validate: {
            query: transaction_validator_1.customerIdValidator,
            payload: transaction_validator_1.createWithdrawTransactionRequestValidator
        },
        response: {
            schema: transaction_validator_1.transactionResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const transaction = yield transaction_service_1.default.createWithdrawTransaction(Object.assign(Object.assign({}, hapiRequest.payload), { sourceCustomerId: hapiRequest.query.customerId }));
            const response = transaction_util_1.mapTransactionResultToResponse(transaction);
            return hapiResponse.response(response).code(module_common_1.Http.StatusCode.CREATED);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.CREATED]: {
                        description: 'Created'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const privateCreateWithdrawTransaction = Object.assign(Object.assign({}, createWithdrawTransaction), { path: '/private/withdraw-transactions' });
const reverseTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/transactions/{transactionId}/revert',
    options: {
        description: 'Private Api to Reverse a transaction',
        notes: 'All information must be valid',
        tags: ['api', 'transactions', 'private'],
        auth: false,
        validate: {
            params: transaction_validator_1.transactionIdValidator,
            payload: transaction_validator_1.reverseRequestValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { params, payload } = hapiRequest;
            yield transactionReversal_service_1.default.reverseTransaction(params.transactionId, payload);
            return hapiResponse.response().code(module_common_1.Http.StatusCode.NO_CONTENT);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.NO_CONTENT]: {
                        description: 'Revert transaction successfully'
                    },
                    [module_common_1.Http.StatusCode.NOT_FOUND]: {
                        description: 'Not Found'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const privateReverseTransaction = Object.assign(Object.assign({}, reverseTransaction), { path: '/private/transactions/{transactionId}/revert' });
const getTransaction = {
    method: module_common_1.Http.Method.GET,
    path: '/transactions/{transactionId}',
    options: {
        description: 'Private Api to get transaction detail by transactionId or externalId',
        notes: 'All information must be valid',
        tags: ['api', 'transactions', 'private'],
        auth: false,
        validate: {
            params: transaction_validator_1.transactionIdValidator
        },
        response: {
            schema: transaction_validator_1.transactionInfoResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { params } = hapiRequest;
            const transaction = yield transaction_service_1.default.getTransaction(params.transactionId);
            return hapiResponse
                .response(transaction_util_1.toTransactionDetail(transaction))
                .code(module_common_1.Http.StatusCode.OK);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'Success'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.NOT_FOUND]: {
                        description: 'Not found'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const privateGetTransaction = Object.assign(Object.assign({}, getTransaction), { path: '/private/transactions/{transactionId}' });
const getTransactionList = {
    method: module_common_1.Http.Method.POST,
    path: '/admin/transactions',
    options: {
        description: 'Admin Api to get transaction list based on filter parameter',
        notes: 'All information must be valid',
        tags: ['api', 'transactions', 'private', 'admin'],
        validate: {
            query: transaction_validator_1.transactionListQueryValidator,
            payload: transaction_validator_1.transactionListRequestValidator
        },
        response: {
            schema: transaction_validator_1.transactionListResponseVaidator
        },
        bind: {
            roles: [module_common_1.AdminPortalUserPermissionCode.CUSTOMER_INFO_WRITE]
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { payload } = hapiRequest;
            const { roles } = authentication_util_1.getAuthPayloadFromAdminToken(hapiRequest);
            const fetchRefund = hapiRequest.query.fetchRefund === 'true';
            const { transactions, refundTransactions } = yield transaction_service_1.default.getTransactionList(payload, fetchRefund);
            return hapiResponse
                .response(Object.assign(Object.assign({}, transactions), { list: transaction_util_1.toTransactionList(transactions.list, roles, refundTransactions, fetchRefund) }))
                .code(module_common_1.Http.StatusCode.OK);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'Success'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.NOT_FOUND]: {
                        description: 'Not found'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const createBlockingTransferTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/blocking-transfer-transactions',
    options: {
        description: 'Private Api to create a blocking transfer transaction',
        notes: 'Blocking transfer transaction will not execute the transaction yet but temporary block transaction amount. The available balance of account is adjusted based on the blocked amount. The blocked transaction amount can not be used until the transaction is unblocked',
        tags: ['api', 'transactions', 'private'],
        auth: false,
        validate: {
            query: transaction_validator_1.customerIdValidator,
            payload: transaction_validator_1.createTransferTransactionRequestValidator
        },
        response: {
            schema: transaction_validator_1.blockingTransactionResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { payload, query } = hapiRequest;
            const blockingTransactionResponse = yield transactionBlocking_service_1.default.createBlockingTransferTransaction(Object.assign(Object.assign({}, payload), { sourceCustomerId: query.customerId }));
            return hapiResponse
                .response({
                id: blockingTransactionResponse.id
            })
                .code(module_common_1.Http.StatusCode.CREATED);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.CREATED]: {
                        description: 'Submit blocking transfer transaction request successfully'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const privateCreateBlockingTransferTransaction = Object.assign(Object.assign({}, createBlockingTransferTransaction), { path: '/private/blocking-transfer-transactions' });
const adjustBlockingTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/blocking-transfer-transactions/{transactionId}/adjust',
    options: {
        description: 'Private End point to adjust transaction amount of a blocking transfer transaction',
        notes: 'All information must be valid',
        tags: ['api', 'transactions', 'private'],
        auth: false,
        validate: {
            query: transaction_validator_1.cifValidator,
            params: transaction_validator_1.transactionIdValidator,
            payload: transaction_validator_1.blockingTransactionAdjustmentRequestValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { params, payload, query } = hapiRequest;
            yield transactionBlocking_service_1.default.adjustBlockingTransferTransaction(query.cif, params.transactionId, payload.amount);
            return hapiResponse.response().code(module_common_1.Http.StatusCode.NO_CONTENT);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.NO_CONTENT]: {
                        description: 'Adjust blocking transaction successfully'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.NOT_FOUND]: {
                        description: 'Not found'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const privateAdjustBlockingTransaction = Object.assign(Object.assign({}, adjustBlockingTransaction), { path: '/private/blocking-transfer-transactions/{transactionId}/adjust' });
const executeBlockingTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/blocking-transfer-transactions/{transactionId}/execute',
    options: {
        description: 'Private Api to execute a blocking transfer transaction',
        notes: 'All information must be valid',
        tags: ['api', 'transactions', 'private'],
        auth: false,
        validate: {
            query: transaction_validator_1.cifValidator,
            params: transaction_validator_1.transactionIdValidator,
            payload: transaction_validator_1.blockingTransactionAdjustmentRequestValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { params, query, payload } = hapiRequest;
            yield transactionBlocking_service_1.default.executeBlockingTransferTransaction(query.cif, params.transactionId, payload.amount);
            return hapiResponse.response().code(module_common_1.Http.StatusCode.NO_CONTENT);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.NO_CONTENT]: {
                        description: 'Execute blocking transaction successfully'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.NOT_FOUND]: {
                        description: 'Not found'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const privateExecuteBlockingTransaction = Object.assign(Object.assign({}, executeBlockingTransaction), { path: '/private/blocking-transfer-transactions/{transactionId}/execute' });
const cancelBlockingTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/blocking-transfer-transactions/{transactionId}/cancel',
    options: {
        description: 'Private Api to cancel blocking transaction',
        notes: 'All information must be valid',
        tags: ['api', 'transactions', 'private'],
        auth: false,
        validate: {
            query: transaction_validator_1.cifValidator,
            params: transaction_validator_1.transactionIdValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { params, query } = hapiRequest;
            yield transactionBlocking_service_1.default.cancelBlockingTransferTransaction(query.cif, params.transactionId);
            return hapiResponse.response().code(module_common_1.Http.StatusCode.NO_CONTENT);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.NO_CONTENT]: {
                        description: 'cancel blocking transaction successfully'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.NOT_FOUND]: {
                        description: 'Not found'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error.'
                    }
                }
            }
        }
    }
};
const privateCancelBlockingTransaction = Object.assign(Object.assign({}, cancelBlockingTransaction), { path: '/private/blocking-transfer-transactions/{transactionId}/cancel' });
const manualAdjustmentUnBlockingTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/manual-adjustment/unblock/{transactionId}',
    options: {
        description: 'Admin Api to manually adjustment unblock amount',
        notes: 'All information must be valid',
        tags: ['api', 'transactions'],
        bind: {
            roles: [module_common_1.AdminPortalUserPermissionCode.CUSTOMER_INFO_WRITE]
        },
        validate: {
            params: transaction_validator_1.transactionIdValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { params } = hapiRequest;
            yield transactionBlocking_service_1.default.manualUnblockingTransferTransaction(params.transactionId);
            return hapiResponse.response().code(module_common_1.Http.StatusCode.NO_CONTENT);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.NO_CONTENT]: {
                        description: 'manual adjustment unblock amount successfully'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.NOT_FOUND]: {
                        description: 'Not found'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error.'
                    }
                }
            }
        }
    }
};
const confirmTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/transaction-confirmation',
    options: {
        description: 'Private Api to Confirm transaction',
        notes: 'All information must be valid. This endpoint is for switching to callback when they finish processing the transaction request',
        tags: ['api', 'transactions'],
        auth: false,
        validate: {
            query: transaction_validator_1.confirmTransactionQueryValidator,
            payload: transaction_validator_1.confirmTransactionRequestValidator
        },
        response: {
            schema: transaction_validator_1.confirmTransactionResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                const { payload, query } = hapiRequest;
                yield contextHandler_1.updateRequestIdInContext(payload.externalId);
                const confirmTransactionResponse = yield transactionConfirmation_service_1.default.confirmTransaction(payload);
                //reversal for entitlement counterUsage (by transaction confirmation status)
                if (yield entitlementFlag_1.default.isEnabled()) {
                    yield entitlement_service_1.default.processReversalEntitlementCounterUsageOnTransactionConfirmation(payload);
                }
                //reversal for atm withdrawal - daily limit
                //it will check based on the transaction confirmation status - non successful status
                transactionUsage_service_1.default.processReversalPocketLimitByTransactionConfirmationInput(payload);
                const response = transaction_util_1.mapConfirmTransactionResultToExternalResponse(confirmTransactionResponse, query);
                return hapiResponse.response(response).code(module_common_1.Http.StatusCode.OK);
            }
            catch (e) {
                //reversal for entitlement counterUsage due to error being thrown
                if (yield entitlementFlag_1.default.isEnabled()) {
                    yield entitlement_service_1.default.processReversalEntitlementCounterUsageBasedOnContext();
                }
                //reversal for entitlement counterUsage due to error being thrown
                transactionUsage_service_1.default.processReversalPocketLimitByTransactionBasedOnContext();
                return e;
            }
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'Process the transaction confirmation request successfully'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const extTxnManualAdjstment = {
    method: module_common_1.Http.Method.POST,
    path: '/admin/manual-adjustment',
    options: {
        description: 'Admin api is used by admin portal to mark pending / MANUAL_INTERVENTION transaction as successful / failed',
        notes: 'All information must be valid',
        tags: ['private', 'api', 'admin'],
        bind: {
            roles: [module_common_1.AdminPortalUserPermissionCode.CUSTOMER_INFO_WRITE]
        },
        validate: {
            payload: transaction_validator_1.extTxnManualAdjstmentRequestValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { transactionId, status: reqStatus, interchange: interchange } = hapiRequest.payload;
            logger_1.default.info(`extTxnManualAdjstment: Manually updating transaction ${transactionId}, status: ${reqStatus}, interchange: ${interchange}`);
            const txnObject = yield transaction_service_1.default.getTransaction(transactionId);
            logger_1.default.info(`extTxnManualAdjstment: Updating transaction ${transactionId}, 
        oldTransactionstatus: ${txnObject.status}, 
        channel ${txnObject.beneficiaryBankCodeChannel}, sourceAccountNo. ${txnObject.sourceAccountNo},
        beneficiaryAccountNo. ${txnObject.beneficiaryAccountName}, externalId : ${txnObject.externalId}`);
            transaction_util_1.validateTransactionForManualAdjustment(txnObject, reqStatus, interchange);
            const txn = interchange &&
                txnObject.beneficiaryBankCodeChannel === module_common_1.BankChannelEnum.EXTERNAL
                ? Object.assign(Object.assign({}, txnObject), { interchange: interchange }) : txnObject;
            if (txn.beneficiaryBankCodeChannel === module_common_1.BankChannelEnum.EXTERNAL ||
                txn.beneficiaryBankCodeChannel === module_common_1.BankChannelEnum.IRIS ||
                txn.beneficiaryBankCodeChannel === module_common_1.BankChannelEnum.WINCOR ||
                txn.beneficiaryBankCodeChannel === module_common_1.BankChannelEnum.TOKOPEDIA ||
                txn.beneficiaryBankCodeChannel === module_common_1.BankChannelEnum.QRIS) {
                yield transactionManualAdjustment_service_1.default.extTxnManualAdjstment(txn, reqStatus);
            }
            if (txn.beneficiaryBankCodeChannel === module_common_1.BankChannelEnum.GOBILLS) {
                yield billPayment_service_1.default.txnManualAdjustment(transactionId, reqStatus, txnObject.status != transaction_enum_1.TransactionStatus.DECLINED);
            }
            logger_1.default.info(`Manual adjustment completed for transaction id ${txn.id} and external id : ${txn.externalId}`);
            return hapiResponse.response({ success: true }).code(module_common_1.Http.StatusCode.OK);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'Transaction manually adjusted'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.NOT_FOUND]: {
                        description: 'Not found'
                    }
                }
            }
        }
    }
};
const createExternalTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/external-transactions',
    options: {
        description: 'Private Api to Create a external transfer transaction',
        notes: 'This api is to handle any transaction that come from 3rd application',
        tags: ['api', 'transactions'],
        auth: false,
        validate: {
            headers: transaction_validator_1.externalTransactionHeaderValidator,
            query: transaction_validator_1.externalTransactionQueryValidator,
            payload: transaction_validator_1.externalTransactionRequestValidator
        },
        response: {
            schema: transaction_validator_1.externalTransactionResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { payload, query, headers } = hapiRequest;
            try {
                const includeCoreBankingTransactions = query.includeCoreBankingTransactions === 'true';
                const idempotencyKey = headers[constant_1.HttpHeaders.X_IDEMPOTENCY_KEY.toLowerCase()];
                const transaction = yield transactionExternal_service_1.default.createExternalTransaction(payload, idempotencyKey);
                const response = transaction_util_1.mapTransactionResultToExternalResponse(transaction, includeCoreBankingTransactions);
                return hapiResponse.response(response).code(module_common_1.Http.StatusCode.CREATED);
            }
            catch (e) {
                if (yield entitlementFlag_1.default.isEnabled()) {
                    yield entitlement_service_1.default.processReversalEntitlementCounterUsageBasedOnContext();
                }
                //check whether the flag is true for pocket level limit validation
                //perform reversal
                transactionUsage_service_1.default.processReversalPocketLimitByTransactionBasedOnContext();
                return e;
            }
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.CREATED]: {
                        description: 'Transfer transaction is created'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const getExternalTransactionFee = {
    method: module_common_1.Http.Method.POST,
    path: '/external-transaction-fee',
    options: {
        description: 'Private Api to calculate transaction fee',
        notes: 'All information must be valid',
        tags: ['api', 'transactions'],
        auth: false,
        validate: {
            payload: transaction_validator_1.externalTransactionFeeRequestValidator
        },
        response: {
            schema: transaction_validator_1.incomingFeeCheckResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { payload } = hapiRequest;
            const response = yield transactionExternal_service_1.default.getExternalTransactionFee(payload);
            return hapiResponse.response(response).code(module_common_1.Http.StatusCode.OK);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'OK'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const refundTransferTransation = {
    method: module_common_1.Http.Method.POST,
    path: '/admin/refund/{transactionId}',
    options: {
        description: 'Admin Api to Refund transaction by transaction id',
        notes: 'All information must be valid',
        tags: ['api', 'transactions', 'refund', 'admin'],
        bind: {
            roles: [module_common_1.AdminPortalUserPermissionCode.CUSTOMER_INFO_WRITE]
        },
        validate: {
            params: transaction_validator_1.refundTransactionRequestValidator,
            query: transaction_validator_1.refundTransactionQueryValidator
        },
        response: {
            schema: transaction_validator_1.refundTransactionResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { params, query } = hapiRequest;
            const response = yield generalRefund_service_1.default.refundTransferTransaction(params.transactionId, query.refundFee);
            return hapiResponse.response(response).code(module_common_1.Http.StatusCode.CREATED);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.CREATED]: {
                        description: 'Refund transaction successfully created'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const checkTransactionStatus = {
    method: module_common_1.Http.Method.POST,
    path: '/transaction-status',
    options: {
        description: 'Api to check transaction status',
        notes: 'All information must be valid',
        tags: ['api', 'check status', 'transactions'],
        validate: {
            payload: transaction_validator_1.transactionStatusRequestValidator
        },
        response: {
            schema: transaction_validator_1.transactionStatusResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { payload } = hapiRequest;
            yield contextHandler_1.updateRequestIdInContext(payload.paymentInstructionId);
            const result = yield transaction_service_1.default.checkTransactionStatus(payload);
            return hapiResponse.response(result).code(module_common_1.Http.StatusCode.OK);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        describe: 'Transaction found'
                    },
                    [module_common_1.Http.StatusCode.NOT_FOUND]: {
                        describe: 'Transaction not found'
                    }
                }
            }
        }
    }
};
const confirmTransactionStatus = {
    method: module_common_1.Http.Method.GET,
    path: '/private/confirm-transaction-status',
    options: {
        description: 'Api to check and resolve pending transaction status',
        notes: 'All information must be valid',
        tags: ['api', 'update pending status', 'transactions'],
        auth: false,
        handler: (_hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            logger_1.default.info('Endpoint /private/confirm-transaction-status is triggered.');
            logger_1.default.info('Query params: ' + JSON.stringify(_hapiRequest.query));
            let enable = _hapiRequest.query.enable === '1';
            let minTransactionAge = _hapiRequest.query.minTransactionAge
                ? parseInt(_hapiRequest.query.minTransactionAge)
                : undefined;
            let maxTransactionAge = _hapiRequest.query.maxTransactionAge
                ? parseInt(_hapiRequest.query.maxTransactionAge)
                : undefined;
            let recordLimit = _hapiRequest.query.recordLimit
                ? parseInt(_hapiRequest.query.recordLimit)
                : undefined;
            if (enable) {
                yield transaction_service_1.default.resolveTransactionStatus(minTransactionAge, maxTransactionAge, recordLimit);
            }
            return hapiResponse
                .response({
                response: 'success',
                enable: enable
            })
                .code(module_common_1.Http.StatusCode.OK);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        describe: 'Transaction found'
                    },
                    [module_common_1.Http.StatusCode.NOT_FOUND]: {
                        describe: 'Transaction not found'
                    }
                }
            }
        }
    }
};
const checkBulkTransactionFees = {
    method: module_common_1.Http.Method.POST,
    path: '/private/bulk/transaction-fees/v1',
    options: {
        description: 'Api to get transaction fees for a list of transaction amounts',
        notes: 'All information must be valid',
        tags: ['api', 'fees', 'transactions', 'bulk', 'private'],
        auth: false,
        validate: {
            payload: transaction_validator_1.bulkTransactionFeesRequestValidator
        },
        response: {
            schema: transaction_validator_1.bulkTransactionFeesResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { payload } = hapiRequest;
            const response = yield transaction_util_1.withReadOnlyMode(() => transaction_service_1.default.checkBulkTransactionFees(payload));
            return hapiResponse.response(response).code(module_common_1.Http.StatusCode.OK);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'OK'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const transactionController = [
    cancelBlockingTransaction,
    adjustBlockingTransaction,
    getTransaction,
    recommendPaymentServices,
    reverseTransaction,
    createTransferTransaction,
    createDepositTransaction,
    createWithdrawTransaction,
    createBlockingTransferTransaction,
    executeBlockingTransaction,
    createInternalTopupTransaction,
    createExternalTopupTransaction,
    confirmTransaction,
    extTxnManualAdjstment,
    createExternalTransaction,
    getExternalTransactionFee,
    recommendPaymentServicesWithoutAuth,
    refundTransferTransation,
    getTransactionList,
    manualAdjustmentUnBlockingTransaction,
    recommendPaymentServicesForAdminPortal,
    privateCreateTransferTransaction,
    privateCreateInternalTopupTransaction,
    privateCreateExternalTopupTransaction,
    privateCreateDepositTransaction,
    privateCreateWithdrawTransaction,
    privateReverseTransaction,
    privateGetTransaction,
    privateCreateBlockingTransferTransaction,
    privateAdjustBlockingTransaction,
    privateExecuteBlockingTransaction,
    privateCancelBlockingTransaction,
    checkTransactionStatus,
    confirmTransactionStatus,
    checkBulkTransactionFees
];
exports.default = transactionController;
//# sourceMappingURL=transaction.controller.js.map