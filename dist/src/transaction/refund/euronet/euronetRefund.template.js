"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const lodash_1 = require("lodash");
const errors_1 = require("../../../common/errors");
const logger_1 = __importDefault(require("../../../logger"));
const transaction_constant_1 = require("../../transaction.constant");
const transaction_repository_1 = __importDefault(require("../../transaction.repository"));
const transaction_enum_1 = require("../../transaction.enum");
const AppError_1 = require("../../../errors/AppError");
const isApplicable = (payload) => lodash_1.includes([
    module_common_1.PaymentServiceTypeEnum.DOMESTIC_VOID_PAYMENT,
    module_common_1.PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT
], payload.paymentServiceType);
const getOriginalTransaction = (payload) => __awaiter(void 0, void 0, void 0, function* () {
    if (!(payload.notes &&
        payload.notes.length === transaction_constant_1.EURONET_REFUND_EXTERNAL_ID_LENGTH)) {
        const detail = 'notes is required for refund type req!';
        logger_1.default.error(`validateRefundRequest: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_REFUND_REQUEST);
    }
    const originalTransaction = yield transaction_repository_1.default.getIncomingNonRefunded(payload.notes, payload.beneficiaryAccountNo);
    if (!originalTransaction) {
        const detail = `Transaction not found for refund request: ${payload.notes}!`;
        logger_1.default.error(`validateRefundRequest: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_REFUND_REQUEST);
    }
    return originalTransaction;
});
const euronetRefundTemplate = {
    isApplicable,
    getOriginalTransaction
};
exports.default = euronetRefundTemplate;
//# sourceMappingURL=euronetRefund.template.js.map