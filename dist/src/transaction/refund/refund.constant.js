"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// To-Do(Sakshi-Dk): Will be fetched from ms-configuration, TEMP data
exports.nonRefundablePaymentServiceCode = [
    'CASHBACK',
    'VA_TOPUP_EXTERNAL',
    'THIRDPARTY_OTH_TO_JAGO',
    'THIRDPARTY_WINCOR_TO_JAGO',
    'VOID_NPG',
    'VOID_VISA',
    'IRIS_OTH_TO_JAGO',
    'THIRDPARTY_JAGO_TO_JAGO_CREDIT',
    'VOID_JAGOPAY',
    'JAGOATM_OTH_TO_JAGO',
    'REFUND_GENERAL'
];
//# sourceMappingURL=refund.constant.js.map