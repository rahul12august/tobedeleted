"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const AppError_1 = require("@dk/module-common/dist/error/AppError");
const errors_1 = require("../../common/errors");
const logger_1 = __importStar(require("../../logger"));
const transaction_repository_1 = __importDefault(require("../transaction.repository"));
const lodash_1 = require("lodash");
const isApplicable = (input) => !lodash_1.isUndefined(input.refund) &&
    !lodash_1.isUndefined(input.refund.originalTransactionId);
const getFee = (input) => __awaiter(void 0, void 0, void 0, function* () {
    let feeAmount = 0;
    if (input.refund && input.refund.originalTransactionId) {
        const originalTransaction = yield transaction_repository_1.default.getByKey(input.refund.originalTransactionId);
        if (!originalTransaction) {
            logger_1.default.error(`General Refund get Fee : Original transcation not found for transactionId : ${input.refund.originalTransactionId}`);
            throw new AppError_1.AppError(errors_1.ERROR_CODE.TRANSACTION_NOT_FOUND);
        }
        if (originalTransaction.fees) {
            feeAmount = lodash_1.sumBy(originalTransaction.fees, 'feeAmount');
        }
    }
    return feeAmount;
});
const refundFeeTemplate = {
    isApplicable,
    getFee
};
exports.default = logger_1.wrapLogs(refundFeeTemplate);
//# sourceMappingURL=refund.template.js.map