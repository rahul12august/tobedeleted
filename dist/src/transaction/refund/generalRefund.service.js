"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../../logger"));
const transaction_service_1 = __importDefault(require("../transaction.service"));
const transactionExecution_service_1 = __importDefault(require("../transactionExecution.service"));
const refund_validator_1 = __importDefault(require("./refund.validator"));
const errors_1 = require("../../common/errors");
const transaction_repository_1 = __importDefault(require("../transaction.repository"));
const transaction_helper_1 = __importDefault(require("../transaction.helper"));
const transaction_util_1 = require("../transaction.util");
const transactionExternal_service_1 = require("../transactionExternal.service");
const transaction_constant_1 = __importDefault(require("../transaction.constant"));
const lodash_1 = require("lodash");
const alto_utils_1 = __importDefault(require("../../alto/alto.utils"));
const AppError_1 = require("../../errors/AppError");
const transaction_enum_1 = require("../transaction.enum");
const { BENEFICIARY_BANK_CODE_COL, SOURCE_BANK_CODE_COL } = transaction_constant_1.default;
function buildTransferAppError(error, key) {
    let detail, actor;
    if (error instanceof AppError_1.TransferAppError) {
        detail = error.detail;
        actor = error.actor;
    }
    else {
        detail = 'Unknown issue with getting bank code info!';
        actor = transaction_enum_1.TransferFailureReasonActor.UNKNOWN;
    }
    return new AppError_1.TransferAppError(detail, actor, error.errorCode, [
        transaction_util_1.getErrorMessage(key)
    ]);
}
const populateTransactionInfo = (input) => __awaiter(void 0, void 0, void 0, function* () {
    const [sourceBankCodeInfo, beneficiaryBankCodeInfo] = yield Promise.all([
        transaction_helper_1.default.getBankCodeInfo(input.sourceBankCode).catch(error => {
            throw buildTransferAppError(error, SOURCE_BANK_CODE_COL);
        }),
        transaction_helper_1.default
            .getBankCodeInfo(input.beneficiaryBankCode)
            .catch(error => {
            throw buildTransferAppError(error, BENEFICIARY_BANK_CODE_COL);
        })
    ]);
    if (!sourceBankCodeInfo || !beneficiaryBankCodeInfo) {
        const detail = `SourceBankInfo bankCode: ${input.sourceBankCode} or beneficiaryBankCodeInfo: ${input.beneficiaryBankCode} not found!`;
        logger_1.default.error(`Error: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_BANK_CODE);
    }
    // Keep track input cif
    const executorCIF = input.sourceCIF;
    const destinationCIF = input.beneficiaryCIF;
    const [sourceAccountInfo, beneficiaryAccountInfo] = yield Promise.all([
        transactionExternal_service_1.populateIncomingAccount(sourceBankCodeInfo, input.sourceAccountNo, input.sourceAccountName, executorCIF),
        transactionExternal_service_1.populateIncomingAccount(beneficiaryBankCodeInfo, input.beneficiaryAccountNo, input.beneficiaryAccountName, destinationCIF)
    ]);
    logger_1.default.info(`General Refund : Source accountNo : ${input.sourceAccountNo} and Beneficiary accountNo: ${input.beneficiaryAccountNo}, info fetched`);
    // get back owner cif
    if (sourceAccountInfo) {
        input.sourceCIF = sourceAccountInfo.cif;
    }
    if (beneficiaryAccountInfo) {
        input.beneficiaryCIF = beneficiaryAccountInfo.cif;
    }
    return transaction_helper_1.default.populateTransactionInput(input, sourceAccountInfo, sourceBankCodeInfo, beneficiaryAccountInfo, beneficiaryBankCodeInfo, executorCIF, destinationCIF);
});
const createRefundTransaction = (param) => __awaiter(void 0, void 0, void 0, function* () {
    const amount = param.amountRefund
        ? param.amountRefund
        : param.originalTransaction.transactionAmount;
    const refundServiceType = transaction_util_1.getRefundServiceType(param.originalTransaction);
    const input = yield populateTransactionInfo({
        sourceAccountNo: param.originalTransaction.beneficiaryAccountNo,
        sourceBankCode: param.originalTransaction.beneficiaryBankCode,
        transactionAmount: amount,
        beneficiaryCIF: param.originalTransaction.sourceCIF,
        beneficiaryBankCode: param.originalTransaction.sourceBankCode,
        beneficiaryAccountNo: param.originalTransaction.sourceAccountNo,
        paymentServiceType: refundServiceType
    });
    if (param.refundFees) {
        input.refund = { originalTransactionId: param.transactionId };
    }
    if (param.refundIdentifier) {
        input.externalId = param.refundIdentifier;
    }
    if (param.refundAddInfo1) {
        input.additionalInformation1 = param.refundAddInfo1;
    }
    if (alto_utils_1.default.isQrisTransaction(param.originalTransaction)) {
        input.additionalInformation3 =
            param.originalTransaction.paymentInstructionId;
        input.additionalInformation2 =
            param.originalTransaction.beneficiaryAccountName;
        input.sourceAccountName = param.originalTransaction.beneficiaryAccountName;
    }
    input.additionalInformation4 = param.transactionId;
    yield refund_validator_1.default.validateGenaralRefundRequest(param.originalTransaction);
    logger_1.default.info(`executionTransaction : Create GENERAL_REFUND transaction with
      sourceBankCode: ${input.sourceBankCode},
      sourceAccountNo: ${input.sourceAccountNo},
      beneficiaryCIF: ${input.beneficiaryCIF}
      beneficiaryBankCode: ${input.beneficiaryBankCode},
      beneficiaryAccountNo: ${input.beneficiaryAccountNo},
      paymentServiceType: ${input.paymentServiceType}
      refund: ${JSON.stringify(input.refund)}
      originalTransactionId: ${input.additionalInformation4}`);
    const refundTxn = yield transactionExecution_service_1.default.executeTransaction(input);
    logger_1.default.info('General Refund: Updating Transaction with refund ');
    yield transaction_service_1.default.updateOriginalTransactionWithRefund(param.originalTransaction, refundTxn);
    yield transaction_service_1.default.updateRefundTransaction(param.originalTransaction, refundTxn);
    return refundTxn;
});
const refundTransferTransaction = (transactionId, refundFees) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`refundTransferTransation: General refund for transcationId: ${transactionId}
        refundFees : ${refundFees}`);
    const originalTransaction = yield transaction_repository_1.default.getByKey(transactionId);
    if (!originalTransaction) {
        const detail = `Original transaction not found for transactionId: ${transactionId}!`;
        logger_1.default.error(`refundTransferTransaction: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.TRANSACTION_NOT_FOUND);
    }
    const createRefundParam = {
        transactionId: transactionId,
        originalTransaction: originalTransaction,
        refundFees: refundFees
    };
    const refundTxn = yield createRefundTransaction(createRefundParam);
    return { id: refundTxn.id };
});
const refundTransaction = (refundInput) => __awaiter(void 0, void 0, void 0, function* () {
    if (!refundInput || lodash_1.isEmpty(refundInput)) {
        const detail = 'refundInput not provided!';
        logger_1.default.error(`refundTransaction: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_MANDATORY_FIELDS);
    }
    const { transactionId, fee } = refundInput;
    if (!transactionId) {
        let detail = 'transactionId not provided!';
        logger_1.default.error(`refundTransaction: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_MANDATORY_FIELDS);
    }
    const refundTrn = yield refundTransferTransaction(transactionId, fee);
    const transaction = yield transaction_repository_1.default.getByKey(refundTrn.id);
    if (!transaction) {
        const detail = `Transaction not found for id: ${refundTrn.id}!`;
        logger_1.default.error(`refundTransaction: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.TRANSACTION_NOT_FOUND);
    }
    return transaction;
});
const generalRefundService = logger_1.wrapLogs({
    refundTransferTransaction,
    refundTransaction,
    createRefundTransaction
});
exports.default = generalRefundService;
//# sourceMappingURL=generalRefund.service.js.map