"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../../logger"));
const errors_1 = require("../../common/errors");
const AppError_1 = require("../../errors/AppError");
const lodash_1 = require("lodash");
const transaction_enum_1 = require("../transaction.enum");
const configuration_repository_1 = __importDefault(require("../../configuration/configuration.repository"));
const module_common_1 = require("@dk/module-common");
const refundPaymentServiceTypeMap = new Map([
    [module_common_1.PaymentServiceTypeEnum.DOMESTIC_VOID_PAYMENT, ['POS_NPG']],
    [module_common_1.PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT, ['POS_VISA']],
    [module_common_1.PaymentServiceTypeEnum.JAGOPAY_REFUND, ['SIT03']],
    [
        module_common_1.PaymentServiceTypeEnum.VOID_RTGS,
        [
            'RTGS',
            'BRANCH_RTGS',
            'RTGS_FOR_BUSINESS',
            'RDN_WITHDRAW_RTGS',
            'RTGS_SHARIA'
        ]
    ],
    [
        module_common_1.PaymentServiceTypeEnum.VOID_SKN,
        ['SKN', 'BRANCH_SKN', 'SKN_FOR_BUSINESS', 'RDN_WITHDRAW_SKN', 'SKN_SHARIA']
    ],
    [
        module_common_1.PaymentServiceTypeEnum.VOID_BIFAST,
        ['BIFAST_OUTGOING', 'BIFAST_OUTGOING_SHARIA']
    ],
    [module_common_1.PaymentServiceTypeEnum.ATOME_VOID_PAYMENT, ['ATOME_TRANSFER_FOR_BUSINESS']]
]);
const logAndThrow = (message) => {
    logger_1.default.error(message);
    throw new AppError_1.TransferAppError(message, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_REFUND_REQUEST);
};
const isOriginalTransactionValid = (payload, originalTransaction) => {
    if (refundPaymentServiceTypeMap.has(payload.paymentServiceType)) {
        const validServiceCodes = refundPaymentServiceTypeMap.get(payload.paymentServiceType);
        if (originalTransaction.paymentServiceCode &&
            validServiceCodes &&
            validServiceCodes.includes(originalTransaction.paymentServiceCode)) {
            return true;
        }
        return false;
    }
    return false;
};
const validateRefundRequest = (payload, originalTransaction) => {
    if (!isOriginalTransactionValid(payload, originalTransaction)) {
        logAndThrow(`validateRefundRequest: paymentServiceType missmatch ${originalTransaction.paymentServiceType}`);
    }
    if (originalTransaction.status !== transaction_enum_1.TransactionStatus.SUCCEED) {
        logAndThrow(`validateRefundRequest: Invalid Transaction Status ${originalTransaction.status}.`);
    }
    if (payload.beneficiaryAccountNo !== originalTransaction.sourceAccountNo) {
        logAndThrow(`validateRefundRequest: Account mismatch.`);
    }
    // TODO: (Dikshit)commenting it for now can be enabled later once we will get nore calrity
    // if (
    //   payload.beneficiaryBankCode !== originalTransaction.sourceBankCode ||
    //   payload.sourceBankCode !== originalTransaction.beneficiaryBankCode
    // ) {
    //   logAndThrow(`validateRefundRequest: Bank Code mismatch.`);
    // }
    if (payload.transactionAmount > originalTransaction.transactionAmount) {
        logAndThrow(`validateRefundRequest: Transaction Amount greater than original transaction Amount.`);
    }
    if (originalTransaction.refund &&
        lodash_1.isNumber(originalTransaction.refund.remainingAmount) &&
        payload.transactionAmount > originalTransaction.refund.remainingAmount) {
        logAndThrow(`validateRefundRequest: Refund amount mismatch, originalTransaction: ${originalTransaction.id}`);
    }
    logger_1.default.info(`validateRefundRequest: Validated refunded transaction of originalTransaction:${originalTransaction.id} complete.`);
};
const validateGenaralRefundRequest = (originalTransaction) => __awaiter(void 0, void 0, void 0, function* () {
    if (originalTransaction.status !== transaction_enum_1.TransactionStatus.SUCCEED) {
        logAndThrow(`validateGenaralRefundRequest: Invalid Transaction Status ${originalTransaction.status}.`);
    }
    if (originalTransaction.refund &&
        originalTransaction.paymentServiceType != module_common_1.PaymentServiceTypeEnum.QRIS) {
        logAndThrow(`validateRefundRequest: Transaction with refund object`);
    }
    logger_1.default.info(`validateGenaralRefundRequest: Validated refunded transaction of originalTransaction:${originalTransaction.id} complete.`);
    const nonRefundablePaymentServiceCode = yield configuration_repository_1.default.getServiceRecommendations(false);
    if (originalTransaction.paymentServiceCode &&
        nonRefundablePaymentServiceCode.includes(originalTransaction.paymentServiceCode)) {
        logAndThrow(`validateGenaralRefundRequest: Invalid refund paymentServiceCode : ${originalTransaction.paymentServiceCode}`);
    }
});
const refundValidator = logger_1.wrapLogs({
    validateRefundRequest,
    validateGenaralRefundRequest
});
exports.default = refundValidator;
//# sourceMappingURL=refund.validator.js.map