"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const transaction_enum_1 = require("./transaction.enum");
const errors_1 = require("../common/errors");
const AppError_1 = require("../errors/AppError");
const logger_1 = __importDefault(require("../logger"));
const VISA_BANK_CODE = '360005';
/**
 * The OCT feature flag is true by default. To turn it off, specify OCT='false'.
 */
const getOCTToggle = () => {
    const oct = process.env.OCT;
    if (oct == 'false') {
        return false;
    }
    return true;
};
const throwOCTError = (transactionInput) => {
    const msg = `Original Credit Transaction is denied. externalId: ${transactionInput.externalId} . This is expected, for details see: DP3-514`;
    logger_1.default.info(msg);
    throw new AppError_1.TransferAppError(msg, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_REQUEST);
};
exports.denyOCT = (transactionInput) => {
    const toggle = getOCTToggle();
    if (!toggle) {
        logger_1.default.info(`OCT toggle state: ${toggle} `);
        return;
    }
    logger_1.default.info(`OCT check: externalId: ${transactionInput.externalId} tranType: ${transactionInput.tranType}`);
    if (transactionInput.tranType === 'OC') {
        throwOCTError(transactionInput);
    }
    if (transactionInput.tranType === undefined &&
        transactionInput.paymentServiceType ===
            module_common_1.PaymentServiceTypeEnum.THIRD_PARTY_TRANSFER &&
        transactionInput.sourceAccountNo === '000000' &&
        (transactionInput.sourceBankCode === VISA_BANK_CODE ||
            transactionInput.sourceBankCode === 'BC995')) {
        throwOCTError(transactionInput);
    }
    logger_1.default.info(`Not an OCT transaction, externalId: ${transactionInput.externalId}`);
};
//# sourceMappingURL=octVerifyer.js.map