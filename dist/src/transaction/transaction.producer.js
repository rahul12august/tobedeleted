"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_message_1 = require("@dk/module-message");
const kafkaProducer_1 = __importDefault(require("../common/kafkaProducer"));
const transaction_producer_enum_1 = require("./transaction.producer.enum");
const logger_1 = __importStar(require("../logger"));
const module_common_1 = require("@dk/module-common");
const errors_1 = require("../common/errors");
const currencyFormatter_1 = require("../common/currencyFormatter");
const dateUtils_1 = require("../common/dateUtils");
const kafka_util_1 = __importDefault(require("../common/kafka.util"));
const transactionSubmissionTemplate_service_1 = __importDefault(require("./transactionSubmissionTemplate.service"));
const customer_repository_1 = __importDefault(require("../customer/customer.repository"));
const alto_utils_1 = __importDefault(require("../alto/alto.utils"));
const transaction_constant_1 = require("./transaction.constant");
exports.reversalRetryTopic = 'transfer.retry-transaction-reversal';
exports.reversalRetryFailedTopic = 'transfer.retry-transaction-reversal-failed';
const shouldSendNotification = (transaction) => {
    const submissionTemplate = transactionSubmissionTemplate_service_1.default.getSubmissionTemplate(transaction);
    return submissionTemplate && submissionTemplate.shouldSendNotification
        ? submissionTemplate.shouldSendNotification()
        : true;
};
const sendFailedTransferNotification = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    if (!shouldSendNotification(transaction)) {
        logger_1.default.info(`sendTransactionFailedMessage : not sending failed message for
      paymentServiceCode: ${transaction.paymentServiceCode},
      externalId: ${transaction.externalId},
      status: ${transaction.status}`);
        return;
    }
    const notificationMessage = {
        notificationCode: transaction_producer_enum_1.NotificationCode.NOTIFICATION_FAILED_TRANSFER,
        cif: transaction.sourceCIF || '',
        payload: {
            amount: module_common_1.Formater.formatMoney(transaction.transactionAmount, {
                currency: transaction.sourceTransactionCurrency || ''
            }),
            beneficiaryName: transaction.beneficiaryAccountName || '',
            accountNumber: transaction.beneficiaryAccountNo || ''
        }
    };
    if (transaction.paymentServiceType === module_common_1.PaymentServiceTypeEnum.QRIS) {
        const customerData = yield customer_repository_1.default.getCustomerInfoById(transaction.sourceCustomerId || '');
        notificationMessage.notificationCode = transaction_producer_enum_1.NotificationCode.NOTIF_FAILED_QRIS;
        notificationMessage.payload.customerName = ((_a = customerData) === null || _a === void 0 ? void 0 : _a.fullName) || '';
    }
    yield kafkaProducer_1.default.sendSerializedValue({
        topic: module_message_1.NotificationTopicConstant.NOTIFICATION_CREATED,
        messages: [
            {
                value: notificationMessage,
                headers: kafka_util_1.default.getHeaders()
            }
        ]
    });
});
const toTransactionMessage = (customerId, transaction) => {
    var _a;
    return {
        transactionId: transaction.id,
        sourceCIF: transaction.sourceCIF,
        sourceAccountNo: transaction.sourceAccountNo,
        sourceAccountType: transaction.sourceAccountType,
        beneficiaryCIF: transaction.beneficiaryCIF,
        beneficiaryAccountNo: transaction.beneficiaryAccountNo,
        beneficiaryAccountType: transaction.beneficiaryAccountType,
        referenceId: transaction.referenceId,
        externalId: transaction.externalId,
        status: transaction.status,
        customerId,
        updatedAt: (_a = transaction.updatedAt) === null || _a === void 0 ? void 0 : _a.toISOString(),
        categoryCode: transaction.categoryCode,
        transactionAmount: transaction.transactionAmount,
        paymentRequestID: transaction.paymentRequestID,
        additionalInformation1: transaction.additionalInformation1,
        additionalInformation2: transaction.additionalInformation2,
        executorCIF: transaction.executorCIF,
        beneficiaryName: transaction.beneficiaryAccountName,
        paymentServiceCode: transaction.paymentServiceCode,
        notes: transaction.note
    };
};
const sendTransactionSucceededMessage = (customerId, transaction) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionMessage = toTransactionMessage(customerId, transaction);
    logger_1.default.info(`sendTransactionSucceededMessage : sending succeeded message for ${customerId}
  paymentServiceCode: ${transactionMessage.paymentServiceCode},
  externalId: ${transactionMessage.externalId},
  status: ${transactionMessage.status},
  transactionId: ${transactionMessage.transactionId}`);
    yield kafkaProducer_1.default.sendSerializedValue({
        topic: module_message_1.TransactionTopicConstant.TRANSFER_TRANSACTION_SUCCEEDED,
        messages: [
            {
                value: transactionMessage,
                headers: kafka_util_1.default.getHeaders()
            }
        ]
    });
});
const sendTransactionFailedMessage = (customerId, transaction) => __awaiter(void 0, void 0, void 0, function* () {
    if (!shouldSendNotification(transaction)) {
        logger_1.default.info(`sendTransactionFailedMessage : not sending failed message for ${customerId}
        paymentServiceCode: ${transaction.paymentServiceCode},
        externalId: ${transaction.externalId},
        status: ${transaction.status}`);
        return;
    }
    const transactionMessage = toTransactionMessage(customerId, transaction);
    transactionMessage.failureReson = transaction.failureReason;
    logger_1.default.info(`sendTransactionFailedMessage : sending failed message for ${customerId}
  paymentServiceCode: ${transactionMessage.paymentServiceCode},
  externalId: ${transactionMessage.externalId},
  status: ${transactionMessage.status},
  transactionId: ${transactionMessage.transactionId}`);
    yield kafkaProducer_1.default.sendSerializedValue({
        topic: module_message_1.TransactionTopicConstant.TRANSFER_TRANSACTION_FAILED,
        messages: [
            {
                value: transactionMessage,
                headers: kafka_util_1.default.getHeaders()
            }
        ]
    });
});
const sendTransactionRetryReversalCommand = (message) => __awaiter(void 0, void 0, void 0, function* () {
    yield kafkaProducer_1.default.sendSerializedValue({
        topic: exports.reversalRetryTopic,
        messages: [
            {
                value: message,
                headers: kafka_util_1.default.getHeaders()
            }
        ]
    });
});
const sendTransactionReversalFailedEvent = (message) => __awaiter(void 0, void 0, void 0, function* () {
    yield kafkaProducer_1.default.sendSerializedValue({
        topic: exports.reversalRetryFailedTopic,
        messages: [
            {
                value: message,
                headers: kafka_util_1.default.getHeaders()
            }
        ]
    });
});
const sendPendingBillPaymentNotification = (message) => __awaiter(void 0, void 0, void 0, function* () {
    const notification = {
        notificationCode: transaction_producer_enum_1.NotificationCode.NOTIFICATION_PENDING_BILLPAYMENT,
        customerId: message.billDetails.customerId,
        payload: {
            amount: currencyFormatter_1.toIndonesianRupiah(message.transactionAmount),
            billKey: message.billDetails.primaryBillKey.value,
            billerName: message.billDetails.billerName,
            transactionId: message.transactionId,
            transactionTime: dateUtils_1.to24HourFormat(message.transactionTime)
        }
    };
    logger_1.default.info(`Bill Payment : Sending notification for pending status with notification code : ${notification.notificationCode} 
    transactionId : ${notification.payload.transactionId} `);
    yield kafkaProducer_1.default.sendSerializedValue({
        topic: module_message_1.NotificationTopicConstant.NOTIFICATION_CREATED,
        messages: [
            {
                value: notification,
                headers: kafka_util_1.default.getHeaders()
            }
        ]
    });
});
const sendPendingBlockedAmountNotification = (transaction, notificationCode) => __awaiter(void 0, void 0, void 0, function* () {
    if (!transaction.sourceCustomerId && !transaction.sourceCIF) {
        logger_1.default.error('Blocked Amount Notification , customer not found with transaction payload');
        throw new Error(errors_1.ERROR_CODE.CUSTOMER_NOT_FOUND);
    }
    const notification = {
        notificationCode: notificationCode,
        customerId: transaction.sourceCustomerId,
        cif: transaction.sourceCIF,
        payload: {
            amount: currencyFormatter_1.toIndonesianRupiah(transaction.transactionAmount),
            beneficiaryName: transaction.beneficiaryAccountName,
            transactionId: transaction.id,
            transactionTime: dateUtils_1.to24HourFormat(transaction.createdAt || transaction.updatedAt)
        }
    };
    logger_1.default.info(`Blocked Amount : Sending notification for pending status with notification code ${notification.notificationCode} 
     transactionId : ${transaction.id} customerId : ${transaction.sourceCustomerId} cif : ${transaction.sourceCIF}`);
    yield kafkaProducer_1.default.sendSerializedValue({
        topic: module_message_1.NotificationTopicConstant.NOTIFICATION_CREATED,
        messages: [
            {
                value: notification,
                headers: kafka_util_1.default.getHeaders()
            }
        ]
    });
});
const sendTransactionMessage = (topic, message, executionType, paymentInstructionCode) => __awaiter(void 0, void 0, void 0, function* () {
    yield kafkaProducer_1.default.sendSerializedValue({
        topic: topic,
        messages: [
            {
                headers: Object.assign({ [module_common_1.Constant.EXECUTION_TYPE]: executionType, [module_common_1.Constant.PI_KEY_HEADER]: paymentInstructionCode }, kafka_util_1.default.getHeaders()),
                value: message
            }
        ]
    });
});
const sendQrisTransactionNotification = (transaction, notificationCode) => __awaiter(void 0, void 0, void 0, function* () {
    if (!transaction.sourceCustomerId && !transaction.sourceCIF) {
        logger_1.default.error('sendQrisTransactionNotification: Cannot send notification: Customer is not found in transaction');
        throw new Error(errors_1.ERROR_CODE.CUSTOMER_NOT_FOUND);
    }
    const notification = {
        notificationCode: notificationCode,
        customerId: transaction.sourceCustomerId,
        cif: transaction.sourceCIF,
        payload: alto_utils_1.default.buildQrisNotificationMessage(transaction)
    };
    logger_1.default.info(`sendQrisTransactionNotification: Sending QRIS transaction notification.
      Notification Code: ${notification.notificationCode} 
      Transaction ID : ${transaction.id}
      Customer ID : ${transaction.sourceCustomerId}
      Source CIF : ${transaction.sourceCIF}`);
    yield kafkaProducer_1.default.sendSerializedValue({
        topic: module_message_1.NotificationTopicConstant.NOTIFICATION_CREATED,
        messages: [
            {
                value: notification,
                headers: kafka_util_1.default.getHeaders()
            }
        ]
    });
});
const sendFailedDebitCardTransactionAmount = (transaction, notificationCode) => __awaiter(void 0, void 0, void 0, function* () {
    if (!transaction.sourceCustomerId && !transaction.sourceCIF) {
        logger_1.default.error('sendFailedDebitCardTransactionAmount: Cannot send notification: Customer is not found in transaction');
    }
    const notification = {
        notificationCode: notificationCode,
        customerId: transaction.sourceCustomerId,
        cif: transaction.sourceCIF,
        payload: {
            amount: currencyFormatter_1.toIndonesianRupiah(transaction.transactionAmount),
            customerName: transaction.sourceAccountName,
            merchantName: transaction.beneficiaryAccountName
        }
    };
    logger_1.default.info(`sendFailedDebitCardTransactionAmount: Sending sendFailedDebitCardTransactionAmount transaction notification.
      Notification Code: ${notification.notificationCode}
      Customer ID : ${transaction.sourceCustomerId}
      customerName: ${transaction.sourceAccountName}
      merchantName: ${transaction.beneficiaryAccountName}`);
    yield kafkaProducer_1.default.sendSerializedValue({
        topic: module_message_1.NotificationTopicConstant.NOTIFICATION_CREATED,
        messages: [
            {
                value: notification,
                headers: kafka_util_1.default.getHeaders()
            }
        ]
    });
});
const sendFailedCashWithdrawalDailyLimit = (message, notificationCode) => __awaiter(void 0, void 0, void 0, function* () {
    const notification = {
        notificationCode: notificationCode,
        customerId: message.customerId,
        payload: {
            pocketName: message.pocketName,
            amount: `${transaction_constant_1.IDR_CURRENCY_PREFIX}${currencyFormatter_1.toIndonesianRupiah(message.amount)}`
        }
    };
    logger_1.default.info(`sendFailedCashWithdrawalDailyLimit: Sending sendFailedCashWithdrawalDailyLimit transaction notification.
      Notification Code: ${notification.notificationCode}
      Customer ID : ${message.customerId}
      pocketName: ${message.pocketName}
    `);
    yield kafkaProducer_1.default.sendSerializedValue({
        topic: module_message_1.NotificationTopicConstant.NOTIFICATION_CREATED,
        messages: [
            {
                value: notification,
                headers: kafka_util_1.default.getHeaders()
            }
        ]
    });
});
const sendFailedMoneyTransferViaAtmDailyLimit = (message, notificationCode) => __awaiter(void 0, void 0, void 0, function* () {
    const notification = {
        notificationCode: notificationCode,
        customerId: message.customerId,
        payload: {
            accountNumber: message.beneficiaryAccountNo,
            beneficiaryName: message.beneficiaryName,
            amount: `${transaction_constant_1.IDR_CURRENCY_PREFIX}${currencyFormatter_1.toIndonesianRupiah(message.amount)}`
        }
    };
    yield kafkaProducer_1.default.sendSerializedValue({
        topic: module_message_1.NotificationTopicConstant.NOTIFICATION_CREATED,
        messages: [
            {
                value: notification,
                headers: kafka_util_1.default.getHeaders()
            }
        ]
    });
});
exports.default = logger_1.wrapLogs({
    sendFailedTransferNotification,
    sendTransactionSucceededMessage,
    sendTransactionFailedMessage,
    sendTransactionRetryReversalCommand,
    sendTransactionReversalFailedEvent,
    sendTransactionMessage,
    sendPendingBillPaymentNotification,
    sendPendingBlockedAmountNotification,
    sendQrisTransactionNotification,
    sendFailedDebitCardTransactionAmount,
    sendFailedCashWithdrawalDailyLimit,
    sendFailedMoneyTransferViaAtmDailyLimit
});
//# sourceMappingURL=transaction.producer.js.map