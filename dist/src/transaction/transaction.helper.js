"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const lodash_1 = __importStar(require("lodash"));
const moment_timezone_1 = __importDefault(require("moment-timezone"));
const account_enum_1 = require("../account/account.enum");
const account_repository_1 = __importDefault(require("../account/account.repository"));
const account_service_1 = __importDefault(require("../account/account.service"));
const award_service_1 = __importDefault(require("../award/award.service"));
const common_util_1 = require("../common/common.util");
const errors_1 = require("../common/errors");
const featureFlag_1 = require("../common/featureFlag");
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const transaction_mapper_1 = __importDefault(require("./transaction.mapper"));
const AppError_1 = require("../errors/AppError");
const fee_service_1 = __importDefault(require("../fee/fee.service"));
const logger_1 = __importStar(require("../logger"));
const transaction_constant_1 = __importStar(require("./transaction.constant"));
const transaction_enum_1 = require("./transaction.enum");
const transaction_repository_1 = __importDefault(require("./transaction.repository"));
const transaction_util_1 = require("./transaction.util");
const bifast_enum_1 = require("../bifast/bifast.enum");
const customerEntitlement_manager_factory_1 = require("../customerEntitlementManager/customerEntitlement.manager.factory");
const constant_1 = require("../common/constant");
const fee_helper_1 = __importDefault(require("../fee/fee.helper"));
const { AMOUNT_IS_LIMITED } = transaction_constant_1.default;
const getStatusesToBeUpdated = (status, transactionInput) => {
    const statusToUpdate = {
        status,
        updatedAt: new Date()
    };
    return !transactionInput.journey || lodash_1.isEmpty(transactionInput.journey)
        ? [statusToUpdate]
        : [...transactionInput.journey, statusToUpdate];
};
const mapAuthenticationRequired = (recommendationServices, transactionAmount, sourceAccountNo) => __awaiter(void 0, void 0, void 0, function* () {
    if (!sourceAccountNo) {
        return recommendationServices;
    }
    return yield Promise.all(recommendationServices.map((item) => __awaiter(void 0, void 0, void 0, function* () {
        let authenticationType = module_common_1.AuthenticationType.NONE;
        if (item.transactionAuthenticationChecking) {
            const isAuthenticationAmount = yield account_service_1.default.authenticateTransactionAmount(sourceAccountNo, transactionAmount);
            if (isAuthenticationAmount) {
                authenticationType = module_common_1.AuthenticationType.PASSWORD;
            }
            else {
                authenticationType = module_common_1.AuthenticationType.PIN;
            }
        }
        return Object.assign(Object.assign({}, item), { authenticationType });
    })));
});
const getUsageCounter = (transactionCode, usageCounters) => __awaiter(void 0, void 0, void 0, function* () {
    const { limitGroupCode, awardGroupCounter } = transactionCode;
    const isAwardV2Enabled = featureFlag_1.isFeatureEnabled(constant_1.FEATURE_FLAG.ENABLE_AWARD_V2);
    if (isAwardV2Enabled && awardGroupCounter) {
        return usageCounters.filter(c => c.limitGroupCode === awardGroupCounter)[0];
    }
    const monthlyTransaction = usageCounters.find(c => c.limitGroupCode === 'RTOL_BIFAST_FEE');
    if (limitGroupCode) {
        return usageCounters.filter(c => {
            if (c.limitGroupCode === limitGroupCode) {
                if (transaction_constant_1.RTOL_BIFAST_LIMIT_GROUPS.includes(c.limitGroupCode)) {
                    c.monthlyAccumulationTransaction =
                        monthlyTransaction &&
                            monthlyTransaction.monthlyAccumulationTransaction
                            ? monthlyTransaction.monthlyAccumulationTransaction
                            : c.monthlyAccumulationTransaction;
                }
                return c;
            }
            return;
        })[0];
    }
    return;
});
const getThresholdCounter = (customerId, transactionCode) => __awaiter(void 0, void 0, void 0, function* () {
    const { awardGroupCounter } = transactionCode;
    if (awardGroupCounter) {
        return yield award_service_1.default.getThresholdCounter(customerId, awardGroupCounter);
    }
    return;
});
const getUsageAndThresholdCounters = (input, transactionCode, usageCounters, isAwardV2Enabled) => __awaiter(void 0, void 0, void 0, function* () {
    let [usageCounter, thresholdCounter] = !input.refund && input.sourceCustomerId
        ? yield Promise.all([
            getUsageCounter(transactionCode.transactionCodeInfo, usageCounters),
            !isAwardV2Enabled
                ? undefined
                : getThresholdCounter(input.sourceCustomerId, transactionCode.transactionCodeInfo)
        ])
        : [undefined, undefined];
    return { usageCounter, thresholdCounter };
});
const getFeeAmountInput = (input, recommendationService, usageCounters) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionCodes = input.refund ||
        input.paymentServiceType === module_common_1.PaymentServiceTypeEnum.OFFER ||
        input.paymentServiceType === module_common_1.PaymentServiceTypeEnum.PAYROLL ||
        input.paymentServiceType === module_common_1.PaymentServiceTypeEnum.CASHBACK ||
        input.paymentServiceType === module_common_1.PaymentServiceTypeEnum.BONUS_INTEREST
        ? recommendationService.creditTransactionCode
        : recommendationService.debitTransactionCode;
    const custEntitlementMgr = yield customerEntitlement_manager_factory_1.CustomerEntitlementManagerFactory.getCustomerEntitlementManagerByFlag();
    //checking based on flag
    const resultEntitlement = yield custEntitlementMgr.preProcessCustomerEntitlementBasedOnTransactionCodes({
        transactionCodes: transactionCodes,
        customerId: input.sourceCustomerId,
        transactionId: input.transactionId,
        paymentServiceType: input.paymentServiceType
    });
    const feeAmountInputs = [];
    if (recommendationService.inquiryFeeRule) {
        const feeAmountInput = {
            feeRuleCode: recommendationService.inquiryFeeRule,
            transactionAmount: input.transactionAmount,
            feeAmount: input.feeAmount,
            monthlyNoTransaction: undefined,
            interchange: undefined,
            targetBankCode: input.beneficiaryBankCode,
            externalId: input.externalId,
            beneficiaryBankCodeChannel: input.beneficiaryBankCodeChannel,
            refund: input.refund,
            customerId: input.sourceCustomerId
        };
        feeAmountInputs.push(feeAmountInput);
    }
    if (!transactionCodes) {
        return feeAmountInputs;
    }
    //generate missing usage counter [processed by entitlement]
    usageCounters = yield custEntitlementMgr.generateMissingUsageCounterBasedOnTransactionCodes({
        transactionCodes: transactionCodes,
        usageCounters,
        entitlementResultResponse: resultEntitlement
    });
    for (const transactionCode of transactionCodes) {
        const isAwardV2Enabled = featureFlag_1.isFeatureEnabled(constant_1.FEATURE_FLAG.ENABLE_AWARD_V2);
        const feeRuleCode = transactionCode.transactionCodeInfo.feeRules;
        if (!feeRuleCode) {
            continue;
        }
        let { usageCounter, thresholdCounter } = yield getUsageAndThresholdCounters(input, transactionCode, usageCounters, isAwardV2Enabled);
        // based on the entitlement flag, custEntitlementMgr will map it accordingly
        // the fallback logic will use usageCounter as is
        usageCounters = custEntitlementMgr.mapEntitlementUsageCounterByLimitGroupCode(usageCounters, resultEntitlement);
        const feeAmountInput = {
            transactionAmount: input.transactionAmount,
            feeAmount: input.feeAmount,
            refund: input.refund,
            thresholdCounter: thresholdCounter,
            targetBankCode: input.beneficiaryBankCode,
            externalId: input.externalId,
            feeRuleCode: feeRuleCode,
            monthlyNoTransaction: usageCounter && usageCounter.monthlyAccumulationTransaction,
            interchange: transactionCode.interchange,
            beneficiaryBankCodeChannel: input.beneficiaryBankCodeChannel,
            customerId: input.sourceCustomerId,
            awardGroupCounter: transactionCode.transactionCodeInfo.awardGroupCounter,
            limitGroupCode: transactionCode.transactionCodeInfo.limitGroupCode,
            usageCounters: usageCounters
        };
        feeAmountInputs.push(feeAmountInput);
    }
    return feeAmountInputs;
});
const getFeeData = (input, recommendationService, usageCounters) => __awaiter(void 0, void 0, void 0, function* () {
    const feeAmountInput = yield getFeeAmountInput(input, recommendationService, usageCounters);
    const feeDataQueryResult = yield Promise.all(feeAmountInput.map(input => fee_service_1.default.getFeeAmount(input))); // IFeeAmount[][]
    return lodash_1.default.flatten(feeDataQueryResult);
});
const mapFeeData = (recommendationServices, input, usageCounters) => __awaiter(void 0, void 0, void 0, function* () {
    if (!input.sourceCustomerId &&
        !input.refund &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.OFFER &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.PAYROLL &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.CASHBACK &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.BONUS_INTEREST) {
        return recommendationServices;
    }
    return yield Promise.all(recommendationServices.map((recommendationService) => __awaiter(void 0, void 0, void 0, function* () {
        const feeData = yield getFeeData(input, recommendationService, usageCounters);
        return Object.assign(Object.assign({}, recommendationService), { feeData });
    })));
});
const getBankCode = (bankCode) => __awaiter(void 0, void 0, void 0, function* () {
    const bankCodeInfo = yield configuration_repository_1.default.getBankCodeMapping(bankCode);
    if (!bankCodeInfo) {
        const detail = `Invalid bank code ${bankCode}!`;
        logger_1.default.error(`getBankCode: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.INVALID_BANK_CODE);
    }
    return bankCodeInfo;
});
const getAccountInfo = (accountNo, cif, bankCode, accountName, transactionAmount, sourceAccountNo, isInquiryApplicable = true) => __awaiter(void 0, void 0, void 0, function* () {
    let accountInfo = {
        accountName: accountName,
        bankCode: bankCode ? bankCode.bankCodeId : undefined,
        accountNumber: accountNo,
        role: account_enum_1.AccountRole.NONE
    };
    if (bankCode && isInquiryApplicable) {
        if (accountNo) {
            const encodedAccountNo = encodeURIComponent(accountNo);
            let inquiryInfo;
            logger_1.default.info('AccountInquiry: Making inquiry for account');
            if (bankCode.isInternal) {
                logger_1.default.info(`AccountInquiry: Making inquiry for an account (with mambu skipped), account number : ${accountNo} and BankCode :${bankCode.bankCodeId}`);
                inquiryInfo = yield account_repository_1.default.getAccountInfo(encodedAccountNo, cif);
            }
            else {
                let bankCodeId = bankCode.bankCodeId;
                logger_1.default.info(`AccountInquiry: Making inquiry for an account with account number : ${accountNo} and BankCode :${bankCodeId}`);
                inquiryInfo = yield account_repository_1.default.inquiryAccountInfo(bankCodeId, encodedAccountNo, transactionAmount, sourceAccountNo
                    ? encodeURIComponent(sourceAccountNo)
                    : sourceAccountNo, cif);
            }
            if (bankCode.isInternal && !('cif' in inquiryInfo)) {
                const detail = 'CIF is missing after account information is fetched!';
                logger_1.default.error(detail);
                throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.INVALID_ACCOUNT);
            }
            accountInfo = Object.assign(Object.assign({}, accountInfo), inquiryInfo);
        }
    }
    return accountInfo;
});
const getBeneficiaryAccount = (accountNo, cif, bankCode, accountName, transactionAmount, sourceAccountNo, isInquiryApplicable = true) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield getAccountInfo(accountNo, cif, bankCode, accountName, transactionAmount, sourceAccountNo, isInquiryApplicable);
    }
    catch (err) {
        const appError = err && err.error && err.error.error;
        if (appError &&
            (appError.code === transaction_constant_1.SWITCHING_ACCOUNT_NOT_FOUND ||
                appError.code === transaction_constant_1.ACCOUNT_NUMBER_NOT_FOUND)) {
            return common_util_1.mapTransferAppError(appError, errors_1.ERROR_CODE.BENEFICIARY_ACCOUNT_INQUIRY_FAILED, err.status >= 500);
        }
        return common_util_1.mapTransferAppError(err, errors_1.ERROR_CODE.BENEFICIARY_ACCOUNT_INQUIRY_FAILED, true);
    }
});
const getSourceAccount = (accountNo, cif, bankCode, accountName, transactionAmount, isInquiryApplicable = true) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let transactionAccount = yield getAccountInfo(accountNo, cif, bankCode, accountName, transactionAmount, accountNo, isInquiryApplicable);
        if (transactionAccount.accountType !== 'RDN' &&
            transactionAccount.accountType !== 'RDS' &&
            transactionAccount.accountType !== 'MBA') {
            if (bankCode && bankCode.isInternal && !cif) {
                logger_1.default.error(`CIF is missing before fetching account information.`);
                throw new AppError_1.TransferAppError(`CIF is missing before fetching account information!`, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_MANDATORY_FIELDS);
            }
            if (transactionAccount.role === account_enum_1.AccountRole.NONE && isInquiryApplicable) {
                const detail = `Invalid account type role: ${transactionAccount.role}!`;
                logger_1.default.error(`getSourceAccount: ${detail}`);
                throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_ACCOUNT);
            }
        }
        return transactionAccount;
    }
    catch (err) {
        const appError = err && err.error && err.error.error;
        if (appError && appError.code === transaction_constant_1.ACCOUNT_NUMBER_NOT_FOUND) {
            return common_util_1.mapTransferAppError(appError, errors_1.ERROR_CODE.SOURCE_ACCOUNT_INQUIRY_FAILED, err.status >= 500);
        }
        return common_util_1.mapTransferAppError(err, errors_1.ERROR_CODE.SOURCE_ACCOUNT_INQUIRY_FAILED, true);
    }
});
const isTransactionIncomingWithdrawal = (sourceBankCodeInfo, sourceAccountInfo) => {
    var _a, _b;
    return !lodash_1.isEmpty((_a = sourceAccountInfo) === null || _a === void 0 ? void 0 : _a.cif) &&
        ((_b = sourceBankCodeInfo) === null || _b === void 0 ? void 0 : _b.rtolCode) == "542" /* RTOL_CODE */;
};
const addPrefixWithAccountNumber = (bankType, accountNumber, prefix) => {
    if (prefix && accountNumber && bankType === module_common_1.InstitutionTypeEnum.WALLET) {
        if (accountNumber.startsWith(transaction_enum_1.PhoneCode.PLUS62)) {
            accountNumber = accountNumber.replace(transaction_enum_1.PhoneCode.PLUS62, '0');
        }
        accountNumber = prefix + accountNumber;
    }
    return accountNumber;
};
const populateTransactionInput = (input, sourceAccountInfo, sourceBankCodeInfo, beneficiaryAccountInfo, beneficiaryBankCodeInfo, executorCIF, destinationCIF) => {
    var _a, _b, _c, _d, _e;
    let transaction = input;
    if (sourceAccountInfo) {
        transaction = Object.assign(Object.assign({}, transaction), { sourceTransactionCurrency: sourceAccountInfo.currency, sourceAccountType: sourceAccountInfo.accountType, sourceAccountName: sourceAccountInfo.accountName, sourceAccountNo: sourceAccountInfo.accountNumber, sourceBankCode: sourceAccountInfo.bankCode, sourceCustomerId: sourceAccountInfo.customerId, sourceAccountRole: sourceAccountInfo.role, moverRemainingDailyUsage: sourceAccountInfo.moverRemainingDailyUsage });
    }
    if (sourceBankCodeInfo) {
        transaction.sourceBankCodeChannel = sourceBankCodeInfo.channel;
        transaction.sourceRtolCode = sourceBankCodeInfo.rtolCode;
        transaction.creditPriority = sourceBankCodeInfo.firstPriority;
        transaction.sourceCIF = transaction.sourceCIF || ((_a = sourceAccountInfo) === null || _a === void 0 ? void 0 : _a.cif); // get cif from populated incoming
        transaction.sourceRemittanceCode = sourceBankCodeInfo.remittanceCode;
        if (!executorCIF) {
            executorCIF = isTransactionIncomingWithdrawal(sourceBankCodeInfo, sourceAccountInfo)
                ? (_b = sourceAccountInfo) === null || _b === void 0 ? void 0 : _b.cif : undefined;
        }
    }
    if (beneficiaryAccountInfo) {
        transaction = Object.assign(Object.assign({}, transaction), { beneficiaryAccountNo: beneficiaryBankCodeInfo &&
                addPrefixWithAccountNumber(beneficiaryBankCodeInfo.type, beneficiaryAccountInfo.accountNumber, beneficiaryBankCodeInfo.prefix), beneficiaryAccountType: ((_c = beneficiaryAccountInfo) === null || _c === void 0 ? void 0 : _c.beneficiaryAccountType) ||
                beneficiaryAccountInfo.accountType, beneficiaryAccountName: beneficiaryAccountInfo.accountName, beneficiaryCIF: transaction.beneficiaryCIF || ((_d = beneficiaryAccountInfo) === null || _d === void 0 ? void 0 : _d.cif), beneficiaryAccountRole: beneficiaryAccountInfo.role, beneficiaryCustomerId: beneficiaryAccountInfo.customerId });
    }
    if (beneficiaryBankCodeInfo) {
        transaction = Object.assign(Object.assign({}, transaction), { beneficiaryBankName: beneficiaryBankCodeInfo.name, beneficiaryBankCodeChannel: beneficiaryBankCodeInfo.channel, debitPriority: beneficiaryBankCodeInfo.firstPriority, beneficiaryRtolCode: beneficiaryBankCodeInfo.rtolCode, beneficiaryRemittanceCode: beneficiaryBankCodeInfo.remittanceCode, billerCode: beneficiaryBankCodeInfo.billerCode, beneficiaryIrisCode: beneficiaryBankCodeInfo.irisCode, beneficiarySupportedChannels: beneficiaryBankCodeInfo.supportedChannels, preferedBankChannel: (_e = beneficiaryAccountInfo) === null || _e === void 0 ? void 0 : _e.preferredChannel });
    }
    transaction.ownerCustomerId = transaction.sourceCustomerId;
    if (lodash_1.isEmpty(transaction.ownerCustomerId)) {
        transaction.ownerCustomerId = transaction.beneficiaryCustomerId;
    }
    return Object.assign(Object.assign({}, transaction), { sourceBankCodeInfo,
        beneficiaryBankCodeInfo,
        sourceAccountInfo,
        beneficiaryAccountInfo,
        executorCIF,
        destinationCIF });
};
const buildNewTransactionModel = (transaction, recommendedService) => {
    var _a, _b, _c, _d;
    const { debitTransactionCode, creditTransactionCode, blocking, feeData } = recommendedService;
    let debitTransactionCodeMapping = (_a = debitTransactionCode) === null || _a === void 0 ? void 0 : _a.map(tc => (Object.assign(Object.assign({}, tc.transactionCodeInfo), { interchange: tc.interchange })));
    let creditTransactionCodeMapping = (_b = creditTransactionCode) === null || _b === void 0 ? void 0 : _b.map(tc => (Object.assign(Object.assign({}, tc.transactionCodeInfo), { interchange: tc.interchange })));
    let feeMapping = feeData;
    const _referenceId = transaction.referenceId || transaction_util_1.generateUniqueId();
    const _externalId = transaction.externalId || transaction_util_1.generateUniqueId();
    let newTransaction = Object.assign(Object.assign({}, transaction), { referenceId: _referenceId, externalId: _externalId, paymentServiceCode: recommendedService.paymentServiceCode, beneficiaryRealBankCode: recommendedService.beneficiaryBankCode, status: transaction_enum_1.TransactionStatus.PENDING, userSelectedCategoryCode: transaction.categoryCode, sourceAccountName: (_d = (_c = transaction.sourceAccountInfo) === null || _c === void 0 ? void 0 : _c.accountName, (_d !== null && _d !== void 0 ? _d : transaction.sourceAccountName)), coreBankingTransactionIds: [], coreBankingTransactions: [], executionType: blocking
            ? transaction_enum_1.ExecutionTypeEnum.BLOCKING
            : transaction_enum_1.ExecutionTypeEnum.NONE_BLOCKING, requireThirdPartyOutgoingId: recommendedService.requireThirdPartyOutgoingId, debitTransactionCodeMapping,
        creditTransactionCodeMapping,
        feeMapping, paymentInstructionId: transaction.paymentInstructionId });
    newTransaction = Object.assign(Object.assign(Object.assign({}, newTransaction), transaction_util_1.extractTransactionInfoByInterchange(newTransaction, transaction.interchange)), { journey: getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.RECOMMENDATION_FETCHED, newTransaction) });
    return newTransaction;
};
const validateTransactionLimitAmount = (amount, paymentServiceMappings, interchange) => {
    let isValid = true;
    paymentServiceMappings.forEach(item => {
        if (!item.debitTransactionCode && !item.creditTransactionCode) {
            return;
        }
        (item.debitTransactionCode || []).forEach(debitTc => {
            const { minAmountPerTx, maxAmountPerTx } = debitTc.transactionCodeInfo;
            if (amount < minAmountPerTx ||
                (amount > maxAmountPerTx &&
                    !debitTc.transactionCode.endsWith(transaction_constant_1.REVERSAL_TC_SUFFIX))) {
                logger_1.default.error(`Failed Amount Limit check from debittransactionCode: ${debitTc.transactionCode}`);
                isValid = false;
            }
        });
        (item.creditTransactionCode || []).forEach(creditTc => {
            const { minAmountPerTx, maxAmountPerTx } = creditTc.transactionCodeInfo;
            if (amount < minAmountPerTx ||
                (amount > maxAmountPerTx && interchange == creditTc.interchange)) {
                logger_1.default.error(`Failed Amount Limit check from credittransactionCode: ${creditTc.transactionCode}`);
                isValid = false;
            }
        });
    });
    if (!isValid) {
        logger_1.default.error('validateTransactionLimitAmount: transaction amount is limited');
        throw new AppError_1.TransferAppError('Transaction amount is limited!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_AMOUNT, [
            {
                message: AMOUNT_IS_LIMITED,
                code: errors_1.ERROR_CODE.INVALID_LIMIT_TRANSACTION_AMOUNT,
                key: ''
            }
        ]);
    }
};
const getPendingBlockingTransaction = (transactionId, cif, amount) => __awaiter(void 0, void 0, void 0, function* () {
    const transaction = yield transaction_repository_1.default.getByKey(transactionId);
    if (!transaction) {
        logger_1.default.error(`getPendingBlockingTransaction: Matching txn was not found for transactionId: ${transactionId}`);
        throw new AppError_1.TransferAppError(`Matching txn was not found for transactionId: ${transactionId}!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.TRANSACTION_NOT_FOUND);
    }
    if (!transaction.sourceCIF || !transaction.sourceAccountNo) {
        logger_1.default.error('getPendingBlockingTransaction: invalid account due to sourceCIF or sourceAccountNo not found');
        throw new AppError_1.TransferAppError(`Invalid account due to sourceCIF or sourceAccountNo not found!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.INVALID_ACCOUNT);
    }
    if (!(transaction.status === transaction_enum_1.TransactionStatus.SUBMITTED ||
        transaction.status === transaction_enum_1.TransactionStatus.PENDING)) {
        logger_1.default.error('getPendingBlockingTransaction: transaction status not pending or submitted');
        throw new AppError_1.TransferAppError(`Transaction status must be pending or submitted but received: ${transaction.status}!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.INVALID_TRANSACTION_STATUS);
    }
    if (!transaction.blockingId || !transaction.cardId) {
        logger_1.default.error('getPendingBlockingTransaction: blocking amount not found');
        throw new AppError_1.TransferAppError('Blocking amount not found, missing blocking ID or card ID!', transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.BLOCKING_ID_NOT_FOUND);
    }
    // TODO: Review the logic later
    if (transaction.sourceCIF != cif) {
        logger_1.default.error('getPendingBlockingTransaction: invalid account as sourceCIF does not match');
        throw new AppError_1.TransferAppError('Invalid account as sourceCIF does not match!', transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.INVALID_ACCOUNT);
    }
    if (amount && transaction.transactionAmount !== amount) {
        logger_1.default.error('getPendingBlockingTransaction: invalid amount as transaction amount does not match');
        throw new AppError_1.TransferAppError('Invalid amount as transaction amount does not match!', transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.INVALID_AMOUNT);
    }
    return Object.assign(Object.assign({}, transaction), { sourceAccountNo: transaction.sourceAccountNo, sourceCIF: transaction.sourceCIF, blockingId: transaction.blockingId, cardId: transaction.cardId, idempotencyKey: transaction.idempotencyKey });
});
const getDeclinedBlockingTransaction = (transactionId) => __awaiter(void 0, void 0, void 0, function* () {
    const transaction = yield transaction_repository_1.default.getByKey(transactionId);
    if (!transaction) {
        logger_1.default.error(`getDeclinedBlockingTransaction: Matching txn was not found for transactionId: ${transactionId}`);
        throw new AppError_1.TransferAppError(`Matching txn was not found for transactionId: ${transactionId}!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.TRANSACTION_NOT_FOUND);
    }
    if (!transaction.sourceCIF || !transaction.sourceAccountNo) {
        logger_1.default.error('getDeclinedBlockingTransaction: Invalid account due to sourceCIF or sourceAccountNo not found');
        throw new AppError_1.TransferAppError(`Invalid account due to sourceCIF or sourceAccountNo not found!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.INVALID_ACCOUNT);
    }
    if (transaction.status !== transaction_enum_1.TransactionStatus.DECLINED) {
        logger_1.default.error('getDeclinedBlockingTransaction: Transaction status is not declined');
        throw new AppError_1.TransferAppError(`Transaction status is not declined!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.INVALID_TRANSACTION_STATUS);
    }
    if (!transaction.blockingId || !transaction.cardId) {
        logger_1.default.error('getDeclinedBlockingTransaction: Blocking id or card id not found');
        throw new AppError_1.TransferAppError(`Blocking id or card id not found!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.BLOCKING_ID_NOT_FOUND);
    }
    return Object.assign(Object.assign({}, transaction), { sourceAccountNo: transaction.sourceAccountNo, sourceCIF: transaction.sourceCIF, blockingId: transaction.blockingId, cardId: transaction.cardId, idempotencyKey: transaction.idempotencyKey });
});
const getPaymentServiceMappings = (data, configs, limitGroups, usageCounters, executionDate = new Date()) => __awaiter(void 0, void 0, void 0, function* () {
    return transaction_mapper_1.default.getPaymentServiceMappings(data, configs, limitGroups, usageCounters, executionDate);
});
const updateTransactionStatus = (transactionId, status) => __awaiter(void 0, void 0, void 0, function* () {
    return transaction_repository_1.default.update(transactionId, {
        status: status
    });
});
const updateFailedTransactionStatus = (transactionId, status, error) => __awaiter(void 0, void 0, void 0, function* () {
    return transaction_repository_1.default.update(transactionId, {
        status: status,
        failureReason: {
            type: errors_1.ERROR_CODE[error.errorCode],
            actor: error.actor,
            detail: error.detail
        }
    });
});
const getBankCodeMapping = (bankCode) => __awaiter(void 0, void 0, void 0, function* () {
    const bankInfo = yield configuration_repository_1.default.getBankCodeMapping(bankCode);
    if (!bankInfo) {
        logger_1.default.error(`Bank Info not found for bankCode: ${bankCode}`);
        throw new AppError_1.TransferAppError(`Bank Info not found for bankCode: ${bankCode}!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.INVALID_REGISTER_PARAMETER);
    }
    return bankInfo;
});
// TODO duplicated logic of getting bank code with getBankCodeMapping and getBankCode
// refactor later
const getBankCodeInfo = (bankCode) => __awaiter(void 0, void 0, void 0, function* () {
    return bankCode ? yield getBankCodeMapping(bankCode) : undefined;
});
const getTransactionSearchStartDate = () => moment_timezone_1.default()
    .subtract(3, 'month')
    .toDate();
const validateTransactionListRequest = (input) => {
    if (!input.transactionId && !input.customerId) {
        logger_1.default.error(`Either Customer Id or Transaction Id .`);
        throw new AppError_1.TransferAppError('Customer ID or Transaction ID is empty!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.MISSING_CUSTOMER_OR_TRANSACTION_ID);
    }
    if ((!input.startDate && input.endDate) ||
        (input.startDate && !input.endDate)) {
        logger_1.default.error(`Start and End Date missing.`);
        throw new AppError_1.TransferAppError('Start and end date missing!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.MISSING_START_AND_END_DATE);
    }
    return true;
};
const validateExternalIdAndPaymentType = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionList = yield transaction_repository_1.default.getAllByExternalId(transactionModel.externalId);
    const transaction = (transactionList || []).find(transaction => {
        return (transaction.paymentServiceType === transactionModel.paymentServiceType);
    });
    if (transaction) {
        logger_1.default.error(`Error in validateExternalIdAndPaymentType : Duplicate transaction found with same exteralId and paymentServiceType
      with Journey : ${JSON.stringify(transactionModel.journey)}`);
        throw new AppError_1.TransferAppError(`Error in validateExternalIdAndPaymentType : Duplicate transaction found with same exteralId = ${transactionModel.externalId} and paymentServiceType = ${transactionModel.paymentServiceType}`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.UNIQUE_FIELD);
    }
});
const buildOutgoingTransactionModel = (transaction) => (Object.assign(Object.assign({}, transaction), { referenceId: transaction_util_1.generateUniqueId(), externalId: transaction.externalId || transaction_util_1.generateUniqueId(), paymentServiceCode: transaction.paymentServiceCode || '', paymentServiceType: transaction.paymentServiceType, beneficiaryRealBankCode: transaction.beneficiaryBankCode, status: transaction_enum_1.TransactionStatus.PENDING, requireThirdPartyOutgoingId: false, journey: [
        {
            status: transaction_enum_1.TransferJourneyStatusEnum.REQUEST_RECEIVED,
            updatedAt: new Date()
        }
    ] }));
const getBifastBeneficiaryAdditionalInfo = (input) => {
    const biFastProxyType = input.additionalInformation3;
    let beneficiaryAccountNo = input.beneficiaryAccountNo;
    let bifastBankCode;
    if (Object.values(bifast_enum_1.BeneficiaryProxyType).includes(biFastProxyType) &&
        biFastProxyType !== bifast_enum_1.BeneficiaryProxyType.ACCOUNT_NUMBER) {
        beneficiaryAccountNo = input.additionalInformation4;
        bifastBankCode = {
            bankCodeId: 'BC000',
            name: 'BIFAST',
            companyName: 'Bank Indonesia',
            isBersamaMember: false,
            isAltoMember: false,
            channel: module_common_1.BankChannelEnum.BIFAST,
            type: module_common_1.InstitutionTypeEnum.BANK,
            isInternal: false
        };
    }
    return [beneficiaryAccountNo, bifastBankCode];
};
const validateProxyTypeAndBankCode = (input, beneficiaryAccountInfo) => {
    const biFastProxyType = input.additionalInformation3;
    if (Object.values(bifast_enum_1.BeneficiaryProxyType).includes(biFastProxyType) &&
        input.additionalInformation3 !== beneficiaryAccountInfo.beneficiaryProxyType) {
        logger_1.default.error(`validateProxyTypeAndBankCode: Proxy type does not match with input`);
        throw new AppError_1.TransferAppError(`Proxy type does not match with input!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.BENEFICIARY_ACCOUNT_INQUIRY_FAILED);
    }
    // To-do : Uncomment when Komi removes hardcoded bankCode from account inquiry
    // if (input.beneficiaryBankCode !== beneficiaryAccountInfo.bankCode) {
    //   logger.error(
    //     `validateProxyTypeAndBankCode: Bank code does not match with input`
    //   );
    //   throw new AppError(ERROR_CODE.BENEFICIARY_ACCOUNT_INQUIRY_FAILED);
    // }
};
const getCustomerIdByModel = (model) => {
    let customerId;
    if (model.sourceBankCodeChannel === module_common_1.BankChannelEnum.INTERNAL ||
        model.sourceCustomerId) {
        customerId = model.sourceCustomerId;
    }
    else if (model.sourceBankCodeChannel === module_common_1.BankChannelEnum.EXTERNAL) {
        customerId = model.beneficiaryCustomerId;
    }
    return customerId;
};
const getAdjustedPaymentRail = (rail = module_common_1.Rail.INTERNAL, interchange) => {
    if (rail === module_common_1.Rail.EURONET && interchange && !lodash_1.isEmpty(interchange)) {
        // For EURONET update the payment rail with an interchange value
        return rail.toUpperCase() + '_' + interchange.toUpperCase();
    }
    return rail.toUpperCase();
};
const setPaymentRailToTransactionData = (transactionModel, rail = module_common_1.Rail.INTERNAL) => {
    const paymentRail = getAdjustedPaymentRail(rail, transactionModel.interchange);
    transactionModel.processingInfo = Object.assign(Object.assign({}, transactionModel.processingInfo), { paymentRail: paymentRail });
};
const getFeeAmountQueries = (feeAmountInputs) => __awaiter(void 0, void 0, void 0, function* () {
    return Promise.all(feeAmountInputs.map((feeAmountInput) => __awaiter(void 0, void 0, void 0, function* () {
        const feeMappings = yield fee_helper_1.default.getFeeMappings(feeAmountInput);
        return {
            input: feeAmountInput,
            feeMappings: feeMappings
        };
    })));
});
exports.default = logger_1.wrapLogs({
    getFeeAmountInput,
    mapAuthenticationRequired,
    mapFeeData,
    getAccountInfo,
    buildNewTransactionModel,
    validateTransactionLimitAmount,
    getBankCode,
    getPendingBlockingTransaction,
    getDeclinedBlockingTransaction,
    getPaymentServiceMappings,
    updateTransactionStatus,
    updateFailedTransactionStatus,
    getBankCodeMapping,
    populateTransactionInput,
    getBankCodeInfo,
    getSourceAccount,
    getTransactionSearchStartDate,
    validateTransactionListRequest,
    getStatusesToBeUpdated,
    getBeneficiaryAccount,
    validateExternalIdAndPaymentType,
    buildOutgoingTransactionModel,
    getBifastBeneficiaryAdditionalInfo,
    validateProxyTypeAndBankCode,
    getCustomerIdByModel,
    setPaymentRailToTransactionData,
    getAdjustedPaymentRail,
    getUsageCounter,
    getFeeAmountQueries
});
//# sourceMappingURL=transaction.helper.js.map