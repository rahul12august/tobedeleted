"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const transaction_util_1 = require("./transaction.util");
const externalIdGenerator_util_1 = require("../common/externalIdGenerator.util");
const transaction_constant_1 = __importStar(require("./transaction.constant"));
const transaction_helper_1 = __importDefault(require("./transaction.helper"));
const transaction_repository_1 = __importDefault(require("./transaction.repository"));
const transaction_producer_1 = __importDefault(require("./transaction.producer"));
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const award_repository_1 = __importDefault(require("../award/award.repository"));
const transactionSubmissionTemplate_service_1 = __importDefault(require("./transactionSubmissionTemplate.service"));
const transactionCoreBanking_helper_1 = __importDefault(require("./transactionCoreBanking.helper"));
const transactionReversal_helper_1 = __importDefault(require("./transactionReversal.helper"));
const transactionBlocking_helper_1 = __importDefault(require("./transactionBlocking.helper"));
const module_common_1 = require("@dk/module-common");
const transactionRecommend_service_1 = __importDefault(require("./transactionRecommend.service"));
const transaction_enum_1 = require("./transaction.enum");
const transactionCoreBankingCreditSubmission_service_1 = __importDefault(require("./transactionCoreBankingCreditSubmission.service"));
const feeExecution_templates_1 = require("./feeExecution/feeExecution.templates");
const contextHandler_1 = require("../common/contextHandler");
const lodash_1 = __importStar(require("lodash"));
const common_util_1 = require("../common/common.util");
const alto_utils_1 = __importDefault(require("../alto/alto.utils"));
const transactionStatus_template_1 = __importDefault(require("./transactionStatus.template"));
const alto_type_1 = require("../alto/alto.type");
const alto_service_1 = __importDefault(require("../alto/alto.service"));
const transaction_producer_enum_1 = require("./transaction.producer.enum");
const monitoringEvent_producer_1 = __importDefault(require("../monitoringEvent/monitoringEvent.producer"));
const module_message_1 = require("@dk/module-message");
const entitlementFlag_1 = __importDefault(require("../common/entitlementFlag"));
const transaction_service_1 = __importDefault(require("./transaction.service"));
const AppError_2 = require("@dk/module-common/dist/error/AppError");
const transactionUsage_service_1 = __importDefault(require("../transactionUsage/transactionUsage.service"));
const abuseDetectionService_1 = require("../abuseDetection/abuseDetectionService");
const { CATEGORY_CODE_COL } = transaction_constant_1.default;
const isRdnAccount = (transaction) => transaction.sourceAccountType === module_common_1.AccountType.RDN ||
    transaction.beneficiaryAccountType === module_common_1.AccountType.RDN ||
    transaction.sourceAccountType === module_common_1.AccountType.RDS ||
    transaction.beneficiaryAccountType === module_common_1.AccountType.RDS;
const shouldIgnoreRdnExceptions = (transaction, error) => isRdnAccount(transaction) &&
    (transaction_constant_1.IGNORE_RDN_EXCEPTIONS.includes(error.code) ||
        !lodash_1.isEmpty(transaction_constant_1.IGNORE_RDN_EXCEPTIONS.find(ex => error && error.message && error.message.includes(ex))));
exports.defaultExecutionHooks = {
    onFailedTransaction: undefined,
    onRecommendedServiceCode: undefined,
    onSucceedTransaction: undefined,
    onNewCreatedTransaction: undefined
};
const validateTransactionCategoryCode = (categoryCode) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionCategory = yield configuration_repository_1.default.getTransactionCategory(categoryCode);
    if (!transactionCategory) {
        logger_1.default.error(`validateTransactionCategoryCode: category code is not available.`);
        throw new AppError_1.TransferAppError('Category code is not available!', transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.INVALID_REGISTER_PARAMETER, [transaction_util_1.getErrorMessage(CATEGORY_CODE_COL)]);
    }
});
const updateUsageCounters = (transactionAmount, limitGroupCodes, customerId) => __awaiter(void 0, void 0, void 0, function* () {
    const data = limitGroupCodes.map(limitGroupCode => ({
        limitGroupCode,
        adjustmentLimitAmount: transactionAmount
    }));
    try {
        yield award_repository_1.default.updateUsageCounters(customerId, data);
    }
    catch (error) {
        logger_1.default.error(`error when update usage counters`, error);
    }
});
const buildFailureReason = (error) => {
    let detail, actor = undefined, type = undefined;
    if (error instanceof AppError_1.TransferAppError) {
        detail = error.detail;
        actor = error.actor;
        type = errors_1.ERROR_CODE[error.errorCode];
    }
    else if (error instanceof AppError_2.AppError) {
        type = errors_1.ERROR_CODE[error.errorCode];
        detail = error.message;
    }
    else {
        logger_1.default.warn(`TransferAppError not received: ${error}`);
        detail = error.message;
    }
    return {
        detail: detail,
        actor: actor,
        type: type
    };
};
const onFailedTransaction = (model, executionHooks, error) => __awaiter(void 0, void 0, void 0, function* () {
    let failedTransaction;
    let failureReason = buildFailureReason(error);
    if (model.paymentServiceType === module_common_1.PaymentServiceTypeEnum.QRIS) {
        let transactionModel = error instanceof AppError_1.RetryableTransferAppError && error.transaction
            ? error.transaction
            : model;
        // all the details already persisted before
        // just need to update status and journey
        failedTransaction = yield transaction_repository_1.default.update(model.id, {
            status: transaction_enum_1.TransactionStatus.DECLINED,
            journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.TRANSFER_FAILURE, transactionModel),
            failureReason: failureReason
        });
        model = failedTransaction;
    }
    else {
        failedTransaction = yield transaction_repository_1.default.update(model.id, Object.assign(Object.assign({}, model), { status: transaction_enum_1.TransactionStatus.DECLINED, journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.TRANSFER_FAILURE, model), failureReason: failureReason }));
    }
    const customerId = transaction_helper_1.default.getCustomerIdByModel(model);
    if (failedTransaction && customerId) {
        logger_1.default.info(`onFailedTransaction: Sending Failed Transaction notification ${failedTransaction.paymentServiceCode} ${customerId}`);
        if (transaction_util_1.isBIFast(failedTransaction.paymentServiceCode)) {
            transaction_producer_1.default.sendFailedTransferNotification(failedTransaction);
        }
        transaction_producer_1.default.sendTransactionFailedMessage(customerId, failedTransaction);
    }
    monitoringEvent_producer_1.default.sendMonitoringEventMessage(module_message_1.MonitoringEventType.PROCESSING_FINISHED, failedTransaction);
    if (executionHooks.onFailedTransaction) {
        yield executionHooks.onFailedTransaction(model);
    }
    if (failedTransaction && failedTransaction.journey) {
        logger_1.default.info(`onFailedTransaction: Journey for Failed Transaction ${JSON.stringify(failedTransaction.journey)}`);
    }
});
const validateCategoryAndfetchRecommendation = (transactionInput) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // validate category if it's supplied
        if (transactionInput.categoryCode) {
            yield validateTransactionCategoryCode(transactionInput.categoryCode);
        }
        logger_1.default.info(`Fetching recommendation service for service type ${transactionInput.paymentServiceType}, externalId : ${transactionInput.externalId},
    executor cif: ${transactionInput.executorCIF}, sourceAccountNumber: ${transactionInput.sourceAccountNo}
    sourceBankCode: ${transactionInput.sourceBankCode}, beneficiaryBankCode: ${transactionInput.beneficiaryBankCode},
    beneficiaryAccountNumber: ${transactionInput.beneficiaryAccountNo}, beneficiaryBankChannel: ${transactionInput.beneficiaryBankCodeChannel}`);
        transactionInput = Object.assign(Object.assign({}, transactionInput), { journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.FETCHING_RECOMMENDATION, transactionInput) });
        const recommendedService = yield transactionRecommend_service_1.default.getRecommendService(transactionInput);
        logger_1.default.info(`Recommendation service fetched : ${JSON.stringify(recommendedService)}`);
        return recommendedService;
    }
    catch (error) {
        logger_1.default.error(`initTransaction: Error while making a recommendationService call ${error.message}`);
        yield transaction_service_1.default.updateFailedTransaction(transactionInput, error);
        throw error;
    }
});
const declineTransactionIfSuspectedAbuse = (paymentServiceCode, transactionInput) => __awaiter(void 0, void 0, void 0, function* () {
    if (transactionInput.ownerCustomerId &&
        transactionInput.beneficiaryAccountNo &&
        (yield abuseDetectionService_1.isSuspectedAbuse(paymentServiceCode, transactionInput.ownerCustomerId, transactionInput.beneficiaryAccountNo, transactionInput.transactionId))) {
        const detail = `Suspected abuse for
          paymentServiceCode: ${paymentServiceCode}
          customerId: ${transactionInput.ownerCustomerId}
          beneficiaryAccountNo: ${transactionInput.beneficiaryAccountNo}.`;
        logger_1.default.error(`declineRequestIfSuspectedAbuse: ${detail}`);
        let suspectedAbuseError = new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.SUSPECTED_ABUSE);
        yield transaction_service_1.default.updateFailedTransaction(transactionInput, suspectedAbuseError);
        suspectedAbuseError.errorCode = errors_1.ERROR_CODE.INVALID_REQUEST;
        throw suspectedAbuseError;
    }
});
const initTransaction = (transactionInput, initialTemplate, executionHooks = exports.defaultExecutionHooks) => __awaiter(void 0, void 0, void 0, function* () {
    const recommendedService = yield validateCategoryAndfetchRecommendation(transactionInput);
    // declining the transaction request if suspected abuse
    yield declineTransactionIfSuspectedAbuse(recommendedService.paymentServiceCode, transactionInput);
    // based on the final recommended service; validate on pocket level rule
    yield transactionUsage_service_1.default.pocketLevelLimitValidation({
        transactionAmount: transactionInput.transactionAmount,
        transactionInterchange: transactionInput.interchange,
        customerId: transactionInput.sourceCustomerId,
        accountNo: transactionInput.sourceAccountNo,
        beneficiaryAccountNo: transactionInput.beneficiaryAccountNo,
        beneficiaryAccountName: transactionInput.beneficiaryAccountName,
        paymentServiceType: transactionInput.paymentServiceType,
        recommendedService
    });
    if (executionHooks.onRecommendedServiceCode) {
        yield executionHooks.onRecommendedServiceCode(recommendedService);
    }
    if (yield entitlementFlag_1.default.isEnabled()) {
        const infoTracker = yield contextHandler_1.getTransactionInfoTracker();
        if (infoTracker) {
            transactionInput.entitlementInfo = infoTracker.map(info => {
                return {
                    entitlementCodeRefId: info.entitlementCodeRefId,
                    counterCode: info.counterCode,
                    customerId: info.customerId
                };
            });
        }
    }
    const transactionModel = transaction_helper_1.default.buildNewTransactionModel(transactionInput, recommendedService);
    if (transactionModel.requireThirdPartyOutgoingId &&
        transactionModel.debitTransactionCode) {
        transactionModel.thirdPartyOutgoingId = yield externalIdGenerator_util_1.generateOutgoingId(transactionModel, transactionModel.debitTransactionCode);
    }
    let model;
    if (transactionInput.transactionId) {
        const cleanTransactionModel = lodash_1.pickBy(transactionModel, lodash_1.identity);
        model = yield transaction_repository_1.default.update(transactionInput.transactionId, cleanTransactionModel);
    }
    else {
        // TODO: @dikshitthakral will remove once update will be stable
        yield transaction_helper_1.default.validateExternalIdAndPaymentType(transactionModel);
        model = yield transaction_repository_1.default.create(transactionModel);
    }
    if (!model) {
        logger_1.default.error(`submitTransaction: failed to update transaction info for txnId ${transactionInput.transactionId}`);
        throw new AppError_1.TransferAppError(`Failed to update transaction info for txnId ${transactionInput.transactionId}`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION);
    }
    logger_1.default.info(`Created a transaction model with id: ${model.id} externalId : ${model.externalId} paymentServiceCode: ${model.paymentServiceCode}`);
    if (executionHooks.onNewCreatedTransaction) {
        yield executionHooks.onNewCreatedTransaction(model);
    }
    try {
        model.additionalPayload = transactionModel.additionalPayload;
        model = yield initialTemplate.submitTransaction(model);
    }
    catch (err) {
        logger_1.default.error(`error when initial transaction with journey : ${JSON.stringify(model.journey)} error :${err && err.message}`);
        yield onFailedTransaction(model, executionHooks, err);
        throw new AppError_1.RetryableTransferAppError(`Error: ${err && err.message} when initial transaction!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, err.errorCode || errors_1.ERROR_CODE.TRANSACTION_FAILED, err.retry || false);
    }
    return model;
});
const defaultTrue = (value) => value === undefined || value == null || value;
const isMambuError = (error) => error &&
    error.errors &&
    error.errors.some((errorDetail) => lodash_1.default.isEqual(errorDetail.message, errors_1.ERROR_CODE.MAMBU_TRANSACTION_AUTHORIZATION_HOLD_EXPIRED));
const settleTransaction = (transactionModel, confirmTemplate, executionHooks = exports.defaultExecutionHooks) => __awaiter(void 0, void 0, void 0, function* () {
    let model = transactionModel;
    try {
        model = yield confirmTemplate.submitTransaction(model);
    }
    catch (error) {
        logger_1.default.error(`settleTransaction: error when confirm transaction for transaction id : ${model.id} , journey : ${JSON.stringify(model.journey)}, status: ${error &&
            error.status}, message: ${error && error.message}`);
        if (isMambuError(error) ||
            defaultTrue(confirmTemplate.declineOnSubmissionFailure)) {
            yield onFailedTransaction(model, executionHooks, error);
        }
        throw new AppError_1.RetryableTransferAppError(`Error when confirm transaction for transaction ID: ${model.id}, message: ${error && error.message}!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.TRANSACTION_FAILED, true);
    }
    const updatedTransaction = yield transaction_repository_1.default.update(model.id, Object.assign(Object.assign({}, model), { status: transaction_enum_1.TransactionStatus.SUCCEED, journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.TRANSFER_SUCCESS, model) }));
    if (!updatedTransaction) {
        logger_1.default.error(`settleTransaction: failed to update transaction status for transaction id: ${model.id}.`);
        throw new AppError_1.TransferAppError(`Failed to update transaction status for transaction id: ${model.id}.`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION_STATUS);
    }
    const customerId = transaction_helper_1.default.getCustomerIdByModel(updatedTransaction);
    if (customerId) {
        logger_1.default.info(`settleTransaction: Sending Succeeded Transaction Notification ${customerId}`);
        transaction_producer_1.default.sendTransactionSucceededMessage(customerId, updatedTransaction);
    }
    monitoringEvent_producer_1.default.sendMonitoringEventMessage(module_message_1.MonitoringEventType.PROCESSING_FINISHED, updatedTransaction);
    if (executionHooks.onSucceedTransaction) {
        yield executionHooks.onSucceedTransaction(updatedTransaction);
    }
    const { debitLimitGroupCode, transactionAmount, sourceCustomerId, debitAwardGroupCounter } = updatedTransaction;
    const counters = [];
    if (debitLimitGroupCode)
        counters.push(debitLimitGroupCode);
    if (debitAwardGroupCounter)
        counters.push(debitAwardGroupCounter);
    if (counters.includes('L002') ||
        counters.includes('L013') ||
        counters.includes('L019')) {
        counters.push(transaction_constant_1.RTOL_BIFAST_FEE);
    }
    if (!lodash_1.isEmpty(counters) && sourceCustomerId) {
        yield updateUsageCounters(transactionAmount, counters, sourceCustomerId);
    }
    contextHandler_1.updateRequestIdInRedis(updatedTransaction.id);
    logger_1.default.info(`transactionCompleted: settleTransaction
      customerId : ${updatedTransaction.sourceCustomerId},
      externalId : ${updatedTransaction.externalId},
      transactionId: ${updatedTransaction.id},
      beneficiaryAccount: ${updatedTransaction.beneficiaryAccountNo},
      beneficiaryBankCode: ${updatedTransaction.beneficiaryBankCode},
      sourceAccount: ${updatedTransaction.sourceAccountNo},
      journey: ${JSON.stringify(updatedTransaction.journey)}`);
    return updatedTransaction;
});
const getCreditTemplate = (transactionModel) => {
    var _a, _b, _c;
    let creditTemplate = undefined;
    if (transactionModel.beneficiaryAccountNo) {
        if (transactionModel.requireThirdPartyOutgoingId) {
            // this is outgoing transaction
            creditTemplate = transactionSubmissionTemplate_service_1.default.getSubmissionTemplate(transactionModel);
            if (!creditTemplate) {
                logger_1.default.error('getCreditTemplate: credit transaction submission template not found');
                throw new AppError_1.TransferAppError('Credit transaction submission template not found.', transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.MISSING_CREDIT_TEMPLATE);
            }
        }
        else if (transactionCoreBankingCreditSubmission_service_1.default.isEligible(transactionModel)) {
            creditTemplate = transactionCoreBankingCreditSubmission_service_1.default;
        }
        else if (transactionModel.creditTransactionCode &&
            !transactionModel.creditTransactionCode.endsWith(transaction_constant_1.REVERSAL_TC_SUFFIX)) {
            // expect valid template if exist creditTransactionCode
            logger_1.default.error('getCreditTemplate: credit transaction submission template not found if exist creditTransactionCode');
            throw new AppError_1.TransferAppError('Credit transaction submission template not found when creditTransactionCode exists.', transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.MISSING_CREDIT_TEMPLATE);
        }
    }
    transaction_helper_1.default.setPaymentRailToTransactionData(transactionModel, (_c = (_a = creditTemplate) === null || _a === void 0 ? void 0 : (_b = _a).getRail) === null || _c === void 0 ? void 0 : _c.call(_b));
    return creditTemplate;
};
exports.debitSource = (model) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    logger_1.default.info('debitSource: debit transaction with source account.');
    try {
        if (model.debitTransactionCode &&
            !model.debitTransactionCode.endsWith(transaction_constant_1.REVERSAL_TC_SUFFIX) &&
            model.debitTransactionChannel &&
            model.sourceAccountNo) {
            const debitTransactionId = yield transactionCoreBanking_helper_1.default.submitDebitTransaction(model.sourceAccountNo, model, model.debitTransactionCode, model.debitTransactionChannel);
            if (model.coreBankingTransactionIds) {
                model.coreBankingTransactionIds.push(debitTransactionId);
            }
            (_a = model.coreBankingTransactions) === null || _a === void 0 ? void 0 : _a.push({
                id: debitTransactionId,
                type: transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_DEBITED
            });
            model.journey = transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_DEBITED, model);
        }
    }
    catch (err) {
        logger_1.default.error(`debitSource: Error while debiting money, code : ${err.code} , message: ${err.message}`);
        if (shouldIgnoreRdnExceptions(model, err)) {
            logger_1.default.info(`Ignoring Exception for RDN Transaction Id: ${model.id}`);
            return;
        }
        throw new AppError_1.RetryableTransferAppError(`Error while debiting money, code: ${err.code}, message: ${err.message}!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.COREBANKING_DEBIT_FAILED, AppError_1.isRetryableError(err.status));
    }
});
const submitFees = (model) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`submitFees: submit fees with transactionId: ${model.id} and externalId : ${model.externalId} `);
    try {
        const feeExecutionTemplate = feeExecution_templates_1.getApplicable(model);
        if (feeExecutionTemplate) {
            yield feeExecutionTemplate.execute(model);
            model.journey = transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.FEE_PROCESSED, model);
        }
    }
    catch (err) {
        logger_1.default.error(`submitFees: Error while submiting fees, code : ${err.code} , message: ${err.message}`);
        if (shouldIgnoreRdnExceptions(model, err)) {
            logger_1.default.info(`Ignoring Exception for RDN Transaction Id: ${model.id}`);
            return;
        }
        throw new AppError_1.RetryableTransferAppError(`Ignoring exception for RDN transaction ID: ${model.id}!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.COREBANKING_FEES_FAILED, AppError_1.isRetryableError(err.status));
    }
});
const creditBeneficiary = (model, creditTemplate) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info('creditBeneficiary: credit transaction using submission template.');
    try {
        if (creditTemplate) {
            const categoryCode = model.paymentServiceType === module_common_1.PaymentServiceTypeEnum.OFFER ||
                model.paymentServiceType === module_common_1.PaymentServiceTypeEnum.PAYROLL ||
                model.paymentServiceType === module_common_1.PaymentServiceTypeEnum.CASHBACK ||
                model.paymentServiceType === module_common_1.PaymentServiceTypeEnum.CONVERSION ||
                model.paymentServiceType ===
                    module_common_1.PaymentServiceTypeEnum.MAIN_POCKET_TO_GL_TRANSFER ||
                model.paymentServiceType === module_common_1.PaymentServiceTypeEnum.BONUS_INTEREST
                ? model.categoryCode
                : undefined; // force enrichment get default category code for credit
            model = yield creditTemplate.submitTransaction(Object.assign(Object.assign({}, model), { categoryCode: categoryCode }));
            if (model && model.journey) {
                model.journey.push({
                    status: transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_CREDITED,
                    updatedAt: new Date()
                });
            }
        }
    }
    catch (err) {
        logger_1.default.error(`creditBeneficiary: Error while crediting money, code : ${err.code} , message: ${err.message}`);
        if (shouldIgnoreRdnExceptions(model, err)) {
            logger_1.default.info(`Ignoring Exception for RDN Transaction Id: ${model.id}`);
            return;
        }
        throw new AppError_1.RetryableTransferAppError(`Error while crediting money, code: ${err.code}, message: ${err.message}!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.COREBANKING_CREDIT_FAILED, AppError_1.isRetryableError(err.status));
    }
});
const submitDisbursement = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    logger_1.default.info('submitDisbursement: disburse loan transaction.');
    try {
        if (transactionModel.sourceAccountNo &&
            transactionModel.debitTransactionChannel) {
            const disburseTransactionId = yield transactionCoreBanking_helper_1.default.submitDisbursementTransaction(transactionModel.sourceAccountNo, transactionModel, transactionModel.debitTransactionChannel);
            (_b = transactionModel.coreBankingTransactions) === null || _b === void 0 ? void 0 : _b.push({
                id: disburseTransactionId,
                type: transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_DISBURSED
            });
            if (transactionModel.coreBankingTransactionIds) {
                transactionModel.coreBankingTransactionIds.push(disburseTransactionId);
            }
            transactionModel.journey = transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_DISBURSED, transactionModel);
        }
    }
    catch (error) {
        logger_1.default.error(`submitDisbursement: Error while disburse money, code: ${error.code}, message: ${error.message}`);
        throw new AppError_1.RetryableTransferAppError(`Error while disburse money, code: ${error.code}, message: ${error.message}!`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.COREBANKING_DISBURSEMENT_FAILED, AppError_1.isRetryableError(error.status));
    }
});
const submitRepayment = (transactionModel, bankIncomeAmount) => __awaiter(void 0, void 0, void 0, function* () {
    var _c;
    logger_1.default.info('submitRepayment: repay loan transaction.');
    try {
        if (transactionModel.beneficiaryAccountNo &&
            transactionModel.creditTransactionChannel) {
            const repaymentTransactionId = yield transactionCoreBanking_helper_1.default.submitRepaymentTransaction(transactionModel.beneficiaryAccountNo, transactionModel, transactionModel.creditTransactionChannel, bankIncomeAmount);
            (_c = transactionModel.coreBankingTransactions) === null || _c === void 0 ? void 0 : _c.push({
                id: repaymentTransactionId,
                type: transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_REPAID
            });
            if (transactionModel.coreBankingTransactionIds) {
                transactionModel.coreBankingTransactionIds.push(repaymentTransactionId);
            }
            transactionModel.journey = transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_REPAID, transactionModel);
        }
    }
    catch (error) {
        logger_1.default.error(`submitRepayment: Error while repay money, code: ${error.code}, message: ${error.message}`);
        throw new AppError_1.RetryableTransferAppError(`Error while repay money, code: ${error.code}, message: ${error.message}!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.COREBANKING_REPAYMENT_FAILED, AppError_1.isRetryableError(error.status));
    }
});
const submitGlTransaction = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    var _d;
    logger_1.default.info('submitGlTransaction: credit gl income');
    try {
        const glIncomeTransactionId = yield transactionCoreBanking_helper_1.default.submitGlTransaction(transactionModel);
        (_d = transactionModel.coreBankingTransactions) === null || _d === void 0 ? void 0 : _d.push({
            id: glIncomeTransactionId,
            type: transaction_enum_1.TransferJourneyStatusEnum.GL_TRANSACTION_SUCCESS
        });
        if (transactionModel.coreBankingTransactionIds) {
            transactionModel.coreBankingTransactionIds.push(glIncomeTransactionId);
        }
        transactionModel.journey = transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.GL_TRANSACTION_SUCCESS, transactionModel);
    }
    catch (error) {
        logger_1.default.error(`submitGlTransaction: Error while submit gl income code: ${error.code}, message: ${error.message}`);
        throw new AppError_1.RetryableTransferAppError(`Error while submit gl income code: ${error.code}, message: ${error.message}`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.COREBANKING_GL_JOURNAL_ENTRIES_FAILED, AppError_1.isRetryableError(error.status));
    }
});
const submitPayOff = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    var _e;
    logger_1.default.info('submitPayOff: payoff loan transaction.');
    try {
        const payOffTransactionId = `ext-${transactionModel.externalId}`;
        if (transactionModel.beneficiaryAccountNo &&
            transactionModel.creditTransactionChannel) {
            yield transactionCoreBanking_helper_1.default.submitPayOffTransaction(transactionModel.beneficiaryAccountNo, transactionModel, transactionModel.creditTransactionChannel);
            (_e = transactionModel.coreBankingTransactions) === null || _e === void 0 ? void 0 : _e.push({
                id: payOffTransactionId,
                type: transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_REPAID
            });
            if (transactionModel.coreBankingTransactionIds) {
                transactionModel.coreBankingTransactionIds.push(payOffTransactionId);
            }
            transactionModel.journey = transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_REPAID, transactionModel);
        }
    }
    catch (error) {
        logger_1.default.error(`submitPayOff: Error while pay off money, code: ${error.code}, message: ${error.message}`);
        throw new AppError_1.RetryableTransferAppError(`Error while pay off money, code: ${error.code}, message: ${error.message}!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.COREBANKING_PAY_OFF_FAILED, AppError_1.isRetryableError(error.status));
    }
});
const submitQrisPayment = (transactionModel, additionalPayload) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info('submitQrisPayment: qris transaction.');
    try {
        transaction_helper_1.default.setPaymentRailToTransactionData(transactionModel, module_common_1.Rail.ALTO_QRIS);
        const paymentResponse = yield transactionCoreBanking_helper_1.default.submitQrisTransaction(transactionModel, additionalPayload);
        alto_utils_1.default.setQrisResponseToTransactionData(transactionModel, paymentResponse.forwardingCustomerReferenceNumber);
        alto_utils_1.default.updateTransactionDataOnQrisPaymentSuccess(transactionModel, paymentResponse);
        alto_service_1.default.updateTransactionDetailsInExternalService(transactionModel, paymentResponse.transactionStatus, paymentResponse.forwardingCustomerReferenceNumber);
    }
    catch (error) {
        logger_1.default.error(`submitQrisPayment: error while submitting qris payment, code: ${error.code}, message: ${error.message}`);
        if (error instanceof alto_type_1.QrisError) {
            alto_utils_1.default.setQrisResponseToTransactionData(transactionModel, error.forwardingCustomerReferenceNumber || '');
            alto_service_1.default.updateTransactionDetailsInExternalService(transactionModel, error.qrisPaymentStatus, error.forwardingCustomerReferenceNumber);
            if (!alto_utils_1.default.isQrisTimeoutError(error)) {
                throw new AppError_1.RetryableTransferAppError((error && error.message) || 'Undetermined error!', transaction_enum_1.TransferFailureReasonActor.UNKNOWN, alto_utils_1.default.getExceptionErrorCode(error), false);
            }
        }
        else {
            throw new AppError_1.RetryableTransferAppError('Qris timeout error!', transaction_enum_1.TransferFailureReasonActor.QRIS, alto_utils_1.default.getExceptionErrorCode(error), AppError_1.isRetryableError(error.status));
        }
    }
});
const executeDebit = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield exports.debitSource(transaction);
        yield submitFees(transaction);
        transaction_helper_1.default.setPaymentRailToTransactionData(transaction, module_common_1.Rail.INTERNAL);
        return transaction;
    }
    catch (error) {
        logger_1.default.error(`executeDebit: error debiting amount for transaction journey: ${JSON.stringify(transaction.journey)}`);
        if (!lodash_1.isEmpty(transaction.coreBankingTransactionIds)) {
            yield transactionReversal_helper_1.default.revertManuallyAdjustedTransaction(transaction.id, transaction);
        }
        throw error;
    }
});
const executeDisbursementTransaction = (model, isUseCreditTemplate = true) => __awaiter(void 0, void 0, void 0, function* () {
    var _f, _g, _h;
    let transactionModel = model;
    let creditTemplate = isUseCreditTemplate
        ? getCreditTemplate(transactionModel)
        : undefined;
    transaction_helper_1.default.setPaymentRailToTransactionData(transactionModel, (_h = (_f = creditTemplate) === null || _f === void 0 ? void 0 : (_g = _f).getRail) === null || _h === void 0 ? void 0 : _h.call(_g));
    try {
        yield submitDisbursement(transactionModel);
        if (module_common_1.BankNetworkEnum.LOAN_PROCESSOR_WINCORE_DISBURSEMENT !== model.interchange)
            yield creditBeneficiary(transactionModel, creditTemplate);
        return transactionModel;
    }
    catch (error) {
        logger_1.default.error(`executeDisbursementTransaction: error executing transaction journey: ${JSON.stringify(model.journey)}`);
        if (!lodash_1.isEmpty(transactionModel.coreBankingTransactionIds)) {
            yield transactionReversal_helper_1.default.revertFailedTransaction(transactionModel.id, undefined, transactionModel);
        }
        throw error;
    }
});
const executeRepaymentTransaction = (model) => __awaiter(void 0, void 0, void 0, function* () {
    let transactionModel = model;
    let generalLedgers;
    const isGlTransactionDetailExists = transaction_util_1.isGlTransactionDetailFieldExists(model);
    transaction_helper_1.default.setPaymentRailToTransactionData(transactionModel, module_common_1.Rail.INTERNAL);
    try {
        if (isGlTransactionDetailExists)
            generalLedgers = yield configuration_repository_1.default.getAllGeneralLedgers();
        transaction_util_1.loanAdditionalPayloadValidator(model, generalLedgers);
        if (isGlTransactionDetailExists)
            yield submitGlTransaction(transactionModel);
        const isRecoveryWriteOff = model.paymentServiceType ===
            module_common_1.PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY;
        if (isRecoveryWriteOff)
            return transactionModel; // skip to execute next line
        const isLoanDirectRepayment = model.paymentServiceType === module_common_1.PaymentServiceTypeEnum.LOAN_DIRECT_REPAYMENT;
        if (isLoanDirectRepayment)
            yield exports.debitSource(transactionModel);
        const shouldRepayWithPayOff = lodash_1.get(model.additionalPayload, 'shouldRepayWithPayOff', false);
        const repaymentAmountForPayOffInterestSettlement = lodash_1.get(model.additionalPayload, 'repaymentAmountForPayOffInterestSettlement');
        if (repaymentAmountForPayOffInterestSettlement || !shouldRepayWithPayOff) {
            const loanRepaymentAllocation = lodash_1.get(transactionModel.additionalPayload, 'loanRepaymentAllocation');
            let bankIncomeAmount = 0;
            if (loanRepaymentAllocation) {
                const bankIncomeData = lodash_1.filter(loanRepaymentAllocation, { type: transaction_constant_1.BANK_INCOME });
                bankIncomeData.forEach(bankIncome => {
                    const amount = lodash_1.get(bankIncome, 'amount', 0);
                    bankIncomeAmount = transaction_util_1.rounding(bankIncomeAmount + amount);
                });
            }
            yield submitRepayment(transactionModel, bankIncomeAmount);
        }
        if (shouldRepayWithPayOff) {
            yield submitPayOff(transactionModel);
        }
        return transactionModel;
    }
    catch (error) {
        logger_1.default.error(`executeRepaymentTransaction: error executing transaction journey: ${JSON.stringify(model.journey)}`);
        if (!lodash_1.isEmpty(transactionModel.coreBankingTransactionIds)) {
            yield transactionReversal_helper_1.default.revertFailedTransaction(transactionModel.id, undefined, transactionModel);
        }
        throw error;
    }
});
const executePocketToGLTransaction = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let transactionInputUpdated = Object.assign(Object.assign({}, transactionModel), { journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.CLOSE_ACCOUNT_POCKET_TO_GL_TRANSACTION, transactionModel) });
        transaction_helper_1.default.setPaymentRailToTransactionData(transactionInputUpdated, module_common_1.Rail.INTERNAL);
        yield exports.debitSource(transactionInputUpdated);
        return transactionInputUpdated;
    }
    catch (error) {
        logger_1.default.error(`executePocketToGLTransaction: error executing transaction, journey details: ${JSON.stringify(transactionModel.journey)}`);
        logger_1.default.error(`executePocketToGLTransaction: error executing transaction, error details: ${JSON.stringify(error)}`);
        if (!lodash_1.isEmpty(transactionModel.coreBankingTransactionIds)) {
            yield transactionReversal_helper_1.default.revertFailedTransaction(transactionModel.id, undefined, transactionModel);
        }
        throw error;
    }
});
const executeForwardPaymentDeposit = (model) => __awaiter(void 0, void 0, void 0, function* () {
    let transactionModel = model;
    transaction_helper_1.default.setPaymentRailToTransactionData(transactionModel, module_common_1.Rail.INTERNAL);
    try {
        yield exports.debitSource(transactionModel);
        return transactionModel;
    }
    catch (error) {
        logger_1.default.error(`executeForwardPaymentDeposit: error executing transaction journey: ${JSON.stringify(model.journey)}`);
        throw error;
    }
});
const executeForwardPaymentWithdrawal = (model) => __awaiter(void 0, void 0, void 0, function* () {
    let transactionModel = model;
    try {
        let creditTemplate = getCreditTemplate(transactionModel);
        yield creditBeneficiary(transactionModel, creditTemplate);
        return transactionModel;
    }
    catch (error) {
        logger_1.default.error(`executeForwardPaymentWithdrawal: error executing transaction journey: ${JSON.stringify(model.journey)}`);
        throw error;
    }
});
const executeNoneBlockingTransaction = (model) => __awaiter(void 0, void 0, void 0, function* () {
    let transactionModel = model;
    // if transaction has credit, then it will require the submission template for credit tranction
    // this is to fail-fast validation
    let creditTemplate = getCreditTemplate(transactionModel);
    try {
        yield exports.debitSource(transactionModel);
        yield creditBeneficiary(transactionModel, creditTemplate);
        yield submitFees(transactionModel);
        return transactionModel;
    }
    catch (error) {
        logger_1.default.error(`executeNoneBlockingTransaction: error executing transaction journey: ${JSON.stringify(model.journey)}`);
        yield transaction_repository_1.default.update(transactionModel.id, {
            coreBankingTransactionIds: transactionModel.coreBankingTransactionIds,
            coreBankingTransactions: transactionModel.coreBankingTransactions
        });
        if (!lodash_1.isEmpty(transactionModel.coreBankingTransactionIds)) {
            yield transactionReversal_helper_1.default.revertFailedTransaction(transactionModel.id, undefined, transactionModel);
        }
        throw error;
    }
});
const executeBlockingTransaction = (model) => __awaiter(void 0, void 0, void 0, function* () {
    let transactionModel = model;
    // this will reject transaction if beneficiary is internal or beneficiary is optional
    if ((!transactionModel.beneficiaryBankCodeChannel &&
        !transactionModel.beneficiaryAccountNo) ||
        (transactionModel.beneficiaryBankCodeChannel === module_common_1.BankChannelEnum.INTERNAL &&
            transactionModel.beneficiaryAccountNo)) {
        logger_1.default.error('executeBlockingTransaction: transaction rejected due to invalid beneficiaryBankCodeChannel, beneficiaryAccountNo');
        throw new AppError_1.TransferAppError('Transaction rejected due to invalid beneficiaryBankCodeChannel or beneficiaryAccountNo.', transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.TRANSACTION_REJECTED);
    }
    // if transaction has credit, then it will require the submission template for credit tranction
    let creditTemplate = getCreditTemplate(model);
    if (!creditTemplate && transactionModel.requireThirdPartyOutgoingId) {
        logger_1.default.error('executeBlockingTransaction: transaction  rejected due to undefined credit template and undefined beneficiary account number.');
        throw new AppError_1.TransferAppError('Transaction rejected due to credit template was not found because of undefined beneficiary account number.', transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.REJECTED_DUE_UNDEFINED_BENEFICIARY_ACCOUNT_NUMBER);
    }
    try {
        if (transactionModel.debitTransactionCode &&
            transactionModel.debitTransactionChannel &&
            transactionModel.sourceAccountNo) {
            // this will blocking principal and fee
            const blockingTransaction = yield transactionBlocking_helper_1.default.createBlockingTransaction(transactionModel, module_common_1.Enum.BlockingAmountType.MOBILE_TRANSACTION);
            transactionModel = Object.assign(Object.assign(Object.assign({}, transactionModel), blockingTransaction), { journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_BLOCKED, model) });
        }
        if (creditTemplate) {
            transactionModel = Object.assign(Object.assign({}, transactionModel), { journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.SUBMITTING_THIRD_PARTY, transactionModel) });
            try {
                transactionModel = yield creditTemplate.submitTransaction(transactionModel);
            }
            catch (err) {
                logger_1.default.error(`executeBlockingTransaction: Error while executing credit template journey: ${JSON.stringify(transactionModel.journey)}, errorCode : ${err.code} message: ${err.message}`);
                return common_util_1.mapTransferAppError(err, errors_1.ERROR_CODE.THIRDPARTY_SUBMISSION_FAILED, true);
            }
        }
        return transactionModel;
    }
    catch (error) {
        logger_1.default.error(`executeBlockingTransaction: Error while executing Blocking transaction Journey: ${JSON.stringify(transactionModel.journey)}`);
        yield transactionBlocking_helper_1.default.cancelBlockingTransaction(transactionModel);
        throw error;
    }
});
const updateTransactionWithQrisDetails = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`updateTransactionWithQrisDetails: saving qris transaction. transactionId: ${transactionModel.id}`);
    try {
        const updatedData = yield transaction_util_1.mapToQrisTransferDb(transactionModel);
        return Object.assign(Object.assign({}, transactionModel), updatedData);
    }
    catch (error) {
        logger_1.default.error(`updateTransactionWithQrisDetails: Error while saving qris transaction, code: ${error.code}, message: ${error.message}`);
        throw new AppError_1.RetryableTransferAppError(`Error while saving qris transaction, code: ${error.code}, message: ${error.message}!`, transaction_enum_1.TransferFailureReasonActor.ALTO, errors_1.ERROR_CODE.ALTO_DATA_STORING_FAILED, AppError_1.isRetryableError(error.status));
    }
});
/**
 * Reverse core banking transactions, if any, and determine the transaction status based on it.
 * Any failed transaction with core banking transactions that is not reversed successfuly should stay in SUBMITTED status,
 * so it can be picked up again by the scheduled status check.
 *
 * @param transactionModel
 * @returns false when core banking transaction is available and the reversal is failed,
 *          true when no core banking transaction or the reversal process is successful,
 */
const shouldFailTransaction = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    return lodash_1.isEmpty(transactionModel.coreBankingTransactionIds) ||
        transactionReversal_helper_1.default.revertFailedTransactionWithoutError(transactionModel.id, undefined, transactionModel);
});
const executeQrisPayment = (model) => __awaiter(void 0, void 0, void 0, function* () {
    let transactionModel = model;
    const qrisPayload = model.additionalPayload;
    try {
        transaction_util_1.qrisAdditionalPayloadValidator(qrisPayload);
        transactionModel = yield updateTransactionWithQrisDetails(transactionModel);
        yield exports.debitSource(transactionModel);
        transactionModel = yield transaction_repository_1.default.update(transactionModel.id, Object.assign(Object.assign({}, transactionModel), { status: transaction_enum_1.TransactionStatus.SUBMITTING, journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.SUBMITTING_THIRD_PARTY, transactionModel) }));
        yield submitQrisPayment(transactionModel, qrisPayload);
        if (alto_utils_1.default.isAmountCredited(transactionModel)) {
            // settle the transaction once the payment submission is successful
            // no need transaction confirmation for QRIS
            transactionModel = yield settleTransaction(transactionModel, {
                submitTransaction: (transactionModel) => __awaiter(void 0, void 0, void 0, function* () { return transactionModel; }),
                isEligible: () => true
            });
        }
        else {
            // update all changes before suspending the transaction
            transactionModel = yield transaction_repository_1.default.update(transactionModel.id, transactionModel);
            yield transaction_producer_1.default.sendQrisTransactionNotification(transactionModel, transaction_producer_enum_1.NotificationCode.NOTIF_PENDING_QRIS);
        }
        return transactionModel;
    }
    catch (error) {
        // save all changes before failing
        transactionModel = yield transaction_repository_1.default.update(transactionModel.id, transactionModel);
        logger_1.default.error(`executeQrisPayment: error executing transaction journey: ${JSON.stringify(model.journey)}`);
        const transactionShouldBeFailed = yield shouldFailTransaction(transactionModel);
        if (!transactionShouldBeFailed) {
            // let the transaction status stay as SUBMITTED,
            // so it being picked up by the scheduled status check
            // to resolve the actual status
            return transactionModel;
        }
        if (error instanceof AppError_1.RetryableTransferAppError) {
            error.transaction = transactionModel;
        }
        throw error;
    }
});
const executeTransaction = (transactionInput, executionHooks = exports.defaultExecutionHooks) => __awaiter(void 0, void 0, void 0, function* () {
    let model = yield initTransaction(transactionInput, {
        submitTransaction: (model) => __awaiter(void 0, void 0, void 0, function* () {
            if (transaction_enum_1.ExecutionTypeEnum.NONE_BLOCKING === model.executionType) {
                switch (model.paymentServiceType) {
                    case module_common_1.PaymentServiceTypeEnum.LOAN_GL_DISBURSEMENT: {
                        return executeDisbursementTransaction(model, false);
                    }
                    case module_common_1.PaymentServiceTypeEnum.LOAN_DIRECT_DISBURSEMENT: {
                        return executeDisbursementTransaction(model);
                    }
                    case module_common_1.PaymentServiceTypeEnum.LOAN_DIRECT_REPAYMENT:
                    case module_common_1.PaymentServiceTypeEnum.LOAN_REPAYMENT:
                    case module_common_1.PaymentServiceTypeEnum.LOAN_GL_REPAYMENT:
                    case module_common_1.PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY: {
                        return executeRepaymentTransaction(model);
                    }
                    case module_common_1.PaymentServiceTypeEnum.MAIN_POCKET_TO_GL_TRANSFER: {
                        return executePocketToGLTransaction(model);
                    }
                    case module_common_1.PaymentServiceTypeEnum.LOAN_FORWARD_PAYMENT_DEPOSIT: {
                        return executeForwardPaymentDeposit(model);
                    }
                    case module_common_1.PaymentServiceTypeEnum.LOAN_FORWARD_PAYMENT_WITHDRAWAL: {
                        return executeForwardPaymentWithdrawal(model);
                    }
                    case module_common_1.PaymentServiceTypeEnum.QRIS: {
                        return executeQrisPayment(model);
                    }
                }
                return executeNoneBlockingTransaction(model);
            }
            if (transaction_enum_1.ExecutionTypeEnum.BLOCKING === model.executionType) {
                return executeBlockingTransaction(model);
            }
            logger_1.default.error(`executeTransaction: missing transaction execution type (blocking or none blocking) for transactionId : ${model.id}`);
            throw new AppError_1.TransferAppError(`Missing transaction execution type (blocking or none blocking) for transactionId : ${model.id}`, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.EXECUTION_TYPE_UNDEFINED);
        }),
        isEligible: () => true
    }, executionHooks);
    if (model.requireThirdPartyOutgoingId) {
        if (model.status === transaction_enum_1.TransactionStatus.SUCCEED ||
            model.status === transaction_enum_1.TransactionStatus.DECLINED) {
            return model;
        }
        const updatedModel = yield transaction_repository_1.default.update(model.id, {
            status: transaction_enum_1.TransactionStatus.SUBMITTED,
            processingInfo: model.processingInfo,
            journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.THIRD_PARTY_SUBMITTED, model)
        });
        if (!updatedModel) {
            logger_1.default.error(`executeTransaction: failed to update transaction status for transactionId : ${model.id}.`);
            throw new AppError_1.TransferAppError(`Failed to update transaction status for transactionId : ${model.id}`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION_STATUS);
        }
        logger_1.default.info(`transactionSubmitted:
      customerId : ${updatedModel.sourceCustomerId},
      externalId : ${updatedModel.externalId},
      transactionId: ${updatedModel.id},
      beneficiaryAccount: ${updatedModel.beneficiaryAccountNo},
      beneficiaryBankCode: ${updatedModel.beneficiaryBankCode},
      sourceAccount: ${updatedModel.sourceAccountNo},
      Journey: ${JSON.stringify(updatedModel.journey)}`);
        return updatedModel;
    }
    else {
        return yield settleTransaction(model, {
            submitTransaction: (transactionModel) => __awaiter(void 0, void 0, void 0, function* () { return transactionModel; }),
            isEligible: () => true
        }, executionHooks);
    }
});
const executeCheckStatus = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    const statusTemplate = transactionStatus_template_1.default.getStatusTemplate(transactionModel);
    if (!lodash_1.isUndefined(statusTemplate) &&
        !lodash_1.isUndefined(statusTemplate.getTransactionStatus)) {
        logger_1.default.info(`executeCheckStatus: status template found`);
        return yield statusTemplate.getTransactionStatus(transactionModel);
    }
    logger_1.default.info(`executeCheckStatus: sending status from transaction db`);
    return { status: transactionModel.status };
});
const transactionExecutionService = logger_1.wrapLogs({
    executeTransaction,
    initTransaction,
    settleTransaction,
    onFailedTransaction,
    executeDebit,
    executeCheckStatus
});
exports.default = transactionExecutionService;
//# sourceMappingURL=transactionExecution.service.js.map