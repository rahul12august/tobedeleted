"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_service_1 = __importDefault(require("./transaction.service"));
const module_common_1 = require("@dk/module-common");
const transaction_enum_1 = require("./transaction.enum");
const config_1 = require("../config");
const transaction_validator_1 = require("./transaction.validator");
const handleValidationErrors_1 = require("../common/handleValidationErrors");
const transactionReversal_service_1 = __importDefault(require("./transactionReversal.service"));
const logger_1 = __importStar(require("../logger"));
const transaction_producer_1 = __importDefault(require("./transaction.producer"));
const lodash_1 = require("lodash");
const transaction_helper_1 = __importDefault(require("./transaction.helper"));
const transactionBlocking_service_1 = __importDefault(require("./transactionBlocking.service"));
const transactionBlocking_service_2 = __importDefault(require("./transactionBlocking.service"));
const transaction_repository_1 = __importDefault(require("./transaction.repository"));
const module_message_1 = require("@dk/module-message");
const transaction_util_1 = require("./transaction.util");
const contextHandler_1 = require("../common/contextHandler");
const util_1 = require("util");
const common_util_1 = require("../common/common.util");
const generalRefund_service_1 = __importDefault(require("./refund/generalRefund.service"));
const transaction_constant_1 = require("./transaction.constant");
const entitlement_service_1 = __importDefault(require("../entitlement/entitlement.service"));
const entitlementFlag_1 = __importDefault(require("../common/entitlementFlag"));
const transactionReversalRetryPolicy = config_1.config.get('transactionReversalRetryPolicy');
exports.executionTransaction = (messageToConsume, messageTopic) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    let result;
    const value = JSON.parse(messageToConsume.message.value);
    const inputPaymentServiceType = value && value.paymentServiceType;
    const isTopupWallet = inputPaymentServiceType === module_common_1.PaymentServiceTypeEnum.WALLET;
    let payload;
    if (isTopupWallet) {
        payload = handleValidationErrors_1.validateHandler(value, transaction_validator_1.topupInternalTransactionRequestPayload, {
            stripUnknown: true // only validate and return fields belong to topupInternalTransaction
        });
    }
    else {
        payload = handleValidationErrors_1.validateHandler(value, transaction_validator_1.createTransferTransactionRequestValidator);
    }
    let executionType = messageToConsume.message.headers[module_common_1.Constant.EXECUTION_TYPE];
    executionType = executionType && executionType.toString();
    let paymentInstructionCode = messageToConsume.message.headers[module_common_1.Constant.PI_KEY_HEADER];
    paymentInstructionCode =
        paymentInstructionCode && paymentInstructionCode.toString();
    const transactionResponseTopic = messageToConsume.message.headers[transaction_constant_1.TRANSACTION_RESPONSE_TOPIC_FIELD];
    if (paymentInstructionCode) {
        const messageTopics = [];
        messageTopics.push(messageTopic);
        if (transactionResponseTopic)
            messageTopics.push(transactionResponseTopic);
        const paymentInstructionExecution = {
            paymentInstructionCode,
            executionType,
            topics: messageTopics
        };
        payload.paymentInstructionExecution = paymentInstructionExecution;
    }
    let externalId;
    let transaction;
    externalId = payload.externalId;
    result = {
        externalId,
        status: transaction_enum_1.PaymentRequestStatus.SUCCEED,
        extra: transaction_util_1.updateExtraFields(payload)
    };
    if (payload.paymentInstructionId) {
        result = Object.assign(Object.assign({}, result), { paymentInstructionId: payload.paymentInstructionId });
    }
    logger_1.default.info(`executionTransaction : Create ${(_a = payload.paymentInstructionExecution) === null || _a === void 0 ? void 0 : _a.executionType} transaction with
      topicName: ${messageToConsume.topicName}
      sourceCIF: ${payload.sourceCIF},
      sourceAccountNo: ${payload.sourceAccountNo},
      beneficiaryBankCode: ${payload.beneficiaryBankCode},
      beneficiaryAccountNo: ${payload.beneficiaryAccountNo},
      paymentServiceCode: ${payload.paymentServiceCode},
      paymentInstructionId: ${payload.paymentRequestID},
      externalId: ${payload.externalId}`);
    try {
        common_util_1.trimStringValuesFromObject(payload);
        switch (executionType) {
            case module_common_1.TransactionExecutionTypeEnum.CREATE_BLOCKING: {
                logger_1.default.debug(`executionTransaction : Create blocking transaction payload : ${JSON.stringify(payload)}`);
                transaction = yield transactionBlocking_service_1.default.createBlockingTransferTransaction(payload);
                result = Object.assign(Object.assign({}, result), { transactionId: transaction.id, transactionStatus: transaction.status });
                logger_1.default.info(`transactionCompleted: Blocking transaction 
            customerId : ${transaction.sourceCustomerId},
            externalId : ${transaction.externalId}, 
            transactionId: ${transaction.id},
            beneficiaryAccount: ${transaction.beneficiaryAccountNo},
            beneficiaryBankCode: ${transaction.beneficiaryBankCode},
            sourceAccount: ${transaction.sourceAccountNo},
            journey: ${JSON.stringify(transaction.journey)}`);
                break;
            }
            case module_common_1.TransactionExecutionTypeEnum.ADJUST_BLOCKING: {
                if (payload.externalId && payload.sourceCIF) {
                    logger_1.default.debug(`executionTransaction : Adjust blocking transaction payload : ${JSON.stringify(payload)}`);
                    const transaction = yield transactionBlocking_service_2.default.adjustBlockingTransferTransaction(payload.sourceCIF, payload.externalId, payload.transactionAmount);
                    result = Object.assign(Object.assign({}, result), { transactionAmount: transaction.transactionAmount, transactionStatus: transaction.status, transactionId: transaction.id });
                    logger_1.default.info(`transactionCompleted: Adjust blocking transaction 
              customerId : ${transaction.sourceCustomerId},
              externalId : ${transaction.externalId}, 
              transactionId: ${transaction.id},
              beneficiaryAccount: ${transaction.beneficiaryAccountNo},
              beneficiaryBankCode: ${transaction.beneficiaryBankCode},
              sourceAccount: ${transaction.sourceAccountNo},
              Journey: ${JSON.stringify(transaction.journey)}`);
                }
                break;
            }
            case module_common_1.TransactionExecutionTypeEnum.CANCEL_BLOCKING: {
                if (payload.externalId && payload.sourceCIF) {
                    logger_1.default.debug(`executionTransaction : Cancel blocking transaction payload : ${JSON.stringify(payload)}`);
                    yield transactionBlocking_service_2.default.cancelBlockingTransferTransaction(payload.sourceCIF, payload.externalId);
                    result = Object.assign(Object.assign({}, result), { transactionStatus: transaction_enum_1.TransactionStatus.DECLINED });
                }
                break;
            }
            case module_common_1.TransactionExecutionTypeEnum.CONFIRM_BLOCKING: {
                if (payload.externalId && payload.sourceCIF) {
                    logger_1.default.debug(`executionTransaction : Confirm blocking transaction payload : ${JSON.stringify(payload)}`);
                    transaction = yield transactionBlocking_service_2.default.executeBlockingTransferTransaction(payload.sourceCIF, payload.externalId, payload.transactionAmount);
                    result = Object.assign(Object.assign({}, result), { transactionStatus: transaction.status, referenceId: transaction.referenceId });
                    logger_1.default.info(`transactionCompleted: Confirm blocking transaction 
              customerId : ${transaction.sourceCustomerId},
              externalId : ${transaction.externalId}, 
              transactionId: ${transaction.id},
              beneficiaryAccount: ${transaction.beneficiaryAccountNo},
              beneficiaryBankCode: ${transaction.beneficiaryBankCode},
              sourceAccount: ${transaction.sourceAccountNo}
              Journey: ${JSON.stringify(transaction.journey)}`);
                }
                break;
            }
            default:
                if (!isTopupWallet) {
                    logger_1.default.debug(`executionTransaction : Create transfer transaction payload : ${JSON.stringify(payload)}`);
                    if (payload.paymentServiceType === module_common_1.PaymentServiceTypeEnum.GENERAL_REFUND) {
                        logger_1.default.info(`executionTransaction: Create general refund transaction, payload: ${JSON.stringify(payload.generalRefund)}`);
                        transaction = yield generalRefund_service_1.default.refundTransaction(payload.generalRefund);
                    }
                    else {
                        logger_1.default.info(`executionTransaction: Create transfer transaction.`);
                        transaction = yield transaction_service_1.default.createTransferTransaction(payload);
                    }
                }
                else {
                    logger_1.default.debug(`executionTransaction : Create internal topup transaction payload : ${JSON.stringify(payload)}`);
                    transaction = yield transaction_service_1.default.createInternalTopupTransaction(payload);
                }
                result = Object.assign(Object.assign({}, result), { transactionId: transaction.id, transactionStatus: transaction.status, referenceId: transaction.referenceId, transactionAmount: transaction.transactionAmount, feeAmount: transaction.fees && transaction.fees.length
                        ? lodash_1.sumBy(transaction.fees, 'feeAmount')
                        : 0 });
                break;
        }
    }
    catch (err) {
        logger_1.default.error(`executionTransaction : Error while executing transaction with external id ${externalId},
        errors : ${util_1.inspect(err, false, 5)},
        errorCode : ${err.errorCode}`);
        let transactionStatus;
        const transaction = externalId &&
            (yield transaction_repository_1.default.getByExternalId(externalId).catch(getErr => {
                logger_1.default.error("can't get transaction by externalId", JSON.stringify(getErr));
                transactionStatus = undefined;
            }));
        if (transaction) {
            transactionStatus = transaction.status;
        }
        result = Object.assign(Object.assign({}, result), { status: transaction_enum_1.PaymentRequestStatus.DECLINED, transactionStatus: transactionStatus, transactionAmount: payload.transactionAmount, error: {
                errorCode: err.errorCode,
                retriable: err.retry,
                errors: err.errors
            }, failureReason: transaction ? transaction.failureReason : undefined });
        if (yield entitlementFlag_1.default.isEnabled()) {
            yield entitlement_service_1.default.processReversalEntitlementCounterUsageBasedOnContext();
        }
    }
    if (messageTopic) {
        logger_1.default.info(`executionTransaction: Producing message on topic : ${messageTopic} with
        referenceId: ${result.referenceId},
        externalId: ${result.externalId},
        transactionId: ${result.transactionId},
        status: ${result.status},
        transactionStatus: ${result.transactionStatus},
        sourceAccountNo: ${payload.sourceAccountNo},
        beneficiaryAccountNo: ${payload.beneficiaryAccountNo},
        paymentInstructionId: ${payload.paymentRequestID},
        error: ${JSON.stringify(result.error)}`);
        transaction_producer_1.default.sendTransactionMessage(messageTopic, result, executionType, paymentInstructionCode);
        if (transactionResponseTopic) {
            transaction_producer_1.default.sendTransactionMessage(transactionResponseTopic, result, executionType, paymentInstructionCode);
        }
    }
});
exports.directTransactionHandler = (messageToConsume) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.debug(`directTransactionHandler: Payload for direct transaction handler : ${JSON.stringify(messageToConsume)} `);
    logger_1.default.info(`directTransactionHandler: Direct transaction handler`);
    const messageTopic = module_message_1.PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE;
    yield contextHandler_1.setRetryCount(3);
    yield exports.executionTransaction(messageToConsume, messageTopic);
});
exports.autoTransferTransactionHandler = (messageToConsume) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.debug(`autoTransferTransactionHandler: Payload for auto transfer transaction handler: ${JSON.stringify(messageToConsume)} `);
    logger_1.default.info(`autoTransferTransactionHandler: Auto transfer transaction handler`);
    const messageTopic = module_message_1.PaymentInstructionTopicConstant.UPDATE_TRANSACTION_STATUS;
    yield contextHandler_1.setRetryCount(3);
    yield exports.executionTransaction(messageToConsume, messageTopic);
});
exports.retryTransactionReversalHandler = (message) => __awaiter(void 0, void 0, void 0, function* () {
    if (message.retryCounter <= transactionReversalRetryPolicy.maxRetries) {
        const transaction = yield transaction_repository_1.default.findOneByQuery({
            _id: message.transactionId
        });
        transaction &&
            (yield transactionReversal_service_1.default
                .reverseTransactionsByInstructionId(message.transactionId, undefined, transaction)
                .catch(err => {
                if (message.retryCounter < transactionReversalRetryPolicy.maxRetries) {
                    logger_1.default.error('retry transaction reversal has failed. Producing next retry message', JSON.stringify(err));
                    const delayTime = transactionReversalRetryPolicy.delayTimeInSeconds *
                        message.retryCounter *
                        1000;
                    lodash_1.delay(transaction_producer_1.default.sendTransactionRetryReversalCommand, delayTime, [
                        {
                            transactionId: message.transactionId,
                            retryCounter: message.retryCounter + 1
                        }
                    ]);
                }
                else {
                    logger_1.default.error(`retry transaction reversal has failed after all retry, max retries setting: ${transactionReversalRetryPolicy.maxRetries}.Moving message to reversal_failed topic`, JSON.stringify(err));
                    transaction_producer_1.default.sendTransactionReversalFailedEvent(message);
                }
            })
                .then(() => __awaiter(void 0, void 0, void 0, function* () {
                yield transaction_helper_1.default.updateTransactionStatus(message.transactionId, transaction_enum_1.TransactionStatus.REVERTED);
            })));
    }
    else {
        logger_1.default.error(`the received message has retryCounter ${message.retryCounter} that exceeds max retries setting: ${transactionReversalRetryPolicy.maxRetries}.Moving message to reversal_failed topic`);
        transaction_producer_1.default.sendTransactionReversalFailedEvent(message);
    }
});
exports.default = logger_1.wrapLogs({
    autoTransferTransactionHandler: exports.autoTransferTransactionHandler,
    retryTransactionReversalHandler: exports.retryTransactionReversalHandler,
    directTransactionHandler: exports.directTransactionHandler
});
//# sourceMappingURL=transaction.consumer.js.map