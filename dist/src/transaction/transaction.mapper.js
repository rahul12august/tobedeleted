"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const lodash_1 = require("lodash");
const logger_1 = require("../logger");
const configuration_service_1 = __importDefault(require("../configuration/configuration.service"));
const limitGroup_1 = __importDefault(require("../limitGroup/limitGroup"));
const logger_2 = __importDefault(require("../logger"));
const transaction_conditions_1 = __importDefault(require("./transaction.conditions"));
const transaction_conditions_2 = require("./transaction.conditions");
const octVerifyer_1 = require("./octVerifyer");
const transaction_constant_1 = require("./transaction.constant");
const customer_cache_1 = __importDefault(require("../customer/customer.cache"));
const transaction_util_1 = require("./transaction.util");
const buildLimitGroupCodesMap = (limitGroups) => {
    return limitGroups.reduce((acc, limitGroup) => {
        acc[limitGroup.code] = limitGroup;
        return acc;
    }, {});
};
const getDailyUsages = (usageCounters, paymentServiceCode) => {
    logger_2.default.info(`getDailyUsages: getting usage counter for paymentServiceCode: ${paymentServiceCode}`);
    const dailyUsages = {
        rtgsDailyUsage: 0,
        rtolDailyUsage: 0,
        sknDailyUsage: 0,
        billDailyUsage: 0,
        bifastDailyUsage: 0
    };
    const { SKN_LIMIT_GROUP_CODE, RTOL_LIMIT_GROUP_CODE, RTGS_LIMIT_GROUP_CODE } = limitGroup_1.default.getLimitGroupCodes(paymentServiceCode);
    usageCounters.forEach(counter => {
        const usage = counter.dailyAccumulationAmount;
        switch (counter.limitGroupCode) {
            case SKN_LIMIT_GROUP_CODE:
                dailyUsages.sknDailyUsage = usage;
                break;
            case RTOL_LIMIT_GROUP_CODE:
                dailyUsages.rtolDailyUsage = usage;
                break;
            case RTGS_LIMIT_GROUP_CODE:
                dailyUsages.rtgsDailyUsage = usage;
                break;
            case transaction_constant_1.BILL_LIMIT_GROUP_CODE:
                dailyUsages.billDailyUsage = usage;
                break;
            case transaction_constant_1.BIFAST_LIMIT_GROUP_CODE:
                dailyUsages.bifastDailyUsage = usage;
                break;
            default:
                break;
        }
    });
    return dailyUsages;
};
const fetchIsShariaCustomer = (data) => __awaiter(void 0, void 0, void 0, function* () {
    const customerType = data.sourceCustomerId &&
        (yield customer_cache_1.default.getCustomerType(data.sourceCustomerId));
    return customerType === "INDIVIDUAL_SHARIA" /* INDIVIDUAL_SHARIA */;
});
const isExternalChannel = (channelType) => {
    return !lodash_1.isEmpty(channelType) && channelType == module_common_1.BankChannelEnum.EXTERNAL;
};
const calcBiFastEnabled = (transactionInput) => __awaiter(void 0, void 0, void 0, function* () {
    let isBiFastEnabled = yield configuration_service_1.default.isBiFastEnabled();
    if (isBiFastEnabled &&
        isExternalChannel(transactionInput.preferedBankChannel)) {
        logger_2.default.info(`Rerouting transaction from BIFAST to External for BankCode: ${transactionInput.beneficiaryBankCode}`);
        isBiFastEnabled = false;
    }
    return isBiFastEnabled;
});
var NO_RECOMMENDATION_PAYMENT_SERVICE_CODE;
(function (NO_RECOMMENDATION_PAYMENT_SERVICE_CODE) {
    NO_RECOMMENDATION_PAYMENT_SERVICE_CODE["SKN_FOR_BUSINESS"] = "SKN_FOR_BUSINESS";
    NO_RECOMMENDATION_PAYMENT_SERVICE_CODE["RTOL_FOR_BUSINESS"] = "RTOL_FOR_BUSINESS";
    NO_RECOMMENDATION_PAYMENT_SERVICE_CODE["RTGS_FOR_BUSINESS"] = "RTGS_FOR_BUSINESS";
    NO_RECOMMENDATION_PAYMENT_SERVICE_CODE["BIFAST_FOR_BUSINESS"] = "BIFAST_FOR_BUSINESS";
})(NO_RECOMMENDATION_PAYMENT_SERVICE_CODE || (NO_RECOMMENDATION_PAYMENT_SERVICE_CODE = {}));
const isAutoRoutingEnabled = (paymentServiceCode) => {
    if (!paymentServiceCode) {
        return true;
    }
    const found = Object.values(NO_RECOMMENDATION_PAYMENT_SERVICE_CODE).find(businessPsc => businessPsc === paymentServiceCode);
    return !found;
};
const filterPaymentConfigRulesForBusinessPSCs = (paymentConfigRules, paymentServiceCode) => {
    if (isAutoRoutingEnabled(paymentServiceCode)) {
        return paymentConfigRules;
    }
    if (!paymentServiceCode) {
        throw new Error('Internal error at filterByPaymentServiceCode');
    }
    return paymentConfigRules.filter(pcr => pcr.serviceRecommendation.includes(paymentServiceCode));
};
/**
 * Retruns the IPaymentServiceMapping[] from first paymentConfigRule
 * that matches to all properties of the current transaction.
 */
const inferMatchingPaymentServiceMappings = (paymentConfigRules, pscToDailyLimits, currentTransaction) => __awaiter(void 0, void 0, void 0, function* () {
    let configPassed;
    for (let paymentConfigRule of paymentConfigRules) {
        configPassed = true;
        for (let ruleFn of transaction_conditions_1.default) {
            const status = ruleFn(currentTransaction, paymentConfigRule, pscToDailyLimits);
            if (!status.isSuccess()) {
                logger_2.default.info(`inferMatchingPaymentServiceMappings: Service rule evaluation failed for config: ${paymentConfigRule.paymentServiceType}, code: ${JSON.stringify(paymentConfigRule.serviceRecommendation)}, rule: ${ruleFn.name}, message: ${status.message}`);
                if (!isAutoRoutingEnabled(currentTransaction.paymentServiceCode)) {
                    throw status.toError();
                }
                configPassed = false;
                break;
            }
        }
        if (configPassed) {
            if (transaction_util_1.isReadOnlyMode()) {
                return paymentConfigRule.paymentServiceMappings;
            }
            const result = yield transaction_conditions_2.filterByDateAndWorkingTime(paymentConfigRule.paymentServiceMappings, currentTransaction.date);
            logger_2.default.info(`inferMatchingPaymentServiceMappings: Passed config serviceType: ${paymentConfigRule.paymentServiceType}, 
        service code: ${paymentConfigRule.paymentServiceMappings[0].paymentServiceCode}
          ${result.status.message}`);
            if (lodash_1.isEmpty(result.mappings) &&
                !isAutoRoutingEnabled(currentTransaction.paymentServiceCode)) {
                throw result.status.toError();
            }
            return result.mappings;
        }
    }
    return [];
});
const getPaymentServiceMappings = (transactionInput, paymentConfigRules, limitGroups, usageCounters, executionDate = new Date()) => __awaiter(void 0, void 0, void 0, function* () {
    octVerifyer_1.denyOCT(transactionInput);
    const limitGroupCodesMap = buildLimitGroupCodesMap(limitGroups);
    const dailyUsages = getDailyUsages(usageCounters, transactionInput.paymentServiceCode);
    const isBiFastEnabled = yield calcBiFastEnabled(transactionInput);
    const isShariaCustomer = yield fetchIsShariaCustomer(transactionInput);
    const ruleInput = Object.assign(Object.assign(Object.assign(Object.assign({}, transactionInput), dailyUsages), { isBiFastEnabled, isShariaCustomer }), { date: executionDate });
    const paymentConfigRulesToUse = filterPaymentConfigRulesForBusinessPSCs(paymentConfigRules, transactionInput.paymentServiceCode);
    return inferMatchingPaymentServiceMappings(paymentConfigRulesToUse, limitGroupCodesMap, ruleInput);
});
exports.default = logger_1.wrapLogs({
    getPaymentServiceMappings
});
//# sourceMappingURL=transaction.mapper.js.map