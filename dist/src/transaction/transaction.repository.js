"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const moment_1 = __importDefault(require("moment"));
const transaction_model_1 = require("./transaction.model");
const bson_1 = require("bson");
const lodash_1 = require("lodash");
const module_common_1 = require("@dk/module-common");
const dateUtils_1 = require("../common/dateUtils");
const transaction_helper_1 = __importDefault(require("./transaction.helper"));
const errors_1 = require("../common/errors");
const AppError_1 = require("../errors/AppError");
const transaction_enum_1 = require("./transaction.enum");
const transaction_constant_1 = __importDefault(require("./transaction.constant"));
const convertTransactionDocumentToObject = (document) => document.toObject({ getters: true });
const convertTransactionListDocumentToObject = (documents) => documents.map(document => convertTransactionDocumentToObject(document));
const create = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    const newTransactionModel = new transaction_model_1.TransactionModel(transactionModel);
    yield newTransactionModel.save();
    return convertTransactionDocumentToObject(newTransactionModel);
});
const update = (id, transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    const transaction = yield transaction_model_1.TransactionModel.findOneAndUpdate({ _id: id }, transactionModel, { new: true });
    const response = transaction && convertTransactionDocumentToObject(transaction);
    if (!response) {
        const detail = `Failed to update transaction info for txnId: ${transactionModel.id}!`;
        logger_1.default.error(`update: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION);
    }
    return response;
});
const updateTransactionsByReferenceId = (referenceId, transactionUpdate // should only extend field when needed
) => __awaiter(void 0, void 0, void 0, function* () {
    const res = yield transaction_model_1.TransactionModel.updateMany({ referenceId }, {
        $set: transactionUpdate
    });
    return res.n > 0;
});
const findByIdAndUpdate = (txnId, transactionUpdate) => __awaiter(void 0, void 0, void 0, function* () {
    const transaction = yield transaction_model_1.TransactionModel.findByIdAndUpdate(txnId, {
        $set: transactionUpdate
    }, { new: true });
    return transaction && convertTransactionDocumentToObject(transaction);
});
const findOneByQuery = (query) => __awaiter(void 0, void 0, void 0, function* () {
    const transaction = yield transaction_model_1.TransactionModel.findOne(query);
    return transaction && convertTransactionDocumentToObject(transaction);
});
/**
 * @description
 * This method is used to find transactions by thirdparty outgoing and externalId if provided
 * for outgoing we will always have unique thirdpartyoutgoing id so we will always have one record
 * for incoming we may have duplicate external ids(thirdpartyOutgoingId)
 * so in case of multiple transactions we need to filter with externalId_date_accountId
 */
const findTransactionForConfirmation = (input, externalId) => __awaiter(void 0, void 0, void 0, function* () {
    const transactions = yield transaction_model_1.TransactionModel.find({
        thirdPartyOutgoingId: input.externalId
    });
    let transaction = undefined;
    if (transactions && transactions.length === 1) {
        // For Outgoing transactions
        transaction = transactions[0];
    }
    else if (transactions && transactions.length > 1 && externalId) {
        // For Incoming transactions should match with externalId first
        transaction = transactions.find(trx => trx.externalId === externalId);
        // Will match with secondaryExternalId if with externalId didn't find any record
        if (lodash_1.isEmpty(transaction)) {
            transaction = transactions.find(trx => trx.secondaryExternalId === externalId);
        }
    }
    return transaction;
});
const updateIfExists = (input, externalId) => __awaiter(void 0, void 0, void 0, function* () {
    const transaction = yield findTransactionForConfirmation(input, externalId);
    if (!transaction || lodash_1.isEmpty(transaction)) {
        return null;
    }
    if ((input.responseCode === transaction_enum_1.ConfirmTransactionStatus.ERROR_EXPECTED ||
        input.responseCode === transaction_enum_1.ConfirmTransactionStatus.ERROR_UNEXPECTED) &&
        transaction.createdAt &&
        !dateUtils_1.isCurrentDate(transaction.createdAt)) {
        logger_1.default.info(`updateIfExists: ignoring reversal of transaction externalId ${input.externalId}, as original transaction is not of same day`);
        return null;
    }
    const updatedTransaction = yield transaction_model_1.TransactionModel.findByIdAndUpdate(transaction.id, {
        $push: {
            reason: {
                responseCode: input.responseCode,
                responseMessage: input.responseMessage,
                transactionDate: input.transactionDate
            },
            journey: {
                status: transaction_enum_1.TransferJourneyStatusEnum.TRANSFER_CONFIRMATION_RECEIVED,
                updatedAt: new Date()
            }
        }
    }, { new: true });
    return (updatedTransaction && convertTransactionDocumentToObject(updatedTransaction));
});
const getByKey = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const queryOr = [
        {
            externalId: id
        }
    ];
    if (bson_1.ObjectId.isValid(id)) {
        queryOr.push({ _id: id });
    }
    return findOneByQuery({ $or: queryOr });
});
const getByThirdPartyOutgoingId = (id) => __awaiter(void 0, void 0, void 0, function* () {
    return findOneByQuery({ thirdPartyOutgoingId: id });
});
const getByQRISRefundIdentifier = (id) => __awaiter(void 0, void 0, void 0, function* () {
    return findOneByQuery({
        externalId: id,
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.VOID_QRIS
    });
});
// TODO: deprecated
const getAllByExternalId = (externalId) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionList = yield transaction_model_1.TransactionModel.find({
        externalId: externalId
    });
    return (transactionList && convertTransactionListDocumentToObject(transactionList));
});
// TODO: deprecated
const getByExternalId = (externalId) => __awaiter(void 0, void 0, void 0, function* () {
    return findOneByQuery({
        externalId: externalId
    });
});
const getIncomingNonRefunded = (externalId, accountNumber) => __awaiter(void 0, void 0, void 0, function* () {
    return findOneByQuery({
        externalId: { $regex: `${externalId}.*${accountNumber}` },
        paymentServiceType: {
            $nin: [
                module_common_1.PaymentServiceTypeEnum.DOMESTIC_VOID_PAYMENT,
                module_common_1.PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT
            ]
        }
    });
});
const getAtomeTxnTobeRefunded = (thirdPartyOutgoingId) => __awaiter(void 0, void 0, void 0, function* () {
    return findOneByQuery({
        thirdPartyOutgoingId: thirdPartyOutgoingId,
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.ATOME_PAYMENT
    });
});
const createOrCondition = (cif) => {
    const searchCriteria = [];
    searchCriteria.push({ sourceCIF: cif });
    searchCriteria.push({ beneficiaryCIF: cif });
    return {
        $or: searchCriteria
    };
};
const getTransactionListQuery = (input, cif) => {
    const searchCriteria = [];
    if (!cif && input.customerId) {
        logger_1.default.info(`getTransactionListQuery: Adding ownerCustomerId in search query`);
        searchCriteria.push({ ownerCustomerId: input.customerId });
    }
    if (cif) {
        searchCriteria.push(createOrCondition(cif));
    }
    if (input.transactionId) {
        searchCriteria.push({ _id: input.transactionId });
    }
    if (input.startDate) {
        searchCriteria.push({
            createdAt: { $gte: dateUtils_1.toDate(input.startDate) }
        });
    }
    if (input.endDate) {
        searchCriteria.push({
            createdAt: { $lte: dateUtils_1.toDate(input.endDate).setHours(23, 59, 59, 999) }
        });
    }
    if (!input.startDate && !input.endDate) {
        const fromDate = transaction_helper_1.default.getTransactionSearchStartDate();
        searchCriteria.push({
            createdAt: { $gte: fromDate }
        });
        searchCriteria.push({
            createdAt: { $lte: new Date().setHours(23, 59, 59, 999) }
        });
    }
    if (input.transactionStatus) {
        searchCriteria.push({ status: { $in: input.transactionStatus } });
    }
    if (input.beneficiaryAccountNo) {
        searchCriteria.push({ beneficiaryAccountNo: input.beneficiaryAccountNo });
    }
    if (input.sourceAccountNo) {
        searchCriteria.push({ sourceAccountNo: input.sourceAccountNo });
    }
    if (input.sourceBankCode) {
        searchCriteria.push({ sourceBankCode: input.sourceBankCode });
    }
    if (input.beneficiaryBankCode) {
        searchCriteria.push({ beneficiaryBankCode: input.beneficiaryBankCode });
    }
    if (input.accountNo) {
        const account = new RegExp(input.accountNo, 'i');
        searchCriteria.push({
            $or: [{ sourceAccountNo: account }, { beneficiaryAccountNo: account }]
        });
    }
    if (input.transactionAmount) {
        searchCriteria.push({
            transactionAmount: {
                $gte: input.transactionAmount,
                $lt: input.transactionAmount + 1
            }
        });
    }
    return lodash_1.isEmpty(searchCriteria)
        ? {}
        : {
            $and: searchCriteria
        };
};
const getTransactionListByCriteria = (input, cif) => __awaiter(void 0, void 0, void 0, function* () {
    const limit = input.limit || transaction_constant_1.default.DEFAULT_RECORD_PER_PAGE;
    const page = input.page || 1;
    const skip = (page - 1) * limit;
    const query = getTransactionListQuery(input, cif);
    const transactionList = yield transaction_model_1.TransactionModel.find(query)
        .sort({
        createdAt: -1
    })
        .skip(skip)
        .limit(limit)
        .exec();
    const totalCount = yield transaction_model_1.TransactionModel.countDocuments(query);
    return {
        list: transactionList &&
            convertTransactionListDocumentToObject(transactionList),
        limit,
        page,
        totalPage: Math.ceil(totalCount / limit)
    };
});
const getTransactionStatusQuery = (input) => {
    const searchCriteria = [];
    if (input.customerId) {
        searchCriteria.push({
            ownerCustomerId: input.customerId
        });
    }
    if (input.paymentInstructionId) {
        searchCriteria.push({
            paymentInstructionId: input.paymentInstructionId
        });
    }
    const transactionDate = input.transactionDate
        ? input.transactionDate
        : new Date();
    const dateFrom = transactionDate.setDate(transactionDate.getDate() - 1);
    const dateUntil = transactionDate.setDate(transactionDate.getDate() + 2);
    searchCriteria.push({
        createdAt: {
            $gte: dateFrom,
            $lte: dateUntil
        }
    });
    return lodash_1.isEmpty(searchCriteria) ? {} : { $and: searchCriteria };
};
const getTransactionStatusByCriteria = (input) => __awaiter(void 0, void 0, void 0, function* () {
    const query = getTransactionStatusQuery(input);
    const transaction = yield transaction_model_1.TransactionModel.findOne(query).exec();
    if (lodash_1.isNull(transaction)) {
        logger_1.default.error(`Transaction not found: 
      ownerCustomerId: ${input.customerId},
      paymentInstructionId: ${input.paymentInstructionId},
      transactionDate: ${input.transactionDate}`);
        return;
    }
    return convertTransactionDocumentToObject(transaction);
});
const getPendingTransactionList = (paymentServiceCodes, minTransactionAge, maxTransactionAge, recordLimit) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.debug(`outOfBanTxAgeMax: ${minTransactionAge}`);
    logger_1.default.debug(`outOfBanTxAgeMin: ${maxTransactionAge}`);
    logger_1.default.debug(`outOfBanTxLimit: ${recordLimit}`);
    const transactionList = yield transaction_model_1.TransactionModel.find({
        paymentServiceCode: { $in: paymentServiceCodes },
        status: {
            $in: [
                transaction_enum_1.TransactionStatus.SUBMITTED,
                transaction_enum_1.TransactionStatus.PENDING,
                transaction_enum_1.TransactionStatus.SUBMITTING
            ]
        },
        createdAt: {
            $lte: moment_1.default
                .utc()
                .subtract(minTransactionAge, 'minutes')
                .toDate(),
            $gte: moment_1.default
                .utc()
                .subtract(maxTransactionAge, 'minutes')
                .toDate()
        }
    })
        .limit(recordLimit)
        .exec();
    return (transactionList && convertTransactionListDocumentToObject(transactionList));
});
const findTransactionByQuery = (query, projection) => __awaiter(void 0, void 0, void 0, function* () {
    const transactions = yield transaction_model_1.TransactionModel.find(query, projection).exec();
    return transactions && convertTransactionListDocumentToObject(transactions);
});
const createIfNotExist = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    const now = new Date();
    const newTransactionModel = new transaction_model_1.TransactionModel(Object.assign(Object.assign({}, transactionModel), { createdAt: now, updatedAt: now }));
    const result = yield transaction_model_1.TransactionModel.findOneAndUpdate({
        externalId: newTransactionModel.externalId,
        paymentServiceType: newTransactionModel.paymentServiceType
    }, { $setOnInsert: newTransactionModel }, { upsert: true, timestamps: false });
    // result == null means successful insertion
    if (result) {
        throw new AppError_1.TransferAppError(`Error in createIfNotExist: Duplicate transaction found with same externalId = ${transactionModel.externalId} and paymentServiceType = ${transactionModel.paymentServiceType}`, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.DUPLICATE_EXTERNAL_ID);
    }
    return convertTransactionDocumentToObject(newTransactionModel);
});
const transactionRepository = {
    getByKey,
    findOneByQuery,
    create,
    update,
    updateTransactionsByReferenceId,
    getByExternalId,
    updateIfExists,
    getIncomingNonRefunded,
    getAtomeTxnTobeRefunded,
    getTransactionListByCriteria,
    findByIdAndUpdate,
    getAllByExternalId,
    getTransactionStatusByCriteria,
    getByThirdPartyOutgoingId,
    getByQRISRefundIdentifier,
    getPendingTransactionList,
    findTransactionByQuery,
    createIfNotExist
};
exports.default = logger_1.wrapLogs(transactionRepository);
//# sourceMappingURL=transaction.repository.js.map