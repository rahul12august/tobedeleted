"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var RuleConditionState;
(function (RuleConditionState) {
    RuleConditionState["ACCEPTED"] = "ACCEPTED";
    RuleConditionState["DECLINED"] = "DECLINED";
    RuleConditionState["TRY_TOMORROW"] = "TRY_TOMORROW";
})(RuleConditionState = exports.RuleConditionState || (exports.RuleConditionState = {}));
//# sourceMappingURL=transaction.conditions.enum.js.map