"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This module is used to calculate payment service code and transaction fee.
 * Payment service code is recommended by payment service rule in business configuration
 */
const module_common_1 = require("@dk/module-common");
const award_repository_1 = __importDefault(require("../award/award.repository"));
const errors_1 = require("../common/errors");
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const configuration_service_1 = __importDefault(require("../configuration/configuration.service"));
const deposit_repository_1 = __importDefault(require("../coreBanking/deposit.repository"));
const decisionEngine_service_1 = __importDefault(require("../decisionEngine/decisionEngine.service"));
const transaction_enum_1 = require("./transaction.enum");
const AppError_1 = require("../errors/AppError");
const logger_1 = __importStar(require("../logger"));
const transaction_constant_1 = require("./transaction.constant");
const transaction_helper_1 = __importDefault(require("./transaction.helper"));
const transaction_producer_1 = __importDefault(require("./transaction.producer"));
const transaction_producer_enum_1 = require("./transaction.producer.enum");
const lodash_1 = require("lodash");
const big_js_1 = require("big.js");
const featureFlag_1 = require("../common/featureFlag");
const constant_1 = require("../common/constant");
const transactionUsageCommon_service_1 = __importDefault(require("../transactionUsage/transactionUsageCommon.service"));
const getTransactionConfiguration = (paymentServiceType, paymentServiceCode, customerId) => __awaiter(void 0, void 0, void 0, function* () {
    const [configRules, limitGroups] = yield Promise.all([
        configuration_service_1.default.getPaymentConfigRules(paymentServiceType, paymentServiceCode, customerId),
        configuration_repository_1.default.getLimitGroupsByCodes()
    ]);
    if (!configRules) {
        const detail = 'Invalid register parameter when no paymentConfig rules found!';
        logger_1.default.error(detail);
        throw new AppError_1.RetryableTransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED, true);
    }
    if (!limitGroups || limitGroups.length === 0) {
        const detail = 'Invalid register parameter when no limitGroupCodes found!';
        logger_1.default.error(detail);
        throw new AppError_1.RetryableTransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED, true);
    }
    if (customerId) {
        yield transactionUsageCommon_service_1.default.applyCustomLimitGroupsIfApplicable(customerId, limitGroups);
        yield configuration_service_1.default.applyCustomLimits(customerId, configRules);
    }
    return {
        limitGroupCodes: limitGroups,
        paymentConfigRules: configRules
    };
});
const validateInputAccountsAndCif = (transactionInput) => {
    if (transactionInput.paymentServiceType !==
        module_common_1.PaymentServiceTypeEnum.GENERAL_REFUND &&
        transactionInput.sourceAccountNo ===
            transactionInput.beneficiaryAccountNo &&
        transactionInput.beneficiaryCIF === transactionInput.sourceCIF) {
        const detail = `Error because sourceAccount equals beneficiaryAccount: ${transactionInput.sourceAccountNo} and source cif equals beneficiary cif: ${transactionInput.sourceCIF}!`;
        logger_1.default.error(`validateInputAccountsAndCif: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_ACCOUNT);
    }
};
const getCustomerUsageCounters = (customerId) => __awaiter(void 0, void 0, void 0, function* () {
    return customerId ? award_repository_1.default.getUsageCounters(customerId) : [];
});
const suggestRecommendationServices = (input, usageCounters) => __awaiter(void 0, void 0, void 0, function* () {
    const { paymentConfigRules: configRules, limitGroupCodes: limitGroups } = yield getTransactionConfiguration(input.paymentServiceType, input.paymentServiceCode, input.sourceCustomerId);
    const executionDate = new Date();
    const paymentServiceMappings = yield transaction_helper_1.default.getPaymentServiceMappings(input, configRules, limitGroups, usageCounters, executionDate);
    if (!paymentServiceMappings.length) {
        logger_1.default.info('no services code found, return decision: NO_RECOMMENDATION_SERVICES');
        throw new AppError_1.RetryableTransferAppError(`Services code not found!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.NO_RECOMMENDATION_SERVICES, false, [
            {
                message: errors_1.ErrorList[errors_1.ERROR_CODE.PAYMENT_CONFIG_RULES_EVALUATION_FAILED]
                    .message,
                key: errors_1.ERROR_CODE.PAYMENT_CONFIG_RULES_EVALUATION_FAILED,
                code: errors_1.ERROR_CODE.PAYMENT_CONFIG_RULES_EVALUATION_FAILED
            }
        ]);
    }
    // 4. build recommendation codes
    const bankCodes = {
        [transaction_enum_1.BankCodeTypes.RTOL]: input.beneficiaryRtolCode,
        [transaction_enum_1.BankCodeTypes.BILLER]: input.billerCode,
        [transaction_enum_1.BankCodeTypes.REMITTANCE]: input.beneficiaryRemittanceCode,
        [transaction_enum_1.BankCodeTypes.IRIS]: input.beneficiaryIrisCode
    };
    return paymentServiceMappings.map(item => {
        const beneficiaryBankCode = bankCodes[item.beneficiaryBankCodeType];
        const recommended = Object.assign(Object.assign({}, item), { beneficiaryBankCode });
        return recommended;
    });
});
const isStringGeneralRefundOrOfferType = (psCode) => {
    return (psCode === transaction_constant_1.PAYMENT_SERVICE_CODE_REFUND_GENERAL ||
        psCode === module_common_1.PaymentServiceTypeEnum.OFFER ||
        psCode === module_common_1.PaymentServiceTypeEnum.PAYROLL ||
        psCode === module_common_1.PaymentServiceTypeEnum.CASHBACK ||
        psCode === module_common_1.PaymentServiceTypeEnum.BONUS_INTEREST ||
        psCode === module_common_1.PaymentServiceTypeEnum.BRANCH_DEPOSIT ||
        psCode === module_common_1.PaymentServiceTypeEnum.TD_MANUAL_PAYMENT_GL);
};
const isGeneralRefundOrOfferType = (item) => !(item.debitTransactionCode && item.debitTransactionCode.length) &&
    item.creditTransactionCode &&
    item.creditTransactionCode.length &&
    isStringGeneralRefundOrOfferType(item.paymentServiceCode);
const isLoanTransaction = (paymentServiceType) => paymentServiceType === module_common_1.PaymentServiceTypeEnum.LOAN_REPAYMENT ||
    paymentServiceType === module_common_1.PaymentServiceTypeEnum.LOAN_DISBURSEMENT ||
    paymentServiceType === module_common_1.PaymentServiceTypeEnum.LOAN_DIRECT_DISBURSEMENT ||
    paymentServiceType === module_common_1.PaymentServiceTypeEnum.LOAN_REFUND ||
    paymentServiceType === module_common_1.PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY ||
    paymentServiceType === module_common_1.PaymentServiceTypeEnum.LOAN_GL_DISBURSEMENT ||
    paymentServiceType === module_common_1.PaymentServiceTypeEnum.LOAN_GL_REPAYMENT;
const constructNotEnoughBalanceMessage = (balance, forDebit, trxAmount, fee) => {
    let result = `Insufficient balance ${balance} < `;
    if (forDebit) {
        result = result.concat(trxAmount.toString());
    }
    if (fee > 0) {
        if (forDebit) {
            result = result.concat(' + ');
        }
        result = result.concat(`${fee} (fee)`);
    }
    return result;
};
/**
 * Tests if the input represents a debit transaction
 * @param item The recommendation service input
 * @returns True if the transaction is a debit, except for reversals
 */
const isDebit = (item) => {
    //
    // This may not be correct because each item can potentially contain both debitTransactionCode and
    // creditTransactionCode
    //
    if (!item.debitTransactionCode) {
        return false;
    }
    if (lodash_1.isEmpty(item.debitTransactionCode)) {
        return false;
    }
    return !item.debitTransactionCode[0].transactionCode.endsWith(transaction_constant_1.REVERSAL_TC_SUFFIX);
};
/**
 * Tests if the transaction request input denotes a transaction involving money being
 * debited from Jago bank.
 * @param input Transaction request input
 * @returns True if the transnsaction requests has Jago Bank (conventional or sharia) as
 * the source bank. If there's not information to decide, returns false.
 */
const amountIsDebitedFromJago = (input) => {
    if (input.sourceRtolCode &&
        (input.sourceRtolCode === "542" /* RTOL_CODE */ ||
            input.sourceRtolCode === "542" /* RTOL_CODE */)) {
        return true;
    }
    if (input.sourceRemittanceCode &&
        (input.sourceRemittanceCode === "ATOSIDJ1" /* REMITTANCE_CODE */ ||
            input.sourceRemittanceCode === "SYATIDJ1" /* REMITTANCE_CODE */)) {
        return true;
    }
    if (input.sourceBankCode &&
        (input.sourceBankCode == transaction_constant_1.JAGO_BANK_CODE ||
            input.sourceBankCode == transaction_constant_1.JAGO_SHARIA_BANK_CODE)) {
        return true;
    }
    logger_1.default.info(`Not checking balance, input is ${JSON.stringify(input)}`);
    return false;
};
/**
 * Picks up the payment service code from the array of recommendation services and associates it with
 * the transaction input in case the corresponding property is not populated yet. This is because in case
 * of failures there's visibility on the association between transaction request and the pscode determined
 * by the recommendation service algorithm.
 * @param input The transfer request
 * @param recommendedServices The list of recommendation services found by the recommendation service algorithm.
 */
const associatePaymentServiceCode = (input, recommendedServices) => {
    if (recommendedServices.length === 0) {
        //
        // The array must not be empty (previous steps do already throw exceptions when array is empty).
        // The condition here is added as extra measure.
        //
        throw new AppError_1.TransferAppError(`Services code not found!`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.NO_RECOMMENDATION_SERVICES);
    }
    if (!input.paymentServiceCode || input.paymentServiceCode === '') {
        let psCodes = recommendedServices
            .map(entry => entry.paymentServiceCode)
            .filter((value, index, arr) => {
            return arr.indexOf(value) === index;
        });
        if (psCodes.length > 1) {
            logger_1.default.warn(`Recommended services mapped to more than one pscode: ${psCodes}`);
        }
        input.paymentServiceCode = psCodes[0];
        logger_1.default.info(`Associated pscode ${input.paymentServiceCode}`);
    }
};
const isExceptedFromBalanceVerification = (psType) => {
    if (isLoanTransaction(psType)) {
        return true;
    }
    if (isStringGeneralRefundOrOfferType(psType)) {
        return true;
    }
    return (psType === module_common_1.PaymentServiceTypeEnum.MIGRATION_TRANSFER ||
        psType === module_common_1.PaymentServiceTypeEnum.SETTLEMENT ||
        psType === module_common_1.PaymentServiceTypeEnum.TD_SHARIA_REPAYMENT ||
        psType === module_common_1.PaymentServiceTypeEnum.LOAN_BULK_TAKEOVER_TRANSFER_REVERSAL ||
        psType === module_common_1.PaymentServiceTypeEnum.LOAN_FORWARD_PAYMENT_WITHDRAWAL);
};
const filterByAvailableBalance = (transactionInput, recommendedServices) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    logger_1.default.info(`getRecommendServices: filtered recommendedServices:
  ${JSON.stringify(recommendedServices)}`);
    const { sourceAccountInfo } = transactionInput, input = __rest(transactionInput, ["sourceAccountInfo"]);
    const balanceCanBeQueried = sourceAccountInfo && sourceAccountInfo.accountNumber && input.sourceCIF;
    if (!balanceCanBeQueried) {
        return recommendedServices;
    }
    const balanceShouldBeChecked = amountIsDebitedFromJago(input) &&
        !isExceptedFromBalanceVerification(input.paymentServiceType);
    if (!balanceShouldBeChecked) {
        return recommendedServices;
    }
    let sourceAccountBalance = 0;
    if ((_a = sourceAccountInfo) === null || _a === void 0 ? void 0 : _a.accountNumber) {
        sourceAccountBalance = yield deposit_repository_1.default.getAvailableBalance(sourceAccountInfo.accountNumber);
        logger_1.default.info(`Checking trx input against account: ${sourceAccountInfo.accountNumber}`);
    }
    recommendedServices = recommendedServices.filter(item => {
        const { feeData } = item;
        // ideally for 1 service code, fee amount is the same for multiple interchange, we take [0] by default
        const fee = (feeData && feeData[0] && feeData[0].feeAmount) || 0;
        const isDebitTrx = isDebit(item);
        // Maybe in the future to consider deducting the fee from the recently received credit.
        if (big_js_1.Big(isDebitTrx ? input.transactionAmount : 0)
            .add(big_js_1.Big(fee))
            .sub(big_js_1.Big(sourceAccountBalance)) > big_js_1.Big(0)) {
            const insufficientBalanceMsg = constructNotEnoughBalanceMessage(sourceAccountBalance, isDebitTrx, input.transactionAmount, fee);
            logger_1.default.info(`${insufficientBalanceMsg}, pscode: ${item.paymentServiceCode}`);
            return false;
        }
        return true;
    });
    return recommendedServices;
});
const filterByAvailableBalanceOriginal = (transactionInput, recommendedServices) => __awaiter(void 0, void 0, void 0, function* () {
    const { sourceAccountInfo } = transactionInput, input = __rest(transactionInput, ["sourceAccountInfo"]);
    const sourceAccountBalance = input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.OFFER &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.PAYROLL &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.CASHBACK &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.BRANCH_DEPOSIT &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.LOAN_DISBURSEMENT &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.SETTLEMENT &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.LOAN_GL_DISBURSEMENT &&
        input.paymentServiceType !==
            module_common_1.PaymentServiceTypeEnum.LOAN_DIRECT_DISBURSEMENT &&
        input.paymentServiceType !==
            module_common_1.PaymentServiceTypeEnum.LOAN_FORWARD_PAYMENT_WITHDRAWAL &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.LOAN_REPAYMENT &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.LOAN_GL_REPAYMENT &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.TD_SHARIA_REPAYMENT &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.BONUS_INTEREST &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.MIGRATION_TRANSFER &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.LOAN_REFUND &&
        input.paymentServiceType !==
            module_common_1.PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.TD_MANUAL_PAYMENT_GL &&
        sourceAccountInfo &&
        sourceAccountInfo.accountNumber &&
        input.sourceCIF
        ? yield deposit_repository_1.default.getAvailableBalance(sourceAccountInfo.accountNumber)
        : undefined;
    logger_1.default.info(`getRecommendServices: filtered recommendedServices: 
    ${JSON.stringify(recommendedServices)}`);
    const filtered = recommendedServices.filter(item => {
        const { feeData } = item;
        // ideally for 1 service code, fee amount is the same for multiple interchange, we take [0] by default
        const fee = (feeData && feeData[0] && feeData[0].feeAmount) || 0;
        if (!(item.debitTransactionCode &&
            !lodash_1.isEmpty(item.debitTransactionCode) &&
            !item.debitTransactionCode[0].transactionCode.endsWith(transaction_constant_1.REVERSAL_TC_SUFFIX) &&
            item.debitTransactionCode.length) &&
            fee === 0) {
            return true; // skip, only validate amount when having debit or fee transfer
        }
        if (isGeneralRefundOrOfferType(item)) {
            return true;
        }
        if (isLoanTransaction(input.paymentServiceType)) {
            return true;
        }
        if (!sourceAccountBalance ||
            big_js_1.Big(input.transactionAmount)
                .add(big_js_1.Big(fee))
                .sub(big_js_1.Big(sourceAccountBalance)) > big_js_1.Big(0)) {
            logger_1.default.info(`Transfer amount insufficient. Source acc balance: ${sourceAccountBalance}, ` +
                `input psType: ${input.paymentServiceType}, input amount: ${input.transactionAmount}, fee: ${fee}` +
                `item psCode: ${item.paymentServiceCode}`);
            return false;
        }
        return true;
    });
    return filtered;
});
/**
 * Get all possible recommend service codes of given input
 *
 * @param transactionInput the transaction input
 */
const getRecommendServices = (transactionInput) => __awaiter(void 0, void 0, void 0, function* () {
    validateInputAccountsAndCif(transactionInput);
    const { sourceAccountInfo } = transactionInput, input = __rest(transactionInput, ["sourceAccountInfo"]);
    //get all usage counters by customerId
    let usageCounters = yield getCustomerUsageCounters(transactionInput.sourceCustomerId);
    let recommendedServices = yield suggestRecommendationServices(transactionInput, usageCounters);
    transaction_helper_1.default.validateTransactionLimitAmount(input.transactionAmount, recommendedServices, transactionInput.interchange);
    recommendedServices = yield transaction_helper_1.default.mapFeeData(recommendedServices, transactionInput, usageCounters);
    associatePaymentServiceCode(transactionInput, recommendedServices);
    if (featureFlag_1.isFeatureEnabled(constant_1.FEATURE_FLAG.LEGACY_BALANCE_CHECK_KEY)) {
        recommendedServices = yield filterByAvailableBalanceOriginal(transactionInput, recommendedServices);
    }
    else {
        recommendedServices = yield filterByAvailableBalance(transactionInput, recommendedServices);
    }
    if (!recommendedServices.length) {
        logger_1.default.error('getRecommendServices: no recommendedServices because of insufficient account balance.');
        const isDebitCardTransaction = yield configuration_service_1.default.isDebitCardTransactionGroup(transactionInput.paymentServiceType);
        if (isDebitCardTransaction) {
            yield transaction_producer_1.default.sendFailedDebitCardTransactionAmount(transactionInput, transaction_producer_enum_1.NotificationCode.NOTIF_DEBIT_CARD_INSUFFICIENT_BALANCE);
        }
        throw new AppError_1.TransferAppError('Insufficient account balance.', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT);
    }
    if (transactionInput.executorCIF &&
        transactionInput.sourceCIF !== transactionInput.executorCIF) {
        for (const recommendedService of recommendedServices) {
            const decisionEngineConfig = yield configuration_repository_1.default.getDecisionEngineConfig(recommendedService.paymentServiceCode);
            if (!decisionEngineConfig) {
                const detail = `Decision engine config not found for paymentServiceCode: ${recommendedService.paymentServiceCode}!`;
                logger_1.default.error(detail);
                throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.DECISION_ENGINE_CONFIG_NOT_FOUND);
            }
            const resultRoles = decisionEngine_service_1.default.runMultipleRoles(decisionEngineConfig, transactionInput);
            logger_1.default.info('DC Engine result: ', resultRoles);
            if (!resultRoles) {
                const detail = 'Failure in multiple decision config roles!';
                logger_1.default.error(detail);
                throw new AppError_1.RetryableTransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.NO_RECOMMENDATION_SERVICES, false);
            }
        }
    }
    return recommendedServices; // already throw error in case there's no recommend
});
/**
 * Get only to recommend service that matched with given paymentServiceCode from the input
 * if input is missing paymentServiceCode, get the first one found
 * Throw exception if there is not qualified recommend code
 * @param transactionInput the transaction input
 */
const getRecommendService = (transactionInput) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const recommendedServices = yield getRecommendServices(transactionInput);
        let recommendedService;
        if (transactionInput.paymentServiceCode) {
            recommendedService = recommendedServices.find(recommendedService => recommendedService.paymentServiceCode ==
                transactionInput.paymentServiceCode);
            if (!recommendedService) {
                const detail = 'No recommended service found!';
                logger_1.default.error(`getRecommendService: ${detail}`);
                throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.INCONSISTENT_PAYMENT_SERVICE);
            }
        }
        else {
            recommendedService = recommendedServices[0];
        }
        return recommendedService;
    }
    catch (err) {
        logger_1.default.error(`getRecommendService: Error in recommendation services journey: ${JSON.stringify(transactionInput.journey)}`);
        throw err;
    }
});
const getRecommendedServiceWithoutFeeData = (transactionInput, usageCounters) => __awaiter(void 0, void 0, void 0, function* () {
    validateInputAccountsAndCif(transactionInput);
    if (!transactionInput.paymentServiceCode) {
        const detail = `Error because Payment Service Code was not specified in the input`;
        logger_1.default.error(`getRecommendedServiceWithoutFeeData: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.PAYMENT_SERVICE_CODE_NOT_FOUND);
    }
    let recommendedServices = yield suggestRecommendationServices(transactionInput, usageCounters);
    const selectedService = recommendedServices.find(recommendedService => recommendedService.paymentServiceCode ==
        transactionInput.paymentServiceCode);
    if (!selectedService) {
        const detail = 'No recommended service found!';
        logger_1.default.error(`getRecommendedServiceWithoutFeeData: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.INCONSISTENT_PAYMENT_SERVICE);
    }
    return selectedService;
});
const recommendedCodeService = logger_1.wrapLogs({
    getRecommendServices,
    getRecommendService,
    getRecommendedServiceWithoutFeeData,
    getCustomerUsageCounters
});
exports.default = recommendedCodeService;
//# sourceMappingURL=transactionRecommend.service.js.map