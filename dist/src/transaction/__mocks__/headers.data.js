"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tokenAuth_data_1 = require("../../plugins/__mocks__/tokenAuth.data");
const constant_1 = require("../../common/constant");
exports.invalidHeader = {
    Authorization: `Basic ${tokenAuth_data_1.mockToken}`
};
exports.validHeader = {
    Authorization: `${constant_1.PREFIX_TOKEN}${tokenAuth_data_1.mockToken}`
};
//# sourceMappingURL=headers.data.js.map