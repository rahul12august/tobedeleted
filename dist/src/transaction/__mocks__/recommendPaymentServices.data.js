"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_data_1 = require("./transaction.data");
const module_common_1 = require("@dk/module-common");
const module_common_2 = require("@dk/module-common");
const configuration_data_1 = require("../../configuration/__mocks__/configuration.data");
const bifast_constant_1 = require("../../bifast/bifast.constant");
exports.createTransaction = () => {
    const transaction = Object.assign(Object.assign({}, transaction_data_1.getITransactionInput()), { beneficiaryAccountName: 'invalid', beneficiaryBankCode: module_common_2.Constant.GIN_BANK_CODE_ID, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryBankName: 'bank', sourceAccountName: 'invalid', sourceAccountType: 'invalid', sourceBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, sourceBankCode: module_common_2.Constant.GIN_BANK_CODE_ID, sourceTransactionCurrency: 'IDR', sourceCustomerId: 'sourceCustomerId' });
    return transaction;
};
exports.getPaymentServiceMappingData = () => ({
    paymentServiceCode: 'SIT02',
    debitTransactionCode: [
        {
            transactionCode: 'WTD01',
            transactionCodeInfo: Object.assign(Object.assign({}, configuration_data_1.getTransactionCodeInfoData()), { code: 'WTD01' })
        }
    ],
    creditTransactionCode: [
        {
            transactionCode: 'WTC01',
            transactionCodeInfo: Object.assign(Object.assign({}, configuration_data_1.getTransactionCodeInfoData()), { code: 'WTD01' })
        }
    ],
    beneficiaryBankCodeType: 'RTOL',
    transactionAuthenticationChecking: true,
    blocking: false,
    requireThirdPartyOutgoingId: false
});
exports.getRecommendPaymentServiceTestCases = () => {
    return [
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.WALLET, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.PARTNER, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 7000000, externalId: 'dummy123' }),
            output: ['SIT02']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.WALLET, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.PARTNER, sourceAccountType: 'DC', beneficiaryAccountType: 'ANY', transactionAmount: 7000000, externalId: 'dummy123' }),
            output: ['SIT02']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'ANY', sourceAccountType: 'MA', beneficiaryCIF: '123', sourceCIF: '1234', transactionAmount: 7000000, externalId: 'dummy123', additionalInformation1: '1', additionalInformation2: '1', additionalInformation3: '01', additionalInformation4: 'test@jago.com' }),
            output: ['SIT01']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, sourceAccountType: 'DC', beneficiaryAccountType: 'ANY', beneficiaryCIF: '123', sourceCIF: '1234', transactionAmount: 7000000, externalId: 'dummy123' }),
            output: ['SIT01']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'FS', sourceAccountType: 'MA', beneficiaryCIF: '123', sourceCIF: '123', transactionAmount: 7000000 }),
            output: ['SAT01']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'FS', sourceAccountType: 'DC', beneficiaryCIF: '123', sourceCIF: '123', transactionAmount: 7000000 }),
            output: ['SAT01']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'MA', sourceAccountType: 'FS', beneficiaryCIF: '123', sourceCIF: '123', transactionAmount: 7000000 }),
            output: ['SAT02']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'DC', sourceAccountType: 'FS', beneficiaryCIF: '123', sourceCIF: '123', transactionAmount: 7000000 }),
            output: ['SAT02']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'DC', sourceAccountType: 'MA', beneficiaryCIF: '123', sourceCIF: '123', transactionAmount: 7000000 }),
            output: ['SAT03']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'MA', sourceAccountType: 'DC', beneficiaryCIF: '123', sourceCIF: '123', transactionAmount: 7000000 }),
            output: ['SAT04']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 110000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['BIFAST_OUTGOING']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'SMA', beneficiaryAccountType: 'ANY', transactionAmount: 110000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['BIFAST_OUTGOING_SHARIA']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 120000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 700000000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 130000000,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['BIFAST_OUTGOING']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'SMA', beneficiaryAccountType: 'ANY', transactionAmount: 120000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 700000000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 130000000,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['BIFAST_OUTGOING_SHARIA']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 60000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 70000000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['BIFAST_OUTGOING']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'SMA', beneficiaryAccountType: 'ANY', transactionAmount: 60000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 70000000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['BIFAST_OUTGOING_SHARIA']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 600000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 100000000,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['RTGS']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'SMA', beneficiaryAccountType: 'ANY', transactionAmount: 500000001 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 100000000,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['RTGS_SHARIA']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.GOBILLS, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY' }),
            output: ['BIL01']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'FS', sourceAccountType: 'FS', beneficiaryCIF: '123', sourceCIF: '123', transactionAmount: 7000000 }),
            output: ['SAT05']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'DC', beneficiaryAccountType: 'ANY', transactionAmount: 30000000 }),
            output: ['BIFAST_OUTGOING']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 30000000 }),
            output: ['BIFAST_OUTGOING']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'FS', sourceAccountType: 'FS', beneficiaryCIF: '1234', sourceCIF: '1234', transactionAmount: 30000000 }),
            output: ['SAT05']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.GIN_PAY, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'MA', sourceAccountType: 'MA', beneficiaryCIF: '123', sourceCIF: '1234', transactionAmount: 30000000 }),
            output: ['SIT03']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.GIN_PAY, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'MA', sourceAccountType: 'DC', beneficiaryCIF: '123', sourceCIF: '1234', transactionAmount: 30000000 }),
            output: ['SIT03']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.MDR, beneficiaryAccountType: 'ANY', sourceAccountType: 'MA', transactionAmount: 30000000 }),
            output: ['MDR']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.CASHBACK, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, sourceAccountType: 'ANY', beneficiaryAccountType: 'MA', transactionAmount: 9000000 }),
            dailyUsage: [],
            output: ['CASHBACK']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.WALLET, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 5000000 }),
            output: ['RTOL_WALLET']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.PAYME, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', beneficiaryCIF: '123', sourceCIF: '1234', transactionAmount: 5000000 }),
            output: ['PAYME']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.CREDIT_CARD, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 5000000 }),
            output: ['RTOL_CC']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], sourceCIF: undefined, paymentServiceType: module_common_1.PaymentServiceTypeEnum.WALLET, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.PARTNER, sourceAccountType: 'ANY', transactionAmount: 9000000 }),
            output: ['VA_TOPUP_EXTERNAL']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], sourceCIF: '123', paymentServiceType: module_common_1.PaymentServiceTypeEnum.WALLET, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, sourceAccountType: 'ANY', beneficiaryAccountType: 'MA', transactionAmount: 9000000 }),
            output: ['VA_WITHDRAW']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TD_SHARIA_PLACEMENT, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', beneficiaryCIF: '123', sourceCIF: '123', transactionAmount: 7000000 }),
            output: ['SAT08']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TD_SHARIA_REPAYMENT, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'MA', beneficiaryCIF: '123', sourceCIF: '123', sourceAccountType: 'ANY', transactionAmount: 7000000 }),
            output: ['SAT09']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, beneficiaryBankCode: 'BC005', beneficiaryCIF: '123', sourceCIF: '123', sourceAccountType: 'MA', transactionAmount: 7000000 }),
            output: ['BIFAST_OUTGOING']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, beneficiaryBankCode: 'BC005', beneficiaryCIF: '123', sourceCIF: '123', sourceAccountType: 'SMA', transactionAmount: 7000000 }),
            output: ['BIFAST_OUTGOING_SHARIA']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { paymentServiceType: module_common_1.PaymentServiceTypeEnum.INCOMING_BIFAST, sourceBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'MA', beneficiaryCIF: '123', sourceCIF: '123', transactionAmount: 7000000 }),
            output: ['BIFAST_INCOMING']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { paymentServiceType: module_common_1.PaymentServiceTypeEnum.INCOMING_BIFAST, sourceBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'SMA', beneficiaryCIF: '123', sourceCIF: '123', transactionAmount: 7000000 }),
            output: ['BIFAST_INCOMING_SHARIA']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.VOID_BIFAST, sourceBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'SMA', beneficiaryCIF: '123', sourceCIF: '123', transactionAmount: 7000000 }),
            output: ['REFUND_GENERAL']
        }
    ];
};
exports.getNonBiFastRecommendPaymentServiceTestCases = () => {
    return [
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiaryBankCode: bifast_constant_1.BIFAST_PROXY_BANK_CODE, beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 110000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: []
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 110000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['SKN', 'RTGS']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'SMA', beneficiaryAccountType: 'ANY', transactionAmount: 110000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['SKN_SHARIA']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 120000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 700000000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 130000000,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['RTGS']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'SMA', beneficiaryAccountType: 'ANY', transactionAmount: 500000003 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 130000000,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['RTGS_SHARIA']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 60000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 70000000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['SKN']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'SMA', beneficiaryAccountType: 'ANY', transactionAmount: 100000002 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['SKN_SHARIA']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.GOBILLS, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY' }),
            output: ['BIL01']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryAccountType: 'FS', sourceAccountType: 'FS', beneficiaryCIF: '123', sourceCIF: '123', transactionAmount: 7000000 }),
            output: ['SAT05']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'DC', beneficiaryAccountType: 'ANY', transactionAmount: 30000000 }),
            output: ['RTOL']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 30000000 }),
            output: ['RTOL']
        }
    ];
};
const getBiFastByAdditionalInformationRecommendPaymentServiceTestCases = () => {
    return [
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiaryBankCode: 'BC005', additionalInformation3: '02', additionalInformation4: 'test@email.com', beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 110000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['BIFAST_OUTGOING']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiaryBankCode: 'BC005', additionalInformation3: '01', additionalInformation4: '+1234567890', beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 250000001 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 10
                }
            ],
            output: []
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiaryBankCode: 'BC005', additionalInformation3: '00', additionalInformation4: '123123123123', beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 25000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 250000000,
                    monthlyAccumulationTransaction: 10
                }
            ],
            output: ['RTOL']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiaryBankCode: 'BC005', additionalInformation3: '02', additionalInformation4: 'test@email.com', beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 110000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 250000000,
                    monthlyAccumulationTransaction: 10
                }
            ],
            output: []
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiaryBankCode: 'BC005', additionalInformation3: '', additionalInformation4: '', beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 110000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 100000000,
                    monthlyAccumulationTransaction: 10
                }
            ],
            output: ['BIFAST_OUTGOING']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiaryBankCode: 'BC005', beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 50000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 250000000,
                    monthlyAccumulationTransaction: 10
                }
            ],
            output: ['RTOL']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiaryBankCode: 'BC005', beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 100000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 250000000,
                    monthlyAccumulationTransaction: 10
                }
            ],
            output: ['SKN']
        }
    ];
};
const getBiFastProxyRecommendPaymentServiceTestCases = () => {
    return [
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiaryBankCode: bifast_constant_1.BIFAST_PROXY_BANK_CODE, beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 110000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['BIFAST_OUTGOING']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiaryBankCode: bifast_constant_1.BIFAST_PROXY_BANK_CODE, beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 250000001 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 10
                }
            ],
            output: []
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiaryBankCode: 'BC005', additionalInformation3: '00', additionalInformation4: '123123123123', beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 25000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 250000000,
                    monthlyAccumulationTransaction: 10
                }
            ],
            output: ['RTOL']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiaryBankCode: bifast_constant_1.BIFAST_PROXY_BANK_CODE, beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 110000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 250000000,
                    monthlyAccumulationTransaction: 10
                }
            ],
            output: []
        }
    ];
};
exports.getBiFastRecommendPaymentServiceTestCases = () => {
    return [
        ...getBiFastByAdditionalInformationRecommendPaymentServiceTestCases(),
        ...getBiFastProxyRecommendPaymentServiceTestCases()
    ];
};
exports.getAutoRouteRecommendPaymentServiceTestCases = () => {
    return [
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 25000, beneficiaryBankCode: 'BC005', additionalInformation3: '00', additionalInformation4: '123123123123', preferedBankChannel: module_common_1.BankChannelEnum.EXTERNAL }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 10
                }
            ],
            output: ['RTOL']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 250000001, beneficiaryBankCode: 'BC005', additionalInformation3: '00', additionalInformation4: '123123123123', preferedBankChannel: module_common_1.BankChannelEnum.EXTERNAL }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['SKN', 'RTGS']
        },
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiaryBankCode: bifast_constant_1.BIFAST_PROXY_BANK_CODE, beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'MA', beneficiaryAccountType: 'ANY', transactionAmount: 25000, preferedBankChannel: module_common_1.BankChannelEnum.EXTERNAL }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 10
                }
            ],
            output: []
        }
    ];
};
exports.getRDNRecommendPaymentServiceTestCases = () => {
    return [
        {
            input: Object.assign(Object.assign({}, exports.createTransaction()), { beneficiaryBankCode: 'BC006', beneficiarySupportedChannels: [
                    module_common_1.BankChannelEnum.BIFAST,
                    module_common_1.BankChannelEnum.EXTERNAL
                ], paymentServiceType: module_common_1.PaymentServiceTypeEnum.RDN, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.EXTERNAL, sourceAccountType: 'RDS', beneficiaryAccountType: 'ANY', transactionAmount: 110000000 }),
            dailyUsage: [
                {
                    limitGroupCode: 'L002',
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L003',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L004',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                },
                {
                    limitGroupCode: 'L019',
                    dailyAccumulationAmount: 10000,
                    monthlyAccumulationTransaction: 0
                }
            ],
            output: ['SKN_SHARIA']
        }
    ];
};
//# sourceMappingURL=recommendPaymentServices.data.js.map