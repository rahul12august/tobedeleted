"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_enum_1 = require("../transaction.enum");
const module_common_1 = require("@dk/module-common");
const transaction_enum_2 = require("../transaction.enum");
exports.mockConfirmTransactionRequest = () => {
    return {
        externalId: '0093384848',
        accountNo: '100023993999400000',
        transactionDate: '200212092500',
        responseMessage: 'OK',
        transactionResponseID: '3499488888',
        responseCode: transaction_enum_1.ConfirmTransactionStatus.SUCCESS,
        interchange: module_common_1.BankNetworkEnum.ARTAJASA
    };
};
exports.mockTransactionFromDb = () => {
    return {
        externalId: 'externalId',
        referenceId: '34839845995-3093059-03434',
        beneficiaryAccountName: 'External Account Name From Switching',
        beneficiaryBankCode: '542',
        sourceAccountName: 'Jago account name',
        sourceBankCode: 'BC002',
        createdAt: new Date('2020-02-12T09:25:00.041Z'),
        status: transaction_enum_1.TransactionStatus.SUBMITTED,
        paymentServiceCode: 'RTOL',
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
        transactionAmount: 900000,
        id: '5df72fa3490b56067991ae39',
        executionType: transaction_enum_2.ExecutionTypeEnum.NONE_BLOCKING,
        requireThirdPartyOutgoingId: true,
        entitlementInfo: [
            {
                entitlementCodeRefId: 'entitlementCodeRefId',
                counterCode: 'counterCode',
                customerId: 'customerId'
            }
        ]
    };
};
//# sourceMappingURL=transactionConfirmation.data.js.map