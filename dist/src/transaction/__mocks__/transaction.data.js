"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const faker_1 = __importDefault(require("faker"));
const module_common_1 = require("@dk/module-common");
const configuration_data_1 = require("../../configuration/__mocks__/configuration.data");
const transaction_enum_1 = require("../transaction.enum");
const fee_data_1 = require("../../fee/__mocks__/fee.data");
const transaction_enum_2 = require("../transaction.enum");
const lodash_1 = require("lodash");
const sourceAccountNo = '2';
const beneficiaryAccountNo = '1';
exports.getTransactionModelData = () => {
    return {
        sourceCIF: faker_1.default.random.alphaNumeric(40),
        sourceAccountType: faker_1.default.finance.accountName(),
        sourceAccountNo: faker_1.default.finance.account(20),
        sourceBankCode: faker_1.default.random.alphaNumeric(4),
        sourceAccountName: faker_1.default.finance.accountName(),
        sourceTransactionCurrency: faker_1.default.finance.currencyCode(),
        transactionAmount: faker_1.default.random.number({ min: 1, max: 100000 }),
        beneficiaryAccountName: faker_1.default.finance.accountName(),
        beneficiaryCIF: faker_1.default.random.alphaNumeric(40),
        beneficiaryBankCode: faker_1.default.random.alphaNumeric(4),
        beneficiaryAccountNo: faker_1.default.finance.account(20),
        beneficiaryBankName: faker_1.default.company.companyName(),
        beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL,
        categoryCode: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.GIN_PAY,
        paymentServiceCode: faker_1.default.random.alphaNumeric(4),
        debitTransactionCode: faker_1.default.random.alphaNumeric(4),
        creditTransactionCode: faker_1.default.random.alphaNumeric(4),
        debitTransactionChannel: faker_1.default.random.alphaNumeric(4),
        creditTransactionChannel: faker_1.default.random.alphaNumeric(4),
        status: transaction_enum_1.TransactionStatus.PENDING,
        referenceId: faker_1.default.random.alphaNumeric(20),
        externalId: faker_1.default.random.alphaNumeric(20),
        thirdPartyOutgoingId: faker_1.default.random.alphaNumeric(20),
        executionType: transaction_enum_2.ExecutionTypeEnum.NONE_BLOCKING,
        requireThirdPartyOutgoingId: false,
        additionalInformation1: faker_1.default.finance.account(20),
        additionalInformation2: faker_1.default.finance.account(20)
    };
};
exports.getTransactionModelFullData = () => {
    return Object.assign(Object.assign({ paymentRequestID: faker_1.default.random.uuid(), beneficiaryAccountType: faker_1.default.finance.accountName(), beneficiaryAccountName: faker_1.default.finance.accountName(), creditPriority: module_common_1.BankNetworkEnum.INTERNAL, debitPriority: module_common_1.BankNetworkEnum.ARTAJASA, institutionalType: module_common_1.InstitutionTypeEnum.BANK, note: faker_1.default.random.alphaNumeric(10), beneficiaryRtolCode: faker_1.default.random.alphaNumeric(10), beneficiaryRemittanceCode: faker_1.default.random.alphaNumeric(10), billerCode: faker_1.default.random.alphaNumeric(10), extendedMessage: faker_1.default.random.alphaNumeric(10), beneficiaryBankName: faker_1.default.company.companyName(), fees: [
            {
                feeCode: faker_1.default.random.alphaNumeric(4),
                customerTc: faker_1.default.random.alphaNumeric(4),
                customerTcChannel: faker_1.default.random.alphaNumeric(4),
                feeAmount: faker_1.default.random.number({ min: 0, max: 5000 })
            }
        ] }, exports.getTransactionModelData()), { sourceCustomerId: 'customer123' });
};
exports.getRecommendPaymentServiceInput = () => ({
    sourceAccountNo: '2',
    sourceCIF: '1',
    beneficiaryAccountNo: '1',
    beneficiaryBankCode: module_common_1.Constant.GIN_BANK_CODE_ID,
    beneficiaryCIF: faker_1.default.random.alphaNumeric(10),
    transactionAmount: faker_1.default.random.number({
        min: 1,
        max: 100
    }),
    paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER
});
exports.getBankCode = () => ({
    bankCodeId: module_common_1.Constant.GIN_BANK_CODE_ID,
    name: 'GIN',
    rtolCode: '542',
    remittanceCode: 'ATOSIDJ1',
    isBersamaMember: false,
    isAltoMember: false,
    firstPriority: module_common_1.BankNetworkEnum.INTERNAL,
    channel: module_common_1.BankChannelEnum.INTERNAL,
    companyName: 'PT. GIN INDONESIA',
    type: 'BANK',
    prefix: undefined,
    isInternal: true
});
exports.getExternalBankCode = () => ({
    bankCodeId: 'BC077',
    name: 'Bank of China',
    rtolCode: '069',
    remittanceCode: 'BKCHIDJA',
    isBersamaMember: true,
    isAltoMember: false,
    firstPriority: module_common_1.BankNetworkEnum.ARTAJASA,
    channel: module_common_1.BankChannelEnum.EXTERNAL,
    companyName: 'BANK OF CHINA, CO., LTD.',
    type: 'BANK',
    prefix: undefined,
    isInternal: false
});
exports.getBiFastBankCode = () => ({
    bankCodeId: 'BC000',
    name: 'BIFAST',
    companyName: 'Bank Indonesia',
    isBersamaMember: false,
    isAltoMember: false,
    channel: module_common_1.BankChannelEnum.EXTERNAL,
    type: module_common_1.InstitutionTypeEnum.BANK,
    isInternal: false
});
exports.getWalletPartnerBankCode = () => ({
    bankCodeId: 'BC004',
    name: 'GoPay (Internal)',
    rtolCode: '542',
    isBersamaMember: false,
    isAltoMember: false,
    firstPriority: module_common_1.BankNetworkEnum.INTERNAL,
    channel: module_common_1.BankChannelEnum.PARTNER,
    companyName: 'PT. GIN INDONESIA',
    type: 'WALLET',
    prefix: '9000',
    isInternal: true
});
exports.getWalletExternalBankCode = () => ({
    bankCodeId: 'BC171',
    name: 'OVO',
    rtolCode: '503',
    isBersamaMember: true,
    isAltoMember: true,
    firstPriority: module_common_1.BankNetworkEnum.ARTAJASA,
    channel: module_common_1.BankChannelEnum.EXTERNAL,
    companyName: 'PT. VISIONET INTERNASIONAL',
    type: 'WALLET',
    prefix: '9',
    isInternal: false
});
exports.getTransactionCategory = () => ({
    code: 'C001',
    isIncoming: false,
    isOutgoing: true
});
exports.createConfigRule = () => {
    return {
        source: module_common_1.BankChannelEnum.INTERNAL,
        target: module_common_1.BankChannelEnum.INTERNAL,
        sourceAccType: ['ANY-TEST'],
        targetAccType: ['ANY-TEST'],
        sameCIF: faker_1.default.random.arrayElement(['YES', 'NO', 'ANY']),
        amountRangeFrom: faker_1.default.random.number(0),
        amountRangeTo: faker_1.default.random.number(10000000),
        rtolDailyUsage: faker_1.default.random.arrayElement(['YES', 'NO', 'ANY']),
        sknDailyUsage: faker_1.default.random.arrayElement(['YES', 'NO', 'ANY']),
        rtgsDailyUsage: faker_1.default.random.arrayElement(['YES', 'NO', 'ANY']),
        serviceRecommendation: [faker_1.default.random.alphaNumeric(5)],
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
        paymentServiceMappings: []
    };
};
exports.getITransferTransactionInput = (paymentServiceCode = 'p01') => {
    return Object.assign(Object.assign({}, exports.getRecommendPaymentServiceInput()), { sourceCustomerId: 'source_customer_id', sourceAccountNo, sourceCIF: '2', sourceBankCode: module_common_1.Constant.GIN_BANK_CODE_ID, beneficiaryCIF: '1', beneficiaryBankCode: module_common_1.Constant.GIN_BANK_CODE_ID, beneficiaryAccountNo,
        paymentServiceCode, paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, externalId: faker_1.default.random.alphaNumeric(20), additionalInformation1: faker_1.default.random.alphaNumeric(20), additionalInformation2: faker_1.default.random.alphaNumeric(20) });
};
exports.createReverseInput = () => {
    return {
        notes: 'notes'
    };
};
exports.getTransactionDetail = () => {
    return {
        id: faker_1.default.random.alphaNumeric(24),
        sourceCIF: '8a8e869a6dfc7ad8016dfd07dc4404c5',
        sourceAccountNo: '1079253293',
        transactionAmount: 2333,
        beneficiaryAccountNo: '30',
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCode: module_common_1.Constant.GIN_BANK_CODE_ID,
        beneficiaryCIF: '8a8e871c6d906f31016d90f1bc8f00d7',
        paymentServiceCode: 'SIT03',
        sourceTransactionCurrency: 'IDR',
        beneficiaryAccountType: 'MA',
        sourceAccountType: 'MA',
        sourceAccountName: 'DGB-1740-Main-2',
        sourceBankCode: module_common_1.Constant.GIN_BANK_CODE_ID,
        sourceRemittanceCode: 'ATOSIDJ1',
        beneficiaryRemittanceCode: 'CENAIDJA',
        beneficiaryAccountName: 'MA Account',
        beneficiaryBankName: 'PT. GIN INDONESIA',
        beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL,
        debitPriority: module_common_1.BankNetworkEnum.INTERNAL,
        creditPriority: module_common_1.BankNetworkEnum.ARTAJASA,
        status: transaction_enum_1.TransactionStatus.SUCCEED,
        categoryCode: 'C056',
        externalId: 'externalId',
        referenceId: 'referenceId',
        fees: [
            {
                feeCode: 'feeCode',
                feeAmount: 3500,
                customerTc: 'customerTc',
                customerTcChannel: 'customerTcChannel'
            }
        ],
        executionType: transaction_enum_2.ExecutionTypeEnum.NONE_BLOCKING,
        requireThirdPartyOutgoingId: false,
        createdAt: new Date('01 June 2020 00:00:00'),
        thirdPartyOutgoingId: '123456',
        sourceRtolCode: '542',
        beneficiaryRtolCode: '542'
    };
};
exports.getTransactionDetailWithFeeAmount = (transactionResult, payload) => ({
    externalId: payload.externalId,
    transactionId: transactionResult.id,
    status: transaction_enum_1.PaymentRequestStatus.SUCCEED,
    transactionAmount: transactionResult.transactionAmount,
    feeAmount: lodash_1.sumBy(transactionResult.fees, 'feeAmount'),
    extra: {
        additionalInformation1: payload.additionalInformation1,
        additionalInformation2: payload.additionalInformation2,
        additionalInformation3: payload.additionalInformation3,
        additionalInformation4: payload.additionalInformation4,
        sourceCIF: payload.sourceCIF
    }
});
exports.getGoBillsTxnData = () => {
    return {
        coreBankingTransactionIds: [],
        requireThirdPartyOutgoingId: true,
        sourceCIF: '8a85c07073e81d7e0174001f2b3d302f',
        sourceBankCode: 'BC002',
        sourceAccountNo: '100813848268',
        transactionAmount: 80000,
        beneficiaryBankCode: 'BC151',
        beneficiaryAccountNo: '3700327266',
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
        externalId: '89913055',
        note: 'A004',
        sourceTransactionCurrency: 'IDR',
        sourceAccountType: 'MA',
        sourceAccountName: 'Sarinem',
        sourceCustomerId: '1358473342',
        sourceBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL,
        sourceRtolCode: '542',
        creditPriority: module_common_1.BankNetworkEnum.INTERNAL,
        beneficiaryBankName: 'Home Credit Indonesia',
        beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.GOBILLS,
        debitPriority: module_common_1.BankNetworkEnum.GOBILLS,
        beneficiaryRtolCode: undefined,
        beneficiaryRemittanceCode: undefined,
        billerCode: 'ADR01',
        beneficiaryIrisCode: undefined,
        executorCIF: '8a85c07073e81d7e0174001f2b3d302f',
        referenceId: '6ded2c70-1330-11eb-b188-715b936ad7d8',
        paymentServiceCode: 'BIL01',
        beneficiaryRealBankCode: 'ADR01',
        status: transaction_enum_1.TransactionStatus.SUCCEED,
        executionType: transaction_enum_2.ExecutionTypeEnum.BLOCKING,
        creditTransactionCodeMapping: [],
        debitTransactionCode: 'BID01',
        debitTransactionChannel: 'BID01',
        debitLimitGroupCode: 'L009',
        fees: [
            {
                feeCode: 'BC151',
                feeAmount: 0,
                customerTc: 'FBD01',
                subsidiaryAmount: 0,
                debitGlNumber: '',
                creditGlNumber: '',
                subsidiary: false,
                customerTcChannel: 'FBD01'
            }
        ],
        categoryCode: '',
        thirdPartyOutgoingId: '89913055',
        reason: [
            {
                responseCode: 200,
                transactionDate: '201021120028',
                responseMessage: ''
            }
        ],
        blockingId: '5f8f7a1dbf793a121a26a27f',
        cardId: 'BLOCK_100813848268',
        additionalInformation1: '70357456',
        additionalInformation2: '10321213131331',
        id: '5f8f7a1c481f6a70b636bc85',
        createdAt: new Date('01 June 2020 00:00:00')
    };
};
exports.getBillDetailsData = () => {
    return {
        referenceId: '89913055',
        requestOrigin: 'PAYMENT_INSTRUCTION',
        customerId: '1358473342',
        billingAggregator: 'GOBILLS',
        billerCode: 'HCI01',
        bankCode: 'BC151',
        primaryBillKey: {
            key: module_common_1.BillKeyType.CUSTOMER_ID,
            value: '3700327266'
        },
        additionBillKeys: [],
        mappedBillInformation: [
            {
                rowType: 'CUSTOMER_INFO',
                outputName: 'CUSTOMER_NAME',
                value: 'Chadrick Hickle'
            },
            {
                rowType: 'DUE_DATE',
                outputName: 'DUE_DATE',
                value: ''
            },
            {
                rowType: 'BILL_INFO',
                outputName: 'PAY_TYPE',
                value: ''
            },
            {
                rowType: 'BILL_INFO',
                outputName: 'REFERENCE',
                value: ''
            },
            {
                rowType: 'PARTNER_AMOUNT',
                outputName: 'BA_AMOUNT',
                value: '80000'
            },
            {
                rowType: 'ADMIN_FEE',
                outputName: 'ADMIN_FEE',
                value: '3500'
            }
        ],
        transactionId: '5f8f7a1c481f6a70b636bc85',
        billerName: 'HCI',
        inquiryParams: {
            skuCode: 'HOME_CREDIT'
        }
    };
};
exports.getITransactionInput = (paymentServiceCode = 'p01') => {
    const input = Object.assign(Object.assign({}, exports.getRecommendPaymentServiceInput()), { sourceAccountNo, sourceCIF: '1', sourceBankCode: module_common_1.Constant.GIN_BANK_CODE_ID, beneficiaryCIF: '1', beneficiaryBankCode: module_common_1.Constant.GIN_BANK_CODE_ID, beneficiaryAccountNo,
        paymentServiceCode, paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER, externalId: faker_1.default.random.alphaNumeric(20), sourceBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL });
    return input;
};
exports.getRecommendedService = () => {
    const recommendationService = {
        paymentServiceCode: '1',
        beneficiaryBankCodeType: transaction_enum_1.BankCodeTypes.RTOL,
        transactionAuthenticationChecking: true,
        debitTransactionCode: [
            {
                transactionCode: 'd01',
                transactionCodeInfo: {
                    code: 'd01',
                    channel: 'd01_channel',
                    defaultCategoryCode: 'defaultD01',
                    minAmountPerTx: 100,
                    maxAmountPerTx: 9999999,
                    limitGroupCode: 'd01'
                }
            }
        ],
        creditTransactionCode: [
            {
                transactionCode: 'c01',
                transactionCodeInfo: {
                    code: 'c01',
                    channel: 'c01_channel',
                    defaultCategoryCode: 'defaultC01',
                    minAmountPerTx: 100,
                    maxAmountPerTx: 9999999,
                    limitGroupCode: 'c01'
                }
            }
        ],
        beneficiaryBankCode: '1',
        feeData: [
            Object.assign(Object.assign({}, fee_data_1.getFeeAmountData()), { feeAmount: 100, customerTc: 'customerTc', customerTcChannel: 'customerTcChannel' })
        ],
        blocking: false,
        requireThirdPartyOutgoingId: false
    };
    return recommendationService;
};
exports.getRecommendedServiceWithRequireThirdPartyOutgoingId = () => {
    const recommendationService = {
        paymentServiceCode: '1',
        beneficiaryBankCodeType: transaction_enum_1.BankCodeTypes.RTOL,
        transactionAuthenticationChecking: true,
        debitTransactionCode: [
            {
                transactionCode: 'd01',
                transactionCodeInfo: {
                    code: 'd01',
                    channel: 'd01_channel',
                    defaultCategoryCode: 'defaultD01',
                    minAmountPerTx: 100,
                    maxAmountPerTx: 9999999,
                    limitGroupCode: 'd01'
                }
            }
        ],
        creditTransactionCode: [
            {
                transactionCode: 'c01',
                transactionCodeInfo: {
                    code: 'c01',
                    channel: 'c01_channel',
                    defaultCategoryCode: 'defaultC01',
                    minAmountPerTx: 100,
                    maxAmountPerTx: 9999999,
                    limitGroupCode: 'c01'
                }
            }
        ],
        beneficiaryBankCode: '1',
        feeData: [
            Object.assign(Object.assign({}, fee_data_1.getFeeAmountData()), { feeAmount: 100, customerTc: 'customerTc', customerTcChannel: 'customerTcChannel' })
        ],
        blocking: false,
        requireThirdPartyOutgoingId: true
    };
    return recommendationService;
};
exports.createExternalTransactionInput = () => ({
    interchange: module_common_1.BankNetworkEnum.ARTAJASA,
    externalId: 'externalId',
    transactionDate: '030111124455',
    paymentServiceType: module_common_1.PaymentServiceTypeEnum.THIRD_PARTY_TRANSFER,
    sourceBankCode: '000124',
    sourceAccountNo,
    sourceAccountName: 'sourceAccountName',
    currency: 'currency',
    transactionAmount: 1000,
    beneficiaryBankCode: '542',
    beneficiaryAccountNo,
    beneficiaryAccountName: 'beneficiaryAccountName',
    notes: 'string',
    additionalInformation1: 'string',
    additionalInformation2: 'string',
    additionalInformation3: 'string',
    additionalInformation4: 'string'
});
exports.getTransactionMockValueObject = () => {
    const beneficiaryBankCode = exports.getBankCode();
    const beneficiaryAccount = {
        bankCode: beneficiaryBankCode.bankCodeId,
        accountNumber: '1',
        accountType: 'MA',
        currency: 'IDR',
        accountName: 'beneficiaryAccountName',
        cif: '1',
        customerId: 'beneficiary_customer_id'
    };
    const sourceBankCode = exports.getBankCode();
    const sourceAccount = {
        bankCode: sourceBankCode.bankCodeId,
        accountNumber: '2',
        accountType: 'MA',
        currency: 'IDR',
        accountName: 'sourceAccountName',
        cif: '2',
        customerId: 'source_customer_id'
    };
    const sourceAccountForWithdrawal = {
        bankCode: sourceBankCode.bankCodeId,
        accountNumber: '100901145538',
        accountType: 'MA',
        currency: 'IDR',
        accountName: 'sourceAccountName',
        cif: '2',
        customerId: 'source_customer_id'
    };
    const transactionCategory = exports.getTransactionCategory();
    const ruleConfigs = configuration_data_1.getPaymentConfigRulesData();
    const limitGroups = configuration_data_1.getLimitGroupsData();
    const usageCounters = [
        {
            limitGroupCode: 'L002',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
        },
        {
            limitGroupCode: 'L003',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
        },
        {
            limitGroupCode: 'L004',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
        }
    ];
    const beneficiaryAccountBifastAccountNo = {
        bankCode: beneficiaryBankCode.bankCodeId,
        accountNumber: '100100100',
        beneficiaryAccountType: 'SVGS',
        accountName: 'beneficiaryAccountName',
        cif: '1',
        customerId: 'beneficiary_customer_id',
        beneficiaryProxyType: '00'
    };
    const beneficiaryAccountBifastPhoneNo = {
        bankCode: beneficiaryBankCode.bankCodeId,
        accountNumber: '+628112233',
        beneficiaryAccountType: 'SVGS',
        accountName: 'beneficiaryAccountName',
        cif: '1',
        customerId: 'beneficiary_customer_id',
        beneficiaryProxyType: '01'
    };
    const beneficiaryAccountBifastEmail = {
        bankCode: beneficiaryBankCode.bankCodeId,
        accountNumber: 'test@test.com',
        beneficiaryAccountType: 'SVGS',
        accountName: 'beneficiaryAccountName',
        cif: '1',
        customerId: 'beneficiary_customer_id',
        beneficiaryProxyType: '02'
    };
    return {
        sourceAccount,
        sourceAccountForWithdrawal,
        sourceBankCode,
        beneficiaryAccount,
        beneficiaryBankCode,
        transactionCategory,
        recommendService: exports.getRecommendedService(),
        ruleConfigs,
        limitGroups,
        usageCounters,
        externalBankCode: exports.getExternalBankCode(),
        beneficiaryAccountBifastAccountNo,
        beneficiaryAccountBifastPhoneNo,
        beneficiaryAccountBifastEmail
    };
};
exports.recommendedServicesWithAwardGroup = [
    Object.assign(Object.assign({}, exports.getRecommendedService()), { debitTransactionCode: [
            {
                transactionCode: 'd01',
                transactionCodeInfo: {
                    code: 'd01',
                    channel: 'd01_channel',
                    defaultCategoryCode: 'defaultD01',
                    minAmountPerTx: 100,
                    maxAmountPerTx: 9999999,
                    limitGroupCode: 'c01',
                    awardGroupCounter: 'Bonus_Transfer',
                    feeRules: '1'
                }
            }
        ], feeData: undefined })
];
exports.getExpectedInterchangeData = () => ({
    additionalInformation1: undefined,
    additionalInformation2: undefined,
    additionalInformation3: undefined,
    additionalInformation4: undefined,
    categoryCode: undefined,
    creditTransactionChannel: undefined,
    creditTransactionCode: undefined,
    debitAwardGroupCounter: undefined,
    debitLimitGroupCode: undefined,
    debitTransactionChannel: undefined,
    debitTransactionCode: undefined,
    interchange: module_common_1.BankNetworkEnum.ALTO,
    fees: [
        {
            feeCode: 'altoFeeCode',
            feeAmount: 0,
            customerTc: 'FBD01',
            subsidiaryAmount: 0,
            debitGlNumber: '',
            creditGlNumber: '',
            subsidiary: false,
            customerTcChannel: 'FBD01',
            id: '123456789',
            blockingId: '123456789',
            interchange: module_common_1.BankNetworkEnum.ALTO
        }
    ],
    processingInfo: {
        paymentRail: 'MOCKED_RAIL'
    }
});
exports.getTransactionForConfirmTranscation = () => (Object.assign(Object.assign({}, exports.getTransactionDetail()), { fees: [
        {
            feeCode: 'feeCode',
            feeAmount: 0,
            customerTc: 'FBD01',
            subsidiaryAmount: 0,
            debitGlNumber: '',
            creditGlNumber: '',
            subsidiary: false,
            customerTcChannel: 'FBD01',
            id: '123456789',
            blockingId: '123456789',
            interchange: module_common_1.BankNetworkEnum.ARTAJASA
        }
    ], feeMapping: [
        {
            feeCode: 'artajasaFeeCode',
            feeAmount: 0,
            customerTc: 'FBD01',
            subsidiaryAmount: 0,
            debitGlNumber: '',
            creditGlNumber: '',
            subsidiary: false,
            customerTcChannel: 'FBD01',
            id: '123456789',
            blockingId: '123456789',
            interchange: module_common_1.BankNetworkEnum.ARTAJASA
        },
        {
            feeCode: 'altoFeeCode',
            feeAmount: 0,
            customerTc: 'FBD01',
            subsidiaryAmount: 0,
            debitGlNumber: '',
            creditGlNumber: '',
            subsidiary: false,
            customerTcChannel: 'FBD01',
            id: '123456789',
            blockingId: '123456789',
            interchange: module_common_1.BankNetworkEnum.ALTO
        }
    ], additionalInformation1: undefined, additionalInformation2: undefined, additionalInformation3: undefined, additionalInformation4: undefined, processingInfo: {
        paymentRail: 'QRIS'
    } }));
exports.getTransactionWithCoreBankingTransactionIds = () => (Object.assign(Object.assign({}, exports.getTransactionModelData()), { id: '1', coreBankingTransactionIds: ['123456789', '987654321'], coreBankingTransactions: [
        {
            id: '123456789',
            type: transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_DEBITED
        },
        {
            id: '987654321',
            type: transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_CREDITED
        }
    ] }));
exports.getTransactionCodeMapping = () => ({
    debitTransactionCodeMapping: [
        {
            code: 'd01',
            channel: 'd01_channel',
            defaultCategoryCode: 'defaultD01',
            minAmountPerTx: 100,
            maxAmountPerTx: 9999999,
            limitGroupCode: 'd01',
            interchange: module_common_1.BankNetworkEnum.ARTAJASA
        },
        {
            code: 'd02',
            channel: 'd02_channel',
            defaultCategoryCode: 'defaultD02',
            minAmountPerTx: 100,
            maxAmountPerTx: 9999999,
            limitGroupCode: 'd02',
            interchange: module_common_1.BankNetworkEnum.ALTO
        }
    ],
    creditTransactionCodeMapping: [
        {
            code: 'c01',
            channel: 'c01_channel',
            defaultCategoryCode: 'defaultC01',
            minAmountPerTx: 100,
            maxAmountPerTx: 9999999,
            limitGroupCode: 'c01',
            interchange: module_common_1.BankNetworkEnum.ARTAJASA
        },
        {
            code: 'c02',
            channel: 'c02_channel',
            defaultCategoryCode: 'defaultC02',
            minAmountPerTx: 100,
            maxAmountPerTx: 9999999,
            limitGroupCode: 'c01',
            interchange: module_common_1.BankNetworkEnum.ALTO
        }
    ]
});
exports.getTransactionCodeMappingWithFeeRule = () => ({
    debitTransactionCodeMapping: [
        {
            code: 'd01',
            channel: 'd01_channel',
            defaultCategoryCode: 'defaultD01',
            minAmountPerTx: 100,
            maxAmountPerTx: 9999999,
            limitGroupCode: 'd01',
            interchange: module_common_1.BankNetworkEnum.ARTAJASA,
            feeRules: 'RTOL_TRANSFER_FEE_RULE'
        },
        {
            code: 'd02',
            channel: 'd02_channel',
            defaultCategoryCode: 'defaultD02',
            minAmountPerTx: 100,
            maxAmountPerTx: 9999999,
            limitGroupCode: 'd02',
            interchange: module_common_1.BankNetworkEnum.ALTO,
            feeRules: 'RTOL_TRANSFER_FEE_RULE'
        }
    ],
    creditTransactionCodeMapping: [
        {
            code: 'c01',
            channel: 'c01_channel',
            defaultCategoryCode: 'defaultC01',
            minAmountPerTx: 100,
            maxAmountPerTx: 9999999,
            limitGroupCode: 'c01',
            interchange: module_common_1.BankNetworkEnum.ARTAJASA
        },
        {
            code: 'c02',
            channel: 'c02_channel',
            defaultCategoryCode: 'defaultC02',
            minAmountPerTx: 100,
            maxAmountPerTx: 9999999,
            limitGroupCode: 'c01',
            interchange: module_common_1.BankNetworkEnum.ALTO
        }
    ]
});
exports.getFeeMapping = () => ({
    feeMapping: [
        {
            feeCode: 'CW003',
            feeAmount: 100,
            customerTc: 'customerTc',
            customerTcChannel: 'customerTcChannel',
            subsidiary: true,
            subsidiaryAmount: 7500,
            debitGlNumber: '01.01.00.00.00.00',
            creditGlNumber: '01.02.00.00.00.00',
            interchange: module_common_1.BankNetworkEnum.ARTAJASA
        },
        {
            feeCode: 'CW004',
            feeAmount: 100,
            customerTc: 'customerTc',
            customerTcChannel: 'customerTcChannel',
            subsidiary: true,
            subsidiaryAmount: 7500,
            debitGlNumber: '01.01.00.00.00.00',
            creditGlNumber: '01.02.00.00.00.00',
            interchange: module_common_1.BankNetworkEnum.ALTO
        }
    ]
});
exports.getRecommendedServiceResponse = () => [
    {
        paymentServiceCode: 'RTOL',
        beneficiaryBankCodeType: 'RTOL',
        debitTransactionCode: [
            {
                interchange: 'INTERNAL',
                transactionCode: 'TFD30',
                transactionCodeInfo: {
                    code: 'TFD30',
                    limitGroupCode: 'L002',
                    feeRules: 'RTOL_TRANSFER_FEE_RULE',
                    minAmountPerTx: 10000,
                    maxAmountPerTx: 50000000,
                    defaultCategoryCode: 'C056',
                    channel: 'TFD30',
                    awardGroupCounter: 'Bonus_Transfer'
                }
            },
            {
                interchange: 'ALTO',
                transactionCode: 'TFD40',
                transactionCodeInfo: {
                    code: 'TFD40',
                    limitGroupCode: 'L002',
                    feeRules: 'RTOL_TRANSFER_FEE_RULE',
                    minAmountPerTx: 10000,
                    maxAmountPerTx: 50000000,
                    defaultCategoryCode: 'C056',
                    channel: 'TFD40',
                    awardGroupCounter: 'Bonus_Transfer'
                }
            }
        ],
        creditTransactionCode: [],
        inquiryFeeRule: null,
        transactionAuthenticationChecking: true,
        blocking: true,
        requireThirdPartyOutgoingId: true,
        cutOffTime: null,
        beneficiaryBankCode: '014',
        feeData: [
            {
                feeCode: 'TF010',
                interchange: 'ARTAJASA',
                feeAmount: 3000,
                customerTc: 'FTD40',
                subsidiaryAmount: 0,
                debitGlNumber: '',
                creditGlNumber: '',
                subsidiary: false,
                customerTcChannel: 'FTD40'
            },
            {
                feeCode: 'TF009',
                interchange: 'ALTO',
                feeAmount: 3000,
                customerTc: 'FTD30',
                subsidiaryAmount: 0,
                debitGlNumber: '',
                creditGlNumber: '',
                subsidiary: false,
                customerTcChannel: 'FTD30'
            }
        ]
    }
];
exports.createForwardPaymentTransactionInput = (paymentServiceType) => {
    const input = Object.assign(Object.assign(Object.assign({}, exports.getITransactionInput()), exports.getRecommendPaymentServiceInput()), { paymentServiceType, paymentServiceCode: paymentServiceType, interchange: module_common_1.BankNetworkEnum.LOAN_PROCESSOR_DEPOSIT_REPAYMENT });
    return input;
};
exports.getNNSMappingData = () => ({
    code: '002',
    reservedNnsPrefix: '93600002',
    switchingName: 'Jalin',
    brandName: 'BRI',
    institutionName: 'BANK RAKYAT INDONESIA'
});
exports.getRefundDetails = () => ({
    remainingAmount: 0,
    transactions: ['5f8f7a1c481f6a70b636bc85', '5f8f7a1c481f6a70b636bc86']
});
exports.makeCodeInfo = (limitGroupCode, feeRule, code) => {
    return {
        limitGroupCode: limitGroupCode,
        feeRules: feeRule,
        code: code,
        minAmountPerTx: 1,
        maxAmountPerTx: 999999,
        channel: module_common_1.BankChannelEnum.INTERNAL
    };
};
exports.getCashBackServiceMappings = () => [
    {
        paymentServiceCode: 'CASHBACK',
        beneficiaryBankCodeType: 'RTOL',
        debitTransactionCode: [],
        creditTransactionCode: [
            {
                interchange: module_common_1.BankNetworkEnum.JAGO,
                transactionCode: 'CBC01',
                transactionCodeInfo: {
                    code: 'CBC01',
                    feeRules: 'CASHBACK_TAX_RULE',
                    minAmountPerTx: 1,
                    maxAmountPerTx: 99999999999,
                    defaultCategoryCode: 'C046',
                    channel: 'CBC01'
                }
            },
            {
                interchange: module_common_1.BankNetworkEnum.PARTNER,
                transactionCode: 'CBC02',
                transactionCodeInfo: {
                    code: 'CBC02',
                    feeRules: 'CASHBACK_TAX_RULE',
                    minAmountPerTx: 1,
                    maxAmountPerTx: 99999999999,
                    defaultCategoryCode: 'C046',
                    channel: 'CBC02'
                }
            },
            {
                interchange: module_common_1.BankNetworkEnum.VISA,
                transactionCode: 'CBC03',
                transactionCodeInfo: {
                    code: 'CBC03',
                    feeRules: 'CASHBACK_TAX_RULE',
                    minAmountPerTx: 1,
                    maxAmountPerTx: 99999999999,
                    defaultCategoryCode: 'C046',
                    channel: 'CBC03'
                }
            },
            {
                interchange: module_common_1.BankNetworkEnum.GIVEAWAY,
                transactionCode: 'CBC04',
                transactionCodeInfo: {
                    code: 'CBC04',
                    feeRules: 'CASHBACK_TAX_RULE',
                    minAmountPerTx: 1,
                    maxAmountPerTx: 99999999999,
                    defaultCategoryCode: 'C046',
                    channel: 'CBC04'
                }
            },
            {
                interchange: module_common_1.BankNetworkEnum.PARTNER_REIMB_RRA,
                transactionCode: 'CBC05',
                transactionCodeInfo: {
                    code: 'CBC05',
                    feeRules: 'ZERO_FEE_RULE',
                    minAmountPerTx: 1,
                    maxAmountPerTx: 99999999999,
                    defaultCategoryCode: 'C040',
                    channel: 'CBC05'
                }
            }
        ],
        transactionAuthenticationChecking: true,
        blocking: false,
        requireThirdPartyOutgoingId: false,
        feeData: [
            {
                feeCode: 'CBC01',
                interchange: module_common_1.BankNetworkEnum.JAGO,
                feeAmount: 0,
                customerTc: 'TXD16',
                subsidiaryAmount: 6.390000000000001,
                debitGlNumber: '470000007.00.00',
                creditGlNumber: '229001051.07.00',
                subsidiary: true,
                customerTcChannel: 'TXD16'
            },
            {
                feeCode: 'CBC02',
                interchange: module_common_1.BankNetworkEnum.PARTNER,
                feeAmount: 0,
                customerTc: 'TXD18',
                subsidiaryAmount: 6.390000000000001,
                debitGlNumber: '470000007.00.00',
                creditGlNumber: '229001051.07.00',
                subsidiary: true,
                customerTcChannel: 'TXD18'
            },
            {
                feeCode: 'CBC03',
                interchange: module_common_1.BankNetworkEnum.VISA,
                feeAmount: 0,
                customerTc: 'TXD19',
                subsidiaryAmount: 6.390000000000001,
                debitGlNumber: '470000007.00.00',
                creditGlNumber: '229001051.07.00',
                subsidiary: true,
                customerTcChannel: 'TXD19'
            },
            {
                feeCode: 'CBC04',
                interchange: module_common_1.BankNetworkEnum.GIVEAWAY,
                feeAmount: 0,
                customerTc: 'TXD20',
                subsidiaryAmount: 6.390000000000001,
                debitGlNumber: '470000007.00.00',
                creditGlNumber: '229001051.07.00',
                subsidiary: true,
                customerTcChannel: 'TXD20'
            }
        ]
    }
];
exports.getTyped = (psCode) => {
    return {
        paymentServiceCode: psCode,
        creditTransactionCode: [
            {
                transactionCode: 'OFC01',
                interchange: module_common_1.BankNetworkEnum.TAX,
                transactionCodeInfo: {
                    code: 'TFD20',
                    feeRules: `${psCode}_CREDIT_FEE_RULE`,
                    minAmountPerTx: 1,
                    maxAmountPerTx: 99999999999,
                    channel: 'OFC01'
                }
            }
        ],
        debitTransactionCode: [
            {
                transactionCode: 'OFC02',
                interchange: module_common_1.BankNetworkEnum.NOTAX,
                transactionCodeInfo: {
                    code: 'OFC02',
                    feeRules: `${psCode}_DEBIT_FEE_RULE`,
                    minAmountPerTx: 1,
                    maxAmountPerTx: 99999999999,
                    channel: 'OFC02'
                }
            }
        ],
        beneficiaryBankCodeType: 'RTOL',
        transactionAuthenticationChecking: false,
        blocking: false,
        requireThirdPartyOutgoingId: false
    };
};
//# sourceMappingURL=transaction.data.js.map