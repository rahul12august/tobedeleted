"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const transaction_enum_1 = require("./transaction.enum");
const module_common_1 = require("@dk/module-common");
const mongoose_util_1 = require("../common/mongoose.util");
const transaction_enum_2 = require("./transaction.enum");
const transaction_constant_1 = require("./transaction.constant");
const errors_1 = require("../common/errors");
exports.collectionName = 'transactions';
const TransactionCodeSchema = new mongoose_1.Schema({
    interchange: {
        type: String,
        required: false
    },
    code: {
        type: String,
        required: true
    },
    limitGroupCode: {
        type: String,
        required: false
    },
    feeRules: {
        type: String,
        required: false
    },
    minAmountPerTx: {
        type: Number,
        required: false
    },
    maxAmountPerTx: {
        type: Number,
        required: false
    },
    defaultCategoryCode: {
        type: String,
        required: false
    },
    channel: {
        type: String,
        required: true
    },
    confirmationCode: {
        type: String
    },
    awardGroupCounter: {
        type: String,
        required: false
    }
});
const FeeSchema = new mongoose_1.Schema({
    feeCode: {
        type: String,
        required: true
    },
    interchange: {
        type: String,
        required: false
    },
    feeAmount: {
        type: Number,
        required: true
    },
    customerTc: {
        type: String,
        required: true
    },
    customerTcChannel: {
        type: String,
        required: true
    },
    subsidiaryAmount: {
        type: Number
    },
    debitGlNumber: {
        type: String
    },
    creditGlNumber: {
        type: String
    },
    subsidiary: {
        type: Boolean
    },
    blockingId: {
        type: String
    },
    idempotencyKey: {
        type: String,
        required: false
    }
});
const PaymentInstructionExecutionSchema = new mongoose_1.Schema({
    paymentInstructionCode: {
        type: String,
        required: true
    },
    executionType: {
        type: String,
        required: true,
        enum: Object.values(module_common_1.TransactionExecutionTypeEnum)
    },
    topics: {
        type: [String],
        required: true
    }
});
const ReasonSchema = new mongoose_1.Schema({
    responseCode: {
        type: Number
    },
    responseMessage: {
        type: String
    },
    transactionDate: {
        type: String
    }
});
const FailureReasonSchema = new mongoose_1.Schema({
    type: {
        type: String,
        enum: Object.values(errors_1.ERROR_CODE),
        required: false
    },
    actor: {
        type: String,
        enum: Object.values(transaction_enum_1.TransferFailureReasonActor),
        required: false
    },
    detail: {
        type: String,
        required: false
    }
}, { _id: false });
const TransferJourneySchema = new mongoose_1.Schema({
    status: {
        type: String,
        enum: Object.values(transaction_enum_1.TransferJourneyStatusEnum),
        required: true
    },
    updatedAt: {
        type: Date,
        required: false
    }
}, { timestamps: false, _id: false });
const RefundSchema = new mongoose_1.Schema({
    remainingAmount: {
        type: Number,
        required: false
    },
    originalTransactionId: {
        type: String,
        required: false
    },
    transactions: {
        type: [String],
        required: false
    },
    revertPaymentServiceCode: {
        type: String,
        required: false
    }
}, { _id: false });
const EntitlementInfoSchema = new mongoose_1.Schema({
    entitlementCodeRefId: {
        type: String,
        required: false
    },
    counterCode: {
        type: String,
        required: false
    },
    customerId: {
        type: String,
        required: false
    }
}, { _id: false });
const CoreBankingTransactionSchema = new mongoose_1.Schema({
    id: { type: String, required: true },
    type: { type: String, required: true }
}, { timestamps: false, _id: false });
const ProcessingInfoSchema = new mongoose_1.Schema({
    paymentRail: { type: String, required: false },
    channel: { type: String, required: false },
    method: {
        type: String,
        enum: Object.values(transaction_constant_1.ExecutionMethodEnum),
        required: false
    },
    partner: { type: String, required: false }
}, { timestamps: false, _id: false });
const TransactionSchema = new mongoose_1.Schema({
    sourceCustomerId: {
        type: String,
        required: false
    },
    paymentServiceType: {
        type: String,
        enum: Object.values(module_common_1.PaymentServiceTypeEnum),
        required: true
    },
    paymentRequestID: {
        type: String,
        required: false
    },
    referenceId: {
        type: String,
        required: true
    },
    externalId: {
        type: String,
        required: true
    },
    secondaryExternalId: {
        type: String,
        required: false
    },
    blockingId: {
        type: String,
        required: false
    },
    cardId: {
        type: String,
        required: false
    },
    sourceCIF: {
        type: String,
        required: false
    },
    sourceAccountType: {
        type: String,
        required: false
    },
    sourceAccountNo: {
        type: String,
        required: false
    },
    sourceAccountName: {
        type: String,
        required: false
    },
    sourceBankCode: {
        type: String,
        required: false
    },
    sourceRtolCode: {
        type: String,
        required: false
    },
    sourceRemittanceCode: {
        type: String,
        required: false
    },
    sourceBankCodeChannel: {
        type: String,
        enum: Object.values(module_common_1.BankChannelEnum),
        required: false
    },
    sourceTransactionCurrency: {
        type: String,
        required: false
    },
    transactionAmount: {
        type: Number,
        required: true
    },
    beneficiaryCIF: {
        type: String,
        required: false
    },
    beneficiaryBankCode: {
        type: String,
        required: false
    },
    beneficiaryRealBankCode: {
        type: String,
        required: false
    },
    beneficiaryAccountType: {
        type: String,
        required: false
    },
    beneficiaryAccountNo: {
        type: String,
        required: false
    },
    beneficiaryAccountName: {
        type: String,
        required: false
    },
    beneficiaryBankName: {
        type: String,
        required: false
    },
    beneficiaryBankCodeChannel: {
        type: String,
        enum: Object.values(module_common_1.BankChannelEnum),
        required: false
    },
    beneficiaryRtolCode: {
        type: String,
        required: false
    },
    beneficiaryRemittanceCode: {
        type: String,
        required: false
    },
    beneficiaryIrisCode: {
        type: String,
        required: false
    },
    beneficiaryCustomerId: {
        type: String,
        required: false
    },
    creditPriority: {
        type: String,
        enum: Object.values(module_common_1.BankNetworkEnum),
        required: false
    },
    debitPriority: {
        type: String,
        enum: Object.values(module_common_1.BankNetworkEnum),
        required: false
    },
    debitLimitGroupCode: {
        type: String,
        required: false
    },
    billerCode: {
        type: String,
        required: false
    },
    institutionalType: {
        type: String,
        enum: Object.values(module_common_1.InstitutionTypeEnum),
        required: false
    },
    note: {
        type: String,
        required: false
    },
    categoryCode: {
        type: String,
        required: false
    },
    userSelectedCategoryCode: {
        type: String,
        required: false
    },
    paymentServiceCode: {
        type: String,
        required: false
    },
    debitTransactionCode: {
        type: String,
        required: false
    },
    debitTransactionChannel: {
        type: String,
        required: false
    },
    creditTransactionCode: {
        type: String,
        required: false
    },
    creditTransactionChannel: {
        type: String,
        required: false
    },
    extendedMessage: {
        type: String,
        required: false
    },
    status: {
        type: String,
        enum: Object.values(transaction_enum_1.TransactionStatus),
        required: true
    },
    fees: {
        type: [FeeSchema],
        required: false
    },
    additionalInformation1: {
        type: String,
        required: false
    },
    additionalInformation2: {
        type: String,
        required: false
    },
    additionalInformation3: {
        type: String,
        required: false
    },
    additionalInformation4: {
        type: String,
        required: false
    },
    promotionCode: {
        type: String,
        required: false
    },
    interchange: {
        type: String,
        required: false
    },
    externalTransactionDate: {
        // MMDDhhmmss
        type: String,
        required: false
    },
    thirdPartyOutgoingId: {
        type: String,
        required: false,
        index: true,
        sparse: true
    },
    coreBankingTransactionIds: {
        type: [String],
        required: false
    },
    executionType: {
        type: String,
        enum: Object.values(transaction_enum_2.ExecutionTypeEnum),
        required: false
    },
    requireThirdPartyOutgoingId: {
        type: Boolean,
        required: true,
        default: false
    },
    debitTransactionCodeMapping: {
        type: [TransactionCodeSchema],
        required: false
    },
    creditTransactionCodeMapping: {
        type: [TransactionCodeSchema],
        required: false
    },
    feeMapping: {
        type: [FeeSchema],
        required: false
    },
    paymentInstructionExecution: {
        type: PaymentInstructionExecutionSchema,
        required: false
    },
    executorCIF: {
        type: String,
        required: false
    },
    destinationCIF: {
        type: String,
        required: false
    },
    moverRemainingDailyUsage: {
        type: Number,
        required: false
    },
    reason: {
        type: [ReasonSchema]
    },
    failureReason: {
        type: FailureReasonSchema
    },
    refund: {
        type: RefundSchema,
        required: false
    },
    manuallyAdjusted: {
        type: Boolean,
        default: false,
        required: false
    },
    debitAwardGroupCounter: {
        type: String,
        required: false
    },
    journey: {
        type: [TransferJourneySchema],
        required: false
    },
    paymentInstructionId: {
        type: String,
        required: false
    },
    coreBankingTransactions: {
        type: [CoreBankingTransactionSchema]
    },
    ownerCustomerId: {
        index: true,
        type: String,
        required: false
    },
    sourceIdNumber: {
        type: String,
        required: false
    },
    sourceCustomerType: {
        type: String,
        required: false
    },
    sourceAccountTypeBI: {
        type: String,
        required: false
    },
    idempotencyKey: {
        type: String,
        required: false,
        index: true,
        sparse: true
    },
    preferedBankChannel: {
        type: String,
        required: false
    },
    entitlementInfo: {
        type: [EntitlementInfoSchema],
        required: false
    },
    processingInfo: {
        type: ProcessingInfoSchema,
        required: false
    },
    workflowId: {
        type: String,
        required: false
    },
    /**
     * correlationId for transactions.
     * In some transaction processing (for example, Atome card transaction), there may be more than one transfer.
     * We use this correlation id to link them together.
     */
    correlationId: {
        type: String,
        required: false
    }
}, {
    timestamps: true
});
// Create index for createdAt
TransactionSchema.index({ createdAt: 1 });
TransactionSchema.index({
    paymentServiceCode: 1,
    ownerCustomerId: 1,
    createdAt: 1
});
TransactionSchema.post('save', mongoose_util_1.handleDuplicationError);
TransactionSchema.post('find', mongoose_util_1.handleDuplicationError);
exports.TransactionModel = mongoose_1.default.model(exports.collectionName, TransactionSchema);
//# sourceMappingURL=transaction.model.js.map