"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const lodash_1 = require("lodash");
const transactionCoreBanking_helper_1 = __importDefault(require("../transactionCoreBanking.helper"));
const deposit_repository_1 = __importDefault(require("../../coreBanking/deposit.repository"));
const logger_1 = __importDefault(require("../../logger"));
const transaction_enum_1 = require("../transaction.enum");
const updateCoreBankingTransactionIds = (model, ids) => {
    var _a;
    if (!lodash_1.isEmpty(ids) && model.coreBankingTransactionIds) {
        for (const id of ids) {
            model.coreBankingTransactionIds.push(id);
            (_a = model.coreBankingTransactions) === null || _a === void 0 ? void 0 : _a.push({
                id: id,
                type: transaction_enum_1.TransferJourneyStatusEnum.FEE_AMOUNT_DEBITED
            });
        }
    }
};
const checkIfFeeIsAvailable = (model) => !(!model.fees || model.fees.length <= 0);
const generalRefundFeeExecution = {
    isApplicable: (model) => checkIfFeeIsAvailable(model) &&
        !lodash_1.isEmpty(model.beneficiaryAccountNo) &&
        model.paymentServiceType === module_common_1.PaymentServiceTypeEnum.GENERAL_REFUND,
    execute: (model) => __awaiter(void 0, void 0, void 0, function* () {
        logger_1.default.info(`generalRefundFeeExecution: executing general refund fee with beneficiary account no.`);
        if (model.beneficiaryAccountNo) {
            const feeTransactionIds = yield transactionCoreBanking_helper_1.default.submitFeeTransactions(model.beneficiaryAccountNo, model, deposit_repository_1.default.makeDeposit);
            updateCoreBankingTransactionIds(model, feeTransactionIds);
        }
    })
};
const offerFeeExecution = {
    isApplicable: (model) => checkIfFeeIsAvailable(model) &&
        !lodash_1.isEmpty(model.beneficiaryAccountNo) &&
        (model.paymentServiceType === module_common_1.PaymentServiceTypeEnum.OFFER ||
            model.paymentServiceType === module_common_1.PaymentServiceTypeEnum.BONUS_INTEREST),
    execute: (model) => __awaiter(void 0, void 0, void 0, function* () {
        logger_1.default.info(`offerFeeExecution: executing offer fee with beneficiary account no.`);
        if (model.beneficiaryAccountNo) {
            const transaction = Object.assign(Object.assign({}, model), { sourceBankCode: model.beneficiaryBankCode, sourceAccountName: model.beneficiaryAccountName, sourceAccountType: model.beneficiaryAccountType, sourceAccountNo: model.beneficiaryAccountNo, beneficiaryAccountNo: model.sourceAccountNo, sourceCIF: model.beneficiaryCIF, beneficiaryCIF: undefined });
            const feeTransactionIds = yield transactionCoreBanking_helper_1.default.submitFeeTransactions(model.beneficiaryAccountNo, transaction, deposit_repository_1.default.makeWithdrawal);
            updateCoreBankingTransactionIds(model, feeTransactionIds);
        }
    })
};
const sourceAccountFeeExecution = {
    isApplicable: (model) => checkIfFeeIsAvailable(model) && !lodash_1.isEmpty(model.sourceAccountNo),
    execute: (model) => __awaiter(void 0, void 0, void 0, function* () {
        logger_1.default.info(`sourceAccountFeeExecution: executing source account fee execution.`);
        if (model.sourceAccountNo) {
            const feeTransactionIds = yield transactionCoreBanking_helper_1.default.submitFeeTransactions(model.sourceAccountNo, model, deposit_repository_1.default.makeWithdrawal);
            updateCoreBankingTransactionIds(model, feeTransactionIds);
        }
    })
};
const cashbackFeeExecution = {
    isApplicable: (model) => checkIfFeeIsAvailable(model) &&
        model.paymentServiceType === module_common_1.PaymentServiceTypeEnum.CASHBACK,
    execute: (model) => __awaiter(void 0, void 0, void 0, function* () {
        logger_1.default.info(`cashbackFeeExecution: executing cashback fee execution.`);
        yield transactionCoreBanking_helper_1.default.submitGlFees(model);
    })
};
const availableTemplates = [
    generalRefundFeeExecution,
    offerFeeExecution,
    cashbackFeeExecution,
    sourceAccountFeeExecution
];
exports.getApplicable = (input) => availableTemplates.find(template => template.isApplicable(input));
//# sourceMappingURL=feeExecution.templates.js.map