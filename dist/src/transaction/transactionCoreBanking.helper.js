"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const AppError_1 = require("./../errors/AppError");
const errors_1 = require("../common/errors");
const transaction_util_1 = require("./transaction.util");
const deposit_repository_1 = __importDefault(require("../coreBanking/deposit.repository"));
const transactionFee_helper_1 = __importDefault(require("./transactionFee.helper"));
const loan_repository_1 = __importDefault(require("../coreBanking/loan.repository"));
const alto_repository_1 = __importDefault(require("../alto/alto.repository"));
const alto_enum_1 = require("../alto/alto.enum");
const module_common_1 = require("@dk/module-common");
const lodash_1 = require("lodash");
const alto_utils_1 = require("../alto/alto.utils");
const transaction_enum_1 = require("./transaction.enum");
const submitGlFees = (transactionModel) => {
    const glFees = transactionFee_helper_1.default.getJournalEntriesGLTransactionInput(transactionModel);
    glFees.forEach(glFee => transaction_util_1.submitIdempotentTransactionWithRetry(deposit_repository_1.default.executeJournalEntriesGL, glFee, transactionModel.idempotencyKey || module_common_1.Http.generateIdempotencyKey()));
};
const submitFeeTransactions = (accountNo, transactionModel, coreBankingAction) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionIds = [];
    const feeInputs = transactionFee_helper_1.default.getFeeTransactionInputs(transactionModel);
    logger_1.default.info(`submitFeeTransactions: Executing fee for transactionId : ${transactionModel.id} and feeInputs lengths ${feeInputs.length}`);
    for (const feeTransaction of feeInputs) {
        const createdFeeTransaction = yield transaction_util_1.submitIdempotentTransactionWithRetry(coreBankingAction, accountNo, feeTransaction.feeTransactionPayload, feeTransaction.feeIdempotencyKey || module_common_1.Http.generateIdempotencyKey());
        transactionIds.push(createdFeeTransaction.id);
    }
    submitGlFees(transactionModel);
    return transactionIds;
});
const submitDebitTransaction = (accountNo, transactionModel, transactionCode, transactionChannel) => __awaiter(void 0, void 0, void 0, function* () {
    const debitTransaction = transaction_util_1.mapToCoreBankingTransaction(transactionModel, transactionCode, transactionChannel);
    const withdrawalTransaction = yield transaction_util_1.submitCoreBankingTransactionWithRetry(deposit_repository_1.default.makeWithdrawal, accountNo, debitTransaction);
    return withdrawalTransaction.id;
});
const submitCreditTransaction = (accountNo, transactionModel, transactionCode, transactionChannel) => __awaiter(void 0, void 0, void 0, function* () {
    const creditTransaction = transaction_util_1.mapToCoreBankingTransaction(transactionModel, transactionCode, transactionChannel);
    const depositTransaction = yield transaction_util_1.submitCoreBankingTransactionWithRetry(deposit_repository_1.default.makeDeposit, accountNo, creditTransaction);
    return depositTransaction.id;
});
const submitCardTransaction = (cardId, blockingId, transactionModel, transactionCode, transactionChannel) => __awaiter(void 0, void 0, void 0, function* () {
    const cardTransaction = transaction_util_1.mapToCoreBankingCardTransaction(transactionModel, transactionCode, transactionChannel, blockingId);
    const withdrawalTransaction = yield transaction_util_1.submitIdempotentTransactionWithRetry(deposit_repository_1.default.makeCardWithdrawal, cardId, cardTransaction, transactionModel.idempotencyKey || module_common_1.Http.generateIdempotencyKey());
    return withdrawalTransaction.encodedKey;
});
const submitFeesCardTransaction = (cardId, transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    const cardTransactions = transactionFee_helper_1.default.getFeeCardTransactionInputs(transactionModel);
    const withdrawalTransactions = yield Promise.all(cardTransactions.map((cardTransaction) => __awaiter(void 0, void 0, void 0, function* () {
        return yield transaction_util_1.submitIdempotentTransactionWithRetry(deposit_repository_1.default.makeCardWithdrawal, cardId, cardTransaction.feeCardTransactionPayload, cardTransaction.feeCardIdempotencyKey || module_common_1.Http.generateIdempotencyKey());
    })));
    submitGlFees(transactionModel);
    return withdrawalTransactions.map(withdrawalTransaction => withdrawalTransaction.encodedKey);
});
const submitDisbursementTransaction = (accountNo, transactionModel, transactionChannel) => __awaiter(void 0, void 0, void 0, function* () {
    const disbursementPayload = transaction_util_1.mapToDisbursementTransaction(transactionModel, transactionChannel);
    const idempotencyKey = lodash_1.get(transactionModel, 'additionalPayload.coreBankingIdempotencyKey') ||
        transactionModel.idempotencyKey ||
        module_common_1.Http.generateIdempotencyKey();
    const disbursementTransaction = yield transaction_util_1.submitIdempotentTransactionWithRetry(loan_repository_1.default.makeDisbursement, accountNo, disbursementPayload, idempotencyKey);
    return disbursementTransaction.id;
});
const submitRepaymentTransaction = (accountNo, transactionModel, transactionChannel, bankIncomeAmount) => __awaiter(void 0, void 0, void 0, function* () {
    const repaymentPayload = transaction_util_1.mapToRepaymentTransaction(transactionModel, transactionChannel, bankIncomeAmount);
    const idempotencyKey = lodash_1.get(transactionModel, 'additionalPayload.coreBankingIdempotencyKey') ||
        transactionModel.idempotencyKey ||
        module_common_1.Http.generateIdempotencyKey();
    const repaymentTransaction = yield transaction_util_1.submitIdempotentTransactionWithRetry(loan_repository_1.default.makeRepayment, accountNo, repaymentPayload, idempotencyKey);
    return repaymentTransaction.id;
});
const submitGlTransaction = (model, isReversal = false, transactionId) => __awaiter(void 0, void 0, void 0, function* () {
    const glIncomePayload = transaction_util_1.mapToGlIncomeTransaction(model, isReversal, transactionId);
    const glIncomeTransaction = yield transaction_util_1.submitIdempotentTransactionWithRetry(deposit_repository_1.default.createGlJournalEntries, glIncomePayload, model.idempotencyKey || module_common_1.Http.generateIdempotencyKey());
    return glIncomeTransaction.id;
});
const submitPayOffTransaction = (accountNo, transactionModel, transactionChannel) => __awaiter(void 0, void 0, void 0, function* () {
    const payoffPayload = transaction_util_1.mapToPayOffTransaction(transactionModel, transactionChannel);
    const idempotencyKey = lodash_1.get(transactionModel, 'additionalPayload.coreBankingIdempotencyKey') ||
        transactionModel.idempotencyKey ||
        module_common_1.Http.generateIdempotencyKey();
    yield transaction_util_1.submitIdempotentTransactionWithRetry(loan_repository_1.default.makePayOff, accountNo, payoffPayload, idempotencyKey);
});
const submitQrisTransaction = (transactionModel, additionalPayload) => __awaiter(void 0, void 0, void 0, function* () {
    const qrisPayload = transaction_util_1.mapToQrisTransaction(transactionModel, additionalPayload);
    let paymentResult = yield module_common_1.Util.retry(Object.assign(Object.assign({}, transaction_util_1.IdempotentRetryConfig()), { shouldRetry: alto_utils_1.shouldRetryQrisTransaction }), alto_repository_1.default.submitTransaction, qrisPayload);
    if (paymentResult.transactionStatus ==
        alto_enum_1.QrisTransactionStatusEnum.SHOULD_CHECK_STATUS) {
        const statusResult = yield alto_repository_1.default.checkPaymentStatus(paymentResult.customerReferenceNumber);
        paymentResult.transactionStatus = statusResult.transactionStatus;
        paymentResult.forwardingCustomerReferenceNumber =
            statusResult.forwardingCustomerReferenceNumber;
        paymentResult.invoiceNo = statusResult.invoiceNo;
        if (statusResult.transactionStatus == alto_enum_1.QrisTransactionStatusEnum.FAILED) {
            const detail = `Qris payment is failed: ${statusResult.transactionStatus}!`;
            logger_1.default.error(`submitQrisTransaction: ${detail}`);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.QRIS, errors_1.ERROR_CODE.ERROR_FROM_ALTO);
        }
    }
    return paymentResult;
});
exports.default = logger_1.wrapLogs({
    submitDebitTransaction,
    submitCreditTransaction,
    submitFeeTransactions,
    submitCardTransaction,
    submitFeesCardTransaction,
    submitGlFees,
    submitDisbursementTransaction,
    submitRepaymentTransaction,
    submitGlTransaction,
    submitPayOffTransaction,
    submitQrisTransaction
});
//# sourceMappingURL=transactionCoreBanking.helper.js.map