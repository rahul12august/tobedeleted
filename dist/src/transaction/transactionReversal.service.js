"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const transaction_repository_1 = __importDefault(require("./transaction.repository"));
const logger_1 = __importStar(require("../logger"));
const deposit_repository_1 = __importDefault(require("../coreBanking/deposit.repository"));
const errors_1 = require("../common/errors");
const AppError_1 = require("../errors/AppError");
const transaction_util_1 = require("./transaction.util");
const transaction_enum_1 = require("./transaction.enum");
const loan_enum_1 = require("../coreBanking/loan.enum");
const loan_repository_1 = __importDefault(require("../coreBanking/loan.repository"));
const transactionCoreBanking_helper_1 = __importDefault(require("./transactionCoreBanking.helper"));
const transaction_helper_1 = __importDefault(require("./transaction.helper"));
const featureFlag_1 = require("../common/featureFlag");
const transaction_constant_1 = require("./transaction.constant");
const lodash_1 = require("lodash");
const constant_1 = require("../common/constant");
const reverseTransactionsByReferenceId = (referenceId, reverseInput) => __awaiter(void 0, void 0, void 0, function* () {
    let transactions = yield deposit_repository_1.default.getTransactionsByReferenceId(referenceId);
    transactions = transactions.filter(transaction_util_1.isDepositTransactionNotReverted);
    if (transactions.length > 0) {
        if (transactions[0].cardTransaction) {
            const cardTransaction = transactions[0].cardTransaction;
            yield deposit_repository_1.default.reverseCardTransaction(cardTransaction.cardToken, cardTransaction.externalReferenceId, {
                amount: cardTransaction.amount,
                externalReferenceId: cardTransaction.externalReferenceId
            });
        }
        else {
            yield deposit_repository_1.default.reverseTransaction(transactions[0].id, reverseInput);
        }
        // mambu revert transaction by stack, id and encodedKey is changed after each revert
        // so we query and revert the updated ones
        if (transactions.length > 1)
            reverseTransactionsByReferenceId(referenceId, reverseInput);
    }
});
const getCoreBankingTransactionIds = (id, creationDate) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const coreBankingTransactionId = yield deposit_repository_1.default.getTransactionsByInstructionId(id, creationDate);
        return coreBankingTransactionId.filter(transaction_util_1.isDepositTransactionNotReverted);
    }
    catch (error) {
        logger_1.default.error(`getCoreBankingTransactionIds: failed in fetching core banking ids for transactions: ${id}`);
        throw new AppError_1.TransferAppError(`Failed fetching core banking ids for transaction: ${id}`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.ERROR_WHILE_FETCHING_COREBANKING_ID);
    }
});
const mapToReverseTransaction = (transaction) => (Object.assign(Object.assign({}, transaction), { sourceAccountNo: transaction.beneficiaryAccountNo, beneficiaryAccountNo: transaction.sourceAccountNo, sourceAccountName: transaction.beneficiaryAccountName, beneficiaryAccountName: transaction.sourceAccountName, beneficiaryAccountType: transaction.sourceAccountType, sourceAccountType: transaction.beneficiaryAccountType, sourceBankCode: transaction.beneficiaryBankCode, beneficiaryBankCode: transaction.sourceBankCode, sourceCIF: transaction.beneficiaryCIF, beneficiaryCIF: transaction.sourceCIF }));
const setCategoryCode = (type, transactionModel) => {
    if (type.includes('CREDITED'))
        return 'C056';
    else if (type.includes('DEBITED'))
        return 'C057';
    else
        return transactionModel.categoryCode;
};
const invertWithdrawalTransaction = (reverseTransaction, coreBankingTransaction) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    let creditTransactionChannel, creditTransactionCode, transactionAmount;
    if (coreBankingTransaction &&
        [
            transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_CREDITED,
            transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_DEBITED
        ].includes(coreBankingTransaction.type)) {
        creditTransactionChannel = reverseTransaction.creditTransactionChannel
            ? reverseTransaction.creditTransactionChannel
            : reverseTransaction.debitTransactionChannel;
        if (reverseTransaction.debitPriority &&
            reverseTransaction.creditTransactionCodeMapping &&
            ((_a = reverseTransaction.creditTransactionCodeMapping) === null || _a === void 0 ? void 0 : _a.length) > 0) {
            const creditTransactionCodeInfo = (_b = reverseTransaction.creditTransactionCodeMapping) === null || _b === void 0 ? void 0 : _b.find(item => item.interchange === reverseTransaction.debitPriority);
            creditTransactionCode =
                creditTransactionCodeInfo && creditTransactionCodeInfo.code
                    ? creditTransactionCodeInfo.code
                    : reverseTransaction.debitTransactionCode + transaction_constant_1.REVERSAL_TC_SUFFIX;
        }
        else {
            creditTransactionCode = reverseTransaction.creditTransactionCode
                ? reverseTransaction.creditTransactionCode
                : reverseTransaction.debitTransactionCode + transaction_constant_1.REVERSAL_TC_SUFFIX;
        }
        transactionAmount = reverseTransaction.transactionAmount;
    }
    else if (coreBankingTransaction &&
        reverseTransaction.fees &&
        !lodash_1.isEmpty(reverseTransaction.fees) &&
        [
            transaction_enum_1.TransferJourneyStatusEnum.FEE_AMOUNT_CREDITED,
            transaction_enum_1.TransferJourneyStatusEnum.FEE_AMOUNT_DEBITED
        ].includes(coreBankingTransaction.type)) {
        creditTransactionChannel = reverseTransaction.fees[0].customerTcChannel
            ? reverseTransaction.fees[0].customerTcChannel
            : reverseTransaction.creditTransactionChannel;
        creditTransactionCode = reverseTransaction.fees[0].customerTc
            ? reverseTransaction.fees[0].customerTc + transaction_constant_1.REVERSAL_TC_SUFFIX
            : reverseTransaction.creditTransactionCode;
        transactionAmount = reverseTransaction.fees[0].feeAmount
            ? reverseTransaction.fees[0].feeAmount
            : reverseTransaction.transactionAmount;
    }
    reverseTransaction.categoryCode = setCategoryCode(coreBankingTransaction.type, reverseTransaction);
    logger_1.default.info(`invertWithdrawalTransaction: credit transaction for beneficiaryAccountNo ${reverseTransaction.beneficiaryAccountNo} with 
      creditTransactionChannel: ${creditTransactionChannel},
      creditTransactionCode: ${creditTransactionCode}`);
    if (transactionAmount &&
        creditTransactionCode &&
        creditTransactionChannel &&
        reverseTransaction.beneficiaryAccountNo) {
        const creditTransactionId = yield transactionCoreBanking_helper_1.default.submitCreditTransaction(reverseTransaction.beneficiaryAccountNo, Object.assign(Object.assign({}, reverseTransaction), { transactionAmount: transactionAmount }), creditTransactionCode, creditTransactionChannel);
        return creditTransactionId;
    }
    logger_1.default.warn(`Ignoring invertWithdrawalTransaction as conditions does not matched`);
    return;
});
const addInverseTransactionToTransferJourney = (transactionModel, journeyType, coreBankingId) => {
    var _a, _b, _c, _d;
    coreBankingId &&
        !((_a = transactionModel.coreBankingTransactionIds) === null || _a === void 0 ? void 0 : _a.includes(coreBankingId)) && ((_b = transactionModel.coreBankingTransactionIds) === null || _b === void 0 ? void 0 : _b.push(coreBankingId));
    if (coreBankingId &&
        !((_c = transactionModel.coreBankingTransactions) === null || _c === void 0 ? void 0 : _c.map(item => item.id).includes(coreBankingId))) {
        coreBankingId && ((_d = transactionModel.coreBankingTransactions) === null || _d === void 0 ? void 0 : _d.push({
            id: coreBankingId,
            type: (journeyType.toString() +
                '_REVERSED')
        }));
    }
    transactionModel.journey = transaction_helper_1.default.getStatusesToBeUpdated(journeyType, transactionModel);
};
exports.invertDepositTransaction = (model) => __awaiter(void 0, void 0, void 0, function* () {
    model.debitTransactionChannel = model.debitTransactionChannel
        ? model.debitTransactionChannel
        : model.creditTransactionChannel;
    model.debitTransactionCode = model.debitTransactionCode
        ? model.debitTransactionCode
        : model.creditTransactionCode;
    logger_1.default.info(`invertDepositTransaction: debit transaction for sourceAccountNo ${model.sourceAccountNo} with 
      debitTransactionChannel: ${model.debitTransactionChannel},
      debitTransactionCode: ${model.debitTransactionChannel}`);
    try {
        if (model.debitTransactionCode &&
            model.debitTransactionChannel &&
            model.sourceAccountNo) {
            const debitTransactionId = yield transactionCoreBanking_helper_1.default.submitDebitTransaction(model.sourceAccountNo, model, model.debitTransactionCode, model.debitTransactionChannel);
            return debitTransactionId;
        }
        logger_1.default.warn(`Ignoring invertDepositTransaction as conditions does not matched`);
        return;
    }
    catch (err) {
        logger_1.default.error(`invertDepositTransaction: Error while debiting money, code: ${err.code}, message: ${err.message}`);
        throw new AppError_1.RetryableTransferAppError(`Error while debiting money, code: ${err.code}, message: ${err.message}!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.COREBANKING_DEBIT_FAILED, AppError_1.isRetryableError(err.status));
    }
    return;
});
const reverseTransactionsByInstructionId = (transactionId, reversalReason, transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    var _c;
    if (!(transactionModel && transactionModel.createdAt)) {
        logger_1.default.error(`reverseTransactionsByInstructionId: creation date did not find for transactions: ${transactionId}`);
        throw new AppError_1.TransferAppError(`Creation date not found for transactions: ${transactionId}`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.ERROR_WHILE_FETCHING_COREBANKING_ID);
    }
    let coreBankingTransactionIds = yield getCoreBankingTransactionIds(transactionId, transactionModel.createdAt);
    if (coreBankingTransactionIds.length > 0) {
        logger_1.default.info(`reverseTransactionsByInstructionId: number of transactions received from corebanking is ${coreBankingTransactionIds.length} for instructionId ${transactionId}`);
        const reverseTransaction = mapToReverseTransaction(transactionModel);
        const isNonBlockingInverse = featureFlag_1.isFeatureEnabled(constant_1.FEATURE_FLAG.ENABLE_NON_BLOCKING_INVERSE_TRANSACTION_ON_REVERSAL);
        for (let i = 0; i < coreBankingTransactionIds.length; i++) {
            let reversalInput = undefined;
            if (reversalReason) {
                reversalInput = {
                    notes: reversalReason
                };
            }
            logger_1.default.info(`reverseTransactionsByInstructionId: reversing transaction corebanking id ${coreBankingTransactionIds[i].id} for instructionId ${transactionId}, isNonBlockingInverse: ${isNonBlockingInverse}`);
            const coreTransactionDetail = (_c = reverseTransaction.coreBankingTransactions) === null || _c === void 0 ? void 0 : _c.find(item => item.id === coreBankingTransactionIds[i].id);
            if (isNonBlockingInverse &&
                coreTransactionDetail &&
                coreBankingTransactionIds[i].type == transaction_enum_1.CoreBankTransactionType.WITHDRAWAL) {
                // Creating inverse transaction of Withdrawal
                const creditTransactionId = yield invertWithdrawalTransaction(reverseTransaction, coreTransactionDetail);
                logger_1.default.info(`reverseTransactionsByInstructionId: reverted original transaction for 
              instructionId: ${transactionId},
              transactionId: ${coreBankingTransactionIds[i].id}
           and got creditTransactionId ${creditTransactionId}`);
                creditTransactionId &&
                    addInverseTransactionToTransferJourney(transactionModel, coreTransactionDetail.type, creditTransactionId);
            }
            else if (isNonBlockingInverse &&
                coreTransactionDetail &&
                coreBankingTransactionIds[i].type == transaction_enum_1.CoreBankTransactionType.DEPOSIT) {
                // Creating inverse transaction of Deposit
                const debitTransactionId = yield exports.invertDepositTransaction(reverseTransaction);
                logger_1.default.info(`reverseTransactionsByInstructionId: reverted original transaction for
              instructionId ${transactionId},
              transactionId: ${coreBankingTransactionIds[i].id}
           and got debitTransactionId ${debitTransactionId}`);
                coreTransactionDetail &&
                    debitTransactionId &&
                    addInverseTransactionToTransferJourney(transactionModel, coreTransactionDetail.type, debitTransactionId);
            }
            else {
                // Adjusting original transaction
                yield deposit_repository_1.default.reverseTransaction(coreBankingTransactionIds[i].id, reversalInput);
                logger_1.default.info(`reverseTransactionsByInstructionId: reverted settlementTransactionId ${coreBankingTransactionIds[i].id} for instructionId ${transactionId}`);
            }
        }
        yield transaction_repository_1.default.update(transactionModel.id, {
            coreBankingTransactionIds: transactionModel.coreBankingTransactionIds,
            coreBankingTransactions: transactionModel.coreBankingTransactions,
            journey: transactionModel.journey
        });
        logger_1.default.info(`reverseTransactionsByInstructionId: reversed all transactions from coreBankingTransactionIds.`);
    }
});
const reverseTransaction = (transactionId, reverseInput) => __awaiter(void 0, void 0, void 0, function* () {
    const transaction = yield transaction_repository_1.default.getByKey(transactionId);
    if (!transaction) {
        logger_1.default.error(`reverseTransaction: Matching txn was not found for transactionId: ${transactionId}`);
        throw new AppError_1.TransferAppError(`Matching txn was not found for transactionId: ${transactionId}`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.TRANSACTION_NOT_FOUND);
    }
    const referenceId = transaction.referenceId;
    logger_1.default.debug(`calling reverseTransactionsByReferenceId with retry, reference id: ${referenceId}`);
    try {
        yield module_common_1.Util.retry(transaction_util_1.RetryConfig(), reverseTransactionsByReferenceId, referenceId, reverseInput);
        yield transaction_repository_1.default.updateTransactionsByReferenceId(referenceId, {
            status: transaction_enum_1.TransactionStatus.REVERTED
        });
    }
    catch (error) {
        // TODO: handle reverting failure, e.g update status for expected error, or retry reverting with cron job, or require manual intervention
        logger_1.default.error('error reverting transaction: ', error);
        throw new AppError_1.TransferAppError(`Error reverting transaction requires manual intervention!`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.CAN_NOT_REVERSE_TRANSACTION);
    }
});
const reverseLoanTransaction = (transactionId, reversalReason, transaction) => __awaiter(void 0, void 0, void 0, function* () {
    const revertTransaction = [];
    if (!transaction) {
        throw new AppError_1.TransferAppError('Transaction not provided!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.TRANSACTION_NOT_FOUND);
    }
    const coreBankingTransactionIds = transaction.coreBankingTransactions || [];
    if (coreBankingTransactionIds.length > 0) {
        logger_1.default.info(`reverseLoanTransaction: number of transactions received from corebanking ${coreBankingTransactionIds.length} for transactionId ${transactionId}`);
        for (const transactionIdData of coreBankingTransactionIds) {
            logger_1.default.info(`reverseLoanTransaction: reversing transaction corebanking id ${transactionIdData.id} for transactionId ${transactionId}`);
            switch (transactionIdData.type) {
                case transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_DISBURSED:
                    const reversalInputDisbursement = {
                        type: loan_enum_1.AdjustLoanTransactionTypeEnum.UNDO_DISBURSEMENT
                    };
                    if (reversalReason) {
                        reversalInputDisbursement.notes = reversalReason;
                    }
                    revertTransaction.push(loan_repository_1.default.adjustLoanTransaction(transaction.sourceAccountNo || '', reversalInputDisbursement));
                    break;
                case transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_REPAID:
                    const reversalInputRepayment = {
                        type: loan_enum_1.AdjustLoanTransactionTypeEnum.UNDO_REPAYMENT,
                        originalTransactionId: transactionIdData.id
                    };
                    if (reversalReason) {
                        reversalInputRepayment.notes = reversalReason;
                    }
                    revertTransaction.push(loan_repository_1.default.adjustLoanTransaction(transaction.beneficiaryAccountNo || '', reversalInputRepayment));
                    break;
                case transaction_enum_1.TransferJourneyStatusEnum.GL_TRANSACTION_SUCCESS:
                    const reversalTransactionId = `${transactionIdData.id}-reversal`;
                    transactionCoreBanking_helper_1.default.submitGlTransaction(transaction, true, reversalTransactionId);
                    break;
                default:
                    let reversalInput = undefined;
                    if (reversalReason) {
                        reversalInput = {
                            notes: reversalReason
                        };
                    }
                    revertTransaction.push(deposit_repository_1.default.reverseTransaction(transactionIdData.id, reversalInput));
                    break;
            }
        }
    }
    yield Promise.all(revertTransaction);
});
const transactionReversalService = logger_1.wrapLogs({
    reverseTransaction,
    reverseTransactionsByInstructionId,
    reverseLoanTransaction
});
exports.default = transactionReversalService;
//# sourceMappingURL=transactionReversal.service.js.map