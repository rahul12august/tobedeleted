"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("joi-extract-type");
const errors_1 = require("../common/errors");
const AppError_1 = require("../errors/AppError");
const transaction_enum_1 = require("./transaction.enum");
class RuleConditionStatus {
    constructor(message, errorCode) {
        this.isSuccess = () => {
            return !this.errorCode;
        };
        this.message = message;
        this.errorCode = errorCode;
    }
    toError() {
        if (this.isSuccess()) {
            throw new AppError_1.TransferAppError('toError cannot be called when status is success.', transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.UNEXPECTED_ERROR);
        }
        const code = this.errorCode || errors_1.ERROR_CODE.UNEXPECTED_ERROR;
        return new AppError_1.TransferAppError(this.message, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, code, [
            {
                message: this.message,
                key: code,
                code: code
            }
        ]);
    }
}
exports.RuleConditionStatus = RuleConditionStatus;
//# sourceMappingURL=transaction.type.js.map