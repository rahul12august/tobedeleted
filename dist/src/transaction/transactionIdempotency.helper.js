"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_repository_1 = __importDefault(require("./transaction.repository"));
const logger_1 = require("../logger");
const transaction_enum_1 = require("./transaction.enum");
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const validateExistTransactionStatus = (existsTransaction) => {
    switch (existsTransaction.status) {
        case transaction_enum_1.TransactionStatus.SUCCEED:
            return existsTransaction;
        case transaction_enum_1.TransactionStatus.PENDING:
        case transaction_enum_1.TransactionStatus.SUBMITTED:
        case transaction_enum_1.TransactionStatus.SUBMITTING:
            throw new AppError_1.TransferAppError('The transaction still in progress', transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.TRANSACTION_STILL_IN_PROGRESS);
        case transaction_enum_1.TransactionStatus.REVERTED:
            throw new AppError_1.TransferAppError(`Transaction already reversed : ${existsTransaction.id}, externalId : ${existsTransaction.externalId}`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.TRANSACTION_ALREADY_REVERSED);
        case transaction_enum_1.TransactionStatus.REVERSAL_FAILED:
        case transaction_enum_1.TransactionStatus.DECLINED:
            const failureReason = existsTransaction.failureReason;
            const detail = failureReason && failureReason.detail
                ? failureReason.detail
                : 'Transaction declined';
            const actor = failureReason && failureReason.actor
                ? failureReason.actor
                : transaction_enum_1.TransferFailureReasonActor.UNKNOWN;
            const errorCode = failureReason && failureReason.type
                ? failureReason.type
                : errors_1.ERROR_CODE.UNEXPECTED_ERROR;
            throw new AppError_1.TransferAppError(detail, actor, errorCode);
        default:
            // Should never happen
            throw new AppError_1.TransferAppError('Idempotency API error. Can not determine the response to answer', transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.UNEXPECTED_ERROR);
    }
};
const matchExistingTransactionByIdempotencyKey = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    if (transactionModel.idempotencyKey) {
        const existingTransaction = yield transaction_repository_1.default.findOneByQuery({
            idempotencyKey: transactionModel.idempotencyKey
        });
        if (existingTransaction &&
            existingTransaction.externalId === transactionModel.externalId) {
            return validateExistTransactionStatus(existingTransaction);
        }
    }
    return null;
});
const transactionIdempotencyHelper = {
    matchExistingTransactionByIdempotencyKey
};
exports.default = logger_1.wrapLogs(transactionIdempotencyHelper);
//# sourceMappingURL=transactionIdempotency.helper.js.map