"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const errors_1 = require("../common/errors");
const AppError_1 = require("../errors/AppError");
const lodash_1 = require("lodash");
const transaction_enum_1 = require("./transaction.enum");
const logAndThrow = (message) => {
    logger_1.default.error(message);
    throw new AppError_1.AppError(errors_1.ERROR_CODE.INVALID_REFUND_REQUEST);
};
const validateRefundRequest = (payload, originalTransaction) => {
    if (lodash_1.head(lodash_1.split(originalTransaction.paymentServiceType, '_')) !==
        lodash_1.head(lodash_1.split(payload.paymentServiceType, '_'))) {
        logAndThrow(`validateRefundRequest: paymentServiceType missmatch ${originalTransaction.paymentServiceType}`);
    }
    if (originalTransaction.status !== transaction_enum_1.TransactionStatus.SUCCEED) {
        logAndThrow(`validateRefundRequest: Invalid Transaction Status ${originalTransaction.status}.`);
    }
    if (payload.beneficiaryAccountNo !== originalTransaction.sourceAccountNo) {
        logAndThrow(`validateRefundRequest: Account mismatch.`);
    }
    // TODO: (Dikshit)commenting it for now can be enabled later once we will get nore calrity
    // if (
    //   payload.beneficiaryBankCode !== originalTransaction.sourceBankCode ||
    //   payload.sourceBankCode !== originalTransaction.beneficiaryBankCode
    // ) {
    //   logAndThrow(`validateRefundRequest: Bank Code mismatch.`);
    // }
    if (payload.transactionAmount > originalTransaction.transactionAmount) {
        logAndThrow(`validateRefundRequest: Transaction Amount greater than original transaction Amount.`);
    }
    if (originalTransaction.refund &&
        lodash_1.isNumber(originalTransaction.refund.remainingAmount) &&
        payload.transactionAmount > originalTransaction.refund.remainingAmount) {
        logAndThrow(`validateRefundRequest: Refund amount mismatch, refund.remainingAmount:${originalTransaction.refund.remainingAmount}`);
    }
    logger_1.default.info(`validateRefundRequest: Validated refunded transaction of originalTransaction:${originalTransaction.id} complete.`);
};
const transactionExternalValidator = logger_1.wrapLogs({
    validateRefundRequest
});
exports.default = transactionExternalValidator;
//# sourceMappingURL=transactionExternal.validator.js.map