"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_util_1 = require("./transaction.util");
const logger_1 = require("../logger");
const lodash_1 = require("lodash");
const getFeeTransactionInputs = (transactionModel) => {
    const fees = transactionModel.fees || [];
    const feeInputs = [];
    for (let i = 0; i < fees.length; i++) {
        if (fees[i].feeAmount > 0) {
            const feeTransaction = transaction_util_1.mapToCoreBankingTransaction(Object.assign(Object.assign({}, transactionModel), { transactionAmount: fees[i].feeAmount, categoryCode: undefined // force enrichment get default category code for fee
             }), fees[i].customerTc, fees[i].customerTcChannel);
            feeInputs.push({
                feeTransactionPayload: feeTransaction,
                feeIdempotencyKey: fees[i].idempotencyKey
            });
        }
    }
    return feeInputs;
};
const getFeeCardTransactionInputs = (transactionModel) => {
    const fees = transactionModel.fees || [];
    const feeInputs = [];
    for (let fee of fees) {
        if (fee.feeAmount > 0 && fee.blockingId) {
            const feeTransaction = transaction_util_1.mapToCoreBankingCardTransaction(Object.assign(Object.assign({}, transactionModel), { transactionAmount: fee.feeAmount, categoryCode: undefined // force enrichment get default category code for fee
             }), fee.customerTc, fee.customerTcChannel, fee.blockingId);
            feeInputs.push({
                feeCardTransactionPayload: feeTransaction,
                feeCardIdempotencyKey: fee.idempotencyKey
            });
        }
    }
    return feeInputs;
};
const isGLTransaction = (fee) => fee.subsidiary && lodash_1.isNumber(fee.subsidiaryAmount) && fee.subsidiaryAmount > 0;
const getJournalEntriesGLTransactionInput = (model) => {
    const fees = model.fees || [];
    const entries = [];
    for (let fee of fees) {
        if (isGLTransaction(fee)) {
            entries.push(transaction_util_1.mapDataJournalEntriesGL(fee, model.id));
        }
    }
    return entries;
};
const feeHelper = {
    getFeeTransactionInputs,
    getJournalEntriesGLTransactionInput,
    getFeeCardTransactionInputs
};
exports.default = logger_1.wrapLogs(feeHelper);
//# sourceMappingURL=transactionFee.helper.js.map