"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// TODO The value is decided by the value of blocking flag in https://dkatalis.atlassian.net/browse/DGB-9136
var ExecutionTypeEnum;
(function (ExecutionTypeEnum) {
    ExecutionTypeEnum["BLOCKING"] = "BLOCKING";
    ExecutionTypeEnum["NONE_BLOCKING"] = "NONE_BLOCKING";
})(ExecutionTypeEnum = exports.ExecutionTypeEnum || (exports.ExecutionTypeEnum = {}));
var PhoneCode;
(function (PhoneCode) {
    PhoneCode["PLUS62"] = "+62";
})(PhoneCode = exports.PhoneCode || (exports.PhoneCode = {}));
var TransactionStatus;
(function (TransactionStatus) {
    TransactionStatus["PENDING"] = "PENDING";
    TransactionStatus["SUCCEED"] = "SUCCEED";
    TransactionStatus["REVERTED"] = "REVERTED";
    TransactionStatus["DECLINED"] = "DECLINED";
    TransactionStatus["REVERSAL_FAILED"] = "REVESAL_FAILED";
    TransactionStatus["SUBMITTED"] = "SUBMITTED";
    TransactionStatus["SUBMITTING"] = "SUBMITTING";
})(TransactionStatus = exports.TransactionStatus || (exports.TransactionStatus = {}));
var TransferFailureReasonActor;
(function (TransferFailureReasonActor) {
    TransferFailureReasonActor["SUBMITTER"] = "SUBMITTER";
    TransferFailureReasonActor["JAGO"] = "JAGO";
    TransferFailureReasonActor["MS_TRANSFER"] = "MS_TRANSFER";
    TransferFailureReasonActor["MS_BILL_PAYMENT"] = "MS_BILL_PAYMENT";
    TransferFailureReasonActor["MS_ENTITLEMENT"] = "MS_ENTITLEMENT";
    TransferFailureReasonActor["TOKOPEDIA"] = "TOKOPEDIA";
    TransferFailureReasonActor["EURONET"] = "EURONET";
    TransferFailureReasonActor["SKN"] = "SKN";
    TransferFailureReasonActor["SKN_SYARIAH"] = "SKN_SYARIAH";
    TransferFailureReasonActor["QRIS"] = "QRIS";
    TransferFailureReasonActor["RTGS"] = "RTGS";
    TransferFailureReasonActor["RTGS_SYARIAH"] = "RTGS_SYARIAH";
    TransferFailureReasonActor["IRIS"] = "IRIS";
    TransferFailureReasonActor["ALTO"] = "ALTO";
    TransferFailureReasonActor["BIFAST"] = "BIFAST";
    TransferFailureReasonActor["UNKNOWN"] = "UNKNOWN";
})(TransferFailureReasonActor = exports.TransferFailureReasonActor || (exports.TransferFailureReasonActor = {}));
var TransferJourneyStatusEnum;
(function (TransferJourneyStatusEnum) {
    TransferJourneyStatusEnum["REQUEST_RECEIVED"] = "REQUEST_RECEIVED";
    TransferJourneyStatusEnum["INQUIRY_STARTED"] = "INQUIRY_STARTED";
    TransferJourneyStatusEnum["INQUIRY_FETCHED"] = "INQUIRY_FETCHED";
    TransferJourneyStatusEnum["FETCHING_RECOMMENDATION"] = "FETCHING_RECOMMENDATION";
    TransferJourneyStatusEnum["RECOMMENDATION_FETCHED"] = "RECOMMENDATION_FETCHED";
    TransferJourneyStatusEnum["AMOUNT_BLOCKED"] = "AMOUNT_BLOCKED";
    TransferJourneyStatusEnum["SUBMITTING_THIRD_PARTY"] = "SUBMITTING_THIRD_PARTY";
    TransferJourneyStatusEnum["THIRD_PARTY_SUBMITTED"] = "THIRD_PARTY_SUBMITTED";
    TransferJourneyStatusEnum["TRANSFER_CONFIRMATION_RECEIVED"] = "TRANSFER_CONFIRMATION_RECEIVED";
    TransferJourneyStatusEnum["AMOUNT_DEBITED"] = "AMOUNT_DEBITED";
    TransferJourneyStatusEnum["AMOUNT_CREDITED"] = "AMOUNT_CREDITED";
    TransferJourneyStatusEnum["AMOUNT_CREDITED_SETTLEMENT"] = "AMOUNT_CREDITED_SETTLEMENT";
    TransferJourneyStatusEnum["AMOUNT_DEBITED_SETTLEMENT"] = "AMOUNT_DEBITED_SETTLEMENT";
    TransferJourneyStatusEnum["AMOUNT_CREDITED_REVERSED"] = "AMOUNT_CREDITED_REVERSED";
    TransferJourneyStatusEnum["AMOUNT_DEBITED_REVERSED"] = "AMOUNT_DEBITED_REVERSED";
    TransferJourneyStatusEnum["FEE_AMOUNT_DEBITED"] = "FEE_AMOUNT_DEBITED";
    TransferJourneyStatusEnum["FEE_AMOUNT_CREDITED"] = "FEE_AMOUNT_CREDITED";
    TransferJourneyStatusEnum["FEE_AMOUNT_DEBITED_REVERSED"] = "FEE_AMOUNT_DEBITED_REVERSED";
    TransferJourneyStatusEnum["FEE_PROCESSED"] = "FEE_PROCESSED";
    TransferJourneyStatusEnum["TRANSFER_SUCCESS"] = "TRANSFER_SUCCESS";
    TransferJourneyStatusEnum["TRANSFER_FAILURE"] = "TRANSFER_FAILURE";
    TransferJourneyStatusEnum["ADJUST_BLOCKING_AMOUNT"] = "ADJUST_BLOCKING_AMOUNT";
    TransferJourneyStatusEnum["BLOCKING_AMOUNT_ADJUSTED"] = "BLOCKING_AMOUNT_ADJUSTED";
    TransferJourneyStatusEnum["AMOUNT_DISBURSED"] = "AMOUNT_DISBURSED";
    TransferJourneyStatusEnum["AMOUNT_REPAID"] = "AMOUNT_REPAID";
    TransferJourneyStatusEnum["GL_TRANSACTION_SUCCESS"] = "GL_TRANSACTION_SUCCESS";
    TransferJourneyStatusEnum["CLOSE_ACCOUNT_POCKET_TO_GL_TRANSACTION"] = "CLOSE_ACCOUNT_POCKET_TO_GL_TRANSACTION";
    TransferJourneyStatusEnum["REFUND_CREDITED"] = "REFUND_CREDITED";
    TransferJourneyStatusEnum["CHECK_STATUS"] = "CHECK_STATUS";
})(TransferJourneyStatusEnum = exports.TransferJourneyStatusEnum || (exports.TransferJourneyStatusEnum = {}));
var BankCodeTypes;
(function (BankCodeTypes) {
    BankCodeTypes["REMITTANCE"] = "REMITTANCE";
    BankCodeTypes["BILLER"] = "BILLER";
    BankCodeTypes["RTOL"] = "RTOL";
    BankCodeTypes["IRIS"] = "IRIS";
})(BankCodeTypes = exports.BankCodeTypes || (exports.BankCodeTypes = {}));
var ManualAdjustReqStatus;
(function (ManualAdjustReqStatus) {
    ManualAdjustReqStatus["SUCCEED"] = "SUCCEED";
    ManualAdjustReqStatus["DECLINED"] = "DECLINED";
})(ManualAdjustReqStatus = exports.ManualAdjustReqStatus || (exports.ManualAdjustReqStatus = {}));
var PaymentRequestStatus;
(function (PaymentRequestStatus) {
    PaymentRequestStatus["SUCCEED"] = "SUCCEED";
    PaymentRequestStatus["DECLINED"] = "DECLINED";
})(PaymentRequestStatus = exports.PaymentRequestStatus || (exports.PaymentRequestStatus = {}));
var ConfirmTransactionStatus;
(function (ConfirmTransactionStatus) {
    ConfirmTransactionStatus[ConfirmTransactionStatus["SUCCESS"] = 200] = "SUCCESS";
    ConfirmTransactionStatus[ConfirmTransactionStatus["ERROR_EXPECTED"] = 400] = "ERROR_EXPECTED";
    ConfirmTransactionStatus[ConfirmTransactionStatus["ERROR_UNEXPECTED"] = 500] = "ERROR_UNEXPECTED";
    ConfirmTransactionStatus[ConfirmTransactionStatus["REQUEST_TIMEOUT"] = 408] = "REQUEST_TIMEOUT";
})(ConfirmTransactionStatus = exports.ConfirmTransactionStatus || (exports.ConfirmTransactionStatus = {}));
var GlTransactionTypes;
(function (GlTransactionTypes) {
    GlTransactionTypes["CREDIT"] = "CREDIT";
    GlTransactionTypes["DEBIT"] = "DEBIT";
})(GlTransactionTypes = exports.GlTransactionTypes || (exports.GlTransactionTypes = {}));
var CustomerEntitlementMode;
(function (CustomerEntitlementMode) {
    CustomerEntitlementMode["PREVIEW"] = "PREVIEW";
    CustomerEntitlementMode["CONSUME"] = "CONSUME";
})(CustomerEntitlementMode = exports.CustomerEntitlementMode || (exports.CustomerEntitlementMode = {}));
var CoreBankTransactionType;
(function (CoreBankTransactionType) {
    CoreBankTransactionType["WITHDRAWAL"] = "WITHDRAWAL";
    CoreBankTransactionType["DEPOSIT"] = "DEPOSIT";
})(CoreBankTransactionType = exports.CoreBankTransactionType || (exports.CoreBankTransactionType = {}));
//# sourceMappingURL=transaction.enum.js.map