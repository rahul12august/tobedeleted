"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = require("../logger");
const transactionSubmissionSwitchingTemplate_service_1 = __importDefault(require("./transactionSubmissionSwitchingTemplate.service"));
const billingAggregatorSubmitPaymentTemplate_service_1 = __importDefault(require("../billingAggregator/billingAggregatorSubmitPaymentTemplate.service"));
const irisSubmissionTemplate_service_1 = __importDefault(require("../iris/irisSubmissionTemplate.service"));
const rtgsSubmissionTemplate_service_1 = __importDefault(require("../rtgs/rtgsSubmissionTemplate.service"));
const sknSubmissionTemplate_service_1 = __importDefault(require("../skn/sknSubmissionTemplate.service"));
const tokopediaSubmissionTemplate_service_1 = __importDefault(require("../tokopedia/tokopediaSubmissionTemplate.service"));
const syariahRtgsSubmissionTemplate_service_1 = __importDefault(require("../rtgs/syariahRtgsSubmissionTemplate.service"));
const syariahSknSubmissionTemplate_service_1 = __importDefault(require("../skn/syariahSknSubmissionTemplate.service"));
const bifastSubmissionTemplate_service_1 = __importDefault(require("../bifast/bifastSubmissionTemplate.service"));
const templates = [
    bifastSubmissionTemplate_service_1.default,
    rtgsSubmissionTemplate_service_1.default,
    syariahRtgsSubmissionTemplate_service_1.default,
    sknSubmissionTemplate_service_1.default,
    syariahSknSubmissionTemplate_service_1.default,
    billingAggregatorSubmitPaymentTemplate_service_1.default,
    tokopediaSubmissionTemplate_service_1.default,
    irisSubmissionTemplate_service_1.default,
    transactionSubmissionSwitchingTemplate_service_1.default
];
/**
 * Get eligible {@link ITransactionSubmissionTemplate} for a given transaction model
 *
 * @param model transaction model
 * @returns the submission template or undefined if it does not has
 */
const getSubmissionTemplate = (model) => {
    const template = templates.find(template => template.isEligible(model));
    return template;
};
const outgoingSubmissionTemplateService = logger_1.wrapLogs({
    getSubmissionTemplate
});
exports.default = outgoingSubmissionTemplateService;
//# sourceMappingURL=transactionSubmissionTemplate.service.js.map