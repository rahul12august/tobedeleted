"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const lodash_1 = require("lodash");
const module_common_1 = require("@dk/module-common");
const transaction_constant_1 = require("./transaction.constant");
const logger_1 = __importStar(require("../logger"));
const transaction_repository_1 = __importDefault(require("./transaction.repository"));
const isRefundRequest = (payload) => lodash_1.includes([
    module_common_1.PaymentServiceTypeEnum.DOMESTIC_VOID_PAYMENT,
    module_common_1.PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT
], payload.paymentServiceType);
const getOriginalTransaction = (payload) => __awaiter(void 0, void 0, void 0, function* () {
    if (!(payload.notes &&
        payload.notes.length === transaction_constant_1.EURONET_REFUND_EXTERNAL_ID_LENGTH)) {
        logger_1.default.error('validateRefundRequest: notes is required for refund type req');
        throw new AppError_1.AppError(errors_1.ERROR_CODE.INVALID_REFUND_REQUEST);
    }
    const originalTransaction = yield transaction_repository_1.default.getIncomingNonRefunded(payload.notes, payload.beneficiaryAccountNo);
    if (!originalTransaction) {
        logger_1.default.error(`validateRefundRequest: No transaction found for refund request with external Id : ${payload.notes}`);
        throw new AppError_1.AppError(errors_1.ERROR_CODE.INVALID_REFUND_REQUEST);
    }
    return originalTransaction;
});
const transactionExternalHelper = logger_1.wrapLogs({
    isRefundRequest,
    getOriginalTransaction
});
exports.default = transactionExternalHelper;
//# sourceMappingURL=transactionExternal.helper.js.map