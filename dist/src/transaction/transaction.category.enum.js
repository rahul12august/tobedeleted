"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
exports.ATM_WITHDRAWAL_PAYMENT_SERVICE_TYPES = [
    module_common_1.PaymentServiceTypeEnum.THIRD_PARTY_ATM_WITHDRAWAL,
    module_common_1.PaymentServiceTypeEnum.INTERNATIONAL_ATM_WITHDRAWAL
];
exports.ATM_WITHDRAWAL_JAGO = {
    paymentServiceType: module_common_1.PaymentServiceTypeEnum.THIRD_PARTY_ATM_WITHDRAWAL,
    paymentServiceCode: 'JAGOATM_CASH_WITHDRAW',
    interchange: '-',
    //transactionCode: 'CWD02',
    limitGroupCode: 'L015'
};
exports.ATM_WITHDRAWAL_THIRD_PARTY_ALTO = {
    paymentServiceType: module_common_1.PaymentServiceTypeEnum.THIRD_PARTY_ATM_WITHDRAWAL,
    paymentServiceCode: 'THIRDPARTY_CASH_WITHDRAW',
    interchange: 'ALTO',
    //transactionCode: 'CWD03',
    limitGroupCode: 'L006'
};
exports.ATM_WITHDRAWAL_THIRD_PARTY_ARTAJASA = {
    paymentServiceType: module_common_1.PaymentServiceTypeEnum.THIRD_PARTY_ATM_WITHDRAWAL,
    paymentServiceCode: 'THIRDPARTY_CASH_WITHDRAW',
    interchange: 'ARTAJASA',
    //transactionCode: 'CWD03',
    limitGroupCode: 'L006'
};
exports.ATM_WITHDRAWAL_INTERNATIONAL = {
    paymentServiceType: module_common_1.PaymentServiceTypeEnum.INTERNATIONAL_ATM_WITHDRAWAL,
    paymentServiceCode: 'VISA_WITHDRAW',
    interchange: 'VISA',
    //transactionCode: 'CWD05',
    limitGroupCode: 'L006'
};
exports.ATM_WITHDRAWAL_PAYMENT_SERVICE_CODES = [
    exports.ATM_WITHDRAWAL_JAGO.paymentServiceCode,
    exports.ATM_WITHDRAWAL_THIRD_PARTY_ALTO.paymentServiceCode,
    exports.ATM_WITHDRAWAL_INTERNATIONAL.paymentServiceCode //international ATM withdrawal
];
exports.TRANSACTION_CATEGORY_ATM_WITHDRAWAL = [
    exports.ATM_WITHDRAWAL_JAGO,
    exports.ATM_WITHDRAWAL_THIRD_PARTY_ARTAJASA,
    exports.ATM_WITHDRAWAL_THIRD_PARTY_ALTO,
    exports.ATM_WITHDRAWAL_INTERNATIONAL
];
exports.ATM_TRANSFER_PAYMENT_SERVICE_TYPES = [
    module_common_1.PaymentServiceTypeEnum.THIRD_PARTY_TRANSFER,
    module_common_1.PaymentServiceTypeEnum.JAGOATM_TRANSFER
];
exports.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE = {
    paymentServiceType: module_common_1.PaymentServiceTypeEnum.JAGOATM_TRANSFER,
    paymentServiceCode: 'JAGOATM_JAGO_TO_JAGO',
    interchange: 'JAGO',
    //transactionCode: 'CWD02',
    limitGroupCode: 'L001'
};
exports.ATM_TRANSFER_JAGO_ATM_ARTAJASA_INTERCHANGE = {
    paymentServiceType: module_common_1.PaymentServiceTypeEnum.JAGOATM_TRANSFER,
    paymentServiceCode: 'JAGOATM_JAGO_TO_OTH',
    interchange: 'ARTAJASA',
    //transactionCode: 'CWD02',
    limitGroupCode: 'L002'
};
exports.ATM_TRANSFER_JAGO_ATM_ALTO_INTERCHANGE = {
    paymentServiceType: module_common_1.PaymentServiceTypeEnum.JAGOATM_TRANSFER,
    paymentServiceCode: 'JAGOATM_JAGO_TO_OTH',
    interchange: 'ALTO',
    //transactionCode: 'CWD02',
    limitGroupCode: 'L002'
};
exports.ATM_TRANSFER_THIRD_PARTY_ATM = {
    paymentServiceType: module_common_1.PaymentServiceTypeEnum.THIRD_PARTY_TRANSFER,
    paymentServiceCode: 'THIRDPARTY_JAGO_TO_OTH',
    interchange: '',
    //transactionCode: 'CWD03',
    limitGroupCode: 'L008'
};
exports.ATM_TRANSFER_PAYMENT_SERVICE_CODES = [
    exports.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
    exports.ATM_TRANSFER_JAGO_ATM_ALTO_INTERCHANGE.paymentServiceCode,
    exports.ATM_TRANSFER_THIRD_PARTY_ATM.paymentServiceCode //third party atm
];
exports.TRANSACTION_CATEGORY_ATM_TRANSFER = [
    exports.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE,
    exports.ATM_TRANSFER_JAGO_ATM_ARTAJASA_INTERCHANGE,
    exports.ATM_TRANSFER_JAGO_ATM_ALTO_INTERCHANGE,
    exports.ATM_TRANSFER_THIRD_PARTY_ATM
];
//# sourceMappingURL=transaction.category.enum.js.map