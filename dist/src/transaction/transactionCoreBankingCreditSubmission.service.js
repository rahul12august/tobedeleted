"use strict";
/**
 * @module transactionCoreBankingCreditSubmission
 *
 * A template for credit transaction submission to core banking
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const logger_1 = require("../logger");
const transactionCoreBanking_helper_1 = __importDefault(require("./transactionCoreBanking.helper"));
const transaction_enum_1 = require("./transaction.enum");
const module_common_2 = require("@dk/module-common");
const transaction_constant_1 = require("./transaction.constant");
const submitTransaction = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    if (transactionModel.creditTransactionCode &&
        transactionModel.creditTransactionChannel &&
        transactionModel.beneficiaryAccountNo) {
        const creditTransactionId = yield transactionCoreBanking_helper_1.default.submitCreditTransaction(transactionModel.beneficiaryAccountNo, transactionModel, transactionModel.creditTransactionCode, transactionModel.creditTransactionChannel);
        //TODO : refactor to use coreBankingTransactions field
        (_a = transactionModel.coreBankingTransactionIds) === null || _a === void 0 ? void 0 : _a.push(creditTransactionId);
        (_b = transactionModel.coreBankingTransactions) === null || _b === void 0 ? void 0 : _b.push({
            id: creditTransactionId,
            type: transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_CREDITED
        });
    }
    return transactionModel;
});
const isEligibleBankCodeChannel = (channel) => {
    // channel is PARTNER when external top-up.
    // when it's external top-up, amount will be credited to merchant's account
    if (module_common_1.BankChannelEnum.INTERNAL === channel ||
        module_common_1.BankChannelEnum.PARTNER === channel) {
        return true;
    }
    return false;
};
const isEligible = (model) => {
    if (model.creditTransactionCode &&
        model.creditTransactionChannel &&
        model.beneficiaryAccountNo &&
        isEligibleBankCodeChannel(model.beneficiaryBankCodeChannel) &&
        !model.creditTransactionCode.endsWith(transaction_constant_1.REVERSAL_TC_SUFFIX)) {
        return true;
    }
    return false;
};
const getRail = () => module_common_2.Rail.INTERNAL;
const internalCreditSubmissionTemplate = {
    submitTransaction,
    isEligible,
    getRail
};
exports.default = logger_1.wrapLogs(internalCreditSubmissionTemplate);
//# sourceMappingURL=transactionCoreBankingCreditSubmission.service.js.map