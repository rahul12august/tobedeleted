"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const switching_enum_1 = require("../switching/switching.enum");
const config_1 = require("../config");
const transaction_util_1 = require("./transaction.util");
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const logger_1 = __importDefault(require("../logger"));
const externalIdGenerator_util_1 = require("../common/externalIdGenerator.util");
const transaction_enum_1 = require("./transaction.enum");
const euronetConfig = config_1.config.get('switching').euronet;
exports.IN_SEQUENCE_MAX_LENGTH = 15;
const IN_ADD_DATA_PROTION_LENGTH = 20;
const MAX_SEQUENCE_COUNTER = 9999999999; // 10 digit number (julian date took 5 digits already)
const generateInSequenceId = (transactionCode) => __awaiter(void 0, void 0, void 0, function* () {
    if (!transactionCode) {
        const detail = `Missing debit transaction code!`;
        logger_1.default.error(`generateInSequenceId: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE);
    }
    const julianDate = externalIdGenerator_util_1.getJulianDate();
    const currentCounter = yield externalIdGenerator_util_1.getNextCounterByPrefix(`${julianDate}${transactionCode}`);
    if (currentCounter > MAX_SEQUENCE_COUNTER) {
        const detail = `Maximum outgoing ID counter is reached, cannot generate thirdPartyOutgoingId!`;
        logger_1.default.error(`generateInSequenceId: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.MAXIMUM_COUNTER_REACHED);
    }
    return `${julianDate}${currentCounter
        .toString()
        .padStart(exports.IN_SEQUENCE_MAX_LENGTH - julianDate.length, '0')}`;
});
const getCurrencyIsoNumber = (currencyCode) => __awaiter(void 0, void 0, void 0, function* () {
    if (!currencyCode) {
        const detail = 'Source transaction currency code is undefined!';
        logger_1.default.error(`generateInSequenceId: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.SOURCE_TRANSACTION_CURRENCY_UNDEFINED);
    }
    const currency = yield configuration_repository_1.default.getCurrencyByCode(currencyCode);
    if (!currency) {
        throw new AppError_1.TransferAppError('Transaction currency code was not found in configuration!', transaction_enum_1.TransferFailureReasonActor.JAGO, errors_1.ERROR_CODE.CURRENCY_NOT_FOUND);
    }
    return currency.isoNumber;
});
const createInAddDataPortion = (source) => {
    let value = '';
    for (let i = 0; i < IN_ADD_DATA_PROTION_LENGTH; i++) {
        const char = source.charAt(i);
        if (char) {
            value = `${value}${char}`;
        }
        else {
            value = `${value} `;
        }
    }
    return value;
};
exports.createInAddData = (transaction) => {
    const { sourceAccountName, beneficiaryAccountName } = transaction;
    return `${createInAddDataPortion(sourceAccountName || '')}${createInAddDataPortion(beneficiaryAccountName || '')}`;
};
exports.mapToSwitchingTransaction = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    const [inSequenceId, currencyIsoNumber] = yield Promise.all([
        generateInSequenceId(transaction.debitTransactionCode),
        getCurrencyIsoNumber(transaction.sourceTransactionCurrency)
    ]);
    return {
        inDeliveryChannel: switching_enum_1.inDeliveryChannelEnum.DEFAULT,
        inAuditData: {
            inApplication: euronetConfig.application,
            inUserId: euronetConfig.userId,
            inPassword: euronetConfig.password,
            inServiceID: switching_enum_1.inServiceIdEnum.FUND_TRANSFER,
            inSequence: inSequenceId
        },
        inProcessingCode: switching_enum_1.transactionProcessingCodeEnum.FUND_TRANSFER,
        inTxnAmt: transaction.transactionAmount.toString(),
        inTxnDateTime: transaction_util_1.formatTransactionDate(transaction.createdAt || new Date()),
        inSTAN: transaction.thirdPartyOutgoingId
            ? transaction.thirdPartyOutgoingId.substr(-6)
            : '',
        inAcqInstId: transaction.sourceRtolCode || '',
        inRRN: transaction.thirdPartyOutgoingId
            ? transaction.thirdPartyOutgoingId
            : '',
        inAddData: exports.createInAddData(transaction),
        inCurrCode: currencyIsoNumber,
        inAcctId1: transaction.sourceAccountNo || '',
        inAcctId2: transaction.beneficiaryAccountNo || '',
        inDestInstId: transaction.beneficiaryRtolCode || '',
        inSysOrg: switching_enum_1.inSysOrgEnum.DEFAULT,
        inTermID: switching_enum_1.inTermIdEnum.DEFAULT,
        inTermLoc: switching_enum_1.inTermLocEnum.DEFAULT
    };
});
//# sourceMappingURL=transactionSwitching.helper.js.map