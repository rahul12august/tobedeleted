"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = require("../logger");
const bifastStatus_template_1 = __importDefault(require("../bifast/bifastStatus.template"));
const templates = [bifastStatus_template_1.default];
const getStatusTemplate = (model) => {
    const template = templates.find(template => template.isEligible(model));
    return template;
};
const outgoingStatusTemplateService = logger_1.wrapLogs({
    getStatusTemplate
});
exports.default = outgoingStatusTemplateService;
//# sourceMappingURL=transactionStatus.template.js.map