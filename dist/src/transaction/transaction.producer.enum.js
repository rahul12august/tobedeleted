"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NotificationCode;
(function (NotificationCode) {
    NotificationCode["NOTIFICATION_FAILED_TRANSFER"] = "Notif_failed_transfer";
    NotificationCode["NOTIFICATION_PENDING_BILLPAYMENT"] = "Notif_Pending_BillPayment";
    NotificationCode["NOTIF_BLOCK_AMOUNT"] = "Notif_Block_Amount";
    NotificationCode["NOTIFICATION_PENDING_TRANSFER"] = "Notif_Pending_Transfer";
    NotificationCode["NOTIF_PENIDNG_SKNRTGS"] = "Notif_Pending_SKNRTGS";
    NotificationCode["NOTIF_FAILED_QRIS"] = "Notif_QRD01_FAILED";
    NotificationCode["NOTIF_PENDING_QRIS"] = "Notif_QRD01_PENDING";
    NotificationCode["NOTIF_SUCCESS_QRIS"] = "Notif_QRD01";
    NotificationCode["NOTIF_DEBIT_CARD_INSUFFICIENT_BALANCE"] = "Notif_Debit_Card_Insufficient_Balance";
    NotificationCode["NOTIF_DAILY_LIMIT_ATM_WITHDRAWAL"] = "Notif_Daily_Limit_Atm_Withdrawal_Exceeds_Limit";
    NotificationCode["NOTIF_DAILY_LIMIT_ATM_TRANSFER"] = "Notif_Daily_Limit_Atm_Transfer_Exceeds_Limit";
})(NotificationCode = exports.NotificationCode || (exports.NotificationCode = {}));
//# sourceMappingURL=transaction.producer.enum.js.map