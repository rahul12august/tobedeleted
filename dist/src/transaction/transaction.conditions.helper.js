"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const currencyFormatter_1 = require("../common/currencyFormatter");
const transaction_conditions_enum_1 = require("./transaction.conditions.enum");
const transaction_constant_1 = require("./transaction.constant");
const transaction_type_1 = require("./transaction.type");
const compareAmountWithDailyLimit = (limitGroup, transactionAmount, dailyAmountSumSoFar) => {
    if (transactionAmount + dailyAmountSumSoFar <= limitGroup.dailyLimitAmount) {
        return transaction_conditions_enum_1.RuleConditionState.ACCEPTED;
    }
    if (transactionAmount <= limitGroup.dailyLimitAmount) {
        return transaction_conditions_enum_1.RuleConditionState.TRY_TOMORROW;
    }
    return transaction_conditions_enum_1.RuleConditionState.DECLINED;
};
const constructDailyLimitEvaluatedRuleConditionStatus = (currentAmount, dailyAmountSumSoFar, tag, dailyLimit, errorCodeDeclined, errorCodeTryTomorrow, currentEvalState) => {
    const totalDailyUsageSoFar = currentAmount + dailyAmountSumSoFar;
    const firstPart = 'The sum of the total daily amount plus the current transaction is: ' +
        totalDailyUsageSoFar +
        ' which is ';
    const lastPart = ' the daily limit of ' + tag + ': ' + dailyLimit;
    if (currentEvalState === transaction_conditions_enum_1.RuleConditionState.ACCEPTED) {
        return new transaction_type_1.RuleConditionStatus(firstPart + 'in ' + lastPart);
    }
    const failureMessage = firstPart + 'beyond ' + lastPart;
    if (currentEvalState === transaction_conditions_enum_1.RuleConditionState.DECLINED) {
        return new transaction_type_1.RuleConditionStatus(failureMessage + '. The amount itself is greater than the daily limit.', errorCodeDeclined);
    }
    return new transaction_type_1.RuleConditionStatus(failureMessage + '. But you can try it tomorrow.', errorCodeTryTomorrow);
};
const constructDailyLimitEvaluatedRuleConditionStatusRefined = (currentAmount, dailyAmountSumSoFar, tag, dailyLimit, errorCodeDeclined, errorCodeTryTomorrow, currentEvalState) => {
    const currentAmountFormatted = `${transaction_constant_1.IDR_CURRENCY_PREFIX} ${currencyFormatter_1.toIndonesianRupiah(currentAmount)}`;
    const dailyLimitFormatted = `${transaction_constant_1.IDR_CURRENCY_PREFIX} ${currencyFormatter_1.toIndonesianRupiah(dailyLimit)}`;
    if (currentEvalState === transaction_conditions_enum_1.RuleConditionState.ACCEPTED) {
        return new transaction_type_1.RuleConditionStatus(`${tag} SUCCESS`);
    }
    else if (currentEvalState === transaction_conditions_enum_1.RuleConditionState.DECLINED) {
        return new transaction_type_1.RuleConditionStatus(`Current ${tag}'s transaction amount [${currentAmountFormatted}] exceeds ${tag}'s Daily Limit [${dailyLimitFormatted}]`, errorCodeDeclined);
    }
    else if (currentEvalState === transaction_conditions_enum_1.RuleConditionState.TRY_TOMORROW) {
        const remainingAmountAvailableToWithdraw = dailyLimit - dailyAmountSumSoFar;
        const remainingAmountAvailableToWithdrawFormatted = `${transaction_constant_1.IDR_CURRENCY_PREFIX} ${currencyFormatter_1.toIndonesianRupiah(remainingAmountAvailableToWithdraw)}`;
        return new transaction_type_1.RuleConditionStatus(`${tag}'s Daily Limit Exceeded. Attempted to perform transaction with amount [${currentAmountFormatted}], however only [${remainingAmountAvailableToWithdrawFormatted}] is available for ${tag}'s transaction`, errorCodeTryTomorrow);
    }
    else {
        return new transaction_type_1.RuleConditionStatus(`${tag} FAILED`);
    }
};
const transactionConditionHelperService = {
    compareAmountWithDailyLimit,
    constructDailyLimitEvaluatedRuleConditionStatus,
    constructDailyLimitEvaluatedRuleConditionStatusRefined
};
exports.default = transactionConditionHelperService;
//# sourceMappingURL=transaction.conditions.helper.js.map