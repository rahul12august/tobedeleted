"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const moment_timezone_1 = __importDefault(require("moment-timezone"));
const lodash_1 = require("lodash");
const bifast_constant_1 = require("../bifast/bifast.constant");
const limitGroup_1 = __importDefault(require("../limitGroup/limitGroup"));
const transaction_constant_1 = require("./transaction.constant");
const transaction_type_1 = require("./transaction.type");
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const errors_1 = require("../common/errors");
const transaction_conditions_enum_1 = require("./transaction.conditions.enum");
const transaction_conditions_helper_1 = __importDefault(require("./transaction.conditions.helper"));
var conditionStaticValue;
(function (conditionStaticValue) {
    conditionStaticValue["ANY"] = "ANY";
    conditionStaticValue["YES"] = "YES";
    conditionStaticValue["NO"] = "NO";
})(conditionStaticValue || (conditionStaticValue = {}));
const isProxyBankCode = (transactionModel) => {
    if (transactionModel.beneficiaryBankCode &&
        transactionModel.beneficiaryBankCode == bifast_constant_1.BIFAST_PROXY_BANK_CODE) {
        return true;
    }
    return false;
};
const isProxyAdditionalInformationAvailable = (transactionModel) => {
    return (!lodash_1.isEmpty(transactionModel.additionalInformation3) &&
        !lodash_1.isEmpty(transactionModel.additionalInformation4) &&
        bifast_constant_1.BIFAST_PROXY_TYPES.includes(transactionModel.additionalInformation3));
};
const isBiFastProxy = (transactionModel) => isProxyBankCode(transactionModel) || // Either BankCode is BC000 ( BIFastProxyBankCode )
    isProxyAdditionalInformationAvailable(transactionModel); // Valid additional information 3 and 4 available
const createStatus = (matcherFn, successMessage, failureMessage, errorCode) => {
    const matched = matcherFn();
    if (matched) {
        return new transaction_type_1.RuleConditionStatus(successMessage);
    }
    return new transaction_type_1.RuleConditionStatus(failureMessage, errorCode);
};
const createStatusWithMessage = (matcherFn, actual, expected, messageBase, errorCode) => {
    return createStatus(matcherFn, `${messageBase} check is passed. actual=${actual} expected=${expected}`, `${messageBase} check is failed. actual=${actual} expected=${expected}`, errorCode);
};
const equals = (actual, expected, messageBase, errorCode) => {
    return createStatusWithMessage(() => {
        return actual === expected;
    }, actual, expected, messageBase, errorCode);
};
const equalsOrAny = (actual, expected, messageBase, errorCode) => {
    return createStatusWithMessage(() => {
        return actual === expected || expected === conditionStaticValue.ANY;
    }, actual, expected, messageBase, errorCode);
};
const paymentServiceTypeMatches = (input, config) => {
    return equals(input.paymentServiceType, config.paymentServiceType, 'Payment service type', errors_1.ERROR_CODE.RECOMMENDATION_PAYMENT_SEVICE_TYPE_MISMATCH);
};
const sourceBankCodeChannelMatches = (input, config) => equalsOrAny(input.sourceBankCodeChannel, config.source, 'Source bank code channel', errors_1.ERROR_CODE.RECOMMENDATION_SOURCE_BANK_CODE_CHANNEL_MISMATCH);
const beneficiaryBankCodeChannelMatches = (input, config) => equalsOrAny(input.beneficiaryBankCodeChannel, config.target, 'Beneficiary bank code channel', errors_1.ERROR_CODE.RECOMMENDATION_BENEFICIARY_BANK_CODE_CHANNEL_MISMATCH);
const sourceAccountTypeMatches = (input, config) => {
    for (let accType of config.sourceAccType) {
        const status = equalsOrAny(input.sourceAccountType, accType, 'Source account type', errors_1.ERROR_CODE.RECOMMENDATION_SOURCE_ACCOUNT_TYPE_MISMATCH);
        if (status.isSuccess()) {
            return status;
        }
    }
    return new transaction_type_1.RuleConditionStatus(`Source account type: ${input.sourceAccountType} does not match any of the required ones: ${config.sourceAccType}`, errors_1.ERROR_CODE.RECOMMENDATION_SOURCE_ACCOUNT_TYPE_MISMATCH);
};
const targetAccountTypeMatches = (input, config) => {
    for (let accType of config.targetAccType) {
        const status = equalsOrAny(input.beneficiaryAccountType, accType, 'Target account type', errors_1.ERROR_CODE.RECOMMENDATION_BENEFICIARY_ACCOUNT_TYPE_MISMATCH);
        if (status.isSuccess()) {
            return status;
        }
    }
    return new transaction_type_1.RuleConditionStatus(`Beneficiary account type: ${input.beneficiaryAccountType} does not match any of the requireds: ${config.targetAccType} `, errors_1.ERROR_CODE.RECOMMENDATION_BENEFICIARY_ACCOUNT_TYPE_MISMATCH);
};
const cifMatches = (input, config) => {
    const equalMsg = `Source and beneficiary CIF are equal: ${input.sourceCIF}`;
    const differMsg = `Source and beneficiary CIF differ. Source: ${input.sourceCIF} beneficiary: ${input.beneficiaryCIF}`;
    switch (config.sameCIF) {
        case conditionStaticValue.YES:
            return createStatus(() => {
                return input.sourceCIF === input.beneficiaryCIF;
            }, equalMsg, differMsg, errors_1.ERROR_CODE.RECOMMENDATION_CIF_MISMATCH);
        case conditionStaticValue.NO:
            return createStatus(() => {
                return input.sourceCIF !== input.beneficiaryCIF;
            }, differMsg, equalMsg, errors_1.ERROR_CODE.RECOMMENDATION_CIF_MISMATCH);
    }
    return new transaction_type_1.RuleConditionStatus('No preference for source and beneficiary CIF.');
};
const isAmountInRange = (input, config) => createStatus(() => input.transactionAmount >= config.amountRangeFrom &&
    input.transactionAmount <= config.amountRangeTo, 'transaction amount: ' +
    input.transactionAmount +
    ' is in the expected range: [' +
    config.amountRangeFrom +
    ' , ' +
    config.amountRangeTo +
    ']', 'transaction amount: ' +
    input.transactionAmount +
    ' is not in the expected range: [' +
    config.amountRangeFrom +
    ' , ' +
    config.amountRangeTo +
    ']', errors_1.ERROR_CODE.RECOMMENDATION_TRANSACTION_AMOUNT_IS_OUT_OF_RANGE);
const negate = (state) => {
    return state == transaction_conditions_enum_1.RuleConditionState.ACCEPTED
        ? transaction_conditions_enum_1.RuleConditionState.DECLINED
        : transaction_conditions_enum_1.RuleConditionState.ACCEPTED;
};
/**
 *
 * @param dailyUsage conditionStaticValue enum
 * @param transactionAmount amount to be currently transfered
 * @param dailyAmountSumSoFar the sum of the transaction amounts so far today - without the current one.
 * @param dailyLimit the daily limit.
 * @returns the condition state
 */
const evalLimit = (dailyUsage, transactionAmount, dailyAmountSumSoFar, limitGroup) => {
    if (dailyUsage === conditionStaticValue.ANY) {
        return transaction_conditions_enum_1.RuleConditionState.ACCEPTED;
    }
    const state = transaction_conditions_helper_1.default.compareAmountWithDailyLimit(limitGroup, transactionAmount, dailyAmountSumSoFar);
    if (dailyUsage === conditionStaticValue.NO) {
        return negate(state);
    }
    return state;
};
const createDailyLimitResult = (dailyUsage, limitGroup, currentAmount, dailyAmountSumSoFar, tag) => {
    const dailyLimit = limitGroup.dailyLimitAmount;
    const state = evalLimit(dailyUsage, currentAmount, dailyAmountSumSoFar, limitGroup);
    return transaction_conditions_helper_1.default.constructDailyLimitEvaluatedRuleConditionStatus(currentAmount, dailyAmountSumSoFar, tag, dailyLimit, errors_1.ERROR_CODE.RECOMMENDATION_OVER_DAILY_LIMIT, errors_1.ERROR_CODE.RECOMMENDATION_OVER_DAILY_LIMIT_RETRY_TOMORROW, state);
};
const checkRTOLDailyLimit = (input, config, limitGroupCodesMap) => {
    const limitGroupCode = limitGroup_1.default.getLimitGroupCodes(input.paymentServiceCode)
        .RTOL_LIMIT_GROUP_CODE;
    return createDailyLimitResult(config.rtolDailyUsage, limitGroupCodesMap[limitGroupCode], input.transactionAmount, input.rtolDailyUsage, 'RTOL');
};
const checkSKNDailyLimit = (input, config, limitGroupCodesMap) => {
    const limitGroupCode = limitGroup_1.default.getLimitGroupCodes(input.paymentServiceCode)
        .SKN_LIMIT_GROUP_CODE;
    return createDailyLimitResult(config.sknDailyUsage, limitGroupCodesMap[limitGroupCode], input.transactionAmount, input.sknDailyUsage, 'SKN');
};
const checkRTGSDailyLimit = (input, config, limitGroupCodesMap) => {
    const limitGroupCode = limitGroup_1.default.getLimitGroupCodes(input.paymentServiceCode)
        .RTGS_LIMIT_GROUP_CODE;
    return createDailyLimitResult(config.rtgsDailyUsage, limitGroupCodesMap[limitGroupCode], input.transactionAmount, input.rtgsDailyUsage, 'RTGS');
};
const checkBillPaymentDailyLimit = (input, _config, limitGroupCodesMap) => {
    if (input.beneficiaryBankCodeChannel === module_common_1.BankChannelEnum.GOBILLS ||
        input.beneficiaryBankCodeChannel === module_common_1.BankChannelEnum.TOKOPEDIA) {
        const result = transaction_conditions_helper_1.default.compareAmountWithDailyLimit(limitGroupCodesMap[transaction_constant_1.BILL_LIMIT_GROUP_CODE], input.transactionAmount, input.billDailyUsage);
        if (result === transaction_conditions_enum_1.RuleConditionState.ACCEPTED) {
            return new transaction_type_1.RuleConditionStatus(`Amount ${input.transactionAmount} is within the bill payment daily limit: ${input.billDailyUsage} .`);
        }
        return new transaction_type_1.RuleConditionStatus(`Amount ${input.transactionAmount} exceeded the bill payment daily limit: ${input.billDailyUsage} .`, errors_1.ERROR_CODE.RECOMMENDATION_OVER_BILL_PAYMENT_LIMIT);
    }
    return new transaction_type_1.RuleConditionStatus('Has no bill payment daily limit.');
};
const getBiFastConfig = (input, config) => {
    var _a;
    const serviceRecommendationContainsBIFAST = config.serviceRecommendation.filter(paymentServiceCode => paymentServiceCode.indexOf(module_common_1.BankChannelEnum.BIFAST) !== -1).length > 0;
    const isBeneficiarySupportsBIFast = input.beneficiarySupportedChannels &&
        input.beneficiarySupportedChannels.includes(module_common_1.BankChannelEnum.BIFAST);
    const isBeneficiaryInternalChannel = (_a = input.beneficiaryBankCodeChannel) === null || _a === void 0 ? void 0 : _a.includes(module_common_1.BankChannelEnum.INTERNAL);
    return {
        isBiFastEnabled: input.isBiFastEnabled,
        serviceRecommendationContainsBIFAST,
        isBeneficiarySupportsBIFast,
        isBeneficiaryInternalChannel
    };
};
const checkBiFastProxy = (input, config) => {
    const { isBiFastEnabled, serviceRecommendationContainsBIFAST } = getBiFastConfig(input, config);
    let passed = true;
    if (isBiFastEnabled) {
        if (isBiFastProxy(input) &&
            !serviceRecommendationContainsBIFAST &&
            input.beneficiaryBankCode !== transaction_constant_1.JAGO_BANK_CODE &&
            input.beneficiaryBankCode !== transaction_constant_1.JAGO_SHARIA_BANK_CODE) {
            passed = false;
        }
    }
    else {
        passed = !isBiFastProxy(input);
    }
    if (passed) {
        return new transaction_type_1.RuleConditionStatus('BI FAST proxy is available.');
    }
    return new transaction_type_1.RuleConditionStatus('BI FAST proxy is not available.', errors_1.ERROR_CODE.RECOMMENDATION_BIFAST_PROXY_UNAVAILABLE);
};
const isSupportBiFastChannel = (input, config, limitGroupCodesMap) => {
    const { isBiFastEnabled, serviceRecommendationContainsBIFAST, isBeneficiarySupportsBIFast, isBeneficiaryInternalChannel } = getBiFastConfig(input, config);
    // Hardcode to disable Sharia
    // if (input.isShariaCustomer) {
    //   if (serviceRecommendationContainsBIFAST) {
    //     return false;
    //   }
    //   return true;
    // }
    // if BIFAST could be recommended but...
    if (serviceRecommendationContainsBIFAST) {
        if (!isBiFastEnabled ||
            (!isBeneficiarySupportsBIFast && !isBeneficiaryInternalChannel) // shouldn't we use || instead of && ?
        ) {
            return new transaction_type_1.RuleConditionStatus('BI Fast is Disabled or beneficiary bank does not support it.', errors_1.ERROR_CODE.RECOMMENDATION_BIFAST_UNSUPPORTED);
        }
    }
    // When BI Fast is Enabled & serviceRecommendaton contains BIFAST check daily limit
    if (isBiFastEnabled && serviceRecommendationContainsBIFAST) {
        return createDailyLimitResult('', limitGroupCodesMap[transaction_constant_1.BIFAST_LIMIT_GROUP_CODE], input.transactionAmount, input.bifastDailyUsage, 'BI Fast');
    }
    if (isBiFastEnabled &&
        isBeneficiarySupportsBIFast &&
        !serviceRecommendationContainsBIFAST &&
        input.transactionAmount <= transaction_constant_1.BI_FAST_MAX_TRANSACTION_AMOUNT &&
        input.paymentServiceType !== module_common_1.PaymentServiceTypeEnum.RDN) {
        const biFastState = transaction_conditions_helper_1.default.compareAmountWithDailyLimit(limitGroupCodesMap[transaction_constant_1.BIFAST_LIMIT_GROUP_CODE], input.transactionAmount, input.bifastDailyUsage);
        const isRtolRecommended = config.serviceRecommendation.filter(paymentServiceCode => paymentServiceCode === 'RTOL').length > 0;
        if (isRtolRecommended) {
            if (biFastState !== transaction_conditions_enum_1.RuleConditionState.ACCEPTED) {
                return new transaction_type_1.RuleConditionStatus('As BI Fast daily limit is exhausted, RTOL is selected.');
            }
            return new transaction_type_1.RuleConditionStatus('Should not use RTOL as still within BI-FAST limit.', errors_1.ERROR_CODE.RECOMMENDATION_STILL_IN_BIFAST_LIMIT);
        }
        const isRtgsOrSKN = config.serviceRecommendation.filter(paymentServiceCode => ['RTGS', 'RTGS_SHARIA', 'SKN', 'SKN_SHARIA'].includes(paymentServiceCode)).length > 0;
        const acceptedForSKN = biFastState === transaction_conditions_enum_1.RuleConditionState.ACCEPTED ? !isRtgsOrSKN : isRtgsOrSKN;
        if (acceptedForSKN) {
            return new transaction_type_1.RuleConditionStatus('As BI Fast daily limit is exhausted, RTGS or SKN is selected.');
        }
        return new transaction_type_1.RuleConditionStatus('Should not use RTGS or SKN as still within BI-FAST limit.', errors_1.ERROR_CODE.RECOMMENDATION_STILL_IN_BIFAST_LIMIT);
    }
    return new transaction_type_1.RuleConditionStatus('BI Fast checks are passed.');
};
const isExecutionDuringWorkingTime = (executionTime, cutOffTime) => __awaiter(void 0, void 0, void 0, function* () {
    const momentDate = moment_timezone_1.default(executionTime, transaction_constant_1.FORMAT_DATE).tz(transaction_constant_1.TMZ);
    let dateFormat = momentDate.format(transaction_constant_1.FORMAT_DATE);
    const dayName = momentDate.format(transaction_constant_1.FORMAT_DAY_NAME);
    const timeWorkingDays = yield configuration_repository_1.default.getWorkingTime(cutOffTime);
    const timeWorkingOfDay = timeWorkingDays.find(workingDay => workingDay.dayName === dayName);
    if (timeWorkingOfDay) {
        let startDateWorking = moment_timezone_1.default.tz(`${dateFormat} ${timeWorkingOfDay.startTime}`, `${transaction_constant_1.FORMAT_DATE} ${transaction_constant_1.FORMAT_HOUR_MINUS_24H}`, transaction_constant_1.TMZ);
        let endDateWorking = moment_timezone_1.default.tz(`${dateFormat} ${timeWorkingOfDay.endTime}`, `${transaction_constant_1.FORMAT_DATE} ${transaction_constant_1.FORMAT_HOUR_MINUS_24H}`, transaction_constant_1.TMZ);
        return !(momentDate.isBefore(startDateWorking) ||
            momentDate.isAfter(endDateWorking));
    }
    return false;
});
const filterPaymentServiceMappingsByWoringDateAndTime = (paymentServiceMappings, executionTime) => __awaiter(void 0, void 0, void 0, function* () {
    let holidayList = null;
    let invalidWorkingTime = [];
    for (let item of paymentServiceMappings) {
        const cutOffTime = item.cutOffTime;
        if (module_common_1.WorkingScheduleCategory.SKN.valueOf() !== cutOffTime &&
            module_common_1.WorkingScheduleCategory.RTGS.valueOf() !== cutOffTime) {
            continue;
        }
        if (holidayList === null) {
            holidayList = yield configuration_repository_1.default.getHolidaysByDateRange(executionTime, executionTime);
        }
        if (holidayList && holidayList.length > 0) {
            invalidWorkingTime.push(cutOffTime);
        }
        const workingTime = yield isExecutionDuringWorkingTime(executionTime, cutOffTime);
        if (!workingTime) {
            invalidWorkingTime.push(cutOffTime);
        }
    }
    if (lodash_1.isEmpty(invalidWorkingTime)) {
        return paymentServiceMappings;
    }
    const updatedServiceMapping = paymentServiceMappings.filter((item) => item.cutOffTime && !invalidWorkingTime.includes(item.cutOffTime));
    return updatedServiceMapping;
});
exports.filterByDateAndWorkingTime = (paymentServiceMappings, date) => __awaiter(void 0, void 0, void 0, function* () {
    const filteredMappings = yield filterPaymentServiceMappingsByWoringDateAndTime(paymentServiceMappings, date);
    const status = !lodash_1.isEmpty(filteredMappings)
        ? new transaction_type_1.RuleConditionStatus('Working date and time rules are passed.')
        : new transaction_type_1.RuleConditionStatus('Transaction is rejected due to the working date and time rules. Try again tomorrow.', errors_1.ERROR_CODE.RECOMMENDATION_REJECTED_BY_WORKING_DAY_RULES);
    return { status: status, mappings: filteredMappings };
});
exports.conditions = {
    paymentServiceTypeMatches,
    beneficiaryBankCodeChannelMatches,
    sourceBankCodeChannelMatches,
    sourceAccountTypeMatches,
    targetAccountTypeMatches,
    cifMatches,
    isAmountInRange,
    checkRTOLDailyLimit,
    checkSKNDailyLimit,
    checkRTGSDailyLimit,
    checkBillPaymentDailyLimit,
    isSupportBiFastChannel,
    checkBiFastProxy
};
exports.default = Object.values(exports.conditions);
//# sourceMappingURL=transaction.conditions.js.map