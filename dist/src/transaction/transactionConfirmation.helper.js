"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_repository_1 = __importDefault(require("./transaction.repository"));
const logger_1 = __importStar(require("../logger"));
const updateRefundAmount = (txId, amount) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`updateRefundAmount: Updating amount on reversal of refund  for original transaction id : ${txId}`);
    const originalTx = yield transaction_repository_1.default.getByKey(txId);
    if (originalTx &&
        originalTx.refund &&
        originalTx.refund.remainingAmount !== undefined &&
        originalTx.refund.remainingAmount !== null) {
        const remainingAmount = originalTx.refund.remainingAmount + amount;
        const updatedTransaction = Object.assign(Object.assign({}, originalTx), { refund: Object.assign(Object.assign({}, originalTx.refund), { remainingAmount: remainingAmount }) });
        yield transaction_repository_1.default.update(originalTx.id, updatedTransaction);
    }
});
const transactionConfirmationHelper = logger_1.wrapLogs({
    updateRefundAmount
});
exports.default = transactionConfirmationHelper;
//# sourceMappingURL=transactionConfirmation.helper.js.map