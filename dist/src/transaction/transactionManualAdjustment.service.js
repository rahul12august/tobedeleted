"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const errors_1 = require("../common/errors");
const AppError_1 = require("../errors/AppError");
const logger_1 = __importStar(require("../logger"));
const transaction_enum_1 = require("./transaction.enum");
const transaction_repository_1 = __importDefault(require("./transaction.repository"));
const transaction_service_1 = __importDefault(require("./transaction.service"));
const alto_service_1 = __importDefault(require("./../alto/alto.service"));
const transactionConfirmation_service_1 = __importStar(require("./transactionConfirmation.service"));
const transactionExecution_service_1 = __importDefault(require("./transactionExecution.service"));
const transactionHistory_repository_1 = __importDefault(require("../transactionHistory/transactionHistory.repository"));
const alto_utils_1 = __importDefault(require("../alto/alto.utils"));
const markTxnDeclineManually = (txn) => __awaiter(void 0, void 0, void 0, function* () {
    const input = {
        externalId: txn.externalId,
        responseCode: transaction_enum_1.ConfirmTransactionStatus.ERROR_EXPECTED,
        transactionResponseID: undefined
    };
    yield transactionConfirmation_service_1.default.reverseTransaction(input, txn);
    if (txn.paymentInstructionExecution) {
        transactionConfirmation_service_1.responseMessageToPaymentInstruction(input, txn);
    }
});
const markTxnSucceedManually = (txn, confirmTemplate) => __awaiter(void 0, void 0, void 0, function* () {
    const input = {
        externalId: txn.externalId,
        responseCode: transaction_enum_1.ConfirmTransactionStatus.SUCCESS,
        transactionResponseID: undefined,
        interchange: txn.interchange
    };
    const updateInfo = transactionConfirmation_service_1.default.extractChangeFromConfirmTransactionRequest(input, txn, false);
    const updatedTxn = yield transaction_repository_1.default.update(txn.id, updateInfo);
    if (!updatedTxn) {
        logger_1.default.error(`markTxnSucceedManually: transaction update failed for id:${txn.id}`);
        throw new AppError_1.TransferAppError(`Transaction update failed for id: ${txn.id}!`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION);
    }
    const updatedTransaction = yield transactionExecution_service_1.default.settleTransaction(updatedTxn, confirmTemplate);
    if (updatedTxn.paymentInstructionExecution) {
        transactionConfirmation_service_1.responseMessageToPaymentInstruction(input, updatedTransaction);
    }
});
const extTxnManualAdjstment = (txn, reqStatus) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c;
    logger_1.default.info(`extTxnManualAdjstment: transaction bank channel ${txn.beneficiaryBankCodeChannel} for manual adjustment`);
    if (txn.status !== transaction_enum_1.TransactionStatus.SUBMITTED &&
        txn.status !== transaction_enum_1.TransactionStatus.DECLINED &&
        txn.status !== transaction_enum_1.TransactionStatus.PENDING) {
        const detail = `Invalid transaction status to manually adjusted for txnId: ${txn.id}!`;
        logger_1.default.error(`extTxnManualAdjstment: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.INVALID_TRANSACTION_STATUS);
    }
    try {
        if ((txn.status === transaction_enum_1.TransactionStatus.SUBMITTED ||
            txn.status === transaction_enum_1.TransactionStatus.PENDING) &&
            reqStatus === transaction_enum_1.TransactionStatus.DECLINED) {
            if (txn.beneficiaryBankCodeChannel == module_common_1.BankChannelEnum.QRIS) {
                yield alto_service_1.default.markQrisTxnDeclineManually(txn);
            }
            else {
                yield markTxnDeclineManually(txn);
            }
        }
        if ((txn.status === transaction_enum_1.TransactionStatus.SUBMITTED ||
            txn.status === transaction_enum_1.TransactionStatus.PENDING) &&
            reqStatus === transaction_enum_1.TransactionStatus.SUCCEED) {
            yield markTxnSucceedManually(txn, {
                submitTransaction: transactionConfirmation_service_1.default.submitConfirmTransaction,
                isEligible: () => true,
                declineOnSubmissionFailure: false
            });
        }
        if (txn.status === transaction_enum_1.TransactionStatus.DECLINED &&
            reqStatus === transaction_enum_1.TransactionStatus.SUCCEED) {
            yield markTxnSucceedManually(txn, {
                submitTransaction: transactionExecution_service_1.default.executeDebit,
                isEligible: () => true,
                declineOnSubmissionFailure: false
            });
        }
        if (txn.beneficiaryBankCodeChannel == module_common_1.BankChannelEnum.QRIS &&
            txn.coreBankingTransactionIds &&
            txn.coreBankingTransactionIds[0]) {
            if (reqStatus === transaction_enum_1.TransactionStatus.SUCCEED) {
                const checkStatus = yield alto_service_1.default.checkStatus((_a = txn.sourceCustomerId, (_a !== null && _a !== void 0 ? _a : '')), (_b = txn.paymentInstructionId, (_b !== null && _b !== void 0 ? _b : '')));
                const updatedTrx = yield transaction_service_1.default.getTransaction(txn.id);
                alto_utils_1.default.setQrisResponseToTransactionData(updatedTrx, checkStatus.forwardingCustomerReferenceNumber);
                alto_utils_1.default.updateTransactionDataOnQrisPaymentSuccess(updatedTrx, checkStatus);
                // don't need to wait the response, let's do this asynchronously
                transactionHistory_repository_1.default.updateTransactionHistory((_c = txn.coreBankingTransactionIds[0], (_c !== null && _c !== void 0 ? _c : '')), alto_utils_1.default.getTransactionHistoryStatus(checkStatus.transactionStatus), checkStatus.forwardingCustomerReferenceNumber);
                yield transaction_repository_1.default.update(updatedTrx.id, updatedTrx);
            }
            else {
                transactionHistory_repository_1.default.updateTransactionHistory(txn.coreBankingTransactionIds[0], reqStatus);
            }
        }
    }
    catch (error) {
        logger_1.default.error(`extTxnManualAdjstment: error while manually adjusting transaction: ${txn.id}, status: ${error && error.status}, message: ${error && error.message}`);
        throw error;
    }
    finally {
        yield transaction_service_1.default.findByIdAndUpdate(txn.id, {
            manuallyAdjusted: true
        });
    }
});
const txnManualAdjustmentService = logger_1.wrapLogs({
    extTxnManualAdjstment
});
exports.default = txnManualAdjustmentService;
//# sourceMappingURL=transactionManualAdjustment.service.js.map