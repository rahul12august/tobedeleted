"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_util_1 = require("./transaction.util");
const errors_1 = require("../common/errors");
const module_common_1 = require("@dk/module-common");
const logger_1 = __importStar(require("../logger"));
const transactionReversal_service_1 = __importDefault(require("./transactionReversal.service"));
const transaction_helper_1 = __importDefault(require("./transaction.helper"));
const transaction_producer_1 = __importDefault(require("./transaction.producer"));
const AppError_1 = require("../errors/AppError");
const transaction_enum_1 = require("./transaction.enum");
const transaction_constant_1 = require("./transaction.constant");
const getReversalTemplate = (transaction) => {
    if (transaction &&
        transaction_constant_1.LOAN_PROCESSOR_PAYMENT_SERVICE_TYPE_LIST.includes(transaction.paymentServiceType)) {
        return transactionReversal_service_1.default.reverseLoanTransaction;
    }
    return transactionReversal_service_1.default.reverseTransactionsByInstructionId;
};
const revertFailedTransaction = (transactionId, reversalReason, transaction) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield module_common_1.Util.retry(transaction_util_1.RetryConfig(), getReversalTemplate(transaction), transactionId, reversalReason, transaction);
    }
    catch (err) {
        const error = err instanceof AppError_1.TransferAppError
            ? err
            : new AppError_1.TransferAppError(err.message || err.status || err.errorCode, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.UNEXPECTED_ERROR);
        yield transaction_helper_1.default.updateFailedTransactionStatus(transactionId, transaction_enum_1.TransactionStatus.REVERSAL_FAILED, error);
        const reversalMessage = {
            transactionId: transactionId,
            retryCounter: 1
        };
        if (errors_1.isRetriableError(err)) {
            transaction_producer_1.default.sendTransactionRetryReversalCommand(reversalMessage);
        }
        else {
            transaction_producer_1.default.sendTransactionReversalFailedEvent(reversalMessage);
            const detail = `Failed to reverse for transaction ID: ${transactionId}!`;
            logger_1.default.error(`revertFailedTransaction: ${detail}`);
            throw new AppError_1.RetryableTransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.FAILED_TO_REVERSE, true);
        }
    }
});
const revertManuallyAdjustedTransaction = (transactionId, transaction) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield module_common_1.Util.retry(transaction_util_1.RetryConfig(), transactionReversal_service_1.default.reverseTransactionsByInstructionId, transactionId, 'Failed to debit amount while manually adjusting transaction', transaction);
    }
    catch (err) {
        const reversalMessage = {
            transactionId: transactionId,
            retryCounter: 1
        };
        transaction_producer_1.default.sendTransactionReversalFailedEvent(reversalMessage);
        const detail = `Failed to reverse for transaction ID: ${transactionId}!`;
        logger_1.default.error(`revertManuallyAdjustedTransaction: ${detail}`);
        throw new AppError_1.RetryableTransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.FAILED_TO_REVERSE, true);
    }
});
const revertFailedTransactionWithoutError = (transactionId, reversalReason, transaction) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield module_common_1.Util.retry(transaction_util_1.RetryConfig(), getReversalTemplate(transaction), transactionId, reversalReason, transaction);
        return true;
    }
    catch (err) {
        const reversalMessage = {
            transactionId: transactionId,
            retryCounter: 1
        };
        if (errors_1.isRetriableError(err)) {
            transaction_producer_1.default.sendTransactionRetryReversalCommand(reversalMessage);
        }
        else {
            transaction_producer_1.default.sendTransactionReversalFailedEvent(reversalMessage);
            logger_1.default.error(`revertFailedQrisTransaction: failed to reverse for transaction Id: ${transactionId}`);
        }
        return false;
    }
});
exports.default = logger_1.wrapLogs({
    revertFailedTransaction,
    revertManuallyAdjustedTransaction,
    revertFailedTransactionWithoutError
});
//# sourceMappingURL=transactionReversal.helper.js.map