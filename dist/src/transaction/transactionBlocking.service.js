"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const transaction_enum_1 = require("./transaction.enum");
const transaction_repository_1 = __importDefault(require("./transaction.repository"));
const transaction_helper_1 = __importDefault(require("./transaction.helper"));
const transactionBlocking_helper_1 = __importDefault(require("./transactionBlocking.helper"));
const transaction_service_1 = __importDefault(require("./transaction.service"));
const transactionExecution_service_1 = __importDefault(require("./transactionExecution.service"));
const transactionCoreBanking_helper_1 = __importDefault(require("./transactionCoreBanking.helper"));
const module_common_1 = require("@dk/module-common");
const blockingAmount_service_1 = __importDefault(require("../blockingAmount/blockingAmount.service"));
const monitoringEvent_producer_1 = __importDefault(require("../monitoringEvent/monitoringEvent.producer"));
const module_message_1 = require("@dk/module-message");
const transaction_constant_1 = require("./transaction.constant");
const submitBlockingTransaction = (model) => __awaiter(void 0, void 0, void 0, function* () {
    const transaction = yield transactionBlocking_helper_1.default.createBlockingTransaction(model, module_common_1.Enum.BlockingAmountType.GINPAY);
    return yield transaction_repository_1.default.update(transaction.id, Object.assign(Object.assign({}, transaction), { status: transaction_enum_1.TransactionStatus.SUBMITTED }));
});
const createBlockingTransferTransaction = (transactionInput) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionModel = transaction_helper_1.default.buildOutgoingTransactionModel(transactionInput);
    yield transaction_helper_1.default.validateExternalIdAndPaymentType(transactionModel);
    const model = yield transaction_repository_1.default.create(transactionModel);
    monitoringEvent_producer_1.default.sendMonitoringEventMessage(module_message_1.MonitoringEventType.PROCESSING_STARTED, model);
    const input = yield transaction_service_1.default.populateTransactionInfo(Object.assign(Object.assign(Object.assign({}, transactionInput), model), { transactionId: model.id }), bankCode => {
        return bankCode.channel !== module_common_1.BankChannelEnum.WINCOR;
    });
    const createdTransaction = yield transactionExecution_service_1.default.initTransaction(input, {
        submitTransaction: submitBlockingTransaction,
        isEligible: () => true
    });
    return createdTransaction;
});
const adjustBlockingTransferTransaction = (cif, transactionId, amount) => __awaiter(void 0, void 0, void 0, function* () {
    const transaction = yield transaction_helper_1.default.getPendingBlockingTransaction(transactionId, cif);
    let updatedTransaction = yield transaction_repository_1.default.update(transaction.id, {
        journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.ADJUST_BLOCKING_AMOUNT, transaction)
    });
    logger_1.default.info(`adjustBlockingTransferTransaction: Updating blocking amount for transactionId: ${transactionId}`);
    yield blockingAmount_service_1.default.updateBlockingAmount(transaction.sourceAccountNo, transaction.blockingId, amount, transaction.transactionAmount, transaction.cardId);
    updatedTransaction = yield transaction_repository_1.default.update(transaction.id, {
        transactionAmount: amount,
        journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.BLOCKING_AMOUNT_ADJUSTED, updatedTransaction)
    });
    if (updatedTransaction === null) {
        logger_1.default.error(`adjustBlockingTransferTransaction: failed to update transaction info for txnId ${transaction.id}`);
        throw new AppError_1.TransferAppError(`Failed to update transaction info for txnId ${transaction.id}.`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION);
    }
    return updatedTransaction;
});
const confirmBlockingTransaction = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    if (!(transaction.debitTransactionCode && transaction.debitTransactionChannel) ||
        !transaction.beneficiaryAccountNo ||
        !transaction.cardId ||
        !transaction.blockingId ||
        !transaction.sourceAccountNo) {
        logger_1.default.error('confirmBlockingTransaction: invalid request, one of field is missing debitTransactionCode, debitTransactionChannel, beneficiaryAccountNo, cardId, blockingId, sourceAccountNo');
        throw new AppError_1.TransferAppError(`Invalid request one of the field is missing debitTransactionCode, debitTransactionChannel, beneficiaryAccountNo, cardId, blockingId, sourceAccountNo!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.INVALID_REQUEST);
    }
    if (!transaction.debitTransactionCode.endsWith(transaction_constant_1.REVERSAL_TC_SUFFIX)) {
        yield transactionCoreBanking_helper_1.default.submitCardTransaction(transaction.cardId, transaction.blockingId, transaction, transaction.debitTransactionCode, transaction.debitTransactionChannel);
    }
    if (transaction.creditTransactionCode &&
        !transaction.creditTransactionCode.endsWith(transaction_constant_1.REVERSAL_TC_SUFFIX) &&
        transaction.creditTransactionChannel) {
        transactionCoreBanking_helper_1.default.submitCreditTransaction(transaction.beneficiaryAccountNo, transaction, transaction.creditTransactionCode, transaction.creditTransactionChannel);
    }
    return transaction;
});
const executeBlockingTransferTransaction = (cif, transactionId, amount) => __awaiter(void 0, void 0, void 0, function* () {
    const transaction = yield transaction_helper_1.default.getPendingBlockingTransaction(transactionId, cif, amount);
    const updatedTransaction = yield transactionExecution_service_1.default.settleTransaction(transaction, {
        submitTransaction: confirmBlockingTransaction,
        isEligible: () => true,
        declineOnSubmissionFailure: false
    });
    return updatedTransaction;
});
const cancelBlockingTransferTransaction = (cif, transactionId) => __awaiter(void 0, void 0, void 0, function* () {
    const transaction = yield transaction_helper_1.default.getPendingBlockingTransaction(transactionId, cif);
    yield transactionBlocking_helper_1.default.cancelBlockingTransaction(transaction);
    // todo await transactionService.updateFailedTransaction(transaction, null);
    yield transaction_repository_1.default.update(transaction.id, {
        status: transaction_enum_1.TransactionStatus.DECLINED,
        journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.TRANSFER_FAILURE, transaction)
    });
});
const manualUnblockingTransferTransaction = (transactionId) => __awaiter(void 0, void 0, void 0, function* () {
    const transaction = yield transaction_helper_1.default.getDeclinedBlockingTransaction(transactionId);
    yield transactionBlocking_helper_1.default.cancelBlockingTransaction(transaction);
});
const transactionBlockingService = logger_1.wrapLogs({
    cancelBlockingTransferTransaction,
    adjustBlockingTransferTransaction,
    createBlockingTransferTransaction,
    executeBlockingTransferTransaction,
    manualUnblockingTransferTransaction
});
exports.default = transactionBlockingService;
//# sourceMappingURL=transactionBlocking.service.js.map