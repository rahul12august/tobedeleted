"use strict";
/**
 * @module switchingSubmissionTemplate handle outgoing transaction to switching
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const logger_1 = __importStar(require("../logger"));
const transactionSwitching_helper_1 = require("./transactionSwitching.helper");
const switching_repository_1 = __importDefault(require("../switching/switching.repository"));
const constant_1 = require("../common/constant");
const lodash_1 = require("lodash");
const transaction_util_1 = __importDefault(require("../transaction/transaction.util"));
const module_common_2 = require("@dk/module-common");
const submitTransaction = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    const switchingTransaction = yield transactionSwitching_helper_1.mapToSwitchingTransaction(transactionModel);
    const response = yield transaction_util_1.default
        .submitThirdPartyTransactionWithRetryAndUpdate(switching_repository_1.default.submitTransaction, switchingTransaction, transactionModel)
        .catch(err => {
        logger_1.default.error('submitThirdPartyTransactionWithRetryAndUpdate: failed submission to switching after all retry or non-retriable condition', err);
        // only throw error if it's not a timeout error
        if (err.code !== constant_1.httpClientTimeoutErrorCode) {
            throw err;
        }
    });
    if (response) {
        return response.updatedTransaction;
    }
    return transactionModel;
});
const isEligible = (model) => module_common_1.BankChannelEnum.EXTERNAL === model.beneficiaryBankCodeChannel &&
    !lodash_1.isEmpty(model.beneficiaryAccountNo);
const shouldSendNotification = () => true;
const getRail = () => module_common_2.Rail.EURONET;
const switchingSubmissionTemplate = {
    submitTransaction,
    isEligible,
    getRail,
    shouldSendNotification
};
exports.default = logger_1.wrapLogs(switchingSubmissionTemplate);
//# sourceMappingURL=transactionSubmissionSwitchingTemplate.service.js.map