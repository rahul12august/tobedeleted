"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const transaction_util_1 = require("./transaction.util");
const module_common_1 = require("@dk/module-common");
const transaction_repository_1 = __importDefault(require("./transaction.repository"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const deposit_repository_1 = __importDefault(require("../coreBanking/deposit.repository"));
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const configuration_constant_1 = require("../configuration/configuration.constant");
const blockingAmount_service_1 = __importDefault(require("../blockingAmount/blockingAmount.service"));
const transaction_enum_1 = require("./transaction.enum");
const transaction_helper_1 = __importDefault(require("./transaction.helper"));
const config_1 = require("../config");
const createBlockingAmount = (accountNo, name, amount, blockingPurposeCode, mambuBlockingCode) => __awaiter(void 0, void 0, void 0, function* () {
    const blockingPayload = {
        amount: amount,
        name: name,
        blockingPurposeCode: blockingPurposeCode,
        mambuBlockingCode
    };
    return blockingAmount_service_1.default.createBlockingAmount(accountNo, blockingPayload);
});
const getMambuBlockingCode = () => __awaiter(void 0, void 0, void 0, function* () {
    const defaultBlockingDurationCode = config_1.config.get('amountBlockDurationCode');
    if (defaultBlockingDurationCode) {
        logger_1.default.info('getMambuBlockingCode: send default mambu blocking code');
        return defaultBlockingDurationCode;
    }
    logger_1.default.info('getMambuBlockingCode: get blocking duration code by bussiness days from config');
    const blockingDurationCodeByBusinessDays = yield configuration_repository_1.default.getCounterByCode(configuration_constant_1.COUNTER_MAMBU_BLOCK_DAYS);
    if (!blockingDurationCodeByBusinessDays) {
        logger_1.default.error(`getMambuBlockingCode: couldn't find mambu blocking code`);
        throw new AppError_1.TransferAppError("Couldn't find mambu blocking code", transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.MAMBU_BLOCKING_COUNTER_NOT_FOUND);
    }
    return blockingDurationCodeByBusinessDays.value.toString();
});
const createBlockingTransaction = (model, blockingPurposeCode) => __awaiter(void 0, void 0, void 0, function* () {
    if (model.sourceCIF && model.sourceAccountNo) {
        const mambuBlockingCode = yield getMambuBlockingCode();
        logger_1.default.info(`createBlockingTransaction: Mambu blocking code: ${mambuBlockingCode}`);
        const blockingMainResponse = yield createBlockingAmount(model.sourceAccountNo, model.additionalInformation4 || 'block for transaction id: ' + model.id, model.transactionAmount, blockingPurposeCode, mambuBlockingCode);
        logger_1.default.info(`create blocking principle successfully with blockId: ${blockingMainResponse.id}`);
        model.blockingId = blockingMainResponse.id;
        model.cardId = blockingMainResponse.cardId;
        if (model.fees &&
            blockingPurposeCode === module_common_1.Enum.BlockingAmountType.MOBILE_TRANSACTION) {
            model.fees = yield Promise.all(model.fees.map((fee) => __awaiter(void 0, void 0, void 0, function* () {
                if (model.sourceCIF && model.sourceAccountNo && fee.feeAmount) {
                    const blockingFeeResponse = yield createBlockingAmount(model.sourceAccountNo, model.additionalInformation4 ||
                        'block for transaction id: ' + model.id, // TODO: will change later, currently blocking require name blocking
                    fee.feeAmount, blockingPurposeCode, mambuBlockingCode);
                    logger_1.default.info(`create blocking fee successfully with blockId: ${blockingFeeResponse.id}`);
                    fee.blockingId = blockingFeeResponse.id;
                    fee.idempotencyKey = blockingFeeResponse.idempotencyKey;
                }
                return fee;
            })));
        }
        model.idempotencyKey = blockingMainResponse.idempotencyKey;
        const transaction = yield transaction_repository_1.default.update(model.id, Object.assign(Object.assign({}, model), { journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_BLOCKED, model) }));
        if (transaction) {
            return transaction;
        }
    }
    logger_1.default.error(`createBlockingTransaction: Mandatory field missing sourceAccount : ${model.sourceAccountNo} or sourceCif: ${model.sourceCIF}`);
    throw new AppError_1.TransferAppError(`Mandatory field missing sourceAccount: ${model.sourceAccountNo} or sourceCif: ${model.sourceCIF}`, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_REQUEST);
});
const reverseAuthorizationHoldWithRetry = (cardReferenceToken, authHoldExtReferenceId) => __awaiter(void 0, void 0, void 0, function* () {
    return yield module_common_1.Util.retry(transaction_util_1.RetryConfig(), deposit_repository_1.default.reverseAuthorizationHold, cardReferenceToken, authHoldExtReferenceId);
});
const cancelBlockingTransaction = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    if (transaction.sourceCIF &&
        transaction.sourceAccountNo &&
        transaction.cardId) {
        if (transaction.blockingId) {
            // unblock principle
            yield reverseAuthorizationHoldWithRetry(transaction.cardId, transaction.blockingId);
        }
        if (transaction.fees) {
            // unblock fee
            yield Promise.all(transaction.fees.map((fee) => __awaiter(void 0, void 0, void 0, function* () {
                if (transaction.sourceCIF &&
                    transaction.sourceAccountNo &&
                    transaction.cardId &&
                    fee.blockingId) {
                    yield reverseAuthorizationHoldWithRetry(transaction.cardId, fee.blockingId);
                }
            })));
        }
    }
});
const transactionBlockingHelper = logger_1.wrapLogs({
    cancelBlockingTransaction,
    createBlockingTransaction
});
exports.default = transactionBlockingHelper;
//# sourceMappingURL=transactionBlocking.helper.js.map