"use strict";
/**
 * This module is to handle any transaction that come from 3rd application or switching
 *
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const transaction_helper_1 = __importDefault(require("./transaction.helper"));
const module_common_1 = require("@dk/module-common");
const virtualAccount_repository_1 = __importDefault(require("../virtualAccount/virtualAccount.repository"));
const transactionRecommend_service_1 = __importDefault(require("./transactionRecommend.service"));
const transactionExecution_service_1 = __importStar(require("./transactionExecution.service"));
const transaction_util_1 = require("./transaction.util");
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const transaction_service_1 = __importDefault(require("./transaction.service"));
const deposit_repository_1 = __importDefault(require("../coreBanking/deposit.repository"));
const refund_validator_1 = __importDefault(require("./refund/refund.validator"));
const euronetRefund_template_1 = __importDefault(require("./refund/euronet/euronetRefund.template"));
const jagoPayRefund_template_1 = __importDefault(require("./refund/jagoPay/jagoPayRefund.template"));
const sknRtgsRefund_template_1 = __importDefault(require("./refund/sknRtgs/sknRtgsRefund.template"));
const bifastRefund_template_1 = __importDefault(require("./refund/bifast/bifastRefund.template"));
const account_repository_1 = __importDefault(require("../account/account.repository"));
const transaction_enum_1 = require("./transaction.enum");
const common_util_1 = require("../common/common.util");
const transaction_repository_1 = __importDefault(require("./transaction.repository"));
const configuration_service_1 = __importDefault(require("../configuration/configuration.service"));
const lodash_1 = require("lodash");
const monitoringEvent_producer_1 = __importDefault(require("../monitoringEvent/monitoringEvent.producer"));
const module_message_1 = require("@dk/module-message");
const transactionIdempotency_helper_1 = __importDefault(require("./transactionIdempotency.helper"));
const atomeRefund_template_1 = __importDefault(require("./refund/atome/atomeRefund.template"));
const jagoAccountNumberMapping = {
    '0': 'BC191',
    '1': 'BC002',
    '2': 'BC002',
    '9': 'BC004',
    '5': 'BC777',
    '7': 'BC002'
};
const jagoBankCodeDefaultMapping = {
    '542': 'BC191' //ARTOS
};
const goBizAccountReferenceGroup = 'goBizAccount';
const allowedBankCodeCheck = (bankCode) => {
    const RTOL_CODES = [
        "542" /* RTOL_CODE */,
        "542" /* RTOL_CODE */
    ];
    const REMITTANCE_CODES = [
        "ATOSIDJ1" /* REMITTANCE_CODE */,
        "SYATIDJ1" /* REMITTANCE_CODE */
    ];
    return [...RTOL_CODES, ...REMITTANCE_CODES].includes(bankCode);
};
const getEligibleBankCode = (bankCode, accountNo) => {
    const logMessage = (findBankCode) => {
        logger_1.default.info(`getEligibleBankCode foundbank code: ${findBankCode} from input: bankCode ${bankCode} & accountNo ${accountNo}`);
    };
    if (accountNo && allowedBankCodeCheck(bankCode)) {
        const findBankCode = jagoAccountNumberMapping[accountNo.charAt(0)];
        if (findBankCode) {
            logMessage(findBankCode);
            return findBankCode;
        }
    }
    else if (allowedBankCodeCheck(bankCode)) {
        const findBankCode = jagoBankCodeDefaultMapping[bankCode];
        if (findBankCode) {
            logMessage(findBankCode);
            return findBankCode;
        }
    }
    logMessage('none');
    return bankCode;
};
const populateExternalWalletTransactionInput = (input) => __awaiter(void 0, void 0, void 0, function* () {
    const virtualAccount = yield virtualAccount_repository_1.default.inquiryVirtualAccount(input.beneficiaryAccountNo);
    const [ginBankCodeInfo, beneficiaryBankCodeInfo] = yield Promise.all([
        transaction_helper_1.default.getBankCodeMapping(module_common_1.Constant.GIN_BANK_CODE_ID),
        transaction_helper_1.default.getBankCodeMapping(virtualAccount.institution.bankCode)
    ]);
    const beneficiaryAccount = yield transaction_helper_1.default.getAccountInfo(virtualAccount.institution.accountId, virtualAccount.institution.cif, ginBankCodeInfo);
    const transactionInput = Object.assign(Object.assign({}, input), { paymentServiceCode: '', beneficiaryBankCode: virtualAccount.institution.bankCode, paymentServiceType: module_common_1.PaymentServiceTypeEnum.WALLET });
    return transaction_helper_1.default.populateTransactionInput(transactionInput, undefined, undefined, beneficiaryAccount, beneficiaryBankCodeInfo);
});
exports.populateIncomingAccount = (bankCode, accountNo, accountName, cif) => __awaiter(void 0, void 0, void 0, function* () {
    const accountInfo = {
        accountName: accountName,
        bankCode: bankCode.bankCodeId,
        accountNumber: accountNo
    };
    logger_1.default.info(`populateIncomingAccount with cif: ${cif}`);
    const inquiryInfo = accountNo &&
        module_common_1.Constant.JAGO_BANK_CODES.includes(bankCode.bankCodeId) &&
        (yield account_repository_1.default.getAccountInfo(accountNo));
    logger_1.default.info(`Inquiry Info: ${typeof inquiryInfo}, accountNo first char: ${accountNo ? accountNo[0] : undefined}, bankCode: ${bankCode.bankCodeId}}`);
    return Object.assign(Object.assign({}, accountInfo), inquiryInfo);
});
const processIncomingTransaction = (transactionInput, executionHooks = transactionExecution_service_1.defaultExecutionHooks) => __awaiter(void 0, void 0, void 0, function* () {
    if (transactionInput.externalId) {
        if (!transactionInput.externalTransactionDate) {
            logger_1.default.error(`processIncomingTransaction : Missing Transaction Date.`);
            throw new AppError_1.TransferAppError('Missing Transaction Date.', transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.REQUIRE_TRANSACTION_DATE);
        }
        const oldExternalId = transactionInput.externalId;
        transactionInput.externalId = transaction_util_1.createIncomingTransactionExternalId(oldExternalId, transactionInput.externalTransactionDate, transactionInput.sourceAccountNo);
        if (transactionInput.beneficiaryAccountNo) {
            transactionInput.secondaryExternalId = transaction_util_1.createIncomingTransactionExternalId(oldExternalId, transactionInput.externalTransactionDate, transactionInput.beneficiaryAccountNo);
        }
        transactionInput.thirdPartyOutgoingId = oldExternalId;
    }
    return transactionExecution_service_1.default.executeTransaction(transactionInput, executionHooks);
});
const populateExternalTransactionInput = (input, transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const eligibleSourceBankCode = getEligibleBankCode(input.sourceBankCode, input.sourceAccountNo);
        const eligibleBeneBankCode = getEligibleBankCode(input.beneficiaryBankCode, input.beneficiaryAccountNo);
        const bankCodes = yield configuration_repository_1.default.inquiryBankAndCurrencyByIncomingCodes(eligibleSourceBankCode, eligibleBeneBankCode, input.currency);
        const [sourceAccountInfo, beneficiaryAccountInfo] = yield Promise.all([
            exports.populateIncomingAccount(bankCodes.sourceBank, input.sourceAccountNo, input.sourceAccountName),
            exports.populateIncomingAccount(bankCodes.beneficiaryBank, input.beneficiaryAccountNo, input.beneficiaryAccountName)
        ]);
        let transactionInput = Object.assign(Object.assign({}, input), { paymentServiceCode: '', externalTransactionDate: input.transactionDate, sourceBankCode: bankCodes.sourceBank.bankCodeId, beneficiaryBankCode: bankCodes.beneficiaryBank.bankCodeId, note: input.notes, sourceTransactionCurrency: bankCodes.currency.code, transactionId: transactionModel && transactionModel.id });
        transactionInput = Object.assign(Object.assign({}, transactionInput), { journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.INQUIRY_FETCHED, transactionInput) });
        logger_1.default.info(`transactionInput BankCodes: 
      source BankCode: ${transactionInput.sourceBankCode},
      beneficiary BankCode: ${transactionInput.beneficiaryBankCode},`);
        return transaction_helper_1.default.populateTransactionInput(transactionInput, sourceAccountInfo, bankCodes.sourceBank, beneficiaryAccountInfo, bankCodes.beneficiaryBank);
    }
    catch (err) {
        logger_1.default.error(`populateExternalTransactionInput: Error while making an inquiry ${err.message}`);
        transactionModel &&
            (yield transaction_service_1.default.updateFailedTransaction(transactionModel, err));
        throw err;
    }
});
const buildNewTransactionModel = (transaction, idempotencyKey) => (Object.assign(Object.assign({}, transaction), { referenceId: transaction_util_1.generateUniqueId(), idempotencyKey: idempotencyKey, externalId: transaction_util_1.createIncomingTransactionExternalId(transaction.externalId || transaction_util_1.generateUniqueId(), transaction.transactionDate, transaction.sourceAccountNo), paymentServiceCode: '', thirdPartyOutgoingId: transaction.externalId, paymentServiceType: transaction.paymentServiceType, beneficiaryRealBankCode: transaction.beneficiaryBankCode, status: transaction_enum_1.TransactionStatus.PENDING, requireThirdPartyOutgoingId: false, journey: [
        {
            status: transaction_enum_1.TransferJourneyStatusEnum.REQUEST_RECEIVED,
            updatedAt: new Date()
        }
    ] }));
const isAccountFromGojekBusiness = (cif) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const env = process.env.NODE_ENV;
    // get eligible cif from parameter setting
    const parameterSetting = yield configuration_service_1.default.getParameterSettingByReferenceGroup(goBizAccountReferenceGroup);
    const goBizCif = (_a = parameterSetting) === null || _a === void 0 ? void 0 : _a.find(param => {
        return param.name === env;
    });
    return goBizCif !== undefined && goBizCif.code === cif;
});
const validateIrisPayment = (input) => __awaiter(void 0, void 0, void 0, function* () {
    if (input.paymentServiceType === module_common_1.PaymentServiceTypeEnum.IRIS_TO_JAGO ||
        input.paymentServiceType === module_common_1.PaymentServiceTypeEnum.GOPAY_TO_JAGO) {
        if (lodash_1.isEmpty(input.sourceAccountNo) || lodash_1.isEmpty(input.sourceCIF)) {
            throw new AppError_1.TransferAppError('Input sourceAccountNo or sourceCIF is empty!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.SOURCE_ACCOUNT_NOT_FOUND);
        }
        const isEligibleAccount = yield isAccountFromGojekBusiness(input.sourceCIF);
        if (!isEligibleAccount) {
            logger_1.default.error(`Account :${input.sourceAccountNo} is not eligible to disburse trx`);
            throw new AppError_1.TransferAppError(`Account :${input.sourceAccountNo} is not eligible to disburse transaction!`, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.UNAUTHORIZED);
        }
    }
});
const executeTransaction = (externalInput, idempotencyKey) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionModel = buildNewTransactionModel(externalInput, idempotencyKey);
    const existingTransaction = yield transactionIdempotency_helper_1.default.matchExistingTransactionByIdempotencyKey(transactionModel);
    if (existingTransaction) {
        return existingTransaction;
    }
    yield transaction_helper_1.default.validateExternalIdAndPaymentType(transactionModel);
    const model = yield transaction_repository_1.default.createIfNotExist(transactionModel);
    monitoringEvent_producer_1.default.sendMonitoringEventMessage(module_message_1.MonitoringEventType.PROCESSING_STARTED, model);
    const input = yield populateExternalTransactionInput(externalInput, model);
    yield validateIrisPayment(input);
    return processIncomingTransaction(input);
});
const processRefund = (input, template) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info('processRefund: Requested transaction for refund.');
    const sourceBankCode = getEligibleBankCode(input.sourceBankCode, input.sourceAccountNo);
    const beneficiaryBankCode = getEligibleBankCode(input.beneficiaryBankCode, input.beneficiaryAccountNo);
    const originalTransaction = yield template.getOriginalTransaction(input);
    refund_validator_1.default.validateRefundRequest(Object.assign(Object.assign({}, input), { sourceBankCode,
        beneficiaryBankCode }), originalTransaction);
    const refundTxn = yield executeTransaction(input);
    logger_1.default.info('processRefund: Updating Transaction records with refund details');
    yield transaction_service_1.default.updateOriginalTransactionWithRefund(originalTransaction, refundTxn);
    yield transaction_service_1.default.updateRefundTransaction(originalTransaction, refundTxn);
    return refundTxn;
});
const refundTemplates = [
    euronetRefund_template_1.default,
    jagoPayRefund_template_1.default,
    sknRtgsRefund_template_1.default,
    bifastRefund_template_1.default,
    atomeRefund_template_1.default
];
const matchRefundTemplate = (transactionInput) => {
    const template = refundTemplates.find(refundTemplate => refundTemplate.isApplicable(transactionInput));
    if (template) {
        logger_1.default.info(`Transaction Refund template found for paymentServiceType : ${transactionInput.paymentServiceType}`);
    }
    return template;
};
const getAvailableBalance = (tx) => __awaiter(void 0, void 0, void 0, function* () {
    let accountNumber;
    if (tx.sourceBankCodeChannel === module_common_1.BankChannelEnum.INTERNAL) {
        accountNumber = tx.sourceAccountNo;
    }
    else if (tx.beneficiaryBankCodeChannel === module_common_1.BankChannelEnum.INTERNAL) {
        accountNumber = tx.beneficiaryAccountNo;
    }
    return accountNumber
        ? deposit_repository_1.default.getAvailableBalance(accountNumber)
        : undefined;
});
/**
 * This will handle any transaction that come from 3rd parties or switching
 *
 * @param transactionInput
 * @param idempotencyKey
 */
const createExternalTransaction = (transactionInput, idempotencyKey) => __awaiter(void 0, void 0, void 0, function* () {
    common_util_1.trimStringValuesFromObject(transactionInput);
    logger_1.default.info(`createExternalTransaction : external transaction payload ${transactionInput.paymentServiceType}, 
      idempotencyKey: ${idempotencyKey},
      externalId: ${transactionInput.externalId},
      sourceBankCode: ${transactionInput.sourceBankCode}, 
      sourceAccountNumber: ${transactionInput.sourceAccountNo},
      beneficiaryBankCode: ${transactionInput.beneficiaryBankCode},
      beneficiaryAccountNumber: ${transactionInput.beneficiaryAccountNo}, 
      interchange: ${transactionInput.interchange}`);
    const normalizedTrxInput = transactionInput;
    const template = matchRefundTemplate(normalizedTrxInput);
    if (template) {
        return processRefund(normalizedTrxInput, template);
    }
    const transaction = yield executeTransaction(normalizedTrxInput, idempotencyKey);
    const availableBalance = yield getAvailableBalance(transaction);
    return Object.assign(Object.assign({}, transaction), { availableBalance });
});
const getExternalTransactionFee = (transactionInput) => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield populateExternalTransactionInput(transactionInput);
    const recommendationService = yield transactionRecommend_service_1.default.getRecommendService(input);
    const result = {
        externalId: transactionInput.externalId,
        feeAmount: (recommendationService.feeData &&
            recommendationService.feeData[0] &&
            recommendationService.feeData[0].feeAmount) ||
            0
    };
    return result;
});
const createExternalTopupTransaction = (input) => __awaiter(void 0, void 0, void 0, function* () {
    common_util_1.trimStringValuesFromObject(input);
    const depositTransactionInput = yield populateExternalWalletTransactionInput(input);
    return processIncomingTransaction(depositTransactionInput);
});
const externalTransactionService = logger_1.wrapLogs({
    createExternalTransaction,
    getExternalTransactionFee,
    createExternalTopupTransaction
});
exports.default = externalTransactionService;
//# sourceMappingURL=transactionExternal.service.js.map