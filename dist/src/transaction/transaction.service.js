"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const module_common_1 = require("@dk/module-common");
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const transaction_util_1 = require("./transaction.util");
const transactionExecution_service_1 = __importStar(require("./transactionExecution.service"));
const virtualAccount_repository_1 = __importDefault(require("../virtualAccount/virtualAccount.repository"));
const transactionRecommend_service_1 = __importDefault(require("./transactionRecommend.service"));
const transaction_repository_1 = __importDefault(require("./transaction.repository"));
const transaction_helper_1 = __importDefault(require("./transaction.helper"));
const transaction_producer_1 = __importDefault(require("./transaction.producer"));
const lodash_1 = __importStar(require("lodash"));
const module_message_1 = require("@dk/module-message");
const transaction_enum_1 = require("./transaction.enum");
const transaction_constant_1 = __importStar(require("./transaction.constant"));
const customer_service_1 = __importDefault(require("../customer/customer.service"));
const dateUtils_1 = require("../common/dateUtils");
const bifast_enum_1 = require("../bifast/bifast.enum");
const alto_constant_1 = require("../alto/alto.constant");
const alto_service_1 = __importDefault(require("../alto/alto.service"));
const monitoringEvent_producer_1 = __importDefault(require("../monitoringEvent/monitoringEvent.producer"));
const fee_helper_1 = __importDefault(require("../fee/fee.helper"));
const fee_batch_1 = __importDefault(require("../fee/fee.batch"));
const { BENEFICIARY_BANK_CODE_COL, SOURCE_BANK_CODE_COL, SOURCE_CIF_COL, BENEFICIARY_ACCOUNT_NO_COL } = transaction_constant_1.default;
const findByIdAndUpdate = (txnId, transactionUpdate) => __awaiter(void 0, void 0, void 0, function* () {
    const transaction = yield transaction_repository_1.default.findByIdAndUpdate(txnId, transactionUpdate);
    if (!transaction) {
        logger_1.default.error(`No transaction found with transactionId:${txnId}`);
        throw new AppError_1.TransferAppError(`No transaction found with transactionId:${txnId}`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.TRANSACTION_NOT_FOUND);
    }
    return transaction;
});
const updateFailedTransaction = (transactionInput, error, transactionStatus = transaction_enum_1.TransactionStatus.DECLINED, transferJourneyStatus = transaction_enum_1.TransferJourneyStatusEnum.TRANSFER_FAILURE) => __awaiter(void 0, void 0, void 0, function* () {
    let updatedTransaction = null;
    if (transactionInput.transactionId) {
        updatedTransaction = yield findByIdAndUpdate(transactionInput.transactionId, {
            status: transactionStatus,
            journey: transaction_helper_1.default.getStatusesToBeUpdated(transferJourneyStatus, transactionInput),
            failureReason: {
                type: errors_1.ERROR_CODE[error.errorCode],
                actor: error.actor,
                detail: error.detail
            },
            ownerCustomerId: transactionInput.ownerCustomerId
        });
    }
    return updatedTransaction;
});
function buildRejectedHandler(actor, detail, key) {
    return (error) => {
        throw new AppError_1.TransferAppError(detail, actor, errors_1.ERROR_CODE[error.errorCode], [transaction_util_1.getErrorMessage(key)]);
    };
}
const getBankInfo = (input) => Promise.all([
    transaction_helper_1.default
        .getBankCodeInfo(input.sourceBankCode)
        .catch(buildRejectedHandler(transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, `Failed to get source bank code info for: ${input.sourceBankCode}!`, SOURCE_BANK_CODE_COL)),
    transaction_helper_1.default
        .getBankCodeInfo(input.beneficiaryBankCode)
        .catch(buildRejectedHandler(transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, `Failed to get beneficiary bank code info for: ${input.beneficiaryBankCode}!`, BENEFICIARY_BANK_CODE_COL))
]);
const getTransactionAmount = (transactionAmount, paymentServiceCode) => paymentServiceCode &&
    (paymentServiceCode.includes('RTGS') || paymentServiceCode.includes('SKN'))
    ? 0
    : transactionAmount;
/**
 * Populate the transaction input before processing. The caller can decide if the inquiry beneficiary account is needed.
 * @param input Transaction input.
 * @param beneficiaryInquiryPredicate Predicate function to determine if we need to make a beneficiary account inquiry.
 * @param sourceInquiryPredicate Predicate function to determine if we need to make a source account inquiry.
 */
const populateTransactionInfo = (input, beneficiaryInquiryPredicate = () => true, sourceInquiryPredicate = () => true) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        input = Object.assign(Object.assign({}, input), { journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.REQUEST_RECEIVED, input) });
        const [sourceBankInfo, beneficiaryBankInfo] = yield getBankInfo(input);
        // Keep track input cif
        const executorCIF = input.sourceCIF;
        const destinationCIF = input.beneficiaryCIF;
        let sourceAccountInfo = {}, beneficiaryAccountInfo = {};
        let sourceInquiryPredicateResult = sourceInquiryPredicate(input.paymentServiceType);
        let beneficiaryInquiryPredicateResult = beneficiaryBankInfo &&
            beneficiaryInquiryPredicate(beneficiaryBankInfo, input.paymentServiceType);
        input = Object.assign(Object.assign({}, input), { journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.INQUIRY_STARTED, input) });
        const [beneficiaryAccountNo, bifastBankCode] = transaction_helper_1.default.getBifastBeneficiaryAdditionalInfo(input);
        if (bifastBankCode && beneficiaryBankInfo) {
            beneficiaryBankInfo.bankCodeId = bifastBankCode.bankCodeId;
        }
        try {
            [sourceAccountInfo, beneficiaryAccountInfo] = yield Promise.all([
                transaction_helper_1.default.getSourceAccount(input.sourceAccountNo, executorCIF, sourceBankInfo, input.sourceAccountName, input.transactionAmount, sourceInquiryPredicateResult),
                transaction_helper_1.default.getBeneficiaryAccount(beneficiaryAccountNo, executorCIF, beneficiaryBankInfo, input.beneficiaryAccountName, getTransactionAmount(input.transactionAmount, input.paymentServiceCode), input.sourceAccountNo, beneficiaryInquiryPredicateResult)
            ]);
        }
        catch (err) {
            logger_1.default.error(`populateTransactionInfo: Error while doing account inquiry journey: ${JSON.stringify(input.journey)}`);
            throw err;
        }
        if (!sourceInquiryPredicateResult) {
            sourceAccountInfo = Object.assign({ cif: input.sourceCIF }, sourceAccountInfo);
        }
        const isTransferSameCifForMigratingAccounts = transaction_util_1.checkTransferSameCifForMigratingAccounts(sourceAccountInfo, beneficiaryAccountInfo);
        if (!isTransferSameCifForMigratingAccounts) {
            logger_1.default.error('populateTransactionInfo: source or beneficiary account is being migrated');
            throw new AppError_1.TransferAppError('Source or beneficiary account is being migrated!', transaction_enum_1.TransferFailureReasonActor.JAGO, errors_1.ERROR_CODE.SOURCE_OR_BENEFICIARY_ACCOUNT_IS_BEING_MIGRATED_TO_SHARIA);
        }
        // get back owner cif
        if (sourceAccountInfo) {
            input.sourceCIF = sourceAccountInfo.cif;
        }
        if (beneficiaryAccountInfo) {
            input.beneficiaryCIF = beneficiaryAccountInfo.cif;
        }
        let actualBeneficiaryBankInfo = undefined;
        if (bifastBankCode) {
            transaction_helper_1.default.validateProxyTypeAndBankCode(input, beneficiaryAccountInfo);
            if (beneficiaryAccountInfo.bankCode === transaction_constant_1.JAGO_BANK_CODE) {
                /*
                  For Jago proxy,updating paymentServiceCode from BIFAST_OUTGOING to undefined.
                  Recommendation service can calculate service on its own.
                */
                input.paymentServiceCode = undefined;
            }
            if (beneficiaryAccountInfo.bankCode) {
                actualBeneficiaryBankInfo = yield transaction_helper_1.default.getBankCode(beneficiaryAccountInfo.bankCode);
                input.beneficiaryBankCode =
                    ((_a = actualBeneficiaryBankInfo) === null || _a === void 0 ? void 0 : _a.bankCodeId) || input.beneficiaryBankCode;
            }
        }
        return transaction_helper_1.default.populateTransactionInput(input, sourceAccountInfo, sourceBankInfo, beneficiaryAccountInfo, actualBeneficiaryBankInfo || beneficiaryBankInfo, executorCIF, destinationCIF);
    }
    catch (err) {
        logger_1.default.error(`populateTransactionInfo: Error while making an inquiry ${err.message}`);
        const failedTransaction = yield updateFailedTransaction(input, err);
        failedTransaction &&
            monitoringEvent_producer_1.default.sendMonitoringEventMessage(module_message_1.MonitoringEventType.PROCESSING_FINISHED, failedTransaction);
        throw err;
    }
});
const executeTransaction = (transactionInput, executionHooks = transactionExecution_service_1.defaultExecutionHooks) => __awaiter(void 0, void 0, void 0, function* () {
    return transactionExecution_service_1.default.executeTransaction(transactionInput, executionHooks);
});
const generateValidationErrorForWallet = (input) => {
    const missingKeys = [];
    if (!input.sourceCIF) {
        missingKeys.push(SOURCE_CIF_COL);
    }
    if (!input.beneficiaryAccountNo) {
        missingKeys.push(BENEFICIARY_ACCOUNT_NO_COL);
    }
    if (!input.beneficiaryBankCode) {
        missingKeys.push(BENEFICIARY_BANK_CODE_COL);
    }
    const code = errors_1.ERROR_CODE.INVALID_MANDATORY_FIELDS;
    return new AppError_1.TransferAppError('Invalid mandatory fields!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_REQUEST, missingKeys.map(key => ({
        message: errors_1.ErrorList[code].message,
        code: code,
        key: key
    })));
};
const populateBeneficiaryAccountFromVirtualAccount = (virtualAccountNumber, beneficiaryBankCode, ginBankCodeInfo) => __awaiter(void 0, void 0, void 0, function* () {
    const virtualAccount = yield virtualAccount_repository_1.default.inquiryVirtualAccount(virtualAccountNumber);
    if (virtualAccount.institution.bankCode !== beneficiaryBankCode) {
        logger_1.default.error(`Beneficiary bank code ${beneficiaryBankCode} does not match with virtual bankCode ${virtualAccount.institution.bankCode}.`);
        throw new AppError_1.TransferAppError(`Beneficiary bank code ${beneficiaryBankCode} does not match with virtual bankCode ${virtualAccount.institution.bankCode}.`, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_REGISTER_PARAMETER, [transaction_util_1.getErrorMessage(BENEFICIARY_BANK_CODE_COL)]);
    }
    return yield transaction_helper_1.default.getAccountInfo(virtualAccount.institution.accountId, virtualAccount.institution.cif, ginBankCodeInfo);
});
const populateWalletTransactionInfo = (input) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (input.sourceCIF &&
            input.beneficiaryAccountNo &&
            input.beneficiaryBankCode) {
            const [beneficiaryBankCodeInfo, ginBankCodeInfo] = yield Promise.all([
                transaction_helper_1.default
                    .getBankCodeMapping(input.beneficiaryBankCode)
                    .catch(buildRejectedHandler(transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, `Failed to get beneficiary bank code mapping for: ${input.sourceBankCode}!`, BENEFICIARY_BANK_CODE_COL)),
                transaction_helper_1.default
                    .getBankCodeMapping(module_common_1.Constant.GIN_BANK_CODE_ID)
                    .catch(buildRejectedHandler(transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, `Failed to get source bank code mapping for: ${module_common_1.Constant.GIN_BANK_CODE_ID}!`, SOURCE_BANK_CODE_COL))
            ]);
            const sourceAccountInfo = yield transaction_helper_1.default.getSourceAccount(input.sourceAccountNo, input.sourceCIF, ginBankCodeInfo, input.sourceAccountName, input.transactionAmount);
            let beneficiaryAccountNo = input.beneficiaryAccountNo;
            let beneficiaryAccountInfo;
            if (beneficiaryBankCodeInfo.channel === module_common_1.BankChannelEnum.PARTNER) {
                beneficiaryAccountInfo = yield populateBeneficiaryAccountFromVirtualAccount(beneficiaryAccountNo, input.beneficiaryBankCode, ginBankCodeInfo);
            }
            else {
                beneficiaryAccountInfo = yield transaction_helper_1.default.getAccountInfo(beneficiaryAccountNo, undefined, beneficiaryBankCodeInfo, undefined, input.transactionAmount);
            }
            return transaction_helper_1.default.populateTransactionInput(Object.assign(Object.assign({}, input), { sourceCIF: sourceAccountInfo.cif }), Object.assign(Object.assign({}, sourceAccountInfo), { bankCode: module_common_1.Constant.GIN_BANK_CODE_ID // main account api don't provide bank code like inquiry
             }), ginBankCodeInfo, beneficiaryAccountInfo, beneficiaryBankCodeInfo, input.sourceCIF // this one will similar executorCIF for keep track
            );
        }
        else {
            throw generateValidationErrorForWallet(input);
        }
    }
    catch (err) {
        logger_1.default.error(`populateWalletTransactionInfo: Error while making an inquiry ${err.message}`);
        input.transactionId &&
            (yield findByIdAndUpdate(input.transactionId, {
                status: transaction_enum_1.TransactionStatus.DECLINED,
                journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.TRANSFER_FAILURE, input)
            }));
        throw err;
    }
});
const getTcByInterchange = (transactionCodeMapping, priority) => {
    let transactionCode;
    if (transactionCodeMapping) {
        transactionCode = transactionCodeMapping.find(tc => tc.interchange === priority);
    }
    return transactionCode ? [transactionCode] : transactionCodeMapping;
};
const populateBifastAdditionalInfo = (transactionInput, input) => {
    // if bank code is bifast, then populate additionalInformation3 and additionalInformation4
    if (transactionInput.beneficiaryBankCode &&
        transaction_util_1.isBIFastBank(transactionInput.beneficiaryBankCode)) {
        transactionInput.additionalInformation3 = lodash_1.startsWith(input.beneficiaryAccountNo, transaction_enum_1.PhoneCode.PLUS62)
            ? bifast_enum_1.BeneficiaryProxyType.PHONE_NUMBER
            : bifast_enum_1.BeneficiaryProxyType.EMAIL;
        transactionInput.additionalInformation4 = input.beneficiaryAccountNo;
    }
};
const getTransactionInfoForRecommendPaymentService = (input) => __awaiter(void 0, void 0, void 0, function* () {
    let transactionInput = Object.assign(Object.assign({}, input), { paymentServiceCode: input.paymentServiceCode || '' });
    populateBifastAdditionalInfo(transactionInput, input);
    if (input.paymentServiceType === module_common_1.PaymentServiceTypeEnum.WALLET) {
        return yield populateWalletTransactionInfo(transactionInput);
    }
    else {
        // if bank code is external, then we don't need to inquiry to switching.
        // if bank code is bifast, then we'll do inquiry to bifast channel.
        return yield populateTransactionInfo(transactionInput, bankCode => {
            return !transaction_util_1.isExternalBank(bankCode) || transaction_util_1.isBIFastBank(bankCode.bankCodeId);
        });
    }
});
const recommendPaymentService = (input) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Entering recommendPaymentService with sourceAccountNo :${input.sourceAccountNo},
  sourceBankCode: ${input.sourceBankCode}, externalId: ${input.externalId}, beneficiaryAccountNumber: ${input.beneficiaryAccountNo}
  beneficiaryBankCode: ${input.beneficiaryBankCode}, paymentServiceType: ${input.paymentServiceType}`);
    const { transactionAmount, sourceAccountNo } = input;
    const transactionInput = yield getTransactionInfoForRecommendPaymentService(input);
    let recommendedServices = yield transactionRecommend_service_1.default.getRecommendServices(transactionInput);
    recommendedServices = yield transaction_helper_1.default.mapAuthenticationRequired(recommendedServices, transactionAmount, sourceAccountNo);
    recommendedServices.map(serviceRecommendation => {
        serviceRecommendation.debitTransactionCode = getTcByInterchange(serviceRecommendation.debitTransactionCode, transactionInput.debitPriority);
        serviceRecommendation.creditTransactionCode = getTcByInterchange(serviceRecommendation.creditTransactionCode, transactionInput.creditPriority);
    });
    return recommendedServices;
});
const produceFailedTransferNotificationMessage = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info('produce transfer failed message for failed transaction...');
    transaction_producer_1.default.sendFailedTransferNotification(transaction);
});
const validate = (input) => {
    if (input.paymentServiceType === module_common_1.PaymentServiceTypeEnum.PAYROLL &&
        !input.interchange) {
        logger_1.default.error('populateTransactionInfo: Interchange required for PAYROLL Transaction');
        throw new AppError_1.TransferAppError('Interchange required for payroll Transaction!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.PAYROLL_INTERCHANGE_REQUIRED);
    }
};
const isRdnTransaction = (model) => model.paymentServiceType === module_common_1.PaymentServiceTypeEnum.RDN;
const beneficiaryInquiryPredicate = (bankCode, paymentServiceType) => bankCode.channel !== module_common_1.BankChannelEnum.WINCOR &&
    bankCode.channel !== module_common_1.BankChannelEnum.TOKOPEDIA &&
    bankCode.bankCodeId != transaction_constant_1.BANK_KSEI &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.TD_PENALTY &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.BRANCH_WITHDRAWAL &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.CARD_OPENING &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.LOAN_ADMIN_FEE &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.OVERBOOKING &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.MAIN_POCKET_TO_GL_TRANSFER &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.LOAN_FORWARD_PAYMENT_DEPOSIT &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.TD_SHARIA_PLACEMENT &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.TD_PAYMENT_GL &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.LOAN_GL_DISBURSEMENT;
const sourceInquiryPredicate = (paymentServiceType) => paymentServiceType !== module_common_1.PaymentServiceTypeEnum.OFFER &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.PAYROLL &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.CASHBACK &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.BRANCH_DEPOSIT &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.LOAN_DISBURSEMENT &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.SETTLEMENT &&
    paymentServiceType !==
        module_common_1.PaymentServiceTypeEnum.LOAN_FORWARD_PAYMENT_WITHDRAWAL &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.LOAN_REPAYMENT &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.TD_SHARIA_REPAYMENT &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.BONUS_INTEREST &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.MIGRATION_TRANSFER &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.LOAN_REFUND &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.TD_MANUAL_PAYMENT_GL &&
    paymentServiceType !== module_common_1.PaymentServiceTypeEnum.LOAN_GL_REPAYMENT;
const createTransferTransaction = (transactionInput) => __awaiter(void 0, void 0, void 0, function* () {
    validate(transactionInput);
    const transactionModel = transaction_helper_1.default.buildOutgoingTransactionModel(transactionInput);
    yield transaction_helper_1.default.validateExternalIdAndPaymentType(transactionModel);
    const model = yield transaction_repository_1.default.create(transactionModel);
    monitoringEvent_producer_1.default.sendMonitoringEventMessage(module_message_1.MonitoringEventType.PROCESSING_STARTED, model);
    let input = yield populateTransactionInfo(Object.assign(Object.assign(Object.assign({}, transactionInput), model), { transactionId: model.id }), beneficiaryInquiryPredicate, sourceInquiryPredicate);
    input = Object.assign(Object.assign({}, input), { journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.INQUIRY_FETCHED, input) });
    return yield executeTransaction(input, {
        onFailedTransaction: (model) => __awaiter(void 0, void 0, void 0, function* () {
            var _b;
            if (!((_b = model.paymentInstructionExecution) === null || _b === void 0 ? void 0 : _b.topics.includes(module_message_1.PaymentInstructionTopicConstant.UPDATE_TRANSACTION_STATUS)) ||
                !isRdnTransaction(model)) {
                produceFailedTransferNotificationMessage(model);
            }
        })
    });
});
const createDepositTransaction = (transactionInput) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionModel = transaction_helper_1.default.buildOutgoingTransactionModel(transactionInput);
    yield transaction_helper_1.default.validateExternalIdAndPaymentType(transactionModel);
    const model = yield transaction_repository_1.default.create(transactionModel);
    monitoringEvent_producer_1.default.sendMonitoringEventMessage(module_message_1.MonitoringEventType.PROCESSING_STARTED, model);
    const input = yield populateTransactionInfo(Object.assign(Object.assign(Object.assign({}, transactionInput), model), { transactionId: model.id }), beneficiaryInquiryPredicate, sourceInquiryPredicate);
    return executeTransaction(input, {
        onRecommendedServiceCode: (code) => __awaiter(void 0, void 0, void 0, function* () {
            // a deposit transaction require the credit transaction must be existed and debit must be not existed
            if (!code.creditTransactionCode || !code.creditTransactionCode.length) {
                logger_1.default.error('createDepositTransaction: missing credit transaction code');
                throw new AppError_1.TransferAppError('Missing credit transaction code while creating deposit transaction!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.MISSING_CREDIT_TRANSACTION_CODE);
            }
            if (code.debitTransactionCode && code.debitTransactionCode.length) {
                logger_1.default.error('createDepositTransaction: missing debit transaction code');
                throw new AppError_1.TransferAppError('Missing debit transaction code while creating deposit transaction!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE);
            }
            return;
        })
    });
});
const createWithdrawTransaction = (transactionInput) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionModel = transaction_helper_1.default.buildOutgoingTransactionModel(transactionInput);
    yield transaction_helper_1.default.validateExternalIdAndPaymentType(transactionModel);
    const model = yield transaction_repository_1.default.create(transactionModel);
    monitoringEvent_producer_1.default.sendMonitoringEventMessage(module_message_1.MonitoringEventType.PROCESSING_STARTED, model);
    const input = yield populateTransactionInfo(Object.assign(Object.assign(Object.assign({}, transactionInput), model), { transactionId: model.id }));
    return executeTransaction(input, {
        onRecommendedServiceCode: (code) => __awaiter(void 0, void 0, void 0, function* () {
            // a withdraw transaction require the debit transaction must be existed and credit transaction must be not existed
            if (code.creditTransactionCode && code.creditTransactionCode.length) {
                logger_1.default.error('createWithdrawTransaction: missing credit transaction code');
                throw new AppError_1.TransferAppError('Missing credit transaction code while creating withdraw transaction!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.MISSING_CREDIT_TRANSACTION_CODE);
            }
            if (!code.debitTransactionCode || !code.debitTransactionCode.length) {
                logger_1.default.error('createWithdrawTransaction: missing debit transaction code');
                throw new AppError_1.TransferAppError('Missing debit transaction code while creating withdraw transaction!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE);
            }
            return;
        })
    });
});
const getTransaction = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const transaction = yield transaction_repository_1.default.getByKey(id);
    if (!transaction) {
        logger_1.default.error(`getTransaction: Matching txn was not found for transactionId: ${id}`);
        throw new AppError_1.TransferAppError(`getTransaction: Matching txn was not found for transactionId: ${id}`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.TRANSACTION_NOT_FOUND);
    }
    return transaction;
});
const createInternalTopupTransaction = (input) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionModel = transaction_helper_1.default.buildOutgoingTransactionModel(input);
    yield transaction_helper_1.default.validateExternalIdAndPaymentType(transactionModel);
    const model = yield transaction_repository_1.default.create(transactionModel);
    monitoringEvent_producer_1.default.sendMonitoringEventMessage(module_message_1.MonitoringEventType.PROCESSING_STARTED, model);
    let transactionInput = yield populateWalletTransactionInfo(Object.assign(Object.assign(Object.assign({}, input), model), { transactionId: model.id }));
    transactionInput = Object.assign(Object.assign({}, transactionInput), { journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.INQUIRY_FETCHED, transactionInput) });
    return executeTransaction(transactionInput);
});
const updateOriginalTransactionWithRefund = (originalTxn, refundTxn) => __awaiter(void 0, void 0, void 0, function* () {
    var _c;
    const earlierRefund = originalTxn.refund;
    if (earlierRefund &&
        earlierRefund.remainingAmount &&
        earlierRefund.transactions) {
        earlierRefund.remainingAmount =
            earlierRefund.remainingAmount - refundTxn.transactionAmount;
        earlierRefund.transactions.push(refundTxn.id);
    }
    else {
        originalTxn = Object.assign(Object.assign({}, originalTxn), { refund: {
                remainingAmount: originalTxn.transactionAmount - refundTxn.transactionAmount,
                transactions: [refundTxn.id]
            } });
    }
    logger_1.default.info(`Updating original transaction with refund details
  transactions: ${JSON.stringify((_c = originalTxn.refund) === null || _c === void 0 ? void 0 : _c.transactions)}`);
    return transaction_repository_1.default.update(originalTxn.id, originalTxn);
});
const updateRefundTransaction = (originalTxn, refundTxn) => __awaiter(void 0, void 0, void 0, function* () {
    var _d;
    refundTxn = Object.assign(Object.assign({}, refundTxn), { refund: {
            originalTransactionId: originalTxn.id,
            revertPaymentServiceCode: originalTxn.paymentServiceCode
        } });
    logger_1.default.info(`Updating refund transaction with orginal transaction details
  originalTransactionId: ${JSON.stringify((_d = refundTxn.refund) === null || _d === void 0 ? void 0 : _d.originalTransactionId)}`);
    return transaction_repository_1.default.update(refundTxn.id, refundTxn);
});
const getTransactionListByCriteria = (input) => __awaiter(void 0, void 0, void 0, function* () {
    let customerInfo;
    if (input.customerId) {
        customerInfo = yield customer_service_1.default.getCustomerById(input.customerId);
    }
    const transactions = yield transaction_repository_1.default.getTransactionListByCriteria(input, customerInfo && customerInfo.cif);
    if (lodash_1.isEmpty(transactions.list)) {
        logger_1.default.error(`getTransactionList: Matching txn was not found`);
        throw new AppError_1.TransferAppError('getTransactionListByCriteria: Matching txn was not found', transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.TRANSACTION_NOT_FOUND);
    }
    return transactions;
});
const shouldGetTransactionWithoutOwnerCustomerId = (input, transactions) => {
    const lastCreatedAt = transactions[transactions.length - 1].createdAt;
    const lastTransactionDate = lastCreatedAt
        ? new Date(lastCreatedAt).setHours(0, 0, 0, 0)
        : undefined;
    const startDate = input.startDate
        ? dateUtils_1.toDate(input.startDate).setHours(0, 0, 0, 0)
        : undefined;
    return lastTransactionDate && startDate
        ? startDate < lastTransactionDate
        : false;
};
const getTransactionList = (input, fetchRefund = false) => __awaiter(void 0, void 0, void 0, function* () {
    transaction_helper_1.default.validateTransactionListRequest(input);
    let transactions = yield transaction_repository_1.default.getTransactionListByCriteria(input);
    if (lodash_1.isEmpty(transactions.list) ||
        shouldGetTransactionWithoutOwnerCustomerId(input, transactions.list)) {
        logger_1.default.info(`getTransactionList: Getting transactions without ownerCustomerId query`);
        transactions = yield getTransactionListByCriteria(input);
    }
    if (!fetchRefund) {
        return { transactions: transactions, refundTransactions: [] };
    }
    const refundedTransactionIds = transactions.list
        .map(transaction => {
        if (!(transaction.refund && transaction.refund.transactions)) {
            return [];
        }
        return transaction.refund.transactions;
    })
        .reduce((prevValue, currValue) => {
        return prevValue.concat(currValue);
    }, []);
    let refundTransactions = [];
    if (refundedTransactionIds.length > 0) {
        const refundTransactionQuery = {
            _id: { $in: refundedTransactionIds }
        };
        const refundTransactionProjection = {
            status: 1,
            transactionAmount: 1,
            createdAt: 1,
            updatedAt: 1
        };
        refundTransactions = transaction_util_1.mapTransactionModelToRefundTransaction(yield transaction_repository_1.default.findTransactionByQuery(refundTransactionQuery, refundTransactionProjection));
    }
    return {
        transactions: transactions,
        refundTransactions: refundTransactions
    };
});
const checkTransactionStatus = (input) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // Check status to DB
        let transaction = yield transaction_repository_1.default.getTransactionStatusByCriteria(input);
        if (!transaction) {
            logger_1.default.error(`checkTransactionStatus: Matching txn was not found for 
        ownerCustomerId: ${input.customerId},
        paymentInstructionId: ${input.paymentInstructionId},
        transactionDate: ${input.transactionDate}`);
            throw new AppError_1.TransferAppError('Matching transaction was not found!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.TRANSACTION_NOT_FOUND);
        }
        if ([transaction_enum_1.TransactionStatus.SUCCEED, transaction_enum_1.TransactionStatus.DECLINED].includes(transaction.status)) {
            return { status: transaction.status };
        }
        // Check status with third party
        return transactionExecution_service_1.default.executeCheckStatus(transaction);
    }
    catch (error) {
        if (error instanceof AppError_1.TransferAppError)
            throw error;
        logger_1.default.error(`Unexpected error while checkTransactionStatus: ${error.message}`);
        throw new AppError_1.TransferAppError(`Unexpected error while checking transaction status with message: ${error.message}!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.UNEXPECTED_ERROR);
    }
});
const resolveTransactionStatus = (minTransactionAge = transaction_constant_1.SCHEDULED_RESOLVE_STATUS_MIN_TRANSACTION_AGE_IN_MINUTES, maxTransactionAge = transaction_constant_1.SCHEDULED_RESOLVE_STATUS_MAX_TRANSACTION_AGE_IN_MINUTES, recordLimit = transaction_constant_1.SCHEDULED_RESOLVE_STATUS_FETCH_RECORD_LIMIT) => __awaiter(void 0, void 0, void 0, function* () {
    var _e;
    const paymentServiceCodes = transaction_constant_1.SCHEDULED_RESOLVE_STATUS_TRANSACTION_CODES;
    logger_1.default.debug(`Resolving transaction status for ${paymentServiceCodes}`);
    const pendingTransactions = yield transaction_repository_1.default.getPendingTransactionList(paymentServiceCodes, minTransactionAge, maxTransactionAge, recordLimit);
    for (const pendingTransaction of pendingTransactions) {
        try {
            logger_1.default.info(`Resolving status for transaction with:
          ID: '${pendingTransaction.id}'
          External ID: ${pendingTransaction.externalId}
          Payment Service Code '${pendingTransaction.paymentServiceCode}'`);
            if (pendingTransaction.paymentServiceCode === alto_constant_1.QRIS_PAYMENT_SERVICE_CODE) {
                yield alto_service_1.default.executeCheckStatus(pendingTransaction);
            }
            else if ((_e = pendingTransaction.paymentServiceCode) === null || _e === void 0 ? void 0 : _e.includes(module_common_1.BankChannelEnum.BIFAST)) {
                yield transactionExecution_service_1.default.executeCheckStatus(pendingTransaction);
            }
            else {
                logger_1.default.info(`Cannot resolve status for transaction ${pendingTransaction.id}. Reason: Not supported.`);
            }
        }
        catch (err) {
            logger_1.default.error(`Cannot resolve status for transaction ${pendingTransaction.id}. Reason: ${err.code}, ${err.message}`);
        }
    }
});
const getFeeAmountInputs = (transactionInput, usageCounters) => __awaiter(void 0, void 0, void 0, function* () {
    const recommendedService = yield transactionRecommend_service_1.default.getRecommendedServiceWithoutFeeData(transactionInput, usageCounters);
    return yield transaction_helper_1.default.getFeeAmountInput(transactionInput, recommendedService, usageCounters);
});
const getUpdatedUsageCounters = (usageCounters, feeAmountInputs) => __awaiter(void 0, void 0, void 0, function* () {
    const updatedUsageCounters = lodash_1.default.cloneDeep(usageCounters);
    for (const feeAmountInput of feeAmountInputs) {
        const transactionCode = {
            code: '',
            minAmountPerTx: NaN,
            maxAmountPerTx: NaN,
            channel: '',
            limitGroupCode: feeAmountInput.limitGroupCode,
            awardGroupCounter: feeAmountInput.awardGroupCounter
        };
        const feeUsageCounter = yield transaction_helper_1.default.getUsageCounter(transactionCode, updatedUsageCounters);
        if (!feeUsageCounter) {
            // No usage counter for this fee input
            continue;
        }
        const recordedUsageCounter = updatedUsageCounters.find(counter => counter.limitGroupCode == feeUsageCounter.limitGroupCode);
        if (recordedUsageCounter) {
            recordedUsageCounter.monthlyAccumulationTransaction++;
            recordedUsageCounter.dailyAccumulationAmount +=
                feeAmountInput.transactionAmount;
        }
    }
    return updatedUsageCounters;
});
const checkBulkTransactionFees = (bulkInputs) => __awaiter(void 0, void 0, void 0, function* () {
    var _f;
    const usageCountersRecord = new Map();
    // Generate the fee queries
    const allFeeAmountInputs = [];
    for (const transaction of bulkInputs.transactions) {
        const transactionInput = yield getTransactionInfoForRecommendPaymentService(transaction);
        const sourceCustomerId = transactionInput.sourceCustomerId;
        let usageCounters = [];
        if (!usageCountersRecord.has(sourceCustomerId)) {
            usageCounters = yield transactionRecommend_service_1.default.getCustomerUsageCounters(sourceCustomerId);
        }
        else {
            usageCounters = (_f = usageCountersRecord.get(sourceCustomerId), (_f !== null && _f !== void 0 ? _f : []));
        }
        const feeAmountInputs = yield getFeeAmountInputs(transactionInput, usageCounters);
        allFeeAmountInputs.push(feeAmountInputs);
        // At this point, usage counters is complete with both counters from ms-award and ms-entitlement
        // So when we record counters, it is a complete set
        const updatedUsageCounters = yield getUpdatedUsageCounters(usageCounters, feeAmountInputs);
        usageCountersRecord.set(sourceCustomerId, updatedUsageCounters);
    }
    // Get the basic fees and try applying custom fees if applicable
    const feeAmountQueries = yield transaction_helper_1.default.getFeeAmountQueries(lodash_1.default.flatten(allFeeAmountInputs));
    yield fee_batch_1.default.applyCustomerSpecificFeeIfAvailable(feeAmountQueries);
    // Now we can use this information to calculate the fee amounts
    // For each external ID, we select the first fee that is available
    const feesResult = new Map();
    const externalIds = new Set();
    for (const feeAmountQuery of feeAmountQueries) {
        const { input: feeAmountInput, feeMappings } = feeAmountQuery;
        const externalId = feeAmountInput.externalId;
        if (!externalId) {
            const detail = 'Expected External ID for fee amount input, but it was not set';
            logger_1.default.error(`checkBulkTransactionFees: ${detail}`);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.MISSING_INQUIRY_ID);
        }
        externalIds.add(externalId);
        if (feesResult.has(externalId)) {
            continue;
        }
        if (feeMappings.length) {
            const feeData = yield fee_helper_1.default.getFeeAmount(feeAmountInput, feeMappings[0]);
            delete feeData.interchange;
            delete feeData.feeCode;
            feesResult.set(externalId, feeData);
        }
    }
    // Fill in any missing data
    const defaultFeeDataResponse = {
        feeAmount: 0
    };
    for (const externalId of externalIds) {
        if (!feesResult.has(externalId)) {
            feesResult.set(externalId, defaultFeeDataResponse);
        }
    }
    return Array.from(feesResult, ([externalId, feeData]) => (Object.assign(Object.assign({}, feeData), { externalId })));
});
const transactionService = logger_1.wrapLogs({
    getTransaction,
    recommendPaymentService,
    createTransferTransaction,
    createDepositTransaction,
    createWithdrawTransaction,
    createInternalTopupTransaction,
    populateTransactionInfo,
    updateRefundTransaction,
    updateOriginalTransactionWithRefund,
    getTransactionList,
    findByIdAndUpdate,
    updateFailedTransaction,
    checkTransactionStatus,
    resolveTransactionStatus,
    checkBulkTransactionFees
});
exports.default = transactionService;
//# sourceMappingURL=transaction.service.js.map