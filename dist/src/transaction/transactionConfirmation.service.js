"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_util_1 = require("./transaction.util");
const logger_1 = __importStar(require("../logger"));
const errors_1 = require("../common/errors");
const transaction_constant_1 = require("./transaction.constant");
const transaction_repository_1 = __importDefault(require("./transaction.repository"));
const transactionReversal_helper_1 = __importDefault(require("./transactionReversal.helper"));
const transactionCoreBanking_helper_1 = __importDefault(require("./transactionCoreBanking.helper"));
const transaction_producer_1 = __importDefault(require("./transaction.producer"));
const AppError_1 = require("../errors/AppError");
const transactionBlocking_helper_1 = __importDefault(require("./transactionBlocking.helper"));
const transactionExecution_service_1 = __importStar(require("./transactionExecution.service"));
const transaction_enum_1 = require("./transaction.enum");
const transactionConfirmation_helper_1 = __importDefault(require("./transactionConfirmation.helper"));
const transaction_producer_enum_1 = require("./transaction.producer.enum");
const transaction_helper_1 = __importDefault(require("./transaction.helper"));
const common_util_1 = require("../common/common.util");
const module_common_1 = require("@dk/module-common");
const contextHandler_1 = require("../common/contextHandler");
const entitlementFlag_1 = __importDefault(require("../common/entitlementFlag"));
const transactionUsage_service_1 = __importDefault(require("../transactionUsage/transactionUsage.service"));
exports.responseMessageToPaymentInstruction = (input, transaction) => {
    const result = {
        externalId: transaction.externalId,
        paymentRequestID: transaction.paymentRequestID,
        transactionAmount: transaction.transactionAmount,
        extra: transaction_util_1.updateExtraFields(transaction)
    };
    let transactionResult;
    if (input.responseCode === transaction_enum_1.ConfirmTransactionStatus.SUCCESS) {
        transactionResult = Object.assign(Object.assign({}, result), { status: transaction_enum_1.PaymentRequestStatus.SUCCEED, transactionId: transaction.id, transactionStatus: transaction_enum_1.TransactionStatus.SUCCEED, referenceId: transaction.referenceId });
    }
    else {
        transactionResult = Object.assign(Object.assign({}, result), { status: transaction_enum_1.PaymentRequestStatus.DECLINED, transactionStatus: transaction_enum_1.TransactionStatus.DECLINED, error: {
                errorCode: errors_1.ERROR_CODE.TRANSACTION_FAILED
            } });
    }
    if (transaction.paymentInstructionExecution) {
        logger_1.default.info(`executionTransaction: Producing message on topic : ${transaction.paymentInstructionExecution.topics} with
        referenceId: ${transactionResult.referenceId},
        externalId: ${transactionResult.externalId},
        transactionId: ${transactionResult.transactionId},
        status: ${transactionResult.status},
        transactionStatus: ${transactionResult.transactionStatus},
        sourceAccountNo: ${transaction.sourceAccountNo},
        beneficiaryAccountNo: ${transaction.beneficiaryAccountNo},
        paymentInstructionId: ${transaction.paymentRequestID},
        error: ${JSON.stringify(transactionResult.error)}`);
        for (const topic of transaction.paymentInstructionExecution.topics) {
            transaction_producer_1.default.sendTransactionMessage(topic, transactionResult, transaction.paymentInstructionExecution.executionType, transaction.paymentInstructionExecution.paymentInstructionCode);
        }
    }
};
exports.extractChangeFromConfirmTransactionRequest = (confirmInput, transaction, mustMatch = true) => {
    var _a;
    const { interchange } = confirmInput;
    const infoByInterchange = transaction_util_1.extractTransactionInfoByInterchange(transaction, interchange, mustMatch // interchange null must match exact default case
    );
    let fee = transaction.fees && transaction.fees[0]; // TODO: enhance when we have more than 1 fee
    const feeCodeInfo = infoByInterchange.fees && infoByInterchange.fees[0];
    if (fee && feeCodeInfo) {
        fee = Object.assign(Object.assign({}, feeCodeInfo), { id: fee.id, blockingId: fee.blockingId, idempotencyKey: fee.idempotencyKey });
    }
    const paymentRail = transaction_helper_1.default.getAdjustedPaymentRail((_a = transaction.processingInfo) === null || _a === void 0 ? void 0 : _a.paymentRail, interchange);
    const updateInfo = Object.assign(Object.assign({}, infoByInterchange), { interchange, additionalInformation1: transaction.additionalInformation1 || confirmInput.additionalInformation1, additionalInformation2: transaction.additionalInformation2 || confirmInput.additionalInformation2, additionalInformation3: transaction.additionalInformation3 || confirmInput.additionalInformation3, additionalInformation4: transaction.additionalInformation4 || confirmInput.additionalInformation4, fees: fee ? [fee] : [], processingInfo: Object.assign(Object.assign({}, transaction.processingInfo), { paymentRail: paymentRail }) });
    return updateInfo;
};
const isAlreadyConfirmed = (input, transaction) => (input.responseCode === transaction_enum_1.ConfirmTransactionStatus.SUCCESS ||
    input.responseCode === transaction_enum_1.ConfirmTransactionStatus.REQUEST_TIMEOUT) &&
    transaction.status === transaction_enum_1.TransactionStatus.SUCCEED;
const isAlreadyReversed = (transaction) => transaction.status === transaction_enum_1.TransactionStatus.DECLINED;
const isAmountValid = (input, transaction) => {
    if (transaction.paymentServiceCode === 'DIGITAL_GOODS') {
        // const fees =
        //   transaction.fees && transaction.fees.length
        //     ? sumBy(transaction.fees, 'feeAmount')
        //     : 0;
        // if (!input.amount) {
        //   return false;
        // }
        // if (input.amount && transaction.transactionAmount + fees !== input.amount) {
        //   return false;
        // }
        return true;
    }
    if (input.amount && transaction.transactionAmount !== input.amount) {
        return false;
    }
    return true;
};
const isRdnReversal = (input, transaction) => input.responseCode === transaction_enum_1.ConfirmTransactionStatus.ERROR_EXPECTED &&
    (transaction.beneficiaryAccountType === module_common_1.AccountType.RDN ||
        transaction.beneficiaryAccountType === module_common_1.AccountType.RDS);
const validateConfirmTransactionRequest = (input, transaction) => {
    if (!isAmountValid(input, transaction)) {
        logger_1.default.error(`validateConfirmTransactionRequest: Amount mismatch.`);
        throw transaction_util_1.GenerateAppError('Confirm transaction Request validation failed amount mismatch.', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_AMOUNT, transaction_constant_1.FIELD_AMOUNT);
    }
    if (isAlreadyConfirmed(input, transaction)) {
        logger_1.default.error(`validateConfirmTransactionRequest: TRANSACTION ALREADY CONFIRMED : ${transaction.id}, externalId : ${transaction.externalId}`);
        throw new AppError_1.TransferAppError(`Transaction already confirmed : ${transaction.id}, externalId : ${transaction.externalId}`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.TRANSACTION_ALREADY_CONFIRMED);
    }
    if (isAlreadyReversed(transaction)) {
        logger_1.default.error(`validateConfirmTransactionRequest: TRANSACTION ALREADY REVERSED : ${transaction.id}, externalId : ${transaction.externalId}`);
        throw new AppError_1.TransferAppError(`Transaction already reversed : ${transaction.id}, externalId : ${transaction.externalId}`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.TRANSACTION_ALREADY_REVERSED);
    }
    if (isRdnReversal(input, transaction)) {
        logger_1.default.error(`validateConfirmTransactionRequest: RDN Reversal not allowed, transactionId: : ${transaction.id}, externalId : ${transaction.externalId}`);
        throw new AppError_1.TransferAppError(`RDN Reversal not allowed, transactionId: : ${transaction.id}, externalId : ${transaction.externalId}`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.RDN_REVERSAL_NOT_ALLOWED);
    }
};
const populateConfirmTransactionResponse = (response, transaction) => {
    return Object.assign(Object.assign({}, response), { id: transaction.id, externalId: transaction.externalId, coreBankingTransactions: transaction.coreBankingTransactions });
};
const debitBlockingTransaction = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (transaction.debitTransactionCode &&
            !transaction.debitTransactionCode.endsWith(transaction_constant_1.REVERSAL_TC_SUFFIX) &&
            transaction.debitTransactionChannel &&
            transaction.blockingId &&
            transaction.cardId &&
            transaction.sourceCIF &&
            transaction.sourceAccountNo) {
            yield transactionCoreBanking_helper_1.default.submitCardTransaction(transaction.cardId, transaction.blockingId, transaction, transaction.debitTransactionCode, transaction.debitTransactionChannel);
            transaction.journey = transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_DEBITED, transaction);
        }
    }
    catch (err) {
        return common_util_1.mapTransferAppError(err, errors_1.ERROR_CODE.COREBANKING_DEBIT_FAILED, true);
    }
});
const debitBlockingFees = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (transaction.cardId &&
            transaction.sourceCIF &&
            transaction.sourceAccountNo) {
            yield transactionCoreBanking_helper_1.default.submitFeesCardTransaction(transaction.cardId, transaction);
            if (transaction.fees) {
                transaction.journey = transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.FEE_PROCESSED, transaction);
            }
        }
    }
    catch (err) {
        return common_util_1.mapTransferAppError(err, errors_1.ERROR_CODE.COREBANKING_FEES_FAILED, true);
    }
});
const executeBlockingDebitAndFees = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield debitBlockingTransaction(transaction);
        yield debitBlockingFees(transaction);
    }
    catch (error) {
        logger_1.default.error(`executeBlockingDebitAndFees: Error while settling blocked transaction ${JSON.stringify(error)}`);
        throw error;
    }
});
const submitConfirmTransaction = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    // TODO: dikshitthakral to refactor, because JAGOPAY uses same tc code for blocking and non-blocking
    if (transaction.executionType === transaction_enum_1.ExecutionTypeEnum.BLOCKING ||
        transaction.paymentServiceType === module_common_1.PaymentServiceTypeEnum.JAGOPAY) {
        yield executeBlockingDebitAndFees(transaction);
    }
    return transaction;
});
const onFailedTransaction = (transaction, executionHooks, error) => __awaiter(void 0, void 0, void 0, function* () {
    yield transactionExecution_service_1.default.onFailedTransaction(transaction, executionHooks, error);
});
const reverseTransaction = (input, transaction) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.warn(`reverseTransaction - calling reverse transaction for
      externalId: ${input.externalId},
      interchange: ${input.interchange},
      accountNumber: ${input.accountNo},
      transactionDate: ${input.transactionDate}`);
    // TODO: dikshitthakral to refactor, because JAGOPAY uses same tc code for blocking and non-blocking
    if (transaction.status === transaction_enum_1.TransactionStatus.PENDING ||
        (transaction.status === transaction_enum_1.TransactionStatus.SUBMITTED &&
            transaction.executionType === transaction_enum_1.ExecutionTypeEnum.BLOCKING) ||
        (transaction.status === transaction_enum_1.TransactionStatus.SUBMITTED &&
            transaction.paymentServiceType === module_common_1.PaymentServiceTypeEnum.JAGOPAY)) {
        try {
            yield transactionBlocking_helper_1.default.cancelBlockingTransaction(transaction);
        }
        catch (error) {
            logger_1.default.error(`reverseTransaction: error while cancelling blocking transaction : ${error &&
                error.message}`);
            if (error.errorCode ===
                errors_1.ERROR_CODE.MAMBU_TRANSACTION_AUTHORIZATION_HOLD_EXPIRED) {
                yield onFailedTransaction(transaction, transactionExecution_service_1.defaultExecutionHooks, error);
                return { transactionResponseID: input.transactionResponseID };
            }
        }
    }
    else {
        yield transactionReversal_helper_1.default.revertFailedTransaction(transaction.id, transaction_constant_1.REVERSED_REASON_ERROR_FROM_SWITCHING, transaction);
        if (transaction.refund && transaction.refund.originalTransactionId) {
            yield transactionConfirmation_helper_1.default.updateRefundAmount(transaction.refund.originalTransactionId, transaction.transactionAmount);
        }
    }
    yield onFailedTransaction(transaction, transactionExecution_service_1.defaultExecutionHooks, new AppError_1.TransferAppError('reverseTransaction', transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.REVERSE_TRANSACTION));
    return { transactionResponseID: input.transactionResponseID };
});
const queryTransaction = (input) => {
    let externalId = undefined;
    if (input.transactionDate && input.accountNo) {
        externalId = transaction_util_1.createIncomingTransactionExternalId(input.externalId, input.transactionDate, input.accountNo);
    }
    return transaction_repository_1.default.updateIfExists(input, externalId);
};
const isValidTransactionStatusForConfirmation = (transactionStatus) => {
    return (transactionStatus === transaction_enum_1.TransactionStatus.SUBMITTED ||
        transactionStatus === transaction_enum_1.TransactionStatus.SUBMITTING);
};
const confirmTransaction = (input) => __awaiter(void 0, void 0, void 0, function* () {
    common_util_1.trimStringValuesFromObject(input);
    let response = {
        transactionResponseID: input.transactionResponseID
    };
    logger_1.default.info(`confirmTransaction: Transaction Confirmation with
      external id: ${input.externalId},
      accountNo: ${input.accountNo},
      transactionDate: ${input.transactionDate},
      responseCode: ${input.responseCode.toString()},
      responseMessage: ${input.responseMessage},
      interchange: ${input.interchange},
      additionalInformation1: ${input.additionalInformation1},
      additionalInformation2: ${input.additionalInformation2},
      additionalInformation3: ${input.additionalInformation3},
      additionalInformation4: ${input.additionalInformation4}`);
    let transaction = yield queryTransaction(input);
    if (yield entitlementFlag_1.default.isEnabled()) {
        if (transaction && transaction.entitlementInfo) {
            yield contextHandler_1.setTransactionInfoTracker(transaction.entitlementInfo);
        }
    }
    yield transactionUsage_service_1.default.mapTransactionInfoToTransactionUsageInfoContext(transaction);
    if (!transaction) {
        throw transaction_util_1.GenerateAppError('Failed to confirm transaction with invalid external id!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_EXTERNALID, transaction_constant_1.FIELD_EXTERNALID);
    }
    logger_1.default.info(`confirmTransaction: Transaction found with 
      external id: ${input.externalId},
      accountNo: ${input.accountNo},
      beneficiaryBankCodeChannel : ${transaction.beneficiaryBankCodeChannel}, 
      beneficiaryBankCode: ${transaction.beneficiaryBankCode},
      beneficiaryAccountNo. ${transaction.beneficiaryAccountNo}
    `);
    validateConfirmTransactionRequest(input, transaction);
    response = populateConfirmTransactionResponse(response, transaction);
    if (input.responseCode === transaction_enum_1.ConfirmTransactionStatus.REQUEST_TIMEOUT) {
        logger_1.default.error(`Updating transaction for timeout with transaction Id : ${transaction.id}`);
        const updatedTransaction = yield transaction_repository_1.default.update(transaction.id, {
            confirmationCode: `${input.responseCode}`
        });
        if (updatedTransaction) {
            logger_1.default.info(`Send pending blocked amount notification for : ${updatedTransaction.id}`);
            yield transaction_producer_1.default.sendPendingBlockedAmountNotification(updatedTransaction, transaction_producer_enum_1.NotificationCode.NOTIFICATION_PENDING_TRANSFER);
        }
        else {
            logger_1.default.error(`confirmTransaction: Transaction not found for Transaction Id : ${transaction.id}`);
        }
        return response;
    }
    // sumbitted to failure
    if (input.responseCode !== transaction_enum_1.ConfirmTransactionStatus.SUCCESS) {
        logger_1.default.info(`Executing Reversal for transaction : ${transaction.id}`);
        const reverseResponse = yield reverseTransaction(input, transaction);
        if (transaction.paymentInstructionExecution) {
            exports.responseMessageToPaymentInstruction(input, transaction);
        }
        return Object.assign(Object.assign({}, response), reverseResponse);
    }
    if (!isValidTransactionStatusForConfirmation(transaction.status)) {
        logger_1.default.warn(`confirmTransaction: invalid transaction status: ${transaction.status},
        transaction ID: ${transaction.id}
        external id: ${input.externalId},
        accountNo: ${input.accountNo},
        beneficiaryBankCodeChannel : ${transaction.beneficiaryBankCodeChannel}, 
        beneficiaryBankCode: ${transaction.beneficiaryBankCode},
        beneficiaryAccountNo. ${transaction.beneficiaryAccountNo}.
        Skip processing confirmTransaction logic`);
        return { transactionResponseID: input.transactionResponseID };
    }
    if (transaction.status === transaction_enum_1.TransactionStatus.SUBMITTING) {
        // we are in a race condition, where we receive confirmation before payment response
        logger_1.default.warn(`confirmTransaction: Abnormal confirmation order received for trx ${transaction.id}, system handled it correctly.`);
        transaction = yield transaction_repository_1.default.update(transaction.id, {
            status: transaction_enum_1.TransactionStatus.SUBMITTED,
            journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.THIRD_PARTY_SUBMITTED, transaction)
        });
    }
    // sumbitted to success
    const updateInfo = exports.extractChangeFromConfirmTransactionRequest(input, transaction);
    const txnId = transaction.id;
    transaction = yield transaction_repository_1.default.update(txnId, Object.assign({}, updateInfo));
    if (!transaction) {
        logger_1.default.error(`confirmTransaction: submitTransaction: failed to update transaction info for txnId ${txnId}`);
        throw new AppError_1.TransferAppError(`Submit transaction failed to update transaction info for txnId ${txnId}`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION);
    }
    yield transactionExecution_service_1.default.settleTransaction(transaction, {
        submitTransaction: submitConfirmTransaction,
        isEligible: () => true,
        declineOnSubmissionFailure: false
    });
    if (transaction.paymentInstructionExecution) {
        exports.responseMessageToPaymentInstruction(input, transaction);
    }
    return response;
});
const switchingTransactionService = logger_1.wrapLogs({
    confirmTransaction,
    reverseTransaction,
    extractChangeFromConfirmTransactionRequest: exports.extractChangeFromConfirmTransactionRequest,
    submitConfirmTransaction
});
exports.default = switchingTransactionService;
//# sourceMappingURL=transactionConfirmation.service.js.map