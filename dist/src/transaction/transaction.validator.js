"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("@hapi/joi"));
const transaction_constant_1 = __importStar(require("./transaction.constant"));
const module_common_1 = require("@dk/module-common");
const transaction_constant_2 = __importDefault(require("./transaction.constant"));
const emptyString = '';
const BENEFICIARY_BANK_CODE_PROP_NAME = 'beneficiaryBankCode';
const matchEmptySchema = joi_1.default.any().valid('', null);
const transaction_enum_1 = require("./transaction.enum");
const constant_1 = require("../common/constant");
const fee_validator_1 = require("../fee/fee.validator");
const paymentServiceCode = module_common_1.JoiValidator.optionalString().description('The payment service code');
const externalIdValidator = module_common_1.JoiValidator.optionalString()
    .max(transaction_constant_2.default.MAX_LENGTH_EXTERNAL_ID)
    .description('External ID');
const commonTransactionRequestPayload = {
    externalId: externalIdValidator.description('Unique identity of transaction'),
    categoryCode: joi_1.default.string()
        .max(transaction_constant_1.default.MAX_LENGTH_CATEGORY_CODE)
        .optional()
        .allow(null)
        .description('Category Code'),
    note: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_NOTE)
        .empty(matchEmptySchema)
        .description('Note'),
    additionalInformation1: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
        .empty(matchEmptySchema)
        .description('additionalInformation1 used to save extra information'),
    additionalInformation2: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
        .empty(matchEmptySchema)
        .description('additionalInformation2 used to save extra information'),
    additionalInformation3: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
        .empty(matchEmptySchema)
        .description('additionalInformation3 used to save extra information'),
    additionalInformation4: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
        .empty(matchEmptySchema)
        .description('additionalInformation4 used to save extra information'),
    paymentRequestID: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_PAYMENT_REQUEST_ID)
        .description('An optional payment request or pay-me request identity'),
    generalRefund: joi_1.default.object({
        transactionId: module_common_1.JoiValidator.optionalString().description('Transaction Id of original transaction'),
        externalId: module_common_1.JoiValidator.optionalString().description('External Id of original transaction'),
        fee: joi_1.default.boolean().optional().description(`General refund with fee
      * TRUE: Refunds fees
      * False: Does not refund fees`),
        interchange: module_common_1.JoiValidator.optionalString()
            .valid(Object.values(module_common_1.BankNetworkEnum))
            .description('Interchange/Bank network for General refund')
    })
        .optional()
        .description(`Extra details required for general refund`)
};
exports.transactionWithRefundValidator = {
    id: joi_1.default.string()
        .required()
        .description('Refund Transaction Id'),
    status: joi_1.default.string()
        .required()
        .description('Refund Transaction Status'),
    amount: joi_1.default.number()
        .required()
        .description('Refund Transaction Amount'),
    createdAt: joi_1.default.date()
        .required()
        .description('Refund Transacation Created'),
    updatedAt: joi_1.default.date()
        .required()
        .description('Refund Transacation Last Updated')
};
exports.transactionInfoResponseValidator = joi_1.default.object(Object.assign(Object.assign({ paymentRequestID: module_common_1.JoiValidator.optionalString().description('An optional payment request or pay-me request identity'), sourceCIF: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Source Cif'), sourceAccountType: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Source Account Type'), sourceAccountNo: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Source Account Number'), sourceAccountName: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Source Account Name'), sourceBankCode: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Source Bank Code'), sourceTransactionCurrency: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Source Tranasaction Currency'), sourceBankCodeChannel: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Source Bank Code Channel'), transactionAmount: [module_common_1.JoiValidator.currency(), module_common_1.JoiValidator.requiredString()], beneficiaryCIF: module_common_1.JoiValidator.optionalString().description('Beneficiary Cif'), beneficiaryBankCode: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Beneficiary Bank Code'), beneficiaryAccountType: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Beneficiary Account Type'), beneficiaryAccountNo: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Beneficiary Account number'), beneficiaryAccountName: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Beneficiary Account Name'), beneficiaryBankName: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Beneficiary Bank Name'), beneficiaryBankCodeChannel: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Beneficiary Bank Code Channel'), debitPriority: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Debit Priority'), creditPriority: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Credit Priority'), institutionalType: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Institutional Type'), note: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Note'), paymentServiceType: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Payment Service Type for rule evaluation'), extendedMessage: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Extended Message'), status: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Status of transaction'), externalId: externalIdValidator, referenceId: module_common_1.JoiValidator.requiredString().description('Reference ID'), fees: joi_1.default.array()
        .items(joi_1.default.object({
        feeAmount: [
            module_common_1.JoiValidator.currency().allow([0, null]),
            module_common_1.JoiValidator.requiredString()
        ]
    })
        .label('FeeAmount')
        .description('fees applied on transaction'))
        .label('FeeAmounts') }, commonTransactionRequestPayload), { categoryCode: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Category Code'), promotionCode: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Promotion Code'), sourceCustomerId: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Source Customer ID'), refund: joi_1.default.object()
        .keys({
        remainingAmount: [
            module_common_1.JoiValidator.currency().allow([0, null]),
            module_common_1.JoiValidator.requiredString()
        ],
        originalTransactionId: module_common_1.JoiValidator.optionalString().description('TransactionId of Original transaction'),
        revertPaymentServiceCode: module_common_1.JoiValidator.optionalString()
            .empty(matchEmptySchema)
            .description('Revert Transaction Payment Service code'),
        transactions: joi_1.default.array()
            .items(joi_1.default.string().optional())
            .optional()
            .description('Transactions mapped to original transaction'),
        latestTransaction: joi_1.default.object()
            .keys(exports.transactionWithRefundValidator)
            .optional()
            .description('SUCCEED / Latest updated refund transaction')
    })
        .optional(), blockingId: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Blocking ID used in case of blocking transactions'), cardId: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Card ID used in case of blocking transactions'), transactionId: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Transaction ID of transaction'), confirmationCode: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Confirmation Code if we get while confirming transaction'), paymentServiceCode: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Payment Service Code used for evaluating rule'), createdAt: joi_1.default.date()
        .description('Transaction date')
        .description('Transaction Created Date'), reason: joi_1.default.array()
        .items(joi_1.default.object()
        .keys({
        responseCode: joi_1.default.number()
            .optional()
            .description('Declined Reason response code'),
        responseMessage: module_common_1.JoiValidator.optionalString().description('Declined response Message code'),
        transactionDate: module_common_1.JoiValidator.optionalString().description('Declined response Transaction Date')
    })
        .label('Declined-reason'))
        .optional(), journey: joi_1.default.array()
        .items(joi_1.default.object()
        .keys({
        status: joi_1.default.string()
            .valid(Object.values(transaction_enum_1.TransferJourneyStatusEnum))
            .required()
            .description('Journey Status'),
        updatedAt: joi_1.default.date().description('Journey created date')
    })
        .label('Journey-of-the-transaction')
        .optional())
        .optional()
        .label('Journey'), ownerCustomerId: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Owner Id of transaction') })).label('TransactionDetail');
const baseRecommendPaymentServiceRequestValidator = {
    sourceCIF: module_common_1.JoiValidator.optionalString()
        .empty('')
        .description('Source Cif'),
    sourceAccountNo: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_SOURCE_ACCOUNT_NO)
        .empty(matchEmptySchema)
        .description('source Account Number'),
    sourceBankCode: module_common_1.JoiValidator.optionalString()
        .empty('')
        .description('source Bank Code'),
    transactionAmount: module_common_1.JoiValidator.currency()
        .not(null, emptyString)
        .required()
        .description('Transaction Amount'),
    beneficiaryCIF: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_CIF)
        .description('Beneficiary Cif'),
    [BENEFICIARY_BANK_CODE_PROP_NAME]: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_BANK_CODE)
        .description('Beneficiary Bank Code'),
    beneficiaryAccountNo: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
        .empty(matchEmptySchema)
        .description('Beneficiary Account No'),
    beneficiaryAccountName: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_ACCOUNT_NAME)
        .empty(matchEmptySchema)
        .description('Beneficiary Account Name'),
    paymentServiceType: joi_1.default.string()
        .valid(Object.values(module_common_1.PaymentServiceTypeEnum))
        .required()
        .description('Payment Service Type for rule evaluation'),
    externalId: externalIdValidator,
    paymentServiceCode: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Payment Service Code for rule evaluation'),
    preferredBankChannel: module_common_1.JoiValidator.optionalString()
        .valid(Object.values(module_common_1.BankChannelEnum))
        .description('Preferred Bank Channel')
};
exports.recommendPaymentServiceRequestValidator = joi_1.default.object(Object.assign({}, baseRecommendPaymentServiceRequestValidator)).label('RecommendPaymentServicePayload');
exports.transactionRequestBaseModelValidator = Object.assign(Object.assign({}, baseRecommendPaymentServiceRequestValidator), commonTransactionRequestPayload);
exports.topupTransactionBaseRequestValidator = Object.assign({ [BENEFICIARY_BANK_CODE_PROP_NAME]: module_common_1.JoiValidator.requiredString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_BANK_CODE)
        .description('Beneficiary Bank Code'), transactionAmount: module_common_1.JoiValidator.currency()
        .not(null, emptyString)
        .required()
        .description('The topup amount'), paymentServiceCode }, commonTransactionRequestPayload);
exports.topupExternalTransactionRequestPayload = joi_1.default.object(Object.assign(Object.assign({}, exports.topupTransactionBaseRequestValidator), { beneficiaryBankCode: joi_1.default.forbidden().description('Beneficiary Bank Code'), paymentServiceCode: joi_1.default.forbidden().description('Payment Service Code for rule evaluation'), beneficiaryAccountNo: module_common_1.JoiValidator.requiredString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
        .description('The wallet identifier number including partner prefix') })).label('ExternalTopupTransactionPayload');
exports.topupInternalTransactionRequestPayload = joi_1.default.object(Object.assign(Object.assign({}, exports.topupTransactionBaseRequestValidator), { sourceCIF: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_SOURCE_CIF)
        .description('Source Cif'), sourceAccountNo: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_SOURCE_ACCOUNT_NO)
        .description('Source Account Number'), beneficiaryAccountNo: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
        .description('The wallet identifier number'), paymentInstructionId: module_common_1.JoiValidator.optionalString().description('Payment Instruction ID'), paymentServiceType: joi_1.default.string()
        .valid(Object.values(module_common_1.PaymentServiceTypeEnum))
        .required()
        .description('Payment Service Type for rule evaluation'), feeAmount: module_common_1.JoiValidator.currency()
        .optional()
        .description('fee amount applied in transaction') })).label('InternalTopupTransactionPayload');
const processingInfoValidator = joi_1.default.object({
    channel: module_common_1.JoiValidator.optionalString().description('Channel of caller'),
    method: module_common_1.JoiValidator.optionalString()
        .valid(Object.values(transaction_constant_1.ExecutionMethodEnum))
        .description('Execution method'),
    partner: module_common_1.JoiValidator.optionalString().description('Partners who initiate the transaction')
})
    .optional()
    .description('Transaction processing information to indicate source channel, execution method, and partner');
exports.createTransferTransactionRequestValidator = joi_1.default.object(Object.assign(Object.assign({}, exports.transactionRequestBaseModelValidator), { sourceCIF: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_SOURCE_CIF)
        .description('Source Cif'), sourceAccountNo: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_SOURCE_ACCOUNT_NO)
        .description('Source Account Number'), sourceBankCode: module_common_1.JoiValidator.requiredString().description('Source Bank Code'), beneficiaryCIF: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_CIF)
        .description('Beneficiary Cif'), [BENEFICIARY_BANK_CODE_PROP_NAME]: module_common_1.JoiValidator.requiredString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_BANK_CODE)
        .description('Beneficiary Bank Code'), beneficiaryAccountNo: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
        .description('Beneficiary Account Number'), paymentServiceCode, paymentServiceType: joi_1.default.string()
        .valid(Object.values(module_common_1.PaymentServiceTypeEnum))
        .required()
        .description('Payment Service Type for rule evaluation'), promotionCode: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_PROMOTION_CODE)
        .description('Promotion Code'), interchange: module_common_1.JoiValidator.optionalString()
        .valid(Object.values(module_common_1.BankNetworkEnum))
        .description('Interchange/Bank network'), paymentInstructionId: module_common_1.JoiValidator.optionalString().description('Payment Instruction ID'), feeAmount: module_common_1.JoiValidator.currency()
        .optional()
        .description('fee amount applied in transaction'), additionalPayload: joi_1.default.object({})
        .unknown()
        .optional(), processingInfo: processingInfoValidator })).label('TransferTransactionPayload');
exports.createDepositTransactionRequestValidator = joi_1.default.object(Object.assign(Object.assign({}, exports.transactionRequestBaseModelValidator), { paymentServiceType: joi_1.default.string()
        .valid([module_common_1.PaymentServiceTypeEnum.WALLET, module_common_1.PaymentServiceTypeEnum.CASHBACK])
        .required()
        .description('Payment Service Type for rule evaluation'), sourceCIF: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Source Cif'), sourceAccountNo: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_SOURCE_ACCOUNT_NO)
        .empty(matchEmptySchema)
        .description('Source Account Number'), sourceAccountName: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Source Account Name'), beneficiaryCIF: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_CIF)
        .description('Beneficiary Cif'), [BENEFICIARY_BANK_CODE_PROP_NAME]: module_common_1.JoiValidator.requiredString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_BANK_CODE)
        .description('Beneficiary Bank Code'), beneficiaryAccountNo: module_common_1.JoiValidator.requiredString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
        .description('Beneficiary Account Number'), referenceId: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_REFERENCE_ID)
        .description('An external referenceId of transaction. It could be used to link multiple transaction with the same reference id'), paymentServiceCode })).label('DepositTransactionPayload');
exports.createWithdrawTransactionRequestValidator = joi_1.default.object(Object.assign(Object.assign({}, exports.transactionRequestBaseModelValidator), { paymentServiceType: joi_1.default.string()
        .valid([module_common_1.PaymentServiceTypeEnum.MDR])
        .required(), sourceCIF: module_common_1.JoiValidator.requiredString().max(transaction_constant_1.default.MAX_LENGTH_SOURCE_CIF), sourceAccountNo: module_common_1.JoiValidator.requiredString().max(transaction_constant_1.default.MAX_LENGTH_SOURCE_ACCOUNT_NO), sourceBankCode: module_common_1.JoiValidator.requiredString(), beneficiaryCIF: module_common_1.JoiValidator.optionalString().max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_CIF), [BENEFICIARY_BANK_CODE_PROP_NAME]: module_common_1.JoiValidator.optionalString().max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_BANK_CODE), beneficiaryAccountNo: module_common_1.JoiValidator.optionalString().max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO), referenceId: module_common_1.JoiValidator.optionalString().max(transaction_constant_1.default.MAX_LENGTH_REFERENCE_ID), paymentServiceCode })).label('WithdrawTransactionPayload');
const baseTransactionResponseValidator = {
    id: joi_1.default.string()
        .required()
        .description('Transaction id'),
    externalId: externalIdValidator
};
exports.transactionResponseValidator = joi_1.default.object(Object.assign(Object.assign({}, baseTransactionResponseValidator), { referenceId: module_common_1.JoiValidator.optionalString().description('Transaction reference ID. Used by consequence transaction to link with this transaction'), paymentServiceType: joi_1.default.string(), transactionIds: joi_1.default.array()
        .items(joi_1.default.string())
        .required()
        .label('CoreBankingTransactionIdentities')
        .description('It is used for debugging purpose'), transactionDate: joi_1.default.date().description('Transaction execution date'), transactionStatus: joi_1.default.string()
        .optional()
        .description('Status of the transaction') })).label('TransactionResponse');
const coreBankingTransactionsValidator = joi_1.default.array()
    .items(joi_1.default.object()
    .keys({
    id: joi_1.default.string()
        .required()
        .description('Core banking transaction Id'),
    type: joi_1.default.string()
        .valid(Object.values(transaction_enum_1.TransferJourneyStatusEnum))
        .required()
        .description('Transfer journey status')
})
    .optional())
    .optional()
    .label('CoreBankingTransactionInfo')
    .description('Core banking transaction IDs mapped to original transaction');
exports.externalTransactionResponseValidator = joi_1.default.object(Object.assign(Object.assign({}, baseTransactionResponseValidator), { availableBalance: joi_1.default.number()
        .optional()
        .description('Customer Available Balance'), coreBankingTransactions: coreBankingTransactionsValidator })).label('ExternalTransactionResponse');
exports.blockingTransactionResponseValidator = joi_1.default.object({
    id: joi_1.default.string()
        .required()
        .description('Blocking Transaction ID')
}).label('BlockingTransferTransaction');
exports.recommendPaymentServiceResponseValidator = joi_1.default.array()
    .items(joi_1.default.object({
    paymentServiceCode: joi_1.default.string()
        .required()
        .description('Payment Service Code for rule evaluation'),
    debitTransactionCode: joi_1.default.string()
        .allow(null)
        .description('Debit Transaction Code'),
    creditTransactionCode: joi_1.default.string()
        .allow(null)
        .description('Credit Transaction Code'),
    realBeneficiaryBankCode: joi_1.default.string()
        .allow(null)
        .description('Beneficiary Bank Code'),
    feeAmount: joi_1.default.number()
        .allow(null)
        .description('Fee Amount applied for transaction'),
    customerTc: joi_1.default.string()
        .allow(null)
        .description('Customer Tc'),
    authenticationRequired: joi_1.default.string()
        .valid(Object.values(module_common_1.AuthenticationType))
        .description('Whether the customer need to authenticate for step-up transaction amount')
}).label('RecommendPaymentService'))
    .label('RecommendPaymentServices');
exports.reverseRequestValidator = joi_1.default.object({
    notes: module_common_1.JoiValidator.optionalString().description('Notes')
})
    .label('RevertTransactionPayload')
    .allow(null);
exports.transactionIdValidator = joi_1.default.object({
    transactionId: joi_1.default.string()
        .required()
        .description('Transaction ID of transaction')
}).label('TransactionIdentities');
exports.blockingTransactionAdjustmentRequestValidator = joi_1.default.object({
    amount: module_common_1.JoiValidator.currency()
        .required()
        .description('Amount in blocking Transaction')
}).label('AdjustBlockingPayload');
exports.cifValidator = joi_1.default.object({
    cif: joi_1.default.string()
        .required()
        .description('Cif')
}).label('CIF');
exports.customerIdValidator = joi_1.default.object({
    customerId: module_common_1.JoiValidator.optionalString().description('Customer ID')
}).label('CustomerId');
const confirmTransactionBaseModelValidator = {
    transactionResponseID: module_common_1.JoiValidator.optionalString()
        .regex(/^\d{6}/)
        .max(transaction_constant_1.default.MAX_LENGTH_TRANSACTION_RESPONSE_ID)
        .description('Accepted format: MMDDYYXXXXXX')
};
const confirmTransactionAdditionalModelValidator = {
    id: module_common_1.JoiValidator.optionalString().description('Unique identity of transaction for internal'),
    externalId: externalIdValidator.description('Unique identity of transaction for external'),
    coreBankingTransactions: coreBankingTransactionsValidator
};
exports.incomingTransactionDate = module_common_1.JoiValidator.requiredString()
    .length(transaction_constant_1.default.LENGTH_TRANSACTION_DATE_CONFIRMATION)
    .regex(/\d{12}/)
    .description('Date time with format: YYMMDDhhmmss');
const transactionCreatedDate = module_common_1.JoiValidator.optionalString()
    .regex(/^\d{4}[\-](0?[1-9]|1[012])[\-](0?[1-9]|[12][0-9]|3[01])$/)
    .description('Date time with format: YYYY-MM-DD');
exports.confirmTransactionRequestValidator = joi_1.default.object(Object.assign({ externalId: joi_1.default.string()
        .required()
        .max(transaction_constant_1.default.MAX_LENGTH_EXTERNAL_ID)
        .description('Unique identity of transaction'), accountNo: module_common_1.JoiValidator.requiredString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
        .description('Account Number with max length 40'), transactionDate: exports.incomingTransactionDate, responseCode: joi_1.default.number()
        .required()
        .valid(Object.values(transaction_enum_1.ConfirmTransactionStatus))
        .description('Response code for confirming transaction'), responseMessage: joi_1.default.string()
        .max(transaction_constant_1.default.MAX_LENGTH_TRANSACTION_RESPONSE_MESSAGE)
        .description('Response Message for confirming transaction'), interchange: module_common_1.JoiValidator.requiredString()
        .valid(Object.values(module_common_1.BankNetworkEnum))
        .description('Interchange while confirming transaction'), additionalInformation1: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
        .empty(matchEmptySchema)
        .description('additionalInformation1 used to save extra information'), additionalInformation2: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
        .empty(matchEmptySchema)
        .description('additionalInformation2 used to save extra information'), additionalInformation3: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
        .empty(matchEmptySchema)
        .description('additionalInformation3 used to save extra information'), additionalInformation4: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
        .empty(matchEmptySchema)
        .description('additionalInformation4 used to save extra information') }, confirmTransactionBaseModelValidator))
    .options({ stripUnknown: true })
    .label('ConfirmTransactionRequest');
exports.confirmTransactionQueryValidator = joi_1.default.object({
    includeTransactionId: joi_1.default.boolean().optional()
        .description(`fetch transactionId for transaction
      * true: include transactionId in response payload
      * false: do not include transactionId in response payload`),
    includeExternalId: joi_1.default.boolean().optional()
        .description(`fetch externalId for transaction
      * true: include in response payload
      * false: do not include in response payload`),
    includeCoreBankingTransactions: joi_1.default.boolean().optional()
        .description(`fetch core banking transaction IDs for transaction 
      * true: include in response payload
      * false: do not include in response payload`)
});
exports.confirmTransactionResponseValidator = joi_1.default.object(Object.assign(Object.assign({}, confirmTransactionBaseModelValidator), confirmTransactionAdditionalModelValidator)).label('ConfirmTransactionResponse');
const baseExternalTransactionPayloadValidator = {
    externalId: externalIdValidator,
    transactionDate: exports.incomingTransactionDate,
    paymentServiceType: module_common_1.JoiValidator.requiredString()
        .valid(Object.values(module_common_1.PaymentServiceTypeEnum))
        .description('Payment Service Type for rule Evaluation'),
    // RTOL, remittance
    sourceBankCode: module_common_1.JoiValidator.requiredString()
        .max(transaction_constant_1.default.MAX_LENGTH_SOURCE_BANK_CODE)
        .description('Source Bank Code with max length 10'),
    sourceAccountNo: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_SOURCE_ACCOUNT_NO)
        .regex(/[^0-9]/, { invert: true })
        .description('Source Account Number with max length 40'),
    sourceAccountName: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_ACCOUNT_NAME)
        .description('Source Account Name with max length 100'),
    // currency iso code
    currency: module_common_1.JoiValidator.requiredString()
        .max(transaction_constant_1.default.MAX_LENGTH_CURRENCY_ISO_NO)
        .description('Currency with max length 5'),
    transactionAmount: module_common_1.JoiValidator.currency()
        .required()
        .description('Transaction Amount'),
    // RTOL, remittance
    beneficiaryBankCode: module_common_1.JoiValidator.requiredString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_BANK_CODE)
        .description('Beneficiary Bank Code with max length 10'),
    beneficiaryAccountNo: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
        .description('Beneficiary Account Number with max length 40'),
    beneficiaryAccountName: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_ACCOUNT_NAME)
        .description('Beneficiary Account Name with max length 100'),
    interchange: module_common_1.JoiValidator.optionalString()
        .valid(Object.values(module_common_1.BankNetworkEnum))
        .description('Interchange/ Bank Network'),
    notes: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_NOTE)
        .description('Notes for transaction'),
    additionalInformation1: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
        .empty(matchEmptySchema)
        .description('additionalInformation1 used to save extra information'),
    additionalInformation2: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
        .empty(matchEmptySchema)
        .description('additionalInformation2 used to save extra information'),
    additionalInformation3: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
        .empty(matchEmptySchema)
        .description('additionalInformation3 used to save extra information'),
    additionalInformation4: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
        .empty(matchEmptySchema)
        .description('additionalInformation4 used to save extra information'),
    processingInfo: processingInfoValidator,
    workflowId: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_WORKFLOW_ID)
        .empty(matchEmptySchema)
        .description('Deprecated, workflowId for transactions. In old Atome card transaction processing, one Atome operation creates 2 transactions. These 2 transactions must have the same workflowId'),
    correlationId: module_common_1.JoiValidator.optionalString()
        .max(transaction_constant_1.default.MAX_LENGTH_WORKFLOW_ID)
        .empty(matchEmptySchema)
        .description('correlationId for transactions. In some transaction processing (for example, Atome card transaction), there may be more than one transfer. We use this correlation id to link them together.'),
    tranType: module_common_1.JoiValidator.optionalString().description('Euronet should send this as OC when this is an Original Credit Transaction from VISA')
};
exports.externalTransactionRequestValidator = joi_1.default.object(Object.assign(Object.assign({}, baseExternalTransactionPayloadValidator), { sourceAccountNo: joi_1.default.alternatives().when('paymentServiceType', {
        is: joi_1.default.any().valid([
            module_common_1.PaymentServiceTypeEnum.INCOMING_SKN,
            module_common_1.PaymentServiceTypeEnum.INCOMING_RTGS,
            module_common_1.PaymentServiceTypeEnum.INCOMING_BIFAST
        ]),
        then: module_common_1.JoiValidator.optionalString()
            .max(transaction_constant_1.default.MAX_LENGTH_SOURCE_ACCOUNT_NO)
            .description('Source Account Number with max length 40'),
        otherwise: module_common_1.JoiValidator.optionalString()
            .max(transaction_constant_1.default.MAX_LENGTH_SOURCE_ACCOUNT_NO)
            .regex(/[^0-9]/, { invert: true })
            .description('Source Account Number with max length 40')
    }), externalId: joi_1.default.string()
        .required()
        .max(transaction_constant_1.default.MAX_LENGTH_EXTERNAL_ID)
        .regex(/[^A-Za-z0-9\-_]/, { invert: true })
        .description('Unique identity of transaction') }))
    .options({ stripUnknown: true })
    .label('ExternalTransactionRequestValidator');
exports.externalTransactionQueryValidator = joi_1.default.object({
    includeCoreBankingTransactions: joi_1.default.string()
        .optional()
        .description('fetch core banking transaction IDs for transaction')
});
exports.externalTransactionHeaderValidator = joi_1.default.object({
    [constant_1.HttpHeaders.X_IDEMPOTENCY_KEY]: joi_1.default.string()
        .optional()
        .description('Key to differentiate requests, will return same response for identical keys')
}).unknown();
exports.externalTransactionFeeRequestValidator = joi_1.default.object(Object.assign({}, baseExternalTransactionPayloadValidator)).label('Request-external-transaction-fee');
exports.incomingFeeCheckResponseValidator = joi_1.default.object({
    externalId: externalIdValidator,
    feeAmount: joi_1.default.number()
        .required()
        .description('fee amount for incoming fee check')
}).label('Response-external-transaction-fee');
exports.refundTransactionRequestValidator = joi_1.default.object({
    transactionId: module_common_1.JoiValidator.requiredString().description('transaction ID for refund transaction')
});
exports.refundTransactionQueryValidator = joi_1.default.object({
    refundFee: joi_1.default.boolean()
        .optional()
        .description('refund fee for refunded transaction'),
    interchange: module_common_1.JoiValidator.optionalString()
        .valid(Object.values(module_common_1.BankNetworkEnum))
        .description('Interchange/ Bank Network')
});
exports.refundTransactionResponseValidator = joi_1.default.object({
    id: module_common_1.JoiValidator.requiredString().description('ID for refund transaction response')
});
exports.transactionListQueryValidator = joi_1.default.object({
    fetchRefund: joi_1.default.string()
});
const pagingRequest = {
    page: joi_1.default.number()
        .integer()
        .positive() // first page is 1
        .empty(1),
    limit: joi_1.default.number()
        .positive()
        .integer()
        .max(transaction_constant_1.default.MAX_RECORD_PER_PAGE)
        .empty(transaction_constant_1.default.DEFAULT_RECORD_PER_PAGE)
};
exports.transactionListRequestValidator = joi_1.default.object(Object.assign({ transactionId: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Transaction ID for the transaction'), customerId: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Transaction customer id'), startDate: transactionCreatedDate, endDate: transactionCreatedDate, transactionStatus: joi_1.default.array()
        .items(joi_1.default.string()
        .optional()
        .valid(Object.values(transaction_enum_1.TransactionStatus))
        .description('Transaction Status'))
        .optional(), beneficiaryAccountNo: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Beneficiary Account number'), sourceAccountNo: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Source Account number'), sourceBankCode: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Source Bank code'), beneficiaryBankCode: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Beneficiary Bank code'), transactionAmount: module_common_1.JoiValidator.currency().description('Transaction Amount'), accountNo: module_common_1.JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Account number') }, pagingRequest)).label('TransactionListRequest');
const pagingResultResponse = {
    limit: joi_1.default.number().required(),
    page: joi_1.default.number().required(),
    totalPage: joi_1.default.number().required()
};
exports.transactionListResponseVaidator = joi_1.default.object(Object.assign(Object.assign({}, pagingResultResponse), { list: joi_1.default.array()
        .items(exports.transactionInfoResponseValidator)
        .label('TransactionList') }));
exports.extTxnManualAdjstmentRequestValidator = joi_1.default.object({
    transactionId: joi_1.default.string()
        .required()
        .description('Transaction ID for the transaction'),
    status: joi_1.default.string()
        .required()
        .valid(transaction_enum_1.TransactionStatus.SUCCEED, transaction_enum_1.TransactionStatus.DECLINED)
        .description('Status of the transaction'),
    interchange: joi_1.default.string()
        .optional()
        .valid(Object.values(module_common_1.BankNetworkEnum))
        .description('Interchange / Bank Network')
}).label('ConfirmTransactionRequest');
exports.transactionStatusRequestValidator = joi_1.default.object({
    customerId: joi_1.default.string()
        .required()
        .description('Owner Customer ID of transaction'),
    paymentInstructionId: joi_1.default.string()
        .required()
        .description('Payment Instruction ID of transaction'),
    transactionDate: joi_1.default.date()
        .optional()
        .description('Date of transaction')
}).label('TransactionIdentities');
exports.transactionStatusResponseValidator = {
    status: joi_1.default.string()
        .required()
        .description('Status of the transaction')
};
exports.bulkTransactionFeesRequestValidator = joi_1.default.object({
    transactions: joi_1.default.array()
        .items(Object.assign(Object.assign({}, baseRecommendPaymentServiceRequestValidator), { externalId: module_common_1.JoiValidator.requiredString()
            .max(transaction_constant_2.default.MAX_LENGTH_EXTERNAL_ID)
            .description('External ID') }))
        .required()
        .max(transaction_constant_2.default.MAX_RECORD_PER_PAGE)
        .unique('externalId')
}).label(`bulkTransactionFeesRequest`);
exports.bulkTransactionFeesResponseValidator = joi_1.default.array()
    .items(fee_validator_1.feeAmountResponseValidator.keys({
    externalId: module_common_1.JoiValidator.requiredString()
        .max(transaction_constant_2.default.MAX_LENGTH_EXTERNAL_ID)
        .description('External ID')
}))
    .label('bulkTransactionFeesResponse');
//# sourceMappingURL=transaction.validator.js.map