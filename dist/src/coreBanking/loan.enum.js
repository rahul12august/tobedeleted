"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AmountTypeEnum;
(function (AmountTypeEnum) {
    AmountTypeEnum["PRINCIPAL"] = "PRINCIPAL";
    AmountTypeEnum["INTEREST"] = "INTEREST";
    AmountTypeEnum["PENALTY"] = "PENALTY";
    AmountTypeEnum["FEE"] = "FEE";
})(AmountTypeEnum = exports.AmountTypeEnum || (exports.AmountTypeEnum = {}));
var AdjustLoanTransactionTypeEnum;
(function (AdjustLoanTransactionTypeEnum) {
    AdjustLoanTransactionTypeEnum["UNDO_REPAYMENT"] = "REPAYMENT_ADJUSTMENT";
    AdjustLoanTransactionTypeEnum["UNDO_DISBURSEMENT"] = "DISBURSMENT_ADJUSTMENT"; //the parameter for the disbursement adjustment call is currently misspelt (i.e. DISBURSMENT_ADJUSTMENT instead of DISBURSEMENT_ADJUSTMENT), need to check the documentation when there is new mambu update version to fix the misspelt.
})(AdjustLoanTransactionTypeEnum = exports.AdjustLoanTransactionTypeEnum || (exports.AdjustLoanTransactionTypeEnum = {}));
//# sourceMappingURL=loan.enum.js.map