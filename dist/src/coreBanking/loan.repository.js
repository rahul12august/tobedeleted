"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const logger_1 = __importStar(require("../logger"));
const httpClient_1 = __importDefault(require("./httpClient"));
const coreBanking_constant_1 = __importDefault(require("./coreBanking.constant"));
const coreBanking_util_1 = require("./coreBanking.util");
const circuitBreaker_1 = __importDefault(require("./circuitBreaker"));
const doMakeDisbursement = (loanAccountId, input, idempotencyKey = module_common_1.Http.generateIdempotencyKey()) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.info(`Submitting Mambu Disbursement transaction accountId: ${loanAccountId}`);
        const result = yield httpClient_1.default.post(`/loans/${loanAccountId}/disbursement-transactions`, input, { headers: { [coreBanking_constant_1.default.IDEMPOTENCY_KEY_HEADER]: idempotencyKey } });
        logger_1.default.debug(`doMakeDisbursement with accountId: ${loanAccountId} and response: ${JSON.stringify(result.data)}`);
        logger_1.default.info(`doMakeDisbursement with accountId: ${loanAccountId} and response with mambu id: ${result.data.id}`);
        return result.data;
    }
    catch (error) {
        logger_1.default.error(`Failed to makeDisbursement with error : ${coreBanking_util_1.stringifiedMambuError(error)}`);
        throw error;
    }
});
const doMakeRepayment = (loanAccountId, input, idempotencyKey = module_common_1.Http.generateIdempotencyKey()) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.info(`Submitting Mambu Repayment transaction accountId: ${loanAccountId}`);
        const result = yield httpClient_1.default.post(`/loans/${loanAccountId}/repayment-transactions`, input, { headers: { [coreBanking_constant_1.default.IDEMPOTENCY_KEY_HEADER]: idempotencyKey } });
        logger_1.default.debug(`doMakeRepayment with accountId: ${loanAccountId} and response ${JSON.stringify(result.data)}`);
        logger_1.default.info(`doMakeRepayment with accountId: ${loanAccountId} and response with mambu id: ${result.data.id}`);
        return result.data;
    }
    catch (error) {
        logger_1.default.error(`Failed to makeRepayment with error : ${coreBanking_util_1.stringifiedMambuError(error)}`);
        throw error;
    }
});
const doMakePayOff = (loanAccountId, input, idempotencyKey = module_common_1.Http.generateIdempotencyKey()) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.info(`Submitting Mambu pay off transaction accountId: ${loanAccountId}`);
        yield httpClient_1.default.post(`/loans/${loanAccountId}:payOff`, input, {
            headers: { [coreBanking_constant_1.default.IDEMPOTENCY_KEY_HEADER]: idempotencyKey }
        });
    }
    catch (error) {
        logger_1.default.error(`Failed to makePayOff with error : ${coreBanking_util_1.stringifiedMambuError(error)} , payload: ${JSON.stringify(input)}`);
        throw error;
    }
});
const doAdjustLoanTransaction = (loanAccountId, input, idempotencyKey = module_common_1.Http.generateIdempotencyKey()) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.debug(`Adjust loan transaction with payload: ${JSON.stringify(input)}`);
        logger_1.default.info(`Adjust loan transaction with account id : ${loanAccountId}`);
        const result = yield httpClient_1.default.post(`/loans/${loanAccountId}/transactions`, input, {
            headers: {
                [coreBanking_constant_1.default.IDEMPOTENCY_KEY_HEADER]: idempotencyKey,
                Accept: coreBanking_constant_1.default.ApiVersion.V1
            }
        });
        logger_1.default.debug(`doAdjustLoanTransaction with accountId: ${loanAccountId} and response ${JSON.stringify(result.data)}`);
        logger_1.default.info(`doAdjustLoanTransaction with accountId: ${loanAccountId} and response with transaction id: ${result.data.transactionId}`);
    }
    catch (error) {
        logger_1.default.error(`Failed to adjustLoanTransaction with error : ${coreBanking_util_1.stringifiedMambuError(error)}`);
        throw error;
    }
});
const makeDisbursement = circuitBreaker_1.default.create(doMakeDisbursement);
const makeRepayment = circuitBreaker_1.default.create(doMakeRepayment);
const makePayOff = circuitBreaker_1.default.create(doMakePayOff);
const adjustLoanTransaction = circuitBreaker_1.default.create(doAdjustLoanTransaction);
const coreBankingLoanRepository = {
    makeDisbursement,
    makeRepayment,
    makePayOff,
    adjustLoanTransaction
};
exports.default = logger_1.wrapLogs(coreBankingLoanRepository);
//# sourceMappingURL=loan.repository.js.map