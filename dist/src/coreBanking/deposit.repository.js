"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const logger_1 = __importStar(require("../logger"));
const httpClient_1 = __importDefault(require("./httpClient"));
const coreBanking_constant_1 = __importDefault(require("./coreBanking.constant"));
const circuitBreaker_1 = __importDefault(require("./circuitBreaker"));
const mambu_error_util_1 = __importDefault(require("../blockingAmount/mambu.error.util"));
const coreBanking_util_1 = require("./coreBanking.util");
const dateUtils_1 = require("../common/dateUtils");
const transaction_constant_1 = require("../transaction/transaction.constant");
const doGetAvailableBalance = (accountNumber) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.info(`doGetAvailableBalance accountId : ${accountNumber}`);
        const result = yield httpClient_1.default.get(`/deposits/${accountNumber}`);
        logger_1.default.info(`doGetAvailableBalance completed and available balance ${result.data.balances.availableBalance}.`);
        return result.data.balances.availableBalance;
    }
    catch (error) {
        logger_1.default.error(`Failed to getAvailableBalance of account ${accountNumber}`, {
            error
        });
        throw error;
    }
});
const doMakeWithdrawal = (depositAccountId, input, idempotencyKey = module_common_1.Http.generateIdempotencyKey()) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.info(`Submitting Mambu Withdrwal transaction accountId : ${depositAccountId} and idempotencyKey: ${idempotencyKey}`);
        const result = yield httpClient_1.default.post(`/deposits/${depositAccountId}/withdrawal-transactions`, input, {
            headers: {
                [coreBanking_constant_1.default.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
            }
        });
        logger_1.default.info(`doMakeWithdrawal with depositAccountId : ${depositAccountId} and mambuId : ${result.data.id}`);
        logger_1.default.debug(`doMakeWithdrawal with depositAccountId : ${depositAccountId} and response : ${JSON.stringify(result.data)}`);
        return result.data;
    }
    catch (error) {
        logger_1.default.error(`Failed to call to withdrawal-transactions error :${coreBanking_util_1.stringifiedMambuError(error)}`);
        mambu_error_util_1.default.tryThrowingProperTransferAppError(error, input);
        throw error;
    }
});
const doMakeDeposit = (depositAccountId, input, idempotencyKey = module_common_1.Http.generateIdempotencyKey()) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.info(`Submitting Mambu Deposit transaction accountId : ${depositAccountId} and idempotencyKey: ${idempotencyKey}`);
        const result = yield httpClient_1.default.post(`/deposits/${depositAccountId}/deposit-transactions`, input, {
            headers: {
                [coreBanking_constant_1.default.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
            }
        });
        logger_1.default.debug(`doMakeDeposit with depositAccountId : ${depositAccountId} and response : ${JSON.stringify(result.data)}`);
        logger_1.default.info(`doMakeDeposit with depositAccountId : ${depositAccountId} and response with mambu id : ${result.data.id}`);
        return result.data;
    }
    catch (error) {
        logger_1.default.error(`Failed to makeDeposit with error : ${coreBanking_util_1.stringifiedMambuError(error)}`);
        throw error;
    }
});
const doReverseTransaction = (transactionId, reverseInput, idempotencyKey = module_common_1.Http.generateIdempotencyKey()) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.debug(`Submitting Reversal transaction transactionId : ${transactionId} and payload : ${JSON.stringify(reverseInput)}`);
        logger_1.default.info(`Submitting Reversal transaction transactionId : ${transactionId}`);
        const result = yield httpClient_1.default.post(`/deposits/transactions/${transactionId}:adjust`, {
            notes: (reverseInput && reverseInput.notes) ||
                `Adjust transaction ${transactionId}`
        }, {
            headers: {
                [coreBanking_constant_1.default.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
            }
        });
        logger_1.default.info(`doReverseTransaction completed with transactionId: ${transactionId}, corebankingId: ${result.data.id}`);
    }
    catch (error) {
        logger_1.default.error(`Failed to reverseTransaction error : 
      ${coreBanking_util_1.stringifiedMambuError(error)}`);
        throw error;
    }
});
const doReverseCardTransaction = (cardId, transactionId, reverseInput, idempotencyKey = module_common_1.Http.generateIdempotencyKey()) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.debug(`Submitting Reversal card transaction with cardId : ${cardId} ,transactionId : ${transactionId} and payload : ${JSON.stringify(reverseInput)}`);
        logger_1.default.info(`Submitting Reversal card transaction with cardId : ${cardId} ,transactionId : ${transactionId}`);
        yield httpClient_1.default.post(`/cards/${cardId}/financialtransactions/${transactionId}:decrease`, reverseInput, {
            headers: {
                [coreBanking_constant_1.default.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
            }
        });
        logger_1.default.info(`doReverseCardTransaction completed for cardId: ${cardId},transactionId : ${transactionId}`);
    }
    catch (error) {
        logger_1.default.error(`Failed to reverseCardTransaction error : 
      ${coreBanking_util_1.stringifiedMambuError(error)}`);
        throw error;
    }
});
const doPatchTransactionDetails = (transactionId, payload) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const editTransactionDetailsUrl = `/deposits/transactions/${transactionId}`;
        logger_1.default.info(`calling patch transaction details on ${editTransactionDetailsUrl}`);
        const result = yield httpClient_1.default.patch(editTransactionDetailsUrl, payload);
        logger_1.default.info(`edit transaction response status: ${result.status}`);
    }
    catch (error) {
        logger_1.default.error(`Failed to patch transaction details for ${transactionId}`, {
            error
        });
        throw error;
    }
});
const searchDepositTransactionsUrl = '/deposits/transactions:search';
const transactionsSortingCriteria = {
    field: 'creationDate',
    order: 'DESC' // order latest to revert latest first
};
const transactionIdSortingCriteria = {
    field: 'id',
    order: 'DESC' // order latest to revert latest first
};
const doGetTransactionsByInstructionId = (id, creationDate) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.info(`get transactions by instructionId : ${id}, date: ${creationDate}`);
        const input = {
            sortingCriteria: transactionIdSortingCriteria,
            filterCriteria: [
                {
                    field: '_Custom_Transaction_Details.Payment_Instruction_ID',
                    operator: 'EQUALS_CASE_SENSITIVE',
                    value: id
                },
                {
                    field: 'creationDate',
                    operator: 'ON',
                    value: dateUtils_1.toJKTDate(creationDate, transaction_constant_1.FORMAT_YYYY_MM_DD)
                }
            ]
        };
        const result = yield httpClient_1.default.post(searchDepositTransactionsUrl, input);
        logger_1.default.info(`doGetTransactionsByInstructionId with instructionId : ${id}`);
        logger_1.default.debug(`doGetTransactionsByInstructionId with instructionId : ${id} and response length : ${result.data.length}`);
        return result.data;
    }
    catch (error) {
        logger_1.default.error(`Failed to get core banking ids for transaction ${id}`, {
            error
        });
        throw error;
    }
});
const doGetTransactionsByReferenceId = (referenceId) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`get transactions by referenceId : ${referenceId}`);
    const input = {
        sortingCriteria: transactionsSortingCriteria,
        filterCriteria: [
            {
                field: '_Custom_Transaction_Details.Reference_ID',
                operator: 'EQUALS_CASE_SENSITIVE',
                value: referenceId
            }
        ]
    };
    const result = yield httpClient_1.default.post(`${searchDepositTransactionsUrl}?detailsLevel=FULL`, input);
    logger_1.default.debug(`doGetTransactionsByReferenceId with referenceId : ${referenceId} and response : ${JSON.stringify(result.data)}`);
    logger_1.default.info(`doGetTransactionsByReferenceId with referenceId : ${referenceId}`);
    return result.data;
});
const doMakeCardWithdrawal = (cardId, input, idempotencyKey = module_common_1.Http.generateIdempotencyKey()) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.info(`Submitting Card Withdrawal transaction with cardId : ${cardId} and payload : ${input.externalReferenceId}`);
        const result = yield httpClient_1.default.post(`/cards/${cardId}/financialtransactions`, Object.assign(Object.assign({}, input), { advice: false }), {
            headers: {
                [coreBanking_constant_1.default.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
            }
        });
        logger_1.default.debug(`doMakeCardWithdrawal transaction with cardId : ${cardId} and response : ${JSON.stringify(result.data)}`);
        logger_1.default.info(`doMakeCardWithdrawal transaction with cardId : ${cardId}`);
        return result.data;
    }
    catch (error) {
        logger_1.default.error(`Failed to makeCardWithdrawal error : 
      ${coreBanking_util_1.stringifiedMambuError(error)}`);
        return mambu_error_util_1.default.checkErrorReverseAuthorizationHold(error);
    }
});
const doReverseAuthorizationHold = (cardReferenceToken, authHoldExtReferenceId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.info(`Deleting reverse authorization hold with cardReferenceToken : ${cardReferenceToken} and authHoldExtReferenceId : ${authHoldExtReferenceId}`);
        yield httpClient_1.default.delete(`/cards/${cardReferenceToken}/authorizationholds/${authHoldExtReferenceId}`);
        logger_1.default.info(`Deleted reverse authorization hold with cardReferenceToken : ${cardReferenceToken} and authHoldExtReferenceId : ${authHoldExtReferenceId}`);
    }
    catch (error) {
        logger_1.default.error(`Failed to reverseAuthorizationHold error : 
      ${coreBanking_util_1.stringifiedMambuError(error)}`);
        mambu_error_util_1.default.checkErrorReverseAuthorizationHold(error);
    }
});
const doExecuteJournalEntriesGL = (input, idempotencyKey = module_common_1.Http.generateIdempotencyKey()) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.debug(`Submitting journal entries GL with payload : ${JSON.stringify(input)}`);
    logger_1.default.info(`Submitting journal entries GL with transaction id : ${input.transactionID}`);
    yield httpClient_1.default.post(`/gljournalentries`, input, {
        headers: {
            Accept: coreBanking_constant_1.default.ApiVersion.V1,
            [coreBanking_constant_1.default.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
        }
    });
    logger_1.default.info(`doExecuteJournalEntriesGL: completed`);
});
const doCreateGlJournalEntries = (input, idempotencyKey = module_common_1.Http.generateIdempotencyKey()) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.debug(`Creating journal entries GL with payload : ${JSON.stringify(input)}`);
    logger_1.default.info(`Creating journal entries GL with transaction id : ${input.transactionId}`);
    const result = yield httpClient_1.default.post(`/gljournalentries`, input, {
        headers: { [coreBanking_constant_1.default.IDEMPOTENCY_KEY_HEADER]: idempotencyKey }
    });
    logger_1.default.info(`doCreateGlJournalEntries: completed`);
    return { id: result.data[0].transactionId };
});
const getAvailableBalance = circuitBreaker_1.default.create(doGetAvailableBalance);
const makeCardWithdrawal = circuitBreaker_1.default.create(doMakeCardWithdrawal);
const makeWithdrawal = circuitBreaker_1.default.create(doMakeWithdrawal);
const makeDeposit = circuitBreaker_1.default.create(doMakeDeposit);
const reverseTransaction = circuitBreaker_1.default.create(doReverseTransaction);
const reverseCardTransaction = circuitBreaker_1.default.create(doReverseCardTransaction);
const executeJournalEntriesGL = circuitBreaker_1.default.create(doExecuteJournalEntriesGL);
const createGlJournalEntries = circuitBreaker_1.default.create(doCreateGlJournalEntries);
const reverseAuthorizationHold = circuitBreaker_1.default.create(doReverseAuthorizationHold);
const getTransactionsByInstructionId = circuitBreaker_1.default.create(doGetTransactionsByInstructionId);
const getTransactionsByReferenceId = circuitBreaker_1.default.create(doGetTransactionsByReferenceId);
const patchTransactionDetails = circuitBreaker_1.default.create(doPatchTransactionDetails);
const coreBankingDepositRepository = {
    executeJournalEntriesGL,
    reverseAuthorizationHold,
    getAvailableBalance,
    makeCardWithdrawal,
    makeWithdrawal,
    makeDeposit,
    reverseTransaction,
    reverseCardTransaction,
    getTransactionsByInstructionId,
    getTransactionsByReferenceId,
    createGlJournalEntries,
    patchTransactionDetails
};
exports.default = logger_1.wrapLogs(coreBankingDepositRepository);
//# sourceMappingURL=deposit.repository.js.map