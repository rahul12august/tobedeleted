"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const config_1 = require("../config");
const context_1 = require("../context");
const logger_1 = __importDefault(require("../logger"));
const constant_1 = require("../common/constant");
const coreBanking_constant_1 = __importDefault(require("./coreBanking.constant"));
const coreBankingConfig = config_1.config.get('coreBanking');
exports.logRequestInterceptor = (config) => {
    try {
        const idempotencyKey = config.headers
            ? config.headers[coreBanking_constant_1.default.IDEMPOTENCY_KEY_HEADER]
            : null;
        logger_1.default.info(`Intercepting ${config.method} ${config.url} with idempotencyKey: ${idempotencyKey}`);
    }
    finally {
        return config;
    }
};
const httpClient = module_common_1.CoreBanking.createCoreBankingHttpClient(Object.assign(Object.assign({}, coreBankingConfig), { headers: {
        apiKey: process.env.MAMBU_PASSWORD
    }, retryConfig: Object.assign(Object.assign({}, constant_1.retryConfig), { networkErrorCodesToRetry: [
            'ENOENT',
            'ECONNRESET',
            'ECONNREFUSED',
            'ECONNABORTED',
            'ETIMEDOUT'
        ], maxRetriesOnError: 5, isBackOffRetry: true, statusCodesToRetry: ['409', '^5'] }) }), logger_1.default, context_1.context);
logger_1.default.info(`Setting up http-client with coreBanking on address : ${coreBankingConfig.baseURL}`);
httpClient.setRequestInterceptor(exports.logRequestInterceptor);
exports.default = httpClient;
//# sourceMappingURL=httpClient.js.map