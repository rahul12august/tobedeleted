"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const httpClient_1 = __importDefault(require("./httpClient"));
const coreBanking_constant_1 = __importDefault(require("./coreBanking.constant"));
const circuitBreaker_1 = __importDefault(require("./circuitBreaker"));
const logger_1 = require("../logger");
const baseCardPath = '/cards';
const createBlockingAmount = (cardReferenceToken, inputBlockingAmount, idempotencyKey) => __awaiter(void 0, void 0, void 0, function* () {
    yield httpClient_1.default.post(`${baseCardPath}/${cardReferenceToken}/authorizationholds`, inputBlockingAmount, {
        headers: {
            [coreBanking_constant_1.default.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
        }
    });
    inputBlockingAmount.idempotencyKey = idempotencyKey;
});
const increaseAuthorizationHold = (cardReferenceToken, authHoldExtReferenceId, amount, idempotencyKey) => __awaiter(void 0, void 0, void 0, function* () {
    yield httpClient_1.default.post(`${baseCardPath}/${cardReferenceToken}/authorizationholds/${authHoldExtReferenceId}:increase`, {
        amount
    }, {
        headers: {
            [coreBanking_constant_1.default.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
        }
    });
});
const decreaseAuthorizationHold = (cardReferenceToken, authHoldExtReferenceId, amount, idempotencyKey) => __awaiter(void 0, void 0, void 0, function* () {
    yield httpClient_1.default.post(`${baseCardPath}/${cardReferenceToken}/authorizationholds/${authHoldExtReferenceId}:decrease`, {
        amount
    }, {
        headers: {
            [coreBanking_constant_1.default.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
        }
    });
});
const coreBankingRepository = {
    createBlockingAmount: circuitBreaker_1.default.create(createBlockingAmount),
    increaseAuthorizationHold: circuitBreaker_1.default.create(increaseAuthorizationHold),
    decreaseAuthorizationHold: circuitBreaker_1.default.create(decreaseAuthorizationHold)
};
exports.default = logger_1.wrapLogs(coreBankingRepository);
//# sourceMappingURL=block.repository.js.map