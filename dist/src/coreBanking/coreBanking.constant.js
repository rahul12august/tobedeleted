"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const ApiVersion = module_common_1.CoreBanking.ApiVersion;
exports.default = {
    IDEMPOTENCY_KEY_HEADER: 'Idempotency-Key',
    VALUE_DATE_MAMBU_FORMAT: 'YYYY-MM-DDT00:00:00Z',
    ApiVersion
};
//# sourceMappingURL=coreBanking.constant.js.map