"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const contextHandler_1 = require("../common/contextHandler");
exports.RetryConfig = () => ({
    shouldRetry: (error) => {
        return error.status >= 500;
    },
    numberOfTries: contextHandler_1.getRetryCount() || 1
});
exports.stringifiedMambuError = (error) => (error &&
    error.error &&
    error.error.errors &&
    JSON.stringify(error.error.errors)) ||
    error.message;
//# sourceMappingURL=coreBanking.util.js.map