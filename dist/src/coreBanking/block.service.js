"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const contextHandler_1 = require("../common/contextHandler");
const logger_1 = require("../logger");
const block_repository_1 = __importDefault(require("./block.repository"));
const coreBanking_util_1 = require("./coreBanking.util");
const createBlockingAmount = (cardReferenceToken, inputBlockingAmount) => module_common_1.Util.retry(coreBanking_util_1.RetryConfig(), block_repository_1.default.createBlockingAmount, cardReferenceToken, inputBlockingAmount, module_common_1.Http.generateIdempotencyKey());
const increaseAuthorizationHold = (cardReferenceToken, authHoldExtReferenceId, amount) => block_repository_1.default.increaseAuthorizationHold(cardReferenceToken, authHoldExtReferenceId, amount, module_common_1.Http.generateIdempotencyKey());
const decreaseAuthorizationHold = (cardReferenceToken, authHoldExtReferenceId, amount, shouldRetry = true) => module_common_1.Util.retry(Object.assign(Object.assign({}, coreBanking_util_1.RetryConfig()), (shouldRetry && { numberOfTries: contextHandler_1.getRetryCount() })), block_repository_1.default.decreaseAuthorizationHold, cardReferenceToken, authHoldExtReferenceId, amount, module_common_1.Http.generateIdempotencyKey());
const blockService = {
    createBlockingAmount,
    increaseAuthorizationHold,
    decreaseAuthorizationHold
};
exports.default = logger_1.wrapLogs(blockService);
//# sourceMappingURL=block.service.js.map