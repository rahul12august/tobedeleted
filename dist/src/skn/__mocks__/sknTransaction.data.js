"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_util_1 = require("../../transaction/transaction.util");
const skn_constant_1 = require("../skn.constant");
exports.getMockedTransaction = () => ({
    externalId: 'externalId',
    paymentServiceType: skn_constant_1.sknOutgoingParameters.paymentServiceType,
    transactionAmount: 50000002,
    transactionDate: transaction_util_1.formatTransactionDate(new Date()),
    transactionType: skn_constant_1.sknOutgoingParameters.transactionType,
    transactionChannel: skn_constant_1.sknOutgoingParameters.transactionChannel,
    beneficiaryBankCode: 'BC005',
    beneficiaryAccountNo: '1234567890',
    beneficiaryAccountName: 'Test123',
    beneficiaryCustomerType: skn_constant_1.sknOutgoingParameters.customerType,
    beneficiaryCustomerCitizenship: 1,
    sourceAccountNo: '11110000',
    sourceAccountName: 'Test321',
    sourceCity: skn_constant_1.sknOutgoingParameters.sourceCity,
    sourceCustomerType: skn_constant_1.sknOutgoingParameters.customerType,
    sourceCustomerCitizenship: 1
});
//# sourceMappingURL=sknTransaction.data.js.map