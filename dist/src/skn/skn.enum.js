"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BeneficiaryCustomerType;
(function (BeneficiaryCustomerType) {
    BeneficiaryCustomerType["INDIVIDUAL"] = "1";
    BeneficiaryCustomerType["CORPORATE"] = "2";
    BeneficiaryCustomerType["GOVERNMENT"] = "3";
    BeneficiaryCustomerType["REMITTANCE"] = "4";
})(BeneficiaryCustomerType = exports.BeneficiaryCustomerType || (exports.BeneficiaryCustomerType = {}));
var BeneficiaryCustomerCitizenship;
(function (BeneficiaryCustomerCitizenship) {
    BeneficiaryCustomerCitizenship["RESIDENT"] = "1";
    BeneficiaryCustomerCitizenship["NON_RESIDENT"] = "2";
    BeneficiaryCustomerCitizenship["REMITTANCE"] = "3";
})(BeneficiaryCustomerCitizenship = exports.BeneficiaryCustomerCitizenship || (exports.BeneficiaryCustomerCitizenship = {}));
var SourceCustomerType;
(function (SourceCustomerType) {
    SourceCustomerType["INDIVIDUAL"] = "1";
    SourceCustomerType["CORPORATE"] = "2";
    SourceCustomerType["GOVERNMENT"] = "3";
    SourceCustomerType["REMITTANCE"] = "4";
})(SourceCustomerType = exports.SourceCustomerType || (exports.SourceCustomerType = {}));
var SourceCustomerCitizenship;
(function (SourceCustomerCitizenship) {
    SourceCustomerCitizenship["RESIDENT"] = "1";
    SourceCustomerCitizenship["NON_RESIDENT"] = "2";
    SourceCustomerCitizenship["REMITTANCE"] = "3";
})(SourceCustomerCitizenship = exports.SourceCustomerCitizenship || (exports.SourceCustomerCitizenship = {}));
//# sourceMappingURL=skn.enum.js.map