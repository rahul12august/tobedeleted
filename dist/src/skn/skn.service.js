"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_enum_1 = require("../transaction/transaction.enum");
const transaction_util_1 = require("../transaction/transaction.util");
const module_common_1 = require("@dk/module-common");
const logger_1 = __importStar(require("../logger"));
const transactionConfirmation_service_1 = __importDefault(require("../transaction/transactionConfirmation.service"));
const transaction_repository_1 = __importDefault(require("../transaction/transaction.repository"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const getResponseCode = (status, data) => {
    if (data) {
        return data.responseCode;
    }
    return status
        ? transaction_enum_1.ConfirmTransactionStatus.SUCCESS
        : transaction_enum_1.ConfirmTransactionStatus.ERROR_EXPECTED;
};
const mapConfirmTransaction = (transaction, status, data) => ({
    externalId: transaction.thirdPartyOutgoingId || '',
    accountNo: transaction.sourceAccountNo,
    transactionDate: transaction.createdAt
        ? transaction_util_1.formatTransactionDate(transaction.createdAt)
        : '',
    responseCode: getResponseCode(status, data),
    interchange: module_common_1.BankNetworkEnum.SKN,
    amount: transaction.transactionAmount,
    transactionResponseID: undefined,
    responseMessage: data && data.responseMessage
});
const confirmSknTransaction = (transaction, status, data) => __awaiter(void 0, void 0, void 0, function* () {
    const payload = mapConfirmTransaction(transaction, status, data);
    logger_1.default.info(`SKN: Transaction Confirmation for transactionId : ${transaction.id} with inquiry id : ${transaction.thirdPartyOutgoingId} 
        with responseCode : ${payload.responseCode}`);
    yield transactionConfirmation_service_1.default.confirmTransaction(payload);
    const result = yield transaction_repository_1.default.getByKey(transaction.id);
    if (!result) {
        const detail = `Failed to get transaction info for txnId ${transaction.id}`;
        logger_1.default.error(`confirmSknTransaction: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_TRANSACTION_ID);
    }
    return result;
});
const sknService = {
    confirmSknTransaction
};
exports.default = logger_1.wrapLogs(sknService);
//# sourceMappingURL=skn.service.js.map