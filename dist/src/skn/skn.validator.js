"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const skn_enum_1 = require("./skn.enum");
const lodash_1 = require("lodash");
const checkBeneficiaryAccountName = (transaction) => !lodash_1.isUndefined(transaction.beneficiaryAccountName);
const checkBeneficiaryAccountNo = (transaction) => !lodash_1.isUndefined(transaction.beneficiaryAccountNo);
const checkBeneficiaryBankCode = (transaction) => !lodash_1.isUndefined(transaction.beneficiaryBankCode);
const checkSourceAccountNo = (transaction) => !lodash_1.isUndefined(transaction.sourceAccountNo);
const checkSourceAccountName = (transaction) => !lodash_1.isUndefined(transaction.sourceAccountName);
const checkAdditionalInformation1 = (transaction) => !lodash_1.isUndefined(transaction.additionalInformation1) &&
    Object.values(skn_enum_1.BeneficiaryCustomerType).includes(transaction.additionalInformation1);
const checkAdditionalInformation2 = (transaction) => !lodash_1.isUndefined(transaction.additionalInformation2) &&
    Object.values(skn_enum_1.BeneficiaryCustomerCitizenship).includes(transaction.additionalInformation2);
exports.conditions = {
    checkBeneficiaryAccountName,
    checkBeneficiaryAccountNo,
    checkBeneficiaryBankCode,
    checkSourceAccountNo,
    checkSourceAccountName,
    checkAdditionalInformation1,
    checkAdditionalInformation2
};
exports.default = Object.values(exports.conditions);
//# sourceMappingURL=skn.validator.js.map