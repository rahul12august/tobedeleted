"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const context_1 = require("../context");
const logger_1 = __importDefault(require("../logger"));
const module_common_1 = require("@dk/module-common");
const https_1 = __importDefault(require("https"));
const { baseUrl } = config_1.config.get('skn');
const constant_1 = require("../common/constant");
const httpClient = module_common_1.Http.createHttpClientForward({
    baseURL: baseUrl,
    httpsAgent: new https_1.default.Agent({ rejectUnauthorized: false }),
    context: context_1.context,
    logger: logger_1.default,
    retryConfig: Object.assign(Object.assign({}, constant_1.retryConfig), { networkErrorCodesToRetry: ['ENOENT', 'ECONNREFUSED'] })
});
logger_1.default.info(`Base URL for SKN: ${baseUrl}`);
exports.default = httpClient;
//# sourceMappingURL=httpClient.js.map