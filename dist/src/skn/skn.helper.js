"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_util_1 = require("../transaction/transaction.util");
const logger_1 = __importDefault(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const skn_constant_1 = require("./skn.constant");
const skn_enum_1 = require("./skn.enum");
const skn_validator_1 = __importDefault(require("./skn.validator"));
const transaction_enum_1 = require("../transaction/transaction.enum");
exports.mapToSknTransaction = (transaction, externalId) => ({
    externalId: externalId,
    paymentServiceType: skn_constant_1.sknOutgoingParameters.paymentServiceType,
    transactionType: skn_constant_1.sknOutgoingParameters.transactionType,
    transactionChannel: skn_constant_1.sknOutgoingParameters.transactionChannel,
    transactionDate: transaction_util_1.formatTransactionDate(transaction.createdAt || new Date()),
    transactionAmount: transaction.transactionAmount,
    beneficiaryBankCode: transaction.beneficiaryRemittanceCode || '',
    beneficiaryAccountName: transaction.beneficiaryAccountName || '',
    beneficiaryAccountNo: transaction.beneficiaryAccountNo || '',
    beneficiaryCustomerType: transaction.additionalInformation1 || '',
    beneficiaryCustomerCitizenship: Number(transaction.additionalInformation2),
    sourceAccountNo: transaction.sourceAccountNo || '',
    sourceAccountName: transaction.sourceAccountName || '',
    sourceCity: skn_constant_1.sknOutgoingParameters.sourceCity,
    sourceCustomerCitizenship: Number(skn_enum_1.SourceCustomerCitizenship.RESIDENT),
    sourceCustomerType: transaction.paymentServiceCode === 'SKN_FOR_BUSINESS'
        ? skn_enum_1.SourceCustomerType.CORPORATE
        : skn_enum_1.SourceCustomerType.INDIVIDUAL,
    notes: transaction.note || ''
});
exports.validateMandatoryFields = (transaction) => {
    let isValid = true;
    for (let rule of skn_validator_1.default) {
        const passed = rule(transaction);
        if (!passed) {
            logger_1.default.error(`validateMandatoryFields: Skn validation failed for rule: ${rule.name}`);
            isValid = false;
            break;
        }
    }
    if (!isValid) {
        throw new AppError_1.TransferAppError('Skn transaction validation failed!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_MANDATORY_FIELDS);
    }
    return;
};
//# sourceMappingURL=skn.helper.js.map