"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const AppError_1 = require("../errors/AppError");
const transaction_repository_1 = __importDefault(require("../transaction/transaction.repository"));
const transaction_util_1 = __importDefault(require("../transaction/transaction.util"));
const logger_1 = __importStar(require("../logger"));
const skn_helper_1 = require("../skn/skn.helper");
const skn_helper_2 = require("./skn.helper");
const errors_1 = require("../common/errors");
const lodash_1 = require("lodash");
const constant_1 = require("../common/constant");
const transaction_enum_1 = require("../transaction/transaction.enum");
const skn_service_1 = __importDefault(require("./skn.service"));
const transaction_helper_1 = __importDefault(require("../transaction/transaction.helper"));
const syariahSkn_repository_1 = __importDefault(require("./syariahSkn.repository"));
const submitTransaction = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    let status = false;
    let data = undefined;
    try {
        skn_helper_1.validateMandatoryFields(transactionModel);
        if (!transactionModel.thirdPartyOutgoingId) {
            const detail = 'Third party outgoing id is missing while doing SKN Syariah transaction.';
            logger_1.default.error(detail);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_EXTERNALID);
        }
        const externalId = transactionModel.thirdPartyOutgoingId;
        transactionModel.thirdPartyOutgoingId = externalId.padStart(16, '0');
        const sknTransactionPayload = skn_helper_2.mapToSknTransaction(transactionModel, transactionModel.thirdPartyOutgoingId);
        data = yield transaction_util_1.default.submitTransactionWithRetry(syariahSkn_repository_1.default.submitTransaction, sknTransactionPayload);
        status = true;
    }
    catch (error) {
        logger_1.default.error('failed submission to skn syariah payment', error);
        if (error.code && error.code === constant_1.httpClientTimeoutErrorCode) {
            logger_1.default.error(`Timeout error from SKN Syariah with code : ${error.code}`);
            status = true;
        }
    }
    const updatedTransaction = yield transaction_repository_1.default.update(transactionModel.id, {
        status: transaction_enum_1.TransactionStatus.SUBMITTED,
        thirdPartyOutgoingId: transactionModel.thirdPartyOutgoingId,
        journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.THIRD_PARTY_SUBMITTED, transactionModel)
    });
    return skn_service_1.default.confirmSknTransaction(updatedTransaction, status, data);
});
const isEligible = (model) => {
    if (model.paymentServiceCode) {
        return (['SKN_SHARIA'].includes(model.paymentServiceCode) &&
            module_common_1.BankChannelEnum.EXTERNAL === model.beneficiaryBankCodeChannel &&
            !lodash_1.isEmpty(model.beneficiaryAccountNo));
    }
    return false;
};
const shouldSendNotification = () => true;
const getRail = () => module_common_1.Rail.SYARIAH_SKN;
const syariahSknSubmissionTemplate = {
    submitTransaction,
    isEligible,
    getRail,
    shouldSendNotification
};
exports.default = logger_1.wrapLogs(syariahSknSubmissionTemplate);
//# sourceMappingURL=syariahSknSubmissionTemplate.service.js.map