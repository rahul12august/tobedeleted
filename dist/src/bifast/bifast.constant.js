"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bifast_enum_1 = require("./bifast.enum");
exports.SOURCE_ACCOUNT_TYPE = 'SVGS';
exports.CHANNEL_TYPE = '02';
exports.BIFAST_PAYMENT_SERVICE_TYPE = 'OUTGOING_BIFAST';
exports.BIFAST_VOID_PAYMENT_SERVICE_CODE = 'REFUND_BIFAST';
exports.BIFAST_OUTGOING_PAYMENT_SERVICE_CODE = 'BIFAST_OUTGOING';
exports.BIFAST_PROXY_BANK_CODE = 'BC000';
exports.CUSTOMER_TYPE_CORPORATE = 'CORPORATE';
exports.BIFAST_PROXY_TYPES = [
    bifast_enum_1.BeneficiaryProxyType.PHONE_NUMBER,
    bifast_enum_1.BeneficiaryProxyType.EMAIL,
    bifast_enum_1.BeneficiaryProxyType.IPT_ID
];
exports.default = {
    MAX_LENGTH_BI_FAST_EXTERNAL_ID: 24,
    MAX_LENGTH_BI_FAST_KOMI_ID: 36
};
//# sourceMappingURL=bifast.constant.js.map