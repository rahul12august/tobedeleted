"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("@hapi/joi"));
const bifast_constant_1 = __importDefault(require("./bifast.constant"));
const transaction_constant_1 = __importDefault(require("../transaction/transaction.constant"));
const module_common_1 = require("@dk/module-common");
const transaction_enum_1 = require("../transaction/transaction.enum");
const transaction_validator_1 = require("../transaction/transaction.validator");
exports.biFastConfirmTransactionRequestValidator = joi_1.default.object({
    externalId: joi_1.default.string()
        .required()
        .max(bifast_constant_1.default.MAX_LENGTH_BI_FAST_EXTERNAL_ID)
        .description('Unique identity of transaction'),
    accountNo: module_common_1.JoiValidator.requiredString()
        .max(transaction_constant_1.default.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
        .description('Account Number with max length 40'),
    transactionDate: transaction_validator_1.incomingTransactionDate,
    responseCode: joi_1.default.number()
        .required()
        .valid(Object.values(transaction_enum_1.ConfirmTransactionStatus))
        .description('Response code for confirming transaction'),
    responseMessage: joi_1.default.string()
        .max(transaction_constant_1.default.MAX_LENGTH_TRANSACTION_RESPONSE_MESSAGE)
        .description('Response Message for confirming transaction'),
    komiUniqueId: joi_1.default.string()
        .optional()
        .max(bifast_constant_1.default.MAX_LENGTH_BI_FAST_KOMI_ID)
        .description('Unique identity of BI Fast Komi')
}).label('BIFastConfirmTransactionRequest');
exports.biFastConfirmTransactionResponseValidator = joi_1.default.object({
    externalId: joi_1.default.string()
        .required()
        .max(bifast_constant_1.default.MAX_LENGTH_BI_FAST_EXTERNAL_ID)
        .description('External unique identity of transaction')
}).label('BIFastConfirmTransactionResponses');
//# sourceMappingURL=bifast.validator.js.map