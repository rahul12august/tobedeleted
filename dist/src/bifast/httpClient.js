"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const context_1 = require("../context");
const logger_1 = __importDefault(require("../logger"));
const module_httpclient_1 = require("@dk/module-httpclient");
const constant_1 = require("../common/constant");
const https_1 = __importDefault(require("https"));
const token = process.env.BIFAST_TOKEN || '';
const baseUrlFromVault = process.env.BIFAST_URL;
logger_1.default.debug(`BIFAST token: ${token}`);
logger_1.default.debug(`BIFAST URL from vault: ${baseUrlFromVault}`);
const { timeout } = config_1.config.get('bifast');
const httpClient = module_httpclient_1.createHttpClient({
    baseURL: baseUrlFromVault,
    headers: {
        Authorization: `Basic ${token}`
    },
    timeout: timeout,
    context: context_1.context,
    logger: logger_1.default,
    retryConfig: Object.assign(Object.assign({}, constant_1.retryConfig), { networkErrorCodesToRetry: ['ENOENT', 'ECONNREFUSED'] }),
    httpsAgent: new https_1.default.Agent({ rejectUnauthorized: false })
});
exports.default = httpClient;
//# sourceMappingURL=httpClient.js.map