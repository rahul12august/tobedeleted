"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const transactionConfirmation_service_1 = __importDefault(require("../transaction/transactionConfirmation.service"));
const confirmTransaction = (input) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`BI Fast: Transaction Confirmation with external id: ${input.externalId} status: ${input.responseCode}`);
    const transactionInput = Object.assign({}, input);
    logger_1.default.info(`BI Fast Confirmation: Sending request to transaction confirmation.`);
    yield transactionConfirmation_service_1.default.confirmTransaction(transactionInput);
    return {
        externalId: input.externalId
    };
});
const biFastService = logger_1.wrapLogs({
    confirmTransaction
});
exports.default = biFastService;
//# sourceMappingURL=bifast.service.js.map