"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const transaction_util_1 = require("../transaction/transaction.util");
const logger_1 = __importStar(require("../logger"));
const bifast_repository_1 = __importDefault(require("./bifast.repository"));
const bifast_helper_1 = require("./bifast.helper");
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const transaction_enum_1 = require("../transaction/transaction.enum");
const lodash_1 = require("lodash");
const getResponseStatus = (result) => {
    switch (result.responseCode) {
        case '200':
            return transaction_enum_1.TransactionStatus.SUCCEED;
        case '400':
            return transaction_enum_1.TransactionStatus.DECLINED;
        default:
            return transaction_enum_1.TransactionStatus.SUBMITTED;
    }
};
const getTransactionStatus = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        transactionModel = bifast_helper_1.populateDefaultBiFastMandatoryFields(transactionModel);
        bifast_helper_1.validateBiFastMandatoryFields(transactionModel);
        const input = bifast_helper_1.mapBIFastTransactionStatus(transactionModel);
        const result = yield bifast_repository_1.default.getPaymentStatus(input);
        if (!result) {
            throw new AppError_1.TransferAppError('Failed get bifast transaction status!', transaction_enum_1.TransferFailureReasonActor.BIFAST, errors_1.ERROR_CODE.ERROR_FROM_BIFAST);
        }
        const status = getResponseStatus(result);
        return { status };
    }
    catch (error) {
        logger_1.default.error(`failed get bi fast transaction status to paymentServiceType: ${transactionModel.paymentServiceType}, paymentServiceCode: ${transactionModel.paymentServiceCode} error: ${JSON.stringify(error)}`);
        throw error;
    }
});
const isEligible = (model) => {
    if (model.paymentServiceCode) {
        return (transaction_util_1.isBIFast(model.paymentServiceCode) &&
            module_common_1.BankChannelEnum.EXTERNAL === model.beneficiaryBankCodeChannel &&
            !lodash_1.isEmpty(model.beneficiaryAccountNo));
    }
    return false;
};
const shouldSendNotification = () => true;
const bifastStatusTemplate = {
    getTransactionStatus,
    isEligible,
    shouldSendNotification
};
exports.default = logger_1.wrapLogs(bifastStatusTemplate);
//# sourceMappingURL=bifastStatus.template.js.map