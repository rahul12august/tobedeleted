"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bifast_enum_1 = require("./bifast.enum");
const lodash_1 = require("lodash");
const bifast_constant_1 = require("./bifast.constant");
const checkBeneficiaryAccountName = (transaction) => !lodash_1.isUndefined(transaction.beneficiaryAccountName);
const checkBeneficiaryAccountNo = (transaction) => !lodash_1.isUndefined(transaction.beneficiaryAccountNo);
const checkBeneficiaryBankCode = (transaction) => !lodash_1.isUndefined(transaction.beneficiaryBankCode);
const checkSourceAccountNo = (transaction) => !lodash_1.isUndefined(transaction.sourceAccountNo);
const checkSourceAccountName = (transaction) => !lodash_1.isUndefined(transaction.sourceAccountName);
const checkSourceBankCode = (transaction) => !lodash_1.isUndefined(transaction.sourceAccountName);
const checkAdditionalInformation1 = (transaction) => !lodash_1.isUndefined(transaction.additionalInformation1) &&
    Object.values(bifast_enum_1.CustomerType).includes(transaction.additionalInformation1);
const checkAdditionalInformation2 = (transaction) => !lodash_1.isUndefined(transaction.additionalInformation2) &&
    Object.values(bifast_enum_1.ResidentialStatus).includes(transaction.additionalInformation2);
const checkSourceNik = (transaction) => !lodash_1.isUndefined(transaction.sourceIdNumber);
const checkBeneficiaryBifastAccountType = (transaction) => !lodash_1.isUndefined(transaction.beneficiaryAccountType) &&
    Object.values(bifast_enum_1.AccountType).includes(transaction.beneficiaryAccountType);
const checkProxyTypeAndAlias = (transaction) => transaction.additionalInformation3 &&
    bifast_constant_1.BIFAST_PROXY_TYPES.includes(transaction.additionalInformation3)
    ? !lodash_1.isEmpty(transaction.additionalInformation4)
    : true;
exports.conditions = {
    checkBeneficiaryAccountName,
    checkBeneficiaryAccountNo,
    checkBeneficiaryBankCode,
    checkBeneficiaryBifastAccountType,
    checkSourceAccountNo,
    checkSourceAccountName,
    checkSourceBankCode,
    checkSourceNik,
    checkAdditionalInformation1,
    checkAdditionalInformation2,
    checkProxyTypeAndAlias
};
exports.default = Object.values(exports.conditions);
//# sourceMappingURL=bifast.condition.js.map