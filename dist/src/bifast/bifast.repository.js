"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const httpClient_1 = __importDefault(require("./httpClient"));
const logger_1 = __importStar(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const constant_1 = require("../common/constant");
const config_1 = require("../config");
const lodash_1 = require("lodash");
const bifast_enum_1 = require("./bifast.enum");
const transaction_enum_1 = require("../transaction/transaction.enum");
const { transactionUrl } = config_1.config.get('bifast');
const paymentStatusUrl = ':paymentStatus';
const submitTransaction = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const bifastRequest = {
            creditTransferRequest: transaction
        };
        logger_1.default.info(`Submitting BI-FAST transaction 
      externalId : ${transaction.transactionId}, 
      beneficiaryBankCode: ${transaction.beneficiaryBankCode}, 
      beneficiaryAccountNo: ${transaction.beneficiaryAccountNo},
      sourceAccountNo: ${transaction.sourceAccountNo},
      sourceNik: ${transaction.sourceNik}`);
        logger_1.default.debug(`submitTransaction: BIFAST transactionUrl: ${transactionUrl}`);
        logger_1.default.debug(`submitTransaction: BIFAST transfer request: ${JSON.stringify(bifastRequest)}`);
        const response = yield httpClient_1.default.post(transactionUrl, bifastRequest);
        logger_1.default.debug(`submitTransaction: got response from bifast ${response.status}`);
        const data = lodash_1.get(response, ['data'], {});
        if (data.creditTransferResponse.responseCode !== bifast_enum_1.BifastResponseCode.SUCCESS) {
            const detail = `Error from BI-FAST while submitting transaction`;
            logger_1.default.error(`submitTransaction: ${detail}, response :
        ${JSON.stringify(data)}`);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.BIFAST, errors_1.ERROR_CODE.ERROR_FROM_BIFAST);
        }
        logger_1.default.info(`submitTransaction: submit BI-FAST transaction response : ${JSON.stringify(data)}`);
        return data;
    }
    catch (error) {
        let detail = 'Error from BI-FAST while submitting transaction', actor = transaction_enum_1.TransferFailureReasonActor.BIFAST, errorCode = errors_1.ERROR_CODE.ERROR_FROM_BIFAST;
        logger_1.default.error(`submitTransaction catch: ${detail}`, error && error.message);
        if (error.code == constant_1.httpClientTimeoutErrorCode) {
            errorCode = errors_1.ERROR_CODE.BIFAST_REQUEST_TIMEOUT;
        }
        if (error instanceof AppError_1.TransferAppError) {
            detail = error.detail;
            actor = error.actor;
        }
        throw new AppError_1.TransferAppError(detail, actor, errorCode);
    }
});
const getPaymentStatus = (paymentStatusRequest) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.info(`getPaymentStatus: payment status payload: ${JSON.stringify(paymentStatusRequest)}`);
        const bifastPaymentStatusRequest = {
            paymentStatusRequest: paymentStatusRequest
        };
        logger_1.default.info(`getPaymentStatus: Checking BI-FAST transaction payment status`);
        const response = yield httpClient_1.default.post(paymentStatusUrl, bifastPaymentStatusRequest);
        const data = lodash_1.get(response, ['data'], {});
        logger_1.default.info(`getPaymentStatus: got response from BI Fast reponse : 
        transactionId: ${data.paymentStatusResponse.transactionId},
        transactionDate: ${data.paymentStatusResponse.transactionDate},
        komiUniqueId: ${data.paymentStatusResponse.komiUniqueId},
        transactionResponseId: ${data.paymentStatusResponse.transactionResponseId},
        responseCode: ${data.paymentStatusResponse.responseCode},
        responseMessage:${data.paymentStatusResponse.responseMessage}`);
        return data.paymentStatusResponse;
    }
    catch (error) {
        const details = 'Error from BI-FAST while getting transaction status!';
        logger_1.default.error(`getPaymentStatus: ${details}`, error && error.message);
        throw new AppError_1.TransferAppError(details, transaction_enum_1.TransferFailureReasonActor.BIFAST, errors_1.ERROR_CODE.ERROR_FROM_BIFAST);
    }
});
const bifastRepository = logger_1.wrapLogs({
    submitTransaction,
    getPaymentStatus
});
exports.default = bifastRepository;
//# sourceMappingURL=bifast.repository.js.map