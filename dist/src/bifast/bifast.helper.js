"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importDefault(require("../logger"));
const bifast_enum_1 = require("./bifast.enum");
const bifast_constant_1 = require("./bifast.constant");
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const bifast_condition_1 = __importDefault(require("./bifast.condition"));
const dateUtils_1 = require("../common/dateUtils");
const transaction_enum_1 = require("../transaction/transaction.enum");
const getBiFastTransaction = (transaction) => (transaction.thirdPartyOutgoingId &&
    transaction.thirdPartyOutgoingId.substr(transaction.thirdPartyOutgoingId.length - 8)) ||
    '';
exports.mapToBifastTransaction = (transaction) => {
    var _a;
    const beneficiaryProxyAlias = transaction.additionalInformation3 &&
        [bifast_enum_1.BeneficiaryProxyType.PHONE_NUMBER].includes(transaction.additionalInformation3)
        ? transaction.additionalInformation4 &&
            transaction.additionalInformation4.includes(transaction_enum_1.PhoneCode.PLUS62)
            ? transaction.additionalInformation4.replace(transaction_enum_1.PhoneCode.PLUS62, '62')
            : transaction.additionalInformation4
        : transaction.additionalInformation4;
    return {
        transactionId: getBiFastTransaction(transaction),
        transactionDate: dateUtils_1.toJKTDate(transaction.createdAt || new Date(), 'YYMMDDHHmmss'),
        paymentServiceType: bifast_constant_1.BIFAST_PAYMENT_SERVICE_TYPE,
        channelType: bifast_constant_1.CHANNEL_TYPE,
        transactionAmount: transaction.transactionAmount.toString(),
        beneficiaryBankCode: transaction.beneficiaryRemittanceCode || '',
        beneficiaryAccountName: transaction.beneficiaryAccountName || '',
        beneficiaryAccountNo: transaction.beneficiaryAccountNo || 'null',
        beneficiaryAccountType: transaction.beneficiaryAccountType || null,
        beneficiaryCustomerType: (transaction.additionalInformation1 &&
            (((_a = transaction.additionalInformation1) === null || _a === void 0 ? void 0 : _a.length) > 1
                ? transaction.additionalInformation1
                : '0' + transaction.additionalInformation1)) ||
            '',
        beneficiaryResidentialStatus: (transaction.additionalInformation2 &&
            '0' + transaction.additionalInformation2) ||
            '',
        sourceNik: transaction.sourceIdNumber || '',
        sourceBankCode: transaction.sourceRemittanceCode || '',
        sourceAccountNo: transaction.sourceAccountNo || '',
        sourceAccountName: transaction.sourceAccountName || '',
        sourceResidentialStatus: '0' + bifast_enum_1.ResidentialStatus.RESIDENT,
        sourceAccountType: transaction.sourceAccountTypeBI || '',
        sourceCustomerType: transaction.sourceCustomerType || '',
        notes: transaction.note || null,
        beneficiaryProxyType: transaction.additionalInformation3 || null,
        beneficiaryProxyAlias: beneficiaryProxyAlias || null
    };
};
exports.populateDefaultBiFastMandatoryFields = (transaction) => {
    var _a, _b, _c, _d;
    logger_1.default.info(`populateDefaultBiFastMandatoryFields: transaction model with additionalInformation1 ${transaction.additionalInformation1} additionalInformation2 ${transaction.additionalInformation2} additionalInformation3 ${transaction.additionalInformation3} additionalInformation4 ${transaction.additionalInformation4}`);
    return Object.assign(Object.assign({}, transaction), { additionalInformation1: (_a = transaction.additionalInformation1, (_a !== null && _a !== void 0 ? _a : bifast_enum_1.CustomerType.INDIVIDUAL)), additionalInformation2: (_b = transaction.additionalInformation2, (_b !== null && _b !== void 0 ? _b : bifast_enum_1.ResidentialStatus.RESIDENT)), additionalInformation3: (_c = transaction.additionalInformation3, (_c !== null && _c !== void 0 ? _c : bifast_enum_1.BeneficiaryProxyType.ACCOUNT_NUMBER)), additionalInformation4: (_d = transaction.additionalInformation4, (_d !== null && _d !== void 0 ? _d : transaction.beneficiaryAccountNo)) });
};
exports.validateBiFastMandatoryFields = (transaction) => {
    let isValid = true;
    for (let rule of bifast_condition_1.default) {
        const passed = rule(transaction);
        if (!passed) {
            logger_1.default.error(`validateBiFastMandatoryFields: Bifast validation failed for rule: ${rule.name}`);
            isValid = false;
            break;
        }
    }
    if (!isValid) {
        throw new AppError_1.TransferAppError('Bifast validation failed!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_MANDATORY_FIELDS);
    }
    return;
};
exports.mapBIFastTransactionStatus = (transactionModel) => {
    const biFastId = getBiFastTransaction(transactionModel);
    return {
        transactionId: biFastId,
        sourceBankCode: transactionModel.sourceRemittanceCode || '',
        oriTransactionId: biFastId,
        oriTransactionDate: dateUtils_1.toJKTDate(transactionModel.createdAt, 'YYMMDDHHmmss'),
        sourceAccountNumber: transactionModel.sourceAccountNo || '',
        channelType: bifast_constant_1.CHANNEL_TYPE
    };
};
//# sourceMappingURL=bifast.helper.js.map