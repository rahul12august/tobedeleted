"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const logger_1 = __importDefault(require("../logger"));
const bifast_service_1 = __importDefault(require("./bifast.service"));
const bifast_validator_1 = require("./bifast.validator");
const contextHandler_1 = require("../common/contextHandler");
const entitlementFlag_1 = __importDefault(require("../common/entitlementFlag"));
const entitlement_service_1 = __importDefault(require("../entitlement/entitlement.service"));
const confirmTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/bi-fast/transaction-confirmation',
    options: {
        description: 'Confirm BI Fast transaction',
        notes: 'Private Api endpoint is called from BI Fast to confirm transaction',
        tags: ['api', 'transactions'],
        auth: false,
        validate: {
            payload: bifast_validator_1.biFastConfirmTransactionRequestValidator
        },
        response: {
            schema: bifast_validator_1.biFastConfirmTransactionResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                const { payload } = hapiRequest;
                logger_1.default.debug(`Payload: ${JSON.stringify(payload)}`);
                logger_1.default.info(`confirmTransaction: BiFast transaction confirmation call initiated`);
                yield contextHandler_1.updateRequestIdInContext(payload.externalId);
                const response = yield bifast_service_1.default.confirmTransaction(payload);
                if (yield entitlementFlag_1.default.isEnabled()) {
                    yield entitlement_service_1.default.processReversalEntitlementCounterUsageOnTransactionConfirmation(payload);
                }
                return hapiResponse.response(response).code(module_common_1.Http.StatusCode.OK);
            }
            catch (e) {
                if (yield entitlementFlag_1.default.isEnabled()) {
                    yield entitlement_service_1.default.processReversalEntitlementCounterUsageBasedOnContext();
                }
                return e;
            }
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'Ok'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    },
                    [module_common_1.Http.StatusCode.UNAUTHORIZED]: {
                        description: 'UNAUTHORIZED'
                    }
                }
            }
        }
    }
};
const bifastController = [confirmTransaction];
exports.default = bifastController;
//# sourceMappingURL=bifast.controller.js.map