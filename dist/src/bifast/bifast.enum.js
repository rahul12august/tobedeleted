"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CustomerType;
(function (CustomerType) {
    CustomerType["INDIVIDUAL"] = "1";
    CustomerType["CORPORATE"] = "2";
    CustomerType["GOVERNMENT"] = "3";
    CustomerType["REMITTANCE"] = "4";
    CustomerType["OTHERS"] = "99";
})(CustomerType = exports.CustomerType || (exports.CustomerType = {}));
var ResidentialStatus;
(function (ResidentialStatus) {
    ResidentialStatus["RESIDENT"] = "1";
    ResidentialStatus["NON_RESIDENT"] = "2";
})(ResidentialStatus = exports.ResidentialStatus || (exports.ResidentialStatus = {}));
var ChannelType;
(function (ChannelType) {
    ChannelType["INTERNET_BANKING"] = "01";
    ChannelType["MOBILE_BANKING"] = "02";
    ChannelType["OVER_THE_COUNTER"] = "03";
    ChannelType["OTHER"] = "99";
})(ChannelType = exports.ChannelType || (exports.ChannelType = {}));
var AccountType;
(function (AccountType) {
    AccountType["CURRENT_ACCOUNT"] = "CACC";
    AccountType["SAVINGS_ACCOUNT"] = "SVGS";
    AccountType["LOAN"] = "LOAN";
    AccountType["CREDIT_CARD"] = "CCRD";
    AccountType["E_MONEY"] = "UESB";
    AccountType["OTHER"] = "OTHR";
})(AccountType = exports.AccountType || (exports.AccountType = {}));
var BeneficiaryProxyType;
(function (BeneficiaryProxyType) {
    BeneficiaryProxyType["ACCOUNT_NUMBER"] = "00";
    BeneficiaryProxyType["PHONE_NUMBER"] = "01";
    BeneficiaryProxyType["EMAIL"] = "02";
    BeneficiaryProxyType["IPT_ID"] = "03";
})(BeneficiaryProxyType = exports.BeneficiaryProxyType || (exports.BeneficiaryProxyType = {}));
var BifastResponseCode;
(function (BifastResponseCode) {
    BifastResponseCode["SUCCESS"] = "200";
    BifastResponseCode["FAILED"] = "400";
    BifastResponseCode["TIMEDOUT"] = "408";
})(BifastResponseCode = exports.BifastResponseCode || (exports.BifastResponseCode = {}));
//# sourceMappingURL=bifast.enum.js.map