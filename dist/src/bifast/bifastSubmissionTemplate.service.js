"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const AppError_1 = require("../errors/AppError");
const transaction_util_1 = __importStar(require("../transaction/transaction.util"));
const logger_1 = __importStar(require("../logger"));
const bifast_helper_1 = require("./bifast.helper");
const errors_1 = require("../common/errors");
const lodash_1 = require("lodash");
const bifast_repository_1 = __importDefault(require("./bifast.repository"));
const customer_service_1 = __importDefault(require("../customer/customer.service"));
const contextHandler_1 = require("../common/contextHandler");
const transaction_repository_1 = __importDefault(require("../transaction/transaction.repository"));
const bifast_enum_1 = require("./bifast.enum");
const bifast_constant_1 = require("./bifast.constant");
const transaction_enum_1 = require("../transaction/transaction.enum");
const submitTransaction = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (!transactionModel.sourceCustomerId) {
            throw new AppError_1.TransferAppError("Transaction model doesn't contain source customer ID!", transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_MANDATORY_FIELDS);
        }
        const customerInfo = yield customer_service_1.default.getCustomerById(transactionModel.sourceCustomerId);
        transactionModel.sourceIdNumber = customerInfo && customerInfo.idNumber;
        if (customerInfo && customerInfo.customerType == bifast_constant_1.CUSTOMER_TYPE_CORPORATE) {
            transactionModel.sourceIdNumber = customerInfo.npwpNumber;
        }
        transactionModel.sourceAccountTypeBI = bifast_enum_1.AccountType.SAVINGS_ACCOUNT;
        transactionModel.sourceCustomerType = '0' + bifast_enum_1.CustomerType.INDIVIDUAL;
        transactionModel = bifast_helper_1.populateDefaultBiFastMandatoryFields(transactionModel);
        bifast_helper_1.validateBiFastMandatoryFields(transactionModel);
        if (!transactionModel.thirdPartyOutgoingId) {
            const detail = 'Bifast third party outgoing id is missing while doing transaction!';
            logger_1.default.error(detail);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_EXTERNALID);
        }
        const bifastTransactionPayload = bifast_helper_1.mapToBifastTransaction(transactionModel);
        if (transactionModel.thirdPartyOutgoingId)
            contextHandler_1.updateRequestIdInRedis(transactionModel.thirdPartyOutgoingId);
        if (transactionModel.paymentInstructionId)
            contextHandler_1.updateRequestIdInRedis(transactionModel.paymentInstructionId);
        const { updatedTransaction } = yield transaction_util_1.default.submitThirdPartyTransactionWithRetryAndUpdate(bifast_repository_1.default.submitTransaction, bifastTransactionPayload, transactionModel);
        transactionModel = updatedTransaction;
    }
    catch (error) {
        logger_1.default.error(`failed submission to paymentServiceType: ${transactionModel.paymentServiceType}, paymentServiceCode: ${transactionModel.paymentServiceCode} error: ${JSON.stringify(error)}`);
        yield transaction_repository_1.default.update(transactionModel.id, {
            sourceIdNumber: transactionModel.sourceIdNumber,
            sourceCustomerType: transactionModel.sourceCustomerType,
            sourceAccountTypeBI: transactionModel.sourceAccountTypeBI
        });
        if (error.errorCode !== errors_1.ERROR_CODE.BIFAST_REQUEST_TIMEOUT) {
            throw error;
        }
    }
    return transactionModel;
});
const isEligible = (model) => {
    if (model.paymentServiceCode) {
        return (transaction_util_1.isBIFast(model.paymentServiceCode) &&
            module_common_1.BankChannelEnum.EXTERNAL === model.beneficiaryBankCodeChannel &&
            !lodash_1.isEmpty(model.beneficiaryAccountNo));
    }
    return false;
};
const shouldSendNotification = () => true;
const getRail = () => module_common_1.Rail.BI_FAST;
const bifastSubmissionTemplate = {
    submitTransaction,
    isEligible,
    getRail,
    shouldSendNotification
};
exports.default = logger_1.wrapLogs(bifastSubmissionTemplate);
//# sourceMappingURL=bifastSubmissionTemplate.service.js.map