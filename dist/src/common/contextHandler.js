"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const context_1 = require("../context");
const redis_1 = __importDefault(require("./redis"));
const logger_1 = __importDefault(require("../logger"));
const constant_1 = require("./constant");
const config_1 = require("../config");
const lodash_1 = require("lodash");
const prefixKey = `${config_1.config.get('serviceName')}-request-id`;
const expiresIn = 600;
/*
   To have same trancing id across we are saving request id in redis for 10 minutes and when external
   api calls to give status we will use same request id.
*/
exports.updateRequestIdInContext = (referenceId) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`updateRequestIdInContext : Get reference id from redis cache and set it in context for ${referenceId}.`);
    try {
        const requestId = yield redis_1.default.get(referenceId, prefixKey);
        logger_1.default.info(`updateRequestIdInContext : Request Id ${requestId} for Reference Id : ${referenceId}`);
        if (requestId) {
            const contextObject = context_1.context.getStore();
            contextObject[constant_1.Tracing.TRANSACTION_ID] = requestId;
        }
    }
    catch (err) {
        logger_1.default.error(`Error updateRequestIdInContext : error while updating request id - ${err}`);
        return;
    }
});
exports.updateRequestIdInRedis = (referenceId) => {
    logger_1.default.info(`updateRequestIdInRedis : Get request id from context and set it in redis for ${referenceId}.`);
    try {
        const contextObject = context_1.context.getStore(); // any becuase can return any object
        const requestId = contextObject && contextObject[constant_1.Tracing.TRANSACTION_ID];
        if (requestId && !lodash_1.isEmpty(requestId)) {
            redis_1.default.mSet({
                prefix: prefixKey,
                keyValueObjectList: [
                    {
                        key: referenceId,
                        value: requestId,
                        ttl: expiresIn
                    }
                ]
            });
        }
    }
    catch (err) {
        logger_1.default.error(`Error updateRequestIdInRedis : error while fetching request id from context - ${err}`);
        return;
    }
};
exports.getRequestId = () => {
    logger_1.default.info(`getRequestId : Get request id from context.`);
    try {
        const contextObject = context_1.context.getStore(); // any becuase can return any object
        return contextObject && contextObject[constant_1.Tracing.TRANSACTION_ID];
    }
    catch (err) {
        logger_1.default.error(`Error getRequestId : error while fetching request id from context - ${err}`);
        return '';
    }
};
exports.setRetryCount = (count) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`setRetryCount : set retry count in context for ${count}.`);
    const contextObject = context_1.context.getStore();
    if (lodash_1.isEmpty(contextObject)) {
        logger_1.default.error(`setRetryCount : Async Storage context is missing.`);
        return;
    }
    contextObject[constant_1.Tracing.RETRY_COUNT] = count;
    logger_1.default.info(contextObject);
});
exports.getRetryCount = () => {
    logger_1.default.info(`getRetryCount : get retry count from context.`);
    const contextObject = context_1.context.getStore();
    if (lodash_1.isEmpty(contextObject)) {
        logger_1.default.error(`getRetryCount : Async Storage context is missing.`);
        return undefined;
    }
    const retryCount = contextObject && contextObject[constant_1.Tracing.RETRY_COUNT];
    return retryCount;
};
exports.setTransactionInfoTracker = (entitlement) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`setTransactionInfoTracker : set transaction info in context for ${entitlement}.`);
    const contextObject = context_1.context.getStore();
    contextObject[constant_1.Tracing.TRANSACTION_INFO_TRACKER] = entitlement;
    logger_1.default.info(contextObject);
});
exports.getTransactionInfoTracker = () => {
    logger_1.default.info(`getTransactionInfoTracker : get TRANSACTION info in context`);
    const contextObject = context_1.context.getStore();
    const entitlement = contextObject[constant_1.Tracing.TRANSACTION_INFO_TRACKER];
    logger_1.default.info(`entitlement info: ${entitlement}`);
    return entitlement;
};
exports.setTransactionUsageInfoTracker = (transactionUsageInfo) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`setTransactionUsageInfoTracker: set transaction limit info in context for ${transactionUsageInfo}`);
    const contextObject = context_1.context.getStore();
    contextObject[constant_1.Tracing.TRANSACTION_USAGE_INFO_TRACKER] = transactionUsageInfo;
    logger_1.default.info(contextObject);
});
exports.getTransactionUsageInfoTracker = () => {
    logger_1.default.info(`getTransactionUsageInfoTracker: get TRANSACTION LIMIT info in context`);
    const contextObject = context_1.context.getStore();
    const transactionUsage = contextObject[constant_1.Tracing.TRANSACTION_USAGE_INFO_TRACKER];
    logger_1.default.info(`transaction limit info: ${transactionUsage}`);
    return transactionUsage;
};
exports.setFnProgressTracker = (initialTime) => __awaiter(void 0, void 0, void 0, function* () {
    const contextObject = context_1.context.getStore();
    contextObject[constant_1.Tracing.PROGRESS_TRACKER] = initialTime;
});
exports.getFnProgressTracker = () => {
    const contextObject = context_1.context.getStore();
    const startTime = contextObject[constant_1.Tracing.PROGRESS_TRACKER];
    return startTime;
};
//# sourceMappingURL=contextHandler.js.map