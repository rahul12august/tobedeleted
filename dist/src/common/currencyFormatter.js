"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const currency_formatter_1 = __importDefault(require("currency-formatter"));
const IDR = 'id-ID';
exports.toIndonesianRupiah = (number) => currency_formatter_1.default.format(number, {
    locale: IDR,
    symbol: ''
});
//# sourceMappingURL=currencyFormatter.js.map