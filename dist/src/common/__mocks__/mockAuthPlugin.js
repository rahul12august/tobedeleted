"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
// provide mockAuthPayload to set up authPayload in headers
const requestWrapper = {
    name: 'mockAuth',
    version: '1.0.0',
    register: (server) => {
        server.ext('onPreHandler', (hapiRequest, hapiResponse) => {
            const authPayload = hapiRequest.headers['mockauthpayload'] &&
                JSON.parse(hapiRequest.headers['mockauthpayload']);
            lodash_1.set(hapiRequest, 'auth.credentials.payload', authPayload);
            return hapiResponse.continue;
        });
    },
    once: true
};
exports.default = requestWrapper;
//# sourceMappingURL=mockAuthPlugin.js.map