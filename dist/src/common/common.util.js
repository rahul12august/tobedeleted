"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AppError_1 = require("../errors/AppError");
const transaction_enum_1 = require("../transaction/transaction.enum");
exports.mapTransferAppError = (err, code, retry = false) => {
    const errorDetail = [];
    errorDetail.push({
        message: (err && err.message) || '',
        key: code,
        code: (err && err.code) || ''
    });
    let detail, actor;
    if (err instanceof AppError_1.TransferAppError) {
        detail = err.detail;
        actor = err.actor;
    }
    else {
        detail = (err && err.message) || 'Unexpected error!';
        actor = transaction_enum_1.TransferFailureReasonActor.UNKNOWN;
    }
    throw new AppError_1.RetryableTransferAppError(detail, actor, code, retry, errorDetail);
};
exports.trimStringValuesFromObject = (payload) => {
    if (typeof payload === 'object') {
        const keys = Object.keys(payload);
        for (let key of keys) {
            if (typeof payload[key] === 'string') {
                payload[key] = payload[key].trim();
            }
        }
    }
    return payload;
};
exports.enforceStringFormat = (payload, field) => {
    if (payload[field]) {
        payload[field] = String(payload[field]);
    }
};
exports.enforceNumberFormat = (payload, field) => {
    if (payload[field]) {
        payload[field] = Number(payload[field]);
    }
};
exports.enforceBooleanFormat = (payload, field) => {
    if (payload[field]) {
        payload[field] = Boolean(payload[field]);
    }
};
//# sourceMappingURL=common.util.js.map