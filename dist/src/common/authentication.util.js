"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const AppError_1 = require("../errors/AppError");
const errors_1 = require("./errors");
const constant_1 = require("./constant");
const transaction_enum_1 = require("../transaction/transaction.enum");
exports.getAuthPayloadFromAdminToken = (request) => {
    const auth = lodash_1.get(request, constant_1.AUTH_CREDENTIALS_PAYLOAD, {});
    if (!auth.roles) {
        throw new AppError_1.TransferAppError("Auth credentials payload doesn't contain any roles!", transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.UNAUTHORIZED);
    }
    return auth;
};
//# sourceMappingURL=authentication.util.js.map