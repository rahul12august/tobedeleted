"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_httpclient_1 = require("@dk/module-httpclient");
const module_common_1 = require("@dk/module-common");
const constant_1 = require("../common/constant");
const create = (config, fowardAuth = false) => {
    const httpClient = module_httpclient_1.createHttpClient(config);
    if (fowardAuth) {
        httpClient.setRequestInterceptor(config => {
            return module_common_1.Http.authTokenInterceptor(config, constant_1.Tracing.TRACER_SESSION, constant_1.HttpHeaders.AUTH);
        });
    }
    return httpClient;
};
exports.create = create;
//# sourceMappingURL=httpClient.js.map