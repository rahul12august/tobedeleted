"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const module_config_1 = require("@dk/module-config");
const logger_1 = __importDefault(require("../logger"));
const module_common_1 = require("@dk/module-common");
exports.MONGO_USER_PARAM = 'MONGO_USER';
exports.MONGO_PASS_PARAM = 'MONGO_PASS';
exports.MONGO_DB_NAME = 'MONGO_DB_NAME';
exports.MONGO_HOSTS = 'MONGO_HOSTS';
exports.connectMongo = () => new Promise((resolve, reject) => {
    const user = module_config_1.config.get(exports.MONGO_USER_PARAM);
    const pass = process.env.MONGO_PASSWORD || '';
    const dbName = module_config_1.config.get(exports.MONGO_DB_NAME);
    const dbHosts = module_config_1.config.get(exports.MONGO_HOSTS);
    logger_1.default.info(`MongoDb URL : ${dbHosts}`);
    const dbUri = module_common_1.Util.getMongoUri(user, pass, dbName, dbHosts, '');
    if (!dbName) {
        logger_1.default.error(`No ${exports.MONGO_DB_NAME} is provided`);
        return reject(`No ${exports.MONGO_DB_NAME} is provided`);
    }
    mongoose_1.connection.once('open', () => resolve());
    mongoose_1.connection.on('error', err => {
        logger_1.default.error('error while connecting to mongodb', err);
        reject(err);
    });
    mongoose_1.connect(dbUri, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        user,
        pass,
        dbName
    });
});
//# sourceMappingURL=mongoDb.js.map