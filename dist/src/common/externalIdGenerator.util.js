"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const outgoingCounter_repository_1 = __importDefault(require("../outgoingCounter/outgoingCounter.repository"));
const logger_1 = __importDefault(require("../logger"));
const errors_1 = require("./errors");
const AppError_1 = require("../errors/AppError");
const lodash_1 = require("lodash");
const constant_1 = require("./constant");
const transaction_util_1 = require("../transaction/transaction.util");
const transaction_constant_1 = require("../transaction/transaction.constant");
const transaction_enum_1 = require("../transaction/transaction.enum");
const MAX_RRN_COUNTER = 999999; // 6 digit number
const MAX_BIFAST_COUNTER = 99999999; //8 digit number
const BIFAST_PADDING_ZEROS = 8;
const OUTGOING_PADDING_ZEROS = 6;
exports.getJulianDate = (format = 'YYDDDD', isBIFAST = false) => {
    return isBIFAST
        ? moment_1.default()
            .tz(transaction_constant_1.TMZ)
            .format(format)
        : moment_1.default().format(format); // moment will format padding 0 - https://momentjs.com/docs/#/displaying/
};
exports.getNextCounterByPrefix = (prefix, isBIFAST) => __awaiter(void 0, void 0, void 0, function* () {
    const outgoingCounter = yield outgoingCounter_repository_1.default.getNextCounterByPrefix(prefix);
    return isBIFAST ? outgoingCounter.counter : outgoingCounter.counter - 1; // transform to base 0
});
const generateId = (code, maxCounter, paddingZeros, isBIFAST, format) => __awaiter(void 0, void 0, void 0, function* () {
    const julianDate = exports.getJulianDate(format, isBIFAST);
    const prefix = `${julianDate}${code}`;
    const currentCounter = yield exports.getNextCounterByPrefix(prefix, isBIFAST);
    if (currentCounter > maxCounter) {
        const detail = `Maximum outgoing id counter is reached, cannot generate thirdPartyOutgoingId!`;
        logger_1.default.error(`generateOutgoingId: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.MAXIMUM_COUNTER_REACHED);
    }
    return `${prefix}${currentCounter.toString().padStart(paddingZeros, '0')}`;
});
const generateBifastExternalId = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    const isBIFAST = transaction_util_1.isBIFast(transaction.paymentServiceCode);
    if (!isBIFAST) {
        logger_1.default.info('generateBifastExternalId: supportedchannel is not bifast');
        return;
    }
    if (!transaction.sourceRemittanceCode) {
        const detail = `Cannot generate bifast thirdPartyOutgoingId!`;
        logger_1.default.error(`generateBifastExternalId: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.JAGO, errors_1.ERROR_CODE.EMPTY_REMITTANCE_CODE);
    }
    return generateId(transaction.sourceRemittanceCode, MAX_BIFAST_COUNTER, BIFAST_PADDING_ZEROS, isBIFAST, constant_1.BI_FAST_DATE_FORMAT);
});
exports.generateOutgoingId = (transaction, debitTransactionCode) => __awaiter(void 0, void 0, void 0, function* () {
    let externalId = yield generateBifastExternalId(transaction);
    if (lodash_1.isEmpty(externalId)) {
        if (!(debitTransactionCode && debitTransactionCode.length)) {
            const detail = `Cannot generate external thirdPartyOutgoingId!`;
            logger_1.default.error(`generateOutgoingId: ${detail}`);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.JAGO, errors_1.ERROR_CODE.EMPTY_TRANSACTION_CODE);
        }
        externalId = yield generateId(debitTransactionCode[0], MAX_RRN_COUNTER, OUTGOING_PADDING_ZEROS);
    }
    return externalId;
});
//# sourceMappingURL=externalIdGenerator.util.js.map