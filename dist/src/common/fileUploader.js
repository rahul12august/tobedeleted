"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const config_1 = require("../config");
const partnerReportBucket = config_1.config.get('ossPartnerReport');
const partnerReportConfigOSS = Object.assign(Object.assign({}, partnerReportBucket), { accessKeyId: process.env.OSS_PARTNER_REPORT_ACCESS_KEY_ID, accessKeySecret: process.env.OSS_PARTNER_REPORT_ACCESS_KEY_SECRET });
const partnerReport = new module_common_1.UploadFile(partnerReportConfigOSS);
const fileUploader = {
    partnerReport
};
exports.default = fileUploader;
//# sourceMappingURL=fileUploader.js.map