"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_kafka_1 = require("@dk/module-kafka");
const module_message_1 = require("@dk/module-message");
const logger_1 = __importDefault(require("../logger"));
const context_1 = require("../context");
const config_1 = require("../config");
const kafKaConfig = {
    serviceName: module_message_1.TransactionGroupConstant.TRANSACTION_HISTORY_GROUPID,
    idempotent: false,
    isLocal: config_1.config.get('kafka').isLocal
};
const producer = module_kafka_1.kafkaProducer.init(kafKaConfig, logger_1.default, context_1.context);
exports.default = producer;
//# sourceMappingURL=kafkaProducer.js.map