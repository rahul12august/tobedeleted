"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
const logger_1 = require("../logger");
const redis_1 = __importDefault(require("./redis"));
const getCacheFromIntermediateKey = (key, onCacheNotFound, ...args) => __awaiter(void 0, void 0, void 0, function* () {
    const intermediateCache = yield redis_1.default.get(key);
    if (lodash_1.default.isEmpty(intermediateCache)) {
        return yield onCacheNotFound(...args);
    }
    return yield redis_1.default.get(intermediateCache, onCacheNotFound);
});
const redisRepository = {
    getCacheFromIntermediateKey
};
exports.default = logger_1.wrapLogs(redisRepository);
//# sourceMappingURL=redis.repository.js.map