"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_config_1 = require("@dk/module-config");
const logger_1 = __importDefault(require("../logger"));
let configFlag = module_config_1.config.get('featureFlags');
exports.isFeatureEnabled = (flag) => {
    if (undefined === configFlag) {
        configFlag = module_config_1.config.get('featureFlags');
    }
    if (!configFlag) {
        return false;
    }
    let isEnabled = configFlag[flag] === 'true';
    logger_1.default.info(`is ${flag} Feature Flag enabled: ${isEnabled}`);
    return isEnabled;
};
exports.refresh = () => {
    configFlag = undefined;
};
//# sourceMappingURL=featureFlag.js.map