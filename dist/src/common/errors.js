"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
var ERROR_CODE;
(function (ERROR_CODE) {
    ERROR_CODE["UNIQUE_FIELD"] = "UNIQUE_FIELD";
    ERROR_CODE["INVALID_ACCOUNT"] = "INVALID_ACCOUNT";
    ERROR_CODE["INCONSISTENT_PAYMENT_SERVICE"] = "INCONSISTENT_PAYMENT_SERVICE";
    ERROR_CODE["INVALID_MANDATORY_FIELDS"] = "INVALID_MANDATORY_FIELDS";
    ERROR_CODE["INVALID_REGISTER_PARAMETER"] = "INVALID_REGISTER_PARAMETER";
    ERROR_CODE["INCORRECT_FIELD"] = "INCORRECT_FIELD";
    ERROR_CODE["INVALID_REQUEST"] = "INVALID_REQUEST";
    ERROR_CODE["INVALID_REQUEST_OCT"] = "INVALID_REQUEST_OCT";
    ERROR_CODE["UNEXPECTED_ERROR"] = "UNEXPECTED_ERROR";
    ERROR_CODE["INVALID_AMOUNT"] = "INVALID_AMOUNT";
    ERROR_CODE["INVALID_LIMIT_TRANSACTION_AMOUNT"] = "INVALID_LIMIT_TRANSACTION_AMOUNT";
    ERROR_CODE["NO_RECOMMENDATION_SERVICES"] = "NO_RECOMMENDATION_SERVICES";
    ERROR_CODE["RECOMMENDATION_PAYMENT_SEVICE_TYPE_MISMATCH"] = "RECOMMENDATION_PAYMENT_SEVICE_TYPE_MISMATCH";
    ERROR_CODE["RECOMMENDATION_SOURCE_BANK_CODE_CHANNEL_MISMATCH"] = "RECOMMENDATION_SOURCE_BANK_CODE_CHANNEL_MISMATCH";
    ERROR_CODE["RECOMMENDATION_BENEFICIARY_BANK_CODE_CHANNEL_MISMATCH"] = "RECOMMENDATION_BENEFICIARY_BANK_CODE_CHANNEL_MISMATCH";
    ERROR_CODE["RECOMMENDATION_SOURCE_ACCOUNT_TYPE_MISMATCH"] = "RECOMMENDATION_SOURCE_ACCOUNT_TYPE_MISMATCH";
    ERROR_CODE["RECOMMENDATION_BENEFICIARY_ACCOUNT_TYPE_MISMATCH"] = "RECOMMENDATION_BENEFICIARY_ACCOUNT_TYPE_MISMATCH";
    ERROR_CODE["RECOMMENDATION_CIF_MISMATCH"] = "RECOMMENDATION_CIF_MISMATCH";
    ERROR_CODE["RECOMMENDATION_TRANSACTION_AMOUNT_IS_OUT_OF_RANGE"] = "RECOMMENDATION_TRANSACTION_AMOUNT_IS_OUT_OF_RANGE";
    ERROR_CODE["RECOMMENDATION_OVER_DAILY_LIMIT"] = "RECOMMENDATION_OVER_DAILY_LIMIT";
    ERROR_CODE["RECOMMENDATION_OVER_DAILY_LIMIT_RETRY_TOMORROW"] = "RECOMMENDATION_OVER_DAILY_LIMIT_RETRY_TOMORROW";
    ERROR_CODE["RECOMMENDATION_OVER_BILL_PAYMENT_LIMIT"] = "RECOMMENDATION_OVER_BILL_PAYMENT_LIMIT";
    ERROR_CODE["RECOMMENDATION_BIFAST_PROXY_UNAVAILABLE"] = "RECOMMENDATION_BIFAST_PROXY_UNAVAILABLE";
    ERROR_CODE["RECOMMENDATION_BIFAST_UNSUPPORTED"] = "RECOMMENDATION_BIFAST_UNSUPPORTED";
    ERROR_CODE["RECOMMENDATION_STILL_IN_BIFAST_LIMIT"] = "RECOMMENDATION_STILL_IN_BIFAST_LIMIT";
    ERROR_CODE["RECOMMENDATION_REJECTED_BY_WORKING_DAY_RULES"] = "RECOMMENDATION_REJECTED_BY_WORKING_DAY_RULES";
    ERROR_CODE["CAN_NOT_REVERSE_TRANSACTION"] = "CAN_NOT_REVERSE_TRANSACTION";
    ERROR_CODE["BALANCE_IS_INSUFFICIENT"] = "BALANCE_IS_INSUFFICIENT";
    ERROR_CODE["TRANSFER_AMOUNT_IS_INSUFFICIENT"] = "TRANSFER_AMOUNT_IS_INSUFFICIENT";
    ERROR_CODE["TRANSACTION_FAILED"] = "TRANSACTION_FAILED";
    ERROR_CODE["TRANSACTION_REJECTED"] = "TRANSACTION_REJECTED";
    ERROR_CODE["TRANSACTION_NOT_FOUND"] = "TRANSACTION_NOT_FOUND";
    ERROR_CODE["INVALID_BANK_CODE"] = "INVALID_BANK_CODE";
    ERROR_CODE["BLOCKING_ID_NOT_FOUND"] = "BLOCKING_ID_NOT_FOUND";
    ERROR_CODE["INVALID_TRANSACTION_STATUS"] = "INVALID_TRANSACTION_STATUS";
    ERROR_CODE["FAILED_TO_UPDATE_TRANSACTION_STATUS"] = "FAILED_TO_UPDATE_TRANSACTION_STATUS";
    ERROR_CODE["FAILED_TO_UPDATE_TRANSACTION"] = "FAILED_TO_UPDATE_TRANSACTION";
    ERROR_CODE["MISSING_DEBIT_TRANSACTION_CODE"] = "MISSING_DEBIT_TRANSACTION_CODE";
    ERROR_CODE["MISSING_CREDIT_TRANSACTION_CODE"] = "MISSING_CREDIT_TRANSACTION_CODE";
    ERROR_CODE["INVALID_EXTERNALID"] = "INVALID_EXTERNALID";
    ERROR_CODE["UNAUTHORIZED"] = "UNAUTHORIZED";
    ERROR_CODE["MAXIMUM_COUNTER_REACHED"] = "MAXIMUM_COUNTER_REACHED";
    ERROR_CODE["EXECUTION_TYPE_UNDEFINED"] = "EXECUTION_TYPE_UNDEFINED";
    ERROR_CODE["INVALID_ORDER_ID"] = "INVALID_ORDER_ID";
    ERROR_CODE["MISSING_CREDIT_TEMPLATE"] = "MISSING_CREDIT_TEMPLATE";
    ERROR_CODE["SOURCE_TRANSACTION_CURRENCY_UNDEFINED"] = "SOURCE_TRANSACTION_CURRENCY_UNDEFINED";
    ERROR_CODE["CURRENCY_NOT_FOUND"] = "CURRENCY_NOT_FOUND";
    ERROR_CODE["ERROR_FROM_SWICHING"] = "ERROR_FROM_SWICHING";
    ERROR_CODE["DEBIT_INTERCHANGE_NOT_FOUND"] = "DEBIT_INTERCHANGE_NOT_FOUND";
    ERROR_CODE["CREDIT_INTERCHANGE_NOT_FOUND"] = "CREDIT_INTERCHANGE_NOT_FOUND";
    ERROR_CODE["FEE_INTERCHANGE_NOT_FOUND"] = "FEE_INTERCHANGE_NOT_FOUND";
    ERROR_CODE["ERROR_FROM_IRIS"] = "ERROR_FROM_IRIS";
    ERROR_CODE["ADMIN_FEE_NOT_FOUND"] = "ADMIN_FEE_NOT_FOUND";
    ERROR_CODE["MISSING_INQUIRY_ID"] = "MISSING_INQUIRY_ID";
    ERROR_CODE["MISSING_FIXED_FEE_CONFIGURATION"] = "MISSING_FIXED_FEE_CONFIGURATION";
    ERROR_CODE["MISSING_PERCENTAGE_FEE_CONFIGURATION"] = "MISSING_PERCENTAGE_FEE_CONFIGURATION";
    ERROR_CODE["INVALID_SUBSIDIARY_VALUE"] = "INVALID_SUBSIDIARY_VALUE";
    ERROR_CODE["MISSING_SUBSIDIARY_RULE"] = "MISSING_SUBSIDIARY_RULE";
    ERROR_CODE["MISSING_FEE_RULE"] = "MISSING_FEE_RULE";
    ERROR_CODE["REQUIRE_TRANSACTION_DATE"] = "REQUIRE_TRANSACTION_DATE";
    ERROR_CODE["FAILED_TO_REVERSE"] = "FAILED_TO_REVERSE";
    ERROR_CODE["DECISION_ENGINE_CONFIG_NOT_FOUND"] = "DECISION_ENGINE_CONFIG_NOT_FOUND";
    ERROR_CODE["DECISION_ENGINE_OPERATOR_NOT_SUPPORT"] = "DECISION_ENGINE_OPERATOR_NOT_SUPPORT";
    ERROR_CODE["UNSUPPORTED_CONFIRM_TRANSACTION_STATUS"] = "UNSUPPORTED_CONFIRM_TRANSACTION_STATUS";
    ERROR_CODE["INVALID_REFUND_REQUEST"] = "INVALID_REFUND_REQUEST";
    ERROR_CODE["MISSING_CUSTOMER_OR_TRANSACTION_ID"] = "MISSING_CUSTOMER_OR_TRANSACTION_ID";
    ERROR_CODE["MISSING_START_AND_END_DATE"] = "MISSING_START_AND_END_DATE";
    ERROR_CODE["CUSTOMER_NOT_FOUND"] = "CUSTOMER_NOT_FOUND";
    ERROR_CODE["INVALID_DATE_FORMAT"] = "INVALID_DATE_FORMAT";
    ERROR_CODE["BILL_DETAILS_NOT_FOUND"] = "BILL_DETAILS_NOT_FOUND";
    ERROR_CODE["INVALID_BANK_CHANNEL"] = "INVALID_BANK_CHANNEL";
    ERROR_CODE["INVALID_MAMBU_BLOCKING_DAY"] = "INVALID_MAMBU_BLOCKING_DAY";
    ERROR_CODE["MAMBU_BLOCKING_COUNTER_NOT_FOUND"] = "MAMBU_BLOCKING_COUNTER_NOT_FOUND";
    ERROR_CODE["TRANSACTION_ALREADY_CONFIRMED"] = "TRANSACTION_ALREADY_CONFIRMED";
    ERROR_CODE["TRANSACTION_ALREADY_REVERSED"] = "TRANSACTION_ALREADY_REVERSED";
    ERROR_CODE["RDN_REVERSAL_NOT_ALLOWED"] = "RDN_REVERSAL_NOT_ALLOWED";
    ERROR_CODE["PAYROLL_INTERCHANGE_REQUIRED"] = "PAYROLL_INTERCHANGE_REQUIRED";
    ERROR_CODE["CANNOT_GET_CONFIG_PARAMETER"] = "CANNOT_GET_CONFIG_PARAMETER";
    ERROR_CODE["ERROR_FROM_RTGS"] = "ERROR_FROM_RTGS";
    ERROR_CODE["ERROR_FROM_SKN"] = "ERROR_FROM_SKN";
    ERROR_CODE["ERROR_FROM_BIFAST"] = "ERROR_FROM_BIFAST";
    ERROR_CODE["NOT_ALLOWED_TO_CREATE_BLOCKING"] = "NOT_ALLOWED_TO_CREATE_BLOCKING";
    ERROR_CODE["SAVING_PRODUCT_NOT_FOUND"] = "SAVING_PRODUCT_NOT_FOUND";
    ERROR_CODE["CAN_NOT_CREATE_BLOCK_AMOUNT"] = "CAN_NOT_CREATE_BLOCK_AMOUNT";
    ERROR_CODE["TRANSFER_INTERCHANGE_FEE_RULE_NOT_CONFIGURED"] = "TRANSFER_INTERCHANGE_FEE_RULE_NOT_CONFIGURED";
    ERROR_CODE["TRANSFER_FEE_RULE_INVALID_MANDATORY_FIELDS"] = "TRANSFER_FEE_RULE_INVALID_MANDATORY_FIELDS";
    ERROR_CODE["TRANSFER_FEE_RULE_INVALID_REGISTER_PARAMETER"] = "TRANSFER_FEE_RULE_INVALID_REGISTER_PARAMETER";
    ERROR_CODE["BLOCK_AMOUNT_IS_INSUFFICIENT"] = "BLOCK_AMOUNT_IS_INSUFFICIENT";
    ERROR_CODE["MAMBU_TRANSACTION_ALREADY_REVERSED"] = "MAMBU_TRANSACTION_ALREADY_REVERSED";
    ERROR_CODE["MAMBU_TRANSACTION_ALREADY_SETTLED"] = "MAMBU_TRANSACTION_ALREADY_SETTLED";
    ERROR_CODE["MAMBU_TRANSACTION_AUTHORIZATION_HOLD_EXPIRED"] = "MAMBU_TRANSACTION_AUTHORIZATION_HOLD_EXPIRED";
    ERROR_CODE["MAMBU_DUPLICATE_CARD_TRANSACTION"] = "MAMBU_DUPLICATE_CARD_TRANSACTION";
    ERROR_CODE["MAMBU_INVALID_PARAMETERS"] = "MAMBU_INVALID_PARAMETERS";
    ERROR_CODE["INVALID_TRANSACTION_ID"] = "INVALID_TRANSACTION_ID";
    ERROR_CODE["SOURCE_ACCOUNT_INQUIRY_FAILED"] = "SOURCE_ACCOUNT_INQUIRY_FAILED";
    ERROR_CODE["BENEFICIARY_ACCOUNT_INQUIRY_FAILED"] = "BENEFICIARY_ACCOUNT_INQUIRY_FAILED";
    ERROR_CODE["PAYMENT_CONFIG_RULE_FETCH_FAILED"] = "PAYMENT_CONFIG_RULE_FETCH_FAILED";
    ERROR_CODE["PAYMENT_CONFIG_RULES_EVALUATION_FAILED"] = "PAYMENT_CONFIG_RULES_EVALUATION_FAILED";
    ERROR_CODE["COREBANKING_BLOCK_AMOUNT_FAILED"] = "COREBANKING_BLOCK_AMOUNT_FAILED";
    ERROR_CODE["THIRDPARTY_SUBMISSION_FAILED"] = "THIRDPARTY_SUBMISSION_FAILED";
    ERROR_CODE["COREBANKING_DEBIT_FAILED"] = "COREBANKING_DEBIT_FAILED";
    ERROR_CODE["COREBANKING_CREDIT_FAILED"] = "COREBANKING_CREDIT_FAILED";
    ERROR_CODE["COREBANKING_FEES_FAILED"] = "COREBANKING_FEES_FAILED";
    ERROR_CODE["COREBANKING_DISBURSEMENT_FAILED"] = "COREBANKING_DISBURSEMENT_FAILED";
    ERROR_CODE["COREBANKING_REPAYMENT_FAILED"] = "COREBANKING_REPAYMENT_FAILED";
    ERROR_CODE["COREBANKING_PAY_OFF_FAILED"] = "COREBANKING_PAY_OFF_FAILED";
    ERROR_CODE["COREBANKING_GL_JOURNAL_ENTRIES_FAILED"] = "COREBANKING_GL_JOURNAL_ENTRIES_FAILED";
    ERROR_CODE["INTERCHANGE_NOT_EXPECTED"] = "INTERCHANGE_NOT_EXPECTED";
    ERROR_CODE["TRANSACTION_DATE_NOT_ALLOWED"] = "TRANSACTION_DATE_NOT_ALLOWED";
    ERROR_CODE["MONGO_TIMEOUT_ERROR"] = "MONGO_TIMEOUT_ERROR";
    ERROR_CODE["ERROR_FROM_TOKOPEDIA"] = "ERROR_FROM_TOKOPEDIA";
    ERROR_CODE["INVALID_SIGNATURE"] = "INVALID_SIGNATURE";
    ERROR_CODE["TOKOPEDIA_AUTH_TOKEN_ACCESS_ERROR"] = "TOKOPEDIA_AUTH_TOKEN_ACCESS_ERROR";
    ERROR_CODE["REQUEST_TEMPLATE_NOT_FOUND"] = "REQUEST_TEMPLATE_NOT_FOUND";
    ERROR_CODE["ADDITIONAL_PAYLOAD_NOT_EXIST"] = "ADDITIONAL_PAYLOAD_NOT_EXIST";
    ERROR_CODE["INVALID_ADDITIONAL_PAYLOAD"] = "INVALID_ADDITIONAL_PAYLOAD";
    ERROR_CODE["SOURCE_OR_BENEFICIARY_ACCOUNT_IS_BEING_MIGRATED_TO_SHARIA"] = "SOURCE_OR_BENEFICIARY_ACCOUNT_IS_BEING_MIGRATED_TO_SHARIA";
    ERROR_CODE["ERROR_FROM_RTGS_SHARIA"] = "ERROR_FROM_RTGS_SHARIA";
    ERROR_CODE["ERROR_FROM_SKN_SHARIA"] = "ERROR_FROM_SKN_SHARIA";
    ERROR_CODE["SOURCE_ACCOUNT_NOT_FOUND"] = "SOURCE_ACCOUNT_NOT_FOUND";
    ERROR_CODE["ERROR_WHILE_FETCHING_COREBANKING_ID"] = "ERROR_WHILE_FETCHING_COREBANKING_ID";
    ERROR_CODE["ALTO_AUTH_TOKEN_ACCESS_ERROR"] = "ALTO_AUTH_TOKEN_ACCESS_ERROR";
    ERROR_CODE["ERROR_FROM_ALTO"] = "ERROR_FROM_ALTO";
    ERROR_CODE["ALTO_SERVER_ERROR"] = "ALTO_SERVER_ERROR";
    ERROR_CODE["ALTO_TRANSACTION_FAILED"] = "ALTO_TRANSACTION_FAILED";
    ERROR_CODE["ALTO_DATA_STORING_FAILED"] = "ALTO_DATA_STORING_FAILED";
    ERROR_CODE["ALTO_DECLINED_NO_REFUND_REQUIRED"] = "ALTO_DECLINED_NO_REFUND_REQUIRED";
    ERROR_CODE["EMPTY_REMITTANCE_CODE"] = "EMPTY_REMITTANCE_CODE";
    ERROR_CODE["EMPTY_TRANSACTION_CODE"] = "EMPTY_TRANSACTION_CODE";
    ERROR_CODE["QRIS_TRANSACTION_NOT_FOUND"] = "QRIS_TRANSACTION_NOT_FOUND";
    ERROR_CODE["QRIS_CHECK_STATUS_FAILED"] = "QRIS_CHECK_STATUS_FAILED";
    ERROR_CODE["QRIS_PAYMENT_CHECK_STATUS_FAILED"] = "QRIS_PAYMENT_CHECK_STATUS_FAILED";
    ERROR_CODE["QRIS_TRANSACTION_TIMEOUT"] = "QRIS_TRANSACTION_TIMEOUT";
    ERROR_CODE["QRIS_NNS_MAPPING_NOT_FOUND"] = "QRIS_NNS_MAPPING_NOT_FOUND";
    ERROR_CODE["QRIS_REFUND_TRANSACTION_FAILED"] = "QRIS_REFUND_TRANSACTION_FAILED";
    ERROR_CODE["BIFAST_REQUEST_TIMEOUT"] = "BIFAST_REQUEST_TIMEOUT";
    ERROR_CODE["ERROR_ENTITLEMENT_NOT_FOUND"] = "ERROR_ENTITLEMENT_NOT_FOUND";
    ERROR_CODE["INACTIVE_ACCOUNT"] = "INACTIVE_ACCOUNT";
    ERROR_CODE["ACCOUNT_NUMBER_NOT_FOUND"] = "ACCOUNT_NUMBER_NOT_FOUND";
    ERROR_CODE["ENTITLEMENT_CONSUMPTION_ERROR"] = "ENTITLEMENT_CONSUMPTION_ERROR";
    ERROR_CODE["LATEST_ENTITLEMENT_RETRIEVAL_ERROR"] = "LATEST_ENTITLEMENT_RETRIEVAL_ERROR";
    ERROR_CODE["FAILED_TO_RETRIEVE_ENTITLEMENT"] = "FAILED_TO_RETRIEVE_ENTITLEMENT";
    ERROR_CODE["FAILED_TO_UPDATE_ENTITLEMENT"] = "FAILED_TO_UPDATE_ENTITLEMENT";
    ERROR_CODE["FAILED_TO_REVERT_ENTITLEMENT"] = "FAILED_TO_REVERT_ENTITLEMENT";
    ERROR_CODE["FAILED_TO_RETRIEVE_ENTITLEMENT_QUOTA_INFO"] = "FAILED_TO_RETRIEVE_ENTITLEMENT_QUOTA_INFO";
    ERROR_CODE["LIMIT_GROUP_CODE_NOT_FOUND_IN_ENTITLEMENT"] = "LIMIT_GROUP_CODE_NOT_FOUND_IN_ENTITLEMENT";
    ERROR_CODE["CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_PREVIEW_ERROR"] = "CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_PREVIEW_ERROR";
    ERROR_CODE["CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_CONSUME_ERROR"] = "CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_CONSUME_ERROR";
    ERROR_CODE["PROCESS_FEE_RULE_INFO_ERROR"] = "PROCESS_FEE_RULE_INFO_ERROR";
    ERROR_CODE["REVERSE_TRANSACTION"] = "REVERSE_TRANSACTION";
    ERROR_CODE["DUPLICATE_EXTERNAL_ID"] = "DUPLICATE_EXTERNAL_ID";
    ERROR_CODE["REJECTED_DUE_UNDEFINED_BENEFICIARY_ACCOUNT_NUMBER"] = "REJECTED_DUE_UNDEFINED_BENEFICIARY_ACCOUNT_NUMBER";
    ERROR_CODE["INVALID_EXTERNAL_ID_LIST"] = "INVALID_EXTERNAL_ID_LIST";
    ERROR_CODE["ATM_WITHDRAWAL_EXECUTOR_MANDATORY_INFO_NOT_FOUND"] = "ATM_WITHDRAWAL_EXECUTOR_MANDATORY_INFO_NOT_FOUND";
    ERROR_CODE["LIMIT_GROUPS_CONF_NOT_FOUND"] = "LIMIT_GROUPS_CONF_NOT_FOUND";
    ERROR_CODE["RECOMMENDED_LIMIT_GROUP_NOT_FOUND_IN_CONFIG"] = "RECOMMENDED_LIMIT_GROUP_NOT_FOUND_IN_CONFIG";
    ERROR_CODE["RECOMMENDED_LIMIT_GROUP_CODE_NOT_EXIST"] = "RECOMMENDED_LIMIT_GROUP_CODE_NOT_EXIST";
    ERROR_CODE["DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT"] = "DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT";
    ERROR_CODE["FAILED_TO_SAVE_UPDATE_TRANSACTION_USAGE"] = "FAILED_TO_SAVE_UPDATE_TRANSACTION_USAGE";
    ERROR_CODE["SUSPECTED_ABUSE"] = "SUSPECTED_ABUSE";
    ERROR_CODE["TRANSACTION_STILL_IN_PROGRESS"] = "TRANSACTION_STILL_IN_PROGRESS";
    ERROR_CODE["TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND"] = "TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND";
    ERROR_CODE["ATM_TRANSFER_EXECUTOR_MANDATORY_INFO_NOT_FOUND"] = "ATM_TRANSFER_EXECUTOR_MANDATORY_INFO_NOT_FOUND";
    ERROR_CODE["ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT"] = "ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT";
    ERROR_CODE["DAILY_USAGE_REPO_FIELD_TO_UPDATE_NOT_FOUND"] = "DAILY_USAGE_REPO_FIELD_TO_UPDATE_NOT_FOUND";
    ERROR_CODE["PAYMENT_SERVICE_CODE_NOT_FOUND"] = "PAYMENT_SERVICE_CODE_NOT_FOUND";
})(ERROR_CODE || (ERROR_CODE = {}));
exports.ERROR_CODE = ERROR_CODE;
// customized joi validation errors
const JoiValidationErrors = {
    required: ERROR_CODE.INVALID_MANDATORY_FIELDS,
    empty: ERROR_CODE.INVALID_MANDATORY_FIELDS,
    invalid: ERROR_CODE.INVALID_MANDATORY_FIELDS
};
exports.JoiValidationErrors = JoiValidationErrors;
const ErrorList = {
    [ERROR_CODE.SOURCE_OR_BENEFICIARY_ACCOUNT_IS_BEING_MIGRATED_TO_SHARIA]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Source account or beneficiary account is being migrated'
    },
    [ERROR_CODE.FAILED_TO_REVERSE]: {
        // this is used to communicate with switching so don't change it
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Failed to reverse'
    },
    [ERROR_CODE.SOURCE_ACCOUNT_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.NOT_FOUND,
        message: 'Source account not found'
    },
    [ERROR_CODE.UNIQUE_FIELD]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Duplicated data input'
    },
    [ERROR_CODE.INVALID_ACCOUNT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Invalid account'
    },
    [ERROR_CODE.INCORRECT_FIELD]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Incorrect field value, data type or length'
    },
    [ERROR_CODE.INVALID_REQUEST]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Invalid request'
    },
    [ERROR_CODE.INVALID_REQUEST_OCT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Invalid request: OCT not supported.'
    },
    [ERROR_CODE.UNEXPECTED_ERROR]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'We caught unexpected error'
    },
    [ERROR_CODE.INVALID_REGISTER_PARAMETER]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Code is not registered yet'
    },
    [ERROR_CODE.INVALID_MANDATORY_FIELDS]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Required field cannot be empty'
    },
    [ERROR_CODE.INVALID_AMOUNT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Invalid amount'
    },
    [ERROR_CODE.INVALID_LIMIT_TRANSACTION_AMOUNT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Transaction amount is limited'
    },
    [ERROR_CODE.NO_RECOMMENDATION_SERVICES]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'No recommendation services'
    },
    [ERROR_CODE.RECOMMENDATION_PAYMENT_SEVICE_TYPE_MISMATCH]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'No recommendation services'
    },
    [ERROR_CODE.RECOMMENDATION_SOURCE_BANK_CODE_CHANNEL_MISMATCH]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'No recommendation services'
    },
    [ERROR_CODE.RECOMMENDATION_BENEFICIARY_BANK_CODE_CHANNEL_MISMATCH]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'No recommendation services'
    },
    [ERROR_CODE.RECOMMENDATION_SOURCE_ACCOUNT_TYPE_MISMATCH]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'No recommendation services'
    },
    [ERROR_CODE.RECOMMENDATION_BENEFICIARY_ACCOUNT_TYPE_MISMATCH]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'No recommendation services'
    },
    [ERROR_CODE.RECOMMENDATION_CIF_MISMATCH]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'No recommendation services'
    },
    [ERROR_CODE.RECOMMENDATION_TRANSACTION_AMOUNT_IS_OUT_OF_RANGE]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'No recommendation services'
    },
    [ERROR_CODE.RECOMMENDATION_OVER_DAILY_LIMIT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'No recommendation services'
    },
    [ERROR_CODE.RECOMMENDATION_OVER_DAILY_LIMIT_RETRY_TOMORROW]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'No recommendation services'
    },
    [ERROR_CODE.RECOMMENDATION_OVER_BILL_PAYMENT_LIMIT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'No recommendation services'
    },
    [ERROR_CODE.RECOMMENDATION_BIFAST_PROXY_UNAVAILABLE]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'No recommendation services'
    },
    [ERROR_CODE.RECOMMENDATION_BIFAST_UNSUPPORTED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'No recommendation services'
    },
    [ERROR_CODE.RECOMMENDATION_STILL_IN_BIFAST_LIMIT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'No recommendation services'
    },
    [ERROR_CODE.RECOMMENDATION_REJECTED_BY_WORKING_DAY_RULES]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'No recommendation services'
    },
    [ERROR_CODE.CAN_NOT_REVERSE_TRANSACTION]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Can not revert transaction'
    },
    [ERROR_CODE.INCONSISTENT_PAYMENT_SERVICE]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Inconsistent payment service code'
    },
    [ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Insufficient available balance'
    },
    [ERROR_CODE.BALANCE_IS_INSUFFICIENT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Insufficient available balance'
    },
    [ERROR_CODE.TRANSACTION_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Transaction failed'
    },
    [ERROR_CODE.TRANSACTION_REJECTED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Transaction rejected'
    },
    [ERROR_CODE.TRANSACTION_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.NOT_FOUND,
        message: 'Could not find Transaction'
    },
    [ERROR_CODE.INVALID_BANK_CODE]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Invalid bank code'
    },
    [ERROR_CODE.BLOCKING_ID_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Blocking amount not found'
    },
    [ERROR_CODE.INVALID_TRANSACTION_STATUS]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Transaction status invalid'
    },
    [ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION_STATUS]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Failed to update transaction status'
    },
    [ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Failed to update transaction info'
    },
    [ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Missing debit transaction code'
    },
    [ERROR_CODE.MISSING_CREDIT_TRANSACTION_CODE]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Missing credit transaction code'
    },
    [ERROR_CODE.INVALID_EXTERNALID]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Invalid_ExternalId'
    },
    [ERROR_CODE.MAXIMUM_COUNTER_REACHED]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Maximum outgoing id counter is reached, cannot generate thirdPartyOutgoingId'
    },
    [ERROR_CODE.UNAUTHORIZED]: {
        statusCode: module_common_1.Http.StatusCode.UNAUTHORIZED,
        message: 'User do not have permission to perform action'
    },
    [ERROR_CODE.INVALID_SIGNATURE]: {
        statusCode: module_common_1.Http.StatusCode.UNAUTHORIZED,
        message: 'Invalid Signature or payload details.'
    },
    [ERROR_CODE.EXECUTION_TYPE_UNDEFINED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Missing transaction execution type (blocking or none blocking)'
    },
    [ERROR_CODE.INVALID_ORDER_ID]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Invalid order id'
    },
    [ERROR_CODE.MISSING_CREDIT_TEMPLATE]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Credit transaction submission template cannot be found'
    },
    [ERROR_CODE.SOURCE_TRANSACTION_CURRENCY_UNDEFINED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Source Transaction Currency is undefined'
    },
    [ERROR_CODE.CURRENCY_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Currency is not found'
    },
    [ERROR_CODE.ERROR_FROM_SWICHING]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Error from switching'
    },
    [ERROR_CODE.DEBIT_INTERCHANGE_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Debit configuration for interchange not found'
    },
    [ERROR_CODE.CREDIT_INTERCHANGE_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Credit configuration for interchange not found'
    },
    [ERROR_CODE.FEE_INTERCHANGE_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Fee configuration for interchange not found'
    },
    [ERROR_CODE.ERROR_FROM_IRIS]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Error from Iris'
    },
    [ERROR_CODE.ADMIN_FEE_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Admin fee not available in bill details'
    },
    [ERROR_CODE.MISSING_INQUIRY_ID]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'External id is not provided'
    },
    [ERROR_CODE.MISSING_FIXED_FEE_CONFIGURATION]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Fixed fee configuration is missing in fee rule'
    },
    [ERROR_CODE.MISSING_PERCENTAGE_FEE_CONFIGURATION]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Percentage fee configuration is missing in fee rule'
    },
    [ERROR_CODE.INVALID_SUBSIDIARY_VALUE]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Invalid subsidiary percentage or amount'
    },
    [ERROR_CODE.REQUIRE_TRANSACTION_DATE]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Require transaction date with incoming external id'
    },
    [ERROR_CODE.MISSING_SUBSIDIARY_RULE]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Subsidiary configuration is missing in fee rule'
    },
    [ERROR_CODE.MISSING_FEE_RULE]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Fixed fee configuration is missing in fee rule'
    },
    [ERROR_CODE.DECISION_ENGINE_CONFIG_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Decision Engine is not found'
    },
    [ERROR_CODE.DECISION_ENGINE_OPERATOR_NOT_SUPPORT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Decision Engine operator is not support'
    },
    [ERROR_CODE.UNSUPPORTED_CONFIRM_TRANSACTION_STATUS]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Unsupported confirm transaction status'
    },
    [ERROR_CODE.INVALID_REFUND_REQUEST]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Invalid refund request'
    },
    [ERROR_CODE.MISSING_CUSTOMER_OR_TRANSACTION_ID]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Either Customer Id or Transaction Id required.'
    },
    [ERROR_CODE.MISSING_START_AND_END_DATE]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Missing Start and End date.'
    },
    [ERROR_CODE.CUSTOMER_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Customer does not exist.'
    },
    [ERROR_CODE.INVALID_DATE_FORMAT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Invalid date format.'
    },
    [ERROR_CODE.BILL_DETAILS_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Bill details not found'
    },
    [ERROR_CODE.INVALID_BANK_CHANNEL]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Invalid transaction bank channel for manual adjustment.'
    },
    [ERROR_CODE.INVALID_MAMBU_BLOCKING_DAY]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Invalid Mambu blocking day.'
    },
    [ERROR_CODE.MAMBU_BLOCKING_COUNTER_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Mambu blocking counter not found.'
    },
    [ERROR_CODE.TRANSACTION_ALREADY_CONFIRMED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Transaction already confirmed'
    },
    [ERROR_CODE.TRANSACTION_ALREADY_REVERSED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Transaction already reversed'
    },
    [ERROR_CODE.PAYROLL_INTERCHANGE_REQUIRED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Interchange for PAYROLL transaction required'
    },
    [ERROR_CODE.CANNOT_GET_CONFIG_PARAMETER]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Cannot get config parameter information'
    },
    [ERROR_CODE.ERROR_FROM_RTGS]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Error from RTGS'
    },
    [ERROR_CODE.ERROR_FROM_SKN]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Error from SKN'
    },
    [ERROR_CODE.ERROR_FROM_BIFAST]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Error from BiFast'
    },
    [ERROR_CODE.NOT_ALLOWED_TO_CREATE_BLOCKING]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'This Account cannot do block amount'
    },
    [ERROR_CODE.SAVING_PRODUCT_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.NOT_FOUND,
        message: 'Saving product is not found'
    },
    [ERROR_CODE.CAN_NOT_CREATE_BLOCK_AMOUNT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Can not create blocking amount'
    },
    [ERROR_CODE.TRANSFER_INTERCHANGE_FEE_RULE_NOT_CONFIGURED]: {
        statusCode: module_common_1.Http.StatusCode.NOT_FOUND,
        message: 'Fee rule does not have result for input interchange'
    },
    [ERROR_CODE.TRANSFER_FEE_RULE_INVALID_MANDATORY_FIELDS]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Fee Rule, Required field cannot be empty'
    },
    [ERROR_CODE.TRANSFER_FEE_RULE_INVALID_REGISTER_PARAMETER]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Fee Rule Code is not registered yet'
    },
    [ERROR_CODE.BLOCK_AMOUNT_IS_INSUFFICIENT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Insufficient Available Balance Amount'
    },
    [ERROR_CODE.MAMBU_TRANSACTION_ALREADY_REVERSED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Mambu Transaction already reversed'
    },
    [ERROR_CODE.MAMBU_TRANSACTION_AUTHORIZATION_HOLD_EXPIRED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Mambu Transaction authorization hold expired'
    },
    [ERROR_CODE.MAMBU_DUPLICATE_CARD_TRANSACTION]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Mambu duplicate card transaction'
    },
    [ERROR_CODE.MAMBU_INVALID_PARAMETERS]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Idempotent request does not match existing request'
    },
    [ERROR_CODE.MAMBU_TRANSACTION_ALREADY_SETTLED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Mambu Transaction already settled'
    },
    [ERROR_CODE.INVALID_TRANSACTION_ID]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Invalid Transaction ID'
    },
    [ERROR_CODE.SOURCE_ACCOUNT_INQUIRY_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Inquiry for source account failed.'
    },
    [ERROR_CODE.BENEFICIARY_ACCOUNT_INQUIRY_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Inquiry for beneficiary account failed.'
    },
    [ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Failed while fetching Payment Config Rules.'
    },
    [ERROR_CODE.PAYMENT_CONFIG_RULES_EVALUATION_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Failed while evaluating Payment Config Rules.'
    },
    [ERROR_CODE.COREBANKING_BLOCK_AMOUNT_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Failed while blocking amount.'
    },
    [ERROR_CODE.THIRDPARTY_SUBMISSION_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Failed while submitting to thirdparty.'
    },
    [ERROR_CODE.COREBANKING_DEBIT_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Failed while debiting amount.'
    },
    [ERROR_CODE.COREBANKING_CREDIT_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Failed while crediting amount.'
    },
    [ERROR_CODE.COREBANKING_FEES_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Failed while executing fees.'
    },
    [ERROR_CODE.COREBANKING_DISBURSEMENT_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Failed while disbursing amount.'
    },
    [ERROR_CODE.COREBANKING_REPAYMENT_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Failed while amount repayment'
    },
    [ERROR_CODE.COREBANKING_GL_JOURNAL_ENTRIES_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Failed while gl journal entries'
    },
    [ERROR_CODE.COREBANKING_PAY_OFF_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Failed while executing pay off transaction'
    },
    [ERROR_CODE.INTERCHANGE_NOT_EXPECTED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Interchange is not expected'
    },
    [ERROR_CODE.TRANSACTION_DATE_NOT_ALLOWED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Transaction date is older than allowed limit.'
    },
    [ERROR_CODE.MONGO_TIMEOUT_ERROR]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Mongo timeout error'
    },
    [ERROR_CODE.ERROR_FROM_TOKOPEDIA]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Error from Tokopedia'
    },
    [ERROR_CODE.TOKOPEDIA_AUTH_TOKEN_ACCESS_ERROR]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Error while fetching TOKOPEDIA access token.'
    },
    [ERROR_CODE.REQUEST_TEMPLATE_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Transaction submission template request cannot be found'
    },
    [ERROR_CODE.ADDITIONAL_PAYLOAD_NOT_EXIST]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Additional Payload data does not exist'
    },
    [ERROR_CODE.INVALID_ADDITIONAL_PAYLOAD]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Invalid additional payload'
    },
    [ERROR_CODE.RDN_REVERSAL_NOT_ALLOWED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'RDN reversal not allowed'
    },
    [ERROR_CODE.ERROR_FROM_RTGS_SHARIA]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Error from RTGS_SHARIA'
    },
    [ERROR_CODE.ERROR_FROM_SKN_SHARIA]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Error from SKN_SHARIA'
    },
    [ERROR_CODE.ERROR_WHILE_FETCHING_COREBANKING_ID]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Error while fetching core banking Ids'
    },
    [ERROR_CODE.ALTO_AUTH_TOKEN_ACCESS_ERROR]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Error while fetching ALTO access token.'
    },
    [ERROR_CODE.ERROR_FROM_ALTO]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Error from Alto'
    },
    [ERROR_CODE.ALTO_SERVER_ERROR]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Alto server error'
    },
    [ERROR_CODE.ALTO_TRANSACTION_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Error while submitting transaction to QRIS / ALTO'
    },
    [ERROR_CODE.ALTO_DATA_STORING_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Failed while updating QRIS data in transaction DB'
    },
    [ERROR_CODE.ALTO_DECLINED_NO_REFUND_REQUIRED]: {
        statusCode: module_common_1.Http.StatusCode.OK,
        message: 'ALTO_DECLINED_NO_REFUND_REQUIRED'
    },
    [ERROR_CODE.EMPTY_REMITTANCE_CODE]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Empty remittance code, cannot generate bifast thirdPartyOutgoingId'
    },
    [ERROR_CODE.EMPTY_TRANSACTION_CODE]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Empty transaction code, cannot generate bifast thirdPartyOutgoingId'
    },
    [ERROR_CODE.QRIS_TRANSACTION_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Transaction for this Customer Reference Number is not found'
    },
    [ERROR_CODE.QRIS_CHECK_STATUS_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Failed when QRIS Checking status'
    },
    [ERROR_CODE.QRIS_PAYMENT_CHECK_STATUS_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Failed when no result from QRIS Checking status'
    },
    [ERROR_CODE.QRIS_TRANSACTION_TIMEOUT]: {
        statusCode: module_common_1.Http.StatusCode.REQUEST_TIMEOUT,
        message: 'Timeout when sending payment to Alto'
    },
    [ERROR_CODE.QRIS_NNS_MAPPING_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'QRIS NNS Mapping Not Found'
    },
    [ERROR_CODE.QRIS_REFUND_TRANSACTION_FAILED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Error calling refund trasaction for QRIS'
    },
    [ERROR_CODE.BIFAST_REQUEST_TIMEOUT]: {
        statusCode: module_common_1.Http.StatusCode.REQUEST_TIMEOUT,
        message: 'Timeout when sending request BIFAST transaction'
    },
    [ERROR_CODE.ERROR_ENTITLEMENT_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.NOT_FOUND,
        message: 'Entitlement code not found'
    },
    [ERROR_CODE.INACTIVE_ACCOUNT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Account is inactive after inquiry process'
    },
    [ERROR_CODE.ACCOUNT_NUMBER_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'AccountNo is not registered'
    },
    [ERROR_CODE.ENTITLEMENT_CONSUMPTION_ERROR]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Entitlement consumption process error'
    },
    [ERROR_CODE.LATEST_ENTITLEMENT_RETRIEVAL_ERROR]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Entitlement code not found'
    },
    [ERROR_CODE.FAILED_TO_RETRIEVE_ENTITLEMENT]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Failed when retrieving entitlement'
    },
    [ERROR_CODE.FAILED_TO_UPDATE_ENTITLEMENT]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Failed when updating entitlement'
    },
    [ERROR_CODE.FAILED_TO_REVERT_ENTITLEMENT]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Failed when reverting entitlement'
    },
    [ERROR_CODE.FAILED_TO_RETRIEVE_ENTITLEMENT_QUOTA_INFO]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Failed when validating whether current used quota exceeded quota'
    },
    [ERROR_CODE.LIMIT_GROUP_CODE_NOT_FOUND_IN_ENTITLEMENT]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Entitlement result does not match the supposedly limit group code'
    },
    [ERROR_CODE.CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_PREVIEW_ERROR]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Customer entitlement recommendation service [PREVIEW] error'
    },
    [ERROR_CODE.CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_CONSUME_ERROR]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Customer entitlement recommendation service [CONSUME] error'
    },
    [ERROR_CODE.PROCESS_FEE_RULE_INFO_ERROR]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Unable to process fee rule info - entitlement'
    },
    [ERROR_CODE.REVERSE_TRANSACTION]: {
        statusCode: module_common_1.Http.StatusCode.OK,
        message: 'REVERSE_TRANSACTION'
    },
    [ERROR_CODE.DUPLICATE_EXTERNAL_ID]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Duplicate externalId - paymentServiceType combination'
    },
    [ERROR_CODE.REJECTED_DUE_UNDEFINED_BENEFICIARY_ACCOUNT_NUMBER]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Transaction rejected due to credit template was not found because of undefined beneficiary account number.'
    },
    [ERROR_CODE.INVALID_EXTERNAL_ID_LIST]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Number of externalIds should be maximum 10 and minimum 1'
    },
    [ERROR_CODE.ATM_WITHDRAWAL_EXECUTOR_MANDATORY_INFO_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Source CustomerId or/and Source Account No not provided or empty'
    },
    [ERROR_CODE.LIMIT_GROUPS_CONF_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'Cannot retrieve any information on limit group setup'
    },
    [ERROR_CODE.RECOMMENDED_LIMIT_GROUP_NOT_FOUND_IN_CONFIG]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: `Cannot retrieve recommended service's limit group information`
    },
    [ERROR_CODE.RECOMMENDED_LIMIT_GROUP_CODE_NOT_EXIST]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: `Recommended service does not provide the required limit group code`
    },
    [ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: `Unable to proceed with money withdrawal; due to daily limit restriction [ATM]`
    },
    [ERROR_CODE.FAILED_TO_SAVE_UPDATE_TRANSACTION_USAGE]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: `Unable to save transaction limit`
    },
    [ERROR_CODE.TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: `Unable to proceed with transaction limit validation as there is no implementation on the validation`
    },
    [ERROR_CODE.SUSPECTED_ABUSE]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Suspected abuse'
    },
    [ERROR_CODE.TRANSACTION_STILL_IN_PROGRESS]: {
        statusCode: module_common_1.Http.StatusCode.CONFLICT,
        message: 'The transaction still in progress'
    },
    [ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: `Unable to proceed with money transfer; due to daily limit restriction [ATM]`
    },
    [ERROR_CODE.ATM_TRANSFER_EXECUTOR_MANDATORY_INFO_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Source CustomerId/Source AccountNo/Beneficiary AccountNo/Beneficiary AccountName not provided or empty'
    },
    [ERROR_CODE.DAILY_USAGE_REPO_FIELD_TO_UPDATE_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: `Transaction Limit's daily limit unable to be persisted`
    },
    [ERROR_CODE.PAYMENT_SERVICE_CODE_NOT_FOUND]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: `Payment Service Code not provided or empty`
    }
};
exports.ErrorList = ErrorList;
const MONGO_ERROR = 'MongoError';
exports.MONGO_ERROR = MONGO_ERROR;
const MONGO_TIMEOUT_ERROR = 'MongoTimeoutError';
exports.MONGO_TIMEOUT_ERROR = MONGO_TIMEOUT_ERROR;
var MONGO_ERROR_CODE;
(function (MONGO_ERROR_CODE) {
    MONGO_ERROR_CODE[MONGO_ERROR_CODE["DUPLICATED_KEY"] = 11000] = "DUPLICATED_KEY";
})(MONGO_ERROR_CODE || (MONGO_ERROR_CODE = {}));
exports.MONGO_ERROR_CODE = MONGO_ERROR_CODE;
const SERVER_ERROR_CODE = 500;
const isRetriableError = (error) => {
    return error.status >= SERVER_ERROR_CODE;
};
exports.isRetriableError = isRetriableError;
//# sourceMappingURL=errors.js.map