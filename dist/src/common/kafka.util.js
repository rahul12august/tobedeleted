"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const v4_1 = __importDefault(require("uuid/v4"));
const constant_1 = require("./constant");
const getHeaders = (key) => ({
    [constant_1.kafkaHeaders.X_IDEMPOTENCY_KEY]: (key !== null && key !== void 0 ? key : v4_1.default())
});
const kafkaUtils = {
    getHeaders
};
exports.default = kafkaUtils;
//# sourceMappingURL=kafka.util.js.map