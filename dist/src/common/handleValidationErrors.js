"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = require("@hapi/joi");
const errors_1 = require("./errors");
const AppError_1 = require("../errors/AppError");
const logger_1 = __importDefault(require("../logger"));
const transaction_enum_1 = require("../transaction/transaction.enum");
const getMappedDetails = (details) => {
    const mappedDetails = details.reduce((acc, detail, index) => {
        if (index !== 0 && detail.path[0] === details[index - 1].path[0]) {
            return acc;
        }
        const constraint = detail.type.split('.').pop() || '';
        const errorCode = errors_1.JoiValidationErrors[constraint] ||
            errors_1.ERROR_CODE.INCORRECT_FIELD;
        const defaultError = errors_1.ErrorList[errorCode];
        acc.push({
            message: defaultError.message,
            code: errorCode,
            key: `${detail.path[0]}`
        });
        return acc;
    }, []);
    return mappedDetails;
};
exports.validateHandler = (payload, schema, options = {}) => {
    const { error, value } = joi_1.validate(payload, schema, options);
    if (error) {
        const mappedDetails = getMappedDetails(error.details);
        logger_1.default.error(`validateHandler: invalid request ${JSON.stringify(mappedDetails)}`);
        throw new AppError_1.TransferAppError(`Invalid request detected in validate handler ${JSON.stringify(mappedDetails)}`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.INVALID_REQUEST, mappedDetails);
    }
    return value;
};
const errorHandler = (_req, res, err) => {
    if (!err) {
        return res.continue;
    }
    if (err.isJoi) {
        const details = err.details;
        const mappedDetails = getMappedDetails(details);
        logger_1.default.error(`errorHandler: invalid request ${JSON.stringify(mappedDetails)}`);
        throw new AppError_1.TransferAppError(`Invalid request detected in error handler ${JSON.stringify(mappedDetails)}`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.INVALID_REQUEST, mappedDetails);
    }
    throw err;
};
exports.default = errorHandler;
//# sourceMappingURL=handleValidationErrors.js.map