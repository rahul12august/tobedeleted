"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importDefault(require("../logger"));
const contextHandler_1 = require("./contextHandler");
class ContextProgressTracker {
    constructor() { }
    initTracker() {
        //set timer to context
        contextHandler_1.setFnProgressTracker(new Date().getTime());
    }
    logProgress(baseMsg) {
        const initTime = contextHandler_1.getFnProgressTracker();
        if (initTime) {
            logger_1.default.debug(`${baseMsg} : ${new Date().getTime() - initTime}ms`);
        }
    }
    static getInstance() {
        if (!ContextProgressTracker.instance) {
            ContextProgressTracker.instance = new ContextProgressTracker();
        }
        return ContextProgressTracker.instance;
    }
}
exports.ContextProgressTracker = ContextProgressTracker;
//# sourceMappingURL=contextProgressTracker.js.map