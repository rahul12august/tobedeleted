"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const punycode_1 = __importDefault(require("punycode"));
const encodeString = (note) => {
    return punycode_1.default.toASCII(note);
};
exports.punycodeUtil = {
    encodeString
};
//# sourceMappingURL=encodeString.util.js.map