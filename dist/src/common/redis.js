"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_redis_1 = __importDefault(require("@dk/module-redis"));
const logger_1 = __importDefault(require("../logger"));
const config_1 = require("../config");
const redisConfig = {
    isLocal: config_1.config.get('redis').isLocal
};
const redis = module_redis_1.default(redisConfig, logger_1.default);
exports.default = redis;
//# sourceMappingURL=redis.js.map