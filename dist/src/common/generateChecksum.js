"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = __importDefault(require("crypto"));
const algorithm = 'sha512';
//secret Key – Key shared between channel and Euronet
//Data String is pipe delimited values
const getSecureSHAHash = (secretKey, dataString) => crypto_1.default
    .createHmac(algorithm, Buffer.from(secretKey, 'utf-8'))
    .update(Buffer.from(dataString, 'utf-8'))
    .digest('hex')
    .toUpperCase();
exports.getSecureSHAHash = getSecureSHAHash;
//# sourceMappingURL=generateChecksum.js.map