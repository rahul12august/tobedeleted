"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importDefault(require("../logger"));
const errors_1 = require("./errors");
const moment_timezone_1 = __importDefault(require("moment-timezone"));
const transaction_constant_1 = require("../transaction/transaction.constant");
const AppError_1 = require("../errors/AppError");
const transaction_enum_1 = require("../transaction/transaction.enum");
exports.toDate = (dateString) => {
    const [year, month, day] = dateString.split('-');
    if (isNaN(Number(year)) || isNaN(Number(month)) || isNaN(Number(day))) {
        const detail = `Invalid date format: ${dateString}!`;
        logger_1.default.error(`toDate: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_DATE_FORMAT);
    }
    return new Date(Number(year), Number(month) - 1, Number(day));
};
exports.isDateWithinYear = (dateInput = new Date()) => {
    const currentDate = moment_timezone_1.default().tz(transaction_constant_1.TMZ);
    const compareDiff = currentDate.diff(moment_timezone_1.default(dateInput).tz(transaction_constant_1.TMZ), `days`);
    return compareDiff <= transaction_constant_1.DAYS_IN_YEAR;
};
exports.to24HourFormat = (dateInput = new Date()) => {
    return moment_timezone_1.default(dateInput)
        .tz(transaction_constant_1.TMZ)
        .format(transaction_constant_1.FORMAT_HOUR_MINUS_24H);
};
exports.toJKTDate = (dateInput = new Date(), dateformat) => {
    return moment_timezone_1.default(dateInput)
        .tz(transaction_constant_1.TMZ)
        .format(dateformat);
};
exports.isCurrentDate = (date) => {
    const currentDate = moment_timezone_1.default().tz(transaction_constant_1.TMZ);
    const transactionDate = moment_timezone_1.default(date).tz(transaction_constant_1.TMZ);
    return (transactionDate.year() === currentDate.year() &&
        transactionDate.month() === currentDate.month() &&
        transactionDate.date() === currentDate.date());
};
//# sourceMappingURL=dateUtils.js.map