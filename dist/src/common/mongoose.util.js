"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const errors_1 = require("./errors");
const AppError_1 = require("../errors/AppError");
const logger_1 = __importDefault(require("../logger"));
const error_util_1 = require("../errors/error.util");
const transaction_enum_1 = require("../transaction/transaction.enum");
exports.handleDuplicationError = (error, _doc, next) => {
    if (error.name === errors_1.MONGO_ERROR &&
        error.code === errors_1.MONGO_ERROR_CODE.DUPLICATED_KEY) {
        const detail = `Trying to save duplicated document in collection!`;
        logger_1.default.error(`handleDuplicationError: ${detail}`);
        next(new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.UNIQUE_FIELD));
    }
    else if ((error.name === errors_1.MONGO_ERROR || error.name === errors_1.MONGO_TIMEOUT_ERROR) &&
        error.message &&
        error.message.includes(errors_1.MONGO_TIMEOUT_ERROR)) {
        const detail = 'Mongo timeout error!';
        logger_1.default.error(`handleDuplicationError: ${detail}`);
        next(new AppError_1.RetryableTransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.MONGO_TIMEOUT_ERROR, true, error_util_1.getErrorDetails(errors_1.ERROR_CODE.MONGO_TIMEOUT_ERROR, error.message, errors_1.ERROR_CODE.MONGO_TIMEOUT_ERROR)));
    }
    else {
        logger_1.default.error(`handleDuplicationError: Mongo error message : ${error.message ||
            error.name}`);
        next();
    }
};
//# sourceMappingURL=mongoose.util.js.map