"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Tracing = {
    TRANSACTION_ID: 'x-request-id',
    TRACER_SESSION: 'TRACER_SESSION',
    RETRY_COUNT: 'RETRY_COUNT',
    TRANSACTION_INFO_TRACKER: 'TRANSACTION_INFO_TRACKER',
    PROGRESS_TRACKER: 'PROGRESS_TRACKER',
    TRANSACTION_USAGE_INFO_TRACKER: 'TRANSACTION_USAGE_INFO_TRACKER'
};
exports.Tracing = Tracing;
const HttpHeaders = {
    AUTH: 'authorization',
    X_IDEMPOTENCY_KEY: 'X-Idempotency-Key',
    CONTENT_TYPE: 'Content-Type',
    ACCEPT: 'Accept',
    SIGNATURE: 'signature'
};
exports.HttpHeaders = HttpHeaders;
const HTTP_HEADERS = {
    AUTH: 'authorization'
};
exports.HTTP_HEADERS = HTTP_HEADERS;
const FEATURE_FLAG = {
    ENABLE_AWARD_V2: 'enableAwardV2',
    ENABLE_NON_BLOCKING_INVERSE_TRANSACTION_ON_REVERSAL: 'enableNonBlockingInverse',
    ENABLE_DAILY_LIMIT_ATM_WITHDRAWAL_POCKET_LIMIT_VALIDATION: 'enableDailyLimitAtmWithdrawalPocketLevelValidation',
    ENABLE_DAILY_LIMIT_ATM_TRANSFER_POCKET_LIMIT_VALIDATION: 'enableDailyLimitAtmTransferPocketLevelValidation',
    LEGACY_BALANCE_CHECK_KEY: 'useLegacyBalanceCheck'
};
exports.FEATURE_FLAG = FEATURE_FLAG;
const PREFIX_TOKEN = 'Bearer ';
exports.PREFIX_TOKEN = PREFIX_TOKEN;
const AUTH_STRATEGY = 'jwt';
exports.AUTH_STRATEGY = AUTH_STRATEGY;
const httpClientTimeoutErrorCode = 'ECONNABORTED';
exports.httpClientTimeoutErrorCode = httpClientTimeoutErrorCode;
exports.AUTH_CREDENTIALS_PAYLOAD = 'auth.credentials.payload';
exports.SENSITIVE_DEBIT_TRANSACTION_CODES = ['PRD01', 'PRD02', 'PRD03'];
exports.SENSITIVE_CREDIT_TRANSACTION_CODES = ['PRC01', 'PRC02', 'PRC03'];
exports.DEFAULT_MASKING_PLACEHOLDER = '*****';
exports.UTC_OFFSET_WESTERN_INDONESIAN = 7;
exports.DEFAULT_DATE_FORMAT = 'YYYY-MM-DD';
exports.BI_FAST_DATE_FORMAT = 'YYMMDD';
const kafkaHeaders = {
    X_IDEMPOTENCY_KEY: 'x-idempotency-key'
};
exports.kafkaHeaders = kafkaHeaders;
exports.retryConfig = {
    networkErrorCodesToRetry: [
        'ENOENT',
        'ECONNRESET',
        'ECONNREFUSED',
        'ECONNABORTED'
    ],
    maxRetriesOnError: 3,
    delay: 1000
};
exports.MICROSERVICE_NAME = 'ms-transfer';
//# sourceMappingURL=constant.js.map