"use strict";
/**
 * @module tokopediaSubmissionTemplate handle outgoing transaction to switching
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const logger_1 = __importStar(require("../logger"));
const tokopedia_repository_1 = __importDefault(require("./tokopedia.repository"));
const tokopedia_helper_1 = require("./tokopedia.helper");
const transaction_util_1 = __importDefault(require("../transaction/transaction.util"));
const transaction_repository_1 = __importDefault(require("../transaction/transaction.repository"));
const transaction_enum_1 = require("../transaction/transaction.enum");
const module_common_2 = require("@dk/module-common");
const contextHandler_1 = require("../common/contextHandler");
const constant_1 = require("../common/constant");
const submitTransaction = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    let status = transaction_enum_1.TransactionStatus.SUBMITTED;
    try {
        const tokopediaTransactionPayload = yield tokopedia_helper_1.mapToTokopediaTransaction(transactionModel);
        const tokopediaTransaction = yield transaction_util_1.default.submitTransactionWithRetry(tokopedia_repository_1.default.submitTransaction, tokopediaTransactionPayload);
        if (tokopediaTransaction.id)
            contextHandler_1.updateRequestIdInRedis(tokopediaTransaction.id);
        return transactionModel;
    }
    catch (error) {
        logger_1.default.error('failed submission to tokopedia after all retry or non-retriable condition', error);
        if (error.code && error.code === constant_1.httpClientTimeoutErrorCode) {
            logger_1.default.info(`Sending pending RTGS blocked amount notification for : ${transactionModel.id}`);
            status = transaction_enum_1.TransactionStatus.SUBMITTED;
            return transactionModel;
        }
        status = transaction_enum_1.TransactionStatus.DECLINED;
        throw error;
    }
    finally {
        yield transaction_repository_1.default.update(transactionModel.id, {
            status: status,
            thirdPartyOutgoingId: transactionModel.externalId
        });
    }
});
const isEligible = (model) => module_common_1.BankChannelEnum.TOKOPEDIA === model.beneficiaryBankCodeChannel;
const shouldSendNotification = () => false;
const getRail = () => module_common_2.Rail.TOKOPEDIA;
const tokopediaSubmissionTemplate = {
    submitTransaction,
    isEligible,
    getRail,
    shouldSendNotification
};
exports.default = logger_1.wrapLogs(tokopediaSubmissionTemplate);
//# sourceMappingURL=tokopediaSubmissionTemplate.service.js.map