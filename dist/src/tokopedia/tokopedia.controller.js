"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
//import { confirmTransactionRequestValidator } from './tokopedia.validator';
const contextHandler_1 = require("../common/contextHandler");
const tokopedia_service_1 = __importDefault(require("./tokopedia.service"));
const tokopedia_encryption_1 = require("./tokopedia.encryption");
const logger_1 = __importDefault(require("../logger"));
const confirmTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/tokopedia/transaction-confirmation',
    options: {
        description: 'Confirm Tokopedia transaction',
        notes: 'Private Api endpoint is called from Tokopedia to confirm transaction',
        tags: ['api', 'transactions'],
        auth: false,
        // validate: {
        //   payload: confirmTransactionRequestValidator
        // },
        payload: {
            parse: false,
            output: 'data'
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { payload } = hapiRequest;
            const jsonPayload = JSON.parse(payload.toString());
            yield contextHandler_1.updateRequestIdInContext(jsonPayload.data.id);
            logger_1.default.debug(`Payload: ${payload.toString()}`);
            logger_1.default.debug(`Signature: ${hapiRequest.headers['signature']}`);
            yield tokopedia_encryption_1.verifySignature(payload.toString(), hapiRequest.headers['signature']);
            yield tokopedia_service_1.default.confirmTransaction(jsonPayload.data);
            return hapiResponse.response().code(module_common_1.Http.StatusCode.NO_CONTENT);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'Ok'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    },
                    [module_common_1.Http.StatusCode.UNAUTHORIZED]: {
                        description: 'UNAUTHORIZED'
                    }
                }
            }
        }
    }
};
const tokopediaController = [confirmTransaction];
exports.default = tokopediaController;
//# sourceMappingURL=tokopedia.controller.js.map