"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const errors_1 = require("../common/errors");
const AppError_1 = require("../errors/AppError");
const logger_1 = __importStar(require("../logger"));
const transaction_enum_1 = require("../transaction/transaction.enum");
const transactionConfirmation_service_1 = __importDefault(require("../transaction/transactionConfirmation.service"));
const tokopedia_enum_1 = require("./tokopedia.enum");
const billDetail_service_1 = __importDefault(require("../billPayment/billDetail/billDetail.service"));
const transaction_repository_1 = __importDefault(require("../transaction/transaction.repository"));
const confirmTransaction = (input) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Tokopedia: Transaction Confirmation with external id: ${input.id} status: ${input.attributes.status}`);
    let responseCode;
    switch (input.attributes.status) {
        case tokopedia_enum_1.ITokopediaPayoutStatusEnum.FAILED:
            responseCode = transaction_enum_1.ConfirmTransactionStatus.ERROR_EXPECTED;
            break;
        case tokopedia_enum_1.ITokopediaPayoutStatusEnum.PENDING:
            responseCode = transaction_enum_1.ConfirmTransactionStatus.REQUEST_TIMEOUT;
            break;
        case tokopedia_enum_1.ITokopediaPayoutStatusEnum.SUCCESS:
            responseCode = transaction_enum_1.ConfirmTransactionStatus.SUCCESS;
            break;
        default:
            const detail = `Unsupported confirm transaction status: ${input.attributes.status}!`;
            logger_1.default.error(`confirmTransaction: ${detail}`);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.TOKOPEDIA, errors_1.ERROR_CODE.UNSUPPORTED_CONFIRM_TRANSACTION_STATUS);
    }
    yield billDetail_service_1.default.updateOrderDetails(input.id, input);
    const transactionInput = {
        externalId: input.id,
        amount: Number(input.attributes.sales_price),
        responseCode,
        responseMessage: input.attributes.error_detail
    };
    const response = yield transactionConfirmation_service_1.default.confirmTransaction(transactionInput);
    const result = yield transaction_repository_1.default.getByExternalId(input.id);
    if (!result) {
        const detail = `Failed to identify transaction by externalId: ${input.id}!`;
        logger_1.default.error(`confirmTransaction: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.TOKOPEDIA, errors_1.ERROR_CODE.INVALID_EXTERNALID);
    }
    return response;
});
const tokopediaService = logger_1.wrapLogs({
    confirmTransaction
});
exports.default = tokopediaService;
//# sourceMappingURL=tokopedia.service.js.map