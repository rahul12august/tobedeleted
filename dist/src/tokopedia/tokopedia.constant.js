"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BASIC_PREFIX = 'Basic';
exports.default = {
    MAX_LENGTH_TRANSACTION_RESPONSE_ID: 12,
    MAX_LENGTH_IRIS_REFERENCE_NO: 40,
    MAX_LENGTH_IRIS_ERROR_MESSAGE: 100,
    MAX_LENGTH_IRIS_ERROR_CODE: 3
};
exports.ORDER_TYPE = 'order';
exports.TokenConstants = {
    TOKEN_ENDPOINT: '/token?grant_type=client_credentials',
    TOKEN_EXPIRY_BUFFER: 600,
    DEFAULT_TTL: 3000
};
exports.REDIS_TOKEN_KEY = 'TOKOPEDIA_TOKEN';
//# sourceMappingURL=tokopedia.constant.js.map