"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const constant_1 = require("../common/constant");
const tokopedia_constant_1 = require("./tokopedia.constant");
const context_1 = require("../context");
const logger_1 = __importDefault(require("../logger"));
const module_httpclient_1 = require("@dk/module-httpclient");
const module_common_1 = require("@dk/module-common");
const constant_2 = require("../common/constant");
const { baseUrl, timeout } = config_1.config.get('tokopedia');
const tokenBaseURL = config_1.config.get('tokopedia').tokenBaseUrl;
const apiKey = process.env.TOKOPEDIA_API_KEY || '';
const httpClient = module_common_1.Http.createHttpClientForward({
    baseURL: baseUrl,
    headers: {
        [constant_1.HttpHeaders.CONTENT_TYPE]: 'application/json',
        [constant_1.HttpHeaders.ACCEPT]: 'application/json',
        [constant_1.HttpHeaders.AUTH]: `${tokopedia_constant_1.BASIC_PREFIX} ${Buffer.from(apiKey).toString('base64')}:`
    },
    timeout: timeout,
    context: context_1.context,
    logger: logger_1.default,
    retryConfig: Object.assign(Object.assign({}, constant_2.retryConfig), { networkErrorCodesToRetry: ['ENOENT', 'ECONNREFUSED'] })
});
const basicToken = process.env.TOKOPEDIA_BASIC_AUTH_TOKEN;
exports.tokenHTTPClient = module_httpclient_1.createHttpClient({
    baseURL: tokenBaseURL,
    headers: {
        Authorization: `Basic ${basicToken}`
    },
    timeout: timeout,
    context: context_1.context,
    logger: logger_1.default
});
exports.default = httpClient;
//# sourceMappingURL=httpClient.js.map