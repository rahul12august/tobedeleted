"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const tokopedia_constant_1 = require("./tokopedia.constant");
const AppError_1 = require("../errors/AppError");
const lodash_1 = require("lodash");
const errors_1 = require("../common/errors");
const transaction_enum_1 = require("../transaction/transaction.enum");
const validateMandatoryFields = (transaction) => {
    let isValid = true;
    if (lodash_1.isEmpty(transaction.additionalInformation1)) {
        logger_1.default.error('validateMandatoryFields: additionalInformation1 should be mandatory for Tokopedia transaction.');
        isValid = false;
    }
    if (lodash_1.isEmpty(transaction.beneficiaryAccountNo)) {
        logger_1.default.error('validateMandatoryFields: beneficiaryAccountNo should be mandatory for Tokopedia transaction.');
        isValid = false;
    }
    if (!isValid) {
        throw new AppError_1.TransferAppError('Validation of mandatory fields failed!', transaction_enum_1.TransferFailureReasonActor.TOKOPEDIA, errors_1.ERROR_CODE.INVALID_MANDATORY_FIELDS);
    }
    return;
};
exports.mapToTokopediaTransaction = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    validateMandatoryFields(transaction);
    const fees = transaction.fees && transaction.fees.length
        ? lodash_1.sumBy(transaction.fees, 'feeAmount')
        : 0;
    return {
        type: tokopedia_constant_1.ORDER_TYPE,
        id: transaction.externalId,
        attributes: {
            /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
            product_code: transaction.additionalInformation1 || '',
            client_number: transaction.beneficiaryAccountNo || '',
            amount: transaction.transactionAmount + fees
        }
    };
});
exports.default = logger_1.wrapLogs(exports.mapToTokopediaTransaction);
//# sourceMappingURL=tokopedia.helper.js.map