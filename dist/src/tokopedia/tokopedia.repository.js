"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const httpClient_1 = __importStar(require("./httpClient"));
const logger_1 = __importStar(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const config_1 = require("../config");
const constant_1 = require("../common/constant");
const tokopedia_constant_1 = require("./tokopedia.constant");
const lodash_1 = require("lodash");
const redis_1 = __importDefault(require("../common/redis"));
const tokopedia_encryption_1 = require("./tokopedia.encryption");
const transaction_enum_1 = require("../transaction/transaction.enum");
const { transactionUrl } = config_1.config.get('tokopedia');
const configurationPrefixKey = 'ms-bill-payment';
const fetchAccessToken = () => __awaiter(void 0, void 0, void 0, function* () {
    const { data: tokenResponse } = yield httpClient_1.tokenHTTPClient.post(`${tokopedia_constant_1.TokenConstants.TOKEN_ENDPOINT}`);
    return tokenResponse;
});
const getToken = () => __awaiter(void 0, void 0, void 0, function* () {
    const errorDetail = 'Tokopedia Get Access Token error';
    try {
        let result = yield redis_1.default.get(tokopedia_constant_1.REDIS_TOKEN_KEY, configurationPrefixKey);
        if (lodash_1.isEmpty(result)) {
            result = yield fetchAccessToken();
            if (!result.access_token || lodash_1.isEmpty(result.access_token)) {
                logger_1.default.error(`${errorDetail}: ${JSON.stringify(result)}`);
                throw new AppError_1.TransferAppError(`${errorDetail}!`, transaction_enum_1.TransferFailureReasonActor.TOKOPEDIA, errors_1.ERROR_CODE.TOKOPEDIA_AUTH_TOKEN_ACCESS_ERROR);
            }
            let tokenExpiry = result.expires_in / 1000 - tokopedia_constant_1.TokenConstants.TOKEN_EXPIRY_BUFFER;
            if (Number.isNaN(tokenExpiry) || tokenExpiry < 0) {
                tokenExpiry = tokopedia_constant_1.TokenConstants.DEFAULT_TTL;
            }
            yield redis_1.default.mSet({
                prefix: configurationPrefixKey,
                keyValueObjectList: [
                    {
                        key: tokopedia_constant_1.REDIS_TOKEN_KEY,
                        value: result,
                        ttl: tokenExpiry,
                        shouldStringifyValue: true
                    }
                ]
            });
        }
        return result.access_token;
    }
    catch (error) {
        logger_1.default.error(`${errorDetail}: ${error && error.message}`);
        if (error instanceof AppError_1.TransferAppError) {
            throw error;
        }
        else {
            throw new AppError_1.TransferAppError(`${errorDetail}!`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.TOKOPEDIA_AUTH_TOKEN_ACCESS_ERROR);
        }
    }
});
const isValidResponse = (result) => !result.data ||
    !result.data.data ||
    (result.data.data.attributes &&
        result.data.data.attributes.error_code &&
        result.data.data.attributes.error_detail);
const submitTransaction = (payload) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c, _d, _e, _f, _g, _h;
    const errorDetail = 'Error from TOKOPEDIA while transferring funds!';
    try {
        logger_1.default.info(`Submitting Tokopedia transaction Id : ${payload.id}, 
        beneficiary_account: ${payload.attributes.client_number},
        skuCode: ${payload.attributes.product_code}`);
        const accessToken = yield getToken();
        const updatedPayload = {
            data: Object.assign({}, payload)
        };
        const signature = yield tokopedia_encryption_1.generateSignature(JSON.stringify(updatedPayload));
        const requestConfig = {
            headers: {
                Authorization: `Bearer ${accessToken}`,
                [constant_1.HttpHeaders.SIGNATURE]: signature
            }
        };
        const result = yield httpClient_1.default.post(transactionUrl, updatedPayload, requestConfig);
        if (isValidResponse(result)) {
            logger_1.default.error(`Tokopedia submitTransaction: ${errorDetail}
          Error Code : ${(_d = (_c = (_b = (_a = result) === null || _a === void 0 ? void 0 : _a.data) === null || _b === void 0 ? void 0 : _b.data) === null || _c === void 0 ? void 0 : _c.attributes) === null || _d === void 0 ? void 0 : _d.error_code},
          Error Details : ${(_h = (_g = (_f = (_e = result) === null || _e === void 0 ? void 0 : _e.data) === null || _f === void 0 ? void 0 : _f.data) === null || _g === void 0 ? void 0 : _g.attributes) === null || _h === void 0 ? void 0 : _h.error_detail},`);
            throw new AppError_1.TransferAppError(errorDetail, transaction_enum_1.TransferFailureReasonActor.TOKOPEDIA, errors_1.ERROR_CODE.ERROR_FROM_TOKOPEDIA);
        }
        logger_1.default.info(`Submit Tokopedia transaction response 
        id : ${result.data.data.id},
        beneficiaryAccount: ${result.data.data.attributes.client_number}`);
        return result.data.data;
    }
    catch (error) {
        logger_1.default.error(`submitTransaction catch: ${errorDetail}`, error && error.message);
        if (error instanceof AppError_1.TransferAppError) {
            throw error;
        }
        else {
            throw new AppError_1.TransferAppError(errorDetail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.ERROR_FROM_TOKOPEDIA);
        }
    }
});
const tokopediaRepository = logger_1.wrapLogs({
    submitTransaction,
    getToken
});
exports.default = tokopediaRepository;
//# sourceMappingURL=tokopedia.repository.js.map