"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ITokopediaPayoutStatusEnum;
(function (ITokopediaPayoutStatusEnum) {
    ITokopediaPayoutStatusEnum["PENDING"] = "Pending";
    ITokopediaPayoutStatusEnum["SUCCESS"] = "Success";
    ITokopediaPayoutStatusEnum["FAILED"] = "Failed";
})(ITokopediaPayoutStatusEnum = exports.ITokopediaPayoutStatusEnum || (exports.ITokopediaPayoutStatusEnum = {}));
//# sourceMappingURL=tokopedia.enum.js.map