"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = __importDefault(require("crypto"));
const logger_1 = __importDefault(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const node_rsa_1 = __importDefault(require("node-rsa"));
const transaction_enum_1 = require("../transaction/transaction.enum");
const privateKey = (process.env.TOKOPEDIA_PRIVATE_KEY &&
    decodeURIComponent(process.env.TOKOPEDIA_PRIVATE_KEY)) ||
    '';
const getPublicKey = () => (process.env.TOKOPEDIA_PUBLIC_KEY &&
    decodeURIComponent(process.env.TOKOPEDIA_PUBLIC_KEY)) ||
    '';
exports.generateSignature = (stringifiedPayload) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Generating Signature for tokopedia`);
    logger_1.default.debug(`Private key value : ${privateKey}`);
    const signature = yield crypto_1.default.sign('sha256', Buffer.from(stringifiedPayload), {
        key: privateKey,
        padding: crypto_1.default.constants.RSA_PKCS1_PSS_PADDING
    });
    const encodedSignature = signature.toString('base64');
    logger_1.default.debug(`Signature value : ${encodedSignature}`);
    return encodedSignature;
});
exports.verifySignature = (payload, signature) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Verifying Signature for tokopedia`);
    const bufferSignature = Buffer.from(signature, 'base64');
    const publicKey = getPublicKey();
    const key = new node_rsa_1.default(publicKey);
    logger_1.default.debug(`key size: ${key.getKeySize()}`);
    const stringifiedPayload = payload;
    const saltLength = key.getKeySize() / 8 -
        2 -
        crypto_1.default
            .createHash('md5')
            .update(stringifiedPayload)
            .digest('hex').length;
    logger_1.default.debug(`saltLength: ${saltLength}`);
    const isVerified = yield crypto_1.default.verify('sha256', Buffer.from(stringifiedPayload), {
        key: publicKey,
        padding: crypto_1.default.constants.RSA_PKCS1_PSS_PADDING,
        saltLength: saltLength
    }, bufferSignature);
    if (!isVerified) {
        const detail = `Invalid Signature or payload details.`;
        logger_1.default.error(detail);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.TOKOPEDIA, errors_1.ERROR_CODE.INVALID_SIGNATURE);
    }
});
//# sourceMappingURL=tokopedia.encryption.js.map