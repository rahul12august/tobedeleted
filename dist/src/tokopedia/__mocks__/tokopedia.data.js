"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const faker_1 = __importDefault(require("faker"));
const tokopedia_enum_1 = require("../tokopedia.enum");
const transaction_data_1 = require("../../transaction/__mocks__/transaction.data");
const module_common_1 = require("@dk/module-common");
const tokopedia_constant_1 = require("../tokopedia.constant");
const newTransactionModel = transaction_data_1.getTransactionModelFullData();
exports.getResponseFields = () => [
    {
        name: 'Optional',
        value: 'Optional'
    }
];
exports.validTransactionModel = Object.assign({ id: faker_1.default.random.uuid() }, newTransactionModel);
exports.inValidTransactionModel = Object.assign(Object.assign({}, newTransactionModel), { id: faker_1.default.random.uuid(), additionalInformation1: undefined, beneficiaryAccountNo: undefined });
const getResponseAttributes = () => ({
    /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
    client_number: '2121212',
    product_code: 'pln-postpaid',
    error_code: undefined,
    error_detail: undefined,
    fulfilled_at: '2020-12-09T10:48:45.000Z',
    partner_fee: 500,
    sales_price: 12500,
    status: tokopedia_enum_1.ITokopediaPayoutStatusEnum.PENDING,
    fields: exports.getResponseFields()
});
exports.errorResponse = () => ({
    type: tokopedia_constant_1.ORDER_TYPE,
    id: faker_1.default.random.uuid(),
    attributes: Object.assign(Object.assign({}, getResponseAttributes()), { 
        /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
        error_code: 'mock_error_code', error_detail: 'mock_error_detail' })
});
exports.getErrorResponseAttributes = () => ({
    /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
    client_number: '2121212',
    product_code: 'pln-postpaid',
    error_code: 'S02',
    error_detail: 'Product is not available',
    fulfilled_at: null,
    partner_fee: 500,
    sales_price: 12500,
    status: tokopedia_enum_1.ITokopediaPayoutStatusEnum.PENDING,
    fields: exports.getResponseFields()
});
exports.getTokopediaTransactionData = () => ({
    type: tokopedia_constant_1.ORDER_TYPE,
    id: faker_1.default.random.uuid(),
    attributes: getResponseAttributes()
});
exports.getTokopediaErrorTransactionData = () => ({
    type: tokopedia_constant_1.ORDER_TYPE,
    id: faker_1.default.random.uuid(),
    attributes: exports.getErrorResponseAttributes()
});
const getRequestAttributes = () => ({
    /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
    client_number: '2121212',
    product_code: 'pln-postpaid',
    amount: 12500
});
exports.getTokopediaTransactionPayload = () => ({
    id: faker_1.default.random.uuid(),
    type: tokopedia_constant_1.ORDER_TYPE,
    attributes: getRequestAttributes()
});
exports.transactionModel = Object.assign(Object.assign({}, transaction_data_1.getTransactionDetail()), { beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.TOKOPEDIA, beneficiaryAccountNo: '2121212', beneficiaryAccountName: 'beneficiary_name', beneficiaryBankCode: 'beneficiary_bank', beneficiaryIrisCode: 'beneficiary_bank', note: 'notes', transactionAmount: 12500, additionalInformation1: 'pln-postpaid' });
//# sourceMappingURL=tokopedia.data.js.map