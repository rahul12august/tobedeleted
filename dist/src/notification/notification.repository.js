"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const form_data_1 = __importDefault(require("form-data"));
const httpClient_1 = __importDefault(require("./httpClient"));
const logger_1 = require("../logger");
const sendEmailReport = (sendEmailPayload) => __awaiter(void 0, void 0, void 0, function* () {
    const form = new form_data_1.default();
    for (const [key, value] of Object.entries(sendEmailPayload)) {
        form.append(key, value);
    }
    yield httpClient_1.default.post(`/send-email-report`, form, {
        headers: form.getHeaders()
    });
});
const notificationRepository = {
    sendEmailReport
};
exports.default = logger_1.wrapLogs(notificationRepository);
//# sourceMappingURL=notification.repository.js.map