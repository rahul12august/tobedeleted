"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const billPayment_constant_1 = require("../billPayment/billPayment.constant");
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const mambuBlockingConfig_constant_1 = require("./mambuBlockingConfig.constant");
const lodash_1 = require("lodash");
const moment_1 = __importDefault(require("moment"));
const addDays = (date, days) => {
    const result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
};
const isDateBetween = (date, fromDate, toDate) => {
    const day = moment_1.default(date).format('YYYY-MM-DD');
    const fromDay = moment_1.default(fromDate).format('YYYY-MM-DD');
    const toDay = moment_1.default(toDate).format('YYYY-MM-DD');
    return moment_1.default(day).isBetween(fromDay, toDay, undefined, '[]');
};
const getHolidayFromDate = (date, holidayList) => {
    for (let holiday of holidayList) {
        if (isDateBetween(date, holiday.fromDate, holiday.toDate)) {
            return holiday;
        }
    }
    return;
};
exports.getMambuBlockingDay = (currentDate) => __awaiter(void 0, void 0, void 0, function* () {
    let lastDate = currentDate;
    const holidayList = yield configuration_repository_1.default.getHolidaysByDateRange(currentDate, addDays(lastDate, billPayment_constant_1.HOLIDAY_DATE_RANGE));
    if (!lodash_1.isEmpty(holidayList)) {
        let workingDays = 0;
        let holiday;
        while (workingDays < mambuBlockingConfig_constant_1.MINIMUM_MAMBU_BLOCKING_DAYS) {
            lastDate = addDays(holiday ? holiday.toDate : lastDate, 1);
            holiday = getHolidayFromDate(lastDate, holidayList);
            if (lodash_1.isUndefined(holiday)) {
                workingDays++;
            }
        }
        return moment_1.default(lastDate).diff(moment_1.default(currentDate), 'days');
    }
    return mambuBlockingConfig_constant_1.MINIMUM_MAMBU_BLOCKING_DAYS;
});
//# sourceMappingURL=mambuBlockingConfig.utils.js.map