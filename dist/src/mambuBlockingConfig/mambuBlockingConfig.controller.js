"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const mambuBlockingConfig_service_1 = __importDefault(require("./mambuBlockingConfig.service"));
const executeAutoUpdateMambuBlockingDays = {
    method: module_common_1.Http.Method.PUT,
    path: '/private/jobs-execution/update-mambu-blocking-days',
    options: {
        description: 'Job Execution mambu blocking days',
        notes: 'Private Api to Auto update mambu blocking days',
        tags: ['api', 'mambu', 'jobs', 'private'],
        auth: false,
        handler: (_, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            mambuBlockingConfig_service_1.default.updateMambuBlockingDays(new Date());
            return hapiResponse.response().code(module_common_1.Http.StatusCode.NO_CONTENT);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.NO_CONTENT]: {
                        description: 'No Content'
                    }
                }
            }
        }
    }
};
const mambuBlockingConfigController = [
    executeAutoUpdateMambuBlockingDays
];
exports.default = mambuBlockingConfigController;
//# sourceMappingURL=mambuBlockingConfig.controller.js.map