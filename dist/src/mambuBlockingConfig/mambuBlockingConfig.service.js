"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const AppError_1 = require("../errors/AppError");
const logger_1 = __importStar(require("../logger"));
const mambuBlockingConfig_utils_1 = require("./mambuBlockingConfig.utils");
const mambuBlockingConfig_constant_1 = require("./mambuBlockingConfig.constant");
const transaction_enum_1 = require("../transaction/transaction.enum");
const errors_1 = require("../common/errors");
const updateMambuBlockingDays = (date) => __awaiter(void 0, void 0, void 0, function* () {
    const noOfDays = yield mambuBlockingConfig_utils_1.getMambuBlockingDay(date);
    const mambuBlockingDays = yield configuration_repository_1.default.getMambuBlockingCodeByDays(noOfDays);
    if (!mambuBlockingDays) {
        const detail = `Mambu blocking days code for number of days: ${noOfDays} not found!`;
        logger_1.default.error(`updateMambuBlockingConfig: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.JAGO, errors_1.ERROR_CODE.INVALID_MAMBU_BLOCKING_DAY);
    }
    logger_1.default.info(`updateMambuBlockingConfig: Updating counter code ${mambuBlockingConfig_constant_1.mambuBlockingCounterCode} with value: ${mambuBlockingDays.code}`);
    configuration_repository_1.default.updateCounter({
        code: mambuBlockingConfig_constant_1.mambuBlockingCounterCode,
        value: parseInt(mambuBlockingDays.code)
    });
});
const mambuBlockingConfigService = logger_1.wrapLogs({
    updateMambuBlockingDays
});
exports.default = mambuBlockingConfigService;
//# sourceMappingURL=mambuBlockingConfig.service.js.map