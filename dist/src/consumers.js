"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("./config");
const kafkaConsumer_utils_1 = require("./common/kafkaConsumer.utils");
const transaction_consumer_1 = require("./transaction/transaction.consumer");
const module_message_1 = require("@dk/module-message");
const { groups } = config_1.config.get('kafka');
const isLocal = config_1.config.get('kafka').isLocal;
const useLocalRedis = config_1.config.get('redis').isLocal;
const kafkaTopics = [
    {
        group: groups.directTransactionsBillPayment,
        topic: module_message_1.TransactionTopicConstant.TRANSFER_DIRECT_TRANSACTION_BILL_PAYMENT
    },
    {
        group: groups.directTransactionsIris,
        topic: module_message_1.TransactionTopicConstant.TRANSFER_DIRECT_TRANSACTION_IRIS
    },
    {
        group: groups.directTransactionsSkn,
        topic: module_message_1.TransactionTopicConstant.TRANSFER_DIRECT_TRANSACTION_SKN
    },
    {
        group: groups.directTransactionsRtgs,
        topic: module_message_1.TransactionTopicConstant.TRANSFER_DIRECT_TRANSACTION_RTGS
    },
    {
        group: groups.directTransactionsEn,
        topic: module_message_1.TransactionTopicConstant.TRANSFER_DIRECT_TRANSACTION_EN
    },
    {
        group: groups.directTransactionsBifast,
        topic: module_message_1.TransactionTopicConstant.TRANSFER_DIRECT_TRANSACTION_BIFAST
    }
];
const kafkaBulkTransactionTopics = [
    {
        group: groups.bulkTransaction,
        topic: module_message_1.TransactionTopicConstant.TRANSFER_BULK_TRANSACTION
    }
];
const init = (isBulkTransactionOnly = false) => __awaiter(void 0, void 0, void 0, function* () {
    let consumers = [];
    let kafkaTopicList = [];
    if (isBulkTransactionOnly) {
        kafkaTopicList = kafkaBulkTransactionTopics;
    }
    else {
        kafkaTopicList = kafkaTopics;
        consumers.push(kafkaConsumer_utils_1.initConsumer({
            groupId: groups.transferTransaction,
            topic: module_message_1.TransactionTopicConstant.SUBMIT_TRANSACTION,
            handler: transaction_consumer_1.autoTransferTransactionHandler,
            consumerRunConfig: {
                partitionsConsumedConcurrently: config_1.config.get('partitionsConsumedConcurrently').autoTransferTransaction,
                isLocal
            },
            consumerInitConfig: {
                maxBytes: config_1.config.get('autoTransferTransactionTopicMaxByte'),
                isLocal,
                useLocalRedis
            }
        }));
        consumers.push(kafkaConsumer_utils_1.initConsumer({
            groupId: groups.directTransaction,
            topic: module_message_1.TransactionTopicConstant.TRANSFER_DIRECT_TRANSACTION,
            handler: transaction_consumer_1.directTransactionHandler,
            consumerRunConfig: {
                partitionsConsumedConcurrently: config_1.config.get('partitionsConsumedConcurrently').directTransaction,
                isLocal
            },
            consumerInitConfig: {
                maxBytes: config_1.config.get('directTransactionTopicMaxByte'),
                isLocal,
                useLocalRedis
            }
        }));
    }
    for (const kafkaTopic of kafkaTopicList) {
        consumers.push(kafkaConsumer_utils_1.initConsumer({
            groupId: kafkaTopic.group,
            topic: kafkaTopic.topic,
            handler: transaction_consumer_1.directTransactionHandler,
            isDqlNotRequired: true,
            consumerRunConfig: {
                isLocal
            },
            consumerInitConfig: {
                isLocal,
                useLocalRedis
            }
        }));
    }
    return Promise.all(consumers);
});
exports.default = {
    init
};
//# sourceMappingURL=consumers.js.map