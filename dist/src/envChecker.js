"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importDefault(require("./logger"));
const constant_1 = require("./constant");
const init = () => {
    const missingRequiredEnv = constant_1.requiredEnvVarNames.filter(envVar => {
        const missing = !process.env[envVar];
        if (missing) {
            logger_1.default.error(`missing required environment variable: ${envVar}`);
        }
        return missing;
    });
    if (missingRequiredEnv.length) {
        process.exit(0);
    }
};
const envChecker = {
    init
};
exports.default = envChecker;
//# sourceMappingURL=envChecker.js.map