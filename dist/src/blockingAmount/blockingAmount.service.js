"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const errors_1 = require("../common/errors");
const configuration_service_1 = __importDefault(require("../configuration/configuration.service"));
const AppError_1 = require("../errors/AppError");
const account_repository_1 = __importDefault(require("../account/account.repository"));
const v4_1 = __importDefault(require("uuid/v4"));
const logger_1 = __importStar(require("../logger"));
const block_service_1 = __importDefault(require("../coreBanking/block.service"));
const error_util_1 = require("../errors/error.util");
const blockingAmount_enum_1 = require("./blockingAmount.enum");
const deposit_repository_1 = __importDefault(require("../coreBanking/deposit.repository"));
const transaction_enum_1 = require("../transaction/transaction.enum");
const createBlockingAmountInCoreBanking = (account, blockingInfo, cardReferenceToken) => __awaiter(void 0, void 0, void 0, function* () {
    const inputBlockingInfo = {
        externalReferenceId: blockingInfo.id,
        amount: blockingInfo.amount,
        advice: false,
        currencyCode: account.currency
    };
    if (blockingInfo.merchantCategoryCode) {
        inputBlockingInfo.cardAcceptor = {
            mcc: blockingInfo.merchantCategoryCode
        };
        logger_1.default.info(`createBlockingAmountInCoreBanking: Mambu blocking code: ${inputBlockingInfo.cardAcceptor.mcc}`);
    }
    else {
        logger_1.default.info(`createBlockingAmountInCoreBanking: Mambu blocking code will set as default, merchantCategoryCode not provided.`);
    }
    try {
        yield block_service_1.default.createBlockingAmount(cardReferenceToken, inputBlockingInfo);
        blockingInfo.idempotencyKey = inputBlockingInfo.idempotencyKey;
    }
    catch (error) {
        let detail, actor;
        if (error instanceof AppError_1.TransferAppError) {
            detail = error.detail;
            actor = error.actor;
        }
        else {
            detail = error.message;
            actor = transaction_enum_1.TransferFailureReasonActor.UNKNOWN;
        }
        throw new AppError_1.RetryableTransferAppError(detail, actor, errors_1.ERROR_CODE.COREBANKING_BLOCK_AMOUNT_FAILED, false, error_util_1.getErrorDetails(errors_1.ERROR_CODE.CAN_NOT_CREATE_BLOCK_AMOUNT, blockingAmount_enum_1.FieldsEnum.BLOCKING_ID, blockingInfo.id));
    }
});
const validateAccountType = (accountType) => __awaiter(void 0, void 0, void 0, function* () {
    const savingProduct = yield configuration_service_1.default.getSavingProduct(accountType);
    if (!savingProduct.blockingCapability) {
        throw new AppError_1.RetryableTransferAppError(`Missing blocking capability from saving product: ${savingProduct.code}!`, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.NOT_ALLOWED_TO_CREATE_BLOCKING, false);
    }
});
const createBlockingAmount = (accountNumber, blockingAmountInput) => __awaiter(void 0, void 0, void 0, function* () {
    const account = yield account_repository_1.default.getActivatedAccount(accountNumber);
    if (!account.cardId) {
        throw new AppError_1.RetryableTransferAppError(`Not allowed to create blocking amount without card ID on account: ${account.id}!`, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.NOT_ALLOWED_TO_CREATE_BLOCKING, false);
    }
    yield validateAccountType(account.accountType);
    const blockingId = v4_1.default();
    const blockingAmountModel = Object.assign(Object.assign({}, blockingAmountInput), { 
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        accountId: account.id, status: blockingAmount_enum_1.BlockingAmountStatusEnum.ACTIVE, id: blockingId });
    if (blockingAmountInput.mambuBlockingCode) {
        blockingAmountModel.merchantCategoryCode =
            blockingAmountInput.mambuBlockingCode;
    }
    yield createBlockingAmountInCoreBanking(account, blockingAmountModel, account.cardId);
    return Object.assign(Object.assign({}, blockingAmountModel), { cardId: account.cardId });
});
const validateBlockingAmountBalance = (blockingAmountInput, accountNumber) => __awaiter(void 0, void 0, void 0, function* () {
    const depositBalance = yield deposit_repository_1.default.getAvailableBalance(accountNumber);
    if (blockingAmountInput > depositBalance) {
        throw new AppError_1.TransferAppError('Deposit balance is less then blocking amount!', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.BLOCK_AMOUNT_IS_INSUFFICIENT);
    }
});
const updateBlockingAmount = (accountNumber, blockId, newBlockingAmount, oldBlockedAmount, cardId) => __awaiter(void 0, void 0, void 0, function* () {
    yield validateBlockingAmountBalance(newBlockingAmount, accountNumber);
    logger_1.default.info(`updateBlockingAmount: Adjusted blocking amount with accountNumber: ${accountNumber} blockId: ${blockId} and cardId: ${cardId}`);
    const amountDiff = newBlockingAmount - oldBlockedAmount;
    if (amountDiff > 0) {
        yield block_service_1.default.increaseAuthorizationHold(cardId, blockId, amountDiff);
    }
    else if (amountDiff < 0) {
        yield block_service_1.default.decreaseAuthorizationHold(cardId, blockId, -amountDiff, false);
    }
});
const blockingAmountService = {
    createBlockingAmount,
    updateBlockingAmount
};
exports.default = logger_1.wrapLogs(blockingAmountService);
//# sourceMappingURL=blockingAmount.service.js.map