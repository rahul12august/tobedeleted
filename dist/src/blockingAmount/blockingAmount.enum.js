"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FieldsEnum;
(function (FieldsEnum) {
    FieldsEnum["EXTERNAL_ID"] = "externalId";
    FieldsEnum["BLOCKING_ID"] = "blockingId";
})(FieldsEnum = exports.FieldsEnum || (exports.FieldsEnum = {}));
var BlockingAmountStatusEnum;
(function (BlockingAmountStatusEnum) {
    BlockingAmountStatusEnum["ACTIVE"] = "ACTIVE";
    BlockingAmountStatusEnum["INACTIVE"] = "INACTIVE";
})(BlockingAmountStatusEnum = exports.BlockingAmountStatusEnum || (exports.BlockingAmountStatusEnum = {}));
//# sourceMappingURL=blockingAmount.enum.js.map