"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mambu_error_constant_1 = __importDefault(require("./mambu.error.constant"));
const lodash_1 = __importDefault(require("lodash"));
const logger_1 = __importDefault(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const transaction_enum_1 = require("../transaction/transaction.enum");
const errorResponseAsMambuResponseError = (error, checker) => error.error.errors && checker(error.error.errors);
const isItExpiredAuthorizationHoldErrorResponse = (error) => errorResponseAsMambuResponseError(error, errors => errors.some(errorDetail => lodash_1.default.isEqual(errorDetail, mambu_error_constant_1.default.expiredAuthorizationHold)));
const isItReversedAuthorizationHoldErrorResponse = (error) => errorResponseAsMambuResponseError(error, errors => errors.some(errorDetail => lodash_1.default.isEqual(errorDetail, mambu_error_constant_1.default.reversedAuthorizationHold)));
const isItSettledAuthorizationHoldErrorResponse = (error) => errorResponseAsMambuResponseError(error, errors => errors.some(errorDetail => lodash_1.default.isEqual(errorDetail, mambu_error_constant_1.default.settledAuthorizationHold)));
const isItDuplicateCardTransactionErrorResponse = (error) => errorResponseAsMambuResponseError(error, errors => errors.some(errorDetail => lodash_1.default.isEqual(errorDetail, mambu_error_constant_1.default.duplicateCardTransaction)));
const isItInvalidParametersErrorResponse = (error) => errorResponseAsMambuResponseError(error, errors => errors.some(errorDetail => lodash_1.default.isEqual(errorDetail, mambu_error_constant_1.default.invalidParameters)));
const checkErrorReverseAuthorizationHold = (error) => {
    if (isItExpiredAuthorizationHoldErrorResponse(error)) {
        const detail = 'Failed reverse authorization hold because its expired';
        logger_1.default.warn(detail);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.MAMBU_TRANSACTION_AUTHORIZATION_HOLD_EXPIRED);
    }
    if (isItReversedAuthorizationHoldErrorResponse(error)) {
        const detail = 'Failed reverse authorization hold because its already reversed';
        logger_1.default.warn(detail);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.MAMBU_TRANSACTION_ALREADY_REVERSED);
    }
    if (isItSettledAuthorizationHoldErrorResponse(error)) {
        const detail = 'Failed settle authorization hold because its already settled';
        logger_1.default.warn(detail);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.MAMBU_TRANSACTION_ALREADY_SETTLED);
    }
    if (isItDuplicateCardTransactionErrorResponse(error)) {
        const detail = 'Duplicate card transaction found';
        logger_1.default.warn(detail);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.MAMBU_DUPLICATE_CARD_TRANSACTION);
    }
    if (isItInvalidParametersErrorResponse(error)) {
        const detail = 'Idempotent request does not match existing request';
        logger_1.default.warn(detail);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.MAMBU_INVALID_PARAMETERS);
    }
    throw error;
};
/**
 * Checks the argument error and tries to asses if the argument is a valid error Mambu
 * response according to the published specs (https://api.mambu.com/#tocserrorresponse)
 * In case the error is conforming with the specified model and if the error can be
 * properly mapped to a resonable TransferAppError, such an error will be thrown.
 * Properly mapping means identifying a valid correspondence between the error code in Mambu
 * and the local model such as actor and cause. In some cases, such as negative balance the
 * mapping is straight forward.
 *
 * In case of model mismatch or of inability to identify a proper correspondende no error
 * is thrown.
 * @param e The original error surfaced during http communication
 * @param trxInput The original transaction request input
 */
const tryThrowingProperTransferAppError = (e, trxInput) => {
    if (e.error &&
        errorResponseAsMambuResponseError(e, errors => errors.some(errorInstance => lodash_1.default.isEqual(errorInstance.errorReason, mambu_error_constant_1.default.BALANCE_BELOW_ZERO)))) {
        logger_1.default.info(`Got a ${mambu_error_constant_1.default.BALANCE_BELOW_ZERO} error for transaction amount ${trxInput.amount}`);
        throw new AppError_1.TransferAppError('Negative balance', transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT);
    }
};
const mambuErrorUtil = {
    checkErrorReverseAuthorizationHold,
    tryThrowingProperTransferAppError
};
exports.default = mambuErrorUtil;
//# sourceMappingURL=mambu.error.util.js.map