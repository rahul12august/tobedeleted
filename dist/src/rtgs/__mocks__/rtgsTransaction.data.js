"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_util_1 = require("../../transaction/transaction.util");
const module_common_1 = require("@dk/module-common");
exports.getMockedTransaction = () => ({
    trn: 'IFT00000',
    externalId: 'externalId',
    transactionDate: transaction_util_1.formatTransactionDate(new Date()),
    transactionAmount: 150000000,
    beneficiaryBankCode: 'BC005',
    beneficiaryAccountName: 'Test123',
    beneficiaryAccountNo: '1234567890',
    sourceBankCode: 'BC002',
    sourceAccountNo: '11110000',
    sourceAccountName: 'Test321',
    notes: 'Test Transaction',
    currency: 'IDR',
    interchange: module_common_1.BankNetworkEnum.RTGS
});
//# sourceMappingURL=rtgsTransaction.data.js.map