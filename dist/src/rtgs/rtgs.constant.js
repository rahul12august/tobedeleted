"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RTGS = 'RTGS';
exports.RDN_KSEI = 'RDN_KSEI';
exports.BRANCH_RTGS = 'BRANCH_RTGS';
exports.RDN_KSEI_TRN = 'IFTSX001';
exports.RTGS_TRN = 'IFT00000';
exports.EXCHANGE_RATE = '0.00';
exports.KSEI = 'KSEI';
exports.RTGS_FOR_BUSINESS = 'RTGS_FOR_BUSINESS';
exports.RDN_WITHDRAW_RTGS = 'RDN_WITHDRAW_RTGS';
exports.RTGS_SHARIA = 'RTGS_SHARIA';
//# sourceMappingURL=rtgs.constant.js.map