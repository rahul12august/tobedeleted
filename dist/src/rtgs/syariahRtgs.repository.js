"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const syariahRtgsHttpClient_1 = __importDefault(require("./syariahRtgsHttpClient"));
const logger_1 = __importStar(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const v4_1 = __importDefault(require("uuid/v4"));
const contextHandler_1 = require("../common/contextHandler");
const config_1 = require("../config");
const constant_1 = require("../common/constant");
const transaction_enum_1 = require("../transaction/transaction.enum");
const { transactionUrl } = config_1.config.get('rtgs');
const submitTransaction = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const externalKey = transaction.externalId || v4_1.default();
        contextHandler_1.updateRequestIdInRedis(externalKey);
        logger_1.default.info(`Submitting RTGS Syariah transaction external id : ${transaction.externalId}, beneficiaryBankCode: ${transaction.beneficiaryBankCode}, 
         beneficiaryAccountNo: ${transaction.beneficiaryAccountNo},
         sourceAccountNo: ${transaction.sourceAccountNo}`);
        const result = yield syariahRtgsHttpClient_1.default.post(transactionUrl, transaction);
        if (!result.data && result.status !== 200) {
            const detail = `Error from RTGS Syariah while transferring funds with status: ${result.status}!`;
            logger_1.default.error(`RTGS Syariah submitTransaction: ${detail}`);
            throw new AppError_1.TransferAppError(detail, result.status === 400 || result.status === 404
                ? transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER
                : transaction_enum_1.TransferFailureReasonActor.RTGS_SYARIAH, errors_1.ERROR_CODE.ERROR_FROM_RTGS_SHARIA);
        }
        return;
    }
    catch (error) {
        const detail = `Error from RTGS Syariah while transferring funds, ${error.code}, message: ${error.message}`;
        logger_1.default.error(`RTGS Syariah submitTransaction catch: ${detail}`);
        // only throw error if it's a timeout error
        if (error.code && error.code === constant_1.httpClientTimeoutErrorCode) {
            throw error;
        }
        if (error instanceof AppError_1.TransferAppError)
            throw error;
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.ERROR_FROM_RTGS_SHARIA);
    }
});
const syariahRtgsRepository = logger_1.wrapLogs({
    submitTransaction
});
exports.default = syariahRtgsRepository;
//# sourceMappingURL=syariahRtgs.repository.js.map