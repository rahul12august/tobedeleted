"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importDefault(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const transaction_enum_1 = require("../transaction/transaction.enum");
exports.validateMandatoryFields = (transaction) => {
    let isValid = true;
    let details = [];
    const description = `should be mandatory for paymentServiceType: ${transaction.paymentServiceType}, paymentServiceCode: ${transaction.paymentServiceCode} transaction.`;
    if (!transaction.beneficiaryAccountName) {
        let message = `BeneficiaryAccountName ${description}`;
        logger_1.default.error(`validateMandatoryFields: ${message}`);
        details.push(message);
        isValid = false;
    }
    if (!transaction.beneficiaryAccountNo) {
        let message = `BeneficiaryAccountNo ${description}`;
        logger_1.default.error(`validateMandatoryFields: ${message}`);
        details.push(message);
        isValid = false;
    }
    if (!transaction.beneficiaryBankCode) {
        let message = `BeneficiaryBankCode ${description}`;
        logger_1.default.error(`validateMandatoryFields: ${message}`);
        details.push(message);
        isValid = false;
    }
    if (!transaction.sourceBankCode) {
        let message = `SourceBankCode ${description}`;
        logger_1.default.error(`validateMandatoryFields: ${message}`);
        details.push(message);
        isValid = false;
    }
    if (!transaction.sourceAccountNo) {
        let message = `SourceAccountNo ${description}`;
        logger_1.default.error(`validateMandatoryFields: ${message}`);
        details.push(message);
        isValid = false;
    }
    if (!transaction.sourceAccountName) {
        let message = `SourceAccountName ${description}`;
        logger_1.default.error(`validateMandatoryFields: ${message}`);
        details.push(message);
        isValid = false;
    }
    if (!isValid) {
        throw new AppError_1.TransferAppError(details.join('\n'), transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_MANDATORY_FIELDS);
    }
    return;
};
//# sourceMappingURL=rtgs.helper.js.map