"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_util_1 = require("../transaction/transaction.util");
const module_common_1 = require("@dk/module-common");
const logger_1 = __importDefault(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const rtgs_constant_1 = require("./rtgs.constant");
const transaction_constant_1 = require("../transaction/transaction.constant");
const transaction_enum_1 = require("../transaction/transaction.enum");
const rdnKseiRequestTemplate = {
    getPayload: (transaction) => {
        if (transaction.beneficiaryBankCode !== transaction_constant_1.BANK_KSEI) {
            const detail = `Invalid Beneficiary BankCode ${transaction.beneficiaryBankCode} with 
        paymentServiceType:${transaction.paymentServiceType}, paymentServiceCode: ${transaction.paymentServiceCode}`;
            logger_1.default.error(`rdnKseiRequestTemplate: ${detail}`);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_BANK_CODE);
        }
        return {
            trn: rtgs_constant_1.RDN_KSEI_TRN,
            externalId: rtgs_constant_1.KSEI + transaction.thirdPartyOutgoingId || '',
            transactionDate: transaction_util_1.formatTransactionDate(transaction.createdAt || new Date()),
            transactionAmount: transaction.transactionAmount,
            beneficiaryBankCode: transaction.beneficiaryRemittanceCode || '',
            beneficiaryAccountNo: transaction.beneficiaryAccountNo || '',
            beneficiaryAccountName: transaction.beneficiaryAccountName || '',
            sourceBankCode: transaction.sourceRemittanceCode || '',
            sourceAccountNo: transaction.sourceAccountNo || '',
            sourceAccountName: transaction.sourceAccountName || '',
            currency: transaction.sourceTransactionCurrency,
            notes: transaction.note || '',
            interchange: module_common_1.BankNetworkEnum.RTGS,
            exchangeRate: rtgs_constant_1.EXCHANGE_RATE,
            addInfo1: transaction.note || ''
        };
    },
    isEligible: (transaction) => transaction.paymentServiceCode === rtgs_constant_1.RDN_KSEI
};
const rtgsRequestTemplate = {
    getPayload: (transaction) => ({
        trn: rtgs_constant_1.RTGS_TRN,
        externalId: (transaction.thirdPartyOutgoingId &&
            transaction.thirdPartyOutgoingId.padStart(16, '0')) ||
            '',
        transactionDate: transaction_util_1.formatTransactionDate(transaction.createdAt || new Date()),
        transactionAmount: transaction.transactionAmount,
        beneficiaryBankCode: transaction.beneficiaryRemittanceCode || '',
        beneficiaryAccountNo: transaction.beneficiaryAccountNo || '',
        beneficiaryAccountName: transaction.beneficiaryAccountName || '',
        sourceBankCode: transaction.sourceRemittanceCode || '',
        sourceAccountNo: transaction.sourceAccountNo || '',
        sourceAccountName: transaction.sourceAccountName || '',
        currency: transaction.sourceTransactionCurrency,
        notes: transaction.note || '',
        interchange: module_common_1.BankNetworkEnum.RTGS
    }),
    isEligible: (transaction) => transaction.paymentServiceCode === rtgs_constant_1.RTGS ||
        transaction.paymentServiceCode === rtgs_constant_1.BRANCH_RTGS ||
        transaction.paymentServiceCode === rtgs_constant_1.RTGS_FOR_BUSINESS ||
        transaction.paymentServiceCode === rtgs_constant_1.RDN_WITHDRAW_RTGS ||
        transaction.paymentServiceCode === rtgs_constant_1.RTGS_SHARIA
};
const rtgsTemplates = [
    rdnKseiRequestTemplate,
    rtgsRequestTemplate
];
exports.mapToRtgsTransaction = (transaction) => {
    const templateRequest = rtgsTemplates.find(template => template.isEligible(transaction));
    if (!templateRequest) {
        const detail = `Transaction submission template request not found with paymentServiceType: ${transaction.paymentServiceType}, paymentServiceCode: ${transaction.paymentServiceCode}`;
        logger_1.default.error(`mapToRtgsTransaction: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.JAGO, errors_1.ERROR_CODE.REQUEST_TEMPLATE_NOT_FOUND);
    }
    return templateRequest.getPayload(transaction);
};
//# sourceMappingURL=rtgsRequest.template.js.map