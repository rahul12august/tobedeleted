"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const module_common_1 = require("@dk/module-common");
const rtgsRequest_template_1 = require("./rtgsRequest.template");
const transaction_util_1 = __importDefault(require("../transaction/transaction.util"));
const transaction_repository_1 = __importDefault(require("../transaction/transaction.repository"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const rtgs_repository_1 = __importDefault(require("./rtgs.repository"));
const lodash_1 = require("lodash");
const transaction_producer_1 = __importDefault(require("../transaction/transaction.producer"));
const transaction_producer_enum_1 = require("../transaction/transaction.producer.enum");
const constant_1 = require("../common/constant");
const rtgs_helper_1 = require("./rtgs.helper");
const transaction_enum_1 = require("../transaction/transaction.enum");
const submitTransaction = (transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        rtgs_helper_1.validateMandatoryFields(transactionModel);
        if (!transactionModel.thirdPartyOutgoingId) {
            const detail = `Third party outgoing id is missing while doing paymentServiceType: ${transactionModel.paymentServiceType}, 
      paymentServiceCode: ${transactionModel.paymentServiceCode} transaction.`;
            logger_1.default.error(detail);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_EXTERNALID);
        }
        const rtgsTransactionPayload = rtgsRequest_template_1.mapToRtgsTransaction(transactionModel);
        transactionModel.thirdPartyOutgoingId = rtgsTransactionPayload.externalId;
        yield transaction_util_1.default.submitTransactionWithRetry(rtgs_repository_1.default.submitTransaction, rtgsTransactionPayload);
        return transactionModel;
    }
    catch (error) {
        logger_1.default.error(`failed submission to paymentServiceType: ${transactionModel.paymentServiceType}, paymentServiceCode: ${transactionModel.paymentServiceCode} error: ${JSON.stringify(error)}`);
        if (error.code && error.code === constant_1.httpClientTimeoutErrorCode) {
            logger_1.default.info(`Sending pending paymentServiceType: ${transactionModel.paymentServiceType}, 
        paymentServiceCode: ${transactionModel.paymentServiceCode} blocked amount notification for : ${transactionModel.id}`);
            transaction_producer_1.default.sendPendingBlockedAmountNotification(transactionModel, transaction_producer_enum_1.NotificationCode.NOTIF_PENIDNG_SKNRTGS);
            return transactionModel;
        }
        throw error;
    }
    finally {
        yield transaction_repository_1.default.update(transactionModel.id, {
            thirdPartyOutgoingId: transactionModel.thirdPartyOutgoingId
        });
    }
});
const isEligible = (model) => {
    if (model.paymentServiceCode) {
        return ([
            'RTGS',
            'BRANCH_RTGS',
            'RDN_KSEI',
            'RTGS_FOR_BUSINESS',
            'RDN_WITHDRAW_RTGS'
        ].includes(model.paymentServiceCode) &&
            module_common_1.BankChannelEnum.EXTERNAL === model.beneficiaryBankCodeChannel &&
            !lodash_1.isEmpty(model.beneficiaryAccountNo));
    }
    return false;
};
const shouldSendNotification = () => true;
const getRail = () => module_common_1.Rail.RTGS;
const rtgsSubmissionTemplate = {
    submitTransaction,
    isEligible,
    getRail,
    shouldSendNotification
};
exports.default = logger_1.wrapLogs(rtgsSubmissionTemplate);
//# sourceMappingURL=rtgsSubmissionTemplate.service.js.map