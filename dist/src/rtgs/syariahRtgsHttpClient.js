"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const context_1 = require("../context");
const logger_1 = __importDefault(require("../logger"));
const module_common_1 = require("@dk/module-common");
const constant_1 = require("../common/constant");
const { baseUrlSharia, timeout } = config_1.config.get('rtgs');
const syariahRtgsHttpClient = module_common_1.Http.createHttpClientForward({
    baseURL: baseUrlSharia,
    timeout: timeout,
    context: context_1.context,
    logger: logger_1.default,
    retryConfig: Object.assign(Object.assign({}, constant_1.retryConfig), { networkErrorCodesToRetry: ['ENOENT', 'ECONNREFUSED'] })
});
exports.default = syariahRtgsHttpClient;
//# sourceMappingURL=syariahRtgsHttpClient.js.map