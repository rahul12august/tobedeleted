"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_config_1 = require("@dk/module-config");
exports.config = module_config_1.config;
const lodash_1 = require("lodash");
/* To overirde any config value call loadConfig with respective config object
ex: loadConfig({ 'server': { port: 9090 } });
*/
// ENV config will apply on reload
// JSON config must be rebuilt to apply
const defaultConf = require('../configs/default.json');
const env = process.env.NODE_ENV;
const envConf = env ? require(`../configs/${env}.json`) : {};
module_config_1.loadConfig(lodash_1.merge(defaultConf, envConf));
//# sourceMappingURL=config.js.map