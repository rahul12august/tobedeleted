"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const context_1 = require("../context");
const logger_1 = __importDefault(require("../logger"));
const module_common_1 = require("@dk/module-common");
const constant_1 = require("../common/constant");
const MS_VIRTUAL_ACCOUNT_URL = config_1.config.get('ms').virtualAccount;
const httpClient = module_common_1.Http.createHttpClientForward({
    baseURL: MS_VIRTUAL_ACCOUNT_URL,
    context: context_1.context,
    logger: logger_1.default,
    retryConfig: constant_1.retryConfig
});
exports.default = httpClient;
//# sourceMappingURL=httpClient.js.map