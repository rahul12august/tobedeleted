"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const wrapLogs = jest.fn(obj => obj);
exports.wrapLogs = wrapLogs;
const logger = {
    error: jest.fn(),
    info: jest.fn(),
    debug: jest.fn(),
    warn: jest.fn(),
    critical: jest.fn()
};
exports.default = logger;
//# sourceMappingURL=logger.js.map