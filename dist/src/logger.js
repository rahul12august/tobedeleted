"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_logger_1 = require("@dk/module-logger");
const module_common_1 = require("@dk/module-common");
const config_1 = require("./config");
const context_1 = require("./context");
const constant_1 = require("./common/constant");
const level = config_1.config.get('logLevel') || module_logger_1.LevelEnum.warn;
const logger = module_logger_1.createLogger({
    defaultMeta: {
        service: constant_1.MICROSERVICE_NAME
    },
    tracing: context_1.context,
    level
});
exports.wrapLogs = new module_common_1.Logger.BaseLogWrapper(logger).getWrapper();
exports.default = logger;
//# sourceMappingURL=logger.js.map