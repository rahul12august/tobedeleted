"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const moment_1 = __importDefault(require("moment"));
const sinbin_model_1 = require("./sinbin.model");
const putInSinBin = (customerId, paymentServiceCode, cooldownDurationHours, offendingTransactionId) => __awaiter(void 0, void 0, void 0, function* () {
    const cooldownExpiry = moment_1.default().add(cooldownDurationHours, 'hours');
    const savedEntry = yield new sinbin_model_1.SinBinModel({
        customerId,
        paymentServiceCode,
        cooldownExpiry,
        offendingTransactionId
    }).save();
    logger_1.default.info(`Added to Sin Bin :: customerId : ${customerId} , paymentServiceCode : ${paymentServiceCode} , cooldownExpiry : ${cooldownExpiry} , offendingTransactionId : ${offendingTransactionId}`, savedEntry);
});
const isInSinBin = (customerId, paymentServiceCode) => __awaiter(void 0, void 0, void 0, function* () {
    return yield sinbin_model_1.SinBinModel.exists({
        customerId,
        paymentServiceCode,
        cooldownExpiry: { $gte: moment_1.default().toDate() }
    });
});
exports.default = logger_1.wrapLogs({
    putInSinBin,
    isInSinBin
});
//# sourceMappingURL=sinbin.repository.js.map