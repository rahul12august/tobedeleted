"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
exports.collectionName = 'sinbin';
const SinBinSchema = new mongoose_1.Schema({
    customerId: {
        type: String,
        required: true
    },
    paymentServiceCode: {
        type: String,
        required: true
    },
    cooldownExpiry: {
        type: Date,
        required: true
    },
    offendingTransactionId: {
        type: String,
        required: false
    }
}, {
    timestamps: true
});
SinBinSchema.index({ customerId: 1, paymentServiceCode: 1, cooldownExpiry: 1 });
exports.SinBinModel = mongoose_1.default.model(exports.collectionName, SinBinSchema);
//# sourceMappingURL=sinbin.model.js.map