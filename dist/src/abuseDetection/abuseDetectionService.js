"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importDefault(require("../logger"));
const config_1 = require("../config");
const transaction_repository_1 = __importDefault(require("../transaction/transaction.repository"));
const sinbin_repository_1 = __importDefault(require("./sinbin.repository"));
const moment_1 = __importDefault(require("moment"));
const abuseConfig = config_1.config.get('abuseDetection');
const configMap = new Map();
abuseConfig.forEach(item => {
    configMap.set(item.paymentServiceCode, item);
});
const previousTransactionProjection = {
    beneficiaryAccountNo: 1
};
exports.isSuspectedAbuse = (paymentServiceCode, ownerCustomerId, beneficiaryAccountNo, transactionId) => __awaiter(void 0, void 0, void 0, function* () {
    const config = configMap.get(paymentServiceCode);
    if (!config) {
        logger_1.default.info(`Abuse check - skip :: customerId : ${ownerCustomerId} , paymentServiceCode : ${paymentServiceCode} , transactionId : ${transactionId}`, {
            ownerCustomerId,
            paymentServiceCode,
            beneficiaryAccountNo,
            transactionId
        });
        return false;
    }
    const searchTime = moment_1.default().subtract(config.timeframe, 'minutes');
    const prevTransactions = yield transaction_repository_1.default.findTransactionByQuery({
        paymentServiceCode,
        ownerCustomerId,
        createdAt: { $gte: searchTime.toDate() }
    }, previousTransactionProjection);
    const destinations = new Set(prevTransactions.map(i => i.beneficiaryAccountNo));
    destinations.add(beneficiaryAccountNo);
    if (destinations.size > config.maxUniqueDestinations) {
        yield sinbin_repository_1.default.putInSinBin(ownerCustomerId, paymentServiceCode, config.cooldown, transactionId);
        logger_1.default.info(`Abuse check - blocked :: customerId : ${ownerCustomerId} , paymentServiceCode : ${paymentServiceCode} , transactionId : ${transactionId} , beneficiaryAccountNo : ${beneficiaryAccountNo}`, {
            ownerCustomerId,
            paymentServiceCode,
            beneficiaryAccountNo,
            transactionId,
            uniqueDestinationsInRange: destinations.size,
            maxAllowedUniqueDestinations: config.maxUniqueDestinations,
            timeRangeInMinutes: config.timeframe
        });
        return true;
    }
    if (yield sinbin_repository_1.default.isInSinBin(ownerCustomerId, paymentServiceCode)) {
        logger_1.default.info(`Abuse check - on cooldown :: customerId : ${ownerCustomerId} , paymentServiceCode : ${paymentServiceCode} , transactionId : ${transactionId} , beneficiaryAccountNo : ${beneficiaryAccountNo}`, {
            ownerCustomerId,
            paymentServiceCode,
            beneficiaryAccountNo,
            transactionId,
            uniqueDestinationsInRange: destinations.size,
            maxAllowedUniqueDestinations: config.maxUniqueDestinations,
            timeRangeInMinutes: config.timeframe
        });
        return true;
    }
    logger_1.default.info(`Abuse check - ok :: customerId : ${ownerCustomerId} , paymentServiceCode : ${paymentServiceCode} , transactionId : ${transactionId} , beneficiaryAccountNo : ${beneficiaryAccountNo}`, {
        ownerCustomerId,
        paymentServiceCode,
        beneficiaryAccountNo,
        transactionId,
        uniqueDestinationsInRange: destinations.size,
        maxAllowedUniqueDestinations: config.maxUniqueDestinations,
        timeRangeInMinutes: config.timeframe
    });
    return false;
});
//# sourceMappingURL=abuseDetectionService.js.map