"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_logger_1 = require("@dk/module-logger");
const good = require('@hapi/good'); // this module have no type declaration yet
const logger_1 = __importDefault(require("../logger"));
const goodOptions = {
    ops: false,
    reporters: {
        loggerReporter: [module_logger_1.createEventLogStream(logger_1.default)]
    }
};
exports.default = {
    plugin: good,
    options: goodOptions
};
//# sourceMappingURL=good.js.map