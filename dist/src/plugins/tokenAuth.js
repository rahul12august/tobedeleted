"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const logger_1 = __importDefault(require("../logger"));
const config_1 = require("../config");
const { jwksUri } = config_1.config.get('idm');
exports.default = {
    plugin: module_common_1.tokenAuth,
    options: {
        logger: logger_1.default,
        jwksUri
    }
};
//# sourceMappingURL=tokenAuth.js.map