"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const HapiSwagger = __importStar(require("hapi-swagger"));
const error_1 = require("@dk/module-common/dist/error");
const Package = __importStar(require("../../package.json"));
const errors_1 = require("../common/errors");
const fs = __importStar(require("fs"));
const path = __importStar(require("path"));
const apiDocPath = path.join(__dirname, '..', '..', 'api-doc.md');
const apiDoc = fs.readFileSync(apiDocPath, 'utf8');
const errorDesc = error_1.displayErrorsDescription(errors_1.ErrorList);
const swaggerOptions = {
    info: {
        title: 'Transfer API Documentation',
        version: Package.version,
        description: apiDoc + '\n' + errorDesc
    },
    cors: process.env.NODE_ENV === 'dev',
    grouping: 'tags',
    securityDefinitions: {
        jwt: {
            type: 'apiKey',
            name: 'Authorization',
            in: 'header'
        }
    },
    security: [{ jwt: [] }]
};
exports.default = {
    plugin: HapiSwagger,
    options: swaggerOptions
};
//# sourceMappingURL=swagger.js.map