"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const requestWrapper_plugin_1 = __importDefault(require("@dk/module-common/dist/hapiPlugin/requestWrapper.plugin"));
const context_1 = require("../context");
const errors_1 = require("../common/errors");
const logger_1 = __importDefault(require("../logger"));
const requestWrapper = {
    plugin: new requestWrapper_plugin_1.default(context_1.context).requestWrapper,
    options: {
        logger: logger_1.default,
        errorList: errors_1.ErrorList
    }
};
exports.default = requestWrapper;
//# sourceMappingURL=requestWrapper.js.map