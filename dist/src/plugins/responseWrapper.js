"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const responseWrapper_plugin_1 = __importDefault(require("@dk/module-common/dist/hapiPlugin/responseWrapper.plugin"));
const logger_1 = __importDefault(require("../logger"));
const errors_1 = require("../common/errors");
const context_1 = require("../context");
const responseWrapper = {
    plugin: new responseWrapper_plugin_1.default(context_1.context).responseWrapper,
    options: {
        logger: logger_1.default,
        errorList: errors_1.ErrorList
    }
};
exports.default = responseWrapper;
//# sourceMappingURL=responseWrapper.js.map