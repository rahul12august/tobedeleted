"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const hapi_1 = __importDefault(require("@hapi/hapi"));
const Inert = __importStar(require("@hapi/inert"));
const Vision = __importStar(require("@hapi/vision"));
const kafkaProducer_1 = __importDefault(require("./common/kafkaProducer"));
const mongoDb_1 = require("./common/mongoDb");
const handleValidationErrors_1 = __importDefault(require("./common/handleValidationErrors"));
const swagger_1 = __importDefault(require("./plugins/swagger"));
const responseWrapper_1 = __importDefault(require("./plugins/responseWrapper"));
const requestWrapper_1 = __importDefault(require("./plugins/requestWrapper"));
require("./joiInitializer");
const routes_1 = require("./routes");
const logger_1 = __importDefault(require("./logger"));
const config_1 = require("./config");
const consumers_1 = __importDefault(require("./consumers"));
const module_common_1 = require("@dk/module-common");
const tokenAuth_1 = __importDefault(require("./plugins/tokenAuth"));
const envChecker_1 = __importDefault(require("./envChecker"));
const isBulkTransactionOnly = process.env.IS_BULK_TRANSACTION_ONLY === 'true';
const { port, host } = config_1.config.get('server');
let kafkaConsumers = [];
let hapiServer;
let processCounter = 0;
const createServer = () => __awaiter(void 0, void 0, void 0, function* () {
    const server = new hapi_1.default.Server({
        port,
        host,
        routes: {
            validate: {
                options: {
                    abortEarly: false
                },
                failAction: handleValidationErrors_1.default
            }
        }
    });
    const plugins = [
        Inert,
        Vision,
        swagger_1.default,
        module_common_1.HapiPlugin.Good(logger_1.default),
        responseWrapper_1.default,
        requestWrapper_1.default,
        tokenAuth_1.default,
        module_common_1.aclWrapper
    ];
    yield server.register(plugins);
    // Register routes
    server.route(routes_1.routes);
    // Increase counter on incoming request
    server.ext({
        type: 'onPreHandler',
        method: function (_request, h) {
            processCounter++;
            logger_1.default.debug(`incoming request. processCounter: ${processCounter}`);
            return h.continue;
        }
    });
    // Decrease counter on completed request
    server.ext({
        type: 'onPostHandler',
        method: function (_request, h) {
            processCounter--;
            logger_1.default.debug(`request completed. processCounter: ${processCounter}`);
            return h.continue;
        }
    });
    return server;
});
exports.init = () => __awaiter(void 0, void 0, void 0, function* () {
    yield mongoDb_1.connectMongo();
    const server = yield createServer();
    yield server
        .initialize()
        .then(() => logger_1.default.info(`server started at ${server.info.host}:${server.info.port}`));
    return server;
});
exports.handleTermination = () => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info('SIGTERM received');
    // disconnect consumer
    // https://kafka.js.org/docs/consumer-example
    if (kafkaConsumers) {
        yield Promise.all(kafkaConsumers.map(consumer => consumer.disconnect()));
        logger_1.default.info('consumers disconnected');
    }
    // wait for pending HTTP request(s) to complete
    logger_1.default.info(`waiting for pending process. processCounter: ${processCounter}`);
    while (processCounter > 0) {
        logger_1.default.info(`waiting processCounter: ${processCounter}`);
        yield new Promise(r => setTimeout(r, 1000));
    }
    logger_1.default.info(`all pending process completed. processCounter: ${processCounter}`);
    // stop server
    // https://hapi.dev/api/?v=18.4.2#-await-serverstopoptions
    if (hapiServer) {
        yield hapiServer.stop({ timeout: config_1.config.get('shutdownTimeout') });
        logger_1.default.info('server stopped');
    }
    // not calling kafkaProducer.disconnect()
    // to allow pending http request that needs to produce message
    // to try to complete
    // not calling process.exit()
    // to allow pending process to try to complete
    return {
        hapiServer,
        kafkaConsumers
    };
});
exports.start = (module) => __awaiter(void 0, void 0, void 0, function* () {
    if (!module.parent) {
        logger_1.default.info('Start server');
        envChecker_1.default.init();
        yield exports.init()
            .then((server) => __awaiter(void 0, void 0, void 0, function* () {
            hapiServer = server;
            yield kafkaProducer_1.default.connect();
            kafkaConsumers = yield consumers_1.default.init(isBulkTransactionOnly);
            yield server.start();
        }))
            .catch(err => {
            logger_1.default.error('Server cannot start', err);
            logger_1.default.onFinished(() => {
                process.exit(1);
            });
        });
    }
});
// handle signal
process.on('SIGTERM', exports.handleTermination);
exports.start(module);
//# sourceMappingURL=server.js.map