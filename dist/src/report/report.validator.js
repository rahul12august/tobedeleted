"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const AppError_1 = require("../errors/AppError");
const joi_1 = __importDefault(require("@hapi/joi"));
const lodash_1 = require("lodash");
const errors_1 = require("../common/errors");
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const transaction_constant_1 = __importDefault(require("../transaction/transaction.constant"));
const report_constant_1 = require("./report.constant");
const report_enum_1 = require("./report.enum");
const logger_1 = __importDefault(require("../logger"));
const matchEmptySchema = joi_1.default.any().valid('', null);
const accountNosValidator = joi_1.default.array()
    .items(joi_1.default.string().max(transaction_constant_1.default.MAX_LENGTH_SOURCE_ACCOUNT_NO))
    .single();
const dateValidator = joi_1.default.date()
    .iso()
    .optional()
    .empty(matchEmptySchema);
exports.reconcileReportValidator = joi_1.default.object({
    fromDate: dateValidator,
    toDate: dateValidator,
    paymentServiceCode: joi_1.default.array()
        .items(joi_1.default.string())
        .optional()
        .single(),
    accountNo: accountNosValidator.optional(),
    type: joi_1.default.string()
        .valid(Object.keys(report_enum_1.AccountSideEnum))
        .optional()
        .empty(matchEmptySchema)
}).label('Reconcile report request');
const isAccountAndAdditionalInfo4Null = (condition) => lodash_1.isEmpty(condition.additionalInformation4) && lodash_1.isEmpty(condition.accountNos);
exports.validateInputReconcileReport = (condition) => __awaiter(void 0, void 0, void 0, function* () {
    if (condition.fromDate &&
        condition.toDate &&
        condition.fromDate > condition.toDate) {
        logger_1.default.error(`validateInputReconcileReport: from date ${condition.fromDate} is greater than to date ${condition.toDate}`);
        throw new AppError_1.AppError(errors_1.ERROR_CODE.FROM_DATE_GT_TO_DATE);
    }
    if (condition.paymentServiceCodes) {
        const paymentServiceRules = yield configuration_repository_1.default.getPaymentConfigRules();
        if (!paymentServiceRules) {
            logger_1.default.error('validateInputReconcileReport: no payment service configuration rule found');
            throw new AppError_1.AppError(errors_1.ERROR_CODE.INVALID_PAYMENT_SERVICE_RULE);
        }
        const paymentServiceCodes = paymentServiceRules.reduce((codes, item) => codes.concat(item.recommendCodes), []);
        if (!condition.paymentServiceCodes.every(item => paymentServiceCodes.includes(item))) {
            logger_1.default.error('validateInputReconcileReport:  no payment service configuration rule with all paymentServiceCodes.');
            throw new AppError_1.AppError(errors_1.ERROR_CODE.INVALID_PAYMENT_SERVICE_RULE);
        }
        if (condition.paymentServiceCodes &&
            condition.paymentServiceCodes.includes(report_constant_1.PAYMENT_SERVICE_CODE_GIN_PAY) &&
            isAccountAndAdditionalInfo4Null(condition)) {
            logger_1.default.error(`validateInputReconcileReport: account number is required and paymentServiceCode should include ${report_constant_1.PAYMENT_SERVICE_CODE_GIN_PAY}`);
            throw new AppError_1.AppError(errors_1.ERROR_CODE.ACCOUNT_NUMBER_REQUIRED);
        }
    }
});
//# sourceMappingURL=report.validator.js.map