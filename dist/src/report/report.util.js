"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const report_enum_1 = require("./report.enum");
const errors_1 = require("../common/errors");
const lodash_1 = require("lodash");
const report_constant_1 = require("./report.constant");
const AppError_1 = require("../errors/AppError");
const configuration_type_1 = require("../configuration/configuration.type");
const moment_timezone_1 = __importDefault(require("moment-timezone"));
const logger_1 = __importDefault(require("../logger"));
const fs = __importStar(require("fs"));
const billDetail_repository_1 = __importDefault(require("../billPayment/billDetail/billDetail.repository"));
const module_common_1 = require("@dk/module-common");
const billPaymentTransaction_service_1 = __importDefault(require("../billPayment/billPaymentTransaction/billPaymentTransaction.service"));
const transaction_enum_1 = require("../transaction/transaction.enum");
const contextHandler_1 = require("../common/contextHandler");
const billDetailsRetryConfig = () => ({
    shouldRetry: (error) => {
        return error.status >= 500;
    },
    numberOfTries: contextHandler_1.getRetryCount() || 1
});
const getKeyReferenceConfig = (reference) => {
    return report_enum_1.PreferenceEnum[reference];
};
const getReconcileReportQuery = (condition) => {
    return {
        fromDate: condition.fromDate,
        toDate: condition.toDate,
        paymentServiceCodes: condition.paymentServiceCodes,
        accountNos: condition.accountNos,
        additionalInformation4: condition.additionalInformation4
    };
};
const decideTxnType = (txn) => {
    var _a, _b;
    return ((_b = (_a = txn) === null || _a === void 0 ? void 0 : _a.refund) === null || _b === void 0 ? void 0 : _b.revertPaymentServiceCode) ? report_enum_1.ReconcileReportTxnType.REFUND
        : report_enum_1.ReconcileReportTxnType.TRANSACTION;
};
const defaultTxnMapper = (transaction, type = 'Beneficiary', reference) => __awaiter(void 0, void 0, void 0, function* () {
    let { beneficiaryAccountNo: beneficiaryAccount, beneficiaryRtolCode: bankId, beneficiaryBankCode: bankCode, sourceAccountNo: sourceAccount, transactionAmount: amount, createdAt: transactionDate, status: status, additionalInformation3, additionalInformation4 } = transaction;
    const referenceId = transaction[getKeyReferenceConfig(reference)];
    if (type === report_enum_1.AccountSideEnum.Source) {
        bankId = transaction.sourceRtolCode;
        bankCode = transaction.sourceBankCode;
    }
    let bankName = '';
    if (bankCode) {
        try {
            const bankdetails = yield configuration_repository_1.default.getBankCodeMapping(bankCode);
            bankName = bankdetails && bankdetails.name ? bankdetails.name : '';
        }
        catch (error) {
            logger_1.default.error(`defaultTxnMapper: getBankCodeMapping fails for bankCode: ${bankCode}`, error);
        }
    }
    return {
        referenceId,
        amount,
        transactionDate,
        status,
        sourceAccount,
        beneficiaryAccount,
        bankId,
        bankName,
        channel: report_constant_1.defaultChannel,
        type: decideTxnType(transaction),
        additionalInformation3,
        additionalInformation4
    };
});
const goBillsTxnMapper = (transaction, type = 'Beneficiary', reference) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c;
    let { beneficiaryAccountNo: beneficiaryAccount, beneficiaryRtolCode: bankId, sourceAccountNo: sourceAccount, transactionAmount: amount, createdAt: transactionDate, status: transactionStatus } = transaction;
    let status = transactionStatus;
    const referenceId = transaction[getKeyReferenceConfig(reference)];
    if (type === report_enum_1.AccountSideEnum.Source) {
        bankId = transaction.sourceRtolCode;
    }
    let billDetails;
    try {
        billDetails = yield module_common_1.Util.retry(billDetailsRetryConfig(), billDetail_repository_1.default.getBillDetails, referenceId);
    }
    catch (error) {
        logger_1.default.error(`getBillDetails fails after retry for referenceId ${referenceId}`, error);
    }
    if (status === transaction_enum_1.TransactionStatus.SUBMITTED) {
        const goBillsTransaction = yield billPaymentTransaction_service_1.default
            .getByTxnId(transaction.id)
            .catch(error => {
            logger_1.default.error(`getBillDetails fails after retry for referenceId ${referenceId}`, error);
        });
        if (goBillsTransaction) {
            status =
                goBillsTransaction.status[goBillsTransaction.status.length - 1].status;
        }
    }
    let bankName = '';
    let detail = '';
    if (billDetails) {
        const SKUcode = ((_b = (_a = billDetails) === null || _a === void 0 ? void 0 : _a.inquiryParams) === null || _b === void 0 ? void 0 : _b.skuCode) || '';
        const foundbillRow = lodash_1.find(billDetails.mappedBillInformation, o => o.rowType === 'PARTNER_AMOUNT');
        const SKUprice = ((_c = foundbillRow) === null || _c === void 0 ? void 0 : _c.value) || '';
        detail = `${SKUcode} | ${SKUprice}`;
        bankName = billDetails.billerName;
    }
    return {
        referenceId,
        amount,
        transactionDate,
        status,
        sourceAccount,
        beneficiaryAccount,
        bankId,
        bankName,
        channel: report_constant_1.defaultChannel,
        detail,
        type: decideTxnType(transaction)
    };
});
const reportMapper = {
    DEFAULT: defaultTxnMapper,
    GOBILL_REPORT: goBillsTxnMapper
};
const parseQueryStringReconcileReport = ({ query: { fromDate, toDate, accountNo, paymentServiceCode, type } }) => ({
    fromDate: lodash_1.isDate(fromDate)
        ? moment_timezone_1.default(fromDate)
            .tz(report_constant_1.JKRT_TIMEZONE)
            .startOf('day')
            .toDate()
        : undefined,
    toDate: lodash_1.isDate(toDate)
        ? moment_timezone_1.default(toDate)
            .tz(report_constant_1.JKRT_TIMEZONE)
            .endOf('day')
            .toDate()
        : undefined,
    accountNos: accountNo,
    paymentServiceCodes: paymentServiceCode,
    type: type
});
const getReportName = (paymentServiceTypes) => {
    if (!paymentServiceTypes) {
        return report_constant_1.reportDefaultName;
    }
    if (paymentServiceTypes.length === 1) {
        return lodash_1.head(paymentServiceTypes) + '_';
    }
    return report_constant_1.reportNameWithMultiTransaction;
};
exports.getRunningDateRange = (runningDate, periodOfReport) => {
    if (runningDate === configuration_type_1.RunningDate.DAILY) {
        return {
            start: moment_timezone_1.default()
                .tz(report_constant_1.JKRT_TIMEZONE)
                .add(periodOfReport, 'day')
                .startOf('day')
                .toDate(),
            end: moment_timezone_1.default()
                .tz(report_constant_1.JKRT_TIMEZONE)
                .add(periodOfReport, 'day')
                .endOf('day')
                .toDate()
        };
    }
    if (runningDate === configuration_type_1.RunningDate.MONTHLY) {
        return {
            start: moment_timezone_1.default()
                .tz(report_constant_1.JKRT_TIMEZONE)
                .add(periodOfReport, 'month')
                .startOf('month')
                .toDate(),
            end: moment_timezone_1.default()
                .tz(report_constant_1.JKRT_TIMEZONE)
                .add(periodOfReport, 'month')
                .endOf('month')
                .toDate()
        };
    }
    logger_1.default.error('unexpected running date type');
    throw new AppError_1.AppError(errors_1.ERROR_CODE.UNEXPECTED_ERROR);
};
const isStartOfMonth = () => moment_timezone_1.default()
    .tz(report_constant_1.JKRT_TIMEZONE)
    .startOf('month')
    .isSame(moment_timezone_1.default()
    .tz(report_constant_1.JKRT_TIMEZONE)
    .startOf('day'));
const removeTempFiles = (fileNames) => {
    for (const file of fileNames) {
        try {
            fs.unlinkSync(file);
        }
        catch (e) {
            logger_1.default.warn('Error deleting temp file', e);
        }
    }
};
const reportUtil = {
    getReconcileReportQuery,
    defaultTxnMapper,
    parseQueryStringReconcileReport,
    getReportName,
    removeTempFiles,
    getRunningDateRange: exports.getRunningDateRange,
    isStartOfMonth,
    reportMapper
};
exports.default = reportUtil;
//# sourceMappingURL=report.util.js.map