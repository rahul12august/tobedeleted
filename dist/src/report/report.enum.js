"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AccountSideEnum;
(function (AccountSideEnum) {
    AccountSideEnum["Source"] = "Source";
    AccountSideEnum["Beneficiary"] = "Beneficiary";
})(AccountSideEnum = exports.AccountSideEnum || (exports.AccountSideEnum = {}));
var PreferenceEnum;
(function (PreferenceEnum) {
    PreferenceEnum["thirdPartyIncomingId"] = "externalId";
    PreferenceEnum["thirdPartyOutgoingId"] = "thirdPartyOutgoingId";
})(PreferenceEnum = exports.PreferenceEnum || (exports.PreferenceEnum = {}));
var ReconcileReportTxnType;
(function (ReconcileReportTxnType) {
    ReconcileReportTxnType["TRANSACTION"] = "Transaction";
    ReconcileReportTxnType["REFUND"] = "Refund";
})(ReconcileReportTxnType = exports.ReconcileReportTxnType || (exports.ReconcileReportTxnType = {}));
//# sourceMappingURL=report.enum.js.map