"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const report_service_1 = __importDefault(require("./report.service"));
const report_validator_1 = require("./report.validator");
const report_util_1 = __importDefault(require("./report.util"));
const getReconcileReport = {
    method: module_common_1.Http.Method.GET,
    path: '/reconcile-report',
    options: {
        description: 'Get Reconcile Report',
        notes: 'All information must valid',
        tags: ['api', 'report', 'private'],
        auth: false,
        validate: {
            query: report_validator_1.reconcileReportValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { report, reportName } = yield report_service_1.default.getReconcileReport(report_util_1.default.parseQueryStringReconcileReport(hapiRequest));
            const fileName = `${reportName}${new Date().toISOString()}.csv`;
            return hapiResponse
                .response(report)
                .type('text/csv')
                .header('Content-Disposition', `attachment; filename=${fileName}`)
                .code(module_common_1.Http.StatusCode.OK);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'Success'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Invalid request'
                    },
                    [module_common_1.Http.StatusCode.NOT_FOUND]: {
                        description: 'Transaction not found'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const executeAutoUploadReconcileReport = {
    method: module_common_1.Http.Method.POST,
    path: '/jobs-execution/upload-reconcile-report',
    options: {
        description: 'Job Execution',
        notes: 'Auto upload transaction report to server',
        tags: ['api', 'report', 'jobs', 'private'],
        auth: false,
        handler: (_, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            report_service_1.default.uploadReconcileReport();
            return hapiResponse.response().code(module_common_1.Http.StatusCode.NO_CONTENT);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.NO_CONTENT]: {
                        description: 'Execute job successfully'
                    }
                }
            }
        }
    }
};
const reportController = [
    getReconcileReport,
    executeAutoUploadReconcileReport
];
exports.default = reportController;
//# sourceMappingURL=report.controller.js.map