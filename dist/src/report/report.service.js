"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const json2csv_1 = require("json2csv");
const report_constant_1 = require("./report.constant");
const report_type_1 = require("./report.type");
const report_util_1 = __importDefault(require("./report.util"));
const configuration_type_1 = require("../configuration/configuration.type");
const moment_timezone_1 = __importDefault(require("moment-timezone"));
const report_constant_2 = require("./report.constant");
const fileUploader_1 = __importDefault(require("../common/fileUploader"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const config_1 = require("../config");
const lodash_1 = require("lodash");
const fs = __importStar(require("fs"));
const path = __importStar(require("path"));
const notification_repository_1 = __importDefault(require("../notification/notification.repository"));
const report_validator_1 = require("./report.validator");
const transaction_repository_1 = __importDefault(require("../transaction/transaction.repository"));
const pathTempFile = config_1.config.get('pathTempFile');
const env = config_1.config.get('NODE_ENV');
exports.getTxnsReconcileReport = (condition) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info('getTxnReconcileReport:start');
    const query = report_util_1.default.getReconcileReportQuery(condition);
    return transaction_repository_1.default.getReconcileReport(query);
});
const mapTxnsToCsvFormat = (transactions, condition, reportJobType) => __awaiter(void 0, void 0, void 0, function* () {
    const mapper = report_util_1.default.reportMapper[reportJobType] || report_util_1.default.reportMapper.DEFAULT;
    return Promise.all(transactions.map(transaction => mapper(transaction, condition.type, condition.preferedId)));
});
const getReconcileReport = (condition, reportJobType = report_type_1.ReconcileReportJobType.DEFAULT) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info('Start validate input');
    yield report_validator_1.validateInputReconcileReport(condition);
    const transactions = yield exports.getTxnsReconcileReport(condition);
    logger_1.default.info('Start mapping data from transactions to report');
    const reconcileReport = yield mapTxnsToCsvFormat(transactions, condition, reportJobType);
    logger_1.default.info('Start parse data to csv');
    return {
        reportName: report_util_1.default.getReportName(condition.paymentServiceCodes),
        report: json2csv_1.parse(reconcileReport, {
            fields: report_constant_1.RECONCILE_REPORT_FIELDS
        })
    };
});
const uploadReport = (fileUploadParameter, transactionContent, { runningDate, mailingList, notificationTemplate, job }) => __awaiter(void 0, void 0, void 0, function* () {
    const fileTimeFormat = moment_timezone_1.default()
        .tz(report_constant_1.JKRT_TIMEZONE)
        .format(runningDate === configuration_type_1.RunningDate.DAILY ? report_constant_2.FORMAT_YYYYMMDDHHMMSS : report_constant_2.FORMAT_YYYYMM);
    const fileName = `${fileUploadParameter.prefixFileName.replace(/<.*>/, fileTimeFormat)}_${lodash_1.toUpper(env)}.csv`;
    const contentType = fileUploadParameter.fileType[0];
    const transactionFilePath = `${pathTempFile &&
        path.resolve(__dirname, pathTempFile) + '/' + fileName}`;
    let stream;
    try {
        yield fs.writeFileSync(transactionFilePath, transactionContent);
        if (fs.existsSync(transactionFilePath)) {
            stream = fs.createReadStream(transactionFilePath);
        }
    }
    catch (err) {
        logger_1.default.error('Can not write file', err);
    }
    let promiseEvents = [];
    promiseEvents.push(fileUploader_1.default.partnerReport.upload({
        contentType,
        file: stream,
        rootPath: fileUploadParameter.location,
        fileName,
        customPath: ''
    }));
    if (mailingList && mailingList.length > 0 && notificationTemplate && stream)
        promiseEvents.push(notification_repository_1.default.sendEmailReport({
            recipientEmails: mailingList.join(),
            notificationCode: notificationTemplate,
            attachedFile: stream,
            params: JSON.stringify({
                jobName: `${lodash_1.toUpper(env)} - ${job}`,
                transactionDate: moment_timezone_1.default()
                    .tz(report_constant_1.JKRT_TIMEZONE)
                    .format(report_constant_1.FORMAT_YYYY_MM_DD)
            })
        }));
    try {
        yield Promise.all(promiseEvents);
    }
    catch (e) {
        logger_1.default.error('Can not send email or upload file', e.toString());
    }
    return transactionFilePath;
});
const uploadSingleReport = (transactionReportConfig, fileUploadParameters) => __awaiter(void 0, void 0, void 0, function* () {
    const { job, runningDate, periodOfReport, paymentServiceCode, accountNo, referenceBank, fileLocation, preferedId, additionalInformation4 } = transactionReportConfig;
    let removingFilePath;
    if (runningDate === configuration_type_1.RunningDate.MONTHLY && !report_util_1.default.isStartOfMonth()) {
        return undefined;
    }
    try {
        const { start, end } = report_util_1.default.getRunningDateRange(runningDate, periodOfReport);
        const fileUploadParam = fileUploadParameters.find(({ code }) => code === fileLocation);
        if (!fileUploadParam) {
            logger_1.default.error('not found file upload parameter');
            return undefined;
        }
        const { report } = yield getReconcileReport({
            fromDate: start,
            toDate: end,
            paymentServiceCodes: paymentServiceCode,
            accountNos: accountNo,
            type: referenceBank,
            preferedId,
            additionalInformation4
        }, job);
        removingFilePath = yield uploadReport(fileUploadParam, report, transactionReportConfig);
    }
    catch (e) {
        logger_1.default.error('error while uploading report', e);
    }
    return removingFilePath;
});
const uploadReconcileReport = () => __awaiter(void 0, void 0, void 0, function* () {
    const configs = yield configuration_repository_1.default.getTransactionReportConfigsAndFileUpload();
    const removingFiles = [];
    if (!configs || !configs.length) {
        logger_1.default.error(`Transaction report config and file upload not found.`);
        throw new AppError_1.AppError(errors_1.ERROR_CODE.TRANSACTION_REPORT_CONFIG_NOT_FOUND);
    }
    const [transactionReportConfigs, fileUploadParameters] = configs;
    try {
        for (const transactionReportConfig of transactionReportConfigs) {
            removingFiles.push(yield uploadSingleReport(transactionReportConfig, fileUploadParameters));
        }
    }
    finally {
        report_util_1.default.removeTempFiles(lodash_1.compact(removingFiles));
    }
});
const reportService = logger_1.wrapLogs({
    getReconcileReport,
    uploadReconcileReport
});
exports.default = reportService;
//# sourceMappingURL=report.service.js.map