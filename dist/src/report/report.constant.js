"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RECONCILE_REPORT_FIELDS = [
    { label: 'Reference Id', value: 'referenceId' },
    { label: 'Amount', value: 'amount' },
    { label: 'Transaction Date', value: 'transactionDate' },
    { label: 'Status', value: 'status' },
    { label: 'Source Account', value: 'sourceAccount' },
    { label: 'Beneficiary Account', value: 'beneficiaryAccount' },
    { label: 'BankId', value: 'bankId' },
    { label: 'Bank Name', value: 'bankName' },
    { label: 'Detail', value: 'detail' },
    { label: 'Channel', value: 'channel' },
    { label: 'Type', value: 'type' },
    { label: 'Additional Information 3', value: 'additionalInformation3' },
    { label: 'Additional Information 4', value: 'additionalInformation4' }
];
exports.reportDefaultName = 'ALL_TRANSACTION_';
exports.reportNameWithMultiTransaction = 'TRANSACTIONS_';
exports.defaultChannel = 'Application';
exports.FORMAT_YYYYMMDDHHMMSS = 'YYYYMMDDHHMMSS';
exports.FORMAT_YYYYMM = 'YYYYMM';
exports.PAYMENT_SERVICE_CODE_GIN_PAY = 'SIT03';
exports.FORMAT_YYYY_MM_DD = 'YYYY-MM-DD';
exports.JKRT_TIMEZONE = 'Asia/Jakarta';
//# sourceMappingURL=report.constant.js.map