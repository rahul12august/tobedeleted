"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const faker_1 = __importDefault(require("faker"));
exports.getCustomerResponse = () => {
    return {
        cif: faker_1.default.random.alphaNumeric(40),
        id: faker_1.default.random.alphaNumeric(40),
        fullName: 'Customer Name',
        languagePreferred: 'en',
        customerType: "BUSINESS_INDIVIDUAL" /* BUSINESS_INDIVIDUAL */
    };
};
//# sourceMappingURL=customer.data.js.map