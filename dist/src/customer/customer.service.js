"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const customer_repository_1 = __importDefault(require("./customer.repository"));
const logger_1 = __importStar(require("../logger"));
const errors_1 = require("../common/errors");
const AppError_1 = require("../errors/AppError");
const transaction_enum_1 = require("../transaction/transaction.enum");
const getCustomerById = (customerId) => __awaiter(void 0, void 0, void 0, function* () {
    const customer = yield customer_repository_1.default.getCustomerInfoById(customerId);
    if (!customer || !customer.cif) {
        let detail = `CIF not found for customerId: ${customerId}!`;
        logger_1.default.error(detail);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.CUSTOMER_NOT_FOUND);
    }
    return customer;
});
const customerService = {
    getCustomerById
};
exports.default = logger_1.wrapLogs(customerService);
//# sourceMappingURL=customer.service.js.map