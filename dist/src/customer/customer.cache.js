"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const redis_1 = __importDefault(require("../common/redis"));
const customer_repository_1 = __importDefault(require("./customer.repository"));
const customerPrefixKey = 'ms-customer';
const getCustomerInfoById = (customerId) => __awaiter(void 0, void 0, void 0, function* () {
    return redis_1.default.get(customerId, customerPrefixKey, customer_repository_1.default.getCustomerInfoById, customerId);
});
const getCustomerType = (customerId) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const customer = yield getCustomerInfoById(customerId);
    return (_a = customer) === null || _a === void 0 ? void 0 : _a.customerType;
});
const customerCache = {
    getCustomerType
};
exports.default = customerCache;
//# sourceMappingURL=customer.cache.js.map