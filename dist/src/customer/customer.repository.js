"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const customer_constants_1 = require("./customer.constants");
const httpClient_1 = __importDefault(require("./httpClient"));
const logger_1 = __importStar(require("../logger"));
const getCustomerInfoById = (customerId) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info('Calling get customer info by customerId : ', customerId);
    const { data: { data: customerInfo } } = yield httpClient_1.default.get(customer_constants_1.getCustomerByIdUrl(customerId));
    return customerInfo;
});
const customerRepository = {
    getCustomerInfoById
};
exports.default = logger_1.wrapLogs(customerRepository);
//# sourceMappingURL=customer.repository.js.map