"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const storage_1 = require("@google-cloud/storage");
const logger_1 = __importDefault(require("../../logger"));
const fileMigration_constant_1 = require("./fileMigration.constant");
class FileMigration {
    constructor() {
        this.storage = new storage_1.Storage();
    }
    parseFileName(file) {
        const fileNameArray = file.split('-');
        if (fileNameArray.length !== 2) {
            logger_1.default.error(`getDestBucketAndFile: Incorrect file name ${file}`);
            return;
        }
        const fileName = fileNameArray[0];
        const bucketName = fileNameArray[1].split('.')[0];
        return { bucketName, fileName };
    }
    moveFile(destination, file) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!destination) {
                    return false;
                }
                logger_1.default.info(`moveFile : Destination Bucket ${destination.bucketName} and file name 
                ${destination.fileName}`);
                yield this.storage
                    .bucket(fileMigration_constant_1.srcBucketName)
                    .file(file)
                    .move(this.storage.bucket(destination.bucketName).file(destination.fileName));
                logger_1.default.info(`succeed:moveFile for source bucket ${fileMigration_constant_1.srcBucketName} and beneficiary bucket ${destination.bucketName} completed!!`);
                return true;
            }
            catch (error) {
                logger_1.default.error(`failure:migrateFileBetweenBuckets for ${fileMigration_constant_1.srcBucketName},  Error : ${JSON.stringify(error)}`);
                return false;
            }
        });
    }
    moveFiles() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let [files] = yield this.storage
                    .bucket(fileMigration_constant_1.srcBucketName)
                    .getFiles();
                files = files || [];
                for (let file of files) {
                    logger_1.default.info(`moveFiles: Moving File ${file.name}`);
                    const destination = this.parseFileName(file.name);
                    yield this.moveFile(destination, file.name);
                }
                return;
            }
            catch (error) {
                logger_1.default.error(`failure: moveFiles for ${fileMigration_constant_1.srcBucketName},Error : ${JSON.stringify(error)}`);
                return;
            }
        });
    }
    processMigration(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const message = req.query.message || req.body.message || 'Migration process triggered!!';
            this.moveFiles();
            return res.status(201).send(message);
        });
    }
}
exports.default = FileMigration;
//# sourceMappingURL=fileMigration.js.map