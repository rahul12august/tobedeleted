"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rdn_constant_1 = require("./rdn.constant");
exports.SKNConfig = {
    dailyTransactionAmountLimit: 500000000,
    cutOffTime: '15:00'
};
exports.RTGSConfig = {
    dailyTransactionAmountLimit: 1000000000,
    cutOffTime: '15:00'
};
exports.paymentCodeConfig = {
    SKN: {
        dailyTransactionAmountLimit: 500000000,
        cutOffTime: '15:00'
    },
    RTGS: {
        dailyTransactionAmountLimit: 1000000000,
        cutOffTime: '15:00'
    }
};
exports.HolidayList = [
    {
        name: 'Weekend-test',
        fromDate: new Date('2021-03-31 00:00:00.000Z'),
        toDate: new Date('2021-03-31 23:59:59.999Z'),
        categories: [rdn_constant_1.WorkingScheduleCategory.BANK]
    },
    {
        name: 'Weekend-test',
        fromDate: new Date('2021-09-03 00:00:00.000Z'),
        toDate: new Date('2021-09-03 23:59:59.999Z'),
        categories: [rdn_constant_1.WorkingScheduleCategory.SKN]
    },
    {
        name: 'Weekend-test',
        fromDate: new Date('2021-09-10 00:00:00.000Z'),
        toDate: new Date('2021-09-10 23:59:59.999Z'),
        categories: [rdn_constant_1.WorkingScheduleCategory.RTGS]
    }
];
//# sourceMappingURL=rdn.config.js.map