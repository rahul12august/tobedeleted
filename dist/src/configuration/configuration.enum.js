"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TransactionCodeType;
(function (TransactionCodeType) {
    TransactionCodeType["DEBIT"] = "DEBIT";
    TransactionCodeType["CREDIT"] = "CREDIT";
})(TransactionCodeType = exports.TransactionCodeType || (exports.TransactionCodeType = {}));
//# sourceMappingURL=configuration.enum.js.map