"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const parameterSettingQuery = `
query parameterSettings($type: ParameterSettingType!) {
  parameterSettings(type: $type){
    code
    name
    value
  }
}
`;
exports.default = parameterSettingQuery;
//# sourceMappingURL=parameterSetting.js.map