"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const holidaysByDateRangeQuery = `        
  query getHolidaysByDateRange($fromDate:Date, $toDate:Date) {
    holidays(fromDate: $fromDate, toDate: $toDate) {
        name
        toDate
        fromDate
    }
  }
`;
exports.default = holidaysByDateRangeQuery;
//# sourceMappingURL=holiday.js.map