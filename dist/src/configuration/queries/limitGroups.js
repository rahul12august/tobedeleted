"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const getlimitGroupsQuery = `        
  query getAllLimitGroups {
    limitGroups {
      code
      dailyLimitAmount
    }
  }
`;
exports.default = getlimitGroupsQuery;
//# sourceMappingURL=limitGroups.js.map