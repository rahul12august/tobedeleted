"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const getDecisionEngineConfigByCodeQuery = `        
  query decisionEngineRules($code:String!) {
      decisionEngineRules(serviceCode: $code) {
        functionality
        description
        serviceCode
        rules {
          property
          operator
          value
          values
        }
      }
  }
`;
exports.default = getDecisionEngineConfigByCodeQuery;
//# sourceMappingURL=decisionEngineConfigByCode.js.map