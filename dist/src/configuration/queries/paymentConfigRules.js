"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.paymentConfigRulesQuery = `
query getPaymentConfigRules {
	paymentConfigRules {
    source
    target
    sourceAccType
    targetAccType
    sameCIF
    amountRangeFrom
    amountRangeTo
    rtolDailyUsage
    sknDailyUsage
    rtgsDailyUsage
    serviceRecommendation
    paymentServiceType
    paymentServiceMappings {
      paymentServiceCode
      beneficiaryBankCodeType
      debitTransactionCode {
        interchange
        transactionCode
        transactionCodeInfo {
          code
          limitGroupCode
          feeRules
          minAmountPerTx
          maxAmountPerTx
          defaultCategoryCode
          channel
          awardGroupCounter
          entitlementCode
          counterCode
        }
      }
      creditTransactionCode {
        interchange
        transactionCode
        transactionCodeInfo {
          code
          limitGroupCode
          feeRules
          minAmountPerTx
          maxAmountPerTx
          defaultCategoryCode
          channel
          awardGroupCounter
          entitlementCode
          counterCode
        }
      }
      inquiryFeeRule
      transactionAuthenticationChecking
      blocking
      requireThirdPartyOutgoingId
      cutOffTime
    }
  }
}
`;
exports.getServiceRecommendationsQuery = `query getServiceRecommendations($refundable: Boolean!) {
  getServiceRecommendations(refundable: $refundable)
}`;
//# sourceMappingURL=paymentConfigRules.js.map