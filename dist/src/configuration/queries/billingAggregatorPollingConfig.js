"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const billingAggregatorPollingConfigQuery = `
query billingAggregatorPollingConfig($billingAggregator: String) {
  billingAggregatorPollingConfig(billingAggregator: $billingAggregator){
    billingAggregator
    configs {
      type
      delayTime
    }
  }
}`;
exports.billingAggregatorPollingConfigQuery = billingAggregatorPollingConfigQuery;
//# sourceMappingURL=billingAggregatorPollingConfig.js.map