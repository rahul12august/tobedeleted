"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bankByIncomingCodeQuery = `
query getBankByIncomingCode($incomingCode: String!) {
    getBankByIncomingCode(incomingCode: $incomingCode){
        id
        bankCodeId
        name
        rtolCode
        remittanceCode
        billerCode
        isBersamaMember
        isAltoMember
        firstPriority
        channel
        companyName
        type
        prefix
        isInternal
        irisCode
        supportedChannels
    }
}
`;
//# sourceMappingURL=bankCodeIncomingCode.js.map