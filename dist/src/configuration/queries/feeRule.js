"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const feeRuleQuery = `
query getFeeRule($feeRuleInput: FeeRuleInput!) {
  feeRule(feeRuleInput: $feeRuleInput) {
    description
    basicFeeMapping {
      interchange
      basicFeeCode
      basicFee {
        feeAmountPercentage
        feeAmountFixed
        customerTc
        customerTcInfo {
          channel
        }
        subsidiary
        subsidiaryAmount
        subsidiaryAmountPercentage
        debitGlNumber
        creditGlNumber,
        thirdPartyFee
      }
    }
  }
}
`;
exports.default = feeRuleQuery;
//# sourceMappingURL=feeRule.js.map