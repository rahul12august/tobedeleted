"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * make sure to update mapFoundCache on saving product
 * configuration cache in this project
 * if there is any new field specified below
 */
exports.savingProductBaseQuery = `
 query getSavingProduct($code: String!) {
   savingProduct(code: $code) {
     encodedKey
     code
     currency
     monthlyFeeCode
     blockingCapability
     deletionCapability
     goalCapability
     maximumAccountPerUser
     type
     initialAmount
     maximumBalancePerAccount
   }
 }
 `;
//# sourceMappingURL=savingProduct.js.map