"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const currencyByCodeQuery = `        
  query currency($code:String!) {
    currency(code:$code) {
      code
      isoNumber
      name
    }
  }
`;
exports.currencyByCodeQuery = currencyByCodeQuery;
const currencyByIsoNumberQuery = `        
  query currencyByIsoNumber($isoNumber:String!) {
    currencyByIsoNumber(isoNumber:$isoNumber) {
      code
      isoNumber
      name
    }
  }
`;
exports.currencyByIsoNumberQuery = currencyByIsoNumberQuery;
//# sourceMappingURL=currency.js.map