"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const getNNSMappingQuery = `
query getNNSMappingByNNS ($reservedNnsPrefix: String!) {
    getNNSMappingByNNS(reservedNnsPrefix: $reservedNnsPrefix) {
        code
        institutionName
        brandName
        switchingName
        reservedNnsPrefix
    }
}
`;
exports.default = getNNSMappingQuery;
//# sourceMappingURL=nnsMapping.js.map