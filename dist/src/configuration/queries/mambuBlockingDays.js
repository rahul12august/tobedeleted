"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mambuBlockingDayQuery = `
query getMambuBlockingDay($days: Float!) {
  mambuBlockingDay(days: $days) {
    code
    name
    days
  }
}
`;
exports.mambuBlockingDayQuery = mambuBlockingDayQuery;
//# sourceMappingURL=mambuBlockingDays.js.map