"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = `
  query inquiryIncomingInfo($sourceBankCode: String!, $beneficiaryBankCode: String!, $isoNumber: String!) {
    sourceBank: getBankByIncomingCode(incomingCode: $sourceBankCode) {
      ...bankCodeFields
    }
    beneficiaryBank: getBankByIncomingCode(incomingCode: $beneficiaryBankCode) {
      ...bankCodeFields
    }
    currency: currencyByIsoNumber(isoNumber: $isoNumber) {
      code
    }
  }

  fragment bankCodeFields on BankCode {
    id
    bankCodeId
    name
    rtolCode
    remittanceCode
    billerCode
    isBersamaMember
    isAltoMember
    firstPriority
    channel
    companyName
    type
    prefix
    isInternal
  }
`;
//# sourceMappingURL=inquiryBankCodes.js.map