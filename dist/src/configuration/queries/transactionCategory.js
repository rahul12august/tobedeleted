"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transactionCategoryQuery = `
query transactionCategoryQuery($code: String!) {
  transactionCategory(code: $code) {
    code
    isIncoming
    isOutgoing
  }
}
`;
exports.default = transactionCategoryQuery;
//# sourceMappingURL=transactionCategory.js.map