"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transactionReportConfigs = `
query{
  transactionReportConfigs{
    job
    accountNo
    paymentServiceCode
    periodOfReport
    type
    runningDate
    runningTime
    fileLocation
    referenceBank
    mailingList
    notificationTemplate
    preferedId
    additionalInformation4
  }
  
  fileUploadParameters{
    code
    prefixFileName
    location
    fileType
  }
}
`;
exports.default = transactionReportConfigs;
//# sourceMappingURL=transactionReportConfigs.js.map