"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const generalLedgerQuery = `
query generalLedgers {
  generalLedgers {
    code
    accountNumber
  }
}
`;
exports.default = generalLedgerQuery;
//# sourceMappingURL=generalLedger.js.map