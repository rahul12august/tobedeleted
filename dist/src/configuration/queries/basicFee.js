"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const basicFeeQuery = `
query basicFee($code: String!) {
    basicFee(code:  $code) {
        feeAmountPercentage
        feeAmountFixed
        customerTc
        customerTcInfo {
            channel
        }
        subsidiary
        subsidiaryAmount
        subsidiaryAmountPercentage
        debitGlNumber
        creditGlNumber,
        thirdPartyFee
    }
}
`;
exports.default = basicFeeQuery;
//# sourceMappingURL=basicFee.js.map