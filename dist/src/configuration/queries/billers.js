"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const getBillersQuery = `
query billerByBankCode($bankCode: String!) {
    getbillerByBankCode(bankCode: $bankCode){
      bankCode
      code
      name
      inquireFeeInfo
  }
}
`;
exports.getBillersQuery = getBillersQuery;
//# sourceMappingURL=billers.js.map