"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.queryWorkingTimeTemplate = `
query getWorkingTimes($category: WorkingScheduleCategory!){
    workingTimes(category:$category) {
        dayName
        startTime
        endTime
    }
}
`;
//# sourceMappingURL=workingTimeTemplate.js.map