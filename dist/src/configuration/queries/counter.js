"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const updateCounterMutation = `
mutation updateCounter($counterInput: UpdateCounterInput!){
  updateCounter(updateCounterInput: $counterInput) {
    code
    value
    name
  }
}`;
exports.updateCounterMutation = updateCounterMutation;
const getCounterQuery = `
query getCounter($code: String!) {
    counter(code: $code) {
        code
      value
      name
    }
}`;
exports.getCounterQuery = getCounterQuery;
//# sourceMappingURL=counter.js.map