"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bankCodeMappingQuery = `
query bankCodeMappingQuery($bankCodeId: String!) {
  bankCode(bankCodeId: $bankCodeId){
    id
    bankCodeId
    name
    rtolCode
    remittanceCode
    billerCode
    isBersamaMember
    isAltoMember
    firstPriority
    channel
    companyName
    type
    prefix
    isInternal
    irisCode
    supportedChannels
  }
}
`;
exports.default = bankCodeMappingQuery;
//# sourceMappingURL=bankCodeMapping.js.map