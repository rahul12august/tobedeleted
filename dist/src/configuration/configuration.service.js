"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importStar(require("lodash"));
const module_common_1 = require("@dk/module-common");
const configuration_repository_1 = __importDefault(require("./configuration.repository"));
const logger_1 = __importStar(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const configuration_constant_1 = require("./configuration.constant");
const transaction_enum_1 = require("../transaction/transaction.enum");
const entitlement_service_1 = __importDefault(require("../entitlement/entitlement.service"));
const configuration_override_1 = __importDefault(require("./configuration.override"));
const getGoBillsPollingConfig = () => __awaiter(void 0, void 0, void 0, function* () {
    const pollingConfigs = yield configuration_repository_1.default.getBillingAggregatorPollingConfig(module_common_1.Enum.BillingAggregator.GOBILLS);
    return pollingConfigs && !lodash_1.isEmpty(pollingConfigs)
        ? pollingConfigs[0].configs
        : [];
});
const filterConfigRulesByServiceType = (paymentConfigRules, paymentServiceType) => __awaiter(void 0, void 0, void 0, function* () {
    return (paymentConfigRules || []).filter(configRule => configRule.paymentServiceType === paymentServiceType);
});
const filterConfigRulesByServiceCode = (paymentConfigRules, paymentServiceCode) => __awaiter(void 0, void 0, void 0, function* () {
    return (paymentConfigRules || []).filter(configRule => configRule.serviceRecommendation.includes(paymentServiceCode));
});
const filterConfigRulesByServiceTypeAndCode = (paymentConfigRules, paymentServiceType, paymentServiceCode) => __awaiter(void 0, void 0, void 0, function* () {
    return (paymentConfigRules || []).filter(configRule => configRule.paymentServiceType === paymentServiceType &&
        configRule.serviceRecommendation.includes(paymentServiceCode));
});
const getTransactionCodeInfo = (paymentConfigRules) => {
    const allTransactionCodeInfo = paymentConfigRules.map(rule => {
        return rule.paymentServiceMappings.map(service => {
            let transactionCodeInfo = [];
            if (service.creditTransactionCode) {
                transactionCodeInfo = transactionCodeInfo.concat(service.creditTransactionCode.map(code => code.transactionCodeInfo));
            }
            if (service.debitTransactionCode) {
                transactionCodeInfo = transactionCodeInfo.concat(service.debitTransactionCode.map(code => code.transactionCodeInfo));
            }
            return transactionCodeInfo;
        });
    });
    return lodash_1.default.flatten(lodash_1.default.flatten(allTransactionCodeInfo));
};
function overrideConfigAmount(customerId, limitGroupCode, entitlementsMap, config, 
// eslint-disable-next-line @typescript-eslint/no-explicit-any
overrideStrategy) {
    const entitlement = entitlementsMap.get(overrideStrategy.mapToEntitlementCode(limitGroupCode));
    if (entitlement) {
        const entitlementAmount = Number(entitlement.quota);
        if (isNaN(entitlementAmount) || entitlementAmount < 0) {
            logger_1.default.error(`Expected the entitlement '${entitlement.entitlement}'` +
                `for customer '${customerId}' to be a positive numerical value, but it was '${entitlement.quota}'`);
            return;
        }
        overrideStrategy.overrideLimit(customerId, config, entitlementAmount);
    }
}
function applyCustomLimitsToTransactionCodeInfo(transactionCodeInfo, customerId, entitlementsMap) {
    for (const codeInfo of transactionCodeInfo) {
        const limitGroupCode = codeInfo.limitGroupCode;
        if (!limitGroupCode)
            continue;
        overrideConfigAmount(customerId, limitGroupCode, entitlementsMap, codeInfo, configuration_override_1.default.minAmountPerTxOverrider);
        overrideConfigAmount(customerId, limitGroupCode, entitlementsMap, codeInfo, configuration_override_1.default.maxAmountPerTxOverrider);
    }
}
const getLimitGroupCodesForPaymentConfigRule = (paymentConfigRule) => {
    const transactionCodeInfo = getTransactionCodeInfo([paymentConfigRule]);
    return lodash_1.default.compact(lodash_1.default.uniqBy(transactionCodeInfo.map(codeInfo => codeInfo.limitGroupCode), code => code));
};
const applyCustomLimitsToPaymentConfigRules = (customerId, paymentConfigRules, entitlementsMap) => {
    for (const configRule of paymentConfigRules) {
        const limitGroupCodes = getLimitGroupCodesForPaymentConfigRule(configRule);
        if (limitGroupCodes.length != 1) {
            logger_1.default.debug('Unable to apply custom limits to payment config rules that do not have a singular limit group code');
            continue;
        }
        overrideConfigAmount(customerId, limitGroupCodes[0], entitlementsMap, configRule, configuration_override_1.default.paymentConfigRuleRangeFromOverrider);
        overrideConfigAmount(customerId, limitGroupCodes[0], entitlementsMap, configRule, configuration_override_1.default.paymentConfigRuleRangeToOverrider);
    }
};
const applyCustomLimits = (customerId, paymentConfigRules) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionCodeInfo = getTransactionCodeInfo(paymentConfigRules);
    const limitGroupCodes = lodash_1.default.compact(lodash_1.default.uniqBy(transactionCodeInfo.map(codeInfo => codeInfo.limitGroupCode), code => code));
    const entitlementCodes = [
        ...limitGroupCodes.map(code => configuration_override_1.default.minAmountPerTxOverrider.mapToEntitlementCode(code)),
        ...limitGroupCodes.map(code => configuration_override_1.default.maxAmountPerTxOverrider.mapToEntitlementCode(code))
    ];
    const entitlementsMap = yield entitlement_service_1.default.getEntitlementsMap(customerId, entitlementCodes);
    applyCustomLimitsToTransactionCodeInfo(transactionCodeInfo, customerId, entitlementsMap);
    applyCustomLimitsToPaymentConfigRules(customerId, paymentConfigRules, entitlementsMap);
});
const filterPaymentConfigRules = (paymentServiceType, paymentServiceCode, paymentConfigRules) => __awaiter(void 0, void 0, void 0, function* () {
    if (paymentServiceType && paymentServiceCode) {
        return yield filterConfigRulesByServiceTypeAndCode(paymentConfigRules, paymentServiceType, paymentServiceCode);
    }
    if (paymentServiceType) {
        return yield filterConfigRulesByServiceType(paymentConfigRules, paymentServiceType);
    }
    if (paymentServiceCode) {
        return yield filterConfigRulesByServiceCode(paymentConfigRules, paymentServiceCode);
    }
    return paymentConfigRules;
});
const getPaymentConfigRules = (paymentServiceType, paymentServiceCode, customerId) => __awaiter(void 0, void 0, void 0, function* () {
    const paymentConfigRules = yield configuration_repository_1.default.getPaymentConfigRules();
    const filteredRules = yield filterPaymentConfigRules(paymentServiceType, paymentServiceCode, paymentConfigRules);
    if (customerId && filteredRules) {
        yield applyCustomLimits(customerId, filteredRules);
    }
    return filteredRules;
});
const getSavingProduct = (productCode) => __awaiter(void 0, void 0, void 0, function* () {
    const savingProduct = yield configuration_repository_1.default.getSavingProduct(productCode);
    if (!savingProduct) {
        throw new AppError_1.TransferAppError(`Saving product not found for: ${productCode}!`, transaction_enum_1.TransferFailureReasonActor.JAGO, errors_1.ERROR_CODE.SAVING_PRODUCT_NOT_FOUND);
    }
    return savingProduct;
});
const getParameterSettingByReferenceGroup = (referenceGroup) => __awaiter(void 0, void 0, void 0, function* () {
    const parameterSetting = yield configuration_repository_1.default.getParameterSettingByReferenceGroup(referenceGroup);
    return parameterSetting && !lodash_1.isEmpty(parameterSetting) ? parameterSetting : [];
});
const isBiFastEnabled = () => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        const parameterSetting = yield configuration_repository_1.default.getParameterSettingByReferenceGroup(configuration_constant_1.BIFAST_REFERENCE_GROUP);
        if (lodash_1.isEmpty(parameterSetting)) {
            logger_1.default.warn(`isBiFastEnabled: ${configuration_constant_1.BIFAST_REFERENCE_GROUP} parameter setting not found`);
            return false;
        }
        const isBiFastEnabled = ((_a = parameterSetting.find(param => param.code === configuration_constant_1.BIFAST_ENABLED_CODE)) === null || _a === void 0 ? void 0 : _a.value) === 'true';
        logger_1.default.info(`isBiFastEnabled: returning BIFAST_ENABLED flag value: ${isBiFastEnabled}`);
        return isBiFastEnabled;
    }
    catch (error) {
        logger_1.default.error(`getParameterSettingByReferenceGroup: Error while fetching parameter setting.`);
        return false;
    }
});
const isDebitCardTransactionGroup = (paymentServiceType) => __awaiter(void 0, void 0, void 0, function* () {
    const parameterSettings = yield configuration_repository_1.default.getParameterSettingByReferenceGroup(configuration_constant_1.DEBIT_CARD_TRANSACTION_GROUP);
    if (parameterSettings) {
        for (let parameterSetting of parameterSettings) {
            if (parameterSetting.code === paymentServiceType) {
                return true;
            }
        }
    }
    return false;
});
const configurationService = {
    getGoBillsPollingConfig,
    getPaymentConfigRules,
    getSavingProduct,
    getParameterSettingByReferenceGroup,
    isBiFastEnabled,
    isDebitCardTransactionGroup,
    applyCustomLimits
};
exports.default = logger_1.wrapLogs(configurationService);
//# sourceMappingURL=configuration.service.js.map