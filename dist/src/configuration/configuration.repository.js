"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const logger_1 = __importStar(require("../logger"));
const httpClient_1 = __importDefault(require("./httpClient"));
const feeRule_1 = __importDefault(require("./queries/feeRule"));
const paymentConfigRules_1 = require("./queries/paymentConfigRules");
const limitGroups_1 = __importDefault(require("./queries/limitGroups"));
const transactionCategory_1 = __importDefault(require("./queries/transactionCategory"));
const bankCodeMapping_1 = __importDefault(require("./queries/bankCodeMapping"));
const currency_1 = require("./queries/currency");
const holiday_1 = __importDefault(require("./queries/holiday"));
const decisionEngineConfigByCode_1 = __importDefault(require("./queries/decisionEngineConfigByCode"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const billingAggregatorPollingConfig_1 = require("./queries/billingAggregatorPollingConfig");
const transactionReportConfigs_1 = __importDefault(require("./queries/transactionReportConfigs"));
const billers_1 = require("./queries/billers");
const redis_1 = __importDefault(require("../common/redis"));
const mambuBlockingDays_1 = require("./queries/mambuBlockingDays");
const counter_1 = require("./queries/counter");
const workingTimeTemplate_1 = require("./queries/workingTimeTemplate");
const savingProduct_1 = require("./queries/savingProduct");
const basicFee_1 = __importDefault(require("./queries/basicFee"));
const transactionCode_1 = __importDefault(require("./queries/transactionCode"));
const configuration_util_1 = require("./configuration.util");
const bankCodeIncomingCode_1 = require("./queries/bankCodeIncomingCode");
const parameterSetting_1 = __importDefault(require("./queries/parameterSetting"));
const generalLedger_1 = __importDefault(require("./queries/generalLedger"));
const nnsMapping_1 = __importDefault(require("./queries/nnsMapping"));
const transaction_enum_1 = require("../transaction/transaction.enum");
const pathOfConfiguration = '';
const paymentConfigRuleAllKey = '{ms-configuration}-paymentConfigRuleAll';
const billingAggregatorPrefix = 'ms-configuration';
const bankCodeIncomingKey = `{ms-configuration-bankCode}:getByIncomingCode-`;
const bankCodeMappingKey = `{ms-configuration-bankCode}:getByBankCodeIdOrPrefix-`;
const transactionCategoryKey = `{ms-configuration-transactionCategory}:`;
const currencyKey = `{ms-configuration-currency}:`;
const limitGroupAllKey = '{ms-configuration-limitGroup}:all?';
const throwGraphQLError = (apiName, errors) => {
    logger_1.default.error(`Error from API ${apiName} and errors are ${JSON.stringify(errors)}`);
    throw new AppError_1.TransferAppError(`Error from API ${apiName} and errors are ${JSON.stringify(errors)}!`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.INVALID_REQUEST, errors.map(error => ({
        code: errors_1.ERROR_CODE.INVALID_REQUEST,
        key: error.path[0],
        message: error.message
    })));
};
const getConfigurationCacheKey = (objectType, key) => `{ms-configuration-${objectType}}:${key}`;
const getFeeRule = (feeRuleCode, monthlyNoTransaction, interchange, targetBankCode, thresholdCounter) => __awaiter(void 0, void 0, void 0, function* () {
    const body = {
        query: feeRule_1.default,
        variables: {
            feeRuleInput: {
                code: feeRuleCode,
                monthlyNoTransaction,
                interchange,
                targetBankCode,
                thresholdCounter
            }
        }
    };
    logger_1.default.info(`getFeeRule : Payload details feeRuleCode : ${feeRuleCode}, monthlyNoTransaction : ${monthlyNoTransaction}, interchange : ${interchange}, targetBankCode : ${targetBankCode}`);
    const result = yield httpClient_1.default.post('', body);
    const data = result.data.data;
    return data ? data.feeRule : undefined;
});
const getTransactionCategory = (categoryCode) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getTransactionCategory : Payload details categoryCode : ${categoryCode}`);
    const key = transactionCategoryKey + categoryCode;
    const cachedObject = yield redis_1.default.get(key);
    if (cachedObject) {
        return cachedObject;
    }
    logger_1.default.info(`getTransactionCategory : Not available in cache fetching from configuration.`);
    const body = {
        query: transactionCategory_1.default,
        variables: {
            code: categoryCode
        }
    };
    const result = yield httpClient_1.default.post(pathOfConfiguration, body);
    const data = result.data.data;
    return data ? data.transactionCategory : undefined;
});
const getBankByRtolCode = (bankCode) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getBankByRtolCode : Payload details bankCode : ${bankCode}`);
    const key = bankCodeIncomingKey + configuration_util_1.standarizeRTOL(bankCode);
    const cachedObject = yield redis_1.default.get(key);
    if (!lodash_1.isEmpty(cachedObject) && !lodash_1.isEmpty(cachedObject[0])) {
        return cachedObject[0];
    }
    logger_1.default.info(`getBankByRtolCode : Not available in cache fetching from configuration.`);
    const body = {
        query: bankCodeIncomingCode_1.bankByIncomingCodeQuery,
        variables: {
            incomingCode: configuration_util_1.standarizeRTOL(bankCode)
        }
    };
    const result = yield httpClient_1.default.post(pathOfConfiguration, body);
    const data = result.data.data;
    return data ? data.getBankByIncomingCode : undefined;
});
const getBankCodeMapping = (bankCode) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getBankCodeMapping : Payload details bankCode : ${bankCode}`);
    const key = bankCodeMappingKey + bankCode;
    const cachedObject = yield redis_1.default.get(key);
    if (cachedObject) {
        return cachedObject;
    }
    logger_1.default.info(`getBankCodeMapping : Not available in cache fetching from configuration.`);
    const body = {
        query: bankCodeMapping_1.default,
        variables: {
            bankCodeId: bankCode
        }
    };
    const result = yield httpClient_1.default.post(pathOfConfiguration, body);
    const data = result.data.data;
    return data ? data.bankCode : undefined;
});
const getPaymentConfigRules = () => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getPaymentConfigRules : Fetching all config rules.`);
    const cachedObject = yield redis_1.default.get(paymentConfigRuleAllKey);
    if (cachedObject) {
        return cachedObject;
    }
    logger_1.default.info(`getPaymentConfigRules : Not available in cache fetching from configuration.`);
    const body = {
        query: paymentConfigRules_1.paymentConfigRulesQuery
    };
    const result = yield httpClient_1.default.post('', body);
    const data = result.data.data;
    yield redis_1.default.set(paymentConfigRuleAllKey, data ? data.paymentConfigRules : undefined, '');
    return data ? data.paymentConfigRules : undefined;
});
const getLimitGroupsByCodes = () => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getLimitGroupsByCodes : Payload for limit group codes`);
    const cachedObjects = yield redis_1.default.get(limitGroupAllKey);
    // Does cached objects exist each of them are truthy?
    if (cachedObjects) {
        return cachedObjects;
    }
    logger_1.default.info(`getLimitGroupsByCodes : Not available in cache fetching from configuration.`);
    const body = {
        query: limitGroups_1.default
    };
    const result = yield httpClient_1.default.post('', body);
    const data = result.data.data;
    return data ? data.limitGroups : undefined;
});
const getCurrencyByIsoNumber = (isoNumber) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getCodeCurrencyByIsoNumber : Payload details currency : ${isoNumber}`);
    const key = currencyKey + isoNumber;
    const cachedObject = yield redis_1.default.get(key);
    if (cachedObject) {
        return cachedObject.code;
    }
    logger_1.default.info(`getCodeCurrencyByIsoNumber : Not available in cache fetching from configuration.`);
    const body = {
        query: currency_1.currencyByIsoNumberQuery,
        variables: {
            isoNumber: isoNumber
        }
    };
    const result = yield httpClient_1.default.post('', body);
    const data = result.data.data;
    return data ? data.currencyByIsoNumber : undefined;
});
const inquiryBankAndCurrencyByIncomingCodes = (sourceBankCode, beneficiaryBankCode, isoNumber) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`inquiryBankAndCurrencyByIncomingCodes : Payload sourceBankCode ${sourceBankCode}, beneficiaryBankCode ${beneficiaryBankCode}, isoNumber ${isoNumber}`);
    let errors = [];
    let result;
    const [sourceBank, beneficiaryBank, currencyData] = yield Promise.all([
        getBankByRtolCode(sourceBankCode),
        getBankByRtolCode(beneficiaryBankCode),
        getCurrencyByIsoNumber(isoNumber)
    ]);
    if (sourceBank &&
        !lodash_1.isEmpty(sourceBank) &&
        beneficiaryBank &&
        !lodash_1.isEmpty(beneficiaryBank) &&
        currencyData) {
        result = {
            sourceBank,
            beneficiaryBank,
            currency: {
                code: currencyData
            }
        };
    }
    else {
        if (lodash_1.isEmpty(sourceBank)) {
            errors.push({
                code: errors_1.ERROR_CODE.INVALID_REGISTER_PARAMETER,
                key: 'sourceBankCode',
                message: errors_1.ErrorList[errors_1.ERROR_CODE.INVALID_REGISTER_PARAMETER].message
            });
        }
        if (lodash_1.isEmpty(beneficiaryBank)) {
            errors.push({
                code: errors_1.ERROR_CODE.INVALID_REGISTER_PARAMETER,
                key: 'beneficiaryBankCode',
                message: errors_1.ErrorList[errors_1.ERROR_CODE.INVALID_REGISTER_PARAMETER].message
            });
        }
        if (!currencyData) {
            errors.push({
                code: errors_1.ERROR_CODE.INVALID_REGISTER_PARAMETER,
                key: 'currency',
                message: errors_1.ErrorList[errors_1.ERROR_CODE.INVALID_REGISTER_PARAMETER].message
            });
        }
        const detail = `Error while fetching bank information and currency!`;
        logger_1.default.error(`inquiryBankAndCurrencyByIncomingCodes: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.JAGO, errors_1.ERROR_CODE.INVALID_REQUEST, errors);
    }
    return result;
});
const getCurrencyByCode = (currencyCode) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getCurrencyByCode : Payload details currency : ${currencyCode}`);
    const key = currencyKey + currencyCode;
    const cachedObject = yield redis_1.default.get(key);
    if (cachedObject) {
        return cachedObject;
    }
    logger_1.default.info(`getCurrencyByCode : Not available in cache fetching from configuration.`);
    const body = {
        query: currency_1.currencyByCodeQuery,
        variables: {
            code: currencyCode
        }
    };
    const result = yield httpClient_1.default.post('', body);
    const data = result.data.data;
    return data ? data.currency : undefined;
});
const getHolidaysByDateRange = (fromDate, toDate) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getHolidaysByDateRange : Payload details fromDate : ${fromDate}, toDate : ${toDate}`);
    const body = {
        query: holiday_1.default,
        variables: {
            fromDate,
            toDate
        }
    };
    const response = yield httpClient_1.default.post('', body);
    const errors = lodash_1.get(response, ['data', 'errors'], []);
    if (errors.length > 0) {
        throwGraphQLError('getHolidaysByDateRange', errors);
    }
    return lodash_1.get(response, ['data', 'data', 'holidays'], []);
});
const getBillingAggregatorPollingConfig = (billingAggregator) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getBillingAggregatorPollingConfig : Payload details billingAggregator : ${billingAggregator}`);
    const cachedObject = yield redis_1.default.mGet([billingAggregator.toString()], billingAggregatorPrefix, true);
    const billingAggregatorCachedObject = cachedObject && cachedObject[0];
    if (!lodash_1.isEmpty(billingAggregatorCachedObject)) {
        return billingAggregatorCachedObject;
    }
    const body = {
        query: billingAggregatorPollingConfig_1.billingAggregatorPollingConfigQuery,
        variables: {
            billingAggregator
        }
    };
    const { data } = yield httpClient_1.default.post('', body);
    return data.data ? data.data.billingAggregatorPollingConfig : undefined;
});
const getDecisionEngineConfig = (code) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getDecisionEngineConfig : Payload details code : ${code}`);
    const cachedObject = yield redis_1.default.get(`{ms-configuration:decisionEngineRule}:${code}`);
    if (cachedObject) {
        let decisionRules = [];
        for (let data of cachedObject) {
            delete data._id;
            for (let i = 0; i < data.rules.length; i++) {
                if (!data.rules[i].value) {
                    data.rules[i].value = null;
                }
                if (!data.rules[i].values) {
                    data.rules[i].values = null;
                }
            }
            decisionRules.push(data);
        }
        if (decisionRules.length > 0) {
            return decisionRules;
        }
    }
    const body = {
        query: decisionEngineConfigByCode_1.default,
        variables: {
            code: code
        }
    };
    const result = yield httpClient_1.default.post(pathOfConfiguration, body);
    const data = result.data.data;
    return data ? data.decisionEngineRules : undefined;
});
const getTransactionReportConfigsAndFileUpload = () => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getTransactionReportConfigsAndFileUpload : Transaction report config and file upload fetching.`);
    const body = {
        query: transactionReportConfigs_1.default
    };
    const result = yield httpClient_1.default.post(pathOfConfiguration, body);
    const data = result.data.data;
    return data
        ? [data.transactionReportConfigs, data.fileUploadParameters]
        : undefined;
});
/**
 * Get a biller identified by the given bank code.
 * @param bankCode
 */
const getBiller = (bankCode) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    logger_1.default.info(`getBiller : Fetching billers payload : ${bankCode}`);
    const cacheKey = getConfigurationCacheKey('biller', `billerByBankCode-${bankCode}`);
    const cachedBiller = yield redis_1.default.get(cacheKey);
    if (cachedBiller) {
        return cachedBiller;
    }
    logger_1.default.info(`getBiller : Not available in cache fetching from configuration.`);
    const requestBody = {
        query: billers_1.getBillersQuery,
        variables: { bankCode: bankCode }
    };
    const result = yield httpClient_1.default.post(pathOfConfiguration, requestBody);
    const biller = (_b = (_a = result.data.data) === null || _a === void 0 ? void 0 : _a.getbillerByBankCode, (_b !== null && _b !== void 0 ? _b : undefined));
    return biller;
});
const getMambuBlockingCodeByDays = (days) => __awaiter(void 0, void 0, void 0, function* () {
    var _c;
    const requestBody = {
        query: mambuBlockingDays_1.mambuBlockingDayQuery,
        variables: { days }
    };
    const result = yield httpClient_1.default.post(pathOfConfiguration, requestBody);
    const mambuBlockingDays = (_c = result.data.data) === null || _c === void 0 ? void 0 : _c.mambuBlockingDay;
    return mambuBlockingDays;
});
const updateCounter = (counterInput) => __awaiter(void 0, void 0, void 0, function* () {
    var _d;
    const requestBody = {
        query: counter_1.updateCounterMutation,
        variables: { counterInput }
    };
    const result = yield httpClient_1.default.post(pathOfConfiguration, requestBody);
    const counter = (_d = result.data.data) === null || _d === void 0 ? void 0 : _d.updateCounter;
    return counter;
});
const getCounterByCode = (code) => __awaiter(void 0, void 0, void 0, function* () {
    const prefixKey = `ms-configuration-counter`;
    const cachedCounter = yield redis_1.default.get(code, prefixKey);
    if (cachedCounter) {
        return cachedCounter;
    }
    const body = {
        query: counter_1.getCounterQuery,
        variables: { code }
    };
    const result = yield httpClient_1.default.post('', body);
    const data = result.data.data;
    return data ? data.counter : undefined;
});
const getServiceRecommendations = (refundable) => __awaiter(void 0, void 0, void 0, function* () {
    const body = {
        query: paymentConfigRules_1.getServiceRecommendationsQuery,
        variables: { refundable }
    };
    const result = yield httpClient_1.default.post('', body);
    const data = result.data.data;
    return data ? data.getServiceRecommendations : [];
});
const getWorkingTime = (category) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const body = {
            query: workingTimeTemplate_1.queryWorkingTimeTemplate,
            variables: { category }
        };
        const result = yield httpClient_1.default.post('', body);
        const errors = lodash_1.get(result, ['data', 'errors'], []);
        if (errors.length > 0) {
            throwGraphQLError('getWorkingTime', errors);
        }
        return result && result.data.data ? result.data.data.workingTimes : [];
    }
    catch (err) {
        const detail = `Missing working time for category: ${category}`;
        logger_1.default.error(`${detail} with exception ${err}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.JAGO, errors_1.ERROR_CODE.CANNOT_GET_CONFIG_PARAMETER);
    }
});
const getSavingProduct = (code) => __awaiter(void 0, void 0, void 0, function* () {
    const prefixKey = `ms-configuration-savingProduct`;
    const cachedSavingProduct = yield redis_1.default.get(code, prefixKey);
    if (cachedSavingProduct) {
        return cachedSavingProduct;
    }
    const body = {
        query: savingProduct_1.savingProductBaseQuery,
        variables: { code }
    };
    const result = yield httpClient_1.default.post('', body);
    const data = result.data.data;
    return data ? data.savingProduct : undefined;
});
const getBasicFees = (code) => __awaiter(void 0, void 0, void 0, function* () {
    const prefixKey = `ms-configuration-basicFee`;
    logger_1.default.info(`getBasicFees: fetching basic fee ${code}`);
    const cachedBasicFee = yield redis_1.default.get(code, prefixKey);
    if (cachedBasicFee) {
        return cachedBasicFee;
    }
    logger_1.default.info(`getBasicFees: Not available in cache fetching from configuration.`);
    const body = {
        query: basicFee_1.default,
        variables: { code }
    };
    const result = yield httpClient_1.default.post('', body);
    const data = result.data.data;
    return data ? data.basicFee : undefined;
});
const getTransactionCode = (code) => __awaiter(void 0, void 0, void 0, function* () {
    const prefixKey = `ms-configuration`;
    logger_1.default.info(`getTransactionCode: fetching Transaction code ${code}`);
    const cachedTransactionCode = yield redis_1.default.get(code, prefixKey);
    if (cachedTransactionCode) {
        return cachedTransactionCode;
    }
    logger_1.default.info(`getTransactionCode: Not available in cache fetching from configuration.`);
    const body = {
        query: transactionCode_1.default,
        variables: { code }
    };
    const result = yield httpClient_1.default.post('', body);
    const data = result.data.data;
    return data ? data.transactionCode : undefined;
});
const getParameterSettingByReferenceGroup = (type) => __awaiter(void 0, void 0, void 0, function* () {
    const prefixKey = `ms-configuration-parameter-setting`;
    logger_1.default.info(`getParameterSetting: fetching Parameter setting ${type}`);
    const cachedParameterSetting = yield redis_1.default.get(type, prefixKey);
    if (cachedParameterSetting) {
        return cachedParameterSetting;
    }
    logger_1.default.info(`getParameterSetting: Not available in cache fetching from configuration.`);
    const body = {
        query: parameterSetting_1.default,
        variables: { type }
    };
    const result = yield httpClient_1.default.post('', body);
    const data = result.data.data;
    return (data && data.parameterSettings) || [];
});
const getAllGeneralLedgers = () => __awaiter(void 0, void 0, void 0, function* () {
    const prefixKey = `{ms-configuration-generalLedger}:all?`;
    logger_1.default.info(`getGeneralLedgers: fetching General Ledgers`);
    const cachedGeneralLedgers = yield redis_1.default.get(prefixKey);
    if (!lodash_1.isEmpty(cachedGeneralLedgers)) {
        return cachedGeneralLedgers;
    }
    logger_1.default.info(`getGeneralLedgers: Not available in cache fetching from configuration.`);
    const body = {
        query: generalLedger_1.default
    };
    const result = yield httpClient_1.default.post('', body);
    const data = result.data.data;
    return (data && data.generalLedgers) || [];
});
const getNNSMapping = (reservedNnsPrefix) => __awaiter(void 0, void 0, void 0, function* () {
    const prefixKey = `ms-configuration-nnsMapping`;
    logger_1.default.info(`getNNSMapping: fetching NNS Mapping with nns ${reservedNnsPrefix}`);
    const cachedTransactionCode = yield redis_1.default.get(reservedNnsPrefix, prefixKey);
    if (cachedTransactionCode) {
        return cachedTransactionCode;
    }
    logger_1.default.info(`getNNSMapping: Not available in cache fetching from configuration.`);
    const body = {
        query: nnsMapping_1.default,
        variables: { reservedNnsPrefix }
    };
    const result = yield httpClient_1.default.post('', body);
    const data = result.data.data;
    return data ? data.getNNSMappingByNNS : undefined;
});
const configurationRepository = logger_1.wrapLogs({
    getLimitGroupsByCodes,
    getFeeRule,
    getTransactionCategory,
    getBankByRtolCode,
    getBankCodeMapping,
    getPaymentConfigRules,
    inquiryBankAndCurrencyByIncomingCodes,
    getCurrencyByCode,
    getHolidaysByDateRange,
    getBillingAggregatorPollingConfig,
    getDecisionEngineConfig,
    getTransactionReportConfigsAndFileUpload,
    getBiller,
    getMambuBlockingCodeByDays,
    updateCounter,
    getCounterByCode,
    getServiceRecommendations,
    getWorkingTime,
    getSavingProduct,
    getBasicFees,
    getTransactionCode,
    getParameterSettingByReferenceGroup,
    getAllGeneralLedgers,
    getNNSMapping
});
exports.default = configurationRepository;
//# sourceMappingURL=configuration.repository.js.map