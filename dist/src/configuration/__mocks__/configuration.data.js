"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
exports.getFeeRuleData = () => {
    return {
        description: 'Overseas ATM Withdrawal Rule',
        basicFeeMapping: [
            {
                basicFeeCode: 'CW003',
                basicFee: {
                    code: 'CW003',
                    feeAmountFixed: 25000,
                    feeAmountPercentage: null,
                    customerTc: 'FCD05',
                    customerTcInfo: {
                        channel: 'FCD05_channel'
                    },
                    subsidiary: true,
                    subsidiaryAmount: 7500,
                    debitGlNumber: '01.01.00.00.00.00',
                    creditGlNumber: '01.02.00.00.00.00'
                }
            }
        ]
    };
};
exports.getFeeRuleDataWithoutGLNumber = () => {
    return {
        description: 'Overseas ATM Withdrawal Rule',
        basicFeeMapping: [
            {
                basicFeeCode: 'CW003',
                basicFee: {
                    code: 'CW003',
                    feeAmountFixed: 25000,
                    feeAmountPercentage: null,
                    customerTc: 'FCD05',
                    customerTcInfo: {
                        channel: 'FCD05_channel'
                    },
                    subsidiary: false
                }
            }
        ]
    };
};
exports.getFeeRuleDataWithoutAnySubsidiaryType = () => ({
    description: 'Overseas ATM Withdrawal Rule',
    basicFeeMapping: [
        {
            basicFeeCode: 'CW003',
            basicFee: {
                code: 'CW003',
                feeAmountFixed: 25000,
                feeAmountPercentage: null,
                customerTc: 'FCD05',
                customerTcInfo: {
                    channel: 'FCD05_channel'
                },
                subsidiary: true,
                debitGlNumber: '01.01.00.00.00.00',
                creditGlNumber: '01.02.00.00.00.00'
            }
        }
    ]
});
exports.getFeeRuleDataWithoutAnyFeeType = () => ({
    description: 'Overseas ATM Withdrawal Rule',
    basicFeeMapping: [
        {
            basicFeeCode: 'CW003',
            basicFee: {
                code: 'CW003',
                feeAmountFixed: null,
                feeAmountPercentage: null,
                customerTc: 'FCD05',
                customerTcInfo: {
                    channel: 'FCD05_channel'
                },
                subsidiary: true,
                debitGlNumber: '01.01.00.00.00.00',
                creditGlNumber: '01.02.00.00.00.00'
            }
        }
    ]
});
exports.getTransactionCodeInfoData = () => ({
    code: 'TFD20',
    defaultCategoryCode: 'C056',
    minAmountPerTx: 1,
    maxAmountPerTx: 9999999999,
    feeRules: 'ZERO_FEE_RULE',
    channel: 'SAD01'
});
exports.getSknPaymentConfigRule = () => [
    {
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
        source: module_common_1.BankChannelEnum.INTERNAL,
        target: module_common_1.BankChannelEnum.EXTERNAL,
        sourceAccType: ['MA'],
        targetAccType: ['ANY'],
        sameCIF: 'ANY',
        amountRangeFrom: 50000001,
        amountRangeTo: 100000000,
        rtolDailyUsage: 'ANY',
        sknDailyUsage: 'YES',
        rtgsDailyUsage: 'ANY',
        serviceRecommendation: ['SKN'],
        paymentServiceMappings: [
            {
                paymentServiceCode: 'SKN',
                debitTransactionCode: [
                    {
                        transactionCode: 'TFD50',
                        transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD50' })
                    }
                ],
                beneficiaryBankCodeType: 'REMITTANCE',
                transactionAuthenticationChecking: true,
                blocking: true,
                requireThirdPartyOutgoingId: true,
                cutOffTime: 'SKN'
            }
        ]
    }
];
exports.getSknAndRtgsPaymentConfigRule = () => [
    {
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
        source: module_common_1.BankChannelEnum.INTERNAL,
        target: module_common_1.BankChannelEnum.EXTERNAL,
        sourceAccType: ['MA'],
        targetAccType: ['ANY'],
        sameCIF: 'ANY',
        amountRangeFrom: 100000001,
        amountRangeTo: 500000000,
        rtolDailyUsage: 'ANY',
        sknDailyUsage: 'YES',
        rtgsDailyUsage: 'YES',
        serviceRecommendation: ['SKN', 'RTGS'],
        paymentServiceMappings: [
            {
                paymentServiceCode: 'SKN',
                debitTransactionCode: [
                    {
                        transactionCode: 'TFD50',
                        transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD50' })
                    }
                ],
                beneficiaryBankCodeType: 'REMITTANCE',
                transactionAuthenticationChecking: true,
                blocking: true,
                requireThirdPartyOutgoingId: true,
                cutOffTime: 'SKN'
            },
            {
                paymentServiceCode: 'RTGS',
                debitTransactionCode: [
                    {
                        transactionCode: 'TFD60',
                        transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD60' })
                    }
                ],
                beneficiaryBankCodeType: 'REMITTANCE',
                transactionAuthenticationChecking: true,
                blocking: true,
                requireThirdPartyOutgoingId: true,
                cutOffTime: 'RTGS'
            }
        ]
    }
];
exports.getRDNPaymentConfigRule = () => [
    {
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.RDN,
        source: module_common_1.BankChannelEnum.INTERNAL,
        target: module_common_1.BankChannelEnum.EXTERNAL,
        sourceAccType: ['RDS'],
        targetAccType: ['ANY'],
        sameCIF: 'ANY',
        amountRangeFrom: 0,
        amountRangeTo: 500000000,
        rtolDailyUsage: 'ANY',
        sknDailyUsage: 'YES',
        rtgsDailyUsage: 'ANY',
        serviceRecommendation: ['SKN_SHARIA'],
        paymentServiceMappings: [
            {
                paymentServiceCode: 'SKN_SHARIA',
                debitTransactionCode: [
                    {
                        transactionCode: 'TFD55',
                        transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD55' })
                    }
                ],
                beneficiaryBankCodeType: 'REMITTANCE',
                transactionAuthenticationChecking: true,
                blocking: true,
                requireThirdPartyOutgoingId: true,
                cutOffTime: 'SKN'
            }
        ]
    }
];
exports.getBifastPaymentConfigRule = () => [
    {
        source: module_common_1.BankChannelEnum.INTERNAL,
        target: module_common_1.BankChannelEnum.EXTERNAL,
        sourceAccType: ['MA', 'DC', 'PY', 'MBA', 'RDN', 'MPO', 'MGO'],
        targetAccType: ['ANY'],
        sameCIF: 'ANY',
        amountRangeFrom: 10000,
        amountRangeTo: 250000000,
        rtolDailyUsage: 'ANY',
        sknDailyUsage: 'ANY',
        rtgsDailyUsage: 'ANY',
        serviceRecommendation: ['BIFAST_OUTGOING'],
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
        paymentServiceMappings: [
            {
                paymentServiceCode: 'BIFAST_OUTGOING',
                beneficiaryBankCodeType: 'RTOL',
                debitTransactionCode: [
                    {
                        transactionCode: 'OTD10',
                        transactionCodeInfo: {
                            code: 'OTD10',
                            limitGroupCode: 'L019',
                            feeRules: 'BIFAST_FEE_RULE',
                            minAmountPerTx: 10000,
                            maxAmountPerTx: 250000000,
                            defaultCategoryCode: 'C056',
                            channel: 'OTD10',
                            awardGroupCounter: 'Bonus_Transfer'
                        }
                    }
                ],
                creditTransactionCode: [],
                transactionAuthenticationChecking: true,
                blocking: true,
                requireThirdPartyOutgoingId: true
            }
        ]
    }
];
exports.getBifastShariaPaymentConfigRule = () => [
    {
        source: module_common_1.BankChannelEnum.INTERNAL,
        target: module_common_1.BankChannelEnum.EXTERNAL,
        sourceAccType: ['SMA', 'SDC', 'RDS'],
        targetAccType: ['ANY'],
        sameCIF: 'ANY',
        amountRangeFrom: 10000,
        amountRangeTo: 250000000,
        rtolDailyUsage: 'ANY',
        sknDailyUsage: 'ANY',
        rtgsDailyUsage: 'ANY',
        serviceRecommendation: ['BIFAST_OUTGOING_SHARIA'],
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
        paymentServiceMappings: [
            {
                paymentServiceCode: 'BIFAST_OUTGOING_SHARIA',
                beneficiaryBankCodeType: 'RTOL',
                debitTransactionCode: [
                    {
                        transactionCode: 'OTD15',
                        transactionCodeInfo: {
                            code: 'OTD15',
                            limitGroupCode: 'L019',
                            feeRules: 'BIFAST_SHARIA_FEE_RULE',
                            minAmountPerTx: 10000,
                            maxAmountPerTx: 250000000,
                            defaultCategoryCode: 'C056',
                            channel: 'OTD15',
                            awardGroupCounter: 'Bonus_Transfer'
                        }
                    }
                ],
                creditTransactionCode: [],
                transactionAuthenticationChecking: true,
                blocking: true,
                requireThirdPartyOutgoingId: true
            }
        ]
    }
];
exports.getRtolPaymentConfigRule = () => [
    {
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
        source: module_common_1.BankChannelEnum.INTERNAL,
        target: module_common_1.BankChannelEnum.EXTERNAL,
        sourceAccType: ['MA', 'DC'],
        targetAccType: ['ANY'],
        sameCIF: 'ANY',
        amountRangeFrom: 0,
        amountRangeTo: 50000000,
        rtolDailyUsage: 'YES',
        sknDailyUsage: 'ANY',
        rtgsDailyUsage: 'ANY',
        serviceRecommendation: ['RTOL'],
        paymentServiceMappings: [
            {
                paymentServiceCode: 'RTOL',
                debitTransactionCode: [
                    {
                        interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                        transactionCode: 'TFD30',
                        transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD30' })
                    },
                    {
                        interchange: module_common_1.BankNetworkEnum.ALTO,
                        transactionCode: 'TFD40',
                        transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD40' })
                    }
                ],
                beneficiaryBankCodeType: 'RTOL',
                transactionAuthenticationChecking: true,
                blocking: true,
                requireThirdPartyOutgoingId: true
            }
        ]
    }
];
exports.getPaymentConfigRulesData = () => {
    return [
        {
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.EXTERNAL,
            sourceAccType: ['MA', 'DC', 'PY', 'MBA', 'RDN', 'MPO', 'MGO'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 10000,
            amountRangeTo: 250000000,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['BIFAST_OUTGOING'],
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'BIFAST_OUTGOING',
                    beneficiaryBankCodeType: 'RTOL',
                    debitTransactionCode: [
                        {
                            transactionCode: 'OTD10',
                            transactionCodeInfo: {
                                code: 'OTD10',
                                limitGroupCode: 'L019',
                                feeRules: 'BIFAST_FEE_RULE',
                                minAmountPerTx: 10000,
                                maxAmountPerTx: 250000000,
                                defaultCategoryCode: 'C056',
                                channel: 'OTD10',
                                awardGroupCounter: 'Bonus_Transfer'
                            }
                        }
                    ],
                    creditTransactionCode: [],
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true
                }
            ]
        },
        {
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.EXTERNAL,
            sourceAccType: ['SMA', 'SDC', 'RDS'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 10000,
            amountRangeTo: 250000000,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['BIFAST_OUTGOING_SHARIA'],
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'BIFAST_OUTGOING_SHARIA',
                    beneficiaryBankCodeType: 'RTOL',
                    debitTransactionCode: [
                        {
                            transactionCode: 'OTD15',
                            transactionCodeInfo: {
                                code: 'OTD15',
                                limitGroupCode: 'L019',
                                feeRules: 'BIFAST_SHARIA_FEE_RULE',
                                minAmountPerTx: 10000,
                                maxAmountPerTx: 250000000,
                                defaultCategoryCode: 'C056',
                                channel: 'OTD15',
                                awardGroupCounter: 'Bonus_Transfer'
                            }
                        }
                    ],
                    creditTransactionCode: [],
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.WALLET,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.PARTNER,
            sourceAccType: ['MA', 'DC'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 0,
            amountRangeTo: 10000000,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['SIT02'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'SIT02',
                    debitTransactionCode: [
                        {
                            transactionCode: 'WTD01',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'WTD01' })
                        }
                    ],
                    creditTransactionCode: [
                        {
                            transactionCode: 'WTC01',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'WTD01' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: true,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['MA', 'DC'],
            targetAccType: ['ANY'],
            sameCIF: 'NO',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['SIT01'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'SIT01',
                    debitTransactionCode: [
                        {
                            transactionCode: 'TFD20',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD20' })
                        }
                    ],
                    creditTransactionCode: [
                        {
                            transactionCode: 'TFC20',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFC20' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: true,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['MA', 'DC'],
            targetAccType: ['FS'],
            sameCIF: 'YES',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['SAT01'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'SAT01',
                    debitTransactionCode: [
                        {
                            transactionCode: 'SAD01',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'SAD01' })
                        }
                    ],
                    creditTransactionCode: [
                        {
                            transactionCode: 'SAC01',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'SAC01' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: false,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['FS'],
            targetAccType: ['MA', 'DC'],
            sameCIF: 'YES',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['SAT02'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'SAT02',
                    debitTransactionCode: [
                        {
                            transactionCode: 'SAD02',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'SAD02' })
                        }
                    ],
                    creditTransactionCode: [
                        {
                            transactionCode: 'SAC02',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'SAC02' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: false,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['MA'],
            targetAccType: ['DC'],
            sameCIF: 'YES',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['SAT03'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'SAT03',
                    debitTransactionCode: [
                        {
                            transactionCode: 'CTD01',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'CTD01' })
                        }
                    ],
                    creditTransactionCode: [
                        {
                            transactionCode: 'CTC01',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'CTC01' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: false,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['DC'],
            targetAccType: ['MA'],
            sameCIF: 'YES',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['SAT04'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'SAT04',
                    debitTransactionCode: [
                        {
                            transactionCode: 'CRD02',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'CRD02' })
                        }
                    ],
                    creditTransactionCode: [
                        {
                            transactionCode: 'CRC02',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'CRC02' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: false,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.EXTERNAL,
            sourceAccType: ['MA', 'DC'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 0,
            amountRangeTo: 50000000,
            rtolDailyUsage: 'YES',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['RTOL'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'RTOL',
                    debitTransactionCode: [
                        {
                            interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                            transactionCode: 'TFD30',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD30' })
                        },
                        {
                            interchange: module_common_1.BankNetworkEnum.ALTO,
                            transactionCode: 'TFD40',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD40' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.EXTERNAL,
            sourceAccType: ['MA'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 50000001,
            amountRangeTo: 100000000,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'YES',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['SKN'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'SKN',
                    debitTransactionCode: [
                        {
                            transactionCode: 'TFD50',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD50' })
                        }
                    ],
                    beneficiaryBankCodeType: 'REMITTANCE',
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true,
                    cutOffTime: 'SKN'
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.EXTERNAL,
            sourceAccType: ['SMA', 'SDC'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 100000001,
            amountRangeTo: 500000000,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'YES',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['SKN_SHARIA'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'SKN_SHARIA',
                    debitTransactionCode: [
                        {
                            transactionCode: 'TFD55',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD55' })
                        }
                    ],
                    beneficiaryBankCodeType: 'REMITTANCE',
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true,
                    cutOffTime: 'SKN'
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.EXTERNAL,
            sourceAccType: ['MA'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 100000001,
            amountRangeTo: 500000000,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'YES',
            rtgsDailyUsage: 'YES',
            serviceRecommendation: ['SKN', 'RTGS'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'SKN',
                    debitTransactionCode: [
                        {
                            transactionCode: 'TFD50',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD50' })
                        }
                    ],
                    beneficiaryBankCodeType: 'REMITTANCE',
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true,
                    cutOffTime: 'SKN'
                },
                {
                    paymentServiceCode: 'RTGS',
                    debitTransactionCode: [
                        {
                            transactionCode: 'TFD60',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD60' })
                        }
                    ],
                    beneficiaryBankCodeType: 'REMITTANCE',
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true,
                    cutOffTime: 'RTGS'
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.EXTERNAL,
            sourceAccType: ['SMA', 'SDC'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 100000001,
            amountRangeTo: 500000000,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'YES',
            rtgsDailyUsage: 'YES',
            serviceRecommendation: ['SKN_SHARIA', 'RTGS_SHARIA'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'SKN_SHARIA',
                    debitTransactionCode: [
                        {
                            transactionCode: 'TFD55',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD55' })
                        }
                    ],
                    beneficiaryBankCodeType: 'REMITTANCE',
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true,
                    cutOffTime: 'SKN'
                },
                {
                    paymentServiceCode: 'RTGS_SHARIA',
                    debitTransactionCode: [
                        {
                            transactionCode: 'TFD65',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD65' })
                        }
                    ],
                    beneficiaryBankCodeType: 'REMITTANCE',
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true,
                    cutOffTime: 'RTGS'
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.EXTERNAL,
            sourceAccType: ['MA'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 100000001,
            amountRangeTo: 500000000,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'NO',
            rtgsDailyUsage: 'YES',
            serviceRecommendation: ['RTGS'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'RTGS',
                    debitTransactionCode: [
                        {
                            transactionCode: 'TFD60',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD60' })
                        }
                    ],
                    beneficiaryBankCodeType: 'REMITTANCE',
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true,
                    cutOffTime: 'RTGS'
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.EXTERNAL,
            sourceAccType: ['MA'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 100000001,
            amountRangeTo: 500000000,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'YES',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['SKN'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'SKN',
                    debitTransactionCode: [
                        {
                            transactionCode: 'TFD50',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD50' })
                        }
                    ],
                    beneficiaryBankCodeType: 'REMITTANCE',
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true,
                    cutOffTime: 'SKN'
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.EXTERNAL,
            sourceAccType: ['MA'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 500000001,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'YES',
            serviceRecommendation: ['RTGS'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'RTGS',
                    debitTransactionCode: [
                        {
                            transactionCode: 'TFD60',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD60' })
                        }
                    ],
                    beneficiaryBankCodeType: 'REMITTANCE',
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true,
                    cutOffTime: 'RTGS'
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.EXTERNAL,
            sourceAccType: ['SMA', 'SDC'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 500000001,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'YES',
            serviceRecommendation: ['RTGS_SHARIA'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'RTGS_SHARIA',
                    debitTransactionCode: [
                        {
                            transactionCode: 'TFD65',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD65' })
                        }
                    ],
                    beneficiaryBankCodeType: 'REMITTANCE',
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true,
                    cutOffTime: 'RTGS'
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.GOBILLS,
            sourceAccType: ['MA'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['BIL01'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'BIL01',
                    debitTransactionCode: [
                        {
                            transactionCode: 'BID01',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'BID01' })
                        }
                    ],
                    beneficiaryBankCodeType: 'BILLER',
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['FS'],
            targetAccType: ['FS'],
            sameCIF: 'YES',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['SAT05'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'SAT05',
                    debitTransactionCode: [
                        {
                            transactionCode: 'SAD03',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'SAD03' })
                        }
                    ],
                    creditTransactionCode: [
                        {
                            transactionCode: 'SAC03',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'SAC03' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: false,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.GIN_PAY,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['MA', 'DC'],
            targetAccType: ['MA'],
            sameCIF: 'NO',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['SIT03'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'SIT03',
                    debitTransactionCode: [
                        {
                            transactionCode: 'GPD01',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'GPD01' })
                        }
                    ],
                    creditTransactionCode: [
                        {
                            transactionCode: 'GPC01',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'GPC01' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: false,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.MDR,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.ANY,
            sourceAccType: ['MA'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['MDR'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'MDR',
                    debitTransactionCode: [
                        {
                            transactionCode: 'MDD01',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'MDD01' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: true,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.CASHBACK,
            source: module_common_1.BankChannelEnum.ANY,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['ANY'],
            targetAccType: ['MA'],
            sameCIF: 'ANY',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['CASHBACK'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'CASHBACK',
                    creditTransactionCode: [
                        {
                            transactionCode: 'CBC01',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'CBC01' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: true,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.WALLET,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.EXTERNAL,
            sourceAccType: ['MA'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 0,
            amountRangeTo: 10000000,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['RTOL_WALLET'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'RTOL_WALLET',
                    debitTransactionCode: [
                        {
                            interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                            transactionCode: 'WTD02',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'WTD02' })
                        },
                        {
                            interchange: module_common_1.BankNetworkEnum.ALTO,
                            transactionCode: 'WTD03',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'WTD03' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.PAYME,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['MA'],
            targetAccType: ['ANY'],
            sameCIF: 'NO',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['PAYME'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'PAYME',
                    debitTransactionCode: [
                        {
                            transactionCode: 'PYD01',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'PYD01' })
                        }
                    ],
                    creditTransactionCode: [
                        {
                            transactionCode: 'PYC01',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'PYC01' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: true,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.CREDIT_CARD,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.EXTERNAL,
            sourceAccType: ['MA'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 0,
            amountRangeTo: 50000000,
            rtolDailyUsage: 'YES',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['RTOL_CC'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'RTOL_CC',
                    debitTransactionCode: [
                        {
                            interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                            transactionCode: 'BID02',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'BID02' })
                        },
                        {
                            interchange: module_common_1.BankNetworkEnum.ALTO,
                            transactionCode: 'BID03',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'BID03' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: true,
                    blocking: true,
                    requireThirdPartyOutgoingId: true
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.WALLET,
            source: module_common_1.BankChannelEnum.ANY,
            target: module_common_1.BankChannelEnum.PARTNER,
            sourceAccType: ['ANY'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 0,
            amountRangeTo: 10000000,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['VA_TOPUP_EXTERNAL'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'VA_TOPUP_EXTERNAL',
                    creditTransactionCode: [
                        {
                            transactionCode: 'WTC03',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'WTC03' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: false,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.WALLET,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['ANY'],
            targetAccType: ['MA'],
            sameCIF: 'ANY',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['VA_WITHDRAW'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'VA_WITHDRAW',
                    debitTransactionCode: [
                        {
                            transactionCode: 'WWD01',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'WWD01' })
                        }
                    ],
                    creditTransactionCode: [
                        {
                            transactionCode: 'WWC01',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'WWC01' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: true,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TD_SHARIA_PLACEMENT,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['MA'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['SAT08'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'SAT08',
                    debitTransactionCode: [
                        {
                            transactionCode: 'TDD31',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TDD31' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: false,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.TD_SHARIA_REPAYMENT,
            source: module_common_1.BankChannelEnum.INTERNAL,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['ANY'],
            targetAccType: ['MA'],
            sameCIF: 'ANY',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['SAT09'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'SAT09',
                    creditTransactionCode: [
                        {
                            interchange: module_common_1.BankNetworkEnum.PRINCIPAL,
                            transactionCode: 'TDC32',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TDC32' })
                        },
                        {
                            interchange: module_common_1.BankNetworkEnum.PROFIT_SHARING,
                            transactionCode: 'ISC24',
                            transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'ISC24' })
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: false,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        },
        {
            source: module_common_1.BankChannelEnum.EXTERNAL,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['ANY'],
            targetAccType: ['MA', 'FS', 'DC', 'PY', 'GA', 'MBA', 'RDN', 'MPO', 'MGO'],
            sameCIF: 'ANY',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['BIFAST_INCOMING'],
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.INCOMING_BIFAST,
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'BIFAST_INCOMING',
                    beneficiaryBankCodeType: 'RTOL',
                    debitTransactionCode: [],
                    creditTransactionCode: [
                        {
                            interchange: undefined,
                            transactionCode: 'OTC20',
                            transactionCodeInfo: {
                                code: 'OTC20',
                                feeRules: 'ZERO_FEE_RULE',
                                minAmountPerTx: 1,
                                maxAmountPerTx: 9999999999,
                                defaultCategoryCode: 'C057',
                                channel: 'OTC20'
                            }
                        }
                    ],
                    inquiryFeeRule: undefined,
                    transactionAuthenticationChecking: true,
                    blocking: false,
                    requireThirdPartyOutgoingId: false,
                    cutOffTime: undefined
                }
            ]
        },
        {
            source: module_common_1.BankChannelEnum.EXTERNAL,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['ANY'],
            targetAccType: ['SMA', 'SFS', 'SDC', 'SGA', 'RDS', 'SBA'],
            sameCIF: 'ANY',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['BIFAST_INCOMING_SHARIA'],
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.INCOMING_BIFAST,
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'BIFAST_INCOMING_SHARIA',
                    beneficiaryBankCodeType: 'RTOL',
                    debitTransactionCode: [],
                    creditTransactionCode: [
                        {
                            interchange: undefined,
                            transactionCode: 'OTC25',
                            transactionCodeInfo: {
                                code: 'OTC25',
                                feeRules: 'ZERO_FEE_RULE',
                                minAmountPerTx: 1,
                                maxAmountPerTx: 9999999999,
                                defaultCategoryCode: 'C057',
                                channel: 'OTC25'
                            }
                        }
                    ],
                    inquiryFeeRule: undefined,
                    transactionAuthenticationChecking: true,
                    blocking: false,
                    requireThirdPartyOutgoingId: false,
                    cutOffTime: undefined
                }
            ]
        },
        {
            source: module_common_1.BankChannelEnum.EXTERNAL,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['ANY'],
            targetAccType: ['ANY'],
            sameCIF: 'ANY',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['REFUND_GENERAL'],
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.VOID_BIFAST,
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'REFUND_GENERAL',
                    beneficiaryBankCodeType: 'RTOL',
                    debitTransactionCode: [],
                    creditTransactionCode: [
                        {
                            interchange: undefined,
                            transactionCode: 'RGC01',
                            transactionCodeInfo: {
                                code: 'RGC01',
                                limitGroupCode: undefined,
                                feeRules: 'GENERAL_REFUND_FEE_RULE',
                                minAmountPerTx: 1,
                                maxAmountPerTx: 9999999999,
                                defaultCategoryCode: 'C054',
                                channel: 'RGC01',
                                awardGroupCounter: undefined
                            }
                        }
                    ],
                    inquiryFeeRule: undefined,
                    transactionAuthenticationChecking: false,
                    blocking: false,
                    requireThirdPartyOutgoingId: false,
                    cutOffTime: undefined
                }
            ]
        },
        {
            paymentServiceType: module_common_1.PaymentServiceTypeEnum.BONUS_INTEREST,
            source: module_common_1.BankChannelEnum.ANY,
            target: module_common_1.BankChannelEnum.INTERNAL,
            sourceAccType: ['ANY'],
            targetAccType: ['MA', 'SMA'],
            sameCIF: 'ANY',
            amountRangeFrom: 0,
            amountRangeTo: 999999999999,
            rtolDailyUsage: 'ANY',
            sknDailyUsage: 'ANY',
            rtgsDailyUsage: 'ANY',
            serviceRecommendation: ['BONUS_INTEREST'],
            paymentServiceMappings: [
                {
                    paymentServiceCode: 'BONUS_INTEREST',
                    creditTransactionCode: [
                        {
                            transactionCode: 'IBC01',
                            interchange: module_common_1.BankNetworkEnum.LFS,
                            transactionCodeInfo: {
                                code: 'IBC01',
                                defaultCategoryCode: 'C046',
                                minAmountPerTx: 1,
                                maxAmountPerTx: 99999999999,
                                feeRules: 'INTEREST_TAX_RULE',
                                channel: 'CBC01'
                            }
                        },
                        {
                            transactionCode: 'IBC02',
                            interchange: module_common_1.BankNetworkEnum.MFS,
                            transactionCodeInfo: {
                                code: 'IBC01',
                                defaultCategoryCode: 'C046',
                                minAmountPerTx: 1,
                                maxAmountPerTx: 99999999999,
                                feeRules: 'INTEREST_TAX_RULE',
                                channel: 'CBC01'
                            }
                        }
                    ],
                    beneficiaryBankCodeType: 'RTOL',
                    transactionAuthenticationChecking: true,
                    blocking: false,
                    requireThirdPartyOutgoingId: false
                }
            ]
        }
    ];
};
exports.getLimitGroupsData = () => {
    return [
        {
            code: 'L002',
            dailyLimitAmount: 999999999999
        },
        {
            code: 'L003',
            dailyLimitAmount: 700000000
        },
        {
            code: 'L004',
            dailyLimitAmount: 800000000
        },
        {
            code: 'L009',
            dailyLimitAmount: 100000000
        },
        {
            code: 'L019',
            dailyLimitAmount: 250000000
        }
    ];
};
exports.getBankCodeData = () => {
    return {
        bankCodeId: 'BC001',
        name: 'GoPay',
        rtolCode: '022',
        remittanceCode: undefined,
        billerCode: undefined,
        companyName: 'PT. Aplikasi Karya Anak Bangsa',
        isBersamaMember: false,
        isAltoMember: false,
        firstPriority: module_common_1.BankNetworkEnum.ARTAJASA,
        channel: module_common_1.BankChannelEnum.EXTERNAL,
        type: 'Wallet',
        ordering: 170,
        images: '/assets/picture/institutional_code/BC001.png',
        prefix: '2849',
        isInternal: false,
        supportedChannels: [module_common_1.BankChannelEnum.BIFAST, module_common_1.BankChannelEnum.EXTERNAL]
    };
};
exports.getHolidaysWithDateRangeData = () => [
    {
        name: `New Year's Day`,
        fromDate: new Date('2020-01-01T00:00:00.000Z'),
        toDate: new Date('2020-01-01T00:00:00.000Z')
    },
    {
        name: 'Weekend',
        fromDate: new Date('2020-01-05T00:00:00.000Z'),
        toDate: new Date('2020-01-05T00:00:00.000Z')
    }
];
exports.getHolidaysWithDateRangeDataWithDate = (date) => {
    const holidayFirstDate = new Date(date);
    holidayFirstDate.setDate(holidayFirstDate.getDate() + 1);
    const holidaySecondDate = new Date(date);
    holidaySecondDate.setDate(holidaySecondDate.getDate() + 2);
    return [
        {
            name: `New Year's Day`,
            fromDate: date,
            toDate: holidayFirstDate
        },
        {
            name: 'Weekend',
            fromDate: holidaySecondDate,
            toDate: holidaySecondDate
        }
    ];
};
exports.getSavingProductConfig = () => [
    {
        encodedKey: '8a80c48d72c145300172c1b4b89903d8',
        code: 'MA',
        blockingCapability: true,
        deletionCapability: false,
        goalCapability: false,
        maximumAccountPerUser: 1,
        monthlyFeeCode: 'MF001',
        currency: 'IDR',
        type: 'CURRENT_ACCOUNT',
        initialAmount: 0,
        maximumBalancePerAccount: 999999999999
    }
];
exports.getBillingAggregatorPollingConfig = () => [
    {
        billingAggregator: module_common_1.Enum.BillingAggregator.GOBILLS,
        configs: [
            {
                type: module_common_1.Enum.BillingAggregatorPollingConfigType.RELATIVE,
                delayTime: '300'
            },
            {
                type: module_common_1.Enum.BillingAggregatorPollingConfigType.RELATIVE,
                delayTime: '500'
            },
            {
                type: module_common_1.Enum.BillingAggregatorPollingConfigType.ABSOLUTE,
                delayTime: '17 15 10'
            }
        ]
    }
];
exports.getBillingAggregatorPollingConfigErrorResponse = () => {
    return {
        error: {
            errors: [
                [
                    {
                        message: 'Unknown type "BillingAggregator". Did you mean "BillingAggregatorPollingConfig"?',
                        locations: [
                            {
                                line: 1,
                                column: 58
                            }
                        ],
                        extensions: {
                            code: 'GRAPHQL_VALIDATION_FAILED',
                            exception: {
                                stacktrace: [
                                    'GraphQLError: Unknown type "BillingAggregator". Did you mean "BillingAggregatorPollingConfig"?',
                                    '    at Object.NamedType (/app/node_modules/graphql/validation/rules/KnownTypeNames.js:61:29)',
                                    '    at Object.enter (/app/node_modules/graphql/language/visitor.js:324:29)',
                                    '    at Object.enter (/app/node_modules/graphql/language/visitor.js:375:25)',
                                    '    at visit (/app/node_modules/graphql/language/visitor.js:242:26)',
                                    '    at Object.validate (/app/node_modules/graphql/validation/validate.js:73:24)',
                                    '    at validate (/app/node_modules/apollo-server-core/dist/requestPipeline.js:210:32)',
                                    '    at Object.<anonymous> (/app/node_modules/apollo-server-core/dist/requestPipeline.js:125:42)',
                                    '    at Generator.next (<anonymous>)',
                                    '    at fulfilled (/app/node_modules/apollo-server-core/dist/requestPipeline.js:5:58)',
                                    '    at process._tickCallback (internal/process/next_tick.js:68:7)'
                                ]
                            }
                        }
                    }
                ]
            ]
        }
    };
};
exports.getCurrencyData = () => ({
    code: 'VND',
    isoNumber: 704,
    name: 'đồng Việt Nam'
});
exports.getDecisionEngineConfiguration = () => ({
    functionality: 'TRANSFER',
    description: 'Internal JAGO Transfer',
    serviceCode: 'RTOL_WALLET',
    rules: [
        {
            property: 'sourceAccountType',
            operator: 'EQUAL',
            value: 'FS'
        },
        {
            property: 'sourceCIF',
            operator: 'EQUAL',
            value: '#beneficiaryCIF'
        },
        {
            property: 'sourceBankCodeChannel',
            operator: 'EQUAL',
            value: 'INTERNAL'
        },
        {
            property: 'sourceBankCodeChannel',
            operator: 'IN',
            values: ['EXTERNAL', 'INTERNAL']
        },
        {
            property: 'transactionAmount',
            operator: 'LTE',
            value: 1000
        },
        {
            property: 'transactionAmount',
            operator: 'LTE',
            value: '#limitAmount'
        }
    ]
});
exports.getDecisionEngineConfigurationRedis = [
    {
        functionality: 'TRANSFER',
        description: 'Internal JAGO Transfer',
        serviceCode: 'SIT01',
        rules: [
            {
                property: 'sourceAccountRole',
                operator: 'EQUAL',
                value: 'MOVER'
            },
            {
                property: 'beneficiaryAccountRole',
                operator: 'EQUAL',
                value: 'OWNER'
            },
            { property: 'sourceAccountType', operator: 'EQUAL', value: 'FS' },
            {
                property: 'beneficiaryAccountType',
                operator: 'EQUAL',
                value: 'MA'
            },
            {
                property: 'transactionAmount',
                operator: 'LTE',
                value: '#moverRemainingDailyUsage'
            }
        ],
        _id: '5f5e5fc85af3157262825a8f'
    },
    {
        functionality: 'TRANSFER',
        description: 'Internal JAGO Transfer',
        serviceCode: 'SIT01',
        rules: [
            {
                property: 'sourceAccountRole',
                operator: 'EQUAL',
                value: 'MOVER'
            },
            {
                property: 'beneficiaryAccountRole',
                operator: 'EQUAL',
                value: 'OWNER'
            },
            { property: 'sourceAccountType', operator: 'EQUAL', value: 'DC' },
            {
                property: 'transactionAmount',
                operator: 'LTE',
                value: '#moverRemainingDailyUsage'
            }
        ],
        _id: '5f5e5fc85af3157262825a95'
    },
    {
        functionality: 'TRANSFER',
        description: 'Internal JAGO Transfer',
        serviceCode: 'SIT01',
        rules: [
            {
                property: 'sourceAccountRole',
                operator: 'EQUAL',
                value: 'MOVER'
            },
            {
                property: 'beneficiaryAccountRole',
                operator: 'EQUAL',
                value: 'NONE'
            },
            { property: 'sourceAccountType', operator: 'EQUAL', value: 'DC' },
            {
                property: 'transactionAmount',
                operator: 'LTE',
                value: '#moverRemainingDailyUsage'
            }
        ],
        _id: '5f5e5fc85af3157262825a9b'
    },
    {
        functionality: 'TRANSFER',
        description: 'Internal JAGO Transfer',
        serviceCode: 'SIT01',
        rules: [
            {
                property: 'sourceAccountRole',
                operator: 'EQUAL',
                value: 'MOVER'
            },
            {
                property: 'beneficiaryAccountRole',
                operator: 'IN',
                values: ['MOVER', 'VIEWER']
            },
            { property: 'sourceAccountType', operator: 'EQUAL', value: 'DC' },
            {
                property: 'sourceCIF',
                operator: 'EQUAL',
                value: '#beneficiaryCIF'
            },
            {
                property: 'transactionAmount',
                operator: 'LTE',
                value: '#moverRemainingDailyUsage'
            }
        ],
        _id: '5f5e5fc85af3157262825aa2'
    }
];
exports.getDecisionEngineConfigurationExpected = [
    {
        description: 'Internal JAGO Transfer',
        functionality: 'TRANSFER',
        rules: [
            {
                operator: 'EQUAL',
                property: 'sourceAccountRole',
                value: 'MOVER',
                values: null
            },
            {
                operator: 'EQUAL',
                property: 'beneficiaryAccountRole',
                value: 'OWNER',
                values: null
            },
            {
                operator: 'EQUAL',
                property: 'sourceAccountType',
                value: 'FS',
                values: null
            },
            {
                operator: 'EQUAL',
                property: 'beneficiaryAccountType',
                value: 'MA',
                values: null
            },
            {
                operator: 'LTE',
                property: 'transactionAmount',
                value: '#moverRemainingDailyUsage',
                values: null
            }
        ],
        serviceCode: 'SIT01'
    },
    {
        description: 'Internal JAGO Transfer',
        functionality: 'TRANSFER',
        rules: [
            {
                operator: 'EQUAL',
                property: 'sourceAccountRole',
                value: 'MOVER',
                values: null
            },
            {
                operator: 'EQUAL',
                property: 'beneficiaryAccountRole',
                value: 'OWNER',
                values: null
            },
            {
                operator: 'EQUAL',
                property: 'sourceAccountType',
                value: 'DC',
                values: null
            },
            {
                operator: 'LTE',
                property: 'transactionAmount',
                value: '#moverRemainingDailyUsage',
                values: null
            }
        ],
        serviceCode: 'SIT01'
    },
    {
        description: 'Internal JAGO Transfer',
        functionality: 'TRANSFER',
        rules: [
            {
                operator: 'EQUAL',
                property: 'sourceAccountRole',
                value: 'MOVER',
                values: null
            },
            {
                operator: 'EQUAL',
                property: 'beneficiaryAccountRole',
                value: 'NONE',
                values: null
            },
            {
                operator: 'EQUAL',
                property: 'sourceAccountType',
                value: 'DC',
                values: null
            },
            {
                operator: 'LTE',
                property: 'transactionAmount',
                value: '#moverRemainingDailyUsage',
                values: null
            }
        ],
        serviceCode: 'SIT01'
    },
    {
        description: 'Internal JAGO Transfer',
        functionality: 'TRANSFER',
        rules: [
            {
                operator: 'EQUAL',
                property: 'sourceAccountRole',
                value: 'MOVER',
                values: null
            },
            {
                operator: 'IN',
                property: 'beneficiaryAccountRole',
                value: null,
                values: ['MOVER', 'VIEWER']
            },
            {
                operator: 'EQUAL',
                property: 'sourceAccountType',
                value: 'DC',
                values: null
            },
            {
                operator: 'EQUAL',
                property: 'sourceCIF',
                value: '#beneficiaryCIF',
                values: null
            },
            {
                operator: 'LTE',
                property: 'transactionAmount',
                value: '#moverRemainingDailyUsage',
                values: null
            }
        ],
        serviceCode: 'SIT01'
    }
];
exports.getLimitGroupRedis = [
    {
        code: 'L002',
        description: 'Realtime Transfer Limit',
        currency: 'IDR',
        dailyLimitAmount: 999999999999,
        maximumDailyNumberOfTransaction: 999,
        monthlyLimitAmount: 999999999999,
        maximumMonthlyNumberOfTransaction: 999
    },
    {
        code: 'L003',
        description: 'SKN Transfer Limit',
        currency: 'IDR',
        dailyLimitAmount: 700000000,
        maximumDailyNumberOfTransaction: 999,
        monthlyLimitAmount: 999999999999,
        maximumMonthlyNumberOfTransaction: 999
    },
    {
        code: 'L004',
        description: 'RTGS Transfer Limit',
        currency: 'IDR',
        dailyLimitAmount: 800000000,
        maximumDailyNumberOfTransaction: 999,
        monthlyLimitAmount: 999999999999,
        maximumMonthlyNumberOfTransaction: 999
    },
    {
        code: 'L009',
        description: 'Bill Payment Limit',
        currency: 'IDR',
        dailyLimitAmount: 100000000,
        maximumDailyNumberOfTransaction: 999,
        monthlyLimitAmount: 999999999999,
        maximumMonthlyNumberOfTransaction: 999
    },
    {
        code: 'L019',
        description: 'BI Fast Payment Limit',
        currency: 'IDR',
        dailyLimitAmount: 250000000,
        maximumDailyNumberOfTransaction: 999,
        monthlyLimitAmount: 999999999999,
        maximumMonthlyNumberOfTransaction: 999
    }
];
exports.getTransactionReportConfigsAndFileUploadParameters = () => ({
    transactionReportConfigs: [
        {
            job: 'TOP_UP_GOPAY_REPORT',
            accountNo: [],
            paymentServiceCode: ['IRIS_JAGO_TO_OTH'],
            periodOfReport: -1,
            type: 'Principal',
            runningDate: 'Daily',
            runningTime: '0:01',
            fileLocation: 'TOP_UP_GOPAY_REPORT',
            referenceBank: 'Beneficiary',
            mailingList: ['test1@dkatalis.com', 'test2@dkatalis.com'],
            notificationTemplate: 'Notif_OPS',
            preferedId: 'thirdPartyIncomingId'
        },
        {
            job: 'RTOL_APP_FEE_REPORT',
            accountNo: [],
            paymentServiceCode: ['RTOL', 'RTOL_WALLET', 'RTOL_CC'],
            periodOfReport: -1,
            type: 'Fee',
            runningDate: 'Monthly',
            runningTime: '0:01',
            fileLocation: 'RTOL_APP_FEE_REPORT',
            referenceBank: 'Beneficiary',
            mailingList: ['test1@dkatalis.com', 'test2@dkatalis.com'],
            notificationTemplate: 'Notif_OPS',
            preferedId: 'thirdPartyOutgoingId'
        },
        {
            job: 'GOPAY_WITHDRAWAL_REPORT',
            accountNo: [],
            paymentServiceCode: ['IRIS_OTH_TO_JAGO'],
            periodOfReport: -1,
            type: 'Principal',
            runningDate: 'Daily',
            runningTime: '0:01',
            fileLocation: 'GOPAY_WITHDRAWAL_REPORT',
            referenceBank: 'Beneficiary'
        },
        {
            job: 'JAGOPAY_GOJEK_REPORT',
            accountNo: [],
            paymentServiceCode: ['JAGOPAY'],
            periodOfReport: -1,
            type: 'Principal',
            runningDate: 'Daily',
            runningTime: '0:01',
            fileLocation: 'JAGOPAY_GOJEK_REPORT',
            referenceBank: 'Beneficiary',
            mailingList: ['test1@dkatalis.com', 'test2@dkatalis.com'],
            notificationTemplate: 'Notif_OPS',
            preferedId: 'thirdPartyIncomingId',
            additionalInformation3: 'additionalInformation3'
        }
    ],
    fileUploadParameters: [
        {
            code: 'TOP_UP_GOPAY_REPORT',
            prefixFileName: 'GOPAYTOPUP_<DATETIME>',
            location: '/report/gojek/',
            fileType: ['text/csv']
        },
        {
            code: 'RTOL_APP_FEE_REPORT',
            prefixFileName: 'RTOL_APP_FEE_<MONTH>',
            location: '/report/bank/fee/',
            fileType: ['text/csv']
        }
    ]
});
exports.getBankCodeMappingData = () => ({
    id: '5f76004a76e66670019666666',
    bankCodeId: 'BC002',
    name: 'Jago',
    rtolCode: '542',
    remittanceCode: 'ATOSIDJ1',
    billerCode: null,
    isBersamaMember: false,
    isAltoMember: false,
    firstPriority: 'INTERNAL',
    channel: 'INTERNAL',
    companyName: 'PT. JAGO INDONESIA',
    type: 'BANK',
    prefix: null,
    isInternal: true,
    irisCode: 'jago'
});
exports.getMambuBlockingDays = () => ({
    code: '4',
    name: 'Four days',
    days: 1100111
});
exports.getCounterData = () => ({
    code: 'counter',
    name: 'Counter',
    value: 1100111
});
exports.getCountersList = (counter) => {
    switch (counter) {
        case 'COUNTER_07':
            return {
                code: 'COUNTER_07',
                type: 'CASH',
                name: 'CASH',
                value: 5
            };
        case 'AWARD_TRANSFER_001':
            return {
                code: 'AWARD_TRANSFER_001',
                type: 'AWARD_TRANSFER_001',
                name: 'AWARD_TRANSFER_001',
                value: 5
            };
        case 'COUNTER_01':
            return {
                code: 'COUNTER_01',
                type: 'CASH',
                name: 'CASH',
                value: 25
            };
        case 'COUNTER_08':
            return {
                code: 'COUNTER_08',
                type: 'CASH',
                name: 'CASH',
                value: 5
            };
        case 'COUNTER_09':
            return {
                code: 'COUNTER_09',
                type: 'CASH',
                name: 'CASH',
                value: 3
            };
        case 'COUNTER_MFS_01':
            return {
                code: 'COUNTER_MFS_01',
                type: 'AWARDS',
                name: 'Free Transfer Maximum Quota MFS',
                value: 100
            };
        case 'AWARD_OVERSEAS_CASHWITHDRAWAL_001':
            return {
                code: 'AWARD_OVERSEAS_CASHWITHDRAWAL_001',
                type: 'ATM',
                name: 'ATM',
                value: 5
            };
        default:
            return {
                code: 'COUNTER_01',
                type: 'AWARDS',
                name: 'name 1',
                value: 30
            };
    }
};
exports.getServiceReccommendation = () => [
    'CASHBACK',
    'THIRDPARTY_OTH_TO_JAGO',
    'VA_TOPUP_EXTERNAL',
    'THIRDPARTY_WINCOR_TO_JAGO',
    'VOID_VISA',
    'JAGOATM_OTH_TO_JAGO',
    'THIRDPARTY_JAGO_TO_JAGO_CREDIT',
    'OFFER',
    'VOID_NPG',
    'IRIS_OTH_TO_JAGO',
    'VOID_JAGOPAY',
    'REFUND_GENERAL',
    'PAYROLL',
    'SAT06',
    'SAT07',
    'SKN_OTH_TO_JAGO',
    'RTGS_OTH_TO_JAGO',
    'REFUND_SKN',
    'REFUND_RTGS',
    'INCOMING_BIFAST'
];
exports.workingTimeData = () => [
    {
        dayName: 'Monday',
        startTime: '00:00',
        endTime: '07:00'
    },
    {
        dayName: 'Tuesday',
        startTime: '00:00',
        endTime: '07:00'
    },
    {
        dayName: 'Wednesday',
        startTime: '00:00',
        endTime: '07:00'
    },
    {
        dayName: 'Thrusday',
        startTime: '00:00',
        endTime: '07:00'
    },
    {
        dayName: 'Friday',
        startTime: '00:00',
        endTime: '07:00'
    }
];
exports.getGeneralLedgersMockData = () => [
    {
        code: module_common_1.GeneralLedgerCode.BANK_INCOME,
        accountNumber: '229029049.00.00'
    },
    {
        code: module_common_1.GeneralLedgerCode.REPAYMENT,
        accountNumber: '229029050.00.00'
    },
    {
        code: module_common_1.GeneralLedgerCode.DEPOSIT,
        accountNumber: '229029051.00.00'
    }
];
exports.getRTGSAndBIFASTPaymentConfigRule = () => [
    ...exports.getBifastPaymentConfigRule(),
    {
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER,
        source: module_common_1.BankChannelEnum.INTERNAL,
        target: module_common_1.BankChannelEnum.EXTERNAL,
        sourceAccType: ['MA'],
        targetAccType: ['ANY'],
        sameCIF: 'ANY',
        amountRangeFrom: 100000001,
        amountRangeTo: 500000000,
        rtolDailyUsage: 'ANY',
        sknDailyUsage: 'ANY',
        rtgsDailyUsage: 'YES',
        serviceRecommendation: ['RTGS'],
        paymentServiceMappings: [
            {
                paymentServiceCode: 'RTGS',
                debitTransactionCode: [
                    {
                        transactionCode: 'TFD60',
                        transactionCodeInfo: Object.assign(Object.assign({}, exports.getTransactionCodeInfoData()), { code: 'TFD60' })
                    }
                ],
                beneficiaryBankCodeType: 'REMITTANCE',
                transactionAuthenticationChecking: true,
                blocking: true,
                requireThirdPartyOutgoingId: true,
                cutOffTime: 'RTGS'
            }
        ]
    }
];
exports.getParameterSettingCardTransactionMockData = () => [
    {
        code: module_common_1.PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT,
        name: 'INTERNATIONAL_PAYMENT',
        value: ''
    },
    {
        code: module_common_1.PaymentServiceTypeEnum.DOMESTIC_PAYMENT,
        name: 'DOMESTIC_PAYMENT',
        value: ''
    },
    {
        code: module_common_1.PaymentServiceTypeEnum.THIRD_PARTY_ATM_WITHDRAWAL,
        name: 'THIRD_PARTY_ATM_WITHDRAWAL',
        value: ''
    },
    {
        code: module_common_1.PaymentServiceTypeEnum.THIRD_PARTY_BALANCE_INQUIRY,
        name: 'THIRD_PARTY_BALANCE_INQUIRY',
        value: ''
    }
];
//# sourceMappingURL=configuration.data.js.map