"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importDefault(require("../logger"));
const minAmountPerTxOverrider = {
    mapToEntitlementCode(limitGroupCode) {
        return `value.tx.${limitGroupCode}.min_amount`;
    },
    overrideLimit(customerId, config, entitlementValue) {
        config.minAmountPerTx = entitlementValue;
        logger_1.default.info(`Minimum amount per transaction has been changed to '${entitlementValue}', ` +
            `for transaction code '${config.code}', for customerId '${customerId}'`);
    }
};
const maxAmountPerTxOverrider = {
    mapToEntitlementCode(limitGroupCode) {
        return `value.tx.${limitGroupCode}.max_amount`;
    },
    overrideLimit(customerId, config, entitlementValue) {
        config.maxAmountPerTx = entitlementValue;
        logger_1.default.info(`Maximum amount per transaction has been changed to '${entitlementValue}', ` +
            `for transaction code '${config.limitGroupCode}', for customerId '${customerId}'`);
    }
};
const paymentConfigRuleRangeFromOverrider = {
    mapToEntitlementCode: minAmountPerTxOverrider.mapToEntitlementCode,
    overrideLimit(customerId, config, entitlementValue) {
        config.amountRangeFrom = entitlementValue;
        logger_1.default.info(`range from amount has been changed to '${entitlementValue}' for payment config rule '${JSON.stringify(config)}', for customerId '${customerId}'`);
    }
};
const paymentConfigRuleRangeToOverrider = {
    mapToEntitlementCode: maxAmountPerTxOverrider.mapToEntitlementCode,
    overrideLimit(customerId, config, entitlementValue) {
        config.amountRangeTo = entitlementValue;
        logger_1.default.info(`range to amount has been changed to '${entitlementValue}' for payment config rule '${JSON.stringify(config)}', for customerId '${customerId}'`);
    }
};
const configurationOverride = {
    minAmountPerTxOverrider,
    maxAmountPerTxOverrider,
    paymentConfigRuleRangeFromOverrider,
    paymentConfigRuleRangeToOverrider
};
exports.default = configurationOverride;
//# sourceMappingURL=configuration.override.js.map