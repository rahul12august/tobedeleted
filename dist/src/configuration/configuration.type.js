"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var RunningDate;
(function (RunningDate) {
    RunningDate["DAILY"] = "Daily";
    RunningDate["MONTHLY"] = "Monthly";
})(RunningDate = exports.RunningDate || (exports.RunningDate = {}));
var ReferenceAccountEnum;
(function (ReferenceAccountEnum) {
    ReferenceAccountEnum["BENEFICIARY"] = "Beneficiary";
    ReferenceAccountEnum["SOURCE"] = "Source";
})(ReferenceAccountEnum = exports.ReferenceAccountEnum || (exports.ReferenceAccountEnum = {}));
var UploadTypeEnum;
(function (UploadTypeEnum) {
    UploadTypeEnum["ASSET"] = "ASSET";
    UploadTypeEnum["CUSTOMER"] = "CUSTOMER";
    UploadTypeEnum["MERCHANT"] = "MERCHANT";
    UploadTypeEnum["REPORT"] = "REPORT";
})(UploadTypeEnum = exports.UploadTypeEnum || (exports.UploadTypeEnum = {}));
//# sourceMappingURL=configuration.type.js.map