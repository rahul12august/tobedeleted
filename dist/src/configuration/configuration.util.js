"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const standarizeRTOL = (code) => {
    code = code.replace(/^0+/, '');
    if (code.length === 1)
        return '00' + code;
    else if (code.length === 2)
        return '0' + code;
    return code;
};
exports.standarizeRTOL = standarizeRTOL;
//# sourceMappingURL=configuration.util.js.map