"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AccountRole;
(function (AccountRole) {
    AccountRole["OWNER"] = "OWNER";
    AccountRole["VIEWER"] = "VIEWER";
    AccountRole["MOVER"] = "MOVER";
    AccountRole["NONE"] = "NONE";
})(AccountRole = exports.AccountRole || (exports.AccountRole = {}));
var AccountStatusEnum;
(function (AccountStatusEnum) {
    AccountStatusEnum["PENDING"] = "PENDING_APPROVAL";
    AccountStatusEnum["PENDING_DISBURSEMENT"] = "PENDING_DISBURSEMENT";
    AccountStatusEnum["APPROVED"] = "APPROVED";
    AccountStatusEnum["ACTIVE"] = "ACTIVE";
    AccountStatusEnum["DORMANT"] = "DORMANT";
    AccountStatusEnum["CLOSED"] = "CLOSED";
    AccountStatusEnum["WITHDRAWN"] = "WITHDRAWN";
    AccountStatusEnum["INACTIVE"] = "INACTIVE";
    AccountStatusEnum["BLOCK"] = "BLOCK";
    AccountStatusEnum["MIGRATING"] = "MIGRATING";
})(AccountStatusEnum = exports.AccountStatusEnum || (exports.AccountStatusEnum = {}));
//# sourceMappingURL=account.enum.js.map