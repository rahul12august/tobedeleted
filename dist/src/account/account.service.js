"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const account_repository_1 = __importDefault(require("./account.repository"));
const account_constant_1 = require("./account.constant");
const logger_1 = require("../logger");
const authenticateTransactionAmount = (sourceAccountNo, amount) => __awaiter(void 0, void 0, void 0, function* () {
    const amountLimit = yield account_repository_1.default.checkAmountLimit(sourceAccountNo, amount);
    if (amountLimit && amountLimit.status === account_constant_1.AMOUNT_LIMIT_REJECT)
        return true;
    return false;
});
const getAccountInfo = (sourceAccountNo) => __awaiter(void 0, void 0, void 0, function* () {
    const accountInfo = yield account_repository_1.default.getAccountInfo(sourceAccountNo);
    return accountInfo;
});
exports.default = logger_1.wrapLogs({
    authenticateTransactionAmount,
    getAccountInfo
});
//# sourceMappingURL=account.service.js.map