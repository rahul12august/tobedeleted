"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateAccountsData = () => ({
    cif: '123456',
    accounts: [
        {
            encodedKey: 'k1',
            sourceAccountNumber: '123456',
            accountType: 'FS1',
            accountNumber: '123456',
            status: 'ACTIVE',
            name: 'name',
            picture: 'http://www.myimage.com',
            currency: 'IDR',
            monthlyFeeCode: 'MF001',
            enabledBlockingAmount: true,
            aliasName: 'test',
            pictureType: 'PHOTO',
            cardLinking: [
                {
                    id: '122333',
                    name: 'test',
                    type: '002'
                }
            ]
        },
        {
            encodedKey: 'k2',
            sourceAccountNumber: '123456',
            accountType: 'FS2',
            accountNumber: '1234567',
            status: 'ACTIVE',
            name: 'name1',
            picture: 'http://www.myimage1.com',
            currency: 'IDR',
            monthlyFeeCode: 'MF001',
            enabledBlockingAmount: true,
            aliasName: 'test',
            pictureType: 'PHOTO',
            jagopayLinking: [
                {
                    id: '12233',
                    name: 'test'
                }
            ]
        }
    ]
});
exports.getAccountInfo = () => {
    return {
        accountNumber: 'string',
        cif: 'string',
        customerId: 'string',
        role: 'OWNER'
    };
};
//# sourceMappingURL=account.data.js.map