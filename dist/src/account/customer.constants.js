"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const getCustomerByIdUrl = (customerId) => `/customers/${customerId}`;
exports.getCustomerByIdUrl = getCustomerByIdUrl;
//# sourceMappingURL=customer.constants.js.map