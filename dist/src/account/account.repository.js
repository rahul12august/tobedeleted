"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const httpClient_1 = __importDefault(require("./httpClient"));
const logger_1 = __importStar(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const config_1 = require("../config");
const common_util_1 = require("../common/common.util");
const module_common_1 = require("@dk/module-common");
const transaction_enum_1 = require("../transaction/transaction.enum");
const inquiryAccountInfo = (bankCode, accountNumber, transactionAmount, sourceAccountNo, cif) => __awaiter(void 0, void 0, void 0, function* () {
    const queryCif = cif ? `&cif=${cif}` : '';
    let endpoint = `/accounts/inquiry?bankCode=${bankCode}&inputAccount=${accountNumber}${queryCif}`;
    if (transactionAmount) {
        endpoint += `&transactionAmount=${transactionAmount}`;
    }
    if (sourceAccountNo) {
        endpoint += `&sourceAccount=${sourceAccountNo}`;
    }
    const response = yield httpClient_1.default.get(endpoint);
    const info = lodash_1.get(response, ['data', 'data'], {});
    if (!info.accountNumber) {
        const detail = `Invalid account for account number: ${accountNumber}!`;
        logger_1.default.error(`inquiryAccountInfo: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.INVALID_ACCOUNT);
    }
    return info;
});
const queryMainAccount = (cif) => __awaiter(void 0, void 0, void 0, function* () {
    const response = yield httpClient_1.default.get(`/customers/${cif}/accounts/main-account`, {
        timeout: config_1.config.get('coreBankingApiCall').account
    });
    return lodash_1.get(response, ['data', 'data'], []);
});
const updateBlockingAmount = (cif, accountNumber, blockId, amount) => __awaiter(void 0, void 0, void 0, function* () {
    yield httpClient_1.default.put(`/private/accounts/${accountNumber}/blocking-amounts/${blockId}?cif=${cif}`, {
        amount
    }, {
        timeout: config_1.config.get('coreBankingApiCall').account
    });
});
const createBlockingAmount = (cif, accountNumber, payload) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.info(`create blocking amount transaction for account: ${accountNumber} and mambuBlockingCode: ${payload.mambuBlockingCode}`);
        const result = yield httpClient_1.default.post(`/accounts/${accountNumber}/blocking-amounts?cif=${cif}`, payload, {
            timeout: config_1.config.get('coreBankingApiCall').account
        });
        const response = result.data.data;
        logger_1.default.info(`createBlockingAmount: response id: ${response.id}, status: ${response.status} and cardId: ${response.cardId}`);
        return response;
    }
    catch (err) {
        logger_1.default.error(`createBlockingAmount: Failed while blocking Amount code : ${err.code} message : ${err.message}`);
        const appError = err && err.error && err.error.error;
        return common_util_1.mapTransferAppError(appError || err, errors_1.ERROR_CODE.COREBANKING_BLOCK_AMOUNT_FAILED, err.status >= 500 ? true : false);
    }
});
const checkAmountLimit = (sourceAccountNo, amount) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`checkAmountLimit: For sourceAccountNo ${sourceAccountNo}`);
    const result = yield httpClient_1.default.post(`/private/accounts/auth-transaction-amount`, {
        authenticationAmount: amount,
        accountNumber: sourceAccountNo
    }, {
        timeout: config_1.config.get('coreBankingApiCall').account
    });
    const response = (result.data && result.data.data) || undefined;
    logger_1.default.info(`checkAmountLimit: Response for sourceAccountNo ${sourceAccountNo} is ${response}`);
    return response;
});
const getAccountInfo = (accountNumber, cif) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.info(`getAccountInfo for account: ${accountNumber} and cif: ${cif}`);
        let accountInfoUrl = `/private/accounts/inquiry?accountNumber=${accountNumber}&includeMigratingAccounts=true`;
        if (cif)
            accountInfoUrl += `&cif=${cif}`;
        const result = yield httpClient_1.default.get(accountInfoUrl);
        logger_1.default.info(`getAccountInfo: getting response for account: ${accountNumber} and cif: ${cif}`);
        return result.data.data;
    }
    catch (error) {
        logger_1.default.error(`getAccountInfo: returns an error: ${error}`);
        throw error && error.status === module_common_1.Http.StatusCode.BAD_REQUEST
            ? new AppError_1.TransferAppError(`Error while getting account info ${error}!`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.INVALID_REQUEST)
            : error;
    }
});
const getActivatedAccount = (accountNumber) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`getActivatedAccount for account: ${accountNumber}`);
    const result = yield httpClient_1.default.get(`/private/accounts/${accountNumber}?fetchBalance=false`);
    logger_1.default.info(`getActivatedAccount: getting response for account: ${accountNumber}`);
    return result.data.data;
});
exports.default = logger_1.wrapLogs({
    inquiryAccountInfo,
    createBlockingAmount,
    updateBlockingAmount,
    queryMainAccount,
    checkAmountLimit,
    getAccountInfo,
    getActivatedAccount
});
//# sourceMappingURL=account.repository.js.map