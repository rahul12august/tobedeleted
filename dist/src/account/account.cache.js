"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const account_cache_util_1 = __importDefault(require("./account.cache.util"));
const account_repository_1 = __importDefault(require("./account.repository"));
const redis_1 = __importDefault(require("../common/redis"));
const redis_repository_1 = __importDefault(require("../common/redis.repository"));
const logger_1 = require("../logger");
const basePrefixKey = config_1.config.get('servicePrefix').account;
const inquiryAccountPrefixKey = `${basePrefixKey}-inquiry-account`;
const getCachedInquiryAccountInfo = (bankCode, accountNumber, transactionAmount, sourceAccountNo, cif) => __awaiter(void 0, void 0, void 0, function* () {
    const cacheArgs = {
        bankCode,
        accountNumber,
        transactionAmount,
        sourceAccountNo,
        cif
    };
    const key = redis_1.default.addPrefix(account_cache_util_1.default.constructCacheKeyFromObject(cacheArgs), inquiryAccountPrefixKey);
    return redis_repository_1.default.getCacheFromIntermediateKey(key, account_repository_1.default.inquiryAccountInfo, ...Object.values(Object.assign({}, cacheArgs)));
});
const accountCache = {
    getCachedInquiryAccountInfo
};
exports.default = logger_1.wrapLogs(accountCache);
//# sourceMappingURL=account.cache.js.map