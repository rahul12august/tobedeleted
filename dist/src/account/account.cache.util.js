"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constructCacheKeyFromObject = (object) => Object.entries(object)
    .filter(([, val]) => val)
    .map(([key, val]) => `${key}=${val}`)
    .join('&');
exports.default = { constructCacheKeyFromObject };
//# sourceMappingURL=account.cache.util.js.map