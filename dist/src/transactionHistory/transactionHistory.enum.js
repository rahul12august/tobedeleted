"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TransactionHistoryStatusEnum;
(function (TransactionHistoryStatusEnum) {
    TransactionHistoryStatusEnum["SUCCEED"] = "SUCCEED";
    TransactionHistoryStatusEnum["PENDING"] = "PENDING";
    TransactionHistoryStatusEnum["FAILED"] = "FAILED";
})(TransactionHistoryStatusEnum = exports.TransactionHistoryStatusEnum || (exports.TransactionHistoryStatusEnum = {}));
//# sourceMappingURL=transactionHistory.enum.js.map