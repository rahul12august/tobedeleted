"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_httpclient_1 = require("@dk/module-httpclient");
const constant_1 = require("../common/constant");
const config_1 = require("../config");
const context_1 = require("../context");
const logger_1 = __importDefault(require("../logger"));
const baseUrl = config_1.config.get('ms').transactionHistory;
const httpClient = module_httpclient_1.createHttpClient({
    baseURL: baseUrl,
    context: context_1.context,
    logger: logger_1.default,
    retryConfig: constant_1.retryConfig
});
exports.default = httpClient;
//# sourceMappingURL=httpClient.js.map