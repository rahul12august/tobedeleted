"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const logger_1 = __importDefault(require("../logger"));
const RETRIABLE_ERROR_CODE = 'INVALID_TRANSACTION_ID';
const DEFAULT_RETRY_COUNT = '3';
const DEFAULT_RETRY_DELAY_IN_MS = '2000';
const getRetryCount = () => process.env.TH_RETRY_COUNT || DEFAULT_RETRY_COUNT;
const getDelayInMS = () => process.env.TH_RETRY_DELAY || DEFAULT_RETRY_DELAY_IN_MS;
const getRetryConfig = () => ({
    shouldRetry: (error) => {
        var _a, _b, _c;
        logger_1.default.info(`Failed to update TH reference ID. ${JSON.stringify(error.error)}`);
        return ((_c = (_b = (_a = error) === null || _a === void 0 ? void 0 : _a.error) === null || _b === void 0 ? void 0 : _b.error) === null || _c === void 0 ? void 0 : _c.code) === RETRIABLE_ERROR_CODE;
    },
    delayInMS: Number(getDelayInMS()),
    numberOfTries: Number(getRetryCount())
});
exports.updateTransactionHistoryReferenceIdWithRetry = (fn, ...args) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const retryConfig = getRetryConfig();
        logger_1.default.debug(`updateTransactionHistoryReferenceIdWithRetry: RetryConfig: 
      ${JSON.stringify(retryConfig)}`);
        return yield module_common_1.Util.retry(retryConfig, fn, ...args);
    }
    catch (err) {
        logger_1.default.error(`Failed updating TH reference ID. ${err}`);
        return undefined;
    }
});
//# sourceMappingURL=transactionHistory.util.js.map