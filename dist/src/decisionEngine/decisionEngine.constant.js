"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const decisionEngine_util_1 = require("./decisionEngine.util");
exports.DecisionEngineOperator = {
    EQUAL: 'EQUAL',
    IN: 'IN',
    LTE: 'LTE'
};
exports.decisionEngineVerifyFunction = {
    [exports.DecisionEngineOperator.EQUAL]: lodash_1.isEqual,
    [exports.DecisionEngineOperator.IN]: lodash_1.includes,
    [exports.DecisionEngineOperator.LTE]: lodash_1.lte
};
exports.decisionEngineGetValueFunction = {
    [exports.DecisionEngineOperator.EQUAL]: decisionEngine_util_1.getValue,
    [exports.DecisionEngineOperator.IN]: decisionEngine_util_1.getValueWithInCondition,
    [exports.DecisionEngineOperator.LTE]: decisionEngine_util_1.getValue
};
exports.prefixValueCol = '#';
exports.emptyString = '';
//# sourceMappingURL=decisionEngine.constant.js.map