"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_data_1 = require("../../transaction/__mocks__/transaction.data");
const module_common_1 = require("@dk/module-common");
exports.getMockTransactionInput = () => (Object.assign(Object.assign({}, transaction_data_1.getITransactionInput()), { sourceAccountType: 'FS', sourceBankCodeChannel: module_common_1.BankChannelEnum.INTERNAL, sourceBankCode: module_common_1.Constant.GIN_BANK_CODE_ID, sourceTransactionCurrency: 'IDR', sourceCIF: 'cif', beneficiaryCIF: 'cif', transactionAmount: 999, limitAmount: 1001 }));
//# sourceMappingURL=decisionEngine.data.js.map