"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const decisionEngine_class_1 = require("./decisionEngine.class");
const runRole = (configRole, input) => {
    logger_1.default.info('Run DC engine Rule', configRole);
    const verifiedRules = configRole.rules.map(rule => new decisionEngine_class_1.DecisionEngineRules(input, rule).run());
    logger_1.default.info('Result DC engine Rule ' + configRole.description, verifiedRules);
    return verifiedRules.every(verifiedItem => verifiedItem === true);
};
const runMultipleRoles = (configRoles, input) => {
    logger_1.default.info(`start run DC engine with ${configRoles.length} configRoles`);
    const verifiedRoles = configRoles.map(configRole => runRole(configRole, input));
    logger_1.default.info('result run DC engine', verifiedRoles);
    return verifiedRoles.some(verifiedItem => verifiedItem === true);
};
exports.default = logger_1.wrapLogs({
    runRole,
    runMultipleRoles
});
//# sourceMappingURL=decisionEngine.service.js.map