"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const decisionEngine_constant_1 = require("./decisionEngine.constant");
const lodash_1 = require("lodash");
exports.isValueGetFromField = (value) => value && value.indexOf(decisionEngine_constant_1.prefixValueCol) > -1;
exports.getValueFormOtherField = (input, value) => value && lodash_1.get(input, value.replace(decisionEngine_constant_1.prefixValueCol, ''));
exports.getValue = ({ value }, input) => exports.isValueGetFromField(String(value))
    ? exports.getValueFormOtherField(input, String(value))
    : value;
exports.getValueWithInCondition = ({ values }) => values;
//# sourceMappingURL=decisionEngine.util.js.map