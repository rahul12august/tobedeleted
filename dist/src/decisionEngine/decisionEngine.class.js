"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const decisionEngine_constant_1 = require("./decisionEngine.constant");
const errors_1 = require("../common/errors");
const lodash_1 = __importDefault(require("lodash"));
const AppError_1 = require("../errors/AppError");
const logger_1 = __importDefault(require("../logger"));
const transaction_enum_1 = require("../transaction/transaction.enum");
class DecisionEngineRules {
    constructor(obj, role) {
        this.obj = obj;
        this.role = role;
    }
    checkOperator() {
        if (Object.values(decisionEngine_constant_1.DecisionEngineOperator).includes(this.role.operator)) {
            return this;
        }
        const detail = `Decision engine operator: ${this.role.operator} is not supported!`;
        logger_1.default.error(`checkOperator: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.DECISION_ENGINE_OPERATOR_NOT_SUPPORT);
    }
    getPropertyValue() {
        this.propertyValue = lodash_1.default.get(this.obj, this.role.property);
        return this;
    }
    getValueData() {
        this.valueData = decisionEngine_constant_1.decisionEngineGetValueFunction[this.role.operator](this.role, this.obj);
        return this;
    }
    swapParams() {
        if (this.role.operator === decisionEngine_constant_1.DecisionEngineOperator.IN) {
            const temp = this.propertyValue;
            this.propertyValue = this.valueData;
            this.valueData = temp;
        }
        return this;
    }
    executeRule() {
        if (!this.role.operator || !this.propertyValue || !this.valueData) {
            return false;
        }
        return decisionEngine_constant_1.decisionEngineVerifyFunction[this.role.operator](this.propertyValue, this.valueData);
    }
    run() {
        return this.checkOperator()
            .getPropertyValue()
            .getValueData()
            .swapParams()
            .executeRule();
    }
}
exports.DecisionEngineRules = DecisionEngineRules;
//# sourceMappingURL=decisionEngine.class.js.map