"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("@hapi/joi"));
const billPaymentTransaction_enum_1 = require("./billPaymentTransaction/billPaymentTransaction.enum");
exports.manualAdjustmentPayloadValidator = joi_1.default.object({
    transactionId: joi_1.default.string().required(),
    status: joi_1.default.string()
        .required()
        .valid(billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.FAILURE, billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.SUCCESS)
}).label('gobillsTransactionManualAdjustment');
//# sourceMappingURL=billPayment.validator.js.map