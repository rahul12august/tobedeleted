"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const billPayment_constant_1 = require("./billPayment.constant");
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const getStartOfTheDay = (date) => {
    date.setHours(0, 0, 0, 0);
    return date;
};
const isBefore = (fromDate, toDate) => getStartOfTheDay(fromDate) < getStartOfTheDay(toDate);
const getNextDate = () => {
    const nextDate = getStartOfTheDay(new Date());
    nextDate.setDate(nextDate.getDate() + 1);
    return nextDate;
};
const addDays = (time, noOfDaysToAdd) => {
    const days = noOfDaysToAdd * 60 * 60 * 24 * 1000;
    return new Date(time + days);
};
const getNextWorkingDay = (startDate) => __awaiter(void 0, void 0, void 0, function* () {
    const endDate = addDays(startDate.getTime(), billPayment_constant_1.HOLIDAY_DATE_RANGE);
    const holidayList = yield configuration_repository_1.default.getHolidaysByDateRange(startDate, endDate);
    for (let i = 0; i < holidayList.length; i++) {
        if (isBefore(startDate, new Date(holidayList[i].fromDate))) {
            return startDate;
        }
        startDate = addDays(new Date(holidayList[i].toDate).getTime(), 1);
    }
    return startDate;
});
exports.getNextTime = (baseDate, hours, minutes, seconds) => new Date(baseDate.getFullYear(), baseDate.getMonth(), baseDate.getDate(), hours, minutes, seconds);
const getNextRelativePollTime = (delayToBeAdded) => {
    const delayedMs = 1000 * delayToBeAdded;
    return new Date(new Date().getTime() + delayedMs);
};
const getNextAbsolutePollTime = (delay) => __awaiter(void 0, void 0, void 0, function* () {
    const time = delay.split(' ');
    const nextDate = getNextDate();
    const nextWorkingDate = yield getNextWorkingDay(nextDate);
    return exports.getNextTime(nextWorkingDate, Number(time[0]), Number(time[1]), Number(time[2]));
});
exports.getNextPollTime = (pollingConfig) => __awaiter(void 0, void 0, void 0, function* () {
    return pollingConfig.type === module_common_1.Enum.BillingAggregatorPollingConfigType.RELATIVE
        ? getNextRelativePollTime(Number(pollingConfig.delayTime))
        : yield getNextAbsolutePollTime(pollingConfig.delayTime);
});
//# sourceMappingURL=billPayment.utils.js.map