"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const billPaymentTransaction_repository_1 = __importDefault(require("./billPaymentTransaction.repository"));
const logger_1 = require("../../logger");
const createTransaction = (billpaymentTransaction) => __awaiter(void 0, void 0, void 0, function* () { return billPaymentTransaction_repository_1.default.create(billpaymentTransaction); });
const updateStatus = (billpaymentTransaction, orderId) => __awaiter(void 0, void 0, void 0, function* () { return billPaymentTransaction_repository_1.default.update(orderId, billpaymentTransaction); });
const getTransactionStatusHistory = (orderId) => __awaiter(void 0, void 0, void 0, function* () { return billPaymentTransaction_repository_1.default.getStatusHistory(orderId); });
const getTransactionStatus = (orderId) => __awaiter(void 0, void 0, void 0, function* () { return billPaymentTransaction_repository_1.default.getRecentTransactionStatus(orderId); });
const getByTxnId = (txnId) => __awaiter(void 0, void 0, void 0, function* () { return billPaymentTransaction_repository_1.default.getByTxnId(txnId); });
const updateGobillsTransaction = (transactionId, transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    return billPaymentTransaction_repository_1.default.updateGobillsTransaction(transactionId, transactionModel);
});
const billPaymentTxnService = logger_1.wrapLogs({
    createTransaction,
    updateStatus,
    getTransactionStatusHistory,
    getTransactionStatus,
    getByTxnId,
    updateGobillsTransaction
});
exports.default = billPaymentTxnService;
//# sourceMappingURL=billPaymentTransaction.service.js.map