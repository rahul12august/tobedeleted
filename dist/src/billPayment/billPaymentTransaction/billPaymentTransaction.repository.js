"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const billPaymentTransaction_model_1 = require("./billPaymentTransaction.model");
const logger_1 = __importStar(require("../../logger"));
const convertTransactionDocumentToObject = (document) => document.toObject({ getters: true });
const create = (billPaymentTransactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Bill Payment : Creating Bill Payment Transaction for transactionId : ${billPaymentTransactionModel.transactionId}, 
     referenceId : ${billPaymentTransactionModel.referenceId}, orderId : ${billPaymentTransactionModel.orderId}`);
    const newBillPaymentTransactionModel = new billPaymentTransaction_model_1.BillPaymentTransactionModel(billPaymentTransactionModel);
    yield newBillPaymentTransactionModel.save();
    return convertTransactionDocumentToObject(newBillPaymentTransactionModel);
});
const update = (orderId, billPaymentTransactionStatus) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Bill Payment : Updating Bill Payment Transaction for payload : ${billPaymentTransactionStatus.status} for orderId : ${orderId}`);
    const billPaymentTransaction = yield billPaymentTransaction_model_1.BillPaymentTransactionModel.findOneAndUpdate({ orderId: orderId }, { $push: { status: billPaymentTransactionStatus } }, { fields: { status: { $slice: -1 } }, new: true });
    return (billPaymentTransaction &&
        convertTransactionDocumentToObject(billPaymentTransaction));
});
const findOneByQuery = (query) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Bill Payment : Fetching Bill Payment Transaction for query : ${JSON.stringify(query)}`);
    const billPaymentTransaction = yield billPaymentTransaction_model_1.BillPaymentTransactionModel.findOne(query);
    return (billPaymentTransaction &&
        convertTransactionDocumentToObject(billPaymentTransaction));
});
const getRecentTransactionStatus = (orderId) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Bill Payment : Fetching recent Bill Payment Transaction for orderId : ${orderId}`);
    const billPaymentTransaction = yield billPaymentTransaction_model_1.BillPaymentTransactionModel.findOne({ orderId: orderId }, { status: { $slice: -1 } });
    return (billPaymentTransaction &&
        convertTransactionDocumentToObject(billPaymentTransaction));
});
const getStatusHistory = (orderId) => __awaiter(void 0, void 0, void 0, function* () { return findOneByQuery({ orderId }); });
const getByTxnId = (transactionId) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Bill Payment : Fetching recent Bill Payment Transaction for orderId : ${transactionId}`);
    const billPaymentTransaction = yield billPaymentTransaction_model_1.BillPaymentTransactionModel.findOne({
        transactionId
    });
    return (billPaymentTransaction &&
        convertTransactionDocumentToObject(billPaymentTransaction));
});
const updateGobillsTransaction = (transactionId, transactionModel) => __awaiter(void 0, void 0, void 0, function* () {
    const transaction = yield billPaymentTransaction_model_1.BillPaymentTransactionModel.findOneAndUpdate({ transactionId }, transactionModel, { new: true });
    return transaction && convertTransactionDocumentToObject(transaction);
});
const billPaymentTransactionRepository = {
    create,
    update,
    getRecentTransactionStatus,
    getStatusHistory,
    getByTxnId,
    updateGobillsTransaction
};
exports.default = logger_1.wrapLogs(billPaymentTransactionRepository);
//# sourceMappingURL=billPaymentTransaction.repository.js.map