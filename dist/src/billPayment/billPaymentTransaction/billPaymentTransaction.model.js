"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const mongoose_util_1 = require("../../common/mongoose.util");
const billPaymentTransaction_enum_1 = require("./billPaymentTransaction.enum");
exports.collectionName = 'gobills_transactions';
const transactionStatus = new mongoose_1.Schema({
    status: {
        type: String,
        enum: Object.values(billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum),
        required: true
    },
    reason: {
        type: String,
        required: false
    },
    capturedAt: {
        type: Date,
        required: true
    }
});
const BillPaymentTransactionSchema = new mongoose_1.Schema({
    orderId: {
        type: String,
        required: true
    },
    transactionId: {
        type: String,
        required: true
    },
    referenceId: {
        type: String,
        required: true
    },
    status: {
        type: [transactionStatus],
        required: true
    },
    accountNo: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    manuallyAdjusted: {
        type: Boolean,
        required: false
    }
}, {
    timestamps: true
});
BillPaymentTransactionSchema.post('save', mongoose_util_1.handleDuplicationError);
exports.BillPaymentTransactionModel = mongoose_1.default.model(exports.collectionName, BillPaymentTransactionSchema);
//# sourceMappingURL=billPaymentTransaction.model.js.map