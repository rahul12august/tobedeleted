"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BillPaymentTransactionStatusEnum;
(function (BillPaymentTransactionStatusEnum) {
    BillPaymentTransactionStatusEnum["TRANSACTION_SUBMITTED"] = "TRANSACTION_SUBMITTED";
    BillPaymentTransactionStatusEnum["TRANSACTION_SUBMIT_FAILED"] = "TRANSACTION_SUBMIT_FAILED";
    BillPaymentTransactionStatusEnum["REQUEST_SENT"] = "REQUEST_SENT";
    BillPaymentTransactionStatusEnum["SUCCESS"] = "SUCCESS";
    BillPaymentTransactionStatusEnum["FAILURE"] = "FAILED";
    BillPaymentTransactionStatusEnum["WAITING"] = "PENDING";
    BillPaymentTransactionStatusEnum["MANUAL_INTERVENTION"] = "MANUAL_INTERVENTION";
})(BillPaymentTransactionStatusEnum = exports.BillPaymentTransactionStatusEnum || (exports.BillPaymentTransactionStatusEnum = {}));
//# sourceMappingURL=billPaymentTransaction.enum.js.map