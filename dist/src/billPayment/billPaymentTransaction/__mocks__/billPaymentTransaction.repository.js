"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    create: jest.fn(),
    update: jest.fn(),
    getRecentTransactionStatus: jest.fn(),
    getStatusHistory: jest.fn(),
    getByTxnId: jest.fn(),
    updateGobillsTransaction: jest.fn()
};
//# sourceMappingURL=billPaymentTransaction.repository.js.map