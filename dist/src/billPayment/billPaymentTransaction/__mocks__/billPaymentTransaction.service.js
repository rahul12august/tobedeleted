"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    createTransaction: jest.fn(),
    updateStatus: jest.fn(),
    getTransactionStatusHistory: jest.fn(),
    getTransactionStatus: jest.fn(),
    getByTxnId: jest.fn(),
    updateGobillsTransaction: jest.fn()
};
//# sourceMappingURL=billPaymentTransaction.service.js.map