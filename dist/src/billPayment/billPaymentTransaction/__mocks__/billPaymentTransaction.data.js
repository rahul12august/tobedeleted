"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const billPaymentTransaction_enum_1 = require("../billPaymentTransaction.enum");
exports.getBillPaymentTransaction = () => ({
    orderId: 'order123',
    transactionId: 'transactionId',
    referenceId: 'REF123',
    status: [
        {
            status: billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.SUCCESS,
            capturedAt: new Date()
        }
    ],
    accountNo: '28205596',
    createdAt: new Date(`01 June 2020 00:00:00`),
    amount: 10000
});
exports.getBillPaymentTransactionWithError = () => ({
    orderId: 'order123',
    transactionId: 'transactionId',
    referenceId: 'REF123',
    status: [
        {
            status: billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.FAILURE,
            capturedAt: new Date(),
            reason: 'Error'
        }
    ],
    accountNo: '28205596',
    createdAt: new Date(`01 June 2020 00:00:00`),
    amount: 10000
});
exports.getBillPaymentTransactionStatus = () => ({
    status: billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.SUCCESS,
    capturedAt: new Date()
});
exports.getBillPaymentTransactionStatusWithReason = () => ({
    status: billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.TRANSACTION_SUBMIT_FAILED,
    capturedAt: new Date(),
    reason: 'Error'
});
exports.getMultipleStatusForTransaction = () => ({
    orderId: 'order123',
    transactionId: 'transactionId',
    referenceId: 'REF123',
    status: [
        {
            status: billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.REQUEST_SENT,
            capturedAt: new Date()
        },
        {
            status: billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.WAITING,
            capturedAt: new Date()
        },
        {
            status: billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.SUCCESS,
            capturedAt: new Date()
        }
    ],
    accountNo: '28205596',
    createdAt: new Date(`01 June 2020 00:00:00`),
    amount: 10000
});
//# sourceMappingURL=billPaymentTransaction.data.js.map