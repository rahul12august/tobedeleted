"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const paymentStatusPolling_service_1 = __importDefault(require("./paymentStatusPolling/paymentStatusPolling.service"));
const logger_1 = __importStar(require("../logger"));
const billingAggregator_service_1 = __importDefault(require("../billingAggregator/billingAggregator.service"));
const lodash_1 = require("lodash");
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const goBills_service_1 = __importDefault(require("../billingAggregator/goBills/goBills.service"));
const decisionEngine_constant_1 = require("../decisionEngine/decisionEngine.constant");
const billPaymentTransaction_service_1 = __importDefault(require("./billPaymentTransaction/billPaymentTransaction.service"));
const transaction_service_1 = __importDefault(require("../transaction/transaction.service"));
const contextHandler_1 = require("../common/contextHandler");
const transaction_enum_1 = require("../transaction/transaction.enum");
const transaction_constant_1 = require("../transaction/transaction.constant");
const updatePendingTransactions = () => __awaiter(void 0, void 0, void 0, function* () {
    const startDate = new Date();
    const pendingStatusRecords = yield paymentStatusPolling_service_1.default.getPendingRecords(startDate);
    if (lodash_1.isEmpty(pendingStatusRecords)) {
        logger_1.default.info(`Payment Statuses for startDate ${startDate} not found.`);
    }
    logger_1.default.warn(`Bill Payment Statuses for startDate ${startDate} with count of pending records : ${pendingStatusRecords.length}`);
    const paymentStatusPromises = pendingStatusRecords.map((paymentStatus) => __awaiter(void 0, void 0, void 0, function* () {
        yield contextHandler_1.updateRequestIdInContext(paymentStatus.orderId);
        return billingAggregator_service_1.default.updateBillPaymentStatus(paymentStatus.orderId);
    }));
    yield Promise.all(paymentStatusPromises);
});
const txnManualAdjustment = (txnId, reqStatus, updateBillDetails = true) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Manual adjustment started for transaction id ${txnId} `);
    const status = transaction_constant_1.MAP_TXN_STATUS_TO_GOBILL[reqStatus];
    const billPaymentTxn = yield billPaymentTransaction_service_1.default.getByTxnId(txnId);
    if (!billPaymentTxn) {
        const detail = `Matching txn was not found for txnId: ${txnId}!`;
        logger_1.default.error(`txnManualAdjustment: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_BILL_PAYMENT, errors_1.ERROR_CODE.TRANSACTION_NOT_FOUND);
    }
    const res = {
        orderId: billPaymentTxn.orderId,
        status,
        voucherCode: decisionEngine_constant_1.emptyString,
        amount: billPaymentTxn.amount.toString(),
        trxDate: decisionEngine_constant_1.emptyString,
        skuTag: decisionEngine_constant_1.emptyString,
        refNumber: billPaymentTxn.referenceId
    };
    try {
        yield goBills_service_1.default.updateTransactionWithResult(res, billPaymentTxn.orderId, updateBillDetails);
    }
    catch (error) {
        logger_1.default.error(`txnManualAdjustment: error while manually adjusting transaction: ${txnId}, status: ${error &&
            error.status}, message: ${error && error.message}`);
        throw error;
    }
    finally {
        const transactionUpdate = updateBillDetails
            ? { manuallyAdjusted: true }
            : { manuallyAdjusted: true, status: reqStatus };
        yield transaction_service_1.default.findByIdAndUpdate(billPaymentTxn.transactionId, transactionUpdate);
        if (updateBillDetails)
            yield paymentStatusPolling_service_1.default.updatePaymentStatusPollingConfig(billPaymentTxn.transactionId, status);
    }
});
const billPaymentService = {
    updatePendingTransactions,
    txnManualAdjustment
};
exports.default = logger_1.wrapLogs(billPaymentService);
//# sourceMappingURL=billPayment.service.js.map