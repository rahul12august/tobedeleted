"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const paymentStatusPolling_model_1 = require("./paymentStatusPolling.model");
const lodash_1 = require("lodash");
const logger_1 = __importStar(require("../../logger"));
const toDocumentObject = (document) => document.toObject({
    getters: true
});
const create = (statusPollEntry) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Bill Payment : Creating bill payment status polling entry : 
    ${JSON.stringify(statusPollEntry)}`);
    const object = yield paymentStatusPolling_model_1.PaymentStatusPollingModel.create(statusPollEntry);
    return toDocumentObject(object);
});
const updateWithIncrement = (updatedPaymentStatusPolling) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Bill Payment : Updating bill payment status polling entry with increment : 
    ${JSON.stringify(updatedPaymentStatusPolling)}`);
    const { orderId } = updatedPaymentStatusPolling, fieldsToUpdate = __rest(updatedPaymentStatusPolling, ["orderId"]);
    const result = yield paymentStatusPolling_model_1.PaymentStatusPollingModel.findOneAndUpdate({ orderId: orderId }, {
        $set: lodash_1.omitBy(fieldsToUpdate, lodash_1.isUndefined),
        $inc: { statusRetryCount: 1 }
    }, { new: true });
    return result && toDocumentObject(result);
});
const getPaymentStatusPollingFromDB = (orderId) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Bill Payment : Fetching bill payment status for orderId : 
    ${orderId}`);
    const paymentStatusPolling = yield paymentStatusPolling_model_1.PaymentStatusPollingModel.findOne({
        orderId
    });
    return paymentStatusPolling && toDocumentObject(paymentStatusPolling);
});
const getPendingRecords = (status, endDateTime) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Bill Payment : Fetching pending bill payment entries for endDateTime : 
    ${endDateTime}`);
    const documents = yield paymentStatusPolling_model_1.PaymentStatusPollingModel.find({
        status: status,
        nextPollTime: {
            $lte: endDateTime
        }
    });
    return documents.map(entry => toDocumentObject(entry));
});
const paymentStatusPollingRepository = {
    create,
    updateWithIncrement,
    getPaymentStatusPollingFromDB,
    getPendingRecords
};
exports.default = logger_1.wrapLogs(paymentStatusPollingRepository);
//# sourceMappingURL=paymentStatusPolling.repository.js.map