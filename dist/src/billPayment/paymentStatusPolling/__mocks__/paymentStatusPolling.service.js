"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    updatePollingConfig: jest.fn(),
    getPendingRecords: jest.fn(),
    updatePaymentStatusPollingConfig: jest.fn()
};
//# sourceMappingURL=paymentStatusPolling.service.js.map