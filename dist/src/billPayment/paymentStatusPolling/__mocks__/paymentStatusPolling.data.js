"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const billPaymentTransaction_enum_1 = require("../../billPaymentTransaction/billPaymentTransaction.enum");
const module_common_1 = require("@dk/module-common");
exports.createPaymentStatusPollingData = () => ({
    orderId: 'REF123',
    status: billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.WAITING,
    statusRetryCount: 0,
    nextPollTime: new Date(2020, 3, 20, 10, 33, 30, 0)
});
exports.createPaymentStatusPollingData2 = () => ({
    orderId: 'REF1234',
    nextPollTime: new Date(2020, 3, 20, 10, 35, 30, 0),
    status: billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.WAITING,
    statusRetryCount: 1
});
exports.createPaymentStatusPollingData3 = () => ({
    orderId: 'REF12345',
    status: billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.FAILURE,
    statusRetryCount: 1,
    nextPollTime: new Date(2020, 3, 20, 10, 38, 30, 0)
});
exports.createPaymentStatusPollingData4 = () => ({
    orderId: 'REF123456',
    status: billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.WAITING,
    statusRetryCount: 1,
    nextPollTime: new Date(2020, 3, 20, 10, 40, 30, 0)
});
exports.updatePaymentStatusPollingData = () => ({
    orderId: 'REF123',
    status: billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.SUCCESS,
    statusRetryCount: 2,
    nextPollTime: new Date(2020, 4, 25, 4, 30, 30, 0)
});
exports.pollingConfigs = [
    {
        type: module_common_1.Enum.BillingAggregatorPollingConfigType.RELATIVE,
        delayTime: '300'
    },
    {
        type: module_common_1.Enum.BillingAggregatorPollingConfigType.RELATIVE,
        delayTime: '300'
    },
    {
        type: module_common_1.Enum.BillingAggregatorPollingConfigType.ABSOLUTE,
        delayTime: '17 15 10'
    }
];
//# sourceMappingURL=paymentStatusPolling.data.js.map