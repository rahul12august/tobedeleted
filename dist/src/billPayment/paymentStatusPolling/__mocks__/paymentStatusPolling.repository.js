"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    create: jest.fn(),
    getPaymentStatusPollingFromDB: jest.fn(),
    updateWithIncrement: jest.fn(),
    getPendingRecords: jest.fn()
};
//# sourceMappingURL=paymentStatusPolling.repository.js.map