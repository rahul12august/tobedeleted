"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const paymentStatusPolling_repository_1 = __importDefault(require("./paymentStatusPolling.repository"));
const billPaymentTransaction_enum_1 = require("../billPaymentTransaction/billPaymentTransaction.enum");
const paymentStatusPolling_constant_1 = require("./paymentStatusPolling.constant");
const billPayment_utils_1 = require("../billPayment.utils");
const configuration_service_1 = __importDefault(require("../../configuration/configuration.service"));
const logger_1 = __importStar(require("../../logger"));
const billPaymentTransaction_service_1 = __importDefault(require("../billPaymentTransaction/billPaymentTransaction.service"));
const transaction_producer_1 = __importDefault(require("../../transaction/transaction.producer"));
const billDetail_service_1 = __importDefault(require("../billDetail/billDetail.service"));
const updatePaymentStatusPollingConfig = (orderId, status, nextPollTime) => __awaiter(void 0, void 0, void 0, function* () {
    const newPollStatusEntry = {
        orderId: orderId,
        status: status,
        nextPollTime: nextPollTime
    };
    const response = yield paymentStatusPolling_repository_1.default.updateWithIncrement(newPollStatusEntry);
    return response;
});
const calculateEndDate = (startDate) => {
    const minutesToAddInMs = paymentStatusPolling_constant_1.MINUTES * 60000;
    return new Date(startDate.getTime() + minutesToAddInMs);
};
const getPendingRecords = (startDate) => __awaiter(void 0, void 0, void 0, function* () {
    return paymentStatusPolling_repository_1.default.getPendingRecords(billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.WAITING, calculateEndDate(startDate));
});
const updateManualIntervention = (orderId) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.error(`Bill Payment : Updating bill payment status for orderId : ${orderId} to ${billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.MANUAL_INTERVENTION}`);
    updatePaymentStatusPollingConfig(orderId, billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.MANUAL_INTERVENTION);
});
const isMarkedForNextDayProcessesing = (nextTime) => nextTime.type === module_common_1.BillingAggregatorPollingConfigType.ABSOLUTE;
const sendBillPaymentWaitingNotif = (orderId) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionStatus = yield billPaymentTransaction_service_1.default.getTransactionStatus(orderId);
    if (transactionStatus) {
        const billDetails = yield billDetail_service_1.default.getBillDetails(transactionStatus.referenceId);
        transaction_producer_1.default.sendPendingBillPaymentNotification({
            billDetails: billDetails,
            transactionAmount: transactionStatus.amount,
            transactionId: transactionStatus.transactionId,
            transactionTime: transactionStatus.createdAt || new Date()
        });
    }
});
const updatePaymentStatusConfig = (paymentStatusConfig, status, pollingConfigs) => __awaiter(void 0, void 0, void 0, function* () {
    switch (status) {
        case billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.SUCCESS: {
            updatePaymentStatusPollingConfig(paymentStatusConfig.orderId, status);
            break;
        }
        case billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.FAILURE: {
            logger_1.default.error(`Bill Payment : Updating bill payment failure status for orderId : ${paymentStatusConfig.orderId}`);
            updatePaymentStatusPollingConfig(paymentStatusConfig.orderId, status);
            break;
        }
        case billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.WAITING: {
            const nextTime = pollingConfigs[paymentStatusConfig.statusRetryCount];
            logger_1.default.info(`Bill Payment : Polling config for next time : ${JSON.stringify(nextTime)} for orderId: ${paymentStatusConfig.orderId}, RetryCount: ${paymentStatusConfig.statusRetryCount}`);
            if (nextTime) {
                if (isMarkedForNextDayProcessesing(nextTime)) {
                    logger_1.default.info(`Bill Payment : Sending notification for pending billpayment for orderId: ${paymentStatusConfig.orderId}`);
                    sendBillPaymentWaitingNotif(paymentStatusConfig.orderId);
                }
                updatePaymentStatusPollingConfig(paymentStatusConfig.orderId, status, yield billPayment_utils_1.getNextPollTime(nextTime));
            }
            else {
                return updateManualIntervention(paymentStatusConfig.orderId);
            }
            break;
        }
    }
});
const updatePollingConfig = (orderId, status) => __awaiter(void 0, void 0, void 0, function* () {
    const pollingConfig = yield configuration_service_1.default.getGoBillsPollingConfig();
    const paymentStatusPolling = yield paymentStatusPolling_repository_1.default.getPaymentStatusPollingFromDB(orderId);
    if (paymentStatusPolling) {
        yield updatePaymentStatusConfig(paymentStatusPolling, status, pollingConfig);
        return pollingConfig[paymentStatusPolling.statusRetryCount];
    }
    if (!paymentStatusPolling &&
        status === billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum.WAITING) {
        yield paymentStatusPolling_repository_1.default.create({
            orderId: orderId,
            statusRetryCount: 1,
            nextPollTime: yield billPayment_utils_1.getNextPollTime(pollingConfig[0]),
            status: status
        });
        return pollingConfig[0];
    }
    return pollingConfig[0];
});
const paymentStatusPollingService = {
    getPendingRecords,
    updatePollingConfig,
    updatePaymentStatusPollingConfig
};
exports.default = logger_1.wrapLogs(paymentStatusPollingService);
//# sourceMappingURL=paymentStatusPolling.service.js.map