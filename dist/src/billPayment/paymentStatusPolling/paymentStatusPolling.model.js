"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const billPaymentTransaction_enum_1 = require("../billPaymentTransaction/billPaymentTransaction.enum");
exports.collectionName = 'gobills_polling_state';
const PaymentStatusPollingSchema = new mongoose_1.Schema({
    orderId: {
        type: String,
        required: true,
        unique: true
    },
    status: {
        type: String,
        enum: Object.values(billPaymentTransaction_enum_1.BillPaymentTransactionStatusEnum),
        required: true
    },
    nextPollTime: {
        type: Date,
        required: true
    },
    statusRetryCount: {
        type: Number,
        required: true,
        default: 0
    }
}, {
    timestamps: true
});
PaymentStatusPollingSchema.index({ nextPollTime: 1, status: 1 });
exports.PaymentStatusPollingModel = mongoose_1.default.model(exports.collectionName, PaymentStatusPollingSchema);
//# sourceMappingURL=paymentStatusPolling.model.js.map