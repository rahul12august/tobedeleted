"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const billPayment_service_1 = __importDefault(require("./billPayment.service"));
const billPayment_validator_1 = require("./billPayment.validator");
const transaction_constant_1 = require("../transaction/transaction.constant");
const updateBillPaymentStatus = {
    method: module_common_1.Http.Method.PUT,
    path: '/private/jobs/bill-payment/status-update',
    options: {
        description: 'Private api is used by the scheduler to trigger update for all pending transaction from gobills',
        notes: 'Trigger update for all pending gobills transactions.',
        tags: ['private'],
        auth: false,
        handler: (_hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            yield billPayment_service_1.default.updatePendingTransactions();
            return hapiResponse.response().code(module_common_1.Http.StatusCode.ACCEPTED);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.ACCEPTED]: {
                        description: 'Update process triggered'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request.'
                    }
                }
            }
        }
    }
};
const updateTransactionStatus = {
    method: module_common_1.Http.Method.POST,
    path: '/bill-payment/manual-adjustment',
    options: {
        description: 'This api is used by admin portal to mark pending / MANUAL_INTERVENTION transaction as successful / failed',
        tags: ['private', 'api'],
        bind: {
            roles: [module_common_1.AdminPortalUserPermissionCode.CUSTOMER_INFO_WRITE]
        },
        validate: {
            payload: billPayment_validator_1.manualAdjustmentPayloadValidator
        },
        handler: (_hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { transactionId, status } = _hapiRequest.payload;
            const reqStatus = transaction_constant_1.MAP_GOBILL_STATUS_TO_TXN[status];
            yield billPayment_service_1.default.txnManualAdjustment(transactionId, reqStatus);
            return hapiResponse.response({ success: true }).code(module_common_1.Http.StatusCode.OK);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'Transaction manually adjusted'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error occured'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Invalid request'
                    },
                    [module_common_1.Http.StatusCode.NOT_FOUND]: {
                        description: 'Transaction not found'
                    }
                }
            }
        }
    }
};
const billPaymentController = [
    updateBillPaymentStatus,
    updateTransactionStatus
];
exports.default = billPaymentController;
//# sourceMappingURL=billPayment.controller.js.map