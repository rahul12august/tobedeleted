"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const httpClient_1 = __importDefault(require("./httpClient"));
const logger_1 = __importStar(require("../../logger"));
const lodash_1 = require("lodash");
const billDetail_constant_1 = require("./billDetail.constant");
const AppError_1 = require("../../errors/AppError");
const errors_1 = require("../../common/errors");
const billDetails_type_1 = require("./billDetails.type");
const transaction_enum_1 = require("../../transaction/transaction.enum");
const getAdminFee = (referenceId) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Bill Payment : Fetching admin fee for referenceId : ${referenceId}`);
    const response = yield httpClient_1.default.get(`${billDetail_constant_1.GET_BILL_DETAIL_URL}/${referenceId}/${billDetail_constant_1.ADMIN_FEE_PATH}`);
    const adminFee = lodash_1.get(response, ['data', 'data', billDetail_constant_1.ADMIN_FEE_PATH], null);
    if (!adminFee) {
        const detail = `Admin fee is not present in bill details for : ${referenceId}!`;
        logger_1.default.error(`Bill Payment: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.JAGO, errors_1.ERROR_CODE.ADMIN_FEE_NOT_FOUND);
    }
    return parseFloat(adminFee);
});
const getBillDetails = (referenceId) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Bill Payment : Fetching bill details for referenceId : ${referenceId}`);
    const response = yield httpClient_1.default.get(`${billDetail_constant_1.GET_BILL_DETAIL_URL}/${referenceId}?mapped_row_type=${billDetails_type_1.BillInfoMappedRowType.ALL}`);
    return lodash_1.get(response, ['data', 'data'], null);
});
const updateOrderDetails = (order) => __awaiter(void 0, void 0, void 0, function* () { return httpClient_1.default.patch(`${billDetail_constant_1.PATCH_ORDER_DETAILS_URL}`, order); });
const billDetailRepository = {
    getAdminFee,
    getBillDetails,
    updateOrderDetails
};
exports.default = logger_1.wrapLogs(billDetailRepository);
//# sourceMappingURL=billDetail.repository.js.map