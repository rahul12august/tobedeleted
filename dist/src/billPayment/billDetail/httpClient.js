"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_httpclient_1 = require("@dk/module-httpclient");
const config_1 = require("../../config");
const logger_1 = __importDefault(require("../../logger"));
const context_1 = require("../../context");
const constant_1 = require("../../common/constant");
const URL = config_1.config.get('ms').billPayment;
const httpClient = module_httpclient_1.createHttpClient({
    baseURL: URL,
    context: context_1.context,
    logger: logger_1.default,
    retryConfig: constant_1.retryConfig
});
logger_1.default.info(`Setting up http-client with bill payments on address : ${URL}`);
exports.default = httpClient;
//# sourceMappingURL=httpClient.js.map