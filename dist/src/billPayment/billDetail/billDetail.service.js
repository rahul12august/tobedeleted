"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const billDetail_repository_1 = __importDefault(require("./billDetail.repository"));
const logger_1 = __importStar(require("../../logger"));
const errors_1 = require("../../common/errors");
const lodash_1 = require("lodash");
const AppError_1 = require("../../errors/AppError");
const transaction_enum_1 = require("../../transaction/transaction.enum");
const getAdminFee = (referenceId) => __awaiter(void 0, void 0, void 0, function* () { return billDetail_repository_1.default.getAdminFee(referenceId); });
const getBillDetails = (referenceId) => __awaiter(void 0, void 0, void 0, function* () {
    const billDetails = yield billDetail_repository_1.default.getBillDetails(referenceId);
    if (!billDetails) {
        const detail = `Bill detail is not present for: ${referenceId}!`;
        logger_1.default.error(`Bill Payment: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_BILL_PAYMENT, errors_1.ERROR_CODE.BILL_DETAILS_NOT_FOUND);
    }
    return billDetails;
});
const updateOrderDetails = (referenceId, order) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (!lodash_1.isEmpty(order)) {
            const body = {
                inquiryId: referenceId,
                order: order
            };
            logger_1.default.info(`Update Order Details : Fetching order details for referenceId : ${referenceId} with orderDetails: ${JSON.stringify(body)}`);
            yield billDetail_repository_1.default.updateOrderDetails(body);
        }
    }
    catch (err) {
        logger_1.default.error(`Error: While updating order details for referenceId: ${referenceId} with payload: ${JSON.stringify(order)}`);
        throw err;
    }
});
const billDetailService = {
    getAdminFee,
    getBillDetails,
    updateOrderDetails
};
exports.default = logger_1.wrapLogs(billDetailService);
//# sourceMappingURL=billDetail.service.js.map