"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const billDetail_service_1 = __importDefault(require("./billDetail.service"));
const logger_1 = __importStar(require("../../logger"));
const AppError_1 = require("../../errors/AppError");
const errors_1 = require("../../common/errors");
const module_common_1 = require("@dk/module-common");
const isApplicable = (input) => input.basicFee.thirdPartyFee === true &&
    input.beneficiaryBankCodeChannel === module_common_1.BankChannelEnum.GOBILLS;
const getFee = (input) => __awaiter(void 0, void 0, void 0, function* () {
    if (!input.externalId) {
        logger_1.default.error('Bill Payment : ExternalId is not provided for getFee, payload : ', JSON.stringify(input));
        throw new AppError_1.AppError(errors_1.ERROR_CODE.MISSING_INQUIRY_ID);
    }
    return billDetail_service_1.default.getAdminFee(input.externalId);
});
const billDetailTemplate = {
    isApplicable,
    getFee
};
exports.default = logger_1.wrapLogs(billDetailTemplate);
//# sourceMappingURL=billDetail.template.js.map