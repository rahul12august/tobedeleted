"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    getAdminFee: jest.fn(),
    getBillDetails: jest.fn(),
    updateOrderDetails: jest.fn()
};
//# sourceMappingURL=billDetail.service.js.map