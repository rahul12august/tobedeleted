"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const getPrimaryBillKey = () => ({
    key: module_common_1.BillKeyType.PHONE_NUMBER,
    value: '1234567890'
});
exports.getBillDetails = () => ({
    customerId: '123456',
    primaryBillKey: getPrimaryBillKey(),
    transactionId: 'TRAN01',
    billerName: 'PLN'
});
exports.billDetailsResponse = () => ({
    data: {
        data: exports.getBillDetails()
    }
});
exports.feeRuleTemplatePayloadForThirdPartyFee = () => ({
    basicFee: {
        thirdPartyFee: true,
        code: 'BC144',
        customerTc: 'dummy',
        customerTcInfo: {
            channel: 'dummy'
        },
        subsidiary: false
    },
    transactionAmount: 1000,
    externalId: 'dummy123',
    beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.GOBILLS
});
exports.adminFeeResponse = () => ({
    data: {
        data: {
            'admin-fee': '100'
        }
    }
});
//# sourceMappingURL=billDetail.data.js.map