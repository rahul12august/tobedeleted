"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BillInfoMappedRowType;
(function (BillInfoMappedRowType) {
    BillInfoMappedRowType["VISIBLE"] = "visible";
    BillInfoMappedRowType["ALL"] = "all";
})(BillInfoMappedRowType || (BillInfoMappedRowType = {}));
exports.BillInfoMappedRowType = BillInfoMappedRowType;
//# sourceMappingURL=billDetails.type.js.map