"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GET_BILL_DETAIL_URL = '/private/bill-detail';
exports.GET_BILL_DETAIL_URL = GET_BILL_DETAIL_URL;
const PATCH_ORDER_DETAILS_URL = '/private/order-details';
exports.PATCH_ORDER_DETAILS_URL = PATCH_ORDER_DETAILS_URL;
const ADMIN_FEE_PATH = 'admin-fee';
exports.ADMIN_FEE_PATH = ADMIN_FEE_PATH;
//# sourceMappingURL=billDetail.constant.js.map