"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ping = {
    method: 'GET',
    path: '/ping',
    options: {
        description: 'Pongs back',
        notes: 'Private Api to check if service pongs on a ping',
        tags: ['api'],
        auth: false,
        handler: (_request, _h) => 'pong!'
    }
};
const healthController = [ping];
exports.default = healthController;
//# sourceMappingURL=healthcheck.controller.js.map