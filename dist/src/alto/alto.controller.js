"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const alto_validator_1 = require("./alto.validator");
const contextHandler_1 = require("../common/contextHandler");
const alto_service_1 = __importDefault(require("./alto.service"));
const logger_1 = __importDefault(require("../logger"));
const alto_utils_1 = __importDefault(require("./alto.utils"));
const alto_constant_1 = require("./alto.constant");
const checkStatus = {
    method: module_common_1.Http.Method.POST,
    path: '/qris/status',
    options: {
        description: 'Checking and updating status for QRIS payment',
        notes: 'Public Api endpoint called from Jago app to check with Alto and update QRIS payment status',
        tags: ['api', 'transactions', 'QRIS', 'payment'],
        validate: {
            payload: alto_validator_1.qrisCheckStatusRequestValidator
        },
        response: {
            schema: alto_validator_1.qrisCheckStatusResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { payload } = hapiRequest;
            yield contextHandler_1.updateRequestIdInContext(payload.paymentInstructionId);
            logger_1.default.info(`Payload: ${JSON.stringify(payload)}`);
            const statusResponse = yield alto_service_1.default.checkStatus(payload.customerId, payload.paymentInstructionId);
            return hapiResponse.response(statusResponse).code(module_common_1.Http.StatusCode.OK);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'Ok'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    },
                    [module_common_1.Http.StatusCode.UNAUTHORIZED]: {
                        description: 'UNAUTHORIZED'
                    }
                }
            }
        }
    }
};
const refundTransaction = {
    method: module_common_1.Http.Method.POST,
    path: '/alto/refund',
    options: {
        description: 'Refund Transaction for QRIS payment',
        notes: 'Webhook API for ALTO to refund QRIS Transaction',
        tags: ['api', 'transactions', 'QRIS', 'refund'],
        auth: false,
        validate: {
            payload: alto_validator_1.altoRefundRequestValidator
        },
        response: {
            schema: alto_validator_1.altoRefundResponseValidator
        },
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const { payload } = hapiRequest;
            yield contextHandler_1.updateRequestIdInContext(payload.data.customer_reference_number);
            logger_1.default.info(`refundTransaction: Payload: ${JSON.stringify(payload)}`);
            const processRefund = yield alto_service_1.default.refundTransaction(payload.data);
            logger_1.default.info(`refundTransaction (delayed ${alto_constant_1.RESPONSE_DELAY}ms) : Response: ${JSON.stringify(processRefund)}`);
            yield alto_utils_1.default.sleep(alto_constant_1.RESPONSE_DELAY);
            return hapiResponse
                .response(processRefund)
                .code(module_common_1.Http.StatusCode.OK)
                .header(module_common_1.NO_WRAPPING, 'true');
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'Ok'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const altoController = [checkStatus, refundTransaction];
exports.default = altoController;
//# sourceMappingURL=alto.controller.js.map