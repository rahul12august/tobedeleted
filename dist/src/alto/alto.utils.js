"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const alto_constant_1 = require("./alto.constant");
const logger_1 = __importDefault(require("../logger"));
const alto_enum_1 = require("./alto.enum");
const module_common_1 = require("@dk/module-common");
const errors_1 = require("../common/errors");
const alto_type_1 = require("./alto.type");
const lodash_1 = require("lodash");
const moment_1 = __importDefault(require("moment"));
const transaction_enum_1 = require("../transaction/transaction.enum");
const transaction_helper_1 = __importDefault(require("../transaction/transaction.helper"));
const transactionHistory_enum_1 = require("../transactionHistory/transactionHistory.enum");
const transaction_producer_enum_1 = require("../transaction/transaction.producer.enum");
const dateUtils_1 = require("../common/dateUtils");
const transaction_constant_1 = require("../transaction/transaction.constant");
const currencyFormatter_1 = require("../common/currencyFormatter");
const module_httpclient_1 = require("@dk/module-httpclient");
exports.shouldRetryQrisTransaction = (error) => {
    return error.errorCode === errors_1.ERROR_CODE.ALTO_SERVER_ERROR;
};
const maskName = (fullName) => {
    let nameArray = fullName
        .toUpperCase()
        .substring(0, alto_constant_1.MAX_LENGTH_CUSTOMER_NAME)
        .trim()
        .split(' ');
    return nameArray
        .map(name => name[0] + name.slice(1).replace(/./g, 'X'))
        .join(' ');
};
const generatePaymentPayload = (payload) => {
    const updatedPayload = {
        command: alto_enum_1.AltoCommandEnum.PAYMENT_CREDIT,
        data: Object.assign({}, payload)
    };
    updatedPayload.data.customer.name = maskName(payload.customer.name);
    return updatedPayload;
};
const generateStatusInquiryPayload = (timestamp, customerReferenceNumber) => {
    /* eslint-disable @typescript-eslint/camelcase */
    return {
        command: alto_enum_1.AltoCommandEnum.CHECK_STATUS,
        data: {
            date_time: timestamp,
            customer_reference_number: customerReferenceNumber
        }
    };
};
const generateAuthorizationId = () => {
    const stringifiedDate = Date.now().toString(36);
    const stringifiedRandomMath = Math.random()
        .toString(36)
        .slice(2);
    return (stringifiedDate.slice(stringifiedDate.length - 3).toUpperCase() +
        stringifiedRandomMath.slice(stringifiedRandomMath.length - 3).toUpperCase());
};
const generateCustomerPan = (pan) => {
    const concatPan = `${alto_constant_1.ISSUER_NNS}${pan}`;
    const getMaxPan = concatPan.length >= alto_constant_1.CUSTOMER_PAN_MAX_LENGTH
        ? concatPan.slice(0, alto_constant_1.CUSTOMER_PAN_MAX_LENGTH - 1)
        : concatPan;
    return `${getMaxPan}${module_common_1.qrisEncryption.getCheckDigit(getMaxPan)}`;
};
const isValidResponse = (result) => result &&
    result.response_code &&
    alto_constant_1.VALID_RESPONSES.includes(result.response_code);
const getExceptionErrorCode = (error) => {
    if (error.errorCode) {
        return error.errorCode;
    }
    else if (error.toString().includes('Error: timeout')) {
        return errors_1.ERROR_CODE.QRIS_TRANSACTION_TIMEOUT;
    }
    else {
        return errors_1.ERROR_CODE.ERROR_FROM_ALTO;
    }
};
const getQrisResponseError = (result) => {
    const responseCode = result.response_code;
    const detail = `Request failed with response status code: ${responseCode}!`;
    if (responseCode === alto_enum_1.AltoResponseCodeEnum.SUSPECT ||
        responseCode === alto_enum_1.AltoResponseCodeEnum.IDENTICAL_TRANSACTION_STILL_ON_PROCESS) {
        return new alto_type_1.QrisError(detail, transaction_enum_1.TransferFailureReasonActor.QRIS, errors_1.ERROR_CODE.QRIS_TRANSACTION_TIMEOUT, alto_enum_1.QrisTransactionStatusEnum.SUSPECT, result.data.forwarding_customer_reference_number, responseCode);
    }
    else {
        return new alto_type_1.QrisError(detail, transaction_enum_1.TransferFailureReasonActor.ALTO, errors_1.ERROR_CODE.ERROR_FROM_ALTO, alto_enum_1.QrisTransactionStatusEnum.FAILED, result.data.forwarding_customer_reference_number, responseCode);
    }
};
const getAltoHttpClientError = (error) => {
    if (error.toString().includes('Error: timeout')) {
        return new alto_type_1.QrisError('Transaction request timeout error!', transaction_enum_1.TransferFailureReasonActor.QRIS, errors_1.ERROR_CODE.QRIS_TRANSACTION_TIMEOUT, alto_enum_1.QrisTransactionStatusEnum.SUSPECT, alto_constant_1.EMPTY);
    }
    else if (error instanceof module_httpclient_1.HttpError && error.status >= 500) {
        return new alto_type_1.QrisError('Alto server encountered an unexpected condition!', transaction_enum_1.TransferFailureReasonActor.ALTO, errors_1.ERROR_CODE.ALTO_SERVER_ERROR, alto_enum_1.QrisTransactionStatusEnum.FAILED, alto_constant_1.EMPTY);
    }
    else {
        return new alto_type_1.QrisError('Unknown issue with alto!', transaction_enum_1.TransferFailureReasonActor.ALTO, errors_1.ERROR_CODE.ERROR_FROM_ALTO, alto_enum_1.QrisTransactionStatusEnum.FAILED, alto_constant_1.EMPTY);
    }
};
const isQrisTimeoutError = (error) => !lodash_1.isEmpty(error) &&
    error instanceof alto_type_1.QrisError &&
    error.errorCode === errors_1.ERROR_CODE.QRIS_TRANSACTION_TIMEOUT;
const getQrisPaymentStatusOnCreatePayment = (altoResponseCode) => {
    logger_1.default.info(`getQrisPaymentStatusOnCreatePayment: getting QRIS transaction status by altoResponseCode: ${altoResponseCode}`);
    if (altoResponseCode === alto_enum_1.AltoResponseCodeEnum.SUCCESS) {
        return alto_enum_1.QrisTransactionStatusEnum.SUCCESS;
    }
    else if (altoResponseCode === alto_enum_1.AltoResponseCodeEnum.SUSPECT ||
        altoResponseCode ===
            alto_enum_1.AltoResponseCodeEnum.IDENTICAL_TRANSACTION_STILL_ON_PROCESS) {
        return alto_enum_1.QrisTransactionStatusEnum.SUSPEND;
    }
    else if (altoResponseCode ===
        alto_enum_1.AltoResponseCodeEnum.IDENTICAL_TRANSACTION_HAS_BEEN_SUCCESS ||
        altoResponseCode ===
            alto_enum_1.AltoResponseCodeEnum.IDENTICAL_TRANSACTION_HAS_BEEN_FAILED) {
        return alto_enum_1.QrisTransactionStatusEnum.SHOULD_CHECK_STATUS;
    }
    else {
        return alto_enum_1.QrisTransactionStatusEnum.FAILED;
    }
};
const buildCreatePaymentResponse = (result) => {
    const data = result.data;
    return {
        amount: data.amount,
        currencyCode: data.currency_code,
        customerReferenceNumber: data.customer_reference_number,
        fee: data.fee,
        forwardingCustomerReferenceNumber: data.forwarding_customer_reference_number,
        invoiceNo: data.invoice_no,
        transactionStatus: getQrisPaymentStatusOnCreatePayment(result.response_code)
    };
};
const getQrisPaymentStatusOnCheckStatus = (altoResponseCode) => {
    logger_1.default.info(`getQrisPaymentStatusOnCheckStatus: getting QRIS transaction status by altoResponseCode: ${altoResponseCode}`);
    if (altoResponseCode === alto_enum_1.AltoResponseCodeEnum.SUCCESS) {
        return alto_enum_1.QrisTransactionStatusEnum.SUCCESS;
    }
    else if (altoResponseCode === alto_enum_1.AltoResponseCodeEnum.REJECTED) {
        return alto_enum_1.QrisTransactionStatusEnum.FAILED;
    }
    else {
        return alto_enum_1.QrisTransactionStatusEnum.SUSPEND;
    }
};
const buildCheckStatusResponse = (altoResponse) => {
    let transactionStatus;
    let responseCode;
    let acquirerResponse = altoResponse.data;
    if (altoResponse.response_code === alto_enum_1.AltoResponseCodeEnum.SUCCESS) {
        // Success from Alto
        // Need to check the response from Acquirer
        responseCode = acquirerResponse.transaction_response_code;
        transactionStatus = getQrisPaymentStatusOnCheckStatus(responseCode);
    }
    else if (altoResponse.response_code === alto_enum_1.AltoResponseCodeEnum.REJECTED) {
        // Rejected from Alto
        responseCode = altoResponse.response_code;
        transactionStatus = alto_enum_1.QrisTransactionStatusEnum.FAILED;
    }
    else {
        // Suspect from Alto or another failed with specific code
        // This will make the transaction refundable from Admin Portal
        responseCode = altoResponse.response_code;
        transactionStatus = alto_enum_1.QrisTransactionStatusEnum.SUSPEND;
    }
    return {
        transactionStatus: transactionStatus,
        transactionResponseCode: responseCode,
        customerReferenceNumber: acquirerResponse.customer_reference_number,
        forwardingCustomerReferenceNumber: acquirerResponse.forwarding_customer_reference_number,
        invoiceNo: acquirerResponse.invoice_no
    };
};
const isSuccessQrisPayment = (qrisTransactionStatus) => qrisTransactionStatus === alto_enum_1.QrisTransactionStatusEnum.SUCCESS;
const isFailedQrisPayment = (qrisTransactionStatus) => qrisTransactionStatus === alto_enum_1.QrisTransactionStatusEnum.FAILED;
const shouldAutoRefundQrisPayment = (response) => isFailedQrisPayment(response.transactionStatus) &&
    response.transactionResponseCode === alto_enum_1.AltoResponseCodeEnum.REJECTED;
const shouldUpdateTransaction = (response) => {
    return (!lodash_1.isEmpty(response.transactionStatus) &&
        (isFailedQrisPayment(response.transactionStatus) ||
            isSuccessQrisPayment(response.transactionStatus)));
};
const buildRefundRequestData = (checkStatusResponse, originalTransaction) => {
    return {
        customer_reference_number: checkStatusResponse.customerReferenceNumber,
        invoice_no: checkStatusResponse.invoiceNo,
        reference_number: originalTransaction.thirdPartyOutgoingId || '',
        date_time: moment_1.default()
            .utc()
            .toISOString(),
        currency_code: alto_constant_1.CURRENCY_CODE,
        amount_refund: originalTransaction.transactionAmount
    };
};
const getAltoReferenceNumber = (forwardingCRN) => forwardingCRN.slice(forwardingCRN.length - alto_constant_1.ALTO_REFERENCE_NUMBER_LENGTH);
const setQrisResponseToTransactionData = (transactionModel, forwardingCRN) => {
    transactionModel.additionalInformation4 = forwardingCRN;
    if (forwardingCRN.length > alto_constant_1.ALTO_REFERENCE_NUMBER_LENGTH) {
        transactionModel.thirdPartyOutgoingId = getAltoReferenceNumber(forwardingCRN);
    }
    else {
        transactionModel.thirdPartyOutgoingId = forwardingCRN;
    }
};
const updateTransactionDataOnQrisPaymentSuccess = (transactionModel, qrisResponse) => {
    var _a;
    (_a = transactionModel.coreBankingTransactions) === null || _a === void 0 ? void 0 : _a.push({
        id: qrisResponse.invoiceNo,
        type: transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_CREDITED
    });
    if (transactionModel.coreBankingTransactionIds && qrisResponse.invoiceNo) {
        transactionModel.coreBankingTransactionIds.push(qrisResponse.invoiceNo);
    }
    transactionModel.journey = transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_CREDITED, transactionModel);
    setQrisResponseToTransactionData(transactionModel, qrisResponse.forwardingCustomerReferenceNumber);
};
const isAmountCredited = (transactionModel) => {
    var _a;
    return !lodash_1.isEmpty(transactionModel.journey) && ((_a = transactionModel.journey) === null || _a === void 0 ? void 0 : _a.some(journeyItem => journeyItem.status === transaction_enum_1.TransferJourneyStatusEnum.AMOUNT_CREDITED));
};
const generateBeneficiaryAccountNumberWithLundCheckDigit = (merchantPan) => {
    const checkDigit = module_common_1.qrisEncryption.getCheckDigit(merchantPan);
    return `${merchantPan}${checkDigit}`;
};
const sleep = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
};
const isQrisTransaction = (transaction) => transaction.paymentServiceType === module_common_1.PaymentServiceTypeEnum.QRIS;
const getTransactionHistoryStatus = (qrisResponseStatus) => {
    let transactionHistoryStatus;
    if (qrisResponseStatus == alto_enum_1.QrisTransactionStatusEnum.SUCCESS) {
        transactionHistoryStatus = transactionHistory_enum_1.TransactionHistoryStatusEnum.SUCCEED;
    }
    else if (qrisResponseStatus == alto_enum_1.QrisTransactionStatusEnum.FAILED) {
        transactionHistoryStatus = transactionHistory_enum_1.TransactionHistoryStatusEnum.FAILED;
    }
    else {
        transactionHistoryStatus = transactionHistory_enum_1.TransactionHistoryStatusEnum.PENDING;
    }
    return transactionHistoryStatus;
};
const getQrisNotificationCode = (qrisResponseStatus) => {
    let notificationCode;
    if (qrisResponseStatus == alto_enum_1.QrisTransactionStatusEnum.SUCCESS) {
        notificationCode = transaction_producer_enum_1.NotificationCode.NOTIF_SUCCESS_QRIS;
    }
    else if (qrisResponseStatus == alto_enum_1.QrisTransactionStatusEnum.FAILED) {
        notificationCode = transaction_producer_enum_1.NotificationCode.NOTIF_FAILED_QRIS;
    }
    else {
        notificationCode = transaction_producer_enum_1.NotificationCode.NOTIF_PENDING_QRIS;
    }
    return notificationCode;
};
const buildQrisNotificationMessage = (transaction) => {
    const jakartaDate = dateUtils_1.toJKTDate(transaction.createdAt || transaction.updatedAt, transaction_constant_1.FORMAT_DATETIME_EMAIL_DETAIL) +
        ' ' +
        alto_constant_1.JAKARTA_TIMEZONE;
    return {
        customerName: transaction.sourceAccountName,
        sourceAccountType: transaction.sourceAccountType,
        sourceAccountNo: transaction.sourceAccountNo,
        beneficiaryBankName: transaction.beneficiaryBankName,
        beneficiaryAccountNo: transaction.beneficiaryAccountNo,
        beneficiaryName: transaction.beneficiaryAccountName,
        amount: currencyFormatter_1.toIndonesianRupiah(transaction.transactionAmount),
        transactionDate: jakartaDate
    };
};
const getQrisTransactionStatus = (transactionStatus) => transactionStatus === transaction_enum_1.TransactionStatus.SUCCEED
    ? alto_enum_1.QrisTransactionStatusEnum.SUCCESS
    : alto_enum_1.QrisTransactionStatusEnum.FAILED;
const isTransactionSettled = (transactionStatus) => transactionStatus !== transaction_enum_1.TransactionStatus.SUBMITTED &&
    transactionStatus !== transaction_enum_1.TransactionStatus.SUBMITTING;
const updateMambuEnabled = () => process.env.QRIS_PATCH_MAMBU_ENABLED == 'true';
const altoUtil = {
    maskName,
    generatePaymentPayload,
    generateStatusInquiryPayload,
    generateAuthorizationId,
    generateBeneficiaryAccountNumberWithLundCheckDigit,
    generateCustomerPan,
    getAltoHttpClientError,
    getAltoReferenceNumber,
    getExceptionErrorCode,
    getQrisNotificationCode,
    getQrisResponseError,
    getQrisTransactionStatus,
    getTransactionHistoryStatus,
    buildCreatePaymentResponse,
    buildCheckStatusResponse,
    buildQrisNotificationMessage,
    buildRefundRequestData,
    isAmountCredited,
    isSuccessQrisPayment,
    isTransactionSettled,
    isFailedQrisPayment,
    isValidResponse,
    isQrisTimeoutError,
    isQrisTransaction,
    setQrisResponseToTransactionData,
    shouldUpdateTransaction,
    shouldAutoRefundQrisPayment,
    sleep,
    updateTransactionDataOnQrisPaymentSuccess,
    updateMambuEnabled
};
exports.default = altoUtil;
//# sourceMappingURL=alto.utils.js.map