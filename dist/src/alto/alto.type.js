"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AppError_1 = require("../errors/AppError");
class QrisError extends AppError_1.TransferAppError {
    constructor(detail, actor, errorCode, status, forwardingCustomerReferenceNumber, responseCode, errors) {
        super(detail, actor, errorCode, errors);
        this.qrisPaymentStatus = status;
        this.responseCode = responseCode;
        this.forwardingCustomerReferenceNumber = forwardingCustomerReferenceNumber;
    }
}
exports.QrisError = QrisError;
//# sourceMappingURL=alto.type.js.map