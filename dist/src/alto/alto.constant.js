"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const alto_enum_1 = require("../alto/alto.enum");
exports.BASIC_PREFIX = 'Basic';
exports.ORDER_TYPE = 'order';
exports.TokenConstants = {
    TOKEN_EXPIRY_BUFFER: 600,
    DEFAULT_TTL: 3000
};
exports.REDIS_TOKEN_KEY = 'ALTO_TOKEN';
exports.PAYMENT_ENDPOINT_METHOD = 'POST';
exports.STATUS_ENDPOINT_METHOD = 'POST';
exports.AltoHttpHeaders = {
    CONTENT_TYPE: 'Content-Type',
    ACCEPT: 'Accept',
    X_ALTO_KEY: 'X-Alto-Key',
    X_ALTO_TIMESTAMP: 'X-Alto-Timestamp',
    X_ALTO_SIGNATURE: 'X-Alto-Signature'
};
exports.DEFAULT_RESPONSE_DELAY = 0;
exports.RESPONSE_DELAY = process.env.ALTO_RESPONSE_DELAY
    ? parseInt(process.env.ALTO_RESPONSE_DELAY)
    : exports.DEFAULT_RESPONSE_DELAY;
exports.DEFAULT_REQUEST_TIMEOUT = 45000;
exports.REQUEST_TIMEOUT = process.env.ALTO_REQUEST_TIMEOUT
    ? parseInt(process.env.ALTO_REQUEST_TIMEOUT)
    : exports.DEFAULT_REQUEST_TIMEOUT;
exports.MAX_LENGTH_CUSTOMER_NAME = 30;
exports.retryConfig = {
    networkErrorCodesToRetry: ['ENOENT', 'ECONNREFUSED'],
    maxRetriesOnError: 3,
    delay: 1000
};
exports.ISSUER_NNS = process.env.ALTO_ISSUER_NNS || '';
exports.CUSTOMER_PAN_MAX_LENGTH = 19;
exports.TIMESTAMP_FORMAT = 'YYYY-MM-DD hh:mm:ss.SSS[Z]';
exports.CURRENCY_CODE = 'IDR';
exports.EMPTY = '';
exports.MAX_CHECK_STATUS_RETRY_ATTEMPT = 3;
exports.ALTO_REFERENCE_NUMBER_LENGTH = 10;
exports.VALID_RESPONSES = [
    alto_enum_1.AltoResponseCodeEnum.SUCCESS,
    alto_enum_1.AltoResponseCodeEnum.IDENTICAL_TRANSACTION_HAS_BEEN_SUCCESS,
    alto_enum_1.AltoResponseCodeEnum.IDENTICAL_TRANSACTION_HAS_BEEN_FAILED
];
exports.JAKARTA_TIMEZONE = 'WIB';
exports.QRIS_PAYMENT_SERVICE_CODE = 'QRIS_PAYOUTS';
//# sourceMappingURL=alto.constant.js.map