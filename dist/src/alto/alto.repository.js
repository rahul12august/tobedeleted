"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable @typescript-eslint/camelcase */
const moment_1 = __importDefault(require("moment"));
const config_1 = require("../config");
const httpClient_1 = __importStar(require("./httpClient"));
const logger_1 = __importStar(require("../logger"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const alto_constant_1 = require("./alto.constant");
const alto_enum_1 = require("./alto.enum");
const lodash_1 = require("lodash");
const redis_1 = __importDefault(require("../common/redis"));
const alto_encryption_1 = require("./alto.encryption");
const alto_utils_1 = __importDefault(require("./alto.utils"));
const transaction_enum_1 = require("../transaction/transaction.enum");
const { transactionPath, tokenPath, statusPath } = config_1.config.get('qrisAlto');
const configurationPrefixKey = 'ms-bill-payment';
const getTokenPayload = 'grant_type=client_credentials';
const apiKey = process.env.ALTO_API_KEY || '';
const getTokenFromCache = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let result = yield redis_1.default.get(alto_constant_1.REDIS_TOKEN_KEY, configurationPrefixKey);
        return result;
    }
    catch (error) {
        logger_1.default.info('Token is missing in cache.');
        return;
    }
});
const setTokenToCache = (result) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let ttl = result.expires_in - alto_constant_1.TokenConstants.TOKEN_EXPIRY_BUFFER;
        if (Number.isNaN(ttl) || ttl < 0) {
            ttl = alto_constant_1.TokenConstants.DEFAULT_TTL;
        }
        yield redis_1.default.mSet({
            prefix: configurationPrefixKey,
            keyValueObjectList: [
                {
                    key: alto_constant_1.REDIS_TOKEN_KEY,
                    value: result,
                    ttl: ttl,
                    shouldStringifyValue: true
                }
            ]
        });
    }
    catch (error) {
        logger_1.default.warn(`Failed to update token in cache. ${error}`);
        // not throwing error, as process can still continue if token is missing from cache
    }
});
const requestTokenFromAlto = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield httpClient_1.tokenHTTPClient.post(tokenPath, getTokenPayload);
        return response.data;
    }
    catch (error) {
        logger_1.default.error(`Error fetching access token from Alto. ${error}`);
        throw new AppError_1.TransferAppError('Error fetching access token from Alto.', transaction_enum_1.TransferFailureReasonActor.ALTO, errors_1.ERROR_CODE.ALTO_AUTH_TOKEN_ACCESS_ERROR);
    }
});
const getToken = () => __awaiter(void 0, void 0, void 0, function* () {
    let result = yield getTokenFromCache();
    if (result && !lodash_1.isEmpty(result)) {
        return result.access_token;
    }
    result = yield requestTokenFromAlto();
    if (!result.access_token || lodash_1.isEmpty(result.access_token)) {
        logger_1.default.error(`Cannot get token from Alto. ${JSON.stringify(result)}`);
        throw new AppError_1.TransferAppError('Cannot get token from Alto.', transaction_enum_1.TransferFailureReasonActor.ALTO, errors_1.ERROR_CODE.ALTO_AUTH_TOKEN_ACCESS_ERROR);
    }
    yield setTokenToCache(result);
    return result.access_token;
});
const sendRequestToAlto = (targetUrl, accessToken, signature, payload) => __awaiter(void 0, void 0, void 0, function* () {
    const requestHeaders = {
        Authorization: `Bearer ${accessToken}`,
        [alto_constant_1.AltoHttpHeaders.X_ALTO_KEY]: apiKey,
        [alto_constant_1.AltoHttpHeaders.X_ALTO_SIGNATURE]: signature,
        [alto_constant_1.AltoHttpHeaders.X_ALTO_TIMESTAMP]: payload.data.date_time
    };
    logger_1.default.info(`Sending request to Alto ${targetUrl}.
      Request Headers: ${JSON.stringify(requestHeaders)},
      Request Payload: ${JSON.stringify(payload)}`);
    try {
        const result = yield httpClient_1.default.post(targetUrl, payload, {
            headers: requestHeaders
        });
        logger_1.default.info(`Receiving response from Alto ${targetUrl}:
        Response Code: ${result.data.response_code},
        Response Type: ${result.data.response_text},
        Response Data: ${JSON.stringify(result.data.data)}`);
        return result.data;
    }
    catch (error) {
        logger_1.default.error(`Error while sending request to Alto. ${error}`);
        throw alto_utils_1.default.getAltoHttpClientError(error);
    }
});
const submitTransaction = (payload) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Submitting QRIS transaction.
      Issuer NNS : ${payload.issuer_nns}, 
      National MID: ${payload.national_mid},
      Customer Reference Number: ${payload.customer_reference_number}`);
    const updatedPayload = alto_utils_1.default.generatePaymentPayload(payload);
    const accessToken = yield getToken();
    const signature = yield alto_encryption_1.generateSignature(alto_constant_1.PAYMENT_ENDPOINT_METHOD, transactionPath, JSON.stringify(updatedPayload), payload.date_time);
    const result = yield sendRequestToAlto(transactionPath, accessToken, signature, updatedPayload);
    if (!alto_utils_1.default.isValidResponse(result)) {
        logger_1.default.info(`QRIS transaction failed with code ${result.response_code}`);
        throw alto_utils_1.default.getQrisResponseError(result);
    }
    return alto_utils_1.default.buildCreatePaymentResponse(result);
});
const checkPaymentStatus = (customerReferenceNumber) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Status inquiry for Customer Reference Number: ${customerReferenceNumber}`);
    const dynamicTimestamp = moment_1.default().format(alto_constant_1.TIMESTAMP_FORMAT);
    const accessToken = yield getToken();
    const inqPayload = alto_utils_1.default.generateStatusInquiryPayload(dynamicTimestamp, customerReferenceNumber);
    const signature = yield alto_encryption_1.generateSignature(alto_constant_1.STATUS_ENDPOINT_METHOD, statusPath, JSON.stringify(inqPayload), dynamicTimestamp);
    try {
        const result = yield sendRequestToAlto(statusPath, accessToken, signature, inqPayload);
        return alto_utils_1.default.buildCheckStatusResponse(result);
    }
    catch (error) {
        if (alto_utils_1.default.isQrisTimeoutError(error)) {
            const qrisError = error;
            return {
                customerReferenceNumber: customerReferenceNumber,
                transactionStatus: alto_enum_1.QrisTransactionStatusEnum.SUSPEND,
                invoiceNo: alto_constant_1.EMPTY,
                forwardingCustomerReferenceNumber: qrisError.forwardingCustomerReferenceNumber || alto_constant_1.EMPTY
            };
        }
        throw error;
    }
});
const altoRepository = logger_1.wrapLogs({
    getToken,
    submitTransaction,
    checkPaymentStatus
});
exports.default = altoRepository;
//# sourceMappingURL=alto.repository.js.map