"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AltoAccountTypeEnum;
(function (AltoAccountTypeEnum) {
    AltoAccountTypeEnum["SAVING"] = "SAVING";
    AltoAccountTypeEnum["CURRENT"] = "CURRENT";
    AltoAccountTypeEnum["CREDIT"] = "CREDIT";
    AltoAccountTypeEnum["E_WALLET"] = "E-WALLET";
})(AltoAccountTypeEnum = exports.AltoAccountTypeEnum || (exports.AltoAccountTypeEnum = {}));
var AltoDefaultAuthorizationId;
(function (AltoDefaultAuthorizationId) {
    AltoDefaultAuthorizationId["FAILED"] = "000000";
})(AltoDefaultAuthorizationId = exports.AltoDefaultAuthorizationId || (exports.AltoDefaultAuthorizationId = {}));
var AltoRefundResponseCodeEnum;
(function (AltoRefundResponseCodeEnum) {
    AltoRefundResponseCodeEnum["SUCCESS"] = "001";
    AltoRefundResponseCodeEnum["INVALID_AMOUNT"] = "050";
    AltoRefundResponseCodeEnum["FAILED"] = "003";
})(AltoRefundResponseCodeEnum = exports.AltoRefundResponseCodeEnum || (exports.AltoRefundResponseCodeEnum = {}));
var AltoRefundResponseTextEnum;
(function (AltoRefundResponseTextEnum) {
    AltoRefundResponseTextEnum["SUCCESS"] = "Success";
    AltoRefundResponseTextEnum["INVALID_AMOUNT"] = "Invalid amount to payment";
    AltoRefundResponseTextEnum["FAILED"] = "Failed";
})(AltoRefundResponseTextEnum = exports.AltoRefundResponseTextEnum || (exports.AltoRefundResponseTextEnum = {}));
var AltoCommandEnum;
(function (AltoCommandEnum) {
    AltoCommandEnum["QRIS_REFUND"] = "qr-refund";
    AltoCommandEnum["CHECK_STATUS"] = "qr-check-status";
    AltoCommandEnum["PAYMENT_CREDIT"] = "qr-payment-credit";
})(AltoCommandEnum = exports.AltoCommandEnum || (exports.AltoCommandEnum = {}));
var AltoResponseCodeEnum;
(function (AltoResponseCodeEnum) {
    AltoResponseCodeEnum["UNKNOWN"] = "000";
    AltoResponseCodeEnum["SUCCESS"] = "001";
    AltoResponseCodeEnum["SUSPECT"] = "002";
    AltoResponseCodeEnum["FAILED"] = "003";
    AltoResponseCodeEnum["UNKNOWN_ERROR_FROM_BANK"] = "004";
    AltoResponseCodeEnum["REJECTED_BY_DESTINATION_BANK"] = "010";
    AltoResponseCodeEnum["REJECTED_BY_ALTO"] = "011";
    AltoResponseCodeEnum["REJECTED_BY_SWITCHING"] = "012";
    AltoResponseCodeEnum["TRANSACTION_TIMED_OUT"] = "020";
    AltoResponseCodeEnum["IDENTICAL_TRANSACTION_HAS_BEEN_SUCCESS"] = "030";
    AltoResponseCodeEnum["IDENTICAL_TRANSACTION_STILL_ON_PROCESS"] = "031";
    AltoResponseCodeEnum["IDENTICAL_TRANSACTION_HAS_BEEN_FAILED"] = "032";
    AltoResponseCodeEnum["INVALID_ACCOUNT_NUMBER_TO_PAYMENT"] = "040";
    AltoResponseCodeEnum["INVALID_ACCOUNT_NUMBER_TO_HOLD"] = "041";
    AltoResponseCodeEnum["INVALID_AMOUNT_TO_PAYMENT"] = "050";
    AltoResponseCodeEnum["INVALID_AMOUNT_TO_HOLD"] = "051";
    AltoResponseCodeEnum["INTERNAL_SERVER_ERROR"] = "060";
    AltoResponseCodeEnum["BANK_NOT_RESPONDING"] = "061";
    AltoResponseCodeEnum["NOT_IN_OPERATING_HOUR"] = "062";
    AltoResponseCodeEnum["MESSAGE_FORMAT_ERROR"] = "070";
    AltoResponseCodeEnum["INVALID_TRANSACTION"] = "071";
    AltoResponseCodeEnum["BAD_METHOD"] = "072";
    AltoResponseCodeEnum["INVALID_HOST"] = "073";
    AltoResponseCodeEnum["UNKNOWN_CLIENT"] = "080";
    AltoResponseCodeEnum["AUTHENTICATION_ERROR"] = "081";
    AltoResponseCodeEnum["INVALID_SIGNATURE"] = "082";
    AltoResponseCodeEnum["INVALID_TOKEN"] = "083";
    AltoResponseCodeEnum["NO_CONTENT"] = "084";
    AltoResponseCodeEnum["ACCOUNT_IS_SUSPENDED"] = "085";
    AltoResponseCodeEnum["BANK_DOES_NOT_SUPPORT_THE_TRANSACTION"] = "086";
    AltoResponseCodeEnum["BANK_NOT_SUPPORTED"] = "087";
    AltoResponseCodeEnum["TRANSACTION_NOT_ALLOWED"] = "088";
    AltoResponseCodeEnum["BANK_ACCOUNT_NOT_FOUND"] = "091";
    AltoResponseCodeEnum["TRANSACTION_NOT_FOUND"] = "092";
    AltoResponseCodeEnum["BANK_NOT_FOUND"] = "093";
    AltoResponseCodeEnum["INSUFFICIENT_FUND"] = "094";
    AltoResponseCodeEnum["REQUESTED_DATA_NOT_FOUND"] = "095";
    AltoResponseCodeEnum["LINK_DOWN"] = "100";
    AltoResponseCodeEnum["SWITCHING_HOST_UNREACHABLE"] = "101";
    AltoResponseCodeEnum["BANK_HOST_UNREACHABLE"] = "102";
    AltoResponseCodeEnum["INVALID_MERCHANT"] = "103";
    AltoResponseCodeEnum["DO_NOT_HONOR"] = "104";
    AltoResponseCodeEnum["FOR"] = "105";
    AltoResponseCodeEnum["REJECTED"] = "A00";
})(AltoResponseCodeEnum = exports.AltoResponseCodeEnum || (exports.AltoResponseCodeEnum = {}));
var QrisTransactionStatusEnum;
(function (QrisTransactionStatusEnum) {
    QrisTransactionStatusEnum["SUCCESS"] = "Success";
    // TODO (Altair): remove after SIT, duplicate with SUSPECT
    QrisTransactionStatusEnum["SUSPEND"] = "Suspend";
    QrisTransactionStatusEnum["FAILED"] = "Failed";
    QrisTransactionStatusEnum["SUSPECT"] = "Suspect";
    QrisTransactionStatusEnum["SHOULD_CHECK_STATUS"] = "SHOULD_CHECK_STATUS";
})(QrisTransactionStatusEnum = exports.QrisTransactionStatusEnum || (exports.QrisTransactionStatusEnum = {}));
//# sourceMappingURL=alto.enum.js.map