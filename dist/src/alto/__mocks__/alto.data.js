"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const faker_1 = __importDefault(require("faker"));
const transaction_data_1 = require("../../transaction/__mocks__/transaction.data");
const module_common_1 = require("@dk/module-common");
const alto_enum_1 = require("../alto.enum");
const alto_utils_1 = __importDefault(require("../alto.utils"));
const transaction_enum_1 = require("../../transaction/transaction.enum");
const newTransactionModel = transaction_data_1.getTransactionModelFullData();
exports.validTransactionModel = Object.assign({ id: faker_1.default.random.uuid() }, newTransactionModel);
exports.inValidTransactionModel = Object.assign(Object.assign({}, newTransactionModel), { id: faker_1.default.random.uuid(), additionalInformation1: undefined, beneficiaryAccountNo: undefined });
exports.getQrisPaymentRequestAttributesData = () => ({
    /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
    date_time: '2022-02-22T08:23:08.168Z',
    customer_reference_number: 'PAY20220223001',
    authorization_id: alto_utils_1.default.generateAuthorizationId(),
    currency_code: 'IDR',
    amount: 100500,
    fee: 500,
    issuer_nns: '93600919',
    national_mid: 'IN1019000000024',
    additional_data: 'ABCDEFGHIJ',
    terminal_label: 'K19',
    merchant: {
        pan: '936011010099879098',
        id: '999540008',
        criteria: 'UME',
        name: 'Somay Mangga Dua',
        city: 'Jakarta',
        mcc: '6600',
        postal_code: '10110',
        country_code: 'ID'
    },
    customer: {
        pan: '9360002319993788396',
        name: 'Sebastian',
        account_type: alto_enum_1.AltoAccountTypeEnum.SAVING
    }
});
exports.getQrisPaymentRequestAttributes = () => ({
    command: 'qr-payment-credit',
    data: exports.getQrisPaymentRequestAttributesData()
});
exports.transactionModel = Object.assign(Object.assign({}, transaction_data_1.getTransactionDetail()), { beneficiaryBankCodeChannel: module_common_1.BankChannelEnum.QRIS, beneficiaryAccountNo: '2121212', beneficiaryAccountName: 'beneficiary_name', beneficiaryBankCode: 'beneficiary_bank', beneficiaryIrisCode: 'beneficiary_bank', note: 'notes', transactionAmount: 12500 });
exports.mockQrisPaymentInquiryResponse = () => ({
    command: 'qr-payment-credit',
    response_code: '001',
    response_text: 'Success',
    data: {
        customer_reference_number: 'PAY20220223001',
        forwarding_customer_reference_number: '077879547900',
        invoice_no: '007',
        currency_code: 'IDR',
        amount: 100500,
        fee: 500
    }
});
exports.mockSuccessQrisPaymentResponse = () => ({
    customerReferenceNumber: 'PAY20220223001',
    forwardingCustomerReferenceNumber: '077879547900',
    invoiceNo: '007',
    currencyCode: 'IDR',
    amount: 100500,
    fee: 500,
    transactionStatus: alto_enum_1.QrisTransactionStatusEnum.SUCCESS
});
exports.mockErrorQrisPaymentResponse = () => ({
    customerReferenceNumber: 'PAY20220223001',
    forwardingCustomerReferenceNumber: '077879547900',
    invoiceNo: '007',
    currencyCode: 'IDR',
    amount: 100500,
    fee: 500,
    transactionStatus: alto_enum_1.QrisTransactionStatusEnum.SHOULD_CHECK_STATUS
});
exports.qrisCreatePaymentResponseExpectation = () => ({
    customerReferenceNumber: 'PAY20220223001',
    forwardingCustomerReferenceNumber: '077879547900',
    invoiceNo: '007',
    currencyCode: 'IDR',
    amount: 100500,
    fee: 500,
    transactionStatus: alto_enum_1.QrisTransactionStatusEnum.SUCCESS
});
exports.getTransactionAdditionalPayloadQrisPayment = () => ({
    /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
    dateTime: '2022-02-22T08:23:08.168Z',
    currencyCode: 'IDR',
    amount: 100500,
    fee: 500,
    issuerNns: '93600919',
    nationalMid: 'IN1019000000024',
    additionalData: 'ABCDEFGHIJ',
    terminalLabel: 'K19',
    merchant: {
        pan: '936011010099879098',
        id: '999540008',
        criteria: 'UME',
        name: 'Somay Mangga Dua',
        city: 'Jakarta',
        mcc: '6600',
        postalCode: '10110',
        countryCode: 'ID'
    },
    customer: {
        pan: '9360002319993788396',
        name: 'Sebastian',
        accountType: alto_enum_1.AltoAccountTypeEnum.SAVING
    }
});
exports.getNotValidTransactionAdditionalPayloadQrisPayment = () => ({
    /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
    dateTime: '2022-02-22T08:23:08.168Z',
    customerReferenceNumber: undefined,
    authorizationId: undefined,
    currencyCode: undefined,
    amount: undefined,
    fee: undefined,
    issuerNns: undefined,
    acquirerNns: undefined,
    nationalMid: undefined,
    additionalData: undefined,
    terminalLabel: undefined,
    forwardingCustomerReferenceNumber: undefined,
    merchant: {
        pan: undefined,
        id: undefined,
        criteria: undefined,
        name: undefined,
        city: undefined,
        mcc: undefined,
        postalCode: undefined,
        countryCode: undefined
    },
    customer: {
        pan: undefined,
        name: undefined,
        accountType: undefined
    }
});
exports.mockQrisCheckStatusDataRequest = (retryAttempt = 1) => {
    return {
        customerId: '0435808014',
        paymentInstructionId: '6255003eaf1a288846cc5b7a',
        retryAttempt: retryAttempt
    };
};
exports.mockAltoCheckStatusResponse = () => ({
    command: 'qr-check-status',
    response_code: '001',
    response_text: 'Success',
    data: {
        transaction_response_code: '001',
        transaction_status: 'Success',
        customer_reference_number: '0013284239ZFX21',
        forwarding_customer_reference_number: '42234093892',
        invoice_no: '73283923681238390213'
    }
});
exports.mockAltoCheckStatusNotFoundResponse = () => ({
    command: 'qr-check-status',
    response_code: '096',
    response_text: 'Requested data not found',
    data: {
        customer_reference_number: '0013284239ZFX21'
    }
});
exports.mockAltoCheckStatusRejected = () => ({
    command: 'qr-check-status',
    response_code: 'A00',
    response_text: 'Rejected',
    data: {
        customer_reference_number: '0013284239ZFX21'
    }
});
exports.mockAltoCheckStatusSuspend = () => ({
    command: 'qr-check-status',
    response_code: '001',
    response_text: 'Success',
    data: {
        transaction_response_code: '002',
        transaction_status: 'Suspect',
        customer_reference_number: 'ad5ade04-db27-42bc-9a6f-85096b924ce3',
        forwarding_customer_reference_number: '914932025574',
        invoice_no: '00000000000000000000'
    }
});
exports.mockQrisCheckStatusResponse = () => {
    return {
        transactionResponseCode: alto_enum_1.AltoResponseCodeEnum.SUCCESS,
        transactionStatus: alto_enum_1.QrisTransactionStatusEnum.SUCCESS,
        customerReferenceNumber: '0013284239ZFX21',
        forwardingCustomerReferenceNumber: '42234093892',
        invoiceNo: '73283923681238390213'
    };
};
exports.mockQrisCheckStatusSuspectResponse = () => {
    return {
        transactionResponseCode: alto_enum_1.AltoResponseCodeEnum.SUSPECT,
        transactionStatus: alto_enum_1.QrisTransactionStatusEnum.SUSPECT,
        customerReferenceNumber: '0013284239ZFX21',
        forwardingCustomerReferenceNumber: '42234093892',
        invoiceNo: '73283923681238390213'
    };
};
exports.mockCheckStatusResponseRejected = () => {
    return {
        customerReferenceNumber: 'QRIS625d179c15622f5ca75931ac',
        forwardingCustomerReferenceNumber: '976418600378',
        invoiceNo: '00000000990000000099',
        transactionResponseCode: alto_enum_1.AltoResponseCodeEnum.REJECTED,
        transactionStatus: alto_enum_1.QrisTransactionStatusEnum.FAILED
    };
};
exports.mockCheckStatusResponseApproved = () => {
    return {
        customerReferenceNumber: 'QRIS625d179c15622f5ca75931ac',
        forwardingCustomerReferenceNumber: '976418600378',
        invoiceNo: '00000000990000000099',
        transactionResponseCode: alto_enum_1.AltoResponseCodeEnum.SUCCESS,
        transactionStatus: alto_enum_1.QrisTransactionStatusEnum.SUCCESS
    };
};
exports.mockCheckStatusResponseUnknown = () => {
    return {
        customerReferenceNumber: 'QRIS625d179c15622f5ca75931ac',
        forwardingCustomerReferenceNumber: '976418600378',
        invoiceNo: '00000000990000000099',
        transactionResponseCode: alto_enum_1.AltoResponseCodeEnum.UNKNOWN,
        transactionStatus: alto_enum_1.QrisTransactionStatusEnum.FAILED
    };
};
exports.mockCheckStatusResponseSuspended = () => {
    return {
        customerReferenceNumber: 'QRIS625d179c15622f5ca75931ac',
        forwardingCustomerReferenceNumber: '976418600378',
        invoiceNo: '00000000990000000099',
        transactionResponseCode: alto_enum_1.AltoResponseCodeEnum.SUSPECT,
        transactionStatus: alto_enum_1.QrisTransactionStatusEnum.SUSPEND
    };
};
exports.mockRefundResponse = () => {
    return {
        command: alto_enum_1.AltoCommandEnum.QRIS_REFUND,
        response_code: alto_enum_1.AltoRefundResponseCodeEnum.SUCCESS,
        response_text: alto_enum_1.AltoRefundResponseTextEnum.SUCCESS,
        data: {
            authorization_id: '1234567',
            customer_reference_number: 'QRIS625d179c15622f5ca75931ac',
            invoice_no_refund: 'generatedNumber',
            reference_number: '6418600378'
        }
    };
};
exports.getQrisCheckPaymentAttributesData = () => ({
    datetime: '2022-02-22T08:23:08.168Z',
    customerReferenceNumber: 'PAY20220223001'
});
exports.getQrisCheckStatusRequestAttributes = () => ({
    command: 'qr-check-status',
    data: exports.getQrisCheckPaymentAttributesData()
});
exports.getQrisPayoutsRecommendedService = () => {
    const recommendationService = {
        paymentServiceCode: 'QRIS_PAYOUTS',
        beneficiaryBankCodeType: transaction_enum_1.BankCodeTypes.RTOL,
        transactionAuthenticationChecking: false,
        debitTransactionCode: [
            {
                transactionCode: 'QRD001',
                transactionCodeInfo: {
                    code: 'QRD001',
                    channel: 'QRIS',
                    defaultCategoryCode: 'QRIS',
                    minAmountPerTx: 1,
                    maxAmountPerTx: 9999999,
                    limitGroupCode: 'QRIS'
                }
            }
        ],
        blocking: false,
        requireThirdPartyOutgoingId: true
    };
    return recommendationService;
};
exports.getQrisWithdrawMambuResponse = () => ({
    id: 'mbu_001',
    encodedKey: '123',
    type: 'type',
    amount: 10000
});
//# sourceMappingURL=alto.data.js.map