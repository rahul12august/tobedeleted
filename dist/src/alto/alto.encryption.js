"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importDefault(require("../logger"));
const module_common_1 = require("@dk/module-common");
const apiKey = process.env.ALTO_API_KEY || '';
const validationKey = process.env.ALTO_VALIDATION_KEY || '';
exports.generateSignature = (httpMethod, relativeURL, stringifiedPayload, timestamp) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`generateSignature`);
    const payload = stringifiedPayload;
    const hashedPayload = module_common_1.qrisEncryption.bodyEncryption(payload).toLowerCase();
    logger_1.default.debug(`generateSignature: hashedPayload: ${hashedPayload}`);
    const stringToSign = `${httpMethod.toUpperCase()}:${relativeURL}:${apiKey}:${hashedPayload}:${timestamp}`;
    const signature = module_common_1.qrisEncryption.signatureEncryption(stringToSign, validationKey);
    logger_1.default.debug(`generateSignature: ${signature}`);
    return signature;
});
//# sourceMappingURL=alto.encryption.js.map