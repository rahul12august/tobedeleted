"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const v4_1 = __importDefault(require("uuid/v4"));
const alto_repository_1 = __importDefault(require("./alto.repository"));
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const transaction_repository_1 = __importDefault(require("../transaction/transaction.repository"));
const generalRefund_service_1 = __importDefault(require("../transaction/refund/generalRefund.service"));
const module_common_1 = require("@dk/module-common");
const alto_enum_1 = require("./alto.enum");
const alto_utils_1 = __importDefault(require("./alto.utils"));
const transaction_enum_1 = require("../transaction/transaction.enum");
const transaction_helper_1 = __importDefault(require("../transaction/transaction.helper"));
const transactionHistory_util_1 = require("../transactionHistory/transactionHistory.util");
const transactionHistory_repository_1 = __importDefault(require("../transactionHistory/transactionHistory.repository"));
const transaction_producer_1 = __importDefault(require("../transaction/transaction.producer"));
const deposit_repository_1 = __importDefault(require("../coreBanking/deposit.repository"));
const deposit_enum_1 = require("../coreBanking/deposit.enum");
const createAltoRefundResponse = (payloadData, responseCode) => {
    const responseDataIdentifier = {
        authorization_id: module_common_1.qrisUtil.generateAuthorizationId(),
        invoice_no_refund: payloadData.invoice_no,
        customer_reference_number: payloadData.customer_reference_number,
        reference_number: payloadData.reference_number.toString()
    };
    let responseText;
    switch (responseCode) {
        case alto_enum_1.AltoRefundResponseCodeEnum.SUCCESS:
            responseText = alto_enum_1.AltoRefundResponseTextEnum.SUCCESS;
            break;
        case alto_enum_1.AltoRefundResponseCodeEnum.FAILED:
            responseDataIdentifier.authorization_id =
                alto_enum_1.AltoDefaultAuthorizationId.FAILED;
            responseText = alto_enum_1.AltoRefundResponseTextEnum.FAILED;
            break;
        case alto_enum_1.AltoRefundResponseCodeEnum.INVALID_AMOUNT:
            responseDataIdentifier.authorization_id =
                alto_enum_1.AltoDefaultAuthorizationId.FAILED;
            responseText = alto_enum_1.AltoRefundResponseTextEnum.INVALID_AMOUNT;
            break;
    }
    return {
        command: alto_enum_1.AltoCommandEnum.QRIS_REFUND,
        response_code: responseCode,
        response_text: responseText,
        data: responseDataIdentifier
    };
};
const markQrisTxnDeclineManually = (txn) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`markQrisTxnDeclineManually: VOID_QRIS for reference_number: ${txn.thirdPartyOutgoingId}`);
    const createRefundParam = {
        transactionId: txn.id,
        originalTransaction: Object.assign(Object.assign({}, txn), { status: transaction_enum_1.TransactionStatus.SUCCEED }),
        amountRefund: txn.transactionAmount,
        refundFees: false,
        refundIdentifier: v4_1.default(),
        refundAddInfo1: txn.thirdPartyOutgoingId
    };
    try {
        yield generalRefund_service_1.default.createRefundTransaction(createRefundParam);
    }
    catch (err) {
        logger_1.default.error(`markQrisTxnDeclineManually: failed to execute refund for: ${txn.id}.`);
        throw new AppError_1.TransferAppError(`Failed to execute refund for: ${txn.id}`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.QRIS_REFUND_TRANSACTION_FAILED);
    }
    const updatedTransaction = yield transaction_repository_1.default.update(txn.id, Object.assign(Object.assign({}, txn), { status: transaction_enum_1.TransactionStatus.DECLINED, journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.TRANSFER_FAILURE, txn) }));
    if (!updatedTransaction) {
        logger_1.default.error(`markQrisTxnDeclineManually: failed to update transaction status for transaction id: ${txn.id}.`);
        throw new AppError_1.TransferAppError(`Failed to update transaction status for transaction id: ${txn.id}.`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION_STATUS);
    }
});
const refundTransaction = (payloadData) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`refundTransaction: VOID_QRIS for reference_number: ${payloadData.reference_number}`);
    const originalTransaction = yield transaction_repository_1.default.getByThirdPartyOutgoingId(payloadData.reference_number.toString());
    if (!originalTransaction) {
        logger_1.default.error(`refundTransaction : Original transaction not found for reference_number : ${payloadData.reference_number}`);
        const failedRefundResponse = createAltoRefundResponse(payloadData, alto_enum_1.AltoRefundResponseCodeEnum.FAILED);
        return failedRefundResponse;
    }
    const existingQrisTransaction = yield transaction_repository_1.default.getByQRISRefundIdentifier(payloadData.customer_reference_number);
    if (existingQrisTransaction) {
        logger_1.default.error(`refundTransaction : duplicate QRIS refund request for CRN : ${payloadData.customer_reference_number}`);
        return createAltoRefundResponse(payloadData, alto_enum_1.AltoRefundResponseCodeEnum.FAILED);
    }
    if (originalTransaction.transactionAmount < payloadData.amount_refund) {
        return createAltoRefundResponse(payloadData, alto_enum_1.AltoRefundResponseCodeEnum.INVALID_AMOUNT);
    }
    if (originalTransaction.refund) {
        if (originalTransaction.refund.remainingAmount !== undefined &&
            payloadData.amount_refund > originalTransaction.refund.remainingAmount) {
            return createAltoRefundResponse(payloadData, alto_enum_1.AltoRefundResponseCodeEnum.INVALID_AMOUNT);
        }
    }
    const createRefundParam = {
        transactionId: originalTransaction.id,
        originalTransaction: originalTransaction,
        amountRefund: payloadData.amount_refund,
        refundFees: false,
        refundIdentifier: payloadData.customer_reference_number,
        refundAddInfo1: payloadData.reference_number
    };
    try {
        yield generalRefund_service_1.default.createRefundTransaction(createRefundParam);
    }
    catch (err) {
        return createAltoRefundResponse(payloadData, alto_enum_1.AltoRefundResponseCodeEnum.FAILED);
    }
    return createAltoRefundResponse(payloadData, alto_enum_1.AltoRefundResponseCodeEnum.SUCCESS);
});
const updateTransactionHistory = (coreBankingTransactionId, qrisTransactionStatus, forwardingCustomerReferenceNumber = undefined) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield transactionHistory_util_1.updateTransactionHistoryReferenceIdWithRetry(transactionHistory_repository_1.default.updateTransactionHistory, coreBankingTransactionId, alto_utils_1.default.getTransactionHistoryStatus(qrisTransactionStatus), forwardingCustomerReferenceNumber);
    }
    catch (error) {
        logger_1.default.warn(`Failed to update transaction history for ${coreBankingTransactionId}`);
    }
});
const patchQrisCustomerReferenceNumberInMambu = (coreBankingTransactionId, forwardingCustomerReferenceNumber) => __awaiter(void 0, void 0, void 0, function* () {
    const patchPayload = {
        op: deposit_enum_1.PatchOperation.REPLACE,
        path: '/_Custom_Transaction_Details/Third_Party_Outgoing_Id',
        value: alto_utils_1.default.getAltoReferenceNumber(forwardingCustomerReferenceNumber)
    };
    try {
        yield deposit_repository_1.default.patchTransactionDetails(coreBankingTransactionId, [patchPayload]);
    }
    catch (error) {
        logger_1.default.warn(`Failed to update Mambu transaction details for ${coreBankingTransactionId}`);
    }
});
/**
 * Update transaction details in Mambu and MS-TH to include Forwarding Customer Reference Number (FCRN).
 * FCRN will be used as reference number in MS-TH and ThirdPartyOutgoingId in Mambu.
 *
 * Note: The update will be processed asynchronously, as the result of it won't affect the transaction.
 */
const updateTransactionDetailsInExternalService = (transactionModel, qrisTransactionStatus, forwardingCustomerReferenceNumber = undefined) => {
    if (transactionModel.coreBankingTransactionIds &&
        transactionModel.coreBankingTransactionIds[0]) {
        const coreBankingTransactionId = transactionModel.coreBankingTransactionIds[0];
        logger_1.default.info(`Updating transaction details for ${coreBankingTransactionId}`);
        updateTransactionHistory(coreBankingTransactionId, qrisTransactionStatus, forwardingCustomerReferenceNumber);
        if (alto_utils_1.default.updateMambuEnabled() && forwardingCustomerReferenceNumber) {
            patchQrisCustomerReferenceNumberInMambu(coreBankingTransactionId, forwardingCustomerReferenceNumber);
        }
    }
    else {
        logger_1.default.warn(`Cannot update transaction details. No coreBankingTransactionId`);
    }
};
const publishQrisTransactionNotification = (transaction, qrisTransactionStatus) => {
    const notificationCode = alto_utils_1.default.getQrisNotificationCode(qrisTransactionStatus);
    transaction_producer_1.default.sendQrisTransactionNotification(transaction, notificationCode);
};
const updateTransactionStatus = (transaction, isSuccessQrisPayment, shouldAutoRefund) => {
    if (isSuccessQrisPayment) {
        transaction.status = transaction_enum_1.TransactionStatus.SUCCEED;
        transaction.journey = transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.TRANSFER_SUCCESS, transaction);
    }
    else if (shouldAutoRefund) {
        // SUCCESS transaction status is required for refund process
        transaction.status = transaction_enum_1.TransactionStatus.SUCCEED;
        transaction.journey = transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.TRANSFER_FAILURE, transaction);
    }
    else {
        // any FAILED payment that don't need refund automatically
        transaction.status = transaction_enum_1.TransactionStatus.DECLINED;
        transaction.journey = transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.TRANSFER_FAILURE, transaction);
        transaction.failureReason = {
            type: errors_1.ERROR_CODE.ALTO_DECLINED_NO_REFUND_REQUIRED,
            actor: transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER,
            detail: `Payment that don't need refund automatically.`
        };
    }
    return transaction;
};
const autoRefundQrisTransaction = (checkStatusResponse, transaction) => __awaiter(void 0, void 0, void 0, function* () {
    const refundRequestData = alto_utils_1.default.buildRefundRequestData(checkStatusResponse, transaction);
    try {
        return yield refundTransaction(refundRequestData);
    }
    catch (err) {
        logger_1.default.error(`Cannot refund transaction ${transaction.id}. ${JSON.stringify(err)}`);
        return {
            command: alto_enum_1.AltoCommandEnum.QRIS_REFUND,
            response_code: alto_enum_1.AltoRefundResponseCodeEnum.FAILED,
            response_text: alto_enum_1.AltoRefundResponseTextEnum.FAILED
        };
    }
});
const updateTransaction = (checkStatusResponse, transaction) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Updating QRIS transaction for status response.
    Customer Reference Number: ${checkStatusResponse.customerReferenceNumber}
    Forwarding Customer Reference Number: ${checkStatusResponse.forwardingCustomerReferenceNumber}
    Invoice No: ${checkStatusResponse.invoiceNo}
    Transaction Status: ${checkStatusResponse.transactionStatus}`);
    const isSuccessQrisPayment = alto_utils_1.default.isSuccessQrisPayment(checkStatusResponse.transactionStatus);
    const shouldAutoRefund = alto_utils_1.default.shouldAutoRefundQrisPayment(checkStatusResponse);
    if (isSuccessQrisPayment) {
        alto_utils_1.default.updateTransactionDataOnQrisPaymentSuccess(transaction, checkStatusResponse);
    }
    transaction = updateTransactionStatus(transaction, isSuccessQrisPayment, shouldAutoRefund);
    let updatedTransaction = yield transaction_repository_1.default.update(transaction.id, transaction);
    if (shouldAutoRefund) {
        const refundResponse = yield autoRefundQrisTransaction(checkStatusResponse, updatedTransaction);
        if (refundResponse.response_code === alto_enum_1.AltoRefundResponseCodeEnum.SUCCESS) {
            updatedTransaction = yield transaction_repository_1.default.update(transaction.id, {
                status: transaction_enum_1.TransactionStatus.DECLINED,
                journey: transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.REFUND_CREDITED, transaction)
            });
        }
    }
    updateTransactionDetailsInExternalService(updatedTransaction, checkStatusResponse.transactionStatus, checkStatusResponse.forwardingCustomerReferenceNumber);
    publishQrisTransactionNotification(updatedTransaction, checkStatusResponse.transactionStatus);
    logger_1.default.info(`QRIS transaction is updated.
    Transaction ID: ${transaction.id}
    Reference Number: ${transaction.externalId}
    Third Party Outgoing Id: ${transaction.thirdPartyOutgoingId}
    Transaction Status: ${transaction.status}`);
});
const executeCheckStatus = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Check status of QRIS transaction.
    Transaction ID: ${transaction.id}
    External ID: ${transaction.externalId}`);
    try {
        const checkStatusResponse = yield alto_repository_1.default.checkPaymentStatus(transaction.externalId);
        if (alto_utils_1.default.shouldUpdateTransaction(checkStatusResponse)) {
            yield updateTransaction(checkStatusResponse, transaction);
        }
        else {
            yield transaction_repository_1.default.update(transaction.id, {
                journey: transaction.journey
            });
        }
        logger_1.default.info(`Check status result:
      Customer Reference Number: ${checkStatusResponse.customerReferenceNumber}
      Forwarding Customer Reference Number: ${checkStatusResponse.forwardingCustomerReferenceNumber}
      Invoice Number: ${checkStatusResponse.invoiceNo}
      Payment Status: ${checkStatusResponse.transactionStatus}
      Payment Response Code: ${checkStatusResponse.transactionResponseCode}`);
        return checkStatusResponse;
    }
    catch (error) {
        logger_1.default.error(`Error while checking status: ${error.errorCode | error.errorMessage}`);
        throw new AppError_1.TransferAppError(`Error while checking status: ${error.errorCode | error.errorMessage}`, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.QRIS_CHECK_STATUS_FAILED);
    }
});
const retrieveTransactionToCheck = (customerId, paymentInstructionId) => __awaiter(void 0, void 0, void 0, function* () {
    const query = {
        ownerCustomerId: customerId,
        paymentInstructionId: paymentInstructionId
    };
    const transaction = yield transaction_repository_1.default.findOneByQuery(query);
    if (transaction == null) {
        logger_1.default.error(`checkStatus: Alto failed to get externalId`);
        throw new AppError_1.TransferAppError('Alto failed to get externalId.', transaction_enum_1.TransferFailureReasonActor.ALTO, errors_1.ERROR_CODE.QRIS_PAYMENT_CHECK_STATUS_FAILED);
    }
    return transaction;
});
const checkStatus = (customerId, paymentInstructionId) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`Checking status for transaction with paymentInstructionId: ${paymentInstructionId}, customerId: ${customerId}`);
    const transaction = yield retrieveTransactionToCheck(customerId, paymentInstructionId);
    if (alto_utils_1.default.isTransactionSettled(transaction.status)) {
        logger_1.default.info(`Cannot update transaction with:
      Id: ${transaction.id}
      Status: ${transaction.status}`);
        return {
            customerReferenceNumber: transaction.externalId,
            forwardingCustomerReferenceNumber: transaction.additionalInformation4 || '',
            invoiceNo: transaction.coreBankingTransactionIds
                ? transaction.coreBankingTransactionIds[1]
                : '',
            transactionStatus: alto_utils_1.default.getQrisTransactionStatus(transaction.status)
        };
    }
    transaction.journey = transaction_helper_1.default.getStatusesToBeUpdated(transaction_enum_1.TransferJourneyStatusEnum.CHECK_STATUS, transaction);
    return yield executeCheckStatus(transaction);
});
const qrisService = logger_1.wrapLogs({
    checkStatus,
    executeCheckStatus,
    refundTransaction,
    markQrisTxnDeclineManually,
    updateTransactionDetailsInExternalService,
    updateTransaction // used for testing
});
exports.default = qrisService;
//# sourceMappingURL=alto.service.js.map