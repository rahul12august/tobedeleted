"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable @typescript-eslint/camelcase */
const joi_1 = __importDefault(require("@hapi/joi"));
exports.qrisCheckStatusRequestValidator = joi_1.default.object({
    customerId: joi_1.default.string()
        .required()
        .description('Customer Id'),
    paymentInstructionId: joi_1.default.string()
        .required()
        .description('Payment instruction Id'),
    retryAttempt: joi_1.default.number()
        .optional()
        .description('Retry attempt counter')
}).label('qrisCheckStatusRequestValidator');
exports.qrisCheckStatusResponseValidator = joi_1.default.object({
    transactionResponseCode: joi_1.default.string()
        .optional()
        .description('Response code of original transaction'),
    transactionStatus: joi_1.default.string()
        .optional()
        .description('Status of original transaction'),
    customerReferenceNumber: joi_1.default.string()
        .required()
        .description('Customer reference number from original transaction'),
    forwardingCustomerReferenceNumber: joi_1.default.string()
        .optional()
        .allow('')
        .description('Forwarding customer reference number of original transactions'),
    invoiceNo: joi_1.default.string()
        .optional()
        .allow('')
        .description('Invoice number of original transactions'),
    altoResponseCode: joi_1.default.string()
        .optional()
        .description('Response code from Alto')
}).label('qrisCheckStatusRequestValidator');
exports.altoRefundRequestValidator = joi_1.default.object({
    command: joi_1.default.string()
        .required()
        .valid('qr-refund')
        .description('ALTO webhook command'),
    data: joi_1.default.object({
        date_time: joi_1.default.string().required(),
        customer_reference_number: joi_1.default.string()
            .min(6)
            .max(100)
            .required(),
        invoice_no: joi_1.default.string().required(),
        currency_code: joi_1.default.string()
            .valid('IDR')
            .required(),
        amount_refund: joi_1.default.number()
            .min(1)
            .required(),
        reference_number: joi_1.default.string().required()
    }).required()
}).label('altoRefundRequestValidator');
exports.altoRefundResponseValidator = joi_1.default.object({
    command: joi_1.default.string()
        .required()
        .valid('qr-refund')
        .description('ALTO webhook command'),
    response_code: joi_1.default.string().required(),
    response_text: joi_1.default.string().required(),
    data: joi_1.default.object({
        authorization_id: joi_1.default.string().required(),
        customer_reference_number: joi_1.default.string()
            .min(6)
            .max(100)
            .required(),
        invoice_no_refund: joi_1.default.string().required(),
        reference_number: joi_1.default.string().required()
    })
}).label('altoRefundResponseValidator');
//# sourceMappingURL=alto.validator.js.map