"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const context_1 = require("../context");
const logger_1 = __importDefault(require("../logger"));
const module_httpclient_1 = require("@dk/module-httpclient");
const alto_constant_1 = require("../alto/alto.constant");
const { host } = config_1.config.get('qrisAlto');
const bearerValue = `Basic ${Buffer.from(process.env.ALTO_USERNAME + ':' + process.env.ALTO_PASSWORD).toString('base64')}`;
const httpClient = module_httpclient_1.createHttpClient({
    baseURL: host,
    timeout: alto_constant_1.REQUEST_TIMEOUT,
    context: context_1.context,
    logger: logger_1.default,
    retryConfig: alto_constant_1.retryConfig
});
exports.tokenHTTPClient = module_httpclient_1.createHttpClient({
    baseURL: host,
    headers: {
        Authorization: bearerValue
    },
    timeout: alto_constant_1.REQUEST_TIMEOUT,
    context: context_1.context,
    logger: logger_1.default,
    retryConfig: alto_constant_1.retryConfig
});
exports.default = httpClient;
//# sourceMappingURL=httpClient.js.map