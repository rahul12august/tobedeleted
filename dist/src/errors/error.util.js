"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const errors_1 = require("../common/errors");
exports.getErrorDetails = (errorCode, key, message) => [
    {
        message: message || errors_1.ErrorList[errorCode].message,
        code: errorCode,
        key: key
    }
];
//# sourceMappingURL=error.util.js.map