"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AppError_1 = require("@dk/module-common/dist/error/AppError");
var AppError_2 = require("@dk/module-common/dist/error/AppError");
exports.AppError = AppError_2.AppError;
class TransferAppError extends AppError_1.AppError {
    constructor(detail, actor, errorCode, errors, transaction) {
        super(errorCode, errors);
        this.detail = detail;
        this.actor = actor;
        this.transaction = transaction;
    }
}
exports.TransferAppError = TransferAppError;
class RetryableTransferAppError extends TransferAppError {
    constructor(detail, actor, errorCode, retry, errors, transaction) {
        super(detail, actor, errorCode, errors, transaction);
        this.retry = retry;
    }
}
exports.RetryableTransferAppError = RetryableTransferAppError;
exports.isRetryableError = (status) => status >= 500;
//# sourceMappingURL=AppError.js.map