"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
exports.ENTITLEMENT_API_ENDPOINT = (customerId) => {
    return `/private/${customerId}/counter`;
};
exports.ENTITLEMENT_EXCLUDED_PAYMENT_SERVICE_TYPES = [
    module_common_1.PaymentServiceTypeEnum.OFFER,
    module_common_1.PaymentServiceTypeEnum.PAYROLL,
    module_common_1.PaymentServiceTypeEnum.CASHBACK,
    module_common_1.PaymentServiceTypeEnum.BONUS_INTEREST,
    module_common_1.PaymentServiceTypeEnum.THIRD_PARTY_BALANCE_INQUIRY,
    module_common_1.PaymentServiceTypeEnum.INTERNATIONAL_BALANCE_INQUIRY,
    module_common_1.PaymentServiceTypeEnum.THIRD_PARTY_JAGO_TO_JAGO_CREDIT
];
//# sourceMappingURL=entitlement.constant.js.map