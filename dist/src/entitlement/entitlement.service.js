"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const entitlement_helper_1 = __importDefault(require("./entitlement.helper"));
const v4_1 = __importDefault(require("uuid/v4"));
const entitlement_repository_1 = __importDefault(require("./entitlement.repository"));
const contextHandler_1 = require("../common/contextHandler");
const logger_1 = __importStar(require("../logger"));
const transaction_repository_1 = __importDefault(require("../transaction/transaction.repository"));
const transaction_enum_1 = require("../transaction/transaction.enum");
const AppError_1 = require("../errors/AppError");
const entitlement_enum_1 = require("./entitlement.enum");
const lodash_1 = __importDefault(require("lodash"));
const errors_1 = require("../common/errors");
const entitlement_constant_1 = require("./entitlement.constant");
const retrieveUniqueEntitlementCodesBasedOnTransactionCodes = (transactionCodes) => {
    //step 1 - create a set [SET entitlementCodes], that is to ensure unique limit group code/entitlement code/counter code
    //const uniqueEntitlementCodes = new Set();
    const uniqueEntitlementCodeMap = new Map();
    //step 2 -- loop
    transactionCodes.forEach(tc => {
        //step 3 - individual transaction code: check for the following
        // validation 1: whether there is awardgroupcounter/entitlement code/counter code
        const { awardGroupCounter, entitlementCode, counterCode, limitGroupCode } = tc.transactionCodeInfo;
        if (lodash_1.default.isEmpty(awardGroupCounter) ||
            lodash_1.default.isUndefined(awardGroupCounter) ||
            lodash_1.default.isEmpty(entitlementCode) ||
            lodash_1.default.isUndefined(entitlementCode) ||
            lodash_1.default.isEmpty(counterCode) ||
            lodash_1.default.isUndefined(counterCode) ||
            lodash_1.default.isEmpty(limitGroupCode) ||
            lodash_1.default.isUndefined(limitGroupCode)) {
            logger_1.default.info(`retrieveUniqueEntitlementCodesBasedOnTransactionCodes: missing awardGroupCounter: ${awardGroupCounter}, 
        entitlementCode: ${entitlementCode}, counterCode: ${counterCode}, limitGroupCode: ${limitGroupCode}`);
            return;
        }
        const isEntitlementExist = !lodash_1.default.isEmpty(awardGroupCounter) &&
            !lodash_1.default.isEmpty(entitlementCode) &&
            !lodash_1.default.isEmpty(counterCode);
        if (!isEntitlementExist) {
            logger_1.default.info(`retrieveUniqueEntitlementCodesBasedOnTransactionCodes: isEntitlementExist: ${isEntitlementExist}`);
            return;
        }
        //validation 2: check whether entitlement code is within the predefined boundaries (currently only handling bonus transfer and bonus atm withdrawal)
        const isWithinFilteredEntitlementCodes = entitlement_helper_1.default
            .getFilteredEntitlementCodes()
            .includes(entitlementCode);
        if (!isWithinFilteredEntitlementCodes) {
            return;
        }
        if (!uniqueEntitlementCodeMap.has(limitGroupCode)) {
            uniqueEntitlementCodeMap.set(limitGroupCode, {
                limitGroupCode,
                awardGroupCounter,
                entitlementCode,
                counterCode
            });
        }
    });
    return uniqueEntitlementCodeMap;
};
const validateLatestEntitlementQuota = (latestEntitlements) => {
    const countersResp = [];
    //latestEntitlements.forEach(entitlement => {
    for (const entitlement of latestEntitlements) {
        const { quota, usedQuota } = entitlement;
        if (lodash_1.default.isUndefined(usedQuota)) {
            continue;
        }
        //check if existing
        const futureQuota = usedQuota + 1;
        if (futureQuota <= quota) {
            //update usedquota to reflect future quota
            entitlement.usedQuota = futureQuota;
            //emulate successful record //if it is still within quota
            countersResp.push({
                id: v4_1.default(),
                recorded: true
            });
        }
        else {
            //if it bigger then quota
            //emulate failed update
            countersResp.push({
                id: v4_1.default(),
                recorded: false,
                reason: {
                    type: entitlement_enum_1.EntitlementErrorReasonType.QUOTA_LIMIT,
                    description: 'exceed quota limit'
                }
            });
        }
    }
    // });
    return {
        entitlements: latestEntitlements,
        counters: countersResp
    };
};
const retrieveCustomerEntitlementRecommendedServicePreview = (params) => __awaiter(void 0, void 0, void 0, function* () {
    //extract params
    const { transactionCodes, customerId } = params;
    const updatedEntitlementMap = new Map();
    //when it is only to preview the recommended service,
    //transaction id will not exists
    if (lodash_1.default.isEmpty(transactionCodes) ||
        lodash_1.default.isUndefined(transactionCodes) ||
        lodash_1.default.isEmpty(customerId) ||
        lodash_1.default.isUndefined(customerId)) {
        const detail = `transactionCodes or customerId is null!`;
        logger_1.default.info(`retrieveCustomerEntitlementRecommendedServicePreview: ${detail}`);
        // return null;
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.JAGO, errors_1.ERROR_CODE.CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_PREVIEW_ERROR);
    }
    //based on multiple transaction codes, extract unique entitlement code map
    const uniqueEntitlementCodeMap = retrieveUniqueEntitlementCodesBasedOnTransactionCodes(transactionCodes);
    //if there is no entitlement map, return empty map
    if (uniqueEntitlementCodeMap == null || uniqueEntitlementCodeMap.size == 0) {
        logger_1.default.info(`retrieveCustomerEntitlementRecommendedServicePreview: uniqueEntitlementCodeMap is empty, 
    hence no changes on usagecounters to be made
    `);
        return null;
    }
    for (let [limitGroupCode, entitlementInfo] of uniqueEntitlementCodeMap) {
        const { entitlementCode, awardGroupCounter } = entitlementInfo;
        logger_1.default.info(`retrieveCustomerEntitlementRecommendedServicePreview: 
    OVERVIEW on entitlement info of
    entitlementCode: [${entitlementCode}] - 
    awardGroupCounter: [${awardGroupCounter}] - 
    `);
        //step 1 - execute query
        const latestEntitlement = yield entitlement_repository_1.default.getEntitlements(customerId, [entitlementCode]);
        //step 2 - emulate usedQuota + 1 to provide overview whether there is a fee for current transaction
        const latestEntitlementValidatedResponse = validateLatestEntitlementQuota(latestEntitlement);
        if (!updatedEntitlementMap.has(limitGroupCode)) {
            updatedEntitlementMap.set(limitGroupCode, latestEntitlementValidatedResponse);
        }
        if (!updatedEntitlementMap.has(awardGroupCounter)) {
            updatedEntitlementMap.set(awardGroupCounter, latestEntitlementValidatedResponse);
        }
    }
    return {
        updatedEntitlementMapResponse: updatedEntitlementMap
    };
});
const processCustomerEntitlementConsumption = (params) => __awaiter(void 0, void 0, void 0, function* () {
    /*
      assumption:
        1. multiple transaction codes
        2. 1 limitGroupCode will only have 1 counterCode
          -> e.g. 1 limitGroupCode is mapped to only 1 awardGroupCounter
        3. in most cases, the uniqueEntitlementCodeMap will only return 1 result
    */
    //extract params
    const { transactionCodes, customerId, transactionId, paymentServiceType } = params;
    const updatedEntitlementMap = new Map();
    if (lodash_1.default.isUndefined(paymentServiceType) || lodash_1.default.isEmpty(paymentServiceType)) {
        const detail = `Payment service type is undefined or empty!`;
        logger_1.default.info(`processCustomerEntitlementConsumption: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.JAGO, errors_1.ERROR_CODE.CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_CONSUME_ERROR);
    }
    //check whether current paymentServiceType excluded from payment serviceType
    if (entitlement_constant_1.ENTITLEMENT_EXCLUDED_PAYMENT_SERVICE_TYPES.includes(paymentServiceType)) {
        return null;
    }
    //if the required parameters not exists, then throw error
    if (lodash_1.default.isUndefined(transactionCodes) ||
        lodash_1.default.isEmpty(transactionCodes) ||
        lodash_1.default.isUndefined(customerId) ||
        lodash_1.default.isEmpty(customerId) ||
        lodash_1.default.isUndefined(transactionId) ||
        lodash_1.default.isEmpty(transactionId)) {
        let detail = `transactionCodes ${transactionCodes}, customerId ${customerId}!`;
        logger_1.default.info(`processCustomerEntitlementConsumption: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.SUBMITTER, errors_1.ERROR_CODE.CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_CONSUME_ERROR);
    }
    //based on multiple transaction codes, extract unique entitlement code map
    const uniqueEntitlementCodeMap = retrieveUniqueEntitlementCodesBasedOnTransactionCodes(transactionCodes);
    //if there is no entitlement map, return empty map
    if (uniqueEntitlementCodeMap == null || uniqueEntitlementCodeMap.size == 0) {
        logger_1.default.info(`processCustomerEntitlementConsumption: uniqueEntitlementCodeMap is empty, 
    hence no changes on usagecounters to be made
    `);
        return null;
    }
    const entitlementInfoArr = [];
    for (let [limitGroupCode, entitlementInfo] of uniqueEntitlementCodeMap) {
        // uniqueEntitlementCodeMap.forEach(async (entitlementInfo, limitGroupCode) => {
        //step 1 -- generate unique ref key
        const entitlementCodeRefId = v4_1.default();
        const { counterCode, entitlementCode, awardGroupCounter } = entitlementInfo;
        logger_1.default.info(`processCustomerEntitlementConsumption: CONSUMING entitlement info of
    counterCode: [${counterCode}] - 
    entitlementCode: [${entitlementCode}] - 
    awardGroupCounter: [${awardGroupCounter}] - 
    `);
        //step 2 -- prepare to update counter usage based on the counter code
        const counterUsages = [
            {
                id: entitlementCodeRefId,
                counterCode: counterCode,
                usedQuota: 1
            }
        ];
        //step 3 - execute update counter usage as well as query the updated result
        const updatedEntitlement = yield entitlement_repository_1.default.updateEntitlementUsageCounters({
            customerId: customerId,
            entitlementCodes: [entitlementCode],
            counterUsages: counterUsages
        });
        if (!lodash_1.default.isEmpty(updatedEntitlement) &&
            !lodash_1.default.isEmpty(updatedEntitlement.counters)) {
            //step 4 -- check if updateEntitlment actually succeed or failed
            const isEntitlementUpdated = updatedEntitlement.counters[0].recorded;
            //only if successful then insert to db
            //entitlementInfoArr only used for reversal, hence only need to record
            //the entitlement that is successfully updated
            if (isEntitlementUpdated) {
                entitlementInfoArr.push({
                    entitlementCodeRefId: entitlementCodeRefId,
                    counterCode: counterCode,
                    customerId: customerId
                });
            }
            //save based on limitgroupcode
            //note: this will still keep track of those
            //entitlements that is not recorded, that is
            //used to indicate mainly the entailing fee
            if (!updatedEntitlementMap.has(limitGroupCode)) {
                updatedEntitlementMap.set(limitGroupCode, updatedEntitlement);
            }
            //save based on award group counter
            //note: this will still keep track of those
            //entitlements that is not recorded, that is
            //used to indicate mainly the entailing fee
            if (!updatedEntitlementMap.has(awardGroupCounter)) {
                updatedEntitlementMap.set(awardGroupCounter, updatedEntitlement);
            }
        }
    }
    if (updatedEntitlementMap.size === 0) {
        const detail = `Error when trying to consume entitlement!`;
        logger_1.default.error(`processCustomerEntitlementConsumption: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.UNKNOWN, errors_1.ERROR_CODE.CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_CONSUME_ERROR);
    }
    //update transaction model
    yield transaction_repository_1.default.update(transactionId, {
        entitlementInfo: entitlementInfoArr
    });
    //setting in context for reversal
    yield contextHandler_1.setTransactionInfoTracker(entitlementInfoArr);
    // step 5 - return the map
    return {
        updatedEntitlementMapResponse: updatedEntitlementMap
    };
});
const processReversalEntitlementCounterUsageBasedOnContext = () => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info('Processing reversal entitlement counter usage based on context');
    const info = contextHandler_1.getTransactionInfoTracker();
    if (info && !lodash_1.default.isEmpty(info)) {
        for (const entitlementInfo of info) {
            if (entitlementInfo) {
                const counterReverts = [
                    {
                        id: entitlementInfo.entitlementCodeRefId,
                        counterCode: entitlementInfo.counterCode
                    }
                ];
                const revertCounterResponse = yield entitlement_repository_1.default.revertEntitlementUsageCounters({
                    customerId: entitlementInfo.customerId,
                    counterReverts: counterReverts
                });
                logger_1.default.info(`processReversalEntitlementCounterUsageBasedOnContext: revert info
            ${JSON.stringify(revertCounterResponse.counterReverts)}
        `);
            }
        }
    }
});
const mapEntitlementUsageCounterByLimitGroupCode = (usageCounters, entitlementResultResponse) => {
    if (entitlementResultResponse != null) {
        const usedUsageCounters = [...usageCounters];
        const entitlementMap = entitlementResultResponse.updatedEntitlementMapResponse;
        usedUsageCounters.map(usageCounter => {
            //usageCounter limitGroupCode here will also include awardGroupCounter
            if (entitlementMap.has(usageCounter.limitGroupCode)) {
                const entitlementResult = entitlementMap.get(usageCounter.limitGroupCode);
                if (entitlementResult &&
                    entitlementResult.entitlements &&
                    entitlementResult.entitlements[0] &&
                    entitlementResult.counters &&
                    entitlementResult.counters[0]) {
                    //only get the first one as we are dealing with only one entitlement code at a time
                    const { usedQuota, quota } = entitlementResult.entitlements[0];
                    //check whether the updates failed or not
                    //whether the fail is due to quota limit
                    const { recorded, reason } = entitlementResult.counters[0];
                    let isQuotaExceeded = false;
                    if (!recorded &&
                        reason &&
                        reason.type == entitlement_enum_1.EntitlementErrorReasonType.QUOTA_LIMIT) {
                        isQuotaExceeded = true;
                    }
                    usageCounter.monthlyAccumulationTransaction = usedQuota;
                    usageCounter.quota = parseInt(quota.toString());
                    usageCounter.isQuotaExceeded = isQuotaExceeded;
                }
            }
        });
        return usedUsageCounters;
    }
    return usageCounters;
};
const processReversalEntitlementCounterUsageOnTransactionConfirmation = (input) => {
    switch (input.responseCode) {
        case transaction_enum_1.ConfirmTransactionStatus.REQUEST_TIMEOUT:
        case transaction_enum_1.ConfirmTransactionStatus.ERROR_EXPECTED:
        case transaction_enum_1.ConfirmTransactionStatus.ERROR_UNEXPECTED:
            processReversalEntitlementCounterUsageBasedOnContext();
            break;
        default: {
            //do nothing
        }
    }
};
const getEntitlementByAwardGroupCounter = (usageCounters, awardGroupCounter) => {
    var _a;
    let defaultEntitlement = {
        monthlyAccumulationTransaction: 0,
        quota: undefined,
        limitGroupCode: undefined,
        dailyAccumulationAmount: 0
    };
    const entitlements = (_a = usageCounters) === null || _a === void 0 ? void 0 : _a.filter(usageCounter => usageCounter.limitGroupCode === awardGroupCounter);
    return !lodash_1.default.isEmpty(entitlements) &&
        !lodash_1.default.isUndefined(entitlements) &&
        entitlements
        ? Object.assign({}, ...entitlements)
        : defaultEntitlement;
};
const getEntitlementsMap = (customerId, entitlementCodes) => __awaiter(void 0, void 0, void 0, function* () {
    let entitlements;
    try {
        entitlements = yield entitlement_repository_1.default.getEntitlements(customerId, entitlementCodes);
    }
    catch (_a) {
        // We don't want to throw any error if there are no custom limits available
        // We just return without doing anything
        return new Map();
    }
    return new Map(entitlements.map(entitlement => [entitlement.entitlement, entitlement]));
});
const entitlementService = logger_1.wrapLogs({
    processReversalEntitlementCounterUsageBasedOnContext,
    processReversalEntitlementCounterUsageOnTransactionConfirmation,
    mapEntitlementUsageCounterByLimitGroupCode,
    getEntitlementByAwardGroupCounter,
    retrieveCustomerEntitlementRecommendedServicePreview,
    processCustomerEntitlementConsumption,
    getEntitlementsMap
});
exports.default = entitlementService;
//# sourceMappingURL=entitlement.service.js.map