"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fee_constant_1 = require("../fee/fee.constant");
const logger_1 = require("../logger");
const entitlement_enum_1 = require("./entitlement.enum");
const constructGetUsageCounterPayload = (entitlementCodes) => {
    return {
        entitlementCodes: entitlementCodes
    };
};
const getFilteredLimitGroupCodes = () => {
    return [
        entitlement_enum_1.AwardLimitGroupCodes.BONUS_TRANSFER,
        entitlement_enum_1.AwardLimitGroupCodes.BONUS_LOCAL_CASH_WITHDRAWAL
    ];
};
const getFilteredEntitlementCodes = () => {
    return [
        entitlement_enum_1.EntitlementCode.BONUS_TRANSFER,
        entitlement_enum_1.EntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL
    ];
};
const getFilteredFeeRuleType = () => {
    return [
        fee_constant_1.FeeRuleType.RTOL_TRANSFER_FEE_RULE,
        fee_constant_1.FeeRuleType.SKN_TRANSFER_FEE_RULE,
        fee_constant_1.FeeRuleType.RTGS_TRANSFER_FEE_RULE,
        fee_constant_1.FeeRuleType.LOCAL_ATM_WITHDRAWAL_RULE,
        fee_constant_1.FeeRuleType.WALLET_FEE_RULE,
        fee_constant_1.FeeRuleType.BIFAST_FEE_RULE,
        fee_constant_1.FeeRuleType.BIFAST_SHARIA_FEE_RULE
    ];
};
const entitlementHelper = logger_1.wrapLogs({
    constructGetUsageCounterPayload,
    getFilteredEntitlementCodes,
    getFilteredLimitGroupCodes,
    getFilteredFeeRuleType
});
exports.default = entitlementHelper;
//# sourceMappingURL=entitlement.helper.js.map