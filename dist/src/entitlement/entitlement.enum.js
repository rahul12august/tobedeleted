"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AwardLimitGroupCodes;
(function (AwardLimitGroupCodes) {
    AwardLimitGroupCodes["BONUS_TRANSFER"] = "Bonus_Transfer";
    AwardLimitGroupCodes["BONUS_LOCAL_CASH_WITHDRAWAL"] = "Bonus_Local_CashWithdrawal";
    AwardLimitGroupCodes["BONUS_OVERSEAS_CASH_WITHDRAWAL"] = "Bonus_Overseas_CashWithdrawal";
})(AwardLimitGroupCodes = exports.AwardLimitGroupCodes || (exports.AwardLimitGroupCodes = {}));
var EntitlementCode;
(function (EntitlementCode) {
    EntitlementCode["BONUS_TRANSFER"] = "count.free.transfer.out";
    EntitlementCode["BONUS_LOCAL_CASH_WITHDRAWAL"] = "count.free.atm_withdrawal";
})(EntitlementCode = exports.EntitlementCode || (exports.EntitlementCode = {}));
var EntitlementErrorReasonType;
(function (EntitlementErrorReasonType) {
    EntitlementErrorReasonType["QUOTA_LIMIT"] = "quotaLimit";
})(EntitlementErrorReasonType = exports.EntitlementErrorReasonType || (exports.EntitlementErrorReasonType = {}));
//# sourceMappingURL=entitlement.enum.js.map