"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CONSUMER_NAME = 'ms-transfer';
exports.CONSUMER_NAME = CONSUMER_NAME;
const PROVIDER_NAME = 'ms-entitlement';
exports.PROVIDER_NAME = PROVIDER_NAME;
const PORT_NUMBER = 49151;
exports.PORT_NUMBER = PORT_NUMBER;
//# sourceMappingURL=contract.constants.js.map