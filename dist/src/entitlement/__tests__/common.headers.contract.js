"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const pact_1 = require("@pact-foundation/pact");
const path_1 = __importDefault(require("path"));
const jestMockInit = () => {
    jest.mock('../../logger');
    jest.mock('../httpClient', () => {
        const httpClient = require('@dk/module-httpclient');
        const context = require('../../context').context;
        const logger = {
            error: jest.fn(),
            info: jest.fn(),
            debug: jest.fn(),
            warn: jest.fn(),
            critical: jest.fn() // critical: e => console.error(e)
        };
        return httpClient.createHttpClient({
            baseURL: 'http://localhost:49151',
            context,
            logger
        });
    });
};
const initPactProvider = (params) => {
    const { consumerName, providerName, portNumber } = params;
    const provider = new pact_1.Pact({
        consumer: consumerName,
        provider: providerName,
        port: portNumber,
        log: path_1.default.resolve(process.cwd(), 'logs', 'pact.log'),
        dir: path_1.default.resolve(process.cwd(), 'pacts'),
        logLevel: 'info'
    });
    return provider;
};
const contractCommonInitFn = { jestMockInit, initPactProvider };
exports.default = contractCommonInitFn;
//# sourceMappingURL=common.headers.contract.js.map