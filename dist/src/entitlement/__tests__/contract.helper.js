"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const pact_1 = require("@pact-foundation/pact");
const uuidGen = __importStar(require("uuid"));
const { like, string, decimal, boolean, uuid } = pact_1.Matchers;
const createCounterUsageObj = (params) => {
    return {
        id: uuidGen.v4(),
        counterCode: params.counterCode,
        usedQuota: params.usedQuota
    };
};
const convertCounterResponseToMap = (counters) => {
    const result = new Map(counters.map(counter => [counter.id, counter]));
    return result;
};
const convertExpectedOutputEntitlementToMatcher = (entitlementResponse) => {
    const emptyTemplate = {
        entitlement: {},
        type: {},
        dataType: {},
        description: {},
        quota: {},
        comments: {}
    };
    const result = entitlementResponse.map(entitlement => {
        const updatedEntitlementTemplate = Object.assign({}, emptyTemplate);
        updatedEntitlementTemplate.entitlement = string(entitlement.entitlement);
        updatedEntitlementTemplate.type = string(entitlement.type); //sample data
        updatedEntitlementTemplate.dataType = string(entitlement.dataType); //sample data
        updatedEntitlementTemplate.description = string(entitlement.description);
        updatedEntitlementTemplate.quota = decimal(entitlement.quota);
        updatedEntitlementTemplate.comments = string(entitlement.comments);
        if (entitlement.usedQuota) {
            updatedEntitlementTemplate.usedQuota = decimal(entitlement.usedQuota);
        }
        if (entitlement.counterCode) {
            updatedEntitlementTemplate.counterCode = string(entitlement.counterCode);
        }
        if (entitlement.value) {
            updatedEntitlementTemplate.value = string(entitlement.value);
        }
        return updatedEntitlementTemplate;
    });
    //return eachLike(result[0]);
    return result;
};
const convertExpectedOutputToCounterMatcher = (expectedOutputRaw) => {
    const emptyTemplate = {
        id: {},
        recorded: {}
    };
    const result = expectedOutputRaw.map(d => {
        const updatedCounterTemplate = Object.assign({}, emptyTemplate);
        updatedCounterTemplate.id = uuid(d.id);
        updatedCounterTemplate.recorded = boolean(d.recorded);
        if (d.reason) {
            updatedCounterTemplate.reason = like({
                type: d.reason.type,
                description: d.reason.description
            });
        }
        return updatedCounterTemplate;
    });
    return result;
};
const convertExpectedOutputToMatcher = (expectedOutputs) => {
    const entitlementResult = convertExpectedOutputEntitlementToMatcher(expectedOutputs.entitlements);
    const counterResult = convertExpectedOutputToCounterMatcher(expectedOutputs.counters);
    return {
        entitlements: entitlementResult,
        counters: counterResult
    };
};
const ContractHelper = {
    createCounterUsageObj,
    convertCounterResponseToMap,
    convertExpectedOutputToMatcher
};
exports.default = ContractHelper;
//# sourceMappingURL=contract.helper.js.map