"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importStar(require("lodash"));
const errors_1 = require("../common/errors");
const AppError_1 = require("../errors/AppError");
const logger_1 = __importStar(require("../logger"));
const entitlement_constant_1 = require("./entitlement.constant");
const entitlement_enum_1 = require("./entitlement.enum");
const entitlement_helper_1 = __importDefault(require("./entitlement.helper"));
const httpClient_1 = __importDefault(require("./httpClient"));
const transaction_enum_1 = require("../transaction/transaction.enum");
const getEntitlements = (customerId, entitlementCodes) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.info(`getEntitlements: Retrieving entitlements for customerId: ${customerId}.`);
        const endpoint = entitlement_constant_1.ENTITLEMENT_API_ENDPOINT(customerId);
        const payload = entitlement_helper_1.default.constructGetUsageCounterPayload(entitlementCodes);
        const result = yield httpClient_1.default.post(endpoint, payload);
        if (lodash_1.default.isEmpty(result)) {
            const detail = `Failed to retrieve entitlement for ${entitlementCodes.toString()}!`;
            logger_1.default.error(`getEntitlements: ${detail}`);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_ENTITLEMENT, errors_1.ERROR_CODE.FAILED_TO_RETRIEVE_ENTITLEMENT);
        }
        return lodash_1.get(result, ['data', 'entitlements'], []);
    }
    catch (error) {
        let detail = `Error getting entitlements for customerID: ${customerId}, ${error.message}!`;
        logger_1.default.error(`getEntitlements: ${detail}`);
        const errorDetail = [];
        errorDetail.push({
            message: (error && error.message) || '',
            key: error.code,
            code: (error && error.status) || ''
        });
        let actor = error.status === 400 || error.status === 404
            ? transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER
            : transaction_enum_1.TransferFailureReasonActor.MS_ENTITLEMENT;
        if (error instanceof AppError_1.TransferAppError) {
            detail = error.detail;
            actor = error.actor;
        }
        throw new AppError_1.TransferAppError(detail, actor, error.code, errorDetail);
    }
});
const updateEntitlementUsageCounters = (params) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const { customerId, entitlementCodes, counterUsages } = params;
    logger_1.default.info(`updateEntitlementUsageCounters [ms-entitlement] for customerId: ${customerId}.`);
    const endpoint = entitlement_constant_1.ENTITLEMENT_API_ENDPOINT(customerId);
    const result = yield httpClient_1.default.post(endpoint, {
        entitlementCodes,
        counterUsages
    });
    if (!result.data.hasOwnProperty('entitlements') ||
        (!result.data.counters[0].recorded &&
            result.data.counters[0].reason.type !==
                entitlement_enum_1.EntitlementErrorReasonType.QUOTA_LIMIT)) {
        const infos = counterUsages
            .map(counterUsage => `[id: ${counterUsage.id} AND counterCode: ${counterUsage.counterCode} AND usedQuota: ${counterUsage.usedQuota}]`)
            .join(', ');
        const detail = `Failed to update entitlement for ${infos}!`;
        logger_1.default.error(`updateEntitlementUsageCounters: ${detail}`);
        throw new AppError_1.TransferAppError(detail, result.status === 400 || result.status === 404
            ? transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER
            : transaction_enum_1.TransferFailureReasonActor.MS_ENTITLEMENT, errors_1.ERROR_CODE.FAILED_TO_UPDATE_ENTITLEMENT);
    }
    return (_a = result) === null || _a === void 0 ? void 0 : _a.data;
});
const revertEntitlementUsageCounters = (params) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    const { customerId, entitlementCodes, counterReverts } = params;
    logger_1.default.info(`revertEntitlementUsageCounters [ms-entitlement] for customerId: ${customerId}.`);
    const endpoint = entitlement_constant_1.ENTITLEMENT_API_ENDPOINT(customerId);
    const result = yield httpClient_1.default.post(endpoint, {
        entitlementCodes,
        counterReverts
    });
    if (!result) {
        const infos = counterReverts
            .map(counterRevert => `[id: ${counterRevert.id} AND counterCode: ${counterRevert.counterCode}]`)
            .join(', ');
        const detail = `Failed to revert entitlement for ${infos}!`;
        logger_1.default.error(`revertEntitlementUsageCounters: ${detail}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errors_1.ERROR_CODE.FAILED_TO_REVERT_ENTITLEMENT);
    }
    return (_b = result) === null || _b === void 0 ? void 0 : _b.data;
});
const entitlementRepository = logger_1.wrapLogs({
    getEntitlements,
    updateEntitlementUsageCounters,
    revertEntitlementUsageCounters
});
exports.default = entitlementRepository;
//# sourceMappingURL=entitlement.repository.js.map