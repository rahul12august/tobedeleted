"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const entitlement_constant_1 = require("../entitlement.constant");
exports.mockExcludedEntitlementPaymentServiceTypes = () => entitlement_constant_1.ENTITLEMENT_EXCLUDED_PAYMENT_SERVICE_TYPES.map(paymentServiceType => {
    return {
        expected: null,
        params: {
            customerId: 'cust1234',
            transactionId: 'transId1',
            transactionCodes: [],
            paymentServiceType: paymentServiceType
        }
    };
});
//# sourceMappingURL=entitlement.service.mock.data.js.map