"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const bankNetwork_enum_1 = require("@dk/module-common/dist/enum/bankNetwork.enum");
const pact_1 = require("@pact-foundation/pact");
const entitlement_enum_1 = require("../entitlement.enum");
exports.getFreeAtmWithdrawalResponse = () => {
    return {
        entitlements: [
            {
                entitlement: pact_1.Matchers.string(),
                type: pact_1.Matchers.string(),
                dataType: pact_1.Matchers.string(),
                description: pact_1.Matchers.string(),
                quota: pact_1.Matchers.integer(),
                usedQuota: pact_1.Matchers.integer(),
                counterCode: pact_1.Matchers.string(),
                comments: pact_1.Matchers.string()
            }
        ]
    };
};
exports.getFreeAtmWithdrawalWithError = (msg, code) => {
    return {
        error: {
            message: pact_1.Matchers.string(msg),
            code: pact_1.Matchers.string(code)
        }
    };
};
exports.mockEntitlementInfoContext = () => {
    return [
        {
            entitlementCode: 'entitlementCode',
            counterCode: 'counterCode',
            entitlementCodeRefId: 'entitlementCodeRefId',
            customerId: 'customerId',
            responseCode: 'responseCode',
            startTimeTracker: new Date()
        }
    ];
};
exports.mockEntitlementReversalResponse = () => {
    return {
        entitlements: [],
        counterReverts: [
            {
                id: 'id',
                recorded: true,
                revertedValue: 1
            }
        ]
    };
};
exports.mockUsageCounterData = () => {
    return [
        {
            limitGroupCode: 'limitGroupCode',
            dailyAccumulationAmount: 10000,
            monthlyAccumulationTransaction: 100000,
            quota: 6
        }
    ];
};
exports.mockEntitlementResultResponse = () => {
    const data = new Map();
    data.set('limitGroupCode', {
        entitlements: [
            {
                entitlement: 'entitlement',
                type: 'type',
                dataType: 'integer',
                description: 'description',
                quota: 6,
                comments: 'comments',
                counterCode: 'counterCode',
                usedQuota: 1
            }
        ],
        counters: [
            {
                id: 'id',
                recorded: true,
                reason: {
                    type: 'type',
                    description: 'description'
                }
            }
        ]
    });
    return { updatedEntitlementMapResponse: data };
};
exports.mockEntitlementResultResponseExceedLimitation = () => {
    const data = new Map();
    data.set('limitGroupCode', {
        entitlements: [
            {
                entitlement: 'entitlement',
                type: 'type',
                dataType: 'integer',
                description: 'description',
                quota: 6,
                comments: 'comments',
                counterCode: 'counterCode',
                usedQuota: 6
            }
        ],
        counters: [
            {
                id: 'id',
                recorded: false,
                reason: {
                    type: entitlement_enum_1.EntitlementErrorReasonType.QUOTA_LIMIT,
                    description: 'description'
                }
            }
        ]
    });
    return { updatedEntitlementMapResponse: data };
};
exports.mockCustomerEntitlementByTransCodeRequest = () => {
    return {
        transactionCodes: [
            {
                interchange: bankNetwork_enum_1.BankNetworkEnum.ALTO,
                transactionCode: 'transactionCode',
                transactionCodeInfo: {
                    code: 'code',
                    limitGroupCode: 'limitGroupCode',
                    feeRules: 'feeRules',
                    minAmountPerTx: 10000,
                    maxAmountPerTx: 100000,
                    defaultCategoryCode: 'defaultCategoryCode',
                    channel: 'channel',
                    awardGroupCounter: 'awardGroupCounter',
                    entitlementCode: 'entitlementCode',
                    counterCode: 'counterCode'
                }
            }
        ],
        customerId: 'customerId',
        transactionId: 'transactionId',
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.TRANSFER
    };
};
exports.mockUpdateUsageCounterEntitlementResponse = () => {
    return {
        entitlements: [
            {
                entitlement: 'entitlement',
                type: 'type',
                dataType: 'dataType',
                description: 'description',
                quota: 5,
                comments: 'comments',
                counterCode: 'counterCode',
                usedQuota: 1,
                value: 'value'
            }
        ],
        counters: [
            {
                id: 'id',
                recorded: true
            }
        ]
    };
};
//# sourceMappingURL=entitlement.data.js.map