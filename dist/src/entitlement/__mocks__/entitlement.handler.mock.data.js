"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const fee_constant_1 = require("../../fee/fee.constant");
const entitlement_enum_1 = require("../entitlement.enum");
exports.createRtolFeeRuleInfo = (request) => {
    const rtolTransferFeeRuleObject = {
        code: request.code,
        counterCode: request.counterCode,
        basicFeeCode1: [
            {
                interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                basicFeeCode: 'TF015'
            },
            {
                interchange: module_common_1.BankNetworkEnum.ALTO,
                basicFeeCode: 'TF008'
            }
        ],
        basicFeeCode2: [
            {
                interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                basicFeeCode: 'TF010'
            },
            {
                interchange: module_common_1.BankNetworkEnum.ALTO,
                basicFeeCode: 'TF009'
            }
        ],
        monthlyNoTransaction: request.monthlyNoTransaction,
        interchange: request.interchange
    };
    return rtolTransferFeeRuleObject;
};
exports.createLocalAtmWithdrawalRule = (request) => {
    const localAtmFeeRuleInfo = {
        code: request.code,
        counterCode: fee_constant_1.COUNTER.COUNTER_07,
        basicFeeCode1: [
            {
                interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                basicFeeCode: 'CW007'
            },
            {
                interchange: module_common_1.BankNetworkEnum.ALTO,
                basicFeeCode: 'CW005'
            }
        ],
        basicFeeCode2: [
            {
                interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                basicFeeCode: 'CW001'
            },
            {
                interchange: module_common_1.BankNetworkEnum.ALTO,
                basicFeeCode: 'CW002'
            }
        ],
        monthlyNoTransaction: request.monthlyNoTransaction,
        interchange: request.interchange
    };
    return localAtmFeeRuleInfo;
};
exports.createOverseasAtmWithdrawalRule = (request) => {
    const localAtmFeeRuleInfo = {
        code: request.code,
        counterCode: fee_constant_1.COUNTER.COUNTER_07,
        basicFeeCode1: [
            {
                interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                basicFeeCode: 'CW007'
            },
            {
                interchange: module_common_1.BankNetworkEnum.ALTO,
                basicFeeCode: 'CW005'
            }
        ],
        basicFeeCode2: [
            {
                interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                basicFeeCode: 'CW001'
            },
            {
                interchange: module_common_1.BankNetworkEnum.ALTO,
                basicFeeCode: 'CW002'
            }
        ],
        monthlyNoTransaction: request.monthlyNoTransaction,
        interchange: request.interchange
    };
    return localAtmFeeRuleInfo;
};
exports.createBiFastFeeRule = (request) => {
    const biFastFeeRuleObject = {
        code: request.code,
        counterCode: fee_constant_1.COUNTER.COUNTER_01,
        basicFeeCode1: [
            {
                basicFeeCode: 'TF022'
            }
        ],
        basicFeeCode2: [
            {
                basicFeeCode: 'TF020'
            }
        ],
        monthlyNoTransaction: request.monthlyNoTransaction,
        interchange: request.interchange
    };
    return biFastFeeRuleObject;
};
exports.createWalletFeeRule = (request) => {
    const walletFeeRuleObject = {
        code: request.code,
        counterCode: fee_constant_1.COUNTER.COUNTER_01,
        basicFeeCode1: [
            {
                interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                basicFeeCode: 'BF011'
            },
            {
                interchange: module_common_1.BankNetworkEnum.ALTO,
                basicFeeCode: 'BF004'
            }
        ],
        basicFeeCode2: [
            {
                interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                basicFeeCode: 'BF005'
            },
            {
                interchange: module_common_1.BankNetworkEnum.ALTO,
                basicFeeCode: 'BF003'
            }
        ],
        monthlyNoTransaction: request.monthlyNoTransaction,
        interchange: request.interchange
    };
    return walletFeeRuleObject;
};
const generateFeeRuleInfoScenarioParams = (quota, paramsFeeRule, paramsMappedEntitlement) => {
    const updatedParamsFeeRule = Object.assign(Object.assign({}, paramsFeeRule), { quota: quota });
    const updatedParamsMappedEntitlement = Object.assign({}, paramsMappedEntitlement);
    updatedParamsMappedEntitlement.usageCounters[0].quota = quota;
    updatedParamsMappedEntitlement.usageCounters[1].quota = quota;
    return {
        //scenario 1 - where used quota < quota
        withinQuota: {
            paramsFeeRule: () => {
                return Object.assign({}, updatedParamsFeeRule);
            },
            paramsMappedEntitlement: () => {
                const output = Object.assign({}, updatedParamsMappedEntitlement);
                output.usageCounters[0].monthlyAccumulationTransaction = 1;
                output.usageCounters[1].monthlyAccumulationTransaction = 1;
                return output;
            }
        },
        //scenario 2 - where used quota == quota, but not exceeded yet
        withinQuota2: {
            paramsFeeRule: () => {
                return Object.assign(Object.assign({}, updatedParamsFeeRule), { monthlyNoTransaction: 24 });
            },
            paramsMappedEntitlement: () => {
                const output = Object.assign({}, updatedParamsMappedEntitlement);
                output.usageCounters[0].monthlyAccumulationTransaction = 25;
                output.usageCounters[1].monthlyAccumulationTransaction = 25;
                return output;
            }
        },
        //scenario 3 - where used quota == quota, but exceeded
        exceedQuota: {
            paramsFeeRule: () => {
                return Object.assign(Object.assign({}, updatedParamsFeeRule), { monthlyNoTransaction: 25 });
            },
            paramsMappedEntitlement: () => {
                const output = Object.assign({}, updatedParamsMappedEntitlement);
                output.usageCounters[0].monthlyAccumulationTransaction = 25;
                output.usageCounters[0].isQuotaExceeded = true;
                output.usageCounters[1].monthlyAccumulationTransaction = 25;
                output.usageCounters[1].isQuotaExceeded = true;
                return output;
            }
        }
    };
};
const generateFeeRuleInfoScenarioOutput = (paramsFeeRule, paramsMappedEntitlement) => {
    let feeRuleInfoTemp = null;
    switch (paramsFeeRule.code) {
        case fee_constant_1.FeeRuleType.RTOL_TRANSFER_FEE_RULE:
            feeRuleInfoTemp = exports.createRtolFeeRuleInfo(paramsFeeRule);
            break;
        case fee_constant_1.FeeRuleType.LOCAL_ATM_WITHDRAWAL_RULE:
            feeRuleInfoTemp = exports.createLocalAtmWithdrawalRule(paramsFeeRule);
            break;
        case fee_constant_1.FeeRuleType.WALLET_FEE_RULE:
            feeRuleInfoTemp = exports.createWalletFeeRule(paramsFeeRule);
            break;
        case fee_constant_1.FeeRuleType.BIFAST_FEE_RULE:
            feeRuleInfoTemp = exports.createBiFastFeeRule(paramsFeeRule);
            break;
        case fee_constant_1.FeeRuleType.OVERSEAS_ATM_WITHDRAWAL_RULE:
            feeRuleInfoTemp = exports.createOverseasAtmWithdrawalRule(paramsFeeRule);
            break;
    }
    // feeRuleInfo = createRtolFeeRuleInfo(paramsFeeRule);
    const { usageCounters } = paramsMappedEntitlement;
    if (feeRuleInfoTemp != null) {
        const feeRuleInfo = Object.assign({}, feeRuleInfoTemp);
        return {
            paramsFeeRule,
            paramsMappedEntitlement,
            output: Object.assign(Object.assign({}, feeRuleInfo), { quota: usageCounters[0].quota, monthlyNoTransaction: usageCounters[0].monthlyAccumulationTransaction, isQuotaExceeded: usageCounters[0].isQuotaExceeded })
        };
    }
    return null;
};
/************************
 * RTOL TRANSFER
 *************************/
exports.mockRtolTransferScenario = (transferFeeQuota) => {
    const paramsFeeTemplate = {
        code: fee_constant_1.FeeRuleType.RTOL_TRANSFER_FEE_RULE,
        transactionAmount: 50000,
        monthlyNoTransaction: 0,
        interchange: module_common_1.BankNetworkEnum.IRIS,
        counterCode: fee_constant_1.COUNTER.COUNTER_01
    };
    const paramsMappedEntitlementTemplate = {
        awardGroupCounter: entitlement_enum_1.AwardLimitGroupCodes.BONUS_TRANSFER,
        usageCounters: [
            //usage counters should be already mapped with result from entitlement at this point
            {
                limitGroupCode: 'L002',
                dailyAccumulationAmount: 50000,
                monthlyAccumulationTransaction: 0,
                quota: 0,
                isQuotaExceeded: false
            },
            {
                limitGroupCode: entitlement_enum_1.AwardLimitGroupCodes.BONUS_TRANSFER,
                dailyAccumulationAmount: 50000,
                monthlyAccumulationTransaction: 0,
                quota: 0,
                isQuotaExceeded: false
            }
        ]
    };
    const { withinQuota: withinQuotaScenario, withinQuota2: withinQuotaScenario2, exceedQuota: exceedQuota } = generateFeeRuleInfoScenarioParams(transferFeeQuota, paramsFeeTemplate, paramsMappedEntitlementTemplate);
    return {
        withinQuota: () => {
            return generateFeeRuleInfoScenarioOutput(withinQuotaScenario.paramsFeeRule(), withinQuotaScenario.paramsMappedEntitlement());
        },
        withinQuota2: () => {
            return generateFeeRuleInfoScenarioOutput(withinQuotaScenario2.paramsFeeRule(), withinQuotaScenario2.paramsMappedEntitlement());
        },
        exceedQuota: () => {
            return generateFeeRuleInfoScenarioOutput(exceedQuota.paramsFeeRule(), exceedQuota.paramsMappedEntitlement());
        }
    };
};
/************************
 * LOCAL ATM WITHDRAWAL
 ************************/
exports.mockLocalAtmWithdrawalScenario = (localAtmWithdrawalQuota) => {
    const paramsFeeRuleTemplate = {
        code: fee_constant_1.FeeRuleType.LOCAL_ATM_WITHDRAWAL_RULE,
        transactionAmount: 50000,
        monthlyNoTransaction: 0,
        interchange: module_common_1.BankNetworkEnum.ALTO,
        counterCode: fee_constant_1.COUNTER.COUNTER_07
    };
    const paramsMappedEntitlementTemplate = {
        awardGroupCounter: entitlement_enum_1.AwardLimitGroupCodes.BONUS_LOCAL_CASH_WITHDRAWAL,
        usageCounters: [
            //usage counters should be already mapped with result from entitlement at this point
            {
                limitGroupCode: 'L006',
                dailyAccumulationAmount: 50000,
                monthlyAccumulationTransaction: 0,
                quota: 0,
                isQuotaExceeded: false
            },
            {
                limitGroupCode: entitlement_enum_1.AwardLimitGroupCodes.BONUS_LOCAL_CASH_WITHDRAWAL,
                dailyAccumulationAmount: 50000,
                monthlyAccumulationTransaction: 0,
                quota: 0,
                isQuotaExceeded: false
            }
        ]
    };
    const { withinQuota: withinQuotaScenario, withinQuota2: withinQuotaScenario2, exceedQuota: exceedQuota } = generateFeeRuleInfoScenarioParams(localAtmWithdrawalQuota, paramsFeeRuleTemplate, paramsMappedEntitlementTemplate);
    return {
        withinQuota: () => {
            return generateFeeRuleInfoScenarioOutput(withinQuotaScenario.paramsFeeRule(), withinQuotaScenario.paramsMappedEntitlement());
        },
        withinQuota2: () => {
            return generateFeeRuleInfoScenarioOutput(withinQuotaScenario2.paramsFeeRule(), withinQuotaScenario2.paramsMappedEntitlement());
        },
        exceedQuota: () => {
            return generateFeeRuleInfoScenarioOutput(exceedQuota.paramsFeeRule(), exceedQuota.paramsMappedEntitlement());
        }
    };
};
/************************
 * OVERSEAS ATM WITHDRAWAL
 ************************/
exports.mockOverseasAtmWithdrawalScenario = (overseasAtmWithdrawalQuota) => {
    const paramsFeeRuleTemplate = {
        code: fee_constant_1.FeeRuleType.OVERSEAS_ATM_WITHDRAWAL_RULE,
        transactionAmount: 50000,
        monthlyNoTransaction: 0,
        interchange: module_common_1.BankNetworkEnum.ALTO,
        counterCode: fee_constant_1.COUNTER.COUNTER_07
    };
    const paramsMappedEntitlementTemplate = {
        awardGroupCounter: entitlement_enum_1.AwardLimitGroupCodes.BONUS_OVERSEAS_CASH_WITHDRAWAL,
        usageCounters: [
            //usage counters should be already mapped with result from entitlement at this point
            {
                limitGroupCode: 'L006',
                dailyAccumulationAmount: 50000,
                monthlyAccumulationTransaction: 0,
                quota: 0,
                isQuotaExceeded: false
            },
            {
                limitGroupCode: entitlement_enum_1.AwardLimitGroupCodes.BONUS_OVERSEAS_CASH_WITHDRAWAL,
                dailyAccumulationAmount: 50000,
                monthlyAccumulationTransaction: 0,
                quota: 0,
                isQuotaExceeded: false
            }
        ]
    };
    const { withinQuota: withinQuotaScenario, withinQuota2: withinQuotaScenario2, exceedQuota: exceedQuota } = generateFeeRuleInfoScenarioParams(overseasAtmWithdrawalQuota, paramsFeeRuleTemplate, paramsMappedEntitlementTemplate);
    return {
        withinQuota: () => {
            return generateFeeRuleInfoScenarioOutput(withinQuotaScenario.paramsFeeRule(), withinQuotaScenario.paramsMappedEntitlement());
        },
        withinQuota2: () => {
            return generateFeeRuleInfoScenarioOutput(withinQuotaScenario2.paramsFeeRule(), withinQuotaScenario2.paramsMappedEntitlement());
        },
        exceedQuota: () => {
            return generateFeeRuleInfoScenarioOutput(exceedQuota.paramsFeeRule(), exceedQuota.paramsMappedEntitlement());
        }
    };
};
/************************
 * WALLET
 ************************/
exports.mockWalletScenario = (walletQuota) => {
    const paramsFeeRuleTemplate = {
        code: fee_constant_1.FeeRuleType.WALLET_FEE_RULE,
        transactionAmount: 50000,
        monthlyNoTransaction: 0,
        interchange: module_common_1.BankNetworkEnum.IRIS,
        counterCode: fee_constant_1.COUNTER.COUNTER_01
    };
    const paramsMappedEntitlementTemplate = {
        awardGroupCounter: entitlement_enum_1.AwardLimitGroupCodes.BONUS_TRANSFER,
        usageCounters: [
            //usage counters should be already mapped with result from entitlement at this point
            {
                limitGroupCode: 'L002',
                dailyAccumulationAmount: 50000,
                monthlyAccumulationTransaction: 0,
                quota: 0,
                isQuotaExceeded: false
            },
            {
                limitGroupCode: entitlement_enum_1.AwardLimitGroupCodes.BONUS_TRANSFER,
                dailyAccumulationAmount: 50000,
                monthlyAccumulationTransaction: 0,
                quota: 0,
                isQuotaExceeded: false
            }
        ]
    };
    const { withinQuota: withinQuotaScenario, withinQuota2: withinQuotaScenario2, exceedQuota: exceedQuota } = generateFeeRuleInfoScenarioParams(walletQuota, paramsFeeRuleTemplate, paramsMappedEntitlementTemplate);
    return {
        withinQuota: () => {
            return generateFeeRuleInfoScenarioOutput(withinQuotaScenario.paramsFeeRule(), withinQuotaScenario.paramsMappedEntitlement());
        },
        withinQuota2: () => {
            return generateFeeRuleInfoScenarioOutput(withinQuotaScenario2.paramsFeeRule(), withinQuotaScenario2.paramsMappedEntitlement());
        },
        exceedQuota: () => {
            return generateFeeRuleInfoScenarioOutput(exceedQuota.paramsFeeRule(), exceedQuota.paramsMappedEntitlement());
        }
    };
};
/************************
 * BIFAST
 ************************/
exports.mockBIFastScenario = (walletQuota) => {
    const paramsFeeRuleTemplate = {
        code: fee_constant_1.FeeRuleType.BIFAST_FEE_RULE,
        transactionAmount: 50000,
        monthlyNoTransaction: 0,
        interchange: module_common_1.BankNetworkEnum.IRIS,
        counterCode: fee_constant_1.COUNTER.COUNTER_01
    };
    const paramsMappedEntitlementTemplate = {
        awardGroupCounter: entitlement_enum_1.AwardLimitGroupCodes.BONUS_TRANSFER,
        usageCounters: [
            //usage counters should be already mapped with result from entitlement at this point
            {
                limitGroupCode: 'L002',
                dailyAccumulationAmount: 50000,
                monthlyAccumulationTransaction: 0,
                quota: 0,
                isQuotaExceeded: false
            },
            {
                limitGroupCode: entitlement_enum_1.AwardLimitGroupCodes.BONUS_TRANSFER,
                dailyAccumulationAmount: 50000,
                monthlyAccumulationTransaction: 0,
                quota: 0,
                isQuotaExceeded: false
            }
        ]
    };
    const { withinQuota: withinQuotaScenario, withinQuota2: withinQuotaScenario2, exceedQuota: exceedQuota } = generateFeeRuleInfoScenarioParams(walletQuota, paramsFeeRuleTemplate, paramsMappedEntitlementTemplate);
    return {
        withinQuota: () => {
            return generateFeeRuleInfoScenarioOutput(withinQuotaScenario.paramsFeeRule(), withinQuotaScenario.paramsMappedEntitlement());
        },
        withinQuota2: () => {
            return generateFeeRuleInfoScenarioOutput(withinQuotaScenario2.paramsFeeRule(), withinQuotaScenario2.paramsMappedEntitlement());
        },
        exceedQuota: () => {
            return generateFeeRuleInfoScenarioOutput(exceedQuota.paramsFeeRule(), exceedQuota.paramsMappedEntitlement());
        }
    };
};
//# sourceMappingURL=entitlement.handler.mock.data.js.map