"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = __importDefault(require("uuid"));
const entitlement_enum_mock_1 = require("./entitlement.enum.mock");
//happy scenario
//quota is available
//and request for usedQuota is valid (within boundary and > 0)
exports.scenario1 = (customerId) => {
    //assumption
    const existingState = [
        {
            entitlementCode: entitlement_enum_mock_1.MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
            counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
            quota: 25,
            usedQuota: 0
        }
    ];
    const usedUuid = uuid_1.default.v4();
    //request data
    const requestData = {
        customerId: customerId,
        entitlementCodes: [entitlement_enum_mock_1.MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL],
        counterUsages: [
            {
                id: usedUuid,
                counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
                usedQuota: 1
            }
        ]
    };
    const responseData = {
        entitlements: [
            {
                entitlement: entitlement_enum_mock_1.MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
                type: 'MONTHLY_COUNTER',
                dataType: 'integer',
                description: 'sample description',
                quota: 25 - 1,
                comments: 'sample comments',
                usedQuota: 0 + 1,
                counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL
            }
        ],
        counters: [
            {
                id: usedUuid,
                recorded: true
            }
        ]
    };
    return {
        existingState,
        requestData,
        responseData
    };
};
//scenario 2
//replicated input, in which the id is the same
exports.scenario2 = (customerId) => {
    //assumption
    const existingState = [
        {
            entitlementCode: entitlement_enum_mock_1.MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
            counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
            quota: 25,
            usedQuota: 0
        }
    ];
    const duplicatedId = uuid_1.default.v4();
    //request data
    const requestData = {
        customerId: customerId,
        entitlementCodes: [entitlement_enum_mock_1.MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL],
        counterUsages: [
            {
                id: duplicatedId,
                counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
                usedQuota: 1
            },
            {
                id: duplicatedId,
                counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
                usedQuota: 2
            }
        ]
    };
    const responseData = {
        entitlements: [
            {
                entitlement: entitlement_enum_mock_1.MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
                type: 'MONTHLY_COUNTER',
                dataType: 'integer',
                description: 'sample description',
                quota: 25 - 1,
                comments: 'sample comments',
                usedQuota: 0 + 1,
                counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL
            }
        ],
        counters: [
            {
                id: duplicatedId,
                recorded: true
            },
            {
                id: duplicatedId,
                recorded: false,
                reason: {
                    type: 'IdNotUnique',
                    description: 'counter usage id is not unique and has been recorded previously'
                }
            }
        ]
    };
    return {
        existingState,
        requestData,
        responseData
    };
};
//negative scenario
//quota is available
//request to usedQuota is bigger than existing quota
//expected output, failed, in which there is insufficient quota
exports.scenario3 = (customerId) => {
    //assumption
    const existingState = [
        {
            entitlementCode: entitlement_enum_mock_1.MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
            counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
            quota: 25,
            usedQuota: 0
        }
    ];
    const uniqueId = uuid_1.default.v4();
    //request data
    const requestData = {
        customerId: customerId,
        entitlementCodes: [entitlement_enum_mock_1.MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL],
        counterUsages: [
            {
                id: uniqueId,
                counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
                usedQuota: 26
            }
        ]
    };
    const responseData = {
        entitlements: [
            {
                entitlement: entitlement_enum_mock_1.MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
                type: 'MONTHLY_COUNTER',
                dataType: 'integer',
                description: 'sample description',
                quota: 25,
                comments: 'sample comments',
                usedQuota: 0,
                counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL
            }
        ],
        counters: [
            {
                id: uniqueId,
                recorded: false,
                reason: {
                    type: 'insufficientQuota',
                    description: 'the requested user quota exceeded the existing quota'
                }
            }
        ]
    };
    return {
        existingState,
        requestData,
        responseData
    };
};
//negative scenario
//quota is not available
exports.scenario4 = (customerId) => {
    //assumption
    const existingState = [
        {
            entitlementCode: entitlement_enum_mock_1.MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
            counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
            quota: 0,
            usedQuota: 25
        }
    ];
    const uniqueId = uuid_1.default.v4();
    //request data
    const requestData = {
        customerId: customerId,
        entitlementCodes: [entitlement_enum_mock_1.MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL],
        counterUsages: [
            {
                id: uniqueId,
                counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
                usedQuota: 1
            }
        ]
    };
    const responseData = {
        entitlements: [
            {
                entitlement: entitlement_enum_mock_1.MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
                type: 'MONTHLY_COUNTER',
                dataType: 'integer',
                description: 'sample description',
                quota: 0,
                comments: 'sample comments',
                usedQuota: 25,
                counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL
            }
        ],
        counters: [
            {
                id: uniqueId,
                recorded: false,
                reason: {
                    type: 'zeroBalancedQuota',
                    description: 'there is no more quota available'
                }
            }
        ]
    };
    return {
        existingState,
        requestData,
        responseData
    };
};
//negative scenario
//usedQuota is negative value
exports.scenario5 = (customerId) => {
    //assumption
    const existingState = [
        {
            entitlementCode: entitlement_enum_mock_1.MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
            counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
            quota: 25,
            usedQuota: 0
        }
    ];
    const uniqueId = uuid_1.default.v4();
    //request data
    const requestData = {
        customerId: customerId,
        entitlementCodes: [entitlement_enum_mock_1.MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL],
        counterUsages: [
            {
                id: uniqueId,
                counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
                usedQuota: -1
            }
        ]
    };
    const responseData = {
        entitlements: [
            {
                entitlement: entitlement_enum_mock_1.MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
                type: 'MONTHLY_COUNTER',
                dataType: 'integer',
                description: 'sample description',
                quota: 25,
                comments: 'sample comments',
                usedQuota: 0,
                counterCode: entitlement_enum_mock_1.MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL
            }
        ],
        counters: [
            {
                id: uniqueId,
                recorded: false,
                reason: {
                    type: 'invalidUsedQuota',
                    description: 'used quota need to be positive number'
                }
            }
        ]
    };
    return {
        existingState,
        requestData,
        responseData
    };
};
//# sourceMappingURL=atm.withdrawal.consume.data.js.map