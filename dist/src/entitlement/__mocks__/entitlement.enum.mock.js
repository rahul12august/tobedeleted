"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MockEntitlementCode;
(function (MockEntitlementCode) {
    MockEntitlementCode["BONUS_TRANSFER"] = "count.free.transfer.out";
    MockEntitlementCode["BONUS_LOCAL_CASH_WITHDRAWAL"] = "count.free.atm_withdrawal";
})(MockEntitlementCode = exports.MockEntitlementCode || (exports.MockEntitlementCode = {}));
var MockEntitlementCounterCode;
(function (MockEntitlementCounterCode) {
    MockEntitlementCounterCode["COUNTER_BONUS_TRANSFER"] = "counter.free.transfer.out";
    MockEntitlementCounterCode["COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL"] = "counter.free.atm_withdrawal";
})(MockEntitlementCounterCode = exports.MockEntitlementCounterCode || (exports.MockEntitlementCounterCode = {}));
//# sourceMappingURL=entitlement.enum.mock.js.map