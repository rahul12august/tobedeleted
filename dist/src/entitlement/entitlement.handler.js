"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
const errors_1 = require("../common/errors");
const AppError_1 = require("../errors/AppError");
const logger_1 = __importDefault(require("../logger"));
const transaction_enum_1 = require("../transaction/transaction.enum");
const entitlement_enum_1 = require("./entitlement.enum");
const entitlement_helper_1 = __importDefault(require("./entitlement.helper"));
const entitlement_service_1 = __importDefault(require("./entitlement.service"));
class EntitlementHandler {
    constructor() {
        this.extractAvailableLimitGroupCodesFromTransactionCodeInfo = (transactionCodes) => {
            const filteredLimitGroupCodes = transactionCodes.filter(tc => !lodash_1.default.isEmpty(tc.transactionCodeInfo.limitGroupCode) &&
                !lodash_1.default.isUndefined(tc.transactionCodeInfo.limitGroupCode));
            const tcValidLimitGroupCodes = new Set();
            for (const tc of filteredLimitGroupCodes) {
                const { limitGroupCode, awardGroupCounter } = tc.transactionCodeInfo;
                if (!lodash_1.default.isUndefined(limitGroupCode) && !lodash_1.default.isEmpty(limitGroupCode)) {
                    tcValidLimitGroupCodes.add(limitGroupCode);
                }
                if (!lodash_1.default.isUndefined(awardGroupCounter) && !lodash_1.default.isEmpty(awardGroupCounter)) {
                    tcValidLimitGroupCodes.add(awardGroupCounter);
                }
            }
            return tcValidLimitGroupCodes;
        };
        this.validateIsExceededQuota = (counterResponse) => {
            if (!lodash_1.default.isEmpty(counterResponse)) {
                logger_1.default.info(`[EntitlementHandler] validateIsExceededQuota: 
        recorded: ${counterResponse[0].recorded}
        reason: ${counterResponse[0].reason}
      `);
                //assume we are only dealing with one response at a time
                const { recorded, reason } = counterResponse[0];
                if (recorded) {
                    //successfully updated
                    return false;
                }
                else {
                    return !!(reason && reason.type === entitlement_enum_1.EntitlementErrorReasonType.QUOTA_LIMIT);
                }
            }
            else {
                throw new AppError_1.TransferAppError(`Failed to retrieve entitlement quota info!`, transaction_enum_1.TransferFailureReasonActor.MS_ENTITLEMENT, errors_1.ERROR_CODE.FAILED_TO_RETRIEVE_ENTITLEMENT_QUOTA_INFO);
            }
        };
    }
    generateMissingUsageCounterBasedOnTransactionCodes(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const { transactionCodes, usageCounters, entitlementResultResponse } = params;
            if (lodash_1.default.isEmpty(transactionCodes) ||
                lodash_1.default.isUndefined(transactionCodes) ||
                lodash_1.default.isEmpty(entitlementResultResponse) ||
                lodash_1.default.isUndefined(entitlementResultResponse) ||
                entitlementResultResponse == null) {
                logger_1.default.info(`[EntitlementHandler] generateMissingUsageCounterBasedOnTransactionCodes result1 [as-is]: ${JSON.stringify(usageCounters)}`);
                //return as is
                return usageCounters;
            }
            //first extract all limitGroupCodes from the transactionCodes
            const tcValidLimitGroupCodes = this.extractAvailableLimitGroupCodesFromTransactionCodeInfo(transactionCodes);
            if (lodash_1.default.isEmpty(tcValidLimitGroupCodes)) {
                logger_1.default.info(`[EntitlementHandler] generateMissingUsageCounterBasedOnTransactionCodes result2 [as-is]: ${JSON.stringify(usageCounters)}`);
                //return as is
                return usageCounters;
            }
            //extract limitGroupCodes from existing usageCounters
            const existingUsageCountersLimitGroupCodes = usageCounters.map(usageCounter => usageCounter.limitGroupCode);
            //getting the difference
            const actualMissingLimitGroupCodes = lodash_1.default.difference([...tcValidLimitGroupCodes], existingUsageCountersLimitGroupCodes);
            //from the entitlement result, cross check which one is the missing usgecounter
            const entitlementMap = entitlementResultResponse.updatedEntitlementMapResponse;
            for (const limitGroupCode of actualMissingLimitGroupCodes) {
                const limitGroupCodeEntitlementResult = entitlementMap.get(limitGroupCode);
                // when entitlementMap does not contain result for missing limit group code
                if (lodash_1.default.isUndefined(limitGroupCodeEntitlementResult)) {
                    throw new AppError_1.TransferAppError(`Limit group code: ${limitGroupCode} not found in the entitlement!`, transaction_enum_1.TransferFailureReasonActor.MS_ENTITLEMENT, errors_1.ERROR_CODE.LIMIT_GROUP_CODE_NOT_FOUND_IN_ENTITLEMENT);
                }
                const { usedQuota, quota } = limitGroupCodeEntitlementResult.entitlements[0];
                const isQuotaExceeded = this.validateIsExceededQuota(limitGroupCodeEntitlementResult.counters);
                //create usageCounter object
                const newUsageCounter = {
                    limitGroupCode: limitGroupCode,
                    dailyAccumulationAmount: 0,
                    monthlyAccumulationTransaction: usedQuota,
                    quota: quota,
                    isQuotaExceeded: isQuotaExceeded
                };
                usageCounters.push(newUsageCounter);
            }
            logger_1.default.info(`[EntitlementHandler] generateMissingUsageCounterBasedOnTransactionCodes result: ${JSON.stringify(usageCounters)}`);
            return usageCounters;
        });
    }
    preProcessCustomerEntitlementBasedOnTransactionCodes(params) {
        return __awaiter(this, void 0, void 0, function* () {
            let mode = transaction_enum_1.CustomerEntitlementMode.CONSUME;
            if (lodash_1.default.isEmpty(params.transactionId)) {
                mode = transaction_enum_1.CustomerEntitlementMode.PREVIEW;
            }
            logger_1.default.info(`[EntitlementHandler] preProcessCustomerEntitlementBasedOnTransactionCodes ${mode} MODE: 
      ${JSON.stringify(params)}
      `);
            switch (mode) {
                case transaction_enum_1.CustomerEntitlementMode.PREVIEW:
                    return entitlement_service_1.default.retrieveCustomerEntitlementRecommendedServicePreview(params);
                case transaction_enum_1.CustomerEntitlementMode.CONSUME:
                    return entitlement_service_1.default.processCustomerEntitlementConsumption(params);
                default:
                    return null;
            }
        });
    }
    mapEntitlementUsageCounterByLimitGroupCode(usageCounters, entitlementResultResponse) {
        logger_1.default.info(`[EntitlementHandler] mapEntitlementUsageCounterByLimitGroupCode: 
        usageCounters ${JSON.stringify(usageCounters)}
        entitlementResultResponse ${JSON.stringify(entitlementResultResponse)}`);
        if (entitlementResultResponse != null) {
            const entitlementMap = entitlementResultResponse.updatedEntitlementMapResponse;
            const mappedLimitGroupCodes = [];
            //reuse from the existing data and then mapped to usagecounter data accordingly
            usageCounters.map(usageCounter => {
                //usageCounter limitGroupCode here will also include awardGroupCounter
                if (entitlementMap.has(usageCounter.limitGroupCode)) {
                    mappedLimitGroupCodes.push(usageCounter.limitGroupCode);
                    const entitlementResult = entitlementMap.get(usageCounter.limitGroupCode);
                    if (entitlementResult &&
                        entitlementResult.entitlements &&
                        entitlementResult.entitlements[0]) {
                        logger_1.default.info(`[EntitlementHandler] mapEntitlementUsageCounterByLimitGroupCode: MAPPING usageCounters: ${usageCounter.limitGroupCode}`);
                        //only get the first one as we are dealing with only one entitlement code at a time
                        const { usedQuota, quota } = entitlementResult.entitlements[0];
                        const isQuotaExceeded = this.validateIsExceededQuota(entitlementResult.counters);
                        usageCounter.monthlyAccumulationTransaction = usedQuota;
                        usageCounter.quota = parseInt(quota.toString());
                        usageCounter.isQuotaExceeded = isQuotaExceeded;
                        logger_1.default.info(`[EntitlementHandler] mapEntitlementUsageCounterByLimitGroupCode: MAPPED usageCounters:
                limitGroupCode: [${usageCounter.limitGroupCode}]
                dailyAccumulationAmount: [${usageCounter.dailyAccumulationAmount}]
                monthlyAccumulationTransaction: [${usageCounter.monthlyAccumulationTransaction}]
                quota: [${usageCounter.quota}]
                isQuotaExceeded: [${usageCounter.isQuotaExceeded}]
              `);
                    }
                }
            });
            logger_1.default.info(`[EntitlementHandler] mapEntitlementUsageCounterByLimitGroupCode: usageCounters ${JSON.stringify(usageCounters)}`);
        }
        return usageCounters;
    }
    validateFeeRuleObject(feeRuleObject) {
        logger_1.default.info(`[EntitlementHandler] validateFeeRuleObject: 
      monthlyNoTransaction: ${feeRuleObject.monthlyNoTransaction}
      quota: ${feeRuleObject.quota}
      isQuotaExceeded: ${feeRuleObject.isQuotaExceeded}
    `);
        if (typeof feeRuleObject.monthlyNoTransaction !== 'number' ||
            typeof feeRuleObject.quota !== 'number' ||
            lodash_1.default.isUndefined(feeRuleObject.isQuotaExceeded)) {
            const detail = `monthlyNoTransaction or counter code not found!`;
            logger_1.default.error(`[EntitlementHandler] validateFeeRuleObject: ${detail}`);
            throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.MS_ENTITLEMENT, errors_1.ERROR_CODE.TRANSFER_FEE_RULE_INVALID_MANDATORY_FIELDS);
        }
        logger_1.default.info(`[EntitlementHandler] getFeeRuleBasedOnMonthlyNoTransaction: 
        Counter code: [${feeRuleObject.counterCode}]
        monthlyNoTransaction: [${feeRuleObject.monthlyNoTransaction}]
        quota limit exceeded: [${feeRuleObject.isQuotaExceeded}]
      `);
        //in most cases
        //basicFeeCode1 is for free transaction
        //basicFeeCode2 is for paid transaction
        return {
            usedQuota: feeRuleObject.monthlyNoTransaction,
            quota: feeRuleObject.quota,
            freeFeeCode: feeRuleObject.basicFeeCode1,
            paidFeeCode: feeRuleObject.basicFeeCode2
        };
    }
    determineBasicFeeMapping(feeRuleObject) {
        return __awaiter(this, void 0, void 0, function* () {
            const { usedQuota, quota, freeFeeCode, paidFeeCode } = yield this.validateFeeRuleObject(feeRuleObject);
            logger_1.default.info(`[EntitlementHandler] determineBasicFeeMapping: 
      quota: [${quota}]
      usedQuota: [${usedQuota}]
      isQuotaExceeded: [${feeRuleObject.isQuotaExceeded}]
    `);
            if (usedQuota === quota) {
                //if usedQuota == quota, check by the flag whether the quota is exceeded or not
                return feeRuleObject.isQuotaExceeded ? paidFeeCode : freeFeeCode;
            }
            else if (usedQuota < quota) {
                //if usedQuota < quota
                return freeFeeCode;
            }
            else {
                return paidFeeCode;
            }
        });
    }
    processFeeRuleInfo(currentFeeRuleInfo, feeRuleType, awardGroupCounter, usageCounters) {
        if (lodash_1.default.isUndefined(feeRuleType) ||
            lodash_1.default.isUndefined(awardGroupCounter) ||
            lodash_1.default.isUndefined(usageCounters)) {
            throw new AppError_1.TransferAppError(`One of following is undefined: feeRuleType, awardGroupCounter, usageCounters!`, transaction_enum_1.TransferFailureReasonActor.MS_ENTITLEMENT, errors_1.ERROR_CODE.PROCESS_FEE_RULE_INFO_ERROR);
        }
        logger_1.default.info(`[EntitlementHandler] processFeeRuleInfo: 
      awardGroupCounter: [${awardGroupCounter}]
      feeRuleType: [${feeRuleType}]
      usageCounters: [${JSON.stringify(usageCounters)}]
    `);
        const isIncludedWithinFilteredLimitGroupCodes = entitlement_helper_1.default
            .getFilteredLimitGroupCodes()
            .includes(awardGroupCounter);
        const isIncludedWithinFilteredFeeRuleTypes = entitlement_helper_1.default
            .getFilteredFeeRuleType()
            .includes(feeRuleType);
        const toModifyEntitlementValues = isIncludedWithinFilteredLimitGroupCodes &&
            isIncludedWithinFilteredFeeRuleTypes;
        logger_1.default.info(`[EntitlementHandler] processFeeRuleInfo: 
        awardGroupCounter: [${awardGroupCounter}]
        feeRuleType: [${feeRuleType}]
        isIncludedWithinFilteredLimitGroupCodes: [${isIncludedWithinFilteredLimitGroupCodes}]
        isIncludedWithinFilteredFeeRuleTypes: [${isIncludedWithinFilteredFeeRuleTypes}]
        toModifyEntitlementValues: [${toModifyEntitlementValues}]
      `);
        if (toModifyEntitlementValues) {
            const modifiedFeeRuleInfo = Object.assign({}, currentFeeRuleInfo);
            const usageCounter = entitlement_service_1.default.getEntitlementByAwardGroupCounter(usageCounters, awardGroupCounter);
            const { monthlyAccumulationTransaction, quota, isQuotaExceeded } = usageCounter;
            //overwrite the monthlynotransaction with used quota
            modifiedFeeRuleInfo.monthlyNoTransaction = monthlyAccumulationTransaction;
            //added quota
            modifiedFeeRuleInfo.quota = quota;
            //added flag indicating whether quota exceeded or not
            modifiedFeeRuleInfo.isQuotaExceeded = (isQuotaExceeded !== null && isQuotaExceeded !== void 0 ? isQuotaExceeded : false);
            logger_1.default.info(`[EntitlementHandler] processFeeRuleInfo result1: 
          monthlyNoTransaction/usedQuota: [${modifiedFeeRuleInfo.monthlyNoTransaction}] 
          quota: [${modifiedFeeRuleInfo.quota}] 
          isQuotaExceeded [${modifiedFeeRuleInfo.isQuotaExceeded}] 
        `);
            return modifiedFeeRuleInfo;
        }
        logger_1.default.info(`[EntitlementHandler] processFeeRuleInfo result2: 
          monthlyNoTransaction/usedQuota: [${currentFeeRuleInfo.monthlyNoTransaction}] 
          quota: [${currentFeeRuleInfo.quota}] 
          isQuotaExceeded [${currentFeeRuleInfo.isQuotaExceeded}] 
        `);
        return currentFeeRuleInfo;
    }
}
exports.EntitlementHandler = EntitlementHandler;
//# sourceMappingURL=entitlement.handler.js.map