"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const collectionName = 'outgoing_counters';
const OutgoingCounterSchema = new mongoose_1.Schema({
    prefix: {
        type: String,
        required: true,
        unique: true,
        index: true
    },
    counter: {
        // base 1 because we use $inc and upsert
        type: Number,
        required: true
    }
});
exports.OutgoingCounterModel = mongoose_1.default.model(collectionName, OutgoingCounterSchema);
//# sourceMappingURL=outgoingCounter.model.js.map