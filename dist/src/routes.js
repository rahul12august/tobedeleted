"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const healthcheck_controller_1 = __importDefault(require("./healthcheck/healthcheck.controller"));
const fee_controller_1 = __importDefault(require("./fee/fee.controller"));
const transaction_controller_1 = __importDefault(require("./transaction/transaction.controller"));
const billPayment_controller_1 = __importDefault(require("./billPayment/billPayment.controller"));
const iris_controller_1 = __importDefault(require("./iris/iris.controller"));
const mambuBlockingConfig_controller_1 = __importDefault(require("./mambuBlockingConfig/mambuBlockingConfig.controller"));
const tokopedia_controller_1 = __importDefault(require("./tokopedia/tokopedia.controller"));
const alto_controller_1 = __importDefault(require("./alto/alto.controller"));
const bifast_controller_1 = __importDefault(require("./bifast/bifast.controller"));
const transactionUsage_controller_1 = __importDefault(require("./transactionUsage/transactionUsage.controller"));
const routes = [
    ...healthcheck_controller_1.default,
    ...fee_controller_1.default,
    ...transaction_controller_1.default,
    ...billPayment_controller_1.default,
    ...iris_controller_1.default,
    ...mambuBlockingConfig_controller_1.default,
    ...tokopedia_controller_1.default,
    ...alto_controller_1.default,
    ...bifast_controller_1.default,
    ...transactionUsage_controller_1.default
];
exports.routes = routes;
//# sourceMappingURL=routes.js.map