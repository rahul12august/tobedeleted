"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.lfsTemplate = {
    limitGroupCodes: (paymentServiceCode) => ({
        RTOL_LIMIT_GROUP_CODE: 'L002',
        SKN_LIMIT_GROUP_CODE: 'L003',
        RTGS_LIMIT_GROUP_CODE: paymentServiceCode && paymentServiceCode === 'RDN_KSEI' ? 'L017' : 'L004'
    }),
    isEligible: (paymentServiceCode) => paymentServiceCode === 'RTOL' ||
        paymentServiceCode === 'SKN' ||
        paymentServiceCode === 'RTGS' ||
        paymentServiceCode === 'RDN_KSEI' ||
        paymentServiceCode === 'RDN_WITHDRAW_RTGS' ||
        paymentServiceCode === 'RDN_WITHDRAW_SKN'
};
exports.bfsTemplate = {
    limitGroupCodes: () => ({
        RTOL_LIMIT_GROUP_CODE: 'L013',
        SKN_LIMIT_GROUP_CODE: 'L014',
        RTGS_LIMIT_GROUP_CODE: 'L012'
    }),
    isEligible: (paymentServiceCode) => paymentServiceCode === 'RTOL_FOR_BUSINESS' ||
        paymentServiceCode === 'SKN_FOR_BUSINESS' ||
        paymentServiceCode === 'RTGS_FOR_BUSINESS'
};
exports.btsTemplate = {
    limitGroupCodes: () => ({
        RTOL_LIMIT_GROUP_CODE: 'L002',
        SKN_LIMIT_GROUP_CODE: 'L003',
        RTGS_LIMIT_GROUP_CODE: 'L004'
    }),
    isEligible: (paymentServiceCode) => paymentServiceCode === 'BRANCH_RTGS' || paymentServiceCode === 'BRANCH_SKN'
};
exports.shariaTemplate = {
    limitGroupCodes: () => ({
        RTOL_LIMIT_GROUP_CODE: 'L002',
        SKN_LIMIT_GROUP_CODE: 'L003',
        RTGS_LIMIT_GROUP_CODE: 'L004'
    }),
    isEligible: (paymentServiceCode) => paymentServiceCode === 'SKN_SHARIA' || paymentServiceCode === 'RTGS_SHARIA'
};
//# sourceMappingURL=templates.js.map