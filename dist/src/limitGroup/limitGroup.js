"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const templates_1 = require("./templates");
const templates = [templates_1.lfsTemplate, templates_1.bfsTemplate, templates_1.btsTemplate, templates_1.shariaTemplate];
const getLimitGroupCodes = (paymentServiceCode) => {
    if (paymentServiceCode) {
        const template = templates.find(template => template.isEligible(paymentServiceCode));
        if (template) {
            logger_1.default.info(`getLimitGroupCodes: limit group codes template found for paymentServiceCode:${paymentServiceCode}`);
            return template.limitGroupCodes(paymentServiceCode);
        }
    }
    logger_1.default.info(`getLimitGroupCodes: template not found, returning default LFS limit group codes`);
    return templates_1.lfsTemplate.limitGroupCodes(paymentServiceCode);
};
exports.default = logger_1.wrapLogs({ getLimitGroupCodes });
//# sourceMappingURL=limitGroup.js.map