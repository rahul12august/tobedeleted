"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const faker_1 = __importDefault(require("faker"));
const transaction_enum_1 = require("../../transaction/transaction.enum");
exports.getTransactionModelData = () => {
    let fees = [
        {
            feeCode: faker_1.default.random.alphaNumeric(4),
            feeAmount: faker_1.default.random.number({ min: 1, max: 100 }),
            customerTc: faker_1.default.random.alphaNumeric(4),
            customerTcChannel: faker_1.default.random.alphaNumeric(4)
        }
    ];
    let journey = [
        {
            status: transaction_enum_1.TransferJourneyStatusEnum.REQUEST_RECEIVED,
            updatedAt: new Date()
        },
        {
            status: transaction_enum_1.TransferJourneyStatusEnum.INQUIRY_STARTED,
            updatedAt: new Date()
        }
    ];
    const processingInfo = {
        paymentRail: module_common_1.Rail.EURONET.toString()
    };
    return {
        id: faker_1.default.random.alphaNumeric(24),
        beneficiaryAccountType: faker_1.default.random.alphaNumeric(4),
        referenceId: faker_1.default.random.alphaNumeric(20),
        externalId: faker_1.default.random.alphaNumeric(20),
        status: transaction_enum_1.TransactionStatus.SUCCEED,
        sourceBankCodeChannel: module_common_1.BankChannelEnum.EURONET,
        categoryCode: faker_1.default.random.alphaNumeric(10),
        transactionAmount: faker_1.default.random.number({ min: 1, max: 100000 }),
        fees: fees,
        paymentRequestID: faker_1.default.random.alphaNumeric(20),
        paymentServiceCode: faker_1.default.random.alphaNumeric(4),
        paymentServiceType: module_common_1.PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT,
        requireThirdPartyOutgoingId: true,
        journey: journey,
        processingInfo: processingInfo,
        executionType: transaction_enum_1.ExecutionTypeEnum.BLOCKING,
        createdAt: new Date(),
        updatedAt: new Date()
    };
};
//# sourceMappingURL=monitoringEvent.data.js.map