"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const kafkaProducer_1 = __importDefault(require("../common/kafkaProducer"));
const kafka_util_1 = __importDefault(require("../common/kafka.util"));
const module_message_1 = require("@dk/module-message");
const contextHandler_1 = require("../common/contextHandler");
const constant_1 = require("../common/constant");
const transaction_helper_1 = __importDefault(require("../transaction/transaction.helper"));
const moment_1 = __importDefault(require("moment"));
const config_1 = require("../config");
const getJourney = (model) => {
    const journey = model.journey &&
        model.journey.map(data => {
            return {
                status: data.status,
                updatedAt: data.updatedAt
            };
        });
    return journey;
};
const toMonitoringEventMessage = (eventType, model) => {
    const eventPayload = {
        requestId: contextHandler_1.getRequestId(),
        beneficiaryAccountType: model.beneficiaryAccountType,
        referenceId: model.referenceId,
        externalId: model.externalId,
        status: model.status,
        sourceBankCodeChannel: model.sourceBankCodeChannel,
        customerId: transaction_helper_1.default.getCustomerIdByModel(model),
        categoryCode: model.categoryCode,
        transactionAmount: model.transactionAmount,
        fees: model.fees,
        paymentRequestID: model.paymentRequestID,
        paymentServiceCode: model.paymentServiceCode,
        processingInfo: model.processingInfo,
        kind: model.executionType,
        journey: getJourney(model),
        failureReason: model.failureReason,
        createdAt: model.createdAt || new Date(),
        updatedAt: model.updatedAt || new Date()
    };
    const monitoringEventMessage = {
        entityId: model.id,
        entityType: module_message_1.MonitoringEntityType.TRANSACTION,
        eventType: eventType,
        timestampMillis: moment_1.default.now(),
        source: constant_1.MICROSERVICE_NAME,
        payload: eventPayload
    };
    return monitoringEventMessage;
};
const sendMonitoringEventMessage = (eventType, transaction) => __awaiter(void 0, void 0, void 0, function* () {
    // Proceed only if monitoring is enabled for environment
    if (!config_1.config.get('monitoring').isEnabled) {
        return;
    }
    const monitoringEventMessage = toMonitoringEventMessage(eventType, transaction);
    logger_1.default.info(`sendMonitoringEventMessage : sending monitoring event message
      entityId: ${monitoringEventMessage.entityId}
      entityType: ${monitoringEventMessage.entityType}
      eventType: ${monitoringEventMessage.eventType}`);
    yield kafkaProducer_1.default.sendSerializedValue({
        topic: module_message_1.EventMonitoringTopicConstant.EVENT_MONITORING,
        messages: [
            {
                value: monitoringEventMessage,
                headers: kafka_util_1.default.getHeaders()
            }
        ]
    });
});
exports.default = logger_1.wrapLogs({
    sendMonitoringEventMessage
});
//# sourceMappingURL=monitoringEvent.producer.js.map