"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const switching_enum_1 = require("../switching.enum");
exports.getMockedTransaction = () => ({
    inDeliveryChannel: switching_enum_1.inDeliveryChannelEnum.DEFAULT,
    inAuditData: {
        inApplication: 'artos',
        inUserId: 'artos-user',
        inPassword: 'artos-password',
        inServiceID: 'string;',
        inSequence: '348384989'
    },
    inProcessingCode: switching_enum_1.transactionProcessingCodeEnum.FUND_TRANSFER,
    inTxnAmt: '250000',
    inTxnDateTime: '301090000',
    inSTAN: '123456',
    inAcqInstId: '542',
    inRRN: '12345690877',
    inAddData: `FROM JAGO ACCOUNT NAME`,
    inCurrCode: 0,
    inAcctId1: '9993883883',
    inAcctId2: '9083887477',
    inDestInstId: '542',
    inSysOrg: switching_enum_1.inSysOrgEnum.DEFAULT,
    inTermID: switching_enum_1.inTermIdEnum.DEFAULT,
    inTermLoc: switching_enum_1.inTermLocEnum.DEFAULT
});
exports.getMockedSwitchingResponse = () => ({
    outRespCode: '00',
    outRespMsg: 'success',
    outAddData: '201174000003',
    outSequence: 'FROMVan Thi1'
});
//# sourceMappingURL=switchingTransaction.data.js.map