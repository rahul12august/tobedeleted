"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importStar(require("../logger"));
const httpClient_1 = __importDefault(require("./httpClient"));
const config_1 = require("../config");
const lodash_1 = require("lodash");
const AppError_1 = require("../errors/AppError");
const errors_1 = require("../common/errors");
const circuitBreaker_1 = __importDefault(require("./circuitBreaker"));
const contextHandler_1 = require("../common/contextHandler");
const transaction_enum_1 = require("../transaction/transaction.enum");
const SWITCHING_SUCCESS_RESPONSE_CODE = '00';
const switchingEuronetConfig = config_1.config.get('switching').euronet;
const doSubmitTransaction = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    contextHandler_1.updateRequestIdInRedis(transaction.inRRN);
    logger_1.default.info(`Submitting Euronet transaction with RRN : ${transaction.inRRN}, processingCode : ${transaction.inProcessingCode},
      sequence : ${transaction.inAuditData.inSequence}, 
      inAcqInstId: ${transaction.inAcqInstId},
      inDestInstId: ${transaction.inDestInstId},
      inAcctId1: ${transaction.inAcctId1},
      inAcctId2: ${transaction.inAcctId2},
      inAddData: ${transaction.inAddData}`);
    const response = yield httpClient_1.default.post(switchingEuronetConfig.transactionUrl, transaction);
    const data = lodash_1.get(response, ['data'], {});
    if (data.outRespCode !== SWITCHING_SUCCESS_RESPONSE_CODE) {
        const detail = `Switching Euronet has returned error response with RRN: ${transaction.inRRN}`;
        logger_1.default.error(`${detail}, response: ${JSON.stringify(data)}`);
        throw new AppError_1.TransferAppError(detail, transaction_enum_1.TransferFailureReasonActor.EURONET, errors_1.ERROR_CODE.ERROR_FROM_SWICHING);
    }
    logger_1.default.info(`Submit Euronet transaction response : ${JSON.stringify(data)}`);
    return data;
});
const submitTransaction = circuitBreaker_1.default.create(doSubmitTransaction);
const switchingRepository = {
    submitTransaction
};
exports.default = logger_1.wrapLogs(switchingRepository);
//# sourceMappingURL=switching.repository.js.map