"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var inDeliveryChannelEnum;
(function (inDeliveryChannelEnum) {
    inDeliveryChannelEnum["DEFAULT"] = "02";
})(inDeliveryChannelEnum = exports.inDeliveryChannelEnum || (exports.inDeliveryChannelEnum = {}));
var transactionProcessingCodeEnum;
(function (transactionProcessingCodeEnum) {
    transactionProcessingCodeEnum["FUND_TRANSFER"] = "401000";
})(transactionProcessingCodeEnum = exports.transactionProcessingCodeEnum || (exports.transactionProcessingCodeEnum = {}));
var inSysOrgEnum;
(function (inSysOrgEnum) {
    inSysOrgEnum["DEFAULT"] = "MS ARTOS";
})(inSysOrgEnum = exports.inSysOrgEnum || (exports.inSysOrgEnum = {}));
var inTermIdEnum;
(function (inTermIdEnum) {
    inTermIdEnum["DEFAULT"] = "JG8888";
})(inTermIdEnum = exports.inTermIdEnum || (exports.inTermIdEnum = {}));
var inTermLocEnum;
(function (inTermLocEnum) {
    inTermLocEnum["DEFAULT"] = "ARTOS DIGITAL BANK  HQ";
})(inTermLocEnum = exports.inTermLocEnum || (exports.inTermLocEnum = {}));
var inServiceIdEnum;
(function (inServiceIdEnum) {
    inServiceIdEnum["FUND_TRANSFER"] = "FundTrnsfr";
})(inServiceIdEnum = exports.inServiceIdEnum || (exports.inServiceIdEnum = {}));
//# sourceMappingURL=switching.enum.js.map