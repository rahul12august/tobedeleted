"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const config_1 = require("../config");
const logger_1 = __importDefault(require("../logger"));
const ueronetTimeout = config_1.config.get('switching').euronet.timeout;
exports.default = module_common_1.Util.createCircuitBreakerFactory({
    timeout: ueronetTimeout,
    resetTimeout: ueronetTimeout,
    name: 'euronet-switching',
    // When >50% requests in a rolling window failing, open the circuit.
    errorThresholdPercentage: 50,
    // When number of requests in a rolling window is <50, don't consider to open circuit.
    volumeThreshold: 50,
    // Don't break the circuit on HTTP status codes 200..<500.
    errorFilter: (error) => {
        const httpError = error;
        return httpError.status >= 200 && httpError.status < 500;
    }
}, logger_1.default);
//# sourceMappingURL=circuitBreaker.js.map