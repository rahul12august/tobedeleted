"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importDefault(require("../logger"));
const transactionUsageRepo_service_1 = __importDefault(require("./transactionUsageRepo.service"));
const transaction_conditions_helper_1 = __importDefault(require("../transaction/transaction.conditions.helper"));
class TransactionUsageManager {
    setTransactionUsageHandlerInstance(transactionUsageHandlerInstance) {
        this.transactionUsageHandlerInstance = transactionUsageHandlerInstance;
    }
    getTransactionUsageHandlerInstance() {
        return this.transactionUsageHandlerInstance;
    }
    validateDailyUsageParams(params) {
        const handlerInstance = this.transactionUsageHandlerInstance;
        return handlerInstance.validateDailyUsageParams(params);
    }
    processReversalBasedOnContext() {
        return __awaiter(this, void 0, void 0, function* () {
            const handlerInstance = this.transactionUsageHandlerInstance;
            handlerInstance.processReversalBasedOnContext();
        });
    }
    isFeatureFlagEnabled() {
        const handlerInstance = this.transactionUsageHandlerInstance;
        return handlerInstance.isFeatureFlagEnabled();
    }
    saveDailyLimitContext(params) {
        const handlerInstance = this.transactionUsageHandlerInstance;
        handlerInstance.saveDailyLimitToContext(params);
    }
    validateTransactionAmountAgainstDailyLimit(validatedParams) {
        return __awaiter(this, void 0, void 0, function* () {
            const handlerInstance = this.transactionUsageHandlerInstance;
            const { customerId, accountNo, debitTransactionCode, debitLimitGroupCode, paymentServiceCode, paymentServiceType, limitGroupInfo, transactionAmount } = validatedParams;
            logger_1.default.info(`[validateDailyLimit] validated params:
      customerId: ${customerId}
      accountNo: ${accountNo}
      debitTransactionCode: ${debitTransactionCode}
      debitLimitGroupCode: ${debitLimitGroupCode}
      paymentServiceType: ${paymentServiceType}
      paymentServiceCode: ${paymentServiceCode}
    `);
            //3. check transaction category whether it is valid by recommend service as the input
            const isValidTransCategory = handlerInstance.isValidTransactionCategoryByRecommendedService(validatedParams);
            //if transaction category cannot be derived from recommended service (and not part of pocket level withdrawal)
            //then should skip the check
            if (!isValidTransCategory) {
                return {
                    isValidated: false
                };
            }
            //4.query the dailyAmount from transaction collection (pocket level)
            const existingTransactionUsage = yield transactionUsageRepo_service_1.default.findTransactionUsageByCustomerAndAccount(customerId, accountNo);
            //2. get current dailyUsages accumulation
            const dailyAmountSoFar = handlerInstance.getDailyLimitAmountSoFar(existingTransactionUsage);
            logger_1.default.info(`[validateTransactionAmountAgainstDailyLimit] dailyAmountSoFar: ${dailyAmountSoFar}`);
            //3.validate the amount and output the state (ACCEPTED, DECLINED, TRY_TOMORROW)
            const resultState = transaction_conditions_helper_1.default.compareAmountWithDailyLimit(limitGroupInfo, transactionAmount, dailyAmountSoFar);
            logger_1.default.info(`[validateTransactionAmountAgainstDailyLimit] validationResult: ${resultState}`);
            return {
                isValidated: true,
                dailyAmountSoFar,
                ruleConditionStateResult: resultState
            };
        });
    }
    processAcceptedState(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const handlerInstance = this.transactionUsageHandlerInstance;
            yield handlerInstance.processAcceptedState({
                dailyAmountSoFar: params.dailyAmountSoFar,
                validatedParams: params.validatedParams
            });
        });
    }
    processRejectedState(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const handlerInstance = this.transactionUsageHandlerInstance;
            const { validatedParams, dailyAmountSoFar, ruleConditionStateResult } = params;
            const { transactionAmount, limitGroupInfo } = validatedParams;
            //generate evalStatus
            const resultStatus = transaction_conditions_helper_1.default.constructDailyLimitEvaluatedRuleConditionStatusRefined(transactionAmount, dailyAmountSoFar, handlerInstance.getTransactionUsageCategory(), limitGroupInfo.dailyLimitAmount, handlerInstance.getEvalStatusErrors().errorCodeDeclined, handlerInstance.getEvalStatusErrors().errorCodeTryTomorrow, ruleConditionStateResult);
            //6. handle when
            yield handlerInstance.processRejectedState({
                evalStatus: resultStatus,
                validatedParams,
                dailyAmountSoFar
            });
        });
    }
}
exports.TransactionUsageManager = TransactionUsageManager;
//# sourceMappingURL=transactionUsageManager.js.map