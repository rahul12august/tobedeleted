"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_category_enum_1 = require("../transaction/transaction.category.enum");
const transaction_enum_1 = require("../transaction/transaction.enum");
exports.TransactionUsageIncludedPaymentServiceCodes = new Map([
    [
        "atm-withdrawal" /* ATM_WITHDRAWAL */,
        [...transaction_category_enum_1.ATM_WITHDRAWAL_PAYMENT_SERVICE_CODES]
    ],
    [
        "atm-transfer" /* ATM_TRANSFER */,
        [...transaction_category_enum_1.ATM_TRANSFER_PAYMENT_SERVICE_CODES]
    ]
]);
exports.reversalTransactionConfirmationResponseCode = [
    transaction_enum_1.ConfirmTransactionStatus.REQUEST_TIMEOUT,
    transaction_enum_1.ConfirmTransactionStatus.ERROR_EXPECTED,
    transaction_enum_1.ConfirmTransactionStatus.ERROR_UNEXPECTED
];
var DailyLimitField;
(function (DailyLimitField) {
    DailyLimitField["ATM_WITHDRAWAL"] = "dailyUsages.atmWithdrawal";
    DailyLimitField["ATM_TRANSFER"] = "dailyUsages.atmTransfer";
    DailyLimitField["NOT_IMPLEMENTED"] = "dailyUsages.notImplemented";
})(DailyLimitField = exports.DailyLimitField || (exports.DailyLimitField = {}));
exports.getDailyLimitFieldRepoByCategory = (category) => {
    switch (category) {
        case "atm-withdrawal" /* ATM_WITHDRAWAL */:
            return DailyLimitField.ATM_WITHDRAWAL;
        case "atm-transfer" /* ATM_TRANSFER */:
            return DailyLimitField.ATM_TRANSFER;
        default:
            return DailyLimitField.NOT_IMPLEMENTED;
    }
};
//# sourceMappingURL=transactionUsage.enum.js.map