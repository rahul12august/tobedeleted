"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const mongoose_util_1 = require("../common/mongoose.util");
exports.collectionName = 'transaction_usages';
const ATMWithdrawalDailyUsageSubSchema = new mongoose_1.Schema({
    amount: {
        type: Number,
        required: true
    },
    lastUpdatedAt: {
        type: Date,
        required: true
    }
}, { _id: false });
const ATMTransferDailyUsageSubSchema = new mongoose_1.Schema({
    amount: {
        type: Number,
        required: true
    },
    lastUpdatedAt: {
        type: Date,
        required: true
    }
}, { _id: false });
const DailyUsageSubSchema = new mongoose_1.Schema({
    atmWithdrawal: {
        type: ATMWithdrawalDailyUsageSubSchema,
        required: false
    },
    atmTransfer: {
        type: ATMTransferDailyUsageSubSchema,
        required: false
    }
}, { _id: false });
const TransactionUsageSchema = new mongoose_1.Schema({
    customerId: {
        type: String,
        required: true
    },
    accountNo: {
        type: String,
        required: true
    },
    dailyUsages: {
        type: DailyUsageSubSchema,
        required: true
    }
});
TransactionUsageSchema.index({ customerId: 1, accountNo: 1 }, { unique: true });
TransactionUsageSchema.post('save', mongoose_util_1.handleDuplicationError);
TransactionUsageSchema.post('find', mongoose_util_1.handleDuplicationError);
exports.TransactionUsageModel = mongoose_1.default.model(exports.collectionName, TransactionUsageSchema);
//# sourceMappingURL=transactionUsage.model.js.map