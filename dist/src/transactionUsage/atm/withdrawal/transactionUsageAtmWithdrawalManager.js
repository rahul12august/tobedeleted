"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transactionUsageManager_1 = require("../../transactionUsageManager");
const transactionUsageAtmWithdrawalHandler_1 = require("./transactionUsageAtmWithdrawalHandler");
class TransactionUsageAtmWithdrawalManager extends transactionUsageManager_1.TransactionUsageManager {
    constructor(transactionUsageHandler = new transactionUsageAtmWithdrawalHandler_1.TransactionUsageAtmWithdrawalHandler()) {
        super();
        super.setTransactionUsageHandlerInstance(transactionUsageHandler);
    }
}
exports.TransactionUsageAtmWithdrawalManager = TransactionUsageAtmWithdrawalManager;
//# sourceMappingURL=transactionUsageAtmWithdrawalManager.js.map