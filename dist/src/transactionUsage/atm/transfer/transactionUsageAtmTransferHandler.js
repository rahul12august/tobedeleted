"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const errors_1 = require("../../../common/errors");
const featureFlag_1 = require("../../../common/featureFlag");
const logger_1 = __importDefault(require("../../../logger"));
const transaction_category_enum_1 = require("../../../transaction/transaction.category.enum");
const lodash_1 = __importDefault(require("lodash"));
const transactionUsageCommon_service_1 = __importDefault(require("../../transactionUsageCommon.service"));
const transactionUsageRepo_service_1 = __importDefault(require("../../transactionUsageRepo.service"));
const AppError_1 = require("../../../errors/AppError");
const transaction_producer_1 = __importDefault(require("../../../transaction/transaction.producer"));
const transaction_producer_enum_1 = require("../../../transaction/transaction.producer.enum");
const transaction_enum_1 = require("../../../transaction/transaction.enum");
const contextHandler_1 = require("../../../common/contextHandler");
const constant_1 = require("../../../common/constant");
class TransactionUsageAtmTransferHandler {
    constructor() {
        this.getDailyLimitAmountSoFar = (existingTransactionUsage) => {
            //1. check if existing transactionUsage record is null
            if (existingTransactionUsage == null ||
                lodash_1.default.isUndefined(existingTransactionUsage.dailyUsages.atmTransfer)) {
                return 0;
            }
            const { amount, lastUpdatedAt } = existingTransactionUsage.dailyUsages.atmTransfer;
            //2. check whether the record is expired (if it is in the past)
            const isExpired = transactionUsageCommon_service_1.default.isDailyLimitExpired(lastUpdatedAt);
            if (isExpired) {
                //if expired, the dailyUsages should reset back to 0
                return 0;
            }
            //3. return latest dailyUsages amount
            return amount;
        };
    }
    isFeatureFlagEnabled() {
        return featureFlag_1.isFeatureEnabled(constant_1.FEATURE_FLAG.ENABLE_DAILY_LIMIT_ATM_TRANSFER_POCKET_LIMIT_VALIDATION);
    }
    getEvalStatusErrors() {
        return {
            errorCodeDeclined: errors_1.ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT,
            errorCodeTryTomorrow: errors_1.ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT
        };
    }
    getTransactionUsageGroupCode() {
        return "ATM_TRANSFER_POCKET_LEVEL_DAILY_LIMIT" /* ATM_TRANSFER */;
    }
    getTransactionUsageCategory() {
        return "atm-transfer" /* ATM_TRANSFER */;
    }
    isValidTransactionCategoryByRecommendedService(validatedParams) {
        //extract validated params
        const { debitLimitGroupCode: limitGroupCode, paymentServiceType, paymentServiceCode } = validatedParams;
        //validate the transaction's category whether it is ATM TRANSFER
        logger_1.default.info(`[isValidTransactionCategoryByRecommendedService]: 
        paymentServiceType: ${paymentServiceType}
        paymentServiceCode: ${paymentServiceCode}
        limitGroupCode: ${limitGroupCode}
      `);
        for (const metadata of transaction_category_enum_1.TRANSACTION_CATEGORY_ATM_TRANSFER) {
            if (metadata.paymentServiceType === paymentServiceType &&
                metadata.paymentServiceCode === paymentServiceCode &&
                metadata.limitGroupCode === limitGroupCode) {
                return true;
            }
        }
        logger_1.default.info(`[isValidTransactionCategoryByRecommendedService] not ATM TRANSFER  category`);
        return false;
    }
    validateDailyUsageParams(params) {
        const { customerId, accountNo, beneficiaryAccountNo, beneficiaryAccountName, transactionAmount, paymentServiceType, recommendedService, limitGroupConfigs, transactionInterchange } = params;
        //validate customerId and accountNo must be EXIST
        if (lodash_1.default.isNil(customerId) ||
            lodash_1.default.isNil(accountNo) ||
            lodash_1.default.isNil(beneficiaryAccountNo) ||
            lodash_1.default.isNil(beneficiaryAccountName)) {
            throw new AppError_1.AppError(errors_1.ERROR_CODE.ATM_TRANSFER_EXECUTOR_MANDATORY_INFO_NOT_FOUND);
        }
        let debitTransactionCodes = recommendedService.debitTransactionCode;
        if (lodash_1.default.isNil(debitTransactionCodes) || lodash_1.default.isEmpty(debitTransactionCodes)) {
            throw new AppError_1.AppError(errors_1.ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE);
        }
        const mappingInfoMatched = transactionUsageCommon_service_1.default.extractInfoFromTransactionCodeMappings({
            transactionCodesMappingInfo: debitTransactionCodes,
            currentTransactionInterchange: transactionInterchange
        });
        let debitLimitGroupCode = mappingInfoMatched.limitGroupCode;
        let debitTransactionCode = mappingInfoMatched.transactionCode;
        const paymentServiceCode = recommendedService.paymentServiceCode;
        const limitGroupInfo = transactionUsageCommon_service_1.default.getLimitGroupCodeInfo(this.getTransactionUsageGroupCode(), limitGroupConfigs);
        return {
            customerId,
            accountNo,
            beneficiaryAccountNo,
            beneficiaryAccountName,
            transactionAmount,
            paymentServiceType,
            debitLimitGroupCode,
            debitTransactionCode,
            limitGroupInfo,
            paymentServiceCode
        };
    }
    saveDailyLimitToContext(params) {
        const { customerId, accountNo, transactionAmount, transactionUsageCategory, paymentServiceCode, limitGroupCode } = params;
        if (lodash_1.default.isUndefined(customerId) ||
            lodash_1.default.isEmpty(customerId) ||
            lodash_1.default.isUndefined(accountNo) ||
            lodash_1.default.isEmpty(accountNo)) {
            return;
        }
        const isSameTransactionUsageCategoryAsHandler = transactionUsageCategory === this.getTransactionUsageCategory();
        if (isSameTransactionUsageCategoryAsHandler) {
            //write it to context
            contextHandler_1.setTransactionUsageInfoTracker({
                customerId: customerId,
                accountNo: accountNo,
                transactionCategory: transactionUsageCategory,
                currentTransactionAmount: transactionAmount,
                paymentServiceCode,
                limitGroupCode
            });
        }
    }
    processAcceptedState(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const { validatedParams, dailyAmountSoFar } = params;
            const { customerId, accountNo, beneficiaryAccountName, beneficiaryAccountNo, transactionAmount, paymentServiceCode, debitLimitGroupCode } = validatedParams;
            const expectedAmountAfterUpdate = dailyAmountSoFar + transactionAmount;
            logger_1.default.debug(`[processAcceptedState] 
      customerId: ${customerId}
      accountNo: ${accountNo}
      beneficiaryAccountNo: ${beneficiaryAccountNo}
      beneficiaryAccountName: ${beneficiaryAccountName}
      dailyAmountSumSoFar: ${dailyAmountSoFar}
      expectedAmountAfterUpdate: ${expectedAmountAfterUpdate}
      paymentServiceCode: ${paymentServiceCode}
      debitLimitGroupCode: ${debitLimitGroupCode}
    `);
            //update  or insert new to db (Directly consume "quota")
            const transactionUsageRecord = yield transactionUsageRepo_service_1.default.insertOrUpdateDailyUsage(customerId, accountNo, expectedAmountAfterUpdate, "atm-transfer" /* ATM_TRANSFER */);
            if (transactionUsageRecord == null) {
                throw new AppError_1.AppError(errors_1.ERROR_CODE.FAILED_TO_SAVE_UPDATE_TRANSACTION_USAGE);
            }
            //save to context
            this.saveDailyLimitToContext({
                customerId: customerId,
                accountNo: accountNo,
                transactionAmount: transactionAmount,
                transactionUsageCategory: this.getTransactionUsageCategory(),
                paymentServiceCode,
                limitGroupCode: debitLimitGroupCode
            });
        });
    }
    constructNotificationMessageParams(validatedParams) {
        return __awaiter(this, void 0, void 0, function* () {
            const { beneficiaryAccountNo, beneficiaryAccountName, transactionAmount, customerId } = validatedParams;
            return {
                customerId,
                beneficiaryAccountNo: beneficiaryAccountNo,
                beneficiaryName: beneficiaryAccountName,
                amount: transactionAmount
            };
        });
    }
    processRejectedState(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const { validatedParams, evalStatus, dailyAmountSoFar } = params;
            const { customerId, accountNo, beneficiaryAccountNo, beneficiaryAccountName } = validatedParams;
            logger_1.default.debug(`[processRejectedState] 
      customerId: ${customerId}
      accountNo: ${accountNo}
      beneficiaryAccountNo: ${beneficiaryAccountNo}
      beneficiaryAccountName: ${beneficiaryAccountName}
      dailyAmountSumSoFar: ${dailyAmountSoFar}
    `);
            let errorCode = evalStatus.errorCode;
            if (lodash_1.default.isNil(errorCode)) {
                //change to generic error code
                errorCode = errors_1.ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT;
            }
            logger_1.default.error(`[processRejectedState]: ${evalStatus.message}`);
            const notifMessage = yield this.constructNotificationMessageParams(validatedParams);
            logger_1.default.info(`[processRejectedState] 
      sending notification
    `);
            //send notification
            yield transaction_producer_1.default.sendFailedMoneyTransferViaAtmDailyLimit(notifMessage, transaction_producer_enum_1.NotificationCode.NOTIF_DAILY_LIMIT_ATM_TRANSFER);
            //then throw error
            throw new AppError_1.RetryableTransferAppError(evalStatus.message, transaction_enum_1.TransferFailureReasonActor.MS_TRANSFER, errorCode, false, [
                {
                    message: errors_1.ErrorList[errorCode].message,
                    key: errorCode,
                    code: errorCode
                }
            ]);
        });
    }
    processReversalBasedOnContext() {
        return __awaiter(this, void 0, void 0, function* () {
            logger_1.default.info(`Processing reversal for ATM Transfer Usage`);
            const transactionUsageInfo = contextHandler_1.getTransactionUsageInfoTracker();
            //if the info does not exist in context
            if (lodash_1.default.isNil(transactionUsageInfo)) {
                return;
            }
            const isTransactionCategoryValid = transactionUsageInfo.transactionCategory ==
                this.getTransactionUsageCategory();
            //if the transaction category does not match ATM Transfer, skip it
            if (!isTransactionCategoryValid) {
                return;
            }
            const { customerId, accountNo, currentTransactionAmount } = transactionUsageInfo;
            logger_1.default.info(`[processReversalBasedOnContext] atm transfer - checking if the dailyAmountSumSoFar should be set to 0`);
            let updatedTransLimit = yield transactionUsageRepo_service_1.default.revertDailyUsageToZero(customerId, accountNo, currentTransactionAmount, "atm-transfer" /* ATM_TRANSFER */);
            if (updatedTransLimit == null) {
                logger_1.default.info(`[processReversalBasedOnContext] atm transfer - checking if the dailyAmountSumSoFar should NOT be set to 0, but 
            just perform revert the atm-transfer amount`);
                updatedTransLimit = yield transactionUsageRepo_service_1.default.revertDailyUsage(customerId, accountNo, currentTransactionAmount, "atm-transfer" /* ATM_TRANSFER */);
            }
            if (!lodash_1.default.isNull(updatedTransLimit) &&
                !lodash_1.default.isUndefined(updatedTransLimit.dailyUsages.atmTransfer)) {
                logger_1.default.debug(`[ATM TRANSFER LIMIT REVERSAL] success: 
            customerId : ${updatedTransLimit.customerId}
            accountNo: ${updatedTransLimit.accountNo}
            currentAccumulatedAmount: ${updatedTransLimit.dailyUsages.atmTransfer.amount}
          `);
            }
            //need to revert rtol daily limit if it is L002 - transaction (RTOL LIMIT)
            // if (limitGroupCode === 'L002') {
            //   //revert RTOL daily limit
            //   awardRepository.updateUsageCounters(customerId);
            // }
        });
    }
}
exports.TransactionUsageAtmTransferHandler = TransactionUsageAtmTransferHandler;
//# sourceMappingURL=transactionUsageAtmTransferHandler.js.map