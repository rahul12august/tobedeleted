"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transactionUsageManager_1 = require("../../transactionUsageManager");
const transactionUsageAtmTransferHandler_1 = require("./transactionUsageAtmTransferHandler");
class TransactionUsageAtmTransferManager extends transactionUsageManager_1.TransactionUsageManager {
    constructor(transactionUsageHandler = new transactionUsageAtmTransferHandler_1.TransactionUsageAtmTransferHandler()) {
        super();
        super.setTransactionUsageHandlerInstance(transactionUsageHandler);
    }
}
exports.TransactionUsageAtmTransferManager = TransactionUsageAtmTransferManager;
//# sourceMappingURL=transactionUsageAtmTransferManager.js.map