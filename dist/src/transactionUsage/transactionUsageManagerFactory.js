"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const errors_1 = require("../common/errors");
const AppError_1 = require("../errors/AppError");
const transactionUsageAtmTransferManager_1 = require("./atm/transfer/transactionUsageAtmTransferManager");
const transactionUsageAtmWithdrawalManager_1 = require("./atm/withdrawal/transactionUsageAtmWithdrawalManager");
class TransactionUsageManagerFactory {
    static getTransactionUsageManagerInstance(category) {
        switch (category) {
            case "atm-withdrawal" /* ATM_WITHDRAWAL */:
                this.transactionUsageManager = new transactionUsageAtmWithdrawalManager_1.TransactionUsageAtmWithdrawalManager();
                return this.transactionUsageManager;
            case "atm-transfer" /* ATM_TRANSFER */:
                this.transactionUsageManager = new transactionUsageAtmTransferManager_1.TransactionUsageAtmTransferManager();
                return this.transactionUsageManager;
            default:
                throw new AppError_1.AppError(errors_1.ERROR_CODE.TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND);
        }
    }
    static resetTransactionUsageManagerInstance() {
        this.transactionUsageManager = null;
    }
}
exports.TransactionUsageManagerFactory = TransactionUsageManagerFactory;
TransactionUsageManagerFactory.transactionUsageManager = null;
//# sourceMappingURL=transactionUsageManagerFactory.js.map