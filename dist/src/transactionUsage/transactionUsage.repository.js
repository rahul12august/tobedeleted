"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = require("../logger");
const transactionUsage_model_1 = require("./transactionUsage.model");
const convertTransactionUsageDocumentToObject = (document) => document.toObject({
    getters: true
});
const getTransactionUsage = (customerId, accountNo) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionUsage = yield transactionUsage_model_1.TransactionUsageModel.findOne({
        customerId: customerId,
        accountNo: accountNo
    });
    return (transactionUsage &&
        convertTransactionUsageDocumentToObject(transactionUsage));
});
const insertOrUpdateTransactionUsage = (query, update) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionUsage = yield transactionUsage_model_1.TransactionUsageModel.findOneAndUpdate(query, update, { new: true, upsert: true });
    return (transactionUsage &&
        convertTransactionUsageDocumentToObject(transactionUsage));
});
const updateTransactionUsageCustom = (query, update) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield transactionUsage_model_1.TransactionUsageModel.findOneAndUpdate(query, update, {
        new: true
    }).exec();
    return data && convertTransactionUsageDocumentToObject(data);
});
const transactionUsageRepository = {
    getTransactionUsage,
    updateTransactionUsageCustom,
    insertOrUpdateTransactionUsage
};
exports.default = logger_1.wrapLogs(transactionUsageRepository);
//# sourceMappingURL=transactionUsage.repository.js.map