"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const errors_1 = require("../common/errors");
const AppError_1 = require("../errors/AppError");
const transactionUsage_enum_1 = require("./transactionUsage.enum");
const transactionUsage_repository_1 = __importDefault(require("./transactionUsage.repository"));
const findTransactionUsageByCustomerAndAccount = (customerId, accountNo) => __awaiter(void 0, void 0, void 0, function* () {
    return transactionUsage_repository_1.default.getTransactionUsage(customerId, accountNo);
});
const insertOrUpdateDailyUsage = (customerId, accountNo, expectedAmountAfterUpdate, transactionCategory) => __awaiter(void 0, void 0, void 0, function* () {
    const filterQuery = {
        customerId: customerId,
        accountNo: accountNo
    };
    const fieldToUpdate = transactionUsage_enum_1.getDailyLimitFieldRepoByCategory(transactionCategory);
    if (fieldToUpdate === transactionUsage_enum_1.DailyLimitField.NOT_IMPLEMENTED) {
        throw new AppError_1.AppError(errors_1.ERROR_CODE.DAILY_USAGE_REPO_FIELD_TO_UPDATE_NOT_FOUND);
    }
    const updateQuery = {
        $set: {
            [fieldToUpdate]: {
                amount: expectedAmountAfterUpdate,
                lastUpdatedAt: new Date()
            }
        }
    };
    return yield transactionUsage_repository_1.default.insertOrUpdateTransactionUsage(filterQuery, updateQuery);
});
const revertDailyUsage = (customerId, accountNo, transactionAmountToBeReverted, transactionCategory) => __awaiter(void 0, void 0, void 0, function* () {
    const filterQuery = {
        customerId: customerId,
        accountNo: accountNo
    };
    const fieldToUpdate = transactionUsage_enum_1.getDailyLimitFieldRepoByCategory(transactionCategory);
    if (fieldToUpdate === transactionUsage_enum_1.DailyLimitField.NOT_IMPLEMENTED) {
        throw new AppError_1.AppError(errors_1.ERROR_CODE.DAILY_USAGE_REPO_FIELD_TO_UPDATE_NOT_FOUND);
    }
    const updateQuery = {
        $set: {
            [fieldToUpdate]: {
                $inc: {
                    amount: -Math.abs(transactionAmountToBeReverted)
                },
                lastUpdatedAt: new Date()
            }
        }
    };
    return yield transactionUsage_repository_1.default.updateTransactionUsageCustom(filterQuery, updateQuery);
});
const revertDailyUsageToZero = (customerId, accountNo, transactionAmountToBeReverted, transactionCategory) => __awaiter(void 0, void 0, void 0, function* () {
    const fieldToUpdate = transactionUsage_enum_1.getDailyLimitFieldRepoByCategory(transactionCategory);
    if (fieldToUpdate === transactionUsage_enum_1.DailyLimitField.NOT_IMPLEMENTED) {
        throw new AppError_1.AppError(errors_1.ERROR_CODE.DAILY_USAGE_REPO_FIELD_TO_UPDATE_NOT_FOUND);
    }
    const filterQuery = {
        customerId: customerId,
        accountNo: accountNo,
        [fieldToUpdate]: {
            amount: {
                $and: [{ $gte: 0 }, { $lte: transactionAmountToBeReverted }]
            }
        }
    };
    const updateQuery = {
        $set: {
            [fieldToUpdate]: {
                amount: 0,
                lastUpdatedAt: new Date()
            }
        }
    };
    return yield transactionUsage_repository_1.default.updateTransactionUsageCustom(filterQuery, updateQuery);
});
const transactionUsageRepoService = {
    findTransactionUsageByCustomerAndAccount,
    insertOrUpdateDailyUsage,
    revertDailyUsage,
    revertDailyUsageToZero
};
exports.default = transactionUsageRepoService;
//# sourceMappingURL=transactionUsageRepo.service.js.map