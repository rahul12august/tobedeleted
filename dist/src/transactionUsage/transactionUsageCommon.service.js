"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const errors_1 = require("../common/errors");
const AppError_1 = require("../errors/AppError");
const lodash_1 = __importDefault(require("lodash"));
const configuration_repository_1 = __importDefault(require("../configuration/configuration.repository"));
const config_1 = require("../config");
const transactionUsage_enum_1 = require("./transactionUsage.enum");
const transactionUsageManagerFactory_1 = require("./transactionUsageManagerFactory");
const logger_1 = __importDefault(require("../logger"));
const entitlement_service_1 = __importDefault(require("../entitlement/entitlement.service"));
const timezone = config_1.config.get('timeZone');
const momentTimeZone = (date) => {
    return moment_1.default(date).utcOffset(timezone);
};
const isDailyLimitExpired = (dailyLimitLastUpdatedAt) => {
    const resetDate = momentTimeZone().startOf('day');
    const dailyLimitMomentDate = momentTimeZone(dailyLimitLastUpdatedAt);
    return !moment_1.default(dailyLimitMomentDate).isAfter(resetDate);
};
const getLimitGroupCodeInfo = (limitGroupCode, limitGroupConfigs) => {
    if (lodash_1.default.isEmpty(limitGroupConfigs)) {
        throw new AppError_1.AppError(errors_1.ERROR_CODE.LIMIT_GROUPS_CONF_NOT_FOUND);
    }
    const foundLimitGroupCode = limitGroupConfigs.find(limitGroup => limitGroup.code === limitGroupCode);
    if (lodash_1.default.isUndefined(foundLimitGroupCode)) {
        throw new AppError_1.AppError(errors_1.ERROR_CODE.RECOMMENDED_LIMIT_GROUP_NOT_FOUND_IN_CONFIG);
    }
    return foundLimitGroupCode;
};
function overrideDailyLimitAmounts(customerId, limitGroupEntitlementCodeMap, entitlementsMap) {
    for (const entitlementKeyPair of entitlementsMap) {
        const [entitlementCode, entitlement] = entitlementKeyPair;
        const limitGroup = limitGroupEntitlementCodeMap.get(entitlementCode);
        if (!limitGroup) {
            continue;
        }
        const customDailyLimitAmount = Number(entitlement.quota);
        if (isNaN(customDailyLimitAmount) || customDailyLimitAmount < 0) {
            logger_1.default.error(`Expected the entitlement '${entitlement.entitlement}'` +
                `for customer '${customerId}' to be a positive numerical value, but it was '${entitlement.quota}'`);
            continue;
        }
        limitGroup.dailyLimitAmount = customDailyLimitAmount;
    }
}
const applyCustomLimitGroupsIfApplicable = (customerId, limitGroups) => __awaiter(void 0, void 0, void 0, function* () {
    if (lodash_1.default.isEmpty(limitGroups)) {
        return;
    }
    const limitGroupEntitlementCodeMap = new Map(limitGroups.map(limitGroup => [
        `limit.tx.${limitGroup.code}.daily`,
        limitGroup
    ]));
    const limitGroupEntitlementCodes = Array.from(limitGroupEntitlementCodeMap.keys());
    const entitlementsMap = yield entitlement_service_1.default.getEntitlementsMap(customerId, limitGroupEntitlementCodes);
    overrideDailyLimitAmounts(customerId, limitGroupEntitlementCodeMap, entitlementsMap);
});
const getLimitGroupsConfig = (customerId) => __awaiter(void 0, void 0, void 0, function* () {
    const limitGroupsInConf = yield configuration_repository_1.default.getLimitGroupsByCodes();
    if (lodash_1.default.isUndefined(limitGroupsInConf) || lodash_1.default.isEmpty(limitGroupsInConf)) {
        throw new AppError_1.AppError(errors_1.ERROR_CODE.LIMIT_GROUPS_CONF_NOT_FOUND);
    }
    if (customerId) {
        yield applyCustomLimitGroupsIfApplicable(customerId, limitGroupsInConf);
    }
    return limitGroupsInConf;
});
const isToProceedWithPocketLimitValidation = (paymentServiceCode) => {
    for (const [category, paymentServiceCodes] of transactionUsage_enum_1.TransactionUsageIncludedPaymentServiceCodes.entries()) {
        if (paymentServiceCodes.includes(paymentServiceCode)) {
            //get the associated manager instance
            const transactionUsageManagerInstance = transactionUsageManagerFactory_1.TransactionUsageManagerFactory.getTransactionUsageManagerInstance(category);
            const isToProceed = transactionUsageManagerInstance.isFeatureFlagEnabled();
            if (!isToProceed) {
                return {
                    isToProceed: false
                };
            }
            return {
                isToProceed: true,
                transactionUsageCategory: category,
                transactionUsageManagerInstance: transactionUsageManagerInstance
            };
        }
    }
    return {
        isToProceed: false
    };
};
const extractInfoFromTransactionCodeMappings = (params) => {
    //assumption transactionCodeMappingInfo is not empty array
    const { transactionCodesMappingInfo, currentTransactionInterchange } = params;
    logger_1.default.info(`extractInfoFromTransactionCodeMappings: 
    currentTransactionInterchange: ${currentTransactionInterchange}
  `);
    const defaultLimitGroupCode = transactionCodesMappingInfo[0].transactionCodeInfo.limitGroupCode;
    logger_1.default.info(`[extractInfoFromTransactionCodeMappings]:
  defaultLimitGroupCode: ${defaultLimitGroupCode}
  `);
    if (lodash_1.default.isNil(defaultLimitGroupCode)) {
        throw new AppError_1.AppError(errors_1.ERROR_CODE.RECOMMENDED_LIMIT_GROUP_CODE_NOT_EXIST);
    }
    //this is the fallback in case it can't found the match
    const defaultResult = {
        transactionCode: transactionCodesMappingInfo[0].transactionCode,
        limitGroupCode: defaultLimitGroupCode
    };
    logger_1.default.info(`[extractInfoFromTransactionCodeMappings] defaultResult: 
    transactionCode: ${defaultResult.transactionCode}
    limitGroupCode: ${defaultResult.limitGroupCode}
  `);
    //if current transaction interchange is not provided,
    //then just return the first array value of transaction code
    if (lodash_1.default.isUndefined(currentTransactionInterchange)) {
        return defaultResult;
    }
    for (const tcMappingInfo of transactionCodesMappingInfo) {
        if (lodash_1.default.isUndefined(tcMappingInfo.interchange)) {
            continue;
        }
        if (tcMappingInfo.interchange === currentTransactionInterchange) {
            let matchedLimitGroupCode = tcMappingInfo.transactionCodeInfo.limitGroupCode;
            logger_1.default.info(`[extractInfoFromTransactionCodeMappings] interchange matched: 
      currentTransactionInterchange: ${currentTransactionInterchange}
      mappingInfo transactionCode: ${tcMappingInfo.transactionCode}
      mappingInfo limitGroupCode: ${matchedLimitGroupCode}
    `);
            if (lodash_1.default.isNil(matchedLimitGroupCode)) {
                logger_1.default.info(`[extractInfoFromTransactionCodeMappings] interchange matched: 
        but limitGroup inside the mapping is undefined
      `);
                throw new AppError_1.AppError(errors_1.ERROR_CODE.RECOMMENDED_LIMIT_GROUP_CODE_NOT_EXIST);
            }
            return {
                transactionCode: tcMappingInfo.transactionCode,
                limitGroupCode: matchedLimitGroupCode
            };
        }
    }
    return defaultResult;
};
const transactionUsageCommonService = {
    isDailyLimitExpired,
    getLimitGroupsConfig,
    getLimitGroupCodeInfo,
    isToProceedWithPocketLimitValidation,
    extractInfoFromTransactionCodeMappings,
    applyCustomLimitGroupsIfApplicable
};
exports.default = transactionUsageCommonService;
//# sourceMappingURL=transactionUsageCommon.service.js.map