"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importDefault(require("../logger"));
const transactionUsageCommon_service_1 = __importDefault(require("./transactionUsageCommon.service"));
const transactionUsageManagerFactory_1 = require("./transactionUsageManagerFactory");
const lodash_1 = __importDefault(require("lodash"));
const transactionUsage_enum_1 = require("./transactionUsage.enum");
const contextHandler_1 = require("../common/contextHandler");
const transaction_conditions_enum_1 = require("../transaction/transaction.conditions.enum");
//pocket-level limit validation feature
const pocketLevelLimitValidation = (params) => __awaiter(void 0, void 0, void 0, function* () {
    const { recommendedService, customerId, accountNo, beneficiaryAccountNo, beneficiaryAccountName, paymentServiceType } = params;
    const paymentServiceCode = recommendedService.paymentServiceCode;
    logger_1.default.info(`[pocketLevelLimitValidation] start:
    customerId: ${customerId}
    accountNo: ${accountNo}
    beneficiaryAccountNo: ${beneficiaryAccountNo}
    beneficiaryAccountName: ${beneficiaryAccountName}
    paymentServiceType: ${paymentServiceType}
    recommendedService paymentServiceCode: ${paymentServiceCode}
  `);
    //perform prevalidation against payment service code, currently only accepting Atm Withdrawal and Atm Transfer
    const preValidationResponse = transactionUsageCommon_service_1.default.isToProceedWithPocketLimitValidation(paymentServiceCode);
    const { isToProceed, transactionUsageCategory, transactionUsageManagerInstance } = preValidationResponse;
    //if prevalidation for pocket limit is invalid, then skip it
    if (!isToProceed ||
        lodash_1.default.isUndefined(transactionUsageCategory) ||
        lodash_1.default.isUndefined(transactionUsageManagerInstance)) {
        return;
    }
    //get the limit groups
    const limitGroupConfigs = yield transactionUsageCommon_service_1.default.getLimitGroupsConfig(params.customerId);
    //validate the params, making sure that subsequent functions will use validated params
    const validatedParams = transactionUsageManagerInstance.validateDailyUsageParams(Object.assign(Object.assign({}, params), { limitGroupConfigs }));
    //validate transaction amount against daily limit
    const validatedTransactionResponse = yield transactionUsageManagerInstance.validateTransactionAmountAgainstDailyLimit(validatedParams);
    //if not validated then skip it
    if (!validatedTransactionResponse.isValidated) {
        return;
    }
    //if dailyAmountSoFar or ruleState don't exist, then skip it
    if (lodash_1.default.isUndefined(validatedTransactionResponse.dailyAmountSoFar) ||
        lodash_1.default.isUndefined(validatedTransactionResponse.ruleConditionStateResult)) {
        return;
    }
    if (validatedTransactionResponse.ruleConditionStateResult ===
        transaction_conditions_enum_1.RuleConditionState.ACCEPTED) {
        yield transactionUsageManagerInstance.processAcceptedState({
            validatedParams,
            dailyAmountSoFar: validatedTransactionResponse.dailyAmountSoFar,
            ruleConditionStateResult: validatedTransactionResponse.ruleConditionStateResult
        });
    }
    else {
        yield transactionUsageManagerInstance.processRejectedState({
            validatedParams,
            dailyAmountSoFar: validatedTransactionResponse.dailyAmountSoFar,
            ruleConditionStateResult: validatedTransactionResponse.ruleConditionStateResult
        });
    }
});
const mapTransactionInfoToTransactionUsageInfoContext = (transaction) => __awaiter(void 0, void 0, void 0, function* () {
    logger_1.default.info(`[mapTransactionInfoToTransactionUsageInfoContext] start`);
    //if transaction is null or payment service code does not exist, then skip it
    if (lodash_1.default.isNull(transaction) || lodash_1.default.isUndefined(transaction.paymentServiceCode)) {
        return;
    }
    logger_1.default.info(`[mapTransactionInfoToTransactionUsageInfoContext] params: 
      sourceCustomerId: ${transaction.sourceCustomerId},
      sourceAccountNo: ${transaction.sourceAccountNo},
      paymentServiceCode: ${transaction.paymentServiceCode}
      debitLimitGroupCode: ${transaction.debitLimitGroupCode}
    `);
    const { sourceCustomerId, sourceAccountNo, transactionAmount, paymentServiceCode, debitLimitGroupCode } = transaction;
    const isSourceCustomerIdValid = !lodash_1.default.isUndefined(sourceCustomerId) && !lodash_1.default.isEmpty(sourceCustomerId);
    const isSourceAccountNoValid = !lodash_1.default.isUndefined(sourceAccountNo) && !lodash_1.default.isEmpty(sourceAccountNo);
    //if required value are not valid, then skip it
    if (!isSourceCustomerIdValid || !isSourceAccountNoValid) {
        return;
    }
    const preValidationResponse = transactionUsageCommon_service_1.default.isToProceedWithPocketLimitValidation(paymentServiceCode);
    const { isToProceed, transactionUsageCategory, transactionUsageManagerInstance } = preValidationResponse;
    //if prevalidation for pocket limit is invalid, then skip it
    if (!isToProceed ||
        lodash_1.default.isUndefined(transactionUsageCategory) ||
        lodash_1.default.isUndefined(transactionUsageManagerInstance)) {
        return;
    }
    transactionUsageManagerInstance.saveDailyLimitContext({
        customerId: sourceCustomerId,
        accountNo: sourceAccountNo,
        transactionUsageCategory,
        transactionAmount: transactionAmount,
        paymentServiceCode: paymentServiceCode,
        limitGroupCode: (debitLimitGroupCode !== null && debitLimitGroupCode !== void 0 ? debitLimitGroupCode : '')
    });
});
const processReversalPocketLimitByTransactionBasedOnContext = () => {
    //currently there is only one context being used - transactionUsageInfoTracker
    const transactionUsageInfoContext = contextHandler_1.getTransactionUsageInfoTracker();
    //if there is no context information found, then skip it
    if (lodash_1.default.isUndefined(transactionUsageInfoContext)) {
        return;
    }
    //extract transaction  category
    const transactionCategory = transactionUsageInfoContext.transactionCategory;
    logger_1.default.info(`[processReversalPocketLimitByTransactionBasedOnContext] transactionCategory: ${transactionCategory}
  `);
    //based ont the category derived the managerHandler
    const transactionUsageManagerInstance = transactionUsageManagerFactory_1.TransactionUsageManagerFactory.getTransactionUsageManagerInstance(transactionCategory);
    transactionUsageManagerInstance.processReversalBasedOnContext();
    logger_1.default.info(`[processReversalPocketLimitByTransactionBasedOnContext] transactionCategory: ${transactionCategory}
  COMPLETED`);
};
const processReversalPocketLimitByTransactionConfirmationInput = (input) => {
    logger_1.default.info(`[processReversalPocketLimitByTransactionConfirmationInput]
    
     transConfirmation responseCode: ${input.responseCode}
    `);
    const isToReverse = transactionUsage_enum_1.reversalTransactionConfirmationResponseCode.includes(input.responseCode);
    logger_1.default.info(`[processReversalPocketLimitByTransactionConfirmationInput] isToReverse: ${isToReverse}
  `);
    if (!isToReverse) {
        return;
    }
    processReversalPocketLimitByTransactionBasedOnContext();
    logger_1.default.info(`[processReversalPocketLimitByTransactionConfirmationInput] COMPLETED
  `);
};
const transactionUsageService = {
    pocketLevelLimitValidation,
    mapTransactionInfoToTransactionUsageInfoContext,
    processReversalPocketLimitByTransactionBasedOnContext,
    processReversalPocketLimitByTransactionConfirmationInput
};
exports.default = transactionUsageService;
//# sourceMappingURL=transactionUsage.service.js.map