"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const transactionUsageCommon_service_1 = __importDefault(require("./transactionUsageCommon.service"));
const customerIdParameter = 'customerId';
const getLimitGroups = {
    method: module_common_1.Http.Method.GET,
    path: `/private/limit-groups/{${customerIdParameter}?}`,
    options: {
        description: 'Api to check limit groups',
        notes: "If 'customerId' is specified, we return custom limits set for that customer",
        tags: ['api', 'transaction usage', 'limits'],
        auth: false,
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const customerId = hapiRequest.params[customerIdParameter];
            const limitGroups = yield transactionUsageCommon_service_1.default.getLimitGroupsConfig(customerId);
            return hapiResponse.response(limitGroups).code(module_common_1.Http.StatusCode.OK);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'OK'
                    },
                    [module_common_1.Http.StatusCode.BAD_REQUEST]: {
                        description: 'Bad request'
                    },
                    [module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR]: {
                        description: 'Internal server error'
                    }
                }
            }
        }
    }
};
const transactionUsageController = [getLimitGroups];
exports.default = transactionUsageController;
//# sourceMappingURL=transactionUsage.controller.js.map