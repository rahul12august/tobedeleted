"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const faker_1 = __importDefault(require("faker"));
const transaction_category_enum_1 = require("../../../../transaction/transaction.category.enum");
exports.getTransactionUsageSampleRecordDB = (customerId, accountNo, id, lastUpdatedAt) => {
    return {
        _id: id,
        customerId: customerId,
        accountId: '',
        accountNo: accountNo,
        dailyUsages: {
            atmWithdrawal: {
                amount: faker_1.default.random.number({ min: 50000, max: 100000 }),
                lastUpdatedAt: lastUpdatedAt
            }
        }
    };
};
exports.getTransactionUsageSampleRecordDB2 = (customerId, accountNo, id, lastUpdatedAt, updatedAmount) => {
    return {
        _id: id,
        customerId: customerId,
        accountId: '',
        accountNo: accountNo,
        dailyUsages: {
            atmWithdrawal: {
                amount: updatedAmount,
                lastUpdatedAt: lastUpdatedAt
            }
        }
    };
};
exports.getJagoAtmWithdrawalTransactionParamsSample = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'CWD02',
                    transactionCodeInfo: {
                        code: 'CWD02',
                        limitGroupCode: 'L015',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        }
    };
};
exports.getThirdPartAltoAtmWithdrawalTransactionParamsSample = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.ALTO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_WITHDRAWAL_THIRD_PARTY_ALTO.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_WITHDRAWAL_THIRD_PARTY_ALTO.paymentServiceCode,
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC005',
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.ALTO,
                    transactionCode: 'CWD03',
                    transactionCodeInfo: {
                        code: 'CWD03',
                        limitGroupCode: 'L006',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'ALTO'
                    }
                }
            ]
        }
    };
};
exports.getThirdPartyArtajasaAtmWithdrawalTransactionParamsSample = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.ARTAJASA,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_WITHDRAWAL_THIRD_PARTY_ARTAJASA.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_WITHDRAWAL_THIRD_PARTY_ARTAJASA.paymentServiceCode,
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC005',
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.ARTAJASA,
                    transactionCode: 'CWD03',
                    transactionCodeInfo: {
                        code: 'CWD03',
                        limitGroupCode: 'L006',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'ARTAJASA'
                    }
                }
            ]
        }
    };
};
exports.getInternationalAtmWithdrawalTransactionParamsSample = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 200000, max: 250000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.VISA,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_WITHDRAWAL_INTERNATIONAL.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_WITHDRAWAL_INTERNATIONAL.paymentServiceCode,
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC005',
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.VISA,
                    transactionCode: 'CWD03',
                    transactionCodeInfo: {
                        code: 'CWD05',
                        limitGroupCode: 'L006',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'VISA'
                    }
                }
            ]
        }
    };
};
exports.validateParamsValid = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'CWD02',
                    transactionCodeInfo: {
                        code: 'CWD02',
                        limitGroupCode: 'L015',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L006',
                dailyLimitAmount: 15000000
            },
            {
                code: 'L015',
                dailyLimitAmount: 15000000
            },
            {
                code: "ATM_WITHDRAWAL_POCKET_LEVEL_DAILY_LIMIT" /* ATM_WITHDRAWAL */,
                dailyLimitAmount: 15000000
            }
        ]
    };
};
exports.validateParamsCustomerIdUndefined = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: undefined,
        accountNo: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'CWD02',
                    transactionCodeInfo: {
                        code: 'CWD02',
                        limitGroupCode: 'L015',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L006',
                dailyLimitAmount: 15000000
            },
            {
                code: 'L015',
                dailyLimitAmount: 15000000
            },
            {
                code: "ATM_WITHDRAWAL_POCKET_LEVEL_DAILY_LIMIT" /* ATM_WITHDRAWAL */,
                dailyLimitAmount: 15000000
            }
        ]
    };
};
exports.validateParamsAccountNoUndefined = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: undefined,
        paymentServiceType: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'CWD02',
                    transactionCodeInfo: {
                        code: 'CWD02',
                        limitGroupCode: 'L015',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L006',
                dailyLimitAmount: 15000000
            },
            {
                code: 'L015',
                dailyLimitAmount: 15000000
            },
            {
                code: "ATM_WITHDRAWAL_POCKET_LEVEL_DAILY_LIMIT" /* ATM_WITHDRAWAL */,
                dailyLimitAmount: 15000000
            }
        ]
    };
};
exports.validateParamsLimitGroupCodeUndefined = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'CWD02',
                    transactionCodeInfo: {
                        code: 'CWD02',
                        limitGroupCode: undefined,
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L006',
                dailyLimitAmount: 15000000
            },
            {
                code: 'L015',
                dailyLimitAmount: 15000000
            },
            {
                code: "ATM_WITHDRAWAL_POCKET_LEVEL_DAILY_LIMIT" /* ATM_WITHDRAWAL */,
                dailyLimitAmount: 15000000
            }
        ]
    };
};
exports.validateParamsDebitTransactionCodeEmptyArray = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceCode,
            debitTransactionCode: [],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L006',
                dailyLimitAmount: 15000000
            },
            {
                code: 'L015',
                dailyLimitAmount: 15000000
            },
            {
                code: "ATM_WITHDRAWAL_POCKET_LEVEL_DAILY_LIMIT" /* ATM_WITHDRAWAL */,
                dailyLimitAmount: 15000000
            }
        ]
    };
};
exports.validateParamsDebitTransactionCodeUndefined = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceCode,
            debitTransactionCode: undefined,
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L006',
                dailyLimitAmount: 15000000
            },
            {
                code: 'L015',
                dailyLimitAmount: 15000000
            },
            {
                code: "ATM_WITHDRAWAL_POCKET_LEVEL_DAILY_LIMIT" /* ATM_WITHDRAWAL */,
                dailyLimitAmount: 15000000
            }
        ]
    };
};
exports.validateParamsTransactionInterchangeUndefined = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: undefined,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'CWD02',
                    transactionCodeInfo: {
                        code: 'CWD02',
                        limitGroupCode: 'L015',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L006',
                dailyLimitAmount: 15000000
            },
            {
                code: 'L015',
                dailyLimitAmount: 15000000
            },
            {
                code: "ATM_WITHDRAWAL_POCKET_LEVEL_DAILY_LIMIT" /* ATM_WITHDRAWAL */,
                dailyLimitAmount: 15000000
            }
        ]
    };
};
exports.validateParamsTransactionEmptyLimitGroupConfigs = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_WITHDRAWAL_JAGO.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'CWD02',
                    transactionCodeInfo: {
                        code: 'CWD02',
                        limitGroupCode: 'L015',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: []
    };
};
exports.getTransactionCodesMapping = () => {
    return [
        {
            interchange: module_common_1.BankNetworkEnum.JAGO,
            transactionCode: 'CWD02',
            transactionCodeInfo: {
                maxAmountPerTx: 5000000,
                minAmountPerTx: 50000,
                limitGroupCode: 'L015',
                channel: 'CWD02',
                code: 'CWD02'
            }
        },
        {
            interchange: module_common_1.BankNetworkEnum.ARTAJASA,
            transactionCode: 'CWD03',
            transactionCodeInfo: {
                maxAmountPerTx: 2500000,
                minAmountPerTx: 50000,
                limitGroupCode: 'L006',
                channel: 'CWD03',
                code: 'CWD03'
            }
        },
        {
            interchange: module_common_1.BankNetworkEnum.ALTO,
            transactionCode: 'CWD04',
            transactionCodeInfo: {
                maxAmountPerTx: 2500000,
                minAmountPerTx: 50000,
                limitGroupCode: 'L006',
                channel: 'CWD04',
                code: 'CWD04'
            }
        },
        {
            interchange: module_common_1.BankNetworkEnum.VISA,
            transactionCode: 'CWD05',
            transactionCodeInfo: {
                maxAmountPerTx: 5000000,
                minAmountPerTx: 100000,
                limitGroupCode: 'L006',
                channel: 'CWD05',
                code: 'CWD05'
            }
        }
    ];
};
//# sourceMappingURL=transactionUsage.data.js.map