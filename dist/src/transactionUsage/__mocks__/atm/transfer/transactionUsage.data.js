"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const faker_1 = __importDefault(require("faker"));
const transaction_category_enum_1 = require("../../../../transaction/transaction.category.enum");
exports.getTransactionUsageSampleRecordDB = (customerId, accountNo, id, lastUpdatedAt) => {
    return {
        _id: id,
        customerId: customerId,
        accountId: '',
        accountNo: accountNo,
        dailyUsages: {
            atmTransfer: {
                amount: faker_1.default.random.number({ min: 50000, max: 100000 }),
                lastUpdatedAt: lastUpdatedAt
            }
        }
    };
};
exports.getTransactionUsageSampleRecordDB2 = (customerId, accountNo, id, lastUpdatedAt, updatedAmount) => {
    return {
        _id: id,
        customerId: customerId,
        accountId: '',
        accountNo: accountNo,
        dailyUsages: {
            atmTransfer: {
                amount: updatedAmount,
                lastUpdatedAt: lastUpdatedAt
            }
        }
    };
};
exports.validateParamsValid = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountName: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'TFD24',
                    transactionCodeInfo: {
                        code: 'TFD24',
                        limitGroupCode: 'L001',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L001',
                dailyLimitAmount: 999999999999
            },
            {
                code: "ATM_TRANSFER_POCKET_LEVEL_DAILY_LIMIT" /* ATM_TRANSFER */,
                dailyLimitAmount: 50000000
            }
        ]
    };
};
exports.validateParamsCustomerIdUndefined = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: undefined,
        accountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountName: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'TFD24',
                    transactionCodeInfo: {
                        code: 'TFD24',
                        limitGroupCode: 'L001',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L001',
                dailyLimitAmount: 999999999999
            },
            {
                code: "ATM_TRANSFER_POCKET_LEVEL_DAILY_LIMIT" /* ATM_TRANSFER */,
                dailyLimitAmount: 50000000
            }
        ]
    };
};
exports.validateParamsAccountNoUndefined = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountName: faker_1.default.random.alphaNumeric(10),
        accountNo: undefined,
        paymentServiceType: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'TFD24',
                    transactionCodeInfo: {
                        code: 'TFD24',
                        limitGroupCode: 'L001',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L001',
                dailyLimitAmount: 999999999999
            },
            {
                code: "ATM_TRANSFER_POCKET_LEVEL_DAILY_LIMIT" /* ATM_TRANSFER */,
                dailyLimitAmount: 50000000
            }
        ]
    };
};
exports.validateParamsBeneficiaryAccountNoUndefined = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountNo: undefined,
        beneficiaryAccountName: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'TFD24',
                    transactionCodeInfo: {
                        code: 'TFD24',
                        limitGroupCode: 'L001',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L001',
                dailyLimitAmount: 999999999999
            },
            {
                code: "ATM_TRANSFER_POCKET_LEVEL_DAILY_LIMIT" /* ATM_TRANSFER */,
                dailyLimitAmount: 50000000
            }
        ]
    };
};
exports.validateParamsBeneficiaryAccountNameUndefined = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountName: undefined,
        paymentServiceType: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'TFD24',
                    transactionCodeInfo: {
                        code: 'TFD24',
                        limitGroupCode: 'L001',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L001',
                dailyLimitAmount: 999999999999
            },
            {
                code: "ATM_TRANSFER_POCKET_LEVEL_DAILY_LIMIT" /* ATM_TRANSFER */,
                dailyLimitAmount: 50000000
            }
        ]
    };
};
exports.validateParamsLimitGroupCodeUndefined = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountName: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'TFD24',
                    transactionCodeInfo: {
                        code: 'TFD24',
                        limitGroupCode: undefined,
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L001',
                dailyLimitAmount: 999999999999
            },
            {
                code: "ATM_TRANSFER_POCKET_LEVEL_DAILY_LIMIT" /* ATM_TRANSFER */,
                dailyLimitAmount: 50000000
            }
        ]
    };
};
exports.validateParamsDebitTransactionCodeEmptyArray = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountName: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
            debitTransactionCode: [],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L001',
                dailyLimitAmount: 999999999999
            },
            {
                code: "ATM_TRANSFER_POCKET_LEVEL_DAILY_LIMIT" /* ATM_TRANSFER */,
                dailyLimitAmount: 50000000
            }
        ]
    };
};
exports.validateParamsDebitTransactionCodeUndefined = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountName: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
            debitTransactionCode: undefined,
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L001',
                dailyLimitAmount: 999999999999
            },
            {
                code: "ATM_TRANSFER_POCKET_LEVEL_DAILY_LIMIT" /* ATM_TRANSFER */,
                dailyLimitAmount: 50000000
            }
        ]
    };
};
exports.validateParamsTransactionInterchangeUndefined = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: undefined,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountName: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'TFD24',
                    transactionCodeInfo: {
                        code: 'TFD24',
                        limitGroupCode: 'L001',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: [
            {
                code: 'L001',
                dailyLimitAmount: 999999999999
            },
            {
                code: "ATM_TRANSFER_POCKET_LEVEL_DAILY_LIMIT" /* ATM_TRANSFER */,
                dailyLimitAmount: 50000000
            }
        ]
    };
};
exports.validateParamsTransactionEmptyLimitGroupConfigs = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountName: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'TFD24',
                    transactionCodeInfo: {
                        code: 'TFD24',
                        limitGroupCode: 'L001',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        },
        limitGroupConfigs: []
    };
};
exports.getJagoAtmTransferJagoInterchangeTransactionParamsSample = () => {
    return {
        transactionAmount: faker_1.default.random.number({ min: 50000, max: 150000 }),
        transactionInterchange: module_common_1.BankNetworkEnum.JAGO,
        customerId: faker_1.default.random.alphaNumeric(10),
        accountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountNo: faker_1.default.random.alphaNumeric(10),
        beneficiaryAccountName: faker_1.default.random.alphaNumeric(10),
        paymentServiceType: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
        recommendedService: {
            paymentServiceCode: transaction_category_enum_1.ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
            debitTransactionCode: [
                {
                    interchange: module_common_1.BankNetworkEnum.JAGO,
                    transactionCode: 'TFD24',
                    transactionCodeInfo: {
                        code: 'TFD24',
                        limitGroupCode: 'L001',
                        minAmountPerTx: 50000,
                        maxAmountPerTx: 2500000,
                        channel: 'JAGO'
                    }
                }
            ],
            beneficiaryBankCodeType: '?',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false,
            beneficiaryBankCode: 'BC191' //jago
        }
    };
};
exports.getTransactionCodesMappingJagoAtmToNonJagoAcc = () => {
    return [
        {
            interchange: module_common_1.BankNetworkEnum.ARTAJASA,
            transactionCode: 'TFD32',
            transactionCodeInfo: {
                maxAmountPerTx: 25000000,
                minAmountPerTx: 10000,
                limitGroupCode: 'L002',
                channel: 'TFD32',
                code: 'TFD32'
            }
        },
        {
            interchange: module_common_1.BankNetworkEnum.ALTO,
            transactionCode: 'TFD42',
            transactionCodeInfo: {
                maxAmountPerTx: 25000000,
                minAmountPerTx: 10000,
                limitGroupCode: 'L002',
                channel: 'TFD42',
                code: 'TFD42'
            }
        }
    ];
};
exports.getTransactionCodesMappingsAll = () => {
    return [
        {
            interchange: module_common_1.BankNetworkEnum.JAGO,
            transactionCode: 'TFD24',
            transactionCodeInfo: {
                maxAmountPerTx: 9999999999.0,
                minAmountPerTx: 1,
                limitGroupCode: 'L001',
                channel: 'SAD01',
                code: 'TFD24'
            }
        },
        {
            interchange: module_common_1.BankNetworkEnum.ARTAJASA,
            transactionCode: 'TFD31',
            transactionCodeInfo: {
                maxAmountPerTx: 25000000,
                minAmountPerTx: 10000,
                limitGroupCode: 'L008',
                channel: 'TFD31',
                code: 'TFD31'
            }
        },
        {
            interchange: module_common_1.BankNetworkEnum.ARTAJASA,
            transactionCode: 'TFD41',
            transactionCodeInfo: {
                maxAmountPerTx: 25000000,
                minAmountPerTx: 10000,
                limitGroupCode: 'L008',
                channel: 'TFD41',
                code: 'TFD41'
            }
        },
        {
            interchange: module_common_1.BankNetworkEnum.ARTAJASA,
            transactionCode: 'TFD32',
            transactionCodeInfo: {
                maxAmountPerTx: 25000000,
                minAmountPerTx: 10000,
                limitGroupCode: 'L002',
                channel: 'TFD32',
                code: 'TFD32'
            }
        },
        {
            interchange: module_common_1.BankNetworkEnum.ALTO,
            transactionCode: 'TFD42',
            transactionCodeInfo: {
                maxAmountPerTx: 25000000,
                minAmountPerTx: 10000,
                limitGroupCode: 'L002',
                channel: 'TFD42',
                code: 'TFD42'
            }
        }
    ];
};
//# sourceMappingURL=transactionUsage.data.js.map