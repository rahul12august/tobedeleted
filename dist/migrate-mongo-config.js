const { config, loadConfig } = require('@dk/module-config');
const { Util } = require('@dk/module-common');
const { merge } = require('lodash');

/* To overirde any config value call loadConfig with respective config object

ex: loadConfig({ 'server': { port: 9090 } });
*/
loadConfig();
// ENV config will apply on reload
// JSON config must be rebuilt to apply
const defaultConf = require('./configs/default.json');
const env = config.get('NODE_ENV');
const envConf = env ? require(`./configs/${env}.json`) : {};

config.overrides(merge(defaultConf, envConf));
// In this file you can configure migrate-mongo

const MONGO_HOSTS = 'MONGO_HOSTS';
const MONGO_USER_PARAM = 'MONGO_USER';
const MONGO_DB_NAME_PARAM = 'MONGO_DB_NAME';

const dbName = config.get(MONGO_DB_NAME_PARAM);
const username = config.get(MONGO_USER_PARAM);
const password = process.env.MONGO_PASSWORD || '';
const dbHosts = config.get(MONGO_HOSTS);
const mongoUri = Util.getMongoUri(username, password, dbName, dbHosts, '');

const auth = username
  ? {
      username,
      password
    }
  : undefined;

const migrationConfig = {
  mongodb: {
    url: mongoUri,

    // TODO Change this to your database name:
    databaseName: dbName,

    options: {
      useNewUrlParser: true, // removes a deprecation warning when connecting
      //   connectTimeoutMS: 3600000, // increase connection timeout to 1 hour
      //   socketTimeoutMS: 3600000, // increase socket timeout to 1 hour
      auth
    }
  },

  // The migrations dir, can be an relative or absolute path. Only edit this when really necessary.
  migrationsDir: `${env === undefined ? 'dist/' : ''}migrations`,

  // The mongodb collection where the applied changes are stored. Only edit this when really necessary.
  changelogCollectionName: 'changelog'
};

//Return the config as a promise
module.exports = migrationConfig;
