# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [7.28.4-5](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.28.4-4...v7.28.4-5) (2023-06-06)

### [7.28.4-4](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.28.4-3...v7.28.4-4) (2023-06-05)

### [7.28.4-3](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.28.4-2...v7.28.4-3) (2023-06-01)

### [7.28.4-2](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.28.4-1...v7.28.4-2) (2023-05-31)

### [7.28.4-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.28.4-0...v7.28.4-1) (2023-05-31)

### [7.28.4-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.28.3...v7.28.4-0) (2023-05-29)

### [7.28.3](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.28.2-1...v7.28.3) (2023-05-25)

### [7.28.2](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.28.2-1...v7.28.2) (2023-05-25)

### [7.28.2-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.28.2-0...v7.28.2-1) (2023-05-23)

### [7.28.2-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.28.1...v7.28.2-0) (2023-05-22)


### Bug Fixes

* Increasing MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION to 250 characters ([6f4a882](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6f4a88220bd860f1ff537d6cee7b8589e4a21120))

### [7.28.1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.28.1-1...v7.28.1) (2023-05-19)

### [7.28.1-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.28.1-0...v7.28.1-1) (2023-05-19)


### Bug Fixes

* update namespace in gitlab-ci ([0eeaec0](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0eeaec0392ed744e9f4e05fa213db1b538cc3db4))

### [7.28.1-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.28.0...v7.28.1-0) (2023-05-19)

## [7.28.0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.27.3...v7.28.0) (2023-05-17)

### [7.27.3](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.27.3-1...v7.27.3) (2023-05-16)

### [7.27.3-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.27.3-0...v7.27.3-1) (2023-05-16)

### [7.27.3-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.27.2...v7.27.3-0) (2023-05-16)


### Bug Fixes

* DP3-581 add default value for bi fast transaction ([27ccca5](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/27ccca59902d281b9825af44963c600db6eea823))

### [7.27.2](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.27.2-1...v7.27.2) (2023-05-12)

### [7.27.2-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.27.2-0...v7.27.2-1) (2023-05-10)

### [7.27.2-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.27.1...v7.27.2-0) (2023-05-10)

### [7.27.1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.27.1-2...v7.27.1) (2023-05-10)

### [7.27.1-2](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.27.1-1...v7.27.1-2) (2023-05-09)

### [7.27.1-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.27.1-0...v7.27.1-1) (2023-05-09)

### [7.27.1-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.27.0...v7.27.1-0) (2023-05-08)

## [7.27.0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.27.0-1...v7.27.0) (2023-05-03)

## [7.27.0-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.8-0...v7.27.0-1) (2023-05-03)

### [7.26.8-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.7...v7.26.8-0) (2023-05-03)


### Bug Fixes

* fixing build:sprint_release job ([bf1c3ac](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/bf1c3ac86feda4241d744b437cd2782e4976e1fb))

### [7.26.7](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.7-0...v7.26.7) (2023-05-02)

### [7.26.7-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.6...v7.26.7-0) (2023-05-02)

### [7.26.6](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.6-0...v7.26.6) (2023-04-28)

### [7.26.6-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.5...v7.26.6-0) (2023-04-28)

### [7.26.5](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.5-1...v7.26.5) (2023-04-28)

### [7.26.5-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.5-0...v7.26.5-1) (2023-04-28)

### [7.26.5-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.4...v7.26.5-0) (2023-04-28)

### [7.26.4](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.3...v7.26.4) (2023-04-27)

### [7.26.3](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.3-1...v7.26.3) (2023-04-26)

### [7.26.3-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.3-0...v7.26.3-1) (2023-04-19)

### [7.26.3-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.1...v7.26.3-0) (2023-04-18)

### [7.26.2](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.1...v7.26.2) (2023-04-18)

### [7.26.2-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.1...v7.26.2-0) (2023-04-17)

### [7.26.1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.1-2...v7.26.1) (2023-04-17)

### [7.26.1-2](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.1-1...v7.26.1-2) (2023-04-17)

### [7.26.1-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.1-0...v7.26.1-1) (2023-04-14)

### [7.26.1-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.0...v7.26.1-0) (2023-04-12)

## [7.26.0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.0-0...v7.26.0) (2023-04-06)

## [7.26.0-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.17-1...v7.26.0-0) (2023-04-05)

### [7.25.17-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.17-0...v7.25.17-1) (2023-04-05)

### [7.25.17-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.16-1...v7.25.17-0) (2023-04-05)

### [7.25.16](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.16-1...v7.25.16) (2023-04-05)

### [7.25.16-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.16-0...v7.25.16-1) (2023-04-05)

### [7.25.16-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.15-3...v7.25.16-0) (2023-04-05)

### [7.25.15](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.15-3...v7.25.15) (2023-04-04)

### [7.25.15-3](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.15-2...v7.25.15-3) (2023-04-03)

### [7.25.15-2](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.15-1...v7.25.15-2) (2023-03-31)

### [7.25.15-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.15-0...v7.25.15-1) (2023-03-31)

### [7.25.15-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.14...v7.25.15-0) (2023-03-30)


### Features

* **LP-426:** update module common version ([37762ca](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/37762ca2cc2a6c38e2e7e827a1cc724efe91f845))

### [7.25.14](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.14-0...v7.25.14) (2023-03-24)

### [7.25.14-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.13...v7.25.14-0) (2023-03-24)

### [7.25.13](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.13-0...v7.25.13) (2023-03-23)

### [7.25.13-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.12...v7.25.13-0) (2023-03-23)

### [7.25.12](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.12-5...v7.25.12) (2023-03-23)

### [7.25.12-5](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.12-4...v7.25.12-5) (2023-03-23)

### [7.25.12-4](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.12-3...v7.25.12-4) (2023-03-23)

### [7.25.12-3](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.12-2...v7.25.12-3) (2023-03-23)

### [7.25.12-2](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.12-1...v7.25.12-2) (2023-03-23)

### [7.25.12-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.12-0...v7.25.12-1) (2023-03-21)

### [7.25.12-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.11...v7.25.12-0) (2023-03-20)

### [7.25.11](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.11-1...v7.25.11) (2023-03-20)

### [7.25.11-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.11-0...v7.25.11-1) (2023-03-20)

### [7.25.11-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.10...v7.25.11-0) (2023-03-20)


### Features

* Add QA env deployment and rename stage name ([73bf7ef](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/73bf7efa983dbd830ed1fc848c0b85b18f4d5bf6))

### [7.25.10](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.10-0...v7.25.10) (2023-03-20)

### [7.25.10-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.9...v7.25.10-0) (2023-03-19)


### Features

* **LP-334:** bump module common to support LOAN_PROCESSOR_GL_ADHOC_INSURANCE interchange ([583ea3d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/583ea3d707ac9cc62c5ed2d72b6b57a996a5c17f))

### [7.25.9](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.9-0...v7.25.9) (2023-03-16)

### [7.25.9-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.8...v7.25.9-0) (2023-03-16)

### [7.25.8](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.8-0...v7.25.8) (2023-03-15)

### [7.25.8-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.7...v7.25.8-0) (2023-03-15)

### [7.25.7](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.7-0...v7.25.7) (2023-03-14)

### [7.25.7-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.6...v7.25.7-0) (2023-03-14)

### [7.25.6](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.6-0...v7.25.6) (2023-03-13)

### [7.25.6-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.5...v7.25.6-0) (2023-03-13)

### [7.25.5](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.5-0...v7.25.5) (2023-03-10)

### [7.25.5-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.4...v7.25.5-0) (2023-03-10)


### Bug Fixes

* **LP-335:** Fix Invalid Booking Date for Backdated Disbursement ([c16ed93](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/c16ed93acd628e26529b8a9fea79137f21385eee))

### [7.25.4](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.3...v7.25.4) (2023-03-10)

### [7.25.3](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.3-0...v7.25.3) (2023-03-10)

### [7.25.3-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.2...v7.25.3-0) (2023-03-09)

## [7.28.0-0](https://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.27.0...v7.28.0-0) (2023-03-09)

## [7.27.0](https://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.27.0-1...v7.27.0) (2023-03-09)

## [7.27.0-1](https://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.27.0-0...v7.27.0-1) (2023-03-09)

## [7.27.0-0](https://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.0-2...v7.27.0-0) (2023-03-09)

## [7.26.0](https://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.0-2...v7.26.0) (2023-03-09)

## [7.26.0-2](https://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.0-1...v7.26.0-2) (2023-03-09)

## [7.26.0-1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.0-0...v7.26.0-1) (2023-03-09)

## [7.26.0-0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.25.2...v7.26.0-0) (2023-03-09)

### [7.25.2](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.0...v7.25.2) (2023-03-08)

## [7.26.0](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v7.26.0-0...v7.26.0) (2023-03-08)

## 7.26.0-0 (2023-03-08)


### Features

* **LP-192:** bump module common to 2.131.0 to support repayment adhoc interchange ([2c5f4b4](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/2c5f4b4c553e4236ff216986026c340261f70df8))


### Bug Fixes

* Broken package-lock.json due to miss in merge process ([9e2f623](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/9e2f6238668db00f43db61f56bea73fffedcb303))
* enable reversal transaction in staging environment ([ab01af5](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/ab01af5d0594f686d84eefd7e1e5043a7ffe1731))

### [7.23.2](https://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.4.0...v7.23.2) (2023-02-15)


### Features

* **BFS-10382:** Deposit Repayment ([caefbb5](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/caefbb5309a2358ec878832fd92409375a10a728))
* **BFS-11104:** Create forward payment transaction ([5026cbe](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/5026cbe5c5842913a26efeb619f56cee86a2c1ec))
* **BFS-11260, BFS-10384:** Loan allowance ([c8d6140](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/c8d61403745333e1e3b7693185f08867fc488c40))
* **BFS-11293:** Allow payoff transaction ms transfer ([0efe758](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0efe7580ec0f758281f40f2370976ac5683e12d8))
* **BFS-12001:** Clawback refund ([e836cd4](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/e836cd4cf82aa734c8840a55049f9f7aea99d6bc))
* **BFS-12706:** Update module common ([3aa7f09](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3aa7f091ec7600f1929f9103e2cd62949f0b1426))
* **LP-192:** bump module common to 2.131.0 to support repayment adhoc interchange ([2c5f4b4](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/2c5f4b4c553e4236ff216986026c340261f70df8))


### Bug Fixes

* Broken package-lock.json due to miss in merge process ([9e2f623](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/9e2f6238668db00f43db61f56bea73fffedcb303))
* fix linter error ([ceb1497](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/ceb149731b01cb52298a3c85bbe8590f4b4aded2))
* **BFS-7712:** Remove BIBIT_INSTANT_REDEMPTION from sourceInquiry ([0042baa](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0042baa27cfe869e06ceeb13fd4b6103c2df24d5))
* **BFS-7712:** Remove BIBIT_INSTANT_REDEMPTION from sourceInquiry- master ([b81ed87](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/b81ed879f0a4ec12bef96b35f8ed2ce2384bea29))
* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### [7.12.1](https://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.4.0...v7.12.1) (2022-09-05)


### Features

* **BFS-10382:** Deposit Repayment ([caefbb5](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/caefbb5309a2358ec878832fd92409375a10a728))
* **BFS-11104:** Create forward payment transaction ([5026cbe](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/5026cbe5c5842913a26efeb619f56cee86a2c1ec))
* **BFS-11260, BFS-10384:** Loan allowance ([c8d6140](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/c8d61403745333e1e3b7693185f08867fc488c40))
* **BFS-11293:** Allow payoff transaction ms transfer ([0efe758](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0efe7580ec0f758281f40f2370976ac5683e12d8))
* **BFS-12001:** Clawback refund ([e836cd4](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/e836cd4cf82aa734c8840a55049f9f7aea99d6bc))
* **BFS-12706:** Update module common ([3aa7f09](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3aa7f091ec7600f1929f9103e2cd62949f0b1426))


### Bug Fixes

* fix linter error ([ceb1497](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/ceb149731b01cb52298a3c85bbe8590f4b4aded2))
* **BFS-7712:** Remove BIBIT_INSTANT_REDEMPTION from sourceInquiry ([0042baa](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0042baa27cfe869e06ceeb13fd4b6103c2df24d5))
* **BFS-7712:** Remove BIBIT_INSTANT_REDEMPTION from sourceInquiry- master ([b81ed87](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/b81ed879f0a4ec12bef96b35f8ed2ce2384bea29))
* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### [7.2.2](https://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.4.0...v7.2.2) (2022-04-25)


### Features

* **BFS-10382:** Deposit Repayment ([caefbb5](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/caefbb5309a2358ec878832fd92409375a10a728))
* **BFS-11104:** Create forward payment transaction ([5026cbe](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/5026cbe5c5842913a26efeb619f56cee86a2c1ec))
* **BFS-11260, BFS-10384:** Loan allowance ([c8d6140](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/c8d61403745333e1e3b7693185f08867fc488c40))
* **BFS-11293:** Allow payoff transaction ms transfer ([0efe758](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0efe7580ec0f758281f40f2370976ac5683e12d8))


### Bug Fixes

* fix linter error ([ceb1497](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/ceb149731b01cb52298a3c85bbe8590f4b4aded2))
* **BFS-7712:** Remove BIBIT_INSTANT_REDEMPTION from sourceInquiry ([0042baa](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0042baa27cfe869e06ceeb13fd4b6103c2df24d5))
* **BFS-7712:** Remove BIBIT_INSTANT_REDEMPTION from sourceInquiry- master ([b81ed87](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/b81ed879f0a4ec12bef96b35f8ed2ce2384bea29))
* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](https://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.224 (2021-12-10)

### 3.4.223 (2021-12-09)

### 3.4.222 (2021-12-03)

### 3.4.221 (2021-12-03)

### 3.4.220 (2021-12-02)

### 3.4.219 (2021-12-01)

### 3.4.218 (2021-11-29)

### 3.4.217 (2021-11-29)

### 3.4.216 (2021-11-25)

### 3.4.215 (2021-11-22)

### 3.4.214 (2021-11-19)

### 3.4.213 (2021-11-19)

### 3.4.212 (2021-11-19)

### 3.4.211 (2021-11-19)

### 3.4.210 (2021-11-19)

### 3.4.209 (2021-11-18)

### 3.4.208 (2021-11-18)

### 3.4.207 (2021-11-18)

### 3.4.206 (2021-11-18)

### 3.4.205 (2021-11-18)

### 3.4.204 (2021-11-17)

### 3.4.203 (2021-11-17)

### 3.4.202 (2021-11-17)

### 3.4.201 (2021-11-17)

### 3.4.200 (2021-11-17)

### 3.4.199 (2021-11-16)

### 3.4.198 (2021-11-16)

### 3.4.197 (2021-11-16)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.196 (2021-11-16)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.195 (2021-11-16)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.194 (2021-11-12)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.193 (2021-11-12)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.192 (2021-11-11)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.191 (2021-11-11)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.190 (2021-11-08)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.189 (2021-11-02)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.188 (2021-10-28)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.187 (2021-10-26)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.186 (2021-10-26)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.185 (2021-10-19)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.184 (2021-10-19)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.183 (2021-10-19)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.182 (2021-10-18)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.181 (2021-10-18)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.180 (2021-10-14)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.179 (2021-10-13)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.178 (2021-10-12)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.177 (2021-10-05)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.176 (2021-10-05)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.175 (2021-10-05)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.174 (2021-10-05)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.173 (2021-10-05)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.172 (2021-10-04)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.171 (2021-09-24)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.170 (2021-09-23)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.169 (2021-09-21)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.168 (2021-09-21)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.167 (2021-09-20)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.166 (2021-09-17)


### Bug Fixes

* Add BRANCH_SKN and BRANCH_RTGS filters in isEligible() ([3b3490d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b3490d2e09e2ee099ac8664469f8755eaafb372))

### 3.4.165 (2021-09-07)

### 3.4.164 (2021-09-06)

### 3.4.163 (2021-09-06)

### 3.4.162 (2021-09-03)

### 3.4.161 (2021-09-03)

### 3.4.160 (2021-09-03)

### 3.4.159 (2021-09-02)

### 3.4.158 (2021-09-02)

### 3.4.157 (2021-08-31)

### 3.4.156 (2021-08-27)

### 3.4.155 (2021-08-26)

### 3.4.154 (2021-08-26)

### 3.4.153 (2021-08-24)

### 3.4.152 (2021-08-16)

### 3.4.151 (2021-08-12)

### 3.4.150 (2021-08-11)

### 3.4.149 (2021-08-10)

### 3.4.148 (2021-08-10)

### 3.4.147 (2021-08-10)

### 3.4.146 (2021-08-09)

### 3.4.145 (2021-08-03)

### 3.4.144 (2021-07-29)

### 3.4.143 (2021-07-29)

### 3.4.142 (2021-07-27)

### 3.4.141 (2021-07-27)

### 3.4.140 (2021-07-27)

### 3.4.139 (2021-07-15)

### 3.4.138 (2021-07-13)

### 3.4.137 (2021-07-12)

### 3.4.136 (2021-07-07)

### 3.4.135 (2021-07-07)

### 3.4.134 (2021-07-05)

### 3.4.133 (2021-07-01)

### 3.4.132 (2021-06-29)

### 3.4.131 (2021-06-29)

### 3.4.130 (2021-06-29)

### 3.4.129 (2021-06-29)

### 3.4.128 (2021-06-24)

### 3.4.127 (2021-06-23)

### 3.4.126 (2021-06-17)

### 3.4.125 (2021-06-15)

### 3.4.124 (2021-06-15)

### 3.4.123 (2021-06-15)

### 3.4.122 (2021-06-11)

### 3.4.121 (2021-06-10)

### 3.4.120 (2021-06-10)

### 3.4.113 (2021-06-07)

### 3.4.112 (2021-05-31)

### 3.4.111 (2021-05-26)

### 3.4.110 (2021-05-25)

### 3.4.109 (2021-05-21)

### 3.4.108 (2021-05-19)

### 3.4.107 (2021-05-18)

### 3.4.106 (2021-05-18)

### 3.4.105 (2021-05-11)

### 3.4.104 (2021-05-07)

### 3.4.103 (2021-05-06)

### 3.4.102 (2021-05-05)

### 3.4.101 (2021-05-04)

### 3.4.100 (2021-05-03)

### 3.4.99 (2021-04-30)

### 3.4.98 (2021-04-30)

### 3.4.97 (2021-04-29)

### 3.4.96 (2021-04-28)

### 3.4.95 (2021-04-27)

### 3.4.94 (2021-04-26)

### 3.4.93 (2021-04-26)

### 3.4.92 (2021-04-22)

### 3.4.91 (2021-04-22)

### 3.4.90 (2021-04-22)

### 3.4.89 (2021-04-21)

### 3.4.88 (2021-04-20)

### 3.4.87 (2021-04-16)

### 3.4.86 (2021-04-15)

### 3.4.85 (2021-04-15)

### 3.4.84 (2021-04-12)

### 3.4.83 (2021-04-09)

### 3.4.82 (2021-04-09)

### 3.4.81 (2021-04-07)

### 3.4.80 (2021-04-06)

### 3.4.79 (2021-04-05)

### 3.4.78 (2021-04-05)

### 3.4.77 (2021-04-01)

### 3.4.76 (2021-03-23)

### 3.4.75 (2021-03-23)

### 3.4.74 (2021-03-22)

### 3.4.73 (2021-03-19)

### 3.4.72 (2021-03-09)

### 3.4.71 (2021-03-09)

### 3.4.70 (2021-03-08)

### 3.4.69 (2021-03-08)

### 3.4.68 (2021-02-23)

### 3.2.140 (2021-02-23)

### 3.2.139 (2021-02-23)

### 3.2.138 (2021-02-23)

### 3.2.137 (2021-02-22)

### 3.2.136 (2021-02-21)

### 3.2.135 (2021-02-19)

### 3.2.134 (2021-02-18)

### 3.2.133 (2021-02-16)

### 3.2.132 (2021-02-16)

### 3.2.131 (2021-02-15)

### 3.2.130 (2021-02-11)

### 3.2.129 (2021-02-05)

### 3.2.128 (2021-02-04)

### 3.2.127 (2021-02-03)

### 3.2.126 (2021-02-03)

### 3.2.125 (2021-02-03)

### 3.2.124 (2021-02-01)

### 3.2.123 (2021-01-29)

### 3.2.122 (2021-01-28)

### 3.2.121 (2021-01-25)

### 3.2.120 (2021-01-25)

### 3.2.119 (2021-01-22)

### 3.2.118 (2021-01-22)

### 3.2.117 (2021-01-20)

### 3.2.116 (2021-01-12)

### 3.2.115 (2021-01-08)

### 3.2.114 (2021-01-08)

### 3.2.113 (2021-01-06)

### 3.2.112 (2021-01-06)

### 3.2.111 (2021-01-04)

### 3.2.110 (2020-12-31)

### 3.2.109 (2020-12-30)

### 3.2.108 (2020-12-24)

### 3.2.107 (2020-12-23)

### 3.2.106 (2020-12-23)

### 3.2.105 (2020-12-23)

### 3.2.104 (2020-12-22)

### 3.2.103 (2020-12-18)

### 3.2.102 (2020-12-17)

### 3.2.101 (2020-12-16)

### 3.2.100 (2020-12-16)

### 3.2.99 (2020-12-11)

### 3.2.98 (2020-12-10)

### 3.2.97 (2020-12-10)

### 3.2.96 (2020-12-10)

### 3.2.95 (2020-12-10)

### 3.2.94 (2020-12-10)

### 3.2.93 (2020-12-08)

### 3.2.92 (2020-12-04)

### 3.2.91 (2020-11-27)

### 3.2.90 (2020-11-26)

### 3.2.89 (2020-11-23)

### 3.2.88 (2020-11-23)

### 3.2.87 (2020-11-19)

### 3.2.86 (2020-11-17)

### 3.2.85 (2020-11-17)

### 3.2.84 (2020-11-15)

### 3.2.83 (2020-11-13)

### 3.2.82 (2020-11-06)

### 3.2.81 (2020-11-05)

### 3.2.80 (2020-11-05)

### 3.2.79 (2020-11-04)

### 3.2.78 (2020-11-03)

### 3.2.77 (2020-11-03)

### 3.2.76 (2020-11-02)

### 3.2.75 (2020-11-02)

### 3.2.74 (2020-11-02)

### 3.2.73 (2020-10-30)

### 3.2.72 (2020-10-29)

### 3.2.71 (2020-10-29)

### 3.2.70 (2020-10-28)

### 3.2.69 (2020-10-27)

### 3.2.68 (2020-10-27)

### 3.2.67 (2020-10-27)

### 3.2.66 (2020-10-26)

### 3.2.65 (2020-10-26)

### 3.2.64 (2020-10-26)

### 3.2.63 (2020-10-26)

### 3.2.62 (2020-10-25)

### 3.2.61 (2020-10-23)

### 3.2.60 (2020-10-23)

### 3.2.59 (2020-10-23)

### 3.2.58 (2020-10-22)

### 3.2.57 (2020-10-22)

### 3.2.56 (2020-10-22)

### 3.2.55 (2020-10-22)

### 3.2.54 (2020-10-22)

### 3.2.53 (2020-10-21)

### 3.2.52 (2020-10-21)

### 3.2.51 (2020-10-20)

### 3.2.50 (2020-10-20)

### 3.2.49 (2020-10-19)

### 3.2.48 (2020-10-19)

### 3.2.47 (2020-10-16)

### 3.2.46 (2020-10-16)

### 3.2.45 (2020-10-16)

### 3.2.44 (2020-10-16)

### 3.2.43 (2020-10-15)

### 3.2.42 (2020-10-14)

### [3.2.41](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.41) (2020-10-13)

### [3.2.40](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.40) (2020-10-12)

### [3.2.39](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.39) (2020-10-12)

### [3.2.38](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.38) (2020-10-09)

### [3.2.37](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.37) (2020-10-08)

### [3.2.36](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.36) (2020-10-08)

### [3.2.35](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.35) (2020-10-07)

### [3.2.34](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.34) (2020-10-06)

### [3.2.33](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.33) (2020-10-05)

### [3.2.32](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.32) (2020-10-01)

### [3.2.31](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.31) (2020-09-29)

### [3.2.30](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.30) (2020-09-29)

### [3.2.29](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.29) (2020-09-29)

### [3.2.28](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.28) (2020-09-28)

### [3.2.27](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.27) (2020-09-28)

### [3.2.26](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.26) (2020-09-25)

### [3.2.25](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.25) (2020-09-24)

### [3.2.24](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.24) (2020-09-24)

### [3.2.23](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.23) (2020-09-24)

### [3.2.22](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.22) (2020-09-23)

### [3.2.21](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.21) (2020-09-23)

### [3.2.20](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.20) (2020-09-23)

### [3.2.19](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.19) (2020-09-23)

### [3.2.18](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.18) (2020-09-23)

### [3.2.16](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.16) (2020-09-23)

### [3.2.15](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.15) (2020-09-23)

### [3.2.14](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.14) (2020-09-22)

### [3.2.13](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.13) (2020-09-22)

### [3.2.12](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.12) (2020-09-22)

### [3.2.11](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.11) (2020-09-21)

### [3.2.10](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.10) (2020-09-19)

### [3.2.9](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.9) (2020-09-19)

### [3.2.8](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.8) (2020-09-19)

### [3.2.7](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.7) (2020-09-15)

### [3.2.5](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.5) (2020-09-15)

### [3.2.4](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.4) (2020-09-15)

### [3.2.3](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.3) (2020-09-14)

### [3.2.1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.2.1) (2020-09-11)

### [3.1.7](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.1.7) (2020-09-07)

### [3.1.14](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.1.14) (2020-09-17)

### [3.1.13](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.1.13) (2020-09-15)

### [3.1.12](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.1.12) (2020-09-14)

### [3.1.11](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.1.11) (2020-09-08)

### [3.1.10](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.1.10) (2020-09-08)

### [3.1.9](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.1.9) (2020-09-04)

### [3.1.8](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.1.8) (2020-09-02)

### [3.1.7](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.1.7) (2020-09-02)

### [3.1.6](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.1.6) (2020-09-02)

### [3.1.5](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.1.5) (2020-09-01)

### [3.1.4](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.1.4) (2020-08-28)

### [3.1.3](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.1.3) (2020-08-28)

### [3.1.2](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v3.1.0...v3.1.2) (2020-08-23)

### [3.0.26](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.26) (2020-08-19)

### [3.0.25](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.25) (2020-08-19)

### [3.0.24](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.24) (2020-08-19)

### [3.0.23](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.23) (2020-08-17)

### [3.0.22](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.22) (2020-08-14)

### [3.0.21](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.21) (2020-08-13)

### [3.0.19](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.19) (2020-08-13)

### [3.0.18](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.18) (2020-08-13)

### [3.0.17](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.17) (2020-08-12)

### [3.0.16](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.16) (2020-08-12)

### [3.0.15](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.15) (2020-08-11)

### [3.0.14](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.14) (2020-08-11)

### [3.0.13](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.13) (2020-08-11)

### [3.0.12](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.12) (2020-08-11)

### [3.0.11](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.11) (2020-08-10)

### [3.0.10](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.10) (2020-08-10)

### [3.0.9](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.9) (2020-08-10)

### [3.0.8](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.8) (2020-08-07)

### [3.0.7](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.7) (2020-08-03)

### [3.0.6](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.6) (2020-08-03)

### [3.0.5](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.5) (2020-08-01)

### [3.0.4](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.4) (2020-07-30)

### [3.0.3](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.3) (2020-07-30)

### [3.0.2](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.2) (2020-07-29)

### [3.0.1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v2.1.0...v3.0.1) (2020-07-28)

### [2.1.2](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v2.1.2) (2020-07-22)

### [2.1.1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v2.1.1) (2020-07-18)

### [1.3.46](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.46) (2020-07-28)

### [1.3.45](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.45) (2020-07-28)

### [1.3.44](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.44) (2020-07-28)

### [1.3.43](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.43) (2020-07-27)

### [1.3.42](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.42) (2020-07-27)

### [1.3.41](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.41) (2020-07-27)

### [1.3.40] (2020-07-25)

### 1.3.39 (2020-07-24)

### 1.3.38 (2020-07-23)

### [1.3.37](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.37) (2020-07-22)

### [1.3.36](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.36) (2020-07-21)

### [1.3.35](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.35) (2020-07-21)

### [1.3.34](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.34) (2020-07-20)

### [1.3.33](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.33) (2020-07-17)

### [1.3.32](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.32) (2020-07-17)

### [1.3.31](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.31) (2020-07-16)

### [1.3.30](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.30) (2020-07-13)

### [1.3.29](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.29) (2020-07-13)

### [1.3.28](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.28) (2020-07-13)

### [1.3.27](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.27) (2020-07-13)

### [1.3.26](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.26) (2020-07-09)

### [1.3.25](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.25) (2020-07-08)

### [1.3.24](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.24) (2020-07-08)

### [1.3.23](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.23) (2020-07-07)

### [1.3.22](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.22) (2020-07-07)

### [1.3.21](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.21) (2020-07-06)

### [1.3.20](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.20) (2020-07-06)

### [1.3.19](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.19) (2020-07-03)

### [1.3.18](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.18) (2020-07-02)

### [1.3.17](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.17) (2020-07-02)

### [1.3.16](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.16) (2020-07-02)

### [1.3.15](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.15) (2020-07-02)

### [1.3.14](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.14) (2020-07-01)

### [1.3.13](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.13) (2020-06-29)

### [1.3.12](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.12) (2020-06-24)

### [1.3.11](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.11) (2020-06-24)

### [1.3.10](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.10) (2020-06-23)

### [1.3.9](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.3.0...v1.3.9) (2020-06-22)

### [1.3.8](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.3.8) (2020-06-19)


### Bug Fixes

* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))

### [1.3.7](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.3.7) (2020-06-19)


### Bug Fixes

* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### [1.3.6](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.3.6) (2020-06-09)


### Bug Fixes

* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### [1.3.5](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.3.5) (2020-06-09)


### Bug Fixes

* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### [1.3.4](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.3.4) (2020-06-05)


### Bug Fixes

* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### [1.3.3](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.3.3) (2020-06-05)


### Bug Fixes

* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### [1.3.2](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.3.2) (2020-06-05)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### [1.3.1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.3.1) (2020-06-04)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### [1.2.61](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.61) (2020-06-04)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### [1.2.60](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.60) (2020-06-02)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### [1.2.59](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.59) (2020-06-02)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.58 (2020-06-02)


### Bug Fixes

* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.57 (2020-06-01)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.56 (2020-06-01)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.55 (2020-05-29)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.54 (2020-05-29)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.53 (2020-05-29)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.52 (2020-05-29)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.51 (2020-05-28)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.50 (2020-05-28)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.49 (2020-05-28)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.48 (2020-05-28)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.47 (2020-05-27)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.46 (2020-05-26)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.45 (2020-05-26)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.44 (2020-05-26)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.43 (2020-05-26)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.42 (2020-05-25)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.41 (2020-05-25)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.40 (2020-05-22)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.39 (2020-05-21)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.38 (2020-05-21)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.37 (2020-05-20)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.36 (2020-05-20)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* update Iris transaction ([3b9368d](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/3b9368df906e71578d9ea10b711689cbfb1c8de6))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.35 (2020-05-19)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.34 (2020-05-19)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))
* Update value of auth from Iris request ([297defc](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/297defccfd1a664f4981950b3586c2d5ba652cb9))

### 1.2.33 (2020-05-18)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))

### 1.2.32 (2020-05-18)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))

### 1.2.31 (2020-05-18)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))

### 1.2.30 (2020-05-18)


### Bug Fixes

* [DGB-9691] Move update thirdPartyOutgoingId to tepmplate ([6d5aa44](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/6d5aa44475a65c46bc522b8e1aa86867fd1d14a6))
* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))

### 1.2.29 (2020-05-18)


### Bug Fixes

* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))

### [1.2.28](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.28) (2020-05-18)


### Bug Fixes

* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))

### [1.2.27](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.27) (2020-05-18)


### Bug Fixes

* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))

### [1.2.26](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.26) (2020-05-18)


### Bug Fixes

* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))

### [1.2.25](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.25) (2020-05-18)


### Bug Fixes

* [DGB-9691] Update wrong path iris ([22d87b2](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/22d87b210be26d9532e35c2c66fed56c89ae4923))

### [1.2.24](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.24) (2020-05-18)

### [1.2.23](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.23) (2020-05-16)

### [1.2.22](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.22) (2020-05-15)

### [1.2.21](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.21) (2020-05-15)

### [1.2.20](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.20) (2020-05-15)

### [1.2.19](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.19) (2020-05-15)

### [1.2.18](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.18) (2020-05-14)

### [1.2.17](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.17) (2020-05-14)

### [1.2.16](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.16) (2020-05-14)

### [1.2.15](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.15) (2020-05-14)

### [1.2.14](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.14) (2020-05-14)

### [1.2.13](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.13) (2020-05-14)

### [1.2.12](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.12) (2020-05-13)

### [1.2.11](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.11) (2020-05-13)

### [1.2.10](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.10) (2020-05-12)

### [1.2.9](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.9) (2020-05-12)

### [1.2.8](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.8) (2020-05-12)

### [1.2.7](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.7) (2020-05-15)

### [1.2.6](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.6) (2020-05-13)

### [1.2.5](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.5) (2020-05-08)

### [1.2.4](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.4) (2020-05-08)

### [1.2.3](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.3) (2020-05-08)

### [1.2.2](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.2) (2020-05-07)

### [1.2.1](http://gitlab.com/dk-digital-bank/services/ms-transfer/compare/v1.2.0...v1.2.1) (2020-05-07)

### 1.1.57 (2020-05-06)

### 1.1.56 (2020-05-06)

### 1.1.55 (2020-05-06)

### 1.1.54 (2020-05-05)

### 1.1.53 (2020-05-05)

### 1.1.52 (2020-05-05)

### 1.1.51 (2020-05-04)

### 1.1.50 (2020-05-04)

### 1.1.49 (2020-05-04)

### 1.1.48 (2020-05-04)

### 1.1.47 (2020-05-04)

### 1.1.46 (2020-05-04)

### 1.1.45 (2020-05-04)

### 1.1.44 (2020-04-30)

### 1.1.43 (2020-04-29)

### 1.1.42 (2020-04-29)

### 1.1.41 (2020-04-29)

### 1.1.40 (2020-04-29)

### 1.1.39 (2020-04-29)

### 1.1.38 (2020-04-28)

### 1.1.37 (2020-04-28)

### 1.1.36 (2020-04-28)

### 1.1.35 (2020-04-28)

### 1.1.34 (2020-04-27)

### 1.1.33 (2020-04-27)

### 1.1.32 (2020-04-24)

### 1.1.31 (2020-04-24)

### 1.1.30 (2020-04-24)

### 1.1.29 (2020-04-24)

### 1.1.28 (2020-04-24)

### 1.1.27 (2020-04-23)

### 1.0.64 (2020-04-07)

### 1.0.63 (2020-04-03)

### 1.0.62 (2020-04-03)

### 1.0.61 (2020-04-03)

### 1.0.60 (2020-04-02)

### 1.0.59 (2020-04-01)

### 1.0.58 (2020-03-31)

### 1.0.57 (2020-03-31)

### 1.0.56 (2020-03-31)

### 1.0.55 (2020-03-31)

### 1.0.54 (2020-03-30)

### 1.0.53 (2020-03-30)

### 1.0.52 (2020-03-30)

### 1.0.51 (2020-03-30)

### 1.0.50 (2020-03-30)

### 1.0.49 (2020-03-27)

### 1.0.48 (2020-03-27)

### 1.0.47 (2020-03-27)

### 1.0.46 (2020-03-27)

### 1.0.45 (2020-03-27)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.44 (2020-03-27)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.43 (2020-03-27)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.42 (2020-03-26)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.41 (2020-03-26)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.40 (2020-03-26)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.39 (2020-03-26)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.38 (2020-03-25)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.37 (2020-03-25)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.36 (2020-03-25)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.35 (2020-03-25)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.34 (2020-03-24)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.33 (2020-03-23)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.32 (2020-03-23)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.31 (2020-03-23)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.30 (2020-03-20)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.29 (2020-03-19)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.28 (2020-03-19)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.27 (2020-03-19)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.26 (2020-03-19)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.25 (2020-03-17)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.24 (2020-03-16)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.23 (2020-03-13)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.22 (2020-03-13)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.21 (2020-03-12)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.20 (2020-03-10)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.19 (2020-03-10)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.18 (2020-03-09)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.17 (2020-03-09)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.16 (2020-03-08)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.15 (2020-03-07)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.14 (2020-03-05)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.13 (2020-03-05)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.12 (2020-03-04)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.11 (2020-03-02)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.10 (2020-03-02)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.9 (2020-02-28)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.8 (2020-02-26)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.7 (2020-02-24)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.6 (2020-02-24)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.5 (2020-02-21)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.4 (2020-02-20)


### Bug Fixes

* use ginBankCode to inquiry institution account ([0b323cb](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/0b323cb76fedbeb2a9ffbe47eb41b2f3d57d5c57))

### 1.0.3 (2020-02-19)

### 1.0.2 (2020-02-18)


### Bug Fixes

* Validate cif if bank code is internal ([ee6db76](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/ee6db763e55f7932167f18f63926bff02b9719d1))

### 1.0.1 (2020-02-18)


### Bug Fixes

* Validate cif if bank code is internal ([ee6db76](http://gitlab.com/dk-digital-bank/services/ms-transfer/commit/ee6db763e55f7932167f18f63926bff02b9719d1))
