let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');
const getTransactionPayload = () => ({
  beneficiaryBankCode: utilConstants.rtolCode.VISA,
  transactionAmount: utilConstants.transactionAmount.ATM_WITHDRAWAL_AMOUNT,
  sourceBankCode: utilConstants.bankCodes.INTERNAL,
  sourceAccountNo: config.mainAccountRegistered,
  paymentServiceType:
    utilConstants.paymentServiceType.THIRD_PARTY_ATM_WITHDRAWAL,
  sourceAccountName: 'Quasar QA',
  transactionDate: '201010154410',
  currency: utilConstants.currency.MAMBU_CODE,
  beneficiaryAccountName: 'Rahul Tiwari',
  externalId: generateRandomAlphanumeric(12),
  interchange: utilConstants.interchange.ARTAJASA,
  notes: '',
  additionalInformation1: 'Quasar Squad',
  additionalInformation2: 'Quasar QA',
  additionalInformation3: 'Rahul Test',
  additionalInformation4: 'Jakarta'
});

const transactionPayloadList = [
  [
    'Third Party ATM Withdrawal from MA account and ARTAJASA interchange from other bank ATM',
    {
      sourceAccountNo: config.mainAccountRegistered,
      interchange: utilConstants.interchange.ARTAJASA,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeMandiri,
      notes: utilConstants.notes.HUNDRED_CHAR,
      paymentServiceCode:
        utilConstants.paymentServiceCode.THIRDPARTY_CASH_WITHDRAW
    }
  ],
  [
    'Third Party ATM Withdrawal from DC account and ARTAJASA interchange from other bank ATM',
    {
      sourceAccountNo: config.dcAccountRegistered,
      interchange: utilConstants.interchange.ARTAJASA,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeMandiri,
      notes: utilConstants.notes.HUNDRED_CHAR,
      paymentServiceCode:
        utilConstants.paymentServiceCode.THIRDPARTY_CASH_WITHDRAW
    }
  ],
  [
    'Third Party ATM Withdrawal using ALTO Interchange and MA account at other bank ATM',
    {
      sourceAccountNo: config.mainAccountRegistered,
      interchange: utilConstants.interchange.ALTO,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeMandiri,
      notes: utilConstants.notes.BLANK_SPACE,
      paymentServiceCode:
        utilConstants.paymentServiceCode.THIRDPARTY_CASH_WITHDRAW
    }
  ],
  [
    'Third Party ATM Withdrawal using ALTO Interchange  and DC account at other bank ATM',
    {
      sourceAccountNo: config.mainAccountRegistered,
      interchange: utilConstants.interchange.ALTO,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeMandiri,
      notes: utilConstants.notes.EMPTY,
      paymentServiceCode:
        utilConstants.paymentServiceCode.THIRDPARTY_CASH_WITHDRAW
    }
  ],
  [
    'Third Party ATM Withdrawal using ARTAJASA Interchange and MA Account at Jago bank ATM',
    {
      sourceAccountNo: config.mainAccountRegistered,
      interchange: utilConstants.interchange.ARTAJASA,
      beneficiaryBankCode: utilConstants.bankCodes.BIBIT,
      notes: utilConstants.notes.TEST_NOTES,
      paymentServiceCode: utilConstants.paymentServiceCode.JAGOATM_CASH_WITHDRAW
    }
  ],
  [
    'Third Party ATM Withdrawal using ARTAJASA Interchange and DC Account at Jago bank ATM',
    {
      sourceAccountNo: config.dcAccountRegistered,
      interchange: utilConstants.interchange.ARTAJASA,
      beneficiaryBankCode: utilConstants.bankCodes.BIBIT,
      notes: utilConstants.notes.TEST_NOTES,
      paymentServiceCode: utilConstants.paymentServiceCode.JAGOATM_CASH_WITHDRAW
    }
  ],
  [
    'Third Party ATM Withdrawal using ALTO Interchange and DC Account at Jago bank ATM',
    {
      sourceAccountNo: config.dcAccountRegistered,
      interchange: utilConstants.interchange.ALTO,
      beneficiaryBankCode: utilConstants.bankCodes.BIBIT,
      notes: utilConstants.notes.EMPTY,
      paymentServiceCode: utilConstants.paymentServiceCode.JAGOATM_CASH_WITHDRAW
    }
  ],
  [
    'Third Party ATM Withdrawal using ALTO Interchange and MA Account at Jago bank ATM',
    {
      sourceAccountNo: config.mainAccountRegistered,
      interchange: utilConstants.interchange.ALTO,
      beneficiaryBankCode: utilConstants.bankCodes.BIBIT,
      notes: utilConstants.notes.HUNDRED_CHAR,
      paymentServiceCode: utilConstants.paymentServiceCode.JAGOATM_CASH_WITHDRAW
    }
  ]
];

const errorPayloadList = [
  [
    'Error case: Third Party ATM Withdrawal with notes more than hundred character',
    {
      sourceAccountNo: config.mainAccountRegistered,
      interchange: utilConstants.interchange.ARTAJASA,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeMandiri,
      notes: utilConstants.notes.MORE_THAN_HUNDRED_CHAR,
      transactionAmount: utilConstants.transactionAmount.ATM_WITHDRAWAL_AMOUNT,
      errorMessage: 'Invalid request',
      errorCode: 'INVALID_REQUEST'
    }
  ],
  [
    'Error case: Third Party ATM Withdrawal with VISA interchange',
    {
      sourceAccountNo: config.mainAccountRegistered,
      interchange: utilConstants.interchange.VISA,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeMandiri,
      notes: utilConstants.notes.TEST_NOTES,
      transactionAmount: utilConstants.transactionAmount.ATM_WITHDRAWAL_AMOUNT,
      errorMessage: 'Debit configuration for interchange not found',
      errorCode: 'DEBIT_INTERCHANGE_NOT_FOUND'
    }
  ],
  [
    'Error case: Third Party ATM Withdrawal with amount of ten thousand IDR',
    {
      sourceAccountNo: config.mainAccountRegistered,
      interchange: utilConstants.interchange.VISA,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeMandiri,
      notes: utilConstants.notes.TEST_NOTES,
      transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
      errorMessage: 'Invalid amount',
      errorCode: 'INVALID_AMOUNT'
    }
  ],
  [
    'Error case: Third Party ATM Withdrawal with amount of one trillion IDR',
    {
      sourceAccountNo: config.mainAccountRegistered,
      interchange: utilConstants.interchange.VISA,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeMandiri,
      notes: utilConstants.notes.TEST_NOTES,
      transactionAmount: utilConstants.transactionAmount.TRILLION,
      errorMessage: 'No recommendation services',
      errorCode: 'NO_RECOMMENDATION_SERVICES'
    }
  ]
];

module.exports = {
  getTransactionPayload,
  transactionPayloadList,
  errorPayloadList
};
