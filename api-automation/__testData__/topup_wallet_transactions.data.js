let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const topUpWalletReqResExpResultList = [
  [
    'IRIS_JAGO_TO_OTH-IRIS: GoPay Wallet top-up : Blank notes',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 120001,
      beneficiaryBankCode: utilConstants.bankCodes.GOPAY,
      beneficiaryAccountNo: config.goPayWalletSuccess,
      externalId: generateRandomAlphanumeric(12),
      paymentServiceType: 'WALLET',
      paymentServiceCode: 'IRIS_JAGO_TO_OTH',
      categoryCode: 'C058',
      note: utilConstants.notes.BLANK_SPACE
    },
    ['id', 'externalId', 'referenceId', 'transactionIds', 'transactionDate'],
    {
      beneficiaryAccountNo: config.goPayWalletSuccess,
      beneficiaryBankName: 'GoPay',
      beneficiaryBankCodeChannel: 'IRIS',
      debitPriority: 'IRIS',
      paymentServiceType: 'WALLET',
      status: 'SUCCEED'
    }
  ],
  [
    'IRIS_JAGO_TO_OTH-IRIS: GoPay Wallet top-up : Declined transaction',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 120001,
      beneficiaryBankCode: utilConstants.bankCodes.GOPAY,
      beneficiaryAccountNo: config.goPayWalletDeclined,
      externalId: generateRandomAlphanumeric(12),
      paymentServiceType: 'WALLET',
      paymentServiceCode: 'IRIS_JAGO_TO_OTH',
      categoryCode: 'C058',
      note: utilConstants.notes.BLANK_SPACE
    },
    ['id', 'externalId', 'referenceId', 'transactionIds', 'transactionDate'],
    {
      beneficiaryAccountNo: config.goPayWalletDeclined,
      beneficiaryBankName: 'GoPay',
      beneficiaryBankCodeChannel: 'IRIS',
      debitPriority: 'IRIS',
      paymentServiceType: 'WALLET',
      status: 'DECLINED'
    }
  ],
  [
    'RTOL_WALLET-EXTERNAL: DANA Wallet top-up',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 10000,
      beneficiaryBankCode: utilConstants.bankCodes.DANA,
      beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      externalId: generateRandomAlphanumeric(12),
      paymentServiceType: 'WALLET',
      paymentServiceCode: 'RTOL_WALLET',
      categoryCode: 'C058',
      note: utilConstants.notes.TEST_NOTES
    },
    ['id', 'externalId', 'referenceId', 'transactionIds', 'transactionDate'],
    {
      beneficiaryBankName: 'DANA',
      beneficiaryBankCodeChannel: 'EXTERNAL',
      debitPriority: 'ARTAJASA',
      paymentServiceType: 'WALLET',
      status: 'SUCCEED'
    }
  ],
  [
    'RTOL_WALLET-EXTERNAL: LinkAja Wallet top-up',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 10000,
      beneficiaryBankCode: utilConstants.bankCodes.LINKAJA,
      beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      externalId: generateRandomAlphanumeric(12),
      paymentServiceType: 'WALLET',
      paymentServiceCode: 'RTOL_WALLET',
      categoryCode: 'C058',
      note: utilConstants.notes.TEST_NOTES
    },
    ['id', 'externalId', 'referenceId', 'transactionIds', 'transactionDate'],
    {
      beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      beneficiaryBankName: 'LinkAja',
      beneficiaryBankCodeChannel: 'EXTERNAL',
      debitPriority: 'ARTAJASA',
      paymentServiceType: 'WALLET',
      status: 'SUCCEED'
    }
  ],
  [
    'RTOL_WALLET-EXTERNAL: ShopeePay Wallet top-up',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 10000,
      beneficiaryBankCode: utilConstants.bankCodes.SHOPEEPAY,
      beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      externalId: generateRandomAlphanumeric(12),
      paymentServiceType: 'WALLET',
      paymentServiceCode: 'RTOL_WALLET',
      categoryCode: 'C058',
      note: utilConstants.notes.TEST_NOTES
    },
    ['id', 'externalId', 'referenceId', 'transactionIds', 'transactionDate'],
    {
      beneficiaryBankName: 'ShopeePay',
      beneficiaryBankCodeChannel: 'EXTERNAL',
      debitPriority: 'ARTAJASA',
      paymentServiceType: 'WALLET',
      status: 'SUCCEED'
    }
  ],
  [
    'RTOL_WALLET-EXTERNAL: Ovo Wallet top-up: Blank notes',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 10000,
      beneficiaryBankCode: utilConstants.bankCodes.OVO,
      beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      paymentServiceType: 'WALLET',
      externalId: generateRandomAlphanumeric(12),
      paymentServiceCode: 'RTOL_WALLET',
      categoryCode: 'C058',
      note: utilConstants.notes.BLANK_SPACE
    },
    ['id', 'externalId', 'referenceId', 'transactionIds', 'transactionDate'],
    {
      beneficiaryBankName: 'OVO',
      beneficiaryBankCodeChannel: 'EXTERNAL',
      debitPriority: 'ARTAJASA',
      paymentServiceType: 'WALLET',
      status: 'SUCCEED'
    }
  ]
];

const BadRequestNExpectedResultList = [
  [
    'Error : IRIS_JAGO_TO_OTH-IRIS : GoPay Invalid amount >10M',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 10000001,
      beneficiaryBankCode: utilConstants.bankCodes.GOPAY,
      beneficiaryAccountNo: config.goPayWalletSuccess,
      paymentServiceType: 'WALLET',
      externalId: generateRandomAlphanumeric(12),
      paymentServiceCode: 'IRIS_JAGO_TO_OTH',
      categoryCode: 'C058'
    },
    {
      message: 'No recommendation services',
      code: 'NO_RECOMMENDATION_SERVICES',
      errors: [
        {
          message: 'Failed while evaluating Payment Config Rules.',
          key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
          code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
        }
      ]
    }
  ],
  [
    'Error: RTOL_WALLET-EXTERNAL: GoPay Wallet top-up <10K',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 9999,
      beneficiaryBankCode: utilConstants.bankCodes.GOPAY,
      beneficiaryAccountNo: config.goPayWalletSuccess,
      paymentServiceType: 'WALLET',
      externalId: generateRandomAlphanumeric(12),
      paymentServiceCode: 'IRIS_JAGO_TO_OTH',
      categoryCode: 'C058'
    },
    {
      message: 'Invalid amount',
      code: 'INVALID_AMOUNT',
      errors: [
        {
          message: 'Transaction amount is limited',
          code: 'INVALID_LIMIT_TRANSACTION_AMOUNT',
          key: ''
        }
      ]
    }
  ],
  [
    'Error: RTOL_WALLET-EXTERNAL: DANA Wallet top-up <10K',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 9999,
      beneficiaryBankCode: utilConstants.bankCodes.DANA,
      beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      paymentServiceType: 'WALLET',
      externalId: generateRandomAlphanumeric(12),
      paymentServiceCode: 'RTOL_WALLET',
      categoryCode: 'C058'
    },
    {
      message: 'Invalid amount',
      code: 'INVALID_AMOUNT',
      errors: [
        {
          message: 'Transaction amount is limited',
          code: 'INVALID_LIMIT_TRANSACTION_AMOUNT',
          key: ''
        }
      ]
    }
  ],
  [
    'Error: RTOL_WALLET-EXTERNAL: LinkAja Wallet top-up <10K',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 9999,
      beneficiaryBankCode: utilConstants.bankCodes.LINKAJA,
      beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      paymentServiceType: 'WALLET',
      externalId: generateRandomAlphanumeric(12),
      paymentServiceCode: 'RTOL_WALLET',
      categoryCode: 'C058'
    },
    {
      message: 'Invalid amount',
      code: 'INVALID_AMOUNT',
      errors: [
        {
          message: 'Transaction amount is limited',
          code: 'INVALID_LIMIT_TRANSACTION_AMOUNT',
          key: ''
        }
      ]
    }
  ],
  [
    'Error: RTOL_WALLET-EXTERNAL: ShopeePay Wallet top-up <10K',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 9999,
      beneficiaryBankCode: utilConstants.bankCodes.SHOPEEPAY,
      beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      paymentServiceType: 'WALLET',
      externalId: generateRandomAlphanumeric(12),
      paymentServiceCode: 'RTOL_WALLET',
      categoryCode: 'C058'
    },
    {
      message: 'Invalid amount',
      code: 'INVALID_AMOUNT',
      errors: [
        {
          message: 'Transaction amount is limited',
          code: 'INVALID_LIMIT_TRANSACTION_AMOUNT',
          key: ''
        }
      ]
    }
  ],
  [
    'Error: RTOL_WALLET-EXTERNAL: Ovo Wallet top-up <20K',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 19999,
      beneficiaryBankCode: utilConstants.bankCodes.OVO,
      beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      paymentServiceType: 'WALLET',
      externalId: generateRandomAlphanumeric(12),
      paymentServiceCode: 'RTOL_WALLET',
      categoryCode: 'C058'
    },
    {
      message: 'Invalid amount',
      code: 'INVALID_AMOUNT',
      errors: [
        {
          message: 'Transaction amount is limited',
          code: 'INVALID_LIMIT_TRANSACTION_AMOUNT',
          key: ''
        }
      ]
    }
  ]
];
module.exports = {
  topUpWalletReqResExpResultList,
  BadRequestNExpectedResultList
};
