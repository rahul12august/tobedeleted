let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const getInternalTransactionBasePayload = {
  sourceCIF: config.sourceCif,
  sourceBankCode: utilConstants.bankCodes.INTERNAL,
  sourceAccountNo: config.mainAccountRegistered,
  transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT,
  beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
  beneficiaryAccountNo: config.anotherMainAccount,
  paymentServiceType: 'TRANSFER',
  externalId: generateRandomAlphanumeric(12),
  categoryCode: 'C001'
};

const internalPaymentPayloadListDifferentCIF = [
  [
    'MA to MA',
    config.mainAccountRegistered,
    utilConstants.transactionAmount.MIN_AMOUNT,
    config.anotherMainAccount,
    generateRandomAlphanumeric(12)
  ],
  [
    'MA to FS',
    config.mainAccountRegistered,
    utilConstants.transactionAmount.MIN_AMOUNT,
    config.otherJagoFS,
    generateRandomAlphanumeric(12)
  ],
  [
    'MA to DC',
    config.mainAccountRegistered,
    utilConstants.transactionAmount.MIN_AMOUNT,
    config.otherJagoDC,
    generateRandomAlphanumeric(12)
  ],
  [
    'DC to MA',
    config.dcAccountRegistered,
    utilConstants.transactionAmount.MIN_AMOUNT,
    config.anotherMainAccount,
    generateRandomAlphanumeric(12)
  ],
  [
    'DC to FS',
    config.dcAccountRegistered,
    utilConstants.transactionAmount.MIN_AMOUNT,
    config.otherJagoFS,
    generateRandomAlphanumeric(12)
  ],
  [
    'DC to DC',
    config.dcAccountRegistered,
    utilConstants.transactionAmount.MIN_AMOUNT,
    config.otherJagoDC,
    generateRandomAlphanumeric(12)
  ],
  [
    'DC to GA',
    config.dcAccountRegistered,
    utilConstants.transactionAmount.MIN_AMOUNT,
    config.anotherCustomerGA,
    generateRandomAlphanumeric(12)
  ],
  [
    'MA to GA',
    config.dcAccountRegistered,
    utilConstants.transactionAmount.MIN_AMOUNT,
    config.anotherCustomerGA,
    generateRandomAlphanumeric(12)
  ]
];

const internalPaymentPayloadListSameCIF = [
  [
    'MA to DC',
    config.mainAccountRegistered,
    utilConstants.transactionAmount.MIN_AMOUNT,
    config.dcAccountRegistered,
    generateRandomAlphanumeric(12),
    'SAT01'
  ],
  [
    'MA to FS',
    config.mainAccountRegistered,
    utilConstants.transactionAmount.MIN_AMOUNT,
    config.fsAccountRegistered,
    generateRandomAlphanumeric(12),
    'SAT01'
  ],
  [
    'DC to MA',
    config.dcAccountRegistered,
    utilConstants.transactionAmount.MIN_AMOUNT,
    config.mainAccountRegistered,
    generateRandomAlphanumeric(12),
    'SAT02'
  ],
  [
    'DC to FS',
    config.dcAccountRegistered,
    utilConstants.transactionAmount.MIN_AMOUNT,
    config.fsAccountRegistered,
    generateRandomAlphanumeric(12),
    'SAT05'
  ],
  [
    'FS to MA',
    config.fsAccountRegistered,
    utilConstants.transactionAmount.MIN_AMOUNT,
    config.mainAccountRegistered,
    generateRandomAlphanumeric(12),
    'SAT02'
  ]
];

const minAmountTransferValidationListSameCIF = [
  [
    'DC to MA same CIF for Amount 0.01',
    config.dcAccountRegistered,
    utilConstants.transactionAmount.MIN_CLOSING_POCKET_AMOUNT,
    config.fsAccountRegistered,
    generateRandomAlphanumeric(12),
    'SAT05'
  ],
  [
    'FS to MA same CIF for Amount 0.01',
    config.fsAccountRegistered,
    utilConstants.transactionAmount.MIN_CLOSING_POCKET_AMOUNT,
    config.mainAccountRegistered,
    generateRandomAlphanumeric(12),
    'SAT02'
  ]
];

module.exports = {
  getInternalTransactionBasePayload,
  internalPaymentPayloadListDifferentCIF,
  internalPaymentPayloadListSameCIF,
  minAmountTransferValidationListSameCIF
};
