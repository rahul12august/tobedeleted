let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');
const getTransactionPayload = () => ({
  beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
  transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT,
  sourceBankCode: utilConstants.bankCodes.GOPAY,
  paymentServiceType: utilConstants.paymentServiceType.IRIS_TO_JAGO,
  transactionDate: '201010154410',
  currency: utilConstants.currency.MAMBU_CODE,
  externalId: generateRandomAlphanumeric(12),
  beneficiaryAccountNo: config.mainAccountRegistered,
  beneficiaryAccountName: 'Rahul',
  interchange: 'ARTAJASA',
  notes: '212b2e921111'
});

const transactionPayloadList = [
  [
    'Gopay withdrawal from Gopay wallet to MA account type',
    {
      beneficiaryAccountNo: config.mainAccountRegistered
    }
  ],
  [
    'Gopay withdrawal from Gopay wallet to FS account type',
    {
      beneficiaryAccountNo: config.fsAccountRegistered
    }
  ],
  [
    'Gopay withdrawal from Gopay wallet to to DC account type',
    {
      beneficiaryAccountNo: config.dcAccountRegistered
    }
  ],
  [
    'Gopay withdrawal from Gopay wallet to to GA account type',
    {
      beneficiaryAccountNo: config.anotherCustomerGA
    }
  ]
];

const errorPayloadList = [
  [
    'Error case: Gopay withdrawal amount less than one IDR',
    {
      beneficiaryAccountNo: config.mainAccountRegistered,
      transactionAmount:
        utilConstants.transactionAmount.MIN_CLOSING_POCKET_AMOUNT,
      errorMessage: 'Invalid amount',
      errorCode: 'INVALID_AMOUNT'
    }
  ],
  [
    'Error case: Gopay withdrawal amount more than max limit',
    {
      beneficiaryAccountNo: config.mainAccountRegistered,
      transactionAmount:
        utilConstants.transactionAmount.GREATER_THAN_MAX_WALLET_AMOUNT,
      errorMessage: 'No recommendation services',
      errorCode: 'NO_RECOMMENDATION_SERVICES'
    }
  ]
];

module.exports = {
  getTransactionPayload,
  transactionPayloadList,
  errorPayloadList
};
