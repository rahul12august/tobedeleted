let config = require('config');
const { utilConstants } = require('../utils/constants');
const { generateRandomAlphanumeric } = require('../utils/common_functions');

const rdnTransactionsList = [
  [
    'RDN-RDN_KSEI : Min transaction amount = 1',
    {
      rdnTransactionRequest: {
        sourceCIF: config.cifMBA,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        sourceAccountNo: config.accountNumberMBA,
        transactionAmount: 1,
        beneficiaryBankCode: utilConstants.bankCodes.bankCodeKSEI,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        beneficiaryAccountName: 'RDN_KSEI Account',
        paymentServiceType: 'RDN',
        externalId: generateRandomAlphanumeric(12),
        categoryCode: 'C056',
        paymentServiceCode: 'RDN_KSEI'
      },
      rdnTransactionResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      rdnTransactionDetailsDb: {
        paymentServiceCode: 'RDN_KSEI',
        paymentServiceType: 'RDN',
        transactionAmount: 1,
        beneficiaryBankName: 'KSEI',
        debitLimitGroupCode: 'L017',
        status: 'SUCCEED',
        interchange: utilConstants.interchange.RTGS
      }
    }
  ],
  [
    'RDN-RDN_KSEI : RTGS min transaction amount = 100M1',
    {
      rdnTransactionRequest: {
        sourceCIF: config.cifMBA,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        sourceAccountNo: config.accountNumberMBA,
        transactionAmount: 100000001,
        beneficiaryBankCode: utilConstants.bankCodes.bankCodeKSEI,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        beneficiaryAccountName: 'RDN_KSEI Account',
        paymentServiceType: 'RDN',
        externalId: generateRandomAlphanumeric(12),
        categoryCode: 'C056',
        paymentServiceCode: 'RDN_KSEI'
      },
      rdnTransactionResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      rdnTransactionDetailsDb: {
        paymentServiceCode: 'RDN_KSEI',
        paymentServiceType: 'RDN',
        transactionAmount: 100000001,
        beneficiaryBankName: 'KSEI',
        debitLimitGroupCode: 'L017',
        status: 'SUCCEED',
        interchange: utilConstants.interchange.RTGS
      }
    }
  ],
  [
    'RDN-RDN_SELL : min transaction amount = 1',
    {
      rdnTransactionRequest: {
        sourceCIF: config.cifMBA,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        sourceAccountNo: config.accountNumberMBA,
        transactionAmount: 1,
        beneficiaryBankCode: utilConstants.bankCodes.bankCodeJago,
        beneficiaryAccountNo: config.accountNumberRdn,
        paymentServiceType: 'RDN',
        externalId: generateRandomAlphanumeric(12),
        categoryCode: 'C040',
        paymentServiceCode: 'RDN_SELL'
      },
      rdnTransactionResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      rdnTransactionDetailsDb: {
        paymentServiceCode: 'RDN_SELL',
        paymentServiceType: 'RDN',
        transactionAmount: 1,
        beneficiaryBankName: 'Jago',
        beneficiaryAccountType: 'RDN',
        status: 'SUCCEED'
      }
    }
  ],
  [
    'RDN-RDN_BUY : min transaction amount = 1',
    {
      rdnTransactionRequest: {
        sourceCIF: config.cifRDN,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        sourceAccountNo: config.accountNumberRdn,
        transactionAmount: 1,
        beneficiaryBankCode: utilConstants.bankCodes.bankCodeJago,
        beneficiaryAccountNo: config.accountNumberMBA,
        paymentServiceType: 'RDN',
        externalId: generateRandomAlphanumeric(12),
        categoryCode: 'C040',
        paymentServiceCode: 'RDN_BUY'
      },
      rdnTransactionResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      rdnTransactionDetailsDb: {
        paymentServiceCode: 'RDN_BUY',
        paymentServiceType: 'RDN',
        transactionAmount: 1,
        beneficiaryBankName: 'Jago',
        beneficiaryAccountType: 'MBA',
        status: 'SUCCEED'
      }
    }
  ],
  [
    'RDN-RDN_WITHDRAW_RTGS : min transaction amount = 500M1',
    {
      rdnTransactionRequest: {
        sourceCIF: config.cifRDN,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        sourceAccountNo: config.accountNumberRdn,
        transactionAmount: 500000001,
        beneficiaryBankCode: utilConstants.bankCodes.bankCodeExternal,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: 'RDN',
        externalId: generateRandomAlphanumeric(12),
        categoryCode: 'C040',
        paymentServiceCode: 'RDN_WITHDRAW_RTGS'
      },
      rdnTransactionResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      rdnTransactionDetailsDb: {
        paymentServiceCode: 'RDN_WITHDRAW_RTGS',
        paymentServiceType: 'RDN',
        transactionAmount: 500000001,
        beneficiaryBankCodeChannel: 'EXTERNAL',
        status: 'SUCCEED',
        interchange: utilConstants.interchange.RTGS
      }
    }
  ],
  [
    'RDN-RDN_WITHDRAW_SKN : min transaction amount = 1',
    {
      rdnTransactionRequest: {
        sourceCIF: config.cifRDN,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        sourceAccountNo: config.accountNumberRdn,
        transactionAmount: 1,
        beneficiaryBankCode: utilConstants.bankCodes.bankCodeExternal,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: 'RDN',
        externalId: generateRandomAlphanumeric(12),
        categoryCode: 'C040',
        paymentServiceCode: 'RDN_WITHDRAW_SKN',
        additionalInformation1: '1',
        additionalInformation2: '1'
      },
      rdnTransactionResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      rdnTransactionDetailsDb: {
        paymentServiceCode: 'RDN_WITHDRAW_SKN',
        paymentServiceType: 'RDN',
        transactionAmount: 1,
        beneficiaryBankCodeChannel: 'EXTERNAL',
        status: 'SUCCEED',
        interchange: utilConstants.interchange.SKN
      }
    }
  ],
  [
    'RDN-RDN_WITHDRAW_SKN : transaction amount = 100M1',
    {
      rdnTransactionRequest: {
        sourceCIF: config.cifRDN,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        sourceAccountNo: config.accountNumberRdn,
        transactionAmount: 100000001,
        beneficiaryBankCode: utilConstants.bankCodes.bankCodeExternal,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: 'RDN',
        externalId: generateRandomAlphanumeric(12),
        categoryCode: 'C040',
        paymentServiceCode: 'RDN_WITHDRAW_SKN',
        additionalInformation1: '1',
        additionalInformation2: '1'
      },
      rdnTransactionResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      rdnTransactionDetailsDb: {
        paymentServiceCode: 'RDN_WITHDRAW_SKN',
        paymentServiceType: 'RDN',
        transactionAmount: 100000001,
        beneficiaryBankCodeChannel: 'EXTERNAL',
        status: 'SUCCEED',
        interchange: utilConstants.interchange.SKN
      }
    }
  ],
  [
    'RDN-RDN_WITHDRAW : RDN to MBA : min transaction amount',
    {
      rdnTransactionRequest: {
        sourceCIF: config.cifRDN,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        sourceAccountNo: config.accountNumberRdn,
        transactionAmount: 1,
        beneficiaryBankCode: utilConstants.bankCodes.bankCodeJago,
        beneficiaryAccountNo: config.accountNumberMBA,
        paymentServiceType: 'RDN',
        externalId: generateRandomAlphanumeric(12),
        categoryCode: 'C040',
        paymentServiceCode: 'RDN_WITHDRAW'
      },
      rdnTransactionResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      rdnTransactionDetailsDb: {
        paymentServiceCode: 'RDN_WITHDRAW',
        paymentServiceType: 'RDN',
        transactionAmount: 1,
        beneficiaryBankName: 'Jago',
        status: 'SUCCEED'
      }
    }
  ],
  [
    'RDN-RDN_WITHDRAW : RDN to MA account',
    {
      rdnTransactionRequest: {
        sourceCIF: config.cifRDN,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        sourceAccountNo: config.accountNumberRdn,
        transactionAmount: 100000001,
        beneficiaryBankCode: utilConstants.bankCodes.bankCodeJago,
        beneficiaryAccountNo: config.mainAccountRegistered,
        paymentServiceType: 'RDN',
        externalId: generateRandomAlphanumeric(12),
        categoryCode: 'C040',
        paymentServiceCode: 'RDN_WITHDRAW'
      },
      rdnTransactionResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      rdnTransactionDetailsDb: {
        paymentServiceCode: 'RDN_WITHDRAW',
        paymentServiceType: 'RDN',
        transactionAmount: 100000001,
        beneficiaryBankName: 'Jago',
        beneficiaryAccountType: 'MA',
        status: 'SUCCEED'
      }
    }
  ]
];

const errorScenariosList = [
  [
    'Error : RDN_KSEI : Bank code other than KSEI (BC1000)',
    {
      sourceCIF: config.cifMBA,
      sourceBankCode: utilConstants.bankCodes.bankCodeJago,
      sourceAccountNo: config.accountNumberMBA,
      transactionAmount: 1,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeExternal,
      beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      beneficiaryAccountName: 'RDN_KSEI Account',
      paymentServiceType: 'RDN',
      externalId: generateRandomAlphanumeric(12),
      categoryCode: 'C056',
      paymentServiceCode: 'RDN_KSEI'
    },
    [
      {
        message: 'Failed while submitting to thirdparty.',
        code: 'THIRDPARTY_SUBMISSION_FAILED'
      }
    ]
  ],
  [
    'Error : RDN_KSEI : Invalid source account type = MA',
    {
      sourceCIF: config.sourceCif,
      sourceBankCode: utilConstants.bankCodes.bankCodeJago,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 1,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeExternal,
      beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      beneficiaryAccountName: 'RDN_KSEI Account',
      paymentServiceType: 'RDN',
      externalId: generateRandomAlphanumeric(12),
      categoryCode: 'C056',
      paymentServiceCode: 'RDN_KSEI'
    },
    [
      {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    ]
  ],
  [
    'Error : RDN_KSEI : Invalid payment service type = TRANSFER',
    {
      sourceCIF: config.sourceCif,
      sourceBankCode: utilConstants.bankCodes.bankCodeJago,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 1,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeExternal,
      beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      beneficiaryAccountName: 'RDN_KSEI Account',
      paymentServiceType: 'TRANSFER',
      externalId: generateRandomAlphanumeric(12),
      categoryCode: 'C056',
      paymentServiceCode: 'RDN_KSEI'
    },
    [
      {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    ]
  ],
  [
    'Error : RDN_WITHDRAW : Invalid source account type = MA',
    {
      sourceCIF: config.sourceCif,
      sourceBankCode: utilConstants.bankCodes.bankCodeJago,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 1,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeJago,
      beneficiaryAccountNo: config.accountNumberMBA,
      paymentServiceType: 'RDN',
      externalId: generateRandomAlphanumeric(12),
      categoryCode: 'C056',
      paymentServiceCode: 'RDN_WITHDRAW'
    },
    [
      {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    ]
  ],
  [
    'Error : RDN_WITHDRAW : Invalid scenario : Transfer to external bank',
    {
      sourceCIF: config.cifRDN,
      sourceBankCode: utilConstants.bankCodes.bankCodeJago,
      sourceAccountNo: config.accountNumberRdn,
      transactionAmount: 1,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeExternal,
      beneficiaryAccountNo: config.beneficiaryAccountNo,
      paymentServiceType: 'RDN',
      externalId: generateRandomAlphanumeric(12),
      categoryCode: 'C040',
      paymentServiceCode: 'RDN_WITHDRAW'
    },
    [
      {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    ]
  ],
  [
    'Error : RDN_WITHDRAW_SKN : Invalid source account type = MA',
    {
      sourceCIF: config.sourceCif,
      sourceBankCode: utilConstants.bankCodes.bankCodeJago,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 1,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeJago,
      beneficiaryAccountNo: config.accountNumberMBA,
      paymentServiceType: 'RDN',
      externalId: generateRandomAlphanumeric(12),
      categoryCode: 'C040',
      paymentServiceCode: 'RDN_WITHDRAW_SKN'
    },
    [
      {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    ]
  ],
  [
    'Error : RDN_WITHDRAW_RTGS: Invalid source account type = MA',
    {
      sourceCIF: config.sourceCif,
      sourceBankCode: utilConstants.bankCodes.bankCodeJago,
      sourceAccountNo: config.mainAccountRegistered,
      transactionAmount: 500000001,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeJago,
      beneficiaryAccountNo: config.accountNumberMBA,
      paymentServiceType: 'RDN',
      externalId: generateRandomAlphanumeric(12),
      categoryCode: 'C040',
      paymentServiceCode: 'RDN_WITHDRAW_RTGS'
    },
    [
      {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    ]
  ],
  [
    'Error : RDN_WITHDRAW_RTGS: Invalid amount < 500M1',
    {
      sourceCIF: config.sourceCif,
      sourceBankCode: utilConstants.bankCodes.bankCodeJago,
      sourceAccountNo: config.accountNumberRdn,
      transactionAmount: 500000000,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeExternal,
      beneficiaryAccountNo: config.accountNumberMBA,
      paymentServiceType: 'RDN',
      externalId: generateRandomAlphanumeric(12),
      categoryCode: 'C040',
      paymentServiceCode: 'RDN_WITHDRAW_RTGS'
    },
    [
      {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    ]
  ],
  [
    'Error : RDN_SELL : Invalid source account type = RDN',
    {
      sourceCIF: config.cifRDN,
      sourceBankCode: utilConstants.bankCodes.bankCodeJago,
      sourceAccountNo: config.accountNumberRdn,
      transactionAmount: 1,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeJago,
      beneficiaryAccountNo: config.otherAccountNumberRdn,
      paymentServiceType: 'RDN',
      externalId: generateRandomAlphanumeric(12),
      categoryCode: 'C040',
      paymentServiceCode: 'RDN_SELL'
    },
    [
      {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    ]
  ],
  [
    'Error : RDN_SELL : Invalid beneficiary account type = MBA',
    {
      sourceCIF: config.cifMBA,
      sourceBankCode: utilConstants.bankCodes.bankCodeJago,
      sourceAccountNo: config.accountNumberMBA,
      transactionAmount: 1,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeJago,
      beneficiaryAccountNo: config.otherAccountNumberMBA,
      paymentServiceType: 'RDN',
      externalId: generateRandomAlphanumeric(12),
      categoryCode: 'C040',
      paymentServiceCode: 'RDN_SELL'
    },
    [
      {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    ]
  ],
  [
    'Error : RDN_SELL : Same beneficiary & source account',
    {
      sourceCIF: config.cifMBA,
      sourceBankCode: utilConstants.bankCodes.bankCodeJago,
      sourceAccountNo: config.accountNumberMBA,
      transactionAmount: 1,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeJago,
      beneficiaryAccountNo: config.accountNumberMBA,
      paymentServiceType: 'RDN',
      externalId: generateRandomAlphanumeric(12),
      categoryCode: 'C040',
      paymentServiceCode: 'RDN_SELL'
    },
    [
      {
        message: 'Invalid account',
        code: 'INVALID_ACCOUNT'
      }
    ]
  ],
  [
    'Error : RDN_BUY : Invalid beneficiary account type = RDN',
    {
      sourceCIF: config.cifRDN,
      sourceBankCode: utilConstants.bankCodes.bankCodeJago,
      sourceAccountNo: config.accountNumberRdn,
      transactionAmount: 1,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeJago,
      beneficiaryAccountNo: config.otherAccountNumberRdn,
      paymentServiceType: 'RDN',
      externalId: generateRandomAlphanumeric(12),
      categoryCode: 'C040',
      paymentServiceCode: 'RDN_BUY'
    },
    [
      {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    ]
  ],
  [
    'Error : RDN_BUY : Invalid source account type = MBA',
    {
      sourceCIF: config.cifMBA,
      sourceBankCode: utilConstants.bankCodes.bankCodeJago,
      sourceAccountNo: config.accountNumberMBA,
      transactionAmount: 1,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeJago,
      beneficiaryAccountNo: config.otherAccountNumberRdn,
      paymentServiceType: 'RDN',
      externalId: generateRandomAlphanumeric(12),
      categoryCode: 'C040',
      paymentServiceCode: 'RDN_BUY'
    },
    [
      {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    ]
  ],
  [
    'Error : RDN_BUY : Same beneficiary & source account',
    {
      sourceCIF: config.cifRDN,
      sourceBankCode: utilConstants.bankCodes.bankCodeJago,
      sourceAccountNo: config.accountNumberRdn,
      transactionAmount: 1,
      beneficiaryBankCode: utilConstants.bankCodes.bankCodeJago,
      beneficiaryAccountNo: config.accountNumberRdn,
      paymentServiceType: 'RDN',
      externalId: generateRandomAlphanumeric(12),
      categoryCode: 'C040',
      paymentServiceCode: 'RDN_BUY'
    },
    [
      {
        message: 'Invalid account',
        code: 'INVALID_ACCOUNT'
      }
    ]
  ]
];
module.exports = {
  rdnTransactionsList,
  errorScenariosList
};
