let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');
const getCashbackPayload = () => ({
  sourceCIF: config.sourceCif,
  sourceBankCode: utilConstants.bankCodes.INTERNAL,
  transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT,
  beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
  beneficiaryAccountNo: config.anotherMainAccount,
  paymentServiceType: 'CASHBACK',
  externalId: generateRandomAlphanumeric(12),
  interchange: ''
});

const paymentPayload = [
  [
    'GIVEAWAY Interchange - MA Account',
    {
      interchange: utilConstants.interchange.GIVEAWAY,
      beneficiaryAccountNo: config.anotherMainAccount,
      externalId: generateRandomAlphanumeric(12)
    }
  ],
  [
    'PARTNER Interchange - MA Account',
    {
      interchange: utilConstants.interchange.PARTNER,
      beneficiaryAccountNo: config.anotherMainAccount,
      externalId: generateRandomAlphanumeric(12)
    }
  ],
  [
    'JAGO Interchange - MA Account',
    {
      interchange: utilConstants.interchange.JAGO,
      beneficiaryAccountNo: config.anotherMainAccount,
      externalId: generateRandomAlphanumeric(12)
    }
  ],
  [
    'VISA Interchange - MA Account',
    {
      interchange: utilConstants.interchange.VISA,
      beneficiaryAccountNo: config.anotherMainAccount,
      externalId: generateRandomAlphanumeric(12)
    }
  ],
  [
    'PARTNER_REIMB_RRA Interchange - MA Account',
    {
      interchange: utilConstants.interchange.PARTNER_REIMB_RRA,
      beneficiaryAccountNo: config.anotherMainAccount,
      externalId: generateRandomAlphanumeric(12)
    }
  ],
  [
    'GIVEAWAY Interchange - DC Account',
    {
      interchange: utilConstants.interchange.GIVEAWAY,
      beneficiaryAccountNo: config.dcAccountRegistered,
      externalId: generateRandomAlphanumeric(12)
    }
  ],
  [
    'PARTNER Interchange - DC Account',
    {
      interchange: utilConstants.interchange.PARTNER,
      beneficiaryAccountNo: config.dcAccountRegistered,
      externalId: generateRandomAlphanumeric(12)
    }
  ],
  [
    'JAGO Interchange - DC Account',
    {
      interchange: utilConstants.interchange.JAGO,
      beneficiaryAccountNo: config.dcAccountRegistered,
      externalId: generateRandomAlphanumeric(12)
    }
  ],
  [
    'VISA Interchange - DC Account',
    {
      interchange: utilConstants.interchange.VISA,
      beneficiaryAccountNo: config.dcAccountRegistered,
      externalId: generateRandomAlphanumeric(12)
    }
  ],
  [
    'PARTNER_REIMB_RRA Interchange - DC Account',
    {
      interchange: utilConstants.interchange.PARTNER_REIMB_RRA,
      beneficiaryAccountNo: config.dcAccountRegistered,
      externalId: generateRandomAlphanumeric(12)
    }
  ]
];

module.exports = {
  getCashbackPayload,
  paymentPayload
};
