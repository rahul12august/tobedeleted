let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const getInternationalPaymentBasePayload = () => ({
  beneficiaryBankCode: utilConstants.rtolCode.BNI,
  transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
  sourceBankCode: config.sourceBankCode,
  sourceAccountNo: config.mainAccountRegistered,
  paymentServiceType: 'INTERNATIONAL_PAYMENT',
  transactionDate: '210710154410',
  currency: utilConstants.currency.MAMBU_CODE,
  beneficiaryAccountName: 'Rahul Auto Test',
  externalId: generateRandomAlphanumeric(12),
  interchange: utilConstants.interchange.ALTO,
  notes: ''
});

const getInternationalVoidPaymentBasePayload = () => ({
  beneficiaryBankCode: config.sourceBankCode,
  transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
  sourceBankCode: utilConstants.rtolCode.BNI,
  beneficiaryAccountNo: config.mainAccountRegistered,
  paymentServiceType: 'INTERNATIONAL_VOID_PAYMENT',
  transactionDate: '210710154410',
  currency: utilConstants.currency.MAMBU_CODE,
  beneficiaryAccountName: 'Rahul Auto Test',
  externalId: generateRandomAlphanumeric(12),
  interchange: utilConstants.interchange.ALTO,
  notes: ''
});

const internationalPaymentInternalToExternalPayload = [
  [
    'POS_VISA',
    utilConstants.bankCodes.INTERNAL,
    config.mainAccountRegistered,
    utilConstants.rtolCode.BNI,
    config.externalAccountNumber,
    utilConstants.interchange.VISA
  ],
  [
    'POS_VISA',
    utilConstants.bankCodes.INTERNAL,
    config.dcAccountRegistered,
    utilConstants.rtolCode.BNI,
    config.externalAccountNumber,
    utilConstants.interchange.VISA
  ]
];

module.exports = {
  internationalPaymentInternalToExternalPayload,
  getInternationalPaymentBasePayload,
  getInternationalVoidPaymentBasePayload
};
