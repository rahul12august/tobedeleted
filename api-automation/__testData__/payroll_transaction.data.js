let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');
const getPayrollPayload = () => ({
  sourceCIF: config.sourceCif,
  sourceBankCode: utilConstants.bankCodes.INTERNAL,
  transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT,
  beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
  beneficiaryAccountNo: config.anotherMainAccount,
  paymentServiceType: 'PAYROLL',
  externalId: generateRandomAlphanumeric(12),
  paymentServiceCode: 'PAYROLL',
  interchange: utilConstants.interchange.DK
});

const payrollPayloadList = [
  [
    'DK Interchange',
    {
      interchange: utilConstants.interchange.DK,
      beneficiaryAccountNo: config.mainAccountRegistered,
      transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT
    }
  ],
  [
    'JAGO Interchange',
    {
      interchange: utilConstants.interchange.JAGO,
      beneficiaryAccountNo: config.mainAccountRegistered,
      transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT
    }
  ],
  [
    'AMAAN Interchange - MA Account',
    {
      interchange: utilConstants.interchange.AMAAN,
      beneficiaryAccountNo: config.mainAccountRegistered,
      transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT
    }
  ]
];

const errorPayrollInputList = [
  [
    'Error in transferring to DC Account',
    {
      interchange: utilConstants.interchange.DK,
      beneficiaryAccountNo: config.dcAccountRegistered,
      transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT,
      errorMessage: 'No recommendation services',
      errorCode: 'NO_RECOMMENDATION_SERVICES'
    }
  ],
  [
    'Error in transferring to FS Account',
    {
      interchange: utilConstants.interchange.JAGO,
      beneficiaryAccountNo: config.fsAccountRegistered,
      transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT,
      errorMessage: 'No recommendation services',
      errorCode: 'NO_RECOMMENDATION_SERVICES'
    }
  ],
  [
    'Error in case of any other interchange',
    {
      interchange: utilConstants.interchange.ALTO,
      beneficiaryAccountNo: config.mainAccountRegistered,
      transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT,
      errorMessage: 'Credit configuration for interchange not found',
      errorCode: 'CREDIT_INTERCHANGE_NOT_FOUND'
    }
  ],
  [
    'Error in case of amount above threshold',
    {
      interchange: utilConstants.interchange.DK,
      beneficiaryAccountNo: config.mainAccountRegistered,
      transactionAmount: utilConstants.transactionAmount.ERR_PAYROLL_AMOUNT,
      errorMessage: 'Invalid amount',
      errorCode: 'INVALID_AMOUNT'
    }
  ]
];

module.exports = {
  getPayrollPayload,
  payrollPayloadList,
  errorPayrollInputList
};
