let config = require('config');
const { utilConstants } = require('../utils/constants');

const recommendedServiceList = [
  [
    'CREDIT_CARD-RTOL_CC : Payment Service Recommendation, Pin Auth',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 120001,
      beneficiaryCIF: null,
      beneficiaryBankCode: utilConstants.bankCodes.CREDIT_CARD,
      beneficiaryAccountNo: utilConstants.accountNumber.CREDIT_CARD,
      paymentServiceType: 'CREDIT_CARD'
    },

    {
      paymentServiceCode: 'RTOL_CC',
      debitTransactionCode: 'BID02',
      creditTransactionCode: null,
      realBeneficiaryBankCode: '023',
      customerTc: 'FTD40',
      authenticationRequired: 'PIN'
    }
  ],
  [
    'CREDIT_CARD-RTOL_CC : Payment Service Recommendation, Password Auth',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 1000000,
      beneficiaryCIF: null,
      beneficiaryBankCode: utilConstants.bankCodes.CREDIT_CARD,
      beneficiaryAccountNo: utilConstants.accountNumber.CREDIT_CARD,
      paymentServiceType: 'CREDIT_CARD'
    },

    {
      paymentServiceCode: 'RTOL_CC',
      debitTransactionCode: 'BID02',
      creditTransactionCode: null,
      realBeneficiaryBankCode: '023',
      customerTc: 'FTD40',
      authenticationRequired: 'PASSWORD'
    }
  ],
  [
    'TRANSFER-RTOL : Payment Service Recommendation',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 100002,
      beneficiaryCIF: null,
      beneficiaryBankCode: utilConstants.bankCodes.CREDIT_CARD,
      beneficiaryAccountNo: utilConstants.accountNumber.CREDIT_CARD,
      paymentServiceType: 'TRANSFER'
    },

    {
      paymentServiceCode: 'RTOL',
      debitTransactionCode: 'TFD30',
      creditTransactionCode: null,
      realBeneficiaryBankCode: '023',
      customerTc: 'FTD40',
      authenticationRequired: 'PIN'
    }
  ],
  [
    'TRANSFER-SKN : Payment Service Recommendation',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 50100002,
      beneficiaryCIF: null,
      beneficiaryBankCode: utilConstants.bankCodes.CREDIT_CARD,
      beneficiaryAccountNo: utilConstants.accountNumber.CREDIT_CARD,
      paymentServiceType: 'TRANSFER'
    },

    {
      paymentServiceCode: 'SKN',
      debitTransactionCode: 'TFD50',
      creditTransactionCode: null,
      realBeneficiaryBankCode: 'BBIJIDJA',
      customerTc: 'FTD50',
      authenticationRequired: 'PASSWORD'
    }
  ],
  [
    'TRANSFER-RTGS : Payment Service Recommendation',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 1000000000,
      beneficiaryCIF: null,
      beneficiaryBankCode: utilConstants.bankCodes.CREDIT_CARD,
      beneficiaryAccountNo: config.otherJagoMA,
      paymentServiceType: 'TRANSFER'
    },

    {
      paymentServiceCode: 'RTGS',
      debitTransactionCode: 'TFD60',
      creditTransactionCode: null,
      realBeneficiaryBankCode: 'BBIJIDJA',
      feeAmount: 25000,
      customerTc: 'FTD60',
      authenticationRequired: 'PASSWORD'
    }
  ],
  [
    'TRANSFER-SKN+RTGS : Payment Service Recommendation',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 100000001,
      beneficiaryCIF: null,
      beneficiaryBankCode: utilConstants.bankCodes.CREDIT_CARD,
      beneficiaryAccountNo: utilConstants.accountNumber.CREDIT_CARD,
      paymentServiceType: 'TRANSFER'
    },
    [
      {
        paymentServiceCode: 'RTGS',
        debitTransactionCode: 'TFD60',
        creditTransactionCode: null,
        realBeneficiaryBankCode: 'BBIJIDJA',
        feeAmount: 25000,
        customerTc: 'FTD60',
        authenticationRequired: 'PASSWORD'
      },
      {
        paymentServiceCode: 'SKN',
        debitTransactionCode: 'TFD50',
        creditTransactionCode: null,
        realBeneficiaryBankCode: 'BBIJIDJA',
        feeAmount: 0,
        customerTc: 'FTD50',
        authenticationRequired: 'PASSWORD'
      }
    ]
  ],
  [
    'TRANSFER-SIT01 : Payment Service Recommendation : Jago MA → Oth Jago MA',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 10,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      beneficiaryAccountNo: config.otherJagoMA,
      paymentServiceType: 'TRANSFER'
    },

    {
      paymentServiceCode: 'SIT01',
      debitTransactionCode: 'TFD20',
      creditTransactionCode: 'TFC20',
      realBeneficiaryBankCode: '542',
      feeAmount: null,
      customerTc: null,
      authenticationRequired: 'PIN'
    }
  ],
  [
    'TRANSFER-SIT01 : Payment Service Recommendation : Jago MA → Oth Jago FS, Pin Auth',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 101,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      beneficiaryAccountNo: config.otherJagoFS,
      paymentServiceType: 'TRANSFER'
    },

    {
      paymentServiceCode: 'SIT01',
      debitTransactionCode: 'TFD20',
      creditTransactionCode: 'TFC20',
      realBeneficiaryBankCode: '542',
      feeAmount: null,
      customerTc: null,
      authenticationRequired: 'PIN'
    }
  ],
  [
    'TRANSFER-SIT01 : Payment Service Recommendation : Jago MA → Oth Jago DC : Pass Auth',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 1000000,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      beneficiaryAccountNo: config.otherJagoDC,
      paymentServiceType: 'TRANSFER'
    },

    {
      paymentServiceCode: 'SIT01',
      debitTransactionCode: 'TFD20',
      creditTransactionCode: 'TFC20',
      realBeneficiaryBankCode: '542',
      feeAmount: null,
      customerTc: null,
      authenticationRequired: 'PASSWORD'
    }
  ],
  [
    'TRANSFER-SAT02 : Payment Service Recommendation : Jago DC → Jago MA',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.dcAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 0.1,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      beneficiaryAccountNo: config.mainAccountRegistered,
      paymentServiceType: 'TRANSFER'
    },

    {
      paymentServiceCode: 'SAT02',
      debitTransactionCode: 'SAD02',
      creditTransactionCode: 'SAC02',
      realBeneficiaryBankCode: '542',
      feeAmount: null,
      customerTc: null,
      authenticationRequired: 'NONE'
    }
  ],
  [
    'TRANSFER-SAT05 : Payment Service Recommendation : Jago DC → Jago FS',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.dcAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 1,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      beneficiaryAccountNo: config.fsAccountRegistered,
      paymentServiceType: 'TRANSFER'
    },

    {
      paymentServiceCode: 'SAT05',
      debitTransactionCode: 'SAD03',
      creditTransactionCode: 'SAC03',
      realBeneficiaryBankCode: '542',
      feeAmount: null,
      customerTc: null,
      authenticationRequired: 'NONE'
    }
  ],
  [
    'TRANSFER-SIT01 : Payment Service Recommendation : Jago DC → Oth Jago MA',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.dcAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 1000000,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      beneficiaryAccountNo: config.anotherMainAccount,
      paymentServiceType: 'TRANSFER'
    },

    {
      paymentServiceCode: 'SIT01',
      debitTransactionCode: 'TFD20',
      creditTransactionCode: 'TFC20',
      realBeneficiaryBankCode: '542',
      feeAmount: null,
      customerTc: null,
      authenticationRequired: 'PASSWORD'
    }
  ],
  [
    'TRANSFER-SAT01 : Payment Service Recommendation : Jago MA → Self Jago DC',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 1000000,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      beneficiaryAccountNo: config.dcAccountRegistered,
      paymentServiceType: 'TRANSFER'
    },

    {
      paymentServiceCode: 'SAT01',
      debitTransactionCode: 'SAD01',
      creditTransactionCode: 'SAC01',
      realBeneficiaryBankCode: '542',
      feeAmount: null,
      customerTc: null,
      authenticationRequired: 'NONE'
    }
  ],
  [
    'TRANSFER-SAT01 : Payment Service Recommendation : Jago MA → Self Jago FS',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 1000000,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      beneficiaryAccountNo: config.fsAccountRegistered,
      paymentServiceType: 'TRANSFER'
    },

    {
      paymentServiceCode: 'SAT01',
      debitTransactionCode: 'SAD01',
      creditTransactionCode: 'SAC01',
      realBeneficiaryBankCode: '542',
      feeAmount: null,
      customerTc: null,
      authenticationRequired: 'NONE'
    }
  ],

  [
    'WALLET-RTOL_WALLET : Payment Service Recommendation : PasswordAuth 10M',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 10000000,
      beneficiaryCIF: null,
      beneficiaryBankCode: 'BC173',
      beneficiaryAccountNo: '1234567890',
      paymentServiceType: 'WALLET'
    },

    {
      paymentServiceCode: 'RTOL_WALLET',
      debitTransactionCode: 'WTD02',
      creditTransactionCode: null,
      realBeneficiaryBankCode: '013',
      customerTc: 'FWD02',
      authenticationRequired: 'PASSWORD'
    }
  ],
  [
    'WALLET-IRIS_JAGO_TO_OTH : GoPay wallet Service Recommendation : PasswordAuth 5M',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 5000001,
      beneficiaryCIF: null,
      beneficiaryBankCode: 'BC194',
      beneficiaryAccountNo: '08123450000',
      externalId: '8293323916968',
      paymentServiceType: 'WALLET'
    },

    {
      paymentServiceCode: 'IRIS_JAGO_TO_OTH',
      debitTransactionCode: 'TFD70',
      creditTransactionCode: null,
      realBeneficiaryBankCode: 'gopay',
      feeAmount: null,
      customerTc: null,
      authenticationRequired: 'PASSWORD'
    }
  ],
  [
    'THIRD_PARTY_TRANSFER-THIRDPARTY_JAGO_TO_OTH : Jago to Other Payment Service Recommendation : PasswordAuth',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 25000000,
      beneficiaryCIF: null,
      beneficiaryBankCode: 'BC059',
      beneficiaryAccountNo: '1234567890',
      paymentServiceType: 'THIRD_PARTY_TRANSFER'
    },

    {
      paymentServiceCode: 'THIRDPARTY_JAGO_TO_OTH',
      debitTransactionCode: 'TFD31',
      creditTransactionCode: null,
      realBeneficiaryBankCode: '146',
      feeAmount: 6500,
      customerTc: 'FTD31',
      authenticationRequired: 'PASSWORD'
    }
  ]
];

const BadRequestRecommendedService = [
  [
    'Error : TRANSFER-RTOL : Invalid amount <10k',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 9999,
      beneficiaryCIF: null,
      beneficiaryBankCode: utilConstants.bankCodes.CREDIT_CARD,
      beneficiaryAccountNo: utilConstants.accountNumber.CREDIT_CARD,
      paymentServiceType: 'TRANSFER'
    },
    [
      {
        message: 'Invalid amount',
        code: 'INVALID_AMOUNT',
        errors: [
          {
            message: 'Transaction amount is limited',
            code: 'INVALID_LIMIT_TRANSACTION_AMOUNT',
            key: ''
          }
        ]
      }
    ]
  ],
  [
    'Error : THIRD_PARTY_TRANSFER-THIRDPARTY_JAGO_TO_OTH : Invalid amount Jago to Other >25M',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 25000001,
      beneficiaryCIF: null,
      beneficiaryBankCode: 'BC059',
      beneficiaryAccountNo: '1234567890',
      paymentServiceType: 'THIRD_PARTY_TRANSFER'
    },
    [
      {
        message: 'Invalid amount',
        code: 'INVALID_AMOUNT',
        errors: [
          {
            message: 'Transaction amount is limited',
            code: 'INVALID_LIMIT_TRANSACTION_AMOUNT',
            key: ''
          }
        ]
      }
    ]
  ],
  [
    'Error: TRANSFER-RTGS : No Recommended Services >1000M',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 1000000001,
      beneficiaryCIF: null,
      beneficiaryBankCode: utilConstants.bankCodes.CREDIT_CARD,
      beneficiaryAccountNo: utilConstants.accountNumber.CREDIT_CARD,
      paymentServiceType: 'TRANSFER'
    },
    [
      {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    ]
  ],
  [
    'Error: CREDIT_CARD-RTOL_CC : No Recommended Services >50M',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 50000001,
      beneficiaryCIF: null,
      beneficiaryBankCode: utilConstants.bankCodes.CREDIT_CARD,
      beneficiaryAccountNo: utilConstants.accountNumber.CREDIT_CARD,
      paymentServiceType: 'CREDIT_CARD'
    },
    [
      {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    ]
  ],
  [
    'Error: WALLET-IRIS_JAGO_TO_OTH : No Recommended Services : GoPay Wallet >50M',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 50000001,
      beneficiaryCIF: null,
      beneficiaryBankCode: 'BC194',
      beneficiaryAccountNo: '08123450000',
      externalId: '8293323916968',
      paymentServiceType: 'WALLET'
    },
    [
      {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    ]
  ],
  [
    'Error : TRANSFER-SIT01 : Invalid amount = 0.1 : Jago To Jago',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 0.1,
      beneficiaryCIF: null,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      beneficiaryAccountNo: config.otherJagoMA,
      paymentServiceType: 'TRANSFER'
    },
    [
      {
        message: 'Invalid amount',
        code: 'INVALID_AMOUNT',
        errors: [
          {
            message: 'Transaction amount is limited',
            code: 'INVALID_LIMIT_TRANSACTION_AMOUNT',
            key: ''
          }
        ]
      }
    ]
  ],
  [
    'Error : TRANSFER-SAT05 : Invalid amount = 0.1 : Pocket To Pocket',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.dcAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 0.1,
      beneficiaryCIF: null,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      beneficiaryAccountNo: config.fsAccountRegistered,
      paymentServiceType: 'TRANSFER'
    },
    [
      {
        message: 'Invalid amount',
        code: 'INVALID_AMOUNT',
        errors: [
          {
            message: 'Transaction amount is limited',
            code: 'INVALID_LIMIT_TRANSACTION_AMOUNT',
            key: ''
          }
        ]
      }
    ]
  ],
  [
    'Error: TRANSFER-SIT01 : Benificiary Account enquiry failure: Jago to Jago',
    {
      sourceCIF: config.sourceCif,
      sourceAccountNo: config.mainAccountRegistered,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 10,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      beneficiaryAccountNo: '100311721906',
      paymentServiceType: 'TRANSFER'
    },
    [
      {
        message: 'Inquiry for beneficiary account failed.',
        code: 'BENEFICIARY_ACCOUNT_INQUIRY_FAILED',
        errors: [
          {
            message: 'AccountNo is not registered',
            key: 'BENEFICIARY_ACCOUNT_INQUIRY_FAILED',
            code: 'ACCOUNT_NUMBER_NOT_FOUND'
          }
        ]
      }
    ]
  ],
  [
    'Error: TRANSFER-SIT01 : Invalid Amount: Jago to Jago >10,000M',
    {
      sourceCIF: '8a85c240766dff3f0176744d4a6c241e',
      sourceAccountNo: '100888104592',
      sourceBankCode: 'BC002',
      transactionAmount: 10000000000,
      beneficiaryBankCode: 'BC002',
      beneficiaryAccountNo: '100127612366',
      paymentServiceType: 'TRANSFER'
    },
    [
      {
        message: 'Invalid amount',
        code: 'INVALID_AMOUNT',
        errors: [
          {
            message: 'Transaction amount is limited',
            code: 'INVALID_LIMIT_TRANSACTION_AMOUNT',
            key: ''
          }
        ]
      }
    ]
  ]
];
module.exports = {
  recommendedServiceList,
  BadRequestRecommendedService
};
