let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');
const getTransactionPayload = () => ({
  beneficiaryBankCode: utilConstants.rtolCode.VISA,
  transactionAmount: utilConstants.transactionAmount.ATM_WITHDRAWAL_AMOUNT,
  sourceBankCode: utilConstants.bankCodes.INTERNAL,
  sourceAccountNo: config.mainAccountRegistered,
  paymentServiceType: 'INTERNATIONAL_ATM_WITHDRAWAL',
  transactionDate: '201010154410',
  currency: utilConstants.currency.MAMBU_CODE,
  beneficiaryAccountName: 'Rahul Tiwari',
  externalId: generateRandomAlphanumeric(12),
  interchange: utilConstants.interchange.ARTAJASA,
  notes: '',
  additionalInformation1: 'Quasar Squad',
  additionalInformation2: 'Quasar QA',
  additionalInformation3: 'Rahul Test',
  additionalInformation4: 'Jakarta'
});

const transactionPayloadList = [
  [
    'International ATM Withdrawal from MA at other bank ATM',
    {
      sourceAccountNo: config.mainAccountRegistered,
      interchange: utilConstants.interchange.ARTAJASA
    }
  ],
  [
    'International ATM Withdrawal from DC at other bank ATM',
    {
      sourceAccountNo: config.dcAccountRegistered,
      interchange: utilConstants.interchange.ARTAJASA
    }
  ],
  [
    'International ATM Withdrawal using ALTO Interchange at other bank ATM',
    {
      sourceAccountNo: config.mainAccountRegistered,
      interchange: utilConstants.interchange.ALTO
    }
  ],
  [
    'International ATM Withdrawal using VISA Interchange at other bank ATM',
    {
      sourceAccountNo: config.mainAccountRegistered,
      interchange: utilConstants.interchange.VISA
    }
  ],
  [
    'International ATM Withdrawal using ARTAJASA Interchange at other bank ATM',
    {
      sourceAccountNo: config.mainAccountRegistered,
      interchange: utilConstants.interchange.ARTAJASA
    }
  ]
];

module.exports = {
  getTransactionPayload,
  transactionPayloadList
};
