let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');
const getTransactionPayload = () => ({
  beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
  transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT,
  sourceBankCode: utilConstants.rtolCode.BNI,
  paymentServiceType: utilConstants.paymentServiceType.INCOMING_RTGS,
  sourceAccountName: 'Quasar QA',
  transactionDate: '201010154410',
  currency: utilConstants.currency.MAMBU_CODE,
  externalId: generateRandomAlphanumeric(12),
  beneficiaryAccountNo: config.mainAccountRegistered
});

const transactionPayloadList = [
  [
    'Incoming RTGS transaction from other bank to MA account type',
    {
      beneficiaryAccountNo: config.mainAccountRegistered
    }
  ],
  [
    'Incoming RTGS transaction from other bank to FS account type',
    {
      beneficiaryAccountNo: config.fsAccountRegistered
    }
  ],
  [
    'Incoming RTGS transaction from other bank to DC account type',
    {
      beneficiaryAccountNo: config.dcAccountRegistered
    }
  ],
  [
    'Incoming RTGS transaction from other bank to GA account type',
    {
      beneficiaryAccountNo: config.anotherCustomerGA
    }
  ]
];

const errorPayloadList = [
  [
    'Error case: Incoming RTGS with amount less than one IDR',
    {
      beneficiaryAccountNo: config.mainAccountRegistered,
      transactionAmount:
        utilConstants.transactionAmount.MIN_CLOSING_POCKET_AMOUNT,
      errorMessage: 'Invalid amount',
      errorCode: 'INVALID_AMOUNT'
    }
  ],
  [
    'Error case: Incoming RTGS with amount more than RTGS limit',
    {
      beneficiaryAccountNo: config.mainAccountRegistered,
      transactionAmount: utilConstants.transactionAmount.ERR_PAYROLL_AMOUNT,
      errorMessage: 'Invalid amount',
      errorCode: 'INVALID_AMOUNT'
    }
  ]
];

module.exports = {
  getTransactionPayload,
  transactionPayloadList,
  errorPayloadList
};
