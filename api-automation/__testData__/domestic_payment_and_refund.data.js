let config = require('config');
const { utilConstants } = require('../utils/constants');
const { generateRandomAlphanumeric } = require('../utils/common_functions');

const getDomesticPaymentBasePayload = () => ({
  beneficiaryBankCode: utilConstants.rtolCode.BNI,
  transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
  sourceBankCode: config.sourceBankCode,
  sourceAccountNo: config.mainAccountRegistered,
  paymentServiceType: 'DOMESTIC_PAYMENT',
  transactionDate: '210710154410',
  currency: utilConstants.currency.MAMBU_CODE,
  beneficiaryAccountName: 'Quasar Auto Test',
  externalId: generateRandomAlphanumeric(12),
  interchange: utilConstants.interchange.ALTO,
  notes: ''
});

const getDomescticVoidPaymentBasePayload = () => ({
  beneficiaryBankCode: config.sourceBankCode,
  transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
  sourceBankCode: utilConstants.rtolCode.BNI,
  beneficiaryAccountNo: config.mainAccountRegistered,
  paymentServiceType: 'DOMESTIC_VOID_PAYMENT',
  transactionDate: '210710154410',
  currency: utilConstants.currency.MAMBU_CODE,
  beneficiaryAccountName: 'Rahul Auto Test',
  externalId: generateRandomAlphanumeric(12),
  interchange: utilConstants.interchange.ALTO,
  notes: ''
});

const domesticPaymentPayloadList = [
  [
    'POS_NPG',
    utilConstants.bankCodes.INTERNAL,
    config.mainAccountRegistered,
    utilConstants.rtolCode.BNI,
    config.externalAccountNumber,
    utilConstants.interchange.ALTO
  ],
  [
    'POS_NPG',
    utilConstants.bankCodes.INTERNAL,
    config.mainAccountRegistered,
    utilConstants.rtolCode.BNI,
    config.externalAccountNumber,
    utilConstants.interchange.ARTAJASA
  ],
  [
    'POS_NPG',
    utilConstants.bankCodes.INTERNAL,
    config.mainAccountRegistered,
    utilConstants.rtolCode.BNI,
    config.externalAccountNumber,
    utilConstants.interchange.VISA
  ]
];

module.exports = {
  domesticPaymentPayloadList,
  getDomesticPaymentBasePayload,
  getDomescticVoidPaymentBasePayload
};
