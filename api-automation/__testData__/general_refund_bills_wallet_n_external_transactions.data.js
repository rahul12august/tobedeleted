let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const walletTopUpList = [
  [
    'WALLET-RTOL_WALLET : OVO : GENERAL REFUND',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
        beneficiaryBankCode: utilConstants.bankCodes.OVO,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: utilConstants.paymentServiceType.WALLET,
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: utilConstants.paymentServiceCode.RTOL_WALLET,
        categoryCode: 'C058',
        note: utilConstants.notes.HUNDRED_CHAR
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.OVO,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'WALLET-IRIS_JAGO_TO_OTH : Gopay : GENERAL REFUND',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
        beneficiaryBankCode: utilConstants.bankCodes.GOPAY,
        beneficiaryAccountNo: config.goPayWalletSuccess,
        externalId: generateRandomAlphanumeric(12),
        paymentServiceType: utilConstants.paymentServiceType.WALLET,
        paymentServiceCode: utilConstants.paymentServiceCode.IRIS_JAGO_TO_OTH,
        categoryCode: 'C058',
        note: utilConstants.notes.TEST_NOTES
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.GOPAY,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'WALLET-RTOL_WALLET : DANA : GENERAL REFUND',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
        beneficiaryBankCode: utilConstants.bankCodes.DANA,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        externalId: generateRandomAlphanumeric(12),
        paymentServiceType: utilConstants.paymentServiceType.WALLET,
        paymentServiceCode: utilConstants.paymentServiceCode.RTOL_WALLET,
        categoryCode: 'C058',
        note: utilConstants.notes.TEST_NOTES
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.DANA,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'WALLET-RTOL_WALLET : OVO : GENERAL REFUND',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
        beneficiaryBankCode: utilConstants.bankCodes.LINKAJA,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        externalId: generateRandomAlphanumeric(12),
        paymentServiceType: utilConstants.paymentServiceType.WALLET,
        paymentServiceCode: utilConstants.paymentServiceCode.RTOL_WALLET,
        categoryCode: 'C058',
        note: utilConstants.notes.TEST_NOTES
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.LINKAJA,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'WALLET-RTOL_WALLET : SHOPEEPAY : GENERAL REFUND',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
        beneficiaryBankCode: utilConstants.bankCodes.SHOPEEPAY,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        externalId: generateRandomAlphanumeric(12),
        paymentServiceType: utilConstants.paymentServiceType.WALLET,
        paymentServiceCode: utilConstants.paymentServiceCode.RTOL_WALLET,
        categoryCode: 'C058',
        note: utilConstants.notes.TEST_NOTES
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.SHOPEEPAY,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ]
];

const billPaymentList = [
  [
    'TRANSFER-DIGITAL_GOODS : INTERNET & TV : GENERAL REFUND',
    {
      queryToFetchTransaction: {
        sourceCustomerId: config.anotherCustomerId,
        beneficiaryBankCode: utilConstants.bankCodes.INTERNET_TV,
        status: 'SUCCEED',
        refund: { $exists: false }
      },
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.INTERNET_TV,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'TRANSFER-DIGITAL_GOODS : PLN_POSTPAID : GENERAL REFUND',
    {
      queryToFetchTransaction: {
        sourceCustomerId: config.anotherCustomerId,
        beneficiaryBankCode: utilConstants.bankCodes.PLN_POSTPAID,
        status: 'SUCCEED',
        refund: { $exists: false }
      },
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.PLN_POSTPAID,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'TRANSFER-DIGITAL_GOODS : MOBILE_PREPAID : GENERAL REFUND',
    {
      queryToFetchTransaction: {
        sourceCustomerId: config.anotherCustomerId,
        beneficiaryBankCode: utilConstants.bankCodes.MOBILE_PREPAID,
        status: 'SUCCEED',
        refund: { $exists: false }
      },
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.MOBILE_PREPAID,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'TRANSFER-DIGITAL_GOODS : MOBILE_POSTPAID: GENERAL REFUND',
    {
      queryToFetchTransaction: {
        sourceCustomerId: config.anotherCustomerId,
        beneficiaryBankCode: utilConstants.bankCodes.MOBILE_POSTPAID,
        status: 'SUCCEED',
        refund: { $exists: false }
      },
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.MOBILE_POSTPAID,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'TRANSFER-DIGITAL_GOODS : WATER: GENERAL REFUND',
    {
      queryToFetchTransaction: {
        sourceCustomerId: config.anotherCustomerId,
        beneficiaryBankCode: utilConstants.bankCodes.AETRA,
        status: 'SUCCEED',
        refund: { $exists: false }
      },
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.AETRA,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'TRANSFER-DIGITAL_GOODS : INSTALLMENT : GENERAL REFUND',
    {
      queryToFetchTransaction: {
        sourceCustomerId: config.anotherCustomerId,
        beneficiaryBankCode: utilConstants.bankCodes.INSTALLMENT,
        status: 'SUCCEED',
        refund: { $exists: false }
      },
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.INSTALLMENT,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ]
];

const creditCardGeneralRefundScenario = [
  [
    'CREDIT_CARD-RTOL_CC : Credit Card : GENERAL REFUND',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
        beneficiaryBankCode: utilConstants.bankCodes.CREDIT_CARD,
        beneficiaryAccountNo: utilConstants.accountNumber.CREDIT_CARD,
        paymentServiceType: utilConstants.paymentServiceType.CREDIT_CARD,
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: utilConstants.paymentServiceCode.RTOL_CC,
        categoryCode: 'C058',
        note: utilConstants.notes.HUNDRED_CHAR
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.CREDIT_CARD,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ]
];

const externalBankGeneralRefundScenario = [
  [
    'TRANSFER-SKN : External transfer : GENERAL REFUND',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        transactionAmount: utilConstants.transactionAmount.MIN_SKN_AMOUNT,
        beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: utilConstants.paymentServiceType.TRANSFER,
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: utilConstants.paymentServiceCode.SKN,
        categoryCode: 'C058',
        note: utilConstants.notes.HUNDRED_CHAR,
        additionalInformation1: '1',
        additionalInformation2: '2'
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.OTHER_BANK,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'TRANSFER-RTGS : External transfer : GENERAL REFUND',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        transactionAmount: utilConstants.transactionAmount.MIN_RTGS_AMOUNT,
        beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: utilConstants.paymentServiceType.TRANSFER,
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: utilConstants.paymentServiceCode.RTGS,
        categoryCode: 'C058',
        note: utilConstants.notes.HUNDRED_CHAR
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.OTHER_BANK,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'TRANSFER-RTOL : External transfer : GENERAL REFUND',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
        beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: utilConstants.paymentServiceType.TRANSFER,
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: utilConstants.paymentServiceCode.RTOL,
        categoryCode: 'C058',
        note: utilConstants.notes.HUNDRED_CHAR
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.OTHER_BANK,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'TRANSFER-SKN_FOR_BUSINESS : External transfer : GENERAL REFUND',
    {
      transferRequest: {
        sourceCIF: config.cifMBA,
        sourceAccountNo: config.accountNumberMBA,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        transactionAmount: utilConstants.transactionAmount.MIN_SKN_AMOUNT,
        beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: utilConstants.paymentServiceType.TRANSFER,
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: utilConstants.paymentServiceCode.SKN_FOR_BUSINESS,
        categoryCode: 'C058',
        note: utilConstants.notes.HUNDRED_CHAR,
        additionalInformation1: '2',
        additionalInformation2: '2'
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.OTHER_BANK,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'TRANSFER-RTGS_FOR_BUSINESS : External transfer : GENERAL REFUND',
    {
      transferRequest: {
        sourceCIF: config.cifMBA,
        sourceAccountNo: config.accountNumberMBA,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        transactionAmount: utilConstants.transactionAmount.MIN_RTGS_AMOUNT,
        beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: utilConstants.paymentServiceType.TRANSFER,
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: utilConstants.paymentServiceCode.RTGS_FOR_BUSINESS,
        categoryCode: 'C058',
        note: utilConstants.notes.HUNDRED_CHAR
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.OTHER_BANK,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'TRANSFER-RTOL_FOR_BUSINESS : External transfer : GENERAL REFUND',
    {
      transferRequest: {
        sourceCIF: config.cifMBA,
        sourceAccountNo: config.accountNumberMBA,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
        beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: utilConstants.paymentServiceType.TRANSFER,
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: utilConstants.paymentServiceCode.RTOL_FOR_BUSINESS,
        categoryCode: 'C058',
        note: utilConstants.notes.HUNDRED_CHAR
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.OTHER_BANK,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ]
];

const refundRdnTransactionsList = [
  [
    'TRANSFER-RDN_KSEI : External transfer : GENERAL REFUND',
    {
      transferRequest: {
        sourceCIF: config.cifMBA,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        sourceAccountNo: config.accountNumberMBA,
        transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT,
        beneficiaryBankCode: utilConstants.bankCodes.bankCodeKSEI,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        beneficiaryAccountName: 'RDN_KSEI Account',
        paymentServiceType: utilConstants.paymentServiceType.RDN,
        externalId: generateRandomAlphanumeric(12),
        categoryCode: 'C056',
        paymentServiceCode: utilConstants.paymentServiceCode.RDN_KSEI
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.bankCodeKSEI,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'TRANSFER-RDN_WITHDRAW_SKN : External transfer : GENERAL REFUND',
    {
      transferRequest: {
        sourceCIF: config.cifRDN,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        sourceAccountNo: config.accountNumberRdn,
        transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT,
        beneficiaryBankCode: utilConstants.bankCodes.bankCodeExternal,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: utilConstants.paymentServiceType.RDN,
        externalId: generateRandomAlphanumeric(12),
        categoryCode: 'C040',
        paymentServiceCode: utilConstants.paymentServiceCode.RDN_WITHDRAW_SKN,
        additionalInformation1: '1',
        additionalInformation2: '1'
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.bankCodeExternal,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ],
  [
    'TRANSFER-RDN_WITHDRAW_RTGS : External transfer : GENERAL REFUND',
    {
      transferRequest: {
        sourceCIF: config.cifRDN,
        sourceBankCode: utilConstants.bankCodes.bankCodeJago,
        sourceAccountNo: config.accountNumberRdn,
        transactionAmount:
          utilConstants.transactionAmount.MIN_RDN_WITHDRAW_RTGS_AMOUNT,
        beneficiaryBankCode: utilConstants.bankCodes.bankCodeExternal,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: utilConstants.paymentServiceType.RDN,
        externalId: generateRandomAlphanumeric(12),
        categoryCode: 'C040',
        paymentServiceCode: utilConstants.paymentServiceCode.RDN_WITHDRAW_RTGS,
        additionalInformation1: '1',
        additionalInformation2: '1'
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      refundTransactionDetails: {
        sourceBankCode: utilConstants.bankCodes.bankCodeExternal,
        paymentServiceType: utilConstants.paymentServiceType.GENERAL_REFUND,
        paymentServiceCode: utilConstants.paymentServiceCode.REFUND_GENERAL,
        status: 'SUCCEED'
      }
    }
  ]
];

const refundTransactionsErrorScenarios = [
  [
    'Error Scenario : TRANSFER-DIGITAL_GOODS : Already refunded transaction request',
    {
      queryToFetchTransaction: {
        sourceCustomerId: config.anotherCustomerId,
        beneficiaryBankCode: utilConstants.bankCodes.INSTALLMENT,
        status: 'SUCCEED',
        refund: { $exists: true }
      },
      transferResponse: {
        message: 'Invalid refund request',
        code: 'INVALID_REFUND_REQUEST'
      }
    }
  ],
  [
    'Error Scenario : TRANSFER-DIGITAL_GOODS : Submitted transaction',
    {
      queryToFetchTransaction: {
        sourceCustomerId: config.anotherCustomerId,
        beneficiaryBankCode: utilConstants.bankCodes.INSTALLMENT,
        status: 'SUBMITTED'
      },
      transferResponse: {
        message: 'Invalid refund request',
        code: 'INVALID_REFUND_REQUEST'
      }
    }
  ],
  [
    'Error Scenario : TRANSFER-RTOL_CC : Submitted transaction',
    {
      queryToFetchTransaction: {
        sourceCustomerId: config.anotherCustomerId,
        beneficiaryBankCode: utilConstants.bankCodes.CREDIT_CARD,
        status: 'SUBMITTED'
      },
      transferResponse: {
        message: 'Invalid refund request',
        code: 'INVALID_REFUND_REQUEST'
      }
    }
  ]
];

const transactionNotFoundList = [
  [
    'Error Scenario : TRANSFER-DIGITAL_GOODS : Transaction not found',
    {
      transferResponse: {
        message: 'Could not find Transaction',
        code: 'TRANSACTION_NOT_FOUND'
      }
    }
  ]
];
module.exports = {
  walletTopUpList,
  billPaymentList,
  creditCardGeneralRefundScenario,
  refundTransactionsErrorScenarios,
  transactionNotFoundList,
  externalBankGeneralRefundScenario,
  refundRdnTransactionsList
};
