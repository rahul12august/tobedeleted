let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const getBlockingTransferPayload = () => ({
  sourceCIF: config.sourceCif,
  sourceBankCode: utilConstants.bankCodes.INTERNAL,
  sourceAccountNo: config.mainAccountRegistered,
  transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT,
  beneficiaryBankCode: utilConstants.bankCodes.BIBIT,
  beneficiaryAccountNo: '00011530766',
  paymentServiceType: 'JAGOPAY',
  externalId: generateRandomAlphanumeric(12)
});

const blockingTransferPayLoadList = [
  [
    'MA To Bibit',
    {
      sourceAccountNo: config.mainAccountRegistered,
      beneficiaryBankCode: utilConstants.bankCodes.BIBIT,
      transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT
    }
  ],
  [
    'DC To Bibit',
    {
      sourceAccountNo: config.dcAccountRegistered,
      beneficiaryBankCode: utilConstants.bankCodes.BIBIT,
      transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT
    }
  ],
  [
    'MA To Gojek',
    {
      sourceAccountNo: config.mainAccountRegistered,
      beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
      transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT
    }
  ],
  [
    ' DC To Gojek',
    {
      sourceAccountNo: config.dcAccountRegistered,
      beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
      transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT
    }
  ]
];

const getOrderAdjustmentPayload = () => ({
  amount: utilConstants.transactionAmount.MIN_FE_AMOUNT
});

const getCaptureToFundPayload = () => ({
  amount: utilConstants.transactionAmount.MIN_FE_AMOUNT
});

module.exports = {
  getBlockingTransferPayload,
  getOrderAdjustmentPayload,
  getCaptureToFundPayload,
  blockingTransferPayLoadList
};
