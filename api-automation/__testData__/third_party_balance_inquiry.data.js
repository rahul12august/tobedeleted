let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const thirdPartyBalanceInquiryReqResExpResultList = [
  [
    'THIRD_PARTY_BALANCE_INQUIRY: Main Account',
    {
      sourceAccountNo: config.mainAccountRegistered,
      externalId: generateRandomAlphanumeric(14),
      transactionAmount: 0,
      sourceBankCode: 'BC002',
      sourceAccountNo: config.mainAccountRegistered,
      paymentServiceType: 'THIRD_PARTY_BALANCE_INQUIRY',
      transactionDate: '210810154410',
      currency: utilConstants.currency.MAMBU_CODE,
      beneficiaryAccountName: 'Rahul Tiwari',
      notes: 'THIRD PARTY BALANCE INQUIRY',
      beneficiaryBankCode: '014'
    },
    ['id', 'externalId', 'availableBalance'],
    {
      sourceAccountType: 'MA',
      status: 'SUCCEED',
      paymentServiceType: 'THIRD_PARTY_BALANCE_INQUIRY',
      paymentServiceCode: 'BALANCE_INQUIRY',
      fees: [
        {
          feeAmount: 4000
        }
      ]
    }
  ],
  [
    'THIRD_PARTY_BALANCE_INQUIRY: FS Account',
    {
      sourceAccountNo: config.mainAccountRegistered,
      externalId: generateRandomAlphanumeric(14),
      transactionAmount: 0,
      sourceBankCode: 'BC002',
      sourceAccountNo: config.fsAccountRegistered,
      paymentServiceType: 'THIRD_PARTY_BALANCE_INQUIRY',
      transactionDate: '210810154410',
      currency: utilConstants.currency.MAMBU_CODE,
      beneficiaryAccountName: 'Rahul Tiwari',
      notes: 'THIRD PARTY BALANCE INQUIRY',
      beneficiaryBankCode: '014'
    },
    ['id', 'externalId', 'availableBalance'],
    {
      sourceAccountType: 'FS',
      status: 'SUCCEED',
      paymentServiceType: 'THIRD_PARTY_BALANCE_INQUIRY',
      paymentServiceCode: 'BALANCE_INQUIRY',
      fees: [
        {
          feeAmount: 4000
        }
      ]
    }
  ],
  [
    'THIRD_PARTY_BALANCE_INQUIRY: DC Account',
    {
      sourceAccountNo: config.mainAccountRegistered,
      externalId: generateRandomAlphanumeric(14),
      transactionAmount: 0,
      sourceBankCode: 'BC002',
      sourceAccountNo: config.dcAccountRegistered,
      paymentServiceType: 'THIRD_PARTY_BALANCE_INQUIRY',
      transactionDate: '210810154410',
      currency: utilConstants.currency.MAMBU_CODE,
      beneficiaryAccountName: 'Rahul Tiwari',
      notes: 'THIRD PARTY BALANCE INQUIRY',
      beneficiaryBankCode: '014'
    },
    ['id', 'externalId', 'availableBalance'],
    {
      sourceAccountType: 'DC',
      status: 'SUCCEED',
      paymentServiceType: 'THIRD_PARTY_BALANCE_INQUIRY',
      paymentServiceCode: 'BALANCE_INQUIRY',
      fees: [
        {
          feeAmount: 4000
        }
      ]
    }
  ]
];

const BadRequestNExpectedResultList = [
  [
    'Error : THIRD_PARTY_BALANCE_INQUIRY : Insufficient amount <4K',
    {
      sourceAccountNo: config.mainAccountRegistered,
      externalId: generateRandomAlphanumeric(14),
      transactionAmount: 0,
      sourceBankCode: 'BC002',
      sourceAccountNo: config.otherJagoDClessMoney,
      paymentServiceType: 'THIRD_PARTY_BALANCE_INQUIRY',
      transactionDate: '210810154410',
      currency: utilConstants.currency.MAMBU_CODE,
      beneficiaryAccountName: 'Rahul Tiwari',
      notes: 'Third party BALANCE INQUIRY',
      beneficiaryBankCode: '014'
    },
    {
      message: 'Insufficient available balance',
      code: 'TRANSFER_AMOUNT_IS_INSUFFICIENT'
    }
  ]
];
module.exports = {
  thirdPartyBalanceInquiryReqResExpResultList,
  BadRequestNExpectedResultList
};
