let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const thirdpartyJagotoJagoReqResExpectedResList = [
  [
    'THIRD_PARTY_JAGO_TO_JAGO_CREDIT-ALTO: Jago to Jago Credit',
    {
      beneficiaryAccountName: ' Harry HSujBHaij ',
      beneficiaryAccountNo: config.mainAccountRegistered,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      currency: utilConstants.currency.MAMBU_CODE,
      externalId: generateRandomAlphanumeric(12),
      interchange: utilConstants.interchange.ALTO,
      paymentServiceType: 'THIRD_PARTY_JAGO_TO_JAGO_CREDIT',
      sourceAccountNo: config.otherJagoMA,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 9999999999,
      transactionDate: '211020051500'
    },
    ['id', 'externalId', 'availableBalance'],
    {
      beneficiaryAccountNo: config.mainAccountRegistered,
      beneficiaryBankName: 'Jago',
      beneficiaryBankCodeChannel: 'INTERNAL',
      debitPriority: 'INTERNAL',
      paymentServiceCode: 'THIRDPARTY_JAGO_TO_JAGO_CREDIT',
      sourceBankCode: utilConstants.bankCodes.INTERNAL
    }
  ],
  [
    'THIRD_PARTY_JAGO_TO_JAGO_CREDIT-ARTAJASA: Jago to Jago Credit',
    {
      beneficiaryAccountName: ' Harry HSujBHaij ',
      beneficiaryAccountNo: config.mainAccountRegistered,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      currency: utilConstants.currency.MAMBU_CODE,
      externalId: generateRandomAlphanumeric(12),
      interchange: utilConstants.interchange.ARTAJASA,
      paymentServiceType: 'THIRD_PARTY_JAGO_TO_JAGO_CREDIT',
      sourceAccountNo: config.otherJagoMA,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 10000,
      transactionDate: '211020051500'
    },
    ['id', 'externalId', 'availableBalance'],
    {
      beneficiaryAccountNo: config.mainAccountRegistered,
      beneficiaryBankName: 'Jago',
      beneficiaryBankCodeChannel: 'INTERNAL',
      debitPriority: 'INTERNAL',
      paymentServiceCode: 'THIRDPARTY_JAGO_TO_JAGO_CREDIT',
      sourceBankCode: utilConstants.bankCodes.INTERNAL
    }
  ],
  [
    'THIRD_PARTY_JAGO_TO_JAGO_DEBIT-ALTO: Jago to Jago Debit',
    {
      beneficiaryAccountName: ' Harry HSujBHaij ',
      beneficiaryAccountNo: config.mainAccountRegistered,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      currency: utilConstants.currency.MAMBU_CODE,
      externalId: generateRandomAlphanumeric(12),
      interchange: utilConstants.interchange.ALTO,
      paymentServiceType: 'THIRD_PARTY_JAGO_TO_JAGO_DEBIT',
      sourceAccountNo: config.otherJagoMA,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 25000000,
      transactionDate: '211020051500'
    },
    ['id', 'externalId', 'availableBalance'],
    {
      beneficiaryAccountNo: config.mainAccountRegistered,
      beneficiaryBankName: 'Jago',
      beneficiaryBankCodeChannel: 'INTERNAL',
      debitPriority: 'INTERNAL',
      paymentServiceCode: 'THIRDPARTY_JAGO_TO_JAGO_DEBIT',
      sourceBankCode: utilConstants.bankCodes.INTERNAL
    }
  ],
  [
    'THIRD_PARTY_JAGO_TO_JAGO_DEBIT-ARTAJASA: Jago to Jago Debit',
    {
      beneficiaryAccountName: ' Harry HSujBHaij ',
      beneficiaryAccountNo: config.mainAccountRegistered,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      currency: utilConstants.currency.MAMBU_CODE,
      externalId: generateRandomAlphanumeric(12),
      interchange: utilConstants.interchange.ARTAJASA,
      paymentServiceType: 'THIRD_PARTY_JAGO_TO_JAGO_DEBIT',
      sourceAccountNo: config.otherJagoMA,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 10000,
      transactionDate: '211020051500'
    },
    ['id', 'externalId', 'availableBalance'],
    {
      beneficiaryAccountNo: config.mainAccountRegistered,
      beneficiaryBankName: 'Jago',
      beneficiaryBankCodeChannel: 'INTERNAL',
      debitPriority: 'INTERNAL',
      paymentServiceCode: 'THIRDPARTY_JAGO_TO_JAGO_DEBIT',
      sourceBankCode: utilConstants.bankCodes.INTERNAL
    }
  ]
];

const BadRequestNExpectedResultList = [
  [
    'Error Scenario : THIRD_PARTY_JAGO_TO_JAGO_DEBIT: Invalid Req-Wrong Channel',
    {
      beneficiaryAccountName: ' Harry HSujBHaij ',
      beneficiaryAccountNo: config.mainAccountRegistered,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      currency: utilConstants.currency.MAMBU_CODE,
      externalId: generateRandomAlphanumeric(12),
      interchange: utilConstants.interchange.WINCOR,
      paymentServiceType: 'THIRD_PARTY_JAGO_TO_JAGO_DEBIT',
      sourceAccountNo: config.otherJagoMA,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 10022,
      transactionDate: '211020051500'
    },
    {
      message: 'Invalid request',
      code: 'INVALID_REQUEST',
      errors: [
        {
          message: 'Incorrect field value, data type or length',
          code: 'INCORRECT_FIELD',
          key: 'interchange'
        }
      ]
    }
  ],
  [
    'Error Scenario : THIRD_PARTY_JAGO_TO_JAGO_CREDIT: Invalid Req-Wrong Channel,Missing field value',
    {
      beneficiaryAccountName: ' Harry HSujBHaij ',
      beneficiaryAccountNo: config.mainAccountRegistered,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      currency: utilConstants.currency.MAMBU_CODE,
      externalId: generateRandomAlphanumeric(12),
      interchange: 'WINCOR',
      paymentServiceType: 'THIRD_PARTY_JAGO_TO_JAGO_CREDIT',
      sourceAccountNo: config.otherJagoMA,
      transactionAmount: 10022,
      transactionDate: '211020051500'
    },
    {
      message: 'Invalid request',
      code: 'INVALID_REQUEST',
      errors: [
        {
          message: 'Required field cannot be empty',
          code: 'INVALID_MANDATORY_FIELDS',
          key: 'sourceBankCode'
        },
        {
          message: 'Incorrect field value, data type or length',
          code: 'INCORRECT_FIELD',
          key: 'interchange'
        }
      ]
    }
  ],
  [
    'Error Scenario : THIRD_PARTY_JAGO_TO_JAGO_CREDIT: CREDIT_INTERCHANGE_NOT_FOUND',
    {
      beneficiaryAccountName: ' Harry HSujBHaij ',
      beneficiaryAccountNo: config.mainAccountRegistered,
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      currency: utilConstants.currency.MAMBU_CODE,
      externalId: generateRandomAlphanumeric(12),
      interchange: utilConstants.interchange.VISA,
      paymentServiceType: 'THIRD_PARTY_JAGO_TO_JAGO_CREDIT',
      sourceAccountNo: config.otherJagoMA,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      transactionAmount: 10022,
      transactionDate: '211020051500'
    },
    {
      message: 'Credit configuration for interchange not found',
      code: 'CREDIT_INTERCHANGE_NOT_FOUND'
    }
  ]
];
module.exports = {
  thirdpartyJagotoJagoReqResExpectedResList,
  BadRequestNExpectedResultList
};
