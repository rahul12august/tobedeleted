let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const getBifastTransactionBasePayload = () => ({
  sourceCIF: config.sourceCif,
  sourceBankCode: utilConstants.bankCodes.INTERNAL,
  sourceAccountNo: config.mainAccountRegistered,
  transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
  beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
  beneficiaryAccountNo: utilConstants.accountNumber.SUCCESS_PHONE_PROXY,
  paymentServiceType: 'TRANSFER',
  externalId: generateRandomAlphanumeric(12),
  categoryCode: 'C056',
  note: 'Test',
  additionalInformation1: '1',
  additionalInformation2: '1',
  additionalInformation3: '01',
  additionalInformation4: utilConstants.accountNumber.SUCCESS_PHONE_PROXY
});

const bifastTransactionPayloadList = [
  [
    'Bifast Transfer using phone proxy',
    config.mainAccountRegistered,
    utilConstants.transactionAmount.MIN_FE_AMOUNT,
    generateRandomAlphanumeric(12),
    'notes',
    utilConstants.accountNumber.SUCCESS_PHONE_PROXY,
    utilConstants.accountNumber.SUCCESS_PHONE_PROXY
  ],
  [
    'Bifast Transfer using account number',
    config.mainAccountRegistered,
    utilConstants.transactionAmount.MIN_FE_AMOUNT,
    generateRandomAlphanumeric(12),
    'notes',
    '00',
    utilConstants.accountNumber.SUCCESS_ACCOUNT_NO,
    utilConstants.accountNumber.SUCCESS_ACCOUNT_NO
  ],
  [
    'Bifast Transfer using email proxy',
    config.mainAccountRegistered,
    utilConstants.transactionAmount.MIN_FE_AMOUNT,
    generateRandomAlphanumeric(12),
    'notes',
    '02',
    utilConstants.accountNumber.SUCCESS_EMAIL_PROXY,
    utilConstants.accountNumber.SUCCESS_EMAIL_PROXY
  ]
];

module.exports = {
  bifastTransactionPayloadList,
  getBifastTransactionBasePayload,
  errorTransactionPayloadList
};
