let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');
const getTransactionBasePayload = {
  sourceCIF: config.sourceCif,
  sourceBankCode: utilConstants.bankCodes.INTERNAL,
  transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT,
  beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
  beneficiaryAccountNo: config.anotherMainAccount,
  paymentServiceType: 'OFFER',
  externalId: generateRandomAlphanumeric(12)
};

const paymentPayload = [
  [
    'OPERATION Interchange',
    utilConstants.interchange.OPERATION,
    generateRandomAlphanumeric(12)
  ],
  [
    'PARTNER Interchange',
    utilConstants.interchange.PARTNER,
    generateRandomAlphanumeric(12)
  ],
  [
    'NOTAX Interchange',
    utilConstants.interchange.NOTAX,
    generateRandomAlphanumeric(12)
  ],
  [
    'TAX Interchange',
    utilConstants.interchange.TAX,
    generateRandomAlphanumeric(12)
  ],
  ['Default Interchange', '', generateRandomAlphanumeric(12)]
];

module.exports = {
  getTransactionBasePayload,
  paymentPayload
};
