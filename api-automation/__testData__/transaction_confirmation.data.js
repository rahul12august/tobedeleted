let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');
const getTransactionPayload = () => ({
  accountNo: config.mainAccountRegistered,
  transactionDate: '201010154410',
  interchange: 'ALTO',
  externalId: generateRandomAlphanumeric(12),
  responseCode: 400,
  responseMessage: 'GENERAL DECLINE',
  transactionResponseID: '101620000010'
});

const getBaseThirdPartyPayload = () => ({
  externalId: generateRandomAlphanumeric(12),
  transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
  sourceBankCode: utilConstants.bankCodes.bankCodeJago,
  sourceAccountNo: config.mainAccountRegistered,
  paymentServiceType: utilConstants.paymentServiceType.THIRD_PARTY_TRANSFER,
  transactionDate: '210810154410',
  currency: utilConstants.currency.MAMBU_CODE,
  beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
  beneficiaryAccountName: 'MS ARTOSX KALTIM',
  notes: utilConstants.notes.TEST_NOTES,
  beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK
});

const errorPayloadList = [
  [
    'Error case while confirming a transaction which is already succeeded',
    {
      responseCode: 200,
      message: 'Transaction already confirmed',
      code: 'TRANSACTION_ALREADY_CONFIRMED'
    }
  ],
  [
    'Error case while declining a transaction which is already declined',
    {
      responseCode: 400,
      message: 'Transaction already reversed',
      code: 'TRANSACTION_ALREADY_REVERSED'
    }
  ],
  [
    'Error case while when external id does not match any transaction',
    {
      responseCode: 400,
      message: 'Invalid_ExternalId',
      code: 'INVALID_EXTERNALID'
    }
  ],
  [
    'Error case while when Euronet sends timeout response code',
    {
      responseCode: 408,
      message: 'Transaction already reversed',
      code: 'TRANSACTION_ALREADY_REVERSED'
    }
  ]
];

const transactionPayloadList = [
  [
    'Successfully declined a transaction which is already succeeded with status code 400',
    {
      responseCode: 400
    }
  ],
  [
    'Successfully declined a transaction which is already succeeded with status code 500',
    {
      responseCode: 500
    }
  ]
];

const getRtolTransactionBasePayload = () => ({
  sourceCIF: config.sourceCif,
  sourceBankCode: utilConstants.bankCodes.INTERNAL,
  sourceAccountNo: config.mainAccountRegistered,
  transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
  beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
  beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUBMITTED,
  paymentServiceType: 'TRANSFER',
  externalId: generateRandomAlphanumeric(12),
  categoryCode: 'C056',
  interchange: utilConstants.interchange.ALTO,
  note: 'Test'
});

const rtolTransactionConfirmationList = [
  [
    'Validate when Euronet sends 200 responseCode transaction gets succeeded',
    {
      responseCode: 200,
      status: 'SUCCEED'
    }
  ],
  [
    'Validate when Euronet sends 400 responseCode transaction gets declined',
    {
      responseCode: 400,
      status: 'DECLINED'
    }
  ]
];
module.exports = {
  getTransactionPayload,
  errorPayloadList,
  transactionPayloadList,
  getBaseThirdPartyPayload,
  getRtolTransactionBasePayload,
  rtolTransactionConfirmationList
};
