let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const amount = {
  minSknAmount: 50000001,
  SknAmount100M1: 100000001,
  invalidSknAmountUpperLimit: 500000001,
  invalidSknAmountLowerLimit: 50000000
};

const outgoingTransactionsListSKN = [
  [
    'TRANSFER-SKN : REFUND_SKN : Amount = 50M1 (Min SKN amount)',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        sourceBankCode: utilConstants.bankCodes.INTERNAL,
        transactionAmount: amount.minSknAmount,
        beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: 'TRANSFER',
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: 'SKN',
        categoryCode: 'C040',
        additionalInformation1: '2',
        additionalInformation2: '2'
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      transferTransactionDetails: {
        paymentServiceCode: 'SKN',
        paymentServiceType: 'TRANSFER',
        transactionAmount: amount.minSknAmount,
        status: 'SUCCEED',
        interchange: utilConstants.interchange.SKN
      },
      refundRequest: {
        externalId: '8213728657130001',
        transactionDate: '211027180300',
        paymentServiceType: 'VOID_SKN',
        sourceBankCode: utilConstants.bankCodes.OTHER_BANK,
        sourceAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        currency: utilConstants.currency.MAMBU_CODE,
        transactionAmount: amount.minSknAmount,
        beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
        beneficiaryAccountNo: config.mainAccountRegistered,
        notes: '000021314T000064',
        interchange: utilConstants.interchange.SKN
      },
      refundTransactionDetails: {
        paymentServiceCode: 'REFUND_SKN',
        status: 'SUCCEED',
        paymentServiceType: 'VOID_SKN',
        transactionAmount: amount.minSknAmount
      }
    }
  ],
  [
    'TRANSFER-SKN : REFUND_SKN : Amount = 100M1',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        sourceBankCode: utilConstants.bankCodes.INTERNAL,
        transactionAmount: amount.SknAmount100M1,
        beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: 'TRANSFER',
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: 'SKN',
        categoryCode: 'C040',
        additionalInformation1: '2',
        additionalInformation2: '2'
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      transferTransactionDetails: {
        paymentServiceCode: 'SKN',
        paymentServiceType: 'TRANSFER',
        transactionAmount: amount.SknAmount100M1,
        status: 'SUCCEED',
        interchange: utilConstants.interchange.SKN
      },
      refundRequest: {
        externalId: '8213728657130001',
        transactionDate: '211027180300',
        paymentServiceType: 'VOID_SKN',
        sourceBankCode: utilConstants.bankCodes.OTHER_BANK,
        sourceAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        currency: utilConstants.currency.MAMBU_CODE,
        transactionAmount: amount.SknAmount100M1,
        beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
        beneficiaryAccountNo: config.mainAccountRegistered,
        notes: '000021314T000064',
        interchange: utilConstants.interchange.SKN
      },
      refundTransactionDetails: {
        paymentServiceCode: 'REFUND_SKN',
        status: 'SUCCEED',
        paymentServiceType: 'VOID_SKN',
        transactionAmount: amount.SknAmount100M1
      }
    }
  ]
];

const outgoingTransactionsErrorScenarios = [
  [
    'Error Scenario : TRANSFER-SKN : Amount = 50M',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        sourceBankCode: utilConstants.bankCodes.INTERNAL,
        transactionAmount: amount.invalidSknAmountLowerLimit,
        beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: 'TRANSFER',
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: 'SKN',
        categoryCode: 'C040'
      },
      transferResponse: {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    }
  ],
  [
    'Error Scenario : TRANSFER-SKN : Amount = 500M1',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        sourceBankCode: utilConstants.bankCodes.INTERNAL,
        transactionAmount: amount.invalidSknAmountUpperLimit,
        beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: 'TRANSFER',
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: 'SKN',
        categoryCode: 'C040'
      },
      transferResponse: {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    }
  ]
];
module.exports = {
  outgoingTransactionsListSKN,
  outgoingTransactionsErrorScenarios
};
