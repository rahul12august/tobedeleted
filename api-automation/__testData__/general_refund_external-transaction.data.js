let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const getBaseOriginalRTOLPayoad = () => ({
  sourceCIF: config.sourceCif,
  sourceBankCode: utilConstants.bankCodes.INTERNAL,
  sourceAccountNo: config.mainAccountRegistered,
  beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
  beneficiaryAccountNo: config.anotherMainAccount,
  transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
  paymentServiceCode: 'RTOL',
  paymentServiceType: 'TRANSFER',
  externalId: generateRandomAlphanumeric(12)
});

const generalRefundExternalToInternalPayload = [
  [config.externalAccountNumber],
  [config.externalAccountNumber],
  [config.externalAccountNumber]
];

module.exports = {
  getBaseOriginalRTOLPayoad,
  generalRefundExternalToInternalPayload
};
