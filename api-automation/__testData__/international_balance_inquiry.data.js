let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const internationalBalanceInquiryReqResExpResultList = [
  [
    'INTERNATIONAL_BALANCE_INQUIRY:VISA_BALANCE_INQUIRY : Main Account',
    {
      sourceAccountNo: config.mainAccountRegistered,
      externalId: generateRandomAlphanumeric(14),
      transactionAmount: utilConstants.transactionAmount.ZERO,
      sourceBankCode: 'BC002',
      sourceAccountNo: config.mainAccountRegistered,
      paymentServiceType: 'INTERNATIONAL_BALANCE_INQUIRY',
      transactionDate: '210810154410',
      currency: utilConstants.currency.MAMBU_CODE,
      beneficiaryAccountName: 'MS ARTOSX KALTIM',
      notes: 'International BALANCE INQUIRY',
      beneficiaryBankCode: '0014'
    },
    ['id', 'externalId', 'availableBalance'],
    {
      sourceAccountType: 'MA',
      status: 'SUCCEED',
      paymentServiceType: 'INTERNATIONAL_BALANCE_INQUIRY',
      paymentServiceCode: 'VISA_BALANCE_INQUIRY',
      fees: [
        {
          feeAmount: 10000
        }
      ]
    }
  ],
  [
    'INTERNATIONAL_BALANCE_INQUIRY:VISA_BALANCE_INQUIRY : FS Account',
    {
      sourceAccountNo: config.mainAccountRegistered,
      externalId: generateRandomAlphanumeric(14),
      transactionAmount: utilConstants.transactionAmount.ZERO,
      sourceBankCode: 'BC002',
      sourceAccountNo: config.fsAccountRegistered,
      paymentServiceType: 'INTERNATIONAL_BALANCE_INQUIRY',
      transactionDate: '210810154410',
      currency: utilConstants.currency.MAMBU_CODE,
      beneficiaryAccountName: 'MS ARTOSX KALTIM',
      notes: 'International BALANCE INQUIRY',
      beneficiaryBankCode: '0014'
    },
    ['id', 'externalId', 'availableBalance'],
    {
      sourceAccountType: 'FS',
      status: 'SUCCEED',
      paymentServiceType: 'INTERNATIONAL_BALANCE_INQUIRY',
      paymentServiceCode: 'VISA_BALANCE_INQUIRY',
      fees: [
        {
          feeAmount: 10000
        }
      ]
    }
  ],
  [
    'INTERNATIONAL_BALANCE_INQUIRY:VISA_BALANCE_INQUIRY : DC Account',
    {
      sourceAccountNo: config.mainAccountRegistered,
      externalId: generateRandomAlphanumeric(14),
      transactionAmount: utilConstants.transactionAmount.ZERO,
      sourceBankCode: 'BC002',
      sourceAccountNo: config.dcAccountRegistered,
      paymentServiceType: 'INTERNATIONAL_BALANCE_INQUIRY',
      transactionDate: '210810154410',
      currency: utilConstants.currency.MAMBU_CODE,
      beneficiaryAccountName: 'MS ARTOSX KALTIM',
      notes: 'International BALANCE INQUIRY',
      beneficiaryBankCode: '0014'
    },
    ['id', 'externalId', 'availableBalance'],
    {
      sourceAccountType: 'DC',
      status: 'SUCCEED',
      paymentServiceType: 'INTERNATIONAL_BALANCE_INQUIRY',
      paymentServiceCode: 'VISA_BALANCE_INQUIRY',
      fees: [
        {
          feeAmount: 10000
        }
      ]
    }
  ]
];

const BadRequestNExpectedResultList = [
  [
    'Error : INTERNATIONAL_BALANCE_INQUIRY : Insufficient amount in FS <10K',
    {
      sourceAccountNo: config.mainAccountRegistered,
      externalId: generateRandomAlphanumeric(14),
      transactionAmount: utilConstants.transactionAmount.ZERO,
      sourceBankCode: 'BC002',
      sourceAccountNo: config.otherJagoFSlessMoney,
      paymentServiceType: 'INTERNATIONAL_BALANCE_INQUIRY',
      transactionDate: '210810154410',
      currency: utilConstants.currency.MAMBU_CODE,
      beneficiaryAccountName: 'MS ARTOSX KALTIM',
      notes: 'International BALANCE INQUIRY',
      beneficiaryBankCode: '0014'
    },
    {
      message: 'Insufficient available balance',
      code: 'TRANSFER_AMOUNT_IS_INSUFFICIENT'
    }
  ],
  [
    'Error : INTERNATIONAL_BALANCE_INQUIRY : Insufficient amount in DC <10K',
    {
      sourceAccountNo: config.mainAccountRegistered,
      externalId: generateRandomAlphanumeric(14),
      transactionAmount: utilConstants.transactionAmount.ZERO,
      sourceBankCode: 'BC002',
      sourceAccountNo: config.otherJagoDClessMoney,
      paymentServiceType: 'INTERNATIONAL_BALANCE_INQUIRY',
      transactionDate: '210810154410',
      currency: utilConstants.currency.MAMBU_CODE,
      beneficiaryAccountName: 'MS ARTOSX KALTIM',
      notes: 'International BALANCE INQUIRY',
      beneficiaryBankCode: '0014'
    },
    {
      message: 'Insufficient available balance',
      code: 'TRANSFER_AMOUNT_IS_INSUFFICIENT'
    }
  ]
];

module.exports = {
  internationalBalanceInquiryReqResExpResultList,
  BadRequestNExpectedResultList
};
