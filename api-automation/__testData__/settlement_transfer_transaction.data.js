let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const getSettlementPayload = () => ({
  sourceBankCode: utilConstants.bankCodes.OTHER_BANK,
  sourceAccountNo: config.anotherMainAccount,
  transactionAmount: utilConstants.transactionAmount.MIN_AMOUNT,
  beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
  paymentServiceType: 'SETTLEMENT',
  externalId: generateRandomAlphanumeric(12)
});

const paymentPayload = [
  [
    'MA ACCOUNT - CREDIT_VOUCHER Interchange',
    {
      interchange: utilConstants.interchange.CREDIT_VOUCHER,
      beneficiaryAccountNo: config.anotherMainAccount
    }
  ],
  [
    'MA ACCOUNT - CREDIT_UNSETTLED Interchange',
    {
      interchange: utilConstants.interchange.CREDIT_UNSETTLED,
      beneficiaryAccountNo: config.anotherMainAccount
    }
  ],
  [
    'FS ACCOUNT - CREDIT_VOUCHER Interchange',
    {
      interchange: utilConstants.interchange.CREDIT_VOUCHER,
      beneficiaryAccountNo: config.fsAccountRegistered
    }
  ],
  [
    'FS ACCOUNT - CREDIT_UNSETTLED Interchange',
    {
      interchange: utilConstants.interchange.CREDIT_UNSETTLED,
      beneficiaryAccountNo: config.fsAccountRegistered
    }
  ],
  [
    'DC ACCOUNT - CREDIT_VOUCHER Interchange',
    {
      interchange: utilConstants.interchange.CREDIT_VOUCHER,
      beneficiaryAccountNo: config.dcAccountRegistered
    }
  ],
  [
    'DC ACCOUNT - CREDIT_UNSETTLED Interchange',
    {
      interchange: utilConstants.interchange.CREDIT_UNSETTLED,
      beneficiaryAccountNo: config.dcAccountRegistered
    }
  ]
];

const errorPayload = [
  [
    'Invalid Interchange',
    {
      interchange: utilConstants.interchange.PARTNER,
      beneficiaryAccountNo: config.dcAccountRegistered,
      expectedErrorMessage: 'Credit configuration for interchange not found',
      expectedErrorCode: 'CREDIT_INTERCHANGE_NOT_FOUND'
    }
  ]
];

module.exports = {
  getSettlementPayload,
  paymentPayload,
  errorPayload
};
