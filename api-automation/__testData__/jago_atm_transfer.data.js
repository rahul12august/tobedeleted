let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const getJagoAtmTransferPayload = () => ({
  beneficiaryBankCode: utilConstants.rtolCode.BNI,
  transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
  sourceBankCode: config.sourceBankCode,
  sourceAccountNo: config.mainAccountRegistered,
  paymentServiceType: 'JAGOATM_TRANSFER',
  transactionDate: '210710154410',
  currency: utilConstants.currency.MAMBU_CODE,
  beneficiaryAccountNo: config.externalAccountNumber,
  externalId: generateRandomAlphanumeric(12),
  beneficiaryAccountName: 'Quasar Senior QA',
  notes: utilConstants.notes.BLANK_SPACE,
  additionalInformation1: 'Automation Test',
  additionalInformation2: 'JAGOATM_TRANSFER',
  additionalInformation3: 'Rahul Tiwari',
  additionalInformation4: 'Testing',
  interchange: 'ALTO'
});

const jagoAtmTransferPayloadList = [
  [
    'Jago ATM Transfer MA To Other Bank Transfer',
    {
      beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      sourceAccountNo: config.mainAccountRegistered,
      beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      interchange: 'ALTO',
      paymentServiceCode: 'JAGOATM_JAGO_TO_OTH'
    }
  ],
  [
    'Jago ATM Transfer DC To Other Bank Transfer',
    {
      beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      sourceAccountNo: config.dcAccountRegistered,
      beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      interchange: 'ALTO',
      paymentServiceCode: 'JAGOATM_JAGO_TO_OTH'
    }
  ],
  [
    'Jago ATM Transfer Other Bank Transfer To MA',
    {
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      sourceBankCode: utilConstants.bankCodes.OTHER_BANK,
      sourceAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      beneficiaryAccountNo: config.mainAccountRegistered,
      interchange: 'ALTO',
      paymentServiceCode: 'JAGOATM_OTH_TO_JAGO'
    }
  ],
  [
    'Jago ATM Transfer Other Bank Transfer To DC',
    {
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      sourceBankCode: utilConstants.bankCodes.OTHER_BANK,
      sourceAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      beneficiaryAccountNo: config.dcAccountRegistered,
      interchange: 'ALTO',
      paymentServiceCode: 'JAGOATM_OTH_TO_JAGO'
    }
  ],
  [
    'Jago ATM Transfer Other Bank Transfer To FS',
    {
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      sourceBankCode: utilConstants.bankCodes.OTHER_BANK,
      sourceAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
      beneficiaryAccountNo: config.fsAccountRegistered,
      interchange: 'ALTO',
      paymentServiceCode: 'JAGOATM_OTH_TO_JAGO'
    }
  ],
  [
    'Jago ATM Transfer MA To Another Jago MA Transfer Different CIF',
    {
      beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
      sourceBankCode: utilConstants.bankCodes.INTERNAL,
      sourceAccountNo: config.mainAccountRegistered,
      beneficiaryAccountNo: config.anotherMainAccount,
      interchange: '',
      paymentServiceCode: 'JAGOATM_JAGO_TO_JAGO'
    }
  ]
];

module.exports = {
  getJagoAtmTransferPayload,
  jagoAtmTransferPayloadList
};
