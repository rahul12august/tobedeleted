let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const getRtolTransactionBasePayload = () => ({
  sourceCIF: config.sourceCif,
  sourceBankCode: utilConstants.bankCodes.INTERNAL,
  sourceAccountNo: config.mainAccountRegistered,
  transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
  beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
  beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
  paymentServiceType: 'TRANSFER',
  externalId: generateRandomAlphanumeric(12),
  categoryCode: 'C056',
  interchange: utilConstants.interchange.ALTO,
  note: 'Test'
});
const rtolTransactionPayloadList = [
  [
    'MA to External-Alto Interchange',
    config.mainAccountRegistered,
    utilConstants.transactionAmount.MIN_FE_AMOUNT,
    generateRandomAlphanumeric(12),
    utilConstants.interchange.ALTO,
    'notes'
  ],
  [
    'MA to External-ARTAJASA Interchange',
    config.mainAccountRegistered,
    utilConstants.transactionAmount.MIN_FE_AMOUNT,
    generateRandomAlphanumeric(12),
    utilConstants.interchange.ARTAJASA,
    'notes'
  ],
  [
    'DC to External-Alto Interchange',
    config.mainAccountRegistered,
    utilConstants.transactionAmount.MIN_FE_AMOUNT,
    generateRandomAlphanumeric(12),
    utilConstants.interchange.ALTO,
    'notes'
  ],
  [
    'DC to External-ARTAJASA Interchange',
    config.dcAccountRegistered,
    utilConstants.transactionAmount.MIN_FE_AMOUNT,
    generateRandomAlphanumeric(12),
    utilConstants.interchange.ARTAJASA,
    ''
  ],
  [
    'MA to External-Space Character in Notes',
    config.mainAccountRegistered,
    utilConstants.transactionAmount.MIN_FE_AMOUNT,
    generateRandomAlphanumeric(12),
    utilConstants.interchange.ALTO,
    ' '
  ]
];

const rtolTransactionErrorPayloadList = [
  [
    'MA to External-Error Minimum Amount',
    config.mainAccountRegistered,
    9999,
    generateRandomAlphanumeric(12),
    utilConstants.interchange.ALTO
  ],
  [
    'DC to External-Error Minimum Amount',
    config.dcAccountRegistered,
    9999,
    generateRandomAlphanumeric(12),
    utilConstants.interchange.ARTAJASA
  ]
];
module.exports = {
  getRtolTransactionBasePayload,
  rtolTransactionPayloadList,
  rtolTransactionErrorPayloadList
};
