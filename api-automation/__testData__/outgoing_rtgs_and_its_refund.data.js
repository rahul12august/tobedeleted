let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const amount = {
  minRtgsAmount: 100000001,
  RtgsAmount500M1: 500000001,
  invalidRtgsAmountUpperLimit: 1000000001,
  invalidRtgsAmountLowerLimit: 100000000
};

const outgoingTransactionsListRTGS = [
  [
    'TRANSFER-RTGS : REFUND_RTGS : Amount = 100M1 (Min RTGS amount)',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        sourceBankCode: utilConstants.bankCodes.INTERNAL,
        transactionAmount: amount.minRtgsAmount,
        beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: 'TRANSFER',
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: 'RTGS',
        categoryCode: 'C040'
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      transferTransactionDetails: {
        paymentServiceCode: 'RTGS',
        paymentServiceType: 'TRANSFER',
        transactionAmount: amount.minRtgsAmount,
        status: 'SUCCEED',
        interchange: utilConstants.interchange.RTGS
      },
      refundRequest: {
        externalId: '8213728657130001',
        transactionDate: '211027180300',
        paymentServiceType: 'VOID_RTGS',
        sourceBankCode: utilConstants.bankCodes.OTHER_BANK,
        sourceAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        currency: utilConstants.currency.MAMBU_CODE,
        transactionAmount: amount.minRtgsAmount,
        beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
        beneficiaryAccountNo: config.mainAccountRegistered,
        notes: '000021314T000064',
        interchange: utilConstants.interchange.RTGS
      },
      refundTransactionDetails: {
        paymentServiceCode: 'REFUND_RTGS',
        status: 'SUCCEED',
        paymentServiceType: 'VOID_RTGS',
        transactionAmount: amount.minRtgsAmount
      }
    }
  ],
  [
    'TRANSFER-RTGS : REFUND_RTGS : Amount = 500M1',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        sourceBankCode: utilConstants.bankCodes.INTERNAL,
        transactionAmount: amount.RtgsAmount500M1,
        beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: 'TRANSFER',
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: 'RTGS',
        categoryCode: 'C040'
      },
      transferResponseKeys: [
        'id',
        'externalId',
        'referenceId',
        'transactionIds',
        'transactionDate'
      ],
      transferTransactionDetails: {
        paymentServiceCode: 'RTGS',
        paymentServiceType: 'TRANSFER',
        transactionAmount: amount.RtgsAmount500M1,
        status: 'SUCCEED',
        interchange: utilConstants.interchange.RTGS
      },
      refundRequest: {
        externalId: '8213728657130001',
        transactionDate: '211027180300',
        paymentServiceType: 'VOID_RTGS',
        sourceBankCode: utilConstants.bankCodes.OTHER_BANK,
        sourceAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        currency: utilConstants.currency.MAMBU_CODE,
        transactionAmount: amount.RtgsAmount500M1,
        beneficiaryBankCode: utilConstants.bankCodes.INTERNAL,
        beneficiaryAccountNo: config.mainAccountRegistered,
        notes: '000021314T000064',
        interchange: utilConstants.interchange.RTGS
      },
      refundTransactionDetails: {
        paymentServiceCode: 'REFUND_RTGS',
        status: 'SUCCEED',
        paymentServiceType: 'VOID_RTGS',
        transactionAmount: amount.RtgsAmount500M1
      }
    }
  ]
];

const outgoingTransactionsErrorScenarios = [
  [
    'Error Scenario : TRANSFER-RTGS : Amount = 100M',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        sourceBankCode: utilConstants.bankCodes.INTERNAL,
        transactionAmount: amount.invalidRtgsAmountLowerLimit,
        beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: 'TRANSFER',
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: 'RTGS',
        categoryCode: 'C040'
      },
      transferResponse: {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    }
  ],
  [
    'Error Scenario : TRANSFER-RTGS : Amount = 1000M1',
    {
      transferRequest: {
        sourceCIF: config.sourceCif,
        sourceAccountNo: config.mainAccountRegistered,
        sourceBankCode: utilConstants.bankCodes.INTERNAL,
        transactionAmount: amount.invalidRtgsAmountUpperLimit,
        beneficiaryBankCode: utilConstants.bankCodes.OTHER_BANK,
        beneficiaryAccountNo: utilConstants.accountNumber.EURONET_SUCCESS,
        paymentServiceType: 'TRANSFER',
        externalId: generateRandomAlphanumeric(12),
        paymentServiceCode: 'RTGS',
        categoryCode: 'C040'
      },
      transferResponse: {
        message: 'No recommendation services',
        code: 'NO_RECOMMENDATION_SERVICES',
        errors: [
          {
            message: 'Failed while evaluating Payment Config Rules.',
            key: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED',
            code: 'PAYMENT_CONFIG_RULES_EVALUATION_FAILED'
          }
        ]
      }
    }
  ]
];
module.exports = {
  outgoingTransactionsListRTGS,
  outgoingTransactionsErrorScenarios
};
