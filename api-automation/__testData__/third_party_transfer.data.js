let config = require('config');
const { generateRandomAlphanumeric } = require('../utils/common_functions');
const { utilConstants } = require('../utils/constants');

const getBaseThirdPartyPayload = () => ({
  externalId: generateRandomAlphanumeric(12),
  transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
  sourceBankCode: '542',
  sourceAccountNo: config.mainAccountRegistered,
  paymentServiceType: 'THIRD_PARTY_TRANSFER',
  transactionDate: '210810154410',
  currency: utilConstants.currency.MAMBU_CODE,
  beneficiaryAccountNo: '1234567890',
  beneficiaryAccountName: 'MS ARTOSX KALTIM',
  notes: '',
  beneficiaryBankCode: 'BC022'
});

const thirdPartyPayloadOthToJagoList = [
  [
    'THIRDPARTY_OTH_TO_JAGO',
    'BC005',
    config.mainAccountRegistered,
    'BC002',
    config.mainAccountRegistered,
    'MA'
  ],
  [
    'THIRDPARTY_OTH_TO_JAGO',
    'BC005',
    config.mainAccountRegistered,
    'BC002',
    config.fsAccountRegistered,
    'FS'
  ],
  [
    'THIRDPARTY_OTH_TO_JAGO',
    'BC005',
    config.mainAccountRegistered,
    'BC002',
    config.dcAccountRegistered,
    'DC'
  ]
];

const thirdPartyPayloadJagoToOthList = [
  [
    'THIRDPARTY_JAGO_TO_OTH',
    'BC002',
    config.mainAccountRegistered,
    'BC005',
    config.mainAccountRegistered,
    'MA'
  ],
  [
    'THIRDPARTY_JAGO_TO_OTH',
    'BC002',
    config.fsAccountRegistered,
    'BC005',
    config.mainAccountRegistered,
    'FS'
  ],
  [
    'THIRDPARTY_JAGO_TO_OTH',
    'BC002',
    config.dcAccountRegistered,
    'BC005',
    config.mainAccountRegistered,
    'DC'
  ]
];

const thirdPartyPayloadWincorToJagoList = [
  [
    'THIRDPARTY_WINCOR_TO_JAGO',
    'BC191',
    config.mainAccountRegistered,
    'BC002',
    config.mainAccountRegistered,
    'MA'
  ]
];

const thirdPartyPayloadWincorToJagoFailure = [
  [
    'THIRDPARTY_WINCOR_TO_JAGO',
    'BC191',
    config.mainAccountRegistered,
    'BC002',
    config.fsAccountRegistered,
    'FS'
  ]
];

module.exports = {
  getBaseThirdPartyPayload,
  thirdPartyPayloadOthToJagoList,
  thirdPartyPayloadJagoToOthList,
  thirdPartyPayloadWincorToJagoList,
  thirdPartyPayloadWincorToJagoFailure
};
