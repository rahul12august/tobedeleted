let config = require('config');
const { utilConstants } = require('../utils/constants');
const { generateRandomAlphanumeric } = require('../utils/common_functions');

const getBIBITInstantRedemptionBasePayload = () => ({
  beneficiaryBankCode: utilConstants.rtolCode.BNI,
  transactionAmount: utilConstants.transactionAmount.MIN_FE_AMOUNT,
  sourceBankCode: utilConstants.bankCodes.INTERNAL,
  sourceAccountNo: config.mainAccountRegistered,
  paymentServiceType: 'BIBIT_INSTANT_REDEMPTION',
  transactionDate: '210710154410',
  currency: utilConstants.currency.MAMBU_CODE,
  beneficiaryAccountNo: config.externalAccountNumber,
  externalId: generateRandomAlphanumeric(12)
});

const bibitInstantRedemptionExternalToInternalPayload = [
  [
    'BIBIT_INSTANT_REDEMPTION',
    utilConstants.bankCodes.OTHER_BANK,
    config.externalAccountNumber,
    utilConstants.bankCodes.INTERNAL,
    config.mainAccountRegistered
  ],
  [
    'BIBIT_INSTANT_REDEMPTION',
    utilConstants.rtolCode.BNI,
    config.externalAccountNumber,
    utilConstants.rtolCode.INTERNAL,
    config.dcAccountRegistered
  ],
  [
    'BIBIT_INSTANT_REDEMPTION',
    utilConstants.rtolCode.BNI,
    config.externalAccountNumber,
    utilConstants.rtolCode.INTERNAL,
    config.fsAccountRegistered
  ]
];

const bibitInstantRedemptionExternalToInternalErrorPayload = [
  [
    10000000000,
    utilConstants.bankCodes.INTERNAL,
    config.mainAccountRegistered,
    'No recommendation services',
    'NO_RECOMMENDATION_SERVICES'
  ]
];

module.exports = {
  bibitInstantRedemptionExternalToInternalPayload,
  bibitInstantRedemptionExternalToInternalErrorPayload,
  getBIBITInstantRedemptionBasePayload
};
