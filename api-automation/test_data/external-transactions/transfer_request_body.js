let config = require('config');
const faker = require('faker');
const ext = faker.random.alphaNumeric(12);

const domesticPayment = {
  beneficiaryBankCode: '360005',
  transactionAmount: config.defaultAmount,
  sourceBankCode: '542',
  sourceAccountNo: config.mainAccountRegistered,
  paymentServiceType: 'DOMESTIC_PAYMENT',
  transactionDate: '201010154410',
  currency: '360',
  beneficiaryAccountName: 'Quasar Auto Test',
  externalId: ext,
  interchange: config.interchangeAlto,
  notes: ''
};

const domesticVoidPayment = {
  beneficiaryBankCode: '542',
  transactionAmount: config.defaultAmount,
  sourceBankCode: '360005',
  beneficiaryAccountNo: config.mainAccountRegistered,
  paymentServiceType: 'DOMESTIC_VOID_PAYMENT',
  transactionDate: '201010154410',
  currency: '360',
  externalId: faker.random.alphaNumeric(14),
  interchange: config.interchangeAlto,
  notes: ext
};

const internationalWithdrawalVisa = {
  beneficiaryBankCode: '360005',
  transactionAmount: config.atmWithdrawalAmount,
  sourceBankCode: '542',
  sourceAccountNo: config.mainAccountRegistered,
  paymentServiceType: 'INTERNATIONAL_ATM_WITHDRAWAL',
  transactionDate: '201010154410',
  currency: '360',
  beneficiaryAccountName: 'SPBU ARTERI PONDOK INDAH',
  externalId: faker.random.alphaNumeric(12),
  interchange: config.interchangeVisa,
  notes: '',
  additionalInformation1: 'EI  string 1',
  additionalInformation2: 'EI  string 2',
  additionalInformation3: 'card present',
  additionalInformation4: 'EI  string 4'
};

const internationalWithdrawalAlto = {
  beneficiaryBankCode: '360005',
  transactionAmount: config.atmWithdrawalAmount,
  sourceBankCode: '542',
  sourceAccountNo: config.mainAccountRegistered,
  paymentServiceType: 'INTERNATIONAL_ATM_WITHDRAWAL',
  transactionDate: '201010154410',
  currency: '360',
  beneficiaryAccountName: 'SPBU ARTERI PONDOK INDAH',
  externalId: faker.random.alphaNumeric(12),
  interchange: config.interchangeAlto,
  notes: '',
  additionalInformation1: 'EI  string 1',
  additionalInformation2: 'EI  string 2',
  additionalInformation3: 'card present',
  additionalInformation4: 'EI  string 4'
};

const internationalWithdrawalArtaJasa = {
  beneficiaryBankCode: '360005',
  transactionAmount: config.atmWithdrawalAmount,
  sourceBankCode: '542',
  sourceAccountNo: config.mainAccountRegistered,
  paymentServiceType: 'INTERNATIONAL_ATM_WITHDRAWAL',
  transactionDate: '201010154410',
  currency: '360',
  beneficiaryAccountName: 'SPBU ARTERI PONDOK INDAH',
  externalId: faker.random.alphaNumeric(12),
  interchange: config.interchangeArtajasa,
  notes: '',
  additionalInformation1: 'EI  string 1',
  additionalInformation2: 'EI  string 2',
  additionalInformation3: 'card present',
  additionalInformation4: 'EI  string 4'
};

const incomingJagoAtmTransfer = {
  externalId: faker.random.alphaNumeric(14),
  transactionDate: '211019150002',
  paymentServiceType: config.jagoAtmTransfer,
  sourceBankCode: '2',
  sourceAccountNo: '10101010',
  currency: '360',
  transactionAmount: config.jagoAtmTransferAmount,
  beneficiaryBankCode: '542',
  beneficiaryAccountNo: config.mainAccountRegistered,
  beneficiaryAccountName: 'Rahul Tiwari',
  notes: 'ATM TRANSFER',
  additionalInformation1: 'Quasar',
  additionalInformation2: 'Automation',
  additionalInformation3: 'Ninja',
  additionalInformation4: 'Rahul',
  interchange: 'ALTO'
};

const outgoingJagoAtmTransfer = {
  externalId: faker.random.alphaNumeric(14),
  transactionDate: '201019150002',
  paymentServiceType: config.jagoAtmTransfer,
  sourceBankCode: 'BRINIDJA',
  sourceAccountNo: '10101010',
  currency: '360',
  transactionAmount: config.defaultAmount,
  beneficiaryBankCode: 'BC002',
  beneficiaryAccountNo: config.mainAccountRegistered,
  beneficiaryAccountName: 'Julie Tiwari',
  notes: 'ATM TRANSFER',
  additionalInformation1: 'Quasar',
  additionalInformation2: 'Automation',
  additionalInformation3: 'Ninja',
  additionalInformation4: 'Rahul',
  interchange: 'ALTO'
};

module.exports = {
  domesticPayment,
  domesticVoidPayment,
  internationalWithdrawalVisa,
  internationalWithdrawalAlto,
  internationalWithdrawalArtaJasa,
  incomingJagoAtmTransfer,
  outgoingJagoAtmTransfer
};
