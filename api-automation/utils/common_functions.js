const sleepFor = sleepDuration => {
  var now = new Date().getTime();
  while (new Date().getTime() < now + sleepDuration) {}
};

function generateRandomAlphanumeric(len) {
  var p = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  return [...Array(len)].reduce(a => a + p[~~(Math.random() * p.length)], '');
}
function printResponseBody(response) {
  // eslint-disable-next-line no-console
  console.log(response.body);
}
module.exports = {
  sleepFor,
  generateRandomAlphanumeric,
  printResponseBody
};
