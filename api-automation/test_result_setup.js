let getBranch = process.env.BRANCH ? process.env.BRANCH : 'dev';
reporter.allure.setOptions({ targetDir: `${getBranch}/allure-results` });
