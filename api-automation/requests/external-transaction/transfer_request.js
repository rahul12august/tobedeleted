let config = require('config');
let { RequestTemplate } = require('@dk/module-api-automation');

class ExternalTransactionRequest extends RequestTemplate {
  get endpoint() {
    return '/transfer/external-transactions';
  }

  get headers() {
    return [];
  }

  get body() {
    return this.requestBody;
  }

  set body(requestBody) {
    this.requestBody = requestBody;
  }
}

class RefundTransactionRequest extends RequestTemplate {
  constructor(transactionId, refundFee) {
    super();
    this.transactionId = transactionId;
    this.refundFee = refundFee;
  }
  get endpoint() {
    return `/transfer/admin/refund/${this.transactionId}`;
  }

  get headers() {
    return [
      {
        key: 'Authorization',
        value: 'Bearer '.concat(config.adminAccessToken)
      }
    ];
  }

  get body() {
    return this.requestBody;
  }

  get query() {
    return [
      {
        refundFee: this.refundFee
      }
    ];
  }

  // eslint-disable-next-line @typescript-eslint/adjacent-overload-signatures
  set body(requestBody) {
    this.requestBody = requestBody;
  }
}

class TransferTransactionRequest extends RequestTemplate {
  get endpoint() {
    return '/transfer/transfer-transactions';
  }

  get headers() {
    return [];
  }

  get body() {
    return this.requestBody;
  }

  set body(requestBody) {
    this.requestBody = requestBody;
  }
}

class TransactionsRequest extends RequestTemplate {
  get endpoint() {
    return '/transfer/transactions';
  }

  get body() {
    return {
      transactionId: this.transactionId
    };
  }

  get headers() {
    return [
      {
        key: 'Authorization',
        value: 'Bearer '.concat(config.adminAccessToken)
      }
    ];
  }

  set body(transactionId) {
    this.transactionId = transactionId;
  }
}

class PrivateRecommendedServiceRequest extends RequestTemplate {
  get endpoint() {
    return '/transfer/private/recommend-payment-services';
  }

  get headers() {
    return [];
  }

  get body() {
    return this.requestBody;
  }

  set body(requestBody) {
    this.requestBody = requestBody;
  }
}

class TopUpInternalTransactions extends RequestTemplate {
  get endpoint() {
    return '/transfer/top-up-internal-transactions';
  }

  get headers() {
    return [];
  }

  get body() {
    return this.requestBody;
  }

  set body(requestBody) {
    this.requestBody = requestBody;
  }
}

class BlockingTransferTransaction extends RequestTemplate {
  get endpoint() {
    return '/transfer/blocking-transfer-transactions';
  }

  get headers() {
    return [];
  }

  get body() {
    return this.requestBody;
  }

  set body(requestBody) {
    this.requestBody = requestBody;
  }
}

class OrderAjustment extends RequestTemplate {
  constructor(transactionId, userCif) {
    super();
    this.transactionId = transactionId;
    this.userCif = userCif;
  }
  get endpoint() {
    return `/transfer/blocking-transfer-transactions/${this.transactionId}/adjust?cif=${this.userCif}`;
  }

  get headers() {
    return [];
  }

  get body() {
    return this.requestBody;
  }

  set body(requestBody) {
    this.requestBody = requestBody;
  }
}

class CaptureToFund extends RequestTemplate {
  constructor(transactionId, userCif) {
    super();
    this.transactionId = transactionId;
    this.userCif = userCif;
  }
  get endpoint() {
    return `/transfer/blocking-transfer-transactions/${this.transactionId}/execute?cif=${this.userCif}`;
  }

  get headers() {
    return [];
  }

  get body() {
    return this.requestBody;
  }

  set body(requestBody) {
    this.requestBody = requestBody;
  }
}

class CancelOrder extends RequestTemplate {
  constructor(transactionId, userCif) {
    super();
    this.transactionId = transactionId;
    this.userCif = userCif;
  }
  get endpoint() {
    return `/transfer/blocking-transfer-transactions/${this.transactionId}/cancel?cif=${this.userCif}`;
  }

  get headers() {
    return [];
  }

  get body() {
    return this.requestBody;
  }

  set body(requestBody) {
    this.requestBody = requestBody;
  }
}

class TransactionConfirmation extends RequestTemplate {
  constructor() {
    super();
  }
  get endpoint() {
    return `/transfer/transaction-confirmation`;
  }

  get headers() {
    return [];
  }

  get body() {
    return this.requestBody;
  }

  set body(requestBody) {
    this.requestBody = requestBody;
  }
}

class GetTransactionDetails extends RequestTemplate {
  constructor(transactionId) {
    super();
    this.transactionId = transactionId;
  }
  get endpoint() {
    return `/transfer/transactions/${this.transactionId}`;
  }

  get headers() {
    return [];
  }

  // get body() {
  //   return this.requestBody;
  // }

  get query() {
    return [];
  }

  // eslint-disable-next-line @typescript-eslint/adjacent-overload-signatures
  // set body(requestBody) {
  //   this.requestBody = requestBody;
  // }
}

module.exports = {
  ExternalTransactionRequest,
  TransferTransactionRequest,
  TransactionsRequest,
  PrivateRecommendedServiceRequest,
  TopUpInternalTransactions,
  RefundTransactionRequest,
  BlockingTransferTransaction,
  OrderAjustment,
  CaptureToFund,
  CancelOrder,
  TransactionConfirmation,
  GetTransactionDetails
};
