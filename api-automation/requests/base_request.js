let config = require('config');
var baseUrl = config.get('baseUrl');
let chai = require('chai');
const { CurlGenerator } = require('curl-generator');

class BaseRequest {
  constructor() {
    this._baseUrl = baseUrl;
  }

  baseRequest() {
    return chai.request(baseUrl);
  }

  logHttpCall(request, response, step = 'Step') {
    reporter.startStep(step);
    reporter.addAttachment('Request', JSON.stringify(request));
    reporter.addAttachment('Response', JSON.stringify(response));
    var curlRequest = this.buildCurlRequest(request);
    reporter.addAttachment('Curl', this.curlString(curlRequest));
    reporter.endStep();
  }

  buildCurlRequest(request) {
    var reqStr = JSON.stringify(request);
    const params = {
      url: request.url,
      method: request.method,
      headers: JSON.parse(reqStr).headers,
      body: JSON.parse(reqStr).data
    };
    return params;
  }

  curlString(curlRequest) {
    const curlSnippet = CurlGenerator(curlRequest);
    return curlSnippet;
  }
}

module.exports = BaseRequest;
