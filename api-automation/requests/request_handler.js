const BaseRequest = require('./base_request');
let allureReporter = require('../reporter/allure_reporter');

class RequestHandler extends BaseRequest {
  constructor() {
    super();
  }

  async get(request, step = 'Step') {
    let chain = this.baseRequest()
      .get(request.endpoint)
      .set('Content-Type', 'application/json');
    request.headers.forEach(
      header => (chain = chain.set(header.key, header.value))
    );
    var res = await chain.send();
    allureReporter.logHttpCall(chain, res, step);
    return res;
  }

  async put(request, step = 'step') {
    let chain = this.baseRequest()
      .put(request.endpoint)
      .set('Content-Type', 'application/json');
    request.headers.forEach(
      header => (chain = chain.set(header.key, header.value))
    );
    var res = chain.send(request.body);
    allureReporter.logHttpCall(chain, res, step);
    return res;
  }

  async post(request, step = 'Step') {
    let chain = this.baseRequest()
      .post(request.endpoint)
      .set('Content-Type', 'application/json');
    request.headers.forEach(
      header => (chain = chain.set(header.key, header.value))
    );
    var res = await chain.send(request.body);
    allureReporter.logHttpCall(chain, res, step);
    return res;
  }

  async patch(request, step = 'step') {
    let chain = this.baseRequest()
      .patch(request.endpoint)
      .set('Content-Type', 'application/json');
    request.headers.forEach(
      header => (chain = chain.set(header.key, header.value))
    );
    var res = chain.send(request.body);
    allureReporter.logHttpCall(chain, res, step);
    return res;
  }

  async delete(request, step = 'step') {
    let chain = this.baseRequest()
      .delete(request.endpoint)
      .set('Content-Type', 'application/json');
    request.headers.forEach(
      header => (chain = chain.set(header.key, header.value))
    );
    var res = chain.send();
    allureReporter.logHttpCall(chain, res, step);
    return res;
  }
}

module.exports = RequestHandler;
