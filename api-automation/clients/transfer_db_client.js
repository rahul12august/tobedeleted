const { findOne, findOneByQuery } = require('../common/mongo.util');
const { mongoConstants } = require('../common/mongo.constants');

const dbName = 'transfer';
const collectionName = mongoConstants.dbCollections.transaction;

const getByExternalId = async externalId => {
  return findOne({ externalId }, dbName, collectionName);
};

const getByQuery = async query => {
  return findOneByQuery(query, dbName, collectionName);
};
module.exports = {
  getByExternalId,
  getByQuery
};
