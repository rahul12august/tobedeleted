let RequestHandler = require('../requests/request_handler');
const {
  ExternalTransactionRequest,
  TransferTransactionRequest,
  TransactionsRequest,
  PrivateRecommendedServiceRequest,
  TopUpInternalTransactions,
  RefundTransactionRequest,
  BlockingTransferTransaction,
  OrderAjustment,
  CaptureToFund,
  CancelOrder,
  TransactionConfirmation,
  GetTransactionDetails
} = require('../requests/external-transaction/transfer_request');

class TransferClient {
  async transfer(transferRequestBody) {
    var transferRequest = new TransferTransactionRequest();
    transferRequest.body = transferRequestBody;
    return new RequestHandler().post(
      transferRequest,
      'Creating a new transfer transaction request'
    );
  }

  async externalTransfer(transferRequestBody) {
    var transferRequest = new ExternalTransactionRequest();
    transferRequest.body = transferRequestBody;
    return new RequestHandler().post(
      transferRequest,
      'Creating a new external transfer request'
    );
  }

  async refundTransfer(transactionId, refundFee) {
    const refundRequest = new RefundTransactionRequest(
      transactionId,
      refundFee
    );
    return new RequestHandler().post(
      refundRequest,
      'Creating a new general refund request'
    );
  }

  async getTransactionDetails(transactionId) {
    var transactionsRequest = new TransactionsRequest();
    transactionsRequest.body = transactionId;
    return new RequestHandler().post(
      transactionsRequest,
      'Get Transaction Details'
    );
  }
  async getTransactionDetail(transactionId) {
    var transactionsRequest = new GetTransactionDetails(transactionId);
    //transactionsRequest.body = transactionId;
    return new RequestHandler().get(
      transactionsRequest,
      'Get Transaction Details'
    );
  }
  async getRecommendedService(transferRequestBody) {
    var transferRequest = new PrivateRecommendedServiceRequest();
    transferRequest.body = transferRequestBody;
    return new RequestHandler().post(
      transferRequest,
      'Creating template for get call of recommended services'
    );
  }

  async topUpInternalTransactions(transferRequestBody) {
    var transferRequest = new TopUpInternalTransactions();
    transferRequest.body = transferRequestBody;
    return new RequestHandler().post(
      transferRequest,
      'Creating template for post call of top-up wallet transactions'
    );
  }

  async blockingTransferTransactions(transferRequestBody) {
    var transferRequest = new BlockingTransferTransaction();
    transferRequest.body = transferRequestBody;
    return new RequestHandler().post(
      transferRequest,
      'Creating a new blocking transfer transactions'
    );
  }

  async orderAdjustment(requestBody, transactionId, sourceCif) {
    var transferRequest = new OrderAjustment(transactionId, sourceCif);
    transferRequest.body = requestBody;
    return new RequestHandler().post(
      transferRequest,
      'Requesting for adjusting the order amount'
    );
  }

  async captureToFund(requestBody, transactionId, sourceCif) {
    var transferRequest = new CaptureToFund(transactionId, sourceCif);
    transferRequest.body = requestBody;
    return new RequestHandler().post(
      transferRequest,
      'Requesting for confirming the order amount'
    );
  }

  async cancelOrder(transactionId, sourceCif) {
    var transferRequest = new CancelOrder(transactionId, sourceCif);
    return new RequestHandler().post(
      transferRequest,
      'Requesting for cancelling the order'
    );
  }

  async transactionConfirmation(requestBody) {
    var transferRequest = new TransactionConfirmation();
    transferRequest.body = requestBody;
    return new RequestHandler().post(
      transferRequest,
      'Requesting for confirming the transaction'
    );
  }
}

module.exports = TransferClient;
