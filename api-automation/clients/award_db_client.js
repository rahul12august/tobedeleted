const { updateMany } = require('../common/mongo.util');
const dbName = 'award';

const resetUsageCounterByQuery = async (query, input) => {
  return updateMany(query, dbName, input);
};

module.exports = {
  resetUsageCounterByQuery
};
