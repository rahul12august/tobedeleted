const dbCollections = {
  usageCounter: 'usage_counters',
  transaction: 'transactions'
};

const dbNames = {
  transfer: 'transfer_db',
  award: 'award_db'
};
const mongoConstants = {
  dbNames: dbNames,
  dbCollections: dbCollections
};

module.exports = { mongoConstants };
