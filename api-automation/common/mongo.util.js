const mongoose = require('mongoose');
const config = require('config');
const { mongoConstants } = require('../common/mongo.constants');

const getConnection = async dbName => {
  try {
    let dbUser, dbPass, db;
    switch (dbName) {
      case 'transfer':
        dbUser = config.dbUserTransfer;
        dbPass = config.dbPassTransfer;
        db = mongoConstants.dbNames.transfer;
        break;
      case 'award':
        dbUser = config.dbUserAward;
        dbPass = config.dbPassAward;
        db = mongoConstants.dbNames.award;
        break;
      default:
        dbUser = config.dbUserTransfer;
        dbPass = config.dbPassTransfer;
        db = mongoConstants.dbNames.transfer;
        break;
    }

    const mongoUri = `mongodb+srv://${dbUser}:${dbPass}@${config.mongoURL}/${db}?authSource=admin`;
    mongoose.connect(mongoUri, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    return mongoose.connection;
  } catch (e) {}
};

const findOne = async (query, dbName, collectionName) => {
  const db = await getConnection(dbName);
  const collection = await db.collection(collectionName);
  const result = await collection.findOne(query);
  await db.close();
  return result;
};

const updateMany = async (query, dbName, update) => {
  const db = await getConnection(dbName);
  const collection = await db.collection(
    mongoConstants.dbCollections.usageCounter
  );
  const result = await collection.updateMany(query, { $set: update });
  await db.close();
  return result;
};

const findOneByQuery = async (query, dbName, collectionName) => {
  const db = await getConnection(dbName);
  const collection = await db.collection(collectionName);
  const result = await collection.findOne(query, {
    sort: { createdAt: -1 }
  });
  await db.close();
  return result;
};

module.exports = {
  findOne,
  updateMany,
  findOneByQuery
};
