class AllureFeature {
  constructor(featureContext) {
    this.featureContext = featureContext;
  }
  recordStory(
    story,
    description = 'This is a story',
    severity = rep.Severity.Normal
  ) {
    reporter
      .description(description)
      .severity(severity)
      .feature(this.featureContext.name)
      .story(story)
      .epic(this.featureContext.epic);
  }

  recordStep(message = 'Feature Step') {
    reporter.startStep(message);
    reporter.endStep();
  }
}

class FeatureContext {
  constructor(name, epic) {
    this.name = name;
    this.epic = epic;
  }
}

module.exports = {
  AllureFeature,
  FeatureContext
};
