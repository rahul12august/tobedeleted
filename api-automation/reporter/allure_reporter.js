const { CurlGenerator } = require('curl-generator');

function recordStep(message = 'Default step') {
  reporter.startStep(message);
  reporter.endStep();
}

function buildCurlRequest(request) {
  var reqStr = JSON.stringify(request);
  const params = {
    url: request.url,
    method: request.method,
    headers: JSON.parse(reqStr).headers,
    body: JSON.parse(reqStr).data
  };
  return params;
}

function curlString(curlRequest) {
  const curlSnippet = CurlGenerator(curlRequest);
  return curlSnippet;
}

function logHttpCall(request, response, step = 'Step') {
  reporter.startStep(step);
  reporter.addAttachment('Request', JSON.stringify(request));
  reporter.addAttachment('Response', JSON.stringify(response));
  var curlRequest = buildCurlRequest(request);
  reporter.addAttachment('Curl', curlString(curlRequest));
  reporter.endStep();
}

module.exports = {
  recordStep,
  logHttpCall
};
