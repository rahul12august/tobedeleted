PROJECT_NAME=$1
JOB_ID=$2
PAGES_URL=$3
BRANCH=$4
SLACK_HOOK=$5
PROJECT_URL=$6

IFS=
getval=$(tail -6 output.txt)
find=".io"
replaceWith=".io\/-"
newUrl=$(echo "$PAGES_URL" | sed "s/$find/$replaceWith/")
newUrl="$newUrl/-/jobs/$JOB_ID/artifacts/api-automation/$BRANCH/allure-report/index.html"

message="{
	\"blocks\": [
		{
			\"type\": \"section\",
			\"text\": {
				\"type\": \"mrkdwn\",
				\"text\": \"*<$PROJECT_URL|$PROJECT_NAME>* Api automation results\"
			}
		},
		{
			\"type\": \"divider\"
		},
		{
			\"type\": \"section\",
			\"text\": {
				\"type\": \"mrkdwn\",
				\"text\": \"*Report Summary*\n$getval\"
			},
			\"accessory\": {
				\"type\": \"image\",
				\"image_url\": \"https://api.slack.com/img/blocks/bkb_template_images/notifications.png\",
				\"alt_text\": \"calendar thumbnail\"
			}
		},
		{
			\"type\": \"divider\"
		},
		{
			\"type\": \"section\",
			\"text\": {
				\"type\": \"mrkdwn\",
				\"text\": \"Check out the detail Report *<$newUrl|here>*\"
			}
		}
	]
}"
echo $newUrl
curl --location --request POST "$SLACK_HOOK" \
--header 'Content-Type: application/json' \
--data "$message"
