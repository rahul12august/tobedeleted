module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  setupFilesAfterEnv: [
    'jest-allure/dist/setup',
    '<rootDir>/test_result_setup.js'
  ],
  reporters: [
    'default',
    'jest-junit',
    'jest-allure',
    [
      'jest-stare',
      {
        resultDir: 'results/jest-stare',
        reportTitle: 'jest-stare!',
        additionalResultsProcessors: ['jest-junit'],
        coverageLink: '../../coverage/lcov-report/index.html',
        jestStareConfigJson: 'jest-stare.json',
        jestGlobalConfigJson: 'globalStuff.json'
      }
    ]
  ],
  runner: 'groups',
  testTimeout: 20000,
  verbose: true,
  maxConcurrency: 2
};
