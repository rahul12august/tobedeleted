const BaseAssertions = require('./base_assertions');
let allureReporter = require('../reporter/allure_reporter');
class ResponseAssertions extends BaseAssertions {
  constructor(res) {
    super(res);
  }
  assertBadRequestDetails(expectedResult) {
    allureReporter.recordStep(
      'Asserting error scenario response against expected values'
    );
    this.assertBadRequest();
    expect(this.res.body.error).toEqual(
      expect.objectContaining(expectedResult)
    );
  }
  assertArrayResponseWithExpected(expectedResult) {
    allureReporter.recordStep(
      'Asserting response against expected values in array'
    );
    this.assertStatusIsSuccessful();
    expect(this.res.body.data).toEqual(expect.arrayContaining(expectedResult));
  }
  assertObjectResponseWithExpected(expectedResult) {
    allureReporter.recordStep(
      'Asserting response against expected values in object'
    );
    this.assertStatusIsSuccessful();
    expect(this.res.body.data[0]).toEqual(
      expect.objectContaining(expectedResult)
    );
  }
  assertTopUpTransactionDetails(expectedResult) {
    allureReporter.recordStep(
      'Asserting transaction details fetched from DB with expected values'
    );
    this.assertStatusIsSuccessful();
    expect(this.res.body.data[0].beneficiaryBankName).to.equal(
      expectedResult.beneficiaryBankName
    );
    expect(this.res.body.data[0].beneficiaryAccountNo).to.equal(
      expectedResult.beneficiaryAccountNo
    );
    expect(this.res.body.data[0].beneficiaryBankCodeChannel).to.equal(
      expectedResult.beneficiaryBankCodeChannel
    );
    expect(this.res.body.data[0].debitPriority).to.equal(
      expectedResult.debitPriority
    );
    expect(this.res.body.data[0].paymentServiceType).to.equal(
      expectedResult.paymentServiceType
    );
  }

  assertBadRequestMessageAndCode(errorMessage, errorCode) {
    allureReporter.recordStep(
      'Asserting error scenario response against expected values'
    );
    this.assertBadRequest();
    expect(this.res.body.error.message).toEqual(errorMessage);
    expect(this.res.body.error.code).toEqual(errorCode);
  }
  assertTransactionDetailsDB(expectedResult) {
    allureReporter.recordStep(
      'Asserting transaction details fetched from DB against expected'
    );
    expect(this.res).toEqual(expect.objectContaining(expectedResult));
  }
  assertNoContentInResponse() {
    allureReporter.recordStep('Asserting no content response');
    this.assertNoContent();
  }
  assertNotFoundMessageAndCode(errorMessage, errorCode) {
    allureReporter.recordStep(
      'Asserting error scenario response against expected values'
    );
    this.assertNotFound();
    expect(this.res.body.error.message).toEqual(errorMessage);
    expect(this.res.body.error.code).toEqual(errorCode);
  }
}
module.exports = { ResponseAssertions };
