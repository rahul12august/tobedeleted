const BaseAssertions = require('./base_assertions');
const { expect } = require('chai');
let allureReporter = require('../reporter/allure_reporter');
class TransferAssertions extends BaseAssertions {
  constructor(res) {
    super(res);
  }

  assertTransferIsSuccessful(keys) {
    allureReporter.recordStep('Asserting if transfer is successful!');
    this.assertThatANewRecordIsCreated();
    expect(this.res.body.data).to.have.keys(keys);
  }
}

class TransactionDetailsAssertions extends BaseAssertions {
  constructor(res) {
    super(res);
  }

  assertTransactionDetails(
    expectedStatus,
    expectedPaymentServiceType,
    expectedPaymentServiceCode,
    expectedTransactionAmount,
    expectedInterchange
  ) {
    allureReporter.recordStep('Asserting transaction details');
    this.assertStatusIsSuccessful();
    expect(this.res.body.data[0].status).to.equal(expectedStatus);
    expect(this.res.body.data[0].paymentServiceType).to.equal(
      expectedPaymentServiceType
    );
    expect(this.res.body.data[0].paymentServiceCode).to.equal(
      expectedPaymentServiceCode
    );
    expect(this.res.body.data[0].transactionAmount).to.equal(
      expectedTransactionAmount
    );
    if (this.res.body.data[0].interchange) {
      expect(this.res.body.data[0].interchange).to.equal(expectedInterchange);
    }
  }
}

module.exports = {
  TransferAssertions,
  TransactionDetailsAssertions
};
