/* eslint-disable no-console */
const { expect } = require('chai');
class BaseAssertions {
  constructor(res) {
    this.res = res;
  }
  assertThatANewRecordIsCreated() {
    if (this.res.statusCode != 201) {
      console.log(this.res.body);
    }
    expect(this.res.statusCode).to.be.eq(201);
  }

  assertStatusIsSuccessful() {
    if (this.res.statusCode != 200) {
      console.log(this.res.body);
    }
    expect(this.res.statusCode).to.be.eq(200);
  }

  assertBadRequest() {
    if (this.res.statusCode != 400) {
      console.log(this.res.body);
    }
    expect(this.res.statusCode).to.be.eq(400);
  }
  assertNoContent() {
    if (this.res.statusCode === 204) {
      console.log(this.res.body);
    }
    expect(this.res.statusCode).to.be.eq(204);
  }
  assertNotFound() {
    if (this.res.statusCode === 404) {
      console.log(this.res.body);
    }
    expect(this.res.statusCode).to.be.eq(404);
  }
}

module.exports = BaseAssertions;
