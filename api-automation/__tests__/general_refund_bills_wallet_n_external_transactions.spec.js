const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

const TransferClient = require('../clients/transfer_client');
const { ResponseAssertions } = require('../__assertions__/response_assertions');
const { TransferAssertions } = require('../__assertions__/transfer_assertions');
const { getByQuery } = require('../clients/transfer_db_client');
const {
  walletTopUpList,
  billPaymentList,
  creditCardGeneralRefundScenario,
  refundTransactionsErrorScenarios,
  transactionNotFoundList,
  externalBankGeneralRefundScenario,
  refundRdnTransactionsList
} = require('../__testData__/general_refund_bills_wallet_n_external_transactions.data');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');
let storySeverity = require('jest-allure/dist/Reporter');

describe('Verification of general refund APIs', () => {
  var transferClient = new TransferClient();
  let featureContext = new FeatureContext('Refund transactions', 'Transfer');
  var feature = new AllureFeature(featureContext);

  describe('Verifing positive scenarios of general refund APIs', () => {
    test.each(walletTopUpList)('%s', async (type, payload, done) => {
      feature.recordStory(
        'Validate refund of wallet top-up transactions',
        type,
        storySeverity.Severity.Normal
      );

      let transferResponse = await transferClient.topUpInternalTransactions(
        payload.transferRequest
      );
      new TransferAssertions(transferResponse).assertTransferIsSuccessful(
        payload.transferResponseKeys
      );

      await new Promise(function(resolve) {
        setTimeout(resolve, 5000);
      });

      const transferRefundResponse = await transferClient.refundTransfer(
        transferResponse.body.data.externalId,
        true
      );
      new TransferAssertions(
        transferRefundResponse
      ).assertTransferIsSuccessful(['id']);

      const refundTransactionDetails = await transferClient.getTransactionDetails(
        transferRefundResponse.body.data.id
      );
      new ResponseAssertions(
        refundTransactionDetails
      ).assertObjectResponseWithExpected(payload.refundTransactionDetails);
      done();
    });

    test.each(billPaymentList)('%s', async (type, payload, done) => {
      feature.recordStory(
        'Validate refund of bill payment transactions',
        type,
        storySeverity.Severity.Normal
      );
      let externalId;

      const transactionDetailsFromDB = await getByQuery(
        payload.queryToFetchTransaction
      );
      externalId = transactionDetailsFromDB.externalId;

      const transferRefundResponse = await transferClient.refundTransfer(
        externalId,
        true
      );
      new TransferAssertions(
        transferRefundResponse
      ).assertTransferIsSuccessful(['id']);

      const refundTransactionDetails = await transferClient.getTransactionDetails(
        transferRefundResponse.body.data.id
      );
      new ResponseAssertions(
        refundTransactionDetails
      ).assertObjectResponseWithExpected(payload.refundTransactionDetails);

      done();
    });

    test.each(creditCardGeneralRefundScenario)(
      '%s',
      async (type, payload, done) => {
        feature.recordStory(
          'Validate refund of bill payment transactions',
          type,
          storySeverity.Severity.Normal
        );
        let externalId;

        let transferResponse = await transferClient.topUpInternalTransactions(
          payload.transferRequest
        );
        new TransferAssertions(transferResponse).assertTransferIsSuccessful(
          payload.transferResponseKeys
        );

        await new Promise(function(resolve) {
          setTimeout(resolve, 5000);
        });
        externalId = transferResponse.body.data.externalId;

        const transferRefundResponse = await transferClient.refundTransfer(
          externalId,
          true
        );
        new TransferAssertions(
          transferRefundResponse
        ).assertTransferIsSuccessful(['id']);

        const refundTransactionDetails = await transferClient.getTransactionDetails(
          transferRefundResponse.body.data.id
        );
        new ResponseAssertions(
          refundTransactionDetails
        ).assertObjectResponseWithExpected(payload.refundTransactionDetails);

        done();
      }
    );

    test.each(externalBankGeneralRefundScenario)(
      '%s',
      async (type, payload, done) => {
        feature.recordStory(
          'Validate refund of bill payment transactions',
          type,
          storySeverity.Severity.Normal
        );
        let externalId;

        let transferResponse = await transferClient.transfer(
          payload.transferRequest
        );

        new TransferAssertions(transferResponse).assertTransferIsSuccessful(
          payload.transferResponseKeys
        );

        await new Promise(function(resolve) {
          setTimeout(resolve, 5000);
        });
        externalId = transferResponse.body.data.externalId;

        const transferRefundResponse = await transferClient.refundTransfer(
          externalId,
          true
        );

        new TransferAssertions(
          transferRefundResponse
        ).assertTransferIsSuccessful(['id']);

        const refundTransactionDetails = await transferClient.getTransactionDetails(
          transferRefundResponse.body.data.id
        );
        new ResponseAssertions(
          refundTransactionDetails
        ).assertObjectResponseWithExpected(payload.refundTransactionDetails);

        done();
      }
    );

    test.each(refundRdnTransactionsList)('%s', async (type, payload, done) => {
      feature.recordStory(
        'Validate refund of bill payment transactions',
        type,
        storySeverity.Severity.Normal
      );
      let externalId;

      let transferResponse = await transferClient.transfer(
        payload.transferRequest
      );
      new TransferAssertions(transferResponse).assertTransferIsSuccessful(
        payload.transferResponseKeys
      );

      await new Promise(function(resolve) {
        setTimeout(resolve, 5000);
      });
      externalId = transferResponse.body.data.externalId;

      const transferRefundResponse = await transferClient.refundTransfer(
        externalId,
        true
      );

      new TransferAssertions(
        transferRefundResponse
      ).assertTransferIsSuccessful(['id']);

      const refundTransactionDetails = await transferClient.getTransactionDetails(
        transferRefundResponse.body.data.id
      );
      new ResponseAssertions(
        refundTransactionDetails
      ).assertObjectResponseWithExpected(payload.refundTransactionDetails);

      done();
    });
  });

  describe('Verification of negative scenario for refund API', () => {
    test.each(refundTransactionsErrorScenarios)(
      '%s',
      async (type, payload, done) => {
        feature.recordStory(
          'Validate negative scenarios for refund transactions',
          type,
          storySeverity.Severity.Normal
        );

        const transactionDetailsFromDB = await getByQuery(
          payload.queryToFetchTransaction
        );
        externalId = transactionDetailsFromDB.externalId;

        const transferRefundResponse = await transferClient.refundTransfer(
          externalId,
          true
        );
        new ResponseAssertions(transferRefundResponse).assertBadRequestDetails(
          payload.transferResponse
        );

        done();
      }
    );

    test.each(transactionNotFoundList)('%s', async (type, payload, done) => {
      feature.recordStory(
        'Validate transaction not found scenario',
        type,
        storySeverity.Severity.Normal
      );
      externalId = '21321';

      const transferRefundResponse = await transferClient.refundTransfer(
        externalId,
        true
      );
      new ResponseAssertions(
        transferRefundResponse
      ).assertNotFoundMessageAndCode(
        payload.transferResponse.message,
        payload.transferResponse.code
      );

      done();
    });
  });
});
