/**
 * @group smoke/ms_transfer
 * @group external_transactions
 */
const chai = require('chai');
const chaiHttp = require('chai-http');
const util = require('../utils/common_functions');
chai.use(chaiHttp);
let storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');

const TransferClient = require('../clients/transfer_client');
const { TransferAssertions } = require('../__assertions__/transfer_assertions');
const {
  generalRefundExternalToInternalPayload,
  getBaseOriginalRTOLPayoad
} = require('../__testData__/general_refund_external-transaction.data');

describe('General Refund', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'RTOL Transactions',
    'Refund Transactions'
  );
  const feature = new AllureFeature(featureContext);

  describe('General Refund to External Bank', () => {
    test.each(generalRefundExternalToInternalPayload)(
      '%s',
      async (sourceAccountNumber, done) => {
        feature.recordStory(
          `Successful General Refund to External Bank using ${sourceAccountNumber}`,
          storySeverity.Severity.Normal
        );

        let baseOriginalTrnPayload = getBaseOriginalRTOLPayoad();
        const rtolTransferResponse = await transferClient.transfer(
          baseOriginalTrnPayload
        );

        new TransferAssertions(
          rtolTransferResponse
        ).assertTransferIsSuccessful([
          'id',
          'externalId',
          'referenceId',
          'transactionDate',
          'transactionIds'
        ]);

        const transactionId = rtolTransferResponse.body.data.id;
        util.sleepFor(3000);
        const transferResponse = await transferClient.refundTransfer(
          transactionId,
          true
        );
        new TransferAssertions(transferResponse).assertTransferIsSuccessful([
          'id'
        ]);
        done();
      }
    );
  });
});
