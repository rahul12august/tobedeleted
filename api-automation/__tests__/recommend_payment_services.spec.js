/**
 * @group smoke/ms_transfer
 * @group recommend-payment-services
 */

const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const config = require('config');
let storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');
const TransferClient = require('../clients/transfer_client');
const { ResponseAssertions } = require('../__assertions__/response_assertions');
const { resetUsageCounterByQuery } = require('../clients/award_db_client');
const {
  recommendedServiceList,
  BadRequestRecommendedService
} = require('../__testData__/recommend_payment_services.data');

const resetLimitGroupCodes = {
  monthlyAccumulationTransaction: 0,
  monthlyAccumulationAmount: 0,
  dailyAccumulationTransaction: 0,
  dailyAccumulationAmount: 0
};

describe('Verification of recommended payment services', () => {
  var transferClient = new TransferClient();
  let featureContext = new FeatureContext(
    'Recommended Payment Services',
    'Transfer'
  );
  var feature = new AllureFeature(featureContext);

  beforeAll(async () => {
    let query = { customerId: config.customerId };
    await resetUsageCounterByQuery(query, resetLimitGroupCodes);
  });

  test.each(recommendedServiceList)(
    '%s',
    async (type, request, response, done) => {
      feature.recordStory(
        'Validate positive scenarios for recommended payment services',
        type,
        storySeverity.Severity.Critical
      );
      let transferResponse = await transferClient.getRecommendedService(
        request
      );
      if (type.includes('SKN+RTGS')) {
        new ResponseAssertions(
          transferResponse
        ).assertArrayResponseWithExpected(response);
      } else
        new ResponseAssertions(
          transferResponse
        ).assertObjectResponseWithExpected(response);

      done();
    }
  );

  test.each(BadRequestRecommendedService)(
    '%s',
    async (type, request, [response], done) => {
      feature.recordStory(
        'Validate negative scenarios for recommended payment services',
        type,
        storySeverity.Severity.Critical
      );
      let transferResponse = await transferClient.getRecommendedService(
        request
      );
      new ResponseAssertions(transferResponse).assertBadRequestDetails(
        response
      );
      done();
    }
  );
});
