/* eslint-disable no-console */
/**
 * @group regression/ms_transfer
 * @group external_transactions
 */
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
const TransferClient = require('../clients/transfer_client');
const { getByExternalId } = require('../clients/transfer_db_client');
const {
  errorPayloadList,
  getTransactionPayload,
  transactionPayloadList,
  getBaseThirdPartyPayload,
  getRtolTransactionBasePayload,
  rtolTransactionConfirmationList
} = require('../__testData__/transaction_confirmation.data');
let storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');
const { ResponseAssertions } = require('../__assertions__/response_assertions');
let externalId = '';
describe('Verify transaction confirmation test cases', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'Transaction confirmation',
    'Transaction confirmation'
  );
  const feature = new AllureFeature(featureContext);

  describe('Transaction Confirmation negative test cases', () => {
    beforeAll(async () => {
      let thirdPartyTransferPayload = {
        ...getBaseThirdPartyPayload()
      };

      externalId = thirdPartyTransferPayload.externalId;
      await transferClient.externalTransfer(thirdPartyTransferPayload);
    });
    test.each(errorPayloadList)('%s', async (type, payload, done) => {
      feature.recordStory(`${type}`, `${type}`, storySeverity.Severity.Normal);

      let transactionConfirmationPayload = {
        ...getTransactionPayload(),
        externalId: externalId,
        responseCode: payload.responseCode
      };
      if (payload.code === 'INVALID_EXTERNALID') {
        transactionConfirmationPayload.externalId = '12211221112';
      }
      const transactionResponse = await transferClient.transactionConfirmation(
        transactionConfirmationPayload
      );
      new ResponseAssertions(
        transactionResponse
      ).assertBadRequestMessageAndCode(payload.message, payload.code);

      done();
    });
    afterEach(async () => {
      let transactionConfirmationPayload = {
        ...getTransactionPayload(),
        externalId: externalId,
        responseCode: 400
      };
      await transferClient.transactionConfirmation(
        transactionConfirmationPayload
      );
    });
  });

  describe('Transaction Confirmation positive test cases', () => {
    beforeAll(async () => {
      let thirdPartyTransferPayload = {
        ...getBaseThirdPartyPayload()
      };

      externalId = thirdPartyTransferPayload.externalId;

      const thirdPartyTransferResponse = await transferClient.externalTransfer(
        thirdPartyTransferPayload
      );

      transactionId = thirdPartyTransferResponse.body.data.id;
    });
    test.each(transactionPayloadList)('%s', async (type, payload, done) => {
      feature.recordStory(`${type}`, `${type}`, storySeverity.Severity.Normal);

      let transactionConfirmationPayload = {
        ...getTransactionPayload(),
        externalId: externalId,
        responseCode: payload.responseCode
      };

      await transferClient.transactionConfirmation(
        transactionConfirmationPayload
      );

      done();
    });
  });
  describe('Validate Transaction Confirmation for RTOL Transaction', () => {
    let thirdPartyOutgoingId = '';
    beforeEach(async () => {
      let rtolTransactionPayload = {
        ...getRtolTransactionBasePayload()
      };
      const transferTransactionResponse = await transferClient.transfer(
        rtolTransactionPayload
      );

      const transactionDetails = await getByExternalId(
        transferTransactionResponse.body.data.externalId
      );
      transactionId = transferTransactionResponse.body.data.id;
      thirdPartyOutgoingId = transactionDetails.thirdPartyOutgoingId;
    });
    test.each(rtolTransactionConfirmationList)(
      '%s',
      async (type, payload, done) => {
        feature.recordStory(
          `${type}`,
          `${type}`,
          storySeverity.Severity.Critical
        );

        let transactionConfirmationPayload = {
          ...getTransactionPayload(),
          externalId: thirdPartyOutgoingId,
          responseCode: payload.responseCode
        };

        await transferClient.transactionConfirmation(
          transactionConfirmationPayload
        );
        done();
      }
    );
  });
});
