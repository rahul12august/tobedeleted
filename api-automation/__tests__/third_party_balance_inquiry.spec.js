/**
 * @group smoke/ms_transfer
 * @group external_transactions
 */
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const TransferClient = require('../clients/transfer_client');
const { TransferAssertions } = require('../__assertions__/transfer_assertions');
const { ResponseAssertions } = require('../__assertions__/response_assertions');
const {
  thirdPartyBalanceInquiryReqResExpResultList,
  BadRequestNExpectedResultList
} = require('../__testData__/third_party_balance_inquiry.data');
const storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');

describe('Verification of Third Party Balance Inquiry', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'External Transactions',
    'Third Party Balance Inquiry'
  );
  const feature = new AllureFeature(featureContext);
  test.each(thirdPartyBalanceInquiryReqResExpResultList)(
    '%s',
    async (type, request, responseKeys, expectedResult, done) => {
      feature.recordStory(
        'Validate Third Party Balance Inquiry for all accountTypes',
        type,
        storySeverity.Severity.Critical
      );

      const transferResponse = await transferClient.externalTransfer(request);

      new TransferAssertions(transferResponse).assertTransferIsSuccessful(
        responseKeys
      );
      done();
    }
  );

  test.each(BadRequestNExpectedResultList)(
    '%s',
    async (type, request, expectedResult, done) => {
      feature.recordStory(
        'Validate negative scenario for Third Party Balance Inquiry',
        type,
        storySeverity.Severity.Critical
      );
      let transferResponse = await transferClient.externalTransfer(request);
      new ResponseAssertions(transferResponse).assertBadRequestDetails(
        expectedResult
      );
      done();
    }
  );
});
