/**
 * @group regression/ms_transfer
 * @group external_transactions
 */
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
const { utilConstants } = require('../utils/constants');

const TransferClient = require('../clients/transfer_client');
const {
  TransferAssertions,
  TransactionDetailsAssertions
} = require('../__assertions__/transfer_assertions');
const {
  transactionPayloadList,
  getTransactionPayload,
  errorPayloadList
} = require('../__testData__/iris_to_jago_transaction.data');
let storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');
const { ResponseAssertions } = require('../__assertions__/response_assertions');

describe('Verify Gopay withdrawal test cases', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'Gopay withdrawaltransaction',
    'Gopay withdrawaltransaction'
  );
  const feature = new AllureFeature(featureContext);

  describe('Gopay withdrawal transaction test cases using various beneficiary account types', () => {
    test.each(transactionPayloadList)('%s', async (type, payload, done) => {
      feature.recordStory(`${type}`, `${type}`, storySeverity.Severity.Normal);

      let gopayWithdrawalPayload = {
        ...getTransactionPayload(),
        beneficiaryAccountNo: payload.beneficiaryAccountNo
      };

      const transactionResponse = await transferClient.externalTransfer(
        gopayWithdrawalPayload
      );

      new TransferAssertions(transactionResponse).assertTransferIsSuccessful([
        'id',
        'externalId',
        'availableBalance'
      ]);
      const transactionId = transactionResponse.body.data.id;
      const transactionDetailsResponse = await transferClient.getTransactionDetails(
        transactionId
      );
      new TransactionDetailsAssertions(
        transactionDetailsResponse
      ).assertTransactionDetails(
        'SUCCEED',
        utilConstants.paymentServiceType.IRIS_TO_JAGO,
        utilConstants.paymentServiceCode.IRIS_OTH_TO_JAGO,
        utilConstants.transactionAmount.MIN_AMOUNT
      );
      done();
    });
  });
  describe('Gopay withdrawal transaction error test cases', () => {
    test.each(errorPayloadList)('%s', async (type, errorPayload, done) => {
      feature.recordStory(`${type}`, `${type}`, storySeverity.Severity.Normal);
      let gopayWithdrawalPayload = {
        ...getTransactionPayload(),
        beneficiaryAccountNo: errorPayload.beneficiaryAccountNo,
        transactionAmount: errorPayload.transactionAmount
      };

      const transactionResponse = await transferClient.externalTransfer(
        gopayWithdrawalPayload
      );

      new ResponseAssertions(
        transactionResponse
      ).assertBadRequestMessageAndCode(
        errorPayload.errorMessage,
        errorPayload.errorCode
      );

      done();
    });
  });
});
