/**
 * @group smoke/ms_transfer
 * @group external_transactions
 */
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
let storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');

const TransferClient = require('../clients/transfer_client');
const { TransferAssertions } = require('../__assertions__/transfer_assertions');
const {
  getInternationalPaymentBasePayload,
  getInternationalVoidPaymentBasePayload
} = require('../__testData__/international_payment_and_refund.data');
const {
  internationalPaymentInternalToExternalPayload
} = require('../__testData__/international_payment_and_refund.data');

describe('Domestic Payment and Refund', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'External Transactions',
    'Third Party Transfer'
  );
  const feature = new AllureFeature(featureContext);

  describe('International Payment to External Bank using interchange VISA', () => {
    test.each(internationalPaymentInternalToExternalPayload)(
      'PaymentServiceCode %s',
      async (
        paymentServiceCode,
        sourceBankCode,
        sourceAccountNumber,
        beneficiaryBankCode,
        interchange
      ) => {
        feature.recordStory(
          `Successful International Payment to External Bank using ${sourceAccountNumber}`,
          `Successful International Paymentto External Bank using ${sourceAccountNumber}`,
          storySeverity.Severity.Normal
        );
        let baseInternationalPaymentPayload = getInternationalPaymentBasePayload();
        baseInternationalPaymentPayload.sourceBankCode = sourceBankCode;
        baseInternationalPaymentPayload.sourceAccountNo = sourceAccountNumber;
        baseInternationalPaymentPayload.beneficiaryBankCode = beneficiaryBankCode;
        baseInternationalPaymentPayload.interchange = interchange;

        let transferResponse = await transferClient.externalTransfer(
          baseInternationalPaymentPayload
        );

        new TransferAssertions(transferResponse).assertTransferIsSuccessful([
          'id',
          'externalId',
          'availableBalance'
        ]);

        let baseInternationalVoidPaymentPayload = getInternationalVoidPaymentBasePayload();
        baseInternationalVoidPaymentPayload.sourceBankCode = beneficiaryBankCode;
        baseInternationalVoidPaymentPayload.beneficiaryBankCode = sourceBankCode;
        baseInternationalVoidPaymentPayload.beneficiaryAccountNo = sourceAccountNumber;
        baseInternationalVoidPaymentPayload.notes =
          baseInternationalPaymentPayload.externalId;

        transferResponse = await transferClient.externalTransfer(
          baseInternationalVoidPaymentPayload
        );
        new TransferAssertions(transferResponse).assertTransferIsSuccessful([
          'id',
          'externalId'
        ]);
      }
    );
  });
});
