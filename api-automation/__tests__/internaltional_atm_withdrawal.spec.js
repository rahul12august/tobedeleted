/**
 * @group smoke/ms_transfer
 * @group external_transactions
 */
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);

const TransferClient = require('../clients/transfer_client');
const { TransferAssertions } = require('../assertions/transfer_assertions');
const {
  transactionPayloadList,
  getTransactionPayload
} = require('../__testData__/international_atm_withdrawal.data');
let storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');

describe('Verify International ATM withdrawal test cases', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'International ATM withdrawal',
    'International ATM withdrawal'
  );
  const feature = new AllureFeature(featureContext);

  describe('International ATM Withdrawal using various account types and interchange', () => {
    test.each(transactionPayloadList)('%s', async (type, payload, done) => {
      feature.recordStory(`${type}`, `${type}`, storySeverity.Severity.Normal);

      let internationalAtmWithdrawalPayload = {
        ...getTransactionPayload(),
        sourceAccountNo: payload.sourceAccountNo,
        interchange: payload.interchange
      };

      const transferResponse = await transferClient.externalTransfer(
        internationalAtmWithdrawalPayload
      );

      new TransferAssertions(transferResponse).assertTransferIsSuccessful([
        'id',
        'externalId',
        'availableBalance'
      ]);

      done();
    });
  });
});
