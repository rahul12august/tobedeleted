/**
 * @group regression/ms_transfer
 * @group transfer_transactions
 */

const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const { utilConstants } = require('../utils/constants');
const TransferClient = require('../clients/transfer_client');
const {
  TransferAssertions,
  TransactionDetailsAssertions
} = require('../__assertions__/transfer_assertions');
const {
  getTransactionBasePayload,
  paymentPayload
} = require('../__testData__/offer_transfer_transaction.data');

const storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');

describe('Verify OFFER Transfer Test Cases', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'OFFER Transactions',
    'OFFER Transactions'
  );
  const feature = new AllureFeature(featureContext);

  describe('OFFER Transfer Transaction', () => {
    test.each(paymentPayload)(
      '%s',
      async (type, interchange, externalId, done) => {
        feature.recordStory(
          `Jago OFFER Transfers: ${type}`,
          `Jago OFFER Transfers: ${type}`,
          storySeverity.Severity.Critical
        );

        let internalTransactionBaseLoad = getTransactionBasePayload;
        internalTransactionBaseLoad.interchange = interchange;
        internalTransactionBaseLoad.externalId = externalId;

        const transferResponse = await transferClient.transfer(
          internalTransactionBaseLoad
        );

        new TransferAssertions(transferResponse).assertTransferIsSuccessful([
          'id',
          'externalId',
          'referenceId',
          'transactionDate',
          'transactionIds'
        ]);
        done();
      }
    );
  });
});
