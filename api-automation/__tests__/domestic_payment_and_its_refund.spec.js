/**
 * @group smoke/ms_transfer
 * @group external_transactions
 */
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
let storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');

const TransferClient = require('../clients/transfer_client');
const { TransferAssertions } = require('../__assertions__/transfer_assertions');
const {
  getDomesticPaymentBasePayload,
  getDomescticVoidPaymentBasePayload
} = require('../__testData__/domestic_payment_and_refund.data');
const {
  domesticPaymentPayloadList
} = require('../__testData__/domestic_payment_and_refund.data');

describe('Domestic Payment and Refund', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'External Transactions',
    'Domestic Payment'
  );
  const feature = new AllureFeature(featureContext);

  describe('From Main Account to External Bank', () => {
    test.each(domesticPaymentPayloadList)(
      'PaymentServiceCode %s',
      async (
        paymentServiceCode,
        sourceBankCode,
        sourceAccountNumber,
        beneficiaryBankCode,
        beneficiaryAccountNumber,
        interchange,
        done
      ) => {
        feature.recordStory(
          `Successful Domestic Payment from Main Account to External Bank using ${interchange}`,
          `Successful Domestic Payment from Main Account to External Bank ${interchange}`,
          storySeverity.Severity.Normal
        );
        let baseDomesticPaymentPayload = getDomesticPaymentBasePayload();
        baseDomesticPaymentPayload.sourceBankCode = sourceBankCode;
        baseDomesticPaymentPayload.sourceAccountNo = sourceAccountNumber;
        baseDomesticPaymentPayload.beneficiaryBankCode = beneficiaryBankCode;
        baseDomesticPaymentPayload.beneficiaryAccountNo = beneficiaryAccountNumber;
        baseDomesticPaymentPayload.interchange = interchange;

        const transferResponse = await transferClient.externalTransfer(
          baseDomesticPaymentPayload
        );
        new TransferAssertions(transferResponse).assertTransferIsSuccessful([
          'id',
          'externalId',
          'availableBalance'
        ]);

        let baseDomesticVoidPaymentPayload = getDomescticVoidPaymentBasePayload();
        baseDomesticVoidPaymentPayload.sourceBankCode = beneficiaryBankCode;
        baseDomesticVoidPaymentPayload.beneficiaryBankCode = sourceBankCode;
        baseDomesticVoidPaymentPayload.beneficiaryAccountNo = sourceAccountNumber;
        baseDomesticVoidPaymentPayload.interchange = interchange;
        baseDomesticVoidPaymentPayload.notes =
          baseDomesticPaymentPayload.externalId;
        const transferRefundResponse = await transferClient.externalTransfer(
          baseDomesticVoidPaymentPayload
        );
        new TransferAssertions(
          transferRefundResponse
        ).assertTransferIsSuccessful(['id', 'externalId']);

        done();
      }
    );
  });
});
