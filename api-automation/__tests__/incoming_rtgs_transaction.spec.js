/**
 * @group regression/ms_transfer
 * @group external_transactions
 */
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
const TransferClient = require('../clients/transfer_client');
const { TransferAssertions } = require('../__assertions__/transfer_assertions');
const {
  transactionPayloadList,
  getTransactionPayload,
  errorPayloadList
} = require('../__testData__/incoming_rtgs_transaction.data');
let storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');
const { ResponseAssertions } = require('../__assertions__/response_assertions');

describe('Verify incoming RTGS transaction test cases', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'Incoming RTGS transaction',
    'Incoming RTGS transaction'
  );
  const feature = new AllureFeature(featureContext);

  describe('Incoming RTGS transaction test cases using various beneficiary account types', () => {
    test.each(transactionPayloadList)('%s', async (type, payload, done) => {
      feature.recordStory(`${type}`, `${type}`, storySeverity.Severity.Normal);

      let incomingRTGSTransactionPayload = {
        ...getTransactionPayload(),
        beneficiaryAccountNo: payload.beneficiaryAccountNo
      };

      const transactionResponse = await transferClient.externalTransfer(
        incomingRTGSTransactionPayload
      );

      new TransferAssertions(transactionResponse).assertTransferIsSuccessful([
        'id',
        'externalId',
        'availableBalance'
      ]);
      done();
    });
  });
  describe('Incoming RTGS transaction error test cases', () => {
    test.each(errorPayloadList)('%s', async (type, errorPayload, done) => {
      feature.recordStory(`${type}`, `${type}`, storySeverity.Severity.Normal);
      let incomingRTGSTransactionPayload = {
        ...getTransactionPayload(),
        beneficiaryAccountNo: errorPayload.beneficiaryAccountNo,
        transactionAmount: errorPayload.transactionAmount
      };

      const transactionResponse = await transferClient.externalTransfer(
        incomingRTGSTransactionPayload
      );

      new ResponseAssertions(
        transactionResponse
      ).assertBadRequestMessageAndCode(
        errorPayload.errorMessage,
        errorPayload.errorCode
      );

      done();
    });
  });
});
