/* eslint-disable no-console */
/**
 * @group smoke/ms_transfer
 * @group internal_transactions
 */
const chai = require('chai');
const chaiHttp = require('chai-http');
const { utilConstants } = require('../utils/constants');
chai.use(chaiHttp);
const TransferClient = require('../clients/transfer_client');
const {
  TransferAssertions,
  TransactionDetailsAssertions
} = require('../__assertions__/transfer_assertions');
const {
  internalPaymentPayloadListDifferentCIF,
  internalPaymentPayloadListSameCIF,
  minAmountTransferValidationListSameCIF
} = require('../__testData__/jago_to_jago_transfer.data');

const {
  getInternalTransactionBasePayload
} = require('../__testData__/jago_to_jago_transfer.data');

const storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');

describe('Verify Jago to Jago Transfer Test Cases', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'Internal Transactions',
    'Jago - Jago Transfer'
  );
  const feature = new AllureFeature(featureContext);

  describe('Jago Transfers different CIFs ', () => {
    test.each(internalPaymentPayloadListDifferentCIF)(
      '%s',
      async (
        type,
        sourceAccountNo,
        transactionAmount,
        beneficiaryAccountNo,
        externalId,
        done
      ) => {
        feature.recordStory(
          `Jago Transfers different CIFs : ${type}`,
          `Jago Transfers different CIFs : ${type}`,
          storySeverity.Severity.Critical
        );

        let internalTransactionBaseLoad = getInternalTransactionBasePayload;
        internalTransactionBaseLoad.sourceAccountNo = sourceAccountNo;
        internalTransactionBaseLoad.transactionAmount = transactionAmount;
        internalTransactionBaseLoad.beneficiaryAccountNo = beneficiaryAccountNo;
        internalTransactionBaseLoad.externalId = externalId;

        const transferResponse = await transferClient.transfer(
          internalTransactionBaseLoad
        );

        new TransferAssertions(transferResponse).assertTransferIsSuccessful([
          'id',
          'externalId',
          'referenceId',
          'transactionDate',
          'transactionIds'
        ]);
        done();
      }
    );
  });

  describe('Jago Transfers same CIFs', () => {
    test.each(internalPaymentPayloadListSameCIF)(
      '%s',
      async (
        type,
        sourceAccountNo,
        transactionAmount,
        beneficiaryAccountNo,
        externalId,
        paymentServiceCode,
        done
      ) => {
        feature.recordStory(
          `Successful Jago Transfers same CIFs-  ${type}`,
          `Successful Jago Transfers same CIFs- ${type}`,
          storySeverity.Severity.Critical
        );

        let internalPaymentPayloadListSameCIF = getInternalTransactionBasePayload;
        internalPaymentPayloadListSameCIF.sourceAccountNo = sourceAccountNo;
        internalPaymentPayloadListSameCIF.transactionAmount = transactionAmount;
        internalPaymentPayloadListSameCIF.beneficiaryAccountNo = beneficiaryAccountNo;
        internalPaymentPayloadListSameCIF.externalId = externalId;

        const transferResponseSameCif = await transferClient.transfer(
          internalPaymentPayloadListSameCIF
        );

        new TransferAssertions(
          transferResponseSameCif
        ).assertTransferIsSuccessful([
          'id',
          'externalId',
          'referenceId',
          'transactionDate',
          'transactionIds'
        ]);

        done();
      }
    );
  });
  describe('Verify Minimum amount for FS> MA & DC to MA is 0.01', () => {
    test.each(minAmountTransferValidationListSameCIF)(
      '%s',
      async (
        type,
        sourceAccountNo,
        transactionAmount,
        beneficiaryAccountNo,
        externalId,
        paymentServiceCode,
        done
      ) => {
        feature.recordStory(
          `Verify Minimum amount transfer is 0.01-  ${type}`,
          `Verify Minimum amount transfer is 0.01- ${type}`,
          storySeverity.Severity.Critical
        );

        let minAmountTransferValidationListSameCIF = getInternalTransactionBasePayload;
        minAmountTransferValidationListSameCIF.sourceAccountNo = sourceAccountNo;
        minAmountTransferValidationListSameCIF.transactionAmount = transactionAmount;
        minAmountTransferValidationListSameCIF.beneficiaryAccountNo = beneficiaryAccountNo;
        minAmountTransferValidationListSameCIF.externalId = externalId;

        const transferResponseMinAmount = await transferClient.transfer(
          minAmountTransferValidationListSameCIF
        );

        new TransferAssertions(
          transferResponseMinAmount
        ).assertTransferIsSuccessful([
          'id',
          'externalId',
          'referenceId',
          'transactionDate',
          'transactionIds'
        ]);

        done();
      }
    );
  });
});
