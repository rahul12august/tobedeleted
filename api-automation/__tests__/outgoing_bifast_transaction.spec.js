const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const TransferClient = require('../clients/transfer_client');
const { TransferAssertions } = require('../__assertions__/transfer_assertions');
const {
  getBifastTransactionBasePayload
} = require('../__testData__/outgoing_bifast_transactions.data');

const {
  bifastTransactionPayloadList
} = require('../__testData__/outgoing_bifast_transactions.data');
const storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');

describe('Verify BIfast Outgoing Transactions using Komi', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'Transfer Transactions',
    'Outgoing BIFast Transfer'
  );
  const feature = new AllureFeature(featureContext);

  test.each(bifastTransactionPayloadList)(
    '%s',
    async (
      type,
      sourceAccountNo,
      transactionAmount,
      externalId,
      additionalInformation3,
      beneficiaryAccountNo,
      additionalInformtion4
    ) => {
      feature.recordStory(
        `BIfast Transfer : ${type}`,
        `Outgoing BIfast Transaction : ${type}`,
        storySeverity.Severity.Normal
      );

      let bifastTransactionBasePayload = getBifastTransactionBasePayload();
      bifastTransactionBasePayload.sourceAccountNo = sourceAccountNo;
      bifastTransactionBasePayload.transactionAmount = transactionAmount;
      bifastTransactionBasePayload.externalId = externalId;
      bifastTransactionBasePayload.additionalInformation3 = additionalInformation3;
      bifastTransactionBasePayload.beneficiaryAccountNo = beneficiaryAccountNo;
      bifastTransactionBasePayload.additionalInformation4 = additionalInformtion4;

      const transferResponse = await transferClient.transfer(
        bifastTransactionBasePayload
      );
      new TransferAssertions(transferResponse).assertTransferIsSuccessful([
        'id',
        'externalId',
        'referenceId',
        'transactionDate',
        'transactionIds'
      ]);
    }
  );
});
