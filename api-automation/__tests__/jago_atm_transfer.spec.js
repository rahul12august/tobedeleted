/**
 * @group smoke/ms_transfer
 * @group external_transactions
 */
const { utilConstants } = require('../utils/constants');
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
let storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');

const TransferClient = require('../clients/transfer_client');
const { TransferAssertions } = require('../assertions/transfer_assertions');
const {
  getJagoAtmTransferPayload,
  jagoAtmTransferPayloadList
} = require('../__testData__/jago_atm_transfer.data');

describe('Jago ATM Transfer', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext('External Transactions');
  const feature = new AllureFeature(featureContext);

  describe('Jago ATM Transfer to External Bank', () => {
    test.each(jagoAtmTransferPayloadList)('%s', async (type, payload, done) => {
      feature.recordStory(
        `Successful Jago ATM Transfer :  ${type}`,
        `Successful Jago ATM Transfer :  ${type}`,
        storySeverity.Severity.Normal
      );
      let jagoAtmTransferInput = {
        ...getJagoAtmTransferPayload(),
        beneficiaryBankCode: payload.beneficiaryBankCode,
        sourceBankCode: payload.sourceBankCode,
        sourceAccountNo: payload.sourceAccountNo,
        beneficiaryAccountNo: payload.beneficiaryAccountNo,
        interchange: payload.interchange
      };

      let jagoAtmTransferResponse = await transferClient.externalTransfer(
        jagoAtmTransferInput
      );

      new TransferAssertions(
        jagoAtmTransferResponse
      ).assertTransferIsSuccessful(['id', 'externalId', 'availableBalance']);

      done();
    });
  });
});
