/* eslint-disable no-console */
/**
 * @group regression/ms_transfer
 * @group transfer_transactions
 */
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const { utilConstants } = require('../utils/constants');
const TransferClient = require('../clients/transfer_client');
const {
  TransferAssertions,
  TransactionDetailsAssertions
} = require('../__assertions__/transfer_assertions');
const {
  getCashbackPayload,
  paymentPayload
} = require('../__testData__/cashback_transaction.data');

const storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');

describe('Verify CASHBACK Transfer Test Cases', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'CASHBACK Transactions',
    'CASHBACK test cases'
  );
  const feature = new AllureFeature(featureContext);

  describe('Jago CASHBACK Transfer Transaction', () => {
    test.each(paymentPayload)('%s', async (type, payload, done) => {
      feature.recordStory(
        `Jago CASHBACK Transfers: ${type}`,
        `Jago CASHBACK Transfers: ${type}`,
        storySeverity.Severity.Normal
      );
      let cashbackTransactionPayload = {
        ...getCashbackPayload(),
        interchange: payload.interchange,
        beneficiaryAccountNo: payload.beneficiaryAccountNo,
        externalId: payload.externalId
      };

      const transferResponse = await transferClient.transfer(
        cashbackTransactionPayload
      );

      new TransferAssertions(transferResponse).assertTransferIsSuccessful([
        'externalId',
        'id',
        'referenceId',
        'transactionDate',
        'transactionIds',
        'transactionStatus'
      ]);

      done();
    });
  });
});
