/**
 * @group regression/ms_transfer
 * @group rdn_transfer_transactions
 */

const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

let storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');
const TransferClient = require('../clients/transfer_client');
const { ResponseAssertions } = require('../__assertions__/response_assertions');
const { TransferAssertions } = require('../__assertions__/transfer_assertions');
const { getByExternalId } = require('../clients/transfer_db_client');
const {
  rdnTransactionsList,
  errorScenariosList
} = require('../__testData__/rdn_transfer_transactions.data');

describe('Verification of RDN transactions', () => {
  var transferClient = new TransferClient();
  let featureContext = new FeatureContext(
    'Recommended Payment Services',
    'Transfer'
  );
  var feature = new AllureFeature(featureContext);

  test.each(rdnTransactionsList)(
    '%s',
    async (type, payload, done) => {
      feature.recordStory(
        'Validate RDN transactions',
        type,
        storySeverity.Severity.Critical
      );

      let transferResponse = await transferClient.transfer(
        payload.rdnTransactionRequest
      );
      new TransferAssertions(transferResponse).assertTransferIsSuccessful(
        payload.rdnTransactionResponseKeys
      );

      await new Promise(function(resolve) {
        setTimeout(resolve, 3000);
      });

      const transactionDetailsFromDB = await getByExternalId(
        transferResponse.body.data.externalId
      );

      new ResponseAssertions(
        transactionDetailsFromDB
      ).assertTransactionDetailsDB(payload.rdnTransactionDetailsDb);

      done();
    },
    20000
  );

  test.each(errorScenariosList)(
    '%s',
    async (type, request, [response], done) => {
      feature.recordStory(
        'Validate negative scenarios for recommended payment services',
        type,
        storySeverity.Severity.Critical
      );
      let transferResponse = await transferClient.transfer(request);
      new ResponseAssertions(transferResponse).assertBadRequestDetails(
        response
      );
      done();
    }
  );
});
