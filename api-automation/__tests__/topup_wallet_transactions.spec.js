/**
 * @group smoke/ms_transfer
 * @group top-up-internal-transactions
 */

const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const TransferClient = require('../clients/transfer_client');
const { ResponseAssertions } = require('../__assertions__/response_assertions');
const { TransferAssertions } = require('../__assertions__/transfer_assertions');
const {
  topUpWalletReqResExpResultList,
  BadRequestNExpectedResultList
} = require('../__testData__/topup_wallet_transactions.data');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');
let storySeverity = require('jest-allure/dist/Reporter');

describe('Verification of e-wallet top-up transactions', () => {
  var transferClient = new TransferClient();
  let featureContext = new FeatureContext(
    'Top-up e-wallet transactions',
    'Transfer'
  );
  var feature = new AllureFeature(featureContext);

  test.each(topUpWalletReqResExpResultList)(
    '%s',
    async (type, request, responseKeys, expectedResult, done) => {
      feature.recordStory(
        'Validate positive scenarios for e-wallet top-up transactions',
        type,
        storySeverity.Severity.Critical
      );

      let transferResponse = await transferClient.topUpInternalTransactions(
        request
      );
      new TransferAssertions(transferResponse).assertTransferIsSuccessful(
        responseKeys
      );

      done();
    }
  );

  test.each(BadRequestNExpectedResultList)(
    '%s',
    async (type, request, expectedResult, done) => {
      feature.recordStory(
        'Validate negative scenarios for e-wallet top-up transactions',
        type,
        storySeverity.Severity.Critical
      );
      let transferResponse = await transferClient.topUpInternalTransactions(
        request
      );
      new ResponseAssertions(transferResponse).assertBadRequestDetails(
        expectedResult
      );
      done();
    }
  );
});
