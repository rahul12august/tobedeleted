/**
 * @group smoke/ms_transfer
 * @group external_transactions
 */

const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

let storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');
const TransferClient = require('../clients/transfer_client');
const { ResponseAssertions } = require('../__assertions__/response_assertions');
const { TransferAssertions } = require('../__assertions__/transfer_assertions');
const {
  thirdpartyJagotoJagoReqResExpectedResList,
  BadRequestNExpectedResultList
} = require('../__testData__/thirdparty_jago_to_jago_credit_debit.data');

describe('Verification of Third Party Jago to Jago Credit & Debit transactions', () => {
  var transferClient = new TransferClient();
  let featureContext = new FeatureContext(
    'Third Party Jago to Jago',
    'Transfer'
  );
  var feature = new AllureFeature(featureContext);
  test.each(thirdpartyJagotoJagoReqResExpectedResList)(
    '%s',
    async (type, request, responseKeys, response, done) => {
      feature.recordStory(
        'Validate positive scenarios for Third Party Jago to Jago Credit & Debit Transactions',
        type,
        storySeverity.Severity.Normal
      );
      let transferResponse = await transferClient.externalTransfer(request);
      new TransferAssertions(transferResponse).assertTransferIsSuccessful(
        responseKeys
      );

      done();
    }
  );

  test.each(BadRequestNExpectedResultList)(
    '%s',
    async (type, request, response, done) => {
      feature.recordStory(
        'Validate negative scenarios for Third Party Jago to Jago Credit & Debit Transactions',
        type,
        storySeverity.Severity.Normal
      );
      let transferResponse = await transferClient.externalTransfer(request);
      new ResponseAssertions(transferResponse).assertBadRequestDetails(
        response
      );
      done();
    }
  );
});
