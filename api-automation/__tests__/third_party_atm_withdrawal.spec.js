/**
 * @group smoke/ms_transfer
 * @group external_transactions
 */
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
const TransferClient = require('../clients/transfer_client');
const { TransferAssertions } = require('../__assertions__/transfer_assertions');
const {
  transactionPayloadList,
  getTransactionPayload,
  errorPayloadList
} = require('../__testData__/third_party_atm_withdrawal.data');
let storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');
const { ResponseAssertions } = require('../__assertions__/response_assertions');

describe('Verify Third Party ATM withdrawal test cases', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'Third Party ATM withdrawal',
    'Third Party ATM withdrawal'
  );
  const feature = new AllureFeature(featureContext);

  describe('Third Party ATM Withdrawal using various account types and interchange', () => {
    test.each(transactionPayloadList)('%s', async (type, payload, done) => {
      feature.recordStory(`${type}`, `${type}`, storySeverity.Severity.Normal);

      let internationalAtmWithdrawalPayload = {
        ...getTransactionPayload(),
        sourceAccountNo: payload.sourceAccountNo,
        interchange: payload.interchange,
        beneficiaryBankCode: payload.beneficiaryBankCode,
        notes: payload.notes
      };

      const transferResponse = await transferClient.externalTransfer(
        internationalAtmWithdrawalPayload
      );

      new TransferAssertions(transferResponse).assertTransferIsSuccessful([
        'id',
        'externalId',
        'availableBalance'
      ]);

      done();
    });
  });
  describe('Third Party ATM Withdrawal error cases', () => {
    test.each(errorPayloadList)('%s', async (type, payload, done) => {
      feature.recordStory(`${type}`, `${type}`, storySeverity.Severity.Normal);

      let internationalAtmWithdrawalPayload = {
        ...getTransactionPayload(),
        sourceAccountNo: payload.sourceAccountNo,
        interchange: payload.interchange,
        beneficiaryBankCode: payload.beneficiaryBankCode,
        notes: payload.notes,
        transactionAmount: payload.transactionAmount
      };

      const transferResponse = await transferClient.externalTransfer(
        internationalAtmWithdrawalPayload
      );

      new ResponseAssertions(transferResponse).assertBadRequestMessageAndCode(
        payload.errorMessage,
        payload.errorCode
      );
      done();
    });
  });
});
