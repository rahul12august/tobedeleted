/**
 * @group regression/ms_transfer
 * @group outgoing_skn
 */
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const config = require('config');
const TransferClient = require('../clients/transfer_client');
const { ResponseAssertions } = require('../__assertions__/response_assertions');
const { TransferAssertions } = require('../__assertions__/transfer_assertions');
const { getByExternalId } = require('../clients/transfer_db_client');
const { resetUsageCounterByQuery } = require('../clients/award_db_client');
const {
  outgoingTransactionsListSKN,
  outgoingTransactionsErrorScenarios
} = require('../__testData__/outgoing_skn_and_its_refund.data');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');
let storySeverity = require('jest-allure/dist/Reporter');
const resetLimitGroupCodes = {
  monthlyAccumulationTransaction: 0,
  monthlyAccumulationAmount: 0,
  dailyAccumulationTransaction: 0,
  dailyAccumulationAmount: 0
};

describe('Outgoing SKN transactions & its refund', () => {
  var transferClient = new TransferClient();
  let featureContext = new FeatureContext(
    'Outgoing SKN & RTGS transactions',
    'Transfer'
  );
  var feature = new AllureFeature(featureContext);

  beforeAll(async () => {
    let query = { customerId: config.customerId };
    await resetUsageCounterByQuery(query, resetLimitGroupCodes);
  });

  describe('Verification of outgoing SKN transactions & its refund', () => {
    test.each(outgoingTransactionsListSKN)(
      '%s',
      async (type, payload, done) => {
        feature.recordStory(
          'Validate outgoing SKN transfer transactions & its refund',
          type,
          storySeverity.Severity.Critical
        );

        let transferResponse = await transferClient.transfer(
          payload.transferRequest
        );

        new TransferAssertions(transferResponse).assertTransferIsSuccessful(
          payload.transferResponseKeys
        );

        await new Promise(function(resolve) {
          setTimeout(resolve, 6000);
        });

        const transactionDetailsFromDB = await getByExternalId(
          transferResponse.body.data.externalId
        );
        new ResponseAssertions(
          transactionDetailsFromDB
        ).assertTransactionDetailsDB(payload.transferTransactionDetails);

        payload.refundRequest.notes =
          transactionDetailsFromDB.thirdPartyOutgoingId;
        payload.refundRequest.externalId = transactionDetailsFromDB.externalId;
        const transferRefundResponse = await transferClient.externalTransfer(
          payload.refundRequest
        );

        new TransferAssertions(
          transferRefundResponse
        ).assertTransferIsSuccessful(['id', 'externalId']);

        const refundTransactionDetails = await transferClient.getTransactionDetails(
          transferRefundResponse.body.data.id
        );

        new ResponseAssertions(
          refundTransactionDetails
        ).assertObjectResponseWithExpected(payload.refundTransactionDetails);

        done();
      },
      20000
    );
  });
  test.each(outgoingTransactionsErrorScenarios)(
    '%s',
    async (type, payload, done) => {
      feature.recordStory(
        'Validate negative scenarios for SKN transfer transactions',
        type,
        storySeverity.Severity.Critical
      );

      let transferResponse = await transferClient.transfer(
        payload.transferRequest
      );

      new ResponseAssertions(transferResponse).assertBadRequestDetails(
        payload.transferResponse
      );

      done();
    },
    15000
  );
});
