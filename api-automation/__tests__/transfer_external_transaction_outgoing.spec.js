/* eslint-disable no-console */
/**
 * @group smoke/ms_transfer
 * @group external_transactions
 */
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const { expect } = require('chai');
const TransferClient = require('../clients/transfer_client');
const { TransferAssertions } = require('../assertions/transfer_assertions');
const {
  getRtolTransactionBasePayload
} = require('../__testData__/transfer_external_transaction_outgoing.data');

const {
  rtolTransactionPayloadList,
  rtolTransactionErrorPayloadList
} = require('../__testData__/transfer_external_transaction_outgoing.data');
const storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');

describe('Verify RTOL Outgoing Transactions using ALTO and ARTAJASA Interchange', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'External Transactions',
    'Outgoing RTOL Transfer'
  );
  const feature = new AllureFeature(featureContext);

  test.each(rtolTransactionPayloadList)(
    '%s',
    async (
      type,
      sourceAccountNo,
      transactionAmount,
      externalId,
      interchange
    ) => {
      feature.recordStory(
        `RTOL Transfer : ${type}`,
        `Outgoing RTOL Transaction : ${type}`,
        storySeverity.Severity.Normal
      );

      let rtolTransactionBasePayload = getRtolTransactionBasePayload();
      rtolTransactionBasePayload.sourceAccountNo = sourceAccountNo;
      rtolTransactionBasePayload.transactionAmount = transactionAmount;
      rtolTransactionBasePayload.externalId = externalId;
      rtolTransactionBasePayload.interchange = interchange;

      const transferResponse = await transferClient.transfer(
        rtolTransactionBasePayload
      );

      new TransferAssertions(transferResponse).assertTransferIsSuccessful([
        'id',
        'externalId',
        'referenceId',
        'transactionDate',
        'transactionIds'
      ]);
    }
  );

  test.each(rtolTransactionErrorPayloadList)(
    '%s',
    async (
      type,
      sourceAccountNo,
      transactionAmount,
      externalId,
      interchange
    ) => {
      feature.recordStory(
        `RTOL Transfer : ${type}`,
        `Outgoing RTOL Transaction : ${type}`,
        storySeverity.Severity.Normal
      );

      let rtolTransactionErrorBasePayload = getRtolTransactionBasePayload();
      rtolTransactionErrorBasePayload.sourceAccountNo = sourceAccountNo;
      rtolTransactionErrorBasePayload.transactionAmount = transactionAmount;
      rtolTransactionErrorBasePayload.externalId = externalId;
      rtolTransactionErrorBasePayload.interchange = interchange;

      const transferErrorResponse = await transferClient.transfer(
        rtolTransactionErrorBasePayload
      );

      expect(transferErrorResponse.body.error.message).to.equal(
        'Invalid amount'
      );
      expect(transferErrorResponse.body.error.code).to.equal('INVALID_AMOUNT');
      expect(transferErrorResponse.statusCode).to.equal(400);
    }
  );
});
