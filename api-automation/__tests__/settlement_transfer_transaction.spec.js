/**
 * @group smoke/ms_transfer
 * @group internal_transactions
 */
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const TransferClient = require('../clients/transfer_client');
const { TransferAssertions } = require('../__assertions__/transfer_assertions');
const {
  getSettlementPayload,
  paymentPayload,
  errorPayload
} = require('../__testData__/settlement_transfer_transaction.data');

const storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');
const { ResponseAssertions } = require('../__assertions__/response_assertions');

describe('Verify SETTLEMENT Transfer Test Cases', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'SETTLEMENT Transactions',
    'SETTLEMENT Transfer'
  );
  const feature = new AllureFeature(featureContext);

  describe('SETTLEMENT Transfer Transaction', () => {
    test.each(paymentPayload)('%s', async (type, payload, done) => {
      feature.recordStory(
        `SETTLEMENT Transfers: ${type}`,
        `SETTLEMENT Transfers: ${type}`,
        storySeverity.Severity.Critical
      );

      let internalTransactionBaseLoad = {
        ...getSettlementPayload(),
        interchange: payload.interchange,
        beneficiaryAccountNo: payload.beneficiaryAccountNo
      };

      const transferResponse = await transferClient.transfer(
        internalTransactionBaseLoad
      );

      new TransferAssertions(transferResponse).assertTransferIsSuccessful([
        'id',
        'externalId',
        'referenceId',
        'transactionDate',
        'transactionIds'
      ]);

      done();
    });
  });

  describe('SETTLEMENT Transfer Transaction with invalid interchange', () => {
    test.each(errorPayload)('%s', async (type, payload, done) => {
      feature.recordStory(
        `SETTLEMENT Transfers: ${type}`,
        `SETTLEMENT Transfers: ${type}`,
        storySeverity.Severity.Critical
      );

      let internalTransactionBaseLoad = {
        ...getSettlementPayload(),
        interchange: payload.interchange,
        beneficiaryAccountNo: payload.beneficiaryAccountNo
      };

      const errorResponse = await transferClient.transfer(
        internalTransactionBaseLoad
      );

      new ResponseAssertions(errorResponse).assertBadRequestMessageAndCode(
        payload.expectedErrorMessage,
        payload.expectedErrorCode
      );
      done();
    });
  });
});
