/**
 * @group smoke/ms_transfer
 * @group external_transactions
 */
const { utilConstants } = require('../utils/constants');
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
let storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');

const TransferClient = require('../clients/transfer_client');
const {
  TransferAssertions,
  TransactionDetailsAssertions
} = require('../assertions/transfer_assertions');
const {
  getBaseThirdPartyPayload
} = require('../test_data/external-transactions/transfer_request_body');
const {
  thirdPartyPayloadOthToJagoList,
  thirdPartyPayloadJagoToOthList,
  thirdPartyPayloadWincorToJagoList,
  thirdPartyPayloadWincorToJagoFailure
} = require('../__testData__/third_party_transfer.data');
const { ERROR_CODE } = require('../../src/common/errors');

describe('Verify THIRD PARTY TRANSFER Payment Service Type Transaction', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'External Transactions',
    'THIRD PARTY TRANSFER'
  );
  const feature = new AllureFeature(featureContext);

  describe('THIRD PARTY TRANSFER Transaction for THIRDPARTY_OTH_TO_JAGO', () => {
    test.each(thirdPartyPayloadOthToJagoList)(
      'PaymentServiceCode %s',
      async (
        paymentServiceCode,
        sourceBankCode,
        sourceAccountNo,
        beneficiaryBankCode,
        beneficiaryAccountNo,
        accountType,
        done
      ) => {
        feature.recordStory(
          `Successful Incoming THIRD PARTY TRANSFER Transaction - THIRDPARTY_OTH_TO_JAGO - Accouny Type ${accountType}`,
          `Successful Incoming THIRD PARTY TRANSFER Transaction - THIRDPARTY_OTH_TO_JAGO - Accouny Type ${accountType}`,
          storySeverity.Severity.Normal
        );
        let baseThirdPartyPayload = getBaseThirdPartyPayload();
        baseThirdPartyPayload.sourceBankCode = sourceBankCode;
        baseThirdPartyPayload.sourceAccountNo = sourceAccountNo;
        baseThirdPartyPayload.beneficiaryBankCode = beneficiaryBankCode;
        baseThirdPartyPayload.beneficiaryAccountNo = beneficiaryAccountNo;

        const transferResponse = await transferClient.transfer(
          baseThirdPartyPayload
        );

        new TransferAssertions(transferResponse).assertTransferIsSuccessful([
          'id',
          'externalId',
          'availableBalance'
        ]);
        const transactionId = transferResponse.body.data.id;
        const transactionDetailsResponse = await transferClient.getTransactionDetails(
          transactionId
        );
        new TransactionDetailsAssertions(
          transactionDetailsResponse
        ).assertTransactionDetails(
          'SUCCEED',
          'THIRD_PARTY_TRANSFER',
          paymentServiceCode,
          utilConstants.transactionAmount.MIN_FE_AMOUNT
        );
        done();
      }
    );
  });

  describe('THIRD PARTY TRANSFER Transaction for THIRDPARTY_JAGO_TO_OTH', () => {
    test.each(thirdPartyPayloadJagoToOthList)(
      'PaymentServiceCode %s',
      async (
        paymentServiceCode,
        sourceBankCode,
        sourceAccountNo,
        beneficiaryBankCode,
        beneficiaryAccountNo,
        accountType,
        done
      ) => {
        feature.recordStory(
          `Successful Incoming THIRD PARTY TRANSFER Transaction - THIRDPARTY_JAGO_TO_OTH - Accouny Type ${accountType}`,
          `Successful Incoming THIRD PARTY TRANSFER Transaction - THIRDPARTY_JAGO_TO_OTH - Accouny Type ${accountType}`,
          storySeverity.Severity.Normal
        );
        let baseThirdPartyPayload = getBaseThirdPartyPayload();
        baseThirdPartyPayload.sourceBankCode = sourceBankCode;
        baseThirdPartyPayload.sourceAccountNo = sourceAccountNo;
        baseThirdPartyPayload.beneficiaryBankCode = beneficiaryBankCode;
        baseThirdPartyPayload.beneficiaryAccountNo = beneficiaryAccountNo;

        const transferResponse = await transferClient.transfer(
          baseThirdPartyPayload
        );

        new TransferAssertions(transferResponse).assertTransferIsSuccessful([
          'id',
          'externalId',
          'availableBalance'
        ]);
        done();
      }
    );
  });

  describe('THIRD PARTY TRANSFER Transaction for THIRDPARTY_WINCOR_TO_JAGO', () => {
    test.each(thirdPartyPayloadWincorToJagoList)(
      'PaymentServiceCode %s',
      async (
        paymentServiceCode,
        sourceBankCode,
        sourceAccountNo,
        beneficiaryBankCode,
        beneficiaryAccountNo,
        accountType,
        done
      ) => {
        feature.recordStory(
          `Successful Incoming THIRD PARTY TRANSFER Transaction - THIRDPARTY_WINCOR_TO_JAGO - Accouny Type ${accountType}`,
          `Successful Incoming THIRD PARTY TRANSFER Transaction - THIRDPARTY_WINCOR_TO_JAGO - Accouny Type ${accountType}`,
          storySeverity.Severity.Normal
        );
        let baseThirdPartyPayload = getBaseThirdPartyPayload();
        baseThirdPartyPayload.sourceBankCode = sourceBankCode;
        baseThirdPartyPayload.sourceAccountNo = sourceAccountNo;
        baseThirdPartyPayload.beneficiaryBankCode = beneficiaryBankCode;
        baseThirdPartyPayload.beneficiaryAccountNo = beneficiaryAccountNo;

        const transferResponse = await transferClient.transfer(
          baseThirdPartyPayload
        );

        new TransferAssertions(transferResponse).assertTransferIsSuccessful([
          'id',
          'externalId',
          'availableBalance'
        ]);

        done();
      }
    );

    test.each(thirdPartyPayloadWincorToJagoFailure)(
      'PaymentServiceCode %s - AccountType',
      async (
        paymentServiceCode,
        sourceBankCode,
        sourceAccountNo,
        beneficiaryBankCode,
        beneficiaryAccountNo,
        accountType,
        done
      ) => {
        feature.recordStory(
          `Failed Incoming THIRD PARTY TRANSFER Transaction - THIRDPARTY_WINCOR_TO_JAGO - Accouny Type ${accountType}`,
          `Failed Incoming THIRD PARTY TRANSFER Transaction - THIRDPARTY_WINCOR_TO_JAGO - Accouny Type ${accountType}`,
          storySeverity.Severity.Normal
        );
        let baseThirdPartyPayload = getBaseThirdPartyPayload();
        baseThirdPartyPayload.sourceBankCode = sourceBankCode;
        baseThirdPartyPayload.sourceAccountNo = sourceAccountNo;
        baseThirdPartyPayload.beneficiaryBankCode = beneficiaryBankCode;
        baseThirdPartyPayload.beneficiaryAccountNo = beneficiaryAccountNo;

        const transferResponse = await transferClient.transfer(
          baseThirdPartyPayload
        );

        new TransferAssertions(transferResponse).assertBadRequest();
        expect(transferResponse.body.error.code).toEqual(
          ERROR_CODE.NO_RECOMMENDATION_SERVICES
        );
        done();
      }
    );
  });
});
