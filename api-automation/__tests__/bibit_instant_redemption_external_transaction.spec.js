/**
 * @group smoke/ms_transfer
 * @group external_transactions
 */
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
let storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');

const TransferClient = require('../clients/transfer_client');
const { TransferAssertions } = require('../__assertions__/transfer_assertions');
const {
  getBIBITInstantRedemptionBasePayload,
  bibitInstantRedemptionExternalToInternalPayload,
  bibitInstantRedemptionExternalToInternalErrorPayload
} = require('../__testData__/bibit_instant_redemption_external_transaction.data');
const { ResponseAssertions } = require('../__assertions__/response_assertions');

describe('Bibit Instant Redemption', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext('External Transactions');
  const feature = new AllureFeature(featureContext);

  describe('Bibit Instant Redemption to External Bank', () => {
    test.each(bibitInstantRedemptionExternalToInternalPayload)(
      'PaymentServiceCode %s',
      async (
        paymentServiceCode,
        sourceBankCode,
        sourceAccountNumber,
        beneficiaryBankCode,
        beneficiaryAccountNumber,
        done
      ) => {
        feature.recordStory(
          `Successful Bibit Instant Redemption to External Bank using ${sourceAccountNumber}`,
          `Successful Bibit Instant Redemption to External Bank using ${sourceAccountNumber}`,
          storySeverity.Severity.Normal
        );
        let baseInternationalPaymentPayload = getBIBITInstantRedemptionBasePayload();
        baseInternationalPaymentPayload.sourceBankCode = sourceBankCode;
        baseInternationalPaymentPayload.sourceAccountNo = sourceAccountNumber;
        baseInternationalPaymentPayload.beneficiaryBankCode = beneficiaryBankCode;
        baseInternationalPaymentPayload.beneficiaryAccountNo = beneficiaryAccountNumber;
        let transferResponse = await transferClient.externalTransfer(
          baseInternationalPaymentPayload
        );

        new TransferAssertions(transferResponse).assertTransferIsSuccessful([
          'id',
          'externalId',
          'availableBalance'
        ]);
        done();
      }
    );

    test.each(bibitInstantRedemptionExternalToInternalErrorPayload)(
      'PaymentServiceCode %s',
      async (
        transactionAmount,
        sourceBankCode,
        beneficiaryAccountNumber,
        expectedErrorMessage,
        expectedErrorCode,
        done
      ) => {
        feature.recordStory(
          `Successful Bibit Instant Redemption to External Bank using ${sourceBankCode}`,
          `Successful Bibit Instant Redemption to External Bank using ${sourceBankCode}`,
          storySeverity.Severity.Normal
        );
        let baseInternationalPaymentPayload = getBIBITInstantRedemptionBasePayload();
        baseInternationalPaymentPayload.sourceBankCode = sourceBankCode;
        baseInternationalPaymentPayload.beneficiaryAccountNo = beneficiaryAccountNumber;
        baseInternationalPaymentPayload.transactionAmount = transactionAmount;
        var errorResponse = await transferClient.externalTransfer(
          baseInternationalPaymentPayload
        );

        new ResponseAssertions(errorResponse).assertBadRequestMessageAndCode(
          expectedErrorMessage,
          expectedErrorCode
        );
        done();
      }
    );
  });
});
