/**
 * @group regression/ms_transfer
 * @group transfer_transactions
 * @group payroll
 */
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const { utilConstants } = require('../utils/constants');
const TransferClient = require('../clients/transfer_client');
const {
  TransferAssertions,
  TransactionDetailsAssertions
} = require('../__assertions__/transfer_assertions');

const { ResponseAssertions } = require('../__assertions__/response_assertions');
const {
  getPayrollPayload,
  payrollPayloadList,
  errorPayrollInputList
} = require('../__testData__/payroll_transaction.data');

const storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');

describe('Verify Payroll Transfer Test Cases', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    'Payroll Transactions',
    'Payroll test cases'
  );
  const feature = new AllureFeature(featureContext);

  describe('Jago Payroll Transfer Transaction', () => {
    test.each(payrollPayloadList)('%s', async (type, payload, done) => {
      feature.recordStory(
        `Jago Payroll Transfers: ${type}`,
        `Jago Payroll Transfers: ${type}`,
        storySeverity.Severity.Normal
      );
      let PayrollTransactionPayload = {
        ...getPayrollPayload(),
        interchange: payload.interchange,
        beneficiaryAccountNo: payload.beneficiaryAccountNo,
        transactionAmount: payload.transactionAmount
      };

      const transferResponse = await transferClient.transfer(
        PayrollTransactionPayload
      );

      new TransferAssertions(transferResponse).assertTransferIsSuccessful([
        'id',
        'externalId',
        'referenceId',
        'transactionDate',
        'transactionIds'
      ]);
      done();
    });
  });

  describe('Jago Payroll Error Cases', () => {
    test.each(errorPayrollInputList)('%s', async (type, payload, done) => {
      feature.recordStory(
        `Payroll Transaction: ${type}`,
        `Payroll Transaction: ${type}`,
        storySeverity.Severity.Normal
      );
      let payrollTransactionPayload = {
        ...getPayrollPayload(),
        interchange: payload.interchange,
        beneficiaryAccountNo: payload.beneficiaryAccountNo,
        transactionAmount: payload.transactionAmount
      };

      const transferResponse = await transferClient.transfer(
        payrollTransactionPayload
      );

      new ResponseAssertions(transferResponse).assertBadRequestMessageAndCode(
        payload.errorMessage,
        payload.errorCode
      );

      done();
    });
  });
});
