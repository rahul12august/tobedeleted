/**
 * @group regression/ms_transfer
 * @group blocking_transactions
 */
const chai = require('chai');
let config = require('config');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const TransferClient = require('../clients/transfer_client');
const { utilConstants } = require('../utils/constants');
const {
  TransferAssertions,
  TransactionDetailsAssertions
} = require('../__assertions__/transfer_assertions');

const { ResponseAssertions } = require('../__assertions__/response_assertions');
const {
  getBlockingTransferPayload,
  getOrderAdjustmentPayload,
  getCaptureToFundPayload,
  blockingTransferPayLoadList
} = require('../__testData__/blocking_cancel_capture_to_fund.data');

const storySeverity = require('jest-allure/dist/Reporter');
const { AllureFeature, FeatureContext } = require('../reporter/allure_feature');

describe('Verify blocking transactions Test Cases', () => {
  const transferClient = new TransferClient();
  const featureContext = new FeatureContext(
    `Blocking,order adjustment,capture to fund,cancel Transactions`,
    `Blocking,order adjustment,capture to fund,cancel Transactions`
  );
  const feature = new AllureFeature(featureContext);

  describe('Blocking - Order Adjustment - Capture to Fund', () => {
    let transactionId = '';
    test.each(blockingTransferPayLoadList)(
      'Blocking Transaction-Order Adjustment-Capture To Fund- Cancel order- %s',
      async (type, payload, done) => {
        feature.recordStory(
          `${type}`,
          `${type}`,
          storySeverity.Severity.Critical
        );
        let blockingTransactionPayload = {
          ...getBlockingTransferPayload(),
          sourceAccountNo: payload.sourceAccountNo,
          beneficiaryBankCode: payload.beneficiaryBankCode,
          transactionAmount: payload.transactionAmount
        };

        const blockingTransferResponse = await transferClient.blockingTransferTransactions(
          blockingTransactionPayload
        );

        new TransferAssertions(
          blockingTransferResponse
        ).assertTransferIsSuccessful(['id']);

        transactionId = blockingTransferResponse.body.data.id;
        let transactionDetailsResponse = await transferClient.getTransactionDetails(
          transactionId
        );

        new TransactionDetailsAssertions(
          transactionDetailsResponse
        ).assertTransactionDetails(
          'SUBMITTED',
          'JAGOPAY',
          'SIT03',
          payload.transactionAmount
        );

        let orderAdjustmentPayload = {
          ...getOrderAdjustmentPayload()
        };

        const orderAdjustmentResponse = await transferClient.orderAdjustment(
          orderAdjustmentPayload,
          transactionId,
          config.sourceCif
        );

        new ResponseAssertions(
          orderAdjustmentResponse
        ).assertNoContentInResponse();

        transactionDetailsResponse = await transferClient.getTransactionDetails(
          transactionId
        );

        let captureToFundPayload = {
          ...getCaptureToFundPayload()
        };

        const captureToFundResponse = await transferClient.captureToFund(
          captureToFundPayload,
          transactionId,
          config.sourceCif
        );

        new ResponseAssertions(
          captureToFundResponse
        ).assertNoContentInResponse();

        transactionDetailsResponse = await transferClient.getTransactionDetails(
          transactionId
        );
        new TransactionDetailsAssertions(
          transactionDetailsResponse
        ).assertTransactionDetails(
          'SUCCEED',
          'JAGOPAY',
          'SIT03',
          utilConstants.transactionAmount.MIN_FE_AMOUNT
        );

        const cancelOrderResponse = await transferClient.cancelOrder(
          transactionId,
          config.sourceCif
        );

        new ResponseAssertions(
          cancelOrderResponse
        ).assertBadRequestMessageAndCode(
          'Transaction status invalid',
          'INVALID_TRANSACTION_STATUS'
        );

        done();
      },
      30000
    );
  });

  describe('Verify order cancellation and order adjustment cases', () => {
    let transactionId = '';
    let declinedTransactionId = '';
    let succeededTransactionId = '';
    beforeEach(async () => {
      let blockingTransactionPayload = {
        ...getBlockingTransferPayload()
      };

      const blockingTransferResponse = await transferClient.blockingTransferTransactions(
        blockingTransactionPayload
      );

      transactionId = blockingTransferResponse.body.data.id;
    });

    beforeAll(async () => {
      let blockingTransactionPayload = {
        ...getBlockingTransferPayload()
      };

      const blockingTransferResponse = await transferClient.blockingTransferTransactions(
        blockingTransactionPayload
      );

      declinedTransactionId = blockingTransferResponse.body.data.id;
      await transferClient.cancelOrder(declinedTransactionId, config.sourceCif);

      let captureToFundPayload = {
        ...getCaptureToFundPayload()
      };
      captureToFundPayload.amount = utilConstants.transactionAmount.MIN_AMOUNT;

      const blockingTransferResponseForSuccess = await transferClient.blockingTransferTransactions(
        getBlockingTransferPayload()
      );
      succeededTransactionId = blockingTransferResponseForSuccess.body.data.id;

      await transferClient.captureToFund(
        captureToFundPayload,
        succeededTransactionId,
        config.sourceCif
      );
    });

    test('Verify order confirmation amount should be same as blocked amount', async () => {
      feature.recordStory(
        `Error case capture to fund`,
        `Error case capture to fund`,
        storySeverity.Severity.Critical
      );

      let captureToFundPayload = {
        ...getCaptureToFundPayload()
      };

      const captureToFundResponse = await transferClient.captureToFund(
        captureToFundPayload,
        transactionId,
        config.sourceCif
      );

      new ResponseAssertions(
        captureToFundResponse
      ).assertBadRequestMessageAndCode('Invalid amount', 'INVALID_AMOUNT');
    });

    test('Verify user is able to successfully cancel a blocked transaction', async () => {
      feature.recordStory(
        `User to cancel a blocked transaction`,
        `User to cancel a blocked transaction`,
        storySeverity.Severity.Critical
      );

      const cancelOrderResponse = await transferClient.cancelOrder(
        transactionId,
        config.sourceCif
      );

      new ResponseAssertions(cancelOrderResponse).assertNoContentInResponse();

      let transactionDetailsResponse = await transferClient.getTransactionDetails(
        transactionId
      );

      new TransactionDetailsAssertions(
        transactionDetailsResponse
      ).assertTransactionDetails(
        'DECLINED',
        'JAGOPAY',
        'SIT03',
        utilConstants.transactionAmount.MIN_AMOUNT
      );
    });

    test('Verify declined transaction cannot be adjusted', async () => {
      feature.recordStory(
        `User cannot adjust a declined transaction`,
        `User cannot adjust a declined transaction`,
        storySeverity.Severity.Normal
      );

      let orderAdjustmentPayload = {
        ...getOrderAdjustmentPayload()
      };

      const orderAdjustmentResponse = await transferClient.orderAdjustment(
        orderAdjustmentPayload,
        declinedTransactionId,
        config.sourceCif
      );

      new ResponseAssertions(
        orderAdjustmentResponse
      ).assertBadRequestMessageAndCode(
        'Transaction status invalid',
        'INVALID_TRANSACTION_STATUS'
      );
    });

    test('Verify succeeded transaction cannot be adjusted', async () => {
      feature.recordStory(
        `User cannot adjust a succeeded transaction`,
        `User cannot adjust a succeeded transaction`,
        storySeverity.Severity.Normal
      );

      let orderAdjustmentPayload = {
        ...getOrderAdjustmentPayload()
      };

      const orderAdjustmentResponse = await transferClient.orderAdjustment(
        orderAdjustmentPayload,
        succeededTransactionId,
        config.sourceCif
      );

      new ResponseAssertions(
        orderAdjustmentResponse
      ).assertBadRequestMessageAndCode(
        'Transaction status invalid',
        'INVALID_TRANSACTION_STATUS'
      );
    });

    test('User trying to adjust a transaction which does not exist', async () => {
      feature.recordStory(
        `Transaction not found case`,
        `Transaction not found case`,
        storySeverity.Severity.Normal
      );
      let orderAdjustmentPayload = {
        ...getOrderAdjustmentPayload()
      };

      const orderAdjustmentResponse = await transferClient.orderAdjustment(
        orderAdjustmentPayload,
        'succeededTransactionId',
        config.sourceCif
      );

      new ResponseAssertions(
        orderAdjustmentResponse
      ).assertNotFoundMessageAndCode(
        'Could not find Transaction',
        'TRANSACTION_NOT_FOUND'
      );
    });
  });
});
