import { ITransactionModel } from '../transaction/transaction.model';
import { IRtgsRequestTemplate, IRtgsTransaction } from './rtgs.type';
import { formatTransactionDate } from '../transaction/transaction.util';
import { BankNetworkEnum } from '@dk/module-common';
import logger from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import {
  BRANCH_RTGS,
  EXCHANGE_RATE,
  KSEI,
  RDN_KSEI,
  RDN_KSEI_TRN,
  RDN_WITHDRAW_RTGS,
  RTGS,
  RTGS_FOR_BUSINESS,
  RTGS_SHARIA,
  RTGS_TRN
} from './rtgs.constant';
import { BANK_KSEI } from '../transaction/transaction.constant';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const rdnKseiRequestTemplate: IRtgsRequestTemplate = {
  getPayload: (transaction: ITransactionModel): IRtgsTransaction => {
    if (transaction.beneficiaryBankCode !== BANK_KSEI) {
      const detail = `Invalid Beneficiary BankCode ${transaction.beneficiaryBankCode} with 
        paymentServiceType:${transaction.paymentServiceType}, paymentServiceCode: ${transaction.paymentServiceCode}`;
      logger.error(`rdnKseiRequestTemplate: ${detail}`);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.SUBMITTER,
        ERROR_CODE.INVALID_BANK_CODE
      );
    }
    return {
      trn: RDN_KSEI_TRN, // Transaction Type : This is for the specific KSEI.
      externalId: KSEI + transaction.thirdPartyOutgoingId || '',
      transactionDate: formatTransactionDate(
        transaction.createdAt || new Date()
      ),
      transactionAmount: transaction.transactionAmount,
      beneficiaryBankCode: transaction.beneficiaryRemittanceCode || '',
      beneficiaryAccountNo: transaction.beneficiaryAccountNo || '',
      beneficiaryAccountName: transaction.beneficiaryAccountName || '',
      sourceBankCode: transaction.sourceRemittanceCode || '',
      sourceAccountNo: transaction.sourceAccountNo || '',
      sourceAccountName: transaction.sourceAccountName || '',
      currency: transaction.sourceTransactionCurrency,
      notes: transaction.note || '',
      interchange: BankNetworkEnum.RTGS,
      exchangeRate: EXCHANGE_RATE,
      addInfo1: transaction.note || ''
    };
  },
  isEligible: (transaction: ITransactionModel): boolean =>
    transaction.paymentServiceCode === RDN_KSEI
};

const rtgsRequestTemplate: IRtgsRequestTemplate = {
  getPayload: (transaction: ITransactionModel): IRtgsTransaction => ({
    trn: RTGS_TRN, // Transaction Type : Customer to Customer Transaction.
    externalId:
      (transaction.thirdPartyOutgoingId &&
        transaction.thirdPartyOutgoingId.padStart(16, '0')) ||
      '',
    transactionDate: formatTransactionDate(transaction.createdAt || new Date()),
    transactionAmount: transaction.transactionAmount,
    beneficiaryBankCode: transaction.beneficiaryRemittanceCode || '',
    beneficiaryAccountNo: transaction.beneficiaryAccountNo || '',
    beneficiaryAccountName: transaction.beneficiaryAccountName || '',
    sourceBankCode: transaction.sourceRemittanceCode || '',
    sourceAccountNo: transaction.sourceAccountNo || '',
    sourceAccountName: transaction.sourceAccountName || '',
    currency: transaction.sourceTransactionCurrency,
    notes: transaction.note || '',
    interchange: BankNetworkEnum.RTGS
  }),
  isEligible: (transaction: ITransactionModel): boolean =>
    transaction.paymentServiceCode === RTGS ||
    transaction.paymentServiceCode === BRANCH_RTGS ||
    transaction.paymentServiceCode === RTGS_FOR_BUSINESS ||
    transaction.paymentServiceCode === RDN_WITHDRAW_RTGS ||
    transaction.paymentServiceCode === RTGS_SHARIA
};

const rtgsTemplates: IRtgsRequestTemplate[] = [
  rdnKseiRequestTemplate,
  rtgsRequestTemplate
];

export const mapToRtgsTransaction = (
  transaction: ITransactionModel
): IRtgsTransaction => {
  const templateRequest = rtgsTemplates.find(template =>
    template.isEligible(transaction)
  );
  if (!templateRequest) {
    const detail = `Transaction submission template request not found with paymentServiceType: ${transaction.paymentServiceType}, paymentServiceCode: ${transaction.paymentServiceCode}`;
    logger.error(`mapToRtgsTransaction: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.JAGO,
      ERROR_CODE.REQUEST_TEMPLATE_NOT_FOUND
    );
  }
  return templateRequest.getPayload(transaction);
};
