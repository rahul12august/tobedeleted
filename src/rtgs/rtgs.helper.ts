import { ITransactionModel } from '../transaction/transaction.model';
import logger from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

export const validateMandatoryFields = (
  transaction: ITransactionModel
): void => {
  let isValid = true;
  let details = [];
  const description = `should be mandatory for paymentServiceType: ${transaction.paymentServiceType}, paymentServiceCode: ${transaction.paymentServiceCode} transaction.`;
  if (!transaction.beneficiaryAccountName) {
    let message = `BeneficiaryAccountName ${description}`;
    logger.error(`validateMandatoryFields: ${message}`);
    details.push(message);
    isValid = false;
  }
  if (!transaction.beneficiaryAccountNo) {
    let message = `BeneficiaryAccountNo ${description}`;
    logger.error(`validateMandatoryFields: ${message}`);
    details.push(message);
    isValid = false;
  }
  if (!transaction.beneficiaryBankCode) {
    let message = `BeneficiaryBankCode ${description}`;
    logger.error(`validateMandatoryFields: ${message}`);
    details.push(message);
    isValid = false;
  }
  if (!transaction.sourceBankCode) {
    let message = `SourceBankCode ${description}`;
    logger.error(`validateMandatoryFields: ${message}`);
    details.push(message);
    isValid = false;
  }
  if (!transaction.sourceAccountNo) {
    let message = `SourceAccountNo ${description}`;
    logger.error(`validateMandatoryFields: ${message}`);
    details.push(message);
    isValid = false;
  }
  if (!transaction.sourceAccountName) {
    let message = `SourceAccountName ${description}`;
    logger.error(`validateMandatoryFields: ${message}`);
    details.push(message);
    isValid = false;
  }
  if (!isValid) {
    throw new TransferAppError(
      details.join('\n'),
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_MANDATORY_FIELDS
    );
  }
  return;
};
