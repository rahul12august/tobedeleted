import {
  BankChannelEnum,
  BankNetworkEnum,
  PaymentServiceTypeEnum
} from '@dk/module-common';
import { getTransactionDetail } from '../../transaction/__mocks__/transaction.data';
import { mapToRtgsTransaction } from '../rtgsRequest.template';
import { ERROR_CODE } from '../../common/errors';
import { formatTransactionDate } from '../../transaction/transaction.util';
import {
  EXCHANGE_RATE,
  KSEI,
  RDN_KSEI,
  RDN_KSEI_TRN,
  RTGS,
  RTGS_TRN,
  BRANCH_RTGS,
  RTGS_FOR_BUSINESS,
  RTGS_SHARIA
} from '../rtgs.constant';
import { BANK_KSEI } from '../../transaction/transaction.constant';

describe('rtgsRequest.template', () => {
  beforeEach(() => {});
  afterEach(() => {
    jest.resetAllMocks();
  });
  // given
  const transactionModel = {
    ...getTransactionDetail(),
    beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
    requireThirdPartyOutgoingId: true,
    debitTransactionCode: 'TFD60',
    thirdPartyOutgoingId: '21042T000041',
    transactionAmount: 150000000,
    paymentServiceCode: RTGS
  };

  describe('mapToRtgsTransaction', () => {
    it('should return mapToRtgsTransaction for RTGS', () => {
      // when
      const transactionEntity = mapToRtgsTransaction(transactionModel);
      expect(transactionEntity).toBeDefined();
      expect(transactionEntity).toEqual({
        trn: RTGS_TRN,
        externalId:
          (transactionModel.thirdPartyOutgoingId &&
            transactionModel.thirdPartyOutgoingId.padStart(16, '0')) ||
          '',
        transactionDate: formatTransactionDate(transactionModel.createdAt),
        transactionAmount: transactionModel.transactionAmount,
        beneficiaryBankCode: transactionModel.beneficiaryRemittanceCode || '',
        beneficiaryAccountName: transactionModel.beneficiaryAccountName || '',
        beneficiaryAccountNo: transactionModel.beneficiaryAccountNo || '',
        sourceBankCode: transactionModel.sourceRemittanceCode || '',
        sourceAccountNo: transactionModel.sourceAccountNo || '',
        sourceAccountName: transactionModel.sourceAccountName || '',
        notes: transactionModel.note || '',
        currency: transactionModel.sourceTransactionCurrency,
        interchange: BankNetworkEnum.RTGS
      });
    });

    it('should return mapToRtgsTransaction for RTGS_FOR_BUSINESS', () => {
      // when
      const rtgsForBusinessTransactionModel = {
        ...transactionModel,
        paymentServiceCode: RTGS_FOR_BUSINESS
      };
      const transactionEntity = mapToRtgsTransaction(
        rtgsForBusinessTransactionModel
      );
      expect(transactionEntity).toBeDefined();
      expect(transactionEntity).toEqual({
        trn: RTGS_TRN,
        externalId:
          (transactionModel.thirdPartyOutgoingId &&
            transactionModel.thirdPartyOutgoingId.padStart(16, '0')) ||
          '',
        transactionDate: formatTransactionDate(transactionModel.createdAt),
        transactionAmount: transactionModel.transactionAmount,
        beneficiaryBankCode: transactionModel.beneficiaryRemittanceCode || '',
        beneficiaryAccountName: transactionModel.beneficiaryAccountName || '',
        beneficiaryAccountNo: transactionModel.beneficiaryAccountNo || '',
        sourceBankCode: transactionModel.sourceRemittanceCode || '',
        sourceAccountNo: transactionModel.sourceAccountNo || '',
        sourceAccountName: transactionModel.sourceAccountName || '',
        notes: transactionModel.note || '',
        currency: transactionModel.sourceTransactionCurrency,
        interchange: BankNetworkEnum.RTGS
      });
    });

    it('should return mapToRtgsTransaction for BRANCH_RTGS', () => {
      // when
      const branchRtgsTransactionModel = {
        ...transactionModel,
        trn: RTGS_TRN,
        exchangeRate: EXCHANGE_RATE,
        paymentServiceCode: BRANCH_RTGS,
        paymentServiceType: PaymentServiceTypeEnum.BRANCH_TRANSFER
      };

      const transactionEntity = mapToRtgsTransaction(
        branchRtgsTransactionModel
      );
      expect(transactionEntity).toBeDefined();
      expect(transactionEntity).toEqual({
        trn: RTGS_TRN,
        externalId:
          (transactionModel.thirdPartyOutgoingId &&
            transactionModel.thirdPartyOutgoingId.padStart(16, '0')) ||
          '',
        transactionDate: formatTransactionDate(transactionModel.createdAt),
        transactionAmount: transactionModel.transactionAmount,
        beneficiaryBankCode: transactionModel.beneficiaryRemittanceCode || '',
        beneficiaryAccountName: transactionModel.beneficiaryAccountName || '',
        beneficiaryAccountNo: transactionModel.beneficiaryAccountNo || '',
        sourceBankCode: transactionModel.sourceRemittanceCode || '',
        sourceAccountNo: transactionModel.sourceAccountNo || '',
        sourceAccountName: transactionModel.sourceAccountName || '',
        notes: transactionModel.note || '',
        currency: transactionModel.sourceTransactionCurrency,
        interchange: BankNetworkEnum.RTGS
      });
    });

    it('should return mapToRtgsTransaction for RDN_KSEI', () => {
      const rdnKseiTransactionModel = {
        ...transactionModel,
        trn: RDN_KSEI_TRN,
        exchangeRate: EXCHANGE_RATE,
        paymentServiceCode: RDN_KSEI,
        beneficiaryBankCode: BANK_KSEI
      };

      // when
      const transactionEntity = mapToRtgsTransaction(rdnKseiTransactionModel);
      expect(transactionEntity).toBeDefined();
      expect(transactionEntity).toEqual({
        trn: RDN_KSEI_TRN,
        externalId: KSEI + transactionModel.thirdPartyOutgoingId || '',
        transactionDate: formatTransactionDate(transactionModel.createdAt),
        transactionAmount: transactionModel.transactionAmount,
        beneficiaryBankCode: transactionModel.beneficiaryRemittanceCode || '',
        beneficiaryAccountName: transactionModel.beneficiaryAccountName || '',
        beneficiaryAccountNo: transactionModel.beneficiaryAccountNo || '',
        sourceBankCode: transactionModel.sourceRemittanceCode || '',
        sourceAccountNo: transactionModel.sourceAccountNo || '',
        sourceAccountName: transactionModel.sourceAccountName || '',
        notes: transactionModel.note || '',
        currency: transactionModel.sourceTransactionCurrency,
        interchange: BankNetworkEnum.RTGS,
        exchangeRate: EXCHANGE_RATE,
        addInfo1: ''
      });
    });

    it('should return mapToRtgsTransaction for RTGS_SHARIA', () => {
      const rtgsShariaTransactionModel = {
        ...transactionModel,
        paymentServiceCode: RTGS_SHARIA
      };

      // when
      const transactionEntity = mapToRtgsTransaction(
        rtgsShariaTransactionModel
      );
      expect(transactionEntity).toBeDefined();
      expect(transactionEntity).toEqual({
        trn: RTGS_TRN,
        externalId:
          (transactionModel.thirdPartyOutgoingId &&
            transactionModel.thirdPartyOutgoingId.padStart(16, '0')) ||
          '',
        transactionDate: formatTransactionDate(transactionModel.createdAt),
        transactionAmount: transactionModel.transactionAmount,
        beneficiaryBankCode: transactionModel.beneficiaryRemittanceCode || '',
        beneficiaryAccountName: transactionModel.beneficiaryAccountName || '',
        beneficiaryAccountNo: transactionModel.beneficiaryAccountNo || '',
        sourceBankCode: transactionModel.sourceRemittanceCode || '',
        sourceAccountNo: transactionModel.sourceAccountNo || '',
        sourceAccountName: transactionModel.sourceAccountName || '',
        notes: transactionModel.note || '',
        currency: transactionModel.sourceTransactionCurrency,
        interchange: BankNetworkEnum.RTGS
      });
    });

    it('should throws error when template request not found', () => {
      const rdnKseiTransactionModel = {
        ...transactionModel,
        trn: RDN_KSEI_TRN,
        exchangeRate: EXCHANGE_RATE,
        paymentServiceCode: 'RANDOM'
      };

      // when
      let error;
      try {
        const transactionEntity = mapToRtgsTransaction(rdnKseiTransactionModel);
      } catch (e) {
        error = e;
      }

      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(ERROR_CODE.REQUEST_TEMPLATE_NOT_FOUND);
    });

    it('should throws error when template payload not found for non KSEI Bank', () => {
      const rdnKseiTransactionModel = {
        ...transactionModel,
        trn: RDN_KSEI_TRN,
        exchangeRate: EXCHANGE_RATE,
        paymentServiceCode: RDN_KSEI,
        beneficiaryBankCode: 'BC005'
      };

      // when
      let error;
      try {
        const transactionEntity = mapToRtgsTransaction(rdnKseiTransactionModel);
      } catch (e) {
        error = e;
      }

      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_BANK_CODE);
    });

    it('should return mapToRtgsTransaction with default values for RTGS ', () => {
      const rdnKseiTransactionModel = {
        ...transactionModel,
        externalId: '',
        transactionDate: formatTransactionDate(new Date()),
        beneficiaryRemittanceCode: '',
        beneficiaryAccountNo: '',
        beneficiaryAccountName: '',
        sourceRemittanceCode: '',
        sourceAccountNo: '',
        sourceAccountName: '',
        notes: ''
      };

      // when
      const transactionEntity = mapToRtgsTransaction(rdnKseiTransactionModel);
      expect(transactionEntity).toBeDefined();
      expect(transactionEntity).toEqual({
        trn: RTGS_TRN,
        externalId:
          (transactionModel.thirdPartyOutgoingId &&
            transactionModel.thirdPartyOutgoingId.padStart(16, '0')) ||
          '',
        transactionDate: formatTransactionDate(transactionModel.createdAt),
        transactionAmount: transactionModel.transactionAmount,
        beneficiaryBankCode: '',
        beneficiaryAccountName: '',
        beneficiaryAccountNo: '',
        sourceBankCode: '',
        sourceAccountNo: '',
        sourceAccountName: '',
        notes: '',
        currency: transactionModel.sourceTransactionCurrency,
        interchange: BankNetworkEnum.RTGS
      });
    });

    it('should return mapToRtgsTransaction with default values for RDN_KSEI', () => {
      const rdnKseiTransactionModel = {
        ...transactionModel,
        trn: RDN_KSEI_TRN,
        exchangeRate: EXCHANGE_RATE,
        paymentServiceCode: RDN_KSEI,
        beneficiaryBankCode: BANK_KSEI,
        transactionDate: formatTransactionDate(new Date()),
        beneficiaryRemittanceCode: '',
        beneficiaryAccountNo: '',
        beneficiaryAccountName: '',
        sourceRemittanceCode: '',
        sourceAccountNo: '',
        sourceAccountName: '',
        notes: ''
      };

      // when
      const transactionEntity = mapToRtgsTransaction(rdnKseiTransactionModel);
      expect(transactionEntity).toBeDefined();
      expect(transactionEntity).toEqual({
        trn: RDN_KSEI_TRN,
        externalId: KSEI + transactionModel.thirdPartyOutgoingId || '',
        transactionDate: formatTransactionDate(transactionModel.createdAt),
        transactionAmount: transactionModel.transactionAmount,
        beneficiaryBankCode: '',
        beneficiaryAccountName: '',
        beneficiaryAccountNo: '',
        sourceBankCode: '',
        sourceAccountNo: '',
        sourceAccountName: '',
        notes: '',
        currency: transactionModel.sourceTransactionCurrency,
        interchange: BankNetworkEnum.RTGS,
        exchangeRate: EXCHANGE_RATE,
        addInfo1: ''
      });
    });
  });
});
