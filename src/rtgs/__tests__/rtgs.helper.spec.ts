import { BankChannelEnum } from '@dk/module-common';
import { getTransactionDetail } from '../../transaction/__mocks__/transaction.data';
import { validateMandatoryFields } from '../rtgs.helper';
import { ERROR_CODE } from '../../common/errors';
import { ITransactionModel } from '../../transaction/transaction.model';
import { TransferAppError } from '../../errors/AppError';
import { RTGS } from '../rtgs.constant';

describe('rtgs.helper', () => {
  beforeEach(() => {});
  afterEach(() => {
    jest.resetAllMocks();
  });
  // given
  const transactionModel = {
    ...getTransactionDetail(),
    beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
    requireThirdPartyOutgoingId: true,
    debitTransactionCode: 'TFD60',
    thirdPartyOutgoingId: '21042T000041',
    transactionAmount: 150000000,
    paymentServiceCode: RTGS
  };

  describe('validateMandatoryFields', () => {
    it('should return error when beneficiaryAccountNo is undefined', () => {
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        beneficiaryAccountNo: undefined
      };
      // when
      let error;
      try {
        validateMandatoryFields(originalTransaction);
      } catch (err) {
        // Then
        error = err;
      }
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
    });

    it('should return error when beneficiaryAccountName is undefined', () => {
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        beneficiaryAccountName: undefined
      };
      // when
      let error;
      try {
        validateMandatoryFields(originalTransaction);
      } catch (err) {
        // Then
        error = err;
      }
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
    });

    it('should return error when beneficiaryBankCode is undefined', () => {
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        beneficiaryBankCode: undefined
      };
      // when
      let error;
      try {
        validateMandatoryFields(originalTransaction);
      } catch (err) {
        // Then
        error = err;
      }
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
    });

    it('should return error when sourceBankCode is undefined', () => {
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        sourceBankCode: undefined
      };
      // when
      let error;
      try {
        validateMandatoryFields(originalTransaction);
      } catch (err) {
        // Then
        error = err;
      }
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
    });

    it('should return error when sourceAccountNo is undefined', () => {
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        sourceAccountNo: undefined
      };
      // when
      let error;
      try {
        validateMandatoryFields(originalTransaction);
      } catch (err) {
        // Then
        error = err;
      }
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
    });

    it('should return error when sourceAccountName is undefined', () => {
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        sourceAccountName: undefined
      };
      // when
      let error;
      try {
        validateMandatoryFields(originalTransaction);
      } catch (err) {
        // Then
        error = err;
      }
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
    });

    it('should return void when mandatory validations passed', () => {
      // when
      let error;
      try {
        validateMandatoryFields(transactionModel);
      } catch (err) {
        // Then
        error = err;
      }
      expect(error).toBeUndefined();
    });
  });
});
