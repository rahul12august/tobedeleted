import syariahRtgsRepository from '../syariahRtgs.repository';
import { BankChannelEnum, Rail, Util } from '@dk/module-common';
import { getTransactionDetail } from '../../transaction/__mocks__/transaction.data';
import { mapToRtgsTransaction } from '../rtgsRequest.template';
import { getMockedTransaction } from '../__mocks__/rtgsTransaction.data';
import { IRtgsTransaction } from '../rtgs.type';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import transactionRepository from '../../transaction/transaction.repository';
import transactionProducer from '../../transaction/transaction.producer';
import { NotificationCode } from '../../transaction/transaction.producer.enum';
import { httpClientTimeoutErrorCode } from '../../common/constant';
import { validateMandatoryFields } from '../rtgs.helper';
import syariahRtgsSubmissionTemplateService from '../syariahRtgsSubmissionTemplate.service';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

jest.mock('../syariahRtgs.repository');
jest.mock('../rtgsRequest.template');
jest.mock('../rtgs.helper');
jest.mock('../../transaction/transaction.repository');
jest.mock('../../transaction/transaction.producer');
describe('syariahRtgsSubmissionTemplate', () => {
  class NetworkError extends Error {
    status: number;
    code?: string;
    constructor(status = 501, code?: string) {
      super();
      this.status = status;
      this.code = code;
    }
  }

  let mockedRetry = jest
    .spyOn(Util, 'retry')
    .mockImplementation(async (_, fn, ...args) => await fn(...args));

  // given
  const transactionModel = {
    ...getTransactionDetail(),
    beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
    requireThirdPartyOutgoingId: true,
    debitTransactionCode: 'TFD60',
    thirdPartyOutgoingId: '21042T000041',
    transactionAmount: 150000000,
    paymentServiceCode: 'RTGS_SHARIA'
  };

  describe('isEligible check', () => {
    it(`should return true when paymentServiceCode is RTGS_SHARIA`, () => {
      // when
      const eligible = syariahRtgsSubmissionTemplateService.isEligible({
        ...transactionModel
      });

      // then
      expect(eligible).toBeTruthy();
    });

    it(`should return false when beneficiaryBankCodeChannel is not ${BankChannelEnum.EXTERNAL}`, () => {
      // when
      const eligible = syariahRtgsSubmissionTemplateService.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL
      });

      // then
      expect(eligible).not.toBeTruthy();
    });

    it('should return false when beneficiaryAccountNo is undefined', async () => {
      // when
      const eligible = syariahRtgsSubmissionTemplateService.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        beneficiaryAccountNo: undefined
      });

      // then
      expect(eligible).not.toBeTruthy();
    });

    it('should return false when paymentServiceCode is not RTGS_SHARIA', async () => {
      // when
      const eligible = syariahRtgsSubmissionTemplateService.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        beneficiaryAccountNo: '123456',
        paymentServiceCode: 'RTOL'
      });

      // then
      expect(eligible).not.toBeTruthy();
    });
  });

  describe('submitTransaction', () => {
    const mockedRTGSModel: IRtgsTransaction = getMockedTransaction();
    const mockedRTGSResponse = {};
    beforeEach(() => {
      (mapToRtgsTransaction as jest.Mock).mockReturnValueOnce(mockedRTGSModel);
      (syariahRtgsRepository.submitTransaction as jest.Mock).mockResolvedValue(
        mockedRTGSResponse
      );
      (validateMandatoryFields as jest.Mock).mockReturnValueOnce(null);
    });
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should submit transaction to rtgs with correct payload', async () => {
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});
      const result = await syariahRtgsSubmissionTemplateService.submitTransaction(
        transactionModel
      );
      expect(result).toBeDefined();
      expect(syariahRtgsRepository.submitTransaction).toHaveBeenCalledWith(
        mockedRTGSModel
      );
      expect(transactionRepository.update).toHaveBeenCalledWith(
        transactionModel.id,
        {
          thirdPartyOutgoingId: transactionModel.thirdPartyOutgoingId
        }
      );
    });

    it('should decline the transaction when thirdparty outgoing id is missing', async () => {
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});
      const model = {
        ...transactionModel,
        thirdPartyOutgoingId: undefined
      };
      const error = await syariahRtgsSubmissionTemplateService
        .submitTransaction(model)
        .catch(err => err);

      expect(error).toBeDefined();
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.actor).toBe(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toBe(
        'Third party outgoing id is missing while doing paymentServiceType: TRANSFER, paymentServiceCode: RTGS_SHARIA transaction.'
      );
      expect(error.errorCode).toBe(ERROR_CODE.INVALID_EXTERNALID);
    });

    it('should retry first times when RTGS repository throw network error before giving response', async () => {
      const networkError = new NetworkError();
      const expectedNetworkError = {
        ...networkError,
        code: 'ECONNABORTED'
      };
      mockedRetry.mockRestore();
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});
      (transactionRepository.getByKey as jest.Mock).mockReturnValueOnce({
        ...transactionModel,
        thirdPartyOutgoingId: transactionModel.thirdPartyOutgoingId.padStart(
          16,
          '0'
        )
      });
      (syariahRtgsRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        expectedNetworkError
      );

      const result = await syariahRtgsSubmissionTemplateService.submitTransaction(
        transactionModel
      );
      expect(result).toBeDefined();
      expect(syariahRtgsRepository.submitTransaction).toHaveBeenCalledTimes(1);
    });

    it('should decline the transaction when rtgs returned bad request error', async () => {
      (syariahRtgsRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.ERROR_FROM_RTGS_SHARIA
        )
      );
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});

      const error = await syariahRtgsSubmissionTemplateService
        .submitTransaction(transactionModel)
        .catch(err => err);
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.detail).toBe('Test error!');
      expect(error.actor).toBe(TransferFailureReasonActor.UNKNOWN);
      expect(error.errorCode).toBe(ERROR_CODE.ERROR_FROM_RTGS_SHARIA);
      expect(syariahRtgsRepository.submitTransaction).toHaveBeenCalledTimes(1);
    });

    it('should update the transaction to submitted when rtgs returned timeout error 408', async () => {
      (syariahRtgsRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        {
          message:
            'HttpClient Error: Error: Request failed with status code 408',
          code: httpClientTimeoutErrorCode
        }
      );
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});
      (transactionProducer.sendPendingBlockedAmountNotification as jest.Mock).mockResolvedValueOnce(
        null
      );

      await syariahRtgsSubmissionTemplateService
        .submitTransaction(transactionModel)
        .catch(err => err);

      expect(syariahRtgsRepository.submitTransaction).toHaveBeenCalledTimes(1);
      expect(
        transactionProducer.sendPendingBlockedAmountNotification
      ).toHaveBeenCalledWith(
        transactionModel,
        NotificationCode.NOTIF_PENIDNG_SKNRTGS
      );
    });
  });

  describe('Rail check', () => {
    it(`should have rail property defined`, () => {
      // when
      const rail = syariahRtgsSubmissionTemplateService.getRail?.();

      // then
      expect(rail).toEqual(Rail.SYARIAH_RTGS);
    });
  });
});
