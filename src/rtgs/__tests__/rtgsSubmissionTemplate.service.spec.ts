import rtgsRepository from '../rtgs.repository';
import { BankChannelEnum, Rail, Util } from '@dk/module-common';
import { getTransactionDetail } from '../../transaction/__mocks__/transaction.data';
import rtgsSubmissionTemplateService from '../rtgsSubmissionTemplate.service';
import { mapToRtgsTransaction } from '../rtgsRequest.template';
import { getMockedTransaction } from '../__mocks__/rtgsTransaction.data';
import { IRtgsTransaction } from '../rtgs.type';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import transactionRepository from '../../transaction/transaction.repository';
import transactionProducer from '../../transaction/transaction.producer';
import { NotificationCode } from '../../transaction/transaction.producer.enum';
import { httpClientTimeoutErrorCode } from '../../common/constant';
import { validateMandatoryFields } from '../rtgs.helper';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

jest.mock('../rtgs.repository');
jest.mock('../rtgsRequest.template');
jest.mock('../rtgs.helper');
jest.mock('../../transaction/transaction.repository');
jest.mock('../../transaction/transaction.producer');

describe('rtgsSubmissionTemplate', () => {
  class NetworkError extends Error {
    status: number;
    code?: string;
    constructor(status = 501, code?: string) {
      super();
      this.status = status;
      this.code = code;
    }
  }
  let mockedRetry = jest
    .spyOn(Util, 'retry')
    .mockImplementation(async (_, fn, ...args) => await fn(...args));

  // given
  const transactionModel = {
    ...getTransactionDetail(),
    beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
    requireThirdPartyOutgoingId: true,
    debitTransactionCode: 'TFD60',
    thirdPartyOutgoingId: '21042T000041',
    transactionAmount: 150000000,
    paymentServiceCode: 'RTGS'
  };

  describe('isEligible check', () => {
    it(`should return true when paymentServiceCode is RTGS`, () => {
      // when
      const eligible = rtgsSubmissionTemplateService.isEligible(
        transactionModel
      );

      // then
      expect(eligible).toBeTruthy();
    });

    it(`should return true when paymentServiceCode is BRANCH_RTGS`, () => {
      // when
      const eligible = rtgsSubmissionTemplateService.isEligible({
        ...transactionModel,
        paymentServiceCode: 'BRANCH_RTGS'
      });

      // then
      expect(eligible).toBeTruthy();
    });

    it(`should return true when paymentServiceCode is RTGS_FOR_BUSINESS`, () => {
      // when
      const eligible = rtgsSubmissionTemplateService.isEligible({
        ...transactionModel,
        paymentServiceCode: 'RTGS_FOR_BUSINESS'
      });

      // then
      expect(eligible).toBeTruthy();
    });

    it(`should return true when paymentServiceCode is RDN_KSEI`, () => {
      // when
      const eligible = rtgsSubmissionTemplateService.isEligible({
        ...transactionModel,
        paymentServiceCode: 'RDN_KSEI'
      });

      // then
      expect(eligible).toBeTruthy();
    });

    it(`should return true when paymentServiceCode is RDN_WITHDRAW_RTGS`, () => {
      // when
      const eligible = rtgsSubmissionTemplateService.isEligible({
        ...transactionModel,
        paymentServiceCode: 'RDN_WITHDRAW_RTGS'
      });

      // then
      expect(eligible).toBeTruthy();
    });

    it(`should return false when beneficiaryBankCodeChannel is not ${BankChannelEnum.EXTERNAL}`, () => {
      // when
      const eligible = rtgsSubmissionTemplateService.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL
      });

      // then
      expect(eligible).not.toBeTruthy();
    });

    it('should return false when beneficiaryAccountNo is undefined', async () => {
      // when
      const eligible = rtgsSubmissionTemplateService.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        beneficiaryAccountNo: undefined
      });

      // then
      expect(eligible).not.toBeTruthy();
    });

    it('should return false when paymentServiceCode is not RTGS, BRANCH_RTGS, RDN_KSEI, and RTGS_FOR_BUSINESS', async () => {
      // when
      const eligible = rtgsSubmissionTemplateService.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        beneficiaryAccountNo: '123456',
        paymentServiceCode: 'RTGS_SHARIA'
      });

      // then
      expect(eligible).not.toBeTruthy();
    });
  });

  describe('submitTransaction', () => {
    const mockedRTGSModel: IRtgsTransaction = getMockedTransaction();
    const mockedRTGSResponse = {};
    beforeEach(() => {
      (mapToRtgsTransaction as jest.Mock).mockReturnValueOnce(mockedRTGSModel);
      (rtgsRepository.submitTransaction as jest.Mock).mockResolvedValue(
        mockedRTGSResponse
      );
      (validateMandatoryFields as jest.Mock).mockReturnValueOnce(null);
    });
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should submit transaction to rtgs with correct payload', async () => {
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});
      const result = await rtgsSubmissionTemplateService.submitTransaction(
        transactionModel
      );
      expect(result).toBeDefined();
      expect(rtgsRepository.submitTransaction).toHaveBeenCalledWith(
        mockedRTGSModel
      );
      expect(transactionRepository.update).toHaveBeenCalledWith(
        transactionModel.id,
        {
          thirdPartyOutgoingId: transactionModel.thirdPartyOutgoingId
        }
      );
    });

    it('should Decline the transaction when thirdparty outgoing id is missing', async () => {
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});
      const model = {
        ...transactionModel,
        thirdPartyOutgoingId: undefined
      };
      let error;
      try {
        await rtgsSubmissionTemplateService.submitTransaction(model);
      } catch (err) {
        error = err;
      }
      expect(error).toBeDefined();
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toBe(ERROR_CODE.INVALID_EXTERNALID);
    });

    it('should retry first times when RTGS repository throw network error before giving response', async () => {
      const networkError = new NetworkError();
      const expectedNetworkError = {
        ...networkError,
        code: 'ECONNABORTED'
      };
      mockedRetry.mockRestore();
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});
      (transactionRepository.getByKey as jest.Mock).mockReturnValueOnce({
        ...transactionModel,
        thirdPartyOutgoingId: transactionModel.thirdPartyOutgoingId.padStart(
          16,
          '0'
        )
      });
      (rtgsRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        expectedNetworkError
      );

      const result = await rtgsSubmissionTemplateService.submitTransaction(
        transactionModel
      );
      expect(result).toBeDefined();
      expect(rtgsRepository.submitTransaction).toHaveBeenCalledTimes(1);
    });

    it('should decline the transaction when rtgs returned bad request error', async () => {
      (rtgsRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.ERROR_FROM_RTGS
        )
      );
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});

      const error = await rtgsSubmissionTemplateService
        .submitTransaction(transactionModel)
        .catch(err => err);
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toBe(ERROR_CODE.ERROR_FROM_RTGS);
      expect(error.actor).toBe(TransferFailureReasonActor.UNKNOWN);
      expect(error.detail).toBe('Test error!');
      expect(rtgsRepository.submitTransaction).toHaveBeenCalledTimes(1);
    });

    it('should update the transaction to submitted when rtgs returned timeout error 408', async () => {
      (rtgsRepository.submitTransaction as jest.Mock).mockRejectedValueOnce({
        message: 'HttpClient Error: Error: Request failed with status code 408',
        code: httpClientTimeoutErrorCode
      });
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});
      (transactionProducer.sendPendingBlockedAmountNotification as jest.Mock).mockResolvedValueOnce(
        null
      );

      await rtgsSubmissionTemplateService
        .submitTransaction(transactionModel)
        .catch(err => err);

      expect(rtgsRepository.submitTransaction).toHaveBeenCalledTimes(1);
      expect(
        transactionProducer.sendPendingBlockedAmountNotification
      ).toHaveBeenCalledWith(
        transactionModel,
        NotificationCode.NOTIF_PENIDNG_SKNRTGS
      );
    });
  });

  describe('Rail check', () => {
    it(`should have rail property defined`, () => {
      // when
      const rail = rtgsSubmissionTemplateService.getRail?.();

      // then
      expect(rail).toEqual(Rail.RTGS);
    });
  });
});
