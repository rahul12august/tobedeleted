import httpClient from '../httpClient';
import rtgsRepository from '../rtgs.repository';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import { getMockedTransaction } from '../__mocks__/rtgsTransaction.data';
import { config } from '../../config';
import { httpClientTimeoutErrorCode } from '../../common/constant';
const { transactionUrl } = config.get('rtgs');

jest.mock('../httpClient');

describe('rtgs.repository', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('submitTransaction', () => {
    const rtgsTransaction = getMockedTransaction();
    it('should return rtgs transaction success', async () => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          payouts: [rtgsTransaction]
        },
        status: 200
      });

      const expected = await rtgsRepository.submitTransaction(rtgsTransaction);

      //const options = { };
      expect(httpClient.post).toBeCalledWith(transactionUrl, rtgsTransaction);
      expect(expected).toEqual(undefined);
    });

    it(`should throw error ${ERROR_CODE.ERROR_FROM_RTGS} when submit error`, async () => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: null
      });

      const expected = await rtgsRepository
        .submitTransaction(rtgsTransaction)
        .catch(err => err);

      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toBe(ERROR_CODE.ERROR_FROM_RTGS);
    });

    it(`should throw error when rtgs api throws 408`, async () => {
      (httpClient.post as jest.Mock).mockRejectedValueOnce({
        message: 'HttpClient Error: Error: Request failed with status code 408',
        code: httpClientTimeoutErrorCode
      });

      const expected = await rtgsRepository
        .submitTransaction(rtgsTransaction)
        .catch(err => err);

      expect(expected).not.toBeInstanceOf(TransferAppError);
      expect(expected.message).toBe(
        'HttpClient Error: Error: Request failed with status code 408'
      );
      expect(expected.code).toBe(httpClientTimeoutErrorCode);
    });
  });
});
