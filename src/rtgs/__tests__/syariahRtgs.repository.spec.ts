import syariahRtgsHttpClient from '../syariahRtgsHttpClient';
import syariahRtgsRepository from '../syariahRtgs.repository';
import { ERROR_CODE } from '../../common/errors';
import { getMockedTransaction } from '../__mocks__/rtgsTransaction.data';
import { config } from '../../config';
import { httpClientTimeoutErrorCode } from '../../common/constant';
import { TransferAppError } from '../../errors/AppError';
const { transactionUrl } = config.get('rtgs');

jest.mock('../syariahRtgsHttpClient');

describe('syariahRtgs.repository', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('submitTransaction', () => {
    const rtgsTransaction = getMockedTransaction();
    it('should return rtgs syariah transaction success', async () => {
      (syariahRtgsHttpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          payouts: [rtgsTransaction]
        },
        status: 200
      });

      const expected = await syariahRtgsRepository.submitTransaction(
        rtgsTransaction
      );

      expect(syariahRtgsHttpClient.post).toBeCalledWith(
        transactionUrl,
        rtgsTransaction
      );
      expect(expected).toEqual(undefined);
    });

    it(`should throw error ${ERROR_CODE.ERROR_FROM_RTGS} when submit error`, async () => {
      (syariahRtgsHttpClient.post as jest.Mock).mockResolvedValueOnce({
        data: null
      });

      const expected = await syariahRtgsRepository
        .submitTransaction(rtgsTransaction)
        .catch(err => err);

      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toBe(ERROR_CODE.ERROR_FROM_RTGS_SHARIA);
    });

    it(`should throw error when rtgs api throws 408`, async () => {
      (syariahRtgsHttpClient.post as jest.Mock).mockRejectedValueOnce({
        message: 'HttpClient Error: Error: Request failed with status code 408',
        code: httpClientTimeoutErrorCode
      });

      const expected = await syariahRtgsRepository
        .submitTransaction(rtgsTransaction)
        .catch(err => err);

      expect(expected).not.toBeInstanceOf(TransferAppError);
      expect(expected.message).toBe(
        'HttpClient Error: Error: Request failed with status code 408'
      );
      expect(expected.code).toBe(httpClientTimeoutErrorCode);
    });
  });
});
