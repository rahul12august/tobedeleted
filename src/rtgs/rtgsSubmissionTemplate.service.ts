import { ITransactionModel } from '../transaction/transaction.model';
import logger, { wrapLogs } from '../logger';
import { ITransactionSubmissionTemplate } from '../transaction/transaction.type';
import { BankChannelEnum, Rail } from '@dk/module-common';
import { mapToRtgsTransaction } from './rtgsRequest.template';
import transactionUtility from '../transaction/transaction.util';
import transactionRepository from '../transaction/transaction.repository';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import rtgsRepository from './rtgs.repository';
import { isEmpty } from 'lodash';
import transactionProducer from '../transaction/transaction.producer';
import { NotificationCode } from '../transaction/transaction.producer.enum';
import { httpClientTimeoutErrorCode } from '../common/constant';
import { validateMandatoryFields } from './rtgs.helper';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const submitTransaction = async (
  transactionModel: ITransactionModel
): Promise<ITransactionModel> => {
  try {
    validateMandatoryFields(transactionModel);
    if (!transactionModel.thirdPartyOutgoingId) {
      const detail = `Third party outgoing id is missing while doing paymentServiceType: ${transactionModel.paymentServiceType}, 
      paymentServiceCode: ${transactionModel.paymentServiceCode} transaction.`;
      logger.error(detail);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.SUBMITTER,
        ERROR_CODE.INVALID_EXTERNALID
      );
    }
    const rtgsTransactionPayload = mapToRtgsTransaction(transactionModel);
    transactionModel.thirdPartyOutgoingId = rtgsTransactionPayload.externalId;
    await transactionUtility.submitTransactionWithRetry(
      rtgsRepository.submitTransaction,
      rtgsTransactionPayload
    );
    return transactionModel;
  } catch (error) {
    logger.error(
      `failed submission to paymentServiceType: ${
        transactionModel.paymentServiceType
      }, paymentServiceCode: ${
        transactionModel.paymentServiceCode
      } error: ${JSON.stringify(error)}`
    );
    if (error.code && error.code === httpClientTimeoutErrorCode) {
      logger.info(
        `Sending pending paymentServiceType: ${transactionModel.paymentServiceType}, 
        paymentServiceCode: ${transactionModel.paymentServiceCode} blocked amount notification for : ${transactionModel.id}`
      );
      transactionProducer.sendPendingBlockedAmountNotification(
        transactionModel,
        NotificationCode.NOTIF_PENIDNG_SKNRTGS
      );
      return transactionModel;
    }
    throw error;
  } finally {
    await transactionRepository.update(transactionModel.id, {
      thirdPartyOutgoingId: transactionModel.thirdPartyOutgoingId
    });
  }
};
const isEligible = (model: ITransactionModel): boolean => {
  if (model.paymentServiceCode) {
    return (
      [
        'RTGS',
        'BRANCH_RTGS',
        'RDN_KSEI',
        'RTGS_FOR_BUSINESS',
        'RDN_WITHDRAW_RTGS'
      ].includes(model.paymentServiceCode) &&
      BankChannelEnum.EXTERNAL === model.beneficiaryBankCodeChannel &&
      !isEmpty(model.beneficiaryAccountNo)
    );
  }
  return false;
};

const shouldSendNotification = () => true;

const getRail = () => Rail.RTGS;

const rtgsSubmissionTemplate: ITransactionSubmissionTemplate = {
  submitTransaction,
  isEligible,
  getRail,
  shouldSendNotification
};

export default wrapLogs(rtgsSubmissionTemplate);
