import syariahRtgsHttpClient from './syariahRtgsHttpClient';
import logger, { wrapLogs } from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import uuidv4 from 'uuid/v4';
import { updateRequestIdInRedis } from '../common/contextHandler';
import { IRtgsTransaction } from './rtgs.type';
import { config } from '../config';
import { httpClientTimeoutErrorCode } from '../common/constant';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const { transactionUrl } = config.get('rtgs');

const submitTransaction = async (
  transaction: IRtgsTransaction
): Promise<void> => {
  try {
    const externalKey = transaction.externalId || uuidv4();
    updateRequestIdInRedis(externalKey);
    logger.info(
      `Submitting RTGS Syariah transaction external id : ${transaction.externalId}, beneficiaryBankCode: ${transaction.beneficiaryBankCode}, 
         beneficiaryAccountNo: ${transaction.beneficiaryAccountNo},
         sourceAccountNo: ${transaction.sourceAccountNo}`
    );
    const result = await syariahRtgsHttpClient.post(
      transactionUrl,
      transaction
    );
    if (!result.data && result.status !== 200) {
      const detail = `Error from RTGS Syariah while transferring funds with status: ${result.status}!`;
      logger.error(`RTGS Syariah submitTransaction: ${detail}`);
      throw new TransferAppError(
        detail,
        result.status === 400 || result.status === 404
          ? TransferFailureReasonActor.MS_TRANSFER
          : TransferFailureReasonActor.RTGS_SYARIAH,
        ERROR_CODE.ERROR_FROM_RTGS_SHARIA
      );
    }
    return;
  } catch (error) {
    const detail = `Error from RTGS Syariah while transferring funds, ${error.code}, message: ${error.message}`;
    logger.error(`RTGS Syariah submitTransaction catch: ${detail}`);
    // only throw error if it's a timeout error
    if (error.code && error.code === httpClientTimeoutErrorCode) {
      throw error;
    }
    if (error instanceof TransferAppError) throw error;
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.ERROR_FROM_RTGS_SHARIA
    );
  }
};

const syariahRtgsRepository = wrapLogs({
  submitTransaction
});

export default syariahRtgsRepository;
