import { config } from '../config';
import { context } from '../context';
import logger from '../logger';
import { HttpClient } from '@dk/module-httpclient';
import { Http } from '@dk/module-common';
import { retryConfig } from '../common/constant';
const { baseUrlSharia, timeout } = config.get('rtgs');

const syariahRtgsHttpClient: HttpClient = Http.createHttpClientForward({
  baseURL: baseUrlSharia,
  timeout: timeout,
  context,
  logger,
  retryConfig: {
    ...retryConfig,
    networkErrorCodesToRetry: ['ENOENT', 'ECONNREFUSED']
  }
});

export default syariahRtgsHttpClient;
