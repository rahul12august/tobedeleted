import { BankNetworkEnum } from '@dk/module-common';
import { ITransactionModel } from '../transaction/transaction.model';

export interface IRtgsTransaction {
  trn: string;
  externalId: string;
  transactionAmount: number;
  transactionDate: string;
  beneficiaryBankCode: string;
  beneficiaryAccountNo: string;
  beneficiaryAccountName: string;
  sourceAccountNo: string;
  sourceAccountName: string;
  sourceBankCode: string;
  notes: string;
  currency?: string;
  exchangeRate?: string;
  interestRate?: string;
  Period?: string;
  addInfo1?: string;
  addInfo2?: string;
  addInfo3?: string;
  addInfo4?: string;
  interchange: BankNetworkEnum;
}

export interface IRtgsRequestTemplate {
  getPayload(model: ITransactionModel): IRtgsTransaction;
  isEligible(model: ITransactionModel): boolean;
}
