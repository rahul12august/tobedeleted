import { config } from '../config';
import { context } from '../context';
import logger from '../logger';
import { HttpClient } from '@dk/module-httpclient';
import { Http } from '@dk/module-common';
import { retryConfig } from '../common/constant';
const { baseUrl, timeout } = config.get('rtgs');

const httpClient: HttpClient = Http.createHttpClientForward({
  baseURL: baseUrl,
  timeout: timeout,
  context,
  logger,
  retryConfig: {
    ...retryConfig,
    networkErrorCodesToRetry: ['ENOENT', 'ECONNREFUSED']
  }
});

export default httpClient;
