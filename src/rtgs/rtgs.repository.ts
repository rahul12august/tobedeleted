import httpClient from './httpClient';
import logger, { wrapLogs } from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import uuidv4 from 'uuid/v4';
import { updateRequestIdInRedis } from '../common/contextHandler';
import { IRtgsTransaction } from './rtgs.type';
import { config } from '../config';
import { httpClientTimeoutErrorCode } from '../common/constant';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const { transactionUrl } = config.get('rtgs');

const submitTransaction = async (
  transaction: IRtgsTransaction
): Promise<void> => {
  try {
    const externalKey = transaction.externalId || uuidv4();
    updateRequestIdInRedis(externalKey);
    logger.info(
      `Submitting RTGS transaction external id : ${transaction.externalId}, beneficiaryBankCode: ${transaction.beneficiaryBankCode}, 
         beneficiaryAccountNo: ${transaction.beneficiaryAccountNo},
         sourceAccountNo: ${transaction.sourceAccountNo}`
    );
    const result = await httpClient.post(transactionUrl, transaction);
    if (!result.data && result.status !== 200) {
      logger.error(
        'RTGS submitTransaction: Error from RTGS while transferring funds.'
      );
      throw `RTGS Error with status ${result.status}`;
    }
    return;
  } catch (error) {
    // todo this block of code should be reviewed and removed if necessary
    const detail = `Error from RTGS while transferring funds, ${error.code}, message: ${error.message}`;
    logger.error(`submitTransaction catch: ${detail}`);
    // only throw error if it's a timeout error
    if (error.code && error.code === httpClientTimeoutErrorCode) {
      throw error;
    }
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.RTGS,
      ERROR_CODE.ERROR_FROM_RTGS
    );
  }
};

const rtgsRepository = wrapLogs({
  submitTransaction
});

export default rtgsRepository;
