import { formatTransactionDate } from '../../transaction/transaction.util';
import { IRtgsTransaction } from '../rtgs.type';
import { BankNetworkEnum } from '@dk/module-common';

export const getMockedTransaction = (): IRtgsTransaction => ({
  trn: 'IFT00000', // TODO: @dikshit.thakral Hardcoded for now will change it in next updation
  externalId: 'externalId',
  transactionDate: formatTransactionDate(new Date()),
  transactionAmount: 150000000,
  beneficiaryBankCode: 'BC005',
  beneficiaryAccountName: 'Test123',
  beneficiaryAccountNo: '1234567890',
  sourceBankCode: 'BC002',
  sourceAccountNo: '11110000',
  sourceAccountName: 'Test321',
  notes: 'Test Transaction',
  currency: 'IDR',
  interchange: BankNetworkEnum.RTGS
});
