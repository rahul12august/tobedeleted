import { config } from './config';
import { initConsumer } from './common/kafkaConsumer.utils';
import {
  autoTransferTransactionHandler,
  directTransactionHandler
} from './transaction/transaction.consumer';
import { TransactionTopicConstant } from '@dk/module-message';
import { Consumer } from 'kafkajs';

const { groups } = config.get('kafka');

const isLocal = config.get('kafka').isLocal;
const useLocalRedis = config.get('redis').isLocal;

const kafkaTopics = [
  {
    group: groups.directTransactionsBillPayment,
    topic: TransactionTopicConstant.TRANSFER_DIRECT_TRANSACTION_BILL_PAYMENT
  },
  {
    group: groups.directTransactionsIris,
    topic: TransactionTopicConstant.TRANSFER_DIRECT_TRANSACTION_IRIS
  },
  {
    group: groups.directTransactionsSkn,
    topic: TransactionTopicConstant.TRANSFER_DIRECT_TRANSACTION_SKN
  },
  {
    group: groups.directTransactionsRtgs,
    topic: TransactionTopicConstant.TRANSFER_DIRECT_TRANSACTION_RTGS
  },
  {
    group: groups.directTransactionsEn,
    topic: TransactionTopicConstant.TRANSFER_DIRECT_TRANSACTION_EN
  },
  {
    group: groups.directTransactionsBifast,
    topic: TransactionTopicConstant.TRANSFER_DIRECT_TRANSACTION_BIFAST
  }
];

const kafkaBulkTransactionTopics = [
  {
    group: groups.bulkTransaction,
    topic: TransactionTopicConstant.TRANSFER_BULK_TRANSACTION
  }
];

const init = async (
  isBulkTransactionOnly: boolean = false
): Promise<Consumer[]> => {
  let consumers = [];
  let kafkaTopicList: { group: string; topic: string }[] = [];
  if (isBulkTransactionOnly) {
    kafkaTopicList = kafkaBulkTransactionTopics;
  } else {
    kafkaTopicList = kafkaTopics;

    consumers.push(
      initConsumer({
        groupId: groups.transferTransaction,
        topic: TransactionTopicConstant.SUBMIT_TRANSACTION,
        handler: autoTransferTransactionHandler,
        consumerRunConfig: {
          partitionsConsumedConcurrently: config.get(
            'partitionsConsumedConcurrently'
          ).autoTransferTransaction,
          isLocal
        },
        consumerInitConfig: {
          maxBytes: config.get('autoTransferTransactionTopicMaxByte'),
          isLocal,
          useLocalRedis
        }
      })
    );

    consumers.push(
      initConsumer({
        groupId: groups.directTransaction,
        topic: TransactionTopicConstant.TRANSFER_DIRECT_TRANSACTION,
        handler: directTransactionHandler,
        consumerRunConfig: {
          partitionsConsumedConcurrently: config.get(
            'partitionsConsumedConcurrently'
          ).directTransaction,
          isLocal
        },
        consumerInitConfig: {
          maxBytes: config.get('directTransactionTopicMaxByte'),
          isLocal,
          useLocalRedis
        }
      })
    );
  }

  for (const kafkaTopic of kafkaTopicList) {
    consumers.push(
      initConsumer({
        groupId: kafkaTopic.group,
        topic: kafkaTopic.topic,
        handler: directTransactionHandler,
        isDqlNotRequired: true,
        consumerRunConfig: {
          isLocal
        },
        consumerInitConfig: {
          isLocal,
          useLocalRedis
        }
      })
    );
  }

  return Promise.all(consumers);
};

export default {
  init
};
