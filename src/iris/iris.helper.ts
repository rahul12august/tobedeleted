import { ITransactionModel } from '../transaction/transaction.model';
import { IIrisTransactionPayload } from './iris.type';
import { GOPAY_BENEFICIARY_BANK_NAME, GOPAY_NOTES } from './iris.constant';
import { wrapLogs } from '../logger';

export const mapToIrisTransaction = async (
  transaction: ITransactionModel
): Promise<IIrisTransactionPayload> => {
  return {
    /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
    beneficiary_name: transaction.beneficiaryAccountName,
    beneficiary_account: transaction.beneficiaryAccountNo,
    beneficiary_bank: transaction.beneficiaryIrisCode,
    amount: transaction.transactionAmount,
    notes:
      transaction.beneficiaryBankName == GOPAY_BENEFICIARY_BANK_NAME
        ? GOPAY_NOTES + transaction.thirdPartyOutgoingId
        : transaction.thirdPartyOutgoingId
  };
};

export default wrapLogs(mapToIrisTransaction);
