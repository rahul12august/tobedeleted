import { TransferAppError } from '../errors/AppError';
import { ConfirmTransactionRequest } from './iris.type';
import { IConfirmTransactionInput } from '../transaction/transaction.type';
import logger, { wrapLogs } from '../logger';
import {
  ConfirmTransactionStatus,
  TransferFailureReasonActor
} from '../transaction/transaction.enum';
import { ConfirmTransactionStatusEnum } from './iris.enum';
import transactionConfirmationService from '../transaction/transactionConfirmation.service';
import { ERROR_CODE } from '../common/errors';
import transactionBlockingHelper from '../transaction/transactionBlocking.helper';
import { ITransactionModel } from '../transaction/transaction.model';
import transactionExecutionService, {
  defaultExecutionHooks
} from '../transaction/transactionExecution.service';

const confirmTransaction = async (
  input: ConfirmTransactionRequest
): Promise<void> => {
  logger.info(
    `IRIS: Transaction Confirmation with external id: ${input.reference_no} status : ${input.status}`
  );
  let responseCode;
  switch (input.status) {
    case ConfirmTransactionStatusEnum.FAILED:
    case ConfirmTransactionStatusEnum.REJECTED:
      responseCode = ConfirmTransactionStatus.ERROR_EXPECTED;
      break;
    case ConfirmTransactionStatusEnum.COMPLETED:
      responseCode = ConfirmTransactionStatus.SUCCESS;
      break;
    case ConfirmTransactionStatusEnum.APPROVED:
    case ConfirmTransactionStatusEnum.PROCESSED:
    case ConfirmTransactionStatusEnum.QUEUED:
      return;
    default:
      const detail = `Unsupported confirm transaction status: ${input.status}!`;
      logger.error(`confirmTransaction: ${detail}`);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.IRIS,
        ERROR_CODE.UNSUPPORTED_CONFIRM_TRANSACTION_STATUS
      );
  }

  const transactionInput: IConfirmTransactionInput = {
    externalId: input.reference_no,
    amount: Number(input.amount),
    responseCode,
    responseMessage: input.error_message
  };
  await transactionConfirmationService.confirmTransaction(transactionInput);
};

const declineTransaction = async (
  updatedTransactionModel: ITransactionModel,
  error: TransferAppError
): Promise<void> => {
  logger.info(`declineTransaction: transaction with status: ${updatedTransactionModel.status}, 
                thirdpartyOutgoingId: ${updatedTransactionModel.thirdPartyOutgoingId}`);
  await transactionBlockingHelper.cancelBlockingTransaction(
    updatedTransactionModel
  );
  await transactionExecutionService.onFailedTransaction(
    updatedTransactionModel,
    defaultExecutionHooks,
    error
  );
  return;
};

const irisService = wrapLogs({
  confirmTransaction,
  declineTransaction
});

export default irisService;
