import hapi from '@hapi/hapi';
import * as Joi from '@hapi/joi';
import { confirmTransactionRequestValidator } from './iris.validator';

export interface IIrisTransactionPayload {
  beneficiary_name?: string;
  beneficiary_account?: string;
  beneficiary_bank?: string;
  beneficiary_email?: string;
  amount?: number;
  notes?: string;
}
export enum IrisPayoutStatusEnum {
  QUEUED = 'queued',
  PROCESSED = 'processed',
  COMPLETED = 'completed',
  FAILED = 'failed'
}
export interface IIrisPayoutResponse {
  status: IrisPayoutStatusEnum;
  /* eslint-disable @typescript-eslint/camelcase */
  reference_no?: string;
  error?: string;
}

export type ConfirmTransactionRequest = Joi.extractType<
  typeof confirmTransactionRequestValidator
>;

export interface IConfirmTransactionRequest extends hapi.Request {
  payload: ConfirmTransactionRequest;
}
