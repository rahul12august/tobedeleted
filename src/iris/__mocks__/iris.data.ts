import faker from 'faker';
import {
  IIrisPayoutResponse,
  IIrisTransactionPayload,
  IrisPayoutStatusEnum
} from '../iris.type';
import { ConfirmTransactionStatusEnum } from '../iris.enum';
import { ConfirmTransactionRequest } from '../iris.type';

export const getIrisTransactionData = (): IIrisPayoutResponse => ({
  status: IrisPayoutStatusEnum.QUEUED,
  reference_no: faker.random.uuid()
});

export const getIrisTransactionPayload = (): IIrisTransactionPayload => ({
  /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
  beneficiary_name: 'beneficiary_name',
  beneficiary_account: 'beneficiary_account',
  beneficiary_bank: 'beneficiary_bank',
  amount: 1000,
  notes: 'notes'
});

export const mockConfirmTransactionRequest = (): ConfirmTransactionRequest => {
  return {
    reference_no: '0093384848',
    amount: '1000.0',
    status: ConfirmTransactionStatusEnum.COMPLETED,
    error_code: undefined,
    error_message: undefined
  };
};
