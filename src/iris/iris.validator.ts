import Joi from '@hapi/joi';
import constants from './iris.constant';
import { JoiValidator } from '@dk/module-common';
import { ConfirmTransactionStatusEnum } from './iris.enum';

/* eslint-disable @typescript-eslint/camelcase */
export const confirmTransactionRequestValidator = Joi.object({
  reference_no: Joi.string()
    .required()
    .max(constants.MAX_LENGTH_IRIS_REFERENCE_NO)
    .description('Transaction reference no from IRIS'),
  amount: JoiValidator.requiredString().description('Transaction amount'),
  status: JoiValidator.requiredString().valid(
    Object.values(ConfirmTransactionStatusEnum)
  ).description(`Trasnaction status 
      * PROCESSED
      * COMPLETED
      * FAILED
      * APPROVED
      * REJECTED
      * QUEUED`),
  updated_at: Joi.date().description('Transaction updated date'),
  error_code: JoiValidator.optionalString()
    .max(constants.MAX_LENGTH_IRIS_ERROR_CODE)
    .description('Error code from IRIS'),
  error_message: JoiValidator.optionalString()
    .max(constants.MAX_LENGTH_IRIS_ERROR_MESSAGE)
    .description('Error message from IRIS')
}).label('confirmTransactionRequestValidator');
