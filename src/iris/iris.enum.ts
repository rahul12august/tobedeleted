export enum ConfirmTransactionStatusEnum {
  PROCESSED = 'processed',
  COMPLETED = 'completed',
  FAILED = 'failed',
  APPROVED = 'approved',
  REJECTED = 'rejected',
  QUEUED = 'queued'
}
