/**
 * @module irisSubmissionTemplate handle outgoing transaction to switching
 */

import { BankChannelEnum, Rail } from '@dk/module-common';
import { ITransactionModel } from '../transaction/transaction.model';
import logger, { wrapLogs } from '../logger';
import { ITransactionSubmissionTemplate } from '../transaction/transaction.type';
import irisRepository from './iris.repository';
import { mapToIrisTransaction } from './iris.helper';
import transactionUtility from '../transaction/transaction.util';
import transactionRepository from '../transaction/transaction.repository';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../transaction/transaction.enum';
import { IrisPayoutStatusEnum } from './iris.type';
import { updateRequestIdInRedis } from '../common/contextHandler';
import irisService from './iris.service';

const submitTransaction = async (
  transactionModel: ITransactionModel
): Promise<ITransactionModel> => {
  try {
    const irisTransactionPayload = await mapToIrisTransaction(transactionModel);
    const irisTransaction = await transactionUtility.submitTransactionWithRetry(
      irisRepository.submitTransaction,
      irisTransactionPayload,
      transactionModel.externalId
    );
    const status: TransactionStatus =
      irisTransaction.status === IrisPayoutStatusEnum.FAILED
        ? TransactionStatus.DECLINED
        : TransactionStatus.SUBMITTED;

    if (irisTransaction.error) {
      const detail = 'Failed submission to iris!';
      logger.error(detail, irisTransaction.error);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.UNKNOWN,
        ERROR_CODE.ERROR_FROM_IRIS
      );
    }
    if (irisTransaction.reference_no)
      updateRequestIdInRedis(irisTransaction.reference_no);

    if (irisTransaction.status === IrisPayoutStatusEnum.FAILED) {
      await irisService.declineTransaction(
        transactionModel,
        new TransferAppError(
          'Decline transaction because iris transaction status is FAILED',
          TransferFailureReasonActor.MS_TRANSFER,
          ERROR_CODE.INVALID_TRANSACTION_STATUS
        )
      );
    }

    const updatedTransactionModel = await transactionRepository.update(
      transactionModel.id,
      {
        thirdPartyOutgoingId: irisTransaction.reference_no,
        status
      }
    );

    if (!updatedTransactionModel) {
      const detail = `Failed to update transaction info for txnId: ${transactionModel.id}!`;
      logger.error(`submitTransaction: ${detail}`);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.SUBMITTER,
        ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION
      );
    }

    return updatedTransactionModel;
  } catch (error) {
    logger.error(
      'failed submission to iris after all retry or non-retriable condition',
      error
    );

    throw error;
  }
};

const isEligible = (model: ITransactionModel): boolean => {
  if (
    BankChannelEnum.IRIS === model.beneficiaryBankCodeChannel &&
    model.beneficiaryAccountNo
  ) {
    return true;
  }
  return false;
};

const shouldSendNotification = () => true;

const getRail = () => Rail.IRIS;

const irisSubmissionTemplate: ITransactionSubmissionTemplate = {
  submitTransaction,
  isEligible,
  getRail,
  shouldSendNotification
};

export default wrapLogs(irisSubmissionTemplate);
