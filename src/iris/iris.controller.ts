import _ from 'lodash';
import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import { IConfirmTransactionRequest } from './iris.type';

import { confirmTransactionRequestValidator } from './iris.validator';

import irisService from './iris.service';
import { updateRequestIdInContext } from '../common/contextHandler';

const confirmTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/iris/transaction-confirmation',
  options: {
    description: 'Confirm transaction from IRIS',
    notes: 'Private api called from IRIS to confirm transaction',
    tags: ['api', 'transactions'],
    auth: false,
    validate: {
      payload: confirmTransactionRequestValidator
    },
    handler: async (
      hapiRequest: IConfirmTransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { payload } = hapiRequest;
      await updateRequestIdInContext(payload.reference_no);
      await irisService.confirmTransaction(payload);
      return hapiResponse.response().code(Http.StatusCode.NO_CONTENT);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Ok'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const irisController: hapi.ServerRoute[] = [confirmTransaction];

export default irisController;
