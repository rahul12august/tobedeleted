import { config } from '../config';
import { HttpHeaders } from '../common/constant';
import { BASIC_PREFIX } from './iris.constant';
import { context } from '../context';
import logger from '../logger';
import { HttpClient } from '@dk/module-httpclient';
import { Http } from '@dk/module-common';
import { retryConfig } from '../common/constant';

const { baseUrl, timeout } = config.get('iris');
const apiKey = process.env.IRIS_API_KEY || '';

const httpClient: HttpClient = Http.createHttpClientForward({
  baseURL: baseUrl,
  headers: {
    [HttpHeaders.CONTENT_TYPE]: 'application/json',
    [HttpHeaders.ACCEPT]: 'application/json',
    [HttpHeaders.AUTH]: `${BASIC_PREFIX} ${Buffer.from(apiKey).toString(
      'base64'
    )}:`
  },
  timeout: timeout,
  context,
  logger,
  retryConfig: {
    ...retryConfig,
    networkErrorCodesToRetry: ['ENOENT', 'ECONNREFUSED']
  }
});

export default httpClient;
