import httpClient from './httpClient';
import { IIrisPayoutResponse, IIrisTransactionPayload } from './iris.type';
import logger, { wrapLogs } from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { HttpHeaders } from '../common/constant';
import uuidv4 from 'uuid/v4';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const submitTransaction = async (
  payload: IIrisTransactionPayload,
  idempotencyKey?: string
): Promise<IIrisPayoutResponse> => {
  try {
    const payouts = {
      payouts: [payload]
    };
    const externalKey = idempotencyKey || uuidv4();
    logger.info(
      `Submitting IRIS transaction ExternalKey : ${externalKey}, 
        beneficiary_account: ${payload.beneficiary_account},
        beneficiary_bank: ${payload.beneficiary_bank}`
    );
    const options = {
      headers: {
        [HttpHeaders.X_IDEMPOTENCY_KEY]: externalKey
      }
    };
    const result = await httpClient.post(`/api/v1/payouts`, payouts, options);
    if (!result.data || !result.data.payouts) {
      let detail = 'Error from IRIS while transferring funds!';
      logger.error(` submitTransaction: ${detail}`);
      throw new TransferAppError(
        detail,
        result.status == 500
          ? TransferFailureReasonActor.IRIS
          : TransferFailureReasonActor.MS_TRANSFER,
        ERROR_CODE.ERROR_FROM_IRIS
      );
    }
    logger.info(
      `Submit IRIS transaction response : ${JSON.stringify(
        result.data.payouts[0]
      )}`
    );
    return result.data.payouts[0];
  } catch (error) {
    const detail = 'Error from IRIS while transferring funds!';
    logger.error(`submitTransaction catch: ${detail}`, error);
    if (error instanceof TransferAppError) throw error;
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.ERROR_FROM_IRIS
    );
  }
};

const irisRepository = wrapLogs({
  submitTransaction
});

export default irisRepository;
