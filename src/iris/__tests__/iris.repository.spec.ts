import httpClient from '../httpClient';
import irisRepository from '../iris.repository';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import {
  getIrisTransactionData,
  getIrisTransactionPayload
} from '../__mocks__/iris.data';

jest.mock('../httpClient');

describe('iris.repository', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('submitTransaction', () => {
    const irisTransactionPayload = getIrisTransactionPayload();
    const irisTransaction = getIrisTransactionData();
    it('should return iris transaction success', async () => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          payouts: [irisTransaction]
        }
      });
      const idempotencyKey = '1234';
      const expected = await irisRepository.submitTransaction(
        irisTransactionPayload,
        idempotencyKey
      );
      const payouts = {
        payouts: [irisTransactionPayload]
      };
      const options = { headers: { 'X-Idempotency-Key': idempotencyKey } };
      expect(httpClient.post).toBeCalledWith(
        `/api/v1/payouts`,
        payouts,
        options
      );
      expect(expected).toEqual(irisTransaction);
    });

    it(`should throw error ${ERROR_CODE.ERROR_FROM_IRIS} when submit error`, async () => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: null
      });

      const expected = await irisRepository
        .submitTransaction(irisTransactionPayload)
        .catch(err => err);

      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toBe(ERROR_CODE.ERROR_FROM_IRIS);
    });
  });
});
