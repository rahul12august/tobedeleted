import irisSubmissionTemplate from '../irisSubmissionTemplate.service';
import { BankChannelEnum, Util } from '@dk/module-common';
import { getTransactionDetail } from '../../transaction/__mocks__/transaction.data';
import {
  IIrisTransactionPayload,
  IIrisPayoutResponse,
  IrisPayoutStatusEnum
} from '../iris.type';
import {
  getIrisTransactionData,
  getIrisTransactionPayload
} from '../__mocks__/iris.data';
import irisRepository from '../iris.repository';
import transactionRepository from '../../transaction/transaction.repository';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../../transaction/transaction.enum';
import { Rail } from '@dk/module-common';
import irisService from '../iris.service';

jest.mock('../iris.repository');
jest.mock('../../transaction/transaction.repository');
jest.mock('../iris.service');

describe('irisSubmissionTemplate', () => {
  let mockedRetry = jest
    .spyOn(Util, 'retry')
    .mockImplementation(async (_, fn, ...args) => await fn(...args)); // given
  const transactionModel = {
    ...getTransactionDetail(),
    beneficiaryBankCodeChannel: BankChannelEnum.IRIS,
    beneficiaryAccountNo: 'beneficiary_account',
    beneficiaryAccountName: 'beneficiary_name',
    beneficiaryBankCode: 'beneficiary_bank',
    beneficiaryIrisCode: 'beneficiary_bank',
    note: 'notes',
    transactionAmount: 1000
  };

  describe('isEligible', () => {
    it(`should return true when beneficiaryBankCodeChannel is ${BankChannelEnum.IRIS}`, () => {
      // when
      const eligible = irisSubmissionTemplate.isEligible(transactionModel);

      // then
      expect(eligible).toBeTruthy();
    });

    it(`should return false when beneficiaryBankCodeChannel is not ${BankChannelEnum.IRIS}`, () => {
      // when
      const eligible = irisSubmissionTemplate.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL
      });

      // then
      expect(eligible).not.toBeTruthy();
    });

    it('should return false when beneficiaryAccountNo is undefined', async () => {
      // when
      const eligible = irisSubmissionTemplate.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.IRIS,
        beneficiaryAccountNo: undefined
      });

      // then
      expect(eligible).not.toBeTruthy();
    });
  });

  describe('submitTransaction', () => {
    const irisTransactionPayload: IIrisTransactionPayload = getIrisTransactionPayload();
    const irisTransactionData = getIrisTransactionData();
    beforeEach(() => {
      (irisRepository.submitTransaction as jest.Mock).mockResolvedValue(
        irisTransactionData
      );
    });
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should submit transaction to iris with correct payload', async () => {
      // Given
      const expectedPayload = {
        ...irisTransactionPayload,
        notes: transactionModel.thirdPartyOutgoingId
      };
      const irisPayoutRes: IIrisPayoutResponse = {
        status: IrisPayoutStatusEnum.COMPLETED,
        /* eslint-disable @typescript-eslint/camelcase */
        reference_no: transactionModel.thirdPartyOutgoingId
      };
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({});
      (irisRepository.submitTransaction as jest.Mock).mockResolvedValueOnce(
        irisPayoutRes
      );

      // When
      await irisSubmissionTemplate.submitTransaction(transactionModel);

      // Then
      expect(irisRepository.submitTransaction).toHaveBeenCalledWith(
        expectedPayload,
        transactionModel.externalId
      );
      expect(transactionRepository.update).toHaveBeenCalledWith(
        transactionModel.id,
        {
          thirdPartyOutgoingId: irisPayoutRes.reference_no,
          status: TransactionStatus.SUBMITTED
        }
      );
    });

    it('should retry first times when iris repository throw network error', async () => {
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({});
      mockedRetry.mockRestore();

      (irisRepository.submitTransaction as jest.Mock).mockResolvedValueOnce({});

      const result = await irisSubmissionTemplate.submitTransaction(
        transactionModel
      );
      expect(result).toBeDefined();
      expect(irisRepository.submitTransaction).toHaveBeenCalledTimes(1);
    });
    it(`should throw error ${ERROR_CODE.UNEXPECTED_ERROR} when iris returned bad request error`, async () => {
      (irisRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.UNEXPECTED_ERROR
        )
      );
      const error = await irisSubmissionTemplate
        .submitTransaction(transactionModel)
        .catch(err => err);

      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toBe(ERROR_CODE.UNEXPECTED_ERROR);
      expect(error.detail).toBe('Test error!');
      expect(error.actor).toBe(TransferFailureReasonActor.UNKNOWN);
      expect(irisRepository.submitTransaction).toHaveBeenCalledTimes(1);
    });

    it(`should throw error ${ERROR_CODE.ERROR_FROM_IRIS} when iris returned error detail`, async () => {
      (irisRepository.submitTransaction as jest.Mock).mockResolvedValueOnce({
        error: 'test'
      });
      const error = await irisSubmissionTemplate
        .submitTransaction(transactionModel)
        .catch(err => err);

      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toBe(ERROR_CODE.ERROR_FROM_IRIS);
      expect(irisRepository.submitTransaction).toHaveBeenCalledTimes(1);
    });
  });

  describe('declineTransaction', () => {
    const irisTransactionPayload: IIrisTransactionPayload = getIrisTransactionPayload();
    afterEach(() => {
      jest.resetAllMocks();
    });
    it('should get failed from iris with correct payload', async () => {
      // Given
      const expectedPayload = {
        ...irisTransactionPayload,
        notes: transactionModel.thirdPartyOutgoingId
      };
      const irisPayout: IIrisPayoutResponse = {
        status: IrisPayoutStatusEnum.FAILED,
        /* eslint-disable @typescript-eslint/camelcase */
        reference_no: transactionModel.thirdPartyOutgoingId
      };
      (irisService.declineTransaction as jest.Mock).mockResolvedValueOnce(null);

      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...transactionModel,
        status: TransactionStatus.DECLINED
      });

      (irisRepository.submitTransaction as jest.Mock).mockResolvedValueOnce(
        irisPayout
      );
      // When
      await irisSubmissionTemplate.submitTransaction(transactionModel);

      // Then
      expect(irisRepository.submitTransaction).toHaveBeenCalledWith(
        expectedPayload,
        transactionModel.externalId
      );
      expect(transactionRepository.update).toHaveBeenCalledWith(
        transactionModel.id,
        {
          thirdPartyOutgoingId: irisPayout.reference_no,
          status: TransactionStatus.DECLINED
        }
      );
      let transferAppError = new TransferAppError(
        'Decline transaction because irisTransaction.status === IrisPayoutStatusEnum.FAILED',
        TransferFailureReasonActor.MS_TRANSFER,
        ERROR_CODE.INVALID_TRANSACTION_STATUS
      );
      expect(irisService.declineTransaction).toHaveBeenCalledWith(
        {
          ...transactionModel
        },
        transferAppError
      );
    });
  });

  describe('Rail check', () => {
    it(`should have rail property defined`, () => {
      // when
      const rail = irisSubmissionTemplate.getRail?.();

      // then
      expect(rail).toEqual(Rail.IRIS);
    });
  });
});
