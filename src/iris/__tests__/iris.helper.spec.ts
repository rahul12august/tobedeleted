import { mapToIrisTransaction } from '../iris.helper';
import { getTransactionDetail } from '../../transaction/__mocks__/transaction.data';
import { GOPAY_NOTES } from '../iris.constant';

describe('iris.helper', () => {
  const transactionModel = {
    ...getTransactionDetail(),
    beneficiaryAccountNo: '08123450000',
    beneficiaryAccountName: 'IRIS Test Customer 1',
    beneficiaryBankName: 'GoPay',
    thirdPartyOutgoingId: '20253T000013',
    transactionAmount: 200000
  };
  describe('mapToIrisTransaction', () => {
    it(`notes from payload should contain "TopUp via Jago" `, async () => {
      const irisTransactionPayload = await mapToIrisTransaction(
        transactionModel
      );

      expect(irisTransactionPayload.notes).toEqual(
        GOPAY_NOTES + transactionModel.thirdPartyOutgoingId
      );
    });
  });
});
