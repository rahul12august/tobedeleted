import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import irisController from '../iris.controller';
import { ConfirmTransactionRequest } from '../iris.type';
import ResponseWrapper from '../../plugins/responseWrapper';
import errorHandler from '../../common/handleValidationErrors';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import irisService from '../iris.service';
import { mockConfirmTransactionRequest } from '../__mocks__/iris.data';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

jest.mock('../iris.service');

describe('irisController', () => {
  let server: hapi.Server;
  beforeAll(() => {
    server = new hapi.Server({
      routes: {
        validate: {
          options: {
            abortEarly: false
          },
          failAction: errorHandler
        }
      }
    });
    server.register([ResponseWrapper]);
    server.route(irisController);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('confirm transaction', () => {
    const confirmTransactionPayload: ConfirmTransactionRequest = mockConfirmTransactionRequest();

    it('should response 204 with no payload', async () => {
      // arrange
      const options = {
        method: 'POST',
        url: '/iris/transaction-confirmation',
        payload: confirmTransactionPayload
      };

      (irisService.confirmTransaction as jest.Mock).mockResolvedValueOnce({});
      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(Http.StatusCode.NO_CONTENT);
      expect(irisService.confirmTransaction).toBeCalledWith(
        confirmTransactionPayload
      );
    });

    it('should return bad request error (400) when service throw AppError', async () => {
      // arrange
      const options = {
        method: 'POST',
        url: '/iris/transaction-confirmation',
        payload: confirmTransactionPayload
      };

      (irisService.confirmTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.INVALID_AMOUNT
        )
      );

      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
      expect(response.result.error.code).toEqual(ERROR_CODE.INVALID_AMOUNT);
    });

    it('should return server error (500) when service throw server error', async () => {
      // arrange
      const options = {
        method: 'POST',
        url: '/iris/transaction-confirmation',
        payload: confirmTransactionPayload
      };

      (irisService.confirmTransaction as jest.Mock).mockRejectedValueOnce(
        new Error()
      );
      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(
        Http.StatusCode.INTERNAL_SERVER_ERROR
      );
    });
  });
});
