import { IConfirmTransactionInput } from '../../transaction/transaction.type';

import { REVERSED_REASON_ERROR_FROM_SWITCHING } from '../../transaction/transaction.constant';

import irisService from '../iris.service';
import { mockTransactionFromDb } from '../../transaction/__mocks__/transactionConfirmation.data';
import transactionReversalHelper from '../../transaction/transactionReversal.helper';
import { mockConfirmTransactionRequest } from '../__mocks__/iris.data';
import { ConfirmTransactionStatusEnum } from '../iris.enum';
import transactionRepository from '../../transaction/transaction.repository';
import transactionConfirmationService from '../../transaction/transactionConfirmation.service';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../../transaction/transaction.enum';
import transactionBlockingHelper from '../../transaction/transactionBlocking.helper';
import transactionExecutionService, {
  defaultExecutionHooks
} from '../../transaction/transactionExecution.service';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';

jest.mock('../../transaction/transaction.repository');
jest.mock('../../transaction/transactionReversal.helper');
jest.mock('../../transaction/transactionBlocking.helper');
jest.mock('../../transaction/transactionExecution.service');

describe('iris.service', () => {
  describe('confirmTransaction', () => {
    const input = mockConfirmTransactionRequest();
    it(`should update transaction status to SUCCEED when payload contains status ${ConfirmTransactionStatusEnum.COMPLETED}`, async () => {
      const mockedTransactionFromDb = {
        ...mockTransactionFromDb(),
        paymentInstructionCode: 'PI_TRANSFER',
        transactionAmount: Number(input.amount),
        thirdPartyOutgoingId: input.reference_no
      };
      const _input: IConfirmTransactionInput = {
        responseCode: 200,
        responseMessage: undefined,
        externalId: '0093384848',
        amount: Number(input.amount)
      };

      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...mockedTransactionFromDb,
        status: TransactionStatus.SUBMITTED
      });
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...mockedTransactionFromDb,
        status: TransactionStatus.SUCCEED
      });
      (transactionExecutionService.settleTransaction as jest.Mock).mockResolvedValueOnce(
        {
          ...mockedTransactionFromDb,
          status: TransactionStatus.SUCCEED
        }
      );
      (transactionRepository.updateIfExists as jest.Mock).mockResolvedValueOnce(
        mockedTransactionFromDb
      );
      await irisService.confirmTransaction(input);
      expect(transactionRepository.updateIfExists).toBeCalledWith(
        _input,
        undefined
      );
      expect(transactionExecutionService.settleTransaction).toBeCalledTimes(1);
    });

    it.each([
      ConfirmTransactionStatusEnum.QUEUED,
      ConfirmTransactionStatusEnum.PROCESSED,
      ConfirmTransactionStatusEnum.APPROVED
    ])(
      'should not do anything when payload contains status %s',
      async status => {
        // Given
        const input = {
          ...mockConfirmTransactionRequest(),
          status
        };
        const spyConfirmTransaction = jest.spyOn(
          transactionConfirmationService,
          'confirmTransaction'
        );

        // When
        await irisService.confirmTransaction(input);

        // Then
        expect(spyConfirmTransaction).not.toBeCalled();
      }
    );

    it.each([
      ConfirmTransactionStatusEnum.FAILED,
      ConfirmTransactionStatusEnum.REJECTED
    ])(
      'should reverse transaction when payload contains status %s',
      async status => {
        const input = {
          ...mockConfirmTransactionRequest(),
          status
        };
        const mockedTransactionFromDb = {
          ...mockTransactionFromDb(),
          transactionAmount: Number(input.amount),
          thirdPartyOutgoingId: input.reference_no,
          status: TransactionStatus.SUCCEED
        };
        (transactionRepository.updateIfExists as jest.Mock).mockResolvedValueOnce(
          mockedTransactionFromDb
        );
        (transactionReversalHelper.revertFailedTransaction as jest.Mock).mockResolvedValueOnce(
          {}
        );

        await irisService.confirmTransaction(input);
        expect(
          transactionReversalHelper.revertFailedTransaction
        ).toBeCalledWith(
          mockedTransactionFromDb.id,
          REVERSED_REASON_ERROR_FROM_SWITCHING,
          mockedTransactionFromDb
        );
      }
    );
  });

  describe('declineTransaction', () => {
    const input = mockConfirmTransactionRequest();
    it(`should update transaction status to DECLINED, `, async () => {
      const mockedTransactionFromDb = {
        ...mockTransactionFromDb(),
        paymentInstructionCode: 'PI_TRANSFER',
        transactionAmount: Number(input.amount),
        thirdPartyOutgoingId: input.reference_no
      };

      (transactionBlockingHelper.cancelBlockingTransaction as jest.Mock).mockResolvedValueOnce(
        null
      );
      (transactionExecutionService.onFailedTransaction as jest.Mock).mockResolvedValueOnce(
        null
      );
      let transferAppError = new TransferAppError(
        'Decline transaction because irisTransaction.status === IrisPayoutStatusEnum.FAILED',
        TransferFailureReasonActor.MS_TRANSFER,
        ERROR_CODE.INVALID_TRANSACTION_STATUS
      );
      await irisService.declineTransaction(
        {
          ...mockedTransactionFromDb,
          status: TransactionStatus.DECLINED
        },
        transferAppError
      );
      expect(
        transactionBlockingHelper.cancelBlockingTransaction
      ).toHaveBeenLastCalledWith({
        ...mockedTransactionFromDb,
        status: TransactionStatus.DECLINED
      });
      expect(
        transactionExecutionService.onFailedTransaction
      ).toHaveBeenCalledWith(
        { ...mockedTransactionFromDb, status: TransactionStatus.DECLINED },
        defaultExecutionHooks,
        transferAppError
      );
    });
  });
});
