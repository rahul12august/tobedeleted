import * as Server from '../server';
import logger from '../logger';
import producer from '../common/kafkaProducer';
import consumers from '../consumers';
import envChecker from '../envChecker';
import { Http } from '@dk/module-common';

jest.unmock('../logger');

jest.mock('../common/mongoDb', () => ({
  connectMongo: jest.fn()
}));

jest.mock('../context', () => ({
  context: { getStore: () => {}, enterWith: () => {} }
}));

jest.mock('../common/kafkaProducer', () => ({
  connect: jest.fn()
}));

jest.mock('../consumers', () => ({
  init: jest.fn()
}));

jest.mock('@hapi/hapi', () => {
  const origin = require.requireActual('@hapi/hapi');
  class ServerMock extends origin.Server {
    start = jest.fn();
    stop = jest.fn();
  }
  return {
    ...origin,
    Server: ServerMock
  };
});

describe('Hapi server', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should create server', async () => {
    const server = await Server.init();
    expect(server).toBeDefined();
  });

  it('should not start server if it run on child module', async () => {
    const spyInfo = jest.spyOn(logger, 'info');
    await Server.start({
      parent: 'having parent'
    } as any);
    expect(spyInfo).not.toBeCalled();
    expect(producer.connect).not.toBeCalled();
  });

  it('should start server if it run on main module', async () => {
    envChecker.init = jest.fn();
    const spyInfo = jest.spyOn(logger, 'info');
    await Server.start({
      parent: null
    } as any);
    expect(envChecker.init).nthCalledWith(1);
    expect(spyInfo).toBeCalled();
  });

  it('should log error if server start error', async () => {
    const spyError = jest.spyOn(logger, 'error');
    const spyInit = jest.spyOn(Server, 'init');
    spyInit.mockRejectedValueOnce('error');
    await Server.start({
      parent: null
    } as any);
    expect(spyError).toBeCalled();
  });

  it('should handle termination', async () => {
    const spyInfo = jest.spyOn(logger, 'info');
    const consumerMock = {
      disconnect: jest.fn()
    };
    consumers.init = jest.fn().mockResolvedValueOnce([consumerMock]);
    envChecker.init = jest.fn();
    await Server.start({
      parent: null
    } as any);
    const { hapiServer, kafkaConsumers } = await Server.handleTermination();
    expect(hapiServer.stop).toBeCalled();
    expect(kafkaConsumers).toBeDefined();
    expect(spyInfo.mock.calls).toEqual([
      ['Start server'],
      ['server started at 0.0.0.0:8080'],
      ['SIGTERM received'],
      ['consumers disconnected'],
      ['waiting for pending process. processCounter: 0'],
      ['all pending process completed. processCounter: 0'],
      ['server stopped']
    ]);
  });

  it('should count process', async () => {
    const spyDebug = jest.spyOn(logger, 'debug');
    const server = await Server.init();
    const response: any = await server.inject({
      method: 'GET',
      url: '/ping'
    });
    expect(response.statusCode).toEqual(Http.StatusCode.OK);
    expect(spyDebug.mock.calls).toEqual([
      ['incoming request. processCounter: 1'],
      ['request completed. processCounter: 0']
    ]);
  });

  it('should wait for process on termination', async () => {
    const spyInfo = jest.spyOn(logger, 'info');
    const server = await Server.init();
    server.route({
      method: 'GET',
      path: '/delay',
      options: {
        auth: false
      },
      handler: async () => {
        await new Promise(r => setTimeout(r, 10));
        return 'ok';
      }
    });
    server.inject({
      method: 'GET',
      url: '/delay'
    });
    await new Promise(r => setTimeout(r, 10));
    await Server.handleTermination();
    const infoCalls = spyInfo.mock.calls;
    expect(infoCalls[3]).toEqual([
      'waiting for pending process. processCounter: 1'
    ]);
    expect(infoCalls[6]).toEqual([
      'all pending process completed. processCounter: 0'
    ]);
  });
});
