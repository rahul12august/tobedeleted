import hapi from '@hapi/hapi';
import * as Inert from '@hapi/inert';
import * as Vision from '@hapi/vision';
import kafkaProducer from './common/kafkaProducer';

import { connectMongo } from './common/mongoDb';
import errorHandler from './common/handleValidationErrors';
import Swagger from './plugins/swagger';
import ResponseWrapper from './plugins/responseWrapper';
import RequestWrapper from './plugins/requestWrapper';
import './joiInitializer';
import { routes } from './routes';
import logger from './logger';
import { config } from './config';
import consumers from './consumers';
import { aclWrapper, HapiPlugin } from '@dk/module-common';
import TokenAuth from './plugins/tokenAuth';
import envChecker from './envChecker';
import { Consumer } from 'kafkajs';

const isBulkTransactionOnly: boolean =
  process.env.IS_BULK_TRANSACTION_ONLY === 'true';

const { port, host } = config.get('server');

let kafkaConsumers: Consumer[] = [];
let hapiServer: hapi.Server;
let processCounter = 0;

const createServer = async () => {
  const server = new hapi.Server({
    port,
    host,
    routes: {
      validate: {
        options: {
          abortEarly: false
        },
        failAction: errorHandler
      }
    }
  });

  const plugins: any[] = [
    Inert,
    Vision,
    Swagger,
    HapiPlugin.Good(logger),
    ResponseWrapper,
    RequestWrapper,
    TokenAuth,
    aclWrapper
  ];
  await server.register(plugins);

  // Register routes
  server.route(routes);

  // Increase counter on incoming request
  server.ext({
    type: 'onPreHandler',
    method: function(_request, h) {
      processCounter++;
      logger.debug(`incoming request. processCounter: ${processCounter}`);
      return h.continue;
    }
  });

  // Decrease counter on completed request
  server.ext({
    type: 'onPostHandler',
    method: function(_request, h) {
      processCounter--;
      logger.debug(`request completed. processCounter: ${processCounter}`);
      return h.continue;
    }
  });

  return server;
};

export const init = async () => {
  await connectMongo();
  const server = await createServer();
  await server
    .initialize()
    .then(() =>
      logger.info(`server started at ${server.info.host}:${server.info.port}`)
    );
  return server;
};

export const handleTermination = async () => {
  logger.info('SIGTERM received');

  // disconnect consumer
  // https://kafka.js.org/docs/consumer-example
  if (kafkaConsumers) {
    await Promise.all(kafkaConsumers.map(consumer => consumer.disconnect()));
    logger.info('consumers disconnected');
  }

  // wait for pending HTTP request(s) to complete
  logger.info(`waiting for pending process. processCounter: ${processCounter}`);
  while (processCounter > 0) {
    logger.info(`waiting processCounter: ${processCounter}`);
    await new Promise(r => setTimeout(r, 1000));
  }
  logger.info(
    `all pending process completed. processCounter: ${processCounter}`
  );

  // stop server
  // https://hapi.dev/api/?v=18.4.2#-await-serverstopoptions
  if (hapiServer) {
    await hapiServer.stop({ timeout: config.get('shutdownTimeout') });
    logger.info('server stopped');
  }

  // not calling kafkaProducer.disconnect()
  // to allow pending http request that needs to produce message
  // to try to complete

  // not calling process.exit()
  // to allow pending process to try to complete
  return {
    hapiServer,
    kafkaConsumers
  };
};

export const start = async (module: NodeModule) => {
  if (!module.parent) {
    logger.info('Start server');
    envChecker.init();
    await init()
      .then(async server => {
        hapiServer = server;
        await kafkaProducer.connect();
        kafkaConsumers = await consumers.init(isBulkTransactionOnly);
        await server.start();
      })
      .catch(err => {
        logger.error('Server cannot start', err);
        logger.onFinished(() => {
          process.exit(1);
        });
      });
  }
};

// handle signal
process.on('SIGTERM', handleTermination);

start(module);
