import { ITransactionCodeMapping } from '../configuration/configuration.type';
import entitlementHelper from './entitlement.helper';
import uuidv4 from 'uuid/v4';

import {
  EntitlementContext,
  ICounterResponse,
  ICounterRevertRequest,
  ICounterUsageRequest,
  ICustomerEntitlementByTransCodeRequest,
  ICustomerEntitlementByTransCodeResponse,
  IEntitlement,
  IEntitlementResponse,
  IUpdateUsageCounterEntitlementResponse
} from './entitlement.interface';
import entitlementRepository from './entitlement.repository';
import { IEntitlementInfo } from '../transaction/transaction.model';
import {
  getTransactionInfoTracker,
  setTransactionInfoTracker
} from '../common/contextHandler';
import logger, { wrapLogs } from '../logger';
import transactionRepository from '../transaction/transaction.repository';
import { UsageCounter } from '../award/award.type';
import { IConfirmTransactionInput } from '../transaction/transaction.type';
import {
  ConfirmTransactionStatus,
  TransferFailureReasonActor
} from '../transaction/transaction.enum';
import { TransferAppError } from '../errors/AppError';
import { EntitlementErrorReasonType } from './entitlement.enum';
import _ from 'lodash';
import { ERROR_CODE } from '../common/errors';
import { ENTITLEMENT_EXCLUDED_PAYMENT_SERVICE_TYPES } from './entitlement.constant';

const retrieveUniqueEntitlementCodesBasedOnTransactionCodes = (
  transactionCodes: ITransactionCodeMapping[]
): Map<string, IEntitlement> => {
  //step 1 - create a set [SET entitlementCodes], that is to ensure unique limit group code/entitlement code/counter code
  //const uniqueEntitlementCodes = new Set();
  const uniqueEntitlementCodeMap = new Map<string, IEntitlement>();

  //step 2 -- loop
  transactionCodes.forEach(tc => {
    //step 3 - individual transaction code: check for the following
    // validation 1: whether there is awardgroupcounter/entitlement code/counter code
    const {
      awardGroupCounter,
      entitlementCode,
      counterCode,
      limitGroupCode
    } = tc.transactionCodeInfo;

    if (
      _.isEmpty(awardGroupCounter) ||
      _.isUndefined(awardGroupCounter) ||
      _.isEmpty(entitlementCode) ||
      _.isUndefined(entitlementCode) ||
      _.isEmpty(counterCode) ||
      _.isUndefined(counterCode) ||
      _.isEmpty(limitGroupCode) ||
      _.isUndefined(limitGroupCode)
    ) {
      logger.info(
        `retrieveUniqueEntitlementCodesBasedOnTransactionCodes: missing awardGroupCounter: ${awardGroupCounter}, 
        entitlementCode: ${entitlementCode}, counterCode: ${counterCode}, limitGroupCode: ${limitGroupCode}`
      );
      return;
    }
    const isEntitlementExist: boolean =
      !_.isEmpty(awardGroupCounter) &&
      !_.isEmpty(entitlementCode) &&
      !_.isEmpty(counterCode);

    if (!isEntitlementExist) {
      logger.info(
        `retrieveUniqueEntitlementCodesBasedOnTransactionCodes: isEntitlementExist: ${isEntitlementExist}`
      );
      return;
    }

    //validation 2: check whether entitlement code is within the predefined boundaries (currently only handling bonus transfer and bonus atm withdrawal)
    const isWithinFilteredEntitlementCodes = entitlementHelper
      .getFilteredEntitlementCodes()
      .includes(entitlementCode);

    if (!isWithinFilteredEntitlementCodes) {
      return;
    }

    if (!uniqueEntitlementCodeMap.has(limitGroupCode)) {
      uniqueEntitlementCodeMap.set(limitGroupCode, {
        limitGroupCode,
        awardGroupCounter,
        entitlementCode,
        counterCode
      });
    }
  });

  return uniqueEntitlementCodeMap;
};

const validateLatestEntitlementQuota = (
  latestEntitlements: IEntitlementResponse[]
): IUpdateUsageCounterEntitlementResponse => {
  const countersResp: ICounterResponse[] = [];

  //latestEntitlements.forEach(entitlement => {
  for (const entitlement of latestEntitlements) {
    const { quota, usedQuota } = entitlement;

    if (_.isUndefined(usedQuota)) {
      continue;
    }

    //check if existing
    const futureQuota = usedQuota + 1;

    if (futureQuota <= quota) {
      //update usedquota to reflect future quota
      entitlement.usedQuota = futureQuota;

      //emulate successful record //if it is still within quota
      countersResp.push({
        id: uuidv4(),
        recorded: true
      });
    } else {
      //if it bigger then quota
      //emulate failed update
      countersResp.push({
        id: uuidv4(),
        recorded: false,
        reason: {
          type: EntitlementErrorReasonType.QUOTA_LIMIT,
          description: 'exceed quota limit'
        }
      });
    }
  }
  // });

  return {
    entitlements: latestEntitlements,
    counters: countersResp
  };
};

const retrieveCustomerEntitlementRecommendedServicePreview = async (
  params: ICustomerEntitlementByTransCodeRequest
): Promise<ICustomerEntitlementByTransCodeResponse | null> => {
  //extract params
  const { transactionCodes, customerId } = params;

  const updatedEntitlementMap: Map<
    string,
    IUpdateUsageCounterEntitlementResponse
  > = new Map();

  //when it is only to preview the recommended service,
  //transaction id will not exists
  if (
    _.isEmpty(transactionCodes) ||
    _.isUndefined(transactionCodes) ||
    _.isEmpty(customerId) ||
    _.isUndefined(customerId)
  ) {
    const detail = `transactionCodes or customerId is null!`;
    logger.info(
      `retrieveCustomerEntitlementRecommendedServicePreview: ${detail}`
    );
    // return null;
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.JAGO,
      ERROR_CODE.CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_PREVIEW_ERROR
    );
  }

  //based on multiple transaction codes, extract unique entitlement code map
  const uniqueEntitlementCodeMap = retrieveUniqueEntitlementCodesBasedOnTransactionCodes(
    transactionCodes
  );

  //if there is no entitlement map, return empty map
  if (uniqueEntitlementCodeMap == null || uniqueEntitlementCodeMap.size == 0) {
    logger.info(`retrieveCustomerEntitlementRecommendedServicePreview: uniqueEntitlementCodeMap is empty, 
    hence no changes on usagecounters to be made
    `);

    return null;
  }

  for (let [limitGroupCode, entitlementInfo] of uniqueEntitlementCodeMap) {
    const { entitlementCode, awardGroupCounter } = entitlementInfo;

    logger.info(`retrieveCustomerEntitlementRecommendedServicePreview: 
    OVERVIEW on entitlement info of
    entitlementCode: [${entitlementCode}] - 
    awardGroupCounter: [${awardGroupCounter}] - 
    `);

    //step 1 - execute query
    const latestEntitlement: IEntitlementResponse[] = await entitlementRepository.getEntitlements(
      customerId,
      [entitlementCode]
    );

    //step 2 - emulate usedQuota + 1 to provide overview whether there is a fee for current transaction
    const latestEntitlementValidatedResponse = validateLatestEntitlementQuota(
      latestEntitlement
    );

    if (!updatedEntitlementMap.has(limitGroupCode)) {
      updatedEntitlementMap.set(
        limitGroupCode,
        latestEntitlementValidatedResponse
      );
    }

    if (!updatedEntitlementMap.has(awardGroupCounter)) {
      updatedEntitlementMap.set(
        awardGroupCounter,
        latestEntitlementValidatedResponse
      );
    }
  }

  return {
    updatedEntitlementMapResponse: updatedEntitlementMap
  };
};

const processCustomerEntitlementConsumption = async (
  params: ICustomerEntitlementByTransCodeRequest
): Promise<ICustomerEntitlementByTransCodeResponse | null> => {
  /*
    assumption:
      1. multiple transaction codes
      2. 1 limitGroupCode will only have 1 counterCode 
        -> e.g. 1 limitGroupCode is mapped to only 1 awardGroupCounter
      3. in most cases, the uniqueEntitlementCodeMap will only return 1 result
  */

  //extract params
  const {
    transactionCodes,
    customerId,
    transactionId,
    paymentServiceType
  } = params;

  const updatedEntitlementMap: Map<
    string,
    IUpdateUsageCounterEntitlementResponse
  > = new Map();

  if (_.isUndefined(paymentServiceType) || _.isEmpty(paymentServiceType)) {
    const detail = `Payment service type is undefined or empty!`;
    logger.info(`processCustomerEntitlementConsumption: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.JAGO,
      ERROR_CODE.CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_CONSUME_ERROR
    );
  }

  //check whether current paymentServiceType excluded from payment serviceType
  if (ENTITLEMENT_EXCLUDED_PAYMENT_SERVICE_TYPES.includes(paymentServiceType)) {
    return null;
  }

  //if the required parameters not exists, then throw error
  if (
    _.isUndefined(transactionCodes) ||
    _.isEmpty(transactionCodes) ||
    _.isUndefined(customerId) ||
    _.isEmpty(customerId) ||
    _.isUndefined(transactionId) ||
    _.isEmpty(transactionId)
  ) {
    let detail = `transactionCodes ${transactionCodes}, customerId ${customerId}!`;
    logger.info(`processCustomerEntitlementConsumption: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_CONSUME_ERROR
    );
  }

  //based on multiple transaction codes, extract unique entitlement code map
  const uniqueEntitlementCodeMap = retrieveUniqueEntitlementCodesBasedOnTransactionCodes(
    transactionCodes
  );

  //if there is no entitlement map, return empty map
  if (uniqueEntitlementCodeMap == null || uniqueEntitlementCodeMap.size == 0) {
    logger.info(`processCustomerEntitlementConsumption: uniqueEntitlementCodeMap is empty, 
    hence no changes on usagecounters to be made
    `);
    return null;
  }

  const entitlementInfoArr: IEntitlementInfo[] = [];

  for (let [limitGroupCode, entitlementInfo] of uniqueEntitlementCodeMap) {
    // uniqueEntitlementCodeMap.forEach(async (entitlementInfo, limitGroupCode) => {
    //step 1 -- generate unique ref key
    const entitlementCodeRefId = uuidv4();

    const { counterCode, entitlementCode, awardGroupCounter } = entitlementInfo;

    logger.info(`processCustomerEntitlementConsumption: CONSUMING entitlement info of
    counterCode: [${counterCode}] - 
    entitlementCode: [${entitlementCode}] - 
    awardGroupCounter: [${awardGroupCounter}] - 
    `);

    //step 2 -- prepare to update counter usage based on the counter code
    const counterUsages: ICounterUsageRequest[] = [
      {
        id: entitlementCodeRefId,
        counterCode: counterCode,
        usedQuota: 1
      }
    ];

    //step 3 - execute update counter usage as well as query the updated result
    const updatedEntitlement: IUpdateUsageCounterEntitlementResponse = await entitlementRepository.updateEntitlementUsageCounters(
      {
        customerId: customerId,
        entitlementCodes: [entitlementCode],
        counterUsages: counterUsages
      }
    );

    if (
      !_.isEmpty(updatedEntitlement) &&
      !_.isEmpty(updatedEntitlement.counters)
    ) {
      //step 4 -- check if updateEntitlment actually succeed or failed
      const isEntitlementUpdated = updatedEntitlement.counters[0].recorded;

      //only if successful then insert to db
      //entitlementInfoArr only used for reversal, hence only need to record
      //the entitlement that is successfully updated
      if (isEntitlementUpdated) {
        entitlementInfoArr.push({
          entitlementCodeRefId: entitlementCodeRefId,
          counterCode: counterCode,
          customerId: customerId
        });
      }

      //save based on limitgroupcode
      //note: this will still keep track of those
      //entitlements that is not recorded, that is
      //used to indicate mainly the entailing fee
      if (!updatedEntitlementMap.has(limitGroupCode)) {
        updatedEntitlementMap.set(limitGroupCode, updatedEntitlement);
      }

      //save based on award group counter
      //note: this will still keep track of those
      //entitlements that is not recorded, that is
      //used to indicate mainly the entailing fee
      if (!updatedEntitlementMap.has(awardGroupCounter)) {
        updatedEntitlementMap.set(awardGroupCounter, updatedEntitlement);
      }
    }
  }

  if (updatedEntitlementMap.size === 0) {
    const detail = `Error when trying to consume entitlement!`;
    logger.error(`processCustomerEntitlementConsumption: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_CONSUME_ERROR
    );
  }

  //update transaction model
  await transactionRepository.update(transactionId, {
    entitlementInfo: entitlementInfoArr
  });

  //setting in context for reversal
  await setTransactionInfoTracker(entitlementInfoArr);
  // step 5 - return the map

  return {
    updatedEntitlementMapResponse: updatedEntitlementMap
  };
};

const processReversalEntitlementCounterUsageBasedOnContext = async () => {
  logger.info('Processing reversal entitlement counter usage based on context');
  const info: EntitlementContext[] | undefined = getTransactionInfoTracker();
  if (info && !_.isEmpty(info)) {
    for (const entitlementInfo of info) {
      if (entitlementInfo) {
        const counterReverts: ICounterRevertRequest[] = [
          {
            id: entitlementInfo.entitlementCodeRefId as string,
            counterCode: entitlementInfo.counterCode as string
          }
        ];

        const revertCounterResponse = await entitlementRepository.revertEntitlementUsageCounters(
          {
            customerId: entitlementInfo.customerId as string,
            counterReverts: counterReverts
          }
        );

        logger.info(`processReversalEntitlementCounterUsageBasedOnContext: revert info
            ${JSON.stringify(revertCounterResponse.counterReverts)}
        `);
      }
    }
  }
};

const mapEntitlementUsageCounterByLimitGroupCode = (
  usageCounters: UsageCounter[],
  entitlementResultResponse: ICustomerEntitlementByTransCodeResponse | null
): UsageCounter[] => {
  if (entitlementResultResponse != null) {
    const usedUsageCounters = [...usageCounters];

    const entitlementMap =
      entitlementResultResponse.updatedEntitlementMapResponse;

    usedUsageCounters.map(usageCounter => {
      //usageCounter limitGroupCode here will also include awardGroupCounter
      if (entitlementMap.has(usageCounter.limitGroupCode)) {
        const entitlementResult = entitlementMap.get(
          usageCounter.limitGroupCode
        );

        if (
          entitlementResult &&
          entitlementResult.entitlements &&
          entitlementResult.entitlements[0] &&
          entitlementResult.counters &&
          entitlementResult.counters[0]
        ) {
          //only get the first one as we are dealing with only one entitlement code at a time
          const { usedQuota, quota } = entitlementResult.entitlements[0];

          //check whether the updates failed or not
          //whether the fail is due to quota limit
          const { recorded, reason } = entitlementResult.counters[0];

          let isQuotaExceeded = false;
          if (
            !recorded &&
            reason &&
            reason.type == EntitlementErrorReasonType.QUOTA_LIMIT
          ) {
            isQuotaExceeded = true;
          }

          usageCounter.monthlyAccumulationTransaction = usedQuota as number;
          usageCounter.quota = parseInt(quota.toString());
          usageCounter.isQuotaExceeded = isQuotaExceeded;
        }
      }
    });
    return usedUsageCounters;
  }

  return usageCounters;
};

const processReversalEntitlementCounterUsageOnTransactionConfirmation = (
  input: IConfirmTransactionInput
) => {
  switch (input.responseCode) {
    case ConfirmTransactionStatus.REQUEST_TIMEOUT:
    case ConfirmTransactionStatus.ERROR_EXPECTED:
    case ConfirmTransactionStatus.ERROR_UNEXPECTED:
      processReversalEntitlementCounterUsageBasedOnContext();
      break;

    default: {
      //do nothing
    }
  }
};

const getEntitlementByAwardGroupCounter = (
  usageCounters?: UsageCounter[],
  awardGroupCounter?: string
): UsageCounter => {
  let defaultEntitlement = {
    monthlyAccumulationTransaction: 0,
    quota: undefined,
    limitGroupCode: undefined,
    dailyAccumulationAmount: 0
  };

  const entitlements = usageCounters?.filter(
    usageCounter => usageCounter.limitGroupCode === awardGroupCounter
  );

  return !_.isEmpty(entitlements) &&
    !_.isUndefined(entitlements) &&
    entitlements
    ? Object.assign({}, ...entitlements)
    : defaultEntitlement;
};

const getEntitlementsMap = async (
  customerId: string,
  entitlementCodes: string[]
): Promise<Map<string, IEntitlementResponse>> => {
  let entitlements: IEntitlementResponse[];
  try {
    entitlements = await entitlementRepository.getEntitlements(
      customerId,
      entitlementCodes
    );
  } catch {
    // We don't want to throw any error if there are no custom limits available
    // We just return without doing anything
    return new Map();
  }

  return new Map(
    entitlements.map(entitlement => [entitlement.entitlement, entitlement])
  );
};

const entitlementService = wrapLogs({
  processReversalEntitlementCounterUsageBasedOnContext,
  processReversalEntitlementCounterUsageOnTransactionConfirmation,
  mapEntitlementUsageCounterByLimitGroupCode,
  getEntitlementByAwardGroupCounter,
  retrieveCustomerEntitlementRecommendedServicePreview,
  processCustomerEntitlementConsumption,
  getEntitlementsMap
});

export default entitlementService;
