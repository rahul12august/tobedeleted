import { UsageCounter } from '../award/award.type';
import { ITransactionCodeMapping } from '../configuration/configuration.type';
import { BasicFeeMapping } from '../fee/fee.type';

export interface IGetEntitlementRequest {
  entitlementCodes: string[];
}

export interface IEntitlementResponse {
  entitlement: string;
  type: string;
  dataType: string;
  description: string;
  quota: number | boolean | string;
  comments: string;
  counterCode: string;
  usedQuota?: number;
  value?: string;
}

export interface IReason {
  type: string;
  description: string;
}
export interface ICounterResponse {
  id: string;
  recorded: boolean;
  reason?: IReason;
}

export interface ICounterUsageRequest {
  id: string;
  counterCode: string;
  usedQuota: number;
}

export interface ICounterRevertRequest {
  id: string;
  counterCode: string;
}

export interface IUpdateUsageCounterEntitlementRequest {
  customerId: string;
  entitlementCodes: string[];
  counterUsages: ICounterUsageRequest[];
}

export interface ICounterRevertEntitlementRequest {
  customerId: string;
  entitlementCodes?: string[];
  counterReverts: ICounterRevertRequest[];
}

export interface IUpdateUsageCounterEntitlementApiParams {
  entitlementCodes: string[];
  counterUsages: ICounterUsageRequest[];
}

export interface IUpdateUsageCounterEntitlementResponse {
  entitlements: IEntitlementResponse[];
  counters: ICounterResponse[];
}

export interface ICounterRevertResponse {
  id: string;
  recorded: boolean;
  revertedValue?: number;
  reason?: IReason;
}

export interface ICounterRevertEntitlementResponse {
  entitlements: IEntitlementResponse[];
  counterReverts: ICounterRevertResponse[];
}

export interface EntitlementQuota {
  quota: number;
  usedQuota: number;
}

export interface EntitlementContext {
  entitlementCode?: string;
  counterCode?: string;
  entitlementCodeRefId?: string;
  customerId?: string;
  responseCode?: string;
  startTimeTracker?: Date;
}

export interface ICustomerEntitlementByTransCodeRequest {
  transactionCodes?: ITransactionCodeMapping[];
  customerId?: string;
  transactionId?: string;
  paymentServiceType?: string;
}

export interface ICustomerEntitlementByTransCodeResponse {
  updatedEntitlementMapResponse: Map<
    string,
    IUpdateUsageCounterEntitlementResponse
  >;
}

export interface ICounterUsageByTransCodeRequest {
  transactionCodes?: ITransactionCodeMapping[];
  usageCounters: UsageCounter[];
  entitlementResultResponse?: ICustomerEntitlementByTransCodeResponse | null;
}

export interface IEntitlement {
  limitGroupCode: string;
  awardGroupCounter: string;
  entitlementCode: string;
  counterCode: string;
}

export interface IValidatedFeeRuleObject {
  usedQuota: number;
  quota: number;
  freeFeeCode: BasicFeeMapping[] | undefined;
  paidFeeCode: BasicFeeMapping[] | undefined;
}
