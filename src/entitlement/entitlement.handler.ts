import _ from 'lodash';
import { UsageCounter } from '../award/award.type';
import { ERROR_CODE } from '../common/errors';

import { ITransactionCodeMapping } from '../configuration/configuration.type';

import { CustomerEntitlementHandler } from '../customerEntitlementManager/customerEntitlement.interface';
import { TransferAppError } from '../errors/AppError';

import { BasicFeeMapping, FeeRuleInfo } from '../fee/fee.type';

import logger from '../logger';
import {
  CustomerEntitlementMode,
  TransferFailureReasonActor
} from '../transaction/transaction.enum';
import { EntitlementErrorReasonType } from './entitlement.enum';
import entitlementHelper from './entitlement.helper';
import {
  ICounterResponse,
  ICounterUsageByTransCodeRequest,
  ICustomerEntitlementByTransCodeRequest,
  ICustomerEntitlementByTransCodeResponse,
  IValidatedFeeRuleObject
} from './entitlement.interface';
import entitlementService from './entitlement.service';

export class EntitlementHandler implements CustomerEntitlementHandler {
  extractAvailableLimitGroupCodesFromTransactionCodeInfo = (
    transactionCodes: ITransactionCodeMapping[]
  ) => {
    const filteredLimitGroupCodes = transactionCodes.filter(
      tc =>
        !_.isEmpty(tc.transactionCodeInfo.limitGroupCode) &&
        !_.isUndefined(tc.transactionCodeInfo.limitGroupCode)
    );

    const tcValidLimitGroupCodes = new Set<string>();
    for (const tc of filteredLimitGroupCodes) {
      const { limitGroupCode, awardGroupCounter } = tc.transactionCodeInfo;
      if (!_.isUndefined(limitGroupCode) && !_.isEmpty(limitGroupCode)) {
        tcValidLimitGroupCodes.add(limitGroupCode);
      }
      if (!_.isUndefined(awardGroupCounter) && !_.isEmpty(awardGroupCounter)) {
        tcValidLimitGroupCodes.add(awardGroupCounter);
      }
    }

    return tcValidLimitGroupCodes;
  };

  async generateMissingUsageCounterBasedOnTransactionCodes(
    params: ICounterUsageByTransCodeRequest
  ): Promise<UsageCounter[]> {
    const {
      transactionCodes,
      usageCounters,
      entitlementResultResponse
    } = params;

    if (
      _.isEmpty(transactionCodes) ||
      _.isUndefined(transactionCodes) ||
      _.isEmpty(entitlementResultResponse) ||
      _.isUndefined(entitlementResultResponse) ||
      entitlementResultResponse == null
    ) {
      logger.info(
        `[EntitlementHandler] generateMissingUsageCounterBasedOnTransactionCodes result1 [as-is]: ${JSON.stringify(
          usageCounters
        )}`
      );

      //return as is
      return usageCounters;
    }

    //first extract all limitGroupCodes from the transactionCodes
    const tcValidLimitGroupCodes = this.extractAvailableLimitGroupCodesFromTransactionCodeInfo(
      transactionCodes
    );

    if (_.isEmpty(tcValidLimitGroupCodes)) {
      logger.info(
        `[EntitlementHandler] generateMissingUsageCounterBasedOnTransactionCodes result2 [as-is]: ${JSON.stringify(
          usageCounters
        )}`
      );

      //return as is
      return usageCounters;
    }

    //extract limitGroupCodes from existing usageCounters
    const existingUsageCountersLimitGroupCodes = usageCounters.map(
      usageCounter => usageCounter.limitGroupCode
    );

    //getting the difference
    const actualMissingLimitGroupCodes = _.difference(
      [...tcValidLimitGroupCodes],
      existingUsageCountersLimitGroupCodes
    );

    //from the entitlement result, cross check which one is the missing usgecounter
    const entitlementMap =
      entitlementResultResponse.updatedEntitlementMapResponse;

    for (const limitGroupCode of actualMissingLimitGroupCodes) {
      const limitGroupCodeEntitlementResult = entitlementMap.get(
        limitGroupCode
      );

      // when entitlementMap does not contain result for missing limit group code
      if (_.isUndefined(limitGroupCodeEntitlementResult)) {
        throw new TransferAppError(
          `Limit group code: ${limitGroupCode} not found in the entitlement!`,
          TransferFailureReasonActor.MS_ENTITLEMENT,
          ERROR_CODE.LIMIT_GROUP_CODE_NOT_FOUND_IN_ENTITLEMENT
        );
      }

      const {
        usedQuota,
        quota
      } = limitGroupCodeEntitlementResult.entitlements[0];

      const isQuotaExceeded = this.validateIsExceededQuota(
        limitGroupCodeEntitlementResult.counters
      );

      //create usageCounter object
      const newUsageCounter: UsageCounter = {
        limitGroupCode: limitGroupCode,
        dailyAccumulationAmount: 0, //this should not be defaulted
        monthlyAccumulationTransaction: usedQuota as number, //this should not be defaulted
        quota: quota as number,
        isQuotaExceeded: isQuotaExceeded
      };

      usageCounters.push(newUsageCounter);
    }

    logger.info(
      `[EntitlementHandler] generateMissingUsageCounterBasedOnTransactionCodes result: ${JSON.stringify(
        usageCounters
      )}`
    );

    return usageCounters;
  }

  async preProcessCustomerEntitlementBasedOnTransactionCodes(
    params: ICustomerEntitlementByTransCodeRequest
  ): Promise<ICustomerEntitlementByTransCodeResponse | null> {
    let mode = CustomerEntitlementMode.CONSUME;

    if (_.isEmpty(params.transactionId)) {
      mode = CustomerEntitlementMode.PREVIEW;
    }

    logger.info(
      `[EntitlementHandler] preProcessCustomerEntitlementBasedOnTransactionCodes ${mode} MODE: 
      ${JSON.stringify(params)}
      `
    );

    switch (mode) {
      case CustomerEntitlementMode.PREVIEW:
        return entitlementService.retrieveCustomerEntitlementRecommendedServicePreview(
          params
        );
      case CustomerEntitlementMode.CONSUME:
        return entitlementService.processCustomerEntitlementConsumption(params);
      default:
        return null;
    }
  }

  validateIsExceededQuota = (counterResponse: ICounterResponse[]): boolean => {
    if (!_.isEmpty(counterResponse)) {
      logger.info(`[EntitlementHandler] validateIsExceededQuota: 
        recorded: ${counterResponse[0].recorded}
        reason: ${counterResponse[0].reason}
      `);
      //assume we are only dealing with one response at a time
      const { recorded, reason } = counterResponse[0];
      if (recorded) {
        //successfully updated
        return false;
      } else {
        return !!(
          reason && reason.type === EntitlementErrorReasonType.QUOTA_LIMIT
        );
      }
    } else {
      throw new TransferAppError(
        `Failed to retrieve entitlement quota info!`,
        TransferFailureReasonActor.MS_ENTITLEMENT,
        ERROR_CODE.FAILED_TO_RETRIEVE_ENTITLEMENT_QUOTA_INFO
      );
    }
  };

  mapEntitlementUsageCounterByLimitGroupCode(
    usageCounters: UsageCounter[],
    entitlementResultResponse: ICustomerEntitlementByTransCodeResponse | null
  ): UsageCounter[] {
    logger.info(
      `[EntitlementHandler] mapEntitlementUsageCounterByLimitGroupCode: 
        usageCounters ${JSON.stringify(usageCounters)}
        entitlementResultResponse ${JSON.stringify(entitlementResultResponse)}`
    );

    if (entitlementResultResponse != null) {
      const entitlementMap =
        entitlementResultResponse.updatedEntitlementMapResponse;

      const mappedLimitGroupCodes: string[] = [];

      //reuse from the existing data and then mapped to usagecounter data accordingly
      usageCounters.map(usageCounter => {
        //usageCounter limitGroupCode here will also include awardGroupCounter
        if (entitlementMap.has(usageCounter.limitGroupCode)) {
          mappedLimitGroupCodes.push(usageCounter.limitGroupCode);

          const entitlementResult = entitlementMap.get(
            usageCounter.limitGroupCode
          );

          if (
            entitlementResult &&
            entitlementResult.entitlements &&
            entitlementResult.entitlements[0]
          ) {
            logger.info(
              `[EntitlementHandler] mapEntitlementUsageCounterByLimitGroupCode: MAPPING usageCounters: ${usageCounter.limitGroupCode}`
            );

            //only get the first one as we are dealing with only one entitlement code at a time
            const { usedQuota, quota } = entitlementResult.entitlements[0];

            const isQuotaExceeded = this.validateIsExceededQuota(
              entitlementResult.counters
            );

            usageCounter.monthlyAccumulationTransaction = usedQuota as number;
            usageCounter.quota = parseInt(quota.toString());
            usageCounter.isQuotaExceeded = isQuotaExceeded;

            logger.info(
              `[EntitlementHandler] mapEntitlementUsageCounterByLimitGroupCode: MAPPED usageCounters:
                limitGroupCode: [${usageCounter.limitGroupCode}]
                dailyAccumulationAmount: [${usageCounter.dailyAccumulationAmount}]
                monthlyAccumulationTransaction: [${usageCounter.monthlyAccumulationTransaction}]
                quota: [${usageCounter.quota}]
                isQuotaExceeded: [${usageCounter.isQuotaExceeded}]
              `
            );
          }
        }
      });

      logger.info(
        `[EntitlementHandler] mapEntitlementUsageCounterByLimitGroupCode: usageCounters ${JSON.stringify(
          usageCounters
        )}`
      );
    }

    return usageCounters;
  }

  validateFeeRuleObject(feeRuleObject: FeeRuleInfo): IValidatedFeeRuleObject {
    logger.info(`[EntitlementHandler] validateFeeRuleObject: 
      monthlyNoTransaction: ${feeRuleObject.monthlyNoTransaction}
      quota: ${feeRuleObject.quota}
      isQuotaExceeded: ${feeRuleObject.isQuotaExceeded}
    `);

    if (
      typeof feeRuleObject.monthlyNoTransaction !== 'number' ||
      typeof feeRuleObject.quota !== 'number' ||
      _.isUndefined(feeRuleObject.isQuotaExceeded)
    ) {
      const detail = `monthlyNoTransaction or counter code not found!`;
      logger.error(`[EntitlementHandler] validateFeeRuleObject: ${detail}`);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.MS_ENTITLEMENT,
        ERROR_CODE.TRANSFER_FEE_RULE_INVALID_MANDATORY_FIELDS
      );
    }

    logger.info(
      `[EntitlementHandler] getFeeRuleBasedOnMonthlyNoTransaction: 
        Counter code: [${feeRuleObject.counterCode}]
        monthlyNoTransaction: [${feeRuleObject.monthlyNoTransaction}]
        quota limit exceeded: [${feeRuleObject.isQuotaExceeded}]
      `
    );

    //in most cases
    //basicFeeCode1 is for free transaction
    //basicFeeCode2 is for paid transaction

    return {
      usedQuota: feeRuleObject.monthlyNoTransaction,
      quota: feeRuleObject.quota,
      freeFeeCode: feeRuleObject.basicFeeCode1,
      paidFeeCode: feeRuleObject.basicFeeCode2
    };
  }

  async determineBasicFeeMapping(
    feeRuleObject: FeeRuleInfo
  ): Promise<BasicFeeMapping[] | undefined> {
    const {
      usedQuota,
      quota,
      freeFeeCode,
      paidFeeCode
    } = await this.validateFeeRuleObject(feeRuleObject);

    logger.info(`[EntitlementHandler] determineBasicFeeMapping: 
      quota: [${quota}]
      usedQuota: [${usedQuota}]
      isQuotaExceeded: [${feeRuleObject.isQuotaExceeded}]
    `);

    if (usedQuota === quota) {
      //if usedQuota == quota, check by the flag whether the quota is exceeded or not
      return feeRuleObject.isQuotaExceeded ? paidFeeCode : freeFeeCode;
    } else if (usedQuota < quota) {
      //if usedQuota < quota
      return freeFeeCode;
    } else {
      return paidFeeCode;
    }
  }

  processFeeRuleInfo(
    currentFeeRuleInfo: FeeRuleInfo,
    feeRuleType: string | undefined,
    awardGroupCounter: string | undefined,
    usageCounters: UsageCounter[] | undefined
  ): FeeRuleInfo {
    if (
      _.isUndefined(feeRuleType) ||
      _.isUndefined(awardGroupCounter) ||
      _.isUndefined(usageCounters)
    ) {
      throw new TransferAppError(
        `One of following is undefined: feeRuleType, awardGroupCounter, usageCounters!`,
        TransferFailureReasonActor.MS_ENTITLEMENT,
        ERROR_CODE.PROCESS_FEE_RULE_INFO_ERROR
      );
    }

    logger.info(`[EntitlementHandler] processFeeRuleInfo: 
      awardGroupCounter: [${awardGroupCounter}]
      feeRuleType: [${feeRuleType}]
      usageCounters: [${JSON.stringify(usageCounters)}]
    `);

    const isIncludedWithinFilteredLimitGroupCodes = entitlementHelper
      .getFilteredLimitGroupCodes()
      .includes(awardGroupCounter);

    const isIncludedWithinFilteredFeeRuleTypes = entitlementHelper
      .getFilteredFeeRuleType()
      .includes(feeRuleType);

    const toModifyEntitlementValues =
      isIncludedWithinFilteredLimitGroupCodes &&
      isIncludedWithinFilteredFeeRuleTypes;

    logger.info(`[EntitlementHandler] processFeeRuleInfo: 
        awardGroupCounter: [${awardGroupCounter}]
        feeRuleType: [${feeRuleType}]
        isIncludedWithinFilteredLimitGroupCodes: [${isIncludedWithinFilteredLimitGroupCodes}]
        isIncludedWithinFilteredFeeRuleTypes: [${isIncludedWithinFilteredFeeRuleTypes}]
        toModifyEntitlementValues: [${toModifyEntitlementValues}]
      `);

    if (toModifyEntitlementValues) {
      const modifiedFeeRuleInfo = { ...currentFeeRuleInfo };

      const usageCounter = entitlementService.getEntitlementByAwardGroupCounter(
        usageCounters,
        awardGroupCounter
      );

      const {
        monthlyAccumulationTransaction,
        quota,
        isQuotaExceeded
      } = usageCounter;

      //overwrite the monthlynotransaction with used quota
      modifiedFeeRuleInfo.monthlyNoTransaction = monthlyAccumulationTransaction;

      //added quota
      modifiedFeeRuleInfo.quota = quota;

      //added flag indicating whether quota exceeded or not
      modifiedFeeRuleInfo.isQuotaExceeded = isQuotaExceeded ?? false;

      logger.info(`[EntitlementHandler] processFeeRuleInfo result1: 
          monthlyNoTransaction/usedQuota: [${modifiedFeeRuleInfo.monthlyNoTransaction}] 
          quota: [${modifiedFeeRuleInfo.quota}] 
          isQuotaExceeded [${modifiedFeeRuleInfo.isQuotaExceeded}] 
        `);

      return modifiedFeeRuleInfo;
    }

    logger.info(`[EntitlementHandler] processFeeRuleInfo result2: 
          monthlyNoTransaction/usedQuota: [${currentFeeRuleInfo.monthlyNoTransaction}] 
          quota: [${currentFeeRuleInfo.quota}] 
          isQuotaExceeded [${currentFeeRuleInfo.isQuotaExceeded}] 
        `);

    return currentFeeRuleInfo;
  }
}
