export enum AwardLimitGroupCodes {
  BONUS_TRANSFER = 'Bonus_Transfer',
  BONUS_LOCAL_CASH_WITHDRAWAL = 'Bonus_Local_CashWithdrawal',
  BONUS_OVERSEAS_CASH_WITHDRAWAL = 'Bonus_Overseas_CashWithdrawal'
}

export enum EntitlementCode {
  BONUS_TRANSFER = 'count.free.transfer.out',
  BONUS_LOCAL_CASH_WITHDRAWAL = 'count.free.atm_withdrawal'
}

export enum EntitlementErrorReasonType {
  QUOTA_LIMIT = 'quotaLimit'
}
