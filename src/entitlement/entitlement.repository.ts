import { IErrorDetail } from '@dk/module-common/dist/error/error.interface';
import _, { get } from 'lodash';
import { ERROR_CODE } from '../common/errors';
import { TransferAppError } from '../errors/AppError';
import logger, { wrapLogs } from '../logger';
import { ENTITLEMENT_API_ENDPOINT } from './entitlement.constant';
import { EntitlementErrorReasonType } from './entitlement.enum';
import entitlementHelper from './entitlement.helper';
import {
  ICounterRevertEntitlementRequest,
  ICounterRevertEntitlementResponse,
  IEntitlementResponse,
  IUpdateUsageCounterEntitlementRequest,
  IUpdateUsageCounterEntitlementResponse
} from './entitlement.interface';

import httpClient from './httpClient';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const getEntitlements = async (
  customerId: string,
  entitlementCodes: string[]
): Promise<IEntitlementResponse[] | []> => {
  try {
    logger.info(
      `getEntitlements: Retrieving entitlements for customerId: ${customerId}.`
    );
    const endpoint = ENTITLEMENT_API_ENDPOINT(customerId);
    const payload = entitlementHelper.constructGetUsageCounterPayload(
      entitlementCodes
    );
    const result = await httpClient.post(endpoint, payload);

    if (_.isEmpty(result)) {
      const detail = `Failed to retrieve entitlement for ${entitlementCodes.toString()}!`;
      logger.error(`getEntitlements: ${detail}`);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.MS_ENTITLEMENT,
        ERROR_CODE.FAILED_TO_RETRIEVE_ENTITLEMENT
      );
    }

    return get(result, ['data', 'entitlements'], []);
  } catch (error) {
    let detail = `Error getting entitlements for customerID: ${customerId}, ${error.message}!`;
    logger.error(`getEntitlements: ${detail}`);
    const errorDetail: IErrorDetail[] = [];
    errorDetail.push({
      message: (error && error.message) || '',
      key: error.code,
      code: (error && error.status) || ''
    });

    let actor =
      error.status === 400 || error.status === 404
        ? TransferFailureReasonActor.MS_TRANSFER
        : TransferFailureReasonActor.MS_ENTITLEMENT;

    if (error instanceof TransferAppError) {
      detail = error.detail;
      actor = error.actor;
    }

    throw new TransferAppError(detail, actor, error.code, errorDetail);
  }
};

const updateEntitlementUsageCounters = async (
  params: IUpdateUsageCounterEntitlementRequest
): Promise<IUpdateUsageCounterEntitlementResponse> => {
  const { customerId, entitlementCodes, counterUsages } = params;

  logger.info(
    `updateEntitlementUsageCounters [ms-entitlement] for customerId: ${customerId}.`
  );

  const endpoint = ENTITLEMENT_API_ENDPOINT(customerId);

  const result = await httpClient.post(endpoint, {
    entitlementCodes,
    counterUsages
  });

  if (
    !result.data.hasOwnProperty('entitlements') ||
    (!result.data.counters[0].recorded &&
      result.data.counters[0].reason.type !==
        EntitlementErrorReasonType.QUOTA_LIMIT)
  ) {
    const infos = counterUsages
      .map(
        counterUsage =>
          `[id: ${counterUsage.id} AND counterCode: ${counterUsage.counterCode} AND usedQuota: ${counterUsage.usedQuota}]`
      )
      .join(', ');

    const detail = `Failed to update entitlement for ${infos}!`;
    logger.error(`updateEntitlementUsageCounters: ${detail}`);
    throw new TransferAppError(
      detail,
      result.status === 400 || result.status === 404
        ? TransferFailureReasonActor.MS_TRANSFER
        : TransferFailureReasonActor.MS_ENTITLEMENT,
      ERROR_CODE.FAILED_TO_UPDATE_ENTITLEMENT
    );
  }

  return result?.data;
};

const revertEntitlementUsageCounters = async (
  params: ICounterRevertEntitlementRequest
): Promise<ICounterRevertEntitlementResponse> => {
  const { customerId, entitlementCodes, counterReverts } = params;

  logger.info(
    `revertEntitlementUsageCounters [ms-entitlement] for customerId: ${customerId}.`
  );

  const endpoint = ENTITLEMENT_API_ENDPOINT(customerId);

  const result = await httpClient.post(endpoint, {
    entitlementCodes,
    counterReverts
  });

  if (!result) {
    const infos = counterReverts
      .map(
        counterRevert =>
          `[id: ${counterRevert.id} AND counterCode: ${counterRevert.counterCode}]`
      )
      .join(', ');

    const detail = `Failed to revert entitlement for ${infos}!`;
    logger.error(`revertEntitlementUsageCounters: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.FAILED_TO_REVERT_ENTITLEMENT
    );
  }

  return result?.data;
};

const entitlementRepository = wrapLogs({
  getEntitlements,
  updateEntitlementUsageCounters,
  revertEntitlementUsageCounters
});

export default entitlementRepository;
