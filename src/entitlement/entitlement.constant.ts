import { PaymentServiceTypeEnum } from '@dk/module-common';

export const ENTITLEMENT_API_ENDPOINT = (customerId: string) => {
  return `/private/${customerId}/counter`;
};

export const ENTITLEMENT_EXCLUDED_PAYMENT_SERVICE_TYPES: string[] = [
  PaymentServiceTypeEnum.OFFER,
  PaymentServiceTypeEnum.PAYROLL,
  PaymentServiceTypeEnum.CASHBACK,
  PaymentServiceTypeEnum.BONUS_INTEREST,
  PaymentServiceTypeEnum.THIRD_PARTY_BALANCE_INQUIRY,
  PaymentServiceTypeEnum.INTERNATIONAL_BALANCE_INQUIRY,
  PaymentServiceTypeEnum.THIRD_PARTY_JAGO_TO_JAGO_CREDIT
];
