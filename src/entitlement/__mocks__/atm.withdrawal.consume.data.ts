import uuid from 'uuid';

import {
  IUpdateUsageCounterEntitlementRequest,
  IUpdateUsageCounterEntitlementResponse
} from '../entitlement.interface';
import {
  MockEntitlementCode,
  MockEntitlementCounterCode
} from './entitlement.enum.mock';

//happy scenario
//quota is available
//and request for usedQuota is valid (within boundary and > 0)

export const scenario1 = (customerId: string) => {
  //assumption
  const existingState = [
    {
      entitlementCode: MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
      counterCode:
        MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
      quota: 25,
      usedQuota: 0
    }
  ];

  const usedUuid = uuid.v4();
  //request data
  const requestData: IUpdateUsageCounterEntitlementRequest = {
    customerId: customerId,
    entitlementCodes: [MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL],
    counterUsages: [
      {
        id: usedUuid,
        counterCode:
          MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
        usedQuota: 1
      }
    ]
  };

  const responseData: IUpdateUsageCounterEntitlementResponse = {
    entitlements: [
      {
        entitlement: MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
        type: 'MONTHLY_COUNTER',
        dataType: 'integer',
        description: 'sample description',
        quota: 25 - 1,
        comments: 'sample comments',
        usedQuota: 0 + 1,
        counterCode:
          MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL
      }
    ],
    counters: [
      {
        id: usedUuid,
        recorded: true
      }
    ]
  };

  return {
    existingState,
    requestData,
    responseData
  };
};

//scenario 2
//replicated input, in which the id is the same
export const scenario2 = (customerId: string) => {
  //assumption
  const existingState = [
    {
      entitlementCode: MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
      counterCode:
        MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
      quota: 25,
      usedQuota: 0
    }
  ];

  const duplicatedId = uuid.v4();
  //request data
  const requestData: IUpdateUsageCounterEntitlementRequest = {
    customerId: customerId,
    entitlementCodes: [MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL],
    counterUsages: [
      {
        id: duplicatedId,
        counterCode:
          MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
        usedQuota: 1
      },
      {
        id: duplicatedId,
        counterCode:
          MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
        usedQuota: 2
      }
    ]
  };

  const responseData: IUpdateUsageCounterEntitlementResponse = {
    entitlements: [
      {
        entitlement: MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
        type: 'MONTHLY_COUNTER',
        dataType: 'integer',
        description: 'sample description',
        quota: 25 - 1,
        comments: 'sample comments',
        usedQuota: 0 + 1,
        counterCode:
          MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL
      }
    ],
    counters: [
      {
        id: duplicatedId,
        recorded: true
      },
      {
        id: duplicatedId,
        recorded: false,
        reason: {
          type: 'IdNotUnique',
          description:
            'counter usage id is not unique and has been recorded previously'
        }
      }
    ]
  };

  return {
    existingState,
    requestData,
    responseData
  };
};

//negative scenario
//quota is available
//request to usedQuota is bigger than existing quota
//expected output, failed, in which there is insufficient quota
export const scenario3 = (customerId: string) => {
  //assumption
  const existingState = [
    {
      entitlementCode: MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
      counterCode:
        MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
      quota: 25,
      usedQuota: 0
    }
  ];

  const uniqueId = uuid.v4();
  //request data
  const requestData: IUpdateUsageCounterEntitlementRequest = {
    customerId: customerId,
    entitlementCodes: [MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL],
    counterUsages: [
      {
        id: uniqueId,
        counterCode:
          MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
        usedQuota: 26
      }
    ]
  };

  const responseData: IUpdateUsageCounterEntitlementResponse = {
    entitlements: [
      {
        entitlement: MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
        type: 'MONTHLY_COUNTER',
        dataType: 'integer',
        description: 'sample description',
        quota: 25,
        comments: 'sample comments',
        usedQuota: 0,
        counterCode:
          MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL
      }
    ],
    counters: [
      {
        id: uniqueId,
        recorded: false,
        reason: {
          type: 'insufficientQuota',
          description: 'the requested user quota exceeded the existing quota'
        }
      }
    ]
  };

  return {
    existingState,
    requestData,
    responseData
  };
};

//negative scenario
//quota is not available
export const scenario4 = (customerId: string) => {
  //assumption
  const existingState = [
    {
      entitlementCode: MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
      counterCode:
        MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
      quota: 0,
      usedQuota: 25
    }
  ];

  const uniqueId = uuid.v4();
  //request data
  const requestData: IUpdateUsageCounterEntitlementRequest = {
    customerId: customerId,
    entitlementCodes: [MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL],
    counterUsages: [
      {
        id: uniqueId,
        counterCode:
          MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
        usedQuota: 1
      }
    ]
  };

  const responseData: IUpdateUsageCounterEntitlementResponse = {
    entitlements: [
      {
        entitlement: MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
        type: 'MONTHLY_COUNTER',
        dataType: 'integer',
        description: 'sample description',
        quota: 0,
        comments: 'sample comments',
        usedQuota: 25,
        counterCode:
          MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL
      }
    ],
    counters: [
      {
        id: uniqueId,
        recorded: false,
        reason: {
          type: 'zeroBalancedQuota',
          description: 'there is no more quota available'
        }
      }
    ]
  };

  return {
    existingState,
    requestData,
    responseData
  };
};

//negative scenario
//usedQuota is negative value
export const scenario5 = (customerId: string) => {
  //assumption
  const existingState = [
    {
      entitlementCode: MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
      counterCode:
        MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
      quota: 25,
      usedQuota: 0
    }
  ];

  const uniqueId = uuid.v4();
  //request data
  const requestData: IUpdateUsageCounterEntitlementRequest = {
    customerId: customerId,
    entitlementCodes: [MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL],
    counterUsages: [
      {
        id: uniqueId,
        counterCode:
          MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL,
        usedQuota: -1
      }
    ]
  };

  const responseData: IUpdateUsageCounterEntitlementResponse = {
    entitlements: [
      {
        entitlement: MockEntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL,
        type: 'MONTHLY_COUNTER',
        dataType: 'integer',
        description: 'sample description',
        quota: 25,
        comments: 'sample comments',
        usedQuota: 0,
        counterCode:
          MockEntitlementCounterCode.COUNTER_BONUS_LOCAL_CASH_WITHDRAWAL
      }
    ],
    counters: [
      {
        id: uniqueId,
        recorded: false,
        reason: {
          type: 'invalidUsedQuota',
          description: 'used quota need to be positive number'
        }
      }
    ]
  };

  return {
    existingState,
    requestData,
    responseData
  };
};
