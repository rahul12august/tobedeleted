import { PaymentServiceTypeEnum } from '@dk/module-common';
import { BankNetworkEnum } from '@dk/module-common/dist/enum/bankNetwork.enum';
import { Matchers } from '@pact-foundation/pact';
import { UsageCounter } from '../../award/award.type';
import { EntitlementErrorReasonType } from '../entitlement.enum';
import {
  EntitlementContext,
  ICounterRevertEntitlementResponse,
  ICustomerEntitlementByTransCodeRequest,
  ICustomerEntitlementByTransCodeResponse,
  IUpdateUsageCounterEntitlementResponse
} from '../entitlement.interface';

export const getFreeAtmWithdrawalResponse = () => {
  return {
    entitlements: [
      {
        entitlement: Matchers.string(),
        type: Matchers.string(),
        dataType: Matchers.string(),
        description: Matchers.string(),
        quota: Matchers.integer(),
        usedQuota: Matchers.integer(),
        counterCode: Matchers.string(),
        comments: Matchers.string()
      }
    ]
  };
};

export const getFreeAtmWithdrawalWithError = (msg: string, code: string) => {
  return {
    error: {
      message: Matchers.string(msg),
      code: Matchers.string(code)
    }
  };
};

export const mockEntitlementInfoContext = (): EntitlementContext[] => {
  return [
    {
      entitlementCode: 'entitlementCode',
      counterCode: 'counterCode',
      entitlementCodeRefId: 'entitlementCodeRefId',
      customerId: 'customerId',
      responseCode: 'responseCode',
      startTimeTracker: new Date()
    }
  ];
};

export const mockEntitlementReversalResponse = (): ICounterRevertEntitlementResponse => {
  return {
    entitlements: [],
    counterReverts: [
      {
        id: 'id',
        recorded: true,
        revertedValue: 1
      }
    ]
  };
};

export const mockUsageCounterData = (): UsageCounter[] => {
  return [
    {
      limitGroupCode: 'limitGroupCode',
      dailyAccumulationAmount: 10000,
      monthlyAccumulationTransaction: 100000,
      quota: 6
    }
  ];
};

export const mockEntitlementResultResponse = (): ICustomerEntitlementByTransCodeResponse => {
  const data = new Map<string, IUpdateUsageCounterEntitlementResponse>();
  data.set('limitGroupCode', {
    entitlements: [
      {
        entitlement: 'entitlement',
        type: 'type',
        dataType: 'integer',
        description: 'description',
        quota: 6,
        comments: 'comments',
        counterCode: 'counterCode',
        usedQuota: 1
      }
    ],
    counters: [
      {
        id: 'id',
        recorded: true,
        reason: {
          type: 'type',
          description: 'description'
        }
      }
    ]
  });
  return { updatedEntitlementMapResponse: data };
};

export const mockEntitlementResultResponseExceedLimitation = (): ICustomerEntitlementByTransCodeResponse => {
  const data = new Map<string, IUpdateUsageCounterEntitlementResponse>();
  data.set('limitGroupCode', {
    entitlements: [
      {
        entitlement: 'entitlement',
        type: 'type',
        dataType: 'integer',
        description: 'description',
        quota: 6,
        comments: 'comments',
        counterCode: 'counterCode',
        usedQuota: 6
      }
    ],
    counters: [
      {
        id: 'id',
        recorded: false,
        reason: {
          type: EntitlementErrorReasonType.QUOTA_LIMIT,
          description: 'description'
        }
      }
    ]
  });
  return { updatedEntitlementMapResponse: data };
};

export const mockCustomerEntitlementByTransCodeRequest = (): ICustomerEntitlementByTransCodeRequest => {
  return {
    transactionCodes: [
      {
        interchange: BankNetworkEnum.ALTO,
        transactionCode: 'transactionCode',
        transactionCodeInfo: {
          code: 'code',
          limitGroupCode: 'limitGroupCode',
          feeRules: 'feeRules',
          minAmountPerTx: 10000,
          maxAmountPerTx: 100000,
          defaultCategoryCode: 'defaultCategoryCode',
          channel: 'channel',
          awardGroupCounter: 'awardGroupCounter',
          entitlementCode: 'entitlementCode',
          counterCode: 'counterCode'
        }
      }
    ],
    customerId: 'customerId',
    transactionId: 'transactionId',
    paymentServiceType: PaymentServiceTypeEnum.TRANSFER
  };
};

export const mockUpdateUsageCounterEntitlementResponse = (): IUpdateUsageCounterEntitlementResponse => {
  return {
    entitlements: [
      {
        entitlement: 'entitlement',
        type: 'type',
        dataType: 'dataType',
        description: 'description',
        quota: 5,
        comments: 'comments',
        counterCode: 'counterCode',
        usedQuota: 1,
        value: 'value'
      }
    ],
    counters: [
      {
        id: 'id',
        recorded: true
      }
    ]
  };
};
