import { ENTITLEMENT_EXCLUDED_PAYMENT_SERVICE_TYPES } from '../entitlement.constant';

export const mockExcludedEntitlementPaymentServiceTypes = () =>
  ENTITLEMENT_EXCLUDED_PAYMENT_SERVICE_TYPES.map(paymentServiceType => {
    return {
      expected: null,
      params: {
        customerId: 'cust1234',
        transactionId: 'transId1',
        transactionCodes: [],
        paymentServiceType: paymentServiceType
      }
    };
  });
