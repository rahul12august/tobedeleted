import httpClient from '../httpClient';
import entitlementRepository from '../entitlement.repository';
import { TransferAppError } from '../../errors/AppError';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';
import { ERROR_CODE } from '../../common/errors';

jest.mock('../httpClient');

describe('entitlement.repository', () => {
  it('should be getEntitlements successfully', async () => {
    const customerId = 'customer-id';
    const entitlementCodes = ['transfer'];
    (httpClient.post as jest.Mock).mockResolvedValue({
      data: {
        entitlements: [
          {
            entitlement: 'count.free.atm_withdrawal',
            type: 'MONTHLY_COUNTER',
            dataType: 'INTEGER',
            description: 'Free ATM Withdrawal',
            quota: 3,
            comments: 'Free ATM withdrawals',
            usedQuota: 1,
            counterCode: 'counter.free.atm_withdrawal'
          }
        ]
      }
    });
    await entitlementRepository.getEntitlements(customerId, entitlementCodes);
    expect(httpClient.post).toHaveBeenCalledWith(
      `/private/${customerId}/counter`,
      {
        entitlementCodes
      }
    );
  });

  it('should be getEntitlements failed', async () => {
    const customerId = 'customer-id';
    const entitlementCodes = ['transfer'];
    (httpClient.post as jest.Mock).mockRejectedValueOnce({});
    try {
      await entitlementRepository.getEntitlements(customerId, entitlementCodes);
    } catch (error) {
      expect(error).toBeInstanceOf(TransferAppError);
    }
  });

  it('should be updateEntitlementUsageCounters successfully', async () => {
    const request = {
      customerId: 'customerId',
      entitlementCodes: ['transfer'],
      counterUsages: [
        {
          id: 'id',
          counterCode: 'counterCode',
          usedQuota: 1
        }
      ]
    };
    (httpClient.post as jest.Mock).mockResolvedValue({
      data: {
        entitlements: [
          {
            entitlement: 'count.free.atm_withdrawal',
            type: 'MONTHLY_COUNTER',
            dataType: 'INTEGER',
            description: 'Free ATM Withdrawal',
            quota: 3,
            comments: 'Free ATM withdrawals',
            usedQuota: 1,
            counterCode: 'counter.free.atm_withdrawal'
          }
        ],
        counters: [
          {
            id: 'feae5a3c-000c-40d0-9b89-bfed0290fe4b',
            recorded: true
          }
        ]
      }
    });
    await entitlementRepository.updateEntitlementUsageCounters(request);
    const { entitlementCodes, counterUsages } = request;
    expect(httpClient.post).toHaveBeenCalledWith(
      `/private/${request.customerId}/counter`,
      {
        entitlementCodes,
        counterUsages
      }
    );
  });

  it('should be revertEntitlementUsageCounters successfully', async () => {
    const request = {
      customerId: 'customerId',
      entitlementCodes: ['transfer'],
      counterReverts: [
        {
          id: 'id',
          counterCode: 'counterCode'
        }
      ]
    };
    (httpClient.post as jest.Mock).mockImplementationOnce(() =>
      Promise.resolve({})
    );
    await entitlementRepository.revertEntitlementUsageCounters(request);
    const { entitlementCodes, counterReverts } = request;
    expect(httpClient.post).toHaveBeenCalledWith(
      `/private/${request.customerId}/counter`,
      {
        entitlementCodes,
        counterReverts
      }
    );
  });

  it('should fail revert entitlement usage counters with failure reason actor MS_TRANSFER', async () => {
    const request = {
      customerId: 'customerId',
      entitlementCodes: ['transfer'],
      counterReverts: [
        {
          id: 'id',
          counterCode: 'counterCode'
        }
      ]
    };
    (httpClient.post as jest.Mock).mockResolvedValueOnce(undefined);

    const error = await entitlementRepository
      .revertEntitlementUsageCounters(request)
      .catch(error => error);

    expect(error).toBeDefined();
    expect(error).toBeInstanceOf(TransferAppError);
    expect(error.actor).toBe(TransferFailureReasonActor.MS_TRANSFER);
    expect(error.errorCode).toBe(ERROR_CODE.FAILED_TO_REVERT_ENTITLEMENT);
    expect(error.detail).toBe(
      'Failed to revert entitlement for [id: id AND counterCode: counterCode]!'
    );
  });

  it('should throw error when get empty data', async () => {
    const customerId = 'customer-id';
    const entitlementCodes = ['transfer'];
    (httpClient.post as jest.Mock).mockResolvedValueOnce({});
    try {
      await entitlementRepository.getEntitlements(customerId, entitlementCodes);
    } catch (error) {
      expect(error).toBeInstanceOf(TransferAppError);
    }
  });

  it('should throw error when update with incorrect entitlement code', async () => {
    const request = {
      customerId: 'customerId',
      entitlementCodes: ['transfer'],
      counterUsages: [
        {
          id: 'id',
          counterCode: 'counterCode',
          usedQuota: 1
        }
      ]
    };
    (httpClient.post as jest.Mock).mockResolvedValue({
      data: {
        counters: [
          {
            id: 'feae5a3c-000c-40d0-9b89-bfed0290fe4b',
            recorded: true
          }
        ]
      }
    });
    try {
      await entitlementRepository.updateEntitlementUsageCounters(request);
    } catch (error) {
      expect(error).toBeInstanceOf(TransferAppError);
    }
  });

  it('should throw error when update with invalid counter code', async () => {
    const request = {
      customerId: 'customerId',
      entitlementCodes: ['transfer'],
      counterUsages: [
        {
          id: 'id',
          counterCode: 'counterCode',
          usedQuota: 1
        }
      ]
    };
    (httpClient.post as jest.Mock).mockResolvedValue({
      data: {
        entitlements: [
          {
            entitlement: 'count.free.atm_withdrawal',
            type: 'MONTHLY_COUNTER',
            dataType: 'INTEGER',
            description: 'Free ATM Withdrawal',
            quota: 3,
            comments: 'Free ATM withdrawals',
            usedQuota: 1,
            counterCode: 'counter.free.atm_withdrawal'
          }
        ],
        counters: [
          {
            id: 'feae5a3c-000c-40d0-9b89-bfed0290fe4b',
            recorded: false,
            reason: {
              type: 'notFound'
            }
          }
        ]
      }
    });
    try {
      await entitlementRepository.updateEntitlementUsageCounters(request);
    } catch (error) {
      expect(error).toBeInstanceOf(TransferAppError);
    }
  });
});
