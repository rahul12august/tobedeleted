import { BankNetworkEnum } from '@dk/module-common';

import { UsageCounter } from '../../award/award.type';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import { COUNTER, FeeRuleType } from '../../fee/fee.constant';
import { FeeRuleInfo } from '../../fee/fee.type';
import {
  AwardLimitGroupCodes,
  EntitlementCode,
  EntitlementErrorReasonType
} from '../entitlement.enum';
import { EntitlementHandler } from '../entitlement.handler';
import {
  ICustomerEntitlementByTransCodeRequest,
  IUpdateUsageCounterEntitlementResponse
} from '../entitlement.interface';
import uuidv4 from 'uuid/v4';
import entitlementService from '../entitlement.service';
import { ITransactionCodeMapping } from '../../configuration/configuration.type';

describe('preProcessCustomerEntitlementBasedOnTransactionCodes', () => {
  const params: ICustomerEntitlementByTransCodeRequest = {
    transactionCodes: [
      {
        interchange: BankNetworkEnum.ALTO,
        transactionCode: 'TFD30',
        transactionCodeInfo: {
          maxAmountPerTx: 50000000,
          minAmountPerTx: 10000,
          limitGroupCode: 'L002',
          awardGroupCounter: AwardLimitGroupCodes.BONUS_TRANSFER,
          entitlementCode: EntitlementCode.BONUS_TRANSFER,
          channel: 'TFD30',
          code: 'TFD30'
        }
      },
      {
        interchange: BankNetworkEnum.ARTAJASA,
        transactionCode: 'TFD40',
        transactionCodeInfo: {
          maxAmountPerTx: 50000000,
          minAmountPerTx: 10000,
          limitGroupCode: 'L002',
          awardGroupCounter: AwardLimitGroupCodes.BONUS_TRANSFER,
          entitlementCode: EntitlementCode.BONUS_TRANSFER,
          channel: 'TFD40',
          code: 'TFD40'
        }
      }
    ],
    customerId: 'customer1',
    transactionId: undefined
  };

  it('test  PREVIEW mode', async () => {
    const toUseParams = { ...params };
    const entitlementHandler = new EntitlementHandler();

    const previewFn = jest
      .spyOn(
        entitlementService,
        'retrieveCustomerEntitlementRecommendedServicePreview'
      )
      .mockResolvedValue(null);

    await entitlementHandler.preProcessCustomerEntitlementBasedOnTransactionCodes(
      toUseParams
    );

    expect(previewFn).toBeCalledTimes(1);
    expect(previewFn).toBeCalledWith(toUseParams);
  });

  it('test CONSUME mode', async () => {
    const toUseParams = { ...params };
    toUseParams.transactionId = 'temptransid';
    const entitlementHandler = new EntitlementHandler();

    const consumeFn = jest
      .spyOn(entitlementService, 'processCustomerEntitlementConsumption')
      .mockResolvedValue(null);

    await entitlementHandler.preProcessCustomerEntitlementBasedOnTransactionCodes(
      toUseParams
    );

    expect(consumeFn).toBeCalledTimes(1);
    expect(consumeFn).toBeCalledWith(toUseParams);
  });
});

describe('mapEntitlementUsageCounterByLimitGroupCode', () => {
  const usageCounters: UsageCounter[] = [
    {
      limitGroupCode: 'L002',
      dailyAccumulationAmount: 50000,
      monthlyAccumulationTransaction: 0,
      quota: 0,
      isQuotaExceeded: false
    },
    {
      limitGroupCode: AwardLimitGroupCodes.BONUS_TRANSFER,
      dailyAccumulationAmount: 50000,
      monthlyAccumulationTransaction: 0,
      quota: 0,
      isQuotaExceeded: false
    }
  ];

  it('should return usageCounter as is, if entitlementResultResponse does not exists', () => {
    const entitlementHandler = new EntitlementHandler();
    const output = entitlementHandler.mapEntitlementUsageCounterByLimitGroupCode(
      usageCounters,
      null
    );
    expect(output).toEqual(usageCounters);
  });

  it('should mapped entitlement to the current usageCounter if response exists', () => {
    const entitlementHandler = new EntitlementHandler();
    const updatedEntitlementMapResponse: Map<
      string,
      IUpdateUsageCounterEntitlementResponse
    > = new Map();

    const entitlementCodeRefId = uuidv4();

    updatedEntitlementMapResponse.set('L002', {
      entitlements: [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 25,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 1,
          value: 'value'
        }
      ],
      counters: [
        {
          id: entitlementCodeRefId,
          recorded: true
        }
      ]
    });

    updatedEntitlementMapResponse.set(AwardLimitGroupCodes.BONUS_TRANSFER, {
      entitlements: [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 25,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 1,
          value: 'value'
        }
      ],
      counters: [
        {
          id: entitlementCodeRefId,
          recorded: true
        }
      ]
    });

    const output = entitlementHandler.mapEntitlementUsageCounterByLimitGroupCode(
      usageCounters,
      {
        updatedEntitlementMapResponse: updatedEntitlementMapResponse
      }
    );

    const updatedUsageCounters = [...usageCounters];
    updatedUsageCounters[0].quota = 25;
    updatedUsageCounters[0].monthlyAccumulationTransaction = 1;
    updatedUsageCounters[0].isQuotaExceeded = false;
    updatedUsageCounters[1].quota = 25;
    updatedUsageCounters[1].monthlyAccumulationTransaction = 1;
    updatedUsageCounters[1].isQuotaExceeded = false;

    expect(output).toEqual(updatedUsageCounters);
  });

  it('should mapped entitlement to the current usageCounter if response exists (exceeded quota)', () => {
    const entitlementHandler = new EntitlementHandler();
    const updatedEntitlementMapResponse: Map<
      string,
      IUpdateUsageCounterEntitlementResponse
    > = new Map();

    const entitlementCodeRefId = uuidv4();

    updatedEntitlementMapResponse.set('L002', {
      entitlements: [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 25,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 25,
          value: 'value'
        }
      ],
      counters: [
        {
          id: entitlementCodeRefId,
          recorded: false,
          reason: {
            type: EntitlementErrorReasonType.QUOTA_LIMIT,
            description: 'exceeded quota'
          }
        }
      ]
    });

    updatedEntitlementMapResponse.set(AwardLimitGroupCodes.BONUS_TRANSFER, {
      entitlements: [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 25,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 25,
          value: 'value'
        }
      ],
      counters: [
        {
          id: entitlementCodeRefId,
          recorded: false,
          reason: {
            type: EntitlementErrorReasonType.QUOTA_LIMIT,
            description: 'exceeded quota'
          }
        }
      ]
    });

    const output = entitlementHandler.mapEntitlementUsageCounterByLimitGroupCode(
      usageCounters,
      {
        updatedEntitlementMapResponse: updatedEntitlementMapResponse
      }
    );

    const updatedUsageCounters = [...usageCounters];
    updatedUsageCounters[0].quota = 25;
    updatedUsageCounters[0].monthlyAccumulationTransaction = 25;
    updatedUsageCounters[0].isQuotaExceeded = true;
    updatedUsageCounters[1].quota = 25;
    updatedUsageCounters[1].monthlyAccumulationTransaction = 25;
    updatedUsageCounters[1].isQuotaExceeded = true;

    expect(output).toEqual(updatedUsageCounters);
  });

  it('should mapped entitlement to the current usageCounter if response exists (exceeded quota, missing counter response)', () => {
    const entitlementHandler = new EntitlementHandler();
    const updatedEntitlementMapResponse: Map<
      string,
      IUpdateUsageCounterEntitlementResponse
    > = new Map();

    updatedEntitlementMapResponse.set('L002', {
      entitlements: [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 25,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 25,
          value: 'value'
        }
      ],
      counters: []
    });

    updatedEntitlementMapResponse.set(AwardLimitGroupCodes.BONUS_TRANSFER, {
      entitlements: [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 25,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 25,
          value: 'value'
        }
      ],
      counters: []
    });

    try {
      entitlementHandler.mapEntitlementUsageCounterByLimitGroupCode(
        usageCounters,
        {
          updatedEntitlementMapResponse: updatedEntitlementMapResponse
        }
      );
    } catch (err) {
      // Then
      expect(err).toBeInstanceOf(TransferAppError);
      expect(err.errorCode).toBe(
        ERROR_CODE.FAILED_TO_RETRIEVE_ENTITLEMENT_QUOTA_INFO
      );
    }
  });
});

describe('determineBasicFeeMapping', () => {
  const feeRuleInfoTemplate: FeeRuleInfo = {
    code: FeeRuleType.RTOL_TRANSFER_FEE_RULE,
    counterCode: COUNTER.COUNTER_01,
    basicFeeCode1: [
      {
        interchange: BankNetworkEnum.ARTAJASA,
        basicFeeCode: 'TF015'
      },
      {
        interchange: BankNetworkEnum.ALTO,
        basicFeeCode: 'TF008'
      }
    ],
    basicFeeCode2: [
      {
        interchange: BankNetworkEnum.ARTAJASA,
        basicFeeCode: 'TF010'
      },
      {
        interchange: BankNetworkEnum.ALTO,
        basicFeeCode: 'TF009'
      }
    ],
    monthlyNoTransaction: 0,
    interchange: BankNetworkEnum.ALTO
  };

  it('should be failed when required fields are undefined', async () => {
    const feeRuleInfo = {
      ...feeRuleInfoTemplate
    };

    feeRuleInfo.quota = undefined;
    feeRuleInfo.isQuotaExceeded = undefined;

    const entitlementHandler = new EntitlementHandler();

    const output = await entitlementHandler
      .determineBasicFeeMapping(feeRuleInfo)
      .catch(err => err);

    // Then
    expect(output).toBeInstanceOf(TransferAppError);
    expect(output.errorCode).toBe(
      ERROR_CODE.TRANSFER_FEE_RULE_INVALID_MANDATORY_FIELDS
    );
  });

  it('should return basicCode1 when usedQuota < quota', async () => {
    const feeRuleInfo = {
      ...feeRuleInfoTemplate
    };

    feeRuleInfo.quota = 25;
    feeRuleInfo.monthlyNoTransaction = 1;
    feeRuleInfo.isQuotaExceeded = false;

    const entitlementHandler = new EntitlementHandler();

    const output = await entitlementHandler.determineBasicFeeMapping(
      feeRuleInfo
    );

    // Then
    expect(output).toEqual(feeRuleInfo.basicFeeCode1);
  });

  it('should return basicCode1 when usedQuota == quota, but it does not exceed the quota yet', async () => {
    const feeRuleInfo = {
      ...feeRuleInfoTemplate
    };

    feeRuleInfo.quota = 25;
    feeRuleInfo.monthlyNoTransaction = 25;
    feeRuleInfo.isQuotaExceeded = false;

    const entitlementHandler = new EntitlementHandler();

    const output = await entitlementHandler.determineBasicFeeMapping(
      feeRuleInfo
    );

    // Then
    expect(output).toEqual(feeRuleInfo.basicFeeCode1);
  });

  it('should return basicCode1 when usedQuota == quota, but it exceeds the quota', async () => {
    const feeRuleInfo = {
      ...feeRuleInfoTemplate
    };

    feeRuleInfo.quota = 25;
    feeRuleInfo.monthlyNoTransaction = 25;
    feeRuleInfo.isQuotaExceeded = true;

    const entitlementHandler = new EntitlementHandler();

    const output = await entitlementHandler.determineBasicFeeMapping(
      feeRuleInfo
    );

    // Then
    expect(output).toEqual(feeRuleInfo.basicFeeCode2);
  });

  describe('generateMissingUsageCounterBasedOnTransactionCodes', () => {
    const updatedEntitlementMapResponse: Map<
      string,
      IUpdateUsageCounterEntitlementResponse
    > = new Map();

    const entitlementCodeRefId = uuidv4();

    updatedEntitlementMapResponse.set('L002', {
      entitlements: [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 25,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 1,
          value: 'value'
        }
      ],
      counters: [
        {
          id: entitlementCodeRefId,
          recorded: true
        }
      ]
    });

    updatedEntitlementMapResponse.set(AwardLimitGroupCodes.BONUS_TRANSFER, {
      entitlements: [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 25,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 1,
          value: 'value'
        }
      ],
      counters: [
        {
          id: entitlementCodeRefId,
          recorded: true
        }
      ]
    });

    const transactionCodes: ITransactionCodeMapping[] = [
      {
        interchange: BankNetworkEnum.ALTO,
        transactionCode: 'TFD30',
        transactionCodeInfo: {
          maxAmountPerTx: 50000000,
          minAmountPerTx: 10000,
          limitGroupCode: 'L002',
          awardGroupCounter: AwardLimitGroupCodes.BONUS_TRANSFER,
          entitlementCode: EntitlementCode.BONUS_TRANSFER,
          channel: 'TFD30',
          code: 'TFD30'
        }
      },
      {
        interchange: BankNetworkEnum.ARTAJASA,
        transactionCode: 'TFD40',
        transactionCodeInfo: {
          maxAmountPerTx: 50000000,
          minAmountPerTx: 10000,
          limitGroupCode: 'L002',
          awardGroupCounter: AwardLimitGroupCodes.BONUS_TRANSFER,
          entitlementCode: EntitlementCode.BONUS_TRANSFER,
          channel: 'TFD40',
          code: 'TFD40'
        }
      }
    ];

    const usageCounters: UsageCounter[] = [];

    it(`when required transactionCodes and entitlementResult are either empty or undefined
      return usageCounter as is
    `, async () => {
      const usageCountersTest: UsageCounter[] = [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 50000,
          monthlyAccumulationTransaction: 0,
          quota: 0,
          isQuotaExceeded: false
        }
      ];
      const entitlementHandler = new EntitlementHandler();
      const result = await entitlementHandler.generateMissingUsageCounterBasedOnTransactionCodes(
        {
          usageCounters: usageCountersTest
        }
      );

      expect(result).toEqual(usageCountersTest);
    });

    it(`when required extractAvailableLimitGroupCodesFromTransactionCodeInfo return empty set
    return usageCounter as is
  `, async () => {
      const usageCountersTest: UsageCounter[] = [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 50000,
          monthlyAccumulationTransaction: 0,
          quota: 0,
          isQuotaExceeded: false
        }
      ];

      const entitlementHandler = new EntitlementHandler();

      jest
        .spyOn(
          entitlementHandler,
          'extractAvailableLimitGroupCodesFromTransactionCodeInfo'
        )
        .mockReturnValue(new Set<string>());

      const result = await entitlementHandler.generateMissingUsageCounterBasedOnTransactionCodes(
        {
          transactionCodes,
          usageCounters: usageCountersTest,
          entitlementResultResponse: {
            updatedEntitlementMapResponse: updatedEntitlementMapResponse
          }
        }
      );

      expect(result).toEqual(usageCountersTest);
    });

    it(`when tcValidLimitGroupCodes are not emptySet
    return missing usageCounter
  `, async () => {
      const entitlementHandler = new EntitlementHandler();

      jest
        .spyOn(
          entitlementHandler,
          'extractAvailableLimitGroupCodesFromTransactionCodeInfo'
        )
        .mockReturnValue(
          new Set(['L002', AwardLimitGroupCodes.BONUS_TRANSFER])
        );

      const result = await entitlementHandler.generateMissingUsageCounterBasedOnTransactionCodes(
        {
          transactionCodes,
          usageCounters,
          entitlementResultResponse: {
            updatedEntitlementMapResponse: updatedEntitlementMapResponse
          }
        }
      );

      const expectedOutput: UsageCounter[] = [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 1,
          quota: 25,
          isQuotaExceeded: false
        },
        {
          limitGroupCode: 'Bonus_Transfer',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 1,
          quota: 25,
          isQuotaExceeded: false
        }
      ];

      expect(result).toEqual(expectedOutput);
    });
  });
});
