import {
  CONSUMER_NAME,
  PROVIDER_NAME,
  PORT_NUMBER
} from './contract.constants';
import entitlementRepository from '../entitlement.repository';

import { ENTITLEMENT_API_ENDPOINT } from '../entitlement.constant';

import ContractHelper from './contract.helper';
import { StatusCode } from '@dk/module-common/dist/http/htttp.enums';
import { IUpdateUsageCounterEntitlementResponse } from '../entitlement.interface';

import {
  scenario1,
  scenario2,
  scenario3,
  scenario4,
  scenario5
} from '../__mocks__/atm.withdrawal.consume.data';

import contractCommonInitFn from './common.headers.contract';
import { getFreeAtmWithdrawalResponse } from '../__mocks__/entitlement.data';
import { TransferAppError } from '../../errors/AppError';
import { EntitlementCode } from '../entitlement.enum';

jest.mock('../../logger');
jest.mock('../httpClient', () => {
  const httpClient = require('@dk/module-httpclient');
  const context = require('../../context').context;
  const logger = {
    error: jest.fn(), // error: e => console.error(e),
    info: jest.fn(), // info: e => console.info(e),
    debug: jest.fn(), // debug: e => console.info(e),
    warn: jest.fn(), // warn: e => console.warn(e),
    critical: jest.fn() // critical: e => console.error(e)
  };
  return httpClient.createHttpClient({
    baseURL: `http://localhost:${PORT_NUMBER}`,
    context,
    logger
  });
});

jest.setTimeout(30000);

const provider = contractCommonInitFn.initPactProvider({
  consumerName: CONSUMER_NAME,
  providerName: PROVIDER_NAME,
  portNumber: PORT_NUMBER
});

describe('entitlement.repository-contract: UPDATE & GET USAGE COUNTERS', () => {
  const mockCustomerId = '1234';
  const responseHeaders = { 'Content-Type': 'application/json' };

  beforeAll(() => provider.setup());
  beforeEach(() => {
    jest.resetAllMocks();
  });
  afterEach(() => provider.verify());
  afterAll(() => provider.finalize());

  it('[scenario 1]: should update properly and retrieved the info back', async () => {
    const requestData = scenario1(mockCustomerId).requestData;
    const responseData = scenario1(mockCustomerId).responseData;

    const expectedMatcherOutput = ContractHelper.convertExpectedOutputToMatcher(
      responseData
    );

    const expectedCounterOutputMap = ContractHelper.convertCounterResponseToMap(
      responseData.counters
    );

    await provider.addInteraction({
      state: 'free atm withdrawal sufficient for deduction',
      uponReceiving: 'deduct the usage counter of free atm withdrawal',
      withRequest: {
        method: 'POST',
        path: ENTITLEMENT_API_ENDPOINT(mockCustomerId),
        body: {
          entitlementCodes: requestData.entitlementCodes,
          counterUsages: requestData.counterUsages
        }
      },
      willRespondWith: {
        status: StatusCode.OK,
        headers: responseHeaders,
        body: expectedMatcherOutput
      }
    });

    const actualResult: IUpdateUsageCounterEntitlementResponse = await entitlementRepository.updateEntitlementUsageCounters(
      requestData
    );

    //the number of entitlement should be the same as the number of entitlement codes
    expect(actualResult.entitlements).toHaveLength(
      responseData.entitlements.length
    );

    //the length should be the same as the counterUsage request
    expect(actualResult.counters).toHaveLength(responseData.counters.length);

    actualResult.counters.forEach(counter => {
      const cResult = expectedCounterOutputMap.get(counter.id);
      if (cResult != null) {
        expect(counter.id).toEqual(cResult.id);
        expect(counter.recorded).toBeTruthy();
        expect(counter.reason).toBeUndefined();
      }
    });
  });

  it('[scenario 2]: replicated uniqueid, idempotency error', async () => {
    const requestData = scenario2(mockCustomerId).requestData;
    const responseData = scenario2(mockCustomerId).responseData;

    const expectedMatcherOutput = ContractHelper.convertExpectedOutputToMatcher(
      responseData
    );

    await provider.addInteraction({
      state: 'free atm withdrawal, where uniqueid is duplicated',
      uponReceiving: `return existing result for the entitlement, 
      and counters should throw error with reason`,
      withRequest: {
        method: 'POST',
        path: ENTITLEMENT_API_ENDPOINT(mockCustomerId),
        body: {
          entitlementCodes: requestData.entitlementCodes,
          counterUsages: requestData.counterUsages
        }
      },
      willRespondWith: {
        status: StatusCode.OK,
        headers: responseHeaders,
        body: expectedMatcherOutput
      }
    });

    const actualResult: IUpdateUsageCounterEntitlementResponse = await entitlementRepository.updateEntitlementUsageCounters(
      requestData
    );

    //the number of entitlement should be the same as the number of entitlement codes
    expect(actualResult.entitlements).toHaveLength(
      responseData.entitlements.length
    );

    //the length should be the same as the counterUsage request
    expect(actualResult.counters).toHaveLength(responseData.counters.length);

    let index = 0;
    actualResult.counters.forEach(counter => {
      const cResult = responseData.counters[index];

      expect(counter.id).toEqual(cResult.id);

      if (cResult.reason) {
        expect(counter.recorded).toBeFalsy();
        expect(counter.reason).not.toBeNull();
      } else {
        expect(counter.recorded).toBeTruthy();
        expect(counter.reason).toBeUndefined();
      }

      index++;
    });
  });

  it('[scenario 3]: used quota > quota, insufficient quota', async () => {
    const requestData = scenario3(mockCustomerId).requestData;
    const responseData = scenario3(mockCustomerId).responseData;

    const expectedMatcherOutput = ContractHelper.convertExpectedOutputToMatcher(
      responseData
    );

    await provider.addInteraction({
      state: `free atm withdrawal, quota is available,
       but used quota is bigger than existing quota`,
      uponReceiving: `return existing result for the entitlement, 
      and counters should throw error with reason`,
      withRequest: {
        method: 'POST',
        path: ENTITLEMENT_API_ENDPOINT(mockCustomerId),
        body: {
          entitlementCodes: requestData.entitlementCodes,
          counterUsages: requestData.counterUsages
        }
      },
      willRespondWith: {
        status: StatusCode.OK,
        headers: responseHeaders,
        body: expectedMatcherOutput
      }
    });

    try {
      await entitlementRepository.updateEntitlementUsageCounters(requestData);
    } catch (e) {
      expect(e).toBeInstanceOf(TransferAppError);
    }
  });

  it('[scenario 4]: quota is not available', async () => {
    const requestData = scenario4(mockCustomerId).requestData;
    const responseData = scenario4(mockCustomerId).responseData;

    const expectedMatcherOutput = ContractHelper.convertExpectedOutputToMatcher(
      responseData
    );

    await provider.addInteraction({
      state: `free atm withdrawal, quota is not available,`,
      uponReceiving: `return existing result for the entitlement, 
      and counters should throw error with reason`,
      withRequest: {
        method: 'POST',
        path: ENTITLEMENT_API_ENDPOINT(mockCustomerId),
        body: {
          entitlementCodes: requestData.entitlementCodes,
          counterUsages: requestData.counterUsages
        }
      },
      willRespondWith: {
        status: StatusCode.OK,
        headers: responseHeaders,
        body: expectedMatcherOutput
      }
    });

    try {
      await entitlementRepository.updateEntitlementUsageCounters(requestData);
    } catch (e) {
      expect(e).toBeInstanceOf(TransferAppError);
    }
  });

  it('[scenario 5]: used quota is negative', async () => {
    const requestData = scenario5(mockCustomerId).requestData;
    const responseData = scenario5(mockCustomerId).responseData;

    const expectedMatcherOutput = ContractHelper.convertExpectedOutputToMatcher(
      responseData
    );

    await provider.addInteraction({
      state: `free atm withdrawal, quota is available, 
      but inputted used quota is negative`,
      uponReceiving: `return existing result for the entitlement, 
      and counters should throw error with reason`,
      withRequest: {
        method: 'POST',
        path: ENTITLEMENT_API_ENDPOINT(mockCustomerId),
        body: {
          entitlementCodes: requestData.entitlementCodes,
          counterUsages: requestData.counterUsages
        }
      },
      willRespondWith: {
        status: StatusCode.OK,
        headers: responseHeaders,
        body: expectedMatcherOutput
      }
    });

    try {
      await entitlementRepository.updateEntitlementUsageCounters(requestData);
    } catch (e) {
      expect(e).toBeInstanceOf(TransferAppError);
    }
  });

  it('[scenario 6]: should return data if found', async () => {
    const mockCustomerId = 'cust-1234';
    const data = getFreeAtmWithdrawalResponse();

    await provider.addInteraction({
      state: 'normal / data exists',
      uponReceiving: 'a request for get free atm withdrawal with data exists',
      withRequest: {
        method: 'POST',
        path: ENTITLEMENT_API_ENDPOINT(mockCustomerId),
        body: {
          entitlementCodes: [EntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL]
        }
      },
      willRespondWith: {
        status: StatusCode.OK,
        headers: responseHeaders,
        body: data
      }
    });
    const response = await entitlementRepository.getEntitlements(
      mockCustomerId,
      [EntitlementCode.BONUS_LOCAL_CASH_WITHDRAWAL]
    );
    expect(response).toHaveLength(1);
  });

  it('[scenario 7]: should return bad request if entitlement codes invalid', async () => {
    const mockCustomerId = 'cust-id';

    await provider.addInteraction({
      state: 'entitlement codes invalid',
      uponReceiving:
        'a request for get free atm withdrawal with entitlement codes invalid',
      withRequest: {
        method: 'POST',
        path: ENTITLEMENT_API_ENDPOINT(mockCustomerId),
        body: {
          entitlementCodes: ['test_quota']
        }
      },
      willRespondWith: {
        status: StatusCode.OK,
        headers: responseHeaders,
        body: {
          //response
        }
      }
    });

    const result = await entitlementRepository.getEntitlements(
      mockCustomerId,
      ['test_quota']
    );

    expect(result).toEqual([]);
  });
});
