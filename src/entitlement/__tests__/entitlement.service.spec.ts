import {
  EntitlementContext,
  ICounterRevertEntitlementResponse,
  IEntitlementResponse
} from '../entitlement.interface';
import entitlementRepository from '../entitlement.repository';
import entitlementService from '../entitlement.service';
import { getTransactionInfoTracker } from '../../common/contextHandler';
import {
  mockEntitlementInfoContext,
  mockEntitlementResultResponse,
  mockEntitlementReversalResponse,
  mockUsageCounterData,
  mockCustomerEntitlementByTransCodeRequest,
  mockUpdateUsageCounterEntitlementResponse,
  mockEntitlementResultResponseExceedLimitation
} from '../__mocks__/entitlement.data';
import { mockExcludedEntitlementPaymentServiceTypes } from '../__mocks__/entitlement.service.mock.data';
import { IConfirmTransactionInput } from '../../transaction/transaction.type';
import { ConfirmTransactionStatus } from '../../transaction/transaction.enum';
import transactionRepository from '../../transaction/transaction.repository';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import { UsageCounter } from '../../award/award.type';

jest.mock('../entitlement.repository');
jest.mock('../../common/contextHandler');
jest.mock('../../transaction/transaction.repository');

describe('entitlement.service', () => {
  afterEach(() => {
    jest.resetAllMocks();
    expect.hasAssertions();
  });

  describe('processReversalEntitlementCounterUsageBasedOnContext', () => {
    it('should process reversal entitlement counter usage based on context', async () => {
      // given
      const mockInfo: EntitlementContext[] = mockEntitlementInfoContext();
      const mockResponse: ICounterRevertEntitlementResponse = mockEntitlementReversalResponse();
      (entitlementRepository.revertEntitlementUsageCounters as jest.Mock).mockReturnValueOnce(
        mockResponse
      );
      (getTransactionInfoTracker as jest.Mock).mockReturnValueOnce(mockInfo);

      // when
      await entitlementService.processReversalEntitlementCounterUsageBasedOnContext();

      // then
      expect(
        entitlementRepository.revertEntitlementUsageCounters
      ).toHaveBeenCalledWith({
        customerId: 'customerId',
        counterReverts: [
          {
            counterCode: 'counterCode',
            id: 'entitlementCodeRefId'
          }
        ]
      });
    });
  });
  describe('processReversalEntitlementCounterUsageOnTransactionConfirmation', () => {
    it('should be process reversal entitlement when ConfirmTransactionStatus.ERROR_UNEXPECTED counter usage on transaction confirmation', async () => {
      const input: IConfirmTransactionInput = {
        responseCode: ConfirmTransactionStatus.ERROR_UNEXPECTED,
        externalId: 'externalId'
      };
      const mockInfo: EntitlementContext[] = mockEntitlementInfoContext();
      const mockResponse: ICounterRevertEntitlementResponse = mockEntitlementReversalResponse();
      (entitlementRepository.revertEntitlementUsageCounters as jest.Mock).mockReturnValueOnce(
        mockResponse
      );
      (getTransactionInfoTracker as jest.Mock).mockReturnValueOnce(mockInfo);

      entitlementService.processReversalEntitlementCounterUsageOnTransactionConfirmation(
        input
      );
      expect(
        entitlementRepository.revertEntitlementUsageCounters
      ).toHaveBeenCalledWith({
        customerId: 'customerId',
        counterReverts: [
          {
            counterCode: 'counterCode',
            id: 'entitlementCodeRefId'
          }
        ]
      });
    });

    it('should be process reversal entitlement when ConfirmTransactionStatus.ERROR_EXPECTED counter usage on transaction confirmation', async () => {
      const input: IConfirmTransactionInput = {
        responseCode: ConfirmTransactionStatus.ERROR_EXPECTED,
        externalId: 'externalId'
      };
      const mockInfo: EntitlementContext[] = mockEntitlementInfoContext();
      const mockResponse: ICounterRevertEntitlementResponse = mockEntitlementReversalResponse();
      (entitlementRepository.revertEntitlementUsageCounters as jest.Mock).mockReturnValueOnce(
        mockResponse
      );
      (getTransactionInfoTracker as jest.Mock).mockReturnValueOnce(mockInfo);

      entitlementService.processReversalEntitlementCounterUsageOnTransactionConfirmation(
        input
      );
      expect(
        entitlementRepository.revertEntitlementUsageCounters
      ).toHaveBeenCalledWith({
        customerId: 'customerId',
        counterReverts: [
          {
            counterCode: 'counterCode',
            id: 'entitlementCodeRefId'
          }
        ]
      });
    });

    it('should be process reversal entitlement when ConfirmTransactionStatus.REQUEST_TIMEOUT counter usage on transaction confirmation', async () => {
      const input: IConfirmTransactionInput = {
        responseCode: ConfirmTransactionStatus.REQUEST_TIMEOUT,
        externalId: 'externalId'
      };
      const mockInfo: EntitlementContext[] = mockEntitlementInfoContext();
      const mockResponse: ICounterRevertEntitlementResponse = mockEntitlementReversalResponse();
      (entitlementRepository.revertEntitlementUsageCounters as jest.Mock).mockReturnValueOnce(
        mockResponse
      );
      (getTransactionInfoTracker as jest.Mock).mockReturnValueOnce(mockInfo);

      entitlementService.processReversalEntitlementCounterUsageOnTransactionConfirmation(
        input
      );
      expect(
        entitlementRepository.revertEntitlementUsageCounters
      ).toHaveBeenCalledWith({
        customerId: 'customerId',
        counterReverts: [
          {
            counterCode: 'counterCode',
            id: 'entitlementCodeRefId'
          }
        ]
      });
    });

    it('should be do nothing when status confirmation are not defined transaction confirmation', async () => {
      const input: IConfirmTransactionInput = {
        responseCode: 999,
        externalId: 'externalId'
      };
      const mockInfo: EntitlementContext[] = mockEntitlementInfoContext();
      const mockResponse: ICounterRevertEntitlementResponse = mockEntitlementReversalResponse();
      (entitlementRepository.revertEntitlementUsageCounters as jest.Mock).mockReturnValueOnce(
        mockResponse
      );
      (getTransactionInfoTracker as jest.Mock).mockReturnValueOnce(mockInfo);

      const result = entitlementService.processReversalEntitlementCounterUsageOnTransactionConfirmation(
        input
      );
      expect(result).toBeUndefined();
    });
  });

  describe('mapEntitlementUsageCounterByLimitGroupCode', () => {
    it('should return usagecounter as is, if entitlementResultResponse is null ', () => {
      const usageCounter = mockUsageCounterData();
      const result = entitlementService.mapEntitlementUsageCounterByLimitGroupCode(
        usageCounter,
        null
      );

      expect(result).toEqual(usageCounter);
    });

    it('should be map entity usage counter by limit group code', async () => {
      const usageCounter = mockUsageCounterData();
      const entitlementResultResponse = mockEntitlementResultResponse();
      const result = entitlementService.mapEntitlementUsageCounterByLimitGroupCode(
        usageCounter,
        entitlementResultResponse
      );

      expect(result).toEqual([
        {
          limitGroupCode: 'limitGroupCode',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 1,
          quota: 6,
          isQuotaExceeded: false
        }
      ]);
    });

    it('should be map entity usage counter by limit group code, with associated reason [EXCEEDED LIMIT]', async () => {
      const usageCounter = mockUsageCounterData();
      const entitlementResultResponse = mockEntitlementResultResponseExceedLimitation();

      const result = entitlementService.mapEntitlementUsageCounterByLimitGroupCode(
        usageCounter,
        entitlementResultResponse
      );

      expect(result).toEqual([
        {
          limitGroupCode: 'limitGroupCode',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 6,
          quota: 6,
          isQuotaExceeded: true
        }
      ]);
    });
  });

  describe('retrieveCustomerEntitlementRecommendedServicePreview', () => {
    it('should return null when retrieve customer entitlement recommended service preview with uniqueEntitlementCodeMap null', async () => {
      const mockCustomerEntitlementByTransCode = mockCustomerEntitlementByTransCodeRequest();
      const entitlementResponse: IEntitlementResponse[] = [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 6,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 3,
          value: 'value'
        }
      ];
      (entitlementRepository.getEntitlements as jest.Mock).mockReturnValueOnce(
        entitlementResponse
      );

      const result = await entitlementService.retrieveCustomerEntitlementRecommendedServicePreview(
        mockCustomerEntitlementByTransCode
      );
      expect(result).toBeNull();
    });

    it('should be retrieve customer entitlement recommended service preview with uniqueEntitlementCodeMap successfully', async () => {
      const mockCustomerEntitlementByTransCode = mockCustomerEntitlementByTransCodeRequest();
      const entitlementResponse: IEntitlementResponse[] = [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 6,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 3,
          value: 'value'
        }
      ];
      const transactionCodeInfo =
        mockCustomerEntitlementByTransCode?.transactionCodes?.[0]
          ?.transactionCodeInfo;

      if (transactionCodeInfo) {
        transactionCodeInfo.awardGroupCounter = 'awardGroupCounter';
        transactionCodeInfo.counterCode = 'L002';
        transactionCodeInfo.entitlementCode = 'count.free.transfer.out';
        transactionCodeInfo.limitGroupCode = 'limitGroupCode';
      }
      (entitlementRepository.getEntitlements as jest.Mock).mockReturnValueOnce(
        entitlementResponse
      );
      const result = await entitlementService.retrieveCustomerEntitlementRecommendedServicePreview(
        mockCustomerEntitlementByTransCode
      );
      const countResult = result?.updatedEntitlementMapResponse.size;
      expect(countResult).toEqual(2);
    });

    it(`
      should be retrieve customer entitlement recommended service 
      preview with uniqueEntitlementCodeMap successfully
      when usedQuota exceeded quota
    `, async () => {
      const mockCustomerEntitlementByTransCode = mockCustomerEntitlementByTransCodeRequest();
      const entitlementResponse: IEntitlementResponse[] = [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 6,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 6,
          value: 'value'
        }
      ];

      const transactionCodeInfo =
        mockCustomerEntitlementByTransCode?.transactionCodes?.[0]
          ?.transactionCodeInfo;
      if (transactionCodeInfo) {
        transactionCodeInfo.awardGroupCounter = 'awardGroupCounter';
        transactionCodeInfo.counterCode = 'L002';
        transactionCodeInfo.entitlementCode = 'count.free.transfer.out';
        transactionCodeInfo.limitGroupCode = 'limitGroupCode';
      }
      (entitlementRepository.getEntitlements as jest.Mock).mockReturnValueOnce(
        entitlementResponse
      );
      const result = await entitlementService.retrieveCustomerEntitlementRecommendedServicePreview(
        mockCustomerEntitlementByTransCode
      );

      const countResult = result?.updatedEntitlementMapResponse.size;
      expect(countResult).toEqual(2);
      const countersResponse = result?.updatedEntitlementMapResponse?.get(
        'limitGroupCode'
      )?.counters[0];
      expect(countersResponse).toBeDefined();
      expect(countersResponse?.recorded).toBeFalsy();
      expect(countersResponse?.reason).toEqual({
        type: 'quotaLimit',
        description: 'exceed quota limit'
      });
    });
  });

  describe('processCustomerEntitlementConsumption', () => {
    it('required field is null, then throw error CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_CONSUME_ERROR', async () => {
      try {
        await entitlementService.processCustomerEntitlementConsumption({
          customerId: undefined,
          transactionId: undefined,
          transactionCodes: undefined,
          paymentServiceType: undefined
        });
      } catch (error) {
        expect(error).toBeInstanceOf(TransferAppError);
        expect(error.errorCode).toEqual(
          ERROR_CODE.CUSTOMER_ENTITLEMENT_RECOMMENDATION_SERVICE_CONSUME_ERROR
        );
      }
    });

    it.each(mockExcludedEntitlementPaymentServiceTypes())(
      'when test against entitlement excluded payment service type, it should return null',
      async (scenario: any) => {
        const result = await entitlementService.processCustomerEntitlementConsumption(
          scenario.params
        );

        expect(result).toBe(scenario.expected);
      }
    );

    it('should be processCustomerEntitlementConsumption with return null and not updated', async () => {
      const mockCustomerEntitlementByTransCode = mockCustomerEntitlementByTransCodeRequest();
      const entitlementResponse: IEntitlementResponse[] = [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 6,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 3,
          value: 'value'
        }
      ];
      (entitlementRepository.getEntitlements as jest.Mock).mockReturnValueOnce(
        entitlementResponse
      );

      const result = await entitlementService.processCustomerEntitlementConsumption(
        mockCustomerEntitlementByTransCode
      );
      expect(result).toEqual(null);
    });

    it('should be processCustomerEntitlementConsumption with update usage counter successfully', async () => {
      const mockCustomerEntitlementByTransCode = mockCustomerEntitlementByTransCodeRequest();
      const entitlementResponse: IEntitlementResponse[] = [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 6,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 3,
          value: 'value'
        }
      ];
      const updateResponse = mockUpdateUsageCounterEntitlementResponse();
      let transactionCodeInfo =
        mockCustomerEntitlementByTransCode?.transactionCodes?.[0]
          ?.transactionCodeInfo;
      if (transactionCodeInfo) {
        transactionCodeInfo.awardGroupCounter = 'awardGroupCounter';
        transactionCodeInfo.counterCode = 'L002';
        transactionCodeInfo.entitlementCode = 'count.free.transfer.out';
        transactionCodeInfo.limitGroupCode = 'limitGroupCode';
      }
      (entitlementRepository.getEntitlements as jest.Mock).mockReturnValueOnce(
        entitlementResponse
      );
      (entitlementRepository.updateEntitlementUsageCounters as jest.Mock).mockReturnValueOnce(
        updateResponse
      );
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        entitlementCodeRefId: 'entitlementCodeRefId',
        counterCode: 'counterCode',
        customerId: 'customerId'
      });
      const result = await entitlementService.processCustomerEntitlementConsumption(
        mockCustomerEntitlementByTransCode
      );

      expect(result).not.toBeNull();
      expect(result?.updatedEntitlementMapResponse.size).toEqual(2);
    });
  });

  describe('getEntitlementByAwardGroupCounter', () => {
    it('should be return entitlements', async () => {
      const usageCounter = mockUsageCounterData();
      const awardGroupCounter = 'limitGroupCode';
      const result = entitlementService.getEntitlementByAwardGroupCounter(
        usageCounter,
        awardGroupCounter
      );
      expect(result).toEqual(usageCounter[0]);
    });

    it('should be return default entitlements', async () => {
      const usageCounter: UsageCounter[] = [];
      const awardGroupCounter = 'limitGroupCode';
      const defaultEntitlement = {
        monthlyAccumulationTransaction: 0,
        quota: undefined,
        limitGroupCode: undefined,
        dailyAccumulationAmount: 0
      };
      const result = entitlementService.getEntitlementByAwardGroupCounter(
        usageCounter,
        awardGroupCounter
      );

      expect(result).toEqual(defaultEntitlement);
    });
  });
});
