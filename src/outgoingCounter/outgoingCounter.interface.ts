export interface IOutgoingCounter {
  prefix: string;
  counter: number;
}
