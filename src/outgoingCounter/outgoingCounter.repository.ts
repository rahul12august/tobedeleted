import { IOutgoingCounter } from './outgoingCounter.interface';
import { OutgoingCounterModel } from './outgoingCounter.model';
import { wrapLogs } from '../logger';

const getNextCounterByPrefix = async (
  prefix: string
): Promise<IOutgoingCounter> => {
  return OutgoingCounterModel.findOneAndUpdate(
    {
      prefix
    },
    {
      $inc: { counter: 1 } // base 1
    },
    {
      new: true,
      upsert: true
    }
  )
    .lean()
    .exec();
};

const outgoingCounterRepository = {
  getNextCounterByPrefix
};

export default wrapLogs(outgoingCounterRepository);
