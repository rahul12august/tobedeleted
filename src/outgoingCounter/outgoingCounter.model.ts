import mongoose, { Schema, Model, Document } from 'mongoose';
import { IOutgoingCounter } from './outgoingCounter.interface';

export type OutgoingCounterDocument = IOutgoingCounter & Document;

const collectionName = 'outgoing_counters';

const OutgoingCounterSchema = new Schema({
  prefix: {
    type: String,
    required: true,
    unique: true,
    index: true
  },
  counter: {
    // base 1 because we use $inc and upsert
    type: Number,
    required: true
  }
});

export const OutgoingCounterModel: Model<OutgoingCounterDocument> = mongoose.model(
  collectionName,
  OutgoingCounterSchema
);
