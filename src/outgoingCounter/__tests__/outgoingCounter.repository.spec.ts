import mongoose from 'mongoose';
import outgoingCounterRepository from '../outgoingCounter.repository';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { OutgoingCounterModel } from '../outgoingCounter.model';

jest.mock('mongoose', () => {
  const mongoose = jest.requireActual('mongoose');
  return new mongoose.Mongoose(); // new mongoose instance and connection for each test
});

describe('outgoingCounterRepository', () => {
  let mongodb: MongoMemoryServer;
  beforeAll(async () => {
    mongodb = new MongoMemoryServer();
    const mongoDbUri = await mongodb.getConnectionString();
    await mongoose.connect(mongoDbUri, {
      useNewUrlParser: true,
      useCreateIndex: true
    });
  });

  afterEach(async () => {
    expect.hasAssertions();
    await OutgoingCounterModel.deleteMany({});
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongodb.stop();
  });

  describe('getNextCounterByPrefix', () => {
    it('should return counter 1 when first retrieve', async () => {
      const result = await outgoingCounterRepository.getNextCounterByPrefix(
        '20123T'
      );
      expect(result.counter).toEqual(1);
    });

    it('should return increased counter when retrieve', async () => {
      const prefix = '20124T';
      const counter = 123;
      await OutgoingCounterModel.create({
        prefix,
        counter
      });
      const result = await outgoingCounterRepository.getNextCounterByPrefix(
        prefix
      );
      expect(result.counter).toEqual(counter + 1);
    });
  });
});
