import logger from './logger';
import { requiredEnvVarNames } from './constant';

const init = (): void => {
  const missingRequiredEnv = requiredEnvVarNames.filter(envVar => {
    const missing = !process.env[envVar];
    if (missing) {
      logger.error(`missing required environment variable: ${envVar}`);
    }
    return missing;
  });

  if (missingRequiredEnv.length) {
    process.exit(0);
  }
};

const envChecker = {
  init
};

export default envChecker;
