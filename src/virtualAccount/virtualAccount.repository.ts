import { IVirtualAccount } from './virtualAccount.type';
import httpClient from './httpClient';
import { get } from 'lodash';
import { wrapLogs } from '../logger';

export const inquiryVirtualAccount = async (
  accountNumber: string
): Promise<IVirtualAccount> => {
  const response = await httpClient.post(`/virtual-accounts/inquiry`, {
    accountNumber
  });
  return get(response, ['data', 'data']);
};

export default wrapLogs({
  inquiryVirtualAccount
});
