export interface IVirtualAccount {
  customerName: string;
  institution: IInstitution;
}

interface IInstitution {
  name: string;
  bankCode: string;
  accountId: string;
  cif: string;
}
