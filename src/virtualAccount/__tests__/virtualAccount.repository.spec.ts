import virtualAccountRepository from '../virtualAccount.repository';
import httpClient from '../httpClient';

jest.mock('../httpClient');

describe('virtualAccount.repository', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });
  describe('inquiryVirtualAccount', () => {
    it('should call ms-virtual-account with correct endpoint and payload ', async () => {
      (httpClient.post as jest.Mock).mockImplementationOnce(() => {
        Promise.resolve({});
      });
      const virtualAccountNumber = '900212382';

      await virtualAccountRepository.inquiryVirtualAccount(
        virtualAccountNumber
      );

      expect(httpClient.post).toHaveBeenCalledWith(
        `/virtual-accounts/inquiry`,
        {
          accountNumber: virtualAccountNumber
        }
      );
    });
  });
});
