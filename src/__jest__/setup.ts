import '../joiInitializer';
jest.mock('../logger');
// no type definitions available for expose-gc. Hence require
const garbageCollector = require('expose-gc/function');

jest.mock('../common/redis', () => ({
  get: jest.fn(),
  mGet: jest.fn(),
  set: jest.fn()
}));

jest.mock('../context', () => ({
  context: { getStore: () => {}, enterWith: () => {} }
}));

jest.mock('@dk/module-kafka', () => ({
  kafkaProducer: {
    init: jest.fn(() => ({
      sendSerializedValue: jest.fn()
    }))
  },
  kafkaConsumer: {
    init: jest.fn(() => ({
      connectSubscribeRun: jest.fn()
    }))
  }
}));

afterEach(() => {
  expect.hasAssertions();
});

afterAll(() => {
  try {
    garbageCollector();
  } catch {}
});

process.env.OSS_PARTNER_REPORT_ACCESS_KEY_ID = 'test';
process.env.OSS_PARTNER_REPORT_ACCESS_KEY_SECRET = 'test';
