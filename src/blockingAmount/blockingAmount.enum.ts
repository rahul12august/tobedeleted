export enum FieldsEnum {
  EXTERNAL_ID = 'externalId',
  BLOCKING_ID = 'blockingId'
}

export enum BlockingAmountStatusEnum {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE'
}
