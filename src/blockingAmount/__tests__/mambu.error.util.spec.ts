import { HttpError } from '@dk/module-httpclient';
import { ERROR_CODE } from '../../common/errors';
import mambuErrorConstants from '../mambu.error.constant';
import mambuErrorUtil from '../mambu.error.util';
import { TransferAppError } from '../../errors/AppError';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';
import { IWithdrawalInput } from '../../coreBanking/deposit.interface';
import logger from '../../logger';

describe('mambuErrorUtil', () => {
  it('should return true if it mambuErrorConstants.expiredAuthorizationHold', () => {
    // Given
    const error = new HttpError(400, {
      errors: [mambuErrorConstants.expiredAuthorizationHold]
    });
    let err;

    // When
    try {
      mambuErrorUtil.checkErrorReverseAuthorizationHold(error);
    } catch (e) {
      err = e;
    }

    // Then
    expect(err.errorCode).toBe(
      ERROR_CODE.MAMBU_TRANSACTION_AUTHORIZATION_HOLD_EXPIRED
    );
  });

  it('should return true if the error contain core banking error reversedAuthorizationHold', () => {
    // Given
    const error = new HttpError(400, {
      errors: [mambuErrorConstants.reversedAuthorizationHold]
    });
    let err;

    // When
    try {
      mambuErrorUtil.checkErrorReverseAuthorizationHold(error);
    } catch (e) {
      err = e;
    }

    // Then
    expect(err.errorCode).toBe(ERROR_CODE.MAMBU_TRANSACTION_ALREADY_REVERSED);
  });

  it('should return true if the error contain core banking error settledAuthorizationHold', () => {
    // Given
    const error = new HttpError(400, {
      errors: [mambuErrorConstants.settledAuthorizationHold]
    });
    let err;

    // When
    try {
      mambuErrorUtil.checkErrorReverseAuthorizationHold(error);
    } catch (e) {
      err = e;
    }

    // Then
    expect(err.errorCode).toBe(ERROR_CODE.MAMBU_TRANSACTION_ALREADY_SETTLED);
  });

  it('should return true if the error contain duplicate card transaction', () => {
    // Given
    const error = new HttpError(400, {
      errors: [mambuErrorConstants.duplicateCardTransaction]
    });
    let err;

    // When
    try {
      mambuErrorUtil.checkErrorReverseAuthorizationHold(error);
    } catch (e) {
      err = e;
    }

    // Then
    expect(err.errorCode).toBe(ERROR_CODE.MAMBU_DUPLICATE_CARD_TRANSACTION);
  });

  it('should return true if the error with idempotency key mismatched', () => {
    // Given
    const error = new HttpError(400, {
      errors: [mambuErrorConstants.invalidParameters]
    });
    let err;

    // When
    try {
      mambuErrorUtil.checkErrorReverseAuthorizationHold(error);
    } catch (e) {
      err = e;
    }

    // Then
    expect(err.errorCode).toBe(ERROR_CODE.MAMBU_INVALID_PARAMETERS);
  });

  describe('Throwing proper TransferAppError instances', () => {
    const withdrawalInput: IWithdrawalInput = {
      transactionDetails: {
        transactionChannelId: 'TFD20'
      },
      amount: 40_500,
      _Custom_Transaction_Details: {
        Transaction_Code: 'TC01',
        Reference_ID: 'REF01',
        Third_Party_Incoming_Id: 'EX01',
        Category_Code: 'C001',
        Payment_Service_Code: 'RTOL',
        Payment_Instruction_ID: '201910010000002',
        Additional_Information_1: 'Test',
        Additional_Information_2: 'Test',
        Additional_Information_3: 'Test',
        Additional_Information_4: 'Test'
      },
      _Custom_Source_Information: {
        Source_Account_Type: 'MA',
        Source_Account_No: '2',
        Source_CIF: 'CIF001'
      },
      _Custom_Target_Information: {
        Institutional_Bank_Code: 'BC002',
        Real_Target_Bank_Code: '542',
        Target_Account_Type: 'FS',
        Target_Account_No: 'TUBM658',
        Target_Account_Name: 'Mahesh',
        Target_Bank_Name: 'BANK ARTOS'
      }
    };

    let msgs: string[] = [];

    beforeEach(() => {
      msgs = [];
      (logger.info as jest.Mock).mockImplementation((msg: string) => {
        msgs.push(msg);
      });
    });

    it('Non Mambu errors are ignored', () => {
      mambuErrorUtil.tryThrowingProperTransferAppError(
        {
          name: 'Toto'
        },
        withdrawalInput
      );
      // The actual expect is that no other error is being thrown
      expect(1).toBeGreaterThan(0);
    });

    it('Non related Mambu errors are ignored', () => {
      mambuErrorUtil.tryThrowingProperTransferAppError(
        {
          error: {
            errors: [
              {
                errorCode: 3,
                errorReason: 'INVALID_API_OPERATION',
                errorSource: 'Some obscure source (does not matter)'
              }
            ]
          }
        },
        withdrawalInput
      );
      // The actual expect is that no other error is being thrown
      expect(1).toBeGreaterThan(0);
    });

    it('Negative balance error is properly mapped and trx amount included', () => {
      try {
        mambuErrorUtil.tryThrowingProperTransferAppError(
          {
            error: {
              errors: [
                {
                  errorCode: 401,
                  errorReason: 'BALANCE_BELOW_ZERO',
                  errorSource: 'Some obscure source (does not matter)'
                }
              ]
            }
          },
          withdrawalInput
        );
      } catch (e) {
        expect(e).toBeInstanceOf(TransferAppError);
        expect((e as TransferAppError).actor).toBe(
          TransferFailureReasonActor.SUBMITTER
        );
        expect((e as TransferAppError).errorCode).toBe(
          ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
        );
        expect(msgs).toContain(
          `Got a ${mambuErrorConstants.BALANCE_BELOW_ZERO} error for transaction amount ${withdrawalInput.amount}`
        );
      }
    });

    it('Negative balance error is identified amongst others', () => {
      try {
        mambuErrorUtil.tryThrowingProperTransferAppError(
          {
            error: {
              errors: [
                {
                  errorCode: 316,
                  errorReason: 'CLIENT_ID_ALREADY_IN_USE',
                  errorSource: 'Some obscure source (does not matter)'
                },
                {
                  errorCode: 401,
                  errorReason: 'BALANCE_BELOW_ZERO',
                  errorSource: 'Some obscure source (does not matter)'
                }
              ]
            }
          },
          withdrawalInput
        );
      } catch (e) {
        expect(e).toBeInstanceOf(TransferAppError);
        expect((e as TransferAppError).actor).toBe(
          TransferFailureReasonActor.SUBMITTER
        );
        expect((e as TransferAppError).errorCode).toBe(
          ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
        );
      }
    });
  });
});
