import blockingAmountService from '../blockingAmount.service';
import coreBankingCardService from '../../coreBanking/block.service';
import accountRepository from '../../account/account.repository';
import configurationService from '../../configuration/configuration.service';
import { BlockingAmountType } from '@dk/module-common';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import depositRepository from '../../coreBanking/deposit.repository';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

describe('blockingAmount.service', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('updateBlockingAmount', () => {
    const accountNumber = '123456';

    it('should increase block amount', async () => {
      const outputAmount = 10000001;
      const inputAmount = 10000000;
      const availableBalance = 100000000;

      depositRepository.getAvailableBalance = jest
        .fn()
        .mockResolvedValue(availableBalance);
      coreBankingCardService.increaseAuthorizationHold = jest.fn();

      const actual = await blockingAmountService.updateBlockingAmount(
        accountNumber,
        'blockId',
        outputAmount,
        inputAmount,
        'cardId'
      );

      expect(actual).toBe(undefined);
      expect(depositRepository.getAvailableBalance).toBeCalledWith(
        accountNumber
      );
    });

    it('should decrease block amount', async () => {
      const outputAmount = 10000000;
      const inputAmount = 10000001;
      const availableBalance = 100000000;

      depositRepository.getAvailableBalance = jest
        .fn()
        .mockResolvedValue(availableBalance);
      coreBankingCardService.decreaseAuthorizationHold = jest.fn();

      const actual = await blockingAmountService.updateBlockingAmount(
        accountNumber,
        'blockId',
        outputAmount,
        inputAmount,
        'cardId'
      );

      expect(actual).toBe(undefined);
      expect(depositRepository.getAvailableBalance).toBeCalledWith(
        accountNumber
      );
    });

    it('should throw error if available Balance is less than new blockingAmount', async () => {
      const outputAmount = 10000001;
      const inputAmount = 10000000;

      const availableBalance = 1000000;

      depositRepository.getAvailableBalance = jest
        .fn()
        .mockResolvedValue(availableBalance);
      coreBankingCardService.increaseAuthorizationHold = jest.fn();
      let error;

      try {
        await blockingAmountService.updateBlockingAmount(
          accountNumber,
          'blockId',
          outputAmount,
          inputAmount,
          'cardId'
        );
      } catch (e) {
        error = e;
      }

      expect(error.errorCode).toBe(ERROR_CODE.BLOCK_AMOUNT_IS_INSUFFICIENT);
    });
  });

  describe('createBlockingAmount', () => {
    const account = {
      cardId: 1234,
      id: 'account-id',
      currency: 'IDR'
    };
    const accountNumber = '123456';
    const blockingRequest = {
      amount: 1000000,
      name: 'jago',
      blockingPurposeCode: BlockingAmountType.ACCOUNT,
      mambuBlockingCode: 'TS10'
    };
    const expected = {
      accountId: 'account-id',
      amount: 1000000,
      blockingPurposeCode: 'ACCOUNT',
      cardId: 1234,
      mambuBlockingCode: 'TS10',
      merchantCategoryCode: 'TS10',
      name: 'jago',
      status: 'ACTIVE'
    };

    it('should create block amount', async () => {
      coreBankingCardService.createBlockingAmount = jest.fn();

      accountRepository.getActivatedAccount = jest
        .fn()
        .mockReturnValueOnce(account);
      configurationService.getSavingProduct = jest
        .fn()
        .mockReturnValueOnce({ blockingCapability: true });

      const actual = await blockingAmountService.createBlockingAmount(
        accountNumber,
        blockingRequest
      );

      expect(actual).toMatchObject(expected);
      expect(coreBankingCardService.createBlockingAmount).toHaveBeenCalledWith(
        account.cardId,
        expect.objectContaining({
          advice: false,
          amount: blockingRequest.amount,
          cardAcceptor: { mcc: blockingRequest.mambuBlockingCode },
          currencyCode: account.currency
        })
      );
    });

    it('should throw error if card is not present', async () => {
      accountRepository.getActivatedAccount = jest
        .fn()
        .mockResolvedValue({ id: 'account-id' });

      const fn = () =>
        blockingAmountService.createBlockingAmount(
          accountNumber,
          blockingRequest
        );

      await expect(fn()).rejects.toThrow(TransferAppError);
      await expect(fn()).rejects.toThrow(
        ERROR_CODE.NOT_ALLOWED_TO_CREATE_BLOCKING
      );
    });

    it('should throw an error if account type is not found', async () => {
      accountRepository.getActivatedAccount = jest
        .fn()
        .mockReturnValue({ cardId: 1234, id: 'account-id' });
      configurationService.getSavingProduct = jest
        .fn()
        .mockImplementation(() => {
          throw new TransferAppError(
            'Test error!',
            TransferFailureReasonActor.UNKNOWN,
            ERROR_CODE.SAVING_PRODUCT_NOT_FOUND
          );
        });

      const fn = () =>
        blockingAmountService.createBlockingAmount(
          accountNumber,
          blockingRequest
        );

      await expect(fn()).rejects.toThrow(TransferAppError);
      await expect(fn()).rejects.toThrow(ERROR_CODE.SAVING_PRODUCT_NOT_FOUND);
    });

    it('should throw error if account does not have blocking capability enabled', async () => {
      accountRepository.getActivatedAccount = jest
        .fn()
        .mockReturnValue({ cardId: 1234, id: 'account-id' });
      configurationService.getSavingProduct = jest
        .fn()
        .mockReturnValue({ blockingCapability: false });

      const fn = () =>
        blockingAmountService.createBlockingAmount(
          accountNumber,
          blockingRequest
        );

      await expect(fn()).rejects.toThrow(TransferAppError);
      await expect(fn()).rejects.toThrow(
        ERROR_CODE.NOT_ALLOWED_TO_CREATE_BLOCKING
      );
    });

    it('should throw error if card service returns error', async () => {
      accountRepository.getActivatedAccount = jest
        .fn()
        .mockReturnValue(account);
      configurationService.getSavingProduct = jest
        .fn()
        .mockReturnValue({ blockingCapability: true });
      coreBankingCardService.createBlockingAmount = jest
        .fn()
        .mockImplementation(() => {
          throw new TransferAppError(
            'Error from Mambu!',
            TransferFailureReasonActor.UNKNOWN,
            ERROR_CODE.INVALID_REQUEST
          );
        });

      const fn = () =>
        blockingAmountService.createBlockingAmount(
          accountNumber,
          blockingRequest
        );

      await expect(fn()).rejects.toThrow(TransferAppError);
      await expect(fn()).rejects.toThrow(
        ERROR_CODE.COREBANKING_BLOCK_AMOUNT_FAILED
      );
      expect(coreBankingCardService.createBlockingAmount).toHaveBeenCalledWith(
        account.cardId,
        expect.objectContaining({
          advice: false,
          amount: blockingRequest.amount,
          cardAcceptor: { mcc: blockingRequest.mambuBlockingCode },
          currencyCode: account.currency
        })
      );
    });
  });
});
