// https://api.mambu.com/ RestError
export interface MambuRestError {
  errorCode?: number;
  errorReason?: string;
  errorSource?: string;
}

// https://api.mambu.com/ ErrorResponse
export interface MambuErrorResponse {
  errors?: MambuRestError[];
}
