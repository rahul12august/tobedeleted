import { MambuRestError } from './mambu.error.interface';

const expiredAuthorizationHold: MambuRestError = {
  errorCode: 2708,
  errorSource: 'The authorization hold is in the EXPIRED state',
  errorReason: 'INVALID_AUTHORIZATION_HOLD_STATE'
};

const reversedAuthorizationHold: MambuRestError = {
  errorCode: 2708,
  errorSource: 'The authorization hold is in the REVERSED state',
  errorReason: 'INVALID_AUTHORIZATION_HOLD_STATE'
};

const settledAuthorizationHold: MambuRestError = {
  errorCode: 2708,
  errorSource: 'The authorization hold is in the SETTLED state',
  errorReason: 'INVALID_AUTHORIZATION_HOLD_STATE'
};

const duplicateCardTransaction: MambuRestError = {
  errorCode: 2705,
  errorSource: 'The card transaction already exists: cardReferenceToken=BLOCK',
  errorReason: 'DUPLICATE_CARD_TRANSACTION'
};

const invalidParameters: MambuRestError = {
  errorCode: 4,
  errorSource:
    'Idempotent request does not match existing request, use a different idempotency-key when changing the request',
  errorReason: 'INVALID_PARAMETERS'
};

const BALANCE_BELOW_ZERO = 'BALANCE_BELOW_ZERO';

const mambuErrorConstants = {
  expiredAuthorizationHold,
  reversedAuthorizationHold,
  settledAuthorizationHold,
  duplicateCardTransaction,
  invalidParameters,
  BALANCE_BELOW_ZERO
};

export default mambuErrorConstants;
