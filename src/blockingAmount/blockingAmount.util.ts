import { ErrorList, ERROR_CODE } from '../common/errors';
import { IErrorDetail } from '@dk/module-common/dist/error/error.interface';

export const getErrorDetails = (
  errorCode: ERROR_CODE,
  key: string,
  message?: string
): IErrorDetail[] => [
  {
    message: message || ErrorList[errorCode].message,
    code: errorCode,
    key: key
  }
];
