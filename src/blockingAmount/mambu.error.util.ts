import { HttpError } from '@dk/module-httpclient';
import mambuErrorConstants from './mambu.error.constant';
import _ from 'lodash';
import { MambuErrorResponse, MambuRestError } from './mambu.error.interface';
import logger from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';
import { ITransactionInput } from '../coreBanking/deposit.interface';

const errorResponseAsMambuResponseError = (
  error: any,
  checker: (errors: MambuRestError[]) => boolean
) => (error.error as MambuErrorResponse).errors && checker(error.error.errors);

const isItExpiredAuthorizationHoldErrorResponse = (error: any) =>
  errorResponseAsMambuResponseError(error, errors =>
    errors.some(errorDetail =>
      _.isEqual(errorDetail, mambuErrorConstants.expiredAuthorizationHold)
    )
  );

const isItReversedAuthorizationHoldErrorResponse = (error: any) =>
  errorResponseAsMambuResponseError(error, errors =>
    errors.some(errorDetail =>
      _.isEqual(errorDetail, mambuErrorConstants.reversedAuthorizationHold)
    )
  );

const isItSettledAuthorizationHoldErrorResponse = (error: any) =>
  errorResponseAsMambuResponseError(error, errors =>
    errors.some(errorDetail =>
      _.isEqual(errorDetail, mambuErrorConstants.settledAuthorizationHold)
    )
  );
const isItDuplicateCardTransactionErrorResponse = (error: any) =>
  errorResponseAsMambuResponseError(error, errors =>
    errors.some(errorDetail =>
      _.isEqual(errorDetail, mambuErrorConstants.duplicateCardTransaction)
    )
  );

const isItInvalidParametersErrorResponse = (error: any) =>
  errorResponseAsMambuResponseError(error, errors =>
    errors.some(errorDetail =>
      _.isEqual(errorDetail, mambuErrorConstants.invalidParameters)
    )
  );

const checkErrorReverseAuthorizationHold = (error: HttpError) => {
  if (isItExpiredAuthorizationHoldErrorResponse(error)) {
    const detail = 'Failed reverse authorization hold because its expired';
    logger.warn(detail);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.MAMBU_TRANSACTION_AUTHORIZATION_HOLD_EXPIRED
    );
  }

  if (isItReversedAuthorizationHoldErrorResponse(error)) {
    const detail =
      'Failed reverse authorization hold because its already reversed';
    logger.warn(detail);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.MAMBU_TRANSACTION_ALREADY_REVERSED
    );
  }

  if (isItSettledAuthorizationHoldErrorResponse(error)) {
    const detail =
      'Failed settle authorization hold because its already settled';
    logger.warn(detail);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.MAMBU_TRANSACTION_ALREADY_SETTLED
    );
  }

  if (isItDuplicateCardTransactionErrorResponse(error)) {
    const detail = 'Duplicate card transaction found';
    logger.warn(detail);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.MAMBU_DUPLICATE_CARD_TRANSACTION
    );
  }

  if (isItInvalidParametersErrorResponse(error)) {
    const detail = 'Idempotent request does not match existing request';
    logger.warn(detail);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.MAMBU_INVALID_PARAMETERS
    );
  }

  throw error;
};

/**
 * Checks the argument error and tries to asses if the argument is a valid error Mambu
 * response according to the published specs (https://api.mambu.com/#tocserrorresponse)
 * In case the error is conforming with the specified model and if the error can be
 * properly mapped to a resonable TransferAppError, such an error will be thrown.
 * Properly mapping means identifying a valid correspondence between the error code in Mambu
 * and the local model such as actor and cause. In some cases, such as negative balance the
 * mapping is straight forward.
 *
 * In case of model mismatch or of inability to identify a proper correspondende no error
 * is thrown.
 * @param e The original error surfaced during http communication
 * @param trxInput The original transaction request input
 */
const tryThrowingProperTransferAppError = (
  e: any,
  trxInput: ITransactionInput
): void => {
  if (
    e.error &&
    errorResponseAsMambuResponseError(e, errors =>
      errors.some(errorInstance =>
        _.isEqual(
          errorInstance.errorReason,
          mambuErrorConstants.BALANCE_BELOW_ZERO
        )
      )
    )
  ) {
    logger.info(
      `Got a ${mambuErrorConstants.BALANCE_BELOW_ZERO} error for transaction amount ${trxInput.amount}`
    );
    throw new TransferAppError(
      'Negative balance',
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
    );
  }
};

const mambuErrorUtil = {
  checkErrorReverseAuthorizationHold,
  tryThrowingProperTransferAppError
};

export default mambuErrorUtil;
