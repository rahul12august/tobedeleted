import { ERROR_CODE } from '../common/errors';
import configurationService from '../configuration/configuration.service';
import {
  RetryableTransferAppError,
  TransferAppError
} from '../errors/AppError';
import accountRepository from '../account/account.repository';
import {
  IAccountInfo,
  IBlockingAmountResponse,
  IBlockingRequestPayload
} from '../account/account.type';
import {
  IBlockingAmountModel,
  IBlockingInfo
} from '../blockingAmount/blockingAmount.type';
import uuidv4 from 'uuid/v4';
import logger, { wrapLogs } from '../logger';
import coreBankingCardService from '../coreBanking/block.service';
import { getErrorDetails } from '../errors/error.util';
import { BlockingAmountStatusEnum, FieldsEnum } from './blockingAmount.enum';
import depositRepository from '../coreBanking/deposit.repository';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const createBlockingAmountInCoreBanking = async (
  account: IAccountInfo,
  blockingInfo: IBlockingAmountModel,
  cardReferenceToken: string
): Promise<void> => {
  const inputBlockingInfo: IBlockingInfo = {
    externalReferenceId: blockingInfo.id,
    amount: blockingInfo.amount,
    advice: false, // note: "false" it's main on coreBanking will validate input amount compare with the balance of account
    currencyCode: account.currency
  };

  if (blockingInfo.merchantCategoryCode) {
    inputBlockingInfo.cardAcceptor = {
      mcc: blockingInfo.merchantCategoryCode
    };
    logger.info(
      `createBlockingAmountInCoreBanking: Mambu blocking code: ${inputBlockingInfo.cardAcceptor.mcc}`
    );
  } else {
    logger.info(
      `createBlockingAmountInCoreBanking: Mambu blocking code will set as default, merchantCategoryCode not provided.`
    );
  }

  try {
    await coreBankingCardService.createBlockingAmount(
      cardReferenceToken,
      inputBlockingInfo
    );
    blockingInfo.idempotencyKey = inputBlockingInfo.idempotencyKey;
  } catch (error) {
    let detail, actor;
    if (error instanceof TransferAppError) {
      detail = error.detail;
      actor = error.actor;
    } else {
      detail = error.message;
      actor = TransferFailureReasonActor.UNKNOWN;
    }
    throw new RetryableTransferAppError(
      detail,
      actor,
      ERROR_CODE.COREBANKING_BLOCK_AMOUNT_FAILED,
      false,
      getErrorDetails(
        ERROR_CODE.CAN_NOT_CREATE_BLOCK_AMOUNT,
        FieldsEnum.BLOCKING_ID,
        blockingInfo.id
      )
    );
  }
};

const validateAccountType = async (accountType: string): Promise<void> => {
  const savingProduct = await configurationService.getSavingProduct(
    accountType
  );

  if (!savingProduct.blockingCapability) {
    throw new RetryableTransferAppError(
      `Missing blocking capability from saving product: ${savingProduct.code}!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.NOT_ALLOWED_TO_CREATE_BLOCKING,
      false
    );
  }
};

const createBlockingAmount = async (
  accountNumber: string,
  blockingAmountInput: IBlockingRequestPayload
): Promise<IBlockingAmountResponse> => {
  const account = await accountRepository.getActivatedAccount(accountNumber);
  if (!account.cardId) {
    throw new RetryableTransferAppError(
      `Not allowed to create blocking amount without card ID on account: ${account.id}!`,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.NOT_ALLOWED_TO_CREATE_BLOCKING,
      false
    );
  }
  await validateAccountType(account.accountType);
  const blockingId = uuidv4();
  const blockingAmountModel: IBlockingAmountModel = {
    ...blockingAmountInput,
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    accountId: account.id!,
    status: BlockingAmountStatusEnum.ACTIVE,
    id: blockingId
  };

  if (blockingAmountInput.mambuBlockingCode) {
    blockingAmountModel.merchantCategoryCode =
      blockingAmountInput.mambuBlockingCode;
  }

  await createBlockingAmountInCoreBanking(
    account,
    blockingAmountModel,
    account.cardId
  );

  return { ...blockingAmountModel, cardId: account.cardId };
};

const validateBlockingAmountBalance = async (
  blockingAmountInput: number,
  accountNumber: string
) => {
  const depositBalance = await depositRepository.getAvailableBalance(
    accountNumber
  );
  if (blockingAmountInput > depositBalance) {
    throw new TransferAppError(
      'Deposit balance is less then blocking amount!',
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.BLOCK_AMOUNT_IS_INSUFFICIENT
    );
  }
};

const updateBlockingAmount = async (
  accountNumber: string,
  blockId: string,
  newBlockingAmount: number,
  oldBlockedAmount: number,
  cardId: string
): Promise<void> => {
  await validateBlockingAmountBalance(newBlockingAmount, accountNumber);
  logger.info(
    `updateBlockingAmount: Adjusted blocking amount with accountNumber: ${accountNumber} blockId: ${blockId} and cardId: ${cardId}`
  );
  const amountDiff = newBlockingAmount - oldBlockedAmount;
  if (amountDiff > 0) {
    await coreBankingCardService.increaseAuthorizationHold(
      cardId,
      blockId,
      amountDiff
    );
  } else if (amountDiff < 0) {
    await coreBankingCardService.decreaseAuthorizationHold(
      cardId,
      blockId,
      -amountDiff,
      false
    );
  }
};

const blockingAmountService = {
  createBlockingAmount,
  updateBlockingAmount
};
export default wrapLogs(blockingAmountService);
