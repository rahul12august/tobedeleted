import { BlockingAmountStatusEnum } from '../blockingAmount/blockingAmount.enum';
import Enum from '@dk/module-common';
export interface IBlockingInfo {
  externalReferenceId: string;
  amount: number;
  advice: boolean;
  cardAcceptor?: {
    mcc: string;
  };
  currencyCode: string;
  idempotencyKey?: string;
}

export interface IBlockingAmountModel {
  id: string;
  accountId: string;
  amount: number;
  name: string;
  releaseDate?: Date;
  status: BlockingAmountStatusEnum;
  merchantCategoryCode?: string;
  blockingPurposeCode: Enum.BlockingAmountType;
  externalId?: string;
  idempotencyKey?: string;
}
