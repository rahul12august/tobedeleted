import {
  BankChannelEnum,
  PaymentServiceTypeEnum,
  WorkingScheduleCategory
} from '@dk/module-common';
import moment from 'moment-timezone';
import { isEmpty } from 'lodash';
import {
  BIFAST_PROXY_BANK_CODE,
  BIFAST_PROXY_TYPES
} from '../bifast/bifast.constant';
import { BeneficiaryProxyType } from '../bifast/bifast.enum';
import {
  IHoliday,
  ILimitGroup,
  IPaymentConfigRule,
  IPaymentServiceMapping
} from '../configuration/configuration.type';
import limitGroup from '../limitGroup/limitGroup';
import {
  BILL_LIMIT_GROUP_CODE,
  BI_FAST_MAX_TRANSACTION_AMOUNT,
  BIFAST_LIMIT_GROUP_CODE,
  JAGO_BANK_CODE,
  FORMAT_DATE,
  FORMAT_DAY_NAME,
  FORMAT_HOUR_MINUS_24H,
  TMZ,
  JAGO_SHARIA_BANK_CODE
} from './transaction.constant';
import {
  ILimitGroupCodesMap,
  RuleConditionStatus,
  RuleConditionFn,
  ITransaction,
  DailyUsages,
  ExecutionTime,
  IBiFastEnabled,
  FilteredMappingResult
} from './transaction.type';
import configurationRepository from '../configuration/configuration.repository';
import { ERROR_CODE } from '../common/errors';
import { RuleConditionState } from './transaction.conditions.enum';
import transactionConditionHelperService from './transaction.conditions.helper';
enum conditionStaticValue {
  ANY = 'ANY',
  YES = 'YES',
  NO = 'NO'
}

const isProxyBankCode = (transactionModel: ITransaction): boolean => {
  if (
    transactionModel.beneficiaryBankCode &&
    transactionModel.beneficiaryBankCode == BIFAST_PROXY_BANK_CODE
  ) {
    return true;
  }
  return false;
};

const isProxyAdditionalInformationAvailable = (
  transactionModel: ITransaction
): boolean => {
  return (
    !isEmpty(transactionModel.additionalInformation3) &&
    !isEmpty(transactionModel.additionalInformation4) &&
    BIFAST_PROXY_TYPES.includes(
      transactionModel.additionalInformation3 as BeneficiaryProxyType
    )
  );
};

const isBiFastProxy = (transactionModel: ITransaction): boolean =>
  isProxyBankCode(transactionModel) || // Either BankCode is BC000 ( BIFastProxyBankCode )
  isProxyAdditionalInformationAvailable(transactionModel); // Valid additional information 3 and 4 available

const createStatus = (
  matcherFn: () => boolean,
  successMessage: string,
  failureMessage: string,
  errorCode: ERROR_CODE
): RuleConditionStatus => {
  const matched = matcherFn();
  if (matched) {
    return new RuleConditionStatus(successMessage);
  }
  return new RuleConditionStatus(failureMessage, errorCode);
};

const createStatusWithMessage = (
  matcherFn: () => boolean,
  actual: string | undefined,
  expected: string | conditionStaticValue.ANY | undefined,
  messageBase: string,
  errorCode: ERROR_CODE
): RuleConditionStatus => {
  return createStatus(
    matcherFn,
    `${messageBase} check is passed. actual=${actual} expected=${expected}`,
    `${messageBase} check is failed. actual=${actual} expected=${expected}`,
    errorCode
  );
};

const equals = (
  actual: string | undefined,
  expected: string | conditionStaticValue.ANY | undefined,
  messageBase: string,
  errorCode: ERROR_CODE
): RuleConditionStatus => {
  return createStatusWithMessage(
    () => {
      return actual === expected;
    },
    actual,
    expected,
    messageBase,
    errorCode
  );
};

const equalsOrAny = (
  actual: string | undefined,
  expected: string | conditionStaticValue.ANY,
  messageBase: string,
  errorCode: ERROR_CODE
): RuleConditionStatus => {
  return createStatusWithMessage(
    () => {
      return actual === expected || expected === conditionStaticValue.ANY;
    },
    actual,
    expected,
    messageBase,
    errorCode
  );
};

const paymentServiceTypeMatches: RuleConditionFn = (
  input,
  config
): RuleConditionStatus => {
  return equals(
    input.paymentServiceType,
    config.paymentServiceType,
    'Payment service type',
    ERROR_CODE.RECOMMENDATION_PAYMENT_SEVICE_TYPE_MISMATCH
  );
};

const sourceBankCodeChannelMatches: RuleConditionFn = (
  input,
  config
): RuleConditionStatus =>
  equalsOrAny(
    input.sourceBankCodeChannel,
    config.source,
    'Source bank code channel',
    ERROR_CODE.RECOMMENDATION_SOURCE_BANK_CODE_CHANNEL_MISMATCH
  );

const beneficiaryBankCodeChannelMatches: RuleConditionFn = (
  input,
  config
): RuleConditionStatus =>
  equalsOrAny(
    input.beneficiaryBankCodeChannel,
    config.target,
    'Beneficiary bank code channel',
    ERROR_CODE.RECOMMENDATION_BENEFICIARY_BANK_CODE_CHANNEL_MISMATCH
  );

const sourceAccountTypeMatches: RuleConditionFn = (
  input,
  config
): RuleConditionStatus => {
  for (let accType of config.sourceAccType) {
    const status = equalsOrAny(
      input.sourceAccountType,
      accType,
      'Source account type',
      ERROR_CODE.RECOMMENDATION_SOURCE_ACCOUNT_TYPE_MISMATCH
    );
    if (status.isSuccess()) {
      return status;
    }
  }
  return new RuleConditionStatus(
    `Source account type: ${input.sourceAccountType} does not match any of the required ones: ${config.sourceAccType}`,
    ERROR_CODE.RECOMMENDATION_SOURCE_ACCOUNT_TYPE_MISMATCH
  );
};

const targetAccountTypeMatches: RuleConditionFn = (
  input,
  config
): RuleConditionStatus => {
  for (let accType of config.targetAccType) {
    const status = equalsOrAny(
      input.beneficiaryAccountType,
      accType,
      'Target account type',
      ERROR_CODE.RECOMMENDATION_BENEFICIARY_ACCOUNT_TYPE_MISMATCH
    );
    if (status.isSuccess()) {
      return status;
    }
  }
  return new RuleConditionStatus(
    `Beneficiary account type: ${input.beneficiaryAccountType} does not match any of the requireds: ${config.targetAccType} `,
    ERROR_CODE.RECOMMENDATION_BENEFICIARY_ACCOUNT_TYPE_MISMATCH
  );
};

const cifMatches: RuleConditionFn = (input, config): RuleConditionStatus => {
  const equalMsg = `Source and beneficiary CIF are equal: ${input.sourceCIF}`;
  const differMsg = `Source and beneficiary CIF differ. Source: ${input.sourceCIF} beneficiary: ${input.beneficiaryCIF}`;

  switch (config.sameCIF) {
    case conditionStaticValue.YES:
      return createStatus(
        () => {
          return input.sourceCIF === input.beneficiaryCIF;
        },
        equalMsg,
        differMsg,
        ERROR_CODE.RECOMMENDATION_CIF_MISMATCH
      );
    case conditionStaticValue.NO:
      return createStatus(
        () => {
          return input.sourceCIF !== input.beneficiaryCIF;
        },
        differMsg,
        equalMsg,
        ERROR_CODE.RECOMMENDATION_CIF_MISMATCH
      );
  }
  return new RuleConditionStatus(
    'No preference for source and beneficiary CIF.'
  );
};

const isAmountInRange: RuleConditionFn = (input, config): RuleConditionStatus =>
  createStatus(
    () =>
      input.transactionAmount >= config.amountRangeFrom &&
      input.transactionAmount <= config.amountRangeTo,
    'transaction amount: ' +
      input.transactionAmount +
      ' is in the expected range: [' +
      config.amountRangeFrom +
      ' , ' +
      config.amountRangeTo +
      ']',
    'transaction amount: ' +
      input.transactionAmount +
      ' is not in the expected range: [' +
      config.amountRangeFrom +
      ' , ' +
      config.amountRangeTo +
      ']',
    ERROR_CODE.RECOMMENDATION_TRANSACTION_AMOUNT_IS_OUT_OF_RANGE
  );

const negate = (state: RuleConditionState): RuleConditionState => {
  return state == RuleConditionState.ACCEPTED
    ? RuleConditionState.DECLINED
    : RuleConditionState.ACCEPTED;
};
/**
 *
 * @param dailyUsage conditionStaticValue enum
 * @param transactionAmount amount to be currently transfered
 * @param dailyAmountSumSoFar the sum of the transaction amounts so far today - without the current one.
 * @param dailyLimit the daily limit.
 * @returns the condition state
 */
const evalLimit = (
  dailyUsage: string,
  transactionAmount: number,
  dailyAmountSumSoFar: number,
  limitGroup: ILimitGroup
): RuleConditionState => {
  if (dailyUsage === conditionStaticValue.ANY) {
    return RuleConditionState.ACCEPTED;
  }
  const state = transactionConditionHelperService.compareAmountWithDailyLimit(
    limitGroup,
    transactionAmount,
    dailyAmountSumSoFar
  );

  if (dailyUsage === conditionStaticValue.NO) {
    return negate(state);
  }

  return state;
};

const createDailyLimitResult = (
  dailyUsage: string,
  limitGroup: ILimitGroup,
  currentAmount: number,
  dailyAmountSumSoFar: number,
  tag: string
): RuleConditionStatus => {
  const dailyLimit: number = limitGroup.dailyLimitAmount;
  const state = evalLimit(
    dailyUsage,
    currentAmount,
    dailyAmountSumSoFar,
    limitGroup
  );

  return transactionConditionHelperService.constructDailyLimitEvaluatedRuleConditionStatus(
    currentAmount,
    dailyAmountSumSoFar,
    tag,
    dailyLimit,
    ERROR_CODE.RECOMMENDATION_OVER_DAILY_LIMIT,
    ERROR_CODE.RECOMMENDATION_OVER_DAILY_LIMIT_RETRY_TOMORROW,
    state
  );
};

const checkRTOLDailyLimit: RuleConditionFn = (
  input,
  config,
  limitGroupCodesMap
): RuleConditionStatus => {
  const limitGroupCode = limitGroup.getLimitGroupCodes(input.paymentServiceCode)
    .RTOL_LIMIT_GROUP_CODE;

  return createDailyLimitResult(
    config.rtolDailyUsage,
    limitGroupCodesMap[limitGroupCode],
    input.transactionAmount,
    input.rtolDailyUsage,
    'RTOL'
  );
};

const checkSKNDailyLimit: RuleConditionFn = (
  input,
  config,
  limitGroupCodesMap
): RuleConditionStatus => {
  const limitGroupCode = limitGroup.getLimitGroupCodes(input.paymentServiceCode)
    .SKN_LIMIT_GROUP_CODE;

  return createDailyLimitResult(
    config.sknDailyUsage,
    limitGroupCodesMap[limitGroupCode],
    input.transactionAmount,
    input.sknDailyUsage,
    'SKN'
  );
};

const checkRTGSDailyLimit: RuleConditionFn = (
  input,
  config,
  limitGroupCodesMap
): RuleConditionStatus => {
  const limitGroupCode = limitGroup.getLimitGroupCodes(input.paymentServiceCode)
    .RTGS_LIMIT_GROUP_CODE;

  return createDailyLimitResult(
    config.rtgsDailyUsage,
    limitGroupCodesMap[limitGroupCode],
    input.transactionAmount,
    input.rtgsDailyUsage,
    'RTGS'
  );
};

const checkBillPaymentDailyLimit: RuleConditionFn = (
  input,
  _config,
  limitGroupCodesMap
): RuleConditionStatus => {
  if (
    input.beneficiaryBankCodeChannel === BankChannelEnum.GOBILLS ||
    input.beneficiaryBankCodeChannel === BankChannelEnum.TOKOPEDIA
  ) {
    const result = transactionConditionHelperService.compareAmountWithDailyLimit(
      limitGroupCodesMap[BILL_LIMIT_GROUP_CODE],
      input.transactionAmount,
      input.billDailyUsage
    );
    if (result === RuleConditionState.ACCEPTED) {
      return new RuleConditionStatus(
        `Amount ${input.transactionAmount} is within the bill payment daily limit: ${input.billDailyUsage} .`
      );
    }
    return new RuleConditionStatus(
      `Amount ${input.transactionAmount} exceeded the bill payment daily limit: ${input.billDailyUsage} .`,
      ERROR_CODE.RECOMMENDATION_OVER_BILL_PAYMENT_LIMIT
    );
  }
  return new RuleConditionStatus('Has no bill payment daily limit.');
};

const getBiFastConfig = (
  input: ITransaction & DailyUsages & ExecutionTime & IBiFastEnabled,
  config: IPaymentConfigRule
) => {
  const serviceRecommendationContainsBIFAST =
    config.serviceRecommendation.filter(
      paymentServiceCode =>
        paymentServiceCode.indexOf(BankChannelEnum.BIFAST) !== -1
    ).length > 0;

  const isBeneficiarySupportsBIFast =
    input.beneficiarySupportedChannels &&
    input.beneficiarySupportedChannels.includes(BankChannelEnum.BIFAST);
  const isBeneficiaryInternalChannel = input.beneficiaryBankCodeChannel?.includes(
    BankChannelEnum.INTERNAL
  );
  return {
    isBiFastEnabled: input.isBiFastEnabled,
    serviceRecommendationContainsBIFAST,
    isBeneficiarySupportsBIFast,
    isBeneficiaryInternalChannel
  };
};

const checkBiFastProxy: RuleConditionFn = (
  input,
  config
): RuleConditionStatus => {
  const {
    isBiFastEnabled,
    serviceRecommendationContainsBIFAST
  } = getBiFastConfig(input, config);
  let passed = true;
  if (isBiFastEnabled) {
    if (
      isBiFastProxy(input) &&
      !serviceRecommendationContainsBIFAST &&
      input.beneficiaryBankCode !== JAGO_BANK_CODE &&
      input.beneficiaryBankCode !== JAGO_SHARIA_BANK_CODE
    ) {
      passed = false;
    }
  } else {
    passed = !isBiFastProxy(input);
  }

  if (passed) {
    return new RuleConditionStatus('BI FAST proxy is available.');
  }
  return new RuleConditionStatus(
    'BI FAST proxy is not available.',
    ERROR_CODE.RECOMMENDATION_BIFAST_PROXY_UNAVAILABLE
  );
};

const isSupportBiFastChannel: RuleConditionFn = (
  input: ITransaction & DailyUsages & ExecutionTime & IBiFastEnabled,
  config: IPaymentConfigRule,
  limitGroupCodesMap: ILimitGroupCodesMap
): RuleConditionStatus => {
  const {
    isBiFastEnabled,
    serviceRecommendationContainsBIFAST,
    isBeneficiarySupportsBIFast,
    isBeneficiaryInternalChannel
  } = getBiFastConfig(input, config);

  // Hardcode to disable Sharia
  // if (input.isShariaCustomer) {
  //   if (serviceRecommendationContainsBIFAST) {
  //     return false;
  //   }
  //   return true;
  // }

  // if BIFAST could be recommended but...
  if (serviceRecommendationContainsBIFAST) {
    if (
      !isBiFastEnabled ||
      (!isBeneficiarySupportsBIFast && !isBeneficiaryInternalChannel) // shouldn't we use || instead of && ?
    ) {
      return new RuleConditionStatus(
        'BI Fast is Disabled or beneficiary bank does not support it.',
        ERROR_CODE.RECOMMENDATION_BIFAST_UNSUPPORTED
      );
    }
  }

  // When BI Fast is Enabled & serviceRecommendaton contains BIFAST check daily limit
  if (isBiFastEnabled && serviceRecommendationContainsBIFAST) {
    return createDailyLimitResult(
      '',
      limitGroupCodesMap[BIFAST_LIMIT_GROUP_CODE],
      input.transactionAmount,
      input.bifastDailyUsage,
      'BI Fast'
    );
  }

  if (
    isBiFastEnabled &&
    isBeneficiarySupportsBIFast &&
    !serviceRecommendationContainsBIFAST &&
    input.transactionAmount <= BI_FAST_MAX_TRANSACTION_AMOUNT &&
    input.paymentServiceType !== PaymentServiceTypeEnum.RDN
  ) {
    const biFastState = transactionConditionHelperService.compareAmountWithDailyLimit(
      limitGroupCodesMap[BIFAST_LIMIT_GROUP_CODE],
      input.transactionAmount,
      input.bifastDailyUsage
    );

    const isRtolRecommended =
      config.serviceRecommendation.filter(
        paymentServiceCode => paymentServiceCode === 'RTOL'
      ).length > 0;

    if (isRtolRecommended) {
      if (biFastState !== RuleConditionState.ACCEPTED) {
        return new RuleConditionStatus(
          'As BI Fast daily limit is exhausted, RTOL is selected.'
        );
      }
      return new RuleConditionStatus(
        'Should not use RTOL as still within BI-FAST limit.',
        ERROR_CODE.RECOMMENDATION_STILL_IN_BIFAST_LIMIT
      );
    }

    const isRtgsOrSKN =
      config.serviceRecommendation.filter(paymentServiceCode =>
        ['RTGS', 'RTGS_SHARIA', 'SKN', 'SKN_SHARIA'].includes(
          paymentServiceCode
        )
      ).length > 0;
    const acceptedForSKN =
      biFastState === RuleConditionState.ACCEPTED ? !isRtgsOrSKN : isRtgsOrSKN;
    if (acceptedForSKN) {
      return new RuleConditionStatus(
        'As BI Fast daily limit is exhausted, RTGS or SKN is selected.'
      );
    }

    return new RuleConditionStatus(
      'Should not use RTGS or SKN as still within BI-FAST limit.',
      ERROR_CODE.RECOMMENDATION_STILL_IN_BIFAST_LIMIT
    );
  }
  return new RuleConditionStatus('BI Fast checks are passed.');
};

const isExecutionDuringWorkingTime = async (
  executionTime: Date,
  cutOffTime?: string
): Promise<boolean> => {
  const momentDate = moment(executionTime, FORMAT_DATE).tz(TMZ);
  let dateFormat = momentDate.format(FORMAT_DATE);
  const dayName = momentDate.format(FORMAT_DAY_NAME);
  const timeWorkingDays = await configurationRepository.getWorkingTime(
    cutOffTime as WorkingScheduleCategory
  );
  const timeWorkingOfDay = timeWorkingDays.find(
    workingDay => workingDay.dayName === dayName
  );
  if (timeWorkingOfDay) {
    let startDateWorking = moment.tz(
      `${dateFormat} ${timeWorkingOfDay.startTime}`,
      `${FORMAT_DATE} ${FORMAT_HOUR_MINUS_24H}`,
      TMZ
    );
    let endDateWorking = moment.tz(
      `${dateFormat} ${timeWorkingOfDay.endTime}`,
      `${FORMAT_DATE} ${FORMAT_HOUR_MINUS_24H}`,
      TMZ
    );
    return !(
      momentDate.isBefore(startDateWorking) ||
      momentDate.isAfter(endDateWorking)
    );
  }
  return false;
};

const filterPaymentServiceMappingsByWoringDateAndTime = async (
  paymentServiceMappings: IPaymentServiceMapping[],
  executionTime: Date
): Promise<IPaymentServiceMapping[]> => {
  let holidayList: IHoliday[] | null = null;
  let invalidWorkingTime: string[] = [];
  for (let item of paymentServiceMappings) {
    const cutOffTime = item.cutOffTime;
    if (
      WorkingScheduleCategory.SKN.valueOf() !== cutOffTime &&
      WorkingScheduleCategory.RTGS.valueOf() !== cutOffTime
    ) {
      continue;
    }

    if (holidayList === null) {
      holidayList = await configurationRepository.getHolidaysByDateRange(
        executionTime,
        executionTime
      );
    }
    if (holidayList && holidayList.length > 0) {
      invalidWorkingTime.push(cutOffTime);
    }

    const workingTime = await isExecutionDuringWorkingTime(
      executionTime,
      cutOffTime
    );
    if (!workingTime) {
      invalidWorkingTime.push(cutOffTime);
    }
  }

  if (isEmpty(invalidWorkingTime)) {
    return paymentServiceMappings;
  }

  const updatedServiceMapping = paymentServiceMappings.filter(
    (item: IPaymentServiceMapping) =>
      item.cutOffTime && !invalidWorkingTime.includes(item.cutOffTime)
  );
  return updatedServiceMapping;
};

export const filterByDateAndWorkingTime = async (
  paymentServiceMappings: IPaymentServiceMapping[],
  date: Date
): Promise<FilteredMappingResult> => {
  const filteredMappings = await filterPaymentServiceMappingsByWoringDateAndTime(
    paymentServiceMappings,
    date
  );
  const status: RuleConditionStatus = !isEmpty(filteredMappings)
    ? new RuleConditionStatus('Working date and time rules are passed.')
    : new RuleConditionStatus(
        'Transaction is rejected due to the working date and time rules. Try again tomorrow.',
        ERROR_CODE.RECOMMENDATION_REJECTED_BY_WORKING_DAY_RULES
      );
  return { status: status, mappings: filteredMappings };
};

export const conditions = {
  paymentServiceTypeMatches,
  beneficiaryBankCodeChannelMatches,
  sourceBankCodeChannelMatches,
  sourceAccountTypeMatches,
  targetAccountTypeMatches,
  cifMatches,
  isAmountInRange,
  checkRTOLDailyLimit,
  checkSKNDailyLimit,
  checkRTGSDailyLimit,
  checkBillPaymentDailyLimit,
  isSupportBiFastChannel,
  checkBiFastProxy
};

export default Object.values(conditions);
