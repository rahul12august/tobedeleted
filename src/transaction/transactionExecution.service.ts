import logger, { wrapLogs } from '../logger';
import {
  ITransactionExecutionHooks,
  ITransactionInput,
  ITransactionStatusResponse,
  ITransactionSubmissionTemplate,
  RecommendationService
} from './transaction.type';
import {
  isRetryableError,
  RetryableTransferAppError,
  TransferAppError
} from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import {
  getErrorMessage,
  isBIFast,
  isGlTransactionDetailFieldExists,
  loanAdditionalPayloadValidator,
  mapToQrisTransferDb,
  qrisAdditionalPayloadValidator,
  rounding
} from './transaction.util';
import { IFailureReasonModel, ITransactionModel } from './transaction.model';
import { generateOutgoingId } from '../common/externalIdGenerator.util';
import transferConstant, {
  BANK_INCOME,
  IGNORE_RDN_EXCEPTIONS,
  REVERSAL_TC_SUFFIX,
  RTOL_BIFAST_FEE
} from './transaction.constant';
import transactionHelper from './transaction.helper';
import transactionRepository from './transaction.repository';
import transactionProducer from './transaction.producer';
import configurationRepository from '../configuration/configuration.repository';
import awardRepository from '../award/award.repository';
import outgoingSubmissionTemplateService from './transactionSubmissionTemplate.service';
import helper from './transactionCoreBanking.helper';
import transactionReversalHelper from './transactionReversal.helper';
import transactionBlockingHelper from './transactionBlocking.helper';
import {
  AccountType,
  BankChannelEnum,
  BankNetworkEnum,
  Enum,
  PaymentServiceTypeEnum,
  Rail
} from '@dk/module-common';
import transactionRecommendService from './transactionRecommend.service';
import {
  ExecutionTypeEnum,
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from './transaction.enum';
import internalCreditSubmissionTemplate from './transactionCoreBankingCreditSubmission.service';
import { getApplicable } from './feeExecution/feeExecution.templates';
import {
  getTransactionInfoTracker,
  updateRequestIdInRedis
} from '../common/contextHandler';
import _, { filter, get, identity, isEmpty, isUndefined, pickBy } from 'lodash';
import { UpdateUsageCounterPayload } from '../award/award.type';
import { mapTransferAppError } from '../common/common.util';
import altoUtil from '../alto/alto.utils';
import outgoingStatusTemplateService from './transactionStatus.template';
import { IQrisCreatePaymentResponse, QrisError } from '../alto/alto.type';
import altoService from '../alto/alto.service';
import { NotificationCode } from './transaction.producer.enum';
import monitoringEventProducer from '../monitoringEvent/monitoringEvent.producer';
import { MonitoringEventType } from '@dk/module-message';
import entitlementFlag from '../common/entitlementFlag';
import { EntitlementContext } from '../entitlement/entitlement.interface';
import transactionService from './transaction.service';
import { AppError } from '@dk/module-common/dist/error/AppError';
import transactionUsageService from '../transactionUsage/transactionUsage.service';
import { isSuspectedAbuse } from '../abuseDetection/abuseDetectionService';

const { CATEGORY_CODE_COL } = transferConstant;

const isRdnAccount = (transaction: ITransactionModel): boolean =>
  transaction.sourceAccountType === AccountType.RDN ||
  transaction.beneficiaryAccountType === AccountType.RDN ||
  transaction.sourceAccountType === AccountType.RDS ||
  transaction.beneficiaryAccountType === AccountType.RDS;

const shouldIgnoreRdnExceptions = (
  transaction: ITransactionModel,
  error?: any
): boolean =>
  isRdnAccount(transaction) &&
  (IGNORE_RDN_EXCEPTIONS.includes(error.code) ||
    !isEmpty(
      IGNORE_RDN_EXCEPTIONS.find(
        ex => error && error.message && error.message.includes(ex)
      )
    ));

export const defaultExecutionHooks: ITransactionExecutionHooks = {
  onFailedTransaction: undefined,
  onRecommendedServiceCode: undefined,
  onSucceedTransaction: undefined,
  onNewCreatedTransaction: undefined
};

const validateTransactionCategoryCode = async (categoryCode: string) => {
  const transactionCategory = await configurationRepository.getTransactionCategory(
    categoryCode
  );

  if (!transactionCategory) {
    logger.error(
      `validateTransactionCategoryCode: category code is not available.`
    );
    throw new TransferAppError(
      'Category code is not available!',
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.INVALID_REGISTER_PARAMETER,
      [getErrorMessage(CATEGORY_CODE_COL)]
    );
  }
};

const updateUsageCounters = async (
  transactionAmount: number,
  limitGroupCodes: string[],
  customerId: string
): Promise<void> => {
  const data: UpdateUsageCounterPayload[] = limitGroupCodes.map(
    limitGroupCode => ({
      limitGroupCode,
      adjustmentLimitAmount: transactionAmount
    })
  );
  try {
    await awardRepository.updateUsageCounters(customerId, data);
  } catch (error) {
    logger.error(`error when update usage counters`, error);
  }
};

const buildFailureReason = (error: Error): IFailureReasonModel => {
  let detail,
    actor = undefined,
    type = undefined;
  if (error instanceof TransferAppError) {
    detail = error.detail;
    actor = error.actor;
    type = ERROR_CODE[error.errorCode as keyof typeof ERROR_CODE];
  } else if (error instanceof AppError) {
    type = ERROR_CODE[error.errorCode as keyof typeof ERROR_CODE];
    detail = error.message;
  } else {
    logger.warn(`TransferAppError not received: ${error}`);
    detail = error.message;
  }
  return {
    detail: detail,
    actor: actor,
    type: type
  };
};

const onFailedTransaction = async (
  model: ITransactionModel,
  executionHooks: ITransactionExecutionHooks,
  error: Error
): Promise<void> => {
  let failedTransaction;
  let failureReason = buildFailureReason(error);
  if (model.paymentServiceType === PaymentServiceTypeEnum.QRIS) {
    let transactionModel: ITransactionModel =
      error instanceof RetryableTransferAppError && error.transaction
        ? error.transaction
        : model;

    // all the details already persisted before
    // just need to update status and journey
    failedTransaction = await transactionRepository.update(model.id, {
      status: TransactionStatus.DECLINED,
      journey: transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.TRANSFER_FAILURE,
        transactionModel
      ),
      failureReason: failureReason
    });
    model = failedTransaction;
  } else {
    failedTransaction = await transactionRepository.update(model.id, {
      ...model,
      status: TransactionStatus.DECLINED,
      journey: transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.TRANSFER_FAILURE,
        model
      ),
      failureReason: failureReason
    });
  }

  const customerId = transactionHelper.getCustomerIdByModel(model);
  if (failedTransaction && customerId) {
    logger.info(
      `onFailedTransaction: Sending Failed Transaction notification ${failedTransaction.paymentServiceCode} ${customerId}`
    );
    if (isBIFast(failedTransaction.paymentServiceCode)) {
      transactionProducer.sendFailedTransferNotification(failedTransaction);
    }
    transactionProducer.sendTransactionFailedMessage(
      customerId,
      failedTransaction
    );
  }
  monitoringEventProducer.sendMonitoringEventMessage(
    MonitoringEventType.PROCESSING_FINISHED,
    failedTransaction
  );

  if (executionHooks.onFailedTransaction) {
    await executionHooks.onFailedTransaction(model);
  }
  if (failedTransaction && failedTransaction.journey) {
    logger.info(
      `onFailedTransaction: Journey for Failed Transaction ${JSON.stringify(
        failedTransaction.journey
      )}`
    );
  }
};

const validateCategoryAndfetchRecommendation = async (
  transactionInput: ITransactionInput
): Promise<RecommendationService> => {
  try {
    // validate category if it's supplied
    if (transactionInput.categoryCode) {
      await validateTransactionCategoryCode(transactionInput.categoryCode);
    }
    logger.info(
      `Fetching recommendation service for service type ${transactionInput.paymentServiceType}, externalId : ${transactionInput.externalId},
    executor cif: ${transactionInput.executorCIF}, sourceAccountNumber: ${transactionInput.sourceAccountNo}
    sourceBankCode: ${transactionInput.sourceBankCode}, beneficiaryBankCode: ${transactionInput.beneficiaryBankCode},
    beneficiaryAccountNumber: ${transactionInput.beneficiaryAccountNo}, beneficiaryBankChannel: ${transactionInput.beneficiaryBankCodeChannel}`
    );
    transactionInput = {
      ...transactionInput,
      journey: transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.FETCHING_RECOMMENDATION,
        transactionInput
      )
    };
    const recommendedService = await transactionRecommendService.getRecommendService(
      transactionInput
    );
    logger.info(
      `Recommendation service fetched : ${JSON.stringify(recommendedService)}`
    );

    return recommendedService;
  } catch (error) {
    logger.error(
      `initTransaction: Error while making a recommendationService call ${error.message}`
    );
    await transactionService.updateFailedTransaction(transactionInput, error);
    throw error;
  }
};

const declineTransactionIfSuspectedAbuse = async (
  paymentServiceCode: string,
  transactionInput: ITransactionInput
): Promise<void> => {
  if (
    transactionInput.ownerCustomerId &&
    transactionInput.beneficiaryAccountNo &&
    (await isSuspectedAbuse(
      paymentServiceCode,
      transactionInput.ownerCustomerId,
      transactionInput.beneficiaryAccountNo,
      transactionInput.transactionId
    ))
  ) {
    const detail = `Suspected abuse for
          paymentServiceCode: ${paymentServiceCode}
          customerId: ${transactionInput.ownerCustomerId}
          beneficiaryAccountNo: ${transactionInput.beneficiaryAccountNo}.`;
    logger.error(`declineRequestIfSuspectedAbuse: ${detail}`);
    let suspectedAbuseError = new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.SUSPECTED_ABUSE
    );
    await transactionService.updateFailedTransaction(
      transactionInput,
      suspectedAbuseError
    );
    suspectedAbuseError.errorCode = ERROR_CODE.INVALID_REQUEST;
    throw suspectedAbuseError;
  }
};

const initTransaction = async (
  transactionInput: ITransactionInput,
  initialTemplate: ITransactionSubmissionTemplate,
  executionHooks: ITransactionExecutionHooks = defaultExecutionHooks
): Promise<ITransactionModel> => {
  const recommendedService = await validateCategoryAndfetchRecommendation(
    transactionInput
  );

  // declining the transaction request if suspected abuse
  await declineTransactionIfSuspectedAbuse(
    recommendedService.paymentServiceCode,
    transactionInput
  );

  // based on the final recommended service; validate on pocket level rule
  await transactionUsageService.pocketLevelLimitValidation({
    transactionAmount: transactionInput.transactionAmount,
    transactionInterchange: transactionInput.interchange,
    customerId: transactionInput.sourceCustomerId,
    accountNo: transactionInput.sourceAccountNo,
    beneficiaryAccountNo: transactionInput.beneficiaryAccountNo,
    beneficiaryAccountName: transactionInput.beneficiaryAccountName,
    paymentServiceType: transactionInput.paymentServiceType,
    recommendedService
  });

  if (executionHooks.onRecommendedServiceCode) {
    await executionHooks.onRecommendedServiceCode(recommendedService);
  }

  if (await entitlementFlag.isEnabled()) {
    const infoTracker:
      | EntitlementContext[]
      | undefined = await getTransactionInfoTracker();

    if (infoTracker) {
      transactionInput.entitlementInfo = infoTracker.map(info => {
        return {
          entitlementCodeRefId: info.entitlementCodeRefId as string,
          counterCode: info.counterCode as string,
          customerId: info.customerId as string
        };
      });
    }
  }

  const transactionModel = transactionHelper.buildNewTransactionModel(
    transactionInput,
    recommendedService
  );

  if (
    transactionModel.requireThirdPartyOutgoingId &&
    transactionModel.debitTransactionCode
  ) {
    transactionModel.thirdPartyOutgoingId = await generateOutgoingId(
      transactionModel,
      transactionModel.debitTransactionCode
    );
  }
  let model;
  if (transactionInput.transactionId) {
    const cleanTransactionModel = pickBy(transactionModel, identity);
    model = await transactionRepository.update(
      transactionInput.transactionId,
      cleanTransactionModel
    );
  } else {
    // TODO: @dikshitthakral will remove once update will be stable
    await transactionHelper.validateExternalIdAndPaymentType(transactionModel);
    model = await transactionRepository.create(transactionModel);
  }

  if (!model) {
    logger.error(
      `submitTransaction: failed to update transaction info for txnId ${transactionInput.transactionId}`
    );
    throw new TransferAppError(
      `Failed to update transaction info for txnId ${transactionInput.transactionId}`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION
    );
  }

  logger.info(
    `Created a transaction model with id: ${model.id} externalId : ${model.externalId} paymentServiceCode: ${model.paymentServiceCode}`
  );

  if (executionHooks.onNewCreatedTransaction) {
    await executionHooks.onNewCreatedTransaction(model);
  }
  try {
    model.additionalPayload = transactionModel.additionalPayload;
    model = await initialTemplate.submitTransaction(model);
  } catch (err) {
    logger.error(
      `error when initial transaction with journey : ${JSON.stringify(
        model.journey
      )} error :${err && err.message}`
    );
    await onFailedTransaction(model, executionHooks, err);
    throw new RetryableTransferAppError(
      `Error: ${err && err.message} when initial transaction!`,
      TransferFailureReasonActor.UNKNOWN,
      err.errorCode || ERROR_CODE.TRANSACTION_FAILED,
      err.retry || false
    );
  }
  return model;
};

const defaultTrue = (value?: boolean) =>
  value === undefined || value == null || value;

const isMambuError = (error: RetryableTransferAppError) =>
  error &&
  error.errors &&
  error.errors.some((errorDetail: any) =>
    _.isEqual(
      errorDetail.message,
      ERROR_CODE.MAMBU_TRANSACTION_AUTHORIZATION_HOLD_EXPIRED
    )
  );

const settleTransaction = async (
  transactionModel: ITransactionModel,
  confirmTemplate: ITransactionSubmissionTemplate,
  executionHooks: ITransactionExecutionHooks = defaultExecutionHooks
): Promise<ITransactionModel> => {
  let model = transactionModel;
  try {
    model = await confirmTemplate.submitTransaction(model);
  } catch (error) {
    logger.error(
      `settleTransaction: error when confirm transaction for transaction id : ${
        model.id
      } , journey : ${JSON.stringify(model.journey)}, status: ${error &&
        error.status}, message: ${error && error.message}`
    );

    if (
      isMambuError(error) ||
      defaultTrue(confirmTemplate.declineOnSubmissionFailure)
    ) {
      await onFailedTransaction(model, executionHooks, error);
    }
    throw new RetryableTransferAppError(
      `Error when confirm transaction for transaction ID: ${
        model.id
      }, message: ${error && error.message}!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.TRANSACTION_FAILED,
      true
    );
  }

  const updatedTransaction = await transactionRepository.update(model.id, {
    ...model,
    status: TransactionStatus.SUCCEED,
    journey: transactionHelper.getStatusesToBeUpdated(
      TransferJourneyStatusEnum.TRANSFER_SUCCESS,
      model
    )
  });

  if (!updatedTransaction) {
    logger.error(
      `settleTransaction: failed to update transaction status for transaction id: ${model.id}.`
    );
    throw new TransferAppError(
      `Failed to update transaction status for transaction id: ${model.id}.`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION_STATUS
    );
  }

  const customerId = transactionHelper.getCustomerIdByModel(updatedTransaction);
  if (customerId) {
    logger.info(
      `settleTransaction: Sending Succeeded Transaction Notification ${customerId}`
    );
    transactionProducer.sendTransactionSucceededMessage(
      customerId,
      updatedTransaction
    );
  }
  monitoringEventProducer.sendMonitoringEventMessage(
    MonitoringEventType.PROCESSING_FINISHED,
    updatedTransaction
  );
  if (executionHooks.onSucceedTransaction) {
    await executionHooks.onSucceedTransaction(updatedTransaction);
  }

  const {
    debitLimitGroupCode,
    transactionAmount,
    sourceCustomerId,
    debitAwardGroupCounter
  } = updatedTransaction;

  const counters = [];
  if (debitLimitGroupCode) counters.push(debitLimitGroupCode);
  if (debitAwardGroupCounter) counters.push(debitAwardGroupCounter);

  if (
    counters.includes('L002') ||
    counters.includes('L013') ||
    counters.includes('L019')
  ) {
    counters.push(RTOL_BIFAST_FEE);
  }
  if (!isEmpty(counters) && sourceCustomerId) {
    await updateUsageCounters(transactionAmount, counters, sourceCustomerId);
  }

  updateRequestIdInRedis(updatedTransaction.id);
  logger.info(
    `transactionCompleted: settleTransaction
      customerId : ${updatedTransaction.sourceCustomerId},
      externalId : ${updatedTransaction.externalId},
      transactionId: ${updatedTransaction.id},
      beneficiaryAccount: ${updatedTransaction.beneficiaryAccountNo},
      beneficiaryBankCode: ${updatedTransaction.beneficiaryBankCode},
      sourceAccount: ${updatedTransaction.sourceAccountNo},
      journey: ${JSON.stringify(updatedTransaction.journey)}`
  );
  return updatedTransaction;
};

const getCreditTemplate = (
  transactionModel: ITransactionModel
): ITransactionSubmissionTemplate | undefined => {
  let creditTemplate: ITransactionSubmissionTemplate | undefined = undefined;
  if (transactionModel.beneficiaryAccountNo) {
    if (transactionModel.requireThirdPartyOutgoingId) {
      // this is outgoing transaction
      creditTemplate = outgoingSubmissionTemplateService.getSubmissionTemplate(
        transactionModel
      );
      if (!creditTemplate) {
        logger.error(
          'getCreditTemplate: credit transaction submission template not found'
        );
        throw new TransferAppError(
          'Credit transaction submission template not found.',
          TransferFailureReasonActor.MS_TRANSFER,
          ERROR_CODE.MISSING_CREDIT_TEMPLATE
        );
      }
    } else if (internalCreditSubmissionTemplate.isEligible(transactionModel)) {
      creditTemplate = internalCreditSubmissionTemplate;
    } else if (
      transactionModel.creditTransactionCode &&
      !transactionModel.creditTransactionCode.endsWith(REVERSAL_TC_SUFFIX)
    ) {
      // expect valid template if exist creditTransactionCode
      logger.error(
        'getCreditTemplate: credit transaction submission template not found if exist creditTransactionCode'
      );
      throw new TransferAppError(
        'Credit transaction submission template not found when creditTransactionCode exists.',
        TransferFailureReasonActor.MS_TRANSFER,
        ERROR_CODE.MISSING_CREDIT_TEMPLATE
      );
    }
  }

  transactionHelper.setPaymentRailToTransactionData(
    transactionModel,
    creditTemplate?.getRail?.()
  );
  return creditTemplate;
};

export const debitSource = async (model: ITransactionModel): Promise<void> => {
  logger.info('debitSource: debit transaction with source account.');
  try {
    if (
      model.debitTransactionCode &&
      !model.debitTransactionCode.endsWith(REVERSAL_TC_SUFFIX) &&
      model.debitTransactionChannel &&
      model.sourceAccountNo
    ) {
      const debitTransactionId = await helper.submitDebitTransaction(
        model.sourceAccountNo,
        model,
        model.debitTransactionCode,
        model.debitTransactionChannel
      );
      if (model.coreBankingTransactionIds) {
        model.coreBankingTransactionIds.push(debitTransactionId);
      }

      model.coreBankingTransactions?.push({
        id: debitTransactionId,
        type: TransferJourneyStatusEnum.AMOUNT_DEBITED
      });

      model.journey = transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.AMOUNT_DEBITED,
        model
      );
    }
  } catch (err) {
    logger.error(
      `debitSource: Error while debiting money, code : ${err.code} , message: ${err.message}`
    );
    if (shouldIgnoreRdnExceptions(model, err)) {
      logger.info(`Ignoring Exception for RDN Transaction Id: ${model.id}`);
      return;
    }
    throw new RetryableTransferAppError(
      `Error while debiting money, code: ${err.code}, message: ${err.message}!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.COREBANKING_DEBIT_FAILED,
      isRetryableError(err.status)
    );
  }
};

const submitFees = async (model: ITransactionModel): Promise<void> => {
  logger.info(
    `submitFees: submit fees with transactionId: ${model.id} and externalId : ${model.externalId} `
  );
  try {
    const feeExecutionTemplate = getApplicable(model);
    if (feeExecutionTemplate) {
      await feeExecutionTemplate.execute(model);
      model.journey = transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.FEE_PROCESSED,
        model
      );
    }
  } catch (err) {
    logger.error(
      `submitFees: Error while submiting fees, code : ${err.code} , message: ${err.message}`
    );
    if (shouldIgnoreRdnExceptions(model, err)) {
      logger.info(`Ignoring Exception for RDN Transaction Id: ${model.id}`);
      return;
    }
    throw new RetryableTransferAppError(
      `Ignoring exception for RDN transaction ID: ${model.id}!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.COREBANKING_FEES_FAILED,
      isRetryableError(err.status)
    );
  }
};

const creditBeneficiary = async (
  model: ITransactionModel,
  creditTemplate: ITransactionSubmissionTemplate | undefined
): Promise<void> => {
  logger.info(
    'creditBeneficiary: credit transaction using submission template.'
  );
  try {
    if (creditTemplate) {
      const categoryCode =
        model.paymentServiceType === PaymentServiceTypeEnum.OFFER ||
        model.paymentServiceType === PaymentServiceTypeEnum.PAYROLL ||
        model.paymentServiceType === PaymentServiceTypeEnum.CASHBACK ||
        model.paymentServiceType === PaymentServiceTypeEnum.CONVERSION ||
        model.paymentServiceType ===
          PaymentServiceTypeEnum.MAIN_POCKET_TO_GL_TRANSFER ||
        model.paymentServiceType === PaymentServiceTypeEnum.BONUS_INTEREST
          ? model.categoryCode
          : undefined; // force enrichment get default category code for credit
      model = await creditTemplate.submitTransaction({
        ...model,
        categoryCode: categoryCode
      });
      if (model && model.journey) {
        model.journey.push({
          status: TransferJourneyStatusEnum.AMOUNT_CREDITED,
          updatedAt: new Date()
        });
      }
    }
  } catch (err) {
    logger.error(
      `creditBeneficiary: Error while crediting money, code : ${err.code} , message: ${err.message}`
    );
    if (shouldIgnoreRdnExceptions(model, err)) {
      logger.info(`Ignoring Exception for RDN Transaction Id: ${model.id}`);
      return;
    }
    throw new RetryableTransferAppError(
      `Error while crediting money, code: ${err.code}, message: ${err.message}!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.COREBANKING_CREDIT_FAILED,
      isRetryableError(err.status)
    );
  }
};

const submitDisbursement = async (
  transactionModel: ITransactionModel
): Promise<void> => {
  logger.info('submitDisbursement: disburse loan transaction.');
  try {
    if (
      transactionModel.sourceAccountNo &&
      transactionModel.debitTransactionChannel
    ) {
      const disburseTransactionId = await helper.submitDisbursementTransaction(
        transactionModel.sourceAccountNo,
        transactionModel,
        transactionModel.debitTransactionChannel
      );

      transactionModel.coreBankingTransactions?.push({
        id: disburseTransactionId,
        type: TransferJourneyStatusEnum.AMOUNT_DISBURSED
      });

      if (transactionModel.coreBankingTransactionIds) {
        transactionModel.coreBankingTransactionIds.push(disburseTransactionId);
      }

      transactionModel.journey = transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.AMOUNT_DISBURSED,
        transactionModel
      );
    }
  } catch (error) {
    logger.error(
      `submitDisbursement: Error while disburse money, code: ${error.code}, message: ${error.message}`
    );
    throw new RetryableTransferAppError(
      `Error while disburse money, code: ${error.code}, message: ${error.message}!`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.COREBANKING_DISBURSEMENT_FAILED,
      isRetryableError(error.status)
    );
  }
};

const submitRepayment = async (
  transactionModel: ITransactionModel,
  bankIncomeAmount: number
): Promise<void> => {
  logger.info('submitRepayment: repay loan transaction.');
  try {
    if (
      transactionModel.beneficiaryAccountNo &&
      transactionModel.creditTransactionChannel
    ) {
      const repaymentTransactionId = await helper.submitRepaymentTransaction(
        transactionModel.beneficiaryAccountNo,
        transactionModel,
        transactionModel.creditTransactionChannel,
        bankIncomeAmount
      );

      transactionModel.coreBankingTransactions?.push({
        id: repaymentTransactionId,
        type: TransferJourneyStatusEnum.AMOUNT_REPAID
      });

      if (transactionModel.coreBankingTransactionIds) {
        transactionModel.coreBankingTransactionIds.push(repaymentTransactionId);
      }

      transactionModel.journey = transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.AMOUNT_REPAID,
        transactionModel
      );
    }
  } catch (error) {
    logger.error(
      `submitRepayment: Error while repay money, code: ${error.code}, message: ${error.message}`
    );
    throw new RetryableTransferAppError(
      `Error while repay money, code: ${error.code}, message: ${error.message}!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.COREBANKING_REPAYMENT_FAILED,
      isRetryableError(error.status)
    );
  }
};

const submitGlTransaction = async (
  transactionModel: ITransactionModel
): Promise<void> => {
  logger.info('submitGlTransaction: credit gl income');
  try {
    const glIncomeTransactionId = await helper.submitGlTransaction(
      transactionModel
    );

    transactionModel.coreBankingTransactions?.push({
      id: glIncomeTransactionId,
      type: TransferJourneyStatusEnum.GL_TRANSACTION_SUCCESS
    });

    if (transactionModel.coreBankingTransactionIds) {
      transactionModel.coreBankingTransactionIds.push(glIncomeTransactionId);
    }

    transactionModel.journey = transactionHelper.getStatusesToBeUpdated(
      TransferJourneyStatusEnum.GL_TRANSACTION_SUCCESS,
      transactionModel
    );
  } catch (error) {
    logger.error(
      `submitGlTransaction: Error while submit gl income code: ${error.code}, message: ${error.message}`
    );
    throw new RetryableTransferAppError(
      `Error while submit gl income code: ${error.code}, message: ${error.message}`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.COREBANKING_GL_JOURNAL_ENTRIES_FAILED,
      isRetryableError(error.status)
    );
  }
};

const submitPayOff = async (
  transactionModel: ITransactionModel
): Promise<void> => {
  logger.info('submitPayOff: payoff loan transaction.');
  try {
    const payOffTransactionId = `ext-${transactionModel.externalId}`;
    if (
      transactionModel.beneficiaryAccountNo &&
      transactionModel.creditTransactionChannel
    ) {
      await helper.submitPayOffTransaction(
        transactionModel.beneficiaryAccountNo,
        transactionModel,
        transactionModel.creditTransactionChannel
      );

      transactionModel.coreBankingTransactions?.push({
        id: payOffTransactionId,
        type: TransferJourneyStatusEnum.AMOUNT_REPAID
      });

      if (transactionModel.coreBankingTransactionIds) {
        transactionModel.coreBankingTransactionIds.push(payOffTransactionId);
      }

      transactionModel.journey = transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.AMOUNT_REPAID,
        transactionModel
      );
    }
  } catch (error) {
    logger.error(
      `submitPayOff: Error while pay off money, code: ${error.code}, message: ${error.message}`
    );
    throw new RetryableTransferAppError(
      `Error while pay off money, code: ${error.code}, message: ${error.message}!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.COREBANKING_PAY_OFF_FAILED,
      isRetryableError(error.status)
    );
  }
};

const submitQrisPayment = async (
  transactionModel: ITransactionModel,
  additionalPayload: any
): Promise<void> => {
  logger.info('submitQrisPayment: qris transaction.');
  try {
    transactionHelper.setPaymentRailToTransactionData(
      transactionModel,
      Rail.ALTO_QRIS
    );
    const paymentResponse: IQrisCreatePaymentResponse = await helper.submitQrisTransaction(
      transactionModel,
      additionalPayload
    );

    altoUtil.setQrisResponseToTransactionData(
      transactionModel,
      paymentResponse.forwardingCustomerReferenceNumber
    );

    altoUtil.updateTransactionDataOnQrisPaymentSuccess(
      transactionModel,
      paymentResponse
    );

    altoService.updateTransactionDetailsInExternalService(
      transactionModel,
      paymentResponse.transactionStatus,
      paymentResponse.forwardingCustomerReferenceNumber
    );
  } catch (error) {
    logger.error(
      `submitQrisPayment: error while submitting qris payment, code: ${error.code}, message: ${error.message}`
    );

    if (error instanceof QrisError) {
      altoUtil.setQrisResponseToTransactionData(
        transactionModel,
        error.forwardingCustomerReferenceNumber || ''
      );

      altoService.updateTransactionDetailsInExternalService(
        transactionModel,
        error.qrisPaymentStatus,
        error.forwardingCustomerReferenceNumber
      );

      if (!altoUtil.isQrisTimeoutError(error)) {
        throw new RetryableTransferAppError(
          (error && error.message) || 'Undetermined error!',
          TransferFailureReasonActor.UNKNOWN,
          altoUtil.getExceptionErrorCode(error),
          false
        );
      }
    } else {
      throw new RetryableTransferAppError(
        'Qris timeout error!',
        TransferFailureReasonActor.QRIS,
        altoUtil.getExceptionErrorCode(error),
        isRetryableError(error.status)
      );
    }
  }
};

const executeDebit = async (transaction: ITransactionModel) => {
  try {
    await debitSource(transaction);
    await submitFees(transaction);
    transactionHelper.setPaymentRailToTransactionData(
      transaction,
      Rail.INTERNAL
    );
    return transaction;
  } catch (error) {
    logger.error(
      `executeDebit: error debiting amount for transaction journey: ${JSON.stringify(
        transaction.journey
      )}`
    );
    if (!isEmpty(transaction.coreBankingTransactionIds)) {
      await transactionReversalHelper.revertManuallyAdjustedTransaction(
        transaction.id,
        transaction
      );
    }
    throw error;
  }
};

const executeDisbursementTransaction = async (
  model: ITransactionModel,
  isUseCreditTemplate: boolean = true
): Promise<ITransactionModel> => {
  let transactionModel = model;

  let creditTemplate = isUseCreditTemplate
    ? getCreditTemplate(transactionModel)
    : undefined;
  transactionHelper.setPaymentRailToTransactionData(
    transactionModel,
    creditTemplate?.getRail?.()
  );

  try {
    await submitDisbursement(transactionModel);

    if (
      BankNetworkEnum.LOAN_PROCESSOR_WINCORE_DISBURSEMENT !== model.interchange
    )
      await creditBeneficiary(transactionModel, creditTemplate);

    return transactionModel;
  } catch (error) {
    logger.error(
      `executeDisbursementTransaction: error executing transaction journey: ${JSON.stringify(
        model.journey
      )}`
    );
    if (!isEmpty(transactionModel.coreBankingTransactionIds)) {
      await transactionReversalHelper.revertFailedTransaction(
        transactionModel.id,
        undefined,
        transactionModel
      );
    }
    throw error;
  }
};

const executeRepaymentTransaction = async (
  model: ITransactionModel
): Promise<ITransactionModel> => {
  let transactionModel = model;
  let generalLedgers;
  const isGlTransactionDetailExists = isGlTransactionDetailFieldExists(model);

  transactionHelper.setPaymentRailToTransactionData(
    transactionModel,
    Rail.INTERNAL
  );
  try {
    if (isGlTransactionDetailExists)
      generalLedgers = await configurationRepository.getAllGeneralLedgers();

    loanAdditionalPayloadValidator(model, generalLedgers);

    if (isGlTransactionDetailExists)
      await submitGlTransaction(transactionModel);

    const isRecoveryWriteOff =
      model.paymentServiceType ===
      PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY;
    if (isRecoveryWriteOff) return transactionModel; // skip to execute next line

    const isLoanDirectRepayment =
      model.paymentServiceType === PaymentServiceTypeEnum.LOAN_DIRECT_REPAYMENT;
    if (isLoanDirectRepayment) await debitSource(transactionModel);

    const shouldRepayWithPayOff = get(
      model.additionalPayload,
      'shouldRepayWithPayOff',
      false
    );

    const repaymentAmountForPayOffInterestSettlement = get(
      model.additionalPayload,
      'repaymentAmountForPayOffInterestSettlement'
    );

    if (repaymentAmountForPayOffInterestSettlement || !shouldRepayWithPayOff) {
      const loanRepaymentAllocation = get(
        transactionModel.additionalPayload,
        'loanRepaymentAllocation'
      );
      let bankIncomeAmount = 0;

      if (loanRepaymentAllocation) {
        const bankIncomeData = filter(
          loanRepaymentAllocation as Record<string, unknown>[],
          { type: BANK_INCOME }
        );
        bankIncomeData.forEach(bankIncome => {
          const amount = get(bankIncome, 'amount', 0) as number;
          bankIncomeAmount = rounding(bankIncomeAmount + amount);
        });
      }

      await submitRepayment(transactionModel, bankIncomeAmount);
    }

    if (shouldRepayWithPayOff) {
      await submitPayOff(transactionModel);
    }

    return transactionModel;
  } catch (error) {
    logger.error(
      `executeRepaymentTransaction: error executing transaction journey: ${JSON.stringify(
        model.journey
      )}`
    );
    if (!isEmpty(transactionModel.coreBankingTransactionIds)) {
      await transactionReversalHelper.revertFailedTransaction(
        transactionModel.id,
        undefined,
        transactionModel
      );
    }
    throw error;
  }
};

const executePocketToGLTransaction = async (
  transactionModel: ITransactionModel
): Promise<ITransactionModel> => {
  try {
    let transactionInputUpdated = {
      ...transactionModel,
      journey: transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.CLOSE_ACCOUNT_POCKET_TO_GL_TRANSACTION,
        transactionModel
      )
    };

    transactionHelper.setPaymentRailToTransactionData(
      transactionInputUpdated,
      Rail.INTERNAL
    );
    await debitSource(transactionInputUpdated);

    return transactionInputUpdated;
  } catch (error) {
    logger.error(
      `executePocketToGLTransaction: error executing transaction, journey details: ${JSON.stringify(
        transactionModel.journey
      )}`
    );
    logger.error(
      `executePocketToGLTransaction: error executing transaction, error details: ${JSON.stringify(
        error
      )}`
    );

    if (!isEmpty(transactionModel.coreBankingTransactionIds)) {
      await transactionReversalHelper.revertFailedTransaction(
        transactionModel.id,
        undefined,
        transactionModel
      );
    }

    throw error;
  }
};

const executeForwardPaymentDeposit = async (
  model: ITransactionModel
): Promise<ITransactionModel> => {
  let transactionModel = model;
  transactionHelper.setPaymentRailToTransactionData(
    transactionModel,
    Rail.INTERNAL
  );

  try {
    await debitSource(transactionModel);
    return transactionModel;
  } catch (error) {
    logger.error(
      `executeForwardPaymentDeposit: error executing transaction journey: ${JSON.stringify(
        model.journey
      )}`
    );
    throw error;
  }
};

const executeForwardPaymentWithdrawal = async (
  model: ITransactionModel
): Promise<ITransactionModel> => {
  let transactionModel = model;
  try {
    let creditTemplate = getCreditTemplate(transactionModel);
    await creditBeneficiary(transactionModel, creditTemplate);
    return transactionModel;
  } catch (error) {
    logger.error(
      `executeForwardPaymentWithdrawal: error executing transaction journey: ${JSON.stringify(
        model.journey
      )}`
    );
    throw error;
  }
};

const executeNoneBlockingTransaction = async (
  model: ITransactionModel
): Promise<ITransactionModel> => {
  let transactionModel = model;
  // if transaction has credit, then it will require the submission template for credit tranction
  // this is to fail-fast validation
  let creditTemplate = getCreditTemplate(transactionModel);
  try {
    await debitSource(transactionModel);
    await creditBeneficiary(transactionModel, creditTemplate);
    await submitFees(transactionModel);
    return transactionModel;
  } catch (error) {
    logger.error(
      `executeNoneBlockingTransaction: error executing transaction journey: ${JSON.stringify(
        model.journey
      )}`
    );
    await transactionRepository.update(transactionModel.id, {
      coreBankingTransactionIds: transactionModel.coreBankingTransactionIds,
      coreBankingTransactions: transactionModel.coreBankingTransactions
    });
    if (!isEmpty(transactionModel.coreBankingTransactionIds)) {
      await transactionReversalHelper.revertFailedTransaction(
        transactionModel.id,
        undefined,
        transactionModel
      );
    }
    throw error;
  }
};

const executeBlockingTransaction = async (
  model: ITransactionModel
): Promise<ITransactionModel> => {
  let transactionModel = model;
  // this will reject transaction if beneficiary is internal or beneficiary is optional
  if (
    (!transactionModel.beneficiaryBankCodeChannel &&
      !transactionModel.beneficiaryAccountNo) ||
    (transactionModel.beneficiaryBankCodeChannel === BankChannelEnum.INTERNAL &&
      transactionModel.beneficiaryAccountNo)
  ) {
    logger.error(
      'executeBlockingTransaction: transaction rejected due to invalid beneficiaryBankCodeChannel, beneficiaryAccountNo'
    );
    throw new TransferAppError(
      'Transaction rejected due to invalid beneficiaryBankCodeChannel or beneficiaryAccountNo.',
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.TRANSACTION_REJECTED
    );
  }

  // if transaction has credit, then it will require the submission template for credit tranction
  let creditTemplate = getCreditTemplate(model);

  if (!creditTemplate && transactionModel.requireThirdPartyOutgoingId) {
    logger.error(
      'executeBlockingTransaction: transaction  rejected due to undefined credit template and undefined beneficiary account number.'
    );
    throw new TransferAppError(
      'Transaction rejected due to credit template was not found because of undefined beneficiary account number.',
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.REJECTED_DUE_UNDEFINED_BENEFICIARY_ACCOUNT_NUMBER
    );
  }

  try {
    if (
      transactionModel.debitTransactionCode &&
      transactionModel.debitTransactionChannel &&
      transactionModel.sourceAccountNo
    ) {
      // this will blocking principal and fee
      const blockingTransaction = await transactionBlockingHelper.createBlockingTransaction(
        transactionModel,
        Enum.BlockingAmountType.MOBILE_TRANSACTION
      );
      transactionModel = {
        ...transactionModel,
        ...blockingTransaction,
        journey: transactionHelper.getStatusesToBeUpdated(
          TransferJourneyStatusEnum.AMOUNT_BLOCKED,
          model
        )
      };
    }

    if (creditTemplate) {
      transactionModel = {
        ...transactionModel,
        journey: transactionHelper.getStatusesToBeUpdated(
          TransferJourneyStatusEnum.SUBMITTING_THIRD_PARTY,
          transactionModel
        )
      };
      try {
        transactionModel = await creditTemplate.submitTransaction(
          transactionModel
        );
      } catch (err) {
        logger.error(
          `executeBlockingTransaction: Error while executing credit template journey: ${JSON.stringify(
            transactionModel.journey
          )}, errorCode : ${err.code} message: ${err.message}`
        );
        return mapTransferAppError(
          err,
          ERROR_CODE.THIRDPARTY_SUBMISSION_FAILED,
          true
        );
      }
    }
    return transactionModel;
  } catch (error) {
    logger.error(
      `executeBlockingTransaction: Error while executing Blocking transaction Journey: ${JSON.stringify(
        transactionModel.journey
      )}`
    );
    await transactionBlockingHelper.cancelBlockingTransaction(transactionModel);
    throw error;
  }
};

const updateTransactionWithQrisDetails = async (
  transactionModel: ITransactionModel
): Promise<ITransactionModel> => {
  logger.info(
    `updateTransactionWithQrisDetails: saving qris transaction. transactionId: ${transactionModel.id}`
  );
  try {
    const updatedData: Partial<ITransactionModel> = await mapToQrisTransferDb(
      transactionModel
    );

    return { ...transactionModel, ...updatedData };
  } catch (error) {
    logger.error(
      `updateTransactionWithQrisDetails: Error while saving qris transaction, code: ${error.code}, message: ${error.message}`
    );
    throw new RetryableTransferAppError(
      `Error while saving qris transaction, code: ${error.code}, message: ${error.message}!`,
      TransferFailureReasonActor.ALTO,
      ERROR_CODE.ALTO_DATA_STORING_FAILED,
      isRetryableError(error.status)
    );
  }
};

/**
 * Reverse core banking transactions, if any, and determine the transaction status based on it.
 * Any failed transaction with core banking transactions that is not reversed successfuly should stay in SUBMITTED status,
 * so it can be picked up again by the scheduled status check.
 *
 * @param transactionModel
 * @returns false when core banking transaction is available and the reversal is failed,
 *          true when no core banking transaction or the reversal process is successful,
 */
const shouldFailTransaction = async (
  transactionModel: ITransactionModel
): Promise<boolean> =>
  isEmpty(transactionModel.coreBankingTransactionIds) ||
  transactionReversalHelper.revertFailedTransactionWithoutError(
    transactionModel.id,
    undefined,
    transactionModel
  );

const executeQrisPayment = async (
  model: ITransactionModel
): Promise<ITransactionModel> => {
  let transactionModel = model;
  const qrisPayload = model.additionalPayload;

  try {
    qrisAdditionalPayloadValidator(qrisPayload);
    transactionModel = await updateTransactionWithQrisDetails(transactionModel);
    await debitSource(transactionModel);

    transactionModel = await transactionRepository.update(transactionModel.id, {
      ...transactionModel,
      status: TransactionStatus.SUBMITTING,
      journey: transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.SUBMITTING_THIRD_PARTY,
        transactionModel
      )
    });
    await submitQrisPayment(transactionModel, qrisPayload);

    if (altoUtil.isAmountCredited(transactionModel)) {
      // settle the transaction once the payment submission is successful
      // no need transaction confirmation for QRIS
      transactionModel = await settleTransaction(transactionModel, {
        submitTransaction: async transactionModel => transactionModel,
        isEligible: () => true
      });
    } else {
      // update all changes before suspending the transaction
      transactionModel = await transactionRepository.update(
        transactionModel.id,
        transactionModel
      );

      await transactionProducer.sendQrisTransactionNotification(
        transactionModel,
        NotificationCode.NOTIF_PENDING_QRIS
      );
    }
    return transactionModel;
  } catch (error) {
    // save all changes before failing
    transactionModel = await transactionRepository.update(
      transactionModel.id,
      transactionModel
    );

    logger.error(
      `executeQrisPayment: error executing transaction journey: ${JSON.stringify(
        model.journey
      )}`
    );
    const transactionShouldBeFailed = await shouldFailTransaction(
      transactionModel
    );
    if (!transactionShouldBeFailed) {
      // let the transaction status stay as SUBMITTED,
      // so it being picked up by the scheduled status check
      // to resolve the actual status
      return transactionModel;
    }
    if (error instanceof RetryableTransferAppError) {
      error.transaction = transactionModel;
    }
    throw error;
  }
};

const executeTransaction = async (
  transactionInput: ITransactionInput,
  executionHooks: ITransactionExecutionHooks = defaultExecutionHooks
): Promise<ITransactionModel> => {
  let model = await initTransaction(
    transactionInput,
    {
      submitTransaction: async model => {
        if (ExecutionTypeEnum.NONE_BLOCKING === model.executionType) {
          switch (model.paymentServiceType) {
            case PaymentServiceTypeEnum.LOAN_GL_DISBURSEMENT: {
              return executeDisbursementTransaction(model, false);
            }
            case PaymentServiceTypeEnum.LOAN_DIRECT_DISBURSEMENT: {
              return executeDisbursementTransaction(model);
            }
            case PaymentServiceTypeEnum.LOAN_DIRECT_REPAYMENT:
            case PaymentServiceTypeEnum.LOAN_REPAYMENT:
            case PaymentServiceTypeEnum.LOAN_GL_REPAYMENT:
            case PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY: {
              return executeRepaymentTransaction(model);
            }
            case PaymentServiceTypeEnum.MAIN_POCKET_TO_GL_TRANSFER: {
              return executePocketToGLTransaction(model);
            }
            case PaymentServiceTypeEnum.LOAN_FORWARD_PAYMENT_DEPOSIT: {
              return executeForwardPaymentDeposit(model);
            }
            case PaymentServiceTypeEnum.LOAN_FORWARD_PAYMENT_WITHDRAWAL: {
              return executeForwardPaymentWithdrawal(model);
            }
            case PaymentServiceTypeEnum.QRIS: {
              return executeQrisPayment(model);
            }
          }

          return executeNoneBlockingTransaction(model);
        }
        if (ExecutionTypeEnum.BLOCKING === model.executionType) {
          return executeBlockingTransaction(model);
        }
        logger.error(
          `executeTransaction: missing transaction execution type (blocking or none blocking) for transactionId : ${model.id}`
        );
        throw new TransferAppError(
          `Missing transaction execution type (blocking or none blocking) for transactionId : ${model.id}`,
          TransferFailureReasonActor.SUBMITTER,
          ERROR_CODE.EXECUTION_TYPE_UNDEFINED
        );
      },
      isEligible: () => true
    },
    executionHooks
  );
  if (model.requireThirdPartyOutgoingId) {
    if (
      model.status === TransactionStatus.SUCCEED ||
      model.status === TransactionStatus.DECLINED
    ) {
      return model;
    }
    const updatedModel = await transactionRepository.update(model.id, {
      status: TransactionStatus.SUBMITTED,
      processingInfo: model.processingInfo,
      journey: transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.THIRD_PARTY_SUBMITTED,
        model
      )
    });

    if (!updatedModel) {
      logger.error(
        `executeTransaction: failed to update transaction status for transactionId : ${model.id}.`
      );
      throw new TransferAppError(
        `Failed to update transaction status for transactionId : ${model.id}`,
        TransferFailureReasonActor.MS_TRANSFER,
        ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION_STATUS
      );
    }
    logger.info(
      `transactionSubmitted:
      customerId : ${updatedModel.sourceCustomerId},
      externalId : ${updatedModel.externalId},
      transactionId: ${updatedModel.id},
      beneficiaryAccount: ${updatedModel.beneficiaryAccountNo},
      beneficiaryBankCode: ${updatedModel.beneficiaryBankCode},
      sourceAccount: ${updatedModel.sourceAccountNo},
      Journey: ${JSON.stringify(updatedModel.journey)}`
    );
    return updatedModel;
  } else {
    return await settleTransaction(
      model,
      {
        submitTransaction: async transactionModel => transactionModel,
        isEligible: () => true
      },
      executionHooks
    );
  }
};

const executeCheckStatus = async (
  transactionModel: ITransactionModel
): Promise<ITransactionStatusResponse> => {
  const statusTemplate = outgoingStatusTemplateService.getStatusTemplate(
    transactionModel
  );
  if (
    !isUndefined(statusTemplate) &&
    !isUndefined(statusTemplate.getTransactionStatus)
  ) {
    logger.info(`executeCheckStatus: status template found`);
    return await statusTemplate.getTransactionStatus(transactionModel);
  }
  logger.info(`executeCheckStatus: sending status from transaction db`);
  return { status: transactionModel.status };
};

const transactionExecutionService = wrapLogs({
  executeTransaction,
  initTransaction,
  settleTransaction,
  onFailedTransaction, // to be triggered by third party
  executeDebit,
  executeCheckStatus
});

export default transactionExecutionService;
