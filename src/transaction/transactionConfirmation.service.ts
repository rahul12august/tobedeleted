import {
  createIncomingTransactionExternalId,
  extractTransactionInfoByInterchange,
  GenerateAppError,
  updateExtraFields
} from './transaction.util';

import {
  ConfirmTransactionResponse,
  IConfirmTransactionInput,
  ITransactionExecutionHooks
} from './transaction.type';
import logger, { wrapLogs } from '../logger';
import { ITransactionModel } from './transaction.model';
import { ERROR_CODE } from '../common/errors';
import {
  FIELD_AMOUNT,
  FIELD_EXTERNALID,
  REVERSAL_TC_SUFFIX,
  REVERSED_REASON_ERROR_FROM_SWITCHING
} from './transaction.constant';
import transactionRepository from './transaction.repository';
import transactionReversalHelper from './transactionReversal.helper';
import coreBankingHelper from './transactionCoreBanking.helper';
import transactionProducer from './transaction.producer';
import { TransferAppError } from '../errors/AppError';
import transactionBlockingHelper from './transactionBlocking.helper';
import transactionExecutionService, {
  defaultExecutionHooks
} from './transactionExecution.service';
import {
  ConfirmTransactionStatus,
  ExecutionTypeEnum,
  PaymentRequestStatus,
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from './transaction.enum';
import { ITransactionResult } from '@dk/module-message';
import transactionConfirmationHelper from './transactionConfirmation.helper';
import { NotificationCode } from './transaction.producer.enum';
import transactionHelper from './transaction.helper';
import {
  mapTransferAppError,
  trimStringValuesFromObject
} from '../common/common.util';
import { AccountType, PaymentServiceTypeEnum } from '@dk/module-common';
import { setTransactionInfoTracker } from '../common/contextHandler';
import entitlementFlag from '../common/entitlementFlag';

import transactionUsageService from '../transactionUsage/transactionUsage.service';

export const responseMessageToPaymentInstruction = (
  input: IConfirmTransactionInput,
  transaction: ITransactionModel
): void => {
  const result = {
    externalId: transaction.externalId,
    paymentRequestID: transaction.paymentRequestID,
    transactionAmount: transaction.transactionAmount,
    extra: updateExtraFields(transaction)
  };
  let transactionResult: ITransactionResult;
  if (input.responseCode === ConfirmTransactionStatus.SUCCESS) {
    transactionResult = {
      ...result,
      status: PaymentRequestStatus.SUCCEED,
      transactionId: transaction.id,
      transactionStatus: TransactionStatus.SUCCEED,
      referenceId: transaction.referenceId
    };
  } else {
    transactionResult = {
      ...result,
      status: PaymentRequestStatus.DECLINED,
      transactionStatus: TransactionStatus.DECLINED,
      error: {
        errorCode: ERROR_CODE.TRANSACTION_FAILED
      }
    };
  }
  if (transaction.paymentInstructionExecution) {
    logger.info(
      `executionTransaction: Producing message on topic : ${
        transaction.paymentInstructionExecution.topics
      } with
        referenceId: ${transactionResult.referenceId},
        externalId: ${transactionResult.externalId},
        transactionId: ${transactionResult.transactionId},
        status: ${transactionResult.status},
        transactionStatus: ${transactionResult.transactionStatus},
        sourceAccountNo: ${transaction.sourceAccountNo},
        beneficiaryAccountNo: ${transaction.beneficiaryAccountNo},
        paymentInstructionId: ${transaction.paymentRequestID},
        error: ${JSON.stringify(transactionResult.error)}`
    );

    for (const topic of transaction.paymentInstructionExecution.topics) {
      transactionProducer.sendTransactionMessage(
        topic,
        transactionResult,
        transaction.paymentInstructionExecution.executionType,
        transaction.paymentInstructionExecution.paymentInstructionCode
      );
    }
  }
};

export const extractChangeFromConfirmTransactionRequest = (
  confirmInput: IConfirmTransactionInput,
  transaction: ITransactionModel,
  mustMatch = true
): Partial<ITransactionModel> => {
  const { interchange } = confirmInput;

  const infoByInterchange = extractTransactionInfoByInterchange(
    transaction,
    interchange,
    mustMatch // interchange null must match exact default case
  );

  let fee = transaction.fees && transaction.fees[0]; // TODO: enhance when we have more than 1 fee
  const feeCodeInfo = infoByInterchange.fees && infoByInterchange.fees[0];
  if (fee && feeCodeInfo) {
    fee = {
      ...feeCodeInfo,
      id: fee.id, // keep submitted id, blocking id...
      blockingId: fee.blockingId,
      idempotencyKey: fee.idempotencyKey
    };
  }

  const paymentRail = transactionHelper.getAdjustedPaymentRail(
    transaction.processingInfo?.paymentRail,
    interchange
  );
  const updateInfo: Partial<ITransactionModel> = {
    ...infoByInterchange,
    interchange,
    additionalInformation1:
      transaction.additionalInformation1 || confirmInput.additionalInformation1,
    additionalInformation2:
      transaction.additionalInformation2 || confirmInput.additionalInformation2,
    additionalInformation3:
      transaction.additionalInformation3 || confirmInput.additionalInformation3,
    additionalInformation4:
      transaction.additionalInformation4 || confirmInput.additionalInformation4,
    fees: fee ? [fee] : [],
    processingInfo: {
      ...transaction.processingInfo,
      paymentRail: paymentRail
    }
  };

  return updateInfo;
};

const isAlreadyConfirmed = (
  input: IConfirmTransactionInput,
  transaction: ITransactionModel
): boolean =>
  (input.responseCode === ConfirmTransactionStatus.SUCCESS ||
    input.responseCode === ConfirmTransactionStatus.REQUEST_TIMEOUT) &&
  transaction.status === TransactionStatus.SUCCEED;

const isAlreadyReversed = (transaction: ITransactionModel): boolean =>
  transaction.status === TransactionStatus.DECLINED;

const isAmountValid = (
  input: IConfirmTransactionInput,
  transaction: ITransactionModel
): boolean => {
  if (transaction.paymentServiceCode === 'DIGITAL_GOODS') {
    // const fees =
    //   transaction.fees && transaction.fees.length
    //     ? sumBy(transaction.fees, 'feeAmount')
    //     : 0;
    // if (!input.amount) {
    //   return false;
    // }
    // if (input.amount && transaction.transactionAmount + fees !== input.amount) {
    //   return false;
    // }
    return true;
  }
  if (input.amount && transaction.transactionAmount !== input.amount) {
    return false;
  }
  return true;
};

const isRdnReversal = (
  input: IConfirmTransactionInput,
  transaction: ITransactionModel
): boolean =>
  input.responseCode === ConfirmTransactionStatus.ERROR_EXPECTED &&
  (transaction.beneficiaryAccountType === AccountType.RDN ||
    transaction.beneficiaryAccountType === AccountType.RDS);

const validateConfirmTransactionRequest = (
  input: IConfirmTransactionInput,
  transaction: ITransactionModel
) => {
  if (!isAmountValid(input, transaction)) {
    logger.error(`validateConfirmTransactionRequest: Amount mismatch.`);
    throw GenerateAppError(
      'Confirm transaction Request validation failed amount mismatch.',
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_AMOUNT,
      FIELD_AMOUNT
    );
  }
  if (isAlreadyConfirmed(input, transaction)) {
    logger.error(
      `validateConfirmTransactionRequest: TRANSACTION ALREADY CONFIRMED : ${transaction.id}, externalId : ${transaction.externalId}`
    );
    throw new TransferAppError(
      `Transaction already confirmed : ${transaction.id}, externalId : ${transaction.externalId}`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.TRANSACTION_ALREADY_CONFIRMED
    );
  }
  if (isAlreadyReversed(transaction)) {
    logger.error(
      `validateConfirmTransactionRequest: TRANSACTION ALREADY REVERSED : ${transaction.id}, externalId : ${transaction.externalId}`
    );
    throw new TransferAppError(
      `Transaction already reversed : ${transaction.id}, externalId : ${transaction.externalId}`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.TRANSACTION_ALREADY_REVERSED
    );
  }
  if (isRdnReversal(input, transaction)) {
    logger.error(
      `validateConfirmTransactionRequest: RDN Reversal not allowed, transactionId: : ${transaction.id}, externalId : ${transaction.externalId}`
    );
    throw new TransferAppError(
      `RDN Reversal not allowed, transactionId: : ${transaction.id}, externalId : ${transaction.externalId}`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.RDN_REVERSAL_NOT_ALLOWED
    );
  }
};

const populateConfirmTransactionResponse = (
  response: ConfirmTransactionResponse,
  transaction: ITransactionModel
): ConfirmTransactionResponse => {
  return {
    ...response,
    id: transaction.id,
    externalId: transaction.externalId,
    coreBankingTransactions: transaction.coreBankingTransactions
  };
};

const debitBlockingTransaction = async (
  transaction: ITransactionModel
): Promise<void> => {
  try {
    if (
      transaction.debitTransactionCode &&
      !transaction.debitTransactionCode.endsWith(REVERSAL_TC_SUFFIX) &&
      transaction.debitTransactionChannel &&
      transaction.blockingId &&
      transaction.cardId &&
      transaction.sourceCIF &&
      transaction.sourceAccountNo
    ) {
      await coreBankingHelper.submitCardTransaction(
        transaction.cardId,
        transaction.blockingId,
        transaction,
        transaction.debitTransactionCode,
        transaction.debitTransactionChannel
      );
      transaction.journey = transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.AMOUNT_DEBITED,
        transaction
      );
    }
  } catch (err) {
    return mapTransferAppError(err, ERROR_CODE.COREBANKING_DEBIT_FAILED, true);
  }
};

const debitBlockingFees = async (
  transaction: ITransactionModel
): Promise<void> => {
  try {
    if (
      transaction.cardId &&
      transaction.sourceCIF &&
      transaction.sourceAccountNo
    ) {
      await coreBankingHelper.submitFeesCardTransaction(
        transaction.cardId,
        transaction
      );

      if (transaction.fees) {
        transaction.journey = transactionHelper.getStatusesToBeUpdated(
          TransferJourneyStatusEnum.FEE_PROCESSED,
          transaction
        );
      }
    }
  } catch (err) {
    return mapTransferAppError(err, ERROR_CODE.COREBANKING_FEES_FAILED, true);
  }
};
const executeBlockingDebitAndFees = async (
  transaction: ITransactionModel
): Promise<void> => {
  try {
    await debitBlockingTransaction(transaction);
    await debitBlockingFees(transaction);
  } catch (error) {
    logger.error(
      `executeBlockingDebitAndFees: Error while settling blocked transaction ${JSON.stringify(
        error
      )}`
    );
    throw error;
  }
};

const submitConfirmTransaction = async (
  transaction: ITransactionModel
): Promise<ITransactionModel> => {
  // TODO: dikshitthakral to refactor, because JAGOPAY uses same tc code for blocking and non-blocking
  if (
    transaction.executionType === ExecutionTypeEnum.BLOCKING ||
    transaction.paymentServiceType === PaymentServiceTypeEnum.JAGOPAY
  ) {
    await executeBlockingDebitAndFees(transaction);
  }
  return transaction;
};

const onFailedTransaction = async (
  transaction: ITransactionModel,
  executionHooks: ITransactionExecutionHooks,
  error: Error
): Promise<void> => {
  await transactionExecutionService.onFailedTransaction(
    transaction,
    executionHooks,
    error
  );
};

const reverseTransaction = async (
  input: IConfirmTransactionInput,
  transaction: ITransactionModel
): Promise<ConfirmTransactionResponse> => {
  logger.warn(
    `reverseTransaction - calling reverse transaction for
      externalId: ${input.externalId},
      interchange: ${input.interchange},
      accountNumber: ${input.accountNo},
      transactionDate: ${input.transactionDate}`
  );
  // TODO: dikshitthakral to refactor, because JAGOPAY uses same tc code for blocking and non-blocking
  if (
    transaction.status === TransactionStatus.PENDING ||
    (transaction.status === TransactionStatus.SUBMITTED &&
      transaction.executionType === ExecutionTypeEnum.BLOCKING) ||
    (transaction.status === TransactionStatus.SUBMITTED &&
      transaction.paymentServiceType === PaymentServiceTypeEnum.JAGOPAY)
  ) {
    try {
      await transactionBlockingHelper.cancelBlockingTransaction(transaction);
    } catch (error) {
      logger.error(
        `reverseTransaction: error while cancelling blocking transaction : ${error &&
          error.message}`
      );
      if (
        error.errorCode ===
        ERROR_CODE.MAMBU_TRANSACTION_AUTHORIZATION_HOLD_EXPIRED
      ) {
        await onFailedTransaction(transaction, defaultExecutionHooks, error);
        return { transactionResponseID: input.transactionResponseID };
      }
    }
  } else {
    await transactionReversalHelper.revertFailedTransaction(
      transaction.id,
      REVERSED_REASON_ERROR_FROM_SWITCHING,
      transaction
    );
    if (transaction.refund && transaction.refund.originalTransactionId) {
      await transactionConfirmationHelper.updateRefundAmount(
        transaction.refund.originalTransactionId,
        transaction.transactionAmount
      );
    }
  }

  await onFailedTransaction(
    transaction,
    defaultExecutionHooks,
    new TransferAppError(
      'reverseTransaction',
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.REVERSE_TRANSACTION
    )
  );
  return { transactionResponseID: input.transactionResponseID };
};

const queryTransaction = (
  input: IConfirmTransactionInput
): Promise<ITransactionModel | null> => {
  let externalId: string | undefined = undefined;
  if (input.transactionDate && input.accountNo) {
    externalId = createIncomingTransactionExternalId(
      input.externalId,
      input.transactionDate,
      input.accountNo
    );
  }
  return transactionRepository.updateIfExists(input, externalId);
};

const isValidTransactionStatusForConfirmation = (
  transactionStatus: TransactionStatus
): boolean => {
  return (
    transactionStatus === TransactionStatus.SUBMITTED ||
    transactionStatus === TransactionStatus.SUBMITTING
  );
};

const confirmTransaction = async (
  input: IConfirmTransactionInput
): Promise<ConfirmTransactionResponse> => {
  trimStringValuesFromObject(input);
  let response: ConfirmTransactionResponse = {
    transactionResponseID: input.transactionResponseID
  };
  logger.info(
    `confirmTransaction: Transaction Confirmation with
      external id: ${input.externalId},
      accountNo: ${input.accountNo},
      transactionDate: ${input.transactionDate},
      responseCode: ${input.responseCode.toString()},
      responseMessage: ${input.responseMessage},
      interchange: ${input.interchange},
      additionalInformation1: ${input.additionalInformation1},
      additionalInformation2: ${input.additionalInformation2},
      additionalInformation3: ${input.additionalInformation3},
      additionalInformation4: ${input.additionalInformation4}`
  );
  let transaction = await queryTransaction(input);

  if (await entitlementFlag.isEnabled()) {
    if (transaction && transaction.entitlementInfo) {
      await setTransactionInfoTracker(transaction.entitlementInfo);
    }
  }

  await transactionUsageService.mapTransactionInfoToTransactionUsageInfoContext(
    transaction
  );

  if (!transaction) {
    throw GenerateAppError(
      'Failed to confirm transaction with invalid external id!',
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_EXTERNALID,
      FIELD_EXTERNALID
    );
  }

  logger.info(
    `confirmTransaction: Transaction found with 
      external id: ${input.externalId},
      accountNo: ${input.accountNo},
      beneficiaryBankCodeChannel : ${transaction.beneficiaryBankCodeChannel}, 
      beneficiaryBankCode: ${transaction.beneficiaryBankCode},
      beneficiaryAccountNo. ${transaction.beneficiaryAccountNo}
    `
  );
  validateConfirmTransactionRequest(input, transaction);
  response = populateConfirmTransactionResponse(response, transaction);

  if (input.responseCode === ConfirmTransactionStatus.REQUEST_TIMEOUT) {
    logger.error(
      `Updating transaction for timeout with transaction Id : ${transaction.id}`
    );
    const updatedTransaction = await transactionRepository.update(
      transaction.id,
      {
        confirmationCode: `${input.responseCode}`
      }
    );
    if (updatedTransaction) {
      logger.info(
        `Send pending blocked amount notification for : ${updatedTransaction.id}`
      );
      await transactionProducer.sendPendingBlockedAmountNotification(
        updatedTransaction,
        NotificationCode.NOTIFICATION_PENDING_TRANSFER
      );
    } else {
      logger.error(
        `confirmTransaction: Transaction not found for Transaction Id : ${transaction.id}`
      );
    }
    return response;
  }

  // sumbitted to failure
  if (input.responseCode !== ConfirmTransactionStatus.SUCCESS) {
    logger.info(`Executing Reversal for transaction : ${transaction.id}`);
    const reverseResponse = await reverseTransaction(input, transaction);
    if (transaction.paymentInstructionExecution) {
      responseMessageToPaymentInstruction(input, transaction);
    }
    return {
      ...response,
      ...reverseResponse
    };
  }

  if (!isValidTransactionStatusForConfirmation(transaction.status)) {
    logger.warn(
      `confirmTransaction: invalid transaction status: ${transaction.status},
        transaction ID: ${transaction.id}
        external id: ${input.externalId},
        accountNo: ${input.accountNo},
        beneficiaryBankCodeChannel : ${transaction.beneficiaryBankCodeChannel}, 
        beneficiaryBankCode: ${transaction.beneficiaryBankCode},
        beneficiaryAccountNo. ${transaction.beneficiaryAccountNo}.
        Skip processing confirmTransaction logic`
    );
    return { transactionResponseID: input.transactionResponseID };
  }

  if (transaction.status === TransactionStatus.SUBMITTING) {
    // we are in a race condition, where we receive confirmation before payment response
    logger.warn(
      `confirmTransaction: Abnormal confirmation order received for trx ${transaction.id}, system handled it correctly.`
    );
    transaction = await transactionRepository.update(transaction.id, {
      status: TransactionStatus.SUBMITTED,
      journey: transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.THIRD_PARTY_SUBMITTED,
        transaction
      )
    });
  }

  // sumbitted to success
  const updateInfo = extractChangeFromConfirmTransactionRequest(
    input,
    transaction
  );
  const txnId = transaction.id;
  transaction = await transactionRepository.update(txnId, {
    ...updateInfo
  });
  if (!transaction) {
    logger.error(
      `confirmTransaction: submitTransaction: failed to update transaction info for txnId ${txnId}`
    );
    throw new TransferAppError(
      `Submit transaction failed to update transaction info for txnId ${txnId}`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION
    );
  }

  await transactionExecutionService.settleTransaction(transaction, {
    submitTransaction: submitConfirmTransaction,
    isEligible: () => true,
    declineOnSubmissionFailure: false
  });

  if (transaction.paymentInstructionExecution) {
    responseMessageToPaymentInstruction(input, transaction);
  }

  return response;
};

const switchingTransactionService = wrapLogs({
  confirmTransaction,
  reverseTransaction,
  extractChangeFromConfirmTransactionRequest,
  submitConfirmTransaction
});

export default switchingTransactionService;
