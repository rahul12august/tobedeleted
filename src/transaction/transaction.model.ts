import mongoose, { Schema, Model, Document } from 'mongoose';
import { ITransaction } from './transaction.type';
import { IBaseModel } from '../common/baseModel.type';
import {
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from './transaction.enum';
import {
  BankChannelEnum,
  BankNetworkEnum,
  InstitutionTypeEnum,
  PaymentServiceTypeEnum,
  TransactionExecutionTypeEnum
} from '@dk/module-common';
import { handleDuplicationError } from '../common/mongoose.util';
import { IFeeAmount } from '../fee/fee.type';
import { ExecutionTypeEnum } from './transaction.enum';
import { ExecutionMethodEnum } from './transaction.constant';
import { ERROR_CODE } from '../common/errors';

export interface IFeeModel {
  feeCode: string;
  feeAmount: number;
  customerTc: string;
  customerTcChannel: string;
  subsidiary?: boolean;
  subsidiaryAmount?: number;
  debitGlNumber?: string;
  creditGlNumber?: string;
  id?: string;
  blockingId?: string;
  interchange?: string;
  idempotencyKey?: string;
}

export interface ITransactionCodeModel {
  interchange?: BankNetworkEnum;
  code: string;
  awardGroupCounter?: string;
  limitGroupCode?: string;
  feeRules?: string;
  minAmountPerTx: number;
  maxAmountPerTx: number;
  defaultCategoryCode?: string;
  channel: string;
}

export interface IPaymentInstructionExecutionModel {
  paymentInstructionCode: string;
  executionType: TransactionExecutionTypeEnum;
  topics: string[];
}

export interface IReasonModel {
  responseCode: number;
  responseMessage: string;
  transactionDate: string;
}

export interface IFailureReasonModel {
  type?: ERROR_CODE;
  actor?: TransferFailureReasonActor;
  detail?: string;
}

export interface IRefund {
  remainingAmount?: number;
  originalTransactionId?: string;
  transactions?: string[];
  revertPaymentServiceCode?: string;
}

export interface ICoreBankingTransaction {
  id: string;
  type: TransferJourneyStatusEnum;
}

export interface IEntitlementInfo {
  entitlementCodeRefId: string;
  counterCode: string;
  customerId: string;
}

export interface IProcessingInfo {
  paymentRail?: string;
  channel?: string;
  method?: ExecutionMethodEnum;
  partner?: string;
}

export interface ITransactionModel extends ITransaction, IBaseModel {
  beneficiaryRealBankCode?: string;
  paymentServiceCode?: string;
  debitTransactionCode?: string;
  debitTransactionChannel?: string;
  debitLimitGroupCode?: string; // in case of blocking, this will be used to update usageCounter
  creditTransactionCode?: string;
  creditTransactionChannel?: string;
  status: TransactionStatus;
  referenceId: string;
  externalId: string;
  extendedMessage?: string;
  fees?: IFeeModel[];
  categoryCode?: string; // value to send to core banking, if userSelectedCategoryCode null then get from tc
  userSelectedCategoryCode?: string; // may be null
  blockingId?: string;
  cardId?: string;
  executionType?: ExecutionTypeEnum;
  coreBankingTransactionIds?: string[];
  requireThirdPartyOutgoingId: boolean;
  debitTransactionCodeMapping?: ITransactionCodeModel[];
  creditTransactionCodeMapping?: ITransactionCodeModel[];
  feeMapping?: IFeeAmount[];
  paymentInstructionExecution?: IPaymentInstructionExecutionModel;
  destinationCIF?: string;
  executorCIF?: string;
  moverRemainingDailyUsage?: number;
  reason?: IReasonModel[];
  failureReason?: IFailureReasonModel;
  confirmationCode?: string;
  refund?: IRefund;
  availableBalance?: number;
  manuallyAdjusted?: boolean;
  additionalInformation4?: string;
  debitAwardGroupCounter?: string;
  paymentInstructionId?: string;
  coreBankingTransactions?: ICoreBankingTransaction[];
  ownerCustomerId?: string;
  idempotencyKey?: string;
  entitlementInfo?: IEntitlementInfo[];
  processingInfo?: IProcessingInfo;
  workflowId?: string;
  correlationId?: string;
}

export type TransactionDocument = ITransactionModel & Document;

export const collectionName = 'transactions';

const TransactionCodeSchema = new Schema({
  interchange: {
    type: String,
    required: false
  },
  code: {
    type: String,
    required: true
  },
  limitGroupCode: {
    type: String,
    required: false
  },
  feeRules: {
    type: String,
    required: false
  },
  minAmountPerTx: {
    type: Number,
    required: false
  },
  maxAmountPerTx: {
    type: Number,
    required: false
  },
  defaultCategoryCode: {
    type: String,
    required: false
  },
  channel: {
    type: String,
    required: true
  },
  confirmationCode: {
    type: String
  },
  awardGroupCounter: {
    type: String,
    required: false
  }
});

const FeeSchema = new Schema({
  feeCode: {
    type: String,
    required: true
  },
  interchange: {
    type: String,
    required: false
  },
  feeAmount: {
    type: Number,
    required: true
  },
  customerTc: {
    type: String,
    required: true
  },
  customerTcChannel: {
    type: String,
    required: true
  },
  subsidiaryAmount: {
    type: Number
  },
  debitGlNumber: {
    type: String
  },
  creditGlNumber: {
    type: String
  },
  subsidiary: {
    type: Boolean
  },
  blockingId: {
    type: String
  },
  idempotencyKey: {
    type: String,
    required: false
  }
});

const PaymentInstructionExecutionSchema = new Schema({
  paymentInstructionCode: {
    type: String,
    required: true
  },
  executionType: {
    type: String,
    required: true,
    enum: Object.values(TransactionExecutionTypeEnum)
  },
  topics: {
    type: [String],
    required: true
  }
});

const ReasonSchema = new Schema({
  responseCode: {
    type: Number
  },
  responseMessage: {
    type: String
  },
  transactionDate: {
    type: String
  }
});

const FailureReasonSchema = new Schema(
  {
    type: {
      type: String,
      enum: Object.values(ERROR_CODE),
      required: false
    },
    actor: {
      type: String,
      enum: Object.values(TransferFailureReasonActor),
      required: false
    },
    detail: {
      type: String,
      required: false
    }
  },
  { _id: false }
);

const TransferJourneySchema = new Schema(
  {
    status: {
      type: String,
      enum: Object.values(TransferJourneyStatusEnum),
      required: true
    },
    updatedAt: {
      type: Date,
      required: false
    }
  },
  { timestamps: false, _id: false }
);

const RefundSchema = new Schema(
  {
    remainingAmount: {
      type: Number,
      required: false
    },
    originalTransactionId: {
      type: String,
      required: false
    },
    transactions: {
      type: [String],
      required: false
    },
    revertPaymentServiceCode: {
      type: String,
      required: false
    }
  },
  { _id: false }
);

const EntitlementInfoSchema = new Schema(
  {
    entitlementCodeRefId: {
      type: String,
      required: false
    },
    counterCode: {
      type: String,
      required: false
    },
    customerId: {
      type: String,
      required: false
    }
  },
  { _id: false }
);

const CoreBankingTransactionSchema = new Schema(
  {
    id: { type: String, required: true },
    type: { type: String, required: true }
  },
  { timestamps: false, _id: false }
);

const ProcessingInfoSchema = new Schema(
  {
    paymentRail: { type: String, required: false },
    channel: { type: String, required: false },
    method: {
      type: String,
      enum: Object.values(ExecutionMethodEnum),
      required: false
    },
    partner: { type: String, required: false }
  },
  { timestamps: false, _id: false }
);

const TransactionSchema = new Schema(
  {
    sourceCustomerId: {
      type: String,
      required: false
    },
    paymentServiceType: {
      type: String,
      enum: Object.values(PaymentServiceTypeEnum),
      required: true
    },
    paymentRequestID: {
      type: String,
      required: false
    },
    referenceId: {
      type: String,
      required: true
    },
    externalId: {
      type: String,
      required: true
    },
    secondaryExternalId: {
      type: String,
      required: false
    },
    blockingId: {
      type: String,
      required: false
    },
    cardId: {
      type: String,
      required: false
    },
    sourceCIF: {
      type: String,
      required: false
    },
    sourceAccountType: {
      type: String,
      required: false
    },
    sourceAccountNo: {
      type: String,
      required: false
    },
    sourceAccountName: {
      type: String,
      required: false
    },
    sourceBankCode: {
      type: String,
      required: false
    },
    sourceRtolCode: {
      type: String,
      required: false
    },
    sourceRemittanceCode: {
      type: String,
      required: false
    },
    sourceBankCodeChannel: {
      type: String,
      enum: Object.values(BankChannelEnum),
      required: false
    },
    sourceTransactionCurrency: {
      type: String,
      required: false
    },
    transactionAmount: {
      type: Number,
      required: true
    },
    beneficiaryCIF: {
      type: String,
      required: false
    },
    beneficiaryBankCode: {
      type: String,
      required: false
    },
    beneficiaryRealBankCode: {
      type: String,
      required: false
    },
    beneficiaryAccountType: {
      type: String,
      required: false
    },
    beneficiaryAccountNo: {
      type: String,
      required: false
    },
    beneficiaryAccountName: {
      type: String,
      required: false
    },
    beneficiaryBankName: {
      type: String,
      required: false
    },
    beneficiaryBankCodeChannel: {
      type: String,
      enum: Object.values(BankChannelEnum),
      required: false
    },
    beneficiaryRtolCode: {
      type: String,
      required: false
    },
    beneficiaryRemittanceCode: {
      type: String,
      required: false
    },
    beneficiaryIrisCode: {
      type: String,
      required: false
    },
    beneficiaryCustomerId: {
      type: String,
      required: false
    },
    creditPriority: {
      type: String,
      enum: Object.values(BankNetworkEnum),
      required: false
    },
    debitPriority: {
      type: String,
      enum: Object.values(BankNetworkEnum),
      required: false
    },
    debitLimitGroupCode: {
      type: String,
      required: false
    },
    billerCode: {
      type: String,
      required: false
    },
    institutionalType: {
      type: String,
      enum: Object.values(InstitutionTypeEnum),
      required: false
    },
    note: {
      type: String,
      required: false
    },
    categoryCode: {
      type: String,
      required: false
    },
    userSelectedCategoryCode: {
      type: String,
      required: false
    },
    paymentServiceCode: {
      type: String,
      required: false
    },
    debitTransactionCode: {
      type: String,
      required: false
    },
    debitTransactionChannel: {
      type: String,
      required: false
    },
    creditTransactionCode: {
      type: String,
      required: false
    },
    creditTransactionChannel: {
      type: String,
      required: false
    },
    extendedMessage: {
      type: String,
      required: false
    },
    status: {
      type: String,
      enum: Object.values(TransactionStatus),
      required: true
    },
    fees: {
      type: [FeeSchema],
      required: false
    },
    additionalInformation1: {
      type: String,
      required: false
    },
    additionalInformation2: {
      type: String,
      required: false
    },
    additionalInformation3: {
      type: String,
      required: false
    },
    additionalInformation4: {
      type: String,
      required: false
    },
    promotionCode: {
      type: String,
      required: false
    },
    interchange: {
      type: String,
      required: false
    },
    externalTransactionDate: {
      // MMDDhhmmss
      type: String,
      required: false
    },
    thirdPartyOutgoingId: {
      type: String,
      required: false,
      index: true,
      sparse: true
    },
    coreBankingTransactionIds: {
      type: [String],
      required: false
    },
    executionType: {
      type: String,
      enum: Object.values(ExecutionTypeEnum),
      required: false
    },
    requireThirdPartyOutgoingId: {
      type: Boolean,
      required: true,
      default: false
    },
    debitTransactionCodeMapping: {
      type: [TransactionCodeSchema],
      required: false
    },
    creditTransactionCodeMapping: {
      type: [TransactionCodeSchema],
      required: false
    },
    feeMapping: {
      type: [FeeSchema],
      required: false
    },
    paymentInstructionExecution: {
      type: PaymentInstructionExecutionSchema,
      required: false
    },
    executorCIF: {
      type: String,
      required: false
    },
    destinationCIF: {
      type: String,
      required: false
    },
    moverRemainingDailyUsage: {
      type: Number,
      required: false
    },
    reason: {
      type: [ReasonSchema]
    },
    failureReason: {
      type: FailureReasonSchema
    },
    refund: {
      type: RefundSchema,
      required: false
    },
    manuallyAdjusted: {
      type: Boolean,
      default: false,
      required: false
    },
    debitAwardGroupCounter: {
      type: String,
      required: false
    },
    journey: {
      type: [TransferJourneySchema],
      required: false
    },
    paymentInstructionId: {
      type: String,
      required: false
    },
    coreBankingTransactions: {
      type: [CoreBankingTransactionSchema]
    },
    ownerCustomerId: {
      index: true,
      type: String,
      required: false
    },
    sourceIdNumber: {
      type: String,
      required: false
    },
    sourceCustomerType: {
      type: String,
      required: false
    },
    sourceAccountTypeBI: {
      type: String,
      required: false
    },
    idempotencyKey: {
      type: String,
      required: false,
      index: true,
      sparse: true
    },
    preferedBankChannel: {
      type: String,
      required: false
    },
    entitlementInfo: {
      type: [EntitlementInfoSchema],
      required: false
    },
    processingInfo: {
      type: ProcessingInfoSchema,
      required: false
    },
    workflowId: {
      type: String,
      required: false
    },
    /**
     * correlationId for transactions.
     * In some transaction processing (for example, Atome card transaction), there may be more than one transfer.
     * We use this correlation id to link them together.
     */
    correlationId: {
      type: String,
      required: false
    }
  },
  {
    timestamps: true
  }
);

// Create index for createdAt
TransactionSchema.index({ createdAt: 1 });

TransactionSchema.index({
  paymentServiceCode: 1,
  ownerCustomerId: 1,
  createdAt: 1
});

TransactionSchema.post('save', handleDuplicationError);

TransactionSchema.post('find', handleDuplicationError);

export const TransactionModel: Model<TransactionDocument> = mongoose.model(
  collectionName,
  TransactionSchema
);
