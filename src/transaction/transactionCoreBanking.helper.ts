import logger, { wrapLogs } from '../logger';
import { TransferAppError } from './../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { ITransactionModel } from './transaction.model';
import {
  mapToCoreBankingCardTransaction,
  mapToCoreBankingTransaction,
  mapToDisbursementTransaction,
  mapToGlIncomeTransaction,
  mapToPayOffTransaction,
  mapToRepaymentTransaction,
  mapToQrisTransaction,
  submitIdempotentTransactionWithRetry,
  submitCoreBankingTransactionWithRetry,
  IdempotentRetryConfig
} from './transaction.util';
import depositRepository from '../coreBanking/deposit.repository';
import feeHelper from './transactionFee.helper';
import loanRepository from '../coreBanking/loan.repository';
import altoRepository from '../alto/alto.repository';
import { IQrisCreatePaymentResponse } from '../alto/alto.type';
import { QrisTransactionStatusEnum } from '../alto/alto.enum';
import { Http, Util } from '@dk/module-common';
import { get } from 'lodash';
import { shouldRetryQrisTransaction } from '../alto/alto.utils';
import { TransferFailureReasonActor } from './transaction.enum';

const submitGlFees = (transactionModel: ITransactionModel) => {
  const glFees = feeHelper.getJournalEntriesGLTransactionInput(
    transactionModel
  );
  glFees.forEach(glFee =>
    submitIdempotentTransactionWithRetry(
      depositRepository.executeJournalEntriesGL,
      glFee,
      transactionModel.idempotencyKey || Http.generateIdempotencyKey()
    )
  );
};

const submitFeeTransactions = async (
  accountNo: string,
  transactionModel: ITransactionModel,
  coreBankingAction: any
): Promise<string[]> => {
  const transactionIds = [];
  const feeInputs = feeHelper.getFeeTransactionInputs(transactionModel);
  logger.info(
    `submitFeeTransactions: Executing fee for transactionId : ${transactionModel.id} and feeInputs lengths ${feeInputs.length}`
  );

  for (const feeTransaction of feeInputs) {
    const createdFeeTransaction = await submitIdempotentTransactionWithRetry(
      coreBankingAction,
      accountNo,
      feeTransaction.feeTransactionPayload,
      feeTransaction.feeIdempotencyKey || Http.generateIdempotencyKey()
    );
    transactionIds.push(createdFeeTransaction.id);
  }
  submitGlFees(transactionModel);
  return transactionIds;
};

const submitDebitTransaction = async (
  accountNo: string,
  transactionModel: ITransactionModel,
  transactionCode: string,
  transactionChannel: string
): Promise<string> => {
  const debitTransaction = mapToCoreBankingTransaction(
    transactionModel,
    transactionCode,
    transactionChannel
  );
  const withdrawalTransaction = await submitCoreBankingTransactionWithRetry(
    depositRepository.makeWithdrawal,
    accountNo,
    debitTransaction
  );
  return withdrawalTransaction.id;
};

const submitCreditTransaction = async (
  accountNo: string,
  transactionModel: ITransactionModel,
  transactionCode: string,
  transactionChannel: string
): Promise<string> => {
  const creditTransaction = mapToCoreBankingTransaction(
    transactionModel,
    transactionCode,
    transactionChannel
  );

  const depositTransaction = await submitCoreBankingTransactionWithRetry(
    depositRepository.makeDeposit,
    accountNo,
    creditTransaction
  );

  return depositTransaction.id;
};

const submitCardTransaction = async (
  cardId: string,
  blockingId: string,
  transactionModel: ITransactionModel,
  transactionCode: string,
  transactionChannel: string
): Promise<string> => {
  const cardTransaction = mapToCoreBankingCardTransaction(
    transactionModel,
    transactionCode,
    transactionChannel,
    blockingId
  );

  const withdrawalTransaction = await submitIdempotentTransactionWithRetry(
    depositRepository.makeCardWithdrawal,
    cardId,
    cardTransaction,
    transactionModel.idempotencyKey || Http.generateIdempotencyKey()
  );

  return withdrawalTransaction.encodedKey;
};

const submitFeesCardTransaction = async (
  cardId: string,
  transactionModel: ITransactionModel
): Promise<string[]> => {
  const cardTransactions = feeHelper.getFeeCardTransactionInputs(
    transactionModel
  );

  const withdrawalTransactions = await Promise.all(
    cardTransactions.map(
      async cardTransaction =>
        await submitIdempotentTransactionWithRetry(
          depositRepository.makeCardWithdrawal,
          cardId,
          cardTransaction.feeCardTransactionPayload,
          cardTransaction.feeCardIdempotencyKey || Http.generateIdempotencyKey()
        )
    )
  );

  submitGlFees(transactionModel);

  return withdrawalTransactions.map(
    withdrawalTransaction => withdrawalTransaction.encodedKey
  );
};

const submitDisbursementTransaction = async (
  accountNo: string,
  transactionModel: ITransactionModel,
  transactionChannel: string
): Promise<string> => {
  const disbursementPayload = mapToDisbursementTransaction(
    transactionModel,
    transactionChannel
  );

  const idempotencyKey =
    get(transactionModel, 'additionalPayload.coreBankingIdempotencyKey') ||
    transactionModel.idempotencyKey ||
    Http.generateIdempotencyKey();

  const disbursementTransaction = await submitIdempotentTransactionWithRetry(
    loanRepository.makeDisbursement,
    accountNo,
    disbursementPayload,
    idempotencyKey
  );
  return disbursementTransaction.id;
};

const submitRepaymentTransaction = async (
  accountNo: string,
  transactionModel: ITransactionModel,
  transactionChannel: string,
  bankIncomeAmount: number
): Promise<string> => {
  const repaymentPayload = mapToRepaymentTransaction(
    transactionModel,
    transactionChannel,
    bankIncomeAmount
  );

  const idempotencyKey =
    get(transactionModel, 'additionalPayload.coreBankingIdempotencyKey') ||
    transactionModel.idempotencyKey ||
    Http.generateIdempotencyKey();

  const repaymentTransaction = await submitIdempotentTransactionWithRetry(
    loanRepository.makeRepayment,
    accountNo,
    repaymentPayload,
    idempotencyKey
  );

  return repaymentTransaction.id;
};

const submitGlTransaction = async (
  model: ITransactionModel,
  isReversal: boolean = false,
  transactionId?: string
): Promise<string> => {
  const glIncomePayload = mapToGlIncomeTransaction(
    model,
    isReversal,
    transactionId
  );
  const glIncomeTransaction = await submitIdempotentTransactionWithRetry(
    depositRepository.createGlJournalEntries,
    glIncomePayload,
    model.idempotencyKey || Http.generateIdempotencyKey()
  );

  return glIncomeTransaction.id;
};

const submitPayOffTransaction = async (
  accountNo: string,
  transactionModel: ITransactionModel,
  transactionChannel: string
): Promise<void> => {
  const payoffPayload = mapToPayOffTransaction(
    transactionModel,
    transactionChannel
  );

  const idempotencyKey =
    get(transactionModel, 'additionalPayload.coreBankingIdempotencyKey') ||
    transactionModel.idempotencyKey ||
    Http.generateIdempotencyKey();

  await submitIdempotentTransactionWithRetry(
    loanRepository.makePayOff,
    accountNo,
    payoffPayload,
    idempotencyKey
  );
};

const submitQrisTransaction = async (
  transactionModel: ITransactionModel,
  additionalPayload: any
): Promise<IQrisCreatePaymentResponse> => {
  const qrisPayload = mapToQrisTransaction(transactionModel, additionalPayload);

  let paymentResult: IQrisCreatePaymentResponse = await Util.retry(
    {
      ...IdempotentRetryConfig(),
      shouldRetry: shouldRetryQrisTransaction
    },
    altoRepository.submitTransaction,
    qrisPayload
  );

  if (
    paymentResult.transactionStatus ==
    QrisTransactionStatusEnum.SHOULD_CHECK_STATUS
  ) {
    const statusResult = await altoRepository.checkPaymentStatus(
      paymentResult.customerReferenceNumber
    );

    paymentResult.transactionStatus = statusResult.transactionStatus;
    paymentResult.forwardingCustomerReferenceNumber =
      statusResult.forwardingCustomerReferenceNumber;
    paymentResult.invoiceNo = statusResult.invoiceNo;

    if (statusResult.transactionStatus == QrisTransactionStatusEnum.FAILED) {
      const detail = `Qris payment is failed: ${statusResult.transactionStatus}!`;
      logger.error(`submitQrisTransaction: ${detail}`);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.QRIS,
        ERROR_CODE.ERROR_FROM_ALTO
      );
    }
  }

  return paymentResult;
};

export default wrapLogs({
  submitDebitTransaction,
  submitCreditTransaction,
  submitFeeTransactions,
  submitCardTransaction,
  submitFeesCardTransaction,
  submitGlFees,
  submitDisbursementTransaction,
  submitRepaymentTransaction,
  submitGlTransaction,
  submitPayOffTransaction,
  submitQrisTransaction
});
