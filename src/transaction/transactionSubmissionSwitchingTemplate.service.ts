/**
 * @module switchingSubmissionTemplate handle outgoing transaction to switching
 */

import { BankChannelEnum } from '@dk/module-common';
import { ITransactionModel } from './transaction.model';
import logger, { wrapLogs } from '../logger';
import { ITransactionSubmissionTemplate } from './transaction.type';
import { mapToSwitchingTransaction } from './transactionSwitching.helper';
import switchingRepository from '../switching/switching.repository';
import { httpClientTimeoutErrorCode } from '../common/constant';
import { isEmpty } from 'lodash';
import transactionUtility from '../transaction/transaction.util';
import { Rail } from '@dk/module-common';

const submitTransaction = async (
  transactionModel: ITransactionModel
): Promise<ITransactionModel> => {
  const switchingTransaction = await mapToSwitchingTransaction(
    transactionModel
  );
  const response = await transactionUtility
    .submitThirdPartyTransactionWithRetryAndUpdate(
      switchingRepository.submitTransaction,
      switchingTransaction,
      transactionModel
    )
    .catch(err => {
      logger.error(
        'submitThirdPartyTransactionWithRetryAndUpdate: failed submission to switching after all retry or non-retriable condition',
        err
      );
      // only throw error if it's not a timeout error
      if (err.code !== httpClientTimeoutErrorCode) {
        throw err;
      }
    });

  if (response) {
    return response.updatedTransaction;
  }
  return transactionModel;
};

const isEligible = (model: ITransactionModel): boolean =>
  BankChannelEnum.EXTERNAL === model.beneficiaryBankCodeChannel &&
  !isEmpty(model.beneficiaryAccountNo);

const shouldSendNotification = () => true;

const getRail = () => Rail.EURONET;

const switchingSubmissionTemplate: ITransactionSubmissionTemplate = {
  submitTransaction,
  isEligible,
  getRail,
  shouldSendNotification
};

export default wrapLogs(switchingSubmissionTemplate);
