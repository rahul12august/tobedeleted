import logger, { wrapLogs } from '../../logger';
import { ITransactionModel } from '../transaction.model';
import { BaseExternalTransactionInput } from '../transaction.type';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import { isNumber } from 'lodash';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../transaction.enum';
import configurationRepository from '../../configuration/configuration.repository';
import { PaymentServiceTypeEnum } from '@dk/module-common';

const refundPaymentServiceTypeMap = new Map<string, string[]>([
  [PaymentServiceTypeEnum.DOMESTIC_VOID_PAYMENT, ['POS_NPG']],
  [PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT, ['POS_VISA']],
  [PaymentServiceTypeEnum.JAGOPAY_REFUND, ['SIT03']],
  [
    PaymentServiceTypeEnum.VOID_RTGS,
    [
      'RTGS',
      'BRANCH_RTGS',
      'RTGS_FOR_BUSINESS',
      'RDN_WITHDRAW_RTGS',
      'RTGS_SHARIA'
    ]
  ],
  [
    PaymentServiceTypeEnum.VOID_SKN,
    ['SKN', 'BRANCH_SKN', 'SKN_FOR_BUSINESS', 'RDN_WITHDRAW_SKN', 'SKN_SHARIA']
  ],
  [
    PaymentServiceTypeEnum.VOID_BIFAST,
    ['BIFAST_OUTGOING', 'BIFAST_OUTGOING_SHARIA']
  ],
  [PaymentServiceTypeEnum.ATOME_VOID_PAYMENT, ['ATOME_TRANSFER_FOR_BUSINESS']]
]);
const logAndThrow = (message: string): void => {
  logger.error(message);
  throw new TransferAppError(
    message,
    TransferFailureReasonActor.SUBMITTER,
    ERROR_CODE.INVALID_REFUND_REQUEST
  );
};

const isOriginalTransactionValid = (
  payload: BaseExternalTransactionInput,
  originalTransaction: ITransactionModel
): boolean => {
  if (refundPaymentServiceTypeMap.has(payload.paymentServiceType)) {
    const validServiceCodes = refundPaymentServiceTypeMap.get(
      payload.paymentServiceType
    );
    if (
      originalTransaction.paymentServiceCode &&
      validServiceCodes &&
      validServiceCodes.includes(originalTransaction.paymentServiceCode)
    ) {
      return true;
    }
    return false;
  }
  return false;
};

const validateRefundRequest = (
  payload: BaseExternalTransactionInput,
  originalTransaction: ITransactionModel
): void => {
  if (!isOriginalTransactionValid(payload, originalTransaction)) {
    logAndThrow(
      `validateRefundRequest: paymentServiceType missmatch ${originalTransaction.paymentServiceType}`
    );
  }

  if (originalTransaction.status !== TransactionStatus.SUCCEED) {
    logAndThrow(
      `validateRefundRequest: Invalid Transaction Status ${originalTransaction.status}.`
    );
  }
  if (payload.beneficiaryAccountNo !== originalTransaction.sourceAccountNo) {
    logAndThrow(`validateRefundRequest: Account mismatch.`);
  }
  // TODO: (Dikshit)commenting it for now can be enabled later once we will get nore calrity
  // if (
  //   payload.beneficiaryBankCode !== originalTransaction.sourceBankCode ||
  //   payload.sourceBankCode !== originalTransaction.beneficiaryBankCode
  // ) {
  //   logAndThrow(`validateRefundRequest: Bank Code mismatch.`);
  // }
  if (payload.transactionAmount > originalTransaction.transactionAmount) {
    logAndThrow(
      `validateRefundRequest: Transaction Amount greater than original transaction Amount.`
    );
  }
  if (
    originalTransaction.refund &&
    isNumber(originalTransaction.refund.remainingAmount) &&
    payload.transactionAmount > originalTransaction.refund.remainingAmount
  ) {
    logAndThrow(
      `validateRefundRequest: Refund amount mismatch, originalTransaction: ${originalTransaction.id}`
    );
  }

  logger.info(
    `validateRefundRequest: Validated refunded transaction of originalTransaction:${originalTransaction.id} complete.`
  );
};

const validateGenaralRefundRequest = async (
  originalTransaction: ITransactionModel
): Promise<void> => {
  if (originalTransaction.status !== TransactionStatus.SUCCEED) {
    logAndThrow(
      `validateGenaralRefundRequest: Invalid Transaction Status ${originalTransaction.status}.`
    );
  }

  if (
    originalTransaction.refund &&
    originalTransaction.paymentServiceType != PaymentServiceTypeEnum.QRIS
  ) {
    logAndThrow(`validateRefundRequest: Transaction with refund object`);
  }

  logger.info(
    `validateGenaralRefundRequest: Validated refunded transaction of originalTransaction:${originalTransaction.id} complete.`
  );
  const nonRefundablePaymentServiceCode = await configurationRepository.getServiceRecommendations(
    false
  );
  if (
    originalTransaction.paymentServiceCode &&
    nonRefundablePaymentServiceCode.includes(
      originalTransaction.paymentServiceCode
    )
  ) {
    logAndThrow(
      `validateGenaralRefundRequest: Invalid refund paymentServiceCode : ${originalTransaction.paymentServiceCode}`
    );
  }
};

const refundValidator = wrapLogs({
  validateRefundRequest,
  validateGenaralRefundRequest
});

export default refundValidator;
