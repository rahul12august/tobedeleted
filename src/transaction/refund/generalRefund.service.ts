import logger, { wrapLogs } from '../../logger';
import transactionService from '../transaction.service';
import transactionExecutionService from '../transactionExecution.service';
import refundValidator from './refund.validator';
import { PaymentServiceTypeEnum } from '@dk/module-common';
import { ERROR_CODE } from '../../common/errors';
import {
  IRefundInput,
  IRefundTransactionResponse,
  ITransactionInput
} from '../transaction.type';
import transactionRepository from '../transaction.repository';
import transactionHelper from '../transaction.helper';
import { getErrorMessage, getRefundServiceType } from '../transaction.util';
import { populateIncomingAccount } from '../transactionExternal.service';
import transferConstant from '../transaction.constant';
import { ITransactionModel } from '../transaction.model';
import { isEmpty } from 'lodash';
import { ICreateRefundParam } from './refund.type';
import altoUtil from '../../alto/alto.utils';
import { TransferAppError } from '../../errors/AppError';
import { TransferFailureReasonActor } from '../transaction.enum';

const { BENEFICIARY_BANK_CODE_COL, SOURCE_BANK_CODE_COL } = transferConstant;

function buildTransferAppError(error: any, key: string) {
  let detail, actor;
  if (error instanceof TransferAppError) {
    detail = error.detail;
    actor = error.actor;
  } else {
    detail = 'Unknown issue with getting bank code info!';
    actor = TransferFailureReasonActor.UNKNOWN;
  }

  return new TransferAppError(detail, actor, error.errorCode, [
    getErrorMessage(key)
  ]);
}

const populateTransactionInfo = async (
  input: ITransactionInput
): Promise<ITransactionInput> => {
  const [sourceBankCodeInfo, beneficiaryBankCodeInfo] = await Promise.all([
    transactionHelper.getBankCodeInfo(input.sourceBankCode).catch(error => {
      throw buildTransferAppError(error, SOURCE_BANK_CODE_COL);
    }),
    transactionHelper
      .getBankCodeInfo(input.beneficiaryBankCode)
      .catch(error => {
        throw buildTransferAppError(error, BENEFICIARY_BANK_CODE_COL);
      })
  ]);

  if (!sourceBankCodeInfo || !beneficiaryBankCodeInfo) {
    const detail = `SourceBankInfo bankCode: ${input.sourceBankCode} or beneficiaryBankCodeInfo: ${input.beneficiaryBankCode} not found!`;
    logger.error(`Error: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_BANK_CODE
    );
  }

  // Keep track input cif
  const executorCIF = input.sourceCIF;
  const destinationCIF = input.beneficiaryCIF;

  const [sourceAccountInfo, beneficiaryAccountInfo] = await Promise.all([
    populateIncomingAccount(
      sourceBankCodeInfo,
      input.sourceAccountNo,
      input.sourceAccountName,
      executorCIF
    ),
    populateIncomingAccount(
      beneficiaryBankCodeInfo,
      input.beneficiaryAccountNo,
      input.beneficiaryAccountName,
      destinationCIF
    )
  ]);
  logger.info(
    `General Refund : Source accountNo : ${input.sourceAccountNo} and Beneficiary accountNo: ${input.beneficiaryAccountNo}, info fetched`
  );
  // get back owner cif
  if (sourceAccountInfo) {
    input.sourceCIF = sourceAccountInfo.cif;
  }
  if (beneficiaryAccountInfo) {
    input.beneficiaryCIF = beneficiaryAccountInfo.cif;
  }

  return transactionHelper.populateTransactionInput(
    input,
    sourceAccountInfo,
    sourceBankCodeInfo,
    beneficiaryAccountInfo,
    beneficiaryBankCodeInfo,
    executorCIF,
    destinationCIF
  );
};

const createRefundTransaction = async (
  param: ICreateRefundParam
): Promise<ITransactionModel> => {
  const amount = param.amountRefund
    ? param.amountRefund
    : param.originalTransaction.transactionAmount;

  const refundServiceType: PaymentServiceTypeEnum = getRefundServiceType(
    param.originalTransaction
  );

  const input: ITransactionInput = await populateTransactionInfo({
    sourceAccountNo: param.originalTransaction.beneficiaryAccountNo,
    sourceBankCode: param.originalTransaction.beneficiaryBankCode,
    transactionAmount: amount,
    beneficiaryCIF: param.originalTransaction.sourceCIF,
    beneficiaryBankCode: param.originalTransaction.sourceBankCode,
    beneficiaryAccountNo: param.originalTransaction.sourceAccountNo,
    paymentServiceType: refundServiceType
  });

  if (param.refundFees) {
    input.refund = { originalTransactionId: param.transactionId };
  }
  if (param.refundIdentifier) {
    input.externalId = param.refundIdentifier;
  }
  if (param.refundAddInfo1) {
    input.additionalInformation1 = param.refundAddInfo1;
  }

  if (altoUtil.isQrisTransaction(param.originalTransaction)) {
    input.additionalInformation3 =
      param.originalTransaction.paymentInstructionId;
    input.additionalInformation2 =
      param.originalTransaction.beneficiaryAccountName;
    input.sourceAccountName = param.originalTransaction.beneficiaryAccountName;
  }

  input.additionalInformation4 = param.transactionId;
  await refundValidator.validateGenaralRefundRequest(param.originalTransaction);

  logger.info(`executionTransaction : Create GENERAL_REFUND transaction with
      sourceBankCode: ${input.sourceBankCode},
      sourceAccountNo: ${input.sourceAccountNo},
      beneficiaryCIF: ${input.beneficiaryCIF}
      beneficiaryBankCode: ${input.beneficiaryBankCode},
      beneficiaryAccountNo: ${input.beneficiaryAccountNo},
      paymentServiceType: ${input.paymentServiceType}
      refund: ${JSON.stringify(input.refund)}
      originalTransactionId: ${input.additionalInformation4}`);

  const refundTxn = await transactionExecutionService.executeTransaction(input);

  logger.info('General Refund: Updating Transaction with refund ');
  await transactionService.updateOriginalTransactionWithRefund(
    param.originalTransaction,
    refundTxn
  );
  await transactionService.updateRefundTransaction(
    param.originalTransaction,
    refundTxn
  );

  return refundTxn;
};

const refundTransferTransaction = async (
  transactionId: string,
  refundFees?: boolean
): Promise<IRefundTransactionResponse> => {
  logger.info(
    `refundTransferTransation: General refund for transcationId: ${transactionId}
        refundFees : ${refundFees}`
  );
  const originalTransaction = await transactionRepository.getByKey(
    transactionId
  );
  if (!originalTransaction) {
    const detail = `Original transaction not found for transactionId: ${transactionId}!`;
    logger.error(`refundTransferTransaction: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.TRANSACTION_NOT_FOUND
    );
  }

  const createRefundParam: ICreateRefundParam = {
    transactionId: transactionId,
    originalTransaction: originalTransaction,
    refundFees: refundFees
  };
  const refundTxn = await createRefundTransaction(createRefundParam);

  return { id: refundTxn.id };
};

const refundTransaction = async (
  refundInput?: IRefundInput
): Promise<ITransactionModel> => {
  if (!refundInput || isEmpty(refundInput)) {
    const detail = 'refundInput not provided!';
    logger.error(`refundTransaction: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_MANDATORY_FIELDS
    );
  }

  const { transactionId, fee } = refundInput;
  if (!transactionId) {
    let detail = 'transactionId not provided!';
    logger.error(`refundTransaction: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_MANDATORY_FIELDS
    );
  }
  const refundTrn: IRefundTransactionResponse = await refundTransferTransaction(
    transactionId,
    fee
  );
  const transaction = await transactionRepository.getByKey(refundTrn.id);

  if (!transaction) {
    const detail = `Transaction not found for id: ${refundTrn.id}!`;
    logger.error(`refundTransaction: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.TRANSACTION_NOT_FOUND
    );
  }
  return transaction;
};

const generalRefundService = wrapLogs({
  refundTransferTransaction,
  refundTransaction,
  createRefundTransaction
});

export default generalRefundService;
