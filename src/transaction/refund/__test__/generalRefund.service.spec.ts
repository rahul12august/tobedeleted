import faker from 'faker';
import transactionRepository from '../../transaction.repository';
import configurationRepository from '../../../configuration/configuration.repository';
import refundValidator from '../refund.validator';
import { BankNetworkEnum, PaymentServiceTypeEnum } from '@dk/module-common';
import {
  getTransactionMockValueObject,
  getTransactionModelFullData
} from '../../__mocks__/transaction.data';
import { ERROR_CODE } from '../../../common/errors';
import generalRefundService from '../generalRefund.service';
import transactionHelper from '../../transaction.helper';
import transactionExecutionService from '../../transactionExecution.service';
import transactionService from '../../transaction.service';
import { ICreateRefundParam } from '../refund.type';
import { TransferAppError } from '../../../errors/AppError';
import { TransferFailureReasonActor } from '../../transaction.enum';

jest.mock('../../../configuration/configuration.repository');
jest.mock('../../../account/account.cache');
jest.mock('../../../transaction/transaction.producer');
jest.mock('../../../virtualAccount/virtualAccount.repository');
jest.mock('../../transaction.repository');
jest.mock('../../transactionExecution.service');
jest.mock('../../transaction.service');
jest.mock('../../refund/refund.validator');
jest.mock('../../transaction.helper');
jest.mock('../../../decisionEngine/decisionEngine.service.ts');

describe('Gerenal Refund', () => {
  const {
    sourceBankCode: bankCode,
    transactionCategory,
    ruleConfigs,
    limitGroups
  } = getTransactionMockValueObject();

  const original = require.requireActual('../../transaction.helper').default;

  const serviceCodes = ['p01'];

  const feeRule = {
    basicFeeMapping: { basicFee: { code: 'f01', refundFees: 1 } }
  };

  beforeEach(() => {
    (configurationRepository.getTransactionCategory as jest.Mock).mockResolvedValue(
      transactionCategory
    );

    // setup bankCode
    (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValue(
      bankCode
    );

    (transactionHelper.mapAuthenticationRequired as jest.Mock).mockImplementation(
      value => value
    );

    (transactionHelper.mapFeeData as jest.Mock).mockImplementation(
      value => value
    );

    (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValue(
      serviceCodes
    );
    (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValue(
      ruleConfigs
    );
    (configurationRepository.getLimitGroupsByCodes as jest.Mock).mockResolvedValue(
      limitGroups
    );
    (configurationRepository.getFeeRule as jest.Mock).mockResolvedValue(
      feeRule
    );
    (transactionHelper.getBankCodeInfo as jest.Mock).mockResolvedValue({
      ...original,
      mapAuthenticationRequired: jest.fn(),
      mapFeeData: jest.fn(),
      getAccountInfo: jest.fn(),
      getSourceAccount: jest.fn(),
      validateTransactionLimitAmount: jest.fn(),
      getPaymentServiceMappings: jest.fn()
    });
    (transactionHelper.populateTransactionInput as jest.Mock).mockResolvedValue(
      {
        ...original,
        mapAuthenticationRequired: jest.fn(),
        mapFeeData: jest.fn(),
        getAccountInfo: jest.fn(),
        getSourceAccount: jest.fn(),
        validateTransactionLimitAmount: jest.fn(),
        getPaymentServiceMappings: jest.fn()
      }
    );
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('refundTransferTransaction', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should throw error INVALID_BANK_CODE Bank info not found', async () => {
      //Given
      let newTransactionModel = getTransactionModelFullData();

      const originalTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.JAGOPAY_REFUND,
        transactionAmount: 50000,
        id: 'test12345'
      };

      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
        originalTransaction
      );
      (transactionHelper.getBankCodeInfo as jest.Mock).mockResolvedValue(null);

      // When
      const error = await generalRefundService
        .refundTransferTransaction('transactionId')
        .catch(e => e);

      // Then
      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_BANK_CODE);
    });

    it('should throw error when original transaction not found', async () => {
      //Given
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(null);

      // When
      const error = await generalRefundService
        .refundTransferTransaction('transactionId')
        .catch(e => e);

      // Then
      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(ERROR_CODE.TRANSACTION_NOT_FOUND);
    });

    it('should throw error when validation fails', async () => {
      //Given
      let newTransactionModel = getTransactionModelFullData();

      const originalTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.JAGOPAY_REFUND,
        transactionAmount: 50000,
        id: 'test12345'
      };

      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
        originalTransaction
      );

      (refundValidator.validateGenaralRefundRequest as jest.Mock).mockImplementationOnce(
        () => {
          throw new TransferAppError(
            'Test error!',
            TransferFailureReasonActor.UNKNOWN,
            ERROR_CODE.INVALID_REFUND_REQUEST
          );
        }
      );

      // When
      const error = await generalRefundService
        .refundTransferTransaction('transactionId')
        .catch(e => e);

      // Then
      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
    });

    it('should throw error when bankcode not found', async () => {
      //Given
      let newTransactionModel = getTransactionModelFullData();

      const originalTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.JAGOPAY_REFUND,
        transactionAmount: 50000,
        id: 'test12345'
      };

      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
        originalTransaction
      );

      (transactionHelper.getBankCodeInfo as jest.Mock).mockRejectedValue(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.INVALID_REGISTER_PARAMETER
        )
      );

      // When
      const error = await generalRefundService
        .refundTransferTransaction('transactionId')
        .catch(e => e);

      // Then
      expect(error).toBeDefined();
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.detail).toEqual('Test error!');
      expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REGISTER_PARAMETER);
    });

    describe('refund general transaction success function', () => {
      afterEach(() => {
        jest.resetAllMocks();
      });
      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.JAGOPAY_REFUND,
        transactionAmount: 50000,
        id: 'test12345',
        fees: [
          {
            feeCode: faker.random.alphaNumeric(4),
            customerTc: faker.random.alphaNumeric(4),
            customerTcChannel: faker.random.alphaNumeric(4),
            feeAmount: faker.random.number({ min: 0, max: 5000 })
          },
          {
            feeCode: faker.random.alphaNumeric(4),
            customerTc: faker.random.alphaNumeric(4),
            customerTcChannel: faker.random.alphaNumeric(4),
            feeAmount: faker.random.number({ min: 0, max: 5000 })
          }
        ]
      };

      const refundedTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.GENERAL_REFUND,
        transactionAmount: 50000,
        id: 'test1234',
        additionalInformation4: originalTransaction.id
      };

      it('should successfully refund transaction', async () => {
        (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
          originalTransaction
        );

        (transactionExecutionService.executeTransaction as jest.Mock).mockResolvedValueOnce(
          refundedTransaction
        );

        (refundValidator.validateGenaralRefundRequest as jest.Mock).mockReturnValueOnce(
          null
        );
        (transactionService.updateOriginalTransactionWithRefund as jest.Mock).mockResolvedValueOnce(
          {
            ...originalTransaction,
            refund: {
              remainingAmount: 0,
              transactions: [refundedTransaction.id]
            }
          }
        );
        (transactionService.updateRefundTransaction as jest.Mock).mockResolvedValueOnce(
          {
            ...refundedTransaction,
            refund: {
              originalTransactionId: originalTransaction.id,
              revertPaymentServiceCode: originalTransaction.paymentServiceCode
            }
          }
        );

        await generalRefundService.refundTransferTransaction(
          'transactionId',
          true
        );

        expect(transactionRepository.getByKey).toBeCalledWith('transactionId');
        expect(refundValidator.validateGenaralRefundRequest).toBeCalled();
        expect(
          transactionExecutionService.executeTransaction
        ).toHaveBeenCalledWith(
          expect.objectContaining({
            refund: {
              originalTransactionId: 'transactionId'
            }
          })
        );
        expect(
          transactionService.updateOriginalTransactionWithRefund
        ).toHaveBeenCalled();
        expect(transactionService.updateRefundTransaction).toHaveBeenCalled();
        expect(refundedTransaction.additionalInformation4).toBeDefined();
      });

      it('should successfully refund transaction with refundFees: false', async () => {
        (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
          originalTransaction
        );

        (transactionExecutionService.executeTransaction as jest.Mock).mockResolvedValueOnce(
          refundedTransaction
        );

        (refundValidator.validateGenaralRefundRequest as jest.Mock).mockReturnValueOnce(
          null
        );
        (transactionService.updateOriginalTransactionWithRefund as jest.Mock).mockResolvedValueOnce(
          {
            ...originalTransaction,
            refund: {
              remainingAmount: 0,
              transactions: [refundedTransaction.id]
            }
          }
        );
        (transactionService.updateRefundTransaction as jest.Mock).mockResolvedValueOnce(
          {
            ...refundedTransaction,
            refund: {
              originalTransactionId: originalTransaction.id,
              revertPaymentServiceCode: originalTransaction.paymentServiceCode
            }
          }
        );

        await generalRefundService.refundTransferTransaction(
          'transactionId',
          false
        );

        expect(transactionRepository.getByKey).toBeCalledWith('transactionId');
        expect(refundValidator.validateGenaralRefundRequest).toBeCalled();
        expect(
          transactionExecutionService.executeTransaction
        ).toHaveBeenCalledWith(
          expect.not.objectContaining({
            refundFees: undefined
          })
        );
        expect(
          transactionService.updateOriginalTransactionWithRefund
        ).toHaveBeenCalledWith(originalTransaction, refundedTransaction);
        expect(transactionService.updateRefundTransaction).toHaveBeenCalledWith(
          originalTransaction,
          refundedTransaction
        );
        expect(refundedTransaction.additionalInformation4).toBeDefined();
      });

      it('should successfully refund transaction with refundFees : undefined', async () => {
        (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
          originalTransaction
        );

        (transactionExecutionService.executeTransaction as jest.Mock).mockResolvedValueOnce(
          refundedTransaction
        );

        (refundValidator.validateGenaralRefundRequest as jest.Mock).mockReturnValueOnce(
          null
        );
        (transactionService.updateOriginalTransactionWithRefund as jest.Mock).mockResolvedValueOnce(
          {
            ...originalTransaction,
            refund: {
              remainingAmount: 0,
              transactions: [refundedTransaction.id]
            }
          }
        );
        (transactionService.updateRefundTransaction as jest.Mock).mockResolvedValueOnce(
          {
            ...refundedTransaction,
            refund: {
              originalTransactionId: originalTransaction.id,
              revertPaymentServiceCode: originalTransaction.paymentServiceCode
            }
          }
        );

        await generalRefundService.refundTransferTransaction('transactionId');

        expect(transactionRepository.getByKey).toBeCalledWith('transactionId');
        expect(refundValidator.validateGenaralRefundRequest).toBeCalled();
        expect(
          transactionExecutionService.executeTransaction
        ).toHaveBeenCalledWith(
          expect.not.objectContaining({
            refundFees: undefined
          })
        );
        expect(
          transactionService.updateOriginalTransactionWithRefund
        ).toHaveBeenCalledWith(originalTransaction, refundedTransaction);
        expect(transactionService.updateRefundTransaction).toHaveBeenCalledWith(
          originalTransaction,
          refundedTransaction
        );
        expect(refundedTransaction.additionalInformation4).toBeDefined();
      });
    });
  });

  describe('createRefundTransaction', () => {
    describe('refund general transaction success function', () => {
      afterEach(() => {
        jest.resetAllMocks();
      });
      let newTransactionModel = getTransactionModelFullData();

      const originalTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.JAGOPAY_REFUND,
        transactionAmount: 50000,
        id: 'test12345',
        fees: [
          {
            feeCode: faker.random.alphaNumeric(4),
            customerTc: faker.random.alphaNumeric(4),
            customerTcChannel: faker.random.alphaNumeric(4),
            feeAmount: faker.random.number({ min: 0, max: 5000 })
          },
          {
            feeCode: faker.random.alphaNumeric(4),
            customerTc: faker.random.alphaNumeric(4),
            customerTcChannel: faker.random.alphaNumeric(4),
            feeAmount: faker.random.number({ min: 0, max: 5000 })
          }
        ]
      };

      const refundedTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.GENERAL_REFUND,
        transactionAmount: 50000,
        id: 'test1234',
        additionalInformation4: originalTransaction.id
      };

      it('should successfully refund transaction', async () => {
        (transactionExecutionService.executeTransaction as jest.Mock).mockResolvedValueOnce(
          refundedTransaction
        );

        (refundValidator.validateGenaralRefundRequest as jest.Mock).mockReturnValueOnce(
          null
        );
        (transactionService.updateOriginalTransactionWithRefund as jest.Mock).mockResolvedValueOnce(
          {
            ...originalTransaction,
            refund: {
              remainingAmount: 0,
              transactions: [refundedTransaction.id]
            }
          }
        );
        (transactionService.updateRefundTransaction as jest.Mock).mockResolvedValueOnce(
          {
            ...refundedTransaction,
            refund: {
              originalTransactionId: originalTransaction.id,
              revertPaymentServiceCode: originalTransaction.paymentServiceCode
            }
          }
        );

        const createRefundParam: ICreateRefundParam = {
          transactionId: originalTransaction.id,
          originalTransaction: originalTransaction,
          refundFees: true
        };
        await generalRefundService.createRefundTransaction(createRefundParam);

        expect(refundValidator.validateGenaralRefundRequest).toBeCalled();
        expect(
          transactionExecutionService.executeTransaction
        ).toHaveBeenCalledWith(
          expect.objectContaining({
            refund: {
              originalTransactionId: originalTransaction.id
            }
          })
        );
        expect(
          transactionService.updateOriginalTransactionWithRefund
        ).toHaveBeenCalled();
        expect(transactionService.updateRefundTransaction).toHaveBeenCalled();
        expect(refundedTransaction.additionalInformation4).toBeDefined();
      });

      it('should successfully refund transaction for QRIS', async () => {
        originalTransaction.paymentServiceType = PaymentServiceTypeEnum.QRIS;

        const createRefundParam: ICreateRefundParam = {
          transactionId: originalTransaction.id,
          originalTransaction: originalTransaction,
          refundFees: true
        };
        await generalRefundService.createRefundTransaction(createRefundParam);

        expect(refundValidator.validateGenaralRefundRequest).toBeCalled();
        expect(
          transactionExecutionService.executeTransaction
        ).toHaveBeenCalledWith(
          expect.objectContaining({
            refund: {
              originalTransactionId: originalTransaction.id
            },
            additionalInformation2: originalTransaction.beneficiaryAccountName,
            sourceAccountName: originalTransaction.beneficiaryAccountName
          })
        );
        expect(
          transactionService.updateOriginalTransactionWithRefund
        ).toHaveBeenCalled();
        expect(transactionService.updateRefundTransaction).toHaveBeenCalled();
        expect(refundedTransaction.additionalInformation4).toBeDefined();
      });

      it('should successfully refund transaction with refundFees: false', async () => {
        (transactionExecutionService.executeTransaction as jest.Mock).mockResolvedValueOnce(
          refundedTransaction
        );

        (refundValidator.validateGenaralRefundRequest as jest.Mock).mockReturnValueOnce(
          null
        );
        (transactionService.updateOriginalTransactionWithRefund as jest.Mock).mockResolvedValueOnce(
          {
            ...originalTransaction,
            refund: {
              remainingAmount: 0,
              transactions: [refundedTransaction.id]
            }
          }
        );
        (transactionService.updateRefundTransaction as jest.Mock).mockResolvedValueOnce(
          {
            ...refundedTransaction,
            refund: {
              originalTransactionId: originalTransaction.id,
              revertPaymentServiceCode: originalTransaction.paymentServiceCode
            }
          }
        );

        const createRefundParam: ICreateRefundParam = {
          transactionId: originalTransaction.id,
          originalTransaction: originalTransaction,
          refundFees: false
        };
        await generalRefundService.createRefundTransaction(createRefundParam);

        expect(refundValidator.validateGenaralRefundRequest).toBeCalled();
        expect(
          transactionExecutionService.executeTransaction
        ).toHaveBeenCalledWith(
          expect.not.objectContaining({
            refundFees: undefined
          })
        );
        expect(
          transactionService.updateOriginalTransactionWithRefund
        ).toHaveBeenCalledWith(originalTransaction, refundedTransaction);
        expect(transactionService.updateRefundTransaction).toHaveBeenCalledWith(
          originalTransaction,
          refundedTransaction
        );
        expect(refundedTransaction.additionalInformation4).toBeDefined();
      });

      it('should successfully refund BIFAST_OUTGOING transaction with refundFees: false', async () => {
        const bifastTransaction = {
          ...originalTransaction,
          paymentServiceCode: 'BIFAST_OUTGOING'
        };
        const bifastRefundedTransaction = {
          ...refundedTransaction,
          paymentServiceCode: 'REFUND_BIFAST'
        };
        (transactionExecutionService.executeTransaction as jest.Mock).mockResolvedValueOnce(
          bifastRefundedTransaction
        );
        (refundValidator.validateGenaralRefundRequest as jest.Mock).mockReturnValueOnce(
          null
        );
        (transactionService.updateOriginalTransactionWithRefund as jest.Mock).mockResolvedValueOnce(
          {
            bifastTransaction,
            refund: {
              remainingAmount: 0,
              transactions: [bifastRefundedTransaction.id]
            }
          }
        );
        (transactionService.updateRefundTransaction as jest.Mock).mockResolvedValueOnce(
          {
            ...bifastRefundedTransaction,
            refund: {
              originalTransactionId: bifastTransaction.id,
              revertPaymentServiceCode: bifastTransaction.paymentServiceCode
            }
          }
        );

        const createRefundParam: ICreateRefundParam = {
          transactionId: bifastTransaction.id,
          originalTransaction: bifastTransaction,
          refundFees: false
        };
        await generalRefundService.createRefundTransaction(createRefundParam);

        expect(refundValidator.validateGenaralRefundRequest).toBeCalled();
        expect(
          transactionExecutionService.executeTransaction
        ).toHaveBeenCalledWith(
          expect.not.objectContaining({
            refundFees: undefined
          })
        );
        expect(
          transactionService.updateOriginalTransactionWithRefund
        ).toHaveBeenCalledWith(bifastTransaction, bifastRefundedTransaction);
        expect(transactionService.updateRefundTransaction).toHaveBeenCalledWith(
          bifastTransaction,
          bifastRefundedTransaction
        );
        expect(bifastRefundedTransaction.additionalInformation4).toBeDefined();
        expect(bifastRefundedTransaction.paymentServiceCode).toEqual(
          'REFUND_BIFAST'
        );
      });

      it('should successfully refund transaction with refundFees : undefined', async () => {
        (transactionExecutionService.executeTransaction as jest.Mock).mockResolvedValueOnce(
          refundedTransaction
        );

        (refundValidator.validateGenaralRefundRequest as jest.Mock).mockReturnValueOnce(
          null
        );
        (transactionService.updateOriginalTransactionWithRefund as jest.Mock).mockResolvedValueOnce(
          {
            ...originalTransaction,
            refund: {
              remainingAmount: 0,
              transactions: [refundedTransaction.id]
            }
          }
        );
        (transactionService.updateRefundTransaction as jest.Mock).mockResolvedValueOnce(
          {
            ...refundedTransaction,
            refund: {
              originalTransactionId: originalTransaction.id,
              revertPaymentServiceCode: originalTransaction.paymentServiceCode
            }
          }
        );

        const createRefundParam: ICreateRefundParam = {
          transactionId: originalTransaction.id,
          originalTransaction: originalTransaction
        };
        await generalRefundService.createRefundTransaction(createRefundParam);

        expect(refundValidator.validateGenaralRefundRequest).toBeCalled();
        expect(
          transactionExecutionService.executeTransaction
        ).toHaveBeenCalledWith(
          expect.not.objectContaining({
            refundFees: undefined
          })
        );
        expect(
          transactionService.updateOriginalTransactionWithRefund
        ).toHaveBeenCalledWith(originalTransaction, refundedTransaction);
        expect(transactionService.updateRefundTransaction).toHaveBeenCalledWith(
          originalTransaction,
          refundedTransaction
        );
        expect(refundedTransaction.additionalInformation4).toBeDefined();
      });
    });
  });

  describe('refundTransaction', () => {
    describe('refundTransaction failure scenarios', () => {
      test('should throw error when general refund object not found', async () => {
        let error;
        await generalRefundService
          .refundTransaction(undefined)
          .catch(err => (error = err));

        expect(error).toBeDefined();
        expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
        expect(transactionRepository.getByKey).not.toHaveBeenCalled();
        expect(
          transactionExecutionService.executeTransaction
        ).not.toHaveBeenCalled();
      });

      test('should throw error when refund object is empty', async () => {
        const input = {};

        let error;
        await generalRefundService
          .refundTransaction(input)
          .catch(err => (error = err));

        expect(error).toBeDefined();
        expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
        expect(transactionRepository.getByKey).not.toHaveBeenCalled();
        expect(
          transactionExecutionService.executeTransaction
        ).not.toHaveBeenCalled();
      });

      test('should throw error when transaction not found', async () => {
        const input = {
          transactionId: 'transactionId'
        };
        (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
          null
        );

        let error;
        await generalRefundService
          .refundTransaction(input)
          .catch(err => (error = err));

        expect(error).toBeDefined();
        expect(error.errorCode).toEqual(ERROR_CODE.TRANSACTION_NOT_FOUND);
        expect(transactionRepository.getByKey).toHaveBeenCalledTimes(1);
        expect(
          transactionExecutionService.executeTransaction
        ).not.toHaveBeenCalled();
      });

      test('should throw error when original transaction not found', async () => {
        const input = {
          transactionId: 'transactionId'
        };
        (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
          null
        );

        let error;
        await generalRefundService
          .refundTransaction(input)
          .catch(err => (error = err));

        expect(error).toBeDefined();
        expect(error.errorCode).toEqual(ERROR_CODE.TRANSACTION_NOT_FOUND);
        expect(transactionRepository.getByKey).toHaveBeenCalledTimes(1);
        expect(
          transactionExecutionService.executeTransaction
        ).not.toHaveBeenCalled();
      });
    });

    describe('refund general transaction success function', () => {
      afterEach(() => {
        jest.resetAllMocks();
      });
      let newTransactionModel = getTransactionModelFullData();

      const originalTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.JAGOPAY_REFUND,
        transactionAmount: 50000,
        id: 'test12345',
        fees: [
          {
            feeCode: faker.random.alphaNumeric(4),
            customerTc: faker.random.alphaNumeric(4),
            customerTcChannel: faker.random.alphaNumeric(4),
            feeAmount: faker.random.number({ min: 0, max: 5000 })
          },
          {
            feeCode: faker.random.alphaNumeric(4),
            customerTc: faker.random.alphaNumeric(4),
            customerTcChannel: faker.random.alphaNumeric(4),
            feeAmount: faker.random.number({ min: 0, max: 5000 })
          }
        ]
      };

      const refundedTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.GENERAL_REFUND,
        transactionAmount: 50000,
        id: 'test1234',
        additionalInformation4: originalTransaction.id,
        interchange: BankNetworkEnum.GENERAL_REFUND_DEFAULT
      };

      beforeEach(() => {
        (transactionExecutionService.executeTransaction as jest.Mock).mockResolvedValueOnce(
          refundedTransaction
        );

        (refundValidator.validateGenaralRefundRequest as jest.Mock).mockReturnValueOnce(
          null
        );
        (transactionService.updateOriginalTransactionWithRefund as jest.Mock).mockResolvedValueOnce(
          {
            ...originalTransaction,
            refund: {
              remainingAmount: 0,
              transactions: [refundedTransaction.id]
            }
          }
        );
        (transactionService.updateRefundTransaction as jest.Mock).mockResolvedValueOnce(
          {
            ...refundedTransaction,
            refund: {
              originalTransactionId: originalTransaction.id,
              revertPaymentServiceCode: originalTransaction.paymentServiceCode
            }
          }
        );
      });

      test('should successfully refund transaction', async () => {
        const input = { transactionId: 'transactionId' };
        (transactionRepository.getByKey as jest.Mock)
          .mockResolvedValueOnce(originalTransaction)
          .mockResolvedValueOnce(refundedTransaction);

        const transaction = await generalRefundService.refundTransaction(input);

        expect(transaction).toEqual(refundedTransaction);
        expect(transactionRepository.getByKey).toHaveBeenNthCalledWith(
          1,
          'transactionId'
        );
        expect(transactionRepository.getByKey).toHaveBeenNthCalledWith(
          2,
          refundedTransaction.id
        );
      });

      test('should throw error when refund transaction not found', async () => {
        const input = { transactionId: 'transactionId' };
        (transactionRepository.getByKey as jest.Mock)
          .mockResolvedValueOnce(originalTransaction)
          .mockResolvedValueOnce(null);

        let error;
        await generalRefundService.refundTransaction(input).catch(err => {
          error = err;
        });

        expect(error).toBeDefined();
        expect(error.errorCode).toEqual(ERROR_CODE.TRANSACTION_NOT_FOUND);
        expect(transactionRepository.getByKey).toHaveBeenNthCalledWith(
          1,
          'transactionId'
        );
        expect(transactionRepository.getByKey).toHaveBeenNthCalledWith(
          2,
          refundedTransaction.id
        );
      });
    });
  });
});
