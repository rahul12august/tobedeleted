import { BaseExternalTransactionInput } from '../../transaction.type';
import { PaymentServiceTypeEnum } from '@dk/module-common';
import { ITransactionModel } from '../../transaction.model';
import { TransactionStatus } from '../../transaction.enum';
import refundValidator from '../refund.validator';
import { getTransactionModelFullData } from '../../__mocks__/transaction.data';
import { ERROR_CODE } from '../../../common/errors';
import { TransferAppError } from '../../../errors/AppError';
import configurationRepository from '../../../configuration/configuration.repository';
import { getServiceReccommendation } from '../../../configuration/__mocks__/configuration.data';

jest.mock('../../../configuration/configuration.repository');

describe('refundValidator', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('validateRefundRequest', () => {
    it('should throw error INVALID_REFUND_REQUEST when original transaction status is other than SUCCEED', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 30000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO,
        notes: '243780485742'
      };
      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.DECLINED
      };
      // When
      try {
        refundValidator.validateRefundRequest(payload, originalTransaction);
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(TransferAppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });

    it('should throw error INVALID_REFUND_REQUEST when original transaction beneficiaryAccountNo is equal to originalTransaction sourceAccountNo ', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 30000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO,
        notes: '243780485742'
      };
      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.SUCCEED
      };
      // When
      try {
        refundValidator.validateRefundRequest(payload, originalTransaction);
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(TransferAppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });

    it('should throw error INVALID_REFUND_REQUEST when refund ammount is more than original transaction ammount ', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 30000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO,
        notes: '243780485742'
      };
      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.SUCCEED,
        sourceAccountNo: payload.beneficiaryAccountNo,
        transactionAmount: payload.transactionAmount - 500
      };
      // When
      try {
        refundValidator.validateRefundRequest(payload, originalTransaction);
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(TransferAppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });

    it('should throw error INVALID_REFUND_REQUEST when 2nd transaction refund ammount is more than refund remaining ammount ', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 1200,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO,
        notes: '243780485742'
      };
      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.SUCCEED,
        sourceAccountNo: payload.beneficiaryAccountNo,
        transactionAmount: 3000,
        refund: { remainingAmount: 0 }
      };
      // When
      try {
        refundValidator.validateRefundRequest(payload, originalTransaction);
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(TransferAppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });

    it('Should success for 1st refund transaction', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 10000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT,
        notes: '243780485742',
        sourceBankCode: 'SC001',
        beneficiaryBankCode: 'BC001'
      };
      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.SUCCEED,
        sourceAccountNo: payload.beneficiaryAccountNo,
        transactionAmount: 30000,
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT,
        paymentServiceCode: 'POS_VISA',
        beneficiaryBankCode: 'SC001',
        sourceBankCode: 'BC001'
      };
      // When

      const result = refundValidator.validateRefundRequest(
        payload,
        originalTransaction
      );
      expect(result).toBeUndefined();
    });

    it('Should throw error INVALID_REFUND_REQUEST when payment service type not match original transaction payment code', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 10000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.VOID_RTGS,
        notes: '243780485742',
        sourceBankCode: 'SC001',
        beneficiaryBankCode: 'BC001'
      };
      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.SUCCEED,
        sourceAccountNo: payload.beneficiaryAccountNo,
        transactionAmount: 30000,
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT,
        paymentServiceCode: 'POS_VISA',
        beneficiaryBankCode: 'SC001',
        sourceBankCode: 'BC001'
      };
      // When
      try {
        refundValidator.validateRefundRequest(payload, originalTransaction);
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(TransferAppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });

    it.each`
      paymentServiceCode | paymentServiceType
      ${'BRANCH_RTGS'}   | ${PaymentServiceTypeEnum.BRANCH_TRANSFER}
      ${'RTGS_SHARIA'}   | ${PaymentServiceTypeEnum.TRANSFER}
    `(
      'Should success for refund RTGS transaction when original transaction payment code is $paymentServiceCode',
      async ({ paymentServiceCode, paymentServiceType }) => {
        // Given
        const payload: BaseExternalTransactionInput = {
          transactionAmount: 10000,
          beneficiaryAccountNo: '100942531246',
          paymentServiceType: PaymentServiceTypeEnum.VOID_RTGS,
          notes: '243780485742',
          sourceBankCode: 'SC001',
          beneficiaryBankCode: 'BC001'
        };
        let newTransactionModel = getTransactionModelFullData();
        const originalTransaction: ITransactionModel = {
          id: '5e412688fd14310c4969099a',
          ...newTransactionModel,
          status: TransactionStatus.SUCCEED,
          sourceAccountNo: payload.beneficiaryAccountNo,
          transactionAmount: 30000,
          paymentServiceType: paymentServiceType,
          paymentServiceCode: paymentServiceCode,
          beneficiaryBankCode: 'SC001',
          sourceBankCode: 'BC001'
        };
        // When
        const result = refundValidator.validateRefundRequest(
          payload,
          originalTransaction
        );
        expect(result).toBeUndefined();
      }
    );

    it.each`
      paymentServiceCode | paymentServiceType
      ${'SKN_SHARIA'}    | ${PaymentServiceTypeEnum.TRANSFER}
    `(
      'Should success for refund SKN transaction when original transaction payment code is $paymentServiceCode',
      async ({ paymentServiceCode, paymentServiceType }) => {
        // Given
        const payload: BaseExternalTransactionInput = {
          transactionAmount: 10000,
          beneficiaryAccountNo: '100942531246',
          paymentServiceType: PaymentServiceTypeEnum.VOID_SKN,
          notes: '243780485742',
          sourceBankCode: 'SC001',
          beneficiaryBankCode: 'BC001'
        };
        let newTransactionModel = getTransactionModelFullData();
        const originalTransaction: ITransactionModel = {
          id: '5e412688fd14310c4969099a',
          ...newTransactionModel,
          status: TransactionStatus.SUCCEED,
          sourceAccountNo: payload.beneficiaryAccountNo,
          transactionAmount: 30000,
          paymentServiceType: paymentServiceType,
          paymentServiceCode: paymentServiceCode,
          beneficiaryBankCode: 'SC001',
          sourceBankCode: 'BC001'
        };
        // When
        const result = refundValidator.validateRefundRequest(
          payload,
          originalTransaction
        );
        expect(result).toBeUndefined();
      }
    );

    it('Should throw error INVALID_REFUND_REQUEST when original payment service type is not RTGS_SHARIA', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 10000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.VOID_RTGS,
        notes: '243780485742',
        sourceBankCode: 'SC001',
        beneficiaryBankCode: 'BC001'
      };
      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.SUCCEED,
        sourceAccountNo: payload.beneficiaryAccountNo,
        transactionAmount: 30000,
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT,
        paymentServiceCode: 'POS_VISA',
        beneficiaryBankCode: 'SC001',
        sourceBankCode: 'BC001'
      };
      // When
      try {
        refundValidator.validateRefundRequest(payload, originalTransaction);
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(TransferAppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });

    it('Should throw error INVALID_REFUND_REQUEST when original payment service type is not SKN_SHARIA', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 10000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.VOID_SKN,
        notes: '243780485742',
        sourceBankCode: 'SC001',
        beneficiaryBankCode: 'BC001'
      };
      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.SUCCEED,
        sourceAccountNo: payload.beneficiaryAccountNo,
        transactionAmount: 30000,
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT,
        paymentServiceCode: 'POS_VISA',
        beneficiaryBankCode: 'SC001',
        sourceBankCode: 'BC001'
      };
      // When
      try {
        refundValidator.validateRefundRequest(payload, originalTransaction);
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(TransferAppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });
  });

  describe('General Refund validator', () => {
    let newTransactionModel = getTransactionModelFullData();

    it('Should throw error when orginalTransaction paymentSreviceType is not in list', async () => {
      // Given
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        paymentServiceCode: 'REFUND_GENERAL',
        status: TransactionStatus.SUCCEED
      };
      const serviceReccommendationData = getServiceReccommendation();
      (configurationRepository.getServiceRecommendations as jest.Mock).mockResolvedValueOnce(
        serviceReccommendationData
      );
      // When
      try {
        await refundValidator.validateGenaralRefundRequest(originalTransaction);
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(TransferAppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });

    it('Should throw error when orginalTransaction status is not Succeed', async () => {
      // Given
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        paymentServiceCode: 'SIT03',
        status: TransactionStatus.DECLINED
      };
      const serviceReccommendationData = getServiceReccommendation();
      (configurationRepository.getServiceRecommendations as jest.Mock).mockResolvedValueOnce(
        serviceReccommendationData
      );
      // When
      try {
        await refundValidator.validateGenaralRefundRequest(originalTransaction);
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(TransferAppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });

    it('Should throw error when orginalTransaction contains refund object', async () => {
      // Given
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        paymentServiceCode: 'SIT03',
        status: TransactionStatus.SUCCEED,
        refund: {
          originalTransactionId: '5e412688fd14310c4969099a'
        }
      };
      const serviceReccommendationData = getServiceReccommendation();
      (configurationRepository.getServiceRecommendations as jest.Mock).mockResolvedValueOnce(
        serviceReccommendationData
      );
      // When
      try {
        await refundValidator.validateGenaralRefundRequest(originalTransaction);
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(TransferAppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });

    it('Should not throw error when all conditions pass', async () => {
      // Given
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        paymentServiceCode: 'SIT03',
        status: TransactionStatus.SUCCEED
      };
      const serviceReccommendationData = getServiceReccommendation();
      (configurationRepository.getServiceRecommendations as jest.Mock).mockResolvedValueOnce(
        serviceReccommendationData
      );
      // When
      const result = await refundValidator.validateGenaralRefundRequest(
        originalTransaction
      );
      //Then
      expect(result).toBeUndefined();
    });
  });
});
