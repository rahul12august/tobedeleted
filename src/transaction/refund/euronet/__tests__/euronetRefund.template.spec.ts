import { PaymentServiceTypeEnum } from '@dk/module-common';
import { BaseExternalTransactionInput } from '../../../transaction.type';
import { TransferAppError } from '../../../../errors/AppError';
import { ERROR_CODE } from '../../../../common/errors';
import { TransactionStatus } from '../../../transaction.enum';
import { ITransactionModel } from '../../../transaction.model';
import { getTransactionModelFullData } from '../../../__mocks__/transaction.data';
import transactionRepository from '../../../transaction.repository';
import euronetRefundTemplate from '../../../refund/euronet/euronetRefund.template';

jest.mock('../../../transaction.repository');

describe('euronetRefundTemplate', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('isApplicable', () => {
    it('should return false if paymentServiceType is other than [DOMESTIC_VOID_PAYMENT,INTERNATIONAL_VOID_PAYMENT]', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO
      };
      // When
      const result = euronetRefundTemplate.isApplicable(payload);
      // Then
      expect(result).toEqual(false);
    });

    it.each([
      PaymentServiceTypeEnum.DOMESTIC_VOID_PAYMENT,
      PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT
    ])(
      'should return true when paymentServiceType is of  [DOMESTIC_VOID_PAYMENT,INTERNATIONAL_VOID_PAYMENT] and have notes',
      async paymentService => {
        // Given
        const payload: BaseExternalTransactionInput = {
          paymentServiceType: paymentService
        };
        // When
        const result = euronetRefundTemplate.isApplicable(payload);
        // Then
        expect(result).toEqual(true);
      }
    );
  });

  describe('getOriginalTransaction', () => {
    it.each([undefined, '22243780485742', '1223'])(
      'should throw error INVALID_REFUND_REQUEST when refund request notes is undefined or length !== 12',
      async notes => {
        // Given
        const payload: BaseExternalTransactionInput = {
          transactionAmount: 30000,
          beneficiaryAccountNo: '100942531246',
          paymentServiceType: PaymentServiceTypeEnum.DOMESTIC_VOID_PAYMENT,
          notes
        };
        // When
        try {
          await euronetRefundTemplate.getOriginalTransaction(payload);
        } catch (err) {
          // Then
          expect(err).toBeInstanceOf(TransferAppError);
          expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
        }
      }
    );

    it('should throw error INVALID_REFUND_REQUEST when original transaction is not available', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 30000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO,
        notes: '243780485742'
      };
      (transactionRepository.getIncomingNonRefunded as jest.Mock).mockResolvedValueOnce(
        null
      );

      // When
      try {
        await euronetRefundTemplate.getOriginalTransaction(payload);
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(TransferAppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });

    it('should original transaction when is original transaction is available', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 30000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO,
        notes: '243780485742'
      };

      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.SUCCEED,
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT
      };
      (transactionRepository.getIncomingNonRefunded as jest.Mock).mockResolvedValueOnce(
        originalTransaction
      );

      // When
      const response = await euronetRefundTemplate.getOriginalTransaction(
        payload
      );
      expect(response).toEqual(originalTransaction);
    });
  });
});
