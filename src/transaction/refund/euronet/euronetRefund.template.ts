import { PaymentServiceTypeEnum } from '@dk/module-common';
import { includes } from 'lodash';
import { ERROR_CODE } from '../../../common/errors';
import logger from '../../../logger';
import { EURONET_REFUND_EXTERNAL_ID_LENGTH } from '../../transaction.constant';
import { ITransactionModel } from '../../transaction.model';
import transactionRepository from '../../transaction.repository';
import { BaseExternalTransactionInput } from '../../transaction.type';
import { IRefundTemplate } from '../refund.type';
import { TransferFailureReasonActor } from '../../transaction.enum';
import { TransferAppError } from '../../../errors/AppError';

const isApplicable = (payload: BaseExternalTransactionInput): boolean =>
  includes(
    [
      PaymentServiceTypeEnum.DOMESTIC_VOID_PAYMENT,
      PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT
    ],
    payload.paymentServiceType
  );

const getOriginalTransaction = async (
  payload: BaseExternalTransactionInput
): Promise<ITransactionModel> => {
  if (
    !(
      payload.notes &&
      payload.notes.length === EURONET_REFUND_EXTERNAL_ID_LENGTH
    )
  ) {
    const detail = 'notes is required for refund type req!';
    logger.error(`validateRefundRequest: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_REFUND_REQUEST
    );
  }

  const originalTransaction = await transactionRepository.getIncomingNonRefunded(
    payload.notes as string,
    payload.beneficiaryAccountNo as string
  );
  if (!originalTransaction) {
    const detail = `Transaction not found for refund request: ${payload.notes}!`;
    logger.error(`validateRefundRequest: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_REFUND_REQUEST
    );
  }
  return originalTransaction;
};

const euronetRefundTemplate: IRefundTemplate = {
  isApplicable,
  getOriginalTransaction
};

export default euronetRefundTemplate;
