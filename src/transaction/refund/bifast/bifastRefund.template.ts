import { PaymentServiceTypeEnum } from '@dk/module-common';
import { includes } from 'lodash';
import { ERROR_CODE } from '../../../common/errors';
import { TransferAppError } from '../../../errors/AppError';
import logger from '../../../logger';
import { ITransactionModel } from '../../transaction.model';
import transactionRepository from '../../transaction.repository';
import { BaseExternalTransactionInput } from '../../transaction.type';
import { IRefundTemplate } from '../refund.type';
import { TransferFailureReasonActor } from '../../transaction.enum';

const isApplicable = (payload: BaseExternalTransactionInput): boolean =>
  includes([PaymentServiceTypeEnum.VOID_BIFAST], payload.paymentServiceType);

const getOriginalTransaction = async (
  payload: BaseExternalTransactionInput
): Promise<ITransactionModel> => {
  if (!payload.notes) {
    const detail = 'notes is required for refund type req!';
    logger.error(`getOriginalTransaction: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_REFUND_REQUEST
    );
  }

  const originalTransaction = await transactionRepository.findOneByQuery({
    thirdPartyOutgoingId: payload.notes as string
  });

  if (!originalTransaction) {
    const detail = `Transaction not found for refund request: ${payload.notes}!`;
    logger.error(`getOriginalTransaction: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_REFUND_REQUEST
    );
  }
  return originalTransaction;
};

const bifastRefundTemplate: IRefundTemplate = {
  isApplicable,
  getOriginalTransaction
};

export default bifastRefundTemplate;
