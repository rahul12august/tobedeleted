import { PaymentServiceTypeEnum } from '@dk/module-common';
import { BaseExternalTransactionInput } from '../../../transaction.type';
import jagoPayRefundTemplate from '../../jagoPay/jagoPayRefund.template';
import transactionRepository from '../../../transaction.repository';
import { TransferAppError } from '../../../../errors/AppError';
import { ERROR_CODE } from '../../../../common/errors';
import { getTransactionModelFullData } from '../../../__mocks__/transaction.data';
import { ITransactionModel } from '../../../transaction.model';
import { TransactionStatus } from '../../../transaction.enum';

jest.mock('../../../transaction.repository');

describe('jagoPayRefundTemplate', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('isApplicable', () => {
    it('should return false if paymentServiceType is other than [JAGOPAY_REFUND]', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO
      };
      // When
      const result = jagoPayRefundTemplate.isApplicable(payload);
      // Then
      expect(result).toEqual(false);
    });

    it('should return true when paymentServiceType is of  [JAGOPAY_REFUND] and have notes', () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        paymentServiceType: PaymentServiceTypeEnum.JAGOPAY_REFUND
      };
      // When
      const result = jagoPayRefundTemplate.isApplicable(payload);
      // Then
      expect(result).toEqual(true);
    });
  });

  describe('getOriginalTransaction', () => {
    it.each([undefined, null])(
      'should throw error INVALID_REFUND_REQUEST when refund request externalId is undefined or null',
      async notes => {
        // Given
        const payload: BaseExternalTransactionInput = {
          transactionAmount: 30000,
          beneficiaryAccountNo: '100942531246',
          paymentServiceType: PaymentServiceTypeEnum.DOMESTIC_VOID_PAYMENT,
          notes
        };
        // When
        try {
          await jagoPayRefundTemplate.getOriginalTransaction(payload);
        } catch (err) {
          // Then
          expect(err).toBeInstanceOf(TransferAppError);
          expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
        }
      }
    );

    it('should throw error INVALID_REFUND_REQUEST when original transaction is not available', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 30000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO,
        notes: '243780485742'
      };
      (transactionRepository.getByExternalId as jest.Mock).mockResolvedValueOnce(
        null
      );

      // When
      try {
        await jagoPayRefundTemplate.getOriginalTransaction(payload);
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(TransferAppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });

    it('should original transaction when is original transaction is available', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 30000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO,
        notes: '243780485742'
      };

      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.SUCCEED,
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT
      };
      (transactionRepository.getByExternalId as jest.Mock).mockResolvedValueOnce(
        originalTransaction
      );

      // When
      const response = await jagoPayRefundTemplate.getOriginalTransaction(
        payload
      );
      expect(response).toEqual(originalTransaction);
    });
  });
});
