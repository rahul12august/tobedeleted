import { PaymentServiceTypeEnum } from '@dk/module-common';
import { includes } from 'lodash';
import { BaseExternalTransactionInput } from '../../transaction.type';
import { IRefundTemplate } from '../refund.type';
import { ITransactionModel } from '../../transaction.model';
import logger from '../../../logger';
import { ERROR_CODE } from '../../../common/errors';
import transactionRepository from '../../transaction.repository';
import { TransferAppError } from '../../../errors/AppError';
import { TransferFailureReasonActor } from '../../transaction.enum';

const isApplicable = (payload: BaseExternalTransactionInput): boolean =>
  includes(
    [PaymentServiceTypeEnum.VOID_RTGS, PaymentServiceTypeEnum.VOID_SKN],
    payload.paymentServiceType
  );

const getOriginalTransaction = async (
  payload: BaseExternalTransactionInput
): Promise<ITransactionModel> => {
  if (!payload.notes) {
    const detail = `notes is required for ${payload.paymentServiceType} refund req!`;
    logger.error(`validateRefundRequest: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_REFUND_REQUEST
    );
  }

  const originalTransaction = await transactionRepository.findOneByQuery({
    thirdPartyOutgoingId: payload.notes as string
  });

  if (!originalTransaction) {
    const detail = `Transaction not found for refund request: ${payload.notes}!`;
    logger.error(`validateRefundRequest: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_REFUND_REQUEST
    );
  }
  return originalTransaction;
};

const sknRtgsRefundTemplate: IRefundTemplate = {
  isApplicable,
  getOriginalTransaction
};

export default sknRtgsRefundTemplate;
