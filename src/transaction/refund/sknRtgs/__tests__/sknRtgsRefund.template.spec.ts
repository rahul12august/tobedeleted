import { PaymentServiceTypeEnum } from '@dk/module-common';
import { BaseExternalTransactionInput } from '../../../transaction.type';
import sknRtgsRefundTemplate from '../sknRtgsRefund.template';
import transactionRepository from '../../../transaction.repository';
import { TransferAppError } from '../../../../errors/AppError';
import { ERROR_CODE } from '../../../../common/errors';
import { getTransactionModelFullData } from '../../../__mocks__/transaction.data';
import { ITransactionModel } from '../../../transaction.model';
import { TransactionStatus } from '../../../transaction.enum';

jest.mock('../../../transaction.repository');

describe('sknRtgsRefundTemplate', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('isApplicable', () => {
    it('should return false if paymentServiceType is other than [VOID_RTGS]', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO
      };
      // When
      const result = sknRtgsRefundTemplate.isApplicable(payload);
      // Then
      expect(result).toEqual(false);
    });

    it('should return true when paymentServiceType is of  [VOID_RTGS] and have notes', () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        paymentServiceType: PaymentServiceTypeEnum.VOID_RTGS
      };
      // When
      const result = sknRtgsRefundTemplate.isApplicable(payload);
      // Then
      expect(result).toEqual(true);
    });

    it('should return true when paymentServiceType is of  [VOID_SKN] and have notes', () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        paymentServiceType: PaymentServiceTypeEnum.VOID_SKN
      };
      // When
      const result = sknRtgsRefundTemplate.isApplicable(payload);
      // Then
      expect(result).toEqual(true);
    });
  });

  describe('getOriginalTransaction', () => {
    it.each([undefined, null])(
      'should throw error INVALID_REFUND_REQUEST when refund request externalId is undefined or null',
      async notes => {
        // Given
        const payload: BaseExternalTransactionInput = {
          transactionAmount: 30000,
          beneficiaryAccountNo: '100942531246',
          paymentServiceType: PaymentServiceTypeEnum.VOID_RTGS,
          notes
        };
        // When
        let error;
        try {
          await sknRtgsRefundTemplate.getOriginalTransaction(payload);
        } catch (err) {
          error = err;
        }
        // Then
        expect(error).toBeInstanceOf(TransferAppError);
        expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
        expect(transactionRepository.findOneByQuery).not.toHaveBeenCalled();
      }
    );

    it('should throw error INVALID_REFUND_REQUEST when original transaction is not available', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 30000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.VOID_RTGS,
        notes: '243780485742'
      };
      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
        null
      );

      // When
      let error;
      try {
        await sknRtgsRefundTemplate.getOriginalTransaction(payload);
      } catch (err) {
        error = err;
      }

      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      expect(transactionRepository.findOneByQuery).toHaveBeenCalledWith({
        thirdPartyOutgoingId: payload.notes
      });
    });

    it('should original transaction when is original transaction is available for VOID_RTGS', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 30000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.VOID_RTGS,
        notes: '243780485742'
      };

      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.SUCCEED,
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        paymentServiceCode: 'RTGS'
      };
      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
        originalTransaction
      );

      // When
      const response = await sknRtgsRefundTemplate.getOriginalTransaction(
        payload
      );

      // Then
      expect(response).toEqual(originalTransaction);
      expect(transactionRepository.findOneByQuery).toHaveBeenCalledWith({
        thirdPartyOutgoingId: payload.notes
      });
    });

    it('should original transaction when is original transaction is available for VOID_SKN', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 30000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.VOID_SKN,
        notes: '243780485742'
      };

      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.SUCCEED,
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        paymentServiceCode: 'SKN'
      };
      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
        originalTransaction
      );

      // When
      const response = await sknRtgsRefundTemplate.getOriginalTransaction(
        payload
      );

      // Then
      expect(response).toEqual(originalTransaction);
      expect(transactionRepository.findOneByQuery).toHaveBeenCalledWith({
        thirdPartyOutgoingId: payload.notes
      });
    });
  });
});
