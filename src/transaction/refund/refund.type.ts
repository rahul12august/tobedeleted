import { BaseExternalTransactionInput } from '../transaction.type';
import { ITransactionModel } from '../transaction.model';

export interface IRefundTemplate {
  isApplicable(input: BaseExternalTransactionInput): boolean;
  getOriginalTransaction(
    input: BaseExternalTransactionInput
  ): Promise<ITransactionModel>;
}

export interface ICreateRefundParam {
  transactionId: string;
  originalTransaction: ITransactionModel;
  refundFees?: boolean;
  amountRefund?: number;
  refundIdentifier?: string;
  refundAddInfo1?: string;
}
