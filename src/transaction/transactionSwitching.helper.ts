import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { ITransactionModel } from './transaction.model';
import {
  inDeliveryChannelEnum,
  inServiceIdEnum,
  transactionProcessingCodeEnum,
  inSysOrgEnum,
  inTermIdEnum,
  inTermLocEnum
} from '../switching/switching.enum';
import {
  EuronetConfig,
  ISwitchingTransaction
} from '../switching/switching.type';
import { config } from '../config';
import { formatTransactionDate } from './transaction.util';
import configurationRepository from '../configuration/configuration.repository';
import logger from '../logger';
import {
  getJulianDate,
  getNextCounterByPrefix
} from '../common/externalIdGenerator.util';
import { TransferFailureReasonActor } from './transaction.enum';
const euronetConfig: EuronetConfig = config.get('switching').euronet;

export const IN_SEQUENCE_MAX_LENGTH = 15;

const IN_ADD_DATA_PROTION_LENGTH = 20;
const MAX_SEQUENCE_COUNTER = 9999999999; // 10 digit number (julian date took 5 digits already)

const generateInSequenceId = async (
  transactionCode?: string
): Promise<string> => {
  if (!transactionCode) {
    const detail = `Missing debit transaction code!`;
    logger.error(`generateInSequenceId: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE
    );
  }
  const julianDate = getJulianDate();

  const currentCounter = await getNextCounterByPrefix(
    `${julianDate}${transactionCode}`
  );
  if (currentCounter > MAX_SEQUENCE_COUNTER) {
    const detail = `Maximum outgoing ID counter is reached, cannot generate thirdPartyOutgoingId!`;
    logger.error(`generateInSequenceId: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.MAXIMUM_COUNTER_REACHED
    );
  }

  return `${julianDate}${currentCounter
    .toString()
    .padStart(IN_SEQUENCE_MAX_LENGTH - julianDate.length, '0')}`;
};

const getCurrencyIsoNumber = async (currencyCode?: string): Promise<number> => {
  if (!currencyCode) {
    const detail = 'Source transaction currency code is undefined!';
    logger.error(`generateInSequenceId: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.SOURCE_TRANSACTION_CURRENCY_UNDEFINED
    );
  }
  const currency = await configurationRepository.getCurrencyByCode(
    currencyCode
  );
  if (!currency) {
    throw new TransferAppError(
      'Transaction currency code was not found in configuration!',
      TransferFailureReasonActor.JAGO,
      ERROR_CODE.CURRENCY_NOT_FOUND
    );
  }
  return currency.isoNumber;
};

const createInAddDataPortion = (source: string): string => {
  let value = '';
  for (let i = 0; i < IN_ADD_DATA_PROTION_LENGTH; i++) {
    const char = source.charAt(i);
    if (char) {
      value = `${value}${char}`;
    } else {
      value = `${value} `;
    }
  }
  return value;
};

export const createInAddData = (transaction: ITransactionModel): string => {
  const { sourceAccountName, beneficiaryAccountName } = transaction;
  return `${createInAddDataPortion(
    sourceAccountName || ''
  )}${createInAddDataPortion(beneficiaryAccountName || '')}`;
};

export const mapToSwitchingTransaction = async (
  transaction: ITransactionModel
): Promise<ISwitchingTransaction> => {
  const [inSequenceId, currencyIsoNumber] = await Promise.all([
    generateInSequenceId(transaction.debitTransactionCode),
    getCurrencyIsoNumber(transaction.sourceTransactionCurrency)
  ]);
  return {
    inDeliveryChannel: inDeliveryChannelEnum.DEFAULT,
    inAuditData: {
      inApplication: euronetConfig.application,
      inUserId: euronetConfig.userId,
      inPassword: euronetConfig.password,
      inServiceID: inServiceIdEnum.FUND_TRANSFER,
      inSequence: inSequenceId
    },
    inProcessingCode: transactionProcessingCodeEnum.FUND_TRANSFER,
    inTxnAmt: transaction.transactionAmount.toString(),
    inTxnDateTime: formatTransactionDate(transaction.createdAt || new Date()),
    inSTAN: transaction.thirdPartyOutgoingId
      ? transaction.thirdPartyOutgoingId.substr(-6)
      : '',
    inAcqInstId: transaction.sourceRtolCode || '',
    inRRN: transaction.thirdPartyOutgoingId
      ? transaction.thirdPartyOutgoingId
      : '',
    inAddData: createInAddData(transaction),
    inCurrCode: currencyIsoNumber,
    inAcctId1: transaction.sourceAccountNo || '',
    inAcctId2: transaction.beneficiaryAccountNo || '',
    inDestInstId: transaction.beneficiaryRtolCode || '',
    inSysOrg: inSysOrgEnum.DEFAULT,
    inTermID: inTermIdEnum.DEFAULT,
    inTermLoc: inTermLocEnum.DEFAULT
  };
};
