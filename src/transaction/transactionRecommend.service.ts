/**
 * This module is used to calculate payment service code and transaction fee.
 * Payment service code is recommended by payment service rule in business configuration
 */
import { PaymentServiceTypeEnum } from '@dk/module-common';
import awardRepository from '../award/award.repository';
import { UsageCounter } from '../award/award.type';
import { ERROR_CODE, ErrorList } from '../common/errors';
import configurationRepository from '../configuration/configuration.repository';
import configurationService from '../configuration/configuration.service';
import depositRepository from '../coreBanking/deposit.repository';
import decisionEngineService from '../decisionEngine/decisionEngine.service';
import {
  JagoExternalBankCode,
  JagoShariaExternalBankCode,
  BankCodeTypes,
  TransferFailureReasonActor
} from './transaction.enum';
import {
  RetryableTransferAppError,
  TransferAppError
} from '../errors/AppError';
import logger, { wrapLogs } from '../logger';
import {
  JAGO_BANK_CODE,
  JAGO_SHARIA_BANK_CODE,
  PAYMENT_SERVICE_CODE_REFUND_GENERAL,
  REVERSAL_TC_SUFFIX
} from './transaction.constant';
import transactionHelper from './transaction.helper';
import {
  ITransactionConfiguration,
  ITransactionInput,
  RecommendationService
} from './transaction.type';
import transactionProducer from './transaction.producer';
import { NotificationCode } from './transaction.producer.enum';
import { isEmpty } from 'lodash';
import { Big as BigDecimal } from 'big.js';
import { isFeatureEnabled } from '../common/featureFlag';
import { FEATURE_FLAG } from '../common/constant';
import transactionUsageCommonService from '../transactionUsage/transactionUsageCommon.service';

const getTransactionConfiguration = async (
  paymentServiceType?: PaymentServiceTypeEnum | undefined,
  paymentServiceCode?: string | undefined,
  customerId?: string
): Promise<ITransactionConfiguration> => {
  const [configRules, limitGroups] = await Promise.all([
    configurationService.getPaymentConfigRules(
      paymentServiceType,
      paymentServiceCode,
      customerId
    ),
    configurationRepository.getLimitGroupsByCodes()
  ]);

  if (!configRules) {
    const detail =
      'Invalid register parameter when no paymentConfig rules found!';
    logger.error(detail);
    throw new RetryableTransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED,
      true
    );
  }

  if (!limitGroups || limitGroups.length === 0) {
    const detail = 'Invalid register parameter when no limitGroupCodes found!';
    logger.error(detail);
    throw new RetryableTransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED,
      true
    );
  }

  if (customerId) {
    await transactionUsageCommonService.applyCustomLimitGroupsIfApplicable(
      customerId,
      limitGroups
    );
    await configurationService.applyCustomLimits(customerId, configRules);
  }

  return {
    limitGroupCodes: limitGroups,
    paymentConfigRules: configRules
  };
};

const validateInputAccountsAndCif = (transactionInput: ITransactionInput) => {
  if (
    transactionInput.paymentServiceType !==
      PaymentServiceTypeEnum.GENERAL_REFUND &&
    transactionInput.sourceAccountNo ===
      transactionInput.beneficiaryAccountNo &&
    transactionInput.beneficiaryCIF === transactionInput.sourceCIF
  ) {
    const detail = `Error because sourceAccount equals beneficiaryAccount: ${transactionInput.sourceAccountNo} and source cif equals beneficiary cif: ${transactionInput.sourceCIF}!`;
    logger.error(`validateInputAccountsAndCif: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_ACCOUNT
    );
  }
};

const getCustomerUsageCounters = async (
  customerId?: string
): Promise<UsageCounter[]> => {
  return customerId ? awardRepository.getUsageCounters(customerId) : [];
};

const suggestRecommendationServices = async (
  input: ITransactionInput,
  usageCounters: UsageCounter[]
): Promise<RecommendationService[]> => {
  const {
    paymentConfigRules: configRules,
    limitGroupCodes: limitGroups
  } = await getTransactionConfiguration(
    input.paymentServiceType,
    input.paymentServiceCode,
    input.sourceCustomerId
  );
  const executionDate = new Date();
  const paymentServiceMappings = await transactionHelper.getPaymentServiceMappings(
    input,
    configRules,
    limitGroups,
    usageCounters,
    executionDate
  );

  if (!paymentServiceMappings.length) {
    logger.info(
      'no services code found, return decision: NO_RECOMMENDATION_SERVICES'
    );

    throw new RetryableTransferAppError(
      `Services code not found!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.NO_RECOMMENDATION_SERVICES,
      false,
      [
        {
          message:
            ErrorList[ERROR_CODE.PAYMENT_CONFIG_RULES_EVALUATION_FAILED]
              .message,
          key: ERROR_CODE.PAYMENT_CONFIG_RULES_EVALUATION_FAILED,
          code: ERROR_CODE.PAYMENT_CONFIG_RULES_EVALUATION_FAILED
        }
      ]
    );
  }

  // 4. build recommendation codes
  const bankCodes = {
    [BankCodeTypes.RTOL]: input.beneficiaryRtolCode,
    [BankCodeTypes.BILLER]: input.billerCode,
    [BankCodeTypes.REMITTANCE]: input.beneficiaryRemittanceCode,
    [BankCodeTypes.IRIS]: input.beneficiaryIrisCode
  };

  return paymentServiceMappings.map(item => {
    const beneficiaryBankCode =
      bankCodes[item.beneficiaryBankCodeType as keyof typeof bankCodes];

    const recommended: RecommendationService = {
      ...item,
      beneficiaryBankCode
    };
    return recommended;
  });
};

const isStringGeneralRefundOrOfferType = (psCode: string): boolean => {
  return (
    psCode === PAYMENT_SERVICE_CODE_REFUND_GENERAL ||
    psCode === PaymentServiceTypeEnum.OFFER ||
    psCode === PaymentServiceTypeEnum.PAYROLL ||
    psCode === PaymentServiceTypeEnum.CASHBACK ||
    psCode === PaymentServiceTypeEnum.BONUS_INTEREST ||
    psCode === PaymentServiceTypeEnum.BRANCH_DEPOSIT ||
    psCode === PaymentServiceTypeEnum.TD_MANUAL_PAYMENT_GL
  );
};

const isGeneralRefundOrOfferType = (item: RecommendationService) =>
  !(item.debitTransactionCode && item.debitTransactionCode.length) &&
  item.creditTransactionCode &&
  item.creditTransactionCode.length &&
  isStringGeneralRefundOrOfferType(item.paymentServiceCode);

const isLoanTransaction = (paymentServiceType: PaymentServiceTypeEnum) =>
  paymentServiceType === PaymentServiceTypeEnum.LOAN_REPAYMENT ||
  paymentServiceType === PaymentServiceTypeEnum.LOAN_DISBURSEMENT ||
  paymentServiceType === PaymentServiceTypeEnum.LOAN_DIRECT_DISBURSEMENT ||
  paymentServiceType === PaymentServiceTypeEnum.LOAN_REFUND ||
  paymentServiceType === PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY ||
  paymentServiceType === PaymentServiceTypeEnum.LOAN_GL_DISBURSEMENT ||
  paymentServiceType === PaymentServiceTypeEnum.LOAN_GL_REPAYMENT;

const constructNotEnoughBalanceMessage = (
  balance: number,
  forDebit: boolean,
  trxAmount: number,
  fee: number
): string => {
  let result = `Insufficient balance ${balance} < `;

  if (forDebit) {
    result = result.concat(trxAmount.toString());
  }

  if (fee > 0) {
    if (forDebit) {
      result = result.concat(' + ');
    }
    result = result.concat(`${fee} (fee)`);
  }

  return result;
};

/**
 * Tests if the input represents a debit transaction
 * @param item The recommendation service input
 * @returns True if the transaction is a debit, except for reversals
 */
const isDebit = (item: RecommendationService): boolean => {
  //
  // This may not be correct because each item can potentially contain both debitTransactionCode and
  // creditTransactionCode
  //
  if (!item.debitTransactionCode) {
    return false;
  }
  if (isEmpty(item.debitTransactionCode)) {
    return false;
  }

  return !item.debitTransactionCode[0].transactionCode.endsWith(
    REVERSAL_TC_SUFFIX
  );
};

/**
 * Tests if the transaction request input denotes a transaction involving money being
 * debited from Jago bank.
 * @param input Transaction request input
 * @returns True if the transnsaction requests has Jago Bank (conventional or sharia) as
 * the source bank. If there's not information to decide, returns false.
 */
const amountIsDebitedFromJago = (input: ITransactionInput): boolean => {
  if (
    input.sourceRtolCode &&
    (input.sourceRtolCode === JagoExternalBankCode.RTOL_CODE ||
      input.sourceRtolCode === JagoShariaExternalBankCode.RTOL_CODE)
  ) {
    return true;
  }

  if (
    input.sourceRemittanceCode &&
    (input.sourceRemittanceCode === JagoExternalBankCode.REMITTANCE_CODE ||
      input.sourceRemittanceCode === JagoShariaExternalBankCode.REMITTANCE_CODE)
  ) {
    return true;
  }

  if (
    input.sourceBankCode &&
    (input.sourceBankCode == JAGO_BANK_CODE ||
      input.sourceBankCode == JAGO_SHARIA_BANK_CODE)
  ) {
    return true;
  }

  logger.info(`Not checking balance, input is ${JSON.stringify(input)}`);
  return false;
};

/**
 * Picks up the payment service code from the array of recommendation services and associates it with
 * the transaction input in case the corresponding property is not populated yet. This is because in case
 * of failures there's visibility on the association between transaction request and the pscode determined
 * by the recommendation service algorithm.
 * @param input The transfer request
 * @param recommendedServices The list of recommendation services found by the recommendation service algorithm.
 */
const associatePaymentServiceCode = (
  input: ITransactionInput,
  recommendedServices: RecommendationService[]
): void => {
  if (recommendedServices.length === 0) {
    //
    // The array must not be empty (previous steps do already throw exceptions when array is empty).
    // The condition here is added as extra measure.
    //
    throw new TransferAppError(
      `Services code not found!`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.NO_RECOMMENDATION_SERVICES
    );
  }

  if (!input.paymentServiceCode || input.paymentServiceCode === '') {
    let psCodes = recommendedServices
      .map(entry => entry.paymentServiceCode)
      .filter((value, index, arr) => {
        return arr.indexOf(value) === index;
      });

    if (psCodes.length > 1) {
      logger.warn(
        `Recommended services mapped to more than one pscode: ${psCodes}`
      );
    }

    input.paymentServiceCode = psCodes[0];
    logger.info(`Associated pscode ${input.paymentServiceCode}`);
  }
};

const isExceptedFromBalanceVerification = (
  psType: PaymentServiceTypeEnum
): boolean => {
  if (isLoanTransaction(psType)) {
    return true;
  }

  if (isStringGeneralRefundOrOfferType(psType)) {
    return true;
  }

  return (
    psType === PaymentServiceTypeEnum.MIGRATION_TRANSFER ||
    psType === PaymentServiceTypeEnum.SETTLEMENT ||
    psType === PaymentServiceTypeEnum.TD_SHARIA_REPAYMENT ||
    psType === PaymentServiceTypeEnum.LOAN_BULK_TAKEOVER_TRANSFER_REVERSAL ||
    psType === PaymentServiceTypeEnum.LOAN_FORWARD_PAYMENT_WITHDRAWAL
  );
};

const filterByAvailableBalance = async (
  transactionInput: ITransactionInput,
  recommendedServices: RecommendationService[]
): Promise<[RecommendationService]> => {
  logger.info(
    `getRecommendServices: filtered recommendedServices:
  ${JSON.stringify(recommendedServices)}`
  );

  const { sourceAccountInfo, ...input } = transactionInput;

  const balanceCanBeQueried =
    sourceAccountInfo && sourceAccountInfo.accountNumber && input.sourceCIF;

  if (!balanceCanBeQueried) {
    return recommendedServices as [RecommendationService];
  }

  const balanceShouldBeChecked =
    amountIsDebitedFromJago(input) &&
    !isExceptedFromBalanceVerification(input.paymentServiceType);

  if (!balanceShouldBeChecked) {
    return recommendedServices as [RecommendationService];
  }

  let sourceAccountBalance = 0;

  if (sourceAccountInfo?.accountNumber) {
    sourceAccountBalance = await depositRepository.getAvailableBalance(
      sourceAccountInfo.accountNumber
    );

    logger.info(
      `Checking trx input against account: ${sourceAccountInfo.accountNumber}`
    );
  }

  recommendedServices = recommendedServices.filter(item => {
    const { feeData } = item;
    // ideally for 1 service code, fee amount is the same for multiple interchange, we take [0] by default
    const fee = (feeData && feeData[0] && feeData[0].feeAmount) || 0;
    const isDebitTrx = isDebit(item);

    // Maybe in the future to consider deducting the fee from the recently received credit.
    if (
      BigDecimal(isDebitTrx ? input.transactionAmount : 0)
        .add(BigDecimal(fee))
        .sub(BigDecimal(sourceAccountBalance)) > BigDecimal(0)
    ) {
      const insufficientBalanceMsg = constructNotEnoughBalanceMessage(
        sourceAccountBalance,
        isDebitTrx,
        input.transactionAmount,
        fee
      );

      logger.info(
        `${insufficientBalanceMsg}, pscode: ${item.paymentServiceCode}`
      );
      return false;
    }
    return true;
  });

  return recommendedServices as [RecommendationService];
};

const filterByAvailableBalanceOriginal = async (
  transactionInput: ITransactionInput,
  recommendedServices: RecommendationService[]
): Promise<[RecommendationService]> => {
  const { sourceAccountInfo, ...input } = transactionInput;

  const sourceAccountBalance =
    input.paymentServiceType !== PaymentServiceTypeEnum.OFFER &&
    input.paymentServiceType !== PaymentServiceTypeEnum.PAYROLL &&
    input.paymentServiceType !== PaymentServiceTypeEnum.CASHBACK &&
    input.paymentServiceType !== PaymentServiceTypeEnum.BRANCH_DEPOSIT &&
    input.paymentServiceType !== PaymentServiceTypeEnum.LOAN_DISBURSEMENT &&
    input.paymentServiceType !== PaymentServiceTypeEnum.SETTLEMENT &&
    input.paymentServiceType !== PaymentServiceTypeEnum.LOAN_GL_DISBURSEMENT &&
    input.paymentServiceType !==
      PaymentServiceTypeEnum.LOAN_DIRECT_DISBURSEMENT &&
    input.paymentServiceType !==
      PaymentServiceTypeEnum.LOAN_FORWARD_PAYMENT_WITHDRAWAL &&
    input.paymentServiceType !== PaymentServiceTypeEnum.LOAN_REPAYMENT &&
    input.paymentServiceType !== PaymentServiceTypeEnum.LOAN_GL_REPAYMENT &&
    input.paymentServiceType !== PaymentServiceTypeEnum.TD_SHARIA_REPAYMENT &&
    input.paymentServiceType !== PaymentServiceTypeEnum.BONUS_INTEREST &&
    input.paymentServiceType !== PaymentServiceTypeEnum.MIGRATION_TRANSFER &&
    input.paymentServiceType !== PaymentServiceTypeEnum.LOAN_REFUND &&
    input.paymentServiceType !==
      PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY &&
    input.paymentServiceType !== PaymentServiceTypeEnum.TD_MANUAL_PAYMENT_GL &&
    sourceAccountInfo &&
    sourceAccountInfo.accountNumber &&
    input.sourceCIF
      ? await depositRepository.getAvailableBalance(
          sourceAccountInfo.accountNumber
        )
      : undefined;

  logger.info(
    `getRecommendServices: filtered recommendedServices: 
    ${JSON.stringify(recommendedServices)}`
  );

  const filtered = recommendedServices.filter(item => {
    const { feeData } = item;
    // ideally for 1 service code, fee amount is the same for multiple interchange, we take [0] by default
    const fee = (feeData && feeData[0] && feeData[0].feeAmount) || 0;
    if (
      !(
        item.debitTransactionCode &&
        !isEmpty(item.debitTransactionCode) &&
        !item.debitTransactionCode[0].transactionCode.endsWith(
          REVERSAL_TC_SUFFIX
        ) &&
        item.debitTransactionCode.length
      ) &&
      fee === 0
    ) {
      return true; // skip, only validate amount when having debit or fee transfer
    }
    if (isGeneralRefundOrOfferType(item)) {
      return true;
    }

    if (isLoanTransaction(input.paymentServiceType)) {
      return true;
    }

    if (
      !sourceAccountBalance ||
      BigDecimal(input.transactionAmount)
        .add(BigDecimal(fee))
        .sub(BigDecimal(sourceAccountBalance)) > BigDecimal(0)
    ) {
      logger.info(
        `Transfer amount insufficient. Source acc balance: ${sourceAccountBalance}, ` +
          `input psType: ${input.paymentServiceType}, input amount: ${input.transactionAmount}, fee: ${fee}` +
          `item psCode: ${item.paymentServiceCode}`
      );
      return false;
    }
    return true;
  });

  return filtered as [RecommendationService];
};

/**
 * Get all possible recommend service codes of given input
 *
 * @param transactionInput the transaction input
 */
const getRecommendServices = async (
  transactionInput: ITransactionInput
): Promise<[RecommendationService]> => {
  validateInputAccountsAndCif(transactionInput);
  const { sourceAccountInfo, ...input } = transactionInput;

  //get all usage counters by customerId
  let usageCounters: UsageCounter[] = await getCustomerUsageCounters(
    transactionInput.sourceCustomerId
  );

  let recommendedServices = await suggestRecommendationServices(
    transactionInput,
    usageCounters
  );

  transactionHelper.validateTransactionLimitAmount(
    input.transactionAmount,
    recommendedServices,
    transactionInput.interchange
  );

  recommendedServices = await transactionHelper.mapFeeData(
    recommendedServices,
    transactionInput,
    usageCounters
  );

  associatePaymentServiceCode(transactionInput, recommendedServices);

  if (isFeatureEnabled(FEATURE_FLAG.LEGACY_BALANCE_CHECK_KEY)) {
    recommendedServices = await filterByAvailableBalanceOriginal(
      transactionInput,
      recommendedServices
    );
  } else {
    recommendedServices = await filterByAvailableBalance(
      transactionInput,
      recommendedServices
    );
  }

  if (!recommendedServices.length) {
    logger.error(
      'getRecommendServices: no recommendedServices because of insufficient account balance.'
    );

    const isDebitCardTransaction = await configurationService.isDebitCardTransactionGroup(
      transactionInput.paymentServiceType
    );

    if (isDebitCardTransaction) {
      await transactionProducer.sendFailedDebitCardTransactionAmount(
        transactionInput,
        NotificationCode.NOTIF_DEBIT_CARD_INSUFFICIENT_BALANCE
      );
    }

    throw new TransferAppError(
      'Insufficient account balance.',
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
    );
  }

  if (
    transactionInput.executorCIF &&
    transactionInput.sourceCIF !== transactionInput.executorCIF
  ) {
    for (const recommendedService of recommendedServices) {
      const decisionEngineConfig = await configurationRepository.getDecisionEngineConfig(
        recommendedService.paymentServiceCode
      );

      if (!decisionEngineConfig) {
        const detail = `Decision engine config not found for paymentServiceCode: ${recommendedService.paymentServiceCode}!`;
        logger.error(detail);
        throw new TransferAppError(
          detail,
          TransferFailureReasonActor.SUBMITTER,
          ERROR_CODE.DECISION_ENGINE_CONFIG_NOT_FOUND
        );
      }

      const resultRoles = decisionEngineService.runMultipleRoles(
        decisionEngineConfig,
        transactionInput
      );
      logger.info('DC Engine result: ', resultRoles);
      if (!resultRoles) {
        const detail = 'Failure in multiple decision config roles!';
        logger.error(detail);
        throw new RetryableTransferAppError(
          detail,
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.NO_RECOMMENDATION_SERVICES,
          false
        );
      }
    }
  }

  return recommendedServices as [RecommendationService]; // already throw error in case there's no recommend
};

/**
 * Get only to recommend service that matched with given paymentServiceCode from the input
 * if input is missing paymentServiceCode, get the first one found
 * Throw exception if there is not qualified recommend code
 * @param transactionInput the transaction input
 */
const getRecommendService = async (
  transactionInput: ITransactionInput
): Promise<RecommendationService> => {
  try {
    const recommendedServices = await getRecommendServices(transactionInput);
    let recommendedService: RecommendationService | undefined;
    if (transactionInput.paymentServiceCode) {
      recommendedService = recommendedServices.find(
        recommendedService =>
          recommendedService.paymentServiceCode ==
          transactionInput.paymentServiceCode
      );
      if (!recommendedService) {
        const detail = 'No recommended service found!';
        logger.error(`getRecommendService: ${detail}`);
        throw new TransferAppError(
          detail,
          TransferFailureReasonActor.MS_TRANSFER,
          ERROR_CODE.INCONSISTENT_PAYMENT_SERVICE
        );
      }
    } else {
      recommendedService = recommendedServices[0];
    }

    return recommendedService;
  } catch (err) {
    logger.error(
      `getRecommendService: Error in recommendation services journey: ${JSON.stringify(
        transactionInput.journey
      )}`
    );
    throw err;
  }
};

const getRecommendedServiceWithoutFeeData = async (
  transactionInput: ITransactionInput,
  usageCounters: UsageCounter[]
): Promise<RecommendationService> => {
  validateInputAccountsAndCif(transactionInput);
  if (!transactionInput.paymentServiceCode) {
    const detail = `Error because Payment Service Code was not specified in the input`;
    logger.error(`getRecommendedServiceWithoutFeeData: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.PAYMENT_SERVICE_CODE_NOT_FOUND
    );
  }

  let recommendedServices = await suggestRecommendationServices(
    transactionInput,
    usageCounters
  );

  const selectedService = recommendedServices.find(
    recommendedService =>
      recommendedService.paymentServiceCode ==
      transactionInput.paymentServiceCode
  );
  if (!selectedService) {
    const detail = 'No recommended service found!';
    logger.error(`getRecommendedServiceWithoutFeeData: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.INCONSISTENT_PAYMENT_SERVICE
    );
  }

  return selectedService;
};

const recommendedCodeService = wrapLogs({
  getRecommendServices,
  getRecommendService,
  getRecommendedServiceWithoutFeeData,
  getCustomerUsageCounters
});

export default recommendedCodeService;
