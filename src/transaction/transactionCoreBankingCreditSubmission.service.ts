/**
 * @module transactionCoreBankingCreditSubmission
 *
 * A template for credit transaction submission to core banking
 */

import { BankChannelEnum } from '@dk/module-common';
import { ITransactionModel } from './transaction.model';
import { wrapLogs } from '../logger';
import { ITransactionSubmissionTemplate } from './transaction.type';
import helper from './transactionCoreBanking.helper';
import { TransferJourneyStatusEnum } from './transaction.enum';
import { Rail } from '@dk/module-common';
import { REVERSAL_TC_SUFFIX } from './transaction.constant';

const submitTransaction = async (
  transactionModel: ITransactionModel
): Promise<ITransactionModel> => {
  if (
    transactionModel.creditTransactionCode &&
    transactionModel.creditTransactionChannel &&
    transactionModel.beneficiaryAccountNo
  ) {
    const creditTransactionId = await helper.submitCreditTransaction(
      transactionModel.beneficiaryAccountNo,
      transactionModel,
      transactionModel.creditTransactionCode,
      transactionModel.creditTransactionChannel
    );
    //TODO : refactor to use coreBankingTransactions field
    transactionModel.coreBankingTransactionIds?.push(creditTransactionId);
    transactionModel.coreBankingTransactions?.push({
      id: creditTransactionId,
      type: TransferJourneyStatusEnum.AMOUNT_CREDITED
    });
  }
  return transactionModel;
};

const isEligibleBankCodeChannel = (
  channel: BankChannelEnum | undefined
): boolean => {
  // channel is PARTNER when external top-up.
  // when it's external top-up, amount will be credited to merchant's account
  if (
    BankChannelEnum.INTERNAL === channel ||
    BankChannelEnum.PARTNER === channel
  ) {
    return true;
  }
  return false;
};

const isEligible = (model: ITransactionModel): boolean => {
  if (
    model.creditTransactionCode &&
    model.creditTransactionChannel &&
    model.beneficiaryAccountNo &&
    isEligibleBankCodeChannel(model.beneficiaryBankCodeChannel) &&
    !model.creditTransactionCode.endsWith(REVERSAL_TC_SUFFIX)
  ) {
    return true;
  }
  return false;
};

const getRail = () => Rail.INTERNAL;

const internalCreditSubmissionTemplate: ITransactionSubmissionTemplate = {
  submitTransaction,
  isEligible,
  getRail
};

export default wrapLogs(internalCreditSubmissionTemplate);
