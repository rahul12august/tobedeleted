import {
  INotificationMessage,
  ITransactionFailedMessage,
  ITransactionMessage,
  ITransactionResult,
  NotificationTopicConstant,
  TransactionTopicConstant
} from '@dk/module-message';
import producer from '../common/kafkaProducer';
import {
  IATMTransferExceedsDailyLimitMessage,
  IATMWithdrawalExceedsDailyLimitMessage,
  IPendingBillPaymentMessage,
  ITransactionRetryReversalMessage
} from './transaction.producer.type';
import { NotificationCode } from './transaction.producer.enum';
import { ITransactionModel } from './transaction.model';
import logger, { wrapLogs } from '../logger';
import {
  Constant,
  Formater,
  PaymentServiceTypeEnum,
  TransactionExecutionTypeEnum
} from '@dk/module-common';
import { ERROR_CODE } from '../common/errors';
import { toIndonesianRupiah } from '../common/currencyFormatter';
import { to24HourFormat } from '../common/dateUtils';
import kafkaUtils from '../common/kafka.util';
import outgoingSubmissionTemplateService from './transactionSubmissionTemplate.service';
import {
  ITransactionInput,
  ITransactionSubmissionTemplate
} from './transaction.type';
import customerRepository from '../customer/customer.repository';
import altoUtil from '../alto/alto.utils';
import { IDR_CURRENCY_PREFIX } from './transaction.constant';

export const reversalRetryTopic = 'transfer.retry-transaction-reversal';
export const reversalRetryFailedTopic =
  'transfer.retry-transaction-reversal-failed';

const shouldSendNotification = (transaction: ITransactionModel): boolean => {
  const submissionTemplate:
    | ITransactionSubmissionTemplate
    | undefined = outgoingSubmissionTemplateService.getSubmissionTemplate(
    transaction
  );
  return submissionTemplate && submissionTemplate.shouldSendNotification
    ? submissionTemplate.shouldSendNotification()
    : true;
};

const sendFailedTransferNotification = async (
  transaction: ITransactionModel
): Promise<void> => {
  if (!shouldSendNotification(transaction)) {
    logger.info(`sendTransactionFailedMessage : not sending failed message for
      paymentServiceCode: ${transaction.paymentServiceCode},
      externalId: ${transaction.externalId},
      status: ${transaction.status}`);
    return;
  }

  const notificationMessage: INotificationMessage = {
    notificationCode: NotificationCode.NOTIFICATION_FAILED_TRANSFER,
    cif: transaction.sourceCIF || '',
    payload: {
      amount: Formater.formatMoney(transaction.transactionAmount, {
        currency: transaction.sourceTransactionCurrency || ''
      }),
      beneficiaryName: transaction.beneficiaryAccountName || '',
      accountNumber: transaction.beneficiaryAccountNo || ''
    }
  };
  if (transaction.paymentServiceType === PaymentServiceTypeEnum.QRIS) {
    const customerData = await customerRepository.getCustomerInfoById(
      transaction.sourceCustomerId || ''
    );
    notificationMessage.notificationCode = NotificationCode.NOTIF_FAILED_QRIS;
    notificationMessage.payload.customerName = customerData?.fullName || '';
  }
  await producer.sendSerializedValue({
    topic: NotificationTopicConstant.NOTIFICATION_CREATED,
    messages: [
      {
        value: notificationMessage,
        headers: kafkaUtils.getHeaders()
      }
    ]
  });
};

const toTransactionMessage = (
  customerId: string,
  transaction: ITransactionModel
): ITransactionMessage => {
  return {
    transactionId: transaction.id,
    sourceCIF: transaction.sourceCIF,
    sourceAccountNo: transaction.sourceAccountNo,
    sourceAccountType: transaction.sourceAccountType,
    beneficiaryCIF: transaction.beneficiaryCIF,
    beneficiaryAccountNo: transaction.beneficiaryAccountNo,
    beneficiaryAccountType: transaction.beneficiaryAccountType,
    referenceId: transaction.referenceId,
    externalId: transaction.externalId,
    status: transaction.status,
    customerId,
    updatedAt: transaction.updatedAt?.toISOString(),
    categoryCode: transaction.categoryCode,
    transactionAmount: transaction.transactionAmount,
    paymentRequestID: transaction.paymentRequestID,
    additionalInformation1: transaction.additionalInformation1,
    additionalInformation2: transaction.additionalInformation2,
    executorCIF: transaction.executorCIF,
    beneficiaryName: transaction.beneficiaryAccountName,
    paymentServiceCode: transaction.paymentServiceCode,
    notes: transaction.note
  };
};

const sendTransactionSucceededMessage = async (
  customerId: string,
  transaction: ITransactionModel
) => {
  const transactionMessage = toTransactionMessage(customerId, transaction);
  logger.info(`sendTransactionSucceededMessage : sending succeeded message for ${customerId}
  paymentServiceCode: ${transactionMessage.paymentServiceCode},
  externalId: ${transactionMessage.externalId},
  status: ${transactionMessage.status},
  transactionId: ${transactionMessage.transactionId}`);
  await producer.sendSerializedValue({
    topic: TransactionTopicConstant.TRANSFER_TRANSACTION_SUCCEEDED,
    messages: [
      {
        value: transactionMessage,
        headers: kafkaUtils.getHeaders()
      }
    ]
  });
};

const sendTransactionFailedMessage = async (
  customerId: string,
  transaction: ITransactionModel
) => {
  if (!shouldSendNotification(transaction)) {
    logger.info(`sendTransactionFailedMessage : not sending failed message for ${customerId}
        paymentServiceCode: ${transaction.paymentServiceCode},
        externalId: ${transaction.externalId},
        status: ${transaction.status}`);
    return;
  }

  const transactionMessage = toTransactionMessage(
    customerId,
    transaction
  ) as ITransactionFailedMessage;
  transactionMessage.failureReson = transaction.failureReason;
  logger.info(`sendTransactionFailedMessage : sending failed message for ${customerId}
  paymentServiceCode: ${transactionMessage.paymentServiceCode},
  externalId: ${transactionMessage.externalId},
  status: ${transactionMessage.status},
  transactionId: ${transactionMessage.transactionId}`);

  await producer.sendSerializedValue({
    topic: TransactionTopicConstant.TRANSFER_TRANSACTION_FAILED,
    messages: [
      {
        value: transactionMessage,
        headers: kafkaUtils.getHeaders()
      }
    ]
  });
};

const sendTransactionRetryReversalCommand = async (
  message: ITransactionRetryReversalMessage
) => {
  await producer.sendSerializedValue({
    topic: reversalRetryTopic,
    messages: [
      {
        value: message,
        headers: kafkaUtils.getHeaders()
      }
    ]
  });
};

const sendTransactionReversalFailedEvent = async (
  message: ITransactionRetryReversalMessage
) => {
  await producer.sendSerializedValue({
    topic: reversalRetryFailedTopic,
    messages: [
      {
        value: message,
        headers: kafkaUtils.getHeaders()
      }
    ]
  });
};

const sendPendingBillPaymentNotification = async (
  message: IPendingBillPaymentMessage
) => {
  const notification: INotificationMessage = {
    notificationCode: NotificationCode.NOTIFICATION_PENDING_BILLPAYMENT,
    customerId: message.billDetails.customerId,
    payload: {
      amount: toIndonesianRupiah(message.transactionAmount),
      billKey: message.billDetails.primaryBillKey.value,
      billerName: message.billDetails.billerName,
      transactionId: message.transactionId,
      transactionTime: to24HourFormat(message.transactionTime)
    }
  };
  logger.info(
    `Bill Payment : Sending notification for pending status with notification code : ${notification.notificationCode} 
    transactionId : ${notification.payload.transactionId} `
  );
  await producer.sendSerializedValue({
    topic: NotificationTopicConstant.NOTIFICATION_CREATED,
    messages: [
      {
        value: notification,
        headers: kafkaUtils.getHeaders()
      }
    ]
  });
};

const sendPendingBlockedAmountNotification = async (
  transaction: ITransactionModel,
  notificationCode: NotificationCode
) => {
  if (!transaction.sourceCustomerId && !transaction.sourceCIF) {
    logger.error(
      'Blocked Amount Notification , customer not found with transaction payload'
    );
    throw new Error(ERROR_CODE.CUSTOMER_NOT_FOUND);
  }
  const notification: INotificationMessage = {
    notificationCode: notificationCode,
    customerId: transaction.sourceCustomerId,
    cif: transaction.sourceCIF,
    payload: {
      amount: toIndonesianRupiah(transaction.transactionAmount),
      beneficiaryName: transaction.beneficiaryAccountName,
      transactionId: transaction.id,
      transactionTime: to24HourFormat(
        transaction.createdAt || transaction.updatedAt
      )
    }
  };
  logger.info(
    `Blocked Amount : Sending notification for pending status with notification code ${notification.notificationCode} 
     transactionId : ${transaction.id} customerId : ${transaction.sourceCustomerId} cif : ${transaction.sourceCIF}`
  );
  await producer.sendSerializedValue({
    topic: NotificationTopicConstant.NOTIFICATION_CREATED,
    messages: [
      {
        value: notification,
        headers: kafkaUtils.getHeaders()
      }
    ]
  });
};

const sendTransactionMessage = async (
  topic: string,
  message: ITransactionResult,
  executionType: TransactionExecutionTypeEnum,
  paymentInstructionCode: string
): Promise<void> => {
  await producer.sendSerializedValue({
    topic: topic,
    messages: [
      {
        headers: {
          [Constant.EXECUTION_TYPE]: executionType,
          [Constant.PI_KEY_HEADER]: paymentInstructionCode,
          ...kafkaUtils.getHeaders()
        },
        value: message
      }
    ]
  });
};

const sendQrisTransactionNotification = async (
  transaction: ITransactionModel,
  notificationCode: NotificationCode
) => {
  if (!transaction.sourceCustomerId && !transaction.sourceCIF) {
    logger.error(
      'sendQrisTransactionNotification: Cannot send notification: Customer is not found in transaction'
    );
    throw new Error(ERROR_CODE.CUSTOMER_NOT_FOUND);
  }
  const notification: INotificationMessage = {
    notificationCode: notificationCode,
    customerId: transaction.sourceCustomerId,
    cif: transaction.sourceCIF,
    payload: altoUtil.buildQrisNotificationMessage(transaction)
  };
  logger.info(
    `sendQrisTransactionNotification: Sending QRIS transaction notification.
      Notification Code: ${notification.notificationCode} 
      Transaction ID : ${transaction.id}
      Customer ID : ${transaction.sourceCustomerId}
      Source CIF : ${transaction.sourceCIF}`
  );
  await producer.sendSerializedValue({
    topic: NotificationTopicConstant.NOTIFICATION_CREATED,
    messages: [
      {
        value: notification,
        headers: kafkaUtils.getHeaders()
      }
    ]
  });
};

const sendFailedDebitCardTransactionAmount = async (
  transaction: ITransactionInput,
  notificationCode: NotificationCode
) => {
  if (!transaction.sourceCustomerId && !transaction.sourceCIF) {
    logger.error(
      'sendFailedDebitCardTransactionAmount: Cannot send notification: Customer is not found in transaction'
    );
  }
  const notification: INotificationMessage = {
    notificationCode: notificationCode,
    customerId: transaction.sourceCustomerId,
    cif: transaction.sourceCIF,
    payload: {
      amount: toIndonesianRupiah(transaction.transactionAmount),
      customerName: transaction.sourceAccountName,
      merchantName: transaction.beneficiaryAccountName
    }
  };
  logger.info(
    `sendFailedDebitCardTransactionAmount: Sending sendFailedDebitCardTransactionAmount transaction notification.
      Notification Code: ${notification.notificationCode}
      Customer ID : ${transaction.sourceCustomerId}
      customerName: ${transaction.sourceAccountName}
      merchantName: ${transaction.beneficiaryAccountName}`
  );
  await producer.sendSerializedValue({
    topic: NotificationTopicConstant.NOTIFICATION_CREATED,
    messages: [
      {
        value: notification,
        headers: kafkaUtils.getHeaders()
      }
    ]
  });
};

const sendFailedCashWithdrawalDailyLimit = async (
  message: IATMWithdrawalExceedsDailyLimitMessage,
  notificationCode: NotificationCode
) => {
  const notification: INotificationMessage = {
    notificationCode: notificationCode,
    customerId: message.customerId,
    payload: {
      pocketName: message.pocketName,
      amount: `${IDR_CURRENCY_PREFIX}${toIndonesianRupiah(message.amount)}`
    }
  };
  logger.info(
    `sendFailedCashWithdrawalDailyLimit: Sending sendFailedCashWithdrawalDailyLimit transaction notification.
      Notification Code: ${notification.notificationCode}
      Customer ID : ${message.customerId}
      pocketName: ${message.pocketName}
    `
  );
  await producer.sendSerializedValue({
    topic: NotificationTopicConstant.NOTIFICATION_CREATED,
    messages: [
      {
        value: notification,
        headers: kafkaUtils.getHeaders()
      }
    ]
  });
};

const sendFailedMoneyTransferViaAtmDailyLimit = async (
  message: IATMTransferExceedsDailyLimitMessage,
  notificationCode: NotificationCode
) => {
  const notification: INotificationMessage = {
    notificationCode: notificationCode,
    customerId: message.customerId,
    payload: {
      accountNumber: message.beneficiaryAccountNo,
      beneficiaryName: message.beneficiaryName,
      amount: `${IDR_CURRENCY_PREFIX}${toIndonesianRupiah(message.amount)}`
    }
  };

  await producer.sendSerializedValue({
    topic: NotificationTopicConstant.NOTIFICATION_CREATED,
    messages: [
      {
        value: notification,
        headers: kafkaUtils.getHeaders()
      }
    ]
  });
};

export default wrapLogs({
  sendFailedTransferNotification,
  sendTransactionSucceededMessage,
  sendTransactionFailedMessage,
  sendTransactionRetryReversalCommand,
  sendTransactionReversalFailedEvent,
  sendTransactionMessage,
  sendPendingBillPaymentNotification,
  sendPendingBlockedAmountNotification,
  sendQrisTransactionNotification,
  sendFailedDebitCardTransactionAmount,
  sendFailedCashWithdrawalDailyLimit,
  sendFailedMoneyTransferViaAtmDailyLimit
});
