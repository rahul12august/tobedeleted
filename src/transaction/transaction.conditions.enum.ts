export enum RuleConditionState {
  ACCEPTED = 'ACCEPTED',
  DECLINED = 'DECLINED',
  TRY_TOMORROW = 'TRY_TOMORROW'
}
