/*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/

import {
  IBankCode,
  IGeneralLedger,
  INNSMapping,
  ITransactionCodeInfo
} from '../configuration/configuration.type';
import {
  ConfirmTransactionQueryInput,
  ConfirmTransactionResponse,
  IExternalTransactionResponse,
  IRefundTransactionBasicDetail,
  ITopupInternalTransactionInput,
  ITransactionInfoByInterchange,
  ITransactionLimitAmount,
  ITransactionResponseValidator,
  ITransferTransactionInput,
  TransactionDetail
} from './transaction.type';
import {
  DEFAULT_EXCHANGE_RATE,
  DEFAULT_ORIGINAL_CURRENCY,
  REVERSAL_TC_SUFFIX,
  TRANSACTION_DATE_FORMAT_STRING
} from './transaction.constant';
import { IFeeModel, ITransactionModel } from './transaction.model';
import { ErrorDetails, TransferAppError } from '../errors/AppError';
import { ERROR_CODE, ErrorList } from '../common/errors';
import uuidv1 from 'uuid/v1';
import {
  GLAccountAmount,
  ICardWithdrawalInput,
  IDepositTransaction,
  IGLJournalEntriesV2,
  IJournalEntriesGL,
  ITransactionInput,
  ITransactionInputCustomFields
} from '../coreBanking/deposit.interface';
import moment from 'moment';
import {
  AdminPortalUserPermissionCode,
  BankChannel,
  BankChannelEnum,
  BankNetworkEnum,
  Http,
  PaymentServiceTypeEnum,
  Util
} from '@dk/module-common';
import { IFeeAmount } from '../fee/fee.type';
import { NewEntity } from '../common/common.type';
import logger, { wrapLogs } from '../logger';
import { punycodeUtil } from '../common/encodeString.util';
import { get, includes, isEmpty, isUndefined, map } from 'lodash';
import { ITransactionExtra } from '@dk/module-message';
import { HttpError } from '@dk/module-httpclient';
import { getRequestId, getRetryCount } from '../common/contextHandler';
import { isDateWithinYear } from '../common/dateUtils';
import {
  GlTransactionTypes,
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from './transaction.enum';
import { getErrorDetails } from '../errors/error.util';
import { FeeRuleType } from '../fee/fee.constant';
import {
  DEFAULT_MASKING_PLACEHOLDER,
  SENSITIVE_CREDIT_TRANSACTION_CODES,
  SENSITIVE_DEBIT_TRANSACTION_CODES,
  UTC_OFFSET_WESTERN_INDONESIAN
} from '../common/constant';
import coreBankingConstant from '../coreBanking/coreBanking.constant';
import { AmountTypeEnum } from '../coreBanking/loan.enum';
import {
  IDisbursementPayload,
  IPayOffPayload,
  IRepaymentPayload
} from '../coreBanking/loan.interface';
import { IErrorDetail } from '@dk/module-common/dist/error/error.interface';
import { IAccount } from '../account/account.type';
import { AccountStatusEnum } from '../account/account.enum';
import {
  enforceBooleanFormat,
  enforceNumberFormat,
  enforceStringFormat
} from '../common/common.util';
import { BeneficiaryProxyType } from '../bifast/bifast.enum';
import { IAltoCreatePaymentRequestData } from '../alto/alto.type';
import { ISSUER_NNS } from '../alto/alto.constant';
import altoUtils from '../alto/alto.utils';
import { AltoAccountTypeEnum } from '../alto/alto.enum';
import configurationRepository from '../configuration/configuration.repository';
import {
  BIFAST_PROXY_BANK_CODE,
  BIFAST_PROXY_TYPES,
  BIFAST_VOID_PAYMENT_SERVICE_CODE
} from '../bifast/bifast.constant';
import transactionRepository from './transaction.repository';
import transactionHelper from './transaction.helper';
import { AsyncLocalStorage } from 'async_hooks';

export const RetryConfig = () => ({
  shouldRetry: (error: HttpError) => {
    return error.status >= 500;
  },
  numberOfTries: 1
});

export const IdempotentRetryConfig = () => ({
  shouldRetry: (error: HttpError) => {
    return error.status >= 500;
  },
  numberOfTries: getRetryCount() || 1
});

export const isBIFast = (paymentServiceCode?: string): boolean =>
  paymentServiceCode?.indexOf(BankChannel.BIFAST) !== -1;

const isRefundBifast = (paymentServiceCode?: string): boolean =>
  paymentServiceCode?.indexOf(BIFAST_VOID_PAYMENT_SERVICE_CODE) !== -1;

const getTargetAccountNo = (
  transactionModel: ITransactionModel
): string | undefined =>
  isBIFast(transactionModel.paymentServiceCode) &&
  transactionModel.additionalInformation4 &&
  !isRefundBifast(transactionModel.paymentServiceCode)
    ? transactionModel.additionalInformation4
    : transactionModel.beneficiaryAccountNo;

const isBiFastProxy = (transactionModel: ITransactionModel): boolean =>
  isBIFast(transactionModel.paymentServiceCode) &&
  !isEmpty(transactionModel.additionalInformation3) &&
  BIFAST_PROXY_TYPES.includes(
    transactionModel.additionalInformation3 as BeneficiaryProxyType
  );

const mapToCoreBankingTransactionCustomFields = (
  transactionModel: ITransactionModel,
  transactionCode: string
): ITransactionInputCustomFields => {
  return {
    _Custom_Transaction_Details: {
      Category_Code: transactionModel.categoryCode || undefined,
      Payment_Service_Code: transactionModel.paymentServiceCode || '',
      Transaction_Id: transactionModel.id,
      Payment_Instruction_ID: transactionModel.id,
      PI_Id: transactionModel.paymentInstructionId,
      Reference_ID: transactionModel.referenceId,
      Third_Party_Incoming_Id: transactionModel.externalId,
      Transaction_Code: transactionCode,
      Notes: transactionModel.note
        ? punycodeUtil.encodeString(transactionModel.note)
        : undefined,
      Currency: transactionModel.sourceTransactionCurrency || undefined,
      Original_Currency: DEFAULT_ORIGINAL_CURRENCY,
      Original_Amount: transactionModel.transactionAmount,
      Exchange_Rate: DEFAULT_EXCHANGE_RATE,
      Additional_Information_1:
        transactionModel.additionalInformation1 || undefined,
      Additional_Information_2:
        transactionModel.additionalInformation2 || undefined,
      Additional_Information_3:
        transactionModel.additionalInformation3 || undefined,
      Additional_Information_4:
        transactionModel.additionalInformation4 || undefined,
      Third_Party_Outgoing_Id:
        transactionModel.thirdPartyOutgoingId || undefined,
      Request_Id: getRequestId()
    },
    _Custom_Source_Information: {
      Source_Account_Type: transactionModel.sourceAccountType,
      Source_Account_No: !isEmpty(transactionModel.sourceAccountNo)
        ? transactionModel.sourceAccountNo
        : undefined,
      Source_Account_Name: !isEmpty(transactionModel.sourceAccountName)
        ? transactionModel.sourceAccountName
        : undefined,
      Executor_CIF: transactionModel.executorCIF,
      Source_CIF: transactionModel.sourceCIF,
      Source_Bank_Code: transactionModel.sourceBankCode
    },
    _Custom_Target_Information: {
      Institutional_Bank_Code: isBiFastProxy(transactionModel)
        ? BIFAST_PROXY_BANK_CODE
        : transactionModel.beneficiaryBankCode,
      Real_Target_Bank_Code: transactionModel.beneficiaryRealBankCode,
      Target_Account_Type: transactionModel.beneficiaryAccountType,
      Target_Account_No: getTargetAccountNo(transactionModel),
      Target_Account_Name: !isEmpty(transactionModel.beneficiaryAccountName)
        ? transactionModel.beneficiaryAccountName
        : undefined,
      Target_Bank_Name: isBiFastProxy(transactionModel)
        ? undefined
        : transactionModel.beneficiaryBankName,
      Target_CIF: transactionModel.beneficiaryCIF
    }
  };
};

export const mapToCoreBankingCardTransaction = (
  transactionModel: ITransactionModel,
  transactionCode: string,
  transactionChannel: string,
  blockingId: string
): ICardWithdrawalInput => {
  const customFields = mapToCoreBankingTransactionCustomFields(
    transactionModel,
    transactionCode
  );
  return {
    ...customFields,
    amount: transactionModel.transactionAmount,
    externalReferenceId: blockingId, // safely use blockingId as externalReferenceId because it unique for each card transaction
    externalAuthorizationReferenceId: blockingId,
    transactionChannelId: transactionChannel
  };
};

export const mapToCoreBankingTransaction = (
  transactionModel: ITransactionModel,
  transactionCode: string,
  transactionChannel: string
): ITransactionInput => {
  const customFields = mapToCoreBankingTransactionCustomFields(
    transactionModel,
    transactionCode
  );
  return {
    ...customFields,
    transactionDetails: {
      transactionChannelId: transactionChannel
    },
    amount: transactionModel.transactionAmount,
    notes: customFields._Custom_Transaction_Details.Notes
  };
};

export const getErrorMessage = (col: string): ErrorDetails => ({
  message: ErrorList[ERROR_CODE.INVALID_REGISTER_PARAMETER].message,
  code: ERROR_CODE.INVALID_REGISTER_PARAMETER,
  key: col
});

export const GenerateAppError = (
  detail: string,
  actor: TransferFailureReasonActor,
  errorCode: ERROR_CODE,
  key: string
): TransferAppError => {
  return new TransferAppError(
    detail,
    actor,
    errorCode,
    getErrorDetails(errorCode, key)
  );
};

export const mapDebitTransactionLimitAmount = (
  transactionCodeInfo: ITransactionCodeInfo
): ITransactionLimitAmount => ({
  transactionCode: transactionCodeInfo.code,
  transactionMinAmount: transactionCodeInfo.minAmountPerTx,
  transactionMaxAmount: transactionCodeInfo.maxAmountPerTx
});

export const generateUniqueId = (): string => {
  return uuidv1();
};

export const toTransactionDetail = (
  transaction: ITransactionModel
): TransactionDetail => {
  const fees =
    transaction.fees &&
    transaction.fees.map(fee => {
      return {
        feeAmount: fee.feeAmount
      };
    });
  const refund = transaction.refund && {
    remainingAmount: transaction.refund.remainingAmount,
    originalTransactionId: transaction.refund.originalTransactionId,
    transactions: transaction.refund.transactions,
    revertPaymentServiceCode: transaction.refund.revertPaymentServiceCode
  };

  const reason =
    transaction.reason &&
    transaction.reason.map(reason => {
      return {
        responseCode: reason.responseCode,
        responseMessage: reason.responseMessage,
        transactionDate: reason.transactionDate
      };
    });
  const journey =
    transaction.journey &&
    transaction.journey.map(data => {
      return {
        status: data.status,
        updatedAt: data.updatedAt
      };
    });
  return {
    paymentRequestID: transaction.paymentRequestID,
    sourceCIF: transaction.sourceCIF,
    sourceAccountType: transaction.sourceAccountType,
    sourceAccountNo: transaction.sourceAccountNo,
    sourceAccountName: transaction.sourceAccountName,
    sourceBankCode: transaction.sourceBankCode,
    sourceTransactionCurrency: transaction.sourceTransactionCurrency,
    sourceBankCodeChannel: transaction.sourceBankCodeChannel,
    transactionAmount: transaction.transactionAmount,
    beneficiaryCIF: transaction.beneficiaryCIF,
    beneficiaryBankCode: transaction.beneficiaryBankCode,
    beneficiaryAccountType: transaction.beneficiaryAccountType,
    beneficiaryAccountNo: transaction.beneficiaryAccountNo,
    beneficiaryAccountName: transaction.beneficiaryAccountName,
    beneficiaryBankName: transaction.beneficiaryBankName,
    beneficiaryBankCodeChannel: transaction.beneficiaryBankCodeChannel,
    creditPriority: transaction.creditPriority,
    debitPriority: transaction.debitPriority,
    institutionalType: transaction.institutionalType,
    note: transaction.note,
    paymentServiceType: transaction.paymentServiceType,
    extendedMessage: transaction.extendedMessage,
    status: transaction.status,
    fees,
    externalId: transaction.externalId,
    referenceId: transaction.referenceId,
    additionalInformation1: transaction.additionalInformation1,
    additionalInformation2: transaction.additionalInformation2,
    additionalInformation3: transaction.additionalInformation3,
    additionalInformation4: transaction.additionalInformation4,
    categoryCode: transaction.categoryCode,
    promotionCode: transaction.promotionCode,
    sourceCustomerId: transaction.sourceCustomerId,
    refund,
    blockingId: transaction.blockingId,
    cardId: transaction.cardId,
    transactionId: transaction.id,
    confirmationCode: transaction.confirmationCode,
    reason,
    paymentServiceCode: transaction.paymentServiceCode,
    createdAt: transaction.createdAt,
    journey: journey,
    ownerCustomerId: transaction.ownerCustomerId
  };
};
const shouldMaskSensitiveTransaction = (
  transaction: ITransactionModel,
  maskData: boolean
) => {
  const isSensitiveCreditTransaction =
    transaction.creditTransactionCode &&
    SENSITIVE_CREDIT_TRANSACTION_CODES.includes(
      transaction.creditTransactionCode
    );

  const isSensitiveDebitTransaction =
    transaction.debitTransactionCode &&
    SENSITIVE_DEBIT_TRANSACTION_CODES.includes(
      transaction.debitTransactionCode
    );

  return (
    maskData && (isSensitiveCreditTransaction || isSensitiveDebitTransaction)
  );
};
export const toTransactionList = (
  transactionList: ITransactionModel[],
  adminRoles: AdminPortalUserPermissionCode[],
  refundTransactions: IRefundTransactionBasicDetail[],
  fetchRefund: boolean
): TransactionDetail[] => {
  const hasEmployeeReadRole = adminRoles.includes(
    AdminPortalUserPermissionCode.EMPLOYEE_INFO_READ
  );
  const refundTransactionsMap = new Map(
    refundTransactions.map(trx => [trx.id, trx])
  );
  return map(transactionList, transaction => {
    const transactionData: TransactionDetail = toTransactionDetail(transaction);
    const applyMasking = shouldMaskSensitiveTransaction(
      transaction,
      !hasEmployeeReadRole
    );

    transactionData.fees =
      transactionData.fees &&
      transactionData.fees.map(fee => {
        return {
          feeAmount: applyMasking ? DEFAULT_MASKING_PLACEHOLDER : fee.feeAmount
        };
      });

    if (transactionData.refund) {
      transactionData.refund.remainingAmount = applyMasking
        ? DEFAULT_MASKING_PLACEHOLDER
        : transactionData.refund.remainingAmount;

      if (fetchRefund && transactionData.refund.transactions) {
        for (const transactionId of transactionData.refund.transactions) {
          let refundTransaction:
            | IRefundTransactionBasicDetail
            | undefined = refundTransactionsMap.get(transactionId);
          if (!refundTransaction) continue;
          if (refundTransaction.status == TransactionStatus.SUCCEED) {
            transactionData.refund.latestTransaction = refundTransaction;
            break;
          } else if (
            !transactionData.refund.latestTransaction ||
            transactionData.refund.latestTransaction.updatedAt <
              refundTransaction.updatedAt
          ) {
            transactionData.refund.latestTransaction = refundTransaction;
          }
        }
      }
    }
    transactionData.transactionAmount = applyMasking
      ? DEFAULT_MASKING_PLACEHOLDER
      : transactionData.transactionAmount;
    return transactionData;
  });
};

export const isDepositTransactionNotReverted = (
  depositTransaction: IDepositTransaction
) => {
  logger.info(
    `isDepositTransactionNotReverted: 
    coreBankingId: ${depositTransaction.id}, 
    adjustmentTransactionKey: ${depositTransaction.adjustmentTransactionKey}
    cardToken: ${depositTransaction.cardTransaction &&
      depositTransaction.cardTransaction?.cardToken},
    externalReferenceId: ${depositTransaction.cardTransaction &&
      depositTransaction.cardTransaction?.externalReferenceId}`
  );
  return !depositTransaction.adjustmentTransactionKey;
};

export const mapTransactionResultToResponse = (
  result: ITransactionModel
): ITransactionResponseValidator => ({
  id: result.id,
  externalId: result.externalId,
  referenceId: result.referenceId,
  transactionIds: result.coreBankingTransactionIds || [],
  transactionDate: result.createdAt
});

export const mapTransactionResultToExternalResponse = (
  result: ITransactionModel,
  includeCoreBankingTransactions: boolean
): IExternalTransactionResponse => {
  const responseWithoutCoreBankingIds = {
    id: result.id,
    externalId: result.externalId,
    availableBalance: result.availableBalance
  };

  if (includeCoreBankingTransactions) {
    return {
      ...responseWithoutCoreBankingIds,
      coreBankingTransactions: result.coreBankingTransactions || []
    };
  }
  return responseWithoutCoreBankingIds;
};

const notExistOrFalsy = (value?: boolean): boolean => {
  return value !== true;
};

export const mapConfirmTransactionResultToExternalResponse = (
  result: ConfirmTransactionResponse,
  query: ConfirmTransactionQueryInput
): ConfirmTransactionResponse => {
  if (notExistOrFalsy(query.includeTransactionId)) {
    delete result.id;
  }
  if (notExistOrFalsy(query.includeExternalId)) {
    delete result.externalId;
  }
  if (notExistOrFalsy(query.includeCoreBankingTransactions)) {
    delete result.coreBankingTransactions;
  }
  return result;
};

export const formatBeneficiaryAccountNoForWallet = (
  walletBankCode: IBankCode,
  identityNumber: string
): string => {
  if (walletBankCode.prefix) {
    return `${walletBankCode.prefix}${identityNumber}`;
  }
  return identityNumber;
};

export const formatTransactionDate = (date: Date): string => {
  return moment(date)
    .utc()
    .format(TRANSACTION_DATE_FORMAT_STRING);
};

export const formatString2Date = (dateString: string, format: string) => {
  return moment(dateString, format).toDate();
};

export const formatDate2String = (date: Date, format: string): string => {
  return moment(date)
    .utc()
    .format(format);
};

export const formatToUtcTime = (
  dateString: string,
  currentFormat: string,
  expectedFormat: string
): string => {
  const date = formatString2Date(dateString, currentFormat);
  return formatDate2String(date, expectedFormat);
};

export const createIncomingTransactionExternalId = (
  externalId: string,
  transactionDate: string,
  accountNumber: string = ''
) => {
  // externalId=${externalId}_${YYMMDDhhmmss}_${accountNo}
  const generateTransactionDate = formatToUtcTime(
    transactionDate,
    TRANSACTION_DATE_FORMAT_STRING,
    TRANSACTION_DATE_FORMAT_STRING
  );
  const accountNumberStr = accountNumber ? `_${accountNumber}` : '';
  const extId = `${externalId}_${generateTransactionDate}` + accountNumberStr;
  logger.info(`createIncomingTransactionExternalId : External Id : ${extId}`);
  return extId;
};

export const mapDataJournalEntriesGL = (
  fee: IFeeModel,
  transactionID: string
): IJournalEntriesGL => {
  logger.info(`mapDataJournalEntriesGL: TransactionId ${transactionID} journalEntries debitGlNumber ${fee.debitGlNumber} 
  and creditGlNumber ${fee.creditGlNumber}`);
  return {
    date: moment().format('YYYY-MM-DD'),
    debitAccount1: fee.debitGlNumber || '',
    debitAmount1: fee.subsidiaryAmount || 0,
    creditAccount1: fee.creditGlNumber || '',
    creditAmount1: fee.subsidiaryAmount || 0,
    transactionID
  };
};

export const submitCoreBankingTransactionWithRetry = async <
  T extends (...args: any[]) => any
>(
  fn: T,
  ...args: Parameters<T>
): Promise<ReturnType<T>> => {
  const idempotencyKey = Http.generateIdempotencyKey();
  args.push(idempotencyKey);
  return await Util.retry(IdempotentRetryConfig(), fn, ...args);
};

const isFeeApplicable = (
  creditTc?: ITransactionCodeInfo,
  debitTc?: ITransactionCodeInfo
) => {
  // Identify fee from credit transaction code
  let creditTcHasReversalSuffix = false;
  if (!isUndefined(creditTc) && !isEmpty(creditTc.code)) {
    creditTcHasReversalSuffix = creditTc.code.endsWith(REVERSAL_TC_SUFFIX);
  }
  const creditTcHasFee =
    creditTc?.feeRules !== FeeRuleType.ZERO_FEE_RULE &&
    !creditTcHasReversalSuffix;

  // Identify fee from debit transaction code
  let debitTcHasReversalSuffix = false;
  if (!isUndefined(debitTc) && !isEmpty(debitTc.code)) {
    debitTcHasReversalSuffix = debitTc.code.endsWith(REVERSAL_TC_SUFFIX);
  }
  const debitTcHasFee =
    debitTc?.feeRules !== FeeRuleType.ZERO_FEE_RULE &&
    !debitTcHasReversalSuffix;

  let isZeroFeeRuleApplicable = false;
  if (!isUndefined(creditTc) && !isEmpty(creditTc.feeRules)) {
    isZeroFeeRuleApplicable = creditTc.feeRules === FeeRuleType.ZERO_FEE_RULE;
  }
  if (!isUndefined(debitTc) && !isEmpty(debitTc.feeRules)) {
    isZeroFeeRuleApplicable = debitTc.feeRules === FeeRuleType.ZERO_FEE_RULE;
  }

  return (creditTcHasFee || debitTcHasFee) && !isZeroFeeRuleApplicable;
};

export const extractTransactionInfoByInterchange = (
  transactionModel: NewEntity<ITransactionModel>,
  interchange?: string,
  mustMatch?: boolean
): ITransactionInfoByInterchange => {
  let debitTc: ITransactionCodeInfo | undefined;
  if (
    transactionModel.debitTransactionCodeMapping &&
    transactionModel.debitTransactionCodeMapping.length
  ) {
    if (!interchange && !mustMatch) {
      debitTc = transactionModel.debitTransactionCodeMapping.find(
        tc => tc.interchange === transactionModel.debitPriority
      );

      // when create transaction without interchange, just get the first value
      if (isEmpty(debitTc))
        debitTc = transactionModel.debitTransactionCodeMapping[0];
    } else {
      debitTc = transactionModel.debitTransactionCodeMapping.find(
        tc => tc.interchange === interchange
      );
      debitTc =
        debitTc ||
        transactionModel.debitTransactionCodeMapping.find(
          tc => !tc.interchange // fall back to default if interchange not match
        );
      if (!debitTc) {
        // debit configuration is not empty but none matches interchange
        logger.error(
          'extractTransactionInfoByInterchange: debit configuration for interchange not found'
        );
        throw new TransferAppError(
          'Debit configuration for interchange not found when extracting transaction info by interchange!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.DEBIT_INTERCHANGE_NOT_FOUND
        );
      }
    }
  }

  let creditTc: ITransactionCodeInfo | undefined;
  if (
    transactionModel.creditTransactionCodeMapping &&
    transactionModel.creditTransactionCodeMapping.length
  ) {
    if (!interchange && !mustMatch) {
      creditTc = transactionModel.creditTransactionCodeMapping.find(
        tc => tc.interchange === transactionModel.creditPriority
      );

      // when create transaction without interchange, just get the first value
      if (isEmpty(creditTc))
        creditTc = transactionModel.creditTransactionCodeMapping[0];
    } else {
      creditTc = transactionModel.creditTransactionCodeMapping.find(
        tc => tc.interchange === interchange
      );
      creditTc =
        creditTc ||
        transactionModel.creditTransactionCodeMapping.find(
          tc => !tc.interchange // fall back to default if interchange not match
        );
      if (!creditTc) {
        // credit configuration is not empty but none matches interchange
        logger.error(
          'extractTransactionInfoByInterchange: credit configuration for interchange not found'
        );
        throw new TransferAppError(
          'Credit configuration for interchange not found when extracting transaction info by interchange!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.CREDIT_INTERCHANGE_NOT_FOUND
        );
      }
    }
  }
  let feeCodeInfo: IFeeAmount | undefined;
  if (
    isFeeApplicable(creditTc, debitTc) &&
    transactionModel.feeMapping &&
    transactionModel.feeMapping.length
  ) {
    if (!interchange && !mustMatch) {
      feeCodeInfo = transactionModel.feeMapping.find(
        feeMapping => feeMapping.interchange === transactionModel.debitPriority
      );

      // when create transaction without interchange, just get the first value
      if (isEmpty(feeCodeInfo)) feeCodeInfo = transactionModel.feeMapping[0];
    } else {
      feeCodeInfo = transactionModel.feeMapping.find(
        fc => fc.interchange === interchange
      );
      feeCodeInfo =
        feeCodeInfo ||
        transactionModel.feeMapping.find(
          fc => !fc.interchange // fall back to default if interchange not match
        );

      if (!feeCodeInfo) {
        // fee configuration is not empty but none matches interchange
        logger.error(
          'extractTransactionInfoByInterchange: fee configuration for interchange not found'
        );
        throw new TransferAppError(
          'Fee configuration for interchange not found when extracting transaction info by interchange!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.FEE_INTERCHANGE_NOT_FOUND
        );
      }
    }
  }

  return {
    debitTransactionCode: debitTc?.code,
    debitTransactionChannel: debitTc?.channel,
    debitLimitGroupCode: debitTc?.limitGroupCode,
    debitAwardGroupCounter: debitTc?.awardGroupCounter,
    creditTransactionCode: creditTc?.code,
    creditTransactionChannel: creditTc?.channel,
    fees: feeCodeInfo && [feeCodeInfo],
    categoryCode:
      transactionModel.userSelectedCategoryCode || debitTc?.defaultCategoryCode
  };
};

export const validateManualAdjustmentInterchangeAndChannel = (
  bankCodeChannel: BankChannelEnum | undefined,
  interchange?: BankNetworkEnum
): void => {
  if (
    !includes(
      [
        BankChannelEnum.EXTERNAL,
        BankChannelEnum.GOBILLS,
        BankChannelEnum.IRIS,
        BankChannelEnum.WINCOR,
        BankChannelEnum.TOKOPEDIA,
        BankChannelEnum.QRIS
      ],
      bankCodeChannel
    )
  ) {
    logger.error(
      `validateManualAdjustmentInterchangeAndChannel: invalid transaction bank channel ${bankCodeChannel} for manual adjustment`
    );
    throw new TransferAppError(
      `Manual adjustment validation failed. Invalid transaction bank channel ${bankCodeChannel} for manual adjustment!`,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_BANK_CHANNEL
    );
  } else if (
    includes(
      [
        BankChannelEnum.GOBILLS,
        BankChannelEnum.IRIS,
        BankChannelEnum.TOKOPEDIA,
        BankChannelEnum.QRIS
      ],
      bankCodeChannel
    ) &&
    interchange
  ) {
    logger.error(
      `validateManualAdjustmentInterchangeAndChannel: interchange not expected ${bankCodeChannel}  and ${interchange} for manual adjustment`
    );
    throw new TransferAppError(
      `Manual adjustment validation failed. Interchange not expected ${bankCodeChannel} and ${interchange} for manual adjustment!`,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INTERCHANGE_NOT_EXPECTED
    );
  }
};

export const validateTransactionForManualAdjustment = (
  transaction: ITransactionModel,
  reqStatus: TransactionStatus,
  interchange: BankNetworkEnum
) => {
  if (reqStatus === transaction.status) {
    logger.error(
      `validateTransactionForManualAdjustment: Invalid transaction status to manually adjusted for txnId: ${transaction.id}`
    );
    throw new TransferAppError(
      `Manual adjustment validation failed. Invalid transaction status to manually adjusted for txnId: ${transaction.id}!`,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_TRANSACTION_STATUS
    );
  }

  if (!isDateWithinYear(transaction.updatedAt)) {
    logger.error(
      `validateTransactionForManualAdjustment: Transaction ${transaction.id} with external Id ${transaction.externalId} is not eligible for Manual adjustment as transaction date is not withing limits.`
    );
    throw new TransferAppError(
      `Manual adjustment validation failed. Transaction ${transaction.id} with external Id ${transaction.externalId} is not eligible for Manual adjustment as transaction date is not withing limits!`,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.TRANSACTION_DATE_NOT_ALLOWED
    );
  }
  validateManualAdjustmentInterchangeAndChannel(
    transaction.beneficiaryBankCodeChannel,
    interchange
  );
};

export const updateExtraFields = (
  payload:
    | ITransferTransactionInput
    | ITopupInternalTransactionInput
    | ITransactionModel
): ITransactionExtra => ({
  additionalInformation1: payload.additionalInformation1,
  additionalInformation2: payload.additionalInformation2,
  additionalInformation3: payload.additionalInformation3,
  additionalInformation4: payload.additionalInformation4,
  sourceCIF: payload.sourceCIF
});

export const submitTransactionWithRetry = async <
  T extends (...args: any[]) => any
>(
  fn: T,
  ...args: Parameters<T>
): Promise<ReturnType<T>> => {
  return await Util.retry(RetryConfig(), fn, ...args);
};

export const submitIdempotentTransactionWithRetry = async <
  T extends (...args: any[]) => any
>(
  fn: T,
  ...args: Parameters<T>
): Promise<ReturnType<T>> => {
  return await Util.retry(IdempotentRetryConfig(), fn, ...args);
};

/**
 * Persist the latest transaction data and load the latest transaction data to/from DB before/after calling thirdPartySubmissionFunction,
 * to prevent data loss in case of race condition caused by out-of-order confirmation from third party.
 *
 * Retry the submission, on Http connection error.
 *
 * @param thirdPartySubmissionFunction third party submission function
 * @param thirdPartySubmissionPayload payload to submit to third party
 * @param transactionModel transaction data
 * @returns updated transaction data
 */
export const submitThirdPartyTransactionWithRetryAndUpdate = async (
  thirdPartySubmissionFunction: (thirdPartyPayload: any) => any,
  thirdPartySubmissionPayload: any,
  transactionModel: ITransactionModel
): Promise<any> => {
  await transactionRepository.update(transactionModel.id, {
    ...transactionModel,
    status: TransactionStatus.SUBMITTING,
    journey: transactionHelper.getStatusesToBeUpdated(
      TransferJourneyStatusEnum.SUBMITTING_THIRD_PARTY,
      transactionModel
    )
  });
  const thirdPartySubmissionResponse = await Util.retry(
    RetryConfig(),
    thirdPartySubmissionFunction,
    thirdPartySubmissionPayload
  );
  transactionModel =
    (await transactionRepository.findOneByQuery({
      _id: transactionModel.id
    })) || transactionModel;
  return {
    submissionResponse: thirdPartySubmissionResponse,
    updatedTransaction: transactionModel
  };
};

export const checkTransferSameCifForMigratingAccounts = (
  sourceAccount: IAccount,
  beneficiaryAccount: IAccount
): boolean => {
  return !(
    (sourceAccount.status == AccountStatusEnum.MIGRATING ||
      beneficiaryAccount.status == AccountStatusEnum.MIGRATING) &&
    sourceAccount.cif !== beneficiaryAccount.cif
  );
};

/**
 * whether bank code is external bank
 *
 * @param bankCode the bank code
 */
export const isExternalBank = (bankCode: IBankCode): boolean => {
  return bankCode.channel === BankChannelEnum.EXTERNAL;
};

/**
 * whether bank code is BIFast bank
 *
 * @param bankCodeId the bank code id
 */
export const isBIFastBank = (bankCodeId: string): boolean => {
  return bankCodeId === BIFAST_PROXY_BANK_CODE;
};

export const mapToDisbursementTransaction = (
  transactionModel: ITransactionModel,
  transactionChannel: string
): IDisbursementPayload => {
  /**
   * If defined always in UTC timezone
   */
  const loanTransactionDate =
    transactionModel.additionalPayload?.loanTransactionDate;

  const currentDate = moment().utcOffset(UTC_OFFSET_WESTERN_INDONESIAN);

  const disbursementPayload: IDisbursementPayload = {
    transactionDetails: {
      transactionChannelId: transactionChannel
    },
    externalId: get(transactionModel, 'additionalPayload.coreBankingExternalId')
  };

  if (loanTransactionDate) {
    const disbursementDate = moment
      .utc(loanTransactionDate as string)
      .utcOffset(UTC_OFFSET_WESTERN_INDONESIAN);

    if (moment(disbursementDate).isBefore(currentDate, 'day')) {
      disbursementPayload.valueDate = moment(disbursementDate)
        .utcOffset(UTC_OFFSET_WESTERN_INDONESIAN)
        .format(coreBankingConstant.VALUE_DATE_MAMBU_FORMAT);
      disbursementPayload.bookingDate = currentDate.format(
        coreBankingConstant.VALUE_DATE_MAMBU_FORMAT
      );
    }
  }

  if (transactionModel.additionalPayload?.shouldDisburseWithAmount) {
    disbursementPayload.amount = transactionModel.transactionAmount;
  }

  return disbursementPayload;
};

export const rounding = (amount: number) => {
  return +(Math.round(parseFloat(amount + `e+2`)) + 'e-2');
};

export const mapToRepaymentTransaction = (
  transactionModel: ITransactionModel,
  transactionChannel: string,
  bankIncomeAmount: number
): IRepaymentPayload => {
  const repaymentDate = transactionModel.additionalPayload?.loanTransactionDate;
  const allowance = get(
    transactionModel.additionalPayload,
    'allowance',
    0
  ) as number;

  const repaymentAmountForPayOffInterestSettlement = get(
    transactionModel.additionalPayload,
    'repaymentAmountForPayOffInterestSettlement'
  ) as number;

  const repaymentAmount =
    repaymentAmountForPayOffInterestSettlement ||
    transactionModel.transactionAmount - bankIncomeAmount - allowance;

  const repaymentPayload: IRepaymentPayload = {
    externalId: get(
      transactionModel,
      'additionalPayload.coreBankingExternalId'
    ),
    amount: rounding(repaymentAmount),
    transactionDetails: {
      transactionChannelId: transactionChannel
    }
  };

  if (repaymentDate) {
    repaymentPayload.valueDate = moment(repaymentDate as string)
      .utcOffset(UTC_OFFSET_WESTERN_INDONESIAN)
      .format(coreBankingConstant.VALUE_DATE_MAMBU_FORMAT);
  }
  if (transactionModel.additionalPayload?.loanRepaymentAllocation) {
    repaymentPayload.customPaymentAmounts = (transactionModel.additionalPayload
      .loanRepaymentAllocation as Record<string, unknown>[])
      .filter(
        repaymentAllocation =>
          (repaymentAllocation.type as string) in AmountTypeEnum
      )
      .map(amountDetail => {
        return {
          amount: amountDetail.amount as number,
          customPaymentAmountType: amountDetail.type as string
        };
      });
  }

  if (repaymentPayload.externalId) {
    repaymentPayload.externalId = `${repaymentPayload.externalId}-rp`;
  }

  return repaymentPayload;
};

export const mapToPayOffTransaction = (
  transactionModel: ITransactionModel,
  transactionChannel: string
): IPayOffPayload => {
  const payOffPayload: IPayOffPayload = {
    externalId: get(
      transactionModel,
      'additionalPayload.coreBankingExternalId'
    ),
    transactionDetails: {
      transactionChannelId: transactionChannel
    },
    notes: transactionModel.note
  };

  const allocations = transactionModel.additionalPayload
    ?.loanRepaymentAllocation as Record<string, unknown>[];

  if (allocations) {
    const interestPaid = (allocations
      .filter(allocation => allocation.type === AmountTypeEnum.INTEREST)
      .map(allocation => allocation.amount)[0] || 0) as number;

    const penaltyPaid = (allocations
      .filter(allocation => allocation.type === AmountTypeEnum.PENALTY)
      .map(allocation => allocation.amount)[0] || 0) as number;

    payOffPayload.payOffAdjustableAmounts = {
      interestPaid,
      penaltyPaid
    };
  }

  if (payOffPayload.externalId) {
    payOffPayload.externalId = `${payOffPayload.externalId}-po`;
  }

  return payOffPayload;
};

const composeGlAccountAmount = (
  glTransactionDetail: Record<string, unknown>[],
  type: GlTransactionTypes
): GLAccountAmount[] => {
  return glTransactionDetail.reduce((result: GLAccountAmount[], item) => {
    if (item.type === type) {
      result.push({
        glAccount: item.accountNumber as string,
        amount: item.amount as number
      });
    }
    return result;
  }, []);
};

export const mapToGlIncomeTransaction = (
  transactionModel: ITransactionModel,
  isReversal: boolean = false,
  transactionId?: string
): IGLJournalEntriesV2 => {
  const transactionDate = transactionModel.additionalPayload
    ?.loanTransactionDate
    ? transactionModel.additionalPayload.loanTransactionDate
    : new Date();

  const date = moment(transactionDate as string)
    .utcOffset(UTC_OFFSET_WESTERN_INDONESIAN)
    .format(coreBankingConstant.VALUE_DATE_MAMBU_FORMAT);

  const glIncomePayload: IGLJournalEntriesV2 = {
    date,
    branchId: get(transactionModel, 'additionalPayload.branchCode'),
    notes: transactionModel.note
  };

  if (transactionId) {
    glIncomePayload.transactionId = transactionId;
  }

  if (
    transactionModel.additionalPayload &&
    transactionModel.additionalPayload.glTransactionDetail
  ) {
    const glTransactionDetail = transactionModel.additionalPayload
      .glTransactionDetail as Record<string, unknown>[];

    const creditTransactions = composeGlAccountAmount(
      glTransactionDetail,
      GlTransactionTypes.CREDIT
    );

    const debitTransactions = composeGlAccountAmount(
      glTransactionDetail,
      GlTransactionTypes.DEBIT
    );

    glIncomePayload.credits = isReversal
      ? debitTransactions
      : creditTransactions;

    glIncomePayload.debits = isReversal
      ? creditTransactions
      : debitTransactions;
  }

  return glIncomePayload;
};

const validateRequiredField = (
  payload: Record<string, unknown>,
  field: string,
  errorList: IErrorDetail[]
) => {
  if (!payload[field]) {
    errorList.push({
      code: ERROR_CODE.INVALID_ADDITIONAL_PAYLOAD,
      key: field,
      message: 'is required'
    });
  }
};

const validateGlTransactionDetail = (
  payload: Record<string, unknown>,
  generalLedgers: IGeneralLedger[],
  errorList: IErrorDetail[]
) => {
  const glTransactionDetail = payload.glTransactionDetail as Record<
    string,
    unknown
  >[];

  glTransactionDetail.forEach(element => {
    validateRequiredField(element, 'glAccountCode', errorList);
    validateRequiredField(element, 'amount', errorList);
    validateRequiredField(element, 'type', errorList);

    enforceStringFormat(element, 'glAccountCode');
    enforceStringFormat(element, 'type');
    enforceNumberFormat(element, 'amount');

    if (
      !Object.values(GlTransactionTypes).includes(
        element.type as GlTransactionTypes
      )
    ) {
      errorList.push({
        code: ERROR_CODE.INCORRECT_FIELD,
        key: 'type',
        message: 'invalid value'
      });
    }

    const generalLedger = generalLedgers.find(glItem => {
      return element.glAccountCode === glItem.code;
    });

    if (!generalLedger) {
      errorList.push({
        code: ERROR_CODE.INCORRECT_FIELD,
        key: 'glAccountCode',
        message: 'invalid value'
      });

      return;
    }

    element.accountNumber = generalLedger.accountNumber;
  });
};

export const loanAdditionalPayloadValidator = (
  transactionModel: ITransactionModel,
  generalLedgers: IGeneralLedger[] = []
) => {
  const payload = transactionModel.additionalPayload;
  const errorList: IErrorDetail[] = [];

  if (payload) {
    enforceBooleanFormat(payload, 'shouldDisburseWithAmount');

    if (payload.glTransactionDetail)
      validateGlTransactionDetail(payload, generalLedgers, errorList);

    if (payload.loanRepaymentAllocation) {
      (payload.loanRepaymentAllocation as Record<string, unknown>[]).forEach(
        element => {
          validateRequiredField(element, 'amount', errorList);
          validateRequiredField(element, 'type', errorList);

          enforceNumberFormat(element, 'amount');
          enforceStringFormat(element, 'type');
        }
      );
    }

    if (payload.repaymentAmountForPayOffInterestSettlement)
      enforceNumberFormat(
        payload,
        'repaymentAmountForPayOffInterestSettlement'
      );

    if (!isEmpty(payload.allowance)) {
      enforceNumberFormat(payload, 'allowance');
    }
  }

  if (errorList.length > 0) {
    logger.error(`Invalid additional payload: ${JSON.stringify(errorList)}`);
    throw new TransferAppError(
      `Invalid additional payload!`,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_ADDITIONAL_PAYLOAD,
      errorList
    );
  }
};

export const isGlTransactionDetailFieldExists = (
  transactionModel: ITransactionModel
): boolean => {
  return !!(
    transactionModel.additionalPayload &&
    transactionModel.additionalPayload.glTransactionDetail
  );
};

export const qrisAdditionalPayloadValidator = (payload: any) => {
  const errorList: IErrorDetail[] = [];

  if (isEmpty(payload)) {
    throw new TransferAppError(
      `Additional payload is empty!`,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.ADDITIONAL_PAYLOAD_NOT_EXIST
    );
  }

  // TODO: extend ITransactionModel to make this many validation as a mandatory
  validateRequiredField(payload, 'currencyCode', errorList);
  validateRequiredField(payload, 'amount', errorList);
  validateRequiredField(payload, 'nationalMid', errorList);
  validateRequiredField(payload.merchant, 'pan', errorList);
  validateRequiredField(payload.merchant, 'id', errorList);
  validateRequiredField(payload.merchant, 'criteria', errorList);
  validateRequiredField(payload.merchant, 'name', errorList);
  validateRequiredField(payload.merchant, 'city', errorList);
  validateRequiredField(payload.merchant, 'mcc', errorList);
  validateRequiredField(payload.merchant, 'postalCode', errorList);
  validateRequiredField(payload.merchant, 'countryCode', errorList);
  validateRequiredField(payload.customer, 'pan', errorList);
  validateRequiredField(payload.customer, 'name', errorList);

  enforceStringFormat(payload, 'currencyCode');

  enforceNumberFormat(payload, 'amount');
  enforceNumberFormat(payload, 'fee');

  enforceStringFormat(payload, 'nationalMid');
  enforceStringFormat(payload, 'terminalLabel');
  enforceStringFormat(payload.merchant, 'merchant.pan');
  enforceStringFormat(payload.merchant, 'merchant.id');
  enforceStringFormat(payload.merchant, 'merchant.criteria');
  enforceStringFormat(payload.merchant, 'merchant.name');
  enforceStringFormat(payload.merchant, 'merchant.city');
  enforceStringFormat(payload.merchant, 'merchant.mcc');
  enforceStringFormat(payload.merchant, 'merchant.postalCode');
  enforceStringFormat(payload.merchant, 'merchant.countryCode');
  enforceStringFormat(payload.customer, 'customer.pan');
  enforceStringFormat(payload.customer, 'customer.name');

  if (errorList.length > 0) {
    logger.error(
      `qrisAdditionalPayloadValidator: ${JSON.stringify(errorList)}`
    );
    throw new TransferAppError(
      `Qris additional payload validation failed!`,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_ADDITIONAL_PAYLOAD,
      errorList
    );
  }
};

export const mapToQrisTransaction = (
  transactionModel: ITransactionModel,
  additionalPayload: any
): IAltoCreatePaymentRequestData => {
  return {
    date_time: moment().format('YYYY-MM-DD hh:mm:ss.SSS[Z]'),
    authorization_id: altoUtils.generateAuthorizationId(),
    customer_reference_number: transactionModel.externalId,
    currency_code: additionalPayload.currencyCode || DEFAULT_ORIGINAL_CURRENCY,
    amount: additionalPayload.amount,
    fee: additionalPayload.fee,
    issuer_nns: ISSUER_NNS,
    national_mid: additionalPayload.nationalMid,
    additional_data: additionalPayload.additionalData,
    terminal_label: additionalPayload.terminalLabel,
    merchant: {
      pan: transactionModel.beneficiaryAccountNo || '',
      id: additionalPayload.merchant.id,
      criteria: additionalPayload.merchant.criteria,
      name: additionalPayload.merchant.name,
      city: additionalPayload.merchant.city,
      mcc: additionalPayload.merchant.mcc,
      postal_code: additionalPayload.merchant.postalCode,
      country_code: additionalPayload.merchant.countryCode
    },
    customer: {
      pan: altoUtils.generateCustomerPan(additionalPayload.customer.pan),
      name: additionalPayload.customer.name,
      account_type: AltoAccountTypeEnum.SAVING
    }
  };
};

export const mapToQrisTransferDb = async (
  transactionModel: ITransactionModel
): Promise<Partial<ITransactionModel>> => {
  const additionalPayload: any = transactionModel.additionalPayload;
  const nnsPrefix = additionalPayload.merchant.pan.substring(0, 8);
  const nnsData:
    | INNSMapping
    | undefined = await configurationRepository.getNNSMapping(nnsPrefix);

  if (nnsData === undefined) {
    throw new TransferAppError(
      `Qris NNS mapping data is undefined!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.QRIS_NNS_MAPPING_NOT_FOUND
    );
  }

  const beneficiaryAccountNo = altoUtils.generateBeneficiaryAccountNumberWithLundCheckDigit(
    additionalPayload.merchant.pan
  );

  const customerPan = altoUtils.generateCustomerPan(
    additionalPayload.customer.pan
  );

  return {
    beneficiaryAccountName: additionalPayload.merchant.name,
    beneficiaryAccountNo: beneficiaryAccountNo,
    beneficiaryAccountType: additionalPayload.merchant.mcc,
    beneficiaryBankName: nnsPrefix,
    thirdPartyOutgoingId: transactionModel.externalId,
    additionalInformation1: nnsData.brandName,
    additionalInformation2: customerPan,
    additionalInformation3: additionalPayload.merchant.city
  };
};

const isBIFastTransaction = (transaction: ITransactionModel): boolean =>
  transaction.paymentServiceCode?.indexOf(BankChannelEnum.BIFAST) !== -1;

export const getRefundServiceType = (
  originalTransaction: ITransactionModel
): PaymentServiceTypeEnum => {
  return altoUtils.isQrisTransaction(originalTransaction)
    ? PaymentServiceTypeEnum.VOID_QRIS
    : isBIFastTransaction(originalTransaction)
    ? PaymentServiceTypeEnum.VOID_BIFAST
    : PaymentServiceTypeEnum.GENERAL_REFUND;
};

export const mapTransactionModelToRefundTransaction = (
  transactions: ITransactionModel[]
): IRefundTransactionBasicDetail[] =>
  transactions.map(transaction => {
    return ({
      id: transaction.id,
      status: transaction.status,
      amount: transaction.transactionAmount,
      createdAt: transaction.createdAt,
      updatedAt: transaction.updatedAt
    } as unknown) as IRefundTransactionBasicDetail;
  });

const readOnlyModeStorage = new AsyncLocalStorage();

export const withReadOnlyMode = <TYPE>(f: () => TYPE) => {
  return readOnlyModeStorage.run(true, f);
};

export const isReadOnlyMode = () => {
  return (readOnlyModeStorage.getStore() as boolean) ?? false;
};

const transactionUtility = {
  submitTransactionWithRetry,
  submitThirdPartyTransactionWithRetryAndUpdate
};

export default wrapLogs(transactionUtility);
