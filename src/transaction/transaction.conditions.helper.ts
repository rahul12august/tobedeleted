import { toIndonesianRupiah } from '../common/currencyFormatter';
import { ERROR_CODE } from '../common/errors';
import { ILimitGroup } from '../configuration/configuration.type';
import { RuleConditionState } from './transaction.conditions.enum';
import { IDR_CURRENCY_PREFIX } from './transaction.constant';
import { RuleConditionStatus } from './transaction.type';

const compareAmountWithDailyLimit = (
  limitGroup: ILimitGroup,
  transactionAmount: number,
  dailyAmountSumSoFar: number
): RuleConditionState => {
  if (transactionAmount + dailyAmountSumSoFar <= limitGroup.dailyLimitAmount) {
    return RuleConditionState.ACCEPTED;
  }
  if (transactionAmount <= limitGroup.dailyLimitAmount) {
    return RuleConditionState.TRY_TOMORROW;
  }
  return RuleConditionState.DECLINED;
};

const constructDailyLimitEvaluatedRuleConditionStatus = (
  currentAmount: number,
  dailyAmountSumSoFar: number,
  tag: string,
  dailyLimit: number,
  errorCodeDeclined: ERROR_CODE,
  errorCodeTryTomorrow: ERROR_CODE,
  currentEvalState: RuleConditionState
): RuleConditionStatus => {
  const totalDailyUsageSoFar = currentAmount + dailyAmountSumSoFar;
  const firstPart =
    'The sum of the total daily amount plus the current transaction is: ' +
    totalDailyUsageSoFar +
    ' which is ';
  const lastPart = ' the daily limit of ' + tag + ': ' + dailyLimit;

  if (currentEvalState === RuleConditionState.ACCEPTED) {
    return new RuleConditionStatus(firstPart + 'in ' + lastPart);
  }
  const failureMessage = firstPart + 'beyond ' + lastPart;
  if (currentEvalState === RuleConditionState.DECLINED) {
    return new RuleConditionStatus(
      failureMessage + '. The amount itself is greater than the daily limit.',
      errorCodeDeclined
    );
  }
  return new RuleConditionStatus(
    failureMessage + '. But you can try it tomorrow.',
    errorCodeTryTomorrow
  );
};

const constructDailyLimitEvaluatedRuleConditionStatusRefined = (
  currentAmount: number,
  dailyAmountSumSoFar: number,
  tag: string,
  dailyLimit: number,
  errorCodeDeclined: ERROR_CODE,
  errorCodeTryTomorrow: ERROR_CODE,
  currentEvalState: RuleConditionState
): RuleConditionStatus => {
  const currentAmountFormatted = `${IDR_CURRENCY_PREFIX} ${toIndonesianRupiah(
    currentAmount
  )}`;
  const dailyLimitFormatted = `${IDR_CURRENCY_PREFIX} ${toIndonesianRupiah(
    dailyLimit
  )}`;

  if (currentEvalState === RuleConditionState.ACCEPTED) {
    return new RuleConditionStatus(`${tag} SUCCESS`);
  } else if (currentEvalState === RuleConditionState.DECLINED) {
    return new RuleConditionStatus(
      `Current ${tag}'s transaction amount [${currentAmountFormatted}] exceeds ${tag}'s Daily Limit [${dailyLimitFormatted}]`,
      errorCodeDeclined
    );
  } else if (currentEvalState === RuleConditionState.TRY_TOMORROW) {
    const remainingAmountAvailableToWithdraw = dailyLimit - dailyAmountSumSoFar;

    const remainingAmountAvailableToWithdrawFormatted = `${IDR_CURRENCY_PREFIX} ${toIndonesianRupiah(
      remainingAmountAvailableToWithdraw
    )}`;
    return new RuleConditionStatus(
      `${tag}'s Daily Limit Exceeded. Attempted to perform transaction with amount [${currentAmountFormatted}], however only [${remainingAmountAvailableToWithdrawFormatted}] is available for ${tag}'s transaction`,
      errorCodeTryTomorrow
    );
  } else {
    return new RuleConditionStatus(`${tag} FAILED`);
  }
};

const transactionConditionHelperService = {
  compareAmountWithDailyLimit,
  constructDailyLimitEvaluatedRuleConditionStatus,
  constructDailyLimitEvaluatedRuleConditionStatusRefined
};

export default transactionConditionHelperService;
