import { ITransactionInput } from './transaction.type';
import { PaymentServiceTypeEnum } from '@dk/module-common';
import { TransferFailureReasonActor } from './transaction.enum';
import { ERROR_CODE } from '../common/errors';
import { TransferAppError } from '../errors/AppError';
import logger from '../logger';

const VISA_BANK_CODE = '360005';
/**
 * The OCT feature flag is true by default. To turn it off, specify OCT='false'.
 */
const getOCTToggle = (): boolean => {
  const oct = process.env.OCT;
  if (oct == 'false') {
    return false;
  }
  return true;
};

const throwOCTError = (transactionInput: ITransactionInput) => {
  const msg = `Original Credit Transaction is denied. externalId: ${transactionInput.externalId} . This is expected, for details see: DP3-514`;
  logger.info(msg);
  throw new TransferAppError(
    msg,
    TransferFailureReasonActor.SUBMITTER,
    ERROR_CODE.INVALID_REQUEST
  );
};

export const denyOCT = (transactionInput: ITransactionInput) => {
  const toggle = getOCTToggle();

  if (!toggle) {
    logger.info(`OCT toggle state: ${toggle} `);
    return;
  }

  logger.info(
    `OCT check: externalId: ${transactionInput.externalId} tranType: ${transactionInput.tranType}`
  );

  if (transactionInput.tranType === 'OC') {
    throwOCTError(transactionInput);
  }

  if (
    transactionInput.tranType === undefined &&
    transactionInput.paymentServiceType ===
      PaymentServiceTypeEnum.THIRD_PARTY_TRANSFER &&
    transactionInput.sourceAccountNo === '000000' &&
    (transactionInput.sourceBankCode === VISA_BANK_CODE ||
      transactionInput.sourceBankCode === 'BC995')
  ) {
    throwOCTError(transactionInput);
  }
  logger.info(
    `Not an OCT transaction, externalId: ${transactionInput.externalId}`
  );
};
