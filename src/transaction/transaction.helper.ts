import {
  AuthenticationType,
  BankChannelEnum,
  BankNetworkEnum,
  InstitutionTypeEnum,
  PaymentServiceTypeEnum,
  Rail
} from '@dk/module-common';
import _, { isEmpty } from 'lodash';
import moment from 'moment-timezone';
import { AccountRole } from '../account/account.enum';
import accountRepository from '../account/account.repository';
import accountService from '../account/account.service';
import awardService from '../award/award.service';
import { UsageCounter } from '../award/award.type';
import { NewEntity } from '../common/common.type';
import { mapTransferAppError } from '../common/common.util';
import { ERROR_CODE } from '../common/errors';
import { isFeatureEnabled } from '../common/featureFlag';
import configurationRepository from '../configuration/configuration.repository';
import transactionMapper from './transaction.mapper';
import {
  IBankCode,
  ILimitGroup,
  IPaymentConfigRule,
  IPaymentServiceMapping,
  ITransactionCodeInfo,
  ITransactionCodeMapping
} from '../configuration/configuration.type';
import { TransferAppError } from '../errors/AppError';
import feeService from '../fee/fee.service';
import { FeeAmountInput, FeeAmountQuery, IFeeAmount } from '../fee/fee.type';
import logger, { wrapLogs } from '../logger';
import transactionConstant, {
  ACCOUNT_NUMBER_NOT_FOUND,
  REVERSAL_TC_SUFFIX,
  RTOL_BIFAST_LIMIT_GROUPS,
  SWITCHING_ACCOUNT_NOT_FOUND
} from './transaction.constant';
import {
  ExecutionTypeEnum,
  JagoExternalBankCode,
  PhoneCode,
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from './transaction.enum';
import { ITransactionModel } from './transaction.model';
import transactionRepository from './transaction.repository';
import {
  IBlockingTransaction,
  ITransaction,
  ITransactionAccount,
  ITransactionInput,
  ITransferJourney,
  RecommendationService,
  TransactionListRequest
} from './transaction.type';
import {
  extractTransactionInfoByInterchange,
  generateUniqueId
} from './transaction.util';
import { BeneficiaryProxyType } from '../bifast/bifast.enum';
import { CustomerEntitlementManager } from '../customerEntitlementManager/customerEntitlement.manager';
import { CustomerEntitlementManagerFactory } from '../customerEntitlementManager/customerEntitlement.manager.factory';
import { FEATURE_FLAG } from '../common/constant';
import feeHelper from '../fee/fee.helper';

const { AMOUNT_IS_LIMITED } = transactionConstant;

const getStatusesToBeUpdated = (
  status: TransferJourneyStatusEnum,
  transactionInput: NewEntity<ITransaction>
): ITransferJourney[] => {
  const statusToUpdate: ITransferJourney = {
    status,
    updatedAt: new Date()
  };
  return !transactionInput.journey || isEmpty(transactionInput.journey)
    ? [statusToUpdate]
    : [...transactionInput.journey, statusToUpdate];
};

const mapAuthenticationRequired = async (
  recommendationServices: RecommendationService[],
  transactionAmount: number,
  sourceAccountNo?: string
): Promise<RecommendationService[]> => {
  if (!sourceAccountNo) {
    return recommendationServices;
  }
  return await Promise.all(
    recommendationServices.map(async item => {
      let authenticationType = AuthenticationType.NONE;
      if (item.transactionAuthenticationChecking) {
        const isAuthenticationAmount = await accountService.authenticateTransactionAmount(
          sourceAccountNo,
          transactionAmount
        );
        if (isAuthenticationAmount) {
          authenticationType = AuthenticationType.PASSWORD;
        } else {
          authenticationType = AuthenticationType.PIN;
        }
      }
      return { ...item, authenticationType };
    })
  );
};

const getUsageCounter = async (
  transactionCode: ITransactionCodeInfo,
  usageCounters: UsageCounter[]
): Promise<UsageCounter | undefined> => {
  const { limitGroupCode, awardGroupCounter } = transactionCode;
  const isAwardV2Enabled = isFeatureEnabled(FEATURE_FLAG.ENABLE_AWARD_V2);
  if (isAwardV2Enabled && awardGroupCounter) {
    return usageCounters.filter(c => c.limitGroupCode === awardGroupCounter)[0];
  }
  const monthlyTransaction = usageCounters.find(
    c => c.limitGroupCode === 'RTOL_BIFAST_FEE'
  );
  if (limitGroupCode) {
    return usageCounters.filter(c => {
      if (c.limitGroupCode === limitGroupCode) {
        if (RTOL_BIFAST_LIMIT_GROUPS.includes(c.limitGroupCode)) {
          c.monthlyAccumulationTransaction =
            monthlyTransaction &&
            monthlyTransaction.monthlyAccumulationTransaction
              ? monthlyTransaction.monthlyAccumulationTransaction
              : c.monthlyAccumulationTransaction;
        }
        return c;
      }
      return;
    })[0];
  }
  return;
};

const getThresholdCounter = async (
  customerId: string,
  transactionCode: ITransactionCodeInfo
): Promise<string | undefined> => {
  const { awardGroupCounter } = transactionCode;
  if (awardGroupCounter) {
    return await awardService.getThresholdCounter(
      customerId,
      awardGroupCounter
    );
  }
  return;
};

const getUsageAndThresholdCounters = async (
  input: ITransactionInput,
  transactionCode: ITransactionCodeMapping,
  usageCounters: UsageCounter[],
  isAwardV2Enabled: boolean
) => {
  let [usageCounter, thresholdCounter] =
    !input.refund && input.sourceCustomerId
      ? await Promise.all([
          getUsageCounter(transactionCode.transactionCodeInfo, usageCounters),
          !isAwardV2Enabled
            ? undefined
            : getThresholdCounter(
                input.sourceCustomerId,
                transactionCode.transactionCodeInfo
              )
        ])
      : [undefined, undefined];
  return { usageCounter, thresholdCounter };
};

const getFeeAmountInput = async (
  input: ITransactionInput,
  recommendationService: RecommendationService,
  usageCounters: UsageCounter[]
): Promise<FeeAmountInput[]> => {
  const transactionCodes =
    input.refund ||
    input.paymentServiceType === PaymentServiceTypeEnum.OFFER ||
    input.paymentServiceType === PaymentServiceTypeEnum.PAYROLL ||
    input.paymentServiceType === PaymentServiceTypeEnum.CASHBACK ||
    input.paymentServiceType === PaymentServiceTypeEnum.BONUS_INTEREST
      ? recommendationService.creditTransactionCode
      : recommendationService.debitTransactionCode;
  const custEntitlementMgr: CustomerEntitlementManager = await CustomerEntitlementManagerFactory.getCustomerEntitlementManagerByFlag();
  //checking based on flag
  const resultEntitlement = await custEntitlementMgr.preProcessCustomerEntitlementBasedOnTransactionCodes(
    {
      transactionCodes: transactionCodes,
      customerId: input.sourceCustomerId,
      transactionId: input.transactionId,
      paymentServiceType: input.paymentServiceType
    }
  );

  const feeAmountInputs: FeeAmountInput[] = [];

  if (recommendationService.inquiryFeeRule) {
    const feeAmountInput: FeeAmountInput = {
      feeRuleCode: recommendationService.inquiryFeeRule,
      transactionAmount: input.transactionAmount,
      feeAmount: input.feeAmount,
      monthlyNoTransaction: undefined,
      interchange: undefined,
      targetBankCode: input.beneficiaryBankCode,
      externalId: input.externalId,
      beneficiaryBankCodeChannel: input.beneficiaryBankCodeChannel,
      refund: input.refund,
      customerId: input.sourceCustomerId
    };
    feeAmountInputs.push(feeAmountInput);
  }

  if (!transactionCodes) {
    return feeAmountInputs;
  }

  //generate missing usage counter [processed by entitlement]
  usageCounters = await custEntitlementMgr.generateMissingUsageCounterBasedOnTransactionCodes(
    {
      transactionCodes: transactionCodes,
      usageCounters,
      entitlementResultResponse: resultEntitlement
    }
  );

  for (const transactionCode of transactionCodes) {
    const isAwardV2Enabled = isFeatureEnabled(FEATURE_FLAG.ENABLE_AWARD_V2);
    const feeRuleCode = transactionCode.transactionCodeInfo.feeRules;
    if (!feeRuleCode) {
      continue;
    }
    let { usageCounter, thresholdCounter } = await getUsageAndThresholdCounters(
      input,
      transactionCode,
      usageCounters,
      isAwardV2Enabled
    );

    // based on the entitlement flag, custEntitlementMgr will map it accordingly
    // the fallback logic will use usageCounter as is
    usageCounters = custEntitlementMgr.mapEntitlementUsageCounterByLimitGroupCode(
      usageCounters,
      resultEntitlement
    );

    const feeAmountInput: FeeAmountInput = {
      transactionAmount: input.transactionAmount,
      feeAmount: input.feeAmount,
      refund: input.refund,
      thresholdCounter: thresholdCounter,
      targetBankCode: input.beneficiaryBankCode,
      externalId: input.externalId,
      feeRuleCode: feeRuleCode,
      monthlyNoTransaction:
        usageCounter && usageCounter.monthlyAccumulationTransaction,
      interchange: transactionCode.interchange,
      beneficiaryBankCodeChannel: input.beneficiaryBankCodeChannel,
      customerId: input.sourceCustomerId,
      awardGroupCounter: transactionCode.transactionCodeInfo.awardGroupCounter,
      limitGroupCode: transactionCode.transactionCodeInfo.limitGroupCode,
      usageCounters: usageCounters
    };
    feeAmountInputs.push(feeAmountInput);
  }

  return feeAmountInputs;
};

const getFeeData = async (
  input: ITransactionInput,
  recommendationService: RecommendationService,
  usageCounters: UsageCounter[]
): Promise<IFeeAmount[]> => {
  const feeAmountInput = await getFeeAmountInput(
    input,
    recommendationService,
    usageCounters
  );
  const feeDataQueryResult = await Promise.all(
    feeAmountInput.map(input => feeService.getFeeAmount(input))
  ); // IFeeAmount[][]
  return _.flatten(feeDataQueryResult);
};

const mapFeeData = async (
  recommendationServices: RecommendationService[],
  input: ITransactionInput,
  usageCounters: UsageCounter[]
): Promise<RecommendationService[]> => {
  if (
    !input.sourceCustomerId &&
    !input.refund &&
    input.paymentServiceType !== PaymentServiceTypeEnum.OFFER &&
    input.paymentServiceType !== PaymentServiceTypeEnum.PAYROLL &&
    input.paymentServiceType !== PaymentServiceTypeEnum.CASHBACK &&
    input.paymentServiceType !== PaymentServiceTypeEnum.BONUS_INTEREST
  ) {
    return recommendationServices;
  }

  return await Promise.all(
    recommendationServices.map(async recommendationService => {
      const feeData = await getFeeData(
        input,
        recommendationService,
        usageCounters
      );
      return { ...recommendationService, feeData };
    })
  );
};

const getBankCode = async (bankCode: string): Promise<IBankCode> => {
  const bankCodeInfo = await configurationRepository.getBankCodeMapping(
    bankCode
  );
  if (!bankCodeInfo) {
    const detail = `Invalid bank code ${bankCode}!`;
    logger.error(`getBankCode: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.INVALID_BANK_CODE
    );
  }
  return bankCodeInfo;
};

const getAccountInfo = async (
  accountNo?: string,
  cif?: string,
  bankCode?: IBankCode,
  accountName?: string,
  transactionAmount?: number,
  sourceAccountNo?: string,
  isInquiryApplicable: boolean = true
): Promise<ITransactionAccount> => {
  let accountInfo: ITransactionAccount = {
    accountName: accountName,
    bankCode: bankCode ? bankCode.bankCodeId : undefined,
    accountNumber: accountNo,
    role: AccountRole.NONE
  };

  if (bankCode && isInquiryApplicable) {
    if (accountNo) {
      const encodedAccountNo = encodeURIComponent(accountNo);
      let inquiryInfo;
      logger.info('AccountInquiry: Making inquiry for account');
      if (bankCode.isInternal) {
        logger.info(
          `AccountInquiry: Making inquiry for an account (with mambu skipped), account number : ${accountNo} and BankCode :${bankCode.bankCodeId}`
        );
        inquiryInfo = await accountRepository.getAccountInfo(
          encodedAccountNo,
          cif
        );
      } else {
        let bankCodeId = bankCode.bankCodeId;
        logger.info(
          `AccountInquiry: Making inquiry for an account with account number : ${accountNo} and BankCode :${bankCodeId}`
        );
        inquiryInfo = await accountRepository.inquiryAccountInfo(
          bankCodeId,
          encodedAccountNo,
          transactionAmount,
          sourceAccountNo
            ? encodeURIComponent(sourceAccountNo)
            : sourceAccountNo,
          cif
        );
      }

      if (bankCode.isInternal && !('cif' in inquiryInfo)) {
        const detail = 'CIF is missing after account information is fetched!';
        logger.error(detail);
        throw new TransferAppError(
          detail,
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.INVALID_ACCOUNT
        );
      }

      accountInfo = {
        ...accountInfo,
        ...inquiryInfo
      };
    }
  }

  return accountInfo;
};

const getBeneficiaryAccount = async (
  accountNo?: string,
  cif?: string,
  bankCode?: IBankCode,
  accountName?: string,
  transactionAmount?: number,
  sourceAccountNo?: string,
  isInquiryApplicable: boolean = true
): Promise<ITransactionAccount> => {
  try {
    return await getAccountInfo(
      accountNo,
      cif,
      bankCode,
      accountName,
      transactionAmount,
      sourceAccountNo,
      isInquiryApplicable
    );
  } catch (err) {
    const appError = err && err.error && err.error.error;
    if (
      appError &&
      (appError.code === SWITCHING_ACCOUNT_NOT_FOUND ||
        appError.code === ACCOUNT_NUMBER_NOT_FOUND)
    ) {
      return mapTransferAppError(
        appError,
        ERROR_CODE.BENEFICIARY_ACCOUNT_INQUIRY_FAILED,
        err.status >= 500
      );
    }
    return mapTransferAppError(
      err,
      ERROR_CODE.BENEFICIARY_ACCOUNT_INQUIRY_FAILED,
      true
    );
  }
};

const getSourceAccount = async (
  accountNo?: string,
  cif?: string,
  bankCode?: IBankCode,
  accountName?: string,
  transactionAmount?: number,
  isInquiryApplicable: boolean = true
): Promise<ITransactionAccount> => {
  try {
    let transactionAccount: ITransactionAccount = await getAccountInfo(
      accountNo,
      cif,
      bankCode,
      accountName,
      transactionAmount,
      accountNo,
      isInquiryApplicable
    );
    if (
      transactionAccount.accountType !== 'RDN' &&
      transactionAccount.accountType !== 'RDS' &&
      transactionAccount.accountType !== 'MBA'
    ) {
      if (bankCode && bankCode.isInternal && !cif) {
        logger.error(`CIF is missing before fetching account information.`);
        throw new TransferAppError(
          `CIF is missing before fetching account information!`,
          TransferFailureReasonActor.SUBMITTER,
          ERROR_CODE.INVALID_MANDATORY_FIELDS
        );
      }

      if (transactionAccount.role === AccountRole.NONE && isInquiryApplicable) {
        const detail = `Invalid account type role: ${transactionAccount.role}!`;
        logger.error(`getSourceAccount: ${detail}`);
        throw new TransferAppError(
          detail,
          TransferFailureReasonActor.SUBMITTER,
          ERROR_CODE.INVALID_ACCOUNT
        );
      }
    }

    return transactionAccount;
  } catch (err) {
    const appError = err && err.error && err.error.error;
    if (appError && appError.code === ACCOUNT_NUMBER_NOT_FOUND) {
      return mapTransferAppError(
        appError,
        ERROR_CODE.SOURCE_ACCOUNT_INQUIRY_FAILED,
        err.status >= 500
      );
    }
    return mapTransferAppError(
      err,
      ERROR_CODE.SOURCE_ACCOUNT_INQUIRY_FAILED,
      true
    );
  }
};

const isTransactionIncomingWithdrawal = (
  sourceBankCodeInfo: IBankCode | undefined,
  sourceAccountInfo: ITransactionAccount | undefined
): boolean =>
  !isEmpty(sourceAccountInfo?.cif) &&
  sourceBankCodeInfo?.rtolCode == JagoExternalBankCode.RTOL_CODE;

const addPrefixWithAccountNumber = (
  bankType: string,
  accountNumber?: string | undefined,
  prefix?: string | undefined
): string | undefined => {
  if (prefix && accountNumber && bankType === InstitutionTypeEnum.WALLET) {
    if (accountNumber.startsWith(PhoneCode.PLUS62)) {
      accountNumber = accountNumber.replace(PhoneCode.PLUS62, '0');
    }
    accountNumber = prefix + accountNumber;
  }
  return accountNumber;
};

const populateTransactionInput = (
  input: ITransactionInput,
  sourceAccountInfo: ITransactionAccount | undefined,
  sourceBankCodeInfo: IBankCode | undefined,
  beneficiaryAccountInfo: ITransactionAccount | undefined,
  beneficiaryBankCodeInfo: IBankCode | undefined,
  executorCIF?: string,
  destinationCIF?: string
): ITransactionInput => {
  let transaction: ITransactionInput = input;
  if (sourceAccountInfo) {
    transaction = {
      ...transaction,
      sourceTransactionCurrency: sourceAccountInfo.currency,
      sourceAccountType: sourceAccountInfo.accountType,
      sourceAccountName: sourceAccountInfo.accountName,
      sourceAccountNo: sourceAccountInfo.accountNumber,
      sourceBankCode: sourceAccountInfo.bankCode,
      sourceCustomerId: sourceAccountInfo.customerId,
      sourceAccountRole: sourceAccountInfo.role,
      moverRemainingDailyUsage: sourceAccountInfo.moverRemainingDailyUsage
    };
  }

  if (sourceBankCodeInfo) {
    transaction.sourceBankCodeChannel = sourceBankCodeInfo.channel;
    transaction.sourceRtolCode = sourceBankCodeInfo.rtolCode;
    transaction.creditPriority = sourceBankCodeInfo.firstPriority;
    transaction.sourceCIF = transaction.sourceCIF || sourceAccountInfo?.cif; // get cif from populated incoming
    transaction.sourceRemittanceCode = sourceBankCodeInfo.remittanceCode;
    if (!executorCIF) {
      executorCIF = isTransactionIncomingWithdrawal(
        sourceBankCodeInfo,
        sourceAccountInfo
      )
        ? sourceAccountInfo?.cif
        : undefined;
    }
  }

  if (beneficiaryAccountInfo) {
    transaction = {
      ...transaction,
      beneficiaryAccountNo:
        beneficiaryBankCodeInfo &&
        addPrefixWithAccountNumber(
          beneficiaryBankCodeInfo.type,
          beneficiaryAccountInfo.accountNumber,
          beneficiaryBankCodeInfo.prefix
        ),
      beneficiaryAccountType:
        beneficiaryAccountInfo?.beneficiaryAccountType ||
        beneficiaryAccountInfo.accountType,
      beneficiaryAccountName: beneficiaryAccountInfo.accountName,
      beneficiaryCIF: transaction.beneficiaryCIF || beneficiaryAccountInfo?.cif, // get cif from populated incoming
      beneficiaryAccountRole: beneficiaryAccountInfo.role,
      beneficiaryCustomerId: beneficiaryAccountInfo.customerId
    };
  }

  if (beneficiaryBankCodeInfo) {
    transaction = {
      ...transaction,
      beneficiaryBankName: beneficiaryBankCodeInfo.name,
      beneficiaryBankCodeChannel: beneficiaryBankCodeInfo.channel,
      debitPriority: beneficiaryBankCodeInfo.firstPriority,
      beneficiaryRtolCode: beneficiaryBankCodeInfo.rtolCode,
      beneficiaryRemittanceCode: beneficiaryBankCodeInfo.remittanceCode,
      billerCode: beneficiaryBankCodeInfo.billerCode,
      beneficiaryIrisCode: beneficiaryBankCodeInfo.irisCode,
      beneficiarySupportedChannels: beneficiaryBankCodeInfo.supportedChannels,
      preferedBankChannel: beneficiaryAccountInfo?.preferredChannel
    };
  }

  transaction.ownerCustomerId = transaction.sourceCustomerId;
  if (isEmpty(transaction.ownerCustomerId)) {
    transaction.ownerCustomerId = transaction.beneficiaryCustomerId;
  }

  return {
    ...transaction,
    sourceBankCodeInfo,
    beneficiaryBankCodeInfo,
    sourceAccountInfo,
    beneficiaryAccountInfo,
    executorCIF,
    destinationCIF
  };
};

const buildNewTransactionModel = (
  transaction: ITransactionInput,
  recommendedService: RecommendationService
): NewEntity<ITransactionModel> => {
  const {
    debitTransactionCode,
    creditTransactionCode,
    blocking,
    feeData
  } = recommendedService;

  let debitTransactionCodeMapping = debitTransactionCode?.map(tc => ({
    ...tc.transactionCodeInfo,
    interchange: tc.interchange
  }));

  let creditTransactionCodeMapping = creditTransactionCode?.map(tc => ({
    ...tc.transactionCodeInfo,
    interchange: tc.interchange
  }));

  let feeMapping = feeData;

  const _referenceId = transaction.referenceId || generateUniqueId();
  const _externalId = transaction.externalId || generateUniqueId();

  let newTransaction: NewEntity<ITransactionModel> = {
    ...transaction,
    referenceId: _referenceId,
    externalId: _externalId,
    paymentServiceCode: recommendedService.paymentServiceCode,
    beneficiaryRealBankCode: recommendedService.beneficiaryBankCode,
    status: TransactionStatus.PENDING,
    userSelectedCategoryCode: transaction.categoryCode,
    sourceAccountName:
      transaction.sourceAccountInfo?.accountName ??
      transaction.sourceAccountName,
    coreBankingTransactionIds: [],
    coreBankingTransactions: [],
    executionType: blocking
      ? ExecutionTypeEnum.BLOCKING
      : ExecutionTypeEnum.NONE_BLOCKING,
    requireThirdPartyOutgoingId: recommendedService.requireThirdPartyOutgoingId,
    debitTransactionCodeMapping,
    creditTransactionCodeMapping,
    feeMapping,
    paymentInstructionId: transaction.paymentInstructionId
  };

  newTransaction = {
    ...newTransaction,
    ...extractTransactionInfoByInterchange(
      newTransaction,
      transaction.interchange
    ),
    journey: getStatusesToBeUpdated(
      TransferJourneyStatusEnum.RECOMMENDATION_FETCHED,
      newTransaction
    )
  };

  return newTransaction;
};

const validateTransactionLimitAmount = (
  amount: number,
  paymentServiceMappings: RecommendationService[],
  interchange?: BankNetworkEnum
): void => {
  let isValid = true;
  paymentServiceMappings.forEach(item => {
    if (!item.debitTransactionCode && !item.creditTransactionCode) {
      return;
    }
    (item.debitTransactionCode || []).forEach(debitTc => {
      const { minAmountPerTx, maxAmountPerTx } = debitTc.transactionCodeInfo;
      if (
        amount < minAmountPerTx ||
        (amount > maxAmountPerTx &&
          !debitTc.transactionCode.endsWith(REVERSAL_TC_SUFFIX))
      ) {
        logger.error(
          `Failed Amount Limit check from debittransactionCode: ${debitTc.transactionCode}`
        );
        isValid = false;
      }
    });

    (item.creditTransactionCode || []).forEach(creditTc => {
      const { minAmountPerTx, maxAmountPerTx } = creditTc.transactionCodeInfo;
      if (
        amount < minAmountPerTx ||
        (amount > maxAmountPerTx && interchange == creditTc.interchange)
      ) {
        logger.error(
          `Failed Amount Limit check from credittransactionCode: ${creditTc.transactionCode}`
        );
        isValid = false;
      }
    });
  });
  if (!isValid) {
    logger.error(
      'validateTransactionLimitAmount: transaction amount is limited'
    );
    throw new TransferAppError(
      'Transaction amount is limited!',
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_AMOUNT,
      [
        {
          message: AMOUNT_IS_LIMITED,
          code: ERROR_CODE.INVALID_LIMIT_TRANSACTION_AMOUNT,
          key: ''
        }
      ]
    );
  }
};

const getPendingBlockingTransaction = async (
  transactionId: string,
  cif: string,
  amount?: number
): Promise<IBlockingTransaction> => {
  const transaction = await transactionRepository.getByKey(transactionId);
  if (!transaction) {
    logger.error(
      `getPendingBlockingTransaction: Matching txn was not found for transactionId: ${transactionId}`
    );
    throw new TransferAppError(
      `Matching txn was not found for transactionId: ${transactionId}!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.TRANSACTION_NOT_FOUND
    );
  }

  if (!transaction.sourceCIF || !transaction.sourceAccountNo) {
    logger.error(
      'getPendingBlockingTransaction: invalid account due to sourceCIF or sourceAccountNo not found'
    );
    throw new TransferAppError(
      `Invalid account due to sourceCIF or sourceAccountNo not found!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.INVALID_ACCOUNT
    );
  }

  if (
    !(
      transaction.status === TransactionStatus.SUBMITTED ||
      transaction.status === TransactionStatus.PENDING
    )
  ) {
    logger.error(
      'getPendingBlockingTransaction: transaction status not pending or submitted'
    );
    throw new TransferAppError(
      `Transaction status must be pending or submitted but received: ${transaction.status}!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.INVALID_TRANSACTION_STATUS
    );
  }

  if (!transaction.blockingId || !transaction.cardId) {
    logger.error('getPendingBlockingTransaction: blocking amount not found');
    throw new TransferAppError(
      'Blocking amount not found, missing blocking ID or card ID!',
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.BLOCKING_ID_NOT_FOUND
    );
  }

  // TODO: Review the logic later
  if (transaction.sourceCIF != cif) {
    logger.error(
      'getPendingBlockingTransaction: invalid account as sourceCIF does not match'
    );
    throw new TransferAppError(
      'Invalid account as sourceCIF does not match!',
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.INVALID_ACCOUNT
    );
  }
  if (amount && transaction.transactionAmount !== amount) {
    logger.error(
      'getPendingBlockingTransaction: invalid amount as transaction amount does not match'
    );
    throw new TransferAppError(
      'Invalid amount as transaction amount does not match!',
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.INVALID_AMOUNT
    );
  }

  return {
    ...transaction,
    sourceAccountNo: transaction.sourceAccountNo,
    sourceCIF: transaction.sourceCIF,
    blockingId: transaction.blockingId,
    cardId: transaction.cardId,
    idempotencyKey: transaction.idempotencyKey
  };
};

const getDeclinedBlockingTransaction = async (
  transactionId: string
): Promise<IBlockingTransaction> => {
  const transaction = await transactionRepository.getByKey(transactionId);
  if (!transaction) {
    logger.error(
      `getDeclinedBlockingTransaction: Matching txn was not found for transactionId: ${transactionId}`
    );
    throw new TransferAppError(
      `Matching txn was not found for transactionId: ${transactionId}!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.TRANSACTION_NOT_FOUND
    );
  }

  if (!transaction.sourceCIF || !transaction.sourceAccountNo) {
    logger.error(
      'getDeclinedBlockingTransaction: Invalid account due to sourceCIF or sourceAccountNo not found'
    );
    throw new TransferAppError(
      `Invalid account due to sourceCIF or sourceAccountNo not found!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.INVALID_ACCOUNT
    );
  }

  if (transaction.status !== TransactionStatus.DECLINED) {
    logger.error(
      'getDeclinedBlockingTransaction: Transaction status is not declined'
    );
    throw new TransferAppError(
      `Transaction status is not declined!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.INVALID_TRANSACTION_STATUS
    );
  }

  if (!transaction.blockingId || !transaction.cardId) {
    logger.error(
      'getDeclinedBlockingTransaction: Blocking id or card id not found'
    );
    throw new TransferAppError(
      `Blocking id or card id not found!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.BLOCKING_ID_NOT_FOUND
    );
  }

  return {
    ...transaction,
    sourceAccountNo: transaction.sourceAccountNo,
    sourceCIF: transaction.sourceCIF,
    blockingId: transaction.blockingId,
    cardId: transaction.cardId,
    idempotencyKey: transaction.idempotencyKey
  };
};

const getPaymentServiceMappings = async (
  data: ITransactionInput,
  configs: IPaymentConfigRule[],
  limitGroups: ILimitGroup[],
  usageCounters: UsageCounter[],
  executionDate: Date = new Date()
): Promise<IPaymentServiceMapping[]> => {
  return transactionMapper.getPaymentServiceMappings(
    data,
    configs,
    limitGroups,
    usageCounters,
    executionDate
  );
};

const updateTransactionStatus = async (
  transactionId: string,
  status: TransactionStatus
): Promise<ITransactionModel | null> => {
  return transactionRepository.update(transactionId, {
    status: status
  });
};

const updateFailedTransactionStatus = async (
  transactionId: string,
  status: TransactionStatus,
  error: TransferAppError
): Promise<ITransactionModel | null> => {
  return transactionRepository.update(transactionId, {
    status: status,
    failureReason: {
      type: ERROR_CODE[error.errorCode as keyof typeof ERROR_CODE],
      actor: error.actor,
      detail: error.detail
    }
  });
};

const getBankCodeMapping = async (bankCode: string): Promise<IBankCode> => {
  const bankInfo = await configurationRepository.getBankCodeMapping(bankCode);
  if (!bankInfo) {
    logger.error(`Bank Info not found for bankCode: ${bankCode}`);
    throw new TransferAppError(
      `Bank Info not found for bankCode: ${bankCode}!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.INVALID_REGISTER_PARAMETER
    );
  }
  return bankInfo;
};

// TODO duplicated logic of getting bank code with getBankCodeMapping and getBankCode
// refactor later
const getBankCodeInfo = async (
  bankCode?: string
): Promise<IBankCode | undefined> => {
  return bankCode ? await getBankCodeMapping(bankCode) : undefined;
};

const getTransactionSearchStartDate = (): Date =>
  moment()
    .subtract(3, 'month')
    .toDate();

const validateTransactionListRequest = (
  input: TransactionListRequest
): boolean => {
  if (!input.transactionId && !input.customerId) {
    logger.error(`Either Customer Id or Transaction Id .`);
    throw new TransferAppError(
      'Customer ID or Transaction ID is empty!',
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.MISSING_CUSTOMER_OR_TRANSACTION_ID
    );
  }
  if (
    (!input.startDate && input.endDate) ||
    (input.startDate && !input.endDate)
  ) {
    logger.error(`Start and End Date missing.`);
    throw new TransferAppError(
      'Start and end date missing!',
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.MISSING_START_AND_END_DATE
    );
  }
  return true;
};

const validateExternalIdAndPaymentType = async (
  transactionModel: NewEntity<ITransactionModel>
): Promise<void> => {
  const transactionList = await transactionRepository.getAllByExternalId(
    transactionModel.externalId
  );
  const transaction = (transactionList || []).find(transaction => {
    return (
      transaction.paymentServiceType === transactionModel.paymentServiceType
    );
  });
  if (transaction) {
    logger.error(
      `Error in validateExternalIdAndPaymentType : Duplicate transaction found with same exteralId and paymentServiceType
      with Journey : ${JSON.stringify(transactionModel.journey)}`
    );
    throw new TransferAppError(
      `Error in validateExternalIdAndPaymentType : Duplicate transaction found with same exteralId = ${transactionModel.externalId} and paymentServiceType = ${transactionModel.paymentServiceType}`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.UNIQUE_FIELD
    );
  }
};

const buildOutgoingTransactionModel = (
  transaction: ITransactionInput
): NewEntity<ITransactionModel> => ({
  ...transaction,
  referenceId: generateUniqueId(),
  externalId: transaction.externalId || generateUniqueId(),
  paymentServiceCode: transaction.paymentServiceCode || '',
  paymentServiceType: transaction.paymentServiceType,
  beneficiaryRealBankCode: transaction.beneficiaryBankCode,
  status: TransactionStatus.PENDING,
  requireThirdPartyOutgoingId: false,
  journey: [
    {
      status: TransferJourneyStatusEnum.REQUEST_RECEIVED,
      updatedAt: new Date()
    }
  ]
});

const getBifastBeneficiaryAdditionalInfo = (
  input: ITransactionInput
): [string | undefined, IBankCode | undefined] => {
  const biFastProxyType = input.additionalInformation3 as BeneficiaryProxyType;
  let beneficiaryAccountNo = input.beneficiaryAccountNo;
  let bifastBankCode: IBankCode | undefined;

  if (
    Object.values(BeneficiaryProxyType).includes(biFastProxyType) &&
    biFastProxyType !== BeneficiaryProxyType.ACCOUNT_NUMBER
  ) {
    beneficiaryAccountNo = input.additionalInformation4;
    bifastBankCode = {
      bankCodeId: 'BC000',
      name: 'BIFAST',
      companyName: 'Bank Indonesia',
      isBersamaMember: false,
      isAltoMember: false,
      channel: BankChannelEnum.BIFAST,
      type: InstitutionTypeEnum.BANK,
      isInternal: false
    };
  }

  return [beneficiaryAccountNo, bifastBankCode];
};

const validateProxyTypeAndBankCode = (
  input: ITransactionInput,
  beneficiaryAccountInfo: ITransactionAccount
): void => {
  const biFastProxyType = input.additionalInformation3 as BeneficiaryProxyType;
  if (
    Object.values(BeneficiaryProxyType).includes(biFastProxyType) &&
    input.additionalInformation3 !== beneficiaryAccountInfo.beneficiaryProxyType
  ) {
    logger.error(
      `validateProxyTypeAndBankCode: Proxy type does not match with input`
    );
    throw new TransferAppError(
      `Proxy type does not match with input!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.BENEFICIARY_ACCOUNT_INQUIRY_FAILED
    );
  }
  // To-do : Uncomment when Komi removes hardcoded bankCode from account inquiry
  // if (input.beneficiaryBankCode !== beneficiaryAccountInfo.bankCode) {
  //   logger.error(
  //     `validateProxyTypeAndBankCode: Bank code does not match with input`
  //   );
  //   throw new AppError(ERROR_CODE.BENEFICIARY_ACCOUNT_INQUIRY_FAILED);
  // }
};

const getCustomerIdByModel = (model: ITransactionModel): string | undefined => {
  let customerId;
  if (
    model.sourceBankCodeChannel === BankChannelEnum.INTERNAL ||
    model.sourceCustomerId
  ) {
    customerId = model.sourceCustomerId;
  } else if (model.sourceBankCodeChannel === BankChannelEnum.EXTERNAL) {
    customerId = model.beneficiaryCustomerId;
  }
  return customerId;
};

const getAdjustedPaymentRail = (
  rail: string = Rail.INTERNAL,
  interchange?: BankNetworkEnum
): string => {
  if (rail === Rail.EURONET && interchange && !isEmpty(interchange)) {
    // For EURONET update the payment rail with an interchange value
    return rail.toUpperCase() + '_' + interchange.toUpperCase();
  }
  return rail.toUpperCase();
};

const setPaymentRailToTransactionData = (
  transactionModel: ITransactionModel,
  rail: Rail = Rail.INTERNAL
): void => {
  const paymentRail = getAdjustedPaymentRail(
    rail,
    transactionModel.interchange
  );

  transactionModel.processingInfo = {
    ...transactionModel.processingInfo,
    paymentRail: paymentRail
  };
};

const getFeeAmountQueries = async (
  feeAmountInputs: FeeAmountInput[]
): Promise<FeeAmountQuery[]> => {
  return Promise.all(
    feeAmountInputs.map(async feeAmountInput => {
      const feeMappings = await feeHelper.getFeeMappings(feeAmountInput);
      return {
        input: feeAmountInput,
        feeMappings: feeMappings
      };
    })
  );
};

export default wrapLogs({
  getFeeAmountInput,
  mapAuthenticationRequired,
  mapFeeData,
  getAccountInfo,
  buildNewTransactionModel,
  validateTransactionLimitAmount,
  getBankCode,
  getPendingBlockingTransaction,
  getDeclinedBlockingTransaction,
  getPaymentServiceMappings,
  updateTransactionStatus,
  updateFailedTransactionStatus,
  getBankCodeMapping,
  populateTransactionInput,
  getBankCodeInfo,
  getSourceAccount,
  getTransactionSearchStartDate,
  validateTransactionListRequest,
  getStatusesToBeUpdated,
  getBeneficiaryAccount,
  validateExternalIdAndPaymentType,
  buildOutgoingTransactionModel,
  getBifastBeneficiaryAdditionalInfo,
  validateProxyTypeAndBankCode,
  getCustomerIdByModel,
  setPaymentRailToTransactionData,
  getAdjustedPaymentRail,
  getUsageCounter,
  getFeeAmountQueries
});
