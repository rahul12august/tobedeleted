import { IMessageProcessorCallbackPayload } from '../common/kafkaConsumer.type';
import transactionService from './transaction.service';
import {
  ITransferTransactionInput,
  ITopupInternalTransactionInput
} from './transaction.type';
import {
  TransactionExecutionTypeEnum,
  Constant,
  PaymentServiceTypeEnum
} from '@dk/module-common';
import { TransactionStatus, PaymentRequestStatus } from './transaction.enum';
import { config } from '../config';
import {
  createTransferTransactionRequestValidator,
  topupInternalTransactionRequestPayload
} from './transaction.validator';
import { validateHandler } from '../common/handleValidationErrors';
import {
  ITransactionRetryReversalMessage,
  ITransactionReversalRetryPolicy
} from './transaction.producer.type';
import transactionReversalService from './transactionReversal.service';
import logger, { wrapLogs } from '../logger';
import transactionProducer from './transaction.producer';
import { delay, sumBy } from 'lodash';
import transactionHelper from './transaction.helper';
import transactionBlocking from './transactionBlocking.service';
import {
  ITransactionModel,
  IPaymentInstructionExecutionModel
} from './transaction.model';
import transactionBlockingService from './transactionBlocking.service';
import transactionRepository from './transaction.repository';
import {
  PaymentInstructionTopicConstant,
  ITransactionResult
} from '@dk/module-message';
import { updateExtraFields } from './transaction.util';
import { setRetryCount } from '../common/contextHandler';
import { inspect } from 'util';
import { trimStringValuesFromObject } from '../common/common.util';
import generalRefundService from './refund/generalRefund.service';
import { TRANSACTION_RESPONSE_TOPIC_FIELD } from './transaction.constant';
import entitlementService from '../entitlement/entitlement.service';
import entitlementFlag from '../common/entitlementFlag';

const transactionReversalRetryPolicy: ITransactionReversalRetryPolicy = config.get(
  'transactionReversalRetryPolicy'
);

export const executionTransaction = async (
  messageToConsume: IMessageProcessorCallbackPayload,
  messageTopic: string
): Promise<void> => {
  let result: ITransactionResult;
  const value = JSON.parse(messageToConsume.message.value);
  const inputPaymentServiceType = value && value.paymentServiceType;
  const isTopupWallet =
    inputPaymentServiceType === PaymentServiceTypeEnum.WALLET;
  let payload: ITransferTransactionInput | ITopupInternalTransactionInput;
  if (isTopupWallet) {
    payload = validateHandler(value, topupInternalTransactionRequestPayload, {
      stripUnknown: true // only validate and return fields belong to topupInternalTransaction
    });
  } else {
    payload = validateHandler(value, createTransferTransactionRequestValidator);
  }

  let executionType = messageToConsume.message.headers[Constant.EXECUTION_TYPE];
  executionType = executionType && executionType.toString();

  let paymentInstructionCode =
    messageToConsume.message.headers[Constant.PI_KEY_HEADER];
  paymentInstructionCode =
    paymentInstructionCode && paymentInstructionCode.toString();

  const transactionResponseTopic =
    messageToConsume.message.headers[TRANSACTION_RESPONSE_TOPIC_FIELD];

  if (paymentInstructionCode) {
    const messageTopics = [];
    messageTopics.push(messageTopic);
    if (transactionResponseTopic) messageTopics.push(transactionResponseTopic);

    const paymentInstructionExecution: IPaymentInstructionExecutionModel = {
      paymentInstructionCode,
      executionType,
      topics: messageTopics
    };
    payload.paymentInstructionExecution = paymentInstructionExecution;
  }

  let externalId;
  let transaction: ITransactionModel;
  externalId = payload.externalId;
  result = {
    externalId,
    status: PaymentRequestStatus.SUCCEED,
    extra: updateExtraFields(payload)
  };

  if (payload.paymentInstructionId) {
    result = {
      ...result,
      paymentInstructionId: payload.paymentInstructionId
    };
  }

  logger.info(
    `executionTransaction : Create ${payload.paymentInstructionExecution?.executionType} transaction with
      topicName: ${messageToConsume.topicName}
      sourceCIF: ${payload.sourceCIF},
      sourceAccountNo: ${payload.sourceAccountNo},
      beneficiaryBankCode: ${payload.beneficiaryBankCode},
      beneficiaryAccountNo: ${payload.beneficiaryAccountNo},
      paymentServiceCode: ${payload.paymentServiceCode},
      paymentInstructionId: ${payload.paymentRequestID},
      externalId: ${payload.externalId}`
  );
  try {
    trimStringValuesFromObject(payload);
    switch (executionType) {
      case TransactionExecutionTypeEnum.CREATE_BLOCKING: {
        logger.debug(
          `executionTransaction : Create blocking transaction payload : ${JSON.stringify(
            payload
          )}`
        );
        transaction = await transactionBlocking.createBlockingTransferTransaction(
          payload as ITransferTransactionInput
        );
        result = {
          ...result,
          transactionId: transaction.id,
          transactionStatus: transaction.status
        };
        logger.info(
          `transactionCompleted: Blocking transaction 
            customerId : ${transaction.sourceCustomerId},
            externalId : ${transaction.externalId}, 
            transactionId: ${transaction.id},
            beneficiaryAccount: ${transaction.beneficiaryAccountNo},
            beneficiaryBankCode: ${transaction.beneficiaryBankCode},
            sourceAccount: ${transaction.sourceAccountNo},
            journey: ${JSON.stringify(transaction.journey)}`
        );
        break;
      }
      case TransactionExecutionTypeEnum.ADJUST_BLOCKING: {
        if (payload.externalId && payload.sourceCIF) {
          logger.debug(
            `executionTransaction : Adjust blocking transaction payload : ${JSON.stringify(
              payload
            )}`
          );
          const transaction = await transactionBlockingService.adjustBlockingTransferTransaction(
            payload.sourceCIF,
            payload.externalId,
            payload.transactionAmount
          );
          result = {
            ...result,
            transactionAmount: transaction.transactionAmount,
            transactionStatus: transaction.status,
            transactionId: transaction.id
          };
          logger.info(
            `transactionCompleted: Adjust blocking transaction 
              customerId : ${transaction.sourceCustomerId},
              externalId : ${transaction.externalId}, 
              transactionId: ${transaction.id},
              beneficiaryAccount: ${transaction.beneficiaryAccountNo},
              beneficiaryBankCode: ${transaction.beneficiaryBankCode},
              sourceAccount: ${transaction.sourceAccountNo},
              Journey: ${JSON.stringify(transaction.journey)}`
          );
        }
        break;
      }
      case TransactionExecutionTypeEnum.CANCEL_BLOCKING: {
        if (payload.externalId && payload.sourceCIF) {
          logger.debug(
            `executionTransaction : Cancel blocking transaction payload : ${JSON.stringify(
              payload
            )}`
          );
          await transactionBlockingService.cancelBlockingTransferTransaction(
            payload.sourceCIF,
            payload.externalId
          );
          result = {
            ...result,
            transactionStatus: TransactionStatus.DECLINED
          };
        }
        break;
      }
      case TransactionExecutionTypeEnum.CONFIRM_BLOCKING: {
        if (payload.externalId && payload.sourceCIF) {
          logger.debug(
            `executionTransaction : Confirm blocking transaction payload : ${JSON.stringify(
              payload
            )}`
          );
          transaction = await transactionBlockingService.executeBlockingTransferTransaction(
            payload.sourceCIF,
            payload.externalId,
            payload.transactionAmount
          );
          result = {
            ...result,
            transactionStatus: transaction.status,
            referenceId: transaction.referenceId
          };
          logger.info(
            `transactionCompleted: Confirm blocking transaction 
              customerId : ${transaction.sourceCustomerId},
              externalId : ${transaction.externalId}, 
              transactionId: ${transaction.id},
              beneficiaryAccount: ${transaction.beneficiaryAccountNo},
              beneficiaryBankCode: ${transaction.beneficiaryBankCode},
              sourceAccount: ${transaction.sourceAccountNo}
              Journey: ${JSON.stringify(transaction.journey)}`
          );
        }
        break;
      }
      default:
        if (!isTopupWallet) {
          logger.debug(
            `executionTransaction : Create transfer transaction payload : ${JSON.stringify(
              payload
            )}`
          );
          if (
            payload.paymentServiceType === PaymentServiceTypeEnum.GENERAL_REFUND
          ) {
            logger.info(
              `executionTransaction: Create general refund transaction, payload: ${JSON.stringify(
                payload.generalRefund
              )}`
            );
            transaction = await generalRefundService.refundTransaction(
              payload.generalRefund
            );
          } else {
            logger.info(`executionTransaction: Create transfer transaction.`);
            transaction = await transactionService.createTransferTransaction(
              payload as ITransferTransactionInput
            );
          }
        } else {
          logger.debug(
            `executionTransaction : Create internal topup transaction payload : ${JSON.stringify(
              payload
            )}`
          );
          transaction = await transactionService.createInternalTopupTransaction(
            payload
          );
        }
        result = {
          ...result,
          transactionId: transaction.id,
          transactionStatus: transaction.status,
          referenceId: transaction.referenceId,
          transactionAmount: transaction.transactionAmount,
          feeAmount:
            transaction.fees && transaction.fees.length
              ? sumBy(transaction.fees, 'feeAmount')
              : 0
        };
        break;
    }
  } catch (err) {
    logger.error(
      `executionTransaction : Error while executing transaction with external id ${externalId},
        errors : ${inspect(err, false, 5)},
        errorCode : ${err.errorCode}`
    );
    let transactionStatus: TransactionStatus | undefined;
    const transaction =
      externalId &&
      (await transactionRepository.getByExternalId(externalId).catch(getErr => {
        logger.error(
          "can't get transaction by externalId",
          JSON.stringify(getErr)
        );
        transactionStatus = undefined;
      }));
    if (transaction) {
      transactionStatus = transaction.status;
    }
    result = {
      ...result,
      status: PaymentRequestStatus.DECLINED,
      transactionStatus: transactionStatus,
      transactionAmount: payload.transactionAmount,
      error: {
        errorCode: err.errorCode,
        retriable: err.retry,
        errors: err.errors
      },
      failureReason: transaction ? transaction.failureReason : undefined
    };
    if (await entitlementFlag.isEnabled()) {
      await entitlementService.processReversalEntitlementCounterUsageBasedOnContext();
    }
  }

  if (messageTopic) {
    logger.info(
      `executionTransaction: Producing message on topic : ${messageTopic} with
        referenceId: ${result.referenceId},
        externalId: ${result.externalId},
        transactionId: ${result.transactionId},
        status: ${result.status},
        transactionStatus: ${result.transactionStatus},
        sourceAccountNo: ${payload.sourceAccountNo},
        beneficiaryAccountNo: ${payload.beneficiaryAccountNo},
        paymentInstructionId: ${payload.paymentRequestID},
        error: ${JSON.stringify(result.error)}`
    );
    transactionProducer.sendTransactionMessage(
      messageTopic,
      result,
      executionType,
      paymentInstructionCode
    );
    if (transactionResponseTopic) {
      transactionProducer.sendTransactionMessage(
        transactionResponseTopic,
        result,
        executionType,
        paymentInstructionCode
      );
    }
  }
};

export const directTransactionHandler = async (
  messageToConsume: IMessageProcessorCallbackPayload
): Promise<void> => {
  logger.debug(
    `directTransactionHandler: Payload for direct transaction handler : ${JSON.stringify(
      messageToConsume
    )} `
  );
  logger.info(`directTransactionHandler: Direct transaction handler`);
  const messageTopic =
    PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE;
  await setRetryCount(3);
  await executionTransaction(messageToConsume, messageTopic);
};

export const autoTransferTransactionHandler = async (
  messageToConsume: IMessageProcessorCallbackPayload
): Promise<void> => {
  logger.debug(
    `autoTransferTransactionHandler: Payload for auto transfer transaction handler: ${JSON.stringify(
      messageToConsume
    )} `
  );
  logger.info(
    `autoTransferTransactionHandler: Auto transfer transaction handler`
  );
  const messageTopic =
    PaymentInstructionTopicConstant.UPDATE_TRANSACTION_STATUS;
  await setRetryCount(3);
  await executionTransaction(messageToConsume, messageTopic);
};

export const retryTransactionReversalHandler = async (
  message: ITransactionRetryReversalMessage
): Promise<void> => {
  if (message.retryCounter <= transactionReversalRetryPolicy.maxRetries) {
    const transaction = await transactionRepository.findOneByQuery({
      _id: message.transactionId
    });
    transaction &&
      (await transactionReversalService
        .reverseTransactionsByInstructionId(
          message.transactionId,
          undefined,
          transaction
        )
        .catch(err => {
          if (
            message.retryCounter < transactionReversalRetryPolicy.maxRetries
          ) {
            logger.error(
              'retry transaction reversal has failed. Producing next retry message',
              JSON.stringify(err)
            );
            const delayTime =
              transactionReversalRetryPolicy.delayTimeInSeconds *
              message.retryCounter *
              1000;
            delay(
              transactionProducer.sendTransactionRetryReversalCommand,
              delayTime,
              [
                {
                  transactionId: message.transactionId,
                  retryCounter: message.retryCounter + 1
                }
              ]
            );
          } else {
            logger.error(
              `retry transaction reversal has failed after all retry, max retries setting: ${transactionReversalRetryPolicy.maxRetries}.Moving message to reversal_failed topic`,
              JSON.stringify(err)
            );
            transactionProducer.sendTransactionReversalFailedEvent(message);
          }
        })
        .then(async () => {
          await transactionHelper.updateTransactionStatus(
            message.transactionId,
            TransactionStatus.REVERTED
          );
        }));
  } else {
    logger.error(
      `the received message has retryCounter ${message.retryCounter} that exceeds max retries setting: ${transactionReversalRetryPolicy.maxRetries}.Moving message to reversal_failed topic`
    );
    transactionProducer.sendTransactionReversalFailedEvent(message);
  }
};

export default wrapLogs({
  autoTransferTransactionHandler,
  retryTransactionReversalHandler,
  directTransactionHandler
});
