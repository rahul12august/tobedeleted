import { ITransactionModel } from './transaction.model';
import { ITransactionStatusTemplate } from './transaction.type';
import { wrapLogs } from '../logger';
import bifastStatusTemplate from '../bifast/bifastStatus.template';

const templates: ITransactionStatusTemplate[] = [bifastStatusTemplate];

const getStatusTemplate = (
  model: ITransactionModel
): ITransactionStatusTemplate | undefined => {
  const template:
    | ITransactionStatusTemplate
    | undefined = templates.find(template => template.isEligible(model));
  return template;
};

const outgoingStatusTemplateService = wrapLogs({
  getStatusTemplate
});

export default outgoingStatusTemplateService;
