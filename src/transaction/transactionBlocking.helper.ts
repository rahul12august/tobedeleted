import logger, { wrapLogs } from '../logger';
import { IFeeModel, ITransactionModel } from './transaction.model';
import { RetryConfig } from './transaction.util';
import {
  IBlockingAmountResponse,
  IBlockingRequestPayload
} from '../account/account.type';
import { Enum, Util } from '@dk/module-common';
import transactionRepository from './transaction.repository';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import depositRepository from '../coreBanking/deposit.repository';
import configurationRepository from '../configuration/configuration.repository';
import { COUNTER_MAMBU_BLOCK_DAYS } from '../configuration/configuration.constant';
import blockingAmountService from '../blockingAmount/blockingAmount.service';
import {
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from './transaction.enum';
import transactionHelper from './transaction.helper';
import { config } from '../config';

const createBlockingAmount = async (
  accountNo: string,
  name: string,
  amount: number,
  blockingPurposeCode: Enum.BlockingAmountType,
  mambuBlockingCode: string
): Promise<IBlockingAmountResponse> => {
  const blockingPayload: IBlockingRequestPayload = {
    amount: amount,
    name: name,
    blockingPurposeCode: blockingPurposeCode,
    mambuBlockingCode
  };
  return blockingAmountService.createBlockingAmount(accountNo, blockingPayload);
};

const getMambuBlockingCode = async (): Promise<string> => {
  const defaultBlockingDurationCode = config.get('amountBlockDurationCode');
  if (defaultBlockingDurationCode) {
    logger.info('getMambuBlockingCode: send default mambu blocking code');
    return defaultBlockingDurationCode;
  }
  logger.info(
    'getMambuBlockingCode: get blocking duration code by bussiness days from config'
  );
  const blockingDurationCodeByBusinessDays = await configurationRepository.getCounterByCode(
    COUNTER_MAMBU_BLOCK_DAYS
  );
  if (!blockingDurationCodeByBusinessDays) {
    logger.error(`getMambuBlockingCode: couldn't find mambu blocking code`);
    throw new TransferAppError(
      "Couldn't find mambu blocking code",
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.MAMBU_BLOCKING_COUNTER_NOT_FOUND
    );
  }
  return blockingDurationCodeByBusinessDays.value.toString();
};

const createBlockingTransaction = async (
  model: ITransactionModel,
  blockingPurposeCode: Enum.BlockingAmountType
): Promise<ITransactionModel> => {
  if (model.sourceCIF && model.sourceAccountNo) {
    const mambuBlockingCode = await getMambuBlockingCode();
    logger.info(
      `createBlockingTransaction: Mambu blocking code: ${mambuBlockingCode}`
    );
    const blockingMainResponse = await createBlockingAmount(
      model.sourceAccountNo,
      model.additionalInformation4 || 'block for transaction id: ' + model.id,
      model.transactionAmount,
      blockingPurposeCode,
      mambuBlockingCode
    );
    logger.info(
      `create blocking principle successfully with blockId: ${blockingMainResponse.id}`
    );
    model.blockingId = blockingMainResponse.id;
    model.cardId = blockingMainResponse.cardId;
    if (
      model.fees &&
      blockingPurposeCode === Enum.BlockingAmountType.MOBILE_TRANSACTION
    ) {
      model.fees = await Promise.all(
        model.fees.map(
          async (fee): Promise<IFeeModel> => {
            if (model.sourceCIF && model.sourceAccountNo && fee.feeAmount) {
              const blockingFeeResponse = await createBlockingAmount(
                model.sourceAccountNo,
                model.additionalInformation4 ||
                  'block for transaction id: ' + model.id, // TODO: will change later, currently blocking require name blocking
                fee.feeAmount,
                blockingPurposeCode,
                mambuBlockingCode
              );
              logger.info(
                `create blocking fee successfully with blockId: ${blockingFeeResponse.id}`
              );
              fee.blockingId = blockingFeeResponse.id;
              fee.idempotencyKey = blockingFeeResponse.idempotencyKey;
            }
            return fee;
          }
        )
      );
    }

    model.idempotencyKey = blockingMainResponse.idempotencyKey;
    const transaction = await transactionRepository.update(model.id, {
      ...model,
      journey: transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.AMOUNT_BLOCKED,
        model
      )
    });

    if (transaction) {
      return transaction;
    }
  }
  logger.error(
    `createBlockingTransaction: Mandatory field missing sourceAccount : ${model.sourceAccountNo} or sourceCif: ${model.sourceCIF}`
  );
  throw new TransferAppError(
    `Mandatory field missing sourceAccount: ${model.sourceAccountNo} or sourceCif: ${model.sourceCIF}`,
    TransferFailureReasonActor.SUBMITTER,
    ERROR_CODE.INVALID_REQUEST
  );
};

const reverseAuthorizationHoldWithRetry = async (
  cardReferenceToken: string,
  authHoldExtReferenceId: string
) => {
  return await Util.retry(
    RetryConfig(),
    depositRepository.reverseAuthorizationHold,
    cardReferenceToken,
    authHoldExtReferenceId
  );
};

const cancelBlockingTransaction = async (
  transaction: ITransactionModel
): Promise<void> => {
  if (
    transaction.sourceCIF &&
    transaction.sourceAccountNo &&
    transaction.cardId
  ) {
    if (transaction.blockingId) {
      // unblock principle
      await reverseAuthorizationHoldWithRetry(
        transaction.cardId,
        transaction.blockingId
      );
    }

    if (transaction.fees) {
      // unblock fee
      await Promise.all(
        transaction.fees.map(
          async (fee): Promise<void> => {
            if (
              transaction.sourceCIF &&
              transaction.sourceAccountNo &&
              transaction.cardId &&
              fee.blockingId
            ) {
              await reverseAuthorizationHoldWithRetry(
                transaction.cardId,
                fee.blockingId
              );
            }
          }
        )
      );
    }
  }
};
const transactionBlockingHelper = wrapLogs({
  cancelBlockingTransaction,
  createBlockingTransaction
});

export default transactionBlockingHelper;
