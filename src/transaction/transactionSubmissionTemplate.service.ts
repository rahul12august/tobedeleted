import { ITransactionModel } from './transaction.model';
import { ITransactionSubmissionTemplate } from './transaction.type';
import { wrapLogs } from '../logger';

import switchingSubmissionTemplate from './transactionSubmissionSwitchingTemplate.service';
import submitPaymentTemplate from '../billingAggregator/billingAggregatorSubmitPaymentTemplate.service';
import irisSubmissionTemplate from '../iris/irisSubmissionTemplate.service';
import rtgsSubmissionTemplate from '../rtgs/rtgsSubmissionTemplate.service';
import sknSubmissionTemplate from '../skn/sknSubmissionTemplate.service';
import tokopediaSubmissionTemplate from '../tokopedia/tokopediaSubmissionTemplate.service';
import syariahRtgsSubmissionTemplateService from '../rtgs/syariahRtgsSubmissionTemplate.service';
import syariahSknSubmissionTemplateService from '../skn/syariahSknSubmissionTemplate.service';
import bifastSubmissionTemplate from '../bifast/bifastSubmissionTemplate.service';

const templates: ITransactionSubmissionTemplate[] = [
  bifastSubmissionTemplate,
  rtgsSubmissionTemplate,
  syariahRtgsSubmissionTemplateService,
  sknSubmissionTemplate,
  syariahSknSubmissionTemplateService,
  submitPaymentTemplate,
  tokopediaSubmissionTemplate,
  irisSubmissionTemplate,
  switchingSubmissionTemplate
];

/**
 * Get eligible {@link ITransactionSubmissionTemplate} for a given transaction model
 *
 * @param model transaction model
 * @returns the submission template or undefined if it does not has
 */
const getSubmissionTemplate = (
  model: ITransactionModel
): ITransactionSubmissionTemplate | undefined => {
  const template:
    | ITransactionSubmissionTemplate
    | undefined = templates.find(template => template.isEligible(model));
  return template;
};

const outgoingSubmissionTemplateService = wrapLogs({
  getSubmissionTemplate
});

export default outgoingSubmissionTemplateService;
