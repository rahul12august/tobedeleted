import { BankChannelEnum } from '@dk/module-common';
import { ERROR_CODE } from '../common/errors';
import { TransferAppError } from '../errors/AppError';
import logger, { wrapLogs } from '../logger';
import {
  ConfirmTransactionStatus,
  TransactionStatus,
  TransferFailureReasonActor
} from './transaction.enum';
import { ITransactionModel } from './transaction.model';
import transactionRepository from './transaction.repository';
import transactionService from './transaction.service';
import altoService from './../alto/alto.service';
import {
  IConfirmTransactionInput,
  ITransactionSubmissionTemplate
} from './transaction.type';
import switchingTransactionService, {
  responseMessageToPaymentInstruction
} from './transactionConfirmation.service';
import transactionExecutionService from './transactionExecution.service';
import transactionHistoryRepository from '../transactionHistory/transactionHistory.repository';
import altoUtil from '../alto/alto.utils';

const markTxnDeclineManually = async (
  txn: ITransactionModel
): Promise<void> => {
  const input: IConfirmTransactionInput = {
    externalId: txn.externalId,
    responseCode: ConfirmTransactionStatus.ERROR_EXPECTED,
    transactionResponseID: undefined
  };
  await switchingTransactionService.reverseTransaction(input, txn);
  if (txn.paymentInstructionExecution) {
    responseMessageToPaymentInstruction(input, txn);
  }
};

const markTxnSucceedManually = async (
  txn: ITransactionModel,
  confirmTemplate: ITransactionSubmissionTemplate
): Promise<void> => {
  const input: IConfirmTransactionInput = {
    externalId: txn.externalId,
    responseCode: ConfirmTransactionStatus.SUCCESS,
    transactionResponseID: undefined,
    interchange: txn.interchange
  };
  const updateInfo = switchingTransactionService.extractChangeFromConfirmTransactionRequest(
    input,
    txn,
    false
  );
  const updatedTxn = await transactionRepository.update(txn.id, updateInfo);
  if (!updatedTxn) {
    logger.error(
      `markTxnSucceedManually: transaction update failed for id:${txn.id}`
    );
    throw new TransferAppError(
      `Transaction update failed for id: ${txn.id}!`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION
    );
  }

  const updatedTransaction = await transactionExecutionService.settleTransaction(
    updatedTxn,
    confirmTemplate
  );

  if (updatedTxn.paymentInstructionExecution) {
    responseMessageToPaymentInstruction(input, updatedTransaction);
  }
};

const extTxnManualAdjstment = async (
  txn: ITransactionModel,
  reqStatus: TransactionStatus
): Promise<void> => {
  logger.info(
    `extTxnManualAdjstment: transaction bank channel ${txn.beneficiaryBankCodeChannel} for manual adjustment`
  );
  if (
    txn.status !== TransactionStatus.SUBMITTED &&
    txn.status !== TransactionStatus.DECLINED &&
    txn.status !== TransactionStatus.PENDING
  ) {
    const detail = `Invalid transaction status to manually adjusted for txnId: ${txn.id}!`;
    logger.error(`extTxnManualAdjstment: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.INVALID_TRANSACTION_STATUS
    );
  }

  try {
    if (
      (txn.status === TransactionStatus.SUBMITTED ||
        txn.status === TransactionStatus.PENDING) &&
      reqStatus === TransactionStatus.DECLINED
    ) {
      if (txn.beneficiaryBankCodeChannel == BankChannelEnum.QRIS) {
        await altoService.markQrisTxnDeclineManually(txn);
      } else {
        await markTxnDeclineManually(txn);
      }
    }

    if (
      (txn.status === TransactionStatus.SUBMITTED ||
        txn.status === TransactionStatus.PENDING) &&
      reqStatus === TransactionStatus.SUCCEED
    ) {
      await markTxnSucceedManually(txn, {
        submitTransaction: switchingTransactionService.submitConfirmTransaction,
        isEligible: () => true,
        declineOnSubmissionFailure: false
      });
    }

    if (
      txn.status === TransactionStatus.DECLINED &&
      reqStatus === TransactionStatus.SUCCEED
    ) {
      await markTxnSucceedManually(txn, {
        submitTransaction: transactionExecutionService.executeDebit,
        isEligible: () => true,
        declineOnSubmissionFailure: false
      });
    }

    if (
      txn.beneficiaryBankCodeChannel == BankChannelEnum.QRIS &&
      txn.coreBankingTransactionIds &&
      txn.coreBankingTransactionIds[0]
    ) {
      if (reqStatus === TransactionStatus.SUCCEED) {
        const checkStatus = await altoService.checkStatus(
          txn.sourceCustomerId ?? '',
          txn.paymentInstructionId ?? ''
        );

        const updatedTrx = await transactionService.getTransaction(txn.id);

        altoUtil.setQrisResponseToTransactionData(
          updatedTrx,
          checkStatus.forwardingCustomerReferenceNumber
        );

        altoUtil.updateTransactionDataOnQrisPaymentSuccess(
          updatedTrx,
          checkStatus
        );

        // don't need to wait the response, let's do this asynchronously
        transactionHistoryRepository.updateTransactionHistory(
          txn.coreBankingTransactionIds[0] ?? '',
          altoUtil.getTransactionHistoryStatus(checkStatus.transactionStatus),
          checkStatus.forwardingCustomerReferenceNumber
        );

        await transactionRepository.update(updatedTrx.id, updatedTrx);
      } else {
        transactionHistoryRepository.updateTransactionHistory(
          txn.coreBankingTransactionIds[0],
          reqStatus
        );
      }
    }
  } catch (error) {
    logger.error(
      `extTxnManualAdjstment: error while manually adjusting transaction: ${
        txn.id
      }, status: ${error && error.status}, message: ${error && error.message}`
    );
    throw error;
  } finally {
    await transactionService.findByIdAndUpdate(txn.id, {
      manuallyAdjusted: true
    });
  }
};

const txnManualAdjustmentService = wrapLogs({
  extTxnManualAdjstment
});

export default txnManualAdjustmentService;
