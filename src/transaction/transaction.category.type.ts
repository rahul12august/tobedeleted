import { PaymentServiceTypeEnum } from '@dk/module-common';

export interface ITransactionCategoryMetadata {
  paymentServiceType: PaymentServiceTypeEnum;
  paymentServiceCode: string;
  limitGroupCode: string;
  //transactionCode: string;
  interchange?: string;
}
