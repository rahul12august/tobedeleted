import { IBillDetailsResponse } from '../billPayment/billDetail/billDetails.type';

export interface IFailedTransferNotificationMessage {
  cif: string;
  currency: string;
  amount: number;
  beneficiaryName: string;
  beneficiaryAccountNumber: string;
}
export interface ITransactionRetryReversalMessage {
  transactionId: string;
  retryCounter: number;
}

export interface ITransactionReversalRetryPolicy {
  maxRetries: number;
  delayTimeInSeconds: number;
}

export interface IPendingBillPaymentMessage {
  billDetails: IBillDetailsResponse;
  transactionAmount: number;
  transactionId: string;
  transactionTime: Date;
}

export interface IATMWithdrawalExceedsDailyLimitMessage {
  customerId: string;
  pocketName: string;
  amount: number;
}

export interface IATMTransferExceedsDailyLimitMessage {
  customerId: string;
  beneficiaryAccountNo: string;
  beneficiaryName: string;
  amount: number;
}
