import {
  createReverseInput,
  getTransactionModelFullData,
  getTransactionWithCoreBankingTransactionIds
} from '../__mocks__/transaction.data';
import depositRepository from '../../coreBanking/deposit.repository';
import transactionReversalService from '../transactionReversal.service';
import transactionRepository from '../transaction.repository';
import { ERROR_CODE } from '../../common/errors';
import {
  CoreBankTransactionType,
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from '../transaction.enum';
import loanRepository from '../../coreBanking/loan.repository';
import { isFeatureEnabled } from '../../common/featureFlag';
import { BankNetworkEnum } from '@dk/module-common';

jest.mock('../../coreBanking/deposit.repository');
jest.mock('../transaction.repository');
jest.mock('../../coreBanking/loan.repository');
jest.mock('../../common/featureFlag');

describe('transaction reversal service', () => {
  describe('reverseTransaction', () => {
    const referenceId = 'referenceId';
    const savedTransaction = {
      ...getTransactionModelFullData(),
      referenceId
    };

    const coreBankingTransaction1 = {
      id: '1',
      amount: 1000,
      adjustmentTransactionKey: undefined
    };
    const coreBankingTransaction2 = {
      id: '2',
      amount: 1000,
      adjustmentTransactionKey: undefined
    };

    beforeEach(() => {
      (transactionRepository.getByKey as jest.Mock).mockResolvedValue(
        savedTransaction
      );
      (depositRepository.getTransactionsByReferenceId as jest.Mock).mockResolvedValue(
        []
      );
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should throw error if transaction not found', async () => {
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(null);
      try {
        await transactionReversalService.reverseTransaction('id', {});
      } catch (e) {
        expect(e.errorCode).toEqual(ERROR_CODE.TRANSACTION_NOT_FOUND);
      }
    });

    it('should throw error if call coreBanking fail', async () => {
      // Given
      (depositRepository.getTransactionsByReferenceId as jest.Mock).mockResolvedValue(
        [coreBankingTransaction1, coreBankingTransaction2]
      );
      const transactionId = '123';
      const inputRequest = createReverseInput();
      (depositRepository.reverseTransaction as jest.Mock).mockRejectedValue(
        new Error()
      );

      // When
      await transactionReversalService
        .reverseTransaction(transactionId, inputRequest)
        .catch(err => {
          expect(depositRepository.reverseTransaction).toBeCalledWith(
            '1',
            inputRequest
          );
          // then
          expect(err.errorCode).toEqual(ERROR_CODE.CAN_NOT_REVERSE_TRANSACTION);
        });
    });

    it('should not revert already revert transaction', async () => {
      // Given
      (depositRepository.getTransactionsByReferenceId as jest.Mock).mockResolvedValueOnce(
        [
          {
            id: '1',
            adjustmentTransactionKey: '3'
          }
        ]
      );
      const transactionId = '123';
      const inputRequest = createReverseInput();

      (depositRepository.reverseTransaction as jest.Mock).mockResolvedValue({
        data: undefined
      });
      // When
      const expected = await transactionReversalService.reverseTransaction(
        transactionId,
        inputRequest
      );
      // then
      expect(expected).toBeUndefined();
      expect(
        transactionRepository.updateTransactionsByReferenceId
      ).toBeCalledWith(referenceId, {
        status: TransactionStatus.REVERTED
      });
      expect(depositRepository.reverseTransaction).not.toBeCalled();
    });

    it('should revert transaction success if coreBanking success', async () => {
      // Given
      (depositRepository.getTransactionsByReferenceId as jest.Mock).mockResolvedValueOnce(
        [coreBankingTransaction1, coreBankingTransaction2]
      );
      (depositRepository.getTransactionsByReferenceId as jest.Mock).mockResolvedValueOnce(
        [coreBankingTransaction2]
      );
      const transactionId = '123';
      const inputRequest = createReverseInput();

      (depositRepository.reverseTransaction as jest.Mock).mockResolvedValue({
        data: undefined
      });
      // When
      const expected = await transactionReversalService.reverseTransaction(
        transactionId,
        inputRequest
      );
      // then
      expect(expected).toBeUndefined();
      expect(
        transactionRepository.updateTransactionsByReferenceId
      ).toBeCalledWith(referenceId, {
        status: TransactionStatus.REVERTED
      });
      expect(depositRepository.reverseTransaction).toBeCalledWith(
        '2',
        inputRequest
      );
    });

    it("should revert card transaction if it's available", async () => {
      // Given
      const cardTransaction = {
        ...coreBankingTransaction1,
        cardTransaction: {
          amount: 10000,
          cardToken: 'card01',
          externalReferenceId: 'ref01'
        }
      };
      (depositRepository.getTransactionsByReferenceId as jest.Mock).mockResolvedValueOnce(
        [cardTransaction]
      );
      const transactionId = '123';
      const inputRequest = createReverseInput();

      (depositRepository.reverseTransaction as jest.Mock).mockResolvedValue({
        data: undefined
      });

      // When
      await transactionReversalService.reverseTransaction(
        transactionId,
        inputRequest
      );

      // then
      expect(depositRepository.reverseCardTransaction).toBeCalledWith(
        cardTransaction.cardTransaction.cardToken,
        cardTransaction.cardTransaction.externalReferenceId,
        {
          amount: cardTransaction.cardTransaction.amount,
          externalReferenceId:
            cardTransaction.cardTransaction.externalReferenceId
        }
      );
    });
  });

  describe('reverseTransactionsByInstructionId', () => {
    const savedTransaction = {
      ...getTransactionModelFullData(),
      referenceId: 'referenceId',
      createdAt: new Date('01 June 2020 00:00:00')
    };

    beforeEach(() => {
      jest.resetAllMocks();
    });
    beforeEach(() => {
      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValue(
        savedTransaction
      );
    });

    it('should not call reverse if un-reverted transaction not found', async () => {
      (depositRepository.getTransactionsByInstructionId as jest.Mock).mockResolvedValueOnce(
        []
      );
      await transactionReversalService.reverseTransactionsByInstructionId(
        '1',
        'test',
        {
          ...savedTransaction,
          id: '1'
        }
      );
      expect(depositRepository.getTransactionsByInstructionId).toBeCalledWith(
        '1',
        savedTransaction.createdAt
      );
      expect(depositRepository.reverseTransaction).not.toBeCalled();
    });

    it('should query and inverse transactions until complete instead of adjust', async () => {
      const coreBankingTransactions = [
        {
          id: '1',
          type: CoreBankTransactionType.DEPOSIT
        },
        {
          id: '2',
          type: CoreBankTransactionType.WITHDRAWAL
        },
        {
          id: '3',
          type: CoreBankTransactionType.WITHDRAWAL
        }
      ];
      let counter = 0;
      (depositRepository.getTransactionsByInstructionId as jest.Mock).mockImplementation(
        () => {
          return coreBankingTransactions.slice(counter++);
        }
      );
      (depositRepository.makeDeposit as jest.Mock).mockResolvedValue({
        id: 'deposit_id'
      });
      (depositRepository.makeWithdrawal as jest.Mock).mockResolvedValue({
        id: 'withdrawal_id'
      });
      (depositRepository.reverseTransaction as jest.Mock).mockResolvedValue(
        undefined
      );

      (isFeatureEnabled as jest.Mock).mockResolvedValueOnce(true);
      await transactionReversalService.reverseTransactionsByInstructionId(
        '1',
        'test',
        {
          ...savedTransaction,
          coreBankingTransactions: [
            {
              id: '3',
              type: TransferJourneyStatusEnum.FEE_AMOUNT_DEBITED
            },
            {
              id: '2',
              type: TransferJourneyStatusEnum.AMOUNT_DEBITED
            },
            {
              id: '1',
              type: TransferJourneyStatusEnum.AMOUNT_CREDITED
            }
          ],
          coreBankingTransactionIds: ['1', '2', '3'],
          debitTransactionCodeMapping: [
            {
              interchange: BankNetworkEnum.INTERNAL,
              code: savedTransaction.debitTransactionCode,
              awardGroupCounter: 'Bonus_Transfer',
              limitGroupCode: 'L004',
              feeRules: 'feeRules1',
              minAmountPerTx: 1,
              maxAmountPerTx: 100000,
              defaultCategoryCode: 'C057',
              channel: savedTransaction.debitTransactionChannel
            }
          ],
          creditTransactionCodeMapping: [
            {
              interchange: BankNetworkEnum.ARTAJASA,
              code: savedTransaction.creditTransactionCode,
              awardGroupCounter: 'Bonus_Transfer',
              limitGroupCode: 'L004',
              feeRules: 'feeRules1',
              minAmountPerTx: 1,
              maxAmountPerTx: 100000,
              defaultCategoryCode: 'C056',
              channel: savedTransaction.creditTransactionChannel
            }
          ],
          id: '1'
        }
      );
      expect(
        depositRepository.getTransactionsByInstructionId
      ).toHaveBeenCalledWith('1', savedTransaction.createdAt);
      expect(depositRepository.makeDeposit).toBeCalledTimes(2);
      expect(depositRepository.makeWithdrawal).toBeCalledTimes(1);
      expect(depositRepository.reverseTransaction).toBeCalledTimes(0);
    });

    it('should query and inverse transactions until complete instead of adjust with credit tc undefined', async () => {
      const coreBankingTransactions = [
        {
          id: '1',
          type: CoreBankTransactionType.DEPOSIT
        },
        {
          id: '2',
          type: CoreBankTransactionType.WITHDRAWAL
        }
      ];
      let counter = 0;
      (depositRepository.getTransactionsByInstructionId as jest.Mock).mockImplementation(
        () => {
          return coreBankingTransactions.slice(counter++);
        }
      );
      (depositRepository.makeDeposit as jest.Mock).mockResolvedValueOnce({
        id: 'deposit_id'
      });
      (depositRepository.makeWithdrawal as jest.Mock).mockResolvedValueOnce({
        id: 'withdrawal_id'
      });
      (isFeatureEnabled as jest.Mock).mockResolvedValueOnce(true);
      await transactionReversalService.reverseTransactionsByInstructionId(
        '1',
        'test',
        {
          ...savedTransaction,
          creditTransactionChannel: undefined,
          creditTransactionCode: undefined,
          coreBankingTransactions: [
            {
              id: '2',
              type: TransferJourneyStatusEnum.AMOUNT_DEBITED
            },
            {
              id: '1',
              type: TransferJourneyStatusEnum.AMOUNT_CREDITED
            }
          ],
          coreBankingTransactionIds: ['1', '2'],
          id: '1'
        }
      );
      expect(
        depositRepository.getTransactionsByInstructionId
      ).toHaveBeenCalledWith('1', savedTransaction.createdAt);
      expect(depositRepository.makeDeposit).toBeCalledTimes(1);
      expect(depositRepository.makeWithdrawal).toBeCalledTimes(1);
      expect(depositRepository.reverseTransaction).toBeCalledTimes(0);
    });

    it('should query and reverse transactions until complete', async () => {
      const coreBankingTransactions = [
        {
          id: '3',
          adjustmentTransactionKey: '1',
          type: CoreBankTransactionType.WITHDRAWAL
        },
        {
          id: '2',
          type: CoreBankTransactionType.WITHDRAWAL
        },
        {
          id: '1',
          type: CoreBankTransactionType.DEPOSIT
        }
      ];
      let counter = 0;
      (depositRepository.getTransactionsByInstructionId as jest.Mock).mockImplementation(
        () => {
          return coreBankingTransactions.slice(counter++);
        }
      );
      (depositRepository.makeDeposit as jest.Mock).mockResolvedValueOnce({
        id: 'deposit_id'
      });
      (depositRepository.makeWithdrawal as jest.Mock).mockResolvedValueOnce({
        id: 'withdrawal_id'
      });
      (depositRepository.reverseTransaction as jest.Mock).mockResolvedValue(
        undefined
      );

      (isFeatureEnabled as jest.Mock).mockResolvedValueOnce(true);
      await transactionReversalService.reverseTransactionsByInstructionId(
        '1',
        'test',
        {
          ...savedTransaction,
          coreBankingTransactions: [
            {
              id: '2',
              type: TransferJourneyStatusEnum.AMOUNT_DEBITED
            },
            {
              id: '1',
              type: TransferJourneyStatusEnum.AMOUNT_CREDITED
            }
          ],
          coreBankingTransactionIds: ['1', '2'],
          id: '1'
        }
      );
      expect(
        depositRepository.getTransactionsByInstructionId
      ).toHaveBeenCalledWith('1', savedTransaction.createdAt);
      expect(depositRepository.makeDeposit).toBeCalledTimes(1);
      expect(depositRepository.makeWithdrawal).toBeCalledTimes(1);
      expect(depositRepository.reverseTransaction).toBeCalledTimes(0);
    });

    it('should not call mambu api to get transaction ids', async () => {
      const coreBankingTransaction1 = {
        id: '1',
        adjustmentTransactionKey: undefined
      };
      const coreBankingTransaction2 = {
        id: '2',
        adjustmentTransactionKey: '2'
      };
      (depositRepository.getTransactionsByInstructionId as jest.Mock).mockResolvedValue(
        [coreBankingTransaction1, coreBankingTransaction2]
      );
      await transactionReversalService.reverseTransactionsByInstructionId(
        '1',
        undefined,
        {
          ...savedTransaction,
          id: '1'
        }
      );
      expect(
        depositRepository.getTransactionsByInstructionId
      ).toHaveBeenCalled();
      expect(depositRepository.reverseTransaction).toBeCalledTimes(1);
    });

    it('should call mambu api to get transaction ids', async () => {
      const transaction = getTransactionWithCoreBankingTransactionIds();
      transaction.coreBankingTransactionIds = [];

      const coreBankingTransactions = [
        {
          id: '1',
          adjustmentTransactionKey: '1'
        },
        {
          id: '2'
        },
        {
          id: '3'
        }
      ];
      let counter = 0;
      (depositRepository.getTransactionsByInstructionId as jest.Mock).mockImplementation(
        () => {
          return coreBankingTransactions.slice(counter++);
        }
      );

      await transactionReversalService.reverseTransactionsByInstructionId(
        '1',
        undefined,
        {
          ...savedTransaction,
          id: '1'
        }
      );

      expect(depositRepository.getTransactionsByInstructionId).toBeCalledWith(
        '1',
        savedTransaction.createdAt
      );
      expect(depositRepository.reverseTransaction).toBeCalledTimes(2);
    });

    it('should get error if fetch core banking ids failed', async () => {
      const error = await transactionReversalService
        .reverseTransactionsByInstructionId('1')
        .catch(error => error);

      await expect(error.detail).toEqual(
        'Creation date not found for transactions: 1'
      );
      await expect(error.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
      await expect(error.errorCode).toEqual(
        ERROR_CODE.ERROR_WHILE_FETCHING_COREBANKING_ID
      );
    });
  });

  describe('reverseLoanTransaction', () => {
    it('should throw TRANSACTION_NOT_FOUND error if transaction is not defined', async () => {
      const transactionModel = {
        ...getTransactionWithCoreBankingTransactionIds(),
        coreBankingTransactions: undefined
      };

      const error = await transactionReversalService
        .reverseLoanTransaction(transactionModel.id)
        .catch(error => error);

      await expect(error.detail).toEqual('Transaction not provided!');
      await expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      await expect(error.errorCode).toEqual(ERROR_CODE.TRANSACTION_NOT_FOUND);
    });

    it('should execute nothing if there is no loan core banking transactions data', async () => {
      const reversalReason = 'test';
      const transactionModel = {
        ...getTransactionWithCoreBankingTransactionIds(),
        coreBankingTransactions: undefined
      };

      await transactionReversalService.reverseLoanTransaction(
        transactionModel.id,
        reversalReason,
        transactionModel
      );

      expect(loanRepository.adjustLoanTransaction).not.toBeCalled();
    });

    it('should execute nothing for GL_TRANSACTION_SUCCESS transaction', async () => {
      const reversalReason = 'test';
      const transactionModel = {
        ...getTransactionWithCoreBankingTransactionIds(),
        coreBankingTransactions: [
          {
            type: TransferJourneyStatusEnum.GL_TRANSACTION_SUCCESS,
            id: '1'
          }
        ]
      };

      await transactionReversalService.reverseLoanTransaction(
        transactionModel.id,
        reversalReason,
        transactionModel
      );

      expect(loanRepository.adjustLoanTransaction).not.toBeCalled();
    });

    it('should add reversal notes if defined at disbursement transaction', async () => {
      const reversalReason = 'test';
      const transactionModel = {
        ...getTransactionWithCoreBankingTransactionIds(),
        coreBankingTransactions: [
          {
            type: TransferJourneyStatusEnum.AMOUNT_DISBURSED,
            id: '1'
          }
        ]
      };

      await transactionReversalService.reverseLoanTransaction(
        transactionModel.id,
        reversalReason,
        transactionModel
      );

      expect(loanRepository.adjustLoanTransaction).toBeCalledWith(
        transactionModel.sourceAccountNo,
        expect.objectContaining({
          notes: reversalReason
        })
      );
    });

    it('should add reversal notes if defined at repayment transaction', async () => {
      const reversalReason = 'test';
      const transactionModel = {
        ...getTransactionWithCoreBankingTransactionIds(),
        coreBankingTransactions: [
          {
            type: TransferJourneyStatusEnum.AMOUNT_REPAID,
            id: '1'
          },
          {
            type: TransferJourneyStatusEnum.AMOUNT_CREDITED,
            id: '2'
          }
        ]
      };

      await transactionReversalService.reverseLoanTransaction(
        transactionModel.id,
        reversalReason,
        transactionModel
      );

      expect(loanRepository.adjustLoanTransaction).toBeCalledWith(
        transactionModel.beneficiaryAccountNo,
        expect.objectContaining({
          notes: reversalReason
        })
      );

      expect(depositRepository.reverseTransaction).toBeCalledWith(
        '2',
        expect.objectContaining({
          notes: reversalReason
        })
      );
    });
  });
});
