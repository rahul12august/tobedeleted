import { BankChannelEnum } from '@dk/module-common';
import { ERROR_CODE } from '../../common/errors';
import { TransactionStatus } from '../transaction.enum';
import transactionRepository from '../transaction.repository';
import transactionHistoryRepository from '../../transactionHistory/transactionHistory.repository';
import transactionService from '../transaction.service';
import switchingTransactionService, {
  responseMessageToPaymentInstruction
} from '../transactionConfirmation.service';
import transactionExecutionService from '../transactionExecution.service';
import txnManualAdjustmentService from '../transactionManualAdjustment.service';
import altoService from '../../alto/alto.service';
import { getTransactionModelData } from '../__mocks__/transaction.data';
import { mockQrisCheckStatusResponse } from '../../alto/__mocks__/alto.data';
import altoUtil from '../../alto/alto.utils';

jest.mock('../transactionExecution.service');
jest.mock('../transaction.repository');
jest.mock('../transaction.service');
jest.mock('../transactionConfirmation.service');
jest.mock('../../alto/alto.service');
jest.mock('../../alto/alto.utils');
jest.mock('../../transactionHistory/transactionHistory.repository');

describe('manualAdjstment', () => {
  describe('manualAdjustment: EXTERNAL transaction', () => {
    beforeEach(() => {
      jest.resetAllMocks();
    });

    it('should throw invalid transaction status error when status is already SUCCEED', async () => {
      // Given
      const txnId = 'dummy123';
      const resTxn = {
        ...getTransactionModelData(),
        status: TransactionStatus.SUCCEED,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        id: txnId
      };
      try {
        // When
        await txnManualAdjustmentService.extTxnManualAdjstment(
          resTxn,
          TransactionStatus.SUCCEED
        );
      } catch (error) {
        // Then
        expect(error.errorCode).toBe(ERROR_CODE.INVALID_TRANSACTION_STATUS);
      }
    });

    it('should update SUBMITTED txn to DECLINED', async () => {
      // Given
      const txnId = 'dummy123';
      const resTxn = {
        ...getTransactionModelData(),
        status: TransactionStatus.SUBMITTED,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        id: txnId,
        paymentInstructionExecution: '123'
      };

      (switchingTransactionService.reverseTransaction as jest.Mock).mockResolvedValueOnce(
        {}
      );
      (transactionService.findByIdAndUpdate as jest.Mock).mockResolvedValueOnce(
        { ...resTxn, status: TransactionStatus.DECLINED }
      );
      // When
      await txnManualAdjustmentService.extTxnManualAdjstment(
        resTxn,
        TransactionStatus.DECLINED
      );

      // Then
      expect(switchingTransactionService.reverseTransaction).toBeCalledWith(
        expect.objectContaining({
          externalId: resTxn.externalId,
          responseCode: 400
        }),
        expect.objectContaining({
          id: txnId
        })
      );
      expect(transactionService.findByIdAndUpdate).toBeCalledWith(txnId, {
        manuallyAdjusted: true
      });
      expect(responseMessageToPaymentInstruction).toBeCalledTimes(1);
    });

    it('should update PENDING txn to DECLINED', async () => {
      // Given
      const txnId = 'dummy123';
      const resTxn = {
        ...getTransactionModelData(),
        status: TransactionStatus.PENDING,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        id: txnId
      };

      (switchingTransactionService.reverseTransaction as jest.Mock).mockResolvedValueOnce(
        {}
      );
      (transactionService.findByIdAndUpdate as jest.Mock).mockResolvedValueOnce(
        { ...resTxn, status: TransactionStatus.DECLINED }
      );
      // When
      await txnManualAdjustmentService.extTxnManualAdjstment(
        resTxn,
        TransactionStatus.DECLINED
      );

      // Then
      expect(switchingTransactionService.reverseTransaction).toBeCalledWith(
        expect.objectContaining({
          externalId: resTxn.externalId,
          responseCode: 400
        }),
        expect.objectContaining({
          id: txnId
        })
      );
      expect(transactionService.findByIdAndUpdate).toBeCalledWith(txnId, {
        manuallyAdjusted: true
      });
    });

    it('should update SUBMITTED txn to SUCCEED', async () => {
      // Given
      const txnId = 'dummy123';
      const resTxn = {
        ...getTransactionModelData(),
        status: TransactionStatus.SUBMITTED,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        id: txnId,
        paymentInstructionExecution: '123'
      };

      (switchingTransactionService.extractChangeFromConfirmTransactionRequest as jest.Mock).mockReturnValue(
        {
          categoryCode: 'C056'
        }
      );
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...resTxn,
        status: TransactionStatus.SUCCEED
      });
      (transactionExecutionService.settleTransaction as jest.Mock).mockResolvedValueOnce(
        { paymentInstructionExecution: '123' }
      );
      (transactionService.findByIdAndUpdate as jest.Mock).mockResolvedValueOnce(
        { ...resTxn, status: TransactionStatus.SUCCEED }
      );
      // When
      await txnManualAdjustmentService.extTxnManualAdjstment(
        resTxn,
        TransactionStatus.SUCCEED
      );

      // Then
      expect(
        switchingTransactionService.extractChangeFromConfirmTransactionRequest
      ).toBeCalledWith(
        expect.objectContaining({
          externalId: resTxn.externalId,
          responseCode: 200
        }),
        expect.objectContaining({
          id: txnId
        }),
        false
      );
      expect(transactionExecutionService.settleTransaction).toBeCalledTimes(1);
      expect(responseMessageToPaymentInstruction).toBeCalledTimes(1);
      expect(transactionService.findByIdAndUpdate).toBeCalledWith(txnId, {
        manuallyAdjusted: true
      });
    });

    it('should update PENDING txn to SUCCEED', async () => {
      // Given
      const txnId = 'dummy123';
      const resTxn = {
        ...getTransactionModelData(),
        status: TransactionStatus.PENDING,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        id: txnId
      };

      (switchingTransactionService.extractChangeFromConfirmTransactionRequest as jest.Mock).mockReturnValue(
        {
          categoryCode: 'C056'
        }
      );
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...resTxn,
        status: TransactionStatus.SUCCEED
      });
      (transactionExecutionService.settleTransaction as jest.Mock).mockResolvedValueOnce(
        {}
      );
      (transactionService.findByIdAndUpdate as jest.Mock).mockResolvedValueOnce(
        { ...resTxn, status: TransactionStatus.SUCCEED }
      );
      // When
      await txnManualAdjustmentService.extTxnManualAdjstment(
        resTxn,
        TransactionStatus.SUCCEED
      );

      // Then
      expect(
        switchingTransactionService.extractChangeFromConfirmTransactionRequest
      ).toBeCalledWith(
        expect.objectContaining({
          externalId: resTxn.externalId,
          responseCode: 200
        }),
        expect.objectContaining({
          id: txnId
        }),
        false
      );
      expect(transactionExecutionService.settleTransaction).toBeCalledTimes(1);
      expect(transactionService.findByIdAndUpdate).toBeCalledWith(txnId, {
        manuallyAdjusted: true
      });
    });

    it('should update DECLINED txn to SUCCEED', async () => {
      // Given
      const txnId = 'dummy123';
      const resTxn = {
        ...getTransactionModelData(),
        status: TransactionStatus.DECLINED,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        id: txnId
      };

      (switchingTransactionService.extractChangeFromConfirmTransactionRequest as jest.Mock).mockReturnValue(
        {
          categoryCode: 'C056'
        }
      );
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...resTxn,
        status: TransactionStatus.SUCCEED
      });
      (transactionExecutionService.settleTransaction as jest.Mock).mockResolvedValueOnce(
        {}
      );
      (transactionService.findByIdAndUpdate as jest.Mock).mockResolvedValueOnce(
        { ...resTxn, status: TransactionStatus.SUCCEED }
      );
      // When
      await txnManualAdjustmentService.extTxnManualAdjstment(
        resTxn,
        TransactionStatus.SUCCEED
      );

      // Then
      expect(
        switchingTransactionService.extractChangeFromConfirmTransactionRequest
      ).toBeCalledWith(
        expect.objectContaining({
          externalId: resTxn.externalId,
          responseCode: 200
        }),
        expect.objectContaining({
          id: txnId
        }),
        false
      );
      expect(transactionExecutionService.settleTransaction).toBeCalledTimes(1);
      expect(transactionService.findByIdAndUpdate).toBeCalledWith(txnId, {
        manuallyAdjusted: true
      });
    });

    it('should update QRIS transaction from SUBMITTED to SUCCEED', async () => {
      // Given
      const txnId = 'dummy123';
      const resTxn = {
        ...getTransactionModelData(),
        coreBankingTransactionIds: ['123'],
        status: TransactionStatus.SUBMITTED,
        beneficiaryBankCodeChannel: BankChannelEnum.QRIS,
        id: txnId
      };

      (switchingTransactionService.extractChangeFromConfirmTransactionRequest as jest.Mock).mockReturnValue(
        {
          categoryCode: 'C056'
        }
      );
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...resTxn,
        status: TransactionStatus.SUCCEED
      });
      (transactionService.getTransaction as jest.Mock).mockResolvedValueOnce({
        ...resTxn,
        status: TransactionStatus.SUCCEED
      });
      (altoService.checkStatus as jest.Mock).mockResolvedValueOnce(
        mockQrisCheckStatusResponse()
      );
      (transactionExecutionService.settleTransaction as jest.Mock).mockResolvedValueOnce(
        {}
      );
      (transactionHistoryRepository.updateTransactionHistory as jest.Mock).mockResolvedValueOnce(
        {}
      );
      (transactionService.findByIdAndUpdate as jest.Mock).mockResolvedValueOnce(
        { ...resTxn, status: TransactionStatus.SUCCEED }
      );
      // When
      await txnManualAdjustmentService.extTxnManualAdjstment(
        resTxn,
        TransactionStatus.SUCCEED
      );

      // Then
      expect(
        switchingTransactionService.extractChangeFromConfirmTransactionRequest
      ).toBeCalledWith(
        expect.objectContaining({
          externalId: resTxn.externalId,
          responseCode: 200
        }),
        expect.objectContaining({
          id: txnId
        }),
        false
      );
      expect(transactionExecutionService.settleTransaction).toBeCalledTimes(1);
      expect(
        transactionHistoryRepository.updateTransactionHistory
      ).toBeCalledTimes(1);
      expect(transactionService.findByIdAndUpdate).toBeCalledWith(txnId, {
        manuallyAdjusted: true
      });
      expect(
        altoUtil.updateTransactionDataOnQrisPaymentSuccess
      ).toBeCalledTimes(1);
      expect(altoUtil.updateTransactionDataOnQrisPaymentSuccess).toBeCalledWith(
        expect.objectContaining({
          status: 'SUCCEED'
        }),
        expect.objectContaining({
          transactionStatus: 'Success'
        })
      );
    });

    it('should update QRIS transaction from SUBMITTED to DECLINED', async () => {
      // Given
      const txnId = 'dummy123';
      const resTxn = {
        ...getTransactionModelData(),
        coreBankingTransactionIds: ['123'],
        status: TransactionStatus.SUBMITTED,
        beneficiaryBankCodeChannel: BankChannelEnum.QRIS,
        id: txnId
      };

      (switchingTransactionService.extractChangeFromConfirmTransactionRequest as jest.Mock).mockReturnValue(
        {
          categoryCode: 'C056'
        }
      );
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...resTxn,
        status: TransactionStatus.DECLINED
      });
      (transactionHistoryRepository.updateTransactionHistory as jest.Mock).mockResolvedValueOnce(
        {}
      );
      (transactionService.findByIdAndUpdate as jest.Mock).mockResolvedValueOnce(
        { ...resTxn, status: TransactionStatus.SUCCEED }
      );
      // When
      await txnManualAdjustmentService.extTxnManualAdjstment(
        resTxn,
        TransactionStatus.DECLINED
      );

      expect(altoService.markQrisTxnDeclineManually).toBeCalledTimes(1);
      expect(
        transactionHistoryRepository.updateTransactionHistory
      ).toBeCalledTimes(1);
      expect(transactionService.findByIdAndUpdate).toBeCalledWith(txnId, {
        manuallyAdjusted: true
      });
    });

    it('failed to update transaction data', async () => {
      // Given
      const txnId = 'dummy123';
      const resTxn = {
        ...getTransactionModelData(),
        coreBankingTransactionIds: ['123'],
        status: TransactionStatus.SUBMITTED,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        id: txnId
      };

      (switchingTransactionService.extractChangeFromConfirmTransactionRequest as jest.Mock).mockReturnValue(
        {
          categoryCode: 'C056'
        }
      );
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (transactionService.findByIdAndUpdate as jest.Mock).mockResolvedValueOnce(
        { ...resTxn, status: TransactionStatus.SUCCEED }
      );
      // When
      var err;
      try {
        await txnManualAdjustmentService.extTxnManualAdjstment(
          resTxn,
          TransactionStatus.SUCCEED
        );
      } catch (error) {
        err = error;
      }

      // Then
      expect(
        switchingTransactionService.extractChangeFromConfirmTransactionRequest
      ).toBeCalledWith(
        expect.objectContaining({
          externalId: resTxn.externalId,
          responseCode: 200
        }),
        expect.objectContaining({
          id: txnId
        }),
        false
      );
      expect(transactionExecutionService.settleTransaction).toBeCalledTimes(0);
      expect(
        transactionHistoryRepository.updateTransactionHistory
      ).toBeCalledTimes(0);
      expect(transactionService.findByIdAndUpdate).toBeCalledWith(txnId, {
        manuallyAdjusted: true
      });
      expect(err.errorCode).toEqual(ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION);
    });
  });
});
