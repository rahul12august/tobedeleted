import moment from 'moment';
import {
  createForwardPaymentTransactionInput,
  getITransactionInput,
  getNNSMappingData,
  getRecommendedService,
  getTransactionDetail,
  getRecommendedServiceWithRequireThirdPartyOutgoingId,
  getTransactionMockValueObject,
  getTransactionModelFullData,
  recommendedServicesWithAwardGroup
} from '../__mocks__/transaction.data';
import {
  RetryableTransferAppError,
  TransferAppError
} from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import {
  BankCodeTypes,
  CoreBankTransactionType,
  ExecutionTypeEnum,
  GlTransactionTypes,
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from '../transaction.enum';
import {
  AccountType,
  BankChannelEnum,
  BankNetworkEnum,
  GeneralLedgerCode,
  Http,
  PaymentServiceTypeEnum,
  qrisEncryption,
  Rail,
  Util
} from '@dk/module-common';
import {
  ITransactionExecutionHooks,
  ITransactionInput,
  ITransactionStatusTemplate,
  ITransactionSubmissionTemplate
} from '../transaction.type';
import { generateOutgoingId } from '../../common/externalIdGenerator.util';
import transactionExecutionService from '../transactionExecution.service';
import transactionRepository from '../transaction.repository';
import transactionProducer from '../transaction.producer';
import monitoringEventProducer from '../../monitoringEvent/monitoringEvent.producer';
import awardRepository from '../../award/award.repository';
import configurationRepository from '../../configuration/configuration.repository';
import depositRepository from '../../coreBanking/deposit.repository';
import transactionHelper from '../transaction.helper';
import transactionBlockingHelper from '../transactionBlocking.helper';
import transactionSubmissionSwitchingTemplate from '../transactionSubmissionSwitchingTemplate.service';
import transactionRecommendService from '../transactionRecommend.service';
import { ITransactionModel } from '../transaction.model';
import { NewEntity } from '../../common/common.type';
import { mapTransferAppError } from '../../common/common.util';
import loanRepository from '../../coreBanking/loan.repository';
import { BANK_INCOME } from '../transaction.constant';
import { AmountTypeEnum } from '../../coreBanking/loan.enum';
import { getGeneralLedgersMockData } from '../../configuration/__mocks__/configuration.data';
import altoRepository from '../../alto/alto.repository';
import {
  getQrisWithdrawMambuResponse,
  mockSuccessQrisPaymentResponse
} from '../../alto/__mocks__/alto.data';
import {
  AltoAccountTypeEnum,
  QrisTransactionStatusEnum
} from '../../alto/alto.enum';
import outgoingStatusTemplateService from '../transactionStatus.template';
import transactionReversalHelper from '../transactionReversal.helper';
import { QrisError } from '../../alto/alto.type';
import { IGeneralLedger } from '../../configuration/configuration.type';
import { IGLJournalEntriesV2 } from '../../coreBanking/deposit.interface';
import { get } from 'lodash';
import { UTC_OFFSET_WESTERN_INDONESIAN } from '../../common/constant';
import coreBankingConstant from '../../coreBanking/coreBanking.constant';
import { MonitoringEventType } from '@dk/module-message';
import entitlementFlag from '../../common/entitlementFlag';
import { getTransactionInfoTracker } from '../../common/contextHandler';
import {
  mapToGlIncomeTransaction,
  mapToRepaymentTransaction
} from '../transaction.util';

import transactionUsageCommonService from '../../transactionUsage/transactionUsageCommon.service';
import transactionUsageService from '../../transactionUsage/transactionUsage.service';
import { isSuspectedAbuse } from '../../abuseDetection/abuseDetectionService';
import transactionService from '../transaction.service';

import transactionHistoryRepository from '../../transactionHistory/transactionHistory.repository';
import { PatchOperation } from '../../coreBanking/deposit.enum';

jest.mock('../transaction.repository');
jest.mock('../../configuration/configuration.repository');
jest.mock('../../transaction/transaction.producer');
jest.mock('../../monitoringEvent/monitoringEvent.producer');
jest.mock('../../award/award.repository');
jest.mock('../../coreBanking/deposit.repository');
jest.mock('../../coreBanking/loan.repository');
jest.mock('../../logger');
jest.mock('../transactionSwitching.helper');
jest.mock('../../common/externalIdGenerator.util');
jest.mock('../transactionBlocking.helper');
jest.mock('../transactionSubmissionSwitchingTemplate.service', () => {
  const original = require.requireActual(
    '../transactionSubmissionSwitchingTemplate.service'
  ).default;
  return {
    ...original,
    submitTransaction: jest.fn()
  };
});
jest.mock('../transactionStatus.template', () => {
  const original = require.requireActual(
    '../../bifast/bifastStatus.template.ts'
  ).default;
  return {
    ...original,
    getTransactionStatus: jest.fn()
  };
});
jest.mock('../transactionRecommend.service');
jest.mock('../../alto/alto.repository');
jest.mock('../../common/contextHandler', () => {
  return {
    getTransactionInfoTracker: jest.fn().mockResolvedValue([
      {
        counterCode: 'counterCode',
        entitlementCodeRefId: 'entitlementCodeRefId',
        customerId: 'customerId'
      }
    ]),
    setFnProgressTracker: jest.fn(),
    getFnProgressTracker: jest.fn(),
    getRetryCount: jest.fn(),
    setRetryCount: jest.fn(),
    getRequestId: jest.fn(),
    updateRequestIdInContext: jest.fn(),
    updateRequestIdInRedis: jest.fn()
  };
});

jest.mock('../../transactionUsage/transactionUsage.service');

jest.mock('../../transactionHistory/transactionHistory.repository');
jest.mock('../../abuseDetection/abuseDetectionService');
jest.mock('../transaction.service');

describe('executeTransaction', () => {
  class NetworkError extends Error {
    status: number;
    code?: string;
    constructor(status = 501, code?: string) {
      super();
      this.status = status;
      this.code = code;
    }
  }

  const {
    sourceBankCode,
    beneficiaryBankCode,
    transactionCategory,
    sourceAccount,
    beneficiaryAccount,
    externalBankCode
  } = getTransactionMockValueObject();

  const coreBankingWithdrawalId = '1';
  const coreBankingDepositId = '1';
  const coreBankingGlJournalEntriesId = '1';
  const coreBankingDisbursementId = '1';
  const coreBankingRepaymentId = '1';
  const transactionId = 'any_id';

  // FIXME need to be review if this is correct
  const transactionInput = transactionHelper.populateTransactionInput(
    {
      ...getITransactionInput(),
      categoryCode: transactionCategory.code,
      sourceCustomerId: 'any_customer_id', // this is come from request parameter
      sourceAccountInfo: sourceAccount,
      beneficiaryAccountInfo: beneficiaryAccount,
      sourceBankCodeInfo: sourceBankCode,
      beneficiaryBankCodeInfo: beneficiaryBankCode,
      transactionId
    },
    sourceAccount,
    sourceBankCode,
    beneficiaryAccount,
    beneficiaryBankCode,
    transactionId
  );

  const recommendService = getRecommendedService();
  const recommendServiceWithRequireThirdPartyOutgoingId = getRecommendedServiceWithRequireThirdPartyOutgoingId();
  const basicFee = recommendService.feeData[0];
  const idempotencyKey = '1';

  let mockedRetry;

  beforeEach(() => {
    (configurationRepository.getBankCodeMapping as jest.Mock).mockImplementation(
      code => {
        if (code === sourceBankCode.bankCodeId) {
          return sourceBankCode;
        }
        if (code === beneficiaryBankCode.bankCodeId) {
          return beneficiaryBankCode;
        }
        if (code === externalBankCode.bankCodeId) {
          return externalBankCode;
        }
        return;
      }
    );

    (configurationRepository.getTransactionCategory as jest.Mock).mockImplementation(
      code => {
        if (code === transactionCategory.code) {
          return transactionCategory;
        }
        return;
      }
    );

    (transactionRepository.update as jest.Mock).mockImplementation(
      (id, model) => {
        return {
          ...model,
          id: id
        };
      }
    );

    (transactionRepository.create as jest.Mock).mockImplementation(model => {
      return {
        ...model,
        id: transactionId
      };
    });

    (transactionRepository.findByIdAndUpdate as jest.Mock).mockImplementation(
      (id, model) => {
        return {
          ...model,
          id: id
        };
      }
    );

    (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValue({});

    (transactionProducer.sendTransactionSucceededMessage as jest.Mock).mockResolvedValue(
      {}
    );

    (transactionProducer.sendTransactionFailedMessage as jest.Mock).mockResolvedValue(
      {}
    );

    (monitoringEventProducer.sendMonitoringEventMessage as jest.Mock).mockResolvedValue(
      {}
    );

    (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValue(
      999999999
    );

    (depositRepository.makeWithdrawal as jest.Mock).mockResolvedValue({
      id: coreBankingWithdrawalId
    });

    (depositRepository.makeDeposit as jest.Mock).mockResolvedValue({
      id: coreBankingDepositId
    });

    (depositRepository.createGlJournalEntries as jest.Mock).mockResolvedValue({
      id: coreBankingGlJournalEntriesId
    });

    (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue({
      id: coreBankingDisbursementId
    });

    (loanRepository.makeRepayment as jest.Mock).mockResolvedValue({
      id: coreBankingRepaymentId
    });

    (loanRepository.adjustLoanTransaction as jest.Mock).mockResolvedValue({});

    Http.generateIdempotencyKey = jest.fn(() => idempotencyKey);

    mockedRetry = jest
      .spyOn(Util, 'retry')
      .mockImplementation(async (_, fn, ...args) => await fn(...args));

    (altoRepository.submitTransaction as jest.Mock).mockResolvedValue(
      mockSuccessQrisPaymentResponse()
    );
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('executeTransaction succeed', () => {
    it('should execute transaction successfully', async () => {
      // Given
      const usageCounterPayload = [
        {
          limitGroupCode: 'd01',
          adjustmentLimitAmount: transactionInput.transactionAmount
        }
      ];
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          debitTransactionCode: [
            {
              ...recommendService.debitTransactionCode[0],
              limitGroupCode: 'd01'
            }
          ]
        }
      );

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce({
        customerId: sourceAccount.customerId,
        data: usageCounterPayload
      });

      // when
      const executed = await transactionExecutionService.executeTransaction(
        transactionInput
      );

      // then
      expect(executed.status).toBe(TransactionStatus.SUCCEED);
      expect(executed.processingInfo?.paymentRail).toBe(
        Rail.INTERNAL.toString()
      );

      expect(transactionRepository.create).not.toHaveBeenCalled();

      expect(transactionRepository.update).toBeCalledWith(
        transactionId,
        expect.objectContaining({
          status: TransactionStatus.SUCCEED
        })
      );

      expect(awardRepository.updateUsageCounters).toHaveBeenCalledTimes(1);
      expect(awardRepository.updateUsageCounters).toBeCalledWith(
        sourceAccount.customerId,
        usageCounterPayload
      );
      expect(transactionProducer.sendTransactionSucceededMessage).toBeCalled();
      expect(monitoringEventProducer.sendMonitoringEventMessage).toBeCalledWith(
        MonitoringEventType.PROCESSING_FINISHED,
        expect.objectContaining({
          id: transactionId,
          status: TransactionStatus.SUCCEED
        })
      );
      expect(transactionRepository.update).toBeCalledTimes(2);
    });

    it('should execute transaction successfully for Main pocket to GL transfer', async () => {
      // Given
      let localTransactionInput = JSON.parse(JSON.stringify(transactionInput));
      localTransactionInput.paymentServiceType =
        PaymentServiceTypeEnum.MAIN_POCKET_TO_GL_TRANSFER;
      localTransactionInput.additionalPayload = {
        bankIncomeGlNumber: '900900001.00.00',
        repaymentGlNumber: '900900001.00.00'
      };
      const usageCounterPayload = [
        {
          limitGroupCode: 'd01',
          adjustmentLimitAmount: localTransactionInput.transactionAmount
        }
      ];
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          debitTransactionCode: [
            {
              // @ts-ignore
              ...recommendService.debitTransactionCode[0],
              limitGroupCode: 'd01'
            }
          ]
        }
      );

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce({
        customerId: sourceAccount.customerId,
        data: usageCounterPayload
      });

      // when
      const executed = await transactionExecutionService.executeTransaction(
        localTransactionInput
      );

      // then
      expect(executed.status).toBe(TransactionStatus.SUCCEED);
      expect(executed.processingInfo?.paymentRail).toBe(
        Rail.INTERNAL.toString()
      );

      expect(transactionRepository.create).not.toHaveBeenCalled();

      expect(transactionRepository.update).toBeCalledWith(
        transactionId,
        expect.objectContaining({
          status: TransactionStatus.SUCCEED
        })
      );

      expect(awardRepository.updateUsageCounters).toHaveBeenCalledTimes(1);
      expect(awardRepository.updateUsageCounters).toBeCalledWith(
        sourceAccount.customerId,
        usageCounterPayload
      );
      expect(transactionProducer.sendTransactionSucceededMessage).toBeCalled();
      expect(monitoringEventProducer.sendMonitoringEventMessage).toBeCalledWith(
        MonitoringEventType.PROCESSING_FINISHED,
        expect.objectContaining({
          id: transactionId,
          status: TransactionStatus.SUCCEED
        })
      );
      expect(transactionRepository.update).toBeCalledTimes(2);
    });

    it('should execute transaction successfully with awardGroup counter present', async () => {
      // Given
      const awardGroupCounter = 'Bonus_Transfer';
      const limitGroupCode = 'c01';
      const updateUsageCounterPayload = [
        {
          limitGroupCode: limitGroupCode,
          adjustmentLimitAmount: transactionInput.transactionAmount
        },
        {
          limitGroupCode: awardGroupCounter,
          adjustmentLimitAmount: transactionInput.transactionAmount
        }
      ];
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendedServicesWithAwardGroup[0]
      );

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce({
        customerId: sourceAccount.customerId,
        data: updateUsageCounterPayload
      });

      // when
      const executed = await transactionExecutionService.executeTransaction(
        transactionInput
      );

      // then
      expect(executed.status).toBe(TransactionStatus.SUCCEED);
      expect(executed.processingInfo?.paymentRail).toBe(
        Rail.INTERNAL.toString()
      );

      expect(transactionRepository.update).toHaveBeenNthCalledWith(
        1,
        transactionId,
        expect.objectContaining({
          status: TransactionStatus.PENDING
        })
      );

      expect(transactionRepository.update).toBeCalledWith(
        transactionId,
        expect.objectContaining({
          status: TransactionStatus.SUCCEED
        })
      );

      expect(awardRepository.updateUsageCounters).toHaveBeenCalledTimes(1);
      expect(awardRepository.updateUsageCounters).toBeCalledWith(
        sourceAccount.customerId,
        updateUsageCounterPayload
      );

      expect(transactionProducer.sendTransactionSucceededMessage).toBeCalled();
      expect(monitoringEventProducer.sendMonitoringEventMessage).toBeCalledWith(
        MonitoringEventType.PROCESSING_FINISHED,
        expect.objectContaining({
          id: transactionId,
          status: TransactionStatus.SUCCEED
        })
      );
    });

    it('should generate external outgoingId if transaction is outgoing transaction', async () => {
      // given
      const outgoingId = 'outgoingId';
      (generateOutgoingId as jest.Mock).mockResolvedValueOnce(outgoingId);

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          requireThirdPartyOutgoingId: true // this is outgoing transaction
        }
      );

      // setup an input to transfer money to external bank
      const input = {
        ...transactionInput,
        beneficiaryBankCode: externalBankCode.bankCodeId,
        beneficiaryBankCodeInfo: externalBankCode,
        beneficiaryBankCodeChannel: externalBankCode.channel
      };

      // when
      const executed = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(executed.status).toBe(TransactionStatus.SUBMITTED);

      // expected EURONET for beneficiaryBankCodeChannel = EXTERNAL and not empty beneficiaryAccountNo
      expect(executed.processingInfo?.paymentRail).toBe(
        Rail.EURONET.toString()
      );

      // expect store transaction with pending status
      expect(transactionRepository.update).toHaveBeenNthCalledWith(
        1,
        transactionId,
        expect.objectContaining({
          status: TransactionStatus.PENDING,
          thirdPartyOutgoingId: outgoingId
        })
      );
    });
  });

  describe('executeTransaction rejected', () => {
    const recommendService = getRecommendedService();
    // const transactionCategory = getTransactionCategory();

    it('should call execution hooks if it is defined', async () => {
      // given
      const hooks: ITransactionExecutionHooks = {
        onFailedTransaction: jest.fn().mockResolvedValue({}),
        onRecommendedServiceCode: jest.fn().mockResolvedValue({}),
        onSucceedTransaction: jest.fn().mockResolvedValue({}),
        onNewCreatedTransaction: jest.fn().mockResolvedValue({})
      };

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          debitTransactionCode: [
            {
              ...recommendService.debitTransactionCode[0],
              transactionCodeInfo: {
                ...recommendService.debitTransactionCode[0].transactionCodeInfo,
                limitGroupCode: 'd01' // to update usage counter
              }
            }
          ]
        }
      );

      // when
      await transactionExecutionService.executeTransaction(
        transactionInput,
        hooks
      );

      // then
      expect(hooks.onFailedTransaction).not.toBeCalled();
      expect(hooks.onRecommendedServiceCode).toBeCalledTimes(1);
      expect(hooks.onSucceedTransaction).toBeCalledTimes(1);
      expect(hooks.onNewCreatedTransaction).toBeCalledTimes(1);
    });
  });

  describe('executeTransaction rejected', () => {
    it('should reject transaction if category code not existed', async () => {
      // given
      (configurationRepository.getTransactionCategory as jest.Mock).mockResolvedValue(
        null
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      const error = await transactionExecutionService
        .executeTransaction(transactionInput)
        .catch(err => err);

      // then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toBe(ERROR_CODE.INVALID_REGISTER_PARAMETER);
    });

    it(`should update status to ${TransactionStatus.DECLINED} when submission failed`, async () => {
      // given
      // fail submission
      (depositRepository.makeDeposit as jest.Mock).mockRejectedValue({});
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      const error = await transactionExecutionService
        .executeTransaction(transactionInput)
        .catch(e => e);

      // then
      expect(error).toBeDefined(); // TODO need to check if we need to transform the exception ehre

      // expect update status to declined when failed
      expect(transactionRepository.update).toBeCalledWith(
        transactionId,
        expect.objectContaining({
          status: TransactionStatus.DECLINED
        })
      );

      // expect send failed message
      expect(transactionProducer.sendTransactionFailedMessage).toBeCalled();
      expect(monitoringEventProducer.sendMonitoringEventMessage).toBeCalledWith(
        MonitoringEventType.PROCESSING_FINISHED,
        expect.objectContaining({
          id: transactionId,
          status: TransactionStatus.DECLINED,
          processingInfo: {
            paymentRail: Rail.INTERNAL.toString()
          }
        })
      );
    });
  });

  describe('executeDebit', () => {
    const transactionModel: NewEntity<ITransactionModel> = transactionHelper.buildNewTransactionModel(
      transactionInput,
      recommendService
    );
    it('should debit source and fees for transaction', async () => {
      const model: ITransactionModel = {
        ...transactionModel,
        id: transactionId
      };

      const result = await transactionExecutionService.executeDebit(model);

      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(result.journey.pop()).toEqual(
        expect.objectContaining({
          status: TransferJourneyStatusEnum.FEE_PROCESSED
        })
      );
    });
  });

  describe('executeNoneBlockingTransaction', () => {
    it('should submit transactions success with fee data is more than 0 and execute gljournalentries', async () => {
      // Given
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          feeData: [
            {
              ...recommendService.feeData[0],
              subsidiary: true // make sure journal gl existed
            }
          ]
        }
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        transactionInput
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(depositRepository.executeJournalEntriesGL).toHaveBeenCalledWith(
        {
          date: moment().format('YYYY-MM-DD'),
          debitAccount1: basicFee.debitGlNumber,
          debitAmount1: basicFee.subsidiaryAmount,
          creditAccount1: basicFee.creditGlNumber,
          creditAmount1: basicFee.subsidiaryAmount,
          transactionID: transactionId
        },
        idempotencyKey
      );
    });

    it('should submit transactions success with paymentServiceType OFFER', async () => {
      // Given
      const input = {
        ...transactionInput,
        sourceAccountNo: undefined,
        sourceAccountName: '',
        paymentServiceCode: 'OFFER',
        paymentServiceType: PaymentServiceTypeEnum.OFFER
      };

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          paymentServiceCode: 'OFFER',
          feeData: [
            {
              ...recommendService.feeData[0],
              subsidiary: true // make sure journal gl existed
            }
          ]
        }
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(depositRepository.makeDeposit).toHaveBeenCalledTimes(1);
      expect(depositRepository.makeWithdrawal).toHaveBeenCalledTimes(1);
      expect(depositRepository.executeJournalEntriesGL).toHaveBeenCalledWith(
        {
          date: moment().format('YYYY-MM-DD'),
          debitAccount1: basicFee.debitGlNumber,
          debitAmount1: basicFee.subsidiaryAmount,
          creditAccount1: basicFee.creditGlNumber,
          creditAmount1: basicFee.subsidiaryAmount,
          transactionID: transactionId
        },
        idempotencyKey
      );
    });

    it('should submit transactions success with paymentServiceType BONUS_INTEREST', async () => {
      // Given
      const input = {
        ...transactionInput,
        sourceAccountNo: undefined,
        sourceAccountName: '',
        paymentServiceCode: 'BONUS_INTEREST',
        paymentServiceType: PaymentServiceTypeEnum.BONUS_INTEREST
      };

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          paymentServiceCode: 'BONUS_INTEREST',
          feeData: [
            {
              ...recommendService.feeData[0],
              subsidiary: true // make sure journal gl existed
            }
          ]
        }
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(depositRepository.makeDeposit).toHaveBeenCalledTimes(1);
      expect(depositRepository.makeWithdrawal).toHaveBeenCalledTimes(1);
      expect(depositRepository.executeJournalEntriesGL).toHaveBeenCalledWith(
        {
          date: moment().format('YYYY-MM-DD'),
          debitAccount1: basicFee.debitGlNumber,
          debitAmount1: basicFee.subsidiaryAmount,
          creditAccount1: basicFee.creditGlNumber,
          creditAmount1: basicFee.subsidiaryAmount,
          transactionID: transactionId
        },
        idempotencyKey
      );
    });

    it('should submit transactions success with paymentServiceType PAYROLL', async () => {
      // Given
      const input = {
        ...transactionInput,
        sourceAccountNo: undefined,
        sourceAccountName: '',
        paymentServiceCode: 'PAYROLL',
        paymentServiceType: PaymentServiceTypeEnum.PAYROLL
      };

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          paymentServiceCode: 'PAYROLL',
          debitTransactionCode: [],
          feeData: []
        }
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(depositRepository.makeDeposit).toHaveBeenCalledTimes(1);
      expect(depositRepository.makeWithdrawal).not.toHaveBeenCalled();
      expect(depositRepository.executeJournalEntriesGL).not.toHaveBeenCalled();
    });

    it('should submit transactions success with paymentServiceType CASHBACK', async () => {
      // Given
      const input = {
        ...transactionInput,
        sourceAccountNo: undefined,
        sourceAccountName: '',
        paymentServiceCode: 'CASHBACK',
        paymentServiceType: PaymentServiceTypeEnum.CASHBACK
      };

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          paymentServiceCode: 'CASHBACK',
          debitTransactionCode: []
        }
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(depositRepository.makeDeposit).toHaveBeenCalledTimes(1);
      expect(depositRepository.makeWithdrawal).not.toHaveBeenCalled();
      expect(depositRepository.executeJournalEntriesGL).toHaveBeenCalledWith(
        {
          date: moment().format('YYYY-MM-DD'),
          debitAccount1: basicFee.debitGlNumber,
          debitAmount1: basicFee.subsidiaryAmount,
          creditAccount1: basicFee.creditGlNumber,
          creditAmount1: basicFee.subsidiaryAmount,
          transactionID: transactionId
        },
        idempotencyKey
      );
    });

    it('should submit transactions success with general refund fee data is more than 0 and execute gljournalentries ', async () => {
      // Given
      const input = {
        ...transactionInput,
        paymentServiceCode: 'REFUND_GENERAL',
        paymentServiceType: PaymentServiceTypeEnum.GENERAL_REFUND,
        refundFees: 3500
      };

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          paymentServiceCode: 'REFUND_GENERAL',
          feeData: [
            {
              ...recommendService.feeData[0],
              subsidiary: true, // make sure journal gl existed,
              blockingId: 'blockingId1',
              idempotencyKey: 'idempotencyKey1'
            }
          ]
        }
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(depositRepository.executeJournalEntriesGL).toHaveBeenCalledWith(
        {
          date: moment().format('YYYY-MM-DD'),
          debitAccount1: basicFee.debitGlNumber,
          debitAmount1: basicFee.subsidiaryAmount,
          creditAccount1: basicFee.creditGlNumber,
          creditAmount1: basicFee.subsidiaryAmount,
          transactionID: transactionId
        },
        idempotencyKey
      );
    });

    it('should call 3 requests to core banking, including deposit, withdraw, and withdraw for fee if fee data is more than 0', async () => {
      // Given
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          feeData: [
            {
              ...recommendService.feeData[0],
              subsidiary: false // don't make journal gl tx call
            }
          ]
        }
      );

      // when
      const submittedModel = await transactionExecutionService.executeTransaction(
        transactionInput
      );

      expect(submittedModel.processingInfo?.paymentRail).toBe(
        Rail.INTERNAL.toString()
      );
      expect(depositRepository.makeDeposit).toHaveBeenCalledWith(
        submittedModel.beneficiaryAccountNo,
        expect.objectContaining({
          transactionDetails: {
            transactionChannelId: 'c01_channel'
          }
        }),
        idempotencyKey
      );

      expect(depositRepository.makeWithdrawal).toHaveBeenCalledWith(
        submittedModel.sourceAccountNo,
        expect.objectContaining({
          transactionDetails: {
            transactionChannelId: 'd01_channel'
          }
        }),
        idempotencyKey
      );

      expect(depositRepository.makeWithdrawal).toHaveBeenCalledWith(
        submittedModel.sourceAccountNo,
        expect.objectContaining({
          transactionDetails: {
            transactionChannelId: 'customerTcChannel'
          }
        }),
        idempotencyKey
      );

      expect(Http.generateIdempotencyKey).toBeCalledTimes(3);
      expect(submittedModel.coreBankingTransactionIds).toHaveLength(3);
    });

    describe('should call 2 requests to core banking, including deposit, withdraw if fee data is not available or 0 ', () => {
      const transactions = [
        [
          'fee is not available',
          {
            ...recommendService,
            feeData: undefined
          }
        ],
        [
          'fee is 0',
          {
            ...recommendService,
            feeData: [
              {
                ...recommendService.feeData[0],
                feeAmount: 0
              }
            ]
          }
        ]
      ];
      it.each(transactions)('%s', async (_, service: any) => {
        (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
          service
        );

        const result = await transactionExecutionService.executeTransaction(
          transactionInput
        );

        expect(result.processingInfo?.paymentRail).toBe(
          Rail.INTERNAL.toString()
        );
        expect(depositRepository.makeDeposit).toHaveBeenCalledWith(
          transactionInput.beneficiaryAccountNo,
          expect.objectContaining({
            transactionDetails: {
              transactionChannelId: 'c01_channel'
            }
          }),
          idempotencyKey
        );
        expect(depositRepository.makeWithdrawal).toHaveBeenCalledWith(
          transactionInput.sourceAccountNo,
          expect.objectContaining({
            transactionDetails: {
              transactionChannelId: 'd01_channel'
            }
          }),
          idempotencyKey
        );
        expect(depositRepository.makeWithdrawal).toBeCalledTimes(1);
      });
    });

    describe('should throw error and revert transactions if any transaction in 3 transactions is failed', () => {
      const cases = [
        ['deposit fails', 'deposit'],
        ['withdraw fails', 'withdraw'],
        ['withdraw fee fails', 'fee']
      ];
      const coreBankingTransaction1 = {
        id: '1',
        adjustmentTransactionKey: undefined,
        type: CoreBankTransactionType.WITHDRAWAL
      };
      const coreBankingTransaction2 = {
        id: '2',
        adjustmentTransactionKey: 2,
        type: CoreBankTransactionType.DEPOSIT
      };
      const mockTransactionRequestByType = (type: string) => {
        if (type === 'deposit') {
          (depositRepository.getTransactionsByInstructionId as jest.Mock).mockResolvedValue(
            [coreBankingTransaction1, coreBankingTransaction2]
          );
          (depositRepository.makeDeposit as jest.Mock).mockImplementation(
            () => {
              throw new NetworkError();
            }
          );
        } else if (type === 'withdraw') {
          (depositRepository.makeWithdrawal as jest.Mock).mockImplementation(
            () => {
              throw new NetworkError();
            }
          );
        } else if (type === 'fee') {
          (depositRepository.makeWithdrawal as jest.Mock).mockImplementation(
            () => {
              throw new NetworkError();
            }
          );
        }
      };

      afterEach(() => {
        jest.clearAllMocks();
      });

      beforeEach(() => {
        const savedTransaction = {
          ...getTransactionModelFullData(),
          referenceId: 'referenceId',
          createdAt: new Date('01 June 2020 00:00:00')
        };

        (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValue(
          savedTransaction
        );
        (depositRepository.makeDeposit as jest.Mock).mockResolvedValue({
          id: 'withdraw_id'
        });
        (depositRepository.makeWithdrawal as jest.Mock).mockResolvedValue({
          id: 'deposit_id'
        });
      });

      it.each(cases)('%s', async (_, type) => {
        mockTransactionRequestByType(type);
        (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
          recommendService
        );

        const error = await transactionExecutionService
          .executeTransaction(transactionInput)
          .catch(err => err);

        if (type === 'deposit') {
          expect(depositRepository.makeWithdrawal).toBeCalledTimes(1);
        } else if (type === 'withdraw') {
          expect(depositRepository.makeWithdrawal).toBeCalledTimes(1);
        } else if (type === 'fee') {
          expect(depositRepository.makeWithdrawal).toBeCalledTimes(1);
        }
        expect(error).toBeInstanceOf(RetryableTransferAppError); // NetworkError is translated to AppError already
      });
    });

    describe('should throw error and disable reversal of transactions if any transaction of 3 is failed in RDN internal to internal', () => {
      const cases = [
        ['withdraw fails', 'withdraw'],
        ['withdraw fee fails', 'fee']
      ];
      const mockTransactionRequestByType = (type: string) => {
        if (type === 'withdraw') {
          (depositRepository.makeWithdrawal as jest.Mock).mockImplementation(
            () => {
              throw new NetworkError();
            }
          );
        } else if (type === 'fee') {
          (depositRepository.makeWithdrawal as jest.Mock).mockImplementation(
            () => {
              throw new NetworkError();
            }
          );
        }
      };

      afterEach(() => {
        jest.clearAllMocks();
      });

      it.each(cases)('%s', async (_, type) => {
        mockTransactionRequestByType(type);

        (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
          recommendService
        );

        transactionInput.sourceAccountType = AccountType.RDN;
        transactionInput.paymentServiceType = PaymentServiceTypeEnum.RDN;
        transactionInput.paymentServiceCode = 'RDN_BUY';
        const error = await transactionExecutionService
          .executeTransaction(transactionInput)
          .catch(err => err);
        expect(depositRepository.reverseTransaction).toBeCalledTimes(0);
        expect(error).toBeInstanceOf(RetryableTransferAppError); // NetworkError is translated to AppError already
      });

      it('should not throw error when account type is RDN and got ENOENT', async () => {
        (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
          recommendService
        );
        (depositRepository.makeWithdrawal as jest.Mock).mockImplementation(
          () => {
            throw { message: 'ENOENT failed', code: 'ENOENT' };
          }
        );

        transactionInput.sourceAccountType = AccountType.RDN;
        transactionInput.paymentServiceType = PaymentServiceTypeEnum.RDN;
        transactionInput.paymentServiceCode = 'RDN_BUY';
        const data = await transactionExecutionService
          .executeTransaction(transactionInput)
          .catch(err => err);
        expect(depositRepository.reverseTransaction).toBeCalledTimes(0);
        expect(data.id).toBeDefined();
      });

      it('should throw error when account type is not and got ENOENT', async () => {
        (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
          recommendService
        );
        (depositRepository.makeWithdrawal as jest.Mock).mockImplementation(
          () => {
            throw { message: 'ENOENT failed', code: 'ENOENT' };
          }
        );

        transactionInput.sourceAccountType = AccountType.DC;
        transactionInput.paymentServiceType = PaymentServiceTypeEnum.RDN;
        transactionInput.paymentServiceCode = 'RDN_BUY';
        const error = await transactionExecutionService
          .executeTransaction(transactionInput)
          .catch(err => err);
        expect(depositRepository.reverseTransaction).toBeCalledTimes(0);
        expect(error).toBeInstanceOf(TransferAppError); // NetworkError is translated to AppError already
      });
    });

    describe('should throw error and disable reversal of transactions if any transaction of 3 is failed in RDS internal to internal', () => {
      const cases = [
        ['withdraw fails', 'withdraw'],
        ['withdraw fee fails', 'fee']
      ];
      const mockTransactionRequestByType = (type: string) => {
        if (type === 'withdraw') {
          (depositRepository.makeWithdrawal as jest.Mock).mockImplementation(
            () => {
              throw new NetworkError();
            }
          );
        } else if (type === 'fee') {
          (depositRepository.makeWithdrawal as jest.Mock).mockImplementation(
            () => {
              throw new NetworkError();
            }
          );
        }
      };

      afterEach(() => {
        jest.clearAllMocks();
      });

      it.each(cases)('%s', async (_, type) => {
        mockTransactionRequestByType(type);

        (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
          recommendService
        );

        transactionInput.sourceAccountType = AccountType.RDS;
        transactionInput.paymentServiceType = PaymentServiceTypeEnum.RDN;
        transactionInput.paymentServiceCode = 'RDN_BUY';
        const error = await transactionExecutionService
          .executeTransaction(transactionInput)
          .catch(err => err);
        expect(depositRepository.reverseTransaction).toBeCalledTimes(0);
        expect(error).toBeInstanceOf(RetryableTransferAppError); // NetworkError is translated to AppError already
      });

      it('should not throw error when account type is RDS and got ENOENT', async () => {
        (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
          recommendService
        );
        (depositRepository.makeWithdrawal as jest.Mock).mockImplementation(
          () => {
            throw { message: 'ENOENT failed', code: 'ENOENT' };
          }
        );

        transactionInput.sourceAccountType = AccountType.RDS;
        transactionInput.paymentServiceType = PaymentServiceTypeEnum.RDN;
        transactionInput.paymentServiceCode = 'RDN_BUY';
        const data = await transactionExecutionService
          .executeTransaction(transactionInput)
          .catch(err => err);
        expect(depositRepository.reverseTransaction).toBeCalledTimes(0);
        expect(data.id).toBeDefined();
      });
    });

    it('should retry 1 requesting transactions if they are failed by network error', async () => {
      mockedRetry.mockRestore();

      (depositRepository.makeDeposit as jest.Mock).mockResolvedValueOnce({});

      (depositRepository.makeWithdrawal as jest.Mock).mockResolvedValueOnce({});

      // fee
      (depositRepository.makeWithdrawal as jest.Mock).mockResolvedValueOnce({});

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      await transactionExecutionService.executeTransaction(transactionInput);

      expect(depositRepository.makeDeposit).toBeCalledTimes(1);
      expect(depositRepository.makeWithdrawal).toBeCalledTimes(2);
    });

    it('should retry the requests to revert transactions if they are failed by network error', async () => {
      mockedRetry.mockRestore();
      const coreBankingTransaction1 = {
        id: '1',
        adjustmentTransactionKey: undefined
      };
      const coreBankingTransaction2 = {
        id: '2',
        adjustmentTransactionKey: 2
      };

      (transactionRepository.update as jest.Mock).mockImplementation(
        (id, model) => {
          return {
            ...model,
            id: transactionId,
            createdAt: new Date('01 June 2020 00:00:00')
          };
        }
      );
      (depositRepository.makeDeposit as jest.Mock).mockRejectedValueOnce(
        new NetworkError()
      );
      (depositRepository.makeWithdrawal as jest.Mock).mockResolvedValueOnce({
        id: 'withdrawal_id'
      });
      (depositRepository.getTransactionsByInstructionId as jest.Mock).mockResolvedValue(
        [coreBankingTransaction1, coreBankingTransaction2]
      );
      (depositRepository.reverseTransaction as jest.Mock).mockRejectedValue(
        new NetworkError()
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      await transactionExecutionService
        .executeTransaction(transactionInput)
        .catch(err => err);

      expect(depositRepository.reverseTransaction).toBeCalledTimes(1);
      expect(
        depositRepository.getTransactionsByInstructionId
      ).toHaveBeenCalled();
      expect(depositRepository.makeWithdrawal).toBeCalledTimes(1);
      expect(depositRepository.makeDeposit).toBeCalledTimes(1);
      //expect(error).toBeInstanceOf(NetworkError); //TODO: verify error type
    });

    it('should not call revert when no mambu transactions are completed', async () => {
      mockedRetry.mockRestore();

      (depositRepository.makeWithdrawal as jest.Mock).mockRejectedValue(
        new NetworkError()
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      const error = await transactionExecutionService
        .executeTransaction(transactionInput)
        .catch(err => err);

      expect(depositRepository.reverseTransaction).not.toHaveBeenCalled();
      expect(
        depositRepository.getTransactionsByInstructionId
      ).not.toHaveBeenCalled();
    });

    it('should not send the debit transactions to core banking if the debit transaction code and fees data is not available', async () => {
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          debitTransactionCode: undefined,
          feeData: undefined
        }
      );

      const submitedModel = await transactionExecutionService.executeTransaction(
        transactionInput
      );

      expect(depositRepository.makeWithdrawal).not.toHaveBeenCalled();
      expect(submitedModel.coreBankingTransactionIds).toHaveLength(1);
    });

    it('should call switching submission when the credit transaction code is not available and eligible for external debit', async () => {
      (transactionSubmissionSwitchingTemplate.submitTransaction as jest.Mock).mockResolvedValueOnce(
        {}
      );
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          creditTransactionCodeInfo: undefined,
          requireThirdPartyOutgoingId: true
        }
      );
      const submittedModel = await transactionExecutionService.executeTransaction(
        {
          ...transactionInput,
          beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL
        }
      );
      expect(submittedModel.processingInfo?.paymentRail).toBe(
        Rail.EURONET.toString()
      );
      expect(submittedModel).toEqual(
        expect.objectContaining({
          status: TransactionStatus.SUBMITTED
        })
      );
      expect(depositRepository.makeDeposit).not.toHaveBeenCalled();
      expect(depositRepository.makeWithdrawal).toHaveBeenCalledTimes(2);
    });

    it('should throw if no matching template found for internal credit', async () => {
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          creditTransactionCode: [
            {
              transactionCode: 'c01',
              transactionCodeInfo: {
                code: 'c01',
                channel: undefined,
                defaultCategoryCode: 'defaultC01',
                minAmountPerTx: 100,
                maxAmountPerTx: 9999999,
                limitGroupCode: 'c01'
              }
            }
          ]
        }
      );

      const error = await transactionExecutionService
        .executeTransaction(transactionInput)
        .catch(err => err);

      expect(error).toBeInstanceOf(RetryableTransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.MISSING_CREDIT_TEMPLATE);
    });

    it('should throw if no matching template found for outgoing transfer', async () => {
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          requireThirdPartyOutgoingId: true
        }
      );

      const error = await transactionExecutionService
        .executeTransaction(transactionInput)
        .catch(err => err);

      expect(error).toBeInstanceOf(RetryableTransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.MISSING_CREDIT_TEMPLATE);
    });
  });

  describe('executeDisbursementTransaction', () => {
    const creditTransactionChannelId = 'creditChannelTest';
    const debitTransactionChannelId = 'debitChannelTest';
    const disbursementTransactionInput = {
      ...transactionInput,
      paymentServiceType: PaymentServiceTypeEnum.LOAN_DIRECT_DISBURSEMENT,
      paymentServiceCode: PaymentServiceTypeEnum.LOAN_DISBURSEMENT,
      interchange: BankNetworkEnum.LOAN_PROCESSOR,
      additionalPayload: {
        debitTransactionChannelId,
        creditTransactionChannelId
      }
    };
    it('should submit disbursement transaction', async () => {
      // given
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        disbursementTransactionInput
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(loanRepository.makeDisbursement).toHaveBeenCalledTimes(1);
      expect(depositRepository.makeDeposit).toHaveBeenCalledTimes(1);
    });

    it(`should success submit disbursement without execute credit beneficiary when interchange is "${BankNetworkEnum.LOAN_PROCESSOR_WINCORE_DISBURSEMENT}"`, async () => {
      // given
      const customDisbursementTransactionInput = {
        ...disbursementTransactionInput,
        interchange: BankNetworkEnum.LOAN_PROCESSOR_WINCORE_DISBURSEMENT
      };

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        customDisbursementTransactionInput
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(loanRepository.makeDisbursement).toBeCalledTimes(1);
      expect(depositRepository.makeDeposit).not.toBeCalledTimes(1);
    });

    it(`should success submit disbursement without execute credit beneficiary when interchange is "${BankNetworkEnum.LOAN_PROCESSOR_GL_RDL}"`, async () => {
      // given
      const customDisbursementTransactionInput = {
        ...disbursementTransactionInput,
        paymentServiceType: PaymentServiceTypeEnum.LOAN_GL_DISBURSEMENT,
        paymentServiceCode: PaymentServiceTypeEnum.LOAN_DISBURSEMENT,
        interchange: BankNetworkEnum.LOAN_PROCESSOR_GL_RDL,
        additionalPayload: {
          debitTransactionChannelId
        }
      };

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      await transactionExecutionService.executeTransaction(
        customDisbursementTransactionInput
      );

      // then
      expect(loanRepository.makeDisbursement).toBeCalledTimes(1);
    });

    it('should not submit disbursement transaction if sourceAccountNo and debitTransactionChannel not exist', async () => {
      // given
      const input = {
        ...disbursementTransactionInput,
        sourceAccountNo: undefined,
        debitTransactionChannel: undefined
      };
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      await transactionExecutionService.executeTransaction(input);

      // then
      expect(loanRepository.makeDisbursement).not.toHaveBeenCalled();
    });

    it('should not call adjust loan transaction if disbursement process if failed when submit disbursement step', async () => {
      // given
      mockedRetry.mockRestore();

      (loanRepository.makeDisbursement as jest.Mock).mockRejectedValueOnce(
        new NetworkError()
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      await transactionExecutionService
        .executeTransaction(disbursementTransactionInput)
        .catch(err => err);

      // then
      expect(loanRepository.adjustLoanTransaction).not.toHaveBeenCalled();
      expect(depositRepository.reverseTransaction).not.toHaveBeenCalled();
    });

    it('should call adjust loan transaction if disbursement process if failed when deposit step', async () => {
      // given
      mockedRetry.mockRestore();

      (depositRepository.makeDeposit as jest.Mock).mockRejectedValue(
        new NetworkError()
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      await transactionExecutionService
        .executeTransaction(disbursementTransactionInput)
        .catch(err => err);

      // then
      expect(loanRepository.adjustLoanTransaction).toHaveBeenCalled();
      expect(depositRepository.reverseTransaction).not.toHaveBeenCalled();
    });
  });

  describe('executeRepaymentTransaction', () => {
    const baseRepaymentTransactionInput = {
      ...transactionInput,
      paymentServiceType: PaymentServiceTypeEnum.LOAN_DIRECT_REPAYMENT,
      paymentServiceCode: PaymentServiceTypeEnum.LOAN_REPAYMENT,
      interchange: BankNetworkEnum.LOAN_PROCESSOR
    };

    it('should submit repayment transaction without submit GL journal if there is no BANK_INCOME', async () => {
      // given
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        baseRepaymentTransactionInput
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(loanRepository.makeRepayment).toHaveBeenCalledTimes(1);
      expect(depositRepository.makeWithdrawal).toHaveBeenCalledTimes(1);
      expect(depositRepository.createGlJournalEntries).not.toHaveBeenCalled();
    });

    it('should submit repayment transaction with repayment allocation without submit GL journal if there is no BANK_INCOME', async () => {
      // given
      const input = {
        ...baseRepaymentTransactionInput,
        additionalPayload: {
          loanRepaymentAllocation: [
            { type: AmountTypeEnum.PRINCIPAL, amount: 10000 }
          ]
        }
      };
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(loanRepository.makeRepayment).toHaveBeenCalledTimes(1);
      expect(depositRepository.makeWithdrawal).toHaveBeenCalledTimes(1);
      expect(depositRepository.createGlJournalEntries).not.toHaveBeenCalled();
    });

    it('should submit repayment transaction and submit GL journal if there is glTransactionDetail', async () => {
      // given
      const repaymentGlNumber = 'glTest1';
      const bankIncomeGlNumber = 'glTest2';
      const input = {
        ...baseRepaymentTransactionInput,
        additionalPayload: {
          glTransactionDetail: [
            {
              glAccountCode: GeneralLedgerCode.REPAYMENT,
              amount: 10000,
              type: GlTransactionTypes.DEBIT
            },
            {
              glAccountCode: GeneralLedgerCode.BANK_INCOME,
              amount: 1000,
              type: GlTransactionTypes.CREDIT
            }
          ],
          loanRepaymentAllocation: [
            { type: AmountTypeEnum.PRINCIPAL, amount: 10000 },
            { type: BANK_INCOME, amount: 10000 }
          ]
        }
      };

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );
      (configurationRepository.getAllGeneralLedgers as jest.Mock).mockResolvedValue(
        getGeneralLedgersMockData()
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(loanRepository.makeRepayment).toHaveBeenCalledTimes(1);
      expect(depositRepository.makeWithdrawal).toHaveBeenCalledTimes(1);
      expect(depositRepository.createGlJournalEntries).toHaveBeenCalledTimes(1);
    });

    it('should submit repayment transaction and submit GL journal if there is glTransactionDetail with multiple BANK_INCOME', async () => {
      // given
      const input = {
        ...baseRepaymentTransactionInput,
        additionalPayload: {
          glTransactionDetail: [
            {
              glAccountCode: GeneralLedgerCode.REPAYMENT,
              amount: 10000,
              type: GlTransactionTypes.DEBIT
            },
            {
              glAccountCode: GeneralLedgerCode.BANK_INCOME,
              amount: 1000,
              type: GlTransactionTypes.CREDIT
            },
            {
              glAccountCode: GeneralLedgerCode.BANK_INCOME,
              amount: 500,
              type: GlTransactionTypes.CREDIT
            }
          ],
          loanRepaymentAllocation: [
            { type: AmountTypeEnum.PRINCIPAL, amount: 10000 },
            { type: BANK_INCOME, amount: 1000 },
            { type: BANK_INCOME, amount: 500 }
          ]
        }
      };

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );
      (configurationRepository.getAllGeneralLedgers as jest.Mock).mockResolvedValue(
        getGeneralLedgersMockData()
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(depositRepository.createGlJournalEntries).toHaveBeenCalledTimes(1);
      expect(depositRepository.createGlJournalEntries).toHaveBeenCalledWith(
        expect.objectContaining(mapToGlIncomeTransaction(result)),
        expect.stringContaining(result.idempotencyKey || '')
      );
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        expect.stringContaining(result.beneficiaryAccountNo || ''),
        expect.objectContaining(
          mapToRepaymentTransaction(
            result,
            result.creditTransactionChannel || '',
            1500
          )
        ),
        expect.stringContaining(result.idempotencyKey || '')
      );
    });

    it('should execute repayment transaction if the payment service type is LOAN_DIRECT_REPAYMENT', async () => {
      // given
      const input = {
        ...baseRepaymentTransactionInput,
        paymentServiceType: PaymentServiceTypeEnum.LOAN_DIRECT_REPAYMENT,
        additionalPayload: {
          glTransactionDetail: [
            {
              glAccountCode: GeneralLedgerCode.REPAYMENT,
              amount: 10000,
              type: GlTransactionTypes.DEBIT
            },
            {
              glAccountCode: GeneralLedgerCode.BANK_INCOME,
              amount: 1000,
              type: GlTransactionTypes.CREDIT
            }
          ],
          loanRepaymentAllocation: [
            { type: AmountTypeEnum.PRINCIPAL, amount: 10000 },
            { type: BANK_INCOME, amount: 10000 }
          ]
        }
      };

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );
      (configurationRepository.getAllGeneralLedgers as jest.Mock).mockResolvedValue(
        getGeneralLedgersMockData()
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(loanRepository.makeRepayment).toHaveBeenCalledTimes(1);
      expect(depositRepository.makeWithdrawal).toHaveBeenCalledTimes(1);
      expect(depositRepository.createGlJournalEntries).toHaveBeenCalledTimes(1);
    });

    it('should execute repayment transaction if the payment service type is LOAN_REPAYMENT', async () => {
      // given
      const input = {
        ...baseRepaymentTransactionInput,
        paymentServiceType: PaymentServiceTypeEnum.LOAN_REPAYMENT,
        additionalPayload: {
          ...baseRepaymentTransactionInput.additionalPayload,
          glTransactionDetail: [
            {
              glAccountCode: GeneralLedgerCode.DEPOSIT,
              amount: 10000,
              type: GlTransactionTypes.DEBIT
            },
            {
              glAccountCode: GeneralLedgerCode.REPAYMENT,
              amount: 9000,
              type: GlTransactionTypes.CREDIT
            },
            {
              glAccountCode: GeneralLedgerCode.BANK_INCOME,
              amount: 1000,
              type: GlTransactionTypes.CREDIT
            }
          ],
          loanRepaymentAllocation: [
            { type: AmountTypeEnum.PRINCIPAL, amount: 10000 },
            { type: BANK_INCOME, amount: 1000 }
          ]
        }
      };

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );
      (configurationRepository.getAllGeneralLedgers as jest.Mock).mockResolvedValue(
        getGeneralLedgersMockData()
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(loanRepository.makeRepayment).toHaveBeenCalledTimes(1);
      expect(depositRepository.makeWithdrawal).not.toHaveBeenCalled();
      expect(depositRepository.createGlJournalEntries).toHaveBeenCalledTimes(1);
    });

    it('should execute repayment transaction if the payment service type is LOAN_GL_REPAYMENT', async () => {
      // given
      const input = {
        ...baseRepaymentTransactionInput,
        paymentServiceType: PaymentServiceTypeEnum.LOAN_GL_REPAYMENT,
        additionalPayload: {
          ...baseRepaymentTransactionInput.additionalPayload,
          glTransactionDetail: [
            {
              glAccountCode: GeneralLedgerCode.REPAYMENT,
              amount: 1000,
              type: GlTransactionTypes.DEBIT
            },
            {
              glAccountCode: GeneralLedgerCode.BANK_INCOME,
              amount: 1000,
              type: GlTransactionTypes.CREDIT
            }
          ],
          loanRepaymentAllocation: [{ type: BANK_INCOME, amount: 1000 }]
        }
      };

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );
      (configurationRepository.getAllGeneralLedgers as jest.Mock).mockResolvedValue(
        getGeneralLedgersMockData()
      );

      // when
      await transactionExecutionService.executeTransaction(input);

      // then
      expect(loanRepository.makeRepayment).toHaveBeenCalledTimes(1);
      expect(depositRepository.makeWithdrawal).not.toHaveBeenCalled();
      expect(depositRepository.createGlJournalEntries).toHaveBeenCalledTimes(1);
    });

    it('should execute payoff transaction', async () => {
      // given
      const input = {
        ...baseRepaymentTransactionInput,
        paymentServiceType: PaymentServiceTypeEnum.LOAN_REPAYMENT,
        additionalPayload: {
          ...baseRepaymentTransactionInput.additionalPayload,
          glTransactionDetail: [
            {
              glAccountCode: GeneralLedgerCode.REPAYMENT,
              amount: 10000,
              type: GlTransactionTypes.DEBIT
            },
            {
              glAccountCode: GeneralLedgerCode.BANK_INCOME,
              amount: 1000,
              type: GlTransactionTypes.CREDIT
            }
          ],
          loanRepaymentAllocation: [
            { type: AmountTypeEnum.INTEREST, amount: 10000 },
            { type: AmountTypeEnum.PENALTY, amount: 1000 }
          ],
          shouldRepayWithPayOff: true
        }
      };

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );
      (configurationRepository.getAllGeneralLedgers as jest.Mock).mockResolvedValue(
        getGeneralLedgersMockData()
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(loanRepository.makePayOff).toHaveBeenCalledTimes(1);
      expect(loanRepository.makeRepayment).not.toHaveBeenCalled();
      expect(depositRepository.makeWithdrawal).not.toHaveBeenCalled();
      expect(depositRepository.createGlJournalEntries).toHaveBeenCalledTimes(1);
    });

    it('should execute normal repayment and payoff transaction when shouldRepayWithPayOff: true and repaymentAmountForPayOffInterestSettlement was defined ', async () => {
      // given
      const input = {
        ...baseRepaymentTransactionInput,
        paymentServiceType: PaymentServiceTypeEnum.LOAN_REPAYMENT,
        additionalPayload: {
          ...baseRepaymentTransactionInput.additionalPayload,
          glTransactionDetail: [
            {
              glAccountCode: GeneralLedgerCode.REPAYMENT,
              amount: 10000,
              type: GlTransactionTypes.DEBIT
            },
            {
              glAccountCode: GeneralLedgerCode.BANK_INCOME,
              amount: 1000,
              type: GlTransactionTypes.CREDIT
            }
          ],
          loanRepaymentAllocation: [
            { type: AmountTypeEnum.INTEREST, amount: 10000 },
            { type: AmountTypeEnum.PENALTY, amount: 1000 }
          ],
          shouldRepayWithPayOff: true,
          repaymentAmountForPayOffInterestSettlement: 11000
        }
      };

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );
      (configurationRepository.getAllGeneralLedgers as jest.Mock).mockResolvedValue(
        getGeneralLedgersMockData()
      );

      // when
      const result = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(Rail.INTERNAL.toString());
      expect(loanRepository.makePayOff).toHaveBeenCalledTimes(1);
      expect(loanRepository.makeRepayment).toHaveBeenCalledTimes(1);
      expect(depositRepository.makeWithdrawal).not.toHaveBeenCalled();
      expect(depositRepository.createGlJournalEntries).toHaveBeenCalledTimes(1);
    });

    it('should not submit repayment transaction if beneficiaryAccountNo and creditTransactionChannel not exist', async () => {
      // given
      const input = {
        ...baseRepaymentTransactionInput,
        beneficiaryAccountNo: undefined,
        creditTransactionChannel: undefined
      };
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      await transactionExecutionService.executeTransaction(input);

      // then
      expect(loanRepository.makeRepayment).not.toHaveBeenCalled();
    });

    it('should not call reverse transaction and  adjust loan transaction if repayment process if failed when debit step', async () => {
      // given
      mockedRetry.mockRestore();

      (depositRepository.makeWithdrawal as jest.Mock).mockRejectedValueOnce(
        new NetworkError()
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      await transactionExecutionService
        .executeTransaction(baseRepaymentTransactionInput)
        .catch(err => err);

      // then
      expect(loanRepository.adjustLoanTransaction).not.toHaveBeenCalled();
      expect(depositRepository.reverseTransaction).not.toHaveBeenCalled();
    });

    it('should call reverse transaction and not call adjust loan transaction if repayment process if failed when submit repayment step', async () => {
      // given
      const repaymentGlNumber = 'glTest1';
      const bankIncomeGlNumber = 'glTest2';
      const input = {
        ...baseRepaymentTransactionInput,
        additionalPayload: {
          ...baseRepaymentTransactionInput.additionalPayload,
          glTransactionDetail: [
            {
              glAccountCode: GeneralLedgerCode.REPAYMENT,
              amount: 10000,
              type: GlTransactionTypes.DEBIT
            },
            {
              glAccountCode: GeneralLedgerCode.BANK_INCOME,
              amount: 10000,
              type: GlTransactionTypes.CREDIT
            }
          ],
          loanRepaymentAllocation: [
            { type: AmountTypeEnum.PRINCIPAL, amount: 10000 },
            { type: BANK_INCOME, amount: 10000 }
          ]
        }
      };
      mockedRetry.mockRestore();

      (loanRepository.makeRepayment as jest.Mock).mockRejectedValue(
        new NetworkError()
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );
      (configurationRepository.getAllGeneralLedgers as jest.Mock).mockResolvedValue(
        getGeneralLedgersMockData()
      );

      // when
      await transactionExecutionService
        .executeTransaction(input)
        .catch(err => err);

      // then
      expect(loanRepository.adjustLoanTransaction).not.toHaveBeenCalled();
      expect(depositRepository.reverseTransaction).toHaveBeenCalled();
      expect(depositRepository.createGlJournalEntries).toHaveBeenCalledTimes(2);
    });

    it('should call reverse transaction and not call adjust loan transaction if make repayment in mambu is failed', async () => {
      // given
      const repaymentGlNumber = 'glTest1';
      const bankIncomeGlNumber = 'glTest2';
      const input = {
        ...baseRepaymentTransactionInput,
        additionalPayload: {
          ...baseRepaymentTransactionInput.additionalPayload,
          glTransactionDetail: [
            {
              glAccountCode: GeneralLedgerCode.REPAYMENT,
              amount: 10000,
              type: GlTransactionTypes.DEBIT
            },
            {
              glAccountCode: GeneralLedgerCode.BANK_INCOME,
              amount: 10000,
              type: GlTransactionTypes.CREDIT
            }
          ],
          loanRepaymentAllocation: [
            { type: AmountTypeEnum.PRINCIPAL, amount: 10000 },
            { type: BANK_INCOME, amount: 10000 }
          ]
        }
      };
      mockedRetry.mockRestore();

      (loanRepository.makeRepayment as jest.Mock).mockRejectedValue(
        new NetworkError()
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );
      (configurationRepository.getAllGeneralLedgers as jest.Mock).mockResolvedValue(
        getGeneralLedgersMockData()
      );

      // when
      await transactionExecutionService
        .executeTransaction(input)
        .catch(err => err);

      // then
      expect(loanRepository.adjustLoanTransaction).not.toHaveBeenCalled();
      expect(depositRepository.reverseTransaction).toHaveBeenCalled();
    });

    it('should call reverse transaction and not call adjust loan transaction if repayment process if failed when submit payoff step', async () => {
      // given
      const input = {
        ...baseRepaymentTransactionInput,
        additionalPayload: {
          ...baseRepaymentTransactionInput.additionalPayload,
          glTransactionDetail: [
            {
              glAccountCode: GeneralLedgerCode.REPAYMENT,
              amount: 10000,
              type: GlTransactionTypes.DEBIT
            },
            {
              glAccountCode: GeneralLedgerCode.BANK_INCOME,
              amount: 10000,
              type: GlTransactionTypes.CREDIT
            }
          ],
          loanRepaymentAllocation: [
            { type: AmountTypeEnum.PRINCIPAL, amount: 10000 },
            { type: BANK_INCOME, amount: 10000 }
          ],
          shouldRepayWithPayOff: true
        }
      };
      mockedRetry.mockRestore();

      (loanRepository.makePayOff as jest.Mock).mockRejectedValue(
        new NetworkError()
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );
      (configurationRepository.getAllGeneralLedgers as jest.Mock).mockResolvedValue(
        getGeneralLedgersMockData()
      );

      // when
      await transactionExecutionService
        .executeTransaction(input)
        .catch(err => err);

      // then
      expect(loanRepository.adjustLoanTransaction).not.toHaveBeenCalled();
      expect(depositRepository.reverseTransaction).toHaveBeenCalled();
    });

    it(`should submit GL entry only for payment service type ${PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY}`, async () => {
      // given
      const input: ITransactionInput = {
        ...baseRepaymentTransactionInput,
        paymentServiceType: PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY,
        paymentServiceCode: PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY,
        additionalPayload: {
          glTransactionDetail: [
            {
              glAccountCode: GeneralLedgerCode.DEPOSIT,
              amount: 500000,
              type: 'DEBIT'
            },
            {
              glAccountCode: GeneralLedgerCode.OPERATIONAL_INCOME_RECOVERY,
              amount: 500000,
              type: 'CREDIT'
            }
          ]
        }
      };

      const mockGeneralLedgers: IGeneralLedger[] = [
        { code: GeneralLedgerCode.DEPOSIT, accountNumber: '0001' },
        {
          code: GeneralLedgerCode.OPERATIONAL_INCOME_RECOVERY,
          accountNumber: '0002'
        }
      ];

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      (configurationRepository.getAllGeneralLedgers as jest.Mock).mockResolvedValueOnce(
        mockGeneralLedgers
      );

      // when
      await transactionExecutionService.executeTransaction(input);

      // then
      const mockLoanTransactionDate = get(
        input.additionalPayload,
        'loanTransactionDate'
      );
      const mockDate = moment(mockLoanTransactionDate)
        .utcOffset(UTC_OFFSET_WESTERN_INDONESIAN)
        .format(coreBankingConstant.VALUE_DATE_MAMBU_FORMAT);
      const expectedParam: IGLJournalEntriesV2 = {
        date: mockDate,
        debits: [{ glAccount: '0001', amount: 500000 }],
        credits: [{ glAccount: '0002', amount: 500000 }]
      };

      expect(depositRepository.createGlJournalEntries).toBeCalledTimes(1);
      expect(depositRepository.createGlJournalEntries).toBeCalledWith(
        expectedParam,
        idempotencyKey
      );
      expect(depositRepository.makeWithdrawal).not.toBeCalled();
      expect(loanRepository.makePayOff).not.toBeCalled();
      expect(loanRepository.makeRepayment).not.toBeCalled();
    });

    it(`should not do reversal when fail submit GL entry for payment service type ${PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY}`, async () => {
      // given
      const input: ITransactionInput = {
        ...baseRepaymentTransactionInput,
        paymentServiceType: PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY,
        paymentServiceCode: PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY,
        additionalPayload: {
          glTransactionDetail: [
            {
              glAccountCode: GeneralLedgerCode.DEPOSIT,
              amount: 500000,
              type: 'DEBIT'
            },
            {
              glAccountCode: GeneralLedgerCode.OPERATIONAL_INCOME_RECOVERY,
              amount: 500000,
              type: 'CREDIT'
            }
          ]
        }
      };

      const mockGeneralLedgers: IGeneralLedger[] = [
        { code: GeneralLedgerCode.DEPOSIT, accountNumber: '0001' },
        {
          code: GeneralLedgerCode.OPERATIONAL_INCOME_RECOVERY,
          accountNumber: '0002'
        }
      ];

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      (configurationRepository.getAllGeneralLedgers as jest.Mock).mockResolvedValueOnce(
        mockGeneralLedgers
      );

      (depositRepository.createGlJournalEntries as jest.Mock).mockRejectedValueOnce(
        new NetworkError()
      );

      // when
      const error = await transactionExecutionService
        .executeTransaction(input)
        .catch(err => err);

      // then
      expect(error).toBeDefined();
      expect(depositRepository.createGlJournalEntries).toBeCalledTimes(1);
      expect(depositRepository.reverseTransaction).not.toBeCalled();
      expect(loanRepository.adjustLoanTransaction).not.toBeCalled();
    });
  });

  describe('executeForwardPaymentDeposit', () => {
    const forwardPaymentDepositTransactionInput = createForwardPaymentTransactionInput(
      PaymentServiceTypeEnum.LOAN_FORWARD_PAYMENT_DEPOSIT
    );
    const transactionModel: NewEntity<ITransactionModel> = transactionHelper.buildNewTransactionModel(
      forwardPaymentDepositTransactionInput,
      recommendService
    );
    const model: ITransactionModel = {
      ...transactionModel,
      id: transactionId
    };

    it('should submit debit transaction', async () => {
      // given
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      await transactionExecutionService.executeTransaction(
        forwardPaymentDepositTransactionInput
      );

      // then
      expect(depositRepository.makeWithdrawal).toHaveBeenCalledTimes(1);
      expect(depositRepository.makeWithdrawal).toHaveBeenCalledWith(
        forwardPaymentDepositTransactionInput.sourceAccountNo,
        expect.objectContaining({
          transactionDetails: {
            transactionChannelId: model.debitTransactionChannel
          },
          amount: forwardPaymentDepositTransactionInput.transactionAmount
        }),
        idempotencyKey
      );
      expect(depositRepository.makeDeposit).not.toHaveBeenCalled();
      expect(transactionRepository.update).toBeCalledWith(transactionId, {
        ...model,
        status: TransactionStatus.SUCCEED,
        journey: expect.arrayContaining([
          {
            status: TransferJourneyStatusEnum.TRANSFER_SUCCESS,
            updatedAt: expect.anything()
          }
        ]),
        coreBankingTransactionIds: ['1'],
        coreBankingTransactions: [
          {
            id: '1',
            type: 'AMOUNT_DEBITED'
          }
        ],
        referenceId: expect.anything(),
        processingInfo: expect.objectContaining({
          paymentRail: Rail.INTERNAL.toString()
        })
      });
    });

    it('should not call reverse transaction if failed when submit debit step', async () => {
      // given
      mockedRetry.mockRestore();

      (depositRepository.makeWithdrawal as jest.Mock).mockRejectedValue(
        new NetworkError()
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      await transactionExecutionService
        .executeTransaction(forwardPaymentDepositTransactionInput)
        .catch(err => err);

      // then
      expect(depositRepository.reverseTransaction).not.toHaveBeenCalled();
      expect(transactionRepository.update).toBeCalledWith(transactionId, {
        ...model,
        status: TransactionStatus.DECLINED,
        journey: expect.arrayContaining([
          {
            status: TransferJourneyStatusEnum.TRANSFER_FAILURE,
            updatedAt: expect.anything()
          }
        ]),
        failureReason: {
          type: ERROR_CODE.COREBANKING_DEBIT_FAILED,
          actor: TransferFailureReasonActor.UNKNOWN,
          detail: 'Error while debiting money, code: undefined, message: !'
        },
        referenceId: expect.anything(),
        processingInfo: expect.objectContaining({
          paymentRail: Rail.INTERNAL.toString()
        })
      });
    });
  });

  describe('executeForwardPaymentWithdrawal', () => {
    const forwardPaymentDepositTransactionInput = createForwardPaymentTransactionInput(
      PaymentServiceTypeEnum.LOAN_FORWARD_PAYMENT_WITHDRAWAL
    );
    const transactionModel: NewEntity<ITransactionModel> = transactionHelper.buildNewTransactionModel(
      forwardPaymentDepositTransactionInput,
      recommendService
    );
    const model: ITransactionModel = {
      ...transactionModel,
      id: transactionId
    };
    it('should submit debit transaction', async () => {
      // given
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      await transactionExecutionService.executeTransaction(
        forwardPaymentDepositTransactionInput
      );

      // then
      expect(depositRepository.makeDeposit).toHaveBeenCalledTimes(1);
      expect(depositRepository.makeWithdrawal).not.toHaveBeenCalled();
      expect(depositRepository.makeDeposit).toHaveBeenCalledWith(
        forwardPaymentDepositTransactionInput.beneficiaryAccountNo,
        expect.objectContaining({
          transactionDetails: {
            transactionChannelId: model.creditTransactionChannel
          },
          amount: forwardPaymentDepositTransactionInput.transactionAmount
        }),
        idempotencyKey
      );
      expect(transactionRepository.update).toBeCalledWith(transactionId, {
        ...model,
        status: TransactionStatus.SUCCEED,
        journey: expect.arrayContaining([
          {
            status: TransferJourneyStatusEnum.TRANSFER_SUCCESS,
            updatedAt: expect.anything()
          }
        ]),
        coreBankingTransactionIds: ['1'],
        coreBankingTransactions: [
          {
            id: '1',
            type: 'AMOUNT_CREDITED'
          }
        ],
        referenceId: expect.anything(),
        processingInfo: expect.objectContaining({
          paymentRail: Rail.INTERNAL.toString()
        })
      });
    });

    it('should not call reverse transaction if failed when submit credit step', async () => {
      // given
      mockedRetry.mockRestore();

      (depositRepository.makeDeposit as jest.Mock).mockRejectedValue(
        new NetworkError()
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        recommendService
      );

      // when
      await transactionExecutionService
        .executeTransaction(forwardPaymentDepositTransactionInput)
        .catch(err => err);

      // then
      expect(depositRepository.reverseTransaction).not.toHaveBeenCalled();
      expect(transactionRepository.update).toBeCalledWith(transactionId, {
        ...model,
        status: TransactionStatus.DECLINED,
        journey: expect.arrayContaining([
          {
            status: TransferJourneyStatusEnum.TRANSFER_FAILURE,
            updatedAt: expect.anything()
          }
        ]),
        failureReason: {
          type: ERROR_CODE.COREBANKING_CREDIT_FAILED,
          actor: TransferFailureReasonActor.UNKNOWN,
          detail: 'Error while crediting money, code: undefined, message: !'
        },
        referenceId: expect.anything(),
        processingInfo: expect.objectContaining({
          paymentRail: Rail.INTERNAL.toString()
        })
      });
    });
  });

  describe('executeBlockingTransaction', () => {
    it('should call method create blocking success', async () => {
      (transactionSubmissionSwitchingTemplate.submitTransaction as jest.Mock).mockImplementation(
        model => {
          return {
            ...model,
            id: transactionId,
            executionType: ExecutionTypeEnum.BLOCKING,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
            status: TransactionStatus.SUCCEED
          };
        }
      );
      (transactionRepository.update as jest.Mock).mockImplementation(
        (id, model) => {
          return {
            ...model,
            id: id,
            executionType: ExecutionTypeEnum.BLOCKING,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL
          };
        }
      );

      (transactionBlockingHelper.createBlockingTransaction as jest.Mock).mockImplementation(
        model => {
          return {
            blockingId: 'principal-blocking-id',
            fees: [
              {
                blockingId: 'fee-blocking-id'
              }
            ]
          };
        }
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          blocking: true,
          feeData: {
            ...recommendService.feeData
          },
          requireThirdPartyOutgoingId: true
        }
      );

      const transaction = await transactionExecutionService.executeTransaction(
        transactionInput
      );
      expect(transactionBlockingHelper.createBlockingTransaction).toBeCalled();
    });

    it('should reject transaction if bank channel and accountNo are INTERNAL', async () => {
      (transactionRepository.create as jest.Mock).mockImplementation(model => ({
        ...model,
        id: transactionId
      }));
      (transactionRepository.update as jest.Mock).mockImplementation(
        (id, model) => ({
          ...model,
          executionType: ExecutionTypeEnum.BLOCKING,
          beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
          beneficiaryAccountNo: 'account123'
        })
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          blocking: true
        }
      );

      const error = await transactionExecutionService
        .executeTransaction(transactionInput)
        .catch(err => err);
      // then
      expect(error).toBeInstanceOf(RetryableTransferAppError);
      expect(error.errorCode).toBe(ERROR_CODE.TRANSACTION_REJECTED);
    });

    it(`
    GIVEN 
      Daily Limit ATM Withdrawal Pocket Level Limit Validation Flag is ENABLED
      AND
      recommend service is not atm withdrawal
    THEN
      Ensure that it doesn't call atmWithdrawaltransactionUsageService.validateDailyLimit
    `, async () => {
      (transactionSubmissionSwitchingTemplate.submitTransaction as jest.Mock).mockImplementation(
        model => {
          return {
            ...model,
            id: transactionId,
            executionType: ExecutionTypeEnum.BLOCKING,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
            status: TransactionStatus.SUCCEED
          };
        }
      );
      (transactionRepository.update as jest.Mock).mockImplementation(
        (id, model) => {
          return {
            ...model,
            id: id,
            executionType: ExecutionTypeEnum.BLOCKING,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL
          };
        }
      );

      (transactionBlockingHelper.createBlockingTransaction as jest.Mock).mockImplementation(
        model => {
          return {
            blockingId: 'principal-blocking-id',
            fees: [
              {
                blockingId: 'fee-blocking-id'
              }
            ]
          };
        }
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          blocking: true,
          feeData: {
            ...recommendService.feeData
          },
          requireThirdPartyOutgoingId: true
        }
      );

      const pocketLevelLimitValidationFn = transactionUsageService.pocketLevelLimitValidation as jest.Mock;
      pocketLevelLimitValidationFn.mockResolvedValueOnce({});

      await transactionExecutionService.executeTransaction(transactionInput);
      expect(pocketLevelLimitValidationFn).toBeCalledTimes(1);
    });

    it('should call transaction info tracker when entitlement flag is enabled', async () => {
      entitlementFlag.isEnabled = jest.fn().mockResolvedValue(true);
      (transactionSubmissionSwitchingTemplate.submitTransaction as jest.Mock).mockImplementation(
        model => {
          return {
            ...model,
            id: transactionId,
            executionType: ExecutionTypeEnum.BLOCKING,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
            status: TransactionStatus.SUCCEED
          };
        }
      );
      (transactionRepository.update as jest.Mock).mockImplementation(
        (id, model) => {
          return {
            ...model,
            id: id,
            executionType: ExecutionTypeEnum.BLOCKING,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL
          };
        }
      );

      (transactionBlockingHelper.createBlockingTransaction as jest.Mock).mockImplementation(
        model => {
          return {
            blockingId: 'principal-blocking-id',
            fees: [
              {
                blockingId: 'fee-blocking-id'
              }
            ]
          };
        }
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          blocking: true,
          feeData: {
            ...recommendService.feeData
          },
          requireThirdPartyOutgoingId: true
        }
      );

      await transactionExecutionService.executeTransaction(transactionInput);
      expect(getTransactionInfoTracker).toBeCalled();
    });

    it('should reject transaction if creditTemplate is undefined because beneficiaryAccountNo was null', async () => {
      (transactionRepository.create as jest.Mock).mockImplementation(model => ({
        ...model,
        id: transactionId
      }));

      (transactionRepository.update as jest.Mock).mockImplementation(
        (_id, model) => ({
          ...model,
          executionType: ExecutionTypeEnum.BLOCKING,
          beneficiaryBankCodeChannel: BankChannelEnum.BIFAST,
          beneficiaryAccountNo: undefined
        })
      );

      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendServiceWithRequireThirdPartyOutgoingId,
          blocking: true
        }
      );

      const error = await transactionExecutionService
        .executeTransaction(transactionInput)
        .catch(err => err);
      // then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toBe(
        ERROR_CODE.REJECTED_DUE_UNDEFINED_BENEFICIARY_ACCOUNT_NUMBER
      );
    });
  });

  describe('settleTransaction', () => {
    const transactionModel: NewEntity<ITransactionModel> = transactionHelper.buildNewTransactionModel(
      transactionInput,
      recommendService
    );
    it('should mark transaction declined if submission failed', async () => {
      const submissionTemplate: ITransactionSubmissionTemplate = {
        submitTransaction: async model => {
          throw new TransferAppError(
            `Test error while submitting transaction: ${model.id}!`,
            TransferFailureReasonActor.UNKNOWN,
            ERROR_CODE.INVALID_REQUEST
          );
        },
        isEligible: () => true
      };

      const model: ITransactionModel = {
        ...transactionModel,
        id: transactionId
      };

      const error = await transactionExecutionService
        .settleTransaction(model, submissionTemplate)
        .catch(error => error);

      await expect(error).toBeInstanceOf(RetryableTransferAppError);
      await expect(error.errorCode).toEqual(ERROR_CODE.TRANSACTION_FAILED);

      expect(transactionRepository.update).toBeCalledWith(transactionId, {
        ...model,
        status: TransactionStatus.DECLINED,
        journey: expect.arrayContaining([
          {
            status: TransferJourneyStatusEnum.TRANSFER_FAILURE,
            updatedAt: expect.anything()
          }
        ]),
        failureReason: {
          type: ERROR_CODE.INVALID_REQUEST,
          actor: TransferFailureReasonActor.UNKNOWN,
          detail: `Test error while submitting transaction: ${model.id}!`
        }
      });
    });

    it('should not mark transaction declined if submission failed', async () => {
      const submissionTemplate: ITransactionSubmissionTemplate = {
        submitTransaction: async _ => {
          throw new TransferAppError(
            'Test error!',
            TransferFailureReasonActor.UNKNOWN,
            ERROR_CODE.TRANSACTION_FAILED
          );
        },
        isEligible: () => true,
        declineOnSubmissionFailure: false
      };

      const model: ITransactionModel = {
        ...transactionModel,
        id: transactionId
      };

      const result = transactionExecutionService.settleTransaction(
        model,
        submissionTemplate
      );

      await expect(result).rejects.toEqual(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.TRANSACTION_FAILED
        )
      );

      expect(transactionRepository.update).not.toHaveBeenCalled();
    });

    it('should set status to declined if submit transaction to core banking is failed with mambu error', async () => {
      const submissionTemplate: ITransactionSubmissionTemplate = {
        submitTransaction: async _ => {
          return mapTransferAppError(
            {
              message: 'MAMBU_TRANSACTION_AUTHORIZATION_HOLD_EXPIRED',
              key: 'COREBANKING_DEBIT_FAILED',
              code: ''
            },
            ERROR_CODE.COREBANKING_DEBIT_FAILED,
            true
          );
        },
        isEligible: () => true
      };
      const model: ITransactionModel = {
        ...transactionModel,
        id: transactionId
      };
      let error;
      try {
        const result = await transactionExecutionService.settleTransaction(
          model,
          submissionTemplate
        );
      } catch (err) {
        error = err;
      }
      expect(error.errorCode).toEqual(ERROR_CODE.TRANSACTION_FAILED);
      expect(transactionRepository.update).toBeCalledWith(transactionId, {
        ...model,
        status: TransactionStatus.DECLINED,
        journey: expect.arrayContaining([
          {
            status: TransferJourneyStatusEnum.TRANSFER_FAILURE,
            updatedAt: expect.anything()
          }
        ]),
        failureReason: {
          type: ERROR_CODE.COREBANKING_DEBIT_FAILED,
          actor: TransferFailureReasonActor.UNKNOWN,
          detail: 'MAMBU_TRANSACTION_AUTHORIZATION_HOLD_EXPIRED'
        }
      });
    });
  });

  describe('executeQrisTransaction', () => {
    let mockUpdateTH;
    let mockUpdateMambu;
    let mockServiceRecommendation;
    let mockNnsMapping;
    let mockSubmitToAlto;
    let mockMakeWithdrawal;
    let mockGetTransaction;
    let mockFindTransaction;
    let mockUpdateTransaction;
    let input: ITransactionInput;

    beforeEach(() => {
      // Mocks
      mockServiceRecommendation = transactionRecommendService.getRecommendService as jest.Mock;
      mockNnsMapping = configurationRepository.getNNSMapping as jest.Mock;
      mockSubmitToAlto = altoRepository.submitTransaction as jest.Mock;
      mockMakeWithdrawal = depositRepository.makeWithdrawal as jest.Mock;
      mockGetTransaction = depositRepository.getTransactionsByInstructionId as jest.Mock;
      mockUpdateTH = transactionHistoryRepository.updateTransactionHistory as jest.Mock;
      mockUpdateMambu = depositRepository.patchTransactionDetails as jest.Mock;
      mockFindTransaction = transactionRepository.findOneByQuery as jest.Mock;
      mockUpdateTransaction = transactionRepository.update as jest.Mock;

      // Entities
      input = {
        ...transactionInput,
        paymentServiceType: PaymentServiceTypeEnum.QRIS,
        paymentServiceCode: 'QRIS_PAYOUTS',
        interchange: BankNetworkEnum.ALTO,
        externalId: '1234567890',
        beneficiaryAccountNo: undefined,
        beneficiaryAccountName: undefined,
        transactionId: undefined,
        additionalPayload: {
          dateTime: '2022-02-22T08:23:08.168Z',
          customerReferenceNumber: 'PAY20220223001',
          authorizationId: 'B5A561',
          currencyCode: 'IDR',
          amount: 100500,
          fee: 500,
          issuerNns: '93600919',
          acquirerNns: '93600009',
          nationalMid: 'IN1019000000024',
          additionalData: 'ABCDEFGHIJ',
          terminalLabel: 'K19',
          forwardingCustomerReferenceNumber: '332692679408',
          merchant: {
            pan: '936011010099879098',
            id: '999540008',
            criteria: 'UME',
            name: 'Somay Mangga Dua',
            city: 'Jakarta',
            mcc: '6600',
            postalCode: '10110',
            countryCode: 'ID'
          },
          customer: {
            pan: '9360002319993788396',
            name: 'Sebastian',
            accountType: AltoAccountTypeEnum.SAVING
          }
        }
      };
      const qrisServiceRecommendation = {
        paymentServiceCode: 'QRIS_PAYOUTS',
        beneficiaryBankCodeType: BankCodeTypes.RTOL,
        transactionAuthenticationChecking: false,
        debitTransactionCode: [
          {
            transactionCode: 'QRD01',
            interchange: 'ALTO',
            transactionCodeInfo: {
              code: 'QRD01',
              channel: 'QRD01',
              defaultCategoryCode: 'C077',
              minAmountPerTx: 1,
              maxAmountPerTx: 10000000,
              limitGroupCode: 'L018'
            }
          }
        ],
        creditTransactionCode: [],
        blocking: false,
        requireThirdPartyOutgoingId: true,
        generalRefundable: true
      };

      // Given
      mockMakeWithdrawal.mockImplementation(() =>
        getQrisWithdrawMambuResponse()
      );
      mockGetTransaction.mockImplementation(() => [
        getQrisWithdrawMambuResponse()
      ]);
      mockSubmitToAlto.mockImplementation(() =>
        mockSuccessQrisPaymentResponse()
      );
      mockServiceRecommendation.mockResolvedValueOnce(
        qrisServiceRecommendation
      );
      mockNnsMapping.mockResolvedValue(getNNSMappingData());
      mockFindTransaction.mockImplementation(model => {
        return {
          ...model,
          id: '123',
          createdAt: '2023-01-01T12:00:00Z',
          updatedAt: '2023-01-01T12:00:00Z'
        };
      });
      mockUpdateTransaction.mockImplementation((id, model) => {
        var result = input;
        Object.keys(model).forEach(key => (result[key] = model[key]));
        return {
          ...result,
          id: transactionId,
          createdAt: new Date('01 June 2020 00:00:00'),
          updatedAt: '2023-01-01T12:00:00Z'
        };
      });
    });

    const mpan = '936011010099879098';
    const lundCheckDigit = qrisEncryption.getCheckDigit(mpan);
    const expectedBeneficiaryAccountNumber = `${mpan}${lundCheckDigit}`;

    const altoFCRN = '077879547900';
    const altoReferenceNumber = '7879547900';

    it('given submission to Alto is successful, then transaction status should be SUCCEED', async () => {
      process.env.QRIS_PATCH_MAMBU_ENABLED = 'true';

      // when
      const result = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(
        Rail.ALTO_QRIS.toString()
      );
      expect(mockUpdateTransaction).toHaveBeenCalledTimes(2);
      const submittingTrx = mockUpdateTransaction.mock.calls[0][1];
      expect(submittingTrx).toMatchObject({
        status: TransactionStatus.SUBMITTING
      });
      const succeededTrx = mockUpdateTransaction.mock.calls[1][1];
      expect(succeededTrx).toMatchObject({
        status: TransactionStatus.SUCCEED,
        beneficiaryAccountName: 'Somay Mangga Dua',
        beneficiaryAccountNo: expectedBeneficiaryAccountNumber,
        beneficiaryAccountType: '6600',
        beneficiaryBankName: '93601101',
        thirdPartyOutgoingId: altoReferenceNumber,
        additionalInformation1: 'BRI',
        additionalInformation2: '9360002319993788398',
        additionalInformation3: 'Jakarta',
        additionalInformation4: altoFCRN
      });

      expect(mockUpdateTH).toBeCalledWith('mbu_001', 'SUCCEED', altoFCRN);
      expect(mockUpdateMambu).toBeCalledWith('mbu_001', [
        {
          op: PatchOperation.REPLACE,
          path: '/_Custom_Transaction_Details/Third_Party_Outgoing_Id',
          value: altoReferenceNumber
        }
      ]);

      expect(configurationRepository.getNNSMapping).toHaveBeenCalledTimes(1);
      expect(altoRepository.submitTransaction).toHaveBeenCalledTimes(1);
      expect(depositRepository.createGlJournalEntries).not.toHaveBeenCalled();

      delete process.env.QRIS_PATCH_MAMBU_ENABLED;
    });

    it('given submission to Alto throwing QRIS_TRANSACTION_TIMEOUT with FCRN provided, then transaction status should be SUBMITTED', async () => {
      process.env.QRIS_PATCH_MAMBU_ENABLED = 'true';

      // the scenario is Alto responding with 002 SUSPECT Response Code.
      // given
      const altoFCRN = '000001000001';
      const altoReferenceNumber = '0001000001';
      mockSubmitToAlto.mockRejectedValueOnce(
        new QrisError(
          'Test error!',
          TransferFailureReasonActor.ALTO,
          ERROR_CODE.QRIS_TRANSACTION_TIMEOUT,
          QrisTransactionStatusEnum.SUSPECT,
          altoFCRN
        )
      );

      // when
      await transactionExecutionService.executeTransaction(input);

      // then
      expect(mockUpdateTransaction).toHaveBeenCalledTimes(3);
      const submittingTrx = mockUpdateTransaction.mock.calls[0][1];
      expect(submittingTrx).toMatchObject({
        status: TransactionStatus.SUBMITTING
      });
      const updatedTrx1 = mockUpdateTransaction.mock.calls[1][1];
      expect(updatedTrx1).toMatchObject({
        beneficiaryAccountName: 'Somay Mangga Dua',
        beneficiaryAccountNo: expectedBeneficiaryAccountNumber,
        beneficiaryAccountType: '6600',
        beneficiaryBankName: '93601101',
        thirdPartyOutgoingId: altoReferenceNumber,
        additionalInformation1: 'BRI',
        additionalInformation2: '9360002319993788398',
        additionalInformation3: 'Jakarta',
        additionalInformation4: altoFCRN
      });
      const updatedTrx = mockUpdateTransaction.mock.calls[2][1];
      expect(updatedTrx).toMatchObject({
        status: TransactionStatus.SUBMITTED
      });
      // never reversed
      expect(depositRepository.reverseTransaction).not.toHaveBeenCalled();
      // external transaction details is updated
      expect(mockUpdateTH).toBeCalledWith('mbu_001', 'PENDING', altoFCRN);
      expect(mockUpdateMambu).toBeCalledWith('mbu_001', [
        {
          op: PatchOperation.REPLACE,
          path: '/_Custom_Transaction_Details/Third_Party_Outgoing_Id',
          value: altoReferenceNumber
        }
      ]);

      delete process.env.QRIS_PATCH_MAMBU_ENABLED;
    });

    it('given submission to Alto is failed and Mambu reversal is failed, then transaction status should be SUBMITTED', async () => {
      process.env.QRIS_PATCH_MAMBU_ENABLED = 'true';

      // given
      const altoFCRN = '000001000001';
      const altoReferenceNumber = '0001000001';
      mockSubmitToAlto.mockImplementation(() => {
        throw new QrisError(
          'Test error!',
          TransferFailureReasonActor.ALTO,
          ERROR_CODE.ERROR_FROM_ALTO,
          QrisTransactionStatusEnum.FAILED,
          altoFCRN
        );
      });

      const spyOnRevertFailedQrisTransaction = spyOn(
        transactionReversalHelper,
        'revertFailedTransactionWithoutError'
      ).and.callFake(() => false);

      // when
      const result = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(
        Rail.ALTO_QRIS.toString()
      );
      expect(spyOnRevertFailedQrisTransaction).toHaveBeenCalledTimes(1);
      expect(mockUpdateTransaction).toHaveBeenCalledTimes(3);
      const submittingTrx = mockUpdateTransaction.mock.calls[0][1];
      expect(submittingTrx).toMatchObject({
        status: TransactionStatus.SUBMITTING
      });
      const updatedTrx = mockUpdateTransaction.mock.calls[1][1];
      expect(updatedTrx).toMatchObject({
        beneficiaryAccountName: 'Somay Mangga Dua',
        beneficiaryAccountNo: expectedBeneficiaryAccountNumber,
        beneficiaryAccountType: '6600',
        beneficiaryBankName: '93601101',
        thirdPartyOutgoingId: altoReferenceNumber,
        additionalInformation1: 'BRI',
        additionalInformation2: '9360002319993788398',
        additionalInformation3: 'Jakarta',
        additionalInformation4: altoFCRN
      });
      const submittedTrx = mockUpdateTransaction.mock.calls[2][1];
      expect(submittedTrx).toEqual({
        status: TransactionStatus.SUBMITTED,
        journey: [
          {
            status: 'RECOMMENDATION_FETCHED',
            updatedAt: expect.any(Date)
          },
          {
            status: 'AMOUNT_DEBITED',
            updatedAt: expect.any(Date)
          },
          {
            status: 'SUBMITTING_THIRD_PARTY',
            updatedAt: expect.any(Date)
          },
          {
            status: 'THIRD_PARTY_SUBMITTED',
            updatedAt: expect.any(Date)
          }
        ],
        processingInfo: expect.objectContaining({
          paymentRail: Rail.ALTO_QRIS.toString()
        })
      });
      // external transaction details is updated
      expect(mockUpdateTH).toBeCalledWith('mbu_001', 'FAILED', altoFCRN);
      expect(mockUpdateMambu).toBeCalledWith('mbu_001', [
        {
          op: PatchOperation.REPLACE,
          path: '/_Custom_Transaction_Details/Third_Party_Outgoing_Id',
          value: altoReferenceNumber
        }
      ]);

      delete process.env.QRIS_PATCH_MAMBU_ENABLED;
    });

    it('given submission to Alto is failed and Mambu reversal is successful, then transaction status should be DECLINED', async () => {
      process.env.QRIS_PATCH_MAMBU_ENABLED = 'true';

      // given
      const altoFCRN = '000001000001';
      const altoReferenceNumber = '0001000001';
      mockSubmitToAlto.mockImplementation(() => {
        throw new QrisError(
          'Test error!',
          TransferFailureReasonActor.ALTO,
          ERROR_CODE.ERROR_FROM_ALTO,
          QrisTransactionStatusEnum.FAILED,
          altoFCRN
        );
      });

      const spyOnRevertFailedQrisTransaction = spyOn(
        transactionReversalHelper,
        'revertFailedTransactionWithoutError'
      ).and.callFake(() => true);

      // when
      await transactionExecutionService.executeTransaction(input).catch(e => e);

      // then
      expect(spyOnRevertFailedQrisTransaction).toHaveBeenCalledTimes(1);
      expect(mockUpdateTransaction).toHaveBeenCalledTimes(3);
      const submittingTrx = mockUpdateTransaction.mock.calls[0][1];
      expect(submittingTrx).toMatchObject({
        status: TransactionStatus.SUBMITTING
      });
      const updatedTrx = mockUpdateTransaction.mock.calls[1][1];
      expect(updatedTrx).toMatchObject({
        beneficiaryAccountName: 'Somay Mangga Dua',
        beneficiaryAccountNo: expectedBeneficiaryAccountNumber,
        beneficiaryAccountType: '6600',
        beneficiaryBankName: '93601101',
        thirdPartyOutgoingId: altoReferenceNumber,
        additionalInformation1: 'BRI',
        additionalInformation2: '9360002319993788398',
        additionalInformation3: 'Jakarta',
        additionalInformation4: altoFCRN
      });
      const declinedTrx = mockUpdateTransaction.mock.calls[2][1];
      expect(declinedTrx).toEqual({
        status: TransactionStatus.DECLINED,
        journey: [
          {
            status: 'RECOMMENDATION_FETCHED',
            updatedAt: expect.any(Date)
          },
          {
            status: 'AMOUNT_DEBITED',
            updatedAt: expect.any(Date)
          },
          {
            status: 'SUBMITTING_THIRD_PARTY',
            updatedAt: expect.any(Date)
          },
          {
            status: 'TRANSFER_FAILURE',
            updatedAt: expect.any(Date)
          }
        ],
        failureReason: {
          type: ERROR_CODE.ERROR_FROM_ALTO,
          actor: TransferFailureReasonActor.UNKNOWN,
          detail: ERROR_CODE.ERROR_FROM_ALTO
        }
      });
      // external transaction details is updated
      expect(mockUpdateTH).toBeCalledWith('mbu_001', 'FAILED', altoFCRN);
      expect(mockUpdateMambu).toBeCalledWith('mbu_001', [
        {
          op: PatchOperation.REPLACE,
          path: '/_Custom_Transaction_Details/Third_Party_Outgoing_Id',
          value: altoReferenceNumber
        }
      ]);

      delete process.env.QRIS_PATCH_MAMBU_ENABLED;
    });

    it('given Mambu debit is failed, then transaction status should be DECLINED and should not call Mambu reversal', async () => {
      // given
      mockedRetry.mockRestore();
      mockMakeWithdrawal.mockRejectedValueOnce(new NetworkError());

      // when
      await transactionExecutionService.executeTransaction(input).catch(e => e);

      // then
      expect(mockSubmitToAlto).not.toHaveBeenCalled();
      expect(depositRepository.reverseTransaction).not.toHaveBeenCalled();
      expect(mockUpdateTransaction).toHaveBeenCalledTimes(2);
      const updatedTrxFirst = mockUpdateTransaction.mock.calls[0][1];
      expect(updatedTrxFirst).toMatchObject({
        status: TransactionStatus.PENDING,
        beneficiaryAccountName: 'Somay Mangga Dua',
        beneficiaryAccountNo: expectedBeneficiaryAccountNumber,
        beneficiaryAccountType: '6600',
        beneficiaryBankName: '93601101',
        thirdPartyOutgoingId: input.externalId,
        additionalInformation1: 'BRI',
        additionalInformation2: '9360002319993788398',
        additionalInformation3: 'Jakarta'
      });
      const updatedTrxSecond = mockUpdateTransaction.mock.calls[1][1];
      expect(updatedTrxSecond).toEqual({
        status: TransactionStatus.DECLINED,
        journey: [
          {
            status: 'RECOMMENDATION_FETCHED',
            updatedAt: expect.any(Date)
          },
          {
            status: 'TRANSFER_FAILURE',
            updatedAt: expect.any(Date)
          }
        ],
        failureReason: {
          actor: 'UNKNOWN',
          detail: 'Error while debiting money, code: undefined, message: !',
          type: ERROR_CODE.COREBANKING_DEBIT_FAILED
        }
      });
    });

    it('should submit QRIS transaction without submit GL journal if there is no BANK_INCOME', async () => {
      // when
      const result = await transactionExecutionService.executeTransaction(
        input
      );

      // then
      expect(result.processingInfo?.paymentRail).toBe(
        Rail.ALTO_QRIS.toString()
      );
      expect(altoRepository.submitTransaction).toHaveBeenCalledTimes(1);
      expect(depositRepository.makeWithdrawal).toHaveBeenCalledTimes(1);
      expect(depositRepository.createGlJournalEntries).not.toHaveBeenCalled();
      expect(mockUpdateTH).toBeCalledWith('mbu_001', 'SUCCEED', altoFCRN);
    });

    it(`given the additionalPayload is undefined, then should throw error ADDITIONAL_PAYLOAD_NOT_EXIST`, async () => {
      // given
      delete input.additionalPayload;

      // when
      const result = transactionExecutionService.executeTransaction(input);

      // then
      await expect(result).rejects.toEqual(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.ADDITIONAL_PAYLOAD_NOT_EXIST
        )
      );
    });

    it('should retry the requests to revert transactions if they are failed by network error', async () => {
      // given
      mockedRetry.mockRestore();
      (depositRepository.makeDeposit as jest.Mock).mockRejectedValueOnce(
        new NetworkError()
      );
      mockMakeWithdrawal.mockResolvedValueOnce({
        id: 'withdraw_id'
      });
      (depositRepository.reverseTransaction as jest.Mock).mockRejectedValue(
        new NetworkError()
      );
      mockSubmitToAlto.mockImplementation(() => {
        throw new NetworkError();
      });

      // when
      await transactionExecutionService.executeTransaction(input);

      // then
      expect(depositRepository.reverseTransaction).toBeCalledTimes(1);
      expect(
        depositRepository.getTransactionsByInstructionId
      ).toHaveBeenCalled();
    });
  });

  describe('executeCheckStatus', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should skipping check status if submission template doesnt have check status method', async () => {
      // Given
      const transactionModel = {
        ...getTransactionDetail()
      };
      outgoingStatusTemplateService.getStatusTemplate = jest
        .fn()
        .mockReturnValue(undefined);

      // Then
      const result = await transactionExecutionService.executeCheckStatus(
        transactionModel
      );

      // Expect
      expect(result).toEqual({ status: transactionModel.status });
    });

    it('should do check status if submission template have check status method', async () => {
      // Given
      const transactionModel = {
        ...getTransactionDetail()
      };
      const template: ITransactionStatusTemplate = {
        getTransactionStatus: async transactionModel => transactionModel,
        isEligible: () => true
      };
      outgoingStatusTemplateService.getStatusTemplate = jest
        .fn()
        .mockReturnValue(template);
      template.getTransactionStatus = jest
        .fn()
        .mockReturnValue({ status: TransactionStatus.SUCCEED });

      // Then
      const result = await transactionExecutionService.executeCheckStatus(
        transactionModel
      );

      // Expect
      expect(result).toEqual({ status: TransactionStatus.SUCCEED });
      expect(template.getTransactionStatus).toBeDefined();
      expect(template.getTransactionStatus).toBeCalledTimes(1);
      expect(template.getTransactionStatus).toBeCalledWith(transactionModel);
    });
  });

  describe('executeTransaction: suspected abuse', () => {
    it('should not decline transaction if there is no suspected abuse', async () => {
      // Given
      const usageCounterPayload = [
        {
          limitGroupCode: 'd01',
          adjustmentLimitAmount: transactionInput.transactionAmount
        }
      ];
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          debitTransactionCode: [
            {
              ...recommendService.debitTransactionCode[0],
              limitGroupCode: 'd01'
            }
          ]
        }
      );

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce({
        customerId: sourceAccount.customerId,
        data: usageCounterPayload
      });

      (isSuspectedAbuse as jest.Mock).mockResolvedValueOnce(false);

      // when
      const executed = await transactionExecutionService.executeTransaction(
        transactionInput
      );

      // then
      expect(executed.status).toBe(TransactionStatus.SUCCEED);
      expect(executed.processingInfo?.paymentRail).toBe(
        Rail.INTERNAL.toString()
      );

      expect(transactionRepository.create).not.toHaveBeenCalled();

      expect(transactionRepository.update).toBeCalledWith(
        transactionId,
        expect.objectContaining({
          status: TransactionStatus.SUCCEED
        })
      );

      expect(awardRepository.updateUsageCounters).toHaveBeenCalledTimes(1);
      expect(awardRepository.updateUsageCounters).toBeCalledWith(
        sourceAccount.customerId,
        usageCounterPayload
      );
      expect(transactionProducer.sendTransactionSucceededMessage).toBeCalled();
      expect(monitoringEventProducer.sendMonitoringEventMessage).toBeCalledWith(
        MonitoringEventType.PROCESSING_FINISHED,
        expect.objectContaining({
          id: transactionId,
          status: TransactionStatus.SUCCEED
        })
      );
      expect(transactionRepository.update).toBeCalledTimes(2);
    });

    it('should decline transaction if suspected abuse', async () => {
      // Given
      const usageCounterPayload = [
        {
          limitGroupCode: 'd01',
          adjustmentLimitAmount: transactionInput.transactionAmount
        }
      ];
      (transactionRecommendService.getRecommendService as jest.Mock).mockResolvedValueOnce(
        {
          ...recommendService,
          debitTransactionCode: [
            {
              ...recommendService.debitTransactionCode[0],
              limitGroupCode: 'd01'
            }
          ]
        }
      );

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce({
        customerId: sourceAccount.customerId,
        data: usageCounterPayload
      });

      (isSuspectedAbuse as jest.Mock).mockResolvedValueOnce(true);

      let error;
      // when
      try {
        await transactionExecutionService.executeTransaction(transactionInput);
      } catch (err) {
        error = err;
      }

      // then
      expect(error.errorCode).toBe(ERROR_CODE.INVALID_REQUEST);

      const detail = `Suspected abuse for
          paymentServiceCode: 1
          customerId: source_customer_id
          beneficiaryAccountNo: 1.`;
      expect(transactionService.updateFailedTransaction).toBeCalledWith(
        expect.anything(),
        expect.objectContaining({
          detail,
          actor: TransferFailureReasonActor.SUBMITTER
        })
      );
    });
  });
});
