import { getTransactionDetail } from '../__mocks__/transaction.data';
import { ITransactionModel } from '../transaction.model';
import { BankChannelEnum } from '@dk/module-common';
import submissionTemplateService from '../transactionSubmissionTemplate.service';

describe('getSubmissionTemplate', () => {
  it('should return transaction submission template', () => {
    // given
    const model: ITransactionModel = {
      ...getTransactionDetail(),
      beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL
    };

    // when
    const template = submissionTemplateService.getSubmissionTemplate(model);

    // then
    expect(template).not.toBeNull();
  });

  it('should return undefined when there is no transaction template found', () => {
    // given
    const model: ITransactionModel = {
      ...getTransactionDetail(),
      beneficiaryBankCodeChannel: null
    };

    // expect
    expect(
      submissionTemplateService.getSubmissionTemplate(model)
    ).not.toBeDefined();
  });
});
