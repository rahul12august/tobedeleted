import transactionHelper from '../transaction.helper';
import {
  ITransactionAccount,
  ITransactionInput,
  RecommendationService,
  TransactionListRequest
} from '../transaction.type';
import accountRepository from '../../account/account.repository';
import {
  TransactionStatus,
  TransferJourneyStatusEnum,
  TransferFailureReasonActor
} from '../transaction.enum';
import awardRepository from '../../award/award.repository';
import feeService from '../../fee/fee.service';
import { generateUniqueId } from '../transaction.util';
import { TransferAppError } from '../../errors/AppError';
import {
  AuthenticationType,
  BankChannelEnum,
  BankNetworkEnum,
  PaymentServiceTypeEnum,
  TransactionExecutionTypeEnum,
  Rail
} from '@dk/module-common';
import configurationRepository from '../../configuration/configuration.repository';
import { ERROR_CODE } from '../../common/errors';
import { IBankCode } from '../../configuration/configuration.type';
import transferRepository from '../transaction.repository';
import { ITransactionModel } from '../transaction.model';
import {
  getITransactionInput,
  getRecommendedService,
  getTransactionModelData,
  getTransactionModelFullData,
  recommendedServicesWithAwardGroup,
  TransactionRecommendationInput
} from '../__mocks__/transaction.data';
import accountService from '../../account/account.service';
import moment from 'moment';
import transactionRepository from '../transaction.repository';
import { FeeAmountInput } from '../../fee/fee.type';
import {
  ExecutionMethodEnum,
  REVERSAL_TC_SUFFIX,
  RTOL_BIFAST_FEE
} from '../transaction.constant';
import { TransactionTopicConstant } from '@dk/module-message';

jest.mock('../../common/featureFlag');
jest.mock('../../account/account.cache');
jest.mock('../../configuration/configuration.repository');
jest.mock('../../account/account.service');
jest.mock('../../account/account.repository');
jest.mock('../../fee/fee.service');
jest.mock('../../award/award.repository');
jest.mock('../transaction.util', () => {
  const original = require.requireActual('../transaction.util');
  return {
    ...original,
    generateUniqueId: jest.fn()
  };
});
jest.mock('../../coreBanking/deposit.repository');
jest.mock('../transaction.repository');
jest.mock('../../configuration/configuration.service');
jest.mock('../../customer/customer.cache');

describe('transactionHelper', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('mapAuthenticationRequired', () => {
    const sourceAccountNo = '123';
    const transactionAmount = 1;
    it('should return recommendedServices with AuthenticationType is PIN', async () => {
      // Given
      const authenticationType = AuthenticationType.PIN;
      const recommendedServices = [
        {
          transactionAuthenticationChecking: true
        }
      ];
      const expected = [
        {
          ...recommendedServices[0],
          authenticationType
        }
      ];

      (accountService.authenticateTransactionAmount as jest.Mock).mockResolvedValueOnce(
        false
      );

      // When
      const result = await transactionHelper.mapAuthenticationRequired(
        recommendedServices as RecommendationService[],
        transactionAmount,
        sourceAccountNo
      );

      // Then
      expect(result).toEqual(expected);
      expect(accountService.authenticateTransactionAmount).toBeCalledWith(
        sourceAccountNo,
        transactionAmount
      );
    });
    it('should return recommendedServices with AuthenticationType is PASSWORD', async () => {
      // Given
      const authenticationType = AuthenticationType.PASSWORD;
      const recommendedServices = [
        {
          transactionAuthenticationChecking: true
        }
      ];
      const expected = [
        {
          ...recommendedServices[0],
          authenticationType
        }
      ];

      (accountService.authenticateTransactionAmount as jest.Mock).mockResolvedValueOnce(
        true
      );

      // When
      const result = await transactionHelper.mapAuthenticationRequired(
        recommendedServices as RecommendationService[],
        transactionAmount,
        sourceAccountNo
      );

      // Then
      expect(result).toEqual(expected);
      expect(accountService.authenticateTransactionAmount).toBeCalledWith(
        sourceAccountNo,
        transactionAmount
      );
    });

    it('should return recommendedServices with AuthenticationType is NONE', async () => {
      // Given
      const recommendedServices = [
        {
          transactionAuthenticationChecking: false
        }
      ];
      const expected = [
        {
          ...recommendedServices[0],
          authenticationType: AuthenticationType.NONE
        }
      ];

      // When
      const result = await transactionHelper.mapAuthenticationRequired(
        recommendedServices as RecommendationService[],
        transactionAmount,
        sourceAccountNo
      );

      // Then
      expect(result).toEqual(expected);
      expect(accountService.authenticateTransactionAmount).not.toBeCalled();
    });

    it('should return origin recommendedServices if sourceAccountNo is not available', async () => {
      // Given
      const recommendedServices = [
        {
          transactionAuthenticationChecking: false
        }
      ];

      // When
      const result = await transactionHelper.mapAuthenticationRequired(
        recommendedServices as RecommendationService[],
        transactionAmount,
        undefined
      );

      // Then
      expect(result).toBe(recommendedServices);
      expect(accountService.authenticateTransactionAmount).not.toBeCalled();
    });
  });

  describe('mapFeeData', () => {
    const limitGroupCode = 'c01';
    const feeRules = '1';
    const defaultRecommendedServices: RecommendationService[] = [
      {
        ...getRecommendedService(),
        debitTransactionCode: [
          {
            transactionCode: 'd01',
            transactionCodeInfo: {
              code: 'd01',
              channel: 'd01_channel',
              defaultCategoryCode: 'defaultD01',
              minAmountPerTx: 100,
              maxAmountPerTx: 9999999,
              limitGroupCode,
              feeRules
            }
          }
        ],
        feeData: undefined
      }
    ];

    it('should return services with fee data', async () => {
      const usageCounter = {
        limitGroupCode: limitGroupCode,
        dailyAccumulationAmount: 0,
        monthlyAccumulationTransaction: 1
      };
      const usageCounters = [usageCounter];
      const transactionAmount = 1;
      const sourceCustomerId = '1';
      const fee = { fee: 1 };
      const beneficiaryBankCode = 'BC00312';
      const externalId = 'externalId';
      const expectedData = [
        {
          ...defaultRecommendedServices[0],
          feeData: [fee]
        }
      ];
      const input: TransactionRecommendationInput = {
        transactionAmount,
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        sourceCustomerId,
        beneficiaryBankCode,
        externalId,
        beneficiaryBankCodeChannel: BankChannelEnum.ANY
      };

      (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(fee);

      const result = await transactionHelper.mapFeeData(
        defaultRecommendedServices,
        input,
        usageCounters
      );

      const expectedFeeAmountInput: FeeAmountInput = {
        externalId,
        transactionAmount,
        feeRuleCode: feeRules,
        monthlyNoTransaction: usageCounter.monthlyAccumulationTransaction,
        interchange: undefined,
        targetBankCode: beneficiaryBankCode,
        beneficiaryBankCodeChannel: BankChannelEnum.ANY,
        refund: undefined,
        thresholdCounter: undefined,
        customerId: sourceCustomerId,
        awardGroupCounter: undefined,
        feeAmount: undefined,
        limitGroupCode: 'c01',
        usageCounters: [
          {
            dailyAccumulationAmount: 0,
            limitGroupCode: 'c01',
            monthlyAccumulationTransaction: 1
          }
        ]
      };
      expect(result).toEqual(expectedData);
      expect(feeService.getFeeAmount).toHaveBeenCalledWith(
        expectedFeeAmountInput
      );
    });

    it('should return fee data when usage counter is get from award group counter', async () => {
      const usageCounter = {
        limitGroupCode: limitGroupCode,
        dailyAccumulationAmount: 0,
        monthlyAccumulationTransaction: 1
      };
      const usageCounters = [usageCounter];
      const transactionAmount = 1;
      const sourceCustomerId = '1';
      const fee = { fee: 1 };
      const targetBankCode = 'BC00312';
      const externalId = 'externalId';
      const beneficiaryBankCodeChannel = BankChannelEnum.ANY;
      const expectedData = [
        {
          ...recommendedServicesWithAwardGroup[0],
          feeData: [fee]
        }
      ];

      const input: TransactionRecommendationInput = {
        transactionAmount,
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        sourceCustomerId,
        beneficiaryBankCode: targetBankCode,
        externalId,
        beneficiaryBankCodeChannel
      };

      (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(fee);

      const result = await transactionHelper.mapFeeData(
        recommendedServicesWithAwardGroup,
        input,
        usageCounters
      );

      const expectedFeeAmountInput: FeeAmountInput = {
        feeRuleCode: feeRules,
        transactionAmount: transactionAmount,
        monthlyNoTransaction: usageCounter.monthlyAccumulationTransaction,
        interchange: undefined,
        targetBankCode: targetBankCode,
        externalId: externalId,
        beneficiaryBankCodeChannel: beneficiaryBankCodeChannel,
        refund: undefined,
        thresholdCounter: undefined,
        customerId: sourceCustomerId,
        limitGroupCode: 'c01',
        usageCounters: [
          {
            dailyAccumulationAmount: 0,
            limitGroupCode: 'c01',
            monthlyAccumulationTransaction: 1
          }
        ],
        awardGroupCounter: 'Bonus_Transfer',
        feeAmount: undefined
      };

      expect(result).toEqual(expectedData);
      expect(feeService.getFeeAmount).toHaveBeenCalledWith(
        expectedFeeAmountInput
      );
    });

    it('should return services with fee data with refund original transaction ', async () => {
      const usageCounter = {
        limitGroupCode: limitGroupCode,
        dailyAccumulationAmount: 0,
        monthlyAccumulationTransaction: 1
      };
      const usageCounters = [usageCounter];
      const thresholdCounter = { thresholdCounter: 'AWARD_TRANSFER_001' };
      const transactionAmount = 1;
      const sourceCustomerId = '1';
      const fee = { fee: 1 };
      const targetBankCode = 'BC00312';
      const externalId = 'externalId';
      const beneficiaryBankCodeChannel = BankChannelEnum.ANY;
      const expectedData = [
        {
          ...defaultRecommendedServices[0],
          debitTransactionCode: undefined,
          creditTransactionCode: [
            {
              transactionCode: 'd01',
              transactionCodeInfo: {
                code: 'd01',
                channel: 'd01_channel',
                defaultCategoryCode: 'defaultD01',
                minAmountPerTx: 100,
                maxAmountPerTx: 9999999,
                limitGroupCode,
                feeRules,
                awardGroupCounter: 'awardGroupCounter1'
              }
            }
          ],
          feeData: [fee]
        }
      ];
      const input: TransactionRecommendationInput = {
        transactionAmount,
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        sourceCustomerId,
        beneficiaryBankCode: targetBankCode,
        externalId,
        beneficiaryBankCodeChannel,
        refund: { originalTransactionId: 'originalTransactionId' }
      };
      (awardRepository.getThresholdCounter as jest.Mock).mockResolvedValueOnce(
        thresholdCounter
      );
      (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(fee);

      const result = await transactionHelper.mapFeeData(
        [
          {
            ...defaultRecommendedServices[0],
            debitTransactionCode: undefined,
            creditTransactionCode: [
              {
                transactionCode: 'd01',
                transactionCodeInfo: {
                  code: 'd01',
                  channel: 'd01_channel',
                  defaultCategoryCode: 'defaultD01',
                  minAmountPerTx: 100,
                  maxAmountPerTx: 9999999,
                  limitGroupCode,
                  feeRules,
                  awardGroupCounter: 'awardGroupCounter1'
                }
              }
            ]
          }
        ],
        input,
        usageCounters
      );

      const expectedFeeAmountInput: FeeAmountInput = {
        feeRuleCode: feeRules,
        transactionAmount: transactionAmount,
        feeAmount: undefined,
        monthlyNoTransaction: undefined,
        interchange: undefined,
        targetBankCode: targetBankCode,
        externalId: externalId,
        beneficiaryBankCodeChannel: beneficiaryBankCodeChannel,
        refund: { originalTransactionId: 'originalTransactionId' },
        thresholdCounter: undefined,
        customerId: sourceCustomerId,
        awardGroupCounter: 'awardGroupCounter1',
        limitGroupCode: 'c01',
        usageCounters: [
          {
            dailyAccumulationAmount: 0,
            limitGroupCode: 'c01',
            monthlyAccumulationTransaction: 1
          }
        ]
      };

      expect(result).toEqual(expectedData);
      expect(awardRepository.getUsageCounters).not.toBeCalled();
      expect(awardRepository.getThresholdCounter).not.toBeCalled();
      expect(feeService.getFeeAmount).toHaveBeenCalledWith(
        expectedFeeAmountInput
      );
    });

    it('should return services with fee data with OFFER paymentServiceType', async () => {
      const usageCounter = {
        limitGroupCode: limitGroupCode,
        dailyAccumulationAmount: 0,
        monthlyAccumulationTransaction: 1
      };
      const usageCounters = [usageCounter];
      const transactionAmount = 1;
      const sourceCustomerId = 'customerId';
      const fee = { fee: 1 };
      const targetBankCode = 'BC00312';
      const externalId = 'externalId';
      const beneficiaryBankCodeChannel = BankChannelEnum.ANY;
      const expectedData = [
        {
          ...defaultRecommendedServices[0],
          debitTransactionCode: undefined,
          creditTransactionCode: [
            {
              transactionCode: 'c01',
              transactionCodeInfo: {
                code: 'c01',
                channel: 'c01_channel',
                defaultCategoryCode: 'defaultC01',
                minAmountPerTx: 100,
                maxAmountPerTx: 9999999,
                limitGroupCode: 'c01',
                feeRules,
                awardGroupCounter: 'awardGroupCounter1'
              }
            }
          ],
          feeData: [fee]
        }
      ];
      (awardRepository.getThresholdCounter as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(fee);

      const input: TransactionRecommendationInput = {
        transactionAmount,
        paymentServiceType: PaymentServiceTypeEnum.OFFER,
        sourceCustomerId,
        beneficiaryBankCode: targetBankCode,
        externalId,
        beneficiaryBankCodeChannel
      };

      const result = await transactionHelper.mapFeeData(
        [
          {
            ...defaultRecommendedServices[0],
            debitTransactionCode: undefined,
            creditTransactionCode: [
              {
                transactionCode: 'c01',
                transactionCodeInfo: {
                  code: 'c01',
                  channel: 'c01_channel',
                  defaultCategoryCode: 'defaultC01',
                  minAmountPerTx: 100,
                  maxAmountPerTx: 9999999,
                  limitGroupCode: 'c01',
                  feeRules,
                  awardGroupCounter: 'awardGroupCounter1'
                }
              }
            ]
          }
        ],
        input,
        usageCounters
      );

      const expectedFeeAmountInput: FeeAmountInput = {
        feeRuleCode: feeRules,
        transactionAmount: transactionAmount,
        feeAmount: undefined,
        monthlyNoTransaction: 1,
        interchange: undefined,
        targetBankCode: targetBankCode,
        externalId: externalId,
        beneficiaryBankCodeChannel: beneficiaryBankCodeChannel,
        refund: undefined,
        thresholdCounter: undefined,
        customerId: sourceCustomerId,
        awardGroupCounter: 'awardGroupCounter1',
        limitGroupCode: 'c01',
        usageCounters: [
          {
            dailyAccumulationAmount: 0,
            limitGroupCode: 'c01',
            monthlyAccumulationTransaction: 1
          }
        ]
      };

      expect(result).toEqual(expectedData);
      expect(feeService.getFeeAmount).toHaveBeenCalledWith(
        expectedFeeAmountInput
      );
    });

    it('should return services with fee data with 0 fee for OFFER paymentServiceType', async () => {
      const usageCounter = {
        limitGroupCode: limitGroupCode,
        dailyAccumulationAmount: 0,
        monthlyAccumulationTransaction: 1
      };
      const usageCounters = [usageCounter];
      const transactionAmount = 1;
      const sourceCustomerId = 'customerId';
      const fee = { fee: 0 };
      const targetBankCode = 'BC00312';
      const externalId = 'externalId';
      const beneficiaryBankCodeChannel = BankChannelEnum.ANY;
      const expectedData = [
        {
          ...defaultRecommendedServices[0],
          debitTransactionCode: undefined,
          creditTransactionCode: [
            {
              interchange: BankNetworkEnum.NOTAX,
              transactionCode: 'c01',
              transactionCodeInfo: {
                code: 'c01',
                channel: 'c01_channel',
                defaultCategoryCode: 'defaultC01',
                minAmountPerTx: 100,
                maxAmountPerTx: 9999999,
                limitGroupCode: 'c01',
                feeRules,
                awardGroupCounter: 'awardGroupCounter1'
              }
            }
          ],
          feeData: [fee]
        }
      ];
      (awardRepository.getThresholdCounter as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(fee);

      const input: TransactionRecommendationInput = {
        transactionAmount,
        paymentServiceType: PaymentServiceTypeEnum.OFFER,
        sourceCustomerId,
        beneficiaryBankCode: targetBankCode,
        externalId,
        beneficiaryBankCodeChannel
      };

      const result = await transactionHelper.mapFeeData(
        [
          {
            ...defaultRecommendedServices[0],
            debitTransactionCode: undefined,
            creditTransactionCode: [
              {
                interchange: BankNetworkEnum.NOTAX,
                transactionCode: 'c01',
                transactionCodeInfo: {
                  code: 'c01',
                  channel: 'c01_channel',
                  defaultCategoryCode: 'defaultC01',
                  minAmountPerTx: 100,
                  maxAmountPerTx: 9999999,
                  limitGroupCode: 'c01',
                  feeRules,
                  awardGroupCounter: 'awardGroupCounter1'
                }
              }
            ]
          }
        ],
        input,
        usageCounters
      );

      const expectedFeeAmountInput: FeeAmountInput = {
        feeRuleCode: feeRules,
        transactionAmount: transactionAmount,
        feeAmount: undefined,
        monthlyNoTransaction: 1,
        interchange: BankNetworkEnum.NOTAX,
        targetBankCode: targetBankCode,
        externalId: externalId,
        beneficiaryBankCodeChannel: beneficiaryBankCodeChannel,
        refund: undefined,
        thresholdCounter: undefined,
        customerId: sourceCustomerId,
        awardGroupCounter: 'awardGroupCounter1',
        limitGroupCode: 'c01',
        usageCounters: [
          {
            dailyAccumulationAmount: 0,
            limitGroupCode: 'c01',
            monthlyAccumulationTransaction: 1
          }
        ]
      };

      expect(result).toEqual(expectedData);
      expect(feeService.getFeeAmount).toHaveBeenCalledWith(
        expectedFeeAmountInput
      );
    });

    it('should return services with fee data with BONUS_INTEREST paymentServiceType', async () => {
      const usageCounter = {
        limitGroupCode: limitGroupCode,
        dailyAccumulationAmount: 0,
        monthlyAccumulationTransaction: 1
      };
      const usageCounters = [usageCounter];
      const transactionAmount = 1;
      const sourceCustomerId = 'customerId';
      const fee = { fee: 1 };
      const targetBankCode = 'BC00312';
      const externalId = 'externalId';
      const beneficiaryBankCodeChannel = BankChannelEnum.ANY;
      const expectedData = [
        {
          ...defaultRecommendedServices[0],
          debitTransactionCode: undefined,
          creditTransactionCode: [
            {
              transactionCode: 'c01',
              transactionCodeInfo: {
                code: 'c01',
                channel: 'c01_channel',
                defaultCategoryCode: 'defaultC01',
                minAmountPerTx: 100,
                maxAmountPerTx: 9999999,
                limitGroupCode: 'c01',
                feeRules,
                awardGroupCounter: 'awardGroupCounter1'
              }
            }
          ],
          feeData: [fee]
        }
      ];
      (awardRepository.getThresholdCounter as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(fee);

      const input: TransactionRecommendationInput = {
        transactionAmount,
        paymentServiceType: PaymentServiceTypeEnum.BONUS_INTEREST,
        sourceCustomerId,
        beneficiaryBankCode: targetBankCode,
        externalId,
        beneficiaryBankCodeChannel
      };

      const result = await transactionHelper.mapFeeData(
        [
          {
            ...defaultRecommendedServices[0],
            debitTransactionCode: undefined,
            creditTransactionCode: [
              {
                transactionCode: 'c01',
                transactionCodeInfo: {
                  code: 'c01',
                  channel: 'c01_channel',
                  defaultCategoryCode: 'defaultC01',
                  minAmountPerTx: 100,
                  maxAmountPerTx: 9999999,
                  limitGroupCode: 'c01',
                  feeRules,
                  awardGroupCounter: 'awardGroupCounter1'
                }
              }
            ]
          }
        ],
        input,
        usageCounters
      );

      const expectedFeeAmountInput: FeeAmountInput = {
        feeRuleCode: feeRules,
        transactionAmount: transactionAmount,
        feeAmount: undefined,
        monthlyNoTransaction: 1,
        interchange: undefined,
        targetBankCode: targetBankCode,
        externalId: externalId,
        beneficiaryBankCodeChannel: beneficiaryBankCodeChannel,
        refund: undefined,
        thresholdCounter: undefined,
        customerId: sourceCustomerId,
        awardGroupCounter: 'awardGroupCounter1',
        limitGroupCode: 'c01',
        usageCounters: [
          {
            dailyAccumulationAmount: 0,
            limitGroupCode: 'c01',
            monthlyAccumulationTransaction: 1
          }
        ]
      };

      expect(result).toEqual(expectedData);
      expect(feeService.getFeeAmount).toHaveBeenCalledWith(
        expectedFeeAmountInput
      );
    });

    it('should return services with fee data with 0 fee for BONUS_INTEREST paymentServiceType', async () => {
      const usageCounter = {
        limitGroupCode: limitGroupCode,
        dailyAccumulationAmount: 0,
        monthlyAccumulationTransaction: 1
      };
      const usageCounters = [usageCounter];
      const transactionAmount = 1;
      const sourceCustomerId = 'customerId';
      const fee = { fee: 0 };
      const targetBankCode = 'BC00312';
      const externalId = 'externalId';
      const beneficiaryBankCodeChannel = BankChannelEnum.ANY;
      const expectedData = [
        {
          ...defaultRecommendedServices[0],
          debitTransactionCode: undefined,
          creditTransactionCode: [
            {
              interchange: BankNetworkEnum.MFS,
              transactionCode: 'c01',
              transactionCodeInfo: {
                code: 'c01',
                channel: 'c01_channel',
                defaultCategoryCode: 'defaultC01',
                minAmountPerTx: 100,
                maxAmountPerTx: 9999999,
                limitGroupCode: 'c01',
                feeRules,
                awardGroupCounter: 'awardGroupCounter1'
              }
            }
          ],
          feeData: [fee]
        }
      ];
      (awardRepository.getThresholdCounter as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(fee);

      const input: TransactionRecommendationInput = {
        transactionAmount,
        paymentServiceType: PaymentServiceTypeEnum.BONUS_INTEREST,
        sourceCustomerId,
        beneficiaryBankCode: targetBankCode,
        externalId,
        beneficiaryBankCodeChannel
      };

      const result = await transactionHelper.mapFeeData(
        [
          {
            ...defaultRecommendedServices[0],
            debitTransactionCode: undefined,
            creditTransactionCode: [
              {
                interchange: BankNetworkEnum.MFS,
                transactionCode: 'c01',
                transactionCodeInfo: {
                  code: 'c01',
                  channel: 'c01_channel',
                  defaultCategoryCode: 'defaultC01',
                  minAmountPerTx: 100,
                  maxAmountPerTx: 9999999,
                  limitGroupCode: 'c01',
                  feeRules,
                  awardGroupCounter: 'awardGroupCounter1'
                }
              }
            ]
          }
        ],
        input,
        usageCounters
      );

      const expectedFeeAmountInput: FeeAmountInput = {
        feeRuleCode: feeRules,
        transactionAmount: transactionAmount,
        feeAmount: undefined,
        monthlyNoTransaction: 1,
        interchange: BankNetworkEnum.MFS,
        targetBankCode: targetBankCode,
        externalId: externalId,
        beneficiaryBankCodeChannel: beneficiaryBankCodeChannel,
        refund: undefined,
        thresholdCounter: undefined,
        customerId: sourceCustomerId,
        awardGroupCounter: 'awardGroupCounter1',
        limitGroupCode: 'c01',
        usageCounters: [
          {
            dailyAccumulationAmount: 0,
            limitGroupCode: 'c01',
            monthlyAccumulationTransaction: 1
          }
        ]
      };

      expect(result).toEqual(expectedData);
      expect(feeService.getFeeAmount).toHaveBeenCalledWith(
        expectedFeeAmountInput
      );
    });

    it('should return services with fee data with limitGroupCode from RTOL and BIFAST', async () => {
      const usageCounter = [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 1
        },
        {
          limitGroupCode: RTOL_BIFAST_FEE,
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 25
        }
      ];
      const usageCounters = usageCounter;
      const transactionAmount = 1;
      const sourceCustomerId = 'customerId';
      const fee = { fee: 1 };
      const targetBankCode = 'BC00312';
      const externalId = 'externalId';
      const beneficiaryBankCodeChannel = BankChannelEnum.ANY;
      const expectedData = [
        {
          ...defaultRecommendedServices[0],
          creditTransactionCode: undefined,
          debitTransactionCode: [
            {
              transactionCode: 'c01',
              transactionCodeInfo: {
                code: 'c01',
                channel: 'c01_channel',
                defaultCategoryCode: 'defaultC01',
                minAmountPerTx: 100,
                maxAmountPerTx: 9999999,
                limitGroupCode: 'L002',
                feeRules,
                awardGroupCounter: 'awardGroupCounter1'
              }
            }
          ],
          feeData: [fee]
        }
      ];
      (awardRepository.getThresholdCounter as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(fee);

      const input: TransactionRecommendationInput = {
        transactionAmount,
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        sourceCustomerId,
        beneficiaryBankCode: targetBankCode,
        externalId,
        beneficiaryBankCodeChannel
      };

      const result = await transactionHelper.mapFeeData(
        [
          {
            ...defaultRecommendedServices[0],
            creditTransactionCode: undefined,
            debitTransactionCode: [
              {
                transactionCode: 'c01',
                transactionCodeInfo: {
                  code: 'c01',
                  channel: 'c01_channel',
                  defaultCategoryCode: 'defaultC01',
                  minAmountPerTx: 100,
                  maxAmountPerTx: 9999999,
                  limitGroupCode: 'L002',
                  feeRules,
                  awardGroupCounter: 'awardGroupCounter1'
                }
              }
            ]
          }
        ],
        input,
        usageCounters
      );

      const expectedFeeAmountInput: FeeAmountInput = {
        feeRuleCode: feeRules,
        transactionAmount: transactionAmount,
        feeAmount: undefined,
        monthlyNoTransaction: 25,
        interchange: undefined,
        targetBankCode: targetBankCode,
        externalId: externalId,
        beneficiaryBankCodeChannel: beneficiaryBankCodeChannel,
        refund: undefined,
        thresholdCounter: undefined,
        customerId: sourceCustomerId,
        awardGroupCounter: 'awardGroupCounter1',
        limitGroupCode: 'L002',
        usageCounters: [
          {
            dailyAccumulationAmount: 0,
            limitGroupCode: 'L002',
            monthlyAccumulationTransaction: 25
          },
          {
            dailyAccumulationAmount: 0,
            limitGroupCode: 'RTOL_BIFAST_FEE',
            monthlyAccumulationTransaction: 25
          }
        ]
      };

      expect(result).toEqual(expectedData);
      expect(feeService.getFeeAmount).toHaveBeenCalledWith(
        expectedFeeAmountInput
      );
    });

    it('should return services with 0 fee data with limitGroupCode not from RTOL and BIFAST', async () => {
      const usageCounter = [
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 1
        },
        {
          limitGroupCode: RTOL_BIFAST_FEE,
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 25
        }
      ];
      const usageCounters = usageCounter;
      const transactionAmount = 1;
      const sourceCustomerId = 'customerId';
      const fee = { fee: 0 };
      const targetBankCode = 'BC00312';
      const externalId = 'externalId';
      const beneficiaryBankCodeChannel = BankChannelEnum.ANY;
      const expectedData = [
        {
          ...defaultRecommendedServices[0],
          creditTransactionCode: undefined,
          debitTransactionCode: [
            {
              transactionCode: 'c01',
              transactionCodeInfo: {
                code: 'c01',
                channel: 'c01_channel',
                defaultCategoryCode: 'defaultC01',
                minAmountPerTx: 100,
                maxAmountPerTx: 9999999,
                limitGroupCode: 'L003',
                feeRules,
                awardGroupCounter: 'awardGroupCounter1'
              }
            }
          ],
          feeData: [fee]
        }
      ];
      (awardRepository.getThresholdCounter as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(fee);

      const input: TransactionRecommendationInput = {
        transactionAmount,
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        sourceCustomerId,
        beneficiaryBankCode: targetBankCode,
        externalId,
        beneficiaryBankCodeChannel
      };

      const result = await transactionHelper.mapFeeData(
        [
          {
            ...defaultRecommendedServices[0],
            creditTransactionCode: undefined,
            debitTransactionCode: [
              {
                transactionCode: 'c01',
                transactionCodeInfo: {
                  code: 'c01',
                  channel: 'c01_channel',
                  defaultCategoryCode: 'defaultC01',
                  minAmountPerTx: 100,
                  maxAmountPerTx: 9999999,
                  limitGroupCode: 'L003',
                  feeRules,
                  awardGroupCounter: 'awardGroupCounter1'
                }
              }
            ]
          }
        ],
        input,
        usageCounters
      );

      const expectedFeeAmountInput: FeeAmountInput = {
        feeRuleCode: feeRules,
        transactionAmount: transactionAmount,
        feeAmount: undefined,
        monthlyNoTransaction: 1,
        interchange: undefined,
        targetBankCode: targetBankCode,
        externalId: externalId,
        beneficiaryBankCodeChannel: beneficiaryBankCodeChannel,
        refund: undefined,
        thresholdCounter: undefined,
        customerId: sourceCustomerId,
        awardGroupCounter: 'awardGroupCounter1',
        limitGroupCode: 'L003',
        usageCounters: [
          {
            dailyAccumulationAmount: 0,
            limitGroupCode: 'L003',
            monthlyAccumulationTransaction: 1
          },
          {
            dailyAccumulationAmount: 0,
            limitGroupCode: 'RTOL_BIFAST_FEE',
            monthlyAccumulationTransaction: 25
          }
        ]
      };

      expect(result).toEqual(expectedData);
      expect(feeService.getFeeAmount).toHaveBeenCalledWith(
        expectedFeeAmountInput
      );
    });

    it('should return origin services if sourceCIF is not available', async () => {
      const input: TransactionRecommendationInput = {
        transactionAmount: 1,
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER
      };
      const result = await transactionHelper.mapFeeData(
        defaultRecommendedServices,
        input,
        []
      );

      expect(result).toBe(defaultRecommendedServices);
    });

    it('should not return services with fee data if debit transaction code is not available', async () => {
      const transactionAmount = 1;
      const sourceCustomerId = '1';
      const recommendedService: RecommendationService = {
        ...defaultRecommendedServices[0],
        debitTransactionCode: undefined
      };
      const input: TransactionRecommendationInput = {
        transactionAmount,
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        sourceCustomerId
      };

      const result = await transactionHelper.mapFeeData(
        [recommendedService],
        input,
        []
      );

      expect(result).toEqual([
        {
          ...recommendedService,
          feeData: []
        }
      ]);
    });

    it('should return fee data if inquiryFeeRule is available and debitTransactionCodeInfo is not available', async () => {
      const transactionAmount = 1;
      const sourceCustomerId = '1';
      const fee = { fee: 1 };
      const targetBankCode = 'BC00312';
      const inquiryFeeRule = 'INQUIRY_FEE';
      const externalId = 'dummy123';
      const beneficiaryBankCodeChannel = BankChannelEnum.ANY;

      const input: TransactionRecommendationInput = {
        transactionAmount,
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        sourceCustomerId,
        beneficiaryBankCode: targetBankCode,
        externalId,
        beneficiaryBankCodeChannel
      };
      const recommendedServices: RecommendationService[] = [
        {
          ...defaultRecommendedServices[0],
          debitTransactionCode: undefined,
          inquiryFeeRule
        }
      ];
      const expectedData = [
        {
          ...recommendedServices[0],
          feeData: [fee]
        }
      ];
      (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(fee);

      const result = await transactionHelper.mapFeeData(
        recommendedServices,
        input,
        []
      );

      const expectedFeeAmountInput: FeeAmountInput = {
        feeRuleCode: inquiryFeeRule,
        transactionAmount: transactionAmount,
        feeAmount: undefined,
        monthlyNoTransaction: undefined,
        interchange: undefined,
        targetBankCode: targetBankCode,
        externalId: externalId,
        beneficiaryBankCodeChannel: beneficiaryBankCodeChannel,
        refund: undefined,
        customerId: sourceCustomerId
      };

      expect(result).toEqual(expectedData);
      expect(feeService.getFeeAmount).toHaveBeenCalledWith(
        expectedFeeAmountInput
      );
    });
  });

  describe('getAccountInfo', () => {
    const accountNumber = '1';
    const cif = '1';
    const bankCode = 'bc005';
    const accountName = 'a';
    const role = 'NONE';

    const bankCodeInfo: IBankCode = {
      bankCodeId: bankCode,
      name: 'name',
      companyName: 'companyName',
      isBersamaMember: true,
      isAltoMember: true,
      channel: BankChannelEnum.PARTNER,
      type: 'Type',
      isInternal: true
    };

    it('should inquiry account info if cif, accountNo is available and bank code is internal', async () => {
      const foundAccount = { accountNumber, cif };
      (accountRepository.getAccountInfo as jest.Mock).mockResolvedValueOnce(
        foundAccount
      );

      const accountInfo = await transactionHelper.getAccountInfo(
        accountNumber,
        cif,
        bankCodeInfo
      );

      expect(accountInfo).toEqual({
        ...foundAccount,
        bankCode: bankCodeInfo.bankCodeId,
        role
      });
      expect(accountRepository.getAccountInfo).toHaveBeenCalledWith('1', '1');
    });

    it('should inquiry account with transactionAmount and sourceAccountNo', async () => {
      const foundAccount = { accountNumber, cif };
      const transactionAmount = 1000;
      const sourceAccountNo = 'anyNo';
      const bankCode = { ...bankCodeInfo, isInternal: false };
      (accountRepository.inquiryAccountInfo as jest.Mock).mockResolvedValueOnce(
        foundAccount
      );
      const accountInfo = await transactionHelper.getAccountInfo(
        accountNumber,
        cif,
        bankCode,
        undefined,
        transactionAmount,
        sourceAccountNo
      );

      expect(accountInfo).toEqual({
        ...foundAccount,
        bankCode: bankCode.bankCodeId,
        role
      });
      expect(accountRepository.inquiryAccountInfo).toHaveBeenCalledWith(
        'bc005',
        '1',
        1000,
        'anyNo',
        '1'
      );
    });

    it('should inquiry account info if bank code is external', async () => {
      (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValueOnce(
        bankCodeInfo
      );

      const foundAccount = { accountNumber, accountName };
      (accountRepository.inquiryAccountInfo as jest.Mock).mockResolvedValueOnce(
        foundAccount
      );

      const accountInfo = await transactionHelper.getAccountInfo(
        accountNumber,
        undefined,
        {
          ...bankCodeInfo,
          isInternal: false
        }
      );

      expect(accountRepository.inquiryAccountInfo).toBeCalledWith(
        bankCodeInfo.bankCodeId,
        accountNumber,
        undefined,
        undefined,
        undefined
      );
      expect(accountInfo).toEqual({
        accountNumber,
        accountName,
        bankCode,
        role
      });
    });

    it('should return account info without inquiring account info from account api if accountNo is not available', async () => {
      const accountInfo = await transactionHelper.getAccountInfo(
        undefined,
        cif,
        bankCodeInfo,
        accountName
      );

      expect(accountInfo).toEqual({
        accountNumber: undefined,
        bankCode: bankCodeInfo.bankCodeId,
        accountName,
        role
      });
      expect(accountRepository.inquiryAccountInfo).not.toBeCalled();
    });

    it('should return account info with bankChannel information if beneficiary bank code support BIFAST or EXTERNAL channel', async () => {
      (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValueOnce(
        bankCodeInfo
      );

      const foundAccount: ITransactionAccount = {
        accountNumber,
        accountName,
        beneficiaryAccountType: 'SVGS',
        preferredChannel: BankChannelEnum.BIFAST
      };

      (accountRepository.inquiryAccountInfo as jest.Mock).mockResolvedValueOnce(
        foundAccount
      );

      const accountInfo = await transactionHelper.getAccountInfo(
        accountNumber,
        undefined,
        {
          ...bankCodeInfo,
          isInternal: false
        }
      );

      expect(accountRepository.inquiryAccountInfo).toBeCalledWith(
        bankCodeInfo.bankCodeId,
        accountNumber,
        undefined,
        undefined,
        undefined
      );
      expect(accountInfo).toEqual({
        accountNumber,
        accountName,
        bankCode,
        role,
        beneficiaryAccountType: 'SVGS',
        preferredChannel: BankChannelEnum.BIFAST
      });
    });

    it('should return account info without inquiring account info from account api if bankCode is undefined', async () => {
      const expectedAccountInfo = {
        accountNumber: accountNumber,
        accountName
      };
      const externalBankCode = { ...bankCodeInfo };
      externalBankCode.isInternal = false;

      const accountInfo = await transactionHelper.getAccountInfo(
        accountNumber,
        cif,
        undefined,
        accountName
      );

      expect(accountInfo).toEqual({
        ...expectedAccountInfo,
        bankCode: undefined,
        role
      });
      expect(accountRepository.inquiryAccountInfo).not.toBeCalled();
    });

    it('should throw error if can not find internal account', async () => {
      (accountRepository.getAccountInfo as jest.Mock).mockResolvedValueOnce({});

      const error = await transactionHelper
        .getAccountInfo(accountNumber, cif, bankCodeInfo)
        .catch(err => err);

      expect(error.errorCode).toBe(ERROR_CODE.INVALID_ACCOUNT);
      expect(accountRepository.getAccountInfo).toHaveBeenCalledWith('1', '1');
    });

    it('should throw error if found internal account with different cif', async () => {
      (accountRepository.getAccountInfo as jest.Mock).mockResolvedValueOnce({});

      const error = await transactionHelper
        .getAccountInfo(accountNumber, cif, bankCodeInfo)
        .catch(err => err);

      expect(error.errorCode).toBe(ERROR_CODE.INVALID_ACCOUNT);
      expect(accountRepository.getAccountInfo).toHaveBeenCalledWith('1', '1');
    });

    it('should not call account inquiry if it is not allowed', async () => {
      const foundAccount = { accountNumber, cif };
      (accountRepository.inquiryAccountInfo as jest.Mock).mockResolvedValueOnce(
        foundAccount
      );

      const accountInfo = await transactionHelper.getAccountInfo(
        accountNumber,
        cif,
        bankCodeInfo,
        undefined,
        undefined,
        undefined,
        false
      );

      expect(accountInfo).toEqual({
        accountName: undefined,
        accountNumber,
        bankCode: bankCodeInfo.bankCodeId,
        role
      });
      expect(accountRepository.inquiryAccountInfo).not.toBeCalled();
    });
  });

  describe('buildNewTransactionModel', () => {
    const defaultRecommendedService: RecommendationService = getRecommendedService();

    it('should return model with generated value of externalId and referenceId', () => {
      const mockGeneratedId = 'abcdg238984wekjdk';

      const transaction: ITransactionInput = {
        ...getITransactionInput(),
        transactionAmount: 1
      };
      delete transaction.externalId;
      delete transaction.referenceId;

      (generateUniqueId as jest.Mock).mockImplementation(() => {
        return mockGeneratedId;
      });

      // When
      const result = transactionHelper.buildNewTransactionModel(
        transaction,
        defaultRecommendedService
      );

      // Then
      const debitTransactionCode =
        defaultRecommendedService.debitTransactionCode ?? [];
      const creditTransactionCode =
        defaultRecommendedService.creditTransactionCode ?? [];
      expect(result).toEqual(
        expect.objectContaining({
          externalId: mockGeneratedId,
          referenceId: mockGeneratedId,
          debitTransactionChannel:
            debitTransactionCode[0].transactionCodeInfo.channel,
          creditTransactionChannel:
            creditTransactionCode[0].transactionCodeInfo.channel
        })
      );
    });

    it(`should set ${TransactionStatus.PENDING} status for new created transaction instance`, () => {
      const transaction: ITransactionInput = {
        ...getITransactionInput(),
        transactionAmount: 1
      };

      // When
      const result = transactionHelper.buildNewTransactionModel(
        transaction,
        defaultRecommendedService
      );

      // Then
      expect(result).toEqual(
        expect.objectContaining({
          status: TransactionStatus.PENDING
        })
      );
    });

    it('should not generate id for referenceId and externalId if these fields are available from input', () => {
      const referenceId = 'ref1';
      const externalId = 'ex1';

      const transaction: ITransactionInput = {
        ...getITransactionInput(),
        transactionAmount: 1,
        referenceId,
        externalId
      };

      // When
      const result = transactionHelper.buildNewTransactionModel(
        transaction,
        defaultRecommendedService
      );

      // Then
      expect(result).toEqual(
        expect.objectContaining({
          referenceId,
          externalId
        })
      );
    });

    it('should use categoryCode from input', () => {
      const transaction: ITransactionInput = {
        ...getITransactionInput(),
        transactionAmount: 1,
        categoryCode: 'c01'
      };

      // When
      const result = transactionHelper.buildNewTransactionModel(
        transaction,
        defaultRecommendedService
      );

      // Then
      expect(result).toEqual(
        expect.objectContaining({
          transactionAmount: 1,
          categoryCode: 'c01'
        })
      );
    });

    it('should add executorCIF if non-blocking transfer and sourceAccount is jago', () => {
      const transaction: ITransactionInput = {
        ...getITransactionInput(),
        transactionAmount: 1,
        executorCIF: '1234545abc',
        sourceAccountInfo: {
          cif: '1234545abc'
        },
        sourceBankCodeInfo: {
          bankCodeId: 'BC002',
          name: 'Jago',
          rtolCode: '542',
          companyName: 'PT Bank Jaog',
          isBersamaMember: false,
          isAltoMember: false,
          channel: BankChannelEnum.INTERNAL,
          type: 'Bank',
          isInternal: true
        }
      };

      // When
      const result = transactionHelper.buildNewTransactionModel(
        transaction,
        defaultRecommendedService
      );

      // Then
      expect(result).toEqual(
        expect.objectContaining({
          transactionAmount: 1,
          executorCIF: '1234545abc'
        })
      );
    });

    it('should set fee data for transaction model when recommended service has fee', () => {
      const transaction: ITransactionInput = {
        ...getITransactionInput(),
        transactionAmount: 1000,
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER
      };
      const result = transactionHelper.buildNewTransactionModel(
        transaction,
        defaultRecommendedService
      );

      expect(result.fees).toMatchObject(
        defaultRecommendedService.feeData ?? []
      );
    });

    it('should fill value of sourceAccountName from database if it existed', () => {
      const transaction: ITransactionInput = {
        ...getITransactionInput(),
        sourceAccountInfo: {
          accountName: 'anyName'
        }
      };
      const result = transactionHelper.buildNewTransactionModel(
        transaction,
        defaultRecommendedService
      );

      expect(result).toEqual(
        expect.objectContaining({
          sourceAccountName: 'anyName'
        })
      );
    });

    it('should store debitLimitGroupCode when debitTransactionCodeInfo is available', () => {
      const transaction = {
        ...getITransactionInput(),
        transactionAmount: 1
      };
      const service: RecommendationService = getRecommendedService();
      const debitTransactionCode = service.debitTransactionCode ?? [];
      debitTransactionCode[0].transactionCodeInfo = {
        ...debitTransactionCode[0].transactionCodeInfo,
        limitGroupCode: 'limitGroupCode',
        code: 'anyCode',
        maxAmountPerTx: 100,
        minAmountPerTx: 1,
        channel: 'anyChannel'
      };
      // When
      const result = transactionHelper.buildNewTransactionModel(
        transaction,
        service
      );

      // Then
      expect(result).toEqual(
        expect.objectContaining({
          debitLimitGroupCode: 'limitGroupCode'
        })
      );
    });

    it('should use paymentInstructionId from input', () => {
      const transaction: ITransactionInput = {
        ...getITransactionInput(),
        transactionAmount: 1,
        paymentInstructionId: '12345abcd12345'
      };

      // When
      const result = transactionHelper.buildNewTransactionModel(
        transaction,
        defaultRecommendedService
      );

      // Then
      expect(result).toEqual(
        expect.objectContaining({
          transactionAmount: 1,
          paymentInstructionId: '12345abcd12345'
        })
      );
    });
  });

  describe('validateTransactionLimitAmount', () => {
    it('should throw err if transaction amount is limited with single debit transaction code', () => {
      // Given
      const serviceMappingResult: RecommendationService[] = [
        {
          ...getRecommendedService(),
          paymentServiceCode: 'p01',
          debitTransactionCode: [
            {
              transactionCode: 'd01',
              transactionCodeInfo: {
                limitGroupCode: 'l01',
                feeRules: 'f01',
                minAmountPerTx: 10000,
                maxAmountPerTx: 50000,
                code: 'd01',
                channel: 'd01_channel',
                defaultCategoryCode: 'defaultD01'
              }
            }
          ]
        }
      ];
      const transactionAmount = 5;
      let error;

      // When
      try {
        transactionHelper.validateTransactionLimitAmount(
          transactionAmount,
          serviceMappingResult
        );
      } catch (err) {
        error = err;
      }

      // then
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_AMOUNT);
    });
    it('should throw err if transaction amount is limited with single credit transaction code', () => {
      // Given
      const serviceMappingResult: RecommendationService[] = [
        {
          ...getRecommendedService(),
          paymentServiceCode: 'p01',
          creditTransactionCode: [
            {
              transactionCode: 'd01',
              transactionCodeInfo: {
                limitGroupCode: 'l01',
                feeRules: 'f01',
                minAmountPerTx: 10000,
                maxAmountPerTx: 50000,
                code: 'c01',
                channel: 'c01_channel',
                defaultCategoryCode: 'defaultC01'
              }
            }
          ]
        }
      ];
      const transactionAmount = 5;
      let error;

      // When
      try {
        transactionHelper.validateTransactionLimitAmount(
          transactionAmount,
          serviceMappingResult
        );
      } catch (err) {
        error = err;
      }

      // then
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_AMOUNT);
    });

    it('should throw err if transaction amount is limited with multiple debit transaction code', () => {
      // Given
      const serviceMappingResult: RecommendationService[] = [
        {
          ...getRecommendedService(),
          paymentServiceCode: 'p01',
          debitTransactionCode: [
            {
              transactionCode: 'd01',
              transactionCodeInfo: {
                limitGroupCode: 'l01',
                feeRules: 'f01',
                minAmountPerTx: 10000,
                maxAmountPerTx: 50000,
                code: 'd01',
                channel: 'd01_channel'
              }
            }
          ]
        },
        {
          ...getRecommendedService(),
          paymentServiceCode: 'p01',
          debitTransactionCode: [
            {
              transactionCode: 'd02',
              transactionCodeInfo: {
                limitGroupCode: 'l01',
                feeRules: 'f01',
                minAmountPerTx: 200000,
                maxAmountPerTx: 10000000,
                code: 'd02',
                channel: 'd02_channel'
              }
            }
          ]
        }
      ];
      const transactionAmount = 11000;
      let error;

      // When
      try {
        transactionHelper.validateTransactionLimitAmount(
          transactionAmount,
          serviceMappingResult
        );
      } catch (err) {
        error = err;
      }

      // then
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_AMOUNT);
    });
    it('should throw err if transaction amount is limited with multiple credit transaction code', () => {
      // Given
      const serviceMappingResult: RecommendationService[] = [
        {
          ...getRecommendedService(),
          paymentServiceCode: 'p01',
          creditTransactionCode: [
            {
              transactionCode: 'c01',
              transactionCodeInfo: {
                limitGroupCode: 'l01',
                feeRules: 'f01',
                minAmountPerTx: 10000,
                maxAmountPerTx: 50000,
                code: 'c01',
                channel: 'c01_channel'
              }
            }
          ]
        },
        {
          ...getRecommendedService(),
          paymentServiceCode: 'p01',
          creditTransactionCode: [
            {
              transactionCode: 'c02',
              transactionCodeInfo: {
                limitGroupCode: 'l01',
                feeRules: 'f01',
                minAmountPerTx: 200000,
                maxAmountPerTx: 10000000,
                code: 'c02',
                channel: 'c02_channel'
              }
            }
          ]
        }
      ];
      const transactionAmount = 11000;
      let error;

      // When
      try {
        transactionHelper.validateTransactionLimitAmount(
          transactionAmount,
          serviceMappingResult
        );
      } catch (err) {
        error = err;
      }

      // then
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_AMOUNT);
    });
    it('should throw err if transaction amount is limited with credit transaction code and interchange', () => {
      // Given
      const serviceMappingResult: RecommendationService[] = [
        {
          ...getRecommendedService(),
          paymentServiceCode: 'p01',
          creditTransactionCode: [
            {
              transactionCode: 'd01',
              interchange: BankNetworkEnum.VISA,
              transactionCodeInfo: {
                limitGroupCode: 'l01',
                feeRules: 'f01',
                minAmountPerTx: 10000,
                maxAmountPerTx: 50000,
                code: 'c01',
                channel: 'c01_channel',
                defaultCategoryCode: 'defaultC01'
              }
            }
          ]
        }
      ];
      const transactionAmount = 5;
      const interchange = BankNetworkEnum.VISA;
      let error;

      // When
      try {
        transactionHelper.validateTransactionLimitAmount(
          transactionAmount,
          serviceMappingResult,
          interchange
        );
      } catch (err) {
        error = err;
      }

      // then
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_AMOUNT);
    });

    it('should throw err if transaction amount is above maximum', () => {
      // Given
      const serviceMappingResult: RecommendationService[] = [
        {
          ...getRecommendedService(),
          paymentServiceCode: 'p01',
          creditTransactionCode: [
            {
              transactionCode: 'd01',
              interchange: BankNetworkEnum.VISA,
              transactionCodeInfo: {
                limitGroupCode: 'l01',
                feeRules: 'f01',
                minAmountPerTx: 10000,
                maxAmountPerTx: 50000,
                code: 'c01',
                channel: 'c01_channel',
                defaultCategoryCode: 'defaultC01'
              }
            }
          ]
        }
      ];
      const transactionAmount = 50001;
      const interchange = BankNetworkEnum.VISA;
      let error;

      // When
      try {
        transactionHelper.validateTransactionLimitAmount(
          transactionAmount,
          serviceMappingResult,
          interchange
        );
      } catch (err) {
        error = err;
      }

      // then
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_AMOUNT);
    });

    it('should not throw err if transaction amount is valid', () => {
      // Given
      const serviceMappingResult: RecommendationService[] = [
        {
          ...getRecommendedService(),
          paymentServiceCode: 'p01',
          debitTransactionCode: [
            {
              transactionCode: 'd01',
              transactionCodeInfo: {
                limitGroupCode: 'l01',
                feeRules: 'f01',
                minAmountPerTx: 10000,
                maxAmountPerTx: 50000,
                code: 'd01',
                channel: 'd01_channel'
              }
            }
          ]
        }
      ];

      const transactionAmount = 11000;

      // When
      const result = transactionHelper.validateTransactionLimitAmount(
        transactionAmount,
        serviceMappingResult
      );

      // then
      expect(result).toBeUndefined();
    });

    describe(`validateTransactionLimitAmount when transaction code contain reversal TC suffix ${REVERSAL_TC_SUFFIX}`, () => {
      it(`should not throw error when debit transaction code contain reversal TC suffix ${REVERSAL_TC_SUFFIX}`, () => {
        // Given
        const serviceMappingResult: RecommendationService[] = [
          {
            ...getRecommendedService(),
            paymentServiceCode: 'p01',
            debitTransactionCode: [
              {
                transactionCode: `c01${REVERSAL_TC_SUFFIX}`,
                transactionCodeInfo: {
                  limitGroupCode: 'l01',
                  feeRules: 'f01',
                  minAmountPerTx: 10000,
                  maxAmountPerTx: 50000,
                  code: 'c01',
                  channel: 'c01_channel',
                  defaultCategoryCode: 'defaultC01'
                }
              }
            ],
            creditTransactionCode: [
              {
                transactionCode: `c01`,
                transactionCodeInfo: {
                  limitGroupCode: 'l01',
                  feeRules: 'f01',
                  minAmountPerTx: 10000,
                  maxAmountPerTx: 50000,
                  code: 'c01',
                  channel: 'c01_channel',
                  defaultCategoryCode: 'defaultC01'
                }
              }
            ]
          }
        ];
        const transactionAmount = 50001;
        const interchange = BankNetworkEnum.RTGS;
        let error: any;

        // When
        try {
          transactionHelper.validateTransactionLimitAmount(
            transactionAmount,
            serviceMappingResult,
            interchange
          );
        } catch (err) {
          error = err;
        }

        // then
        expect(error).toBeUndefined();
      });
    });
  });

  describe('getBankCode', () => {
    it('should return bankCode if found', async () => {
      // Given
      const bankCode = 'BC002';
      (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValueOnce(
        {}
      );

      // When
      const bankCodeInfoResult = await transactionHelper.getBankCode(bankCode);

      // Then
      expect(bankCodeInfoResult).toBeDefined();
      expect(configurationRepository.getBankCodeMapping).toHaveBeenCalledWith(
        bankCode
      );
    });

    it('should throw error if bankCode is not found', async () => {
      // Given
      const bankCode = 'BC002';
      (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValueOnce(
        undefined
      );

      // When
      const error = await transactionHelper.getBankCode(bankCode).catch(e => e);

      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_BANK_CODE);
      expect(configurationRepository.getBankCodeMapping).toHaveBeenCalledWith(
        bankCode
      );
    });
  });

  describe('getPendingBlockingTransaction', () => {
    it('should throw error when transaction not found', async () => {
      //given
      const cif = 'cif123';
      const transactionId = '123';
      (transferRepository.getByKey as jest.Mock).mockResolvedValueOnce(null);

      //when
      const expected = await transactionHelper
        .getPendingBlockingTransaction(transactionId, cif)
        .catch(error => error);

      //then
      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toEqual(ERROR_CODE.TRANSACTION_NOT_FOUND);
    });

    it('should throw error when status transaction is not pending or submitted', async () => {
      //given
      const cif = 'cif123';
      let newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        status: TransactionStatus.SUCCEED
      };
      (transferRepository.getByKey as jest.Mock).mockResolvedValueOnce(data);

      //when
      const expected = await transactionHelper
        .getPendingBlockingTransaction(data.id, cif)
        .catch(error => error);

      //then
      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toEqual(ERROR_CODE.INVALID_TRANSACTION_STATUS);
    });

    it('should throw error when blockId not found', async () => {
      //given
      const cif = 'cif123';
      let newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        status: TransactionStatus.SUBMITTED
      };
      (transferRepository.getByKey as jest.Mock).mockResolvedValueOnce(data);

      //when
      const expected = await transactionHelper
        .getPendingBlockingTransaction(data.id, cif)
        .catch(error => error);

      //then
      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toEqual(ERROR_CODE.BLOCKING_ID_NOT_FOUND);
    });
    it('should throw error when account is invalid', async () => {
      //given
      const cif = 'cif123';
      let newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        blockingId: 'blockingId123',
        sourceCIF: undefined,
        status: TransactionStatus.PENDING
      };
      (transferRepository.getByKey as jest.Mock).mockResolvedValueOnce(data);

      //when
      const expected = await transactionHelper
        .getPendingBlockingTransaction(data.id, cif)
        .catch(error => error);

      //then
      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toEqual(ERROR_CODE.INVALID_ACCOUNT);
    });
    it('should throw error when cif not contain transaction', async () => {
      //given
      const cif = 'cif123';
      let newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        cardId: 'cardId01',
        blockingId: 'blockingId123',
        sourceCIF: 'cif789',
        status: TransactionStatus.PENDING
      };
      (transferRepository.getByKey as jest.Mock).mockResolvedValueOnce(data);

      //when
      const expected = await transactionHelper
        .getPendingBlockingTransaction(data.id, cif)
        .catch(error => error);

      //then
      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toEqual(ERROR_CODE.INVALID_ACCOUNT);
    });
  });

  describe('getSourceAccount', () => {
    const accountNumber = '1';
    const cif = '1';
    const bankCode = 'bc005';
    const bankCodeInfo: IBankCode = {
      bankCodeId: bankCode,
      name: 'name',
      companyName: 'companyName',
      isBersamaMember: true,
      isAltoMember: true,
      channel: BankChannelEnum.PARTNER,
      type: 'Type',
      isInternal: true
    };

    it('should inquiry account info if cif, accountNo is available, bank code is internal and role is MOVER', async () => {
      const foundAccount = { accountNumber, cif, role: 'MOVER' };

      (accountRepository.getAccountInfo as jest.Mock).mockResolvedValueOnce(
        foundAccount
      );

      const accountInfo = await transactionHelper.getSourceAccount(
        accountNumber,
        cif,
        bankCodeInfo
      );

      expect(accountInfo).toEqual({
        ...foundAccount,
        bankCode: bankCodeInfo.bankCodeId
      });
      expect(accountRepository.getAccountInfo).toHaveBeenCalledWith('1', '1');
    });

    it('should inquiry account info if cif is undefined, bank code is internal and accountType is RDN', async () => {
      const foundAccount = {
        accountNumber,
        cif,
        role: 'NONE',
        accountType: 'RDN'
      };

      (accountRepository.getAccountInfo as jest.Mock).mockResolvedValueOnce(
        foundAccount
      );

      const accountInfo = await transactionHelper.getSourceAccount(
        accountNumber,
        undefined,
        bankCodeInfo
      );

      expect(accountInfo).toEqual({
        ...foundAccount,
        bankCode: bankCodeInfo.bankCodeId
      });
      expect(accountRepository.getAccountInfo).toHaveBeenCalledWith(
        '1',
        undefined
      );
    });

    it('should inquiry account info if cif is undefined, bank code is internal and accountType is RDS', async () => {
      const foundAccount = {
        accountNumber,
        cif,
        role: 'NONE',
        accountType: 'RDS'
      };

      (accountRepository.getAccountInfo as jest.Mock).mockResolvedValueOnce(
        foundAccount
      );

      const accountInfo = await transactionHelper.getSourceAccount(
        accountNumber,
        undefined,
        bankCodeInfo
      );

      expect(accountInfo).toEqual({
        ...foundAccount,
        bankCode: bankCodeInfo.bankCodeId
      });
      expect(accountRepository.getAccountInfo).toHaveBeenCalledWith(
        '1',
        undefined
      );
    });

    it('should inquiry account info if cif is undefined, bank code is internal and accountType is MBA', async () => {
      const foundAccount = {
        accountNumber,
        cif,
        role: 'NONE',
        accountType: 'MBA'
      };

      (accountRepository.getAccountInfo as jest.Mock).mockResolvedValueOnce(
        foundAccount
      );

      const accountInfo = await transactionHelper.getSourceAccount(
        accountNumber,
        undefined,
        bankCodeInfo
      );

      expect(accountInfo).toEqual({
        ...foundAccount,
        bankCode: bankCodeInfo.bankCodeId
      });
      expect(accountRepository.getAccountInfo).toHaveBeenCalledWith(
        '1',
        undefined
      );
    });

    it('should throw Error when receive role NONE ', async () => {
      const foundAccount = { accountNumber, cif, role: 'NONE' };

      (accountRepository.getAccountInfo as jest.Mock).mockResolvedValueOnce(
        foundAccount
      );

      const result = await transactionHelper
        .getSourceAccount(accountNumber, cif, bankCodeInfo)
        .catch(error => error);

      expect(result.errorCode).toEqual(
        ERROR_CODE.SOURCE_ACCOUNT_INQUIRY_FAILED
      );
    });

    it('should throw error if can not find account', async () => {
      (accountRepository.getAccountInfo as jest.Mock).mockRejectedValueOnce({
        error: {
          error: {
            code: 'ACCOUNT_NUMBER_NOT_FOUND'
          }
        }
      });

      const error = await transactionHelper
        .getSourceAccount(accountNumber, cif, bankCodeInfo)
        .catch(err => err);

      expect(error.errorCode).toBe(ERROR_CODE.SOURCE_ACCOUNT_INQUIRY_FAILED);
      expect(accountRepository.getAccountInfo).toHaveBeenCalledWith('1', '1');
    });

    it('should throw error if bankCode is internal but missing cif value when not RDN, RDS or MBA', async () => {
      const foundAccount = {
        accountNumber,
        cif,
        role: 'NONE'
      };

      (accountRepository.getAccountInfo as jest.Mock).mockResolvedValueOnce(
        foundAccount
      );

      const error = await transactionHelper
        .getSourceAccount(accountNumber, undefined, bankCodeInfo)
        .catch(err => err);

      expect(error.errorCode).toBe(ERROR_CODE.SOURCE_ACCOUNT_INQUIRY_FAILED);
    });
  });

  describe('getTransactionSearchStartDate', () => {
    it('should get 3 months ago date when transaction date is not provided', () => {
      // Given
      const result = moment()
        .subtract(3, 'month')
        .toDate();
      // When
      const response = transactionHelper.getTransactionSearchStartDate();

      // Then
      expect(response).toBeDefined();
      expect(response.getDate()).toEqual(result.getDate());
      expect(response.getFullYear()).toEqual(result.getFullYear());
      expect(response.getMonth()).toEqual(result.getMonth());
    });
  });

  describe('validateTransactionListRequest', () => {
    it('should return true when either customerId or transactionId is provided', () => {
      // Given
      const input: TransactionListRequest = {
        transactionId: 'test123'
      };
      // When
      const response = transactionHelper.validateTransactionListRequest(input);

      // Then
      expect(response).toBeDefined();
      expect(response).toBeTruthy();
    });

    it('should throw error when  customerId and transactionId both not provided', () => {
      // Given
      const input: TransactionListRequest = {
        startDate: '2020-10-20'
      };
      // When
      let error;
      try {
        transactionHelper.validateTransactionListRequest(input);
      } catch (err) {
        error = err;
      }

      // Then
      expect(error).toBeDefined();
      expect(error).toEqual(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.MISSING_CUSTOMER_OR_TRANSACTION_ID
        )
      );
    });

    it('should throw error when  customerId is present and startDate but no end date ', () => {
      // Given
      const input: TransactionListRequest = {
        startDate: '2020-10-20',
        customerId: 'test123'
      };
      // When
      let error;
      try {
        transactionHelper.validateTransactionListRequest(input);
      } catch (err) {
        error = err;
      }

      // Then
      expect(error).toBeDefined();
      expect(error).toEqual(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.MISSING_START_AND_END_DATE
        )
      );
    });
  });

  describe('getDeclinedBlockingTransaction', () => {
    it('should successfully give transaction', async () => {
      const transaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...getTransactionModelFullData(),
        status: TransactionStatus.DECLINED,
        blockingId: 'blockingId',
        cardId: 'cardId'
      };

      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
        transaction
      );

      const result = await transactionHelper.getDeclinedBlockingTransaction(
        transaction.id
      );

      expect(result).toEqual({
        ...transaction,
        sourceAccountNo: transaction.sourceAccountNo,
        sourceCIF: transaction.sourceCIF,
        blockingId: transaction.blockingId,
        cardId: transaction.cardId
      });
    });

    it('should throw error when transaction not found', async () => {
      const transaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...getTransactionModelFullData(),
        status: TransactionStatus.DECLINED,
        blockingId: 'blockingId',
        cardId: 'cardId'
      };

      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(null);

      const error = await transactionHelper
        .getDeclinedBlockingTransaction(transaction.id)
        .catch(e => e);

      expect(error.errorCode).toEqual(ERROR_CODE.TRANSACTION_NOT_FOUND);
    });

    it('should throw error INVALID_ACCOUNT when source account no', async () => {
      const transaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...getTransactionModelFullData(),
        sourceAccountNo: undefined
      };

      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
        transaction
      );

      const error = await transactionHelper
        .getDeclinedBlockingTransaction(transaction.id)
        .catch(e => e);

      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_ACCOUNT);
    });

    it('should throw error INVALID_TRANSACTION_STATUS when status is not declined', async () => {
      const transaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...getTransactionModelFullData()
      };

      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
        transaction
      );

      const error = await transactionHelper
        .getDeclinedBlockingTransaction(transaction.id)
        .catch(e => e);

      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_TRANSACTION_STATUS);
    });

    it('should throw error BLOCKING_ID_NOT_FOUND when blocking id & cardId not found', async () => {
      const transaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...getTransactionModelFullData(),
        status: TransactionStatus.DECLINED
      };

      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
        transaction
      );

      const error = await transactionHelper
        .getDeclinedBlockingTransaction(transaction.id)
        .catch(e => e);

      expect(error.errorCode).toEqual(ERROR_CODE.BLOCKING_ID_NOT_FOUND);
    });
  });

  describe('getBeneficiaryAccount', () => {
    const accountNumber = '1';
    const cif = '1';
    const bankCode = 'bc005';
    const accountName = 'a';
    const role = 'NONE';

    const bankCodeInfo: IBankCode = {
      bankCodeId: bankCode,
      name: 'name',
      companyName: 'companyName',
      isBersamaMember: true,
      isAltoMember: true,
      channel: BankChannelEnum.PARTNER,
      type: 'Type',
      isInternal: true
    };

    it('should inquiry account info if cif, accountNo is available and bank code is internal', async () => {
      const foundAccount = { accountNumber, cif };
      (accountRepository.getAccountInfo as jest.Mock).mockResolvedValueOnce(
        foundAccount
      );

      const accountInfo = await transactionHelper.getBeneficiaryAccount(
        accountNumber,
        cif,
        bankCodeInfo
      );

      expect(accountInfo).toEqual({
        ...foundAccount,
        bankCode: bankCodeInfo.bankCodeId,
        role
      });
      expect(accountRepository.getAccountInfo).toHaveBeenCalledWith('1', '1');
    });

    it('should inquiry account with transactionAmount and sourceAccountNo', async () => {
      const foundAccount = { accountNumber, cif };
      const transactionAmount = 1000;
      const sourceAccountNo = 'anyNo';
      const bankCode = { ...bankCodeInfo, isInternal: false };
      (accountRepository.inquiryAccountInfo as jest.Mock).mockResolvedValueOnce(
        foundAccount
      );
      const accountInfo = await transactionHelper.getBeneficiaryAccount(
        accountNumber,
        cif,
        bankCode,
        undefined,
        transactionAmount,
        sourceAccountNo
      );

      expect(accountInfo).toEqual({
        ...foundAccount,
        bankCode: bankCode.bankCodeId,
        role
      });
      expect(accountRepository.inquiryAccountInfo).toHaveBeenCalledWith(
        'bc005',
        '1',
        1000,
        'anyNo',
        '1'
      );
    });

    it('should return beneficiary account info with bankChannel information if beneficiary bank code support BIFAST or EXTERNAL channel', async () => {
      const foundAccount: ITransactionAccount = {
        accountNumber,
        accountName,
        beneficiaryAccountType: 'SVGS',
        preferredChannel: BankChannelEnum.BIFAST
      };
      const transactionAmount = 1000;
      const sourceAccountNo = 'anyNo';
      const bankCode = { ...bankCodeInfo, isInternal: false };

      (accountRepository.inquiryAccountInfo as jest.Mock).mockResolvedValueOnce(
        foundAccount
      );

      const accountInfo = await transactionHelper.getBeneficiaryAccount(
        accountNumber,
        cif,
        bankCode,
        undefined,
        transactionAmount,
        sourceAccountNo
      );

      expect(accountInfo).toEqual({
        ...foundAccount,
        bankCode: bankCodeInfo.bankCodeId,
        role: 'NONE'
      });
      expect(accountRepository.inquiryAccountInfo).toHaveBeenCalledWith(
        bankCode.bankCodeId,
        accountNumber,
        transactionAmount,
        sourceAccountNo,
        cif
      );
    });

    it('should throw error if can not find internal account', async () => {
      (accountRepository.getAccountInfo as jest.Mock).mockResolvedValueOnce({});

      const error = await transactionHelper
        .getBeneficiaryAccount(accountNumber, cif, bankCodeInfo)
        .catch(err => err);

      expect(error.errorCode).toBe(
        ERROR_CODE.BENEFICIARY_ACCOUNT_INQUIRY_FAILED
      );
      expect(accountRepository.getAccountInfo).toHaveBeenCalledWith('1', '1');
    });

    it('should throw error if can not find account', async () => {
      (accountRepository.getAccountInfo as jest.Mock).mockRejectedValueOnce({
        error: {
          error: {
            code: 'ACCOUNT_NUMBER_NOT_FOUND'
          }
        }
      });

      const error = await transactionHelper
        .getBeneficiaryAccount(accountNumber, cif, bankCodeInfo)
        .catch(err => err);

      expect(error.errorCode).toBe(
        ERROR_CODE.BENEFICIARY_ACCOUNT_INQUIRY_FAILED
      );
      expect(accountRepository.getAccountInfo).toHaveBeenCalledWith('1', '1');
    });

    it('should throw error if found internal account with different cif', async () => {
      (accountRepository.getAccountInfo as jest.Mock).mockResolvedValueOnce({});

      const error = await transactionHelper
        .getBeneficiaryAccount(accountNumber, cif, bankCodeInfo)
        .catch(err => err);

      expect(error.errorCode).toBe(
        ERROR_CODE.BENEFICIARY_ACCOUNT_INQUIRY_FAILED
      );
      expect(accountRepository.getAccountInfo).toHaveBeenCalledWith('1', '1');
    });

    it('should throw error if bankCode is internal but missing cif value', async () => {
      const error = await transactionHelper
        .getBeneficiaryAccount(accountNumber, undefined, bankCodeInfo)
        .catch(err => err);

      expect(error.errorCode).toBe(
        ERROR_CODE.BENEFICIARY_ACCOUNT_INQUIRY_FAILED
      );
    });
  });

  describe('validateExternalIdAndPaymentType', () => {
    test('should not throw error when transaction not found', async () => {
      const transaction = getTransactionModelData();

      (transactionRepository.getAllByExternalId as jest.Mock).mockResolvedValueOnce(
        null
      );

      let error;
      try {
        await transactionHelper.validateExternalIdAndPaymentType(transaction);
      } catch (err) {
        error = err;
      }

      expect(error).toBeUndefined();
    });

    test('should not throw error when transaction found with different paymentServiceType', async () => {
      const transaction = getTransactionModelData();

      (transactionRepository.getAllByExternalId as jest.Mock).mockResolvedValueOnce(
        [
          {
            ...transaction,
            paymentServiceType: PaymentServiceTypeEnum.TRANSFER
          }
        ]
      );

      let error;
      try {
        await transactionHelper.validateExternalIdAndPaymentType(transaction);
      } catch (err) {
        error = err;
      }

      expect(error).toBeUndefined();
    });

    test('should throw error when transaction found in DB with same externalId & paymentServiceType', async () => {
      const transaction = getTransactionModelData();

      (transactionRepository.getAllByExternalId as jest.Mock).mockResolvedValue(
        [transaction]
      );

      let error;
      try {
        await transactionHelper.validateExternalIdAndPaymentType(transaction);
      } catch (err) {
        error = err;
      }

      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(ERROR_CODE.UNIQUE_FIELD);
      expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      expect(error.detail).toEqual(
        `Error in validateExternalIdAndPaymentType : Duplicate transaction found with same exteralId = ${transaction.externalId} and paymentServiceType = ${transaction.paymentServiceType}`
      );
    });
  });

  describe('buildOutgoingTransactionModel', () => {
    test('should return model with generated value of externalId and referenceId', () => {
      const mockGeneratedId = 'abcdg238984wekjdk';

      const transactionInput: ITransactionInput = {
        ...getITransactionInput(),
        transactionAmount: 1
      };
      delete transactionInput.externalId;
      delete transactionInput.referenceId;

      (generateUniqueId as jest.Mock).mockImplementation(() => {
        return mockGeneratedId;
      });

      // When
      const result = transactionHelper.buildOutgoingTransactionModel(
        transactionInput
      );

      // Then
      expect(result).toEqual(
        expect.objectContaining({
          referenceId: mockGeneratedId,
          externalId: mockGeneratedId,
          paymentServiceCode: transactionInput.paymentServiceCode,
          paymentServiceType: transactionInput.paymentServiceType,
          beneficiaryRealBankCode: transactionInput.beneficiaryBankCode,
          status: TransactionStatus.PENDING,
          requireThirdPartyOutgoingId: false,
          journey: expect.arrayContaining([
            expect.objectContaining({
              status: TransferJourneyStatusEnum.REQUEST_RECEIVED
            })
          ])
        })
      );
    });
    test('should return model with generated value of referenceId', () => {
      const mockGeneratedId = 'abcdg238984wekjdk';

      const transactionInput: ITransactionInput = {
        ...getITransactionInput(),
        transactionAmount: 1
      };
      delete transactionInput.referenceId;

      (generateUniqueId as jest.Mock).mockImplementation(() => {
        return mockGeneratedId;
      });

      // When
      const result = transactionHelper.buildOutgoingTransactionModel(
        transactionInput
      );

      // Then
      expect(result).toEqual(
        expect.objectContaining({
          referenceId: mockGeneratedId,
          externalId: transactionInput.externalId,
          paymentServiceCode: transactionInput.paymentServiceCode,
          paymentServiceType: transactionInput.paymentServiceType,
          beneficiaryRealBankCode: transactionInput.beneficiaryBankCode,
          status: TransactionStatus.PENDING,
          requireThirdPartyOutgoingId: false,
          journey: expect.arrayContaining([
            expect.objectContaining({
              status: TransferJourneyStatusEnum.REQUEST_RECEIVED
            })
          ])
        })
      );
    });
  });

  describe('getBifastBeneficiaryAdditionalInfo', () => {
    const biFastBankInfo = {
      bankCodeId: 'BC000',
      channel: 'BIFAST',
      companyName: 'Bank Indonesia',
      isAltoMember: false,
      isBersamaMember: false,
      isInternal: false,
      name: 'BIFAST',
      type: 'BANK'
    };

    test('should return Account number response', () => {
      // Given
      const transactionInput: ITransactionInput = {
        ...getITransactionInput()
      };

      //When
      const result = transactionHelper.getBifastBeneficiaryAdditionalInfo(
        transactionInput
      );

      //Then
      expect(result).toEqual([
        transactionInput.beneficiaryAccountNo,
        undefined
      ]);
    });

    test('should return Account number response when additionInformation3 is NONE', () => {
      // Given
      const transactionInput: ITransactionInput = {
        ...getITransactionInput(),
        additionalInformation3: 'NONE'
      };

      //When
      const result = transactionHelper.getBifastBeneficiaryAdditionalInfo(
        transactionInput
      );

      //Then
      expect(result).toEqual([
        transactionInput.beneficiaryAccountNo,
        undefined
      ]);
    });

    test('should return Email & bankCodeInfo in response', () => {
      // Given
      const transactionInput: ITransactionInput = {
        ...getITransactionInput(),
        additionalInformation3: '01',
        additionalInformation4: 'dummy@gmail.com'
      };

      //When
      const result = transactionHelper.getBifastBeneficiaryAdditionalInfo(
        transactionInput
      );

      //Then
      expect(result).toEqual([
        transactionInput.additionalInformation4,
        biFastBankInfo
      ]);
    });

    test('should return Phone number & bankCodeInfo in response', () => {
      // Given
      const transactionInput: ITransactionInput = {
        ...getITransactionInput(),
        additionalInformation3: '02',
        additionalInformation4: '+6288888888'
      };

      //When
      const result = transactionHelper.getBifastBeneficiaryAdditionalInfo(
        transactionInput
      );

      //Then
      expect(result).toEqual([
        transactionInput.additionalInformation4,
        biFastBankInfo
      ]);
    });
  });

  describe('Populate processing info object based on transaction input', () => {
    it('should return model with processing info object taken from payload', () => {
      const mockGeneratedId = 'abcdg238984wekjdk';

      const transactionInput: ITransactionInput = {
        ...getITransactionInput(),
        transactionAmount: 1,
        processingInfo: {
          channel: 'BI_SNAP_OPEN_API',
          method: ExecutionMethodEnum.ONE_BY_ONE,
          partner: 'ARTAJASA001'
        },
        paymentInstructionExecution: {
          paymentInstructionCode: 'paymentInstructionCode',
          executionType: TransactionExecutionTypeEnum.DIRECT_TRANSFER,
          topics: [TransactionTopicConstant.TRANSFER_DIRECT_TRANSACTION_BIFAST]
        }
      };
      delete transactionInput.externalId;
      delete transactionInput.referenceId;

      (generateUniqueId as jest.Mock).mockImplementation(() => {
        return mockGeneratedId;
      });

      // When
      const result = transactionHelper.buildOutgoingTransactionModel(
        transactionInput
      );

      // Then
      expect(result).toEqual(
        expect.objectContaining({
          referenceId: mockGeneratedId,
          externalId: mockGeneratedId,
          paymentServiceCode: transactionInput.paymentServiceCode,
          paymentServiceType: transactionInput.paymentServiceType,
          beneficiaryRealBankCode: transactionInput.beneficiaryBankCode,
          status: TransactionStatus.PENDING,
          requireThirdPartyOutgoingId: false,
          journey: expect.arrayContaining([
            expect.objectContaining({
              status: TransferJourneyStatusEnum.REQUEST_RECEIVED
            })
          ]),
          processingInfo: {
            channel: 'BI_SNAP_OPEN_API',
            method: ExecutionMethodEnum.ONE_BY_ONE,
            partner: 'ARTAJASA001'
          }
        })
      );
    });
  });

  describe('setPaymentRailToTransactionData', () => {
    test('should set paymentRail for the transaction model', () => {
      const testCases = [
        {
          railToSet: undefined, // Should set INTERNAL if railToSet is not passed
          expectedRail: Rail.INTERNAL
        },
        {
          railToSet: Rail.INTERNAL,
          expectedRail: Rail.INTERNAL
        },
        {
          railToSet: Rail.BI_FAST,
          expectedRail: Rail.BI_FAST
        },
        {
          railToSet: Rail.GOBILLS,
          expectedRail: Rail.GOBILLS
        },
        {
          railToSet: Rail.IRIS,
          expectedRail: Rail.IRIS
        },
        {
          railToSet: Rail.RTGS,
          expectedRail: Rail.RTGS
        },
        {
          railToSet: Rail.BI_FAST,
          expectedRail: Rail.BI_FAST
        },
        {
          railToSet: Rail.SKN,
          expectedRail: Rail.SKN
        },
        {
          railToSet: Rail.TOKOPEDIA,
          expectedRail: Rail.TOKOPEDIA
        },
        {
          railToSet: Rail.ALTO_QRIS,
          expectedRail: Rail.ALTO_QRIS
        },
        {
          interchange: undefined, // If interchange is not defined for EURONET
          railToSet: Rail.EURONET,
          expectedRail: Rail.EURONET
        },
        {
          interchange: BankNetworkEnum.ARTAJASA, // If interchange is defined for EURONET merge rail and interchange
          railToSet: Rail.EURONET,
          expectedRail: Rail.EURONET + '_' + BankNetworkEnum.ARTAJASA
        }
      ];

      testCases.forEach(testCase => {
        // Given
        const transactionModel: ITransactionModel = {
          ...getTransactionModelFullData(),
          id: '1',
          interchange: testCase?.interchange
        };

        // When
        transactionHelper.setPaymentRailToTransactionData(
          transactionModel,
          testCase.railToSet
        );

        // Then
        expect(transactionModel.processingInfo?.paymentRail).toBe(
          testCase.expectedRail
        );
      });
    });
  });
});
