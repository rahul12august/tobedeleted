import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import { TransferFailureReasonActor } from '../transaction.enum';
import { IFailureReasonModel, ITransactionModel } from '../transaction.model';
import { ITransactionSubmissionTemplate } from '../transaction.type';
import sut from '../transactionExecution.service';
import {
  getITransactionInput,
  getTransactionModelData
} from '../__mocks__/transaction.data';
import transactionRecommendService from '../transactionRecommend.service';
import transactionHelper from '../transaction.helper';
import transactionRepository from '../transaction.repository';

import { getRecommendedService as getRecommendedServiceSample } from '../__mocks__/transaction.data';
import { PaymentServiceTypeEnum } from '@dk/module-common';
import { NewEntity } from '../../common/common.type';

jest.mock('../../transactionUsage/transactionUsage.service');
jest.mock('../transaction.helper');
jest.mock('../transaction.repository');
jest.mock('../../monitoringEvent/monitoringEvent.producer');
jest.mock('../transactionRecommend.service');

describe('Transaction failures with error reason', () => {
  let lastPersistedFailureReason: IFailureReasonModel | undefined;

  beforeEach(() => {
    (transactionRecommendService.getRecommendService as jest.Mock).mockReturnValue(
      getRecommendedServiceSample()
    );

    (transactionRepository.update as jest.Mock).mockImplementation(
      (id: any, trxModel: Partial<ITransactionModel>) => {
        lastPersistedFailureReason = trxModel.failureReason;
      }
    );
  });

  afterEach(() => {
    jest.resetAllMocks();
    lastPersistedFailureReason = undefined;
  });

  const withError = (
    preparedError: TransferAppError
  ): ITransactionSubmissionTemplate => {
    return {
      submitTransaction: (): Promise<ITransactionModel> => {
        throw preparedError;
      },
      isEligible: (): boolean => {
        return true;
      }
    };
  };
  const withTrxModel = (model: NewEntity<ITransactionModel>): void => {
    (transactionHelper.buildNewTransactionModel as jest.Mock).mockReturnValue(
      model
    );

    (transactionRepository.create as jest.Mock).mockReturnValue(model);
  };

  it('Should persist failureReason to the transaction database for QRIS trx', async () => {
    const trxInput = getITransactionInput();
    const preparedError: TransferAppError = new TransferAppError(
      'More details for this error',
      TransferFailureReasonActor.JAGO,
      ERROR_CODE.ALTO_SERVER_ERROR
    );

    const localTrxModel = getTransactionModelData();
    localTrxModel.paymentServiceType = PaymentServiceTypeEnum.QRIS;

    withTrxModel(localTrxModel);

    const responseError = await sut
      .initTransaction(trxInput, withError(preparedError))
      .catch(e => e);
    expect(responseError).toBeInstanceOf(Error);

    expect(lastPersistedFailureReason).toBeDefined();
    expect(lastPersistedFailureReason?.actor).toBe(preparedError.actor);
    expect(lastPersistedFailureReason?.type).toBe(preparedError.errorCode);
    expect(lastPersistedFailureReason?.detail).toBe(preparedError.detail);
  });

  it('Should persist instances of TransferAppError to the transaction database', async () => {
    const trxInput = getITransactionInput();
    const preparedError: TransferAppError = new TransferAppError(
      'Sample message specified as detail',
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
    );

    withTrxModel(getTransactionModelData());

    const responseError = await sut
      .initTransaction(trxInput, withError(preparedError))
      .catch(e => e);
    expect(responseError).toBeInstanceOf(Error);

    expect(lastPersistedFailureReason).toBeDefined();
    expect(lastPersistedFailureReason?.actor).toBe(
      TransferFailureReasonActor.SUBMITTER
    );
    expect(lastPersistedFailureReason?.type).toBe(
      ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
    );
    expect(lastPersistedFailureReason?.detail).toBe(preparedError.detail);
  });
});
