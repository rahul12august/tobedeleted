import { getTransactionModelFullData } from '../__mocks__/transaction.data';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../transaction.enum';
import transactionIdempotencyHelper from '../transactionIdempotency.helper';
import transactionRepository from '../transaction.repository';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import { IFailureReasonModel } from '../transaction.model';

jest.mock('../transaction.repository');

describe('matchExistingTransactionByIdempotencyKey', () => {
  const idempotencyKey = 'c754ec35-3f76-4c10-a5ca-8fcdc6479aae';

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should return null if idempotency key is not defined', async () => {
    // Given
    const transactionModel = {
      ...getTransactionModelFullData(),
      status: TransactionStatus.SUCCEED,
      idempotencyKey: undefined
    };

    (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce({
      id: 1,
      ...transactionModel
    });

    // When
    const result = await transactionIdempotencyHelper.matchExistingTransactionByIdempotencyKey(
      transactionModel
    );

    // Then
    expect(result).toBeNull();
  });

  it('should return null if previous transaction with the same idempotency key is not exists', async () => {
    // Given
    const transactionModel = {
      ...getTransactionModelFullData(),
      status: TransactionStatus.SUCCEED,
      idempotencyKey: idempotencyKey
    };

    (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
      null
    );

    // When
    const result = await transactionIdempotencyHelper.matchExistingTransactionByIdempotencyKey(
      transactionModel
    );

    // Then
    expect(result).toBeNull();
  });

  it('should return null if previous transaction with the same idempotency key has different externalId', async () => {
    // Given
    const transactionModel = {
      ...getTransactionModelFullData(),
      status: TransactionStatus.SUCCEED,
      idempotencyKey: idempotencyKey,
      externalId: 'someExternalId'
    };

    (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce({
      id: 1,
      ...transactionModel,
      externalId: 'differentExternalId'
    });

    // When
    const result = await transactionIdempotencyHelper.matchExistingTransactionByIdempotencyKey(
      transactionModel
    );

    // Then
    expect(result).toBeNull();
  });

  describe('if previous transaction exists in database with the same externalId and idempotency key', () => {
    it('should return previous transaction model if transaction status is SUCCEED', async () => {
      // Given
      const transactionModel = {
        ...getTransactionModelFullData(),
        status: TransactionStatus.SUCCEED,
        idempotencyKey: idempotencyKey,
        externalId: 'sameExternalId'
      };

      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
        { id: 1, ...transactionModel, externalId: 'sameExternalId' }
      );

      // When
      const result = await transactionIdempotencyHelper.matchExistingTransactionByIdempotencyKey(
        transactionModel
      );

      // Then
      expect(result).toEqual({
        id: 1,
        ...transactionModel,
        externalId: 'sameExternalId'
      });
    });

    it('should throw an error if transaction status is PENDING', async () => {
      // Given
      const transactionModel = {
        ...getTransactionModelFullData(),
        status: TransactionStatus.PENDING,
        idempotencyKey: idempotencyKey,
        externalId: 'sameExternalId'
      };

      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
        { id: 1, ...transactionModel, externalId: 'sameExternalId' }
      );

      // When
      const error = await transactionIdempotencyHelper
        .matchExistingTransactionByIdempotencyKey(transactionModel)
        .catch(e => e);

      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.TRANSACTION_STILL_IN_PROGRESS);
      expect(error.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
      expect(error.detail).toEqual('The transaction still in progress');
    });

    it('should throw an error if transaction status is SUBMITTED', async () => {
      // Given
      const transactionModel = {
        ...getTransactionModelFullData(),
        status: TransactionStatus.SUBMITTED,
        idempotencyKey: idempotencyKey,
        externalId: 'sameExternalId'
      };

      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
        { id: 1, ...transactionModel, externalId: 'sameExternalId' }
      );

      // When
      const error = await transactionIdempotencyHelper
        .matchExistingTransactionByIdempotencyKey(transactionModel)
        .catch(e => e);

      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.TRANSACTION_STILL_IN_PROGRESS);
      expect(error.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
      expect(error.detail).toEqual('The transaction still in progress');
    });

    it('should throw an error if transaction status is SUBMITTING', async () => {
      // Given
      const transactionModel = {
        ...getTransactionModelFullData(),
        status: TransactionStatus.SUBMITTING,
        idempotencyKey: idempotencyKey,
        externalId: 'sameExternalId'
      };

      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
        { id: 1, ...transactionModel, externalId: 'sameExternalId' }
      );

      // When
      const error = await transactionIdempotencyHelper
        .matchExistingTransactionByIdempotencyKey(transactionModel)
        .catch(e => e);

      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.TRANSACTION_STILL_IN_PROGRESS);
      expect(error.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
      expect(error.detail).toEqual('The transaction still in progress');
    });

    it('should throw an error if transaction status is REVERTED', async () => {
      // Given
      const transactionModel = {
        ...getTransactionModelFullData(),
        status: TransactionStatus.REVERTED,
        idempotencyKey: idempotencyKey,
        externalId: 'sameExternalId'
      };

      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
        { id: 1, ...transactionModel, externalId: 'sameExternalId' }
      );

      // When
      const error = await transactionIdempotencyHelper
        .matchExistingTransactionByIdempotencyKey(transactionModel)
        .catch(e => e);

      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.TRANSACTION_ALREADY_REVERSED);
      expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      expect(error.detail).toEqual(
        'Transaction already reversed : 1, externalId : sameExternalId'
      );
    });

    it('should throw an error if transaction status is REVERSAL_FAILED', async () => {
      // Given
      const expectedFailureReason: IFailureReasonModel = {
        detail: 'Failed to reverse',
        actor: TransferFailureReasonActor.UNKNOWN,
        type: ERROR_CODE.FAILED_TO_REVERSE
      };

      const transactionModel = {
        ...getTransactionModelFullData(),
        status: TransactionStatus.REVERSAL_FAILED,
        idempotencyKey: idempotencyKey,
        externalId: 'sameExternalId',
        failureReason: expectedFailureReason
      };

      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
        { id: 1, ...transactionModel, externalId: 'sameExternalId' }
      );

      // When
      const error = await transactionIdempotencyHelper
        .matchExistingTransactionByIdempotencyKey(transactionModel)
        .catch(e => e);

      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(expectedFailureReason.type);
      expect(error.actor).toEqual(expectedFailureReason.actor);
      expect(error.detail).toEqual(expectedFailureReason.detail);
    });

    it('should throw an error if transaction status is DECLINED', async () => {
      // Given
      const expectedFailureReason: IFailureReasonModel = {
        detail: 'Insufficient account balance.',
        actor: TransferFailureReasonActor.SUBMITTER,
        type: ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
      };

      const transactionModel = {
        ...getTransactionModelFullData(),
        status: TransactionStatus.DECLINED,
        idempotencyKey: idempotencyKey,
        externalId: 'sameExternalId',
        failureReason: expectedFailureReason
      };

      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
        { id: 1, ...transactionModel, externalId: 'sameExternalId' }
      );

      // When
      const error = await transactionIdempotencyHelper
        .matchExistingTransactionByIdempotencyKey(transactionModel)
        .catch(e => e);

      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(expectedFailureReason.type);
      expect(error.actor).toEqual(expectedFailureReason.actor);
      expect(error.detail).toEqual(expectedFailureReason.detail);
    });

    it('should throw an error if transaction status is DECLINED and failure reason is not set', async () => {
      // Given
      const transactionModel = {
        ...getTransactionModelFullData(),
        status: TransactionStatus.DECLINED,
        idempotencyKey: idempotencyKey,
        externalId: 'sameExternalId',
        failureReason: undefined
      };

      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
        { id: 1, ...transactionModel, externalId: 'sameExternalId' }
      );

      // When
      const error = await transactionIdempotencyHelper
        .matchExistingTransactionByIdempotencyKey(transactionModel)
        .catch(e => e);

      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.UNEXPECTED_ERROR);
      expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      expect(error.detail).toEqual('Transaction declined');
    });
  });
});
