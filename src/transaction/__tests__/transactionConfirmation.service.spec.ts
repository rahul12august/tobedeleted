import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import {
  ConfirmTransactionRequest,
  ConfirmTransactionResponse,
  IConfirmTransactionInput
} from '../transaction.type';
import { ITransactionModel } from '../transaction.model';
import { REVERSED_REASON_ERROR_FROM_SWITCHING } from '../transaction.constant';
import {
  ConfirmTransactionStatus,
  ExecutionTypeEnum,
  PaymentRequestStatus,
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from '../transaction.enum';
import {
  AccountType,
  BankNetworkEnum,
  PaymentInstructionCode,
  PaymentServiceTypeEnum,
  TransactionExecutionTypeEnum
} from '@dk/module-common';
import switchingTransactionService, {
  extractChangeFromConfirmTransactionRequest,
  responseMessageToPaymentInstruction
} from '../transactionConfirmation.service';
import transactionReversalHelper from '../transactionReversal.helper';
import coreBankingHelper from '../transactionCoreBanking.helper';
import depositRepository from '../../coreBanking/deposit.repository';
import {
  getExpectedInterchangeData,
  getTransactionDetail,
  getTransactionForConfirmTranscation
} from '../__mocks__/transaction.data';
import { getFeeAmountData } from '../../fee/__mocks__/fee.data';
import {
  mockConfirmTransactionRequest,
  mockTransactionFromDb
} from '../__mocks__/transactionConfirmation.data';
import transactionRepository from '../transaction.repository';
import transactionProducer from '../transaction.producer';
import {
  BillPaymentTopicConstant,
  PaymentInstructionTopicConstant
} from '@dk/module-message';
import { createIncomingTransactionExternalId } from '../transaction.util';
import transactionConfirmationHelper from '../transactionConfirmation.helper';
import entitlementFlag from '../../common/entitlementFlag';
import { setTransactionInfoTracker } from '../../common/contextHandler';
import transactionHelper from '../transaction.helper';
import transactionUsageService from '../../transactionUsage/transactionUsage.service';

jest.mock('../transaction.repository');
jest.mock('../transactionReversal.helper');
jest.mock('../transactionCoreBanking.helper');
jest.mock('../../coreBanking/deposit.repository');
jest.mock('../transaction.producer');
jest.mock('../transaction.producer');
jest.mock('../transactionConfirmation.helper');
jest.mock('../transaction.helper');
jest.mock('../../common/contextHandler');

describe('transactionConfirmation.service', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  describe('confirmTransaction', () => {
    // case valid request
    const input: ConfirmTransactionRequest = mockConfirmTransactionRequest();
    const mockedTransactionFromDb: ITransactionModel = {
      ...mockTransactionFromDb(),
      beneficiaryAccountNo: input.accountNo,
      thirdPartyOutgoingId: input.externalId
    };
    const expectedResponse: ConfirmTransactionResponse = {
      transactionResponseID: input.transactionResponseID,
      externalId: mockedTransactionFromDb.externalId,
      id: mockedTransactionFromDb.id,
      coreBankingTransactions: mockedTransactionFromDb.coreBankingTransactions
    };
    const mockUpdateTransactionStatusSuccess = () => {
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...mockedTransactionFromDb,
        status: TransactionStatus.SUCCEED
      });
    };
    const mockGetTransactionReturnData = (data?: ITransactionModel) => {
      if (!data) {
        data = mockedTransactionFromDb;
      }
      (transactionRepository.updateIfExists as jest.Mock).mockResolvedValue(
        data
      );
      (transactionRepository.update as jest.Mock).mockResolvedValue({
        ...data,
        status: TransactionStatus.SUCCEED
      });
    };
    const mockGetTransactionReturnNull = () => {
      (transactionRepository.updateIfExists as jest.Mock).mockResolvedValueOnce(
        null
      );
    };

    const mockReverseTransactionSuccess = () => {
      (transactionReversalHelper.revertFailedTransaction as jest.Mock).mockResolvedValueOnce(
        {}
      );
    };

    beforeEach(() => {
      jest.resetAllMocks();
      mockGetTransactionReturnData();
    });

    it('should update transaction status to SUCCEED when payload contains ResponseCode 200', async () => {
      mockUpdateTransactionStatusSuccess();

      const result = await switchingTransactionService.confirmTransaction(
        input
      );
      expect(result).toEqual(expectedResponse);
      expect(transactionRepository.updateIfExists).toBeCalledWith(
        input,
        createIncomingTransactionExternalId(
          input.externalId,
          input.transactionDate,
          input.accountNo
        )
      );
      expect(transactionRepository.update).toBeCalledWith(
        mockedTransactionFromDb.id,
        {
          ...mockedTransactionFromDb,
          status: TransactionStatus.SUCCEED
        }
      );
    });

    it('should throw if cannot update transaction info', async () => {
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce(null);
      try {
        await switchingTransactionService.confirmTransaction(input);
      } catch (e) {
        expect(e.errorCode).toEqual(ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION);
      }
    });

    it('should not validate accountNo and transactionDate if they are optional', async () => {
      mockUpdateTransactionStatusSuccess();

      const result = await switchingTransactionService.confirmTransaction({
        ...input,
        accountNo: '100023993999400000',
        transactionDate: '200212092500'
      });
      expect(result).toEqual(expectedResponse);
      expect(transactionRepository.updateIfExists).toBeCalledWith(
        input,
        createIncomingTransactionExternalId(
          input.externalId,
          input.transactionDate,
          input.accountNo
        )
      );
      expect(transactionRepository.update).toBeCalledWith(
        mockedTransactionFromDb.id,
        {
          ...mockedTransactionFromDb,
          status: TransactionStatus.SUCCEED
        }
      );
    });

    it('should reverse transaction when payload contains error different than TIME_OUT', async () => {
      const testInput: ConfirmTransactionRequest = {
        ...input,
        responseCode: ConfirmTransactionStatus.ERROR_EXPECTED,
        responseMessage: 'Sample error message from switching_euroNet'
      };
      mockReverseTransactionSuccess();

      const result = await switchingTransactionService.confirmTransaction(
        testInput
      );
      expect(result).toEqual(expectedResponse);
      expect(transactionReversalHelper.revertFailedTransaction).toBeCalledWith(
        mockedTransactionFromDb.id,
        REVERSED_REASON_ERROR_FROM_SWITCHING,
        mockedTransactionFromDb
      );
    });

    it('should reverse refund transaction when payload contains error different than TIME_OUT', async () => {
      const testInput: ConfirmTransactionRequest = {
        ...input,
        responseCode: ConfirmTransactionStatus.ERROR_EXPECTED,
        responseMessage: 'Sample error message from switching_euroNet'
      };
      const transaction = {
        ...mockedTransactionFromDb,
        refund: {
          originalTransactionId: '12345'
        }
      };
      mockReverseTransactionSuccess();
      mockGetTransactionReturnData(transaction);
      (transactionConfirmationHelper.updateRefundAmount as jest.Mock).mockResolvedValue(
        null
      );

      const result = await switchingTransactionService.confirmTransaction(
        testInput
      );
      expect(result).toEqual(expectedResponse);
      expect(transactionReversalHelper.revertFailedTransaction).toBeCalledWith(
        mockedTransactionFromDb.id,
        REVERSED_REASON_ERROR_FROM_SWITCHING,
        transaction
      );
    });

    it('should return transactionResponseID and update confirmationCode when payload contains responseCode is REQUEST_TIMEOUT', async () => {
      const testInput: ConfirmTransactionRequest = {
        ...input,
        responseCode: ConfirmTransactionStatus.REQUEST_TIMEOUT,
        responseMessage: 'Request timeout'
      };

      const result = await switchingTransactionService.confirmTransaction(
        testInput
      );
      expect(result).toEqual(expectedResponse);
      expect(transactionRepository.update).toBeCalledWith(
        mockedTransactionFromDb.id,
        {
          confirmationCode: `${ConfirmTransactionStatus.REQUEST_TIMEOUT}`
        }
      );
    });

    it('should change transaction state to declined', async () => {
      const input: IConfirmTransactionInput = {
        externalId: '0093384848',
        responseCode: ConfirmTransactionStatus.SUCCESS,
        interchange: undefined
      };

      mockUpdateTransactionStatusSuccess();

      const model: ITransactionModel = {
        ...getTransactionDetail(),
        status: TransactionStatus.SUBMITTED,
        executionType: ExecutionTypeEnum.BLOCKING
      };

      (depositRepository.reverseAuthorizationHold as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.MAMBU_TRANSACTION_AUTHORIZATION_HOLD_EXPIRED
        )
      );

      await switchingTransactionService.reverseTransaction(input, model);

      expect(transactionRepository.update).toBeCalledWith(model.id, {
        ...model,
        status: TransactionStatus.DECLINED,
        failureReason: {
          actor: TransferFailureReasonActor.UNKNOWN,
          detail: 'reverseTransaction',
          type: ERROR_CODE.REVERSE_TRANSACTION
        }
      });
    });

    it('should process transaction when status is SUBMITTING', async () => {
      mockUpdateTransactionStatusSuccess();
      mockGetTransactionReturnData({
        ...mockedTransactionFromDb,
        status: TransactionStatus.SUBMITTING,
        journey: [
          {
            status: TransferJourneyStatusEnum.SUBMITTING_THIRD_PARTY
          },
          {
            status: TransferJourneyStatusEnum.TRANSFER_CONFIRMATION_RECEIVED
          }
        ]
      });

      const result: ConfirmTransactionResponse = await switchingTransactionService.confirmTransaction(
        input
      );
      expect(result).toEqual(expectedResponse);
      expect(transactionRepository.updateIfExists).toBeCalledWith(
        input,
        createIncomingTransactionExternalId(
          input.externalId,
          input.transactionDate,
          input.accountNo
        )
      );
      expect(transactionRepository.update).toBeCalledTimes(3);
      expect(transactionRepository.update).lastCalledWith(
        mockedTransactionFromDb.id,
        expect.objectContaining({
          status: TransactionStatus.SUCCEED
        })
      );
    });

    describe('validation rules', () => {
      it('INVALID_EXTERNALID', async () => {
        mockGetTransactionReturnNull();
        const result = await switchingTransactionService
          .confirmTransaction(input)
          .catch(err => err);
        expect(result).toBeInstanceOf(TransferAppError);
        expect(result.errorCode).toEqual(ERROR_CODE.INVALID_EXTERNALID);
      });

      it('INVALID_AMOUNT', async () => {
        const testInput: IConfirmTransactionInput = {
          ...input,
          amount: 1000
        };
        const result = await switchingTransactionService
          .confirmTransaction(testInput)
          .catch(err => err);
        expect(result).toBeInstanceOf(TransferAppError);
        expect(result.errorCode).toEqual(ERROR_CODE.INVALID_AMOUNT);
      });

      // it('INVALID_AMOUNT when service code is DIGITAL_GOODS', async () => {
      //   mockGetTransactionReturnData({
      //     ...mockedTransactionFromDb,
      //     paymentServiceCode: 'DIGITAL_GOODS'
      //   });
      //   const testInput: IConfirmTransactionInput = {
      //     ...input,
      //     amount: 1000
      //   };
      //   const result = await switchingTransactionService
      //     .confirmTransaction(testInput)
      //     .catch(err => err);
      //   expect(result).toBeInstanceOf(TransferAppError);
      //   expect(result.errorCode).toEqual(ERROR_CODE.INVALID_AMOUNT);
      // });

      it('should skip processing and throw an exception when transaction is already success', async () => {
        mockGetTransactionReturnData({
          ...mockedTransactionFromDb,
          status: TransactionStatus.SUCCEED
        });

        const error = await switchingTransactionService
          .confirmTransaction(input)
          .catch(error => error);

        expect(error).toBeDefined();
        expect(error.errorCode).toEqual(
          ERROR_CODE.TRANSACTION_ALREADY_CONFIRMED
        );
        expect(error.detail).toEqual(
          'Transaction already confirmed : 5df72fa3490b56067991ae39, externalId : externalId'
        );
        expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      });

      it('should skip processing and throw an exception when transaction is already success', async () => {
        mockGetTransactionReturnData({
          ...mockedTransactionFromDb,
          status: TransactionStatus.SUCCEED
        });
        const updatedInput = {
          ...input,
          responseCode: 408
        };

        const error = await switchingTransactionService
          .confirmTransaction(updatedInput)
          .catch(err => err);

        expect(error).toBeDefined();
        expect(error.errorCode).toEqual(
          ERROR_CODE.TRANSACTION_ALREADY_CONFIRMED
        );
        expect(error.detail).toEqual(
          'Transaction already confirmed : 5df72fa3490b56067991ae39, externalId : externalId'
        );
        expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      });

      it('should skip processing and throw an exception when transaction is already declined', async () => {
        mockGetTransactionReturnData({
          ...mockedTransactionFromDb,
          status: TransactionStatus.DECLINED
        });
        const updatedInput = {
          ...input,
          responseCode: 400
        };

        const error = await switchingTransactionService
          .confirmTransaction(updatedInput)
          .catch(error => error);

        expect(error).toBeDefined();
        expect(error.errorCode).toEqual(
          ERROR_CODE.TRANSACTION_ALREADY_REVERSED
        );
        expect(error.detail).toEqual(
          'Transaction already reversed : 5df72fa3490b56067991ae39, externalId : externalId'
        );
        expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      });

      it('should throw an exception when transaction is RDN Reversal', async () => {
        mockGetTransactionReturnData({
          ...mockedTransactionFromDb,
          beneficiaryAccountType: AccountType.RDN,
          status: TransactionStatus.SUCCEED
        });
        const updatedInput = {
          ...input,
          responseCode: 400
        };

        const error = await switchingTransactionService
          .confirmTransaction(updatedInput)
          .catch(error => error);

        expect(error).toBeDefined();
        expect(error.errorCode).toEqual(ERROR_CODE.RDN_REVERSAL_NOT_ALLOWED);
        expect(error.detail).toEqual(
          'RDN Reversal not allowed, transactionId: : 5df72fa3490b56067991ae39, externalId : externalId'
        );
        expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      });

      it('should throw an exception when transaction is RDS Reversal', async () => {
        mockGetTransactionReturnData({
          ...mockedTransactionFromDb,
          beneficiaryAccountType: AccountType.RDS,
          status: TransactionStatus.SUCCEED
        });
        const updatedInput = {
          ...input,
          responseCode: 400
        };

        const error = await switchingTransactionService
          .confirmTransaction(updatedInput)
          .catch(error => error);

        expect(error).toBeDefined();
        expect(error.errorCode).toEqual(ERROR_CODE.RDN_REVERSAL_NOT_ALLOWED);
        expect(error.detail).toEqual(
          'RDN Reversal not allowed, transactionId: : 5df72fa3490b56067991ae39, externalId : externalId'
        );
        expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      });
    });

    describe('should execute blocking debit and fee if type is blocking transaction', () => {
      const blockingTransaction = {
        externalId: 'externalId',
        referenceId: '34839845995-3093059-03434',
        beneficiaryAccountNo: input.accountNo,
        beneficiaryAccountName: 'External Account Name From Switching',
        beneficiaryBankCode: '542',
        sourceAccountName: 'Jago account name',
        sourceCIF: 'sourceCIF',
        sourceBankCode: 'BC002',
        sourceAccountNo: 'sourceAccountNo',
        createdAt: new Date('2020-02-12T09:25:00.041Z'),
        status: TransactionStatus.SUBMITTED,
        paymentServiceCode: 'RTOL',
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        transactionAmount: 900000,
        id: '5df72fa3490b56067991ae39',
        thirdPartyOutgoingId: input.externalId,
        requireThirdPartyOutgoingId: true,
        executionType: ExecutionTypeEnum.BLOCKING,
        cardId: 'cardId',
        blockingId: 'blockingId',
        debitTransactionCode: 'debitTransactionCode',
        debitTransactionChannel: 'debitTransactionChannel',
        fees: [
          {
            feeAmount: 3500,
            customerTc: 'customerTc',
            customerTcChannel: 'customerTcChannel',
            subsidiary: true,
            blockingId: 'blockingId'
          }
        ]
      };
      it('should change status to success if no blocking', async () => {
        const noneBlocking = {
          ...blockingTransaction,
          cardId: null,
          executionType: ExecutionTypeEnum.NONE_BLOCKING
        };
        (transactionRepository.updateIfExists as jest.Mock).mockResolvedValueOnce(
          noneBlocking
        );
        (transactionRepository.update as jest.Mock).mockResolvedValueOnce(
          noneBlocking
        );

        const result = await switchingTransactionService.confirmTransaction(
          input
        );
        expect(result).toEqual(expectedResponse);
        expect(transactionRepository.updateIfExists).toBeCalledWith(
          input,
          createIncomingTransactionExternalId(
            input.externalId,
            input.transactionDate,
            input.accountNo
          )
        );
        expect(transactionRepository.update).toBeCalledWith(
          mockedTransactionFromDb.id,
          {
            ...noneBlocking,
            status: TransactionStatus.SUCCEED
          }
        );
      });

      it('should resolve blocking and change status to success if blocking', async () => {
        (transactionRepository.updateIfExists as jest.Mock).mockResolvedValueOnce(
          blockingTransaction
        );

        (transactionRepository.update as jest.Mock).mockResolvedValueOnce(
          blockingTransaction
        );
        mockUpdateTransactionStatusSuccess();

        const result = await switchingTransactionService.confirmTransaction(
          input
        );
        expect(result).toEqual(expectedResponse);
        expect(transactionRepository.updateIfExists).toBeCalledWith(
          input,
          createIncomingTransactionExternalId(
            input.externalId,
            input.transactionDate,
            input.accountNo
          )
        );
        expect(coreBankingHelper.submitCardTransaction).toBeCalledWith(
          blockingTransaction.cardId,
          blockingTransaction.blockingId,
          blockingTransaction,
          blockingTransaction.debitTransactionCode,
          blockingTransaction.debitTransactionChannel
        );
        expect(coreBankingHelper.submitFeesCardTransaction).toBeCalledWith(
          blockingTransaction.cardId,
          blockingTransaction
        );
        expect(transactionRepository.update).toBeCalledWith(
          mockedTransactionFromDb.id,
          {
            ...blockingTransaction,
            status: TransactionStatus.SUCCEED
          }
        );
      });
    });

    describe('should revert blocking debit and fee if blocking transaction fail', () => {
      const blockingTransaction = {
        externalId: 'externalId',
        referenceId: '34839845995-3093059-03434',
        beneficiaryAccountNo: input.accountNo,
        beneficiaryAccountName: 'External Account Name From Switching',
        beneficiaryBankCode: '542',
        sourceAccountName: 'Jago account name',
        sourceCIF: 'sourceCIF',
        sourceBankCode: 'BC002',
        sourceAccountNo: 'sourceAccountNo',
        createdAt: new Date('2020-02-12T09:25:00.041Z'),
        status: TransactionStatus.SUBMITTED,
        paymentServiceCode: 'RTOL',
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        transactionAmount: 900000,
        id: '5df72fa3490b56067991ae39',
        thirdPartyOutgoingId: input.externalId,
        requireThirdPartyOutgoingId: true,
        executionType: ExecutionTypeEnum.BLOCKING,
        cardId: 'cardId',
        blockingId: 'blockingId',
        debitTransactionCode: 'debitTransactionCode',
        debitTransactionChannel: 'debitTransactionChannel',
        fees: [
          {
            feeAmount: 3500,
            customerTc: 'customerTc',
            customerTcChannel: 'customerTcChannel',
            subsidiary: true,
            blockingId: 'feeBlockingId'
          }
        ]
      };
      it('should release blocking', async () => {
        (transactionRepository.updateIfExists as jest.Mock).mockResolvedValueOnce(
          blockingTransaction
        );

        (transactionRepository.update as jest.Mock).mockResolvedValueOnce(
          blockingTransaction
        );
        const _input: ConfirmTransactionRequest = input;
        _input.responseCode = 400;
        mockUpdateTransactionStatusSuccess();

        const result = await switchingTransactionService.confirmTransaction({
          ...input,
          responseCode: 400
        });
        expect(result).toEqual(expectedResponse);
        expect(transactionRepository.updateIfExists).toBeCalledWith(
          _input,
          createIncomingTransactionExternalId(
            input.externalId,
            input.transactionDate,
            input.accountNo
          )
        );
        expect(depositRepository.reverseAuthorizationHold).toBeCalledWith(
          blockingTransaction.cardId,
          blockingTransaction.blockingId
        );
      });
    });

    describe('should revert transaction if confirm fail', () => {
      const succeededTransaction = {
        externalId: 'externalId',
        referenceId: '34839845995-3093059-03434',
        beneficiaryAccountNo: input.accountNo,
        beneficiaryAccountName: 'External Account Name From Switching',
        beneficiaryBankCode: '542',
        sourceAccountName: 'Jago account name',
        sourceCIF: 'sourceCIF',
        sourceBankCode: 'BC002',
        sourceAccountNo: 'sourceAccountNo',
        createdAt: new Date('2020-02-12T09:25:00.041Z'),
        status: TransactionStatus.SUCCEED,
        paymentServiceCode: 'RTOL',
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        transactionAmount: 900000,
        id: '5df72fa3490b56067991ae39',
        thirdPartyOutgoingId: input.externalId,
        requireThirdPartyOutgoingId: true,
        executionType: ExecutionTypeEnum.BLOCKING,
        cardId: 'cardId',
        blockingId: 'blockingId',
        debitTransactionCode: 'debitTransactionCode',
        debitTransactionChannel: 'debitTransactionChannel',
        fees: [
          {
            feeAmount: 3500,
            customerTc: 'customerTc',
            customerTcChannel: 'customerTcChannel',
            subsidiary: true,
            blockingId: 'feeBlockingId'
          }
        ]
      };
      it('should call to revertFailedTransaction', async () => {
        (transactionRepository.updateIfExists as jest.Mock).mockResolvedValueOnce(
          succeededTransaction
        );
        const _input: ConfirmTransactionRequest = input;
        _input.responseCode = 400;

        const result = await switchingTransactionService.confirmTransaction({
          ...input,
          responseCode: 400
        });
        expect(result).toEqual(expectedResponse);
        expect(transactionRepository.updateIfExists).toBeCalledWith(
          _input,
          createIncomingTransactionExternalId(
            input.externalId,
            input.transactionDate,
            input.accountNo
          )
        );
        expect(
          transactionReversalHelper.revertFailedTransaction
        ).toBeCalledWith(
          succeededTransaction.id,
          REVERSED_REASON_ERROR_FROM_SWITCHING,
          succeededTransaction
        );
      });
    });

    it('should call reversal entitlement info when flag is enabled', async () => {
      mockUpdateTransactionStatusSuccess();
      entitlementFlag.isEnabled = jest.fn().mockResolvedValueOnce(true);
      await switchingTransactionService.confirmTransaction(input);
      expect(setTransactionInfoTracker).toBeCalled();
    });

    it(`
      GIVEN 
      - valid params
      THEN
        ensure setTransactionUsageInfoTracker to be called 
    `, async () => {
      mockUpdateTransactionStatusSuccess();
      entitlementFlag.isEnabled = jest.fn().mockResolvedValueOnce(false);

      const mapTransactionInfoToTransactionUsageInfoContextFn = jest.spyOn(
        transactionUsageService,
        'mapTransactionInfoToTransactionUsageInfoContext'
      );
      mapTransactionInfoToTransactionUsageInfoContextFn.mockImplementationOnce(
        _ => {}
      );

      await switchingTransactionService.confirmTransaction(input);
      expect(mapTransactionInfoToTransactionUsageInfoContextFn).toBeCalledTimes(
        1
      );
    });
  });

  describe('extractChangeFromConfirmTransactionRequest', () => {
    const mockGetAdjustedPaymentRail = () => {
      (transactionHelper.getAdjustedPaymentRail as jest.Mock).mockReturnValueOnce(
        'MOCKED_RAIL'
      );
    };

    it('should get interchange from input', () => {
      mockGetAdjustedPaymentRail();
      const input: ConfirmTransactionRequest = {
        externalId: '0093384848',
        accountNo: '100023993999400000',
        transactionDate: '0212092500',
        responseMessage: 'OK',
        transactionResponseID: '3499488888',
        responseCode: ConfirmTransactionStatus.SUCCESS,
        interchange: BankNetworkEnum.ARTAJASA
      };

      const model = {
        ...getTransactionDetail(),
        fees: undefined,
        additionalInformation1: undefined,
        additionalInformation2: undefined,
        additionalInformation3: undefined,
        additionalInformation4: undefined,
        processingInfo: {
          paymentRail: 'EURONET'
        }
      };
      const result = extractChangeFromConfirmTransactionRequest(input, model);
      expect(result).toEqual({
        interchange: BankNetworkEnum.ARTAJASA,
        fees: [],
        processingInfo: {
          paymentRail: 'MOCKED_RAIL'
        }
      });
      expect(transactionHelper.getAdjustedPaymentRail).toBeCalledWith(
        model.processingInfo.paymentRail,
        input.interchange
      );
    });

    it('should get interchange from input along with fee of same interchange', () => {
      mockGetAdjustedPaymentRail();
      const input: ConfirmTransactionRequest = {
        externalId: '0093384848',
        accountNo: '100023993999400000',
        transactionDate: '0212092500',
        responseMessage: 'OK',
        transactionResponseID: '3499488888',
        responseCode: ConfirmTransactionStatus.SUCCESS,
        interchange: BankNetworkEnum.ALTO
      };

      const model = getTransactionForConfirmTranscation();

      const result = extractChangeFromConfirmTransactionRequest(input, model);

      expect(result).toEqual(getExpectedInterchangeData());
      expect(transactionHelper.getAdjustedPaymentRail).toBeCalledWith(
        model.processingInfo.paymentRail,
        input.interchange
      );
    });

    it('should only get additional information if model doesnot have them', () => {
      mockGetAdjustedPaymentRail();
      const input: ConfirmTransactionRequest = {
        externalId: '0093384848',
        accountNo: '100023993999400000',
        transactionDate: '200212092500',
        responseMessage: 'OK',
        transactionResponseID: '3499488888',
        responseCode: ConfirmTransactionStatus.SUCCESS,
        additionalInformation1: 'newAdditionalInformation1',
        additionalInformation2: 'newAdditionalInformation2',
        additionalInformation3: 'newAdditionalInformation3',
        additionalInformation4: 'newAdditionalInformation4'
      };

      const model = {
        ...getTransactionDetail(),
        fees: undefined,
        additionalInformation1: undefined,
        additionalInformation2: 'additionalInformation2',
        additionalInformation3: undefined,
        additionalInformation4: 'additionalInformation4',
        processingInfo: {
          paymentRail: 'IRIS'
        }
      };

      expect(extractChangeFromConfirmTransactionRequest(input, model)).toEqual({
        additionalInformation1: 'newAdditionalInformation1',
        additionalInformation2: 'additionalInformation2',
        additionalInformation3: 'newAdditionalInformation3',
        additionalInformation4: 'additionalInformation4',
        fees: [],
        processingInfo: {
          paymentRail: 'MOCKED_RAIL'
        }
      });
      expect(transactionHelper.getAdjustedPaymentRail).toBeCalledWith(
        model.processingInfo.paymentRail,
        input.interchange
      );
    });

    it('should throw if interchange not match any debitTc config', () => {
      const input: ConfirmTransactionRequest = {
        externalId: '0093384848',
        responseCode: ConfirmTransactionStatus.SUCCESS,
        interchange: null
      };

      const model: ITransactionModel = {
        ...getTransactionDetail(),
        debitTransactionCodeMapping: [
          {
            interchange: BankNetworkEnum.ALTO,
            code: 'debitCode',
            channel: 'debitChannel',
            minAmountPerTx: 0,
            maxAmountPerTx: 9999999
          }
        ]
      };

      try {
        extractChangeFromConfirmTransactionRequest(input, model);
      } catch (e) {
        expect(e.errorCode).toEqual(ERROR_CODE.DEBIT_INTERCHANGE_NOT_FOUND);
      }
    });

    it('should throw if interchange not match any creditTc config', () => {
      const input: ConfirmTransactionRequest = {
        externalId: '0093384848',
        responseCode: ConfirmTransactionStatus.SUCCESS,
        interchange: null
      };

      const model: ITransactionModel = {
        ...getTransactionDetail(),
        creditTransactionCodeMapping: [
          {
            interchange: BankNetworkEnum.ALTO,
            code: 'creditCode',
            channel: 'creditChannel',
            minAmountPerTx: 0,
            maxAmountPerTx: 9999999
          }
        ]
      };

      try {
        extractChangeFromConfirmTransactionRequest(input, model);
      } catch (e) {
        expect(e.errorCode).toEqual(ERROR_CODE.CREDIT_INTERCHANGE_NOT_FOUND);
      }
    });

    it('should throw if interchange not match any fee code config', () => {
      const input: ConfirmTransactionRequest = {
        externalId: '0093384848',
        responseCode: ConfirmTransactionStatus.SUCCESS,
        interchange: null
      };

      const model: ITransactionModel = {
        ...getTransactionDetail(),
        feeMapping: [
          {
            interchange: BankNetworkEnum.ALTO,
            ...getFeeAmountData()
          }
        ]
      };

      try {
        extractChangeFromConfirmTransactionRequest(input, model);
      } catch (e) {
        expect(e.errorCode).toEqual(ERROR_CODE.FEE_INTERCHANGE_NOT_FOUND);
      }
    });
  });

  describe('responseMessageToPaymentInstruction', () => {
    it('should send message to payment instruction in success case', () => {
      const input: IConfirmTransactionInput = {
        externalId: '0093384848',
        responseCode: ConfirmTransactionStatus.SUCCESS,
        interchange: null
      };
      const transaction: ITransactionModel = {
        ...getTransactionDetail(),
        paymentInstructionExecution: {
          paymentInstructionCode: 'PI_TRANSFER',
          topics: [PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE],
          executionType: TransactionExecutionTypeEnum.DIRECT_TRANSFER
        }
      };
      (transactionProducer.sendTransactionMessage as jest.Mock).mockResolvedValueOnce(
        {}
      );
      responseMessageToPaymentInstruction(input, transaction);
      const expectedMessage = {
        externalId: transaction.externalId,
        paymentRequestID: transaction.paymentRequestID,
        referenceId: transaction.referenceId,
        status: PaymentRequestStatus.SUCCEED,
        transactionStatus: TransactionStatus.SUCCEED,
        transactionAmount: transaction.transactionAmount,
        transactionId: transaction.id,
        extra: {
          additionalInformation1: transaction.additionalInformation1,
          additionalInformation2: transaction.additionalInformation2,
          additionalInformation3: transaction.additionalInformation3,
          additionalInformation4: transaction.additionalInformation4,
          sourceCIF: transaction.sourceCIF
        }
      };
      expect(transactionProducer.sendTransactionMessage).toBeCalledWith(
        transaction.paymentInstructionExecution.topics[0],
        expectedMessage,
        transaction.paymentInstructionExecution.executionType,
        transaction.paymentInstructionExecution.paymentInstructionCode
      );
    });

    it('should send message to payment instruction in success case with extra', () => {
      const input: IConfirmTransactionInput = {
        externalId: '0093384848',
        responseCode: ConfirmTransactionStatus.SUCCESS,
        interchange: null
      };
      const additionalInformation1 = 'execute-pi-from-partner';
      const transaction: ITransactionModel = {
        ...getTransactionDetail(),
        additionalInformation1,
        paymentInstructionExecution: {
          paymentInstructionCode: 'PI_TRANSFER',
          topics: [PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE],
          executionType: TransactionExecutionTypeEnum.DIRECT_TRANSFER
        }
      };
      (transactionProducer.sendTransactionMessage as jest.Mock).mockResolvedValueOnce(
        {}
      );
      responseMessageToPaymentInstruction(input, transaction);
      const expectedMessage = {
        externalId: transaction.externalId,
        paymentRequestID: transaction.paymentRequestID,
        referenceId: transaction.referenceId,
        status: PaymentRequestStatus.SUCCEED,
        transactionStatus: TransactionStatus.SUCCEED,
        transactionAmount: transaction.transactionAmount,
        transactionId: transaction.id,
        extra: {
          additionalInformation1: transaction.additionalInformation1,
          additionalInformation2: transaction.additionalInformation2,
          additionalInformation3: transaction.additionalInformation3,
          additionalInformation4: transaction.additionalInformation4,
          sourceCIF: transaction.sourceCIF
        }
      };
      expect(transactionProducer.sendTransactionMessage).toBeCalledWith(
        transaction.paymentInstructionExecution.topics[0],
        expectedMessage,
        transaction.paymentInstructionExecution.executionType,
        transaction.paymentInstructionExecution.paymentInstructionCode
      );
    });

    it('should send message to payment instruction in failed case', () => {
      const input: IConfirmTransactionInput = {
        externalId: '0093384848',
        responseCode: ConfirmTransactionStatus.ERROR_EXPECTED,
        interchange: null
      };
      const transaction: ITransactionModel = {
        ...getTransactionDetail(),
        paymentInstructionExecution: {
          paymentInstructionCode: 'PI_TRANSFER',
          topics: [PaymentInstructionTopicConstant.UPDATE_TRANSACTION_STATUS],
          executionType: TransactionExecutionTypeEnum.DIRECT_TRANSFER
        }
      };
      (transactionProducer.sendTransactionMessage as jest.Mock).mockResolvedValueOnce(
        {}
      );
      responseMessageToPaymentInstruction(input, transaction);
      const expectedMessage = {
        externalId: transaction.externalId,
        paymentRequestID: transaction.paymentRequestID,
        status: PaymentRequestStatus.DECLINED,
        transactionStatus: TransactionStatus.DECLINED,
        transactionAmount: transaction.transactionAmount,
        error: {
          errorCode: ERROR_CODE.TRANSACTION_FAILED
        },
        extra: {
          additionalInformation1: transaction.additionalInformation1,
          additionalInformation2: transaction.additionalInformation2,
          additionalInformation3: transaction.additionalInformation3,
          additionalInformation4: transaction.additionalInformation4,
          sourceCIF: transaction.sourceCIF
        }
      };
      expect(transactionProducer.sendTransactionMessage).toBeCalledWith(
        transaction.paymentInstructionExecution.topics[0],
        expectedMessage,
        transaction.paymentInstructionExecution.executionType,
        transaction.paymentInstructionExecution.paymentInstructionCode
      );
    });

    it('should send message to bill-payment in success case', () => {
      const input: IConfirmTransactionInput = {
        externalId: '0093384848',
        responseCode: ConfirmTransactionStatus.SUCCESS,
        interchange: null
      };
      const transaction: ITransactionModel = {
        ...getTransactionDetail(),
        paymentInstructionExecution: {
          paymentInstructionCode: PaymentInstructionCode.PI_BILLER,
          topics: [
            PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE,
            BillPaymentTopicConstant.BILL_PAYMENT_TRANSACTION_RESPONSE
          ],
          executionType: TransactionExecutionTypeEnum.DIRECT_TRANSFER
        },
        paymentServiceCode: 'DIGITAL_GOODS'
      };
      (transactionProducer.sendTransactionMessage as jest.Mock).mockResolvedValueOnce(
        {}
      );

      const expectedMessage = {
        externalId: transaction.externalId,
        paymentRequestID: transaction.paymentRequestID,
        referenceId: transaction.referenceId,
        status: PaymentRequestStatus.SUCCEED,
        transactionStatus: TransactionStatus.SUCCEED,
        transactionAmount: transaction.transactionAmount,
        transactionId: transaction.id,
        extra: {
          additionalInformation1: transaction.additionalInformation1,
          additionalInformation2: transaction.additionalInformation2,
          additionalInformation3: transaction.additionalInformation3,
          additionalInformation4: transaction.additionalInformation4,
          sourceCIF: transaction.sourceCIF
        }
      };

      responseMessageToPaymentInstruction(input, transaction);

      expect(transactionProducer.sendTransactionMessage).toBeCalledTimes(2);
      expect(transactionProducer.sendTransactionMessage).toBeCalledWith(
        BillPaymentTopicConstant.BILL_PAYMENT_TRANSACTION_RESPONSE,
        expectedMessage,
        transaction.paymentInstructionExecution.executionType,
        transaction.paymentInstructionExecution.paymentInstructionCode
      );
    });

    it('should send message to bill-payment in failed case', () => {
      const input: IConfirmTransactionInput = {
        externalId: '0093384848',
        responseCode: ConfirmTransactionStatus.ERROR_EXPECTED,
        interchange: null
      };
      const transaction: ITransactionModel = {
        ...getTransactionDetail(),
        paymentInstructionExecution: {
          paymentInstructionCode: PaymentInstructionCode.PI_BILLER,
          topics: [
            PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE,
            BillPaymentTopicConstant.BILL_PAYMENT_TRANSACTION_RESPONSE
          ],
          executionType: TransactionExecutionTypeEnum.DIRECT_TRANSFER
        },
        paymentServiceCode: 'DIGITAL_GOODS'
      };
      (transactionProducer.sendTransactionMessage as jest.Mock).mockResolvedValueOnce(
        {}
      );

      const expectedMessage = {
        externalId: transaction.externalId,
        paymentRequestID: transaction.paymentRequestID,
        status: PaymentRequestStatus.DECLINED,
        transactionStatus: TransactionStatus.DECLINED,
        transactionAmount: transaction.transactionAmount,
        error: {
          errorCode: ERROR_CODE.TRANSACTION_FAILED
        },
        extra: {
          additionalInformation1: transaction.additionalInformation1,
          additionalInformation2: transaction.additionalInformation2,
          additionalInformation3: transaction.additionalInformation3,
          additionalInformation4: transaction.additionalInformation4,
          sourceCIF: transaction.sourceCIF
        }
      };

      responseMessageToPaymentInstruction(input, transaction);

      expect(transactionProducer.sendTransactionMessage).toBeCalledTimes(2);
      expect(transactionProducer.sendTransactionMessage).toBeCalledWith(
        BillPaymentTopicConstant.BILL_PAYMENT_TRANSACTION_RESPONSE,
        expectedMessage,
        transaction.paymentInstructionExecution.executionType,
        transaction.paymentInstructionExecution.paymentInstructionCode
      );
    });
  });
});
