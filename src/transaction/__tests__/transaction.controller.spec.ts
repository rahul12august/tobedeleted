import hapi from '@hapi/hapi';
import {
  AuthenticationType,
  BankChannelEnum,
  BankNetworkEnum,
  Http,
  PaymentServiceTypeEnum,
  AdminPortalUserPermissionCode
} from '@dk/module-common';
import transactionController from '../transaction.controller';
import transactionService from '../transaction.service';
import {
  BaseExternalTransactionInput,
  ConfirmTransactionRequest,
  ConfirmTransactionResponse,
  ExternalTransactionInput,
  ITransferTransactionInput,
  RecommendationService,
  RecommendPaymentServiceInput,
  TopupExternalTransactionPayload,
  TopupInternalTransactionPayload,
  TransactionListRequest
} from '../transaction.type';
import {
  createReverseInput,
  getITransferTransactionInput,
  getRecommendPaymentServiceInput,
  getTransactionDetail,
  getTransactionModelData
} from '../__mocks__/transaction.data';
import { mockConfirmTransactionRequest } from '../__mocks__/transactionConfirmation.data';
import ResponseWrapper from '../../plugins/responseWrapper';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import transactionReversalService from '../transactionReversal.service';
import switchingTransactionService from '../transactionConfirmation.service';
import externalTransactionService from '../transactionExternal.service';
import transactionBlockingService from '../transactionBlocking.service';
import { getTransactionCodeInfoData } from '../../configuration/__mocks__/configuration.data';
import { getFeeAmountData } from '../../fee/__mocks__/fee.data';
import TokenAuth from '../../plugins/tokenAuth';
import jwt from 'hapi-auth-jwt2';
import { AUTH_STRATEGY, HttpHeaders } from '../../common/constant';
import { mockPublicKey } from '../../plugins/__mocks__/tokenAuth.data';
import { invalidHeader, validHeader } from '../__mocks__/headers.data';
import generalRefundService from '../refund/generalRefund.service';
import {
  ExecutionTypeEnum,
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from '../transaction.enum';
import txnManualAdjustmentService from '../transactionManualAdjustment.service';
import billPaymentService from '../../billPayment/billPayment.service';
import { getAuthPayloadFromAdminToken } from '../../common/authentication.util';
import faker from 'faker';
import entitlementService from '../../entitlement/entitlement.service';
import entitlementFlag from '../../common/entitlementFlag';
import { ExecutionMethodEnum } from '../transaction.constant';

import transactionUsageService from '../../transactionUsage/transactionUsage.service';

jest.mock('../../billPayment/billPayment.service');
jest.mock('../transaction.service');
jest.mock('../transactionManualAdjustment.service');
jest.mock('../transactionReversal.service');
jest.mock('../transactionConfirmation.service');
jest.mock('../transactionExternal.service');
jest.mock('../transactionBlocking.service');
jest.mock('../refund/generalRefund.service');
jest.mock('../../common/authentication.util');

jest.mock('../../transactionUsage/transactionUsage.service');

describe('transactionController', () => {
  let server: hapi.Server;
  beforeAll(async () => {
    TokenAuth.plugin.register = async (server: hapi.Server) => {
      server.register(jwt);
      server.auth.strategy(AUTH_STRATEGY, AUTH_STRATEGY, {
        complete: true,
        key: mockPublicKey,
        validate: () => {
          return { isValid: true };
        },
        verifyOptions: {
          algorithms: ['RS256']
        }
      });
      server.auth.default(AUTH_STRATEGY);
    };
    const plugins: any[] = [ResponseWrapper, TokenAuth];
    server = new hapi.Server();
    await server.register(plugins);
    server.route(transactionController);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('recommend payment service', () => {
    beforeEach(() => {
      (transactionService.recommendPaymentService as jest.Mock).mockResolvedValue(
        []
      );
    });
    it('should responds 201 with correct payload', async () => {
      // Given
      const transferInput: RecommendPaymentServiceInput = getRecommendPaymentServiceInput();
      const options = {
        method: 'POST',
        url: '/recommend-payment-services',
        payload: transferInput,
        headers: validHeader
      };
      const serviceRecommendations: RecommendationService[] = [
        {
          paymentServiceCode: 'p01',
          debitTransactionCode: [
            {
              transactionCode: 'dt01',
              transactionCodeInfo: {
                ...getTransactionCodeInfoData(),
                code: 'dt01'
              }
            }
          ],
          creditTransactionCode: [
            {
              transactionCode: 'ct01',
              transactionCodeInfo: {
                ...getTransactionCodeInfoData(),
                code: 'ct01'
              }
            }
          ],
          beneficiaryBankCode: undefined,
          feeData: [
            {
              ...getFeeAmountData(),
              feeAmount: 0,
              customerTc: undefined
            }
          ],
          authenticationType: AuthenticationType.NONE,
          beneficiaryBankCodeType: 'RTOL',
          transactionAuthenticationChecking: false,
          blocking: false,
          requireThirdPartyOutgoingId: false
        }
      ];
      const expected = serviceRecommendations.map(serviceRecommendation => {
        return {
          paymentServiceCode: serviceRecommendation.paymentServiceCode,
          debitTransactionCode:
            serviceRecommendation.debitTransactionCode[0].transactionCode,
          creditTransactionCode:
            serviceRecommendation.creditTransactionCode[0].transactionCode,
          realBeneficiaryBankCode: null,
          feeAmount: 0,
          customerTc: null,
          authenticationRequired: AuthenticationType.NONE
        };
      });

      (transactionService.recommendPaymentService as jest.Mock).mockResolvedValueOnce(
        serviceRecommendations
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(response.result.data).toEqual(expected);
      expect(transactionService.recommendPaymentService).toBeCalledWith(
        transferInput
      );
    });
    it('should responds 401 when invalid headers', async () => {
      // Given
      const options = {
        method: 'POST',
        url: '/recommend-payment-services',
        headers: invalidHeader
      };

      // When
      const response = await server.inject(options);

      // Then
      expect(response.statusCode).toBe(Http.StatusCode.UNAUTHORIZED);
    });
    it('should responds bad request with empty payload', async () => {
      // Given
      const options = {
        method: 'POST',
        url: '/recommend-payment-services',
        headers: validHeader
      };

      // When
      const response = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
    });
    it('should handle preferredBankChannel', async () => {
      // Given
      const transferInput: RecommendPaymentServiceInput = {
        ...getRecommendPaymentServiceInput(),
        preferredBankChannel: BankChannelEnum.EXTERNAL
      };
      const options = {
        method: 'POST',
        url: '/recommend-payment-services',
        payload: transferInput,
        headers: validHeader
      };
      const serviceRecommendations: RecommendationService[] = [
        {
          paymentServiceCode: 'p01',
          debitTransactionCode: [
            {
              transactionCode: 'dt01',
              transactionCodeInfo: {
                ...getTransactionCodeInfoData(),
                code: 'dt01'
              }
            }
          ],
          creditTransactionCode: [
            {
              transactionCode: 'ct01',
              transactionCodeInfo: {
                ...getTransactionCodeInfoData(),
                code: 'ct01'
              }
            }
          ],
          beneficiaryBankCode: undefined,
          feeData: [
            {
              ...getFeeAmountData(),
              feeAmount: 0,
              customerTc: undefined
            }
          ],
          authenticationType: AuthenticationType.NONE,
          beneficiaryBankCodeType: 'RTOL',
          transactionAuthenticationChecking: false,
          blocking: false,
          requireThirdPartyOutgoingId: false
        }
      ];
      const expected = serviceRecommendations.map(serviceRecommendation => {
        return {
          paymentServiceCode: serviceRecommendation.paymentServiceCode,
          debitTransactionCode:
            serviceRecommendation.debitTransactionCode[0].transactionCode,
          creditTransactionCode:
            serviceRecommendation.creditTransactionCode[0].transactionCode,
          realBeneficiaryBankCode: null,
          feeAmount: 0,
          customerTc: null,
          authenticationRequired: AuthenticationType.NONE
        };
      });

      (transactionService.recommendPaymentService as jest.Mock).mockResolvedValueOnce(
        serviceRecommendations
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(response.result.data).toEqual(expected);
      expect(transactionService.recommendPaymentService).toBeCalledWith(
        transferInput
      );
    });

    const mandatoryFields = ['transactionAmount', 'paymentServiceType'];
    it.each(mandatoryFields)(
      `should responds bad request with payload missing mandatory field %s`,
      async field => {
        // Given
        const payload = { ...getRecommendPaymentServiceInput() };
        delete payload[field];
        const options = {
          method: 'POST',
          url: '/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
      }
    );
    it.each(mandatoryFields)(
      `should responds bad request with payload have mandatory field is null %s`,
      async field => {
        // Given
        const payload = { ...getRecommendPaymentServiceInput() };
        payload[field] = null;
        const options = {
          method: 'POST',
          url: '/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
      }
    );
    it.each(mandatoryFields)(
      `should responds bad request with payload have mandatory field is empty string %s`,
      async field => {
        // Given
        const payload = { ...getRecommendPaymentServiceInput() };
        payload[field] = '';
        const options = {
          method: 'POST',
          url: '/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
      }
    );

    const optionalFields = ['beneficiaryCIF'];

    it.each(optionalFields)(
      `should respond success if optional field %s is missing`,
      async field => {
        // Given
        const payload = {
          ...getRecommendPaymentServiceInput(),
          beneficiaryBankCode: 'BC001'
        };
        delete payload[field];
        const options = {
          method: 'POST',
          url: '/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.OK);
      }
    );

    it.each(optionalFields)(
      `should responds success with payload have optional field is null %s`,
      async field => {
        // Given
        const payload = {
          ...getRecommendPaymentServiceInput(),
          beneficiaryBankCode: 'BC001'
        };
        payload[field] = null;
        const options = {
          method: 'POST',
          url: '/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.OK);
      }
    );

    it.each(optionalFields)(
      `should responds success with payload have optional field is empty string %s`,
      async field => {
        // Given
        const payload = {
          ...getRecommendPaymentServiceInput(),
          beneficiaryBankCode: 'BC001'
        };
        payload[field] = '';
        const options = {
          method: 'POST',
          url: '/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.OK);
      }
    );
  });

  describe('new recommend payment service', () => {
    beforeEach(() => {
      (transactionService.recommendPaymentService as jest.Mock).mockResolvedValue(
        []
      );
    });
    it('should responds 201 with correct payload', async () => {
      // Given
      const transferInput: RecommendPaymentServiceInput = getRecommendPaymentServiceInput();
      const options = {
        method: 'POST',
        url: '/admin/recommend-payment-services',
        payload: transferInput,
        headers: validHeader
      };
      const serviceRecommendations: RecommendationService[] = [
        {
          paymentServiceCode: 'p01',
          debitTransactionCode: [
            {
              transactionCode: 'dt01',
              transactionCodeInfo: {
                ...getTransactionCodeInfoData(),
                code: 'dt01'
              }
            }
          ],
          creditTransactionCode: [
            {
              transactionCode: 'ct01',
              transactionCodeInfo: {
                ...getTransactionCodeInfoData(),
                code: 'ct01'
              }
            }
          ],
          beneficiaryBankCode: undefined,
          feeData: [
            {
              ...getFeeAmountData(),
              feeAmount: 0,
              customerTc: undefined
            }
          ],
          authenticationType: AuthenticationType.NONE,
          beneficiaryBankCodeType: 'RTOL',
          transactionAuthenticationChecking: false,
          blocking: false,
          requireThirdPartyOutgoingId: false
        }
      ];
      const expected = serviceRecommendations.map(serviceRecommendation => {
        return {
          paymentServiceCode: serviceRecommendation.paymentServiceCode,
          debitTransactionCode:
            serviceRecommendation.debitTransactionCode[0].transactionCode,
          creditTransactionCode:
            serviceRecommendation.creditTransactionCode[0].transactionCode,
          realBeneficiaryBankCode: null,
          feeAmount: 0,
          customerTc: null,
          authenticationRequired: AuthenticationType.NONE
        };
      });

      (transactionService.recommendPaymentService as jest.Mock).mockResolvedValueOnce(
        serviceRecommendations
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(response.result.data).toEqual(expected);
      expect(transactionService.recommendPaymentService).toBeCalledWith(
        transferInput
      );
    });
    it('should responds 401 when invalid headers', async () => {
      // Given
      const options = {
        method: 'POST',
        url: '/admin/recommend-payment-services',
        headers: invalidHeader
      };

      // When
      const response = await server.inject(options);

      // Then
      expect(response.statusCode).toBe(Http.StatusCode.UNAUTHORIZED);
    });
    it('should responds bad request with empty payload', async () => {
      // Given
      const options = {
        method: 'POST',
        url: '/admin/recommend-payment-services',
        headers: validHeader
      };

      // When
      const response = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
    });

    const mandatoryFields = ['transactionAmount', 'paymentServiceType'];
    it.each(mandatoryFields)(
      `should responds bad request with payload missing mandatory field %s`,
      async field => {
        // Given
        const payload = { ...getRecommendPaymentServiceInput() };
        delete payload[field];
        const options = {
          method: 'POST',
          url: '/admin/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
      }
    );
    it.each(mandatoryFields)(
      `should responds bad request with payload have mandatory field is null %s`,
      async field => {
        // Given
        const payload = { ...getRecommendPaymentServiceInput() };
        payload[field] = null;
        const options = {
          method: 'POST',
          url: '/admin/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
      }
    );
    it.each(mandatoryFields)(
      `should responds bad request with payload have mandatory field is empty string %s`,
      async field => {
        // Given
        const payload = { ...getRecommendPaymentServiceInput() };
        payload[field] = '';
        const options = {
          method: 'POST',
          url: '/admin/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
      }
    );

    const optionalFields = ['beneficiaryCIF'];

    it.each(optionalFields)(
      `should respond success if optional field %s is missing`,
      async field => {
        // Given
        const payload = {
          ...getRecommendPaymentServiceInput(),
          beneficiaryBankCode: 'BC001'
        };
        delete payload[field];
        const options = {
          method: 'POST',
          url: '/admin/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.OK);
      }
    );

    it.each(optionalFields)(
      `should responds success with payload have optional field is null %s`,
      async field => {
        // Given
        const payload = {
          ...getRecommendPaymentServiceInput(),
          beneficiaryBankCode: 'BC001'
        };
        payload[field] = null;
        const options = {
          method: 'POST',
          url: '/admin/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.OK);
      }
    );

    it.each(optionalFields)(
      `should responds success with payload have optional field is empty string %s`,
      async field => {
        // Given
        const payload = {
          ...getRecommendPaymentServiceInput(),
          beneficiaryBankCode: 'BC001'
        };
        payload[field] = '';
        const options = {
          method: 'POST',
          url: '/admin/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.OK);
      }
    );
  });

  describe('private recommend payment service', () => {
    beforeEach(() => {
      (transactionService.recommendPaymentService as jest.Mock).mockResolvedValue(
        []
      );
    });
    it('should responds 201 with correct payload', async () => {
      // Given
      const transferInput: RecommendPaymentServiceInput = getRecommendPaymentServiceInput();
      const options = {
        method: 'POST',
        url: '/private/recommend-payment-services',
        payload: transferInput,
        headers: validHeader
      };
      const serviceRecommendations: RecommendationService[] = [
        {
          paymentServiceCode: 'p01',
          debitTransactionCode: [
            {
              transactionCode: 'dt01',
              transactionCodeInfo: {
                ...getTransactionCodeInfoData(),
                code: 'dt01'
              }
            }
          ],
          creditTransactionCode: [
            {
              transactionCode: 'ct01',
              transactionCodeInfo: {
                ...getTransactionCodeInfoData(),
                code: 'ct01'
              }
            }
          ],
          beneficiaryBankCode: undefined,
          feeData: [
            {
              ...getFeeAmountData(),
              feeAmount: 0,
              customerTc: undefined
            }
          ],
          authenticationType: AuthenticationType.NONE,
          beneficiaryBankCodeType: 'RTOL',
          transactionAuthenticationChecking: false,
          blocking: false,
          requireThirdPartyOutgoingId: false
        }
      ];
      const expected = serviceRecommendations.map(serviceRecommendation => {
        return {
          paymentServiceCode: serviceRecommendation.paymentServiceCode,
          debitTransactionCode:
            serviceRecommendation.debitTransactionCode[0].transactionCode,
          creditTransactionCode:
            serviceRecommendation.creditTransactionCode[0].transactionCode,
          realBeneficiaryBankCode: null,
          feeAmount: 0,
          customerTc: null,
          authenticationRequired: AuthenticationType.NONE
        };
      });

      (transactionService.recommendPaymentService as jest.Mock).mockResolvedValueOnce(
        serviceRecommendations
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(response.result.data).toEqual(expected);
      expect(transactionService.recommendPaymentService).toBeCalledWith(
        transferInput
      );
    });

    it('should responds bad request with empty payload', async () => {
      // Given
      const options = {
        method: 'POST',
        url: '/private/recommend-payment-services',
        headers: validHeader
      };

      // When
      const response = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
    });

    const mandatoryFields = ['transactionAmount', 'paymentServiceType'];
    it.each(mandatoryFields)(
      `should responds bad request with payload missing mandatory field %s`,
      async field => {
        // Given
        const payload = { ...getRecommendPaymentServiceInput() };
        delete payload[field];
        const options = {
          method: 'POST',
          url: '/private/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
      }
    );
    it.each(mandatoryFields)(
      `should responds bad request with payload have mandatory field is null %s`,
      async field => {
        // Given
        const payload = { ...getRecommendPaymentServiceInput() };
        payload[field] = null;
        const options = {
          method: 'POST',
          url: '/private/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
      }
    );
    it.each(mandatoryFields)(
      `should responds bad request with payload have mandatory field is empty string %s`,
      async field => {
        // Given
        const payload = { ...getRecommendPaymentServiceInput() };
        payload[field] = '';
        const options = {
          method: 'POST',
          url: '/private/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
      }
    );

    const optionalFields = ['beneficiaryCIF'];

    it.each(optionalFields)(
      `should respond success if optional field %s is missing`,
      async field => {
        // Given
        const payload = {
          ...getRecommendPaymentServiceInput(),
          beneficiaryBankCode: 'BC001'
        };
        delete payload[field];
        const options = {
          method: 'POST',
          url: '/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.OK);
      }
    );

    it.each(optionalFields)(
      `should responds success with payload have optional field is null %s`,
      async field => {
        // Given
        const payload = {
          ...getRecommendPaymentServiceInput(),
          beneficiaryBankCode: 'BC001'
        };
        payload[field] = null;
        const options = {
          method: 'POST',
          url: '/private/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.OK);
      }
    );

    it.each(optionalFields)(
      `should responds success with payload have optional field is empty string %s`,
      async field => {
        // Given
        const payload = {
          ...getRecommendPaymentServiceInput(),
          beneficiaryBankCode: 'BC001'
        };
        payload[field] = '';
        const options = {
          method: 'POST',
          url: '/private/recommend-payment-services',
          payload,
          headers: validHeader
        };

        // When
        const response = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.OK);
      }
    );
  });

  describe('create transfer transaction', () => {
    const expectedTransactionResponse = {
      id: '5da02ad923d9f74ced0087d1',
      transactionIds: ['1', '2']
    };
    it('should responds 201 with correct payload', async () => {
      // Given
      const transactionInput: ITransferTransactionInput = {
        ...getITransferTransactionInput(),
        sourceCustomerId: undefined,
        interchange: BankNetworkEnum.AMAAN
      };
      const options = {
        method: 'POST',
        url: '/transfer-transactions',
        payload: { ...transactionInput }
      };

      (transactionService.createTransferTransaction as jest.Mock).mockResolvedValue(
        {
          ...expectedTransactionResponse,
          coreBankingTransactionIds: expectedTransactionResponse.transactionIds
        }
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data).toEqual(expectedTransactionResponse);
      expect(transactionService.createTransferTransaction).toBeCalledWith({
        ...transactionInput
      });
    });

    it('should responds 201 with correct transaction status', async () => {
      // Given
      const transactionInput: ITransferTransactionInput = {
        ...getITransferTransactionInput(),
        sourceCustomerId: undefined,
        interchange: BankNetworkEnum.AMAAN
      };
      const options = {
        method: 'POST',
        url: '/transfer-transactions',
        payload: {
          ...transactionInput
        }
      };

      (transactionService.createTransferTransaction as jest.Mock).mockResolvedValue(
        {
          ...expectedTransactionResponse,
          coreBankingTransactionIds: expectedTransactionResponse.transactionIds,
          executionType: ExecutionTypeEnum.NONE_BLOCKING,
          status: TransactionStatus.SUCCEED
        }
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(transactionService.createTransferTransaction).toBeCalledWith({
        ...transactionInput
      });
      expect(response.result.data.transactionStatus).toEqual(
        TransactionStatus.SUCCEED
      );
    });

    it('should convert empty string or null additional information to undefined', async () => {
      // Given
      const transactionInput: ITransferTransactionInput = {
        ...getITransferTransactionInput()
      };
      delete transactionInput.additionalInformation1;
      delete transactionInput.additionalInformation2;
      const options = {
        method: 'POST',
        url: `/transfer-transactions?customerId=${transactionInput.sourceCustomerId}`,
        payload: {
          ...transactionInput,
          sourceCustomerId: undefined,
          additionalInformation1: '',
          additionalInformation2: null,
          additionalInformation3: 'additionalInformation3'
        }
      };

      (transactionService.createTransferTransaction as jest.Mock).mockResolvedValue(
        {
          ...expectedTransactionResponse,
          coreBankingTransactionIds: expectedTransactionResponse.transactionIds
        }
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data).toEqual({
        id: expectedTransactionResponse.id,
        transactionIds: expectedTransactionResponse.transactionIds
      });
      expect(transactionService.createTransferTransaction).toBeCalledWith({
        ...transactionInput,
        additionalInformation3: 'additionalInformation3'
      });
    });

    it('should responds 201 without sourceCif sourceAccountNo', async () => {
      // Given
      const transactionInput: ITransferTransactionInput = {
        ...getITransferTransactionInput(),
        sourceCustomerId: undefined,
        sourceAccountNo: undefined,
        sourceCIF: undefined
      };
      const options = {
        method: 'POST',
        url: '/transfer-transactions',
        payload: { ...transactionInput }
      };

      (transactionService.createTransferTransaction as jest.Mock).mockResolvedValue(
        {
          ...expectedTransactionResponse,
          coreBankingTransactionIds: expectedTransactionResponse.transactionIds
        }
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data).toEqual(expectedTransactionResponse);
      expect(transactionService.createTransferTransaction).toBeCalledWith({
        ...transactionInput
      });
    });

    it('should responds 201 without beneficiaryCIF beneficiaryAccountNo', async () => {
      // Given
      const transactionInput: ITransferTransactionInput = {
        ...getITransferTransactionInput(),
        sourceCustomerId: undefined,
        beneficiaryAccountNo: undefined,
        beneficiaryCIF: undefined
      };
      const options = {
        method: 'POST',
        url: '/transfer-transactions',
        payload: { ...transactionInput }
      };

      (transactionService.createTransferTransaction as jest.Mock).mockResolvedValue(
        {
          ...expectedTransactionResponse,
          coreBankingTransactionIds: expectedTransactionResponse.transactionIds
        }
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data).toEqual(expectedTransactionResponse);
      expect(transactionService.createTransferTransaction).toBeCalledWith({
        ...transactionInput
      });
    });

    describe('Private create transfer transaction', () => {
      it('should responds 201 with correct payload', async () => {
        // Given
        const transactionInput: ITransferTransactionInput = {
          ...getITransferTransactionInput(),
          sourceCustomerId: undefined,
          interchange: BankNetworkEnum.AMAAN
        };
        const options = {
          method: 'POST',
          url: '/private/transfer-transactions',
          payload: { ...transactionInput }
        };

        (transactionService.createTransferTransaction as jest.Mock).mockResolvedValue(
          {
            ...expectedTransactionResponse,
            coreBankingTransactionIds:
              expectedTransactionResponse.transactionIds
          }
        );

        // When
        const response: any = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
        expect(response.result.data).toEqual(expectedTransactionResponse);
        expect(transactionService.createTransferTransaction).toBeCalledWith({
          ...transactionInput
        });
      });

      it('should convert empty string or null additional information to undefined', async () => {
        // Given
        const transactionInput: ITransferTransactionInput = {
          ...getITransferTransactionInput()
        };
        delete transactionInput.additionalInformation1;
        delete transactionInput.additionalInformation2;
        const options = {
          method: 'POST',
          url: `/private/transfer-transactions?customerId=${transactionInput.sourceCustomerId}`,
          payload: {
            ...transactionInput,
            sourceCustomerId: undefined,
            additionalInformation1: '',
            additionalInformation2: null,
            additionalInformation3: 'additionalInformation3'
          }
        };

        (transactionService.createTransferTransaction as jest.Mock).mockResolvedValue(
          {
            ...expectedTransactionResponse,
            coreBankingTransactionIds:
              expectedTransactionResponse.transactionIds
          }
        );

        // When
        const response: any = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
        expect(response.result.data).toEqual({
          id: expectedTransactionResponse.id,
          transactionIds: expectedTransactionResponse.transactionIds
        });
        expect(transactionService.createTransferTransaction).toBeCalledWith({
          ...transactionInput,
          additionalInformation3: 'additionalInformation3'
        });
      });

      it('should responds 201 without sourceCif sourceAccountNo', async () => {
        // Given
        const transactionInput: ITransferTransactionInput = {
          ...getITransferTransactionInput(),
          sourceCustomerId: undefined,
          sourceAccountNo: undefined,
          sourceCIF: undefined
        };
        const options = {
          method: 'POST',
          url: '/private/transfer-transactions',
          payload: { ...transactionInput }
        };

        (transactionService.createTransferTransaction as jest.Mock).mockResolvedValue(
          {
            ...expectedTransactionResponse,
            coreBankingTransactionIds:
              expectedTransactionResponse.transactionIds
          }
        );

        // When
        const response: any = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
        expect(response.result.data).toEqual(expectedTransactionResponse);
        expect(transactionService.createTransferTransaction).toBeCalledWith({
          ...transactionInput
        });
      });
    });
  });

  describe('create deposit transaction', () => {
    const expectedTransactionResponse = {
      id: '5da02ad923d9f74ced0087d1',
      referenceId: '5da02ad923d9f74ced0099h1',
      transactionIds: ['1', '2']
    };
    it('should responds 201 with correct payload', async () => {
      // Given
      const transactionInput: ITransferTransactionInput = {
        ...getITransferTransactionInput(),
        sourceCustomerId: undefined
      };
      const payload = { ...transactionInput, paymentServiceType: 'CASHBACK' };
      const options = {
        method: 'POST',
        url: '/deposit-transactions?customerId=customerId',
        payload
      };

      (transactionService.createDepositTransaction as jest.Mock).mockResolvedValueOnce(
        {
          ...expectedTransactionResponse,
          coreBankingTransactionIds: expectedTransactionResponse.transactionIds
        }
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data).toEqual(expectedTransactionResponse);
      expect(transactionService.createDepositTransaction).toBeCalledWith({
        ...payload,
        sourceCustomerId: 'customerId'
      });
    });

    describe('private', () => {
      it('should responds 201 with correct payload', async () => {
        // Given
        const transactionInput: ITransferTransactionInput = {
          ...getITransferTransactionInput(),
          sourceCustomerId: undefined
        };
        const payload = { ...transactionInput, paymentServiceType: 'CASHBACK' };
        const options = {
          method: 'POST',
          url: '/private/deposit-transactions?customerId=customerId',
          payload
        };

        (transactionService.createDepositTransaction as jest.Mock).mockResolvedValueOnce(
          {
            ...expectedTransactionResponse,
            coreBankingTransactionIds:
              expectedTransactionResponse.transactionIds
          }
        );

        // When
        const response: any = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
        expect(response.result.data).toEqual(expectedTransactionResponse);
        expect(transactionService.createDepositTransaction).toBeCalledWith({
          ...payload,
          sourceCustomerId: 'customerId'
        });
      });
    });
  });

  describe('create withdraw transaction', () => {
    const expectedTransactionResponse = {
      id: '5da02ad923d9f74ced0087d1',
      referenceId: '5da02ad923d9f74ced0099h1',
      transactionIds: ['1', '2']
    };
    it('should responds 201 with correct payload', async () => {
      // Given
      const transactionInput: ITransferTransactionInput = {
        ...getITransferTransactionInput(),
        sourceCustomerId: undefined
      };
      const payload = { ...transactionInput, paymentServiceType: 'MDR' };
      const options = {
        method: 'POST',
        url: '/withdraw-transactions?customerId=customerId',
        payload
      };

      (transactionService.createWithdrawTransaction as jest.Mock).mockResolvedValueOnce(
        {
          ...expectedTransactionResponse,
          coreBankingTransactionIds: expectedTransactionResponse.transactionIds
        }
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data).toEqual(expectedTransactionResponse);
      expect(transactionService.createWithdrawTransaction).toBeCalledWith({
        ...payload,
        sourceCustomerId: 'customerId'
      });
    });

    describe('private', () => {
      it('should responds 201 with correct payload', async () => {
        // Given
        const transactionInput: ITransferTransactionInput = {
          ...getITransferTransactionInput(),
          sourceCustomerId: undefined
        };
        const payload = { ...transactionInput, paymentServiceType: 'MDR' };
        const options = {
          method: 'POST',
          url: '/private/withdraw-transactions?customerId=customerId',
          payload
        };

        (transactionService.createWithdrawTransaction as jest.Mock).mockResolvedValueOnce(
          {
            ...expectedTransactionResponse,
            coreBankingTransactionIds:
              expectedTransactionResponse.transactionIds
          }
        );

        // When
        const response: any = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
        expect(response.result.data).toEqual(expectedTransactionResponse);
        expect(transactionService.createWithdrawTransaction).toBeCalledWith({
          ...payload,
          sourceCustomerId: 'customerId'
        });
      });
    });
  });

  describe('revert transaction', () => {
    it('should responds 201 with correct payload', async () => {
      const transactionId = '1222';
      const inputRequest = createReverseInput();
      // Given
      const options = {
        method: 'POST',
        url: `/transactions/${transactionId}/revert`,
        payload: inputRequest
      };
      (transactionReversalService.reverseTransaction as jest.Mock).mockResolvedValueOnce(
        undefined
      );

      // When
      const response: any = await server.inject(options);
      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.NO_CONTENT);
      expect(transactionReversalService.reverseTransaction).toBeCalledWith(
        transactionId,
        inputRequest
      );
    });

    it('should throw error if call coreBanking fail', async () => {
      const transactionId = '1222';
      const inputRequest = createReverseInput();
      // Given
      const options = {
        method: 'POST',
        url: `/transactions/${transactionId}/revert`,
        payload: inputRequest
      };
      (transactionReversalService.reverseTransaction as jest.Mock).mockRejectedValueOnce(
        {
          error: {
            message: 'Can not revert transaction',
            code: 'CAN_NOT_REVERSE_TRANSACTION'
          }
        }
      );
      // When
      const response: any = await server.inject(options);
      // Then
      expect(response.statusCode).toEqual(
        Http.StatusCode.INTERNAL_SERVER_ERROR
      );
      expect(transactionReversalService.reverseTransaction).toBeCalledWith(
        transactionId,
        inputRequest
      );
    });

    describe('private transaction revert', () => {
      it('should responds 201 with correct payload', async () => {
        const transactionId = '1222';
        const inputRequest = createReverseInput();
        // Given
        const options = {
          method: 'POST',
          url: `/private/transactions/${transactionId}/revert`,
          payload: inputRequest
        };
        (transactionReversalService.reverseTransaction as jest.Mock).mockResolvedValueOnce(
          undefined
        );

        // When
        const response: any = await server.inject(options);
        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.NO_CONTENT);
        expect(transactionReversalService.reverseTransaction).toBeCalledWith(
          transactionId,
          inputRequest
        );
      });

      it('should throw error if call coreBanking fail', async () => {
        const transactionId = '1222';
        const inputRequest = createReverseInput();
        // Given
        const options = {
          method: 'POST',
          url: `/private/transactions/${transactionId}/revert`,
          payload: inputRequest
        };
        (transactionReversalService.reverseTransaction as jest.Mock).mockRejectedValueOnce(
          {
            error: {
              message: 'Can not revert transaction',
              code: 'CAN_NOT_REVERSE_TRANSACTION'
            }
          }
        );
        // When
        const response: any = await server.inject(options);
        // Then
        expect(response.statusCode).toEqual(
          Http.StatusCode.INTERNAL_SERVER_ERROR
        );
        expect(transactionReversalService.reverseTransaction).toBeCalledWith(
          transactionId,
          inputRequest
        );
      });
    });
  });

  describe('get transaction', () => {
    it('should responds 200 with correct payload', async () => {
      const transactionId = '1222';
      (transactionService.getTransaction as jest.Mock).mockResolvedValueOnce(
        getTransactionDetail()
      );

      // Given
      const options = {
        method: 'GET',
        url: `/transactions/${transactionId}`
      };
      // When
      const response: hapi.ServerInjectResponse = await server.inject(options);
      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(transactionService.getTransaction).toBeCalledWith(transactionId);
    });
    it('should return 404 if transaction not found', async () => {
      // Given
      (transactionService.getTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.TRANSACTION_NOT_FOUND
        )
      );
      const transactionId = '1222';
      // When
      const options = {
        method: 'GET',
        url: `/transactions/${transactionId}`
      };
      const response: hapi.ServerInjectResponse = await server.inject(options);
      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.NOT_FOUND);
      expect(transactionService.getTransaction).toBeCalledWith(transactionId);
    });

    describe('private get transaction', () => {
      it('should responds 200 with correct payload', async () => {
        const transactionId = '1222';
        (transactionService.getTransaction as jest.Mock).mockResolvedValueOnce(
          getTransactionDetail()
        );

        // Given
        const options = {
          method: 'GET',
          url: `/private/transactions/${transactionId}`
        };
        // When
        const response: hapi.ServerInjectResponse = await server.inject(
          options
        );
        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.OK);
        expect(transactionService.getTransaction).toBeCalledWith(transactionId);
      });
      it('should return 404 if transaction not found', async () => {
        // Given
        (transactionService.getTransaction as jest.Mock).mockRejectedValueOnce(
          new TransferAppError(
            'Test error!',
            TransferFailureReasonActor.UNKNOWN,
            ERROR_CODE.TRANSACTION_NOT_FOUND
          )
        );
        const transactionId = '1222';
        // When
        const options = {
          method: 'GET',
          url: `/private/transactions/${transactionId}`
        };
        const response: hapi.ServerInjectResponse = await server.inject(
          options
        );
        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.NOT_FOUND);
        expect(transactionService.getTransaction).toBeCalledWith(transactionId);
      });
    });
  });

  describe('get transaction list', () => {
    it('should responds 200 with correct payload', async () => {
      const transactionListInput: TransactionListRequest = {
        transactionId: '1222'
      };

      (transactionService.getTransactionList as jest.Mock).mockResolvedValueOnce(
        {
          transactions: {
            list: [getTransactionDetail()],
            page: 1,
            limit: 20,
            totalPage: 10
          },
          refundTransactions: []
        }
      );

      (getAuthPayloadFromAdminToken as jest.Mock).mockReturnValueOnce({
        roles: [AdminPortalUserPermissionCode.CUSTOMER_INFO_WRITE]
      });

      // Given
      const options = {
        method: 'POST',
        url: `/admin/transactions`,
        payload: {
          ...transactionListInput
        },
        headers: validHeader
      };
      // When
      const response: hapi.ServerInjectResponse = await server.inject(options);
      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(transactionService.getTransactionList).toBeCalledWith(
        transactionListInput,
        false
      );
    });
    it('should responds 200 when fetchRefund is passed', async () => {
      (transactionService.getTransactionList as jest.Mock).mockResolvedValueOnce(
        {
          transactions: {
            list: [getTransactionDetail()],
            page: 1,
            limit: 20,
            totalPage: 10
          },
          refundTransactions: []
        }
      );

      (getAuthPayloadFromAdminToken as jest.Mock).mockReturnValueOnce({
        roles: [AdminPortalUserPermissionCode.CUSTOMER_INFO_WRITE]
      });

      const transactionListInput: TransactionListRequest = {
        customerId: '0234728915'
      };

      // Given
      const options = {
        method: 'POST',
        url: `/admin/transactions?fetchRefund=true`,
        payload: {
          ...transactionListInput
        },
        headers: validHeader
      };

      // When
      const response: hapi.ServerInjectResponse = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(transactionService.getTransactionList).toBeCalledWith(
        transactionListInput,
        true
      );
    });
    it('should return 200 if transaction not found for particular selection', async () => {
      // Given
      const transactionListInput: TransactionListRequest = {
        transactionId: '1222'
      };
      (transactionService.getTransactionList as jest.Mock).mockResolvedValueOnce(
        {
          transactions: { list: null, page: 1, limit: 20, totalPage: 0 },
          refundTransactions: []
        }
      );
      (getAuthPayloadFromAdminToken as jest.Mock).mockReturnValueOnce({
        roles: [AdminPortalUserPermissionCode.CUSTOMER_INFO_WRITE]
      });
      // When
      const options = {
        method: 'POST',
        url: `/admin/transactions`,
        payload: {
          ...transactionListInput
        },
        headers: validHeader
      };
      const response: hapi.ServerInjectResponse = await server.inject(options);
      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(transactionService.getTransactionList).toBeCalledWith(
        transactionListInput,
        false
      );
    });

    it('should return 404 if transaction not found', async () => {
      // Given
      const transactionListInput: TransactionListRequest = {
        transactionId: '1222'
      };
      (transactionService.getTransactionList as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.TRANSACTION_NOT_FOUND
        )
      );
      (getAuthPayloadFromAdminToken as jest.Mock).mockReturnValueOnce({
        roles: [AdminPortalUserPermissionCode.CUSTOMER_INFO_WRITE]
      });
      // When
      const options = {
        method: 'POST',
        url: `/admin/transactions`,
        payload: {
          ...transactionListInput
        },
        headers: validHeader
      };
      const response: hapi.ServerInjectResponse = await server.inject(options);
      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.NOT_FOUND);
      expect(transactionService.getTransactionList).toBeCalledWith(
        transactionListInput,
        false
      );
    });
  });

  describe('createBlockingTransferTransaction', () => {
    const expectedBlockingTransactionResponse = {
      id: '5da02ad923d9f74ced0087d1'
    };
    it('should responds 201 with correct payload', async () => {
      // Given
      const transactionInput: ITransferTransactionInput = {
        ...getITransferTransactionInput(),
        sourceCustomerId: undefined
      };
      const options = {
        method: 'POST',
        url: '/blocking-transfer-transactions?customerId=customerId',
        payload: {
          ...transactionInput
        }
      };

      (transactionBlockingService.createBlockingTransferTransaction as jest.Mock).mockResolvedValueOnce(
        expectedBlockingTransactionResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data).toEqual(expectedBlockingTransactionResponse);
      expect(
        transactionBlockingService.createBlockingTransferTransaction
      ).toBeCalledWith({
        ...transactionInput,
        sourceCustomerId: 'customerId'
      });
    });

    describe('private', () => {
      it('should responds 201 with correct payload', async () => {
        // Given
        const transactionInput: ITransferTransactionInput = {
          ...getITransferTransactionInput(),
          sourceCustomerId: undefined
        };
        const options = {
          method: 'POST',
          url: '/private/blocking-transfer-transactions?customerId=customerId',
          payload: {
            ...transactionInput
          }
        };

        (transactionBlockingService.createBlockingTransferTransaction as jest.Mock).mockResolvedValueOnce(
          expectedBlockingTransactionResponse
        );

        // When
        const response: any = await server.inject(options);

        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
        expect(response.result.data).toEqual(
          expectedBlockingTransactionResponse
        );
        expect(
          transactionBlockingService.createBlockingTransferTransaction
        ).toBeCalledWith({
          ...transactionInput,
          sourceCustomerId: 'customerId'
        });
      });
    });
  });

  describe('adjust blocking transaction', () => {
    it('should responds 204 with correct payload', async () => {
      const cif = 'cif123';
      const transactionId = '1222';
      const amount = 1000;
      (transactionBlockingService.adjustBlockingTransferTransaction as jest.Mock).mockResolvedValueOnce(
        undefined
      );

      // Given
      const options = {
        method: 'POST',
        url: `/blocking-transfer-transactions/${transactionId}/adjust?cif=${cif}`,
        payload: {
          amount
        }
      };
      // When
      const response: hapi.ServerInjectResponse = await server.inject(options);
      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.NO_CONTENT);
      expect(
        transactionBlockingService.adjustBlockingTransferTransaction
      ).toBeCalledWith(cif, transactionId, amount);
    });

    describe('private', () => {
      it('should responds 204 with correct payload', async () => {
        const cif = 'cif123';
        const transactionId = '1222';
        const amount = 1000;
        (transactionBlockingService.adjustBlockingTransferTransaction as jest.Mock).mockResolvedValueOnce(
          undefined
        );

        // Given
        const options = {
          method: 'POST',
          url: `/private/blocking-transfer-transactions/${transactionId}/adjust?cif=${cif}`,
          payload: {
            amount
          }
        };
        // When
        const response: hapi.ServerInjectResponse = await server.inject(
          options
        );
        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.NO_CONTENT);
        expect(
          transactionBlockingService.adjustBlockingTransferTransaction
        ).toBeCalledWith(cif, transactionId, amount);
      });
    });
  });

  describe('cancel blocking transaction', () => {
    it('should responds 204 with correct payload', async () => {
      const cif = 'cif123';
      const transactionId = '1222';
      (transactionBlockingService.cancelBlockingTransferTransaction as jest.Mock).mockResolvedValueOnce(
        undefined
      );

      // Given
      const options = {
        method: 'POST',
        url: `/blocking-transfer-transactions/${transactionId}/cancel?cif=${cif}`,
        payload: undefined
      };
      // When
      const response: hapi.ServerInjectResponse = await server.inject(options);
      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.NO_CONTENT);
      expect(
        transactionBlockingService.cancelBlockingTransferTransaction
      ).toBeCalledWith(cif, transactionId);
    });

    describe('private', () => {
      it('should responds 204 with correct payload', async () => {
        const cif = 'cif123';
        const transactionId = '1222';
        (transactionBlockingService.cancelBlockingTransferTransaction as jest.Mock).mockResolvedValueOnce(
          undefined
        );

        // Given
        const options = {
          method: 'POST',
          url: `/private/blocking-transfer-transactions/${transactionId}/cancel?cif=${cif}`,
          payload: undefined
        };
        // When
        const response: hapi.ServerInjectResponse = await server.inject(
          options
        );
        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.NO_CONTENT);
        expect(
          transactionBlockingService.cancelBlockingTransferTransaction
        ).toBeCalledWith(cif, transactionId);
      });
    });
  });

  describe('manual adjustment unblocking transaction', () => {
    it('should responds 204 with correct payload', async () => {
      const transactionId = '1222';
      (transactionBlockingService.manualUnblockingTransferTransaction as jest.Mock).mockResolvedValueOnce(
        undefined
      );

      // Given
      const options = {
        method: 'POST',
        url: `/manual-adjustment/unblock/${transactionId}`,
        payload: undefined,
        headers: validHeader
      };
      // When
      const response: hapi.ServerInjectResponse = await server.inject(options);
      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.NO_CONTENT);
      expect(
        transactionBlockingService.manualUnblockingTransferTransaction
      ).toBeCalledWith(transactionId);
    });

    it('should responds 404 with transaction not found', async () => {
      const transactionId = undefined;
      (transactionBlockingService.manualUnblockingTransferTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.TRANSACTION_NOT_FOUND
        )
      );

      // Given
      const options = {
        method: 'POST',
        url: `/manual-adjustment/unblock/${transactionId}`,
        payload: undefined,
        headers: validHeader
      };
      // When
      const response: hapi.ServerInjectResponse = await server.inject(options);
      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.NOT_FOUND);
    });

    it('should responds 401 without authorization', async () => {
      const transactionId = '1222';
      (transactionBlockingService.manualUnblockingTransferTransaction as jest.Mock).mockResolvedValueOnce(
        undefined
      );

      // Given
      const options = {
        method: 'POST',
        url: `/manual-adjustment/unblock/${transactionId}`,
        payload: undefined
      };
      // When
      const response: hapi.ServerInjectResponse = await server.inject(options);
      // Then
      expect(response.statusCode).toEqual(401);
    });
  });

  describe('execute blocking transaction', () => {
    it('should responds 204 with correct payload', async () => {
      const cif = 'cif123';
      const transactionId = '1222';
      const amount = 3000;
      (transactionBlockingService.executeBlockingTransferTransaction as jest.Mock).mockResolvedValueOnce(
        undefined
      );

      // Given
      const options = {
        method: 'POST',
        url: `/blocking-transfer-transactions/${transactionId}/execute?cif=${cif}`,
        payload: {
          amount
        }
      };
      // When
      const response: hapi.ServerInjectResponse = await server.inject(options);
      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.NO_CONTENT);
      expect(
        transactionBlockingService.executeBlockingTransferTransaction
      ).toBeCalledWith(cif, transactionId, amount);
    });

    describe('private', () => {
      it('should responds 204 with correct payload', async () => {
        const cif = 'cif123';
        const transactionId = '1222';
        const amount = 3000;
        (transactionBlockingService.executeBlockingTransferTransaction as jest.Mock).mockResolvedValueOnce(
          undefined
        );

        // Given
        const options = {
          method: 'POST',
          url: `/private/blocking-transfer-transactions/${transactionId}/execute?cif=${cif}`,
          payload: {
            amount
          }
        };
        // When
        const response: hapi.ServerInjectResponse = await server.inject(
          options
        );
        // Then
        expect(response.statusCode).toEqual(Http.StatusCode.NO_CONTENT);
        expect(
          transactionBlockingService.executeBlockingTransferTransaction
        ).toBeCalledWith(cif, transactionId, amount);
      });
    });
  });

  describe('top up internal transaction', () => {
    it('should responds 201 with valid payload', async () => {
      // arrange
      const topupTransactionPayload: TopupInternalTransactionPayload = {
        sourceCIF: '8a8e86866df7cf8d016df87dae2f0262',
        beneficiaryAccountNo: '90020908971798',
        beneficiaryBankCode: 'BC003',
        transactionAmount: 15400,
        paymentServiceCode: 'SIT02',
        additionalInformation1: 'note 1',
        additionalInformation2: 'note 2',
        additionalInformation3: 'note 3',
        additionalInformation4: 'note 4',
        paymentServiceType: PaymentServiceTypeEnum.WALLET
      };
      const options = {
        method: 'POST',
        url: `/top-up-internal-transactions?customerId=customerId`,
        payload: topupTransactionPayload
      };
      const expectedTransactionResponse = {
        id: '1288437484',
        transactionIds: ['123', '456']
      };
      (transactionService.createInternalTopupTransaction as jest.Mock).mockResolvedValueOnce(
        {
          ...expectedTransactionResponse,
          coreBankingTransactionIds: expectedTransactionResponse.transactionIds
        }
      );
      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data).toEqual(expectedTransactionResponse);
      expect(transactionService.createInternalTopupTransaction).toBeCalledWith({
        ...topupTransactionPayload,
        sourceCustomerId: 'customerId'
      });
    });

    describe('private', () => {
      it('should responds 201 with valid payload', async () => {
        // arrange
        const topupTransactionPayload: TopupInternalTransactionPayload = {
          sourceCIF: '8a8e86866df7cf8d016df87dae2f0262',
          beneficiaryAccountNo: '90020908971798',
          beneficiaryBankCode: 'BC003',
          transactionAmount: 15400,
          paymentServiceCode: 'SIT02',
          additionalInformation1: 'note 1',
          additionalInformation2: 'note 2',
          additionalInformation3: 'note 3',
          additionalInformation4: 'note 4',
          paymentServiceType: PaymentServiceTypeEnum.WALLET
        };
        const options = {
          method: 'POST',
          url: `/private/top-up-internal-transactions?customerId=customerId`,
          payload: topupTransactionPayload
        };
        const expectedTransactionResponse = {
          id: '1288437484',
          transactionIds: ['123', '456']
        };
        (transactionService.createInternalTopupTransaction as jest.Mock).mockResolvedValueOnce(
          {
            ...expectedTransactionResponse,
            coreBankingTransactionIds:
              expectedTransactionResponse.transactionIds
          }
        );
        // act
        const response: any = await server.inject(options);

        // assert
        expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
        expect(response.result.data).toEqual(expectedTransactionResponse);
        expect(
          transactionService.createInternalTopupTransaction
        ).toBeCalledWith({
          ...topupTransactionPayload,
          sourceCustomerId: 'customerId'
        });
      });
    });
  });

  describe('top up external transaction', () => {
    it('should responds 201 with valid payload', async () => {
      // arrange
      const topupTransactionPayload: TopupExternalTransactionPayload = {
        beneficiaryAccountNo: '90020908971798',
        transactionAmount: 15400,
        additionalInformation1: 'note 1',
        additionalInformation2: 'note 2',
        additionalInformation3: 'note 3',
        additionalInformation4: 'note 4'
      };
      const options = {
        method: 'POST',
        url: `/top-up-external-transactions`,
        payload: topupTransactionPayload
      };
      const expectedTransactionResponse = {
        id: '1288437484',
        transactionIds: ['123']
      };
      (externalTransactionService.createExternalTopupTransaction as jest.Mock).mockResolvedValueOnce(
        {
          ...expectedTransactionResponse,
          coreBankingTransactionIds: expectedTransactionResponse.transactionIds
        }
      );
      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data).toEqual(expectedTransactionResponse);
      expect(
        externalTransactionService.createExternalTopupTransaction
      ).toBeCalledWith(topupTransactionPayload);
    });

    describe('private', () => {
      it('should responds 201 with valid payload', async () => {
        // arrange
        const topupTransactionPayload: TopupExternalTransactionPayload = {
          beneficiaryAccountNo: '90020908971798',
          transactionAmount: 15400,
          additionalInformation1: 'note 1',
          additionalInformation2: 'note 2',
          additionalInformation3: 'note 3',
          additionalInformation4: 'note 4'
        };
        const options = {
          method: 'POST',
          url: `/private/top-up-external-transactions`,
          payload: topupTransactionPayload
        };
        const expectedTransactionResponse = {
          id: '1288437484',
          transactionIds: ['123']
        };
        (externalTransactionService.createExternalTopupTransaction as jest.Mock).mockResolvedValueOnce(
          {
            ...expectedTransactionResponse,
            coreBankingTransactionIds:
              expectedTransactionResponse.transactionIds
          }
        );
        // act
        const response: any = await server.inject(options);

        // assert
        expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
        expect(response.result.data).toEqual(expectedTransactionResponse);
        expect(
          externalTransactionService.createExternalTopupTransaction
        ).toBeCalledWith(topupTransactionPayload);
      });
    });
  });

  describe('confirm transaction', () => {
    const confirmTransactionPayload: ConfirmTransactionRequest = mockConfirmTransactionRequest();

    const expectedResponse: ConfirmTransactionResponse = {
      transactionResponseID: confirmTransactionPayload.transactionResponseID
    };

    it('should response 200 with valid payload', async () => {
      // arrange
      const options = {
        method: 'POST',
        url: `/transaction-confirmation`,
        payload: confirmTransactionPayload
      };

      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        expectedResponse
      );
      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(response.result.data).toEqual(expectedResponse);
      expect(switchingTransactionService.confirmTransaction).toBeCalledWith(
        confirmTransactionPayload
      );
    });

    it('should response 200 with valid payload and allow extra invalid field', async () => {
      // arrange
      const options = {
        method: 'POST',
        url: `/transaction-confirmation`,
        payload: {
          ...confirmTransactionPayload,
          demo: 'test'
        }
      };

      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        expectedResponse
      );
      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(response.result.data).toEqual(expectedResponse);
      expect(switchingTransactionService.confirmTransaction).toBeCalledWith(
        confirmTransactionPayload
      );
    });

    it('should return 400 response if transactionDate is invalid format', async () => {
      // arrange
      const payload = {
        ...confirmTransactionPayload,
        transactionDate: '0612121212' // invalid YYMMDDhhmmss format
      };
      const options = {
        method: 'POST',
        url: `/transaction-confirmation`,
        payload
      };
      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
    });

    it('should return bad request error (400) when service throw AppError', async () => {
      // arrange
      const options = {
        method: 'POST',
        url: `/transaction-confirmation`,
        payload: confirmTransactionPayload
      };

      (switchingTransactionService.confirmTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.INVALID_EXTERNALID
        )
      );

      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
      expect(response.result.error.code).toEqual(ERROR_CODE.INVALID_EXTERNALID);
    });

    it('should return server error (500) when service throw server error', async () => {
      // arrange
      const options = {
        method: 'POST',
        url: `/transaction-confirmation`,
        payload: confirmTransactionPayload
      };

      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        new Error()
      );
      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(
        Http.StatusCode.INTERNAL_SERVER_ERROR
      );
    });

    it('should return 400 response if responseCode is invalid', async () => {
      // give
      const payload = {
        ...confirmTransactionPayload,
        responseCode: 409
      };
      const options = {
        method: 'POST',
        url: `/transaction-confirmation`,
        payload
      };

      // when
      const response: any = await server.inject(options);

      // then
      expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
    });

    it('should call reversal entitlement when something went wrong', async () => {
      // arrange
      const options = {
        method: 'POST',
        url: `/transaction-confirmation`,
        payload: confirmTransactionPayload
      };

      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        new Error()
      );
      entitlementFlag.isEnabled = jest.fn().mockResolvedValue(true);
      entitlementService.processReversalEntitlementCounterUsageOnTransactionConfirmation = jest
        .fn()
        .mockResolvedValue({});
      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(
        Http.StatusCode.INTERNAL_SERVER_ERROR
      );
      expect(
        entitlementService.processReversalEntitlementCounterUsageOnTransactionConfirmation
      ).toBeCalledTimes(1);
    });

    it(`
    GIVEN 
    - something went wrong
    - atm withdrawal daily limit pocket level validation flag is enabled
    THEN
      - ensured transactionUsageService.processReversalBasedOnContext called once
    `, async () => {
      // arrange
      const options = {
        method: 'POST',
        url: `/transaction-confirmation`,
        payload: confirmTransactionPayload
      };

      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        new Error()
      );
      entitlementFlag.isEnabled = jest.fn().mockResolvedValue(false);

      entitlementService.processReversalEntitlementCounterUsageOnTransactionConfirmation = jest
        .fn()
        .mockResolvedValue({});

      const processReversalPocketLimitByTransactionConfirmationInputFn = jest.spyOn(
        transactionUsageService,
        'processReversalPocketLimitByTransactionConfirmationInput'
      );
      processReversalPocketLimitByTransactionConfirmationInputFn.mockImplementationOnce(
        _ => {}
      );

      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(
        Http.StatusCode.INTERNAL_SERVER_ERROR
      );
      expect(
        processReversalPocketLimitByTransactionConfirmationInputFn
      ).toBeCalledTimes(1);
    });

    it(`should return response 200 with valid payload when query parameter (includeTransactionId, includeExternalId, includeCoreBankingTransactions) is 'true'`, async () => {
      // arrange
      const options = {
        method: 'POST',
        url: `/transaction-confirmation?includeCoreBankingTransactions=true&includeTransactionId=true&includeExternalId=true`,
        payload: confirmTransactionPayload
      };
      const expectedTransactionConfirmationResponse = {
        ...expectedResponse,
        externalId: 'externalId',
        id: 'id',
        coreBankingTransactions: [
          {
            id: '500001',
            type: TransferJourneyStatusEnum.AMOUNT_DEBITED
          },
          {
            id: '500002',
            type: TransferJourneyStatusEnum.AMOUNT_DEBITED_REVERSED
          }
        ]
      };

      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        {
          ...expectedTransactionConfirmationResponse
        }
      );

      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(response.result.data.id).toEqual(
        expectedTransactionConfirmationResponse.id
      );
      expect(response.result.data.externalId).toEqual(
        expectedTransactionConfirmationResponse.externalId
      );
      expect(response.result.data.coreBankingTransactions).toEqual(
        expectedTransactionConfirmationResponse.coreBankingTransactions
      );
      expect(switchingTransactionService.confirmTransaction).toBeCalledWith(
        confirmTransactionPayload
      );
    });

    it(`should return response 200 with valid payload when query parameter (includeTransactionId, includeExternalId, includeCoreBankingTransactions) is 'false'`, async () => {
      // arrange
      const options = {
        method: 'POST',
        url: `/transaction-confirmation`,
        payload: confirmTransactionPayload
      };
      const expectedTransactionConfirmationResponse = {
        ...expectedResponse,
        externalId: 'externalId',
        id: 'id',
        coreBankingTransactions: [
          {
            id: '500001',
            type: TransferJourneyStatusEnum.AMOUNT_DEBITED
          },
          {
            id: '500002',
            type: TransferJourneyStatusEnum.AMOUNT_DEBITED_REVERSED
          }
        ]
      };

      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionConfirmationResponse
      );

      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(response.result.data).toEqual(expectedResponse);
      expect(switchingTransactionService.confirmTransaction).toBeCalledWith(
        confirmTransactionPayload
      );
    });
  });

  describe('handle external transaction', () => {
    const transactionInput: ExternalTransactionInput = {
      externalId: 'externalId',
      interchange: BankNetworkEnum.ALTO,
      transactionDate: '200612111111',
      paymentServiceType: PaymentServiceTypeEnum.THIRD_PARTY_TRANSFER,
      // RTOL, remittance
      sourceBankCode: '008',
      sourceAccountNo: '123',
      sourceAccountName: 'external',
      // currency iso code
      currency: '123',
      transactionAmount: 10000,
      // RTOL, remittance
      beneficiaryBankCode: '245',
      beneficiaryAccountNo: '1234567',
      beneficiaryAccountName: 'internal',
      notes: 'note',
      additionalInformation1: 'additionalInformation1',
      additionalInformation2: 'additionalInformation2',
      additionalInformation3: 'additionalInformation3',
      additionalInformation4: 'additionalInformation4'
    };

    const expectedTransactionResponse = {
      id: '5da02ad923d9f74ced0087d1',
      externalId: transactionInput.externalId,
      coreBankingTransactions: [
        {
          id: '500001',
          type: TransferJourneyStatusEnum.AMOUNT_DEBITED
        },
        {
          id: '500002',
          type: TransferJourneyStatusEnum.AMOUNT_CREDITED
        }
      ]
    };

    it('should response 201 with valid payload and not set query parameters', async () => {
      // Given

      const options = {
        method: 'POST',
        url: '/external-transactions',
        payload: transactionInput
      };

      (externalTransactionService.createExternalTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data.id).toEqual(expectedTransactionResponse.id);
      expect(response.result.data.externalId).toEqual(
        expectedTransactionResponse.externalId
      );
      expect(response.result.data.coreBankingTransactions).toBeUndefined();
      expect(
        externalTransactionService.createExternalTransaction
      ).toBeCalledWith(transactionInput, undefined);
    });

    it('should response 201 with valid payload and allow extra invalid fields', async () => {
      // Given

      const options = {
        method: 'POST',
        url: '/external-transactions',
        payload: {
          ...transactionInput,
          demo: 'test'
        }
      };

      (externalTransactionService.createExternalTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data.id).toEqual(expectedTransactionResponse.id);
      expect(response.result.data.externalId).toEqual(
        expectedTransactionResponse.externalId
      );
      expect(response.result.data.coreBankingTransactions).toBeUndefined();
      expect(
        externalTransactionService.createExternalTransaction
      ).toBeCalledWith(transactionInput, undefined);
    });

    it('should response 201 with valid payload and idempotencyKey', async () => {
      // Given

      const options = {
        method: 'POST',
        url: '/external-transactions',
        payload: transactionInput,
        headers: {
          [HttpHeaders.X_IDEMPOTENCY_KEY]: 'idempotencyKey'
        }
      };

      (externalTransactionService.createExternalTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data.id).toEqual(expectedTransactionResponse.id);
      expect(response.result.data.externalId).toEqual(
        expectedTransactionResponse.externalId
      );
      expect(response.result.data.coreBankingTransactions).toBeUndefined();
      expect(
        externalTransactionService.createExternalTransaction
      ).toBeCalledWith(transactionInput, 'idempotencyKey');
    });

    it('should response 201 for payload containing processingInfo and workflowId and invoke externalTransactionService.createExternalTransaction() with processingInfo and workflowId', async () => {
      // Given
      const input = {
        ...transactionInput,
        processingInfo: {
          partner: 'ATOME',
          method: ExecutionMethodEnum.ONE_BY_ONE
        },
        workflowId: 'workflowIdValue'
      };
      const options = {
        method: 'POST',
        url: '/external-transactions',
        payload: input
      };

      (externalTransactionService.createExternalTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(
        externalTransactionService.createExternalTransaction
      ).toBeCalledWith(
        expect.objectContaining({
          processingInfo: {
            partner: 'ATOME',
            method: ExecutionMethodEnum.ONE_BY_ONE
          },
          workflowId: 'workflowIdValue'
        }),
        undefined
      );
    });

    it('should response 201 for payload containing processingInfo and correlationId and invoke externalTransactionService.createExternalTransaction() with processingInfo and correlationId', async () => {
      // Given
      const input = {
        ...transactionInput,
        processingInfo: {
          partner: 'ATOME',
          method: ExecutionMethodEnum.ONE_BY_ONE
        },
        correlationId: 'correlationIdValue'
      };
      const options = {
        method: 'POST',
        url: '/external-transactions',
        payload: input
      };

      (externalTransactionService.createExternalTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(
        externalTransactionService.createExternalTransaction
      ).toBeCalledWith(
        expect.objectContaining({
          processingInfo: {
            partner: 'ATOME',
            method: ExecutionMethodEnum.ONE_BY_ONE
          },
          correlationId: 'correlationIdValue'
        }),
        undefined
      );
    });

    it('should response 201 with valid payload and query parameter includeCoreBankingTransactions is false', async () => {
      // Given

      const options = {
        method: 'POST',
        url: '/external-transactions?includeCoreBankingTransactions=false',
        payload: transactionInput
      };

      (externalTransactionService.createExternalTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data.id).toEqual(expectedTransactionResponse.id);
      expect(response.result.data.externalId).toEqual(
        expectedTransactionResponse.externalId
      );
      expect(response.result.data.coreBankingTransactions).toBeUndefined();
      expect(
        externalTransactionService.createExternalTransaction
      ).toBeCalledWith(transactionInput, undefined);
    });

    it('should response 201 with valid payload and query parameter includeCoreBankingTransactions is true', async () => {
      // Given

      const options = {
        method: 'POST',
        url: '/external-transactions?includeCoreBankingTransactions=true',
        payload: transactionInput
      };

      (externalTransactionService.createExternalTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data).toEqual(expectedTransactionResponse);
      expect(
        externalTransactionService.createExternalTransaction
      ).toBeCalledWith(transactionInput, undefined);
    });

    it('should call entitlement reverse when flag is enabled', async () => {
      const options = {
        method: 'POST',
        url: '/external-transactions',
        payload: transactionInput
      };

      (externalTransactionService.createExternalTransaction as jest.Mock).mockRejectedValueOnce(
        false
      );
      entitlementFlag.isEnabled = jest.fn().mockResolvedValue(true);
      entitlementService.processReversalEntitlementCounterUsageBasedOnContext = jest
        .fn()
        .mockResolvedValue({});

      // When
      await server.inject(options);
      expect(
        entitlementService.processReversalEntitlementCounterUsageBasedOnContext
      ).toBeCalled();
    });

    it(`
      GIVEN 
          the external transaction failed
        AND
          feature flag for entitlement is disabled
      THEN
        ensure that pocketLimit reversal is called
    `, async () => {
      const options = {
        method: 'POST',
        url: '/external-transactions',
        payload: transactionInput
      };

      (externalTransactionService.createExternalTransaction as jest.Mock).mockRejectedValueOnce(
        false
      );

      //entitlement flag is disabled
      entitlementFlag.isEnabled = jest.fn().mockResolvedValue(false);

      const processReversalPocketLimitByTransactionBasedOnContextFn = transactionUsageService.processReversalPocketLimitByTransactionBasedOnContext as jest.Mock;
      processReversalPocketLimitByTransactionBasedOnContextFn.mockImplementationOnce(
        () => {}
      );

      // When
      await server.inject(options);
      expect(
        processReversalPocketLimitByTransactionBasedOnContextFn
      ).toBeCalledTimes(1);
    });

    it(`
    GIVEN 
        the external transaction failed
    THEN
      - ensure that entitlement reversal is called
      - ensure that pocketLimit reversal is called
  `, async () => {
      const options = {
        method: 'POST',
        url: '/external-transactions',
        payload: transactionInput
      };

      (externalTransactionService.createExternalTransaction as jest.Mock).mockRejectedValueOnce(
        false
      );

      //entitlement flag is disabled
      entitlementFlag.isEnabled = jest.fn().mockResolvedValue(true);
      entitlementService.processReversalEntitlementCounterUsageBasedOnContext = jest
        .fn()
        .mockResolvedValue({});

      entitlementFlag.isEnabled = jest.fn().mockResolvedValue(true);

      const processReversalPocketLimitByTransactionBasedOnContextFn = transactionUsageService.processReversalPocketLimitByTransactionBasedOnContext as jest.Mock;
      processReversalPocketLimitByTransactionBasedOnContextFn.mockImplementationOnce(
        () => {}
      );

      // When
      await server.inject(options);
      expect(
        entitlementService.processReversalEntitlementCounterUsageBasedOnContext
      ).toBeCalledTimes(1);
      expect(
        processReversalPocketLimitByTransactionBasedOnContextFn
      ).toBeCalledTimes(1);
    });

    it('should allow alphanumeric source account number for INCOMING_RTGS', async () => {
      // Given
      transactionInput.paymentServiceType =
        PaymentServiceTypeEnum.INCOMING_RTGS;
      transactionInput.sourceAccountNo = '123a4';
      const idempotencyKey = '0102030405';
      const options = {
        method: 'POST',
        url: '/external-transactions',
        payload: transactionInput,
        headers: {
          'x-idempotency-key': idempotencyKey
        }
      };

      (externalTransactionService.createExternalTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data.id).toEqual(expectedTransactionResponse.id);
      expect(response.result.data.externalId).toEqual(
        expectedTransactionResponse.externalId
      );
      expect(response.result.data.coreBankingTransactions).toBeUndefined();
      expect(
        externalTransactionService.createExternalTransaction
      ).toBeCalledWith(transactionInput, idempotencyKey);
    });

    it('should allow alphanumeric source account number for INCOMING_SKN', async () => {
      // Given
      transactionInput.paymentServiceType = PaymentServiceTypeEnum.INCOMING_SKN;
      transactionInput.sourceAccountNo = '123a4';
      const idempotencyKey = '0102030405';
      const options = {
        method: 'POST',
        url: '/external-transactions',
        payload: transactionInput,
        headers: {
          'x-idempotency-key': idempotencyKey
        }
      };

      (externalTransactionService.createExternalTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data.id).toEqual(expectedTransactionResponse.id);
      expect(response.result.data.externalId).toEqual(
        expectedTransactionResponse.externalId
      );
      expect(response.result.data.coreBankingTransactions).toBeUndefined();
      expect(
        externalTransactionService.createExternalTransaction
      ).toBeCalledWith(transactionInput, idempotencyKey);
    });

    it('should allow special character on source account number for INCOMING_BIFAST', async () => {
      // Given
      transactionInput.paymentServiceType =
        PaymentServiceTypeEnum.INCOMING_BIFAST;
      transactionInput.sourceAccountNo = '62-123456789';
      const idempotencyKey = '0102030405';
      const options = {
        method: 'POST',
        url: '/external-transactions',
        payload: transactionInput,
        headers: {
          'x-idempotency-key': idempotencyKey
        }
      };

      (externalTransactionService.createExternalTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data.id).toEqual(expectedTransactionResponse.id);
      expect(response.result.data.externalId).toEqual(
        expectedTransactionResponse.externalId
      );
      expect(response.result.data.coreBankingTransactions).toBeUndefined();
      expect(
        externalTransactionService.createExternalTransaction
      ).toBeCalledWith(transactionInput, idempotencyKey);
    });

    it('should return 400 for alphanumeric source account number when paymentServiceType is neither INCOMING_RTGS nor INCOMING_SKN or INCOMING_BIFAST', async () => {
      // Given
      transactionInput.paymentServiceType =
        PaymentServiceTypeEnum.THIRD_PARTY_ATM_WITHDRAWAL;
      transactionInput.sourceAccountNo = '123a4';
      const options = {
        method: 'POST',
        url: '/external-transactions',
        payload: transactionInput,
        headers: {
          'x-idempotency-key': '0102030405'
        }
      };

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
    });

    it('should return 400 when additionalInformation1 more than 250 characters', async () => {
      // Given
      transactionInput.additionalInformation1 =
        'LOmHWPfxdfVLBnoGkRA8XJOn8nNufO4Sy4KHl8qIU8A50fplBhjFkJvKWw16cLcOrmYV93GWn7NOtpTkn9QXchLuNkhqb9pCFZRz8BgaYGy11ltg4fMtwC7uiag4J6dwfwS74NHXRaMK0LTO0TayBBnlySAQEtHjDizPiG0fYT8wCr8FhCHywAzEWILoBmI8TIxVOQd5BEdJ8eng0keRlIe7EJLEH1E4bP';
      const options = {
        method: 'POST',
        url: '/external-transactions',
        payload: transactionInput
      };

      (externalTransactionService.createExternalTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
    });
  });

  describe('get external transaction fee', () => {
    it('should response 200 with valid payload', async () => {
      // Given
      const incomingInput: BaseExternalTransactionInput = {
        externalId: 'externalId',
        transactionDate: '200612111111',
        paymentServiceType: PaymentServiceTypeEnum.THIRD_PARTY_TRANSFER,
        // RTOL, remittance
        sourceBankCode: '008',
        sourceAccountNo: '123',
        sourceAccountName: 'external',
        // currency iso code
        currency: '123',
        transactionAmount: 10000,
        // RTOL, remittance
        beneficiaryBankCode: '245',
        beneficiaryAccountNo: '1234567',
        beneficiaryAccountName: 'internal',
        notes: 'note',
        additionalInformation1: 'additionalInformation1',
        additionalInformation2: 'additionalInformation2',
        additionalInformation3: 'additionalInformation3',
        additionalInformation4: 'additionalInformation4',
        interchange: BankNetworkEnum.ALTO
      };
      const options = {
        method: 'POST',
        url: '/external-transaction-fee',
        payload: incomingInput
      };

      const expectedResponse = {
        externalId: '5da02ad923d9f74ced0087d1',
        feeAmount: 123
      };

      (externalTransactionService.getExternalTransactionFee as jest.Mock).mockResolvedValueOnce(
        expectedResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(response.result.data).toEqual(expectedResponse);
      expect(
        externalTransactionService.getExternalTransactionFee
      ).toBeCalledWith(incomingInput);
    });
  });

  describe('handle refund transaction', () => {
    it('should response 201 with valid payload', async () => {
      // Given
      const transactionInput = {
        transactionId: '5da02ad923d9f74ced0087d1'
      };
      const options = {
        method: 'POST',
        url: `/admin/refund/${transactionInput.transactionId}`,
        headers: validHeader
      };

      const expectedResponse = {
        id: '5da02ad923d9f74ced008657'
      };

      (generalRefundService.refundTransferTransaction as jest.Mock).mockResolvedValueOnce(
        expectedResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data).toEqual(expectedResponse);
      expect(generalRefundService.refundTransferTransaction).toBeCalledWith(
        transactionInput.transactionId,
        undefined
      );
    });

    it('should response 201 with valid payload with refundFees true', async () => {
      // Given
      const transactionInput = {
        transactionId: '5da02ad923d9f74ced0087d1'
      };
      const options = {
        method: 'POST',
        url: `/admin/refund/${transactionInput.transactionId}?refundFee=true`,
        headers: validHeader
      };

      const expectedResponse = {
        id: '5da02ad923d9f74ced008657'
      };

      (generalRefundService.refundTransferTransaction as jest.Mock).mockResolvedValueOnce(
        expectedResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.CREATED);
      expect(response.result.data).toEqual(expectedResponse);
      expect(generalRefundService.refundTransferTransaction).toBeCalledWith(
        transactionInput.transactionId,
        true
      );
    });
  });

  describe('controller: /admin/manual-adjustment', () => {
    beforeEach(() => {
      (transactionService.getTransaction as jest.Mock).mockRestore();
    });
    it('Should error with 400 bad request: only allowed status SUCCEED | DECLINED', async () => {
      // Given
      const txnId = 'dummy123';
      const options = {
        method: Http.Method.POST,
        url: '/admin/manual-adjustment',
        payload: {
          transactionId: txnId,
          status: TransactionStatus.PENDING
        },
        headers: validHeader
      };
      // When
      const response = await server.inject(options);

      // Then
      expect(response.statusCode).toBe(Http.StatusCode.BAD_REQUEST);
    });

    it('Should error with 400 bad request: only allowed BankChannel EXTERNAL | GOBILL', async () => {
      // Given
      const txnId = 'dummy123';
      const options = {
        method: Http.Method.POST,
        url: '/admin/manual-adjustment',
        payload: {
          transactionId: txnId,
          status: TransactionStatus.DECLINED
        },
        headers: validHeader
      };
      const resTxn = {
        ...getTransactionModelData(),
        status: TransactionStatus.SUBMITTED,
        beneficiaryBankCodeChannel: BankChannelEnum.GOJEK,
        id: txnId
      };
      (transactionService.getTransaction as jest.Mock).mockResolvedValueOnce(
        resTxn
      );

      // When
      const response = await server.inject(options);

      // Then
      expect(transactionService.getTransaction).toBeCalledWith(txnId);
      expect(response.statusCode).toBe(Http.StatusCode.BAD_REQUEST);
    });

    it('Should error with 400 bad request: only allowed valid interchange', async () => {
      // Given
      const txnId = 'dummy123';
      const options = {
        method: Http.Method.POST,
        url: '/admin/manual-adjustment',
        payload: {
          transactionId: txnId,
          status: TransactionStatus.DECLINED,
          interchange: 'dummyInterchange'
        },
        headers: validHeader
      };
      // When
      const response = await server.inject(options);

      // Then
      expect(response.statusCode).toBe(Http.StatusCode.BAD_REQUEST);
    });

    it('Should error with 400 bad request: transaction date is beyond adjustment', async () => {
      // Given
      const txnId = 'dummy123';
      const options = {
        method: Http.Method.POST,
        url: '/admin/manual-adjustment',
        payload: {
          transactionId: txnId,
          status: TransactionStatus.SUCCEED,
          interchange: BankNetworkEnum.ALTO
        },
        headers: validHeader
      };
      const resTxn = {
        ...getTransactionModelData(),
        status: TransactionStatus.DECLINED,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        id: txnId,
        interchange: BankNetworkEnum.ALTO,
        updatedAt: new Date('2019-11-11T11:40:44.786Z')
      };
      (transactionService.getTransaction as jest.Mock).mockResolvedValueOnce(
        resTxn
      );
      // When
      const response = await server.inject(options);

      // Then
      expect(response.statusCode).toBe(Http.StatusCode.BAD_REQUEST);
    });

    it('Should error with 400 bad request: interchange not expected for IRIS | GOBILLS', async () => {
      // Given
      const txnId = 'dummy123';
      const options = {
        method: Http.Method.POST,
        url: '/admin/manual-adjustment',
        payload: {
          transactionId: txnId,
          status: TransactionStatus.SUCCEED,
          interchange: 'ALTO'
        },
        headers: validHeader
      };
      const resTxn = {
        ...getTransactionModelData(),
        status: TransactionStatus.SUBMITTED,
        beneficiaryBankCodeChannel: BankChannelEnum.IRIS,
        id: txnId
      };
      (transactionService.getTransaction as jest.Mock).mockResolvedValueOnce(
        resTxn
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toBe(Http.StatusCode.BAD_REQUEST);
      expect(response.result.error.code).toBe(
        ERROR_CODE.INTERCHANGE_NOT_EXPECTED
      );
    });

    it('should respond 200:extTxnManualAdjstment', async () => {
      // Given
      const txnId = 'dummy123';
      const options = {
        method: Http.Method.POST,
        url: '/admin/manual-adjustment',
        payload: {
          transactionId: txnId,
          status: TransactionStatus.DECLINED,
          interchange: BankNetworkEnum.ALTO
        },
        headers: validHeader
      };

      const resTxn = {
        ...getTransactionModelData(),
        status: TransactionStatus.SUBMITTED,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        id: txnId,
        interchange: BankNetworkEnum.ALTO
      };

      (transactionService.getTransaction as jest.Mock).mockResolvedValueOnce(
        resTxn
      );

      (txnManualAdjustmentService.extTxnManualAdjstment as jest.Mock).mockResolvedValueOnce(
        null
      );
      // When
      const response: any = await server.inject(options);

      // Then
      expect(transactionService.getTransaction).toBeCalledWith(txnId);
      expect(txnManualAdjustmentService.extTxnManualAdjstment).toBeCalledWith(
        expect.objectContaining({
          id: txnId
        }),
        TransactionStatus.DECLINED
      );
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
    });

    it('should respond 200:extTxnManualAdjstment for bankCode Channel WINCOR', async () => {
      // Given
      const txnId = 'dummy123';
      const options = {
        method: Http.Method.POST,
        url: '/admin/manual-adjustment',
        payload: {
          transactionId: txnId,
          status: TransactionStatus.DECLINED
        },
        headers: validHeader
      };

      const resTxn = {
        ...getTransactionModelData(),
        status: TransactionStatus.SUBMITTED,
        beneficiaryBankCodeChannel: BankChannelEnum.WINCOR,
        beneficiaryBankCode: 'BC191',
        id: txnId
      };

      (transactionService.getTransaction as jest.Mock).mockResolvedValueOnce(
        resTxn
      );

      (txnManualAdjustmentService.extTxnManualAdjstment as jest.Mock).mockResolvedValueOnce(
        null
      );
      // When
      const response: any = await server.inject(options);

      // Then
      expect(transactionService.getTransaction).toBeCalledWith(txnId);
      expect(txnManualAdjustmentService.extTxnManualAdjstment).toBeCalledWith(
        expect.objectContaining({
          id: txnId
        }),
        TransactionStatus.DECLINED
      );
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
    });

    it('should respond 200:extTxnManualAdjstment for bankCode Channel TOKOPEDIA', async () => {
      // Given
      const txnId = 'dummy123';
      const options = {
        method: Http.Method.POST,
        url: '/admin/manual-adjustment',
        payload: {
          transactionId: txnId,
          status: TransactionStatus.DECLINED
        },
        headers: validHeader
      };

      const resTxn = {
        ...getTransactionModelData(),
        status: TransactionStatus.SUBMITTED,
        beneficiaryBankCodeChannel: BankChannelEnum.TOKOPEDIA,
        beneficiaryBankCode: 'BC191',
        id: txnId
      };

      (transactionService.getTransaction as jest.Mock).mockResolvedValueOnce(
        resTxn
      );

      (txnManualAdjustmentService.extTxnManualAdjstment as jest.Mock).mockResolvedValueOnce(
        null
      );
      // When
      const response: any = await server.inject(options);

      // Then
      expect(transactionService.getTransaction).toBeCalledWith(txnId);
      expect(txnManualAdjustmentService.extTxnManualAdjstment).toBeCalledWith(
        expect.objectContaining({
          id: txnId
        }),
        TransactionStatus.DECLINED
      );
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
    });

    it('should respond 200:txnManualAdjustment', async () => {
      // Given
      const txnId = 'dummy123';
      const options = {
        method: Http.Method.POST,
        url: '/admin/manual-adjustment',
        payload: {
          transactionId: txnId,
          status: TransactionStatus.DECLINED
        },
        headers: validHeader
      };

      const resTxn = {
        ...getTransactionModelData(),
        status: TransactionStatus.SUBMITTED,
        beneficiaryBankCodeChannel: BankChannelEnum.GOBILLS,
        id: txnId
      };

      (transactionService.getTransaction as jest.Mock).mockResolvedValueOnce(
        resTxn
      );

      (billPaymentService.txnManualAdjustment as jest.Mock).mockResolvedValueOnce(
        null
      );
      // When
      const response: any = await server.inject(options);

      // Then
      expect(transactionService.getTransaction).toBeCalledWith(txnId);
      expect(billPaymentService.txnManualAdjustment).toBeCalledWith(
        txnId,
        TransactionStatus.DECLINED,
        true
      );
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
    });

    it('should respond 200:txnManualAdjustment DECLINED to SUCCEED', async () => {
      // Given
      const txnId = 'dummy123';
      const options = {
        method: Http.Method.POST,
        url: '/admin/manual-adjustment',
        payload: {
          transactionId: txnId,
          status: TransactionStatus.SUCCEED
        },
        headers: validHeader
      };

      const resTxn = {
        ...getTransactionModelData(),
        status: TransactionStatus.DECLINED,
        beneficiaryBankCodeChannel: BankChannelEnum.GOBILLS,
        id: txnId
      };

      (transactionService.getTransaction as jest.Mock).mockResolvedValueOnce(
        resTxn
      );

      (billPaymentService.txnManualAdjustment as jest.Mock).mockResolvedValueOnce(
        null
      );
      // When
      const response: any = await server.inject(options);

      // Then
      expect(transactionService.getTransaction).toBeCalledWith(txnId);
      expect(billPaymentService.txnManualAdjustment).toBeCalledWith(
        txnId,
        TransactionStatus.SUCCEED,
        false
      );
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
    });
  });

  describe('controller: /transaction-status', () => {
    beforeEach(() => {
      jest.clearAllMocks();
      jest.resetAllMocks();
    });

    it('should return statusCode 404 when transaction is not found', async () => {
      // Given
      const transactionInput = {
        customerId: '123',
        paymentInstructionId: '123zxc',
        transactionDate: new Date()
      };
      const options = {
        method: Http.Method.POST,
        url: `/transaction-status`,
        payload: transactionInput,
        headers: validHeader
      };
      (transactionService.checkTransactionStatus as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.TRANSACTION_NOT_FOUND
        )
      );

      // When
      const response: hapi.ServerInjectResponse = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.NOT_FOUND);
      expect(transactionService.checkTransactionStatus).toBeCalledWith(
        transactionInput
      );
    });

    it('should return statusCode 200 when transaction is found', async () => {
      // Given
      const transactionInput = {
        customerId: '123',
        paymentInstructionId: '123zxc'
      };
      const options = {
        method: Http.Method.POST,
        url: `/transaction-status`,
        payload: transactionInput,
        headers: validHeader
      };
      (transactionService.checkTransactionStatus as jest.Mock).mockResolvedValueOnce(
        { status: 'SUCCEED' }
      );

      // When
      const response: hapi.ServerInjectResponse = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(transactionService.checkTransactionStatus).toBeCalledWith(
        transactionInput
      );
      expect(JSON.parse(response.payload)).toEqual({
        data: {
          status: 'SUCCEED'
        }
      });
    });

    it('should return statusCode 401 when received unauthorized request', async () => {
      // Given
      const transactionInput = {
        customerId: '123',
        paymentInstructionId: '123zxc'
      };
      const options = {
        method: Http.Method.POST,
        url: `/transaction-status`,
        payload: transactionInput,
        headers: invalidHeader
      };

      // When
      const response: hapi.ServerInjectResponse = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.UNAUTHORIZED);
      expect(transactionService.checkTransactionStatus).not.toBeCalled();
    });
  });

  describe('controller: /confirm-transaction-status', () => {
    it('should run the service with correct params', async () => {
      // Given
      const minTransactionAge = faker.random.number();
      const maxTransactionAge = faker.random.number();
      const recordLimit = faker.random.number();
      const enable = 1;
      const options = {
        method: 'GET',
        url: `/private/confirm-transaction-status?enable=${enable}&minTransactionAge=${minTransactionAge}&maxTransactionAge=${maxTransactionAge}&recordLimit=${recordLimit}`
      };

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(JSON.parse(response.payload)).toEqual({
        data: {
          response: 'success',
          enable: true
        }
      });
      expect(transactionService.resolveTransactionStatus).toBeCalledWith(
        minTransactionAge,
        maxTransactionAge,
        recordLimit
      );
    });

    it('should pass undefined to service if no params provided', async () => {
      // Given
      const enable = 1;
      const options = {
        method: 'GET',
        url: `/private/confirm-transaction-status?enable=${enable}`
      };

      // When
      const response: hapi.ServerInjectResponse = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(JSON.parse(response.payload)).toEqual({
        data: {
          response: 'success',
          enable: true
        }
      });
      expect(transactionService.resolveTransactionStatus).toBeCalledWith(
        undefined,
        undefined,
        undefined
      );
    });

    it('should not run the service if no flag in params', async () => {
      // Given
      const options = {
        method: 'GET',
        url: `/private/confirm-transaction-status`
      };

      // When
      const response: hapi.ServerInjectResponse = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(JSON.parse(response.payload)).toEqual({
        data: {
          response: 'success',
          enable: false
        }
      });
      expect(
        transactionService.resolveTransactionStatus
      ).not.toHaveBeenCalled();
    });
  });
});
