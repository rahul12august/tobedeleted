import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE, ErrorList } from '../../common/errors';
import {
  getBiFastBankCode,
  getITransferTransactionInput,
  getRecommendedService,
  getRecommendedServiceResponse,
  getRefundDetails,
  getTransactionDetail,
  getTransactionMockValueObject,
  getTransactionModelFullData,
  getWalletExternalBankCode,
  getWalletPartnerBankCode
} from '../__mocks__/transaction.data';
import {
  CreateDepositTransactionPayload,
  CreateWithdrawTransactionPayload,
  ITopupInternalTransactionInput,
  ITransferTransactionInput,
  RecommendationService,
  RecommendPaymentServiceInput,
  TopupInternalTransactionPayload,
  TransactionListRequest
} from '../transaction.type';
import { IBankCode } from '../../configuration/configuration.type';
import { ITransactionModel } from '../transaction.model';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../transaction.enum';
import {
  BankChannelEnum,
  BankNetworkEnum,
  PaymentServiceTypeEnum,
  Constant
} from '@dk/module-common';
import { IBasicAccountInfo } from '../../account/account.type';
import { IFeeAmount } from '../../fee/fee.type';
import { IVirtualAccount } from '../../virtualAccount/virtualAccount.type';
import depositRepository from '../../coreBanking/deposit.repository';
import transactionHelper from '../transaction.helper';
import awardRepository from '../../award/award.repository';
import transactionProducer from '../transaction.producer';
import monitoringEventProducer from '../../monitoringEvent/monitoringEvent.producer';
import virtualAccountRepository from '../../virtualAccount/virtualAccount.repository';
import transactionRepository from '../transaction.repository';
import transactionService from '../transaction.service';
import configurationRepository from '../../configuration/configuration.repository';
import transactionSubmissionSwitchingTemplate from '../transactionSubmissionSwitchingTemplate.service';
import { generateOutgoingId } from '../../common/externalIdGenerator.util';
import decisionEngineService from '../../decisionEngine/decisionEngine.service';
import { getDecisionEngineConfiguration } from '../../configuration/__mocks__/configuration.data';
import customerService from '../../customer/customer.service';
import transactionExecutionService from '../transactionExecution.service';
import recommendedCodeService from '../transactionRecommend.service';
import { BeneficiaryProxyType } from '../../bifast/bifast.enum';
import {
  SCHEDULED_RESOLVE_STATUS_FETCH_RECORD_LIMIT,
  SCHEDULED_RESOLVE_STATUS_MAX_TRANSACTION_AGE_IN_MINUTES,
  SCHEDULED_RESOLVE_STATUS_MIN_TRANSACTION_AGE_IN_MINUTES,
  SCHEDULED_RESOLVE_STATUS_TRANSACTION_CODES
} from '../transaction.constant';
import qrisService from '../../alto/alto.service';
import { QRIS_PAYMENT_SERVICE_CODE } from '../../alto/alto.constant';
import faker from 'faker';
import { MonitoringEventType } from '@dk/module-message';

jest.mock('../../account/account.cache');
jest.mock('../../configuration/configuration.repository');
jest.mock('../../logger');
jest.mock('../../coreBanking/deposit.repository');
jest.mock('../../award/award.repository');
jest.mock('../transaction.repository');
jest.mock('../../account/account.repository');
jest.mock('../../transaction/transaction.producer');
jest.mock('../../monitoringEvent/monitoringEvent.producer');
jest.mock('../../virtualAccount/virtualAccount.repository');
jest.mock('../transactionSwitching.helper');
jest.mock('../../common/externalIdGenerator.util');
jest.mock('../../customer/customer.service');
jest.mock('../../alto/alto.service');
jest.mock('../transaction.helper', () => {
  const original = require.requireActual('../transaction.helper').default;
  return {
    ...original,
    mapAuthenticationRequired: jest.fn(),
    mapFeeData: jest.fn(),
    getAccountInfo: jest.fn(),
    getSourceAccount: jest.fn(),
    validateTransactionLimitAmount: jest.fn(),
    getPaymentServiceMappings: jest.fn(),
    validateTransactionListRequest: jest.fn(),
    getStatusesToBeUpdated: jest.fn(),
    getBeneficiaryAccount: jest.fn()
  };
});
jest.mock('../transactionSubmissionSwitchingTemplate.service', () => {
  const original = require.requireActual(
    '../transactionSubmissionSwitchingTemplate.service'
  ).default;
  return {
    ...original,
    submitTransaction: jest.fn()
  };
});
jest.mock('../../decisionEngine/decisionEngine.service.ts');

describe('transactionService', () => {
  const transactionId = 'any_id';
  const {
    sourceBankCode: bankCode,
    sourceAccount,
    beneficiaryAccount,
    transactionCategory,
    ruleConfigs,
    limitGroups,
    usageCounters,
    beneficiaryAccountBifastAccountNo,
    beneficiaryAccountBifastPhoneNo,
    beneficiaryAccountBifastEmail
  } = getTransactionMockValueObject();

  const serviceCodes = ['p01'];

  const transferTransactionInput = getITransferTransactionInput();

  const feeRule = { basicFee: { code: 'f01', feeAmountFixed: 1 } };

  const mockReturnBankCodeOnce = (mockedBankCode: IBankCode) => {
    (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValueOnce(
      mockedBankCode
    );
  };
  const mockReturnDefaultInternalBankCode = () => {
    (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValue(
      {
        ...bankCode,
        isInternal: true
      }
    );
  };

  beforeEach(() => {
    (configurationRepository.getTransactionCategory as jest.Mock).mockResolvedValue(
      transactionCategory
    );

    mockReturnDefaultInternalBankCode();

    (transactionHelper.mapAuthenticationRequired as jest.Mock).mockImplementation(
      value => value
    );

    (transactionHelper.mapFeeData as jest.Mock).mockImplementation(
      value => value
    );

    (transactionHelper.getAccountInfo as jest.Mock).mockImplementation(
      (accountNo, _, __, ___, ____) => {
        if (accountNo === sourceAccount.accountNumber) {
          return sourceAccount;
        }
        if (accountNo === beneficiaryAccount.accountNumber) {
          return beneficiaryAccount;
        }
        return null;
      }
    );
    (transactionHelper.getBeneficiaryAccount as jest.Mock).mockImplementation(
      (accountNo, _, __, ___, ____) => {
        if (accountNo === sourceAccount.accountNumber) {
          return sourceAccount;
        }
        if (accountNo === beneficiaryAccount.accountNumber) {
          return beneficiaryAccount;
        } else if (
          accountNo === beneficiaryAccountBifastAccountNo.accountNumber
        ) {
          return beneficiaryAccountBifastAccountNo;
        } else if (
          accountNo === beneficiaryAccountBifastPhoneNo.accountNumber
        ) {
          return beneficiaryAccountBifastPhoneNo;
        } else if (accountNo === beneficiaryAccountBifastEmail.accountNumber) {
          return beneficiaryAccountBifastEmail;
        } else if (accountNo === 'dummy@fail.com') {
          return {
            ...beneficiaryAccountBifastEmail,
            beneficiaryProxyType: '00'
          };
        }

        return null;
      }
    );

    (transactionHelper.getSourceAccount as jest.Mock).mockImplementation(
      (accountNo, _, __, ___) => {
        if (accountNo === sourceAccount.accountNumber) {
          return sourceAccount;
        }
        if (accountNo === beneficiaryAccount.accountNumber) {
          return beneficiaryAccount;
        }
        return null;
      }
    );

    (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValue(
      serviceCodes
    );
    (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValue(
      ruleConfigs
    );
    (configurationRepository.getLimitGroupsByCodes as jest.Mock).mockResolvedValue(
      limitGroups
    );
    (configurationRepository.getFeeRule as jest.Mock).mockResolvedValue(
      feeRule
    );
    (awardRepository.getUsageCounters as jest.Mock).mockResolvedValue(
      usageCounters
    );
    (monitoringEventProducer.sendMonitoringEventMessage as jest.Mock).mockResolvedValue(
      {}
    );

    // setup default transaction repository
    (transactionRepository.create as jest.Mock).mockImplementation(model => {
      return {
        ...model,
        id: transactionId,
        createdAt: new Date()
      };
    });

    (transactionRepository.update as jest.Mock).mockImplementation(
      (id, model) => {
        return {
          ...model,
          id: transactionId
        };
      }
    );

    (transactionRepository.findByIdAndUpdate as jest.Mock).mockImplementation(
      (id, model) => ({
        ...model,
        id: id
      })
    );

    (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValue({
      ...getTransactionModelFullData(),
      createdAt: new Date('01 June 2020 00:00:00')
    });

    // setup default core banking api
    (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValueOnce(
      999999999
    );

    (depositRepository.makeDeposit as jest.Mock).mockResolvedValue({
      id: 'deposit_id'
    });

    (depositRepository.makeWithdrawal as jest.Mock).mockResolvedValue({
      id: 'withdrawal_id'
    });

    // setup default usage counter
    (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValue({});

    (decisionEngineService.runMultipleRoles as jest.Mock).mockResolvedValue(
      true
    );
    (configurationRepository.getDecisionEngineConfig as jest.Mock).mockResolvedValue(
      getDecisionEngineConfiguration()
    );
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('execute transaction', () => {
    const fee: IFeeAmount = {
      feeCode: 'feeCode',
      feeAmount: 1,
      customerTc: 'customerTc',
      subsidiaryAmount: 1,
      customerTcChannel: 'customerTcChannel',
      creditGlNumber: 'creditGlNumber',
      debitGlNumber: 'debitGlNumber',
      subsidiary: true
    };

    const mockUpdateTransactionStatusReturnNullOnce = () => {
      (transactionRepository.update as jest.Mock)
        .mockImplementationOnce((_, model) => {
          return {
            ...model,
            id: transactionId
          };
        })
        .mockResolvedValueOnce(null);
    };

    it('should throw error if source balance cannot pay fee', async () => {
      // Given
      const transactionInput: ITransferTransactionInput = getITransferTransactionInput();
      const feeData = [{ feeAmount: 99999999999 }];

      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData,
          paymentServiceCode: transactionInput.paymentServiceCode
        }
      ]);

      (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValueOnce(
        transactionInput.transactionAmount
      );
      (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValueOnce(
        {
          ...bankCode,
          isInternal: true
        }
      );

      const findByIdAndUpdateMock = transactionRepository.findByIdAndUpdate as jest.Mock;

      // When
      const error = await transactionService
        .createTransferTransaction(transactionInput)
        .catch(error => error);

      // Then
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Insufficient account balance.');
      expect(error.errorCode).toEqual(
        ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
      );

      const transferUpdateCall = findByIdAndUpdateMock.mock.calls[0];

      expect(transferUpdateCall).toBeDefined();
      expect(transferUpdateCall[0]).toBe(transactionId);
      expect(transferUpdateCall[1]).toBeDefined();

      const failureReason = transferUpdateCall[1].failureReason;

      expect(failureReason).toBeDefined();
      expect(failureReason.actor).toBe(TransferFailureReasonActor.SUBMITTER);
      expect(failureReason.detail).toBe('Insufficient account balance.');
      expect(failureReason.type).toBe(
        ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
      );
    });

    it('should throw error of Insufficient balance & not send notif when RDN transaction', async () => {
      // Given
      const transactionInput: ITransferTransactionInput = {
        ...getITransferTransactionInput(),
        paymentServiceType: PaymentServiceTypeEnum.RDN,
        paymentServiceCode: 'RDN_BUY'
      };
      const feeData = [{ feeAmount: 99999999999 }];

      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData,
          paymentServiceCode: transactionInput.paymentServiceCode
        }
      ]);

      (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValueOnce(
        transactionInput.transactionAmount
      );
      (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValueOnce(
        {
          ...bankCode,
          isInternal: true
        }
      );

      const findByIdAndUpdateMock = transactionRepository.findByIdAndUpdate as jest.Mock;

      // When
      const error = await transactionService
        .createTransferTransaction(transactionInput)
        .catch(e => e);

      // Then
      expect(error.errorCode).toEqual(
        ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
      );
      expect(
        transactionProducer.sendFailedTransferNotification
      ).not.toHaveBeenCalled();

      const transferUpdateCall = findByIdAndUpdateMock.mock.calls[0];

      expect(transferUpdateCall).toBeDefined();
      expect(transferUpdateCall[0]).toBe(transactionId);
      expect(transferUpdateCall[1]).toBeDefined();

      const failureReason = transferUpdateCall[1].failureReason;

      expect(failureReason).toBeDefined();
      expect(failureReason.actor).toBe(TransferFailureReasonActor.SUBMITTER);
      expect(failureReason.detail).toBe('Insufficient account balance.');
      expect(failureReason.type).toBe(
        ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
      );
    });

    it('should throw error when recommendationServiceCode is not found', async () => {
      // Given
      const transactionInput = {
        ...getITransferTransactionInput(),
        paymentServiceCode: 'notExisted'
      };

      (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValueOnce(
        {
          ...bankCode,
          isInternal: true
        }
      );

      const findByIdAndUpdateMock = transactionRepository.findByIdAndUpdate as jest.Mock;

      // When
      const error = await transactionService
        .createTransferTransaction(transactionInput)
        .catch(e => e);

      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INCONSISTENT_PAYMENT_SERVICE);
      expect(error.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
      expect(error.detail).toEqual('No recommended service found!');

      const transferUpdateCall = findByIdAndUpdateMock.mock.calls[0];

      expect(transferUpdateCall).toBeDefined();
      expect(transferUpdateCall[0]).toBe(transactionId);
      expect(transferUpdateCall[1]).toBeDefined();

      const failureReason = transferUpdateCall[1].failureReason;

      expect(failureReason).toBeDefined();
      expect(failureReason.actor).toBe(TransferFailureReasonActor.MS_TRANSFER);
      expect(failureReason.detail).toBe('No recommended service found!');
      expect(failureReason.type).toBe(ERROR_CODE.INCONSISTENT_PAYMENT_SERVICE);
    });

    it('should return created transaction with transaction ids from core banking', async () => {
      // Given
      const paymentServiceCode = 'p01';
      const transactionInput = getITransferTransactionInput(paymentServiceCode);
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        { ...getRecommendedService(), feeData: [fee], paymentServiceCode }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toEqual(
        expect.objectContaining({
          ...transactionInput,
          status: TransactionStatus.SUCCEED,
          id: transactionId
        })
      );

      expect(transactionRepository.create).toBeCalledWith(
        expect.objectContaining(transactionInput)
      );

      expect(transactionRepository.update).toHaveBeenNthCalledWith(
        1,
        transactionId,
        expect.objectContaining({
          ...transactionInput,
          status: TransactionStatus.PENDING,
          destinationCIF: transactionInput.beneficiaryCIF
        })
      );
    });

    it('should return created transaction when using shared account', async () => {
      // Given
      const paymentServiceCode = 'p01';
      const executorCIF = 'sCIF';
      const ownerSourceCIF = 'rSCIF';
      const beneficiaryCIF = 'bCIF';
      const ownerBeneficiaryCIF = 'rBCIF';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        sourceCIF: executorCIF,
        beneficiaryCIF
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        { ...getRecommendedService(), feeData: [fee], paymentServiceCode }
      ]);

      (transactionHelper.getSourceAccount as jest.Mock).mockResolvedValueOnce({
        ...sourceAccount,
        cif: ownerSourceCIF,
        moverRemainingDailyUsage: 10
      });

      (transactionHelper.getAccountInfo as jest.Mock).mockResolvedValueOnce({
        ...beneficiaryAccount,
        cif: ownerBeneficiaryCIF
      });

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toEqual(
        expect.objectContaining({
          ...transactionInput,
          status: TransactionStatus.SUCCEED,
          id: transactionId,
          sourceCIF: ownerSourceCIF,
          beneficiaryCIF: '1',
          destinationCIF: beneficiaryCIF,
          moverRemainingDailyUsage: 10
        })
      );

      expect(transactionRepository.create).toBeCalledWith(
        expect.objectContaining(transactionInput)
      );

      expect(transactionRepository.update).toHaveBeenNthCalledWith(
        1,
        transactionId,
        expect.objectContaining({
          ...transactionInput,
          status: TransactionStatus.PENDING,
          sourceCIF: ownerSourceCIF,
          beneficiaryCIF: '1',
          destinationCIF: beneficiaryCIF,
          moverRemainingDailyUsage: 10
        })
      );
    });

    it('should submit transaction with OFFER paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'OFFER';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: undefined,
        paymentServiceType: PaymentServiceTypeEnum.OFFER,
        paymentServiceCode: 'OFFER'
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [fee],
          paymentServiceCode,
          debitTransactionCode: undefined
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        false
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        true
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should submit transaction with BONUS_INTEREST paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'BONUS_INTEREST';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: undefined,
        paymentServiceType: PaymentServiceTypeEnum.BONUS_INTEREST,
        paymentServiceCode: 'BONUS_INTEREST'
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [fee],
          paymentServiceCode,
          debitTransactionCode: undefined
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        false
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        true
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should submit transaction with BRANCH_DEPOSIT paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'BRANCH_DEPOSIT';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.BRANCH_DEPOSIT
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [],
          paymentServiceCode,
          debitTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        false
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        true
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should submit transaction with CARD_OPENING_FEE paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'CARD_OPENING_FEE';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.CARD_OPENING
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [],
          paymentServiceCode,
          debitTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        true
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        false
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should submit transaction with BRANCH_WITHDRAWAL paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'BRANCH_WITHDRAWAL';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.BRANCH_WITHDRAWAL
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [],
          paymentServiceCode,
          debitTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        true
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        false
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should submit transaction with TRANSFER paymentServiceType and RTGS paymentServiceCode', async () => {
      // Given
      const paymentServiceCode = 'RTGS';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [],
          paymentServiceCode,
          debitTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        true
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        0,
        transactionInput.sourceAccountNo,
        true
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should submit transaction with TRANSFER paymentServiceType and SKN paymentServiceCode', async () => {
      // Given
      const paymentServiceCode = 'SKN';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [],
          paymentServiceCode,
          debitTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        true
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        0,
        transactionInput.sourceAccountNo,
        true
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should submit transaction with TD_PENALTY paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'SAT07';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.TD_PENALTY
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [],
          paymentServiceCode,
          creditTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        true
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        false
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it("should throw error when TD_PENALTY doesn't have sourceAccountNo", async () => {
      // Given
      const paymentServiceCode = 'SAT07';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.TD_PENALTY,
        sourceAccountNo: undefined
      };

      (transactionHelper.getSourceAccount as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test failure!',
          TransferFailureReasonActor.MS_TRANSFER,
          ERROR_CODE.INVALID_REQUEST
        )
      );

      // When
      const error = await transactionService
        .createTransferTransaction(transactionInput)
        .catch(e => e);

      // Then
      expect(error).toBeDefined();
      expect(error.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
      expect(error.detail).toEqual('Test failure!');
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
    });

    it('should submit transaction with PAYROLL paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'PAYROLL';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.PAYROLL,
        paymentServiceCode: 'PAYROLL',
        interchange: BankNetworkEnum.JAGO
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [],
          paymentServiceCode,
          debitTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        false
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        true
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should submit transaction with CASHBACK paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'CASHBACK';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.CASHBACK,
        paymentServiceCode: 'CASHBACK'
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          paymentServiceCode,
          debitTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        false
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        true
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should submit transaction with LOAN_DISBURSEMENT paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'DISBURSEMENT';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.LOAN_DISBURSEMENT
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [],
          paymentServiceCode,
          debitTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        false
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        true
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should submit transaction with LOAN_REFUND paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'LOAN_PROCESSOR_CLAWBACK';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.LOAN_REFUND
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [],
          paymentServiceCode,
          debitTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        false
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        true
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should throw error for OFFER paymentServiceType when bank account info not found', async () => {
      // Given
      const paymentServiceCode = 'OFFER';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: undefined,
        paymentServiceType: PaymentServiceTypeEnum.OFFER,
        paymentServiceCode: 'OFFER',
        beneficiaryAccountNo: undefined
      };

      mockReturnDefaultInternalBankCode();
      (transactionHelper.getSourceAccount as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test failure!',
          TransferFailureReasonActor.MS_TRANSFER,
          ERROR_CODE.INVALID_REQUEST
        )
      );

      // When
      const error = await transactionService
        .createTransferTransaction(transactionInput)
        .catch(err => err);

      // Then
      expect(error).toBeDefined();
      expect(error.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
      expect(error.detail).toEqual('Test failure!');
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
    });

    it('should throw error for BONUS_INTEREST paymentServiceType when bank account info not found', async () => {
      // Given
      const paymentServiceCode = 'BONUS_INTEREST';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: undefined,
        paymentServiceType: PaymentServiceTypeEnum.BONUS_INTEREST,
        paymentServiceCode: 'BONUS_INTEREST',
        beneficiaryAccountNo: undefined
      };

      mockReturnDefaultInternalBankCode();
      (transactionHelper.getSourceAccount as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test failure!',
          TransferFailureReasonActor.MS_TRANSFER,
          ERROR_CODE.INVALID_REQUEST
        )
      );

      // When
      const error = await transactionService
        .createTransferTransaction(transactionInput)
        .catch(err => err);

      // Then
      expect(error).toBeDefined();
      expect(error.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
      expect(error.detail).toEqual('Test failure!');
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
    });

    it('should throw error with CASHBACK paymentServiceType when bank account info not found', async () => {
      // Given
      const paymentServiceCode = 'CASHBACK';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: undefined,
        paymentServiceType: PaymentServiceTypeEnum.CASHBACK,
        beneficiaryAccountNo: undefined
      };
      mockReturnDefaultInternalBankCode();
      (transactionHelper.getSourceAccount as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test failure!',
          TransferFailureReasonActor.MS_TRANSFER,
          ERROR_CODE.INVALID_REQUEST
        )
      );

      // When
      const error = await transactionService
        .createTransferTransaction(transactionInput)
        .catch(err => err);

      // Then
      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
    });

    it('should throw error with PAYROLL paymentServiceType when interchange not provided', async () => {
      // Given
      const paymentServiceCode = 'PAYROLL';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: undefined,
        paymentServiceType: PaymentServiceTypeEnum.PAYROLL,
        beneficiaryAccountNo: undefined
      };

      // When
      const error = await transactionService
        .createTransferTransaction(transactionInput)
        .catch(err => err);

      // Then
      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(ERROR_CODE.PAYROLL_INTERCHANGE_REQUIRED);
    });

    it('should throw error with PAYROLL paymentServiceType when bank account info not found', async () => {
      // Given
      const paymentServiceCode = 'PAYROLL';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: undefined,
        paymentServiceType: PaymentServiceTypeEnum.PAYROLL,
        paymentServiceCode: 'PAYROLL',
        beneficiaryAccountNo: undefined,
        interchange: BankNetworkEnum.JAGO
      };
      (transactionHelper.getSourceAccount as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test failure!',
          TransferFailureReasonActor.MS_TRANSFER,
          ERROR_CODE.INVALID_REQUEST
        )
      );

      // When
      const error = await transactionService
        .createTransferTransaction(transactionInput)
        .catch(err => err);

      // Then
      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
    });

    it('should throw error with LOAN_DISBURSEMENT paymentServiceType when bank account info not found', async () => {
      // Given
      const paymentServiceCode = 'DISBURSEMENT';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: undefined,
        paymentServiceType: PaymentServiceTypeEnum.LOAN_DISBURSEMENT,
        paymentServiceCode: 'DISBURSEMENT',
        beneficiaryAccountNo: undefined,
        interchange: BankNetworkEnum.JAGO
      };

      (transactionHelper.getSourceAccount as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.INVALID_REQUEST
        )
      );

      const error = await transactionService
        .createTransferTransaction(transactionInput)
        .catch(error => error);

      expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      expect(error.detail).toEqual('Test error!');
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
    });

    it('should submit transaction with SETTLEMENT paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'SETTLEMENT';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.SETTLEMENT
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [],
          paymentServiceCode,
          debitTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        false
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        true
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should submit transaction with MAIN_POCKET_TO_GL_TRANSFER paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'MAIN_POCKET_TO_GL_TRANSFER';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.MAIN_POCKET_TO_GL_TRANSFER
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [],
          paymentServiceCode,
          debitTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      let originalExecTransFn = transactionExecutionService.executeTransaction;
      transactionExecutionService.executeTransaction = jest
        .fn()
        .mockResolvedValue(transactionInput);
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );
      transactionExecutionService.executeTransaction = originalExecTransFn;

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        true
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        false
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should submit transaction with TD_SHARIA_PLACEMENT paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'TD_SHARIA_PLACEMENT';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.TD_SHARIA_PLACEMENT
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [],
          paymentServiceCode,
          debitTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        true
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        false
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should submit transaction with TD_SHARIA_REPAYMENT paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'TD_SHARIA_REPAYMENT';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.TD_SHARIA_REPAYMENT
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [],
          paymentServiceCode,
          debitTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        false
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        true
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should submit transaction with BONUS_INTEREST paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'BONUS_INTEREST';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.BONUS_INTEREST
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [],
          paymentServiceCode,
          debitTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        false
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        true
      );
      expect(transactionRepository.create).toBeCalled();
    });

    it('should submit transaction with TD_MANUAL_PAYMENT_GL paymentServiceType', async () => {
      // Given
      const paymentServiceCode = 'TD_MANUAL_PAYMENT_GL';
      const transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        debitTransactionCode: [],
        paymentServiceType: PaymentServiceTypeEnum.TD_MANUAL_PAYMENT_GL
      };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData: [],
          paymentServiceCode,
          debitTransactionCode: []
        }
      ]);

      (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      mockReturnDefaultInternalBankCode();

      // When
      const result = await transactionService.createTransferTransaction(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
        transactionInput.sourceAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        false
      );
      expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
        transactionInput.beneficiaryAccountNo,
        transactionInput.sourceCIF,
        bankCode,
        undefined,
        transactionInput.transactionAmount,
        transactionInput.sourceAccountNo,
        true
      );
      expect(transactionRepository.create).toBeCalled();
    });

    describe('populateTransactionInfo for transactionAmount', () => {
      it('should send transactionAmount to 0 Correctly with PaymentServiceType RTGS', async () => {
        // Given
        const paymentServiceCode = 'RTGS';
        const transactionInput = {
          ...getITransferTransactionInput(paymentServiceCode),
          debitTransactionCode: [],
          paymentServiceType: PaymentServiceTypeEnum.TRANSFER
        };
        const result = await transactionService.populateTransactionInfo(
          {
            ...transactionInput,
            transactionId: '1234'
          },
          () => true,
          () => true
        );
        expect(result).toBeDefined();
        expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
          '1',
          '2',
          {
            bankCodeId: 'BC002',
            name: 'GIN',
            rtolCode: '542',
            remittanceCode: 'ATOSIDJ1',
            isBersamaMember: false,
            isAltoMember: false,
            firstPriority: 'INTERNAL',
            channel: 'INTERNAL',
            companyName: 'PT. GIN INDONESIA',
            type: 'BANK',
            isInternal: true
          },
          undefined,
          0,
          '2',
          true
        );
      });

      it('should send transactionAmount to 1000 Correctly with PaymentServiceType DISBURSEMENT_TO_GL', async () => {
        // Given
        const paymentServiceCode = 'DISBURSEMENT_TO_GL';
        const transactionInput = {
          ...getITransferTransactionInput(paymentServiceCode),
          debitTransactionCode: [],
          paymentServiceType: PaymentServiceTypeEnum.LOAN_GL_DISBURSEMENT,
          transactionAmount: 1000
        };
        const result = await transactionService.populateTransactionInfo(
          {
            ...transactionInput,
            transactionId: '1234'
          },
          () => true,
          () => false
        );
        expect(result).toBeDefined();
        expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
          '1',
          '2',
          {
            bankCodeId: 'BC002',
            name: 'GIN',
            rtolCode: '542',
            remittanceCode: 'ATOSIDJ1',
            isBersamaMember: false,
            isAltoMember: false,
            firstPriority: 'INTERNAL',
            channel: 'INTERNAL',
            companyName: 'PT. GIN INDONESIA',
            type: 'BANK',
            isInternal: true
          },
          undefined,
          1000,
          '2',
          true
        );
      });

      it('should send transactionAmount to 0 Correctly with PaymentServiceType BRANCH_RTGS', async () => {
        // Given
        const paymentServiceCode = 'BRANCH_RTGS';
        const transactionInput = {
          ...getITransferTransactionInput(paymentServiceCode),
          debitTransactionCode: [],
          paymentServiceType: PaymentServiceTypeEnum.TRANSFER
        };
        const result = await transactionService.populateTransactionInfo(
          {
            ...transactionInput,
            transactionId: '1234'
          },
          () => true,
          () => true
        );
        expect(result).toBeDefined();
        expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
          '1',
          '2',
          {
            bankCodeId: 'BC002',
            name: 'GIN',
            rtolCode: '542',
            remittanceCode: 'ATOSIDJ1',
            isBersamaMember: false,
            isAltoMember: false,
            firstPriority: 'INTERNAL',
            channel: 'INTERNAL',
            companyName: 'PT. GIN INDONESIA',
            type: 'BANK',
            isInternal: true
          },
          undefined,
          0,
          '2',
          true
        );
      });

      it('should send transactionAmount not 0, with PaymentServiceType RTOL', async () => {
        // Given
        const paymentServiceCode = 'RTOL';
        const transactionInput = {
          ...getITransferTransactionInput(paymentServiceCode),
          debitTransactionCode: [],
          paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
          transactionAmount: 10000
        };
        const result = await transactionService.populateTransactionInfo(
          {
            ...transactionInput,
            transactionId: '1234'
          },
          () => true,
          () => true
        );
        expect(result).toBeDefined();
        expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
          '1',
          '2',
          {
            bankCodeId: 'BC002',
            name: 'GIN',
            rtolCode: '542',
            remittanceCode: 'ATOSIDJ1',
            isBersamaMember: false,
            isAltoMember: false,
            firstPriority: 'INTERNAL',
            channel: 'INTERNAL',
            companyName: 'PT. GIN INDONESIA',
            type: 'BANK',
            isInternal: true
          },
          undefined,
          10000,
          '2',
          true
        );
      });

      it('should populate correct transaction info with PaymentServiceCode BIFAST_OUTGOING and using proxy as beneficiaryAccountNo', async () => {
        // Given
        const paymentServiceCode = 'BIFAST_OUTGOING';
        const transactionInput = {
          ...getITransferTransactionInput(paymentServiceCode),
          debitTransactionCode: [],
          beneficiaryAccountNo: '100100100',
          paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
          additionalInformation1: '1',
          additionalInformation2: '1',
          additionalInformation3: '01',
          additionalInformation4: '+628112233'
        };

        // When
        const result = await transactionService.populateTransactionInfo({
          ...transactionInput,
          transactionId: '1234'
        });

        // Then
        expect(result).toBeDefined();
        expect(result.additionalInformation3).toEqual('01');
        expect(result.additionalInformation4).toEqual('+628112233');
        expect(result.beneficiaryAccountType).toEqual('SVGS');
      });

      it('should change beneficiary BankCodeId to BC000 (required as BankCode when do account inquiry) when using email address as beneficiaryAccountNo', async () => {
        // Given
        const paymentServiceCode = 'BIFAST_OUTGOING';
        const transactionInput = {
          ...getITransferTransactionInput(paymentServiceCode),
          debitTransactionCode: [],
          beneficiaryAccountNo: '100100100',
          paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
          additionalInformation1: '1',
          additionalInformation2: '1',
          additionalInformation3: '02',
          additionalInformation4: 'test@test.com'
        };

        // When
        const result = await transactionService.populateTransactionInfo({
          ...transactionInput,
          transactionId: '1234'
        });

        // Then
        expect(result).toBeDefined();
        expect(result.beneficiaryBankCodeInfo?.bankCodeId).toEqual('BC000');
      });

      it('should not change beneficiary BankCodeId to BC000 (required as BankCode when do account inquiry) when using real accountNo as beneficiaryAccountNo', async () => {
        // Given
        const paymentServiceCode = 'BIFAST_OUTGOING';
        const transactionInput = {
          ...getITransferTransactionInput(paymentServiceCode),
          debitTransactionCode: [],
          beneficiaryAccountNo: '100100100',
          paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
          additionalInformation1: '1',
          additionalInformation2: '1',
          additionalInformation3: '00'
        };

        // When
        const result = await transactionService.populateTransactionInfo({
          ...transactionInput,
          transactionId: '1234'
        });

        // Then
        expect(result).toBeDefined();
        expect(result.beneficiaryBankCodeInfo?.bankCodeId).not.toEqual('BC000');
      });

      it('should throw error when input proxyType does not match with account proxyType ', async () => {
        // Given
        const paymentServiceCode = 'BIFAST_OUTGOING';
        const transactionInput = {
          ...getITransferTransactionInput(paymentServiceCode),
          debitTransactionCode: [],
          beneficiaryAccountNo: '100100100',
          paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
          additionalInformation1: '1',
          additionalInformation2: '1',
          additionalInformation3: '02',
          additionalInformation4: 'dummy@fail.com'
        };

        // When
        const error = await transactionService
          .populateTransactionInfo({
            ...transactionInput,
            transactionId: '1234'
          })
          .catch(err => err);

        // Then
        expect(error).toBeDefined();
        expect(error.errorCode).toEqual(
          ERROR_CODE.BENEFICIARY_ACCOUNT_INQUIRY_FAILED
        );
      });

      it('should throw error and send monitoring data when transactionHelper.getSourceAccount throws error ', async () => {
        // Given
        const transactionInput = {
          ...getITransferTransactionInput(),
          transactionId: '1234'
        };
        (transactionHelper.getSourceAccount as jest.Mock).mockRejectedValueOnce(
          new TransferAppError(
            'Test error!',
            TransferFailureReasonActor.UNKNOWN,
            ERROR_CODE.SOURCE_ACCOUNT_INQUIRY_FAILED
          )
        );

        // When
        const error = await transactionService
          .populateTransactionInfo(transactionInput)
          .catch(error => error);

        // Then
        expect(error).toBeDefined();
        expect(error.errorCode).toEqual(
          ERROR_CODE.SOURCE_ACCOUNT_INQUIRY_FAILED
        );
        expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
        expect(error.detail).toEqual('Test error!');
        expect(transactionHelper.getSourceAccount).toHaveBeenCalled();
        expect(
          monitoringEventProducer.sendMonitoringEventMessage
        ).toHaveBeenCalledWith(
          MonitoringEventType.PROCESSING_FINISHED,
          expect.anything()
        );
      });

      // To-do : Uncomment when Komi removes hardcoded bankCode from account inquiry
      // it('should throw error when input bankCode does not match with account bankCode', async () => {
      //   // Given
      //   const paymentServiceCode = 'BIFAST_OUTGOING';
      //   const transactionInput = {
      //     ...getITransferTransactionInput(paymentServiceCode),
      //     debitTransactionCode: [],
      //     beneficiaryAccountNo: '100100100',
      //     beneficiaryBankCode: 'BC065',
      //     paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
      //     additionalInformation1: '1',
      //     additionalInformation2: '1',
      //     additionalInformation3: '02',
      //     additionalInformation4: beneficiaryAccountBifastEmail.accountNumber
      //   };

      //   // When
      //   const error = await transactionService
      //     .populateTransactionInfo({
      //       ...transactionInput,
      //       transactionId: '1234'
      //     })
      //     .catch(err => err);

      //   // Then
      //   expect(error).toBeDefined();
      //   expect(error.errorCode).toEqual(
      //     ERROR_CODE.BENEFICIARY_ACCOUNT_INQUIRY_FAILED
      //   );
      // });

      it('should re-populating actual bankCode information when using proxy as beneficiaryAccountNo', async () => {
        // Given
        const paymentServiceCode = 'BIFAST_OUTGOING';
        const transactionInput = {
          ...getITransferTransactionInput(paymentServiceCode),
          debitTransactionCode: [],
          beneficiaryAccountNo: '100100100',
          paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
          additionalInformation1: '1',
          additionalInformation2: '1',
          additionalInformation3: '00',
          beneficiaryBankCode: 'BC000'
        };

        // When
        const result = await transactionService.populateTransactionInfo({
          ...transactionInput,
          transactionId: '1234'
        });

        // Then
        expect(result).toBeDefined();
        expect(result.beneficiaryBankCodeInfo?.bankCodeId).toEqual('BC002');
      });

      it('should skip source account inquiry if paymentServiceType is MIGRATION_TRANSFER', async () => {
        const paymentServiceCode = 'SIT04';
        const transactionInput = {
          ...getITransferTransactionInput(paymentServiceCode),
          debitTransactionCode: [],
          beneficiaryAccountNo: '100100100',
          paymentServiceType: PaymentServiceTypeEnum.MIGRATION_TRANSFER,
          additionalInformation1: '1',
          additionalInformation2: '1',
          additionalInformation3: '00',
          beneficiaryBankCode: 'BC000'
        };

        (transactionHelper.getSourceAccount as jest.Mock).mockImplementation(
          (accountNo, _, bankCode, accountName, isInquiryApplicable) => {
            if (!isInquiryApplicable) {
              return {
                accountName: accountName,
                bankCode: bankCode,
                accountNumber: accountNo,
                role: 'NONE'
              };
            }
            return null;
          }
        );

        const result = await transactionService.populateTransactionInfo(
          {
            ...transactionInput,
            transactionId: '1234'
          },
          (_, __) => true,
          paymentServiceType =>
            paymentServiceType !== PaymentServiceTypeEnum.MIGRATION_TRANSFER
        );

        expect(result).toBeDefined();
        expect(result.beneficiaryBankCodeInfo?.bankCodeId).toEqual('BC002');
        expect(transactionHelper.getSourceAccount).toHaveBeenCalledWith(
          transactionInput.sourceAccountNo,
          transactionInput.sourceCIF,
          {
            bankCodeId: 'BC002',
            channel: 'INTERNAL',
            companyName: 'PT. GIN INDONESIA',
            firstPriority: 'INTERNAL',
            isAltoMember: false,
            isBersamaMember: false,
            isInternal: true,
            name: 'GIN',
            prefix: undefined,
            remittanceCode: 'ATOSIDJ1',
            rtolCode: '542',
            type: 'BANK'
          },
          undefined,
          transactionInput.transactionAmount,
          false
        );
      });

      it('should send transactionAmount to 1000 Correctly with PaymentServiceType TD_PAYMENT_GL', async () => {
        // Given
        const paymentServiceCode = 'TD_PAYMENT_GL';
        const transactionInput = {
          ...getITransferTransactionInput(paymentServiceCode),
          debitTransactionCode: [],
          paymentServiceType: PaymentServiceTypeEnum.TD_PAYMENT_GL,
          transactionAmount: 1000
        };
        const result = await transactionService.populateTransactionInfo(
          {
            ...transactionInput,
            transactionId: '1234'
          },
          () => true,
          () => false
        );
        expect(result).toBeDefined();
        expect(transactionHelper.getBeneficiaryAccount).toHaveBeenCalledWith(
          '1',
          '2',
          {
            bankCodeId: 'BC002',
            name: 'GIN',
            rtolCode: '542',
            remittanceCode: 'ATOSIDJ1',
            isBersamaMember: false,
            isAltoMember: false,
            firstPriority: 'INTERNAL',
            channel: 'INTERNAL',
            companyName: 'PT. GIN INDONESIA',
            type: 'BANK',
            isInternal: true
          },
          undefined,
          1000,
          '2',
          true
        );
      });
    });

    describe('submit transactions to core banking after saving data to db', () => {
      const paymentServiceCode = 'p01';
      let transactionInput: ITransferTransactionInput;

      beforeEach(() => {
        transactionInput = getITransferTransactionInput();

        (transactionHelper.mapFeeData as jest.Mock).mockResolvedValue([
          {
            ...getRecommendedService(),
            paymentServiceCode
          }
        ]);
      });

      afterEach(() => {
        jest.resetAllMocks();
      });

      describe('createTransferTransaction', () => {
        it('should submit transaction to core banking', async () => {
          // When
          await transactionService.createTransferTransaction(transactionInput);

          // Then
          expect(
            monitoringEventProducer.sendMonitoringEventMessage
          ).toHaveBeenCalledWith(
            MonitoringEventType.PROCESSING_STARTED,
            expect.anything()
          );
          expect(depositRepository.makeWithdrawal).toBeCalled();
          expect(depositRepository.makeDeposit).toBeCalled();
        });

        it('should set status of the transaction in transfer db to success after submitting to core banking successfully', async () => {
          // When
          await transactionService.createTransferTransaction(transactionInput);

          // Then
          expect(transactionRepository.update).toBeCalledWith(
            transactionId,
            expect.objectContaining({
              status: TransactionStatus.SUCCEED
            })
          );

          // message is sent to customer of sourceAccount
          expect(
            transactionProducer.sendTransactionSucceededMessage
          ).toBeCalledWith(
            sourceAccount.customerId,
            expect.objectContaining({
              status: TransactionStatus.SUCCEED
            })
          );
        });

        it('should set status of the transaction in transfer db to decline and produce failed transfer message if submit to core banking failed', async () => {
          // given
          const coreBankingTransaction1 = {
            id: '1',
            adjustmentTransactionKey: undefined
          };
          const failureReason = {
            actor: TransferFailureReasonActor.UNKNOWN,
            detail: 'Error while crediting money, code: undefined, message: !',
            type: ERROR_CODE.COREBANKING_CREDIT_FAILED
          };
          const coreBankingTransaction2 = {
            id: '2',
            adjustmentTransactionKey: '2'
          };
          //let newTransactionModel = getTransactionModelFullData();
          (depositRepository.makeDeposit as jest.Mock).mockRejectedValue(
            new Error()
          );

          // (transactionRepository.create as jest.Mock).mockResolvedValueOnce({
          //   ...newTransactionModel,
          //   createdAt: new Date()
          // });

          // (checkTransferSameCifForMigratingAccounts as jest.Mock).mockResolvedValueOnce(
          //   true
          // );

          (depositRepository.getTransactionsByInstructionId as jest.Mock).mockResolvedValue(
            [coreBankingTransaction1, coreBankingTransaction2]
          );

          (transactionProducer.sendFailedTransferNotification as jest.Mock).mockResolvedValueOnce(
            {}
          );
          // When
          const error = await transactionService
            .createTransferTransaction(transactionInput)
            .catch(err => err);

          // Then
          expect(transactionRepository.update).toBeCalledWith(
            transactionId,
            expect.objectContaining({
              status: TransactionStatus.DECLINED
            })
          );
          expect(error).toBeInstanceOf(Error);
          expect(error.errorCode).toBe(ERROR_CODE.COREBANKING_CREDIT_FAILED);
          expect(
            transactionProducer.sendFailedTransferNotification
          ).toBeCalledWith(
            expect.objectContaining({
              status: TransactionStatus.PENDING,
              paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
              beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL
            })
          );

          expect(
            transactionProducer.sendTransactionFailedMessage
          ).toBeCalledWith(
            sourceAccount.customerId,
            expect.objectContaining({
              status: TransactionStatus.DECLINED
            })
          );
        });

        it('should throw AppError when updating transaction status failed after submitting to core banking', async () => {
          // given
          mockUpdateTransactionStatusReturnNullOnce();

          // When
          const error = await transactionService
            .createTransferTransaction(transactionInput)
            .catch(err => err);

          // Then
          expect(error.errorCode).toEqual(
            ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION_STATUS
          );
        });

        // TODO: will add this back when we know the proper rule for validation
        // as this is only applicable for internal transfer
        // it('should throw error when executing a transfer transaction that is missing debit transaction code', async () => {
        //   // Given
        //   const serviceMappingResult = getPaymentServiceMappingResult(
        //     paymentServiceCode
        //   ).map(result => {
        //     return {
        //       ...result,
        //       debitTransactionCodeInfo: undefined
        //     };
        //   });

        //   (configurationRepository.getPaymentServiceMappingResults as jest.Mock).mockResolvedValue(
        //     serviceMappingResult
        //   );

        //   // when
        //   const error = await transactionService
        //     .createTransferTransaction(transactionInput)
        //     .catch(err => err);

        //   // Then
        //   expect(error.errorCode).toEqual(
        //     ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE
        //   );
        // });

        // it('should throw error when executing a transfer transaction that is missing credit transaction code', async () => {
        //   // Given
        //   const serviceMappingResult = getPaymentServiceMappingResult(
        //     paymentServiceCode
        //   ).map(result => {
        //     return {
        //       ...result,
        //       creditTransactionCodeInfo: undefined
        //     };
        //   });

        //   (configurationRepository.getPaymentServiceMappingResults as jest.Mock).mockResolvedValue(
        //     serviceMappingResult
        //   );

        //   // when
        //   const error = await transactionService
        //     .createTransferTransaction(transactionInput)
        //     .catch(err => err);

        //   // Then
        //   expect(error.errorCode).toEqual(
        //     ERROR_CODE.MISSING_CREDIT_TRANSACTION_CODE
        //   );
        // });
      });

      describe('withdrawTransaction', () => {
        it('should process transaction successfully for valid request', async () => {
          // ARRANGE
          const input: CreateWithdrawTransactionPayload = {
            ...transferTransactionInput,
            beneficiaryAccountNo: beneficiaryAccount.accountNumber,
            beneficiaryBankCode: undefined,
            paymentServiceType: PaymentServiceTypeEnum.MDR
          };

          (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
            {
              ...getRecommendedService(),
              creditTransactionCode: undefined,
              paymentServiceCode: input.paymentServiceCode,
              feeData: undefined
            }
          ]);

          // ACT
          const result = await transactionService.createWithdrawTransaction(
            input
          );

          // ASSERT
          expect(result).toEqual(
            expect.objectContaining({
              status: TransactionStatus.SUCCEED
            })
          );
          expect(transactionRepository.create).toHaveBeenCalledWith(
            expect.objectContaining({
              status: TransactionStatus.PENDING,
              paymentServiceType: PaymentServiceTypeEnum.MDR,
              beneficiaryAccountNo: expect.any(String),
              beneficiaryBankCode: undefined
            })
          );

          expect(depositRepository.makeWithdrawal).toBeCalledTimes(1);
          expect(
            monitoringEventProducer.sendMonitoringEventMessage
          ).toHaveBeenCalledWith(
            MonitoringEventType.PROCESSING_STARTED,
            expect.anything()
          );
        });

        it('should throw error if payment service code includes the credit transaction code', async () => {
          // given
          const input: CreateWithdrawTransactionPayload = {
            ...transferTransactionInput,
            beneficiaryAccountNo: beneficiaryAccount.accountNumber,
            beneficiaryBankCode: undefined,
            paymentServiceType: PaymentServiceTypeEnum.MDR
          };

          (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
            {
              ...getRecommendedService(),
              paymentServiceCode: input.paymentServiceCode,
              feeData: undefined
            }
          ]);

          // when
          const result = await transactionService
            .createWithdrawTransaction(input)
            .catch(error => error);

          // expect
          expect(result.errorCode).toBe(
            ERROR_CODE.MISSING_CREDIT_TRANSACTION_CODE
          );
        });
      });

      describe('depositTransaction', () => {
        it('should process transaction successfully for valid request', async () => {
          //ARRANGE
          const input: CreateDepositTransactionPayload = {
            ...transferTransactionInput,
            sourceAccountNo: sourceAccount.accountNumber,
            sourceAccountName: undefined,
            paymentServiceType: PaymentServiceTypeEnum.CASHBACK
          };

          (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
            {
              ...getRecommendedService(),
              debitTransactionCode: undefined,
              paymentServiceCode: input.paymentServiceCode,
              feeData: undefined
            }
          ]);

          // ACT
          const result = await transactionService.createDepositTransaction(
            input
          );

          // ASSERT
          expect(result).toEqual(
            expect.objectContaining({
              id: transactionId,
              status: TransactionStatus.SUCCEED
            })
          );

          expect(transactionRepository.create).toHaveBeenCalledWith(
            expect.objectContaining({
              status: TransactionStatus.PENDING,
              paymentServiceType: PaymentServiceTypeEnum.CASHBACK
            })
          );

          expect(depositRepository.makeDeposit).toBeCalledTimes(1);
          expect(
            monitoringEventProducer.sendMonitoringEventMessage
          ).toHaveBeenCalledWith(
            MonitoringEventType.PROCESSING_STARTED,
            expect.anything()
          );
        });

        it('should throw error if payment service code includes debit transaction code', async () => {
          // given
          const input: CreateDepositTransactionPayload = {
            ...transferTransactionInput,
            sourceAccountNo: sourceAccount.accountNumber,
            sourceAccountName: sourceAccount.accountName,
            paymentServiceType: PaymentServiceTypeEnum.TRANSFER
          };

          (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
            {
              ...getRecommendedService(),
              paymentServiceCode: input.paymentServiceCode
            }
          ]);

          // when
          const result = await transactionService
            .createDepositTransaction(input)
            .catch(error => error);

          // then
          expect(result.errorCode).toBe(
            ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE
          );
        });

        it('should get flag isInquiryApplicable false for CASHBACK payment service type', async () => {
          // Given
          const input: CreateDepositTransactionPayload = {
            ...transferTransactionInput,
            paymentServiceType: PaymentServiceTypeEnum.CASHBACK
          };

          (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
            {
              ...getRecommendedService(),
              paymentServiceCode: input.paymentServiceCode
            }
          ]);
          //When
          const result = await transactionService
            .createDepositTransaction(input)
            .catch(error => error);

          //Then
          expect(transactionHelper.getSourceAccount).toBeCalledWith(
            input.sourceAccountNo,
            input.sourceCIF,
            expect.any(Object),
            input.sourceAccountName,
            input.transactionAmount,
            false
          );
        });

        it('should get flag isInquiryApplicable true for payment service type other than the whitelisted', async () => {
          // Given
          const input: CreateDepositTransactionPayload = {
            ...transferTransactionInput,
            paymentServiceType: PaymentServiceTypeEnum.TRANSFER
          };

          (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
            {
              ...getRecommendedService(),
              paymentServiceCode: input.paymentServiceCode
            }
          ]);
          // When
          await transactionService
            .createDepositTransaction(input)
            .catch(error => error);

          // Then
          expect(transactionHelper.getSourceAccount).toBeCalledWith(
            input.sourceAccountNo,
            input.sourceCIF,
            expect.any(Object),
            input.sourceAccountName,
            input.transactionAmount,
            true
          );
        });
      });
    });

    describe('createInternalTopupTransaction', () => {
      const mockedWalletPartnerBankCode = getWalletPartnerBankCode();
      const mockedVirtualAccount: IVirtualAccount = {
        customerName: 'test customer name',
        institution: {
          name: 'gojek',
          bankCode: mockedWalletPartnerBankCode.bankCodeId,
          accountId: '2384738473',
          cif: 'insitution-cif'
        }
      };
      const mockReturnVirtualAccount = () => {
        (virtualAccountRepository.inquiryVirtualAccount as jest.Mock).mockResolvedValue(
          mockedVirtualAccount
        );
      };
      const mockReturnVirtualAccountOnce = (mockedAccount: IVirtualAccount) => {
        (virtualAccountRepository.inquiryVirtualAccount as jest.Mock).mockResolvedValueOnce(
          mockedAccount
        );
      };
      const topupTransactionInput: TopupInternalTransactionPayload = {
        sourceCIF: '8a8e86866df7cf8d016df87dae2f0262',
        beneficiaryAccountNo: '1287284734',
        beneficiaryBankCode: mockedWalletPartnerBankCode.bankCodeId,
        transactionAmount: 15400,
        paymentServiceCode: 'SIT02',
        additionalInformation1: 'note 1',
        additionalInformation2: 'note 2',
        additionalInformation3: 'note 3',
        additionalInformation4: 'note 4'
      };
      const accountInfo: IBasicAccountInfo = {
        accountNumber: 'test-number',
        accountType: 'MA',
        currency: 'IDR',
        accountName: 'test',
        aliasName: 'aliasName'
      };
      const mockReturnSourceAccount = () => {
        (transactionHelper.getSourceAccount as jest.Mock).mockResolvedValueOnce(
          {
            ...accountInfo,
            cif: topupTransactionInput.sourceCIF
          }
        );
      };

      const paymentServicesForWallet: RecommendationService[] = [
        {
          paymentServiceCode: 'SIT02',
          debitTransactionCode: [
            {
              transactionCode: 'WTD01',
              transactionCodeInfo: {
                limitGroupCode: 'l01',
                feeRules: 'f01',
                code: 'WTD01',
                minAmountPerTx: 1,
                maxAmountPerTx: 999999,
                channel: BankChannelEnum.INTERNAL
              }
            }
          ],
          creditTransactionCode: [
            {
              transactionCode: 'WTD02',
              transactionCodeInfo: {
                limitGroupCode: 'l01',
                feeRules: 'f01',
                code: 'WTD02',
                minAmountPerTx: 1,
                maxAmountPerTx: 999999,
                channel: BankChannelEnum.INTERNAL
              }
            }
          ],
          transactionAuthenticationChecking: true,
          beneficiaryBankCodeType: 'RTOL',
          requireThirdPartyOutgoingId: false,
          blocking: false
        }
      ];
      const sourceBankCode = bankCode;
      const walletIdAsBeneficiaryAcccountNo = `${topupTransactionInput.beneficiaryAccountNo}`;

      afterEach(() => {
        jest.resetAllMocks();
      });
      describe('Partner Wallet', () => {
        beforeEach(() => {
          mockReturnSourceAccount();
          mockReturnBankCodeOnce(mockedWalletPartnerBankCode);
          mockReturnBankCodeOnce(sourceBankCode);

          mockReturnVirtualAccount();
          (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce(
            paymentServicesForWallet
          );
        });

        afterEach(() => {
          jest.resetAllMocks();
        });

        it('should submit to core banking and persist transaction with valid input for partner wallet', async () => {
          // ACT
          const result = await transactionService.createInternalTopupTransaction(
            topupTransactionInput
          );

          // ASSERT
          expect(result).toEqual(
            expect.objectContaining({
              ...topupTransactionInput,
              status: TransactionStatus.SUCCEED
            })
          );
          expect(
            configurationRepository.getBankCodeMapping
          ).toHaveBeenCalledTimes(2);
          expect(
            virtualAccountRepository.inquiryVirtualAccount
          ).toHaveBeenCalledWith(walletIdAsBeneficiaryAcccountNo);

          expect(
            monitoringEventProducer.sendMonitoringEventMessage
          ).toHaveBeenCalledWith(
            MonitoringEventType.PROCESSING_STARTED,
            expect.anything()
          );
        });

        it('should update transaction status to DECLINED and not produce failed transfer when submit to core banking failed', async () => {
          // Top-up transaction will withdrawl money from customer account
          (depositRepository.makeWithdrawal as jest.Mock).mockRejectedValue({});

          // ACT
          const error = await transactionService
            .createInternalTopupTransaction(topupTransactionInput)
            .catch(err => err);

          // ASSERT
          expect(transactionRepository.update).toBeCalledWith(
            transactionId,
            expect.objectContaining({
              status: TransactionStatus.DECLINED
            })
          );
          expect(error).toBeInstanceOf(Error);
          expect(error.errorCode).toBe(ERROR_CODE.COREBANKING_DEBIT_FAILED);

          expect(
            transactionProducer.sendFailedTransferNotification
          ).not.toBeCalled();

          expect(
            monitoringEventProducer.sendMonitoringEventMessage
          ).toHaveBeenCalledWith(
            MonitoringEventType.PROCESSING_STARTED,
            expect.anything()
          );
        });

        it('should persist categoryCode when it is provided in payload', async () => {
          // ARRANGE
          const input: ITopupInternalTransactionInput = {
            ...topupTransactionInput,
            categoryCode: 'C058'
          };
          // ACT
          const result = await transactionService.createInternalTopupTransaction(
            input
          );

          // ASSERT
          expect(result).toEqual(
            expect.objectContaining({
              categoryCode: input.categoryCode,
              status: TransactionStatus.SUCCEED
            })
          );

          expect(transactionRepository.create).toBeCalledWith(
            expect.objectContaining({
              status: TransactionStatus.PENDING,
              categoryCode: input.categoryCode
            })
          );

          expect(
            monitoringEventProducer.sendMonitoringEventMessage
          ).toHaveBeenCalledWith(
            MonitoringEventType.PROCESSING_STARTED,
            expect.anything()
          );
        });

        it('should throw error if categoryCode is invalid', async () => {
          // given
          const input: ITopupInternalTransactionInput = {
            ...topupTransactionInput,
            categoryCode: 'C058'
          };
          (transactionRepository.findByIdAndUpdate as jest.Mock).mockImplementation(
            model => {
              return {
                ...model,
                sourceCIF: undefined,
                id: transactionId,
                status: TransactionStatus.DECLINED
              };
            }
          );

          (configurationRepository.getTransactionCategory as jest.Mock).mockResolvedValueOnce(
            undefined
          );
          // when
          const error = await transactionService
            .createInternalTopupTransaction(input)
            .catch(error => error);

          // then
          expect(error).toBeInstanceOf(TransferAppError);
          expect(error.errorCode).toEqual(
            ERROR_CODE.INVALID_REGISTER_PARAMETER
          );
          expect(error.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
          expect(error.detail).toEqual('Category code is not available!');
        });

        it('should throw INCONSISTENT_PAYMENT_SERVICE error when recommendedService is not consistent', async () => {
          // given
          const input = {
            ...topupTransactionInput,
            paymentServiceCode: '123'
          };
          (transactionRepository.findByIdAndUpdate as jest.Mock).mockImplementation(
            model => {
              return {
                ...model,
                sourceCIF: undefined,
                id: transactionId,
                status: TransactionStatus.DECLINED
              };
            }
          );

          // when
          const error = await transactionService
            .createInternalTopupTransaction(input)
            .catch(error => error);

          // then
          expect(error).toBeInstanceOf(TransferAppError);
          expect(error.errorCode).toEqual(
            ERROR_CODE.INCONSISTENT_PAYMENT_SERVICE
          );
          expect(error.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
          expect(error.detail).toEqual('No recommended service found!');
        });

        it('should throw app error when failing to retrieve virtual-account', async () => {
          // ARRANGE
          (virtualAccountRepository.inquiryVirtualAccount as jest.Mock).mockRejectedValueOnce(
            new Error()
          );

          // ACT
          const error = await transactionService
            .createInternalTopupTransaction(topupTransactionInput)
            .catch(e => e);

          // ASSERT
          expect(error).toBeInstanceOf(Error);
        });

        it('should throw validation error when institution bank code is different than input beneficiary bank code', async () => {
          const virtualAccount = {
            ...mockedVirtualAccount,
            institution: {
              ...mockedVirtualAccount.institution,
              bankCode: '28734'
            }
          };

          mockReturnVirtualAccountOnce(virtualAccount);

          (transactionRepository.findByIdAndUpdate as jest.Mock).mockImplementation(
            model => {
              return {
                ...model,
                sourceCIF: undefined,
                id: transactionId,
                status: TransactionStatus.DECLINED
              };
            }
          );
          const error = await transactionService
            .createInternalTopupTransaction(topupTransactionInput)
            .catch(e => e);

          expect(error.errorCode).toEqual(
            ERROR_CODE.INVALID_REGISTER_PARAMETER
          );
        });
      });
      describe('Partner Wallet with sourceAccountNo belongs to type is DC', () => {
        const cifOfSourceAccountNo = '123456';
        beforeEach(() => {
          (transactionHelper.getSourceAccount as jest.Mock).mockResolvedValueOnce(
            {
              ...accountInfo,
              cif: cifOfSourceAccountNo
            }
          );
          mockReturnBankCodeOnce(mockedWalletPartnerBankCode);
          mockReturnBankCodeOnce(sourceBankCode);
          mockReturnVirtualAccount();
          (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce(
            paymentServicesForWallet
          );
        });

        it('should make transaction success ', async () => {
          // ACT
          const result = await transactionService.createInternalTopupTransaction(
            topupTransactionInput
          );

          // ASSERT
          expect(result).toEqual(
            expect.objectContaining({
              ...topupTransactionInput,
              sourceCIF: cifOfSourceAccountNo,
              status: TransactionStatus.SUCCEED
            })
          );
          expect(
            configurationRepository.getBankCodeMapping
          ).toHaveBeenCalledTimes(2);
          expect(
            virtualAccountRepository.inquiryVirtualAccount
          ).toHaveBeenCalledWith(walletIdAsBeneficiaryAcccountNo);

          expect(
            monitoringEventProducer.sendMonitoringEventMessage
          ).toHaveBeenCalledWith(
            MonitoringEventType.PROCESSING_STARTED,
            expect.anything()
          );
        });
      });
      describe('External Wallet', () => {
        const serviceMappingForWalletExternal: RecommendationService[] = [
          {
            paymentServiceCode: 'RTOL_WALLET',
            debitTransactionCode: [
              {
                transactionCode: 'WTD02',
                transactionCodeInfo: {
                  limitGroupCode: 'l01',
                  feeRules: 'f01',
                  code: 'WTD02',
                  minAmountPerTx: 1,
                  maxAmountPerTx: 999999,
                  channel: BankChannelEnum.INTERNAL
                }
              }
            ],
            transactionAuthenticationChecking: true,
            beneficiaryBankCodeType: 'RTOL',
            requireThirdPartyOutgoingId: true, //should be true to be able to call switching-submission
            blocking: false
          }
        ];

        const walletExternalBankCode = getWalletExternalBankCode();
        const sourceBankCode = { ...bankCode };
        const topupTransactionInputForExternalWallet: TopupInternalTransactionPayload = {
          ...topupTransactionInput,
          beneficiaryBankCode: walletExternalBankCode.bankCodeId,
          paymentServiceCode: 'RTOL_WALLET'
        };
        const walletIdAsBeneficiaryAcccountNo = `${topupTransactionInputForExternalWallet.beneficiaryAccountNo}`;
        const beneficiaryCif = undefined;

        beforeEach(() => {
          mockReturnSourceAccount();
          mockReturnBankCodeOnce(walletExternalBankCode);
          mockReturnBankCodeOnce(sourceBankCode);

          const outgoingId = 'outgoingId';
          (generateOutgoingId as jest.Mock).mockResolvedValueOnce(outgoingId);
          (transactionSubmissionSwitchingTemplate.submitTransaction as jest.Mock).mockResolvedValueOnce(
            {
              ...topupTransactionInputForExternalWallet
            }
          );
          (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce(
            serviceMappingForWalletExternal
          );
        });

        it('should submit to core banking and persist transaction with valid input for external wallet', async () => {
          // ACT
          const result = await transactionService.createInternalTopupTransaction(
            topupTransactionInputForExternalWallet
          );

          // ASSERT
          expect(result).toEqual(
            expect.objectContaining({
              //...topupTransactionInputForExternalWallet, TODO: need discussion
              status: TransactionStatus.SUBMITTED
            })
          );
          expect(
            virtualAccountRepository.inquiryVirtualAccount
          ).not.toBeCalled();
          expect(
            configurationRepository.getBankCodeMapping
          ).toHaveBeenCalledTimes(2);
          expect(transactionHelper.getAccountInfo).toHaveBeenCalledWith(
            walletIdAsBeneficiaryAcccountNo,
            beneficiaryCif,
            walletExternalBankCode,
            undefined,
            topupTransactionInputForExternalWallet.transactionAmount
          );

          expect(
            monitoringEventProducer.sendMonitoringEventMessage
          ).toHaveBeenCalledWith(
            MonitoringEventType.PROCESSING_STARTED,
            expect.anything()
          );
        });
      });

      it('should throw INVALID REQUEST when missing sourceCif for Wallet Payment Type', async () => {
        // given
        const input = {
          ...topupTransactionInput,
          sourceCIF: undefined
        };
        const expectedErrorCode = ERROR_CODE.INVALID_MANDATORY_FIELDS;
        const expectedErrorKeyField = 'sourceCIF';
        (transactionRepository.create as jest.Mock).mockImplementation(
          model => {
            return {
              ...model,
              sourceCIF: undefined,
              id: transactionId
            };
          }
        );
        (transactionRepository.findByIdAndUpdate as jest.Mock).mockImplementation(
          model => {
            return {
              ...model,
              sourceCIF: undefined,
              id: transactionId,
              status: TransactionStatus.DECLINED
            };
          }
        );

        // when
        const error = await transactionService
          .createInternalTopupTransaction(input)
          .catch(error => error);

        // then
        expect(error).toBeInstanceOf(TransferAppError);
        expect(error.detail).toEqual('Invalid mandatory fields!');
        expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
        expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
        expect(error.errors[0]).toEqual({
          code: expectedErrorCode,
          key: expectedErrorKeyField,
          message: ErrorList[expectedErrorCode].message
        });
      });

      it('should throw INVALID REQUEST when missing beneficiaryAccountNo for Wallet Payment Type', async () => {
        // // ARRANGE
        const input = {
          ...topupTransactionInput,
          beneficiaryAccountNo: undefined
        };
        (transactionRepository.create as jest.Mock).mockImplementation(
          model => {
            return {
              ...model,
              beneficiaryAccountNo: undefined,
              id: transactionId
            };
          }
        );
        (transactionRepository.findByIdAndUpdate as jest.Mock).mockImplementation(
          model => {
            return {
              ...model,
              beneficiaryAccountNo: undefined,
              id: transactionId,
              status: TransactionStatus.DECLINED
            };
          }
        );
        const expectedErrorCode = ERROR_CODE.INVALID_MANDATORY_FIELDS;
        const expectedErrorKeyField = 'beneficiaryAccountNo';
        // // ACT
        const error = await transactionService
          .createInternalTopupTransaction(input)
          .catch(e => e);

        // ASSERT
        expect(error).toBeInstanceOf(TransferAppError);
        expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
        expect(error.detail).toEqual('Invalid mandatory fields!');
        expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
        expect(error.errors[0]).toEqual({
          code: expectedErrorCode,
          key: expectedErrorKeyField,
          message: ErrorList[expectedErrorCode].message
        });
      });
    });
  });

  describe('getTransaction', () => {
    it('should return transaction successfully by Id', async () => {
      let newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel
      };
      //given
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(data);

      //when
      const expected = await transactionService.getTransaction(data.id);

      //then
      expect(expected).toBeDefined();
      expect(expected).toMatchObject(data);
    });
    it('should return transaction successfully by externalId', async () => {
      let newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        id: '123',
        ...newTransactionModel,
        externalId: 'externalId'
      };
      //given
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(data);

      //when
      const expected = await transactionService.getTransaction(data.externalId);

      //then
      expect(expected).toBeDefined();
      expect(expected).toMatchObject(data);
    });

    it('should throw error when transaction not found', async () => {
      //given
      const id = '123';
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(null);

      //when
      const expected = await transactionService
        .getTransaction(id)
        .catch(error => error);
      //then
      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toEqual(ERROR_CODE.TRANSACTION_NOT_FOUND);
      expect(expected.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
      expect(expected.detail).toEqual(
        `getTransaction: Matching txn was not found for transactionId: ${id}`
      );
    });
  });

  describe('updateRefundTransaction', () => {
    it('should update new refunded transaction successfully with original transaction without refund obejct by Id', async () => {
      let newTransactionModel = getTransactionModelFullData();
      const refundedTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT,
        transactionAmount: 1000,
        id: 'test12345'
      };
      const originalTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT,
        transactionAmount: 10000,
        id: 'test1234'
      };
      //given
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...refundedTransaction,
        refund: {
          remainingAmount: 9000,
          originalTransactionId: originalTransaction.id
        }
      });
      //when
      const expected = await transactionService.updateRefundTransaction(
        originalTransaction,
        refundedTransaction
      );

      //then
      expect(expected).toBeDefined();
      expect(expected).toMatchObject({
        ...refundedTransaction,
        refund: {
          remainingAmount: 9000,
          originalTransactionId: originalTransaction.id
        }
      });
    });

    it('should update refunded transaction successfully with original transaction with refund obejct by Id', async () => {
      let newTransactionModel = getTransactionModelFullData();
      const refundedTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT,
        transactionAmount: 1000,
        id: 'test12345'
      };
      const originalTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT,
        transactionAmount: 10000,
        id: 'test1234',
        refund: {
          remainingAmount: 7000,
          refundedTransaction: ['1234567']
        }
      };
      //given
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...refundedTransaction,
        refund: {
          remainingAmount: 6000,
          originalTransactionId: originalTransaction.id
        }
      });
      //when
      const expected = await transactionService.updateRefundTransaction(
        originalTransaction,
        refundedTransaction
      );

      //then
      expect(expected).toBeDefined();
      expect(expected).toMatchObject({
        ...refundedTransaction,
        refund: {
          remainingAmount: 6000,
          originalTransactionId: originalTransaction.id
        }
      });
    });
  });

  describe('updateOriginalTransactionWithRefundDetails', () => {
    it('should update original transaction successfully with new refund obejct', async () => {
      let newTransactionModel = getTransactionModelFullData();
      const refundedTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT,
        transactionAmount: 1000,
        id: 'test12345'
      };
      const originalTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT,
        transactionAmount: 10000,
        id: 'test1234'
      };
      //given
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...originalTransaction,
        refund: {
          remainingAmount: 9000,
          refundedTransaction: [refundedTransaction.id]
        }
      });
      //when
      const expected = await transactionService.updateOriginalTransactionWithRefund(
        originalTransaction,
        refundedTransaction
      );

      //then
      expect(expected).toBeDefined();
      expect(expected).toMatchObject({
        ...originalTransaction,
        refund: {
          remainingAmount: 9000,
          refundedTransaction: [refundedTransaction.id]
        }
      });
    });

    it('should update refunded transaction successfully with original transaction with refund obejct by Id', async () => {
      let newTransactionModel = getTransactionModelFullData();
      const refundedTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT,
        transactionAmount: 2000,
        id: 'test12345'
      };
      const originalTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT,
        transactionAmount: 10000,
        id: 'test1234',
        refund: {
          remainingAmount: 7000,
          refundedTransaction: ['1234567']
        }
      };
      //given
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...originalTransaction,
        refund: {
          remainingAmount: 5000,
          refundedTransaction: ['1234567', refundedTransaction.id]
        }
      });
      //when
      const expected = await transactionService.updateOriginalTransactionWithRefund(
        originalTransaction,
        refundedTransaction
      );

      //then
      expect(expected).toBeDefined();
      expect(expected).toMatchObject({
        ...originalTransaction,
        refund: {
          remainingAmount: 5000,
          refundedTransaction: ['1234567', refundedTransaction.id]
        }
      });
    });
  });

  describe('getTransactionList', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });
    it('should return transaction List successfully when payload is provided', async () => {
      //given
      let newTransactionModel = getTransactionModelFullData();
      const input: TransactionListRequest = {
        transactionId: 'Test123'
      };
      const data: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel
      };
      (transactionHelper.validateTransactionListRequest as jest.Mock).mockReturnValueOnce(
        true
      );
      (transactionRepository.getTransactionListByCriteria as jest.Mock).mockResolvedValueOnce(
        { list: [data], limit: 20, page: 1, totalPage: 10 }
      );

      //when
      const {
        transactions,
        refundTransactions
      } = await transactionService.getTransactionList(input);

      //then
      expect(transactions).toBeDefined();
      expect(transactions.list[0]).toMatchObject(data);
      expect(refundTransactions.length).toBe(0);
    });

    it('should return empty refundTransactions if no refund transaction list found for any transaction', async () => {
      // Given
      let newTransactionModel = getTransactionModelFullData();
      const input: TransactionListRequest = {
        transactionId: 'Test123'
      };
      const data: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel
      };
      (transactionHelper.validateTransactionListRequest as jest.Mock).mockReturnValueOnce(
        true
      );
      (transactionRepository.getTransactionListByCriteria as jest.Mock).mockResolvedValueOnce(
        { list: [data], limit: 20, page: 1, totalPage: 10 }
      );

      // When
      const {
        transactions,
        refundTransactions
      } = await transactionService.getTransactionList(input, true);

      // Then
      expect(transactions).toBeDefined();
      expect(transactions.list[0]).toMatchObject(data);
      expect(refundTransactions.length).toBe(0);
    });

    it('should fetch refund transactions if refund transaction list is present', async () => {
      // Given
      let newTransactionModel = getTransactionModelFullData();
      const input: TransactionListRequest = {
        transactionId: 'Test123'
      };
      const data: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        refund: getRefundDetails()
      };
      (transactionHelper.validateTransactionListRequest as jest.Mock).mockReturnValueOnce(
        true
      );
      (transactionRepository.getTransactionListByCriteria as jest.Mock).mockResolvedValueOnce(
        { list: [data], limit: 20, page: 1, totalPage: 10 }
      );
      transactionRepository.findTransactionByQuery = jest
        .fn()
        .mockResolvedValueOnce([
          {
            id: '5f8f7a1c481f6a70b636bc85',
            status: TransactionStatus.SUCCEED,
            createdAt: new Date(),
            updatedAt: new Date(),
            transactionAmount: 10000
          }
        ]);

      // When
      const {
        transactions,
        refundTransactions
      } = await transactionService.getTransactionList(input, true);

      // Then
      expect(transactions).toBeDefined();
      expect(transactions.list[0]).toMatchObject(data);
      expect(refundTransactions.length).toBe(1);
      expect(refundTransactions[0].id).toBe('5f8f7a1c481f6a70b636bc85');
    });

    it('should return transaction successfully by when customerId is given', async () => {
      //given
      let newTransactionModel = getTransactionModelFullData();
      const input: TransactionListRequest = {
        customerId: 'Test123'
      };
      const data: ITransactionModel = {
        id: '123',
        ...newTransactionModel,
        externalId: 'externalId'
      };
      (customerService.getCustomerById as jest.Mock).mockResolvedValueOnce({
        cif: 'Test123456',
        id: 'Test123'
      });
      (transactionHelper.validateTransactionListRequest as jest.Mock).mockReturnValueOnce(
        true
      );
      (transactionRepository.getTransactionListByCriteria as jest.Mock).mockResolvedValueOnce(
        { list: [data], limit: 20, page: 1, totalPage: 10 }
      );

      //when
      const { transactions } = await transactionService.getTransactionList(
        input,
        false
      );

      //then
      expect(transactions).toBeDefined();
      expect(transactions.list[0]).toMatchObject(data);
    });

    it('should get transactions with ownerCustomerId query', async () => {
      //given
      let newTransactionModel = getTransactionModelFullData();
      const input: TransactionListRequest = {
        customerId: '13245678'
      };
      const data: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel
      };

      (transactionHelper.validateTransactionListRequest as jest.Mock).mockReturnValueOnce(
        true
      );
      (customerService.getCustomerById as jest.Mock).mockResolvedValueOnce({
        cif: 'Test123456',
        id: 'Test123'
      });
      (transactionRepository.getTransactionListByCriteria as jest.Mock).mockResolvedValueOnce(
        { list: [data], limit: 20, page: 1, totalPage: 10 }
      );

      //when
      const { transactions } = await transactionService
        .getTransactionList(input, false)
        .catch(error => error);

      //then

      expect(transactions.list.length).toEqual(1);
      expect(customerService.getCustomerById).not.toHaveBeenCalled();
      expect(
        transactionRepository.getTransactionListByCriteria
      ).toHaveBeenCalledTimes(1);
      expect(
        transactionRepository.getTransactionListByCriteria
      ).toHaveBeenNthCalledWith(1, input);
    });

    it('should get transactions without OwnerCustomerId query', async () => {
      // Given
      let newTransactionModel = getTransactionModelFullData();
      const input: TransactionListRequest = {
        customerId: '13245678'
      };
      const data: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel
      };
      (transactionHelper.validateTransactionListRequest as jest.Mock).mockReturnValueOnce(
        true
      );
      (customerService.getCustomerById as jest.Mock).mockResolvedValueOnce({
        cif: 'Test123456',
        id: 'Test123'
      });
      (transactionRepository.getTransactionListByCriteria as jest.Mock)
        .mockResolvedValueOnce({ list: [], limit: 20, page: 1, totalPage: 10 })
        .mockResolvedValueOnce({
          list: [data],
          limit: 20,
          page: 1,
          totalPage: 10
        });

      // When
      const { transactions } = await transactionService
        .getTransactionList(input, false)
        .catch(error => error);

      // Then
      expect(transactions.list.length).toEqual(1);
      expect(
        transactionRepository.getTransactionListByCriteria
      ).toHaveBeenNthCalledWith(1, input);
      expect(
        transactionRepository.getTransactionListByCriteria
      ).toHaveBeenNthCalledWith(2, input, 'Test123456');
    });

    it('should get transactions without OwnerCustomerId query when startDate is less then lastTransaction date', async () => {
      // Given
      let newTransactionModel = getTransactionModelFullData();
      const transactionsList: ITransactionModel[] = [
        {
          id: '5e412688fd14310c4969099a',
          ...newTransactionModel,
          createdAt: new Date('2022-01-31T10:53:36.998Z')
        },
        {
          id: '5e412688fd14310c4969099a',
          ...newTransactionModel,
          createdAt: new Date('2022-01-27T10:53:36.998Z')
        }
      ];
      const input: TransactionListRequest = {
        customerId: '13245678',
        endDate: '2022-02-01',
        startDate: '2022-01-27'
      };
      (transactionHelper.validateTransactionListRequest as jest.Mock).mockReturnValueOnce(
        true
      );
      (customerService.getCustomerById as jest.Mock).mockResolvedValueOnce({
        cif: 'Test123456',
        id: 'Test123'
      });
      (transactionRepository.getTransactionListByCriteria as jest.Mock)
        .mockResolvedValueOnce({
          list: [transactionsList[0]],
          limit: 20,
          page: 1,
          totalPage: 10
        })
        .mockResolvedValueOnce({
          list: transactionsList,
          limit: 20,
          page: 1,
          totalPage: 10
        });

      // When
      const { transactions } = await transactionService
        .getTransactionList(input, false)
        .catch(error => error);

      // Then
      expect(transactions.list.length).toEqual(2);
      expect(customerService.getCustomerById).toHaveBeenCalledTimes(1);
      expect(
        transactionRepository.getTransactionListByCriteria
      ).toHaveBeenNthCalledWith(1, input);
      expect(
        transactionRepository.getTransactionListByCriteria
      ).toHaveBeenNthCalledWith(2, input, 'Test123456');
    });

    it('should throw error when transaction not found', async () => {
      //given
      const input: TransactionListRequest = {
        customerId: '123455678'
      };
      (transactionHelper.validateTransactionListRequest as jest.Mock).mockReturnValueOnce(
        true
      );
      (customerService.getCustomerById as jest.Mock).mockResolvedValueOnce({
        cif: 'Test123456',
        id: 'Test123'
      });
      (transactionRepository.getTransactionListByCriteria as jest.Mock)
        .mockResolvedValueOnce([])
        .mockResolvedValueOnce([]);

      //when
      const expected = await transactionService
        .getTransactionList(input, false)
        .catch(error => error);

      //then
      expect(
        transactionRepository.getTransactionListByCriteria
      ).toBeCalledTimes(2);
      expect(customerService.getCustomerById).toHaveBeenCalledTimes(1);
      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toEqual(ERROR_CODE.TRANSACTION_NOT_FOUND);
      expect(expected.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
      expect(expected.detail).toEqual(
        'getTransactionListByCriteria: Matching txn was not found'
      );
    });
  });

  describe('recommendPaymentService', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should return serviceRecommendations successfully', async () => {
      //given
      recommendedCodeService.getRecommendServices = jest
        .fn()
        .mockReturnValueOnce(getRecommendedServiceResponse());

      const recommendPaymentServiceInput: RecommendPaymentServiceInput = {
        beneficiaryBankCode: 'BC005',
        sourceBankCode: 'BC002',
        sourceCIF: '8a85c240766dff3f0176744d4a6c241e',
        beneficiaryAccountNo: '1',
        sourceAccountNo: '2',
        paymentServiceType: PaymentServiceTypeEnum.WALLET,
        transactionAmount: 10000
      };

      const expected = [
        {
          paymentServiceCode: 'RTOL',
          beneficiaryBankCodeType: 'RTOL',
          debitTransactionCode: [
            {
              interchange: 'INTERNAL',
              transactionCode: 'TFD30',
              transactionCodeInfo: {
                code: 'TFD30',
                limitGroupCode: 'L002',
                feeRules: 'RTOL_TRANSFER_FEE_RULE',
                minAmountPerTx: 10000,
                maxAmountPerTx: 50000000,
                defaultCategoryCode: 'C056',
                channel: 'TFD30',
                awardGroupCounter: 'Bonus_Transfer'
              }
            }
          ],
          creditTransactionCode: [],
          inquiryFeeRule: null,
          transactionAuthenticationChecking: true,
          blocking: true,
          requireThirdPartyOutgoingId: true,
          cutOffTime: null,
          beneficiaryBankCode: '014',
          feeData: [
            {
              feeCode: 'TF010',
              interchange: 'ARTAJASA',
              feeAmount: 3000,
              customerTc: 'FTD40',
              subsidiaryAmount: 0,
              debitGlNumber: '',
              creditGlNumber: '',
              subsidiary: false,
              customerTcChannel: 'FTD40'
            },
            {
              feeCode: 'TF009',
              interchange: 'ALTO',
              feeAmount: 3000,
              customerTc: 'FTD30',
              subsidiaryAmount: 0,
              debitGlNumber: '',
              creditGlNumber: '',
              subsidiary: false,
              customerTcChannel: 'FTD30'
            }
          ]
        }
      ];

      //when
      const result = await transactionService.recommendPaymentService(
        recommendPaymentServiceInput
      );

      //then
      expect(result).toBeDefined();
      expect(result).toMatchObject(expected);
    });

    it('should call beneficiary account inquiry when beneficiary bank code is BC000 - Email', async () => {
      // Given
      const recommendPaymentServiceInput: RecommendPaymentServiceInput = {
        beneficiaryBankCode: 'BC000',
        beneficiaryAccountNo: 'test@test.com',
        sourceBankCode: 'BC002',
        sourceCIF: '8a85c240766dff3f0176744d4a6c241e',
        sourceAccountNo: '2',
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        transactionAmount: 10000
      };
      mockReturnBankCodeOnce({
        ...bankCode,
        isInternal: true
      });
      mockReturnBankCodeOnce(getBiFastBankCode());

      recommendedCodeService.getRecommendServices = jest
        .fn()
        .mockReturnValueOnce(getRecommendedServiceResponse());

      // When
      await transactionService.recommendPaymentService(
        recommendPaymentServiceInput
      );

      // Then
      expect(recommendedCodeService.getRecommendServices).toBeCalledTimes(1);
      expect(recommendedCodeService.getRecommendServices).toBeCalledWith(
        expect.objectContaining({
          additionalInformation3: BeneficiaryProxyType.EMAIL,
          additionalInformation4:
            recommendPaymentServiceInput.beneficiaryAccountNo
        })
      );
      expect(transactionHelper.getBeneficiaryAccount).toBeCalledWith(
        recommendPaymentServiceInput.beneficiaryAccountNo,
        recommendPaymentServiceInput.sourceCIF,
        getBiFastBankCode(),
        undefined,
        recommendPaymentServiceInput.transactionAmount,
        recommendPaymentServiceInput.sourceAccountNo,
        true
      );
    });

    it('should call beneficiary account inquiry when beneficiary bank code is BC000 - PhoneNo', async () => {
      // Given
      const recommendPaymentServiceInput: RecommendPaymentServiceInput = {
        beneficiaryBankCode: 'BC000',
        beneficiaryAccountNo: '+628112233',
        sourceBankCode: 'BC002',
        sourceCIF: '8a85c240766dff3f0176744d4a6c241e',
        sourceAccountNo: '2',
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        transactionAmount: 10000
      };
      mockReturnBankCodeOnce({
        ...bankCode,
        isInternal: true
      });
      mockReturnBankCodeOnce(getBiFastBankCode());

      recommendedCodeService.getRecommendServices = jest
        .fn()
        .mockReturnValueOnce(getRecommendedServiceResponse());

      // When
      await transactionService.recommendPaymentService(
        recommendPaymentServiceInput
      );

      // Then
      expect(recommendedCodeService.getRecommendServices).toBeCalledTimes(1);
      expect(recommendedCodeService.getRecommendServices).toBeCalledWith(
        expect.objectContaining({
          additionalInformation3: BeneficiaryProxyType.PHONE_NUMBER,
          additionalInformation4:
            recommendPaymentServiceInput.beneficiaryAccountNo
        })
      );
      expect(transactionHelper.getBeneficiaryAccount).toBeCalledWith(
        recommendPaymentServiceInput.beneficiaryAccountNo,
        recommendPaymentServiceInput.sourceCIF,
        getBiFastBankCode(),
        undefined,
        recommendPaymentServiceInput.transactionAmount,
        recommendPaymentServiceInput.sourceAccountNo,
        true
      );
    });

    it('should return SKN when preferredBankChannel is EXTERNAL', async () => {
      //given
      const recommendPaymentServiceInput: RecommendPaymentServiceInput = {
        beneficiaryBankCode: 'BC005',
        sourceBankCode: 'BC002',
        sourceCIF: '8a85c240766dff3f0176744d4a6c241e',
        beneficiaryAccountNo: '1',
        sourceAccountNo: '2',
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        transactionAmount: 50000001,
        preferredBankChannel: BankChannelEnum.EXTERNAL
      };

      mockReturnBankCodeOnce({
        ...bankCode,
        isInternal: true
      });
      mockReturnBankCodeOnce(getBiFastBankCode());

      recommendedCodeService.getRecommendServices = jest
        .fn()
        .mockReturnValueOnce(getRecommendedServiceResponse());

      // When
      await transactionService.recommendPaymentService(
        recommendPaymentServiceInput
      );

      // Then
      expect(recommendedCodeService.getRecommendServices).toBeCalledTimes(1);
      expect(recommendedCodeService.getRecommendServices).toBeCalledWith(
        expect.objectContaining({
          preferredBankChannel: BankChannelEnum.EXTERNAL,
          sourceAccountNo: recommendPaymentServiceInput.sourceAccountNo,
          transactionAmount: recommendPaymentServiceInput.transactionAmount
        })
      );
      expect(transactionHelper.getBeneficiaryAccount).toBeCalledWith(
        recommendPaymentServiceInput.beneficiaryAccountNo,
        recommendPaymentServiceInput.sourceCIF,
        getBiFastBankCode(),
        undefined,
        recommendPaymentServiceInput.transactionAmount,
        recommendPaymentServiceInput.sourceAccountNo,
        true
      );
    });
  });

  describe('checkTransactionStatus', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('Should throw error when getTransactionStatusByCriteria is not found', async () => {
      // Given
      const transactionInput = {
        customerId: '123',
        paymentInstructionId: '123zxc',
        transactionDate: new Date()
      };
      (transactionRepository.getTransactionStatusByCriteria as jest.Mock).mockResolvedValueOnce(
        undefined
      );

      // When
      const error = await transactionService
        .checkTransactionStatus(transactionInput)
        .catch(e => e);

      // Then
      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(ERROR_CODE.TRANSACTION_NOT_FOUND);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Matching transaction was not found!');
    });

    it('Should return transaction and not calling check status to thirdparty when transaction status on DB are SUCCEED or DECLINED', async () => {
      // Given
      const transactionInput = {
        customerId: '123',
        paymentInstructionId: '123zxc',
        transactionDate: new Date()
      };
      const transactionModel = {
        ...getTransactionDetail()
      };
      (transactionRepository.getTransactionStatusByCriteria as jest.Mock).mockResolvedValueOnce(
        transactionModel
      );
      transactionExecutionService.executeCheckStatus = jest
        .fn()
        .mockResolvedValue(undefined);

      // When
      const result = await transactionService.checkTransactionStatus(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(transactionExecutionService.executeCheckStatus).not.toBeCalled();
    });

    it('Should return transaction and calling check status to thirdparty when transaction status on DB are neiter SUCCEED nor DECLINED', async () => {
      // Given
      const transactionInput = {
        customerId: '123',
        paymentInstructionId: '123zxc',
        transactionDate: new Date()
      };
      const transactionModel = {
        ...getTransactionDetail(),
        status: TransactionStatus.SUBMITTED
      };
      (transactionRepository.getTransactionStatusByCriteria as jest.Mock).mockResolvedValueOnce(
        transactionModel
      );
      transactionExecutionService.executeCheckStatus = jest
        .fn()
        .mockResolvedValue({
          status: TransactionStatus.SUCCEED
        });

      // When
      const result = await transactionService.checkTransactionStatus(
        transactionInput
      );

      // Then
      expect(result).toBeDefined();
      expect(result).toEqual({
        status: TransactionStatus.SUCCEED
      });
      expect(transactionExecutionService.executeCheckStatus).toBeCalledTimes(1);
      expect(transactionExecutionService.executeCheckStatus).toBeCalledWith(
        transactionModel
      );
    });

    it('should throw correct error when got TransferAppError in checkTransactionStatus', async () => {
      // given
      const transactionInput = {
        customerId: '123',
        paymentInstructionId: '123zxc',
        transactionDate: new Date()
      };
      (transactionRepository.getTransactionStatusByCriteria as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Example failure reason!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.TRANSACTION_NOT_FOUND
        )
      );

      // when
      const error = await transactionService
        .checkTransactionStatus(transactionInput)
        .catch(error => error);

      // then
      expect(error).toBeDefined();
      expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      expect(error.detail).toEqual('Example failure reason!');
      expect(error.errorCode).toEqual(ERROR_CODE.TRANSACTION_NOT_FOUND);
    });

    it('should throw unexpected error when something unexpected happen in checkTransactionStatus', async () => {
      // Given
      const transactionInput = {
        customerId: '123',
        paymentInstructionId: '123zxc',
        transactionDate: new Date()
      };
      (transactionRepository.getTransactionStatusByCriteria as jest.Mock).mockRejectedValueOnce(
        new Error()
      );

      // When
      const error = await transactionService
        .checkTransactionStatus(transactionInput)
        .catch(e => e);

      // Then
      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(ERROR_CODE.UNEXPECTED_ERROR);
      expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      expect(error.detail).toEqual(
        'Unexpected error while checking transaction status with message: !'
      );
    });
  });

  describe('resolveTransactionStatus', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('Should call getPendingTransactionList and executeCheckStatus - QRIS', async () => {
      // Given
      const minTransactionAge = faker.random.number();
      const maxTransactionAge = faker.random.number();
      const recordLimit = faker.random.number();
      const transaction: ITransactionModel = {
        ...getTransactionModelFullData(),
        id: '5e412688fd14310c4969099a',
        paymentServiceCode: QRIS_PAYMENT_SERVICE_CODE
      };
      (transactionRepository.getPendingTransactionList as jest.Mock).mockResolvedValueOnce(
        [transaction]
      );

      // When
      const error = await transactionService
        .resolveTransactionStatus(
          minTransactionAge,
          maxTransactionAge,
          recordLimit
        )
        .catch(e => e);

      // Then
      expect(error).toBeUndefined();
      expect(transactionRepository.getPendingTransactionList).toBeCalledTimes(
        1
      );
      expect(transactionRepository.getPendingTransactionList).toBeCalledWith(
        SCHEDULED_RESOLVE_STATUS_TRANSACTION_CODES,
        minTransactionAge,
        maxTransactionAge,
        recordLimit
      );
      expect(qrisService.executeCheckStatus).toBeCalledTimes(1);
      expect(qrisService.executeCheckStatus).toBeCalledWith(transaction);
    });

    it('Should call getPendingTransactionList and executeCheckStatus - BIFAST', async () => {
      // Given
      const minTransactionAge = faker.random.number();
      const maxTransactionAge = faker.random.number();
      const recordLimit = faker.random.number();
      const transaction: ITransactionModel = {
        ...getTransactionModelFullData(),
        id: '5e412688fd14310c4969099a',
        paymentServiceCode: 'BIFAST_OUTGOING'
      };
      (transactionRepository.getPendingTransactionList as jest.Mock).mockResolvedValueOnce(
        [transaction]
      );

      // When
      const error = await transactionService
        .resolveTransactionStatus(
          minTransactionAge,
          maxTransactionAge,
          recordLimit
        )
        .catch(e => e);

      // Then
      expect(error).toBeUndefined();
      expect(transactionRepository.getPendingTransactionList).toBeCalledTimes(
        1
      );
      expect(transactionRepository.getPendingTransactionList).toBeCalledWith(
        SCHEDULED_RESOLVE_STATUS_TRANSACTION_CODES,
        minTransactionAge,
        maxTransactionAge,
        recordLimit
      );
      expect(qrisService.executeCheckStatus).toBeCalledTimes(0);
      expect(transactionExecutionService.executeCheckStatus).toBeCalledTimes(1);
      expect(transactionExecutionService.executeCheckStatus).toBeCalledWith(
        transaction
      );
    });

    it('Should call getPendingTransactionList with default values', async () => {
      // Given
      const transaction: ITransactionModel = {
        ...getTransactionModelFullData(),
        id: '5e412688fd14310c4969099a',
        paymentServiceCode: QRIS_PAYMENT_SERVICE_CODE
      };
      (transactionRepository.getPendingTransactionList as jest.Mock).mockResolvedValueOnce(
        [transaction]
      );

      // When
      const error = await transactionService
        .resolveTransactionStatus(undefined, undefined, undefined)
        .catch(e => e);

      // Then
      expect(error).toBeUndefined();
      expect(transactionRepository.getPendingTransactionList).toBeCalledTimes(
        1
      );
      expect(transactionRepository.getPendingTransactionList).toBeCalledWith(
        SCHEDULED_RESOLVE_STATUS_TRANSACTION_CODES,
        SCHEDULED_RESOLVE_STATUS_MIN_TRANSACTION_AGE_IN_MINUTES,
        SCHEDULED_RESOLVE_STATUS_MAX_TRANSACTION_AGE_IN_MINUTES,
        SCHEDULED_RESOLVE_STATUS_FETCH_RECORD_LIMIT
      );
      expect(qrisService.executeCheckStatus).toBeCalledTimes(1);
      expect(qrisService.executeCheckStatus).toBeCalledWith(transaction);
    });
  });
});
