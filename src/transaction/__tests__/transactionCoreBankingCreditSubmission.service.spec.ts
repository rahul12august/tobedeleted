import { BankChannelEnum } from '@dk/module-common';
import { getTransactionDetail } from '../__mocks__/transaction.data';
import internalSubmissionTemplate from '../transactionCoreBankingCreditSubmission.service';
import { Rail } from '@dk/module-common';

describe('internalSubmissionTemplate', () => {
  it(`should return true when beneficiaryBankCodeChannel is ${BankChannelEnum.INTERNAL}`, () => {
    // given
    const model = {
      ...getTransactionDetail(),
      creditTransactionCode: 'anycode',
      creditTransactionChannel: 'anychannel',
      beneficiaryAccountNo: 'anyNo',
      beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL
    };

    // when
    const eligible = internalSubmissionTemplate.isEligible(model);

    // then
    expect(eligible).toBeTruthy();
  });

  it(`should return false when beneficiaryBankCodeChannel is not ${BankChannelEnum.INTERNAL}`, () => {
    // given
    const model = {
      ...getTransactionDetail(),
      creditTransactionCode: 'anycode',
      creditTransactionChannel: 'anychannel',
      beneficiaryAccountNo: 'anyNo',
      beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL
    };

    // when
    const eligible = internalSubmissionTemplate.isEligible(model);

    // then
    expect(eligible).not.toBeTruthy();
  });

  describe('Rail check', () => {
    it(`should have rail property defined`, () => {
      // when
      const rail = internalSubmissionTemplate.getRail?.();

      // then
      expect(rail).toEqual(Rail.INTERNAL);
    });
  });
});
