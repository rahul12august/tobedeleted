import { Util } from '@dk/module-common';
import transactionReversalHelper from '../transactionReversal.helper';
import transactionReversalService from '../transactionReversal.service';
import { HttpError } from '@dk/module-httpclient';
import transactionHelper from '../transaction.helper';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../transaction.enum';
import transactionProducer from '../transaction.producer';
import { AppError, TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import { getTransactionModelFullData } from '../__mocks__/transaction.data';

jest.mock('../transactionReversal.service');
jest.mock('../transaction.helper');
jest.mock('../transaction.producer');

describe('revertFailedTransaction', () => {
  let mockedRetry: jest.SpyInstance;
  const transactionId = '12344';
  const reversalReason = 'fail to process credit transaction';
  beforeEach(() => {
    mockedRetry = jest
      .spyOn(Util, 'retry')
      .mockImplementation(async (_, fn, ...args) => await fn(...args));

    (transactionHelper.updateTransactionStatus as jest.Mock).mockResolvedValueOnce(
      {}
    );
  });
  const savedTransaction = {
    ...getTransactionModelFullData(),
    referenceId: 'referenceId',
    createdAt: new Date('01 June 2020 00:00:00'),
    id: '12344'
  };
  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('revertFailedTransaction', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should call transactionRevesalService with retry', async () => {
      await transactionReversalHelper.revertFailedTransaction(
        transactionId,
        reversalReason
      );

      expect(Util.retry).toBeCalledTimes(1);
    });

    it('should produce retry reversal message when transactionRevesalService throws retriable error', async () => {
      mockedRetry.mockRestore();
      const transferAppError = new TransferAppError(
        'Test error!',
        TransferFailureReasonActor.UNKNOWN,
        ERROR_CODE.UNEXPECTED_ERROR
      );

      (transactionReversalService.reverseTransactionsByInstructionId as jest.Mock).mockRejectedValue(
        transferAppError
      );
      (transactionProducer.sendTransactionRetryReversalCommand as jest.Mock).mockResolvedValueOnce(
        {}
      );

      await transactionReversalHelper
        .revertFailedTransaction(transactionId, reversalReason)
        .catch(err => err);
      expect(
        transactionReversalService.reverseTransactionsByInstructionId
      ).toBeCalledTimes(1);

      expect(transactionHelper.updateTransactionStatus).not.toHaveBeenCalled();

      expect(
        transactionProducer.sendTransactionRetryReversalCommand
      ).not.toHaveBeenCalled();
    });

    it('should produce failed retry reversal event when transactionRevesalService throws non-retriable error', async () => {
      mockedRetry.mockRestore();
      const nonRetriableError: HttpError = {
        status: 400,
        message: 'bad request',
        name: 'server error',
        error: {}
      };

      (transactionReversalService.reverseTransactionsByInstructionId as jest.Mock).mockRejectedValue(
        nonRetriableError
      );
      (transactionProducer.sendTransactionRetryReversalCommand as jest.Mock).mockResolvedValueOnce(
        {}
      );

      const error = await transactionReversalHelper
        .revertFailedTransaction(transactionId, reversalReason)
        .catch(error => error);

      expect(error).toBeInstanceOf(AppError);
      expect(error.errorCode).toEqual(ERROR_CODE.FAILED_TO_REVERSE);
      expect(error.detail).toEqual(
        'Failed to reverse for transaction ID: 12344!'
      );

      expect(
        transactionReversalService.reverseTransactionsByInstructionId
      ).toBeCalledTimes(1);

      expect(transactionHelper.updateTransactionStatus).not.toHaveBeenCalled();

      expect(
        transactionProducer.sendTransactionReversalFailedEvent
      ).toBeCalledWith({
        transactionId: transactionId,
        retryCounter: 1
      });
    });

    it('should save failure reason and produce error', async () => {
      mockedRetry.mockRestore();
      const transferAppError = new TransferAppError(
        'Test error!',
        TransferFailureReasonActor.UNKNOWN,
        ERROR_CODE.TRANSACTION_FAILED
      );

      (transactionReversalService.reverseTransactionsByInstructionId as jest.Mock).mockRejectedValue(
        transferAppError
      );
      (transactionProducer.sendTransactionRetryReversalCommand as jest.Mock).mockResolvedValueOnce(
        {}
      );

      const error = await transactionReversalHelper
        .revertFailedTransaction(transactionId, reversalReason)
        .catch(error => error);

      expect(error).toBeInstanceOf(TransferAppError);

      expect(
        transactionReversalService.reverseTransactionsByInstructionId
      ).toBeCalledTimes(1);

      expect(transactionHelper.updateFailedTransactionStatus).toBeCalledWith(
        transactionId,
        TransactionStatus.REVERSAL_FAILED,
        transferAppError
      );

      expect(
        transactionProducer.sendTransactionReversalFailedEvent
      ).toBeCalledWith({
        transactionId: transactionId,
        retryCounter: 1
      });
    });
  });

  describe('revertManuallyAdjustedTransaction', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should call revertManuallyAdjustedTransaction with retry', async () => {
      await transactionReversalHelper.revertManuallyAdjustedTransaction(
        transactionId,
        savedTransaction
      );
      expect(Util.retry).toBeCalledTimes(1);
    });
    it('should produce retry reversal message when revertManuallyAdjustedTransaction throws retriable error', async () => {
      mockedRetry.mockRestore();
      const retriableError: HttpError = {
        status: 500,
        message: 'server error',
        name: 'server error',
        error: {}
      };

      (transactionReversalService.reverseTransactionsByInstructionId as jest.Mock).mockRejectedValue(
        retriableError
      );
      (transactionProducer.sendTransactionReversalFailedEvent as jest.Mock).mockResolvedValueOnce(
        {}
      );

      await transactionReversalHelper
        .revertManuallyAdjustedTransaction(transactionId, savedTransaction)
        .catch(err => err);
      expect(
        transactionReversalService.reverseTransactionsByInstructionId
      ).toBeCalledTimes(1);

      expect(
        transactionProducer.sendTransactionReversalFailedEvent
      ).toBeCalledTimes(1);
    });
  });

  describe('revertFailedTransactionWithoutError', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });
    it('should call revertFailedTransactionWithoutError with retry', async () => {
      await transactionReversalHelper.revertFailedTransactionWithoutError(
        transactionId,
        undefined,
        savedTransaction
      );

      expect(Util.retry).toBeCalledTimes(1);
    });

    it('should produce retry reversal message when revertFailedTransactionWithoutError throws retriable error', async () => {
      mockedRetry.mockRestore();
      const retriableError: HttpError = {
        status: 500,
        message: 'server error',
        name: 'server error',
        error: {}
      };

      (transactionReversalService.reverseTransactionsByInstructionId as jest.Mock).mockRejectedValue(
        retriableError
      );
      (transactionProducer.sendTransactionRetryReversalCommand as jest.Mock).mockResolvedValueOnce(
        {}
      );

      await transactionReversalHelper
        .revertFailedTransactionWithoutError(
          transactionId,
          undefined,
          savedTransaction
        )
        .catch(err => err);
      expect(
        transactionReversalService.reverseTransactionsByInstructionId
      ).toBeCalledTimes(1);

      expect(
        transactionProducer.sendTransactionRetryReversalCommand
      ).toBeCalledWith({
        transactionId: transactionId,
        retryCounter: 1
      });
    });

    it('should produce failed retry reversal event when revertFailedTransactionWithoutError throws non-retriable error', async () => {
      mockedRetry.mockRestore();
      const nonRetriableError: HttpError = {
        status: 400,
        message: 'bad request',
        name: 'server error',
        error: {}
      };

      (transactionReversalService.reverseTransactionsByInstructionId as jest.Mock).mockRejectedValue(
        nonRetriableError
      );
      (transactionProducer.sendTransactionRetryReversalCommand as jest.Mock).mockResolvedValueOnce(
        {}
      );

      await transactionReversalHelper
        .revertFailedTransactionWithoutError(
          transactionId,
          undefined,
          savedTransaction
        )
        .catch(error => error);

      expect(
        transactionReversalService.reverseTransactionsByInstructionId
      ).toBeCalledTimes(1);

      expect(
        transactionProducer.sendTransactionReversalFailedEvent
      ).toBeCalledWith({
        transactionId: transactionId,
        retryCounter: 1
      });
    });
  });
});
