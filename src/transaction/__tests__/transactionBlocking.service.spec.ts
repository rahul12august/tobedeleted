import moment from 'moment';
import { ERROR_CODE } from '../../common/errors';
import {
  getTransactionModelFullData,
  getITransferTransactionInput,
  getTransactionMockValueObject,
  getRecommendedService
} from '../__mocks__/transaction.data';
import { ITransferTransactionInput } from '../transaction.type';
import { ITransactionModel } from '../transaction.model';
import {
  TransactionStatus,
  TransferJourneyStatusEnum
} from '../transaction.enum';
import { Enum } from '@dk/module-common';
import {
  IBlockingAmountResponse,
  IBlockingRequestPayload
} from '../../account/account.type';
import awardRepository from '../../award/award.repository';
import transactionProducer from '../transaction.producer';
import monitoringEventProducer from '../../monitoringEvent/monitoringEvent.producer';
import depositRepository from '../../coreBanking/deposit.repository';
import transactionBlockingService from '../transactionBlocking.service';
import coreBankingHelper from '../transactionCoreBanking.helper';
import configurationRepository from '../../configuration/configuration.repository';
import transactionHelper from '../transaction.helper';
import accountRepository from '../../account/account.repository';
import transactionRepository from '../transaction.repository';
import decisionEngineService from '../../decisionEngine/decisionEngine.service';
import {
  getDecisionEngineConfiguration,
  getCounterData
} from '../../configuration/__mocks__/configuration.data';
import { config } from '@dk/module-config';
import blockingAmountService from '../../blockingAmount/blockingAmount.service';
import { MonitoringEventType } from '@dk/module-message';

jest.mock('../../configuration/configuration.repository');
jest.mock('../../logger');
jest.mock('../../coreBanking/deposit.repository');
jest.mock('../../award/award.repository');
jest.mock('../transaction.repository');
jest.mock('../../account/account.repository');
jest.mock('../../transaction/transaction.producer');
jest.mock('../../monitoringEvent/monitoringEvent.producer');
jest.mock('../transactionCoreBanking.helper');
jest.mock('../transaction.helper', () => {
  const original = require.requireActual('../transaction.helper').default;
  return {
    ...original,
    mapAuthenticationRequired: jest.fn(),
    mapFeeData: jest.fn(),
    getAccountInfo: jest.fn(),
    getSourceAccount: jest.fn(),
    validateTransactionLimitAmount: jest.fn(),
    getPaymentServiceMappings: jest.fn(),
    getBeneficiaryAccount: jest.fn()
  };
});
jest.mock('../../decisionEngine/decisionEngine.service.ts');
jest.mock('../../blockingAmount/blockingAmount.service');
jest.mock('../../common/featureFlag');

describe('transactionBlockingService', () => {
  const transactionId = '123456';
  const {
    beneficiaryAccount,
    sourceAccount,
    sourceBankCode: bankCode,
    transactionCategory,
    ruleConfigs,
    limitGroups,
    usageCounters
  } = getTransactionMockValueObject();

  const serviceCodes = ['p01'];
  const feeRule = { basicFee: { code: 'f01', feeAmountFixed: 1 } };

  const mockReturnDefaultInternalBankCode = () => {
    (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValue(
      {
        ...bankCode,
        isInternal: true
      }
    );
    (configurationRepository.getCounterByCode as jest.Mock).mockResolvedValue(
      getCounterData()
    );
  };

  beforeEach(() => {
    (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValue(
      999999999
    );
    (configurationRepository.getTransactionCategory as jest.Mock).mockResolvedValue(
      transactionCategory
    );

    mockReturnDefaultInternalBankCode();
    (transactionHelper.mapAuthenticationRequired as jest.Mock).mockImplementation(
      value => value
    );
    (transactionHelper.mapFeeData as jest.Mock).mockImplementation(
      value => value
    );

    (transactionHelper.getAccountInfo as jest.Mock).mockImplementation(
      (accountNo, cif, bankCode, accountName) => {
        if (accountNo === sourceAccount.accountNumber) {
          return sourceAccount;
        }
        if (accountNo === beneficiaryAccount.accountNumber) {
          return beneficiaryAccount;
        }
        return null;
      }
    );

    (transactionHelper.getSourceAccount as jest.Mock).mockImplementation(
      (accountNo, cif, bankCode, accountName) => {
        if (accountNo === sourceAccount.accountNumber) {
          return sourceAccount;
        }
        if (accountNo === beneficiaryAccount.accountNumber) {
          return beneficiaryAccount;
        }
        return null;
      }
    );

    (transactionHelper.getBeneficiaryAccount as jest.Mock).mockImplementation(
      (accountNo, cif, bankCode, accountName) => {
        if (accountNo === sourceAccount.accountNumber) {
          return sourceAccount;
        }
        if (accountNo === beneficiaryAccount.accountNumber) {
          return beneficiaryAccount;
        }
        return null;
      }
    );

    (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValue(
      serviceCodes
    );

    // update should do nothing and just return the input for happy case
    (transactionRepository.update as jest.Mock).mockImplementation(
      (id, model) => {
        return model;
      }
    );

    (monitoringEventProducer.sendMonitoringEventMessage as jest.Mock).mockResolvedValue(
      {}
    );

    (decisionEngineService.runMultipleRoles as jest.Mock).mockResolvedValue(
      true
    );
    (configurationRepository.getDecisionEngineConfig as jest.Mock).mockResolvedValue(
      getDecisionEngineConfiguration()
    );
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  const mockRecommendationServiceValidationPassed = () => {
    (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValue(
      ruleConfigs
    );
    (configurationRepository.getLimitGroupsByCodes as jest.Mock).mockResolvedValue(
      limitGroups
    );
    (configurationRepository.getFeeRule as jest.Mock).mockResolvedValue(
      feeRule
    );
    (awardRepository.getUsageCounters as jest.Mock).mockResolvedValue(
      usageCounters
    );
  };

  describe('createBlockingTransferTransaction', () => {
    let transactionInput: ITransferTransactionInput;
    let blockingAmountResponse: IBlockingAmountResponse;
    let blockingRequestPayload: IBlockingRequestPayload;

    const initTestData = () => {
      const paymentServiceCode = 'P001';
      transactionInput = {
        ...getITransferTransactionInput(paymentServiceCode),
        additionalInformation4: 'blockName'
      };

      blockingRequestPayload = {
        amount: transactionInput.transactionAmount,
        name: transactionInput.additionalInformation4,
        mambuBlockingCode: '1000000007',
        blockingPurposeCode: Enum.BlockingAmountType.GINPAY
      };
      blockingAmountResponse = {
        id: '5e0f27148d671823074e9ddb',
        status: 'ACTIVE',
        cardId: 'card-id-01'
      };
    };

    const mockGetRecommendationServicesSuccess = () => {
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          paymentServiceCode: transactionInput.paymentServiceCode
        }
      ]);
    };
    const mockPersistTransactionSuccess = () => {
      (transactionRepository.create as jest.Mock).mockImplementation(model => {
        return {
          ...model,
          id: transactionId
        };
      });
    };

    const mockCreateBlockingAmountSuccess = () => {
      (blockingAmountService.createBlockingAmount as jest.Mock).mockResolvedValueOnce(
        blockingAmountResponse
      );
    };
    const mockCreateBlockingAmountFailed = () => {
      (blockingAmountService.createBlockingAmount as jest.Mock).mockRejectedValueOnce(
        new Error()
      );
    };
    const mockMomentNow = () => {
      const mockedDateNow = moment('2020-01-01T14:48:00.000Z').valueOf();
      moment.now = jest.fn().mockReturnValue(mockedDateNow);
    };

    beforeEach(() => {
      initTestData();
      mockRecommendationServiceValidationPassed();
      mockGetRecommendationServicesSuccess();
      mockPersistTransactionSuccess();
      mockMomentNow();
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('Should create blocking amount and persist transaction successfully', async () => {
      mockCreateBlockingAmountSuccess();

      const actualBlockingAmountCreationResponse = await transactionBlockingService.createBlockingTransferTransaction(
        transactionInput
      );

      expect(actualBlockingAmountCreationResponse).toMatchObject({
        id: transactionId
      });
      expect(transactionRepository.update).toBeCalledWith(
        transactionId,
        expect.objectContaining({
          blockingId: blockingAmountResponse.id,
          cardId: blockingAmountResponse.cardId
        })
      );

      expect(monitoringEventProducer.sendMonitoringEventMessage).toBeCalledWith(
        MonitoringEventType.PROCESSING_STARTED,
        expect.objectContaining({
          id: transactionId
        })
      );

      expect(blockingAmountService.createBlockingAmount).toBeCalledWith(
        transactionInput.sourceAccountNo,
        blockingRequestPayload
      );
    });

    it('Should not persist transaction when creating blocking amount failed', async () => {
      // given
      mockCreateBlockingAmountFailed();

      // when
      await transactionBlockingService
        .createBlockingTransferTransaction(transactionInput)
        .catch(e => e);

      // then should update transaction status to DECLINED
      expect(transactionRepository.update).toBeCalledWith(
        transactionId,
        expect.objectContaining({
          status: TransactionStatus.DECLINED
        })
      );

      expect(monitoringEventProducer.sendMonitoringEventMessage).toBeCalledWith(
        MonitoringEventType.PROCESSING_STARTED,
        expect.objectContaining({
          id: transactionId
        })
      );
    });
  });

  describe('adjustBlockingTransferTransaction', () => {
    it('should update blocking amount successfully', async () => {
      //given
      const cif = 'cif123';
      const newAmount = 10000;
      const newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        blockingId: 'blockingId123',
        cardId: 'cardId01',
        status: TransactionStatus.SUBMITTED,
        sourceCIF: cif
      };
      const updatedTransaction = {
        ...data,
        transactionAmount: newAmount,
        journey: [
          {
            status: 'BLOCKING_AMOUNT_ADJUSTED',
            updatedAt: expect.anything()
          }
        ]
      };
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(data);
      (blockingAmountService.updateBlockingAmount as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (transactionRepository.update as jest.Mock)
        .mockResolvedValueOnce({
          ...updatedTransaction,
          journey: [
            {
              status: TransferJourneyStatusEnum.ADJUST_BLOCKING_AMOUNT,
              updatedAt: new Date()
            }
          ]
        })
        .mockResolvedValueOnce({
          ...updatedTransaction,
          journey: [
            {
              status: TransferJourneyStatusEnum.BLOCKING_AMOUNT_ADJUSTED,
              updatedAt: new Date()
            }
          ]
        });

      //when
      const expected = await transactionBlockingService.adjustBlockingTransferTransaction(
        cif,
        data.id,
        newAmount
      );
      //then
      expect(expected).toEqual(updatedTransaction);
      expect(blockingAmountService.updateBlockingAmount).toBeCalledWith(
        data.sourceAccountNo,
        data.blockingId,
        newAmount,
        data.transactionAmount,
        data.cardId
      );
      expect(transactionRepository.update).toBeCalledTimes(2);
    });

    it('should update blocking amount successfully if internal blocking enabled', async () => {
      //given
      jest.resetAllMocks();
      config.get = jest.fn().mockImplementation(key => {
        switch (key) {
          case 'featureFlags': {
            return {
              enableInternalBlocking: 'true'
            };
          }
          default: {
            return undefined;
          }
        }
      });
      const cif = 'cif123';
      const newAmount = 10000;
      const newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        blockingId: 'blockingId123',
        cardId: 'cardId01',
        status: TransactionStatus.SUBMITTED,
        sourceCIF: cif
      };
      const updatedTransaction = {
        ...data,
        transactionAmount: newAmount,
        journey: [
          {
            status: TransferJourneyStatusEnum.BLOCKING_AMOUNT_ADJUSTED,
            updatedAt: expect.anything()
          }
        ]
      };
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(data);
      (blockingAmountService.updateBlockingAmount as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (accountRepository.updateBlockingAmount as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (transactionRepository.update as jest.Mock)
        .mockResolvedValueOnce(updatedTransaction)
        .mockResolvedValueOnce(updatedTransaction);
      //when
      const expected = await transactionBlockingService.adjustBlockingTransferTransaction(
        cif,
        data.id,
        newAmount
      );
      //then
      expect(expected).toEqual({
        ...updatedTransaction,
        journey: [
          {
            status: TransferJourneyStatusEnum.BLOCKING_AMOUNT_ADJUSTED,
            updatedAt: expect.anything()
          }
        ]
      });
      expect(accountRepository.updateBlockingAmount).not.toHaveBeenCalled();
      expect(blockingAmountService.updateBlockingAmount).toBeCalledWith(
        data.sourceAccountNo,
        data.blockingId,
        newAmount,
        data.transactionAmount,
        data.cardId
      );
      expect(transactionRepository.update).toBeCalledTimes(2);
    });
  });

  describe('executeBlockingTransferTransaction', () => {
    const cif = 'cif123';
    const newTransactionModel = getTransactionModelFullData();
    const transactionModel: ITransactionModel = {
      ...newTransactionModel,
      id: transactionId,
      blockingId: 'blockingId123',
      cardId: 'cardId01',
      status: TransactionStatus.SUBMITTED,
      sourceCIF: cif,
      sourceCustomerId: sourceAccount.customerId
    };

    it('should send correct transactions to core banking and update status', async () => {
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
        transactionModel
      );

      await transactionBlockingService.executeBlockingTransferTransaction(
        cif,
        transactionModel.id,
        transactionModel.transactionAmount
      );

      expect(coreBankingHelper.submitCardTransaction).toHaveBeenCalledWith(
        transactionModel.cardId,
        transactionModel.blockingId,
        transactionModel,
        transactionModel.debitTransactionCode,
        transactionModel.debitTransactionChannel
      );

      expect(transactionRepository.update).toHaveBeenCalledWith(
        transactionModel.id,
        expect.objectContaining({
          status: TransactionStatus.SUCCEED
        })
      );

      expect(coreBankingHelper.submitCreditTransaction).toHaveBeenCalledWith(
        transactionModel.beneficiaryAccountNo,
        transactionModel,
        transactionModel.creditTransactionCode,
        transactionModel.creditTransactionChannel
      );

      expect(
        transactionProducer.sendTransactionSucceededMessage
      ).toBeCalledWith(sourceAccount.customerId, {
        ...transactionModel,
        status: TransactionStatus.SUCCEED,
        journey: expect.objectContaining([
          {
            status: 'TRANSFER_SUCCESS',
            updatedAt: expect.anything()
          }
        ])
      });
    });

    it('should set status to declined if submit transaction to core banking is failed', async () => {
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
        transactionModel
      );
      (coreBankingHelper.submitCardTransaction as jest.Mock).mockRejectedValueOnce(
        undefined
      );

      const error = await transactionBlockingService
        .executeBlockingTransferTransaction(
          cif,
          transactionModel.id,
          transactionModel.transactionAmount
        )
        .catch(err => err);

      expect(error.errorCode).toBe(ERROR_CODE.TRANSACTION_FAILED);
      expect(transactionRepository.update).toBeCalledTimes(0);
      expect(coreBankingHelper.submitCreditTransaction).not.toBeCalled();
      expect(transactionProducer.sendTransactionFailedMessage).toBeCalledTimes(
        0
      );
    });

    it('should throw error when debit & credit transaction code not found', async () => {
      const transaction = { ...transactionModel };
      delete transaction.creditTransactionCode;
      delete transaction.creditTransactionChannel;
      delete transaction.debitTransactionCode;
      delete transaction.debitTransactionChannel;

      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
        transaction
      );

      const error = await transactionBlockingService
        .executeBlockingTransferTransaction(
          cif,
          transaction.id,
          transaction.transactionAmount
        )
        .catch(err => err);

      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(ERROR_CODE.TRANSACTION_FAILED);
      expect(coreBankingHelper.submitCardTransaction).not.toHaveBeenCalled();
      expect(coreBankingHelper.submitCreditTransaction).not.toHaveBeenCalled();
      expect(transactionRepository.update).toBeCalledTimes(0);
      expect(transactionProducer.sendTransactionFailedMessage).toBeCalledTimes(
        0
      );
    });

    it('should call debit transaction function when credit tc is absent & debit tc is present', async () => {
      const transaction = { ...transactionModel };
      delete transaction.creditTransactionCode;
      delete transaction.creditTransactionChannel;

      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
        transaction
      );

      await transactionBlockingService.executeBlockingTransferTransaction(
        cif,
        transaction.id,
        transaction.transactionAmount
      );

      expect(coreBankingHelper.submitCardTransaction).toHaveBeenCalledWith(
        transaction.cardId,
        transaction.blockingId,
        transaction,
        transaction.debitTransactionCode,
        transaction.debitTransactionChannel
      );
      expect(transactionRepository.update).toHaveBeenCalledWith(
        transaction.id,
        expect.objectContaining({
          status: TransactionStatus.SUCCEED
        })
      );
      expect(
        transactionProducer.sendTransactionSucceededMessage
      ).toBeCalledWith(sourceAccount.customerId, {
        ...transaction,
        status: TransactionStatus.SUCCEED,
        journey: expect.objectContaining([
          {
            status: 'TRANSFER_SUCCESS',
            updatedAt: expect.anything()
          }
        ])
      });

      expect(coreBankingHelper.submitCreditTransaction).not.toHaveBeenCalled();
    });

    it('should throw INVALI_AMOUNT error when transaction amount & request amount does not match', async () => {
      const transaction = { ...transactionModel };
      delete transaction.creditTransactionCode;
      delete transaction.creditTransactionChannel;

      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
        transaction
      );

      const error = await transactionBlockingService
        .executeBlockingTransferTransaction(cif, transaction.id, 3000)
        .catch(err => err);

      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_AMOUNT);
      expect(coreBankingHelper.submitCardTransaction).not.toHaveBeenCalled();
      expect(coreBankingHelper.submitCreditTransaction).not.toHaveBeenCalled();
      expect(transactionRepository.update).not.toHaveBeenCalled();
      expect(
        transactionProducer.sendTransactionFailedMessage
      ).not.toHaveBeenCalled();
    });
  });

  describe('cancelBlockingTransferTransaction', () => {
    it('should cancel blocking amount successfully', async () => {
      //given
      const cif = 'cif123';
      let newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        cardId: 'cardId01',
        blockingId: 'blockingId123',
        status: TransactionStatus.SUBMITTED,
        sourceCIF: cif
      };
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(data);

      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...data,
        status: TransactionStatus.DECLINED
      });

      //when
      const expected = await transactionBlockingService.cancelBlockingTransferTransaction(
        cif,
        data.id
      );
      //then
      expect(expected).toBeUndefined();
      expect(depositRepository.reverseAuthorizationHold).toBeCalledWith(
        data.cardId,
        data.blockingId
      );
      expect(transactionRepository.update).toBeCalledWith(data.id, {
        status: TransactionStatus.DECLINED,
        journey: expect.objectContaining([
          {
            status: 'TRANSFER_FAILURE',
            updatedAt: expect.anything()
          }
        ])
      });
    });
  });

  describe('manualUnblockingTransferTransaction', () => {
    it('should manual adjustment unblocking amount successfully', async () => {
      //given
      const cif = 'cif123';
      let newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        cardId: 'cardId01',
        blockingId: 'blockingId123',
        status: TransactionStatus.DECLINED,
        sourceCIF: cif
      };
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(data);

      //when
      const expected = await transactionBlockingService.manualUnblockingTransferTransaction(
        data.id
      );
      //then
      expect(expected).toBeUndefined();
      expect(depositRepository.reverseAuthorizationHold).toBeCalledWith(
        data.cardId,
        data.blockingId
      );
    });

    it('should give error when transaction status other than declined', async () => {
      //given
      const cif = 'cif123';
      let newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        cardId: 'cardId01',
        blockingId: 'blockingId123',
        status: TransactionStatus.PENDING,
        sourceCIF: cif
      };
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(data);
      let error;

      //when
      try {
        await transactionBlockingService.manualUnblockingTransferTransaction(
          data.id
        );
      } catch (e) {
        error = e;
      }
      //then
      expect(error.errorCode).toBe(ERROR_CODE.INVALID_TRANSACTION_STATUS);
    });
  });
});
