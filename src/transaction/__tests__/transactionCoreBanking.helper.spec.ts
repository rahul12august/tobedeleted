import { Http } from '@dk/module-common';
import depositRepository from '../../coreBanking/deposit.repository';
import {
  mapToCoreBankingCardTransaction,
  mapToDisbursementTransaction,
  mapToRepaymentTransaction,
  mapToPayOffTransaction
} from '../transaction.util';
import { TransactionStatus } from '../transaction.enum';
import {
  getBankCode,
  getRecommendPaymentServiceInput,
  getTransactionDetail
} from '../__mocks__/transaction.data';
import { ITransactionModel } from '../transaction.model';
import { Constant } from '@dk/module-common';
import helper from '../transactionCoreBanking.helper';
import transactionCoreBankingHelper from '../transactionCoreBanking.helper';
import { ExecutionTypeEnum } from '../transaction.enum';
import loanRepository from '../../coreBanking/loan.repository';

jest.mock('../../coreBanking/deposit.repository');
jest.mock('../../coreBanking/loan.repository');
jest.mock('../../switching/switching.repository');
jest.mock('../transactionSwitching.helper');

describe('transactionCoreBankingHelper', () => {
  const debitTransactionCode = 'tc01';
  const debitTransactionChannel = 'tc01_channel';
  const creditTransactionCode = 'tc02';
  const creditTransactionChannel = 'tc02_channel';
  const bankCode = getBankCode();
  const transactionInput = {
    ...getRecommendPaymentServiceInput(),
    paymentServiceCode: 'p01'
  };

  const idempotencyKey = '1';
  const defaultTransactionModel: ITransactionModel = {
    ...transactionInput,
    id: '123456',
    beneficiaryAccountType: 'MA',
    beneficiaryAccountName: 'beneficiary account',
    sourceAccountType: 'MA',
    sourceTransactionCurrency: 'IDR',
    beneficiaryBankCodeChannel: bankCode.channel,
    creditTransactionCode: creditTransactionCode,
    debitTransactionCode: debitTransactionCode,
    creditTransactionChannel: creditTransactionChannel,
    debitTransactionChannel: debitTransactionChannel,
    debitPriority: bankCode.firstPriority,
    fees: [
      {
        feeCode: 'feeCode',
        feeAmount: 1,
        customerTc: 'feeTc',
        customerTcChannel: 'feeChannel'
      }
    ],
    beneficiaryBankName: bankCode.name,
    billerCode: bankCode.billerCode,
    beneficiaryRemittanceCode: bankCode.remittanceCode,
    beneficiaryRtolCode: bankCode.rtolCode,
    status: TransactionStatus.PENDING,
    sourceBankCode: Constant.GIN_BANK_CODE_ID,
    categoryCode: 'c01',
    referenceId: 'sample-reference-id-value',
    externalId: 'sample-external-id-value',
    cardId: 'card01',
    blockingId: 'block01',
    idempotencyKey: 'idempotencyKey1',
    executionType: ExecutionTypeEnum.NONE_BLOCKING,
    requireThirdPartyOutgoingId: false
  };

  let transactionModel = defaultTransactionModel;
  const transactionIdOfWithdrawal = '1';
  beforeEach(() => {
    jest.resetAllMocks();
    Http.generateIdempotencyKey = jest.fn(() => idempotencyKey);

    (depositRepository.makeWithdrawal as jest.Mock).mockResolvedValue({
      id: transactionIdOfWithdrawal
    });
    (depositRepository.makeDeposit as jest.Mock).mockResolvedValue({
      id: '2'
    });
    (depositRepository.getTransactionsByInstructionId as jest.Mock).mockResolvedValue(
      []
    );
  });

  afterEach(() => {
    transactionModel = defaultTransactionModel;
  });

  describe('submitCardTransaction', () => {
    it('should submit data to core banking', async () => {
      const cardWithdrawalResponse = { encodedKey: 'encodedKey01' };
      const cardTransactionInput = mapToCoreBankingCardTransaction(
        transactionModel,
        transactionModel.debitTransactionCode,
        transactionModel.debitTransactionChannel,
        transactionModel.blockingId
      );
      (depositRepository.makeCardWithdrawal as jest.Mock).mockResolvedValue(
        cardWithdrawalResponse
      );
      const transactionId = await helper.submitCardTransaction(
        transactionModel.cardId,
        transactionModel.blockingId,
        transactionModel,
        transactionModel.debitTransactionCode,
        transactionModel.debitTransactionChannel
      );

      expect(transactionId).toBe(cardWithdrawalResponse.encodedKey);
      expect(depositRepository.makeCardWithdrawal).toHaveBeenCalledWith(
        transactionModel.cardId,
        cardTransactionInput,
        transactionModel.idempotencyKey
      );
    });
  });

  describe('submitFeesCardTransaction', () => {
    const transaction = {
      ...getTransactionDetail(),
      fees: [
        {
          feeCode: 'feeCode',
          feeAmount: 3500,
          customerTc: 'customerTc',
          customerTcChannel: 'customerTcChannel',
          subsidiary: true,
          blockingId: 'blockingId',
          idempotencyKey: 'idempotencyKey1',
          subsidiaryAmount: 10
        }
      ]
    };
    it('should submit fee info to card withdrawal', async () => {
      const cardId = 'cardId';
      (depositRepository.makeCardWithdrawal as jest.Mock).mockResolvedValueOnce(
        {
          encodedKey: 'encodedKey'
        }
      );
      await transactionCoreBankingHelper.submitFeesCardTransaction(
        cardId,
        transaction
      );
      expect(depositRepository.makeCardWithdrawal).toBeCalled();
      expect(depositRepository.executeJournalEntriesGL).toBeCalled();
    });
  });

  describe('submitFeeTransactions', () => {
    it('should submit fee info to card withdrawal', async () => {
      const cardId = 'cardId';
      const transaction = {
        ...getTransactionDetail(),
        fees: [
          {
            feeCode: 'feeCode',
            feeAmount: 3500,
            customerTc: 'customerTc',
            customerTcChannel: 'customerTcChannel',
            subsidiary: true,
            blockingId: 'blockingId',
            idempotencyKey: 'idempotencyKey1',
            subsidiaryAmount: 10
          }
        ]
      };

      await transactionCoreBankingHelper.submitFeeTransactions(
        cardId,
        transaction,
        () => 'transactionId'
      );

      expect(depositRepository.executeJournalEntriesGL).toBeCalled();
    });

    it('should not submit fee info to card withdrawal if subsidiaryAmount is 0', async () => {
      const cardId = 'cardId';
      const transaction = {
        ...getTransactionDetail(),
        fees: [
          {
            feeCode: 'feeCode',
            feeAmount: 3500,
            customerTc: 'customerTc',
            customerTcChannel: 'customerTcChannel',
            subsidiary: true,
            blockingId: 'blockingId',
            idempotencyKey: 'idempotencyKey1',
            subsidiaryAmount: 0
          }
        ]
      };

      await transactionCoreBankingHelper.submitFeeTransactions(
        cardId,
        transaction,
        () => 'transactionId'
      );

      expect(depositRepository.executeJournalEntriesGL).not.toBeCalled();
    });
  });

  describe('submitDisbursementTransaction', () => {
    const disbursementTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      disbursementTransactionModel.additionalPayload = {
        ...disbursementTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitRepaymentTransaction', () => {
    const repaymentTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    const bankIncome = 0;
    const makeRepaymentResponse = {
      id: '1'
    };
    it('should submit data to core banking', async () => {
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      repaymentTransactionModel.additionalPayload = {
        ...repaymentTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitPayoffTransaction', () => {
    const payoffTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      payoffTransactionModel.additionalPayload = {
        ...payoffTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitDisbursementTransaction', () => {
    const disbursementTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      disbursementTransactionModel.additionalPayload = {
        ...disbursementTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitRepaymentTransaction', () => {
    const repaymentTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    const bankIncome = 0;
    const makeRepaymentResponse = {
      id: '1'
    };
    it('should submit data to core banking', async () => {
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      repaymentTransactionModel.additionalPayload = {
        ...repaymentTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitPayoffTransaction', () => {
    const payoffTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      payoffTransactionModel.additionalPayload = {
        ...payoffTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitDisbursementTransaction', () => {
    const disbursementTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      disbursementTransactionModel.additionalPayload = {
        ...disbursementTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitRepaymentTransaction', () => {
    const repaymentTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    const bankIncome = 0;
    const makeRepaymentResponse = {
      id: '1'
    };
    it('should submit data to core banking', async () => {
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      repaymentTransactionModel.additionalPayload = {
        ...repaymentTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitPayoffTransaction', () => {
    const payoffTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      payoffTransactionModel.additionalPayload = {
        ...payoffTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitDisbursementTransaction', () => {
    const disbursementTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      disbursementTransactionModel.additionalPayload = {
        ...disbursementTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitRepaymentTransaction', () => {
    const repaymentTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    const bankIncome = 0;
    const makeRepaymentResponse = {
      id: '1'
    };
    it('should submit data to core banking', async () => {
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      repaymentTransactionModel.additionalPayload = {
        ...repaymentTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitPayoffTransaction', () => {
    const payoffTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      payoffTransactionModel.additionalPayload = {
        ...payoffTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitDisbursementTransaction', () => {
    const disbursementTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      disbursementTransactionModel.additionalPayload = {
        ...disbursementTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitRepaymentTransaction', () => {
    const repaymentTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    const bankIncome = 0;
    const makeRepaymentResponse = {
      id: '1'
    };
    it('should submit data to core banking', async () => {
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      repaymentTransactionModel.additionalPayload = {
        ...repaymentTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitPayoffTransaction', () => {
    const payoffTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      payoffTransactionModel.additionalPayload = {
        ...payoffTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitDisbursementTransaction', () => {
    const disbursementTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      disbursementTransactionModel.additionalPayload = {
        ...disbursementTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitRepaymentTransaction', () => {
    const repaymentTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    const bankIncome = 0;
    const makeRepaymentResponse = {
      id: '1'
    };
    it('should submit data to core banking', async () => {
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      repaymentTransactionModel.additionalPayload = {
        ...repaymentTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitPayoffTransaction', () => {
    const payoffTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      payoffTransactionModel.additionalPayload = {
        ...payoffTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitDisbursementTransaction', () => {
    const disbursementTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      disbursementTransactionModel.additionalPayload = {
        ...disbursementTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitRepaymentTransaction', () => {
    const repaymentTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    const bankIncome = 0;
    const makeRepaymentResponse = {
      id: '1'
    };
    it('should submit data to core banking', async () => {
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      repaymentTransactionModel.additionalPayload = {
        ...repaymentTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitPayoffTransaction', () => {
    const payoffTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      payoffTransactionModel.additionalPayload = {
        ...payoffTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitDisbursementTransaction', () => {
    const disbursementTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      disbursementTransactionModel.additionalPayload = {
        ...disbursementTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitRepaymentTransaction', () => {
    const repaymentTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    const bankIncome = 0;
    const makeRepaymentResponse = {
      id: '1'
    };
    it('should submit data to core banking', async () => {
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      repaymentTransactionModel.additionalPayload = {
        ...repaymentTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitPayoffTransaction', () => {
    const payoffTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      payoffTransactionModel.additionalPayload = {
        ...payoffTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitDisbursementTransaction', () => {
    const disbursementTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      disbursementTransactionModel.additionalPayload = {
        ...disbursementTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitRepaymentTransaction', () => {
    const repaymentTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    const bankIncome = 0;
    const makeRepaymentResponse = {
      id: '1'
    };
    it('should submit data to core banking', async () => {
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      repaymentTransactionModel.additionalPayload = {
        ...repaymentTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitPayoffTransaction', () => {
    const payoffTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      payoffTransactionModel.additionalPayload = {
        ...payoffTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitDisbursementTransaction', () => {
    const disbursementTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      disbursementTransactionModel.additionalPayload = {
        ...disbursementTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitRepaymentTransaction', () => {
    const repaymentTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    const bankIncome = 0;
    const makeRepaymentResponse = {
      id: '1'
    };
    it('should submit data to core banking', async () => {
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      repaymentTransactionModel.additionalPayload = {
        ...repaymentTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitPayoffTransaction', () => {
    const payoffTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      payoffTransactionModel.additionalPayload = {
        ...payoffTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitDisbursementTransaction', () => {
    const disbursementTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      disbursementTransactionModel.additionalPayload = {
        ...disbursementTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitRepaymentTransaction', () => {
    const repaymentTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    const bankIncome = 0;
    const makeRepaymentResponse = {
      id: '1'
    };
    it('should submit data to core banking', async () => {
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      repaymentTransactionModel.additionalPayload = {
        ...repaymentTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitPayoffTransaction', () => {
    const payoffTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      payoffTransactionModel.additionalPayload = {
        ...payoffTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitDisbursementTransaction', () => {
    const disbursementTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      disbursementTransactionModel.additionalPayload = {
        ...disbursementTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitRepaymentTransaction', () => {
    const repaymentTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    const bankIncome = 0;
    const makeRepaymentResponse = {
      id: '1'
    };
    it('should submit data to core banking', async () => {
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      repaymentTransactionModel.additionalPayload = {
        ...repaymentTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitPayoffTransaction', () => {
    const payoffTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      payoffTransactionModel.additionalPayload = {
        ...payoffTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitDisbursementTransaction', () => {
    const disbursementTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      disbursementTransactionModel.additionalPayload = {
        ...disbursementTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitRepaymentTransaction', () => {
    const repaymentTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    const bankIncome = 0;
    const makeRepaymentResponse = {
      id: '1'
    };
    it('should submit data to core banking', async () => {
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      repaymentTransactionModel.additionalPayload = {
        ...repaymentTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitPayoffTransaction', () => {
    const payoffTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      payoffTransactionModel.additionalPayload = {
        ...payoffTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitDisbursementTransaction', () => {
    const disbursementTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      const makeDisbursementResponse = {
        id: '1'
      };
      disbursementTransactionModel.additionalPayload = {
        ...disbursementTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const disbursementPayload = mapToDisbursementTransaction(
        disbursementTransactionModel,
        debitTransactionChannel
      );
      (loanRepository.makeDisbursement as jest.Mock).mockResolvedValue(
        makeDisbursementResponse
      );
      const transactionId = await helper.submitDisbursementTransaction(
        accountNo,
        disbursementTransactionModel,
        debitTransactionChannel
      );

      expect(transactionId).toBe(makeDisbursementResponse.id);
      expect(loanRepository.makeDisbursement).toHaveBeenCalledWith(
        accountNo,
        disbursementPayload,
        disbursementTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitRepaymentTransaction', () => {
    const repaymentTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    const bankIncome = 0;
    const makeRepaymentResponse = {
      id: '1'
    };
    it('should submit data to core banking', async () => {
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      repaymentTransactionModel.additionalPayload = {
        ...repaymentTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const repaymentPayload = mapToRepaymentTransaction(
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );
      (loanRepository.makeRepayment as jest.Mock).mockResolvedValue(
        makeRepaymentResponse
      );
      const transactionId = await helper.submitRepaymentTransaction(
        accountNo,
        repaymentTransactionModel,
        creditTransactionChannel,
        bankIncome
      );

      expect(transactionId).toBe(makeRepaymentResponse.id);
      expect(loanRepository.makeRepayment).toHaveBeenCalledWith(
        accountNo,
        repaymentPayload,
        repaymentTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });

  describe('submitPayoffTransaction', () => {
    const payoffTransactionModel = { ...transactionModel };
    const accountNo = 'test123';
    it('should submit data to core banking', async () => {
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.idempotencyKey
      );
    });

    it('should submit data to core banking with additional payload idempotency key', async () => {
      payoffTransactionModel.additionalPayload = {
        ...payoffTransactionModel.additionalPayload,
        coreBankingIdempotencyKey: 'qwe123'
      };
      const payoffPayload = mapToPayOffTransaction(
        payoffTransactionModel,
        creditTransactionChannel
      );
      (loanRepository.makePayOff as jest.Mock).mockResolvedValue(null);
      await helper.submitPayOffTransaction(
        accountNo,
        payoffTransactionModel,
        creditTransactionChannel
      );

      expect(loanRepository.makePayOff).toHaveBeenCalledWith(
        accountNo,
        payoffPayload,
        payoffTransactionModel.additionalPayload.coreBankingIdempotencyKey
      );
    });
  });
});
