import {
  BankChannelEnum,
  BankNetworkEnum,
  PaymentServiceTypeEnum
} from '@dk/module-common';
import awardRepository from '../../award/award.repository';
import { ErrorList, ERROR_CODE } from '../../common/errors';
import producer from '../../common/kafkaProducer';
import configurationRepository from '../../configuration/configuration.repository';
import configurationService from '../../configuration/configuration.service';
import {
  IBankCode,
  IPaymentServiceMapping
} from '../../configuration/configuration.type';
import {
  getBankCodeData,
  getDecisionEngineConfiguration
} from '../../configuration/__mocks__/configuration.data';
import depositRepository from '../../coreBanking/deposit.repository';
import decisionEngineService from '../../decisionEngine/decisionEngine.service';
import {
  RetryableTransferAppError,
  TransferAppError
} from '../../errors/AppError';
import virtualAccountRepository from '../../virtualAccount/virtualAccount.repository';
import { IVirtualAccount } from '../../virtualAccount/virtualAccount.type';
import transactionHelper from '../transaction.helper';
import transactionService from '../transaction.service';
import { RecommendPaymentServiceInput } from '../transaction.type';
import transactionRecommendService from '../transactionRecommend.service';
import {
  getPaymentServiceMappingData,
  getRecommendPaymentServiceTestCases
} from '../__mocks__/recommendPaymentServices.data';
import {
  getBankCode,
  getRecommendPaymentServiceInput,
  getTransactionMockValueObject,
  getWalletExternalBankCode,
  getWalletPartnerBankCode,
  TransactionRecommendationInput
} from '../__mocks__/transaction.data';
import { TransferFailureReasonActor } from '../transaction.enum';
import { isFeatureEnabled } from '../../common/featureFlag';

jest.mock('../../configuration/configuration.repository');
jest.mock('../../configuration/configuration.service');
jest.mock('../../logger');
jest.mock('../../coreBanking/deposit.repository');
jest.mock('../../award/award.repository');
jest.mock('../../account/account.repository');
jest.mock('../../virtualAccount/virtualAccount.repository');
jest.mock('../../common/featureFlag');
jest.mock('../transaction.helper', () => {
  const original = require.requireActual('../transaction.helper').default;
  return {
    ...original,
    mapAuthenticationRequired: jest.fn(),
    mapFeeData: jest.fn(),
    getAccountInfo: jest.fn(),
    getSourceAccount: jest.fn(),
    validateTransactionLimitAmount: jest.fn(),
    getPaymentServiceMappings: jest.fn(),
    getBeneficiaryAccount: jest.fn()
  };
});
jest.mock('../../decisionEngine/decisionEngine.service.ts');

describe('transactionRecommendService', () => {
  const {
    sourceAccount,
    sourceBankCode,
    beneficiaryBankCode,
    beneficiaryAccount,
    ruleConfigs,
    limitGroups,
    usageCounters,
    externalBankCode
  } = getTransactionMockValueObject();

  const feeRule = { basicFee: { code: 'f01', feeAmountFixed: 1 } };
  const serviceMappingResult: IPaymentServiceMapping[] = [
    {
      paymentServiceCode: 'p01',
      debitTransactionCode: [
        {
          transactionCode: 'abc',
          transactionCodeInfo: {
            limitGroupCode: 'l01',
            feeRules: 'f01',
            code: 'abc',
            minAmountPerTx: 1,
            maxAmountPerTx: 999999,
            channel: BankChannelEnum.INTERNAL
          }
        }
      ],
      transactionAuthenticationChecking: true,
      beneficiaryBankCodeType: 'INTERNAL',
      blocking: false,
      requireThirdPartyOutgoingId: false
    }
  ];
  const zeroFeeData = { feeAmount: 0 };
  const mockReturnZeroFee = () => {
    (transactionHelper.mapFeeData as jest.Mock).mockResolvedValue([
      { ...serviceMappingResult[0], zeroFeeData }
    ]);
  };

  const mockReturnDefaultServiceMappingResult = () => {
    (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
      serviceMappingResult
    );
  };
  const mockRejectedVirtualAccountOnce = () => {
    (virtualAccountRepository.inquiryVirtualAccount as jest.Mock).mockRejectedValueOnce(
      new Error()
    );
  };
  const mockedWalletPartnerBankCode = getWalletPartnerBankCode();
  const mockedVirtualAccount: IVirtualAccount = {
    customerName: 'test customer name',
    institution: {
      name: 'gojek',
      bankCode: mockedWalletPartnerBankCode.bankCodeId,
      accountId: '2384738473',
      cif: 'insitution-cif'
    }
  };
  const mockReturnVirtualAccount = () => {
    (virtualAccountRepository.inquiryVirtualAccount as jest.Mock).mockResolvedValue(
      mockedVirtualAccount
    );
  };
  const mockReturnVirtualAccountOnce = (mockedAccount: IVirtualAccount) => {
    (virtualAccountRepository.inquiryVirtualAccount as jest.Mock).mockResolvedValueOnce(
      mockedAccount
    );
  };
  const mockReturnBankCodeOnce = (mockedBankCode: IBankCode) => {
    (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValueOnce(
      mockedBankCode
    );
  };

  const resetBankCodeMock = () => {
    (configurationRepository.getBankCodeMapping as jest.Mock).mockReset();
  };

  const mockSourceAccount = () => {
    (transactionHelper.getSourceAccount as jest.Mock).mockResolvedValueOnce({
      ...sourceAccount
    });
  };

  const setupLegacyFeatureFlag = () => {
    const confIsFeatureEnabled = isFeatureEnabled as jest.Mock;
    confIsFeatureEnabled.mockImplementation(() => {
      return true;
    });
  };

  beforeEach(() => {
    (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValue(
      999999999
    );

    (configurationRepository.getBankCodeMapping as jest.Mock).mockImplementation(
      code => {
        if (code === beneficiaryBankCode.bankCodeId) {
          return beneficiaryBankCode;
        }
        if (code === sourceBankCode.bankCodeId) {
          return sourceBankCode;
        }
        if (code === externalBankCode.bankCodeId) {
          return externalBankCode;
        }
        return;
      }
    );

    (transactionHelper.mapAuthenticationRequired as jest.Mock).mockImplementation(
      value => value
    );

    (transactionHelper.mapFeeData as jest.Mock).mockImplementation(
      value => value
    );

    (transactionHelper.getAccountInfo as jest.Mock).mockImplementation(
      accountNo => {
        if (accountNo === sourceAccount.accountNumber) {
          return sourceAccount;
        }
        if (accountNo === beneficiaryAccount.accountNumber) {
          return beneficiaryAccount;
        }
        return null;
      }
    );

    (transactionHelper.getBeneficiaryAccount as jest.Mock).mockImplementation(
      accountNo => {
        if (accountNo === sourceAccount.accountNumber) {
          return sourceAccount;
        }
        if (accountNo === beneficiaryAccount.accountNumber) {
          return beneficiaryAccount;
        }
        return null;
      }
    );

    (transactionHelper.getSourceAccount as jest.Mock).mockImplementation(
      accountNo => {
        if (accountNo === sourceAccount.accountNumber) {
          return sourceAccount;
        }
        if (accountNo === beneficiaryAccount.accountNumber) {
          return beneficiaryAccount;
        }
        return null;
      }
    );

    (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValue([
      getPaymentServiceMappingData()
    ]);

    (decisionEngineService.runMultipleRoles as jest.Mock).mockResolvedValue(
      true
    );
    (configurationRepository.getDecisionEngineConfig as jest.Mock).mockResolvedValue(
      getDecisionEngineConfiguration()
    );

    // Toggle the legacy feature flag to use the original implementation
    setupLegacyFeatureFlag();
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('pass validate recommendPaymentService input', async () => {
    // given
    const recommendInput = getRecommendPaymentServiceInput();

    // when
    await transactionService
      .recommendPaymentService(recommendInput)
      .catch(err => err);

    // then
    expect(configurationRepository.getBankCodeMapping).toBeCalledWith(
      recommendInput.beneficiaryBankCode
    );
    expect(configurationRepository.getBankCodeMapping).toBeCalledTimes(1);
  });

  it('should throw error if beneficiary account is the same as source account', async () => {
    // given
    const recommendInput = getRecommendPaymentServiceInput();
    recommendInput.beneficiaryCIF = recommendInput.sourceCIF;
    recommendInput.beneficiaryAccountNo = recommendInput.sourceAccountNo;

    // when
    const error = await transactionService
      .recommendPaymentService(recommendInput)
      .catch(err => err);

    // then
    expect(error.errorCode).toBe(ERROR_CODE.INVALID_ACCOUNT);
  });

  it('should throw error if category code input but not valid', async () => {
    const recommendInput = getRecommendPaymentServiceInput();
    (configurationRepository.getTransactionCategory as jest.Mock).mockResolvedValueOnce(
      undefined
    );
    const error = await transactionService
      .recommendPaymentService(recommendInput)
      .catch(err => err);
    expect(error.errorCode).toEqual(
      ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED
    );
  });

  it('should throw error if payment config rules is null or undefined', async () => {
    const recommendInput = getRecommendPaymentServiceInput();
    (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValueOnce(
      undefined
    );
    const error = await transactionService
      .recommendPaymentService(recommendInput)
      .catch(err => err);
    expect(error.errorCode).toEqual(
      ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED
    );
  });

  it('should throw error when there are not recommend service codes', async () => {
    const recommendInput = getRecommendPaymentServiceInput();
    (transactionHelper.getPaymentServiceMappings as jest.Mock).mockResolvedValueOnce(
      []
    );
    const error = await transactionService
      .recommendPaymentService(recommendInput)
      .catch(err => err);
    expect(error.errorCode).toEqual(
      ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED
    );
  });

  it('should throw error when getPaymentServiceMappingResults return empty', async () => {
    const recommendInput = getRecommendPaymentServiceInput();
    const error = await transactionService
      .recommendPaymentService(recommendInput)
      .catch(err => err);
    expect(error.errorCode).toEqual(
      ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED
    );
  });

  it('should throw error when input beneficiaryCIF and sourceCIF are the same', async () => {
    const recommendInput = {
      ...getRecommendPaymentServiceInput(),
      beneficiaryCIF: 'same_person',
      sourceCIF: 'same_person'
    };

    const error = await transactionService
      .recommendPaymentService(recommendInput)
      .catch(err => err);
    expect(error.errorCode).toEqual(
      ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED
    );
  });

  it('should throw error if limit group code is null or empty', async () => {
    const recommendInput = getRecommendPaymentServiceInput();
    (configurationRepository.getLimitGroupsByCodes as jest.Mock).mockResolvedValueOnce(
      undefined
    );
    const error = await transactionService
      .recommendPaymentService(recommendInput)
      .catch(err => err);
    expect(error.errorCode).toEqual(
      ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED
    );
  });

  it('should throw error if payment config rules are not found', async () => {
    const recommendInput = getRecommendPaymentServiceInput();
    (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValueOnce(
      undefined
    );

    const error = await transactionService
      .recommendPaymentService(recommendInput)
      .catch(err => err);

    expect(error.errorCode).toBe(ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED);
  });

  it('should throw error if limit groups are not found', async () => {
    const recommendInput = getRecommendPaymentServiceInput();
    (configurationService.getPaymentConfigRules as jest.Mock).mockResolvedValueOnce(
      {}
    );
    (configurationRepository.getLimitGroupsByCodes as jest.Mock).mockResolvedValueOnce(
      []
    );

    const error = await transactionService
      .recommendPaymentService(recommendInput)
      .catch(err => err);

    expect(error.errorCode).toBe(ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED);
  });

  it('should fail to run createTransfer when call bankCode is undefined', async () => {
    // given
    const recommendInput = getRecommendPaymentServiceInput();

    (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValueOnce(
      undefined
    );

    // when
    const error = await transactionService
      .recommendPaymentService(recommendInput)
      .catch(error => error);

    expect(error).toBeInstanceOf(TransferAppError);
    expect(error.detail).toBe(
      'Failed to get beneficiary bank code info for: BC002!'
    );
    expect(error.actor).toBe(TransferFailureReasonActor.MS_TRANSFER);
    expect(error.errorCode).toBe(ERROR_CODE.INVALID_REGISTER_PARAMETER);
  });

  it('should fail to run createTransfer when call getTransferConfigurations return null', async () => {
    // given
    const recommendInput = getRecommendPaymentServiceInput();
    (configurationRepository.getTransactionCategory as jest.Mock).mockResolvedValueOnce(
      null
    );

    // when
    const error = await transactionService
      .recommendPaymentService(recommendInput)
      .catch(error => error);

    // then
    expect(error).toBeInstanceOf(RetryableTransferAppError);
    expect(error.errorCode).toBe(ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED);
  });

  it('should fail to run createTransfer when call bankName is wrong', async () => {
    // given
    const bankCode = {
      ...getBankCode(),
      name: 'NIG'
    };
    const recommendInput = getRecommendPaymentServiceInput();

    (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValue(
      bankCode
    );

    // when
    const error = await transactionService
      .recommendPaymentService(recommendInput)
      .catch(error => error);

    // then
    expect(error).toBeInstanceOf(RetryableTransferAppError);
    expect(error.errorCode).toBe(ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED);
  });

  it('should fail to run createTransfer when call transactionCategory are undefined', async () => {
    // given
    const recommendInput = getRecommendPaymentServiceInput();

    (configurationRepository.getTransactionCategory as jest.Mock).mockResolvedValueOnce(
      null
    );

    // when
    const error = await transactionService
      .recommendPaymentService(recommendInput)
      .catch(error => error);

    // then
    expect(error).toBeInstanceOf(RetryableTransferAppError);
    expect(error.errorCode).toBe(ERROR_CODE.PAYMENT_CONFIG_RULE_FETCH_FAILED);
  });

  it('should fail to run createTransfer when call bank code is null from configuration', async () => {
    // given
    const recommendInput = getRecommendPaymentServiceInput();

    (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValue(
      null
    );

    // when
    const error = await transactionService
      .recommendPaymentService(recommendInput)
      .catch(error => error);

    // then
    expect(error).toBeInstanceOf(TransferAppError);
    expect(error.detail).toBe(
      'Failed to get beneficiary bank code info for: BC002!'
    );
    expect(error.actor).toBe(TransferFailureReasonActor.MS_TRANSFER);
    expect(error.errorCode).toBe(ERROR_CODE.INVALID_REGISTER_PARAMETER);
  });

  describe('return recommended services', () => {
    const testCases = getRecommendPaymentServiceTestCases();
    const testCaseTransferMAToMA = testCases[1];
    const input = testCaseTransferMAToMA.input;

    const recommendPaymentServiceInput: RecommendPaymentServiceInput = {
      sourceCIF: input.sourceCIF,
      sourceAccountNo: input.sourceAccountNo,
      beneficiaryCIF: input.beneficiaryCIF,
      beneficiaryAccountNo: input.beneficiaryAccountNo,
      beneficiaryBankCode: input.beneficiaryBankCode,
      paymentServiceType: input.paymentServiceType,
      transactionAmount: input.transactionAmount,
      externalId: input.externalId
    };

    const recommendPaymentServiceInputPartnerWallet: RecommendPaymentServiceInput = {
      sourceCIF: input.sourceCIF,
      beneficiaryAccountNo: '2387484343', // wallet ID - identity/phone number
      beneficiaryBankCode: mockedWalletPartnerBankCode.bankCodeId,
      paymentServiceType: PaymentServiceTypeEnum.WALLET,
      transactionAmount: input.transactionAmount
    };

    beforeEach(() => {
      (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValueOnce(
        ruleConfigs
      );
      (configurationService.getPaymentConfigRules as jest.Mock).mockResolvedValueOnce(
        ruleConfigs
      );
      (configurationRepository.getLimitGroupsByCodes as jest.Mock).mockResolvedValueOnce(
        limitGroups
      );
      (configurationRepository.getFeeRule as jest.Mock).mockResolvedValueOnce(
        feeRule
      );
      (awardRepository.getUsageCounters as jest.Mock).mockResolvedValueOnce(
        usageCounters
      );
      (transactionHelper.getSourceAccount as jest.Mock).mockResolvedValueOnce({
        ...sourceAccount
      });
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should throw error if source balance cannot pay fee', async () => {
      // Given
      const feeData = [{ feeAmount: 1 }];
      mockReturnDefaultServiceMappingResult();

      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        { ...serviceMappingResult[0], feeData }
      ]);

      (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValueOnce(
        recommendPaymentServiceInput.transactionAmount
      );

      // When
      const error = await transactionService
        .recommendPaymentService(recommendPaymentServiceInput)
        .catch(e => e);

      // Then
      expect(error.errorCode).toEqual(
        ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
      );
    });

    it('should send Notif_Debit_Card_Insufficient_Balance if source balance is insufficient and transaction is debit card', async () => {
      // Given
      const feeData = [{ feeAmount: 1 }];
      mockReturnDefaultServiceMappingResult();

      (configurationService.isDebitCardTransactionGroup as jest.Mock).mockResolvedValue(
        true
      );

      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        { ...serviceMappingResult[0], feeData }
      ]);

      (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValueOnce(
        recommendPaymentServiceInput.transactionAmount
      );

      // When
      const error = await transactionRecommendService
        .getRecommendService(recommendPaymentServiceInput)
        .catch(e => e);

      // Then
      expect(error.errorCode).toEqual(
        ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
      );
      expect(producer.sendSerializedValue).toBeCalled();
    });

    it('should not send Notif_Debit_Card_Insufficient_Balance if source balance is insufficient and transaction is not debit card', async () => {
      // Given
      const feeData = [{ feeAmount: 1 }];
      mockReturnDefaultServiceMappingResult();

      (configurationService.isDebitCardTransactionGroup as jest.Mock).mockResolvedValue(
        false
      );

      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        { ...serviceMappingResult[0], feeData }
      ]);

      (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValueOnce(
        recommendPaymentServiceInput.transactionAmount
      );

      // When
      const error = await transactionRecommendService
        .getRecommendService(recommendPaymentServiceInput)
        .catch(e => e);

      // Then
      expect(error.errorCode).toEqual(
        ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
      );
      expect(producer.sendSerializedValue).not.toBeCalled();
    });
    it.each([
      [
        true,
        {
          amount: 117100.0,
          fee: 2900,
          balance: 120000.0
        }
      ],
      [
        true,
        {
          amount: 261247.67,
          fee: 2900,
          balance: 264147.67
        }
      ],
      [
        true,
        {
          amount: 924766.95,
          fee: 2900,
          balance: 927666.95
        }
      ],
      [
        true,
        {
          amount: 31490.99,
          fee: 2900,
          balance: 34390.99
        }
      ],
      [
        true,
        {
          amount: 31684.99,
          fee: 2900,
          balance: 34584.99
        }
      ],
      [
        true,
        {
          amount: 62861.98,
          fee: 2900,
          balance: 65761.98
        }
      ],
      [
        false,
        {
          amount: 62861.99,
          fee: 2900,
          balance: 65761.98
        }
      ]
    ])(
      'Should return %s when balance & amount is %j',
      async (expected: any, rules: any) => {
        // Given
        const feeData = [{ feeAmount: rules.fee }];
        mockReturnDefaultServiceMappingResult();
        const recommendPaymentServiceInputData = {
          ...recommendPaymentServiceInput,
          transactionAmount: rules.amount
        };

        (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
          { ...serviceMappingResult[0], feeData }
        ]);

        (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValueOnce(
          rules.balance
        );

        // When
        const error = await transactionService
          .recommendPaymentService(recommendPaymentServiceInputData)
          .catch(e => e);

        // Then
        expect(error.errorCode === undefined).toEqual(expected);
      }
    );

    it('should return recommended services with fee data', async () => {
      // Given
      mockReturnDefaultServiceMappingResult();
      const bankCode = getBankCodeData();
      bankCode.bankCodeId = 'BC002';
      resetBankCodeMock();
      mockReturnBankCodeOnce({
        ...bankCode,
        firstPriority: BankNetworkEnum.INTERNAL,
        isInternal: true
      });
      mockReturnBankCodeOnce({
        ...bankCode,
        firstPriority: BankNetworkEnum.ARTAJASA,
        isInternal: false
      });

      mockReturnZeroFee();
      const feeDataTxnInput: TransactionRecommendationInput = {
        transactionAmount: recommendPaymentServiceInput.transactionAmount,
        paymentServiceType: recommendPaymentServiceInput.paymentServiceType,
        sourceCustomerId: sourceAccount.customerId,
        beneficiaryBankCode: recommendPaymentServiceInput.beneficiaryBankCode,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        externalId: input.externalId
      };

      const expected = [
        {
          ...serviceMappingResult[0],
          zeroFeeData
        }
      ];

      // When
      const result = await transactionService.recommendPaymentService({
        ...recommendPaymentServiceInput,
        sourceBankCode: bankCode.bankCodeId
      });

      // Then
      expect(result).toEqual(expected);
      expect(transactionHelper.mapFeeData).toBeCalledWith(
        [
          {
            beneficiaryBankCode: undefined,
            beneficiaryBankCodeType: 'INTERNAL',
            debitTransactionCode: [
              {
                transactionCode: 'abc',
                transactionCodeInfo: {
                  code: 'abc',
                  feeRules: 'f01',
                  limitGroupCode: 'l01',
                  maxAmountPerTx: 999999,
                  minAmountPerTx: 1,
                  channel: BankChannelEnum.INTERNAL
                }
              }
            ],
            paymentServiceCode: 'p01',
            transactionAuthenticationChecking: true,
            blocking: false,
            requireThirdPartyOutgoingId: false
          }
        ],
        expect.objectContaining(feeDataTxnInput),
        usageCounters
      );
      expect(transactionHelper.mapAuthenticationRequired).toBeCalledWith(
        expected,
        recommendPaymentServiceInput.transactionAmount,
        recommendPaymentServiceInput.sourceAccountNo
      );
    });

    it('should return recommended services with undefined fee data if fee is not found', async () => {
      // Given
      const serviceMappingResult: IPaymentServiceMapping[] = [
        {
          ...getPaymentServiceMappingData(),
          paymentServiceCode: 'p01'
        }
      ];
      let paymentServiceMapping = serviceMappingResult[0];
      let transactionCodeMapping =
        paymentServiceMapping?.debitTransactionCode?.[0];
      if (transactionCodeMapping) {
        transactionCodeMapping.transactionCodeInfo.limitGroupCode = 'l01';
        transactionCodeMapping.transactionCodeInfo.feeRules = undefined;
      }
      (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
        serviceMappingResult
      );
      const expected = [
        {
          ...paymentServiceMapping,
          feeData: undefined,
          beneficiaryBankCode: '542'
        }
      ];

      // When
      const result = await transactionService.recommendPaymentService({
        ...recommendPaymentServiceInput,
        sourceBankCode: sourceBankCode.bankCodeId
      });

      // Then
      expect(result).toEqual(expected);
    });

    it('should return recommended services without run through DC engine', async () => {
      // Given
      const executorCIF = undefined;
      const ownerSourceCIF = 'ownerSCIF';
      const beneficiaryCIF = 'bCIF';
      const ownerBeneficiaryCIF = 'ownerBCIF';
      const serviceMappingResult: IPaymentServiceMapping[] = [
        {
          ...getPaymentServiceMappingData(),
          paymentServiceCode: 'p01'
        }
      ];
      (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
        serviceMappingResult
      );
      const expected = [
        {
          ...serviceMappingResult[0],
          feeData: undefined,
          beneficiaryBankCode: '542'
        }
      ];

      (transactionHelper.getSourceAccount as jest.Mock).mockResolvedValueOnce({
        ...sourceAccount,
        cif: ownerSourceCIF
      });

      (transactionHelper.getAccountInfo as jest.Mock).mockResolvedValueOnce({
        ...beneficiaryAccount,
        cif: ownerBeneficiaryCIF
      });

      // When
      const result = await transactionService.recommendPaymentService({
        ...recommendPaymentServiceInput,
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        sourceCIF: executorCIF,
        beneficiaryCIF
      });

      // Then
      expect(decisionEngineService.runMultipleRoles).toBeCalledTimes(0);
      expect(result).toEqual(expected);
    });

    it('should throw error when not pass DC engine', async () => {
      // Given
      const executorCIF = 'sCIF';
      const ownerSourceCIF = 'ownerSCIF';
      const beneficiaryCIF = 'bCIF';
      const ownerBeneficiaryCIF = 'ownerBCIF';
      const serviceMappingResult: IPaymentServiceMapping[] = [
        {
          ...getPaymentServiceMappingData(),
          paymentServiceCode: 'p01'
        }
      ];
      (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
        serviceMappingResult
      );

      (transactionHelper.getSourceAccount as jest.Mock).mockResolvedValueOnce({
        ...sourceAccount,
        cif: ownerSourceCIF
      });

      (transactionHelper.getAccountInfo as jest.Mock).mockResolvedValueOnce({
        ...beneficiaryAccount,
        cif: ownerBeneficiaryCIF
      });
      (decisionEngineService.runMultipleRoles as jest.Mock).mockReturnValueOnce(
        false
      );

      // When
      const result = await transactionService
        .recommendPaymentService({
          ...recommendPaymentServiceInput,
          paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
          sourceCIF: executorCIF,
          beneficiaryCIF
        })
        .catch(error => error);

      // Then
      expect(decisionEngineService.runMultipleRoles).toBeCalledTimes(1);
      expect(result.errorCode).toEqual(ERROR_CODE.NO_RECOMMENDATION_SERVICES);
    });

    it('should throw error when can not find out DC engine config', async () => {
      // Given
      const executorCIF = 'sCIF';
      const ownerSourceCIF = 'ownerSCIF';
      const beneficiaryCIF = 'bCIF';
      const ownerBeneficiaryCIF = 'ownerBCIF';
      const serviceMappingResult: IPaymentServiceMapping[] = [
        {
          ...getPaymentServiceMappingData(),
          paymentServiceCode: 'p01'
        }
      ];
      (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
        serviceMappingResult
      );

      (transactionHelper.getSourceAccount as jest.Mock).mockResolvedValueOnce({
        ...sourceAccount,
        cif: ownerSourceCIF
      });

      (transactionHelper.getAccountInfo as jest.Mock).mockResolvedValueOnce({
        ...beneficiaryAccount,
        cif: ownerBeneficiaryCIF
      });
      (configurationRepository.getDecisionEngineConfig as jest.Mock).mockResolvedValueOnce(
        undefined
      );

      // When
      const result = await transactionService
        .recommendPaymentService({
          ...recommendPaymentServiceInput,
          paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
          sourceCIF: executorCIF,
          beneficiaryCIF
        })
        .catch(error => error);

      // Then
      expect(result.errorCode).toEqual(
        ERROR_CODE.DECISION_ENGINE_CONFIG_NOT_FOUND
      );
      expect(decisionEngineService.runMultipleRoles).toBeCalledTimes(0);
    });

    it('should return recommended services with undefined fee data if limit group code is not found', async () => {
      // Given
      const serviceMappingResult: IPaymentServiceMapping[] = [
        {
          ...getPaymentServiceMappingData(),
          paymentServiceCode: 'p01'
        }
      ];
      const paymentServiceMapping = serviceMappingResult[0];
      const debitTransactionCode =
        paymentServiceMapping.debitTransactionCode?.[0];
      if (debitTransactionCode) {
        debitTransactionCode.transactionCodeInfo.limitGroupCode = undefined;
        debitTransactionCode.transactionCodeInfo.feeRules = 'f01';
      }
      (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
        serviceMappingResult
      );

      const expected = [
        {
          ...paymentServiceMapping,
          feeData: undefined,
          beneficiaryBankCode: '542'
        }
      ];

      // When
      const result = await transactionService.recommendPaymentService({
        ...recommendPaymentServiceInput,
        sourceBankCode: sourceBankCode.bankCodeId
      });

      // Then
      expect(result).toEqual(expected);
    });

    it('should throw error if recommended service is not found', async () => {
      // given
      (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
        []
      );

      // when
      const result = await transactionService
        .recommendPaymentService(recommendPaymentServiceInput)
        .catch(err => err);

      // then
      expect(result.errorCode).toBe(ERROR_CODE.NO_RECOMMENDATION_SERVICES);
    });

    describe('RecommendPaymentService for Wallet Payment Type', () => {
      beforeEach(() => {
        resetBankCodeMock();
        mockSourceAccount();
      });
      describe('Partner Wallet', () => {
        const walletIdAsBeneficiaryAcccountNo = `${recommendPaymentServiceInputPartnerWallet.beneficiaryAccountNo}`;

        beforeEach(() => {
          mockReturnBankCodeOnce(mockedWalletPartnerBankCode);
          mockReturnBankCodeOnce(sourceBankCode);
        });
        afterEach(() => {
          resetBankCodeMock();
        });

        it('should retrieve virtual account and main account then return recommended service for partner', async () => {
          // ARRANGE
          mockReturnVirtualAccount();
          mockReturnDefaultServiceMappingResult();

          const expected = [
            {
              ...serviceMappingResult[0]
            }
          ];

          // ACT
          const result = await transactionService.recommendPaymentService(
            recommendPaymentServiceInputPartnerWallet
          );

          // ASSERT

          expect(result).toEqual(expected);
          expect(transactionHelper.mapAuthenticationRequired).toBeCalledWith(
            expected,
            recommendPaymentServiceInputPartnerWallet.transactionAmount,
            recommendPaymentServiceInputPartnerWallet.sourceAccountNo
          );
          expect(virtualAccountRepository.inquiryVirtualAccount).toBeCalledWith(
            walletIdAsBeneficiaryAcccountNo
          );
          expect(configurationRepository.getBankCodeMapping).toBeCalledTimes(2);
        });
        it('should throw validation error when institution bank code is different than input beneficiary bank code', async () => {
          const virtualAccount = {
            ...mockedVirtualAccount,
            institution: {
              ...mockedVirtualAccount.institution,
              bankCode: '28734'
            }
          };
          mockReturnVirtualAccountOnce(virtualAccount);
          const error = await transactionService
            .recommendPaymentService(recommendPaymentServiceInputPartnerWallet)
            .catch(err => err);
          expect(error.errorCode).toEqual(
            ERROR_CODE.INVALID_REGISTER_PARAMETER
          );
        });
        it('should throw app error when failing to retrieve virtual-account', async () => {
          // ARRANGE

          mockRejectedVirtualAccountOnce();

          // ACT
          const error = await transactionService
            .recommendPaymentService(recommendPaymentServiceInputPartnerWallet)
            .catch(e => e);

          // ASSERT
          expect(error).toBeInstanceOf(Error);
        });
      });
      describe('External Wallet', () => {
        it('should return recommended service for external wallet', async () => {
          // ARRANGE
          const walletExternalBankCode = getWalletExternalBankCode();
          const walletIdAsBeneficiaryAcccountNo = `${recommendPaymentServiceInputPartnerWallet.beneficiaryAccountNo}`;
          const beneficiaryCif = undefined;
          mockReturnBankCodeOnce(walletExternalBankCode);
          mockReturnBankCodeOnce(sourceBankCode);
          mockReturnDefaultServiceMappingResult();

          const expected = [
            {
              ...serviceMappingResult[0]
            }
          ];
          const paymentInput = {
            ...recommendPaymentServiceInputPartnerWallet,
            beneficiaryBankCode: walletExternalBankCode.bankCodeId
          };
          // ACT
          const result = await transactionService.recommendPaymentService(
            paymentInput
          );

          // ASSERT
          expect(result).toEqual(expected);
          expect(transactionHelper.mapAuthenticationRequired).toBeCalledWith(
            expected,
            paymentInput.transactionAmount,
            paymentInput.sourceAccountNo
          );
          expect(
            virtualAccountRepository.inquiryVirtualAccount
          ).not.toBeCalled();

          expect(configurationRepository.getBankCodeMapping).toBeCalledTimes(2);
          expect(transactionHelper.getAccountInfo).toHaveBeenCalledWith(
            walletIdAsBeneficiaryAcccountNo,
            beneficiaryCif,
            walletExternalBankCode,
            undefined,
            paymentInput.transactionAmount
          );
        });
      });

      it('should throw INVALID REQUEST when missing sourceCif for Wallet Payment Type', async () => {
        // given
        const input = {
          ...recommendPaymentServiceInputPartnerWallet,
          sourceCIF: undefined
        };
        const expectedErrorCode = ERROR_CODE.INVALID_MANDATORY_FIELDS;
        const expectedErrorKeyField = 'sourceCIF';

        // when
        const error = await transactionService
          .recommendPaymentService(input)
          .catch(e => e);

        // then
        expect(error).toBeInstanceOf(TransferAppError);
        expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
        expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
        expect(error.detail).toEqual('Invalid mandatory fields!');
        expect(error.errors[0]).toEqual({
          code: expectedErrorCode,
          key: expectedErrorKeyField,
          message: ErrorList[expectedErrorCode].message
        });
      });

      it('should throw INVALID REQUEST when missing beneficiaryAccountNo for Wallet Payment Type', async () => {
        // given
        const input = {
          ...recommendPaymentServiceInputPartnerWallet,
          beneficiaryAccountNo: undefined
        };
        const expectedErrorCode = ERROR_CODE.INVALID_MANDATORY_FIELDS;
        const expectedErrorKeyField = 'beneficiaryAccountNo';

        // when
        const error = await transactionService
          .recommendPaymentService(input)
          .catch(e => e);

        // then
        expect(error).toBeInstanceOf(TransferAppError);
        expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
        expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
        expect(error.detail).toEqual('Invalid mandatory fields!');
        expect(error.errors[0]).toEqual({
          code: expectedErrorCode,
          key: expectedErrorKeyField,
          message: ErrorList[expectedErrorCode].message
        });
      });

      it('should throw INVALID REQUEST when missing beneficiaryBankCode for Wallet Payment Type', async () => {
        // given
        const input = {
          ...recommendPaymentServiceInputPartnerWallet,
          beneficiaryBankCode: undefined
        };
        const expectedErrorCode = ERROR_CODE.INVALID_MANDATORY_FIELDS;
        const expectedErrorKeyField = 'beneficiaryBankCode';

        // when
        const error = await transactionService
          .recommendPaymentService(input)
          .catch(e => e);

        // then
        expect(error).toBeInstanceOf(TransferAppError);
        expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
        expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
        expect(error.detail).toEqual('Invalid mandatory fields!');
        expect(error.errors[0]).toEqual({
          code: expectedErrorCode,
          key: expectedErrorKeyField,
          message: ErrorList[expectedErrorCode].message
        });
      });
    });
  });

  describe('getRecommendService method', () => {
    const testCases = getRecommendPaymentServiceTestCases();
    const testCaseTransferMAToMA = testCases[1];
    const input = testCaseTransferMAToMA.input;

    const recommendPaymentServiceInput: RecommendPaymentServiceInput = {
      sourceCIF: input.sourceCIF,
      sourceAccountNo: input.sourceAccountNo,
      beneficiaryCIF: input.beneficiaryCIF,
      beneficiaryAccountNo: input.beneficiaryAccountNo,
      beneficiaryBankCode: input.beneficiaryBankCode,
      paymentServiceType: input.paymentServiceType,
      transactionAmount: input.transactionAmount
    };

    beforeEach(() => {
      (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValueOnce(
        ruleConfigs
      );
      (configurationService.getPaymentConfigRules as jest.Mock).mockResolvedValueOnce(
        ruleConfigs
      );
      (configurationRepository.getLimitGroupsByCodes as jest.Mock).mockResolvedValueOnce(
        limitGroups
      );
      (configurationRepository.getFeeRule as jest.Mock).mockResolvedValueOnce(
        feeRule
      );
      (awardRepository.getUsageCounters as jest.Mock).mockResolvedValueOnce(
        usageCounters
      );
      (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValue(
        999999999
      );
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    const transactionInput = {
      ...recommendPaymentServiceInput,
      sourceAccountInfo: {
        accountNumber: recommendPaymentServiceInput.sourceAccountNo
      }
    };

    describe('if input paymentServiceCode', () => {
      it('should throw if no matching paymentServiceCode from recommendation', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            paymentServiceCode: 'p01'
          }
        ];
        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const error = await transactionRecommendService
          .getRecommendService({
            refund: { originalTransactionId: 'originalTransactionId' },
            ...transactionInput,
            paymentServiceCode: 'not a code'
          })
          .catch(e => e);

        // Then
        expect(error.errorCode).toEqual(
          ERROR_CODE.INCONSISTENT_PAYMENT_SERVICE
        );
      });

      it('should return recommendation that match paymentServiceCode', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            paymentServiceCode: 'p01'
          }
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          refund: { originalTransactionId: 'originalTransactionId' },
          ...transactionInput,
          paymentServiceCode: serviceMappingResult[0].paymentServiceCode
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
      });

      it('should return recommendation that match paymentServiceCode with OFFER', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            paymentServiceCode: 'OFFER',
            debitTransactionCode: []
          }
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          ...transactionInput,
          paymentServiceType: PaymentServiceTypeEnum.OFFER,
          sourceAccountNo: undefined
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
        expect(depositRepository.getAvailableBalance).not.toBeCalled();
      });

      it('should return recommendation that match paymentServiceCode with BONUS_INTEREST', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            paymentServiceCode: 'BONUS_INTEREST',
            debitTransactionCode: []
          }
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          ...transactionInput,
          paymentServiceType: PaymentServiceTypeEnum.BONUS_INTEREST,
          sourceAccountNo: undefined
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
        expect(depositRepository.getAvailableBalance).not.toBeCalled();
      });

      it('should return recommendation that match paymentServiceCode with REPAYMENT', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            paymentServiceCode: 'REPAYMENT'
          }
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          ...transactionInput,
          paymentServiceType: PaymentServiceTypeEnum.LOAN_REPAYMENT,
          sourceAccountNo: undefined
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
        expect(depositRepository.getAvailableBalance).not.toBeCalled();
      });

      it('should return recommendation that match paymentServiceCode with REPAYMENT_FROM_GL', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            paymentServiceCode: 'REPAYMENT_FROM_GL'
          }
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          ...transactionInput,
          paymentServiceType: PaymentServiceTypeEnum.LOAN_GL_REPAYMENT,
          sourceAccountNo: undefined
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
        expect(depositRepository.getAvailableBalance).not.toBeCalled();
      });

      it('should return recommendation that match paymentServiceCode with LOAN_GL_DISBURSEMENT', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            paymentServiceCode: 'LOAN_GL_DISBURSEMENT'
          }
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          ...transactionInput,
          paymentServiceType: PaymentServiceTypeEnum.LOAN_REPAYMENT,
          sourceAccountNo: undefined
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
        expect(depositRepository.getAvailableBalance).not.toBeCalled();
      });

      it('should return recommendation that match paymentServiceCode with DISBURSEMENT and paymentServiceType LOAN_DIRECT_DISBURSEMENT', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            paymentServiceCode: 'DISBURSEMENT'
          }
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          ...transactionInput,
          paymentServiceType: PaymentServiceTypeEnum.LOAN_DIRECT_DISBURSEMENT,
          sourceAccountNo: undefined
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
        expect(depositRepository.getAvailableBalance).not.toBeCalled();
      });

      it('should return recommendation that match paymentServiceCode with DISBURSEMENT and paymentServiceType LOAN_DISBURSEMENT', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            paymentServiceCode: 'DISBURSEMENT'
          }
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          ...transactionInput,
          paymentServiceType: PaymentServiceTypeEnum.LOAN_DISBURSEMENT,
          sourceAccountNo: undefined
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
        expect(depositRepository.getAvailableBalance).not.toBeCalled();
      });

      it('should return recommendation that match paymentServiceCode with PAYROLL', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            paymentServiceCode: 'PAYROLL',
            debitTransactionCode: []
          }
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          ...transactionInput,
          paymentServiceType: PaymentServiceTypeEnum.PAYROLL
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
        expect(depositRepository.getAvailableBalance).not.toBeCalled();
      });

      it('should return recommendation that match paymentServiceCode with CASHBACK', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            debitTransactionCode: [],
            paymentServiceCode: 'CASHBACK'
          }
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          ...transactionInput,
          paymentServiceType: PaymentServiceTypeEnum.CASHBACK,
          paymentServiceCode: serviceMappingResult[0].paymentServiceCode
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
        expect(depositRepository.getAvailableBalance).not.toBeCalled();
      });

      it('should return recommendation that match paymentServiceType LOAN_REFUND', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          getPaymentServiceMappingData()
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          ...transactionInput,
          paymentServiceType: PaymentServiceTypeEnum.LOAN_REFUND,
          sourceAccountNo: undefined
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
        expect(depositRepository.getAvailableBalance).not.toBeCalled();
      });

      it('should return recommendation that match paymentServiceCode with BRANCH_DEPOSIT', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            paymentServiceCode: 'BRANCH_DEPOSIT',
            debitTransactionCode: []
          }
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          ...transactionInput,
          paymentServiceType: PaymentServiceTypeEnum.BRANCH_DEPOSIT
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
        expect(depositRepository.getAvailableBalance).not.toBeCalled();
      });

      it('should return recommendation that match paymentServiceCode with LOAN_DISBURSEMENT', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            paymentServiceCode: 'DISBURSEMENT',
            debitTransactionCode: []
          }
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          ...transactionInput,
          paymentServiceType: PaymentServiceTypeEnum.LOAN_DISBURSEMENT
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
        expect(depositRepository.getAvailableBalance).not.toBeCalled();
      });

      it('should return recommendation that match paymentServiceCode with SETTLEMENT', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            paymentServiceCode: 'SETTLEMENT',
            debitTransactionCode: []
          }
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          ...transactionInput,
          interchange: BankNetworkEnum.CREDIT_UNSETTLED,
          paymentServiceType: PaymentServiceTypeEnum.SETTLEMENT
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
        expect(depositRepository.getAvailableBalance).not.toBeCalled();
      });

      it('should return recommendation that match paymentServiceCode with SIT04 and skip checking source account balance', async () => {
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            paymentServiceCode: 'SIT04',
            debitTransactionCode: []
          }
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        const result = await transactionRecommendService.getRecommendService({
          ...transactionInput,
          paymentServiceType: PaymentServiceTypeEnum.MIGRATION_TRANSFER
        });

        expect(result).toEqual(serviceMappingResult[0]);
        expect(depositRepository.getAvailableBalance).not.toBeCalled();
      });

      it('should return recommendation that match paymentServiceCode with SKN when preferedBankChannel is EXTERNAL', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          {
            ...getPaymentServiceMappingData(),
            paymentServiceCode: 'SKN'
          }
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          refund: { originalTransactionId: 'originalTransactionId' },
          ...transactionInput,
          paymentServiceCode: serviceMappingResult[0].paymentServiceCode,
          preferedBankChannel: BankChannelEnum.EXTERNAL
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
      });
    });

    it('should return recommendation that match paymentServiceCode with TD_MANUAL_PAYMENT_GL', async () => {
      // Given
      const serviceMappingResult: IPaymentServiceMapping[] = [
        {
          ...getPaymentServiceMappingData(),
          paymentServiceCode: 'TD_MANUAL_PAYMENT_GL',
          debitTransactionCode: []
        }
      ];

      (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
        serviceMappingResult
      );

      // When
      const result = await transactionRecommendService.getRecommendService({
        ...transactionInput,
        paymentServiceType: PaymentServiceTypeEnum.TD_MANUAL_PAYMENT_GL
      });

      // Then
      expect(result).toEqual(serviceMappingResult[0]);
      expect(depositRepository.getAvailableBalance).not.toBeCalled();
    });

    describe('if not input paymentServiceCode', () => {
      it('should return first recommendation', async () => {
        // Given
        const serviceMappingResult: IPaymentServiceMapping[] = [
          getPaymentServiceMappingData()
        ];

        (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValueOnce(
          serviceMappingResult
        );

        // When
        const result = await transactionRecommendService.getRecommendService({
          ...transactionInput,
          paymentServiceCode: undefined
        });

        // Then
        expect(result).toEqual(serviceMappingResult[0]);
      });
    });
  });
});
