import feeHelper from '../transactionFee.helper';
import { getTransactionModelFullData } from '../__mocks__/transaction.data';
import { TransactionStatus } from '../transaction.enum';
import { ITransactionModel } from '../transaction.model';

describe('transactionFee.helper', () => {
  describe('getFeeTransactionInputs', () => {
    afterEach(() => {
      jest.resetAllMocks();
      expect.hasAssertions();
    });

    it('should return fee transaction', () => {
      let newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        status: TransactionStatus.SUCCEED
      };

      const result = feeHelper.getFeeTransactionInputs(data);

      expect(result.length).toEqual(1);
    });

    it('should return fee transaction when fees are undefined', () => {
      let newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        ...newTransactionModel,
        fees: undefined,
        id: '1234',
        status: TransactionStatus.SUCCEED
      };

      const result = feeHelper.getFeeTransactionInputs(data);

      expect(result.length).toEqual(0);
    });

    it('should return empty array ', () => {
      let newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        status: TransactionStatus.SUCCEED,
        fees: []
      };

      const result = feeHelper.getFeeTransactionInputs(data);

      expect(result.length).toEqual(0);
    });
  });

  describe('getFeeCardTransactionInputs', () => {
    afterEach(() => {
      jest.resetAllMocks();
      expect.hasAssertions();
    });

    it('should return fee card transaction', () => {
      let newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        fees: [
          {
            feeCode: 'feeCode1',
            customerTc: 'customerTc1',
            customerTcChannel: 'channel1',
            feeAmount: 123,
            blockingId: 'blockingId1',
            idempotencyKey: 'idempotencyKey'
          }
        ],
        status: TransactionStatus.SUCCEED
      };

      const result = feeHelper.getFeeCardTransactionInputs(data);

      expect(result.length).toEqual(1);
    });

    it('should return fee card transaction when fees are undefined', () => {
      let newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        ...newTransactionModel,
        fees: undefined,
        id: '1234',
        status: TransactionStatus.SUCCEED
      };

      const result = feeHelper.getFeeCardTransactionInputs(data);

      expect(result.length).toEqual(0);
    });

    it('should return empty array ', () => {
      let newTransactionModel = getTransactionModelFullData();
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        status: TransactionStatus.SUCCEED,
        fees: []
      };

      const result = feeHelper.getFeeCardTransactionInputs(data);

      expect(result.length).toEqual(0);
    });
  });
});
