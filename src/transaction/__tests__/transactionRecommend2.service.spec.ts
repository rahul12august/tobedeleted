import {
  BankChannelEnum,
  BankNetworkEnum,
  Constant,
  PaymentServiceTypeEnum
} from '@dk/module-common';
import { ERROR_CODE } from '../../common/errors';
import configurationRepository from '../../configuration/configuration.repository';
import configurationService from '../../configuration/configuration.service';
import { IPaymentServiceMapping } from '../../configuration/configuration.type';
import depositRepository from '../../coreBanking/deposit.repository';
import transactionHelper from '../transaction.helper';
import {
  JagoExternalBankCode,
  JagoShariaExternalBankCode,
  TransferFailureReasonActor
} from '../transaction.enum';
import {
  ITransactionInput,
  RecommendPaymentServiceInput
} from '../transaction.type';
import transactionRecommendService from '../transactionRecommend.service';
import logger from '../../logger';
import {
  getCashBackServiceMappings,
  getTransactionMockValueObject,
  getTyped,
  makeCodeInfo
} from '../__mocks__/transaction.data';
import { JAGO_BANK_CODE, JAGO_SHARIA_BANK_CODE } from '../transaction.constant';
import { TransferAppError } from '../../errors/AppError';

jest.mock('../../configuration/configuration.repository');
jest.mock('../../configuration/configuration.service');
jest.mock('../../coreBanking/deposit.repository');
jest.mock('../transaction.helper', () => {
  const original = require.requireActual('../transaction.helper').default;
  return {
    ...original,
    mapAuthenticationRequired: jest.fn(),
    mapFeeData: jest.fn(),
    getAccountInfo: jest.fn(),
    getSourceAccount: jest.fn(),
    validateTransactionLimitAmount: jest.fn(),
    getPaymentServiceMappings: jest.fn(),
    getBeneficiaryAccount: jest.fn()
  };
});

let infoMsgs: string[] = [];

const { ruleConfigs, limitGroups } = getTransactionMockValueObject();

const input = {
  beneficiaryAccountNo: '123',
  beneficiaryCIF: 'B-CIF-123',
  beneficiaryBankCode: Constant.GIN_BANK_CODE_ID,
  beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
  beneficiaryBankName: 'bank',
  sourceCIF: 'S-CIF-123',
  sourceAccountName: 'invalid',
  sourceAccountNo: 'SRC-1',
  sourceAccountType: 'DC',
  sourceBankCodeChannel: BankChannelEnum.INTERNAL,
  sourceBankCode: Constant.GIN_BANK_CODE_ID,
  sourceTransactionCurrency: 'IDR',
  sourceCustomerId: 'sourceCustomerId',

  beneficiarySupportedChannels: [
    BankChannelEnum.BIFAST,
    BankChannelEnum.EXTERNAL
  ],
  paymentServiceType: PaymentServiceTypeEnum.WALLET,
  beneficiaryAccountType: 'ANY',
  transactionAmount: 7000000,
  externalId: 'dummy123'
};

const serviceMappings: IPaymentServiceMapping[] = [
  {
    paymentServiceCode: 'SIT01',
    debitTransactionCode: [
      {
        transactionCode: 'abc',
        transactionCodeInfo: {
          limitGroupCode: 'l01',
          feeRules: 'f01',
          code: 'abc',
          minAmountPerTx: 1,
          maxAmountPerTx: 999999,
          channel: BankChannelEnum.INTERNAL
        }
      }
    ],
    transactionAuthenticationChecking: true,
    beneficiaryBankCodeType: 'INTERNAL',
    blocking: false,
    requireThirdPartyOutgoingId: false
  },
  {
    paymentServiceCode: 'CRE-1',
    creditTransactionCode: [
      {
        transactionCode: 'credit-01',
        transactionCodeInfo: makeCodeInfo('l01', 'f01', 'abc')
      }
    ],
    transactionAuthenticationChecking: true,
    beneficiaryBankCodeType: 'INTERNAL',
    blocking: false,
    requireThirdPartyOutgoingId: false
  },
  {
    paymentServiceCode: 'BIBIT_INSTANT_REDEMPTION',
    beneficiaryBankCodeType: 'RTOL',
    creditTransactionCode: [
      {
        transactionCode: 'IRC01',
        transactionCodeInfo: makeCodeInfo('l04', 'h02', 'ghj')
      }
    ],
    debitTransactionCode: [
      {
        transactionCode: 'IRD01',
        transactionCodeInfo: makeCodeInfo('l05', 'h03', 'ghk')
      }
    ],
    transactionAuthenticationChecking: false,
    blocking: false,
    requireThirdPartyOutgoingId: false
  },
  {
    paymentServiceCode: 'THIRDPARTY_JAGO_TO_JAGO_CREDIT',
    creditTransactionCode: [
      {
        interchange: BankNetworkEnum.ARTAJASA,
        transactionCode: 'TFC21',
        transactionCodeInfo: makeCodeInfo('l02', 'g01', 'abd')
      },
      {
        interchange: BankNetworkEnum.ALTO,
        transactionCode: 'TFC22',
        transactionCodeInfo: makeCodeInfo('l03', 'h01', 'ghi')
      }
    ],
    beneficiaryBankCodeType: 'RTOL',
    transactionAuthenticationChecking: false,
    requireThirdPartyOutgoingId: false,
    blocking: false,
    debitTransactionCode: [
      {
        interchange: BankNetworkEnum.ARTAJASA,
        transactionCode: 'TFC21-R',
        transactionCodeInfo: makeCodeInfo('l04', 'h02', 'ghj')
      },
      {
        interchange: BankNetworkEnum.ALTO,
        transactionCode: 'TFC22-R',
        transactionCodeInfo: makeCodeInfo('l05', 'h03', 'c01')
      }
    ]
  }
];

//
// Convenient aliases
//
const debitMapping: IPaymentServiceMapping = serviceMappings[0];
const creditMapping: IPaymentServiceMapping = serviceMappings[1];
const creditDebitMapping: IPaymentServiceMapping = serviceMappings[2];
const creditAndDebitReversalMapping: IPaymentServiceMapping =
  serviceMappings[3];

describe('Balance and fee filtering', () => {
  beforeEach(() => {
    (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValue(
      ruleConfigs
    );
    (configurationService.getPaymentConfigRules as jest.Mock).mockResolvedValue(
      ruleConfigs
    );
    (configurationRepository.getLimitGroupsByCodes as jest.Mock).mockResolvedValue(
      limitGroups
    );
    (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValue(
      serviceMappings
    );

    infoMsgs = [];
    (logger.info as jest.Mock).mockImplementation((msg: string) => {
      infoMsgs.push(msg);
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  const recommendPaymentServiceInput: RecommendPaymentServiceInput = {
    sourceCIF: input.sourceCIF,
    sourceAccountNo: input.sourceAccountNo,
    beneficiaryCIF: input.beneficiaryCIF,
    beneficiaryAccountNo: input.beneficiaryAccountNo,
    beneficiaryBankCode: input.beneficiaryBankCode,
    paymentServiceType: input.paymentServiceType,
    transactionAmount: input.transactionAmount
  };

  const transactionInput = {
    ...recommendPaymentServiceInput,
    sourceAccountInfo: {
      accountNumber: recommendPaymentServiceInput.sourceAccountNo
    }
  };

  const withFee = (
    fee: number,
    mapping: IPaymentServiceMapping | IPaymentServiceMapping[] = debitMapping
  ) => {
    if (Array.isArray(mapping)) {
      const feeData = { feeAmount: fee };
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValue(
        mapping.map(x => {
          return { ...x, feeData };
        })
      );
    } else {
      const feeData = [{ feeAmount: fee }];
      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValue([
        { ...mapping, feeData }
      ]);
    }
  };

  const withBalance = (balance: number) => {
    (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValue(
      balance
    );
  };

  describe('Balance Checking Tests', () => {
    it('Debit reversal with fee is checked against balance', async () => {
      withBalance(1200);
      withFee(1500, creditAndDebitReversalMapping);

      const result = await transactionRecommendService
        .getRecommendServices({
          ...transactionInput,
          sourceBankCode: JAGO_BANK_CODE,
          transactionAmount: 2000
        })
        .catch(e => e);

      expect(result).toBeInstanceOf(Error);
      expect(result.errorCode).toBe(ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT);
      expect(infoMsgs).toContain(
        `Insufficient balance 1200 < 1500 (fee), pscode: ${creditAndDebitReversalMapping.paymentServiceCode}`
      );
    });

    it('Debit reversal with no fee is not checked against balance', async () => {
      withBalance(3000);
      withFee(0, creditAndDebitReversalMapping);

      const result = await transactionRecommendService
        .getRecommendServices({ ...transactionInput, transactionAmount: 5000 })
        .catch(e => e);

      expect(result).toBeInstanceOf(Array);
      expect(result[0]).not.toBeInstanceOf(Error);
    });

    it('Debit trx no fee, not enough funds', async () => {
      withBalance(3000);
      withFee(15);

      const result = await transactionRecommendService
        .getRecommendServices({
          ...transactionInput,
          transactionAmount: 5500,
          sourceBankCode: JAGO_SHARIA_BANK_CODE
        })
        .catch(e => e);

      expect(result).toBeInstanceOf(Error);
      expect(result.errorCode).toBe(ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT);
    });

    it('Debit trx with fee edge case (fee + trx.amount = balance)', async () => {
      withBalance(3000);
      withFee(1000);

      const result = await transactionRecommendService
        .getRecommendServices({ ...transactionInput, transactionAmount: 2000 })
        .catch(e => e);

      expect(result).toBeInstanceOf(Array);
      expect(result[0]).not.toBeInstanceOf(Error);
    });

    it('Credit trx with fee, not enough funds on Sharia', async () => {
      withBalance(500);
      withFee(1500, creditMapping);

      const result = await transactionRecommendService
        .getRecommendServices({
          ...transactionInput,
          sourceBankCode: JAGO_SHARIA_BANK_CODE,
          transactionAmount: 42000
        })
        .catch(e => e);

      expect(result).toBeInstanceOf(Error);
      expect(result.errorCode).toBe(ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT);
    });

    it('Credit trx with fee (future SWIFT) should log the correct message when balance is not enough', async () => {
      withBalance(500);
      withFee(501, creditMapping);

      const result = await transactionRecommendService
        .getRecommendServices({
          ...transactionInput,
          sourceBankCode: JAGO_BANK_CODE,
          transactionAmount: 400
        })
        .catch(e => e);

      expect(result).toBeInstanceOf(Error);
      expect(result.errorCode).toBe(ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT);

      expect(infoMsgs).toContain(
        `Insufficient balance 500 < 501 (fee), pscode: ${creditMapping.paymentServiceCode}`
      );
    });

    it('Debit trx with no fee should log the correct message when balance is not enough', async () => {
      withBalance(1000);
      withFee(0);

      const result = await transactionRecommendService
        .getRecommendServices({
          ...transactionInput,
          sourceBankCode: JAGO_BANK_CODE,
          transactionAmount: 5000
        })
        .catch(e => e);

      expect(result).toBeInstanceOf(Error);
      expect(result.errorCode).toBe(ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT);

      expect(infoMsgs).toContain(
        'Insufficient balance 1000 < 5000, pscode: SIT01'
      );
    });

    it('Debit trx with fee should log the correct message when balance is not enough', async () => {
      withBalance(500);
      withFee(123, { ...debitMapping, paymentServiceCode: 'ABC' });

      const result = await transactionRecommendService
        .getRecommendServices({
          ...transactionInput,
          sourceBankCode: JAGO_BANK_CODE,
          transactionAmount: 400
        })
        .catch(e => e);

      expect(result).toBeInstanceOf(Error);
      expect(result.errorCode).toBe(ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT);

      expect(infoMsgs).toContain(
        'Insufficient balance 500 < 400 + 123 (fee), pscode: ABC'
      );
    });

    it('Bonus interest fails with insufficient balance although it is not the case', async () => {
      withBalance(500);
      withFee(0, creditDebitMapping); // payment service mapping contains both debitTransactionCode and creditTransactionCode

      const result = await transactionRecommendService
        .getRecommendServices({
          ...transactionInput,
          sourceBankCode: JAGO_SHARIA_BANK_CODE,
          paymentServiceType: PaymentServiceTypeEnum.BONUS_INTEREST, // or any of the other 17 such possible types
          transactionAmount: 100
        })
        .catch(e => e);

      expect(result).not.toBeInstanceOf(Error);
    });

    it('Casback trx', async () => {
      withBalance(5000);
      withFee(0, getCashBackServiceMappings());

      const trxInput: ITransactionInput = {
        transactionAmount: 100,
        paymentServiceCode: 'CASHBACK',
        paymentServiceType: PaymentServiceTypeEnum.CASHBACK,
        sourceBankCode: 'BC002',
        sourceAccountNo: '100436504089'
      };

      const result = await transactionRecommendService
        .getRecommendServices(trxInput)
        .catch(e => e);

      expect(result).not.toBeInstanceOf(Error);
    });

    it('Balance check for excepted payment service types still work', async () => {
      const exceptedFromBalanceCheck: PaymentServiceTypeEnum[] = [
        PaymentServiceTypeEnum.BONUS_INTEREST,
        PaymentServiceTypeEnum.BRANCH_DEPOSIT,
        PaymentServiceTypeEnum.CASHBACK,

        // Checking with Andre Surya on the following two entries
        PaymentServiceTypeEnum.LOAN_BULK_TAKEOVER_TRANSFER_REVERSAL, // not in the original code, but it should be
        PaymentServiceTypeEnum.LOAN_FORWARD_PAYMENT_WITHDRAWAL,

        PaymentServiceTypeEnum.LOAN_GL_DISBURSEMENT,
        PaymentServiceTypeEnum.LOAN_GL_REPAYMENT,
        PaymentServiceTypeEnum.LOAN_DIRECT_DISBURSEMENT,
        PaymentServiceTypeEnum.LOAN_DISBURSEMENT,
        PaymentServiceTypeEnum.LOAN_REPAYMENT,
        PaymentServiceTypeEnum.LOAN_REFUND,
        PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY,
        PaymentServiceTypeEnum.MIGRATION_TRANSFER,
        PaymentServiceTypeEnum.OFFER,
        PaymentServiceTypeEnum.PAYROLL,
        PaymentServiceTypeEnum.SETTLEMENT,
        PaymentServiceTypeEnum.TD_MANUAL_PAYMENT_GL,
        PaymentServiceTypeEnum.TD_SHARIA_REPAYMENT
      ];

      withBalance(5000);
      for (const psType of exceptedFromBalanceCheck) {
        withFee(0, getTyped(psType.toString()));

        const trxInput: ITransactionInput = {
          transactionAmount: 10000,
          paymentServiceType: psType,
          sourceBankCode: JAGO_BANK_CODE,
          sourceCIF: 'CIF-123',
          sourceAccountInfo: {
            accountNumber: '100436504089'
          }
        };

        const result = await transactionRecommendService
          .getRecommendServices(trxInput)
          .catch(e => e);

        expect(result).not.toBeInstanceOf(Error);
      }
    });

    it('Non Jago banks are not subject to balance verification', async () => {
      withFee(0, debitMapping);

      const trxInput: ITransactionInput = {
        transactionAmount: 100,
        paymentServiceCode: 'CASHBACK',
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        sourceBankCode: 'BC123', // HSBC
        sourceAccountInfo: {
          accountNumber: '12231990'
        },
        beneficiaryAccountNo: '400100200',
        sourceCIF: 'CIF-123'
      };

      const result = await transactionRecommendService
        .getRecommendServices(trxInput)
        .catch(e => e);

      expect(result).not.toBeInstanceOf(Error);
      expect(depositRepository.getAvailableBalance).not.toHaveBeenCalled();
    });

    it('The recommendation services input is still printed, even when balance should not be checked', async () => {
      withFee(0, debitMapping);

      const trxInput: ITransactionInput = {
        transactionAmount: 10000,
        paymentServiceType: PaymentServiceTypeEnum.LOAN_REFUND,
        sourceBankCode: JAGO_BANK_CODE,
        sourceCIF: 'CIF-123',
        sourceAccountInfo: {
          accountNumber: '100436504089'
        }
      };

      const result = await transactionRecommendService
        .getRecommendServices(trxInput)
        .catch(e => e);

      expect(result).not.toBeInstanceOf(Error);

      let filtered = infoMsgs.filter(msg =>
        msg.startsWith('getRecommendServices: filtered recommendedServices:')
      );

      expect(filtered).toBeDefined();
      expect(filtered.length).toBeGreaterThan(0);
    });

    it('Check the proper message is printed when balance verification is performed', async () => {
      withBalance(50_000);
      withFee(0, debitMapping);

      const trxInput: ITransactionInput = {
        transactionAmount: 10_000,
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        sourceBankCode: JAGO_BANK_CODE,
        sourceCIF: 'CIF-123',
        sourceAccountInfo: {
          accountNumber: '100436504089'
        }
      };

      const result = await transactionRecommendService
        .getRecommendServices(trxInput)
        .catch(e => e);

      expect(result).not.toBeInstanceOf(Error);
      expect(infoMsgs).toContain(
        `Checking trx input against account: ${trxInput.sourceAccountInfo?.accountNumber}`
      );
    });

    it('Check the verification message is NOT printed when balance verification should be skipped', async () => {
      withFee(0, debitMapping);
      withBalance(50_000);

      const trxInput: ITransactionInput = {
        transactionAmount: 10_000,
        paymentServiceType: PaymentServiceTypeEnum.LOAN_REFUND,
        sourceBankCode: JAGO_BANK_CODE,
        sourceCIF: 'CIF-123',
        sourceAccountInfo: {
          accountNumber: '100436504089'
        }
      };

      const result = await transactionRecommendService
        .getRecommendServices(trxInput)
        .catch(e => e);

      expect(result).not.toBeInstanceOf(Error);
      expect(infoMsgs).not.toContain(
        `Checking trx input against account: ${trxInput.sourceAccountInfo?.accountNumber}`
      );
    });

    it('Debit is checked for transactions with Jago remittance code only', async () => {
      withFee(0, debitMapping);
      withBalance(500);

      const trxInput: ITransactionInput = {
        transactionAmount: 10_000,
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        sourceRemittanceCode: 'SOME-VALUE',
        sourceCIF: 'CIF-123',
        sourceAccountInfo: {
          accountNumber: '100436504089'
        }
      };

      const remittanceCodes = [
        JagoExternalBankCode.REMITTANCE_CODE,
        JagoShariaExternalBankCode.REMITTANCE_CODE
      ];

      for (let remittanceCode of remittanceCodes) {
        const result = await transactionRecommendService
          .getRecommendServices({
            ...trxInput,
            sourceRemittanceCode: remittanceCode
          })
          .catch(e => e);

        expect(result).toBeInstanceOf(Error);
      }
    });

    it('Debit is checked for transactions with ROTL codes only', async () => {
      withFee(0, debitMapping);
      withBalance(500);

      const trxInput: ITransactionInput = {
        transactionAmount: 10_000,
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        sourceCIF: 'CIF-123',
        sourceRtolCode: 'SOME-VALUE',
        sourceAccountInfo: {
          accountNumber: '100436504089'
        }
      };

      const rtolCodes = [
        JagoExternalBankCode.RTOL_CODE,
        JagoShariaExternalBankCode.RTOL_CODE
      ];

      for (let rtolCode of rtolCodes) {
        const result = await transactionRecommendService
          .getRecommendServices({
            ...trxInput,
            sourceRtolCode: rtolCode
          })
          .catch(e => e);

        expect(result).toBeInstanceOf(Error);
      }
    });
  });

  describe('Payment Service Code Association Tests', () => {
    let infoWarnMsgs: string[] = [];

    const trxInput: ITransactionInput = {
      transactionAmount: 10000,
      paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
      sourceBankCode: JAGO_BANK_CODE,
      sourceCIF: 'CIF-123',
      sourceAccountInfo: {
        accountNumber: '100436504089'
      }
    };

    beforeEach(() => {
      infoWarnMsgs = [];
      (logger.warn as jest.Mock).mockImplementation((msg: string) => {
        infoWarnMsgs.push(msg);
      });
      (logger.info as jest.Mock).mockImplementation((msg: string) => {
        infoWarnMsgs.push(msg);
      });

      // Reset the mutated object back to original state
      delete trxInput.paymentServiceCode;
    });

    it('Payment Service Code is picked up from the single element of recm services (property missing)', async () => {
      withBalance(2 * trxInput.transactionAmount);
      withFee(0, getTyped('ABC'));

      const result = await transactionRecommendService
        .getRecommendServices(trxInput)
        .catch(e => e);

      expect(result).not.toBeInstanceOf(Error);
      expect(trxInput.paymentServiceCode).toBe('ABC');
      expect(infoWarnMsgs).toContain('Associated pscode ABC');
    });

    it('Payment Service Code is picked up from the single element of recm services (property empty)', async () => {
      withBalance(2 * trxInput.transactionAmount);
      withFee(0, getTyped('ABC'));

      const localCopy = JSON.parse(JSON.stringify(trxInput));
      localCopy.paymentServiceCode = '';

      const result = await transactionRecommendService
        .getRecommendServices(localCopy)
        .catch(e => e);

      expect(result).not.toBeInstanceOf(Error);
      expect(localCopy.paymentServiceCode).toBe('ABC');
    });

    it('Payment Service Code is picked up from the first element of recm services and warning printed', async () => {
      withBalance(1.5 * trxInput.transactionAmount);
      withFee(0, [getTyped('XYZ'), getTyped('ABC')]);

      const result = await transactionRecommendService
        .getRecommendServices(trxInput)
        .catch(e => e);

      expect(result).not.toBeInstanceOf(Error);
      expect(trxInput.paymentServiceCode).toBe('XYZ');
      expect(infoWarnMsgs).toContain(
        `Recommended services mapped to more than one pscode: XYZ,ABC`
      );
    });

    it('Payment Service Code is picked up from the first element of recm services and the warning prints distinct pscodes', async () => {
      withBalance(3 * trxInput.transactionAmount);
      withFee(0, [
        getTyped('ABC'),
        getTyped('XYZ'),
        getTyped('ABC'),
        getTyped('XYZ')
      ]);

      const result = await transactionRecommendService
        .getRecommendServices(trxInput)
        .catch(e => e);

      expect(result).not.toBeInstanceOf(Error);
      expect(trxInput.paymentServiceCode).toBe('ABC');
      expect(infoWarnMsgs).toContain(
        `Recommended services mapped to more than one pscode: ABC,XYZ`
      );
    });

    it('Picking up recommendation service is protected against empty array of recm services', async () => {
      withBalance(2 * trxInput.transactionAmount);
      withFee(0, []);

      const result = await transactionRecommendService
        .getRecommendServices(trxInput)
        .catch(e => e);

      expect(result).toBeInstanceOf(TransferAppError);
      expect((result as TransferAppError).actor).toBe(
        TransferFailureReasonActor.MS_TRANSFER
      );
      expect((result as TransferAppError).errorCode).toBe(
        ERROR_CODE.NO_RECOMMENDATION_SERVICES
      );
    });

    it('Existing Payment Service Code is not ovewriten by the recm entry', async () => {
      withBalance(2 * trxInput.transactionAmount);
      withFee(0, getTyped('ABC'));
      const pscOpinionated = JSON.parse(JSON.stringify(trxInput));
      pscOpinionated.paymentServiceCode = 'ORIGINAL';

      const result = await transactionRecommendService
        .getRecommendServices(trxInput)
        .catch(e => e);

      expect(result).not.toBeInstanceOf(Error);
      expect(pscOpinionated.paymentServiceCode).toBe('ORIGINAL');
    });
  });
});
