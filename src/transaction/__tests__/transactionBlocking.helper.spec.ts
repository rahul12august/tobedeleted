import transactionBlockingHelper from '../transactionBlocking.helper';
import accountRepository from '../../account/account.repository';
import { Enum } from '@dk/module-common';
import { ITransactionModel } from '../transaction.model';
import { getTransactionModelFullData } from '../__mocks__/transaction.data';
import { IBlockingAmountResponse } from '../../account/account.type';
import transactionRepository from '../transaction.repository';
import depositRepository from '../../coreBanking/deposit.repository';
import configurationRepository from '../../configuration/configuration.repository';
import { getCounterData } from '../../configuration/__mocks__/configuration.data';
import { ERROR_CODE } from '../../common/errors';
import { COUNTER_MAMBU_BLOCK_DAYS } from '../../configuration/configuration.constant';
import blockingAmountService from '../../blockingAmount/blockingAmount.service';
import { TransferAppError } from '../../errors/AppError';

import configurationService from '../../configuration/configuration.service';
import coreBankingCardService from '../../coreBanking/block.service';
import { config } from '../../config';

jest.mock('../../configuration/configuration.repository');
jest.mock('../../account/account.repository');
jest.mock('../../transaction/transaction.repository');
jest.mock('../../coreBanking/deposit.repository');
jest.mock('../../transaction/transaction.producer');
jest.mock('../../common/featureFlag');

describe('transactionBlockingHelper', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });
  const blockingPrincipal: IBlockingAmountResponse = {
    id: 'blockingId456',
    cardId: 'cardId-123',
    status: 'ACTIVE'
  };
  const blockingFee: IBlockingAmountResponse = {
    id: 'blocking-fee-456',
    cardId: 'cardId-123',
    status: 'ACTIVE'
  };
  let newTransactionModel = getTransactionModelFullData();

  describe('createBlockingTransaction', () => {
    beforeEach(() => {
      jest.resetAllMocks();
    });
    it('should create external blocking principal transaction ', async () => {
      // give
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        fees: undefined
      };
      config.get = jest.fn().mockImplementation(() => undefined);
      (configurationRepository.getCounterByCode as jest.Mock).mockResolvedValue(
        getCounterData()
      );
      (accountRepository.createBlockingAmount as jest.Mock).mockResolvedValueOnce(
        blockingPrincipal
      );
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...data,
        blockingId: blockingPrincipal.id,
        cardId: blockingPrincipal.cardId
      });
      coreBankingCardService.createBlockingAmount = jest.fn();
      accountRepository.getActivatedAccount = jest
        .fn()
        .mockReturnValueOnce({ cardId: 123, id: 'account-id' });
      configurationService.getSavingProduct = jest
        .fn()
        .mockReturnValueOnce({ blockingCapability: true });

      //when
      const expected = await transactionBlockingHelper.createBlockingTransaction(
        data,
        Enum.BlockingAmountType.MOBILE_TRANSACTION
      );

      //then
      expect(expected.blockingId).toEqual(blockingPrincipal.id);
      expect(expected.cardId).toEqual(blockingPrincipal.cardId);
      expect(configurationRepository.getCounterByCode).toHaveBeenCalledWith(
        COUNTER_MAMBU_BLOCK_DAYS
      );
    });

    it('should create blocking transaction with MAMBU_BLOCKING_CODE from config', async () => {
      // give
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        fees: undefined
      };
      config.get = jest.fn().mockImplementation(() => '10000007');
      (accountRepository.createBlockingAmount as jest.Mock).mockResolvedValueOnce(
        blockingPrincipal
      );
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...data,
        blockingId: blockingPrincipal.id,
        cardId: blockingPrincipal.cardId
      });
      coreBankingCardService.createBlockingAmount = jest.fn();
      accountRepository.getActivatedAccount = jest
        .fn()
        .mockReturnValueOnce({ cardId: 123, id: 'account-id' });
      configurationService.getSavingProduct = jest
        .fn()
        .mockReturnValueOnce({ blockingCapability: true });

      //when
      const expected = await transactionBlockingHelper.createBlockingTransaction(
        data,
        Enum.BlockingAmountType.MOBILE_TRANSACTION
      );

      //then
      expect(expected.blockingId).toEqual(blockingPrincipal.id);
      expect(expected.cardId).toEqual(blockingPrincipal.cardId);
      expect(configurationRepository.getCounterByCode).not.toHaveBeenCalled();
    });

    it('should create external blocking fee transaction ', async () => {
      //given
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        fees: [
          {
            customerTc: 'customerTc',
            customerTcChannel: 'customerTcChannel',
            feeAmount: 500,
            feeCode: 'feeCode'
          }
        ]
      };
      (configurationRepository.getCounterByCode as jest.Mock).mockResolvedValue(
        getCounterData()
      );
      (accountRepository.createBlockingAmount as jest.Mock).mockResolvedValue(
        blockingFee
      );
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...data,
        cardId: blockingFee.cardId,
        blockingId: blockingPrincipal.id,
        fees: [
          {
            ...data.fees[0],
            blockingId: blockingFee.id
          }
        ]
      });
      coreBankingCardService.createBlockingAmount = jest.fn();
      accountRepository.getActivatedAccount = jest
        .fn()
        .mockReturnValue({ cardId: 123, id: 'account-id' })
        .mockReturnValue({ cardId: 123, id: 'account-id' });
      configurationService.getSavingProduct = jest
        .fn()
        .mockReturnValueOnce({ blockingCapability: true })
        .mockReturnValueOnce({ blockingCapability: true });

      //when
      const expected = await transactionBlockingHelper.createBlockingTransaction(
        data,
        Enum.BlockingAmountType.MOBILE_TRANSACTION
      );
      //then
      expect(expected.fees[0].blockingId).toEqual(blockingFee.id);
      expect(expected.cardId).toEqual(blockingFee.cardId);
      expect(configurationRepository.getCounterByCode).toHaveBeenCalledWith(
        COUNTER_MAMBU_BLOCK_DAYS
      );
    });

    it('should throw when mambu blocking counter not found', async () => {
      // give
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        fees: undefined
      };
      (configurationRepository.getCounterByCode as jest.Mock).mockResolvedValue(
        null
      );

      //when
      const error = await transactionBlockingHelper
        .createBlockingTransaction(
          data,
          Enum.BlockingAmountType.MOBILE_TRANSACTION
        )
        .catch(err => err);

      //then
      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(
        ERROR_CODE.MAMBU_BLOCKING_COUNTER_NOT_FOUND
      );
      expect(configurationRepository.getCounterByCode).toHaveBeenCalledWith(
        COUNTER_MAMBU_BLOCK_DAYS
      );
      expect(accountRepository.createBlockingAmount).not.toBeCalled();
      expect(transactionRepository.update).not.toBeCalled();
    });

    it('should create internal blocking principal transaction ', async () => {
      // give
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        fees: undefined
      };
      (configurationRepository.getCounterByCode as jest.Mock).mockResolvedValue(
        getCounterData()
      );
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...data,
        blockingId: blockingPrincipal.id,
        cardId: blockingPrincipal.cardId
      });

      blockingAmountService.createBlockingAmount = jest
        .fn()
        .mockReturnValue(blockingPrincipal);

      //when
      const expected = await transactionBlockingHelper.createBlockingTransaction(
        data,
        Enum.BlockingAmountType.MOBILE_TRANSACTION
      );

      //then
      expect(expected.blockingId).toEqual(blockingPrincipal.id);
      expect(expected.cardId).toEqual(blockingPrincipal.cardId);
      expect(configurationRepository.getCounterByCode).toHaveBeenCalledWith(
        COUNTER_MAMBU_BLOCK_DAYS
      );
      expect(accountRepository.createBlockingAmount).toHaveBeenCalledTimes(0);
      expect(blockingAmountService.createBlockingAmount).toHaveBeenCalledTimes(
        1
      );
    });

    it('should throw error for invalid request', async () => {
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        sourceCif: undefined,
        sourceAccountNo: undefined
      };

      const fn = async () =>
        await transactionBlockingHelper.createBlockingTransaction(
          data,
          Enum.BlockingAmountType.MOBILE_TRANSACTION
        );

      await expect(fn()).rejects.toThrow(TransferAppError);
      await expect(fn()).rejects.toThrow(ERROR_CODE.INVALID_REQUEST);
    });
  });

  describe('cancelBlockingTransaction', () => {
    it('should cancel blocking principal and fee transaction ', async () => {
      // given
      const data: ITransactionModel = {
        ...newTransactionModel,
        id: '1234',
        blockingId: blockingPrincipal.id,
        cardId: blockingPrincipal.cardId,
        fees: [
          {
            customerTc: 'customerTc',
            customerTcChannel: 'customerTcChannel',
            feeAmount: 500,
            blockingId: blockingFee.id,
            feeCode: 'feeCode'
          }
        ]
      };

      // when
      await transactionBlockingHelper.cancelBlockingTransaction(data);

      // then
      expect(depositRepository.reverseAuthorizationHold).toBeCalledWith(
        data.cardId,
        data.blockingId
      );
      expect(depositRepository.reverseAuthorizationHold).toBeCalledWith(
        data.cardId,
        data.fees[0].blockingId
      );
    });
  });
});
