import outgoingCounterRepository from '../../outgoingCounter/outgoingCounter.repository';
import {
  mapToSwitchingTransaction,
  createInAddData,
  IN_SEQUENCE_MAX_LENGTH
} from '../transactionSwitching.helper';
import { ERROR_CODE } from '../../common/errors';
import { advanceTo, clear } from 'jest-date-mock';
import {
  getTransactionModelFullData,
  getTransactionDetail
} from '../__mocks__/transaction.data';
import { ITransactionModel } from '../transaction.model';
import { TransactionStatus } from '../transaction.enum';
import { ISwitchingTransaction } from '../../switching/switching.type';
import configurationRepository from '../../configuration/configuration.repository';
import { ICurrency } from '../../configuration/configuration.type';
import { IOutgoingCounter } from '../../outgoingCounter/outgoingCounter.interface';
import { formatTransactionDate } from '../transaction.util';
import {
  inDeliveryChannelEnum,
  transactionProcessingCodeEnum,
  inSysOrgEnum,
  inTermIdEnum,
  inTermLocEnum,
  inServiceIdEnum
} from '../../switching/switching.enum';
import { TransferAppError } from '../../errors/AppError';
import { ExecutionTypeEnum } from '../transaction.enum';

jest.mock('../../outgoingCounter/outgoingCounter.repository');
jest.mock('../../configuration/configuration.repository');

describe('transactionSwitchingHelper', () => {
  afterAll(() => {
    clear();
  });
  const mockedJulianDate = '20085';
  const mockJulianDateGeneration = () => {
    advanceTo('2020-03-25T08:14:34.662Z');
  };
  const mockOutgoingCounter = (value: IOutgoingCounter) => {
    (outgoingCounterRepository.getNextCounterByPrefix as jest.Mock).mockResolvedValue(
      value
    );
  };
  describe('mapToSwitchingTransaction', () => {
    const transactionModel: ITransactionModel & {
      thirdPartyOutgoingId: string;
    } = {
      ...getTransactionModelFullData(),
      sourceBankCode: 'BC002',
      sourceRtolCode: '542',
      beneficiaryRtolCode: '542',
      thirdPartyOutgoingId: '12345678',
      status: TransactionStatus.PENDING,
      executionType: ExecutionTypeEnum.NONE_BLOCKING,
      id: '2394839483',
      createdAt: new Date('2020-04-26T17:31:38.664Z'),
      requireThirdPartyOutgoingId: true
    };

    const mockedCurrency: ICurrency = {
      code: 'IDR',
      name: 'IDR',
      isoNumber: 360
    };
    const mockCurrencyRetrieval = (value: ICurrency | undefined) => {
      (configurationRepository.getCurrencyByCode as jest.Mock).mockResolvedValue(
        value
      );
    };

    const mockedCounter: IOutgoingCounter = {
      prefix: '20123TC0',
      counter: 1
    };
    const mockSequenceGenerationSuccess = () => {
      mockJulianDateGeneration();
      mockOutgoingCounter(mockedCounter);
    };
    const mockedConfig = {
      application: 'ARTOS',
      userId: 'euronet_test_user',
      password: 'artos_pw'
    };

    const expectedSwitchingTransaction: ISwitchingTransaction = {
      inDeliveryChannel: inDeliveryChannelEnum.DEFAULT,
      inAuditData: {
        inApplication: mockedConfig.application,
        inUserId: mockedConfig.userId,
        inPassword: mockedConfig.password,
        inServiceID: inServiceIdEnum.FUND_TRANSFER,
        inSequence: `${mockedJulianDate}${mockedJulianDate.padStart(10, '0')}`
      },
      inProcessingCode: transactionProcessingCodeEnum.FUND_TRANSFER,
      inTxnAmt: transactionModel.transactionAmount.toString(),
      inTxnDateTime: formatTransactionDate(transactionModel.createdAt),
      inSTAN: transactionModel.thirdPartyOutgoingId.substr(-6),
      inAcqInstId: transactionModel.sourceRtolCode,
      inRRN: transactionModel.thirdPartyOutgoingId,
      inAddData: createInAddData(transactionModel),
      inCurrCode: mockedCurrency.isoNumber,
      inAcctId1: transactionModel.sourceAccountNo,
      inAcctId2: transactionModel.beneficiaryAccountNo,
      inDestInstId: transactionModel.beneficiaryRtolCode,
      inSysOrg: inSysOrgEnum.DEFAULT,
      inTermID: inTermIdEnum.DEFAULT,
      inTermLoc: inTermLocEnum.DEFAULT
    };
    beforeEach(() => {
      mockCurrencyRetrieval(mockedCurrency);
      mockSequenceGenerationSuccess();
    });
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should map to switching transaction successfully with valid input model', async () => {
      const switchingTransaction = await mapToSwitchingTransaction(
        transactionModel
      );

      expect(switchingTransaction.inAuditData.inSequence).toContain(
        mockedJulianDate
      );

      expect(switchingTransaction.inAuditData.inSequence.length).toEqual(
        IN_SEQUENCE_MAX_LENGTH
      );

      expect(switchingTransaction).toEqual(
        expect.objectContaining({
          ...expectedSwitchingTransaction,
          inAuditData: {
            ...expectedSwitchingTransaction.inAuditData,
            inSequence: switchingTransaction.inAuditData.inSequence
          }
        })
      );
    });

    it('should map to switching with empty string with missing fields on input model', async () => {
      const switchingTransaction = await mapToSwitchingTransaction({
        ...transactionModel,
        sourceAccountNo: undefined,
        beneficiaryAccountNo: undefined,
        beneficiaryRtolCode: undefined,
        sourceRtolCode: undefined,
        thirdPartyOutgoingId: undefined
      });
      expect(switchingTransaction).toEqual({
        ...expectedSwitchingTransaction,
        inAuditData: {
          ...expectedSwitchingTransaction.inAuditData,
          inSequence: switchingTransaction.inAuditData.inSequence
        },
        inAcctId1: '',
        inAcctId2: '',
        inDestInstId: '',
        inAcqInstId: '',
        inSTAN: '',
        inRRN: ''
      });
    });

    it('should throw error when currency is not found from ms-configuration', async () => {
      mockCurrencyRetrieval(undefined);
      const error = await mapToSwitchingTransaction(transactionModel).catch(
        err => err
      );
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.CURRENCY_NOT_FOUND);
    });
    it('should throw error when currency code is undefined', async () => {
      const error = await mapToSwitchingTransaction({
        ...transactionModel,
        sourceTransactionCurrency: undefined
      }).catch(err => err);
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(
        ERROR_CODE.SOURCE_TRANSACTION_CURRENCY_UNDEFINED
      );
    });
    it('should throw error (while generating generateInSequenceId) when debit transaction code is misssing', async () => {
      const error = await mapToSwitchingTransaction({
        ...transactionModel,
        debitTransactionCode: undefined
      }).catch(err => err);
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(
        ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE
      );
    });

    it('should throw error when sequence number overflowed', async () => {
      mockOutgoingCounter({
        prefix: '20123T',
        counter: 10000000001 // base 1
      });
      const error = await mapToSwitchingTransaction(transactionModel).catch(
        err => err
      );
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.MAXIMUM_COUNTER_REACHED);
    });
  });

  describe('createInAddData', () => {
    it('should create idAddData with length is 40', () => {
      // given
      const model: ITransactionModel = {
        ...getTransactionDetail(),
        sourceAccountName: '123',
        beneficiaryAccountName: '456'
      };

      // when
      const name = createInAddData(model);

      // then
      expect(name.length).toBe(40);
    });

    it('should create idAddData by 20 first character of source account name and 20 first chars of beneficiary account name', () => {
      // given
      const model: ITransactionModel = {
        ...getTransactionDetail(),
        sourceAccountName: '0000000000000000000022',
        beneficiaryAccountName: '1111111111111111111133'
      };

      // when
      const name = createInAddData(model);

      // then
      expect(name).toBe('0000000000000000000011111111111111111111');
    });
  });
});
