import { BaseExternalTransactionInput } from '../transaction.type';
import { PaymentServiceTypeEnum } from '@dk/module-common';
import { ITransactionModel } from '../transaction.model';
import { TransactionStatus } from '../transaction.enum';
import transactionExternalValidator from '../transactionExternal.validator';
import { getTransactionModelFullData } from '../__mocks__/transaction.data';
import { ERROR_CODE } from '../../common/errors';
import { AppError } from '../../errors/AppError';

describe('transactionExternalValidator', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('validateRefundRequest', () => {
    it('should throw error INVALID_REFUND_REQUEST when original transaction status is other than SUCCEED', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 30000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO,
        notes: '243780485742'
      };
      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.DECLINED
      };
      // When
      try {
        transactionExternalValidator.validateRefundRequest(
          payload,
          originalTransaction
        );
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(AppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });

    it('should throw error INVALID_REFUND_REQUEST when original transaction beneficiaryAccountNo is equal to originalTransaction sourceAccountNo ', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 30000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO,
        notes: '243780485742'
      };
      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.SUCCEED
      };
      // When
      try {
        transactionExternalValidator.validateRefundRequest(
          payload,
          originalTransaction
        );
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(AppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });

    it('should throw error INVALID_REFUND_REQUEST when refund ammount is more than original transaction ammount ', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 30000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO,
        notes: '243780485742'
      };
      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.SUCCEED,
        sourceAccountNo: payload.beneficiaryAccountNo,
        transactionAmount: payload.transactionAmount - 500
      };
      // When
      try {
        transactionExternalValidator.validateRefundRequest(
          payload,
          originalTransaction
        );
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(AppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });

    it('should throw error INVALID_REFUND_REQUEST when 2nd transaction refund ammount is more than refund remaining ammount ', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 1200,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO,
        notes: '243780485742'
      };
      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.SUCCEED,
        sourceAccountNo: payload.beneficiaryAccountNo,
        transactionAmount: 3000,
        refund: { remainingAmount: 0 }
      };
      // When
      try {
        transactionExternalValidator.validateRefundRequest(
          payload,
          originalTransaction
        );
      } catch (err) {
        // Then
        expect(err).toBeInstanceOf(AppError);
        expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
      }
    });

    it('Should success for 1st refund transaction', async () => {
      // Given
      const payload: BaseExternalTransactionInput = {
        transactionAmount: 10000,
        beneficiaryAccountNo: '100942531246',
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT,
        notes: '243780485742',
        sourceBankCode: 'SC001',
        beneficiaryBankCode: 'BC001'
      };
      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...newTransactionModel,
        status: TransactionStatus.SUCCEED,
        sourceAccountNo: payload.beneficiaryAccountNo,
        transactionAmount: 30000,
        paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT,
        beneficiaryBankCode: 'SC001',
        sourceBankCode: 'BC001'
      };
      // When

      const result = transactionExternalValidator.validateRefundRequest(
        payload,
        originalTransaction
      );
      expect(result).toBeUndefined();
    });

    // TODO: (Dikshit)commenting it for now can be enabled later once we will get nore calrity
    // it('should throw error INVALID_REFUND_REQUEST when source and beneficiary bank Codes are not same ', async () => {
    //   // Given
    //   const payload: BaseExternalTransactionInput = {
    //     transactionAmount: 10000,
    //     beneficiaryAccountNo: '100942531246',
    //     paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT,
    //     notes: '243780485742',
    //     sourceBankCode: 'SC002',
    //     beneficiaryBankCode: 'BC001'
    //   };
    //   let newTransactionModel = getTransactionModelFullData();
    //   const originalTransaction: ITransactionModel = {
    //     id: '5e412688fd14310c4969099a',
    //     ...newTransactionModel,
    //     status: TransactionStatus.SUCCEED,
    //     sourceAccountNo: payload.beneficiaryAccountNo,
    //     transactionAmount: 30000,
    //     paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT,
    //     beneficiaryBankCode: 'SC001',
    //     sourceBankCode: 'BC001'
    //   };
    //   // When
    //   try {
    //     transactionExternalValidator.validateRefundRequest(
    //       payload,
    //       originalTransaction
    //     );
    //   } catch (err) {
    //     // Then
    //     expect(err).toBeInstanceOf(AppError);
    //     expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REFUND_REQUEST);
    //   }
    // });
  });
});
