import transactionConsumer from '../transaction.consumer';
import { IMessageProcessorCallbackPayload } from '../../common/kafkaConsumer.type';
import transactionService from '../transaction.service';
import transactionBlocking from '../transactionBlocking.service';
import transactionBlockingService from '../transactionBlocking.service';
import { ITransferTransactionInput } from '../transaction.type';
import {
  getITransferTransactionInput,
  getTransactionDetailWithFeeAmount,
  getTransactionModelFullData
} from '../__mocks__/transaction.data';
import { ITransactionRetryReversalMessage } from '../transaction.producer.type';
import transactionReversalService from '../transactionReversal.service';
import transactionProducer from '../transaction.producer';
import _ from 'lodash';
import transactionHelper from '../transaction.helper';
import {
  PaymentRequestStatus,
  TransactionStatus,
  TransferFailureReasonActor
} from '../transaction.enum';
import {
  Constant,
  PaymentServiceTypeEnum,
  TransactionExecutionTypeEnum
} from '@dk/module-common';
import { PaymentInstructionTopicConstant } from '@dk/module-message';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import transactionRepository from '../transaction.repository';
import { setRetryCount } from '../../common/contextHandler';
import { TRANSACTION_RESPONSE_TOPIC_FIELD } from '../transaction.constant';
import generalRefundService from '../refund/generalRefund.service';
import entitlementFlag from '../../common/entitlementFlag';
import entitlementService from '../../entitlement/entitlement.service';
import { AppError } from '@dk/module-common/dist/error/AppError';

jest.mock('../transaction.service');
jest.mock('../refund/generalRefund.service');
jest.mock('../transactionBlocking.service');
jest.mock('../transactionReversal.service');
jest.mock('../transaction.producer');
jest.mock('../transaction.helper');
jest.mock('../transaction.repository');
jest.mock('../../common/contextHandler');

describe('transaction.consumer', () => {
  afterEach(() => {
    jest.resetAllMocks();
    expect.hasAssertions();
  });

  const transactionInput: ITransferTransactionInput = getITransferTransactionInput();
  delete transactionInput.sourceCustomerId;
  const messageConsumer: IMessageProcessorCallbackPayload = {
    message: {
      key: 'string',
      value: JSON.stringify(transactionInput),
      headers: {}
    },
    topicName: 'test',
    partition: 0
  };

  describe('autoTransferTransactionHandler', () => {
    const expectedTransactionResponse = {
      id: '5da02ad923d9f74ced0087d1',
      transactionIds: ['1', '2']
    };

    it('should return valid data and no produce message back', async () => {
      (transactionService.createTransferTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );

      (setRetryCount as jest.Mock).mockResolvedValueOnce(null);

      await transactionConsumer.autoTransferTransactionHandler(messageConsumer);

      expect(transactionService.createTransferTransaction).toBeCalledWith(
        transactionInput
      );
    });

    it('should return valid data and produce message back', async () => {
      (transactionService.createTransferTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );
      (transactionProducer.sendTransactionMessage as jest.Mock).mockResolvedValueOnce(
        {}
      );
      messageConsumer.message.headers = {
        'service-name': 'payment-instruction'
      };

      await transactionConsumer.autoTransferTransactionHandler(messageConsumer);

      expect(transactionService.createTransferTransaction).toBeCalledWith(
        transactionInput
      );
    });

    it('should return valid data and no produce message back because not existed service-name', async () => {
      (transactionService.createTransferTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );
      (setRetryCount as jest.Mock).mockResolvedValueOnce(null);

      await transactionConsumer.autoTransferTransactionHandler(messageConsumer);

      expect(transactionService.createTransferTransaction).toBeCalledWith(
        transactionInput
      );
    });
  });

  describe('directTransactionHandler', () => {
    beforeEach(() => {
      jest.resetAllMocks();
    });

    const expectedTransactionResponse = {
      id: '5da02ad923d9f74ced0087d1',
      transactionIds: ['1', '2'],
      transactionAmount: 9000,
      fees: [{ feeAmount: 1000 }, { feeAmount: 2000 }]
    };

    it('should return valid data and no produce message back because not existed service-name', async () => {
      (transactionService.createTransferTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );
      (setRetryCount as jest.Mock).mockResolvedValueOnce(null);

      await transactionConsumer.directTransactionHandler(messageConsumer);

      expect(transactionService.createTransferTransaction).toBeCalledWith(
        transactionInput
      );
    });

    it('should process directTransactionHandler with offer claim id successfully', async () => {
      const transactionInputSample: ITransferTransactionInput = {
        ...getITransferTransactionInput()
      };

      const paymentInstructionCode = 'PI_TRANSFER';

      delete transactionInputSample.sourceCustomerId;
      const messageConsumerSample: IMessageProcessorCallbackPayload = {
        message: {
          key: 'string',
          value: JSON.stringify(transactionInputSample),
          headers: {
            [Constant.PI_KEY_HEADER]: paymentInstructionCode,
            [Constant.EXECUTION_TYPE]:
              TransactionExecutionTypeEnum.DIRECT_TRANSFER
          }
        },
        topicName: 'test',
        partition: 0
      };
      (transactionService.createTransferTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );

      await transactionConsumer.directTransactionHandler(messageConsumerSample);

      expect(transactionService.createTransferTransaction).toBeCalledWith({
        ...transactionInputSample,
        paymentInstructionExecution: {
          paymentInstructionCode,
          executionType: TransactionExecutionTypeEnum.DIRECT_TRANSFER,
          topics: [PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE]
        }
      });
      expect(transactionProducer.sendTransactionMessage).toBeCalledWith(
        PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE,
        expect.objectContaining({
          status: PaymentRequestStatus.SUCCEED
        }),
        TransactionExecutionTypeEnum.DIRECT_TRANSFER,
        'PI_TRANSFER'
      );
    });

    it(`should execution transaction with type = CREATE_BLOCKING return valid data and produce message back`, async () => {
      (transactionBlocking.createBlockingTransferTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );
      (transactionProducer.sendTransactionMessage as jest.Mock).mockResolvedValueOnce(
        {}
      );

      messageConsumer.message.headers = {
        'service-name': 'payment-instruction',
        [Constant.EXECUTION_TYPE]: TransactionExecutionTypeEnum.CREATE_BLOCKING
      };
      const producerSerializedValue = {
        externalId: transactionInput.externalId,
        status: 'SUCCEED',
        extra: {
          additionalInformation1: transactionInput.additionalInformation1,
          additionalInformation2: transactionInput.additionalInformation2,
          additionalInformation3: undefined,
          additionalInformation4: undefined,
          sourceCIF: transactionInput.sourceCIF
        },
        transactionId: expectedTransactionResponse.id,
        paymentRequestID: undefined,
        referenceId: undefined,
        transactionAmount: undefined,
        transactionStatus: undefined
      };

      await transactionConsumer.directTransactionHandler(messageConsumer);
      expect(
        transactionBlocking.createBlockingTransferTransaction
      ).toBeCalledWith(transactionInput);
      expect(transactionProducer.sendTransactionMessage).toBeCalledWith(
        PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE,
        producerSerializedValue,
        TransactionExecutionTypeEnum.CREATE_BLOCKING,
        undefined
      );
    });

    it(`should execution transaction with type = DIRECT_TRANSFER return valid data and produce message back`, async () => {
      const payload = JSON.parse(messageConsumer.message.value);
      const expectedTransactionResult = getTransactionDetailWithFeeAmount(
        expectedTransactionResponse,
        payload
      );
      (transactionService.createTransferTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );
      (transactionProducer.sendTransactionMessage as jest.Mock).mockResolvedValueOnce(
        {}
      );

      messageConsumer.message.headers = {
        'service-name': 'payment-instruction',
        [Constant.PI_KEY_HEADER]: 'PI_TRANSFER',
        [Constant.EXECUTION_TYPE]: TransactionExecutionTypeEnum.DIRECT_TRANSFER
      };

      await transactionConsumer.directTransactionHandler(messageConsumer);
      expect(transactionService.createTransferTransaction).toBeCalledWith({
        ...transactionInput,
        paymentInstructionExecution: {
          paymentInstructionCode: 'PI_TRANSFER',
          executionType: TransactionExecutionTypeEnum.DIRECT_TRANSFER,
          topics: [PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE]
        }
      });
      expect(transactionProducer.sendTransactionMessage).toHaveBeenCalledWith(
        PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE,
        expectedTransactionResult,
        TransactionExecutionTypeEnum.DIRECT_TRANSFER,
        'PI_TRANSFER'
      );
    });

    it(`should execute GENERAL_REFUND transaction with type = DIRECT_TRANSFER return valid data and produce message back`, async () => {
      const input = {
        ...transactionInput,
        paymentServiceType: PaymentServiceTypeEnum.GENERAL_REFUND,
        generalRefund: {
          transactionId: 'transactionId'
        }
      };
      const messageConsumer: IMessageProcessorCallbackPayload = {
        message: {
          key: 'string',
          value: JSON.stringify(input),
          headers: {}
        },
        topicName: 'test',
        partition: 0
      };
      const payload = JSON.parse(messageConsumer.message.value);
      const expectedTransactionResult = getTransactionDetailWithFeeAmount(
        expectedTransactionResponse,
        payload
      );
      (generalRefundService.refundTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );
      (transactionProducer.sendTransactionMessage as jest.Mock).mockResolvedValueOnce(
        {}
      );

      messageConsumer.message.headers = {
        'service-name': 'payment-instruction',
        [Constant.PI_KEY_HEADER]: 'PI_TRANSFER',
        [Constant.EXECUTION_TYPE]: TransactionExecutionTypeEnum.DIRECT_TRANSFER
      };

      await transactionConsumer.directTransactionHandler(messageConsumer);
      expect(generalRefundService.refundTransaction).toBeCalledWith(
        input.generalRefund
      );
      expect(transactionProducer.sendTransactionMessage).toHaveBeenCalledWith(
        PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE,
        expectedTransactionResult,
        TransactionExecutionTypeEnum.DIRECT_TRANSFER,
        'PI_TRANSFER'
      );
    });

    it('should execution transaction with type = ADJUST_BLOCKING return valid data and produce message back', async () => {
      (transactionBlocking.adjustBlockingTransferTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );
      messageConsumer.message.headers = {
        [Constant.EXECUTION_TYPE]: TransactionExecutionTypeEnum.ADJUST_BLOCKING
      };
      await transactionConsumer.directTransactionHandler(messageConsumer);
      expect(
        transactionBlockingService.adjustBlockingTransferTransaction
      ).toBeCalledWith(
        transactionInput.sourceCIF,
        transactionInput.externalId,
        transactionInput.transactionAmount
      );
    });

    describe('execution transaction with type = CANCEL_BLOCKING', () => {
      it('should return valid data and produce message back', async () => {
        (transactionBlocking.cancelBlockingTransferTransaction as jest.Mock).mockResolvedValueOnce(
          expectedTransactionResponse
        );
        messageConsumer.message.headers = {
          [Constant.EXECUTION_TYPE]:
            TransactionExecutionTypeEnum.CANCEL_BLOCKING
        };
        await transactionConsumer.directTransactionHandler(messageConsumer);
        expect(
          transactionBlockingService.cancelBlockingTransferTransaction
        ).toBeCalledWith(
          transactionInput.sourceCIF,
          transactionInput.externalId
        );
      });
    });

    describe('execution transaction with type = CONFIRM_BLOCKING', () => {
      it('should return valid data and produce message back', async () => {
        (transactionBlocking.executeBlockingTransferTransaction as jest.Mock).mockResolvedValueOnce(
          expectedTransactionResponse
        );
        messageConsumer.message.headers = {
          [Constant.EXECUTION_TYPE]:
            TransactionExecutionTypeEnum.CONFIRM_BLOCKING
        };
        await transactionConsumer.directTransactionHandler(messageConsumer);
        expect(
          transactionBlockingService.executeBlockingTransferTransaction
        ).toBeCalledWith(
          transactionInput.sourceCIF,
          transactionInput.externalId,
          transactionInput.transactionAmount
        );
      });
    });

    it('should execution transaction with type = CREATE_BLOCKING return valid data and produce message back', async () => {
      (transactionBlocking.createBlockingTransferTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );
      messageConsumer.message.headers = {
        [Constant.EXECUTION_TYPE]: TransactionExecutionTypeEnum.CREATE_BLOCKING
      };
      await transactionConsumer.directTransactionHandler(messageConsumer);
      expect(
        transactionBlockingService.createBlockingTransferTransaction
      ).toBeCalled();
    });

    it('should do top up if paymentServiceType is WALLET', async () => {
      (transactionService.createInternalTopupTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );

      const topupMessage = {
        ...messageConsumer,
        message: {
          ...messageConsumer.message,
          headers: {
            [Constant.EXECUTION_TYPE]:
              TransactionExecutionTypeEnum.DIRECT_TRANSFER
          },
          value: JSON.stringify({
            ...transactionInput,
            paymentServiceType: PaymentServiceTypeEnum.WALLET
          })
        }
      };

      await transactionConsumer.directTransactionHandler(topupMessage);
      expect(transactionService.createInternalTopupTransaction).toBeCalledWith(
        _.omit(
          {
            ...transactionInput,
            paymentServiceType: PaymentServiceTypeEnum.WALLET
          },
          ['beneficiaryCIF', 'sourceBankCode']
        )
      );
    });

    it(`should send status DECLINED when CREATE_BLOCKING throw error`, async () => {
      const transaction = {
        ...getTransactionModelFullData(),
        id: '5da02ad923d9f74ced0087d1',
        failureReason: {
          detail: 'detail',
          actor: 'actor',
          type: 'type'
        }
      };
      const payload = JSON.parse(messageConsumer.message.value);
      const transferAppError = new TransferAppError(
        'Detail',
        TransferFailureReasonActor.UNKNOWN,
        ERROR_CODE.UNEXPECTED_ERROR
      );
      (transactionBlocking.createBlockingTransferTransaction as jest.Mock).mockRejectedValueOnce(
        transferAppError
      );
      (transactionRepository.getByExternalId as jest.Mock).mockResolvedValueOnce(
        transaction
      );
      (transactionProducer.sendTransactionMessage as jest.Mock).mockResolvedValueOnce(
        {}
      );

      messageConsumer.message.headers = {
        'service-name': 'payment-instruction',
        [Constant.EXECUTION_TYPE]: TransactionExecutionTypeEnum.CREATE_BLOCKING
      };
      const producerSerializedValue = {
        externalId: transactionInput.externalId,
        status: 'DECLINED',
        extra: {
          additionalInformation1: transactionInput.additionalInformation1,
          additionalInformation2: transactionInput.additionalInformation2,
          additionalInformation3: undefined,
          additionalInformation4: undefined,
          sourceCIF: transactionInput.sourceCIF
        },
        transactionAmount: payload.transactionAmount,
        transactionStatus: transaction.status,
        error: {
          details: undefined,
          errorCode: 'UNEXPECTED_ERROR'
        },
        failureReason: {
          detail: 'detail',
          actor: 'actor',
          type: 'type'
        }
      };

      await transactionConsumer.directTransactionHandler(messageConsumer);

      expect(
        transactionBlocking.createBlockingTransferTransaction
      ).toBeCalledWith(transactionInput);
      expect(transactionProducer.sendTransactionMessage).toBeCalledWith(
        PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE,
        producerSerializedValue,
        TransactionExecutionTypeEnum.CREATE_BLOCKING,
        undefined
      );
    });

    it(`should send status DECLINED when CREATE_BLOCKING throw error And not found ByExternalId`, async () => {
      const payload = JSON.parse(messageConsumer.message.value);
      (transactionBlocking.createBlockingTransferTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'detail',
          TransferFailureReasonActor.MS_TRANSFER,
          ERROR_CODE.UNEXPECTED_ERROR
        )
      );
      (transactionRepository.getByExternalId as jest.Mock).mockRejectedValueOnce(
        new AppError(ERROR_CODE.UNIQUE_FIELD)
      );
      (transactionProducer.sendTransactionMessage as jest.Mock).mockResolvedValueOnce(
        {}
      );

      messageConsumer.message.headers = {
        'service-name': 'payment-instruction',
        [Constant.EXECUTION_TYPE]: TransactionExecutionTypeEnum.CREATE_BLOCKING
      };
      const producerSerializedValue = {
        externalId: transactionInput.externalId,
        status: 'DECLINED',
        extra: {
          additionalInformation1: transactionInput.additionalInformation1,
          additionalInformation2: transactionInput.additionalInformation2,
          additionalInformation3: undefined,
          additionalInformation4: undefined,
          sourceCIF: transactionInput.sourceCIF
        },
        transactionAmount: payload.transactionAmount,
        transactionStatus: undefined,
        error: {
          errors: undefined,
          errorCode: 'UNEXPECTED_ERROR',
          retriable: undefined
        },
        failureReason: undefined
      };

      await transactionConsumer.directTransactionHandler(messageConsumer);

      expect(
        transactionBlocking.createBlockingTransferTransaction
      ).toHaveBeenCalledWith(transactionInput);
      expect(transactionRepository.getByExternalId).toHaveBeenCalledWith(
        payload.externalId
      );
      expect(transactionProducer.sendTransactionMessage).toHaveBeenCalledWith(
        PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE,
        producerSerializedValue,
        TransactionExecutionTypeEnum.CREATE_BLOCKING,
        undefined
      );
    });

    it(`should execution transaction with type = DIRECT_TRANSFER return valid data and produce message to default and spesific topic if Transaction-Response-Topic is defined`, async () => {
      const payload = JSON.parse(messageConsumer.message.value);
      const expectedTransactionResult = getTransactionDetailWithFeeAmount(
        expectedTransactionResponse,
        payload
      );
      (transactionService.createTransferTransaction as jest.Mock).mockResolvedValueOnce(
        expectedTransactionResponse
      );
      (transactionProducer.sendTransactionMessage as jest.Mock).mockResolvedValueOnce(
        {}
      );

      messageConsumer.message.headers = {
        'service-name': 'payment-instruction',
        [Constant.PI_KEY_HEADER]: 'PI_TRANSFER',
        [Constant.EXECUTION_TYPE]: TransactionExecutionTypeEnum.DIRECT_TRANSFER,
        [TRANSACTION_RESPONSE_TOPIC_FIELD]: 'test'
      };

      await transactionConsumer.directTransactionHandler(messageConsumer);

      expect(transactionProducer.sendTransactionMessage).toHaveBeenCalledTimes(
        2
      );
      expect(transactionProducer.sendTransactionMessage).toHaveBeenCalledWith(
        PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE,
        expectedTransactionResult,
        TransactionExecutionTypeEnum.DIRECT_TRANSFER,
        'PI_TRANSFER'
      );
      expect(transactionProducer.sendTransactionMessage).toHaveBeenCalledWith(
        'test',
        expectedTransactionResult,
        TransactionExecutionTypeEnum.DIRECT_TRANSFER,
        'PI_TRANSFER'
      );
    });
  });

  describe('retryTransactionReversalHandler', () => {
    const savedTransaction = {
      ...getTransactionModelFullData(),
      referenceId: 'referenceId',
      createdAt: new Date('01 June 2020 00:00:00')
    };

    const mockRevertTransactionSuccess = () => {
      (transactionReversalService.reverseTransactionsByInstructionId as jest.Mock).mockResolvedValueOnce(
        {}
      );
      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValue(
        savedTransaction
      );
    };

    const mockRevertTransactionThrowError = () => {
      (transactionReversalService.reverseTransactionsByInstructionId as jest.Mock).mockRejectedValueOnce(
        new Error()
      );
    };

    let message: ITransactionRetryReversalMessage;
    const expectedRetryPolicy = {
      maxRetries: 1,
      delayTimeInSeconds: 300
    };
    beforeEach(() => {
      message = {
        transactionId: '1293993',
        retryCounter: 1
      };
      jest.resetAllMocks();
    });
    afterEach(() => {
      jest.resetAllMocks();
    });
    it('should process retryReversal command when counter does not exceed max retry', async () => {
      mockRevertTransactionSuccess();
      _.delay = jest.fn();
      await transactionConsumer.retryTransactionReversalHandler(message);

      expect(
        transactionReversalService.reverseTransactionsByInstructionId
      ).toBeCalledTimes(1);
      expect(transactionHelper.updateTransactionStatus).toBeCalledWith(
        message.transactionId,
        TransactionStatus.REVERTED
      );
    });

    it('should sendTransactionReversalFailedEvent when the retry operation throws exception', async () => {
      mockRevertTransactionThrowError();
      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValue(
        savedTransaction
      );

      await transactionConsumer.retryTransactionReversalHandler(message);

      expect(
        transactionReversalService.reverseTransactionsByInstructionId
      ).toBeCalledWith(message.transactionId, undefined, savedTransaction);

      expect(_.delay).not.toBeCalled();
    });

    it('should produce next retry command when the retry operation throws exception', async () => {
      mockRevertTransactionThrowError();
      _.delay = jest.fn();
      message.retryCounter = 0;
      await transactionConsumer.retryTransactionReversalHandler(message);

      expect(
        transactionReversalService.reverseTransactionsByInstructionId
      ).not.toHaveBeenCalled();

      expect(_.delay).not.toBeCalled();
    });

    it('should produce reversal failed event after all retries', async () => {
      message = {
        ...message,
        retryCounter: expectedRetryPolicy.maxRetries
      };
      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValue(
        savedTransaction
      );
      mockRevertTransactionThrowError();
      await transactionConsumer.retryTransactionReversalHandler(message);
      expect(
        transactionReversalService.reverseTransactionsByInstructionId
      ).toBeCalledWith(message.transactionId, undefined, savedTransaction);
      expect(_.delay).not.toBeCalled();
      expect(
        transactionProducer.sendTransactionReversalFailedEvent
      ).toBeCalledWith(message);
    });

    it('should produce failed reversal event when message counter exceeds max retries', async () => {
      message = {
        ...message,
        retryCounter: expectedRetryPolicy.maxRetries + 1
      };
      await transactionConsumer.retryTransactionReversalHandler(message);

      expect(
        transactionProducer.sendTransactionReversalFailedEvent
      ).toBeCalledWith(message);
    });
  });

  describe('retryTransactionReversalHandler', () => {
    it(`should send status DECLINED when CREATE_BLOCKING throw error`, async () => {
      const transaction = {
        ...getTransactionModelFullData(),
        id: '5da02ad923d9f74ced0087d1',
        failureReason: {
          detail: 'detail',
          actor: 'actor',
          type: 'type'
        }
      };
      const payload = JSON.parse(messageConsumer.message.value);
      (transactionBlocking.createBlockingTransferTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.UNEXPECTED_ERROR
        )
      );
      (transactionRepository.getByExternalId as jest.Mock).mockResolvedValueOnce(
        transaction
      );
      (transactionProducer.sendTransactionMessage as jest.Mock).mockResolvedValueOnce(
        {}
      );
      entitlementFlag.isEnabled = jest.fn().mockResolvedValue(true);
      entitlementService.processReversalEntitlementCounterUsageBasedOnContext = jest
        .fn()
        .mockResolvedValue({});
      messageConsumer.message.headers = {
        'service-name': 'payment-instruction',
        [Constant.EXECUTION_TYPE]: TransactionExecutionTypeEnum.CREATE_BLOCKING
      };
      payload.paymentInstructionId = '123';
      messageConsumer.message.value = JSON.stringify(payload);
      transactionInput.paymentInstructionId = '123';
      const producerSerializedValue = {
        externalId: transactionInput.externalId,
        status: 'DECLINED',
        extra: {
          additionalInformation1: transactionInput.additionalInformation1,
          additionalInformation2: transactionInput.additionalInformation2,
          additionalInformation3: undefined,
          additionalInformation4: undefined,
          sourceCIF: transactionInput.sourceCIF
        },
        transactionAmount: payload.transactionAmount,
        transactionStatus: transaction.status,
        error: {
          details: undefined,
          errorCode: 'UNEXPECTED_ERROR'
        },
        failureReason: {
          detail: 'detail',
          actor: 'actor',
          type: 'type'
        },
        paymentInstructionId: '123'
      };

      await transactionConsumer.directTransactionHandler(messageConsumer);

      expect(
        transactionBlocking.createBlockingTransferTransaction
      ).toBeCalledWith(payload);
      expect(transactionProducer.sendTransactionMessage).toBeCalledWith(
        PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE,
        producerSerializedValue,
        TransactionExecutionTypeEnum.CREATE_BLOCKING,
        undefined
      );
      expect(
        entitlementService.processReversalEntitlementCounterUsageBasedOnContext
      ).toBeCalled();
    });
  });
});
