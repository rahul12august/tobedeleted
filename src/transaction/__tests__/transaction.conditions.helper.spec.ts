import { toIndonesianRupiah } from '../../common/currencyFormatter';
import { ERROR_CODE } from '../../common/errors';
import { ILimitGroup } from '../../configuration/configuration.type';
import { RuleConditionState } from '../transaction.conditions.enum';
import transactionConditionHelperService from '../transaction.conditions.helper';
import { IDR_CURRENCY_PREFIX } from '../transaction.constant';

describe('transactionConditionHelperService', () => {
  const limitGroupInfo: ILimitGroup = {
    code: 'L002',
    dailyLimitAmount: 15000000
  };

  describe('compareAmountWithDailyLimit', () => {
    it(`
    GIVEN the sum of transaction amount with the current accumulation sum 
    is still within the dailyLimit,
    THEN return ACCEPTED
    `, () => {
      const result = transactionConditionHelperService.compareAmountWithDailyLimit(
        limitGroupInfo,
        1000000,
        11000000
      );

      expect(result).toBe(RuleConditionState.ACCEPTED);
    });

    it(`
    GIVEN the sum of transaction amount with the current accumulation sum 
    is equal to the dailyLimit,
    THEN return ACCEPTED
    `, () => {
      const result = transactionConditionHelperService.compareAmountWithDailyLimit(
        limitGroupInfo,
        15000000,
        0
      );

      expect(result).toBe(RuleConditionState.ACCEPTED);
    });

    it(`
    GIVEN 
    - transaction amount + dailyAmountSumSoFar > dailyLimit
    - transaction amount is less or equal to dailyLimit,
    THEN return TRY_TOMORROW
    `, () => {
      const result = transactionConditionHelperService.compareAmountWithDailyLimit(
        limitGroupInfo,
        5000000,
        11000000
      );

      expect(result).toBe(RuleConditionState.TRY_TOMORROW);
    });

    it(`
    GIVEN the transactionSum alone exceeds the dailyLimit 
    THEN return DECLINED
    `, () => {
      const result = transactionConditionHelperService.compareAmountWithDailyLimit(
        limitGroupInfo,
        15000001,
        0
      );

      expect(result).toBe(RuleConditionState.DECLINED);
    });
  });

  describe('constructDailyLimitEvaluatedRuleConditionStatus', () => {
    const errorCodeDeclined =
      ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT;
    const errorCodeTryTomorrow =
      ERROR_CODE.ATM_WITHDRAWAL_ACCUMULATION_SUM_EXCEEDS_DAILY_LIMIT;

    it(`
        GIVEN the state is accepted,
        return the appropriate message
    `, () => {
      const currentTransactionAmount = 1000000;
      const dailyAmountSumSoFar = 0;
      const dailyLimit = 15000000;

      const result = transactionConditionHelperService.constructDailyLimitEvaluatedRuleConditionStatus(
        currentTransactionAmount,
        dailyAmountSumSoFar,
        'ATM WITHDRAWAL',
        dailyLimit,
        errorCodeDeclined,
        errorCodeTryTomorrow,
        RuleConditionState.ACCEPTED
      );

      const expectedMessage = `The sum of the total daily amount plus the current transaction is: ${currentTransactionAmount} which is in  the daily limit of ATM WITHDRAWAL: ${dailyLimit}`;
      expect(result.message).toEqual(expectedMessage);
      expect(result.errorCode).toBeUndefined();
    });

    it(`GIVEN the state is declined,
    return the appropriate message + the error code`, () => {
      const currentTransactionAmount = 15000001;
      const dailyAmountSumSoFar = 0;
      const dailyLimit = 15000000;

      const result = transactionConditionHelperService.constructDailyLimitEvaluatedRuleConditionStatus(
        currentTransactionAmount,
        dailyAmountSumSoFar,
        'ATM WITHDRAWAL',
        dailyLimit,
        errorCodeDeclined,
        errorCodeTryTomorrow,
        RuleConditionState.DECLINED
      );

      const expectedMessage = `The sum of the total daily amount plus the current transaction is: ${currentTransactionAmount} which is beyond  the daily limit of ATM WITHDRAWAL: ${dailyLimit}. The amount itself is greater than the daily limit.`;

      expect(result.message).toEqual(expectedMessage);
      expect(result.errorCode).toEqual(errorCodeDeclined);
    });

    it(`GIVEN the state is try tomorrow,
    return the appropriate message + the error code`, () => {
      const currentTransactionAmount = 4000000;
      const dailyAmountSumSoFar = 12000000;
      const dailyLimit = 15000000;

      const result = transactionConditionHelperService.constructDailyLimitEvaluatedRuleConditionStatus(
        currentTransactionAmount,
        dailyAmountSumSoFar,
        'ATM WITHDRAWAL',
        dailyLimit,
        errorCodeDeclined,
        errorCodeTryTomorrow,
        RuleConditionState.TRY_TOMORROW
      );

      const expectedMessage = `The sum of the total daily amount plus the current transaction is: ${currentTransactionAmount +
        dailyAmountSumSoFar} which is beyond  the daily limit of ATM WITHDRAWAL: ${dailyLimit}. But you can try it tomorrow.`;

      expect(result.message).toEqual(expectedMessage);
      expect(result.errorCode).toEqual(errorCodeTryTomorrow);
    });
  });

  describe('constructDailyLimitEvaluatedRuleConditionStatusRefined', () => {
    const errorCodeDeclined =
      ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT;
    const errorCodeTryTomorrow =
      ERROR_CODE.ATM_WITHDRAWAL_ACCUMULATION_SUM_EXCEEDS_DAILY_LIMIT;

    const getMessage = (
      state: RuleConditionState,
      currentTransactionAmount: number,
      dailyAmountSumSoFar: number,
      dailyLimit: number,
      tag: string
    ) => {
      const currentAmountFormatted = `${IDR_CURRENCY_PREFIX} ${toIndonesianRupiah(
        currentTransactionAmount
      )}`;
      const dailyLimitFormatted = `${IDR_CURRENCY_PREFIX} ${toIndonesianRupiah(
        dailyLimit
      )}`;

      const remainingAmountAvailableToWithdraw =
        dailyLimit - dailyAmountSumSoFar;
      const remainingAmountAvailableToWithdrawFormatted = `${IDR_CURRENCY_PREFIX} ${toIndonesianRupiah(
        remainingAmountAvailableToWithdraw
      )}`;

      switch (state) {
        case RuleConditionState.ACCEPTED:
          return `${tag} SUCCESS`;
        case RuleConditionState.DECLINED:
          return `Current ${tag}'s transaction amount [${currentAmountFormatted}] exceeds ${tag}'s Daily Limit [${dailyLimitFormatted}]`;
        case RuleConditionState.TRY_TOMORROW:
          return `${tag}'s Daily Limit Exceeded. Attempted to perform transaction with amount [${currentAmountFormatted}], however only [${remainingAmountAvailableToWithdrawFormatted}] is available for ${tag}'s transaction`;
        default:
          return 'NONE';
      }
    };

    it(`
        GIVEN the state is accepted,
        return the appropriate message
    `, () => {
      const currentTransactionAmount = 1000000;
      const dailyAmountSumSoFar = 0;
      const dailyLimit = 15000000;
      const tag = 'ATM WITHDRAWAL';

      const result = transactionConditionHelperService.constructDailyLimitEvaluatedRuleConditionStatusRefined(
        currentTransactionAmount,
        dailyAmountSumSoFar,
        tag,
        dailyLimit,
        errorCodeDeclined,
        errorCodeTryTomorrow,
        RuleConditionState.ACCEPTED
      );

      const expectedMessage = getMessage(
        RuleConditionState.ACCEPTED,
        currentTransactionAmount,
        dailyAmountSumSoFar,
        dailyLimit,
        tag
      );

      expect(result.message).toEqual(expectedMessage);
      expect(result.errorCode).toBeUndefined();
    });

    it(`GIVEN the state is declined,
    return the appropriate message + the error code`, () => {
      const currentTransactionAmount = 15000001;
      const dailyAmountSumSoFar = 0;
      const dailyLimit = 15000000;
      const tag = 'ATM WITHDRAWAL';

      const result = transactionConditionHelperService.constructDailyLimitEvaluatedRuleConditionStatusRefined(
        currentTransactionAmount,
        dailyAmountSumSoFar,
        tag,
        dailyLimit,
        errorCodeDeclined,
        errorCodeTryTomorrow,
        RuleConditionState.DECLINED
      );

      const expectedMessage = getMessage(
        RuleConditionState.DECLINED,
        currentTransactionAmount,
        dailyAmountSumSoFar,
        dailyLimit,
        tag
      );

      expect(result.message).toEqual(expectedMessage);
      expect(result.errorCode).toEqual(errorCodeDeclined);
    });

    it(`GIVEN the state is try tomorrow,
    return the appropriate message + the error code`, () => {
      const currentTransactionAmount = 4000000;
      const dailyAmountSumSoFar = 12000000;
      const dailyLimit = 15000000;
      const tag = 'ATM WITHDRAWAL';

      const result = transactionConditionHelperService.constructDailyLimitEvaluatedRuleConditionStatusRefined(
        currentTransactionAmount,
        dailyAmountSumSoFar,
        tag,
        dailyLimit,
        errorCodeDeclined,
        errorCodeTryTomorrow,
        RuleConditionState.TRY_TOMORROW
      );

      const expectedMessage = getMessage(
        RuleConditionState.TRY_TOMORROW,
        currentTransactionAmount,
        dailyAmountSumSoFar,
        dailyLimit,
        tag
      );

      expect(result.message).toEqual(expectedMessage);
      expect(result.errorCode).toEqual(errorCodeTryTomorrow);
    });
  });
});
