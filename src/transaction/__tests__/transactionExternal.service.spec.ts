import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import {
  getBankCode,
  getITransferTransactionInput,
  getWalletPartnerBankCode,
  createExternalTransactionInput,
  getTransactionMockValueObject,
  getRecommendedService,
  getTransactionModelFullData
} from '../__mocks__/transaction.data';
import {
  getBankCodeData,
  getDecisionEngineConfiguration
} from '../../configuration/__mocks__/configuration.data';
import awardRepository from '../../award/award.repository';
import {
  ITransactionAccount,
  TopupExternalTransactionPayload,
  ITransferTransactionInput,
  RecommendationService
} from '../transaction.type';
import { IVirtualAccount } from '../../virtualAccount/virtualAccount.type';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../transaction.enum';
import {
  Constant,
  BankChannelEnum,
  PaymentServiceTypeEnum
} from '@dk/module-common';
import transactionRepository from '../transaction.repository';
import transactionHelper from '../transaction.helper';
import externalTransactionService from '../transactionExternal.service';
import transactionProducer from '../transaction.producer';
import virtualAccountRepository from '../../virtualAccount/virtualAccount.repository';
import depositRepository from '../../coreBanking/deposit.repository';
import configurationRepository from '../../configuration/configuration.repository';
import decisionEngineService from '../../decisionEngine/decisionEngine.service';
import { createIncomingTransactionExternalId } from '../transaction.util';
import refundValidator from '../refund/refund.validator';
import { omit } from 'lodash';
import accountCache from '../../account/account.cache';
import configurationService from '../../configuration/configuration.service';
import monitoringEventProducer from '../../monitoringEvent/monitoringEvent.producer';
import { ExecutionMethodEnum, JAGO_BANK_CODE } from '../transaction.constant';
import { IFailureReasonModel } from '../transaction.model';

jest.mock('../../account/account.cache');
jest.mock('../../configuration/configuration.repository');
jest.mock('../../logger');
jest.mock('../../coreBanking/deposit.repository');
jest.mock('../../award/award.repository');
jest.mock('../../account/account.repository');
jest.mock('../../transaction/transaction.producer');
jest.mock('../../virtualAccount/virtualAccount.repository');
jest.mock('../transaction.repository');
jest.mock('../refund/refund.validator');
jest.mock('../../monitoringEvent/monitoringEvent.producer');
jest.mock('../transaction.helper', () => {
  const original = require.requireActual('../transaction.helper').default;
  return {
    ...original,
    mapAuthenticationRequired: jest.fn(),
    mapFeeData: jest.fn(),
    getAccountInfo: jest.fn(),
    getSourceAccount: jest.fn(),
    validateTransactionLimitAmount: jest.fn(),
    getPaymentServiceMappings: jest.fn()
  };
});
jest.mock('../../decisionEngine/decisionEngine.service.ts');
jest.mock('../../configuration/configuration.service', () => {
  const original = require.requireActual(
    '../../configuration/configuration.service'
  ).default;
  return {
    ...original,
    getParameterSettingByReferenceGroup: jest.fn()
  };
});

describe('transactionService', () => {
  const transactionId = 'any_id';
  const {
    sourceBankCode: bankCode,
    beneficiaryBankCode,
    sourceAccount,
    sourceAccountForWithdrawal,
    beneficiaryAccount,
    transactionCategory,
    ruleConfigs,
    limitGroups,
    usageCounters
  } = getTransactionMockValueObject();

  const serviceCodes = ['p01'];

  const feeRule = { basicFee: { code: 'f01', feeAmountFixed: 1 } };

  const mockedWalletPartnerBankCode = getWalletPartnerBankCode();
  const mockedVirtualAccount: IVirtualAccount = {
    customerName: 'test customer name',
    institution: {
      name: 'gojek',
      bankCode: mockedWalletPartnerBankCode.bankCodeId,
      accountId: '2384738473',
      cif: 'insitution-cif'
    }
  };

  beforeEach(() => {
    (configurationRepository.getTransactionCategory as jest.Mock).mockResolvedValue(
      transactionCategory
    );

    // setup bankCode
    (configurationRepository.getBankCodeMapping as jest.Mock).mockImplementation(
      code => {
        if (code === bankCode.bankCodeId) {
          return bankCode;
        }
        if (code === beneficiaryBankCode.bankCodeId) {
          return beneficiaryBankCode;
        }
        if (code === mockedWalletPartnerBankCode.bankCodeId) {
          return mockedWalletPartnerBankCode;
        }
        return;
      }
    );

    (transactionHelper.mapAuthenticationRequired as jest.Mock).mockImplementation(
      value => value
    );

    (transactionHelper.mapFeeData as jest.Mock).mockImplementation(
      value => value
    );

    (transactionHelper.getAccountInfo as jest.Mock).mockImplementation(
      (accountNo, _, __, ___) => {
        if (accountNo === sourceAccount.accountNumber) {
          return sourceAccount;
        }
        if (accountNo === beneficiaryAccount.accountNumber) {
          return beneficiaryAccount;
        }
        return null;
      }
    );

    (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValue(
      serviceCodes
    );
    (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValue(
      ruleConfigs
    );
    (configurationRepository.getLimitGroupsByCodes as jest.Mock).mockResolvedValue(
      limitGroups
    );
    (configurationRepository.getFeeRule as jest.Mock).mockResolvedValue(
      feeRule
    );
    (awardRepository.getUsageCounters as jest.Mock).mockResolvedValue(
      usageCounters
    );

    // setup default transaction repository
    (transactionRepository.create as jest.Mock).mockImplementation(model => {
      return {
        ...model,
        id: transactionId
      };
    });

    (transactionRepository.createIfNotExist as jest.Mock).mockImplementation(
      model => {
        return {
          ...model,
          id: transactionId
        };
      }
    );

    (transactionRepository.createIfNotExist as jest.Mock).mockImplementation(
      model => {
        return {
          ...model,
          id: transactionId
        };
      }
    );

    (transactionRepository.update as jest.Mock).mockImplementation(
      (id, model) => ({
        ...model,
        id: id
      })
    );

    (transactionRepository.findByIdAndUpdate as jest.Mock).mockImplementation(
      (id, model) => ({
        ...model,
        id: id
      })
    );

    // setup default core banking api
    (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValue(
      999999999
    );

    (depositRepository.makeDeposit as jest.Mock).mockResolvedValue({
      id: 'deposit_id'
    });

    (depositRepository.makeWithdrawal as jest.Mock).mockResolvedValue({
      id: 'withdrawal_id'
    });

    // setup default usage counter
    (awardRepository.updateUsageCounters as jest.Mock).mockResolvedValue({});

    // setup virtual account
    (virtualAccountRepository.inquiryVirtualAccount as jest.Mock).mockResolvedValue(
      mockedVirtualAccount
    );

    (decisionEngineService.runMultipleRoles as jest.Mock).mockResolvedValue(
      true
    );
    (configurationRepository.getDecisionEngineConfig as jest.Mock).mockResolvedValue(
      getDecisionEngineConfiguration()
    );
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('execute transaction', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    describe('createExternalTopupTransaction', () => {
      const beneficiaryAccountNo = `${mockedWalletPartnerBankCode.prefix}1287284734`;
      const topupTransactionInput: TopupExternalTransactionPayload = {
        beneficiaryAccountNo,
        transactionAmount: 15400,
        additionalInformation1: 'note 1',
        additionalInformation2: 'note 2',
        additionalInformation3: 'note 3',
        additionalInformation4: 'note 4',
        note: 'topup notes'
      };

      const mockedBeneficiaryAccount: ITransactionAccount = {
        ...beneficiaryAccount,
        accountName: 'test-account-name',
        accountNumber: '1232434'
      };
      const mockBeneficiaryAccountRetrival = () => {
        (transactionHelper.getAccountInfo as jest.Mock).mockResolvedValueOnce(
          mockedBeneficiaryAccount
        );
      };
      const paymentServiceForWalletFromExternal: RecommendationService[] = [
        {
          paymentServiceCode: 'VA_TOPUP_EXTERNAL',
          creditTransactionCode: [
            {
              transactionCode: 'WTC03',
              transactionCodeInfo: {
                limitGroupCode: 'l01',
                feeRules: 'f01',
                code: 'WTC03',
                minAmountPerTx: 1,
                maxAmountPerTx: 999999,
                channel: BankChannelEnum.INTERNAL
              }
            }
          ],
          transactionAuthenticationChecking: true,
          beneficiaryBankCodeType: 'RTOL',
          requireThirdPartyOutgoingId: false,
          blocking: false // FIXME
        }
      ];
      const ginBankCodeInfo = getBankCode();

      beforeEach(() => {
        mockBeneficiaryAccountRetrival();
        (transactionHelper.mapFeeData as jest.Mock).mockResolvedValue(
          paymentServiceForWalletFromExternal
        );
      });

      afterEach(() => {
        jest.resetAllMocks();
      });

      it('should submit to core banking and persist transaction with valid input for partner wallet', async () => {
        // ACT
        const result = await externalTransactionService.createExternalTopupTransaction(
          topupTransactionInput
        );

        // ASSERT
        expect(result).toEqual(
          expect.objectContaining({
            status: TransactionStatus.SUCCEED
          })
        );
        expect(transactionHelper.getAccountInfo).toBeCalledWith(
          mockedVirtualAccount.institution.accountId,
          mockedVirtualAccount.institution.cif,
          ginBankCodeInfo
        );
      });

      it('should update transaction status to DECLINED and not produce failed transfer when submit to core banking failed', async () => {
        // ARRANGE
        (depositRepository.makeDeposit as jest.Mock).mockRejectedValue({}); // external top-up will deposit to merchant account id

        (transactionRepository.findByIdAndUpdate as jest.Mock).mockImplementation(
          (id, model) => ({
            ...model,
            id: id
          })
        );

        // ACT
        const error = await externalTransactionService
          .createExternalTopupTransaction(topupTransactionInput)
          .catch(err => err);

        // ASSERT
        expect(transactionRepository.update).toBeCalledWith(
          transactionId,
          expect.objectContaining({
            status: TransactionStatus.DECLINED
          })
        );
        expect(error).toBeInstanceOf(Error);
        expect(error.errorCode).toBe(ERROR_CODE.COREBANKING_CREDIT_FAILED);
        expect(
          transactionProducer.sendFailedTransferNotification
        ).not.toBeCalled();
      });

      it('should persist categoryCode when it is provided in payload', async () => {
        // ARRANGE
        const input: TopupExternalTransactionPayload = {
          ...topupTransactionInput,
          categoryCode: 'C058'
        };
        // ACT
        const result = await externalTransactionService.createExternalTopupTransaction(
          input
        );

        // ASSERT
        expect(result).toEqual(
          expect.objectContaining({
            status: TransactionStatus.SUCCEED,
            categoryCode: input.categoryCode
          })
        );
        expect(transactionRepository.create).toBeCalledWith(
          expect.objectContaining({
            status: TransactionStatus.PENDING,
            categoryCode: input.categoryCode
          })
        );
      });

      it('should throw error if categoryCode is invalid', async () => {
        // given
        const input: TopupExternalTransactionPayload = {
          ...topupTransactionInput,
          categoryCode: 'C058'
        };

        (configurationRepository.getTransactionCategory as jest.Mock).mockResolvedValueOnce(
          undefined
        );

        // when
        const error = await externalTransactionService
          .createExternalTopupTransaction(input)
          .catch(err => err);

        // then
        expect(error).toBeInstanceOf(TransferAppError);
        expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REGISTER_PARAMETER);
      });
    });
  });

  describe('createExternalTransaction', () => {
    beforeEach(() => {
      (configurationRepository.inquiryBankAndCurrencyByIncomingCodes as jest.Mock).mockResolvedValue(
        {
          sourceBank: {
            ...getBankCodeData(),
            bankCodeId: Constant.GIN_BANK_CODE_ID
          },
          beneficiaryBank: {
            ...getBankCodeData(),
            channel: BankChannelEnum.INTERNAL
          },
          currency: {
            code: 'IDR'
          }
        }
      );
      (monitoringEventProducer.sendMonitoringEventMessage as jest.Mock).mockResolvedValue(
        {}
      );
    });
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should be throw error 404 for undefined sourceAccountNo', async () => {
      // Given
      const incomingInput = createExternalTransactionInput();
      incomingInput.paymentServiceType = PaymentServiceTypeEnum.IRIS_TO_JAGO;
      incomingInput.sourceAccountNo = undefined;

      // When
      const error = await externalTransactionService
        .createExternalTransaction(incomingInput)
        .catch(e => e);

      // Then
      expect(error.errorCode).toEqual(ERROR_CODE.SOURCE_ACCOUNT_NOT_FOUND);
    });

    it('should be throw error 404 when account not found', async () => {
      // Given
      const incomingInput = createExternalTransactionInput();
      incomingInput.paymentServiceType = PaymentServiceTypeEnum.IRIS_TO_JAGO;

      // When
      const error = await externalTransactionService
        .createExternalTransaction(incomingInput)
        .catch(e => e);

      // Then
      expect(error.errorCode).toEqual(ERROR_CODE.SOURCE_ACCOUNT_NOT_FOUND);
    });

    it('failed checking gobiz account', async () => {
      // Given
      const incomingInput = {
        ...createExternalTransactionInput(),
        sourceBankCode: '000124',
        beneficiaryBankCode: 'MASBIDJ1',
        sourceCIF: 'sourceCIF',
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO
      };

      (configurationService.getParameterSettingByReferenceGroup as jest.Mock).mockResolvedValueOnce(
        undefined
      );

      // When
      const error = await externalTransactionService
        .createExternalTransaction(incomingInput)
        .catch(e => e);

      // Then
      expect(error.errorCode).toEqual(ERROR_CODE.UNAUTHORIZED);
    });

    it('should pass checking gobiz account  and should success create external transfer', async () => {
      // Given
      const incomingInput = {
        ...createExternalTransactionInput(),
        sourceCIF: 'sourceCIF',
        sourceBankCode: '000124',
        beneficiaryBankCode: 'MASBIDJ1',
        paymentServiceType: PaymentServiceTypeEnum.IRIS_TO_JAGO,
        sourceAccountNo: 'sourceAccountNo'
      };

      (configurationService.getParameterSettingByReferenceGroup as jest.Mock).mockResolvedValueOnce(
        [
          {
            code: 'sourceCIF',
            name: 'test',
            description: 'CIF GoBiz DEV'
          }
        ]
      );

      (accountCache.getCachedInquiryAccountInfo as jest.Mock).mockImplementation(
        (_, accountNo) => {
          if (accountNo === sourceAccount.accountNumber) {
            return sourceAccount;
          }

          if (accountNo === beneficiaryAccount.accountNumber) {
            return beneficiaryAccount;
          }
          return;
        }
      );

      // When
      const result = await externalTransactionService.createExternalTransaction(
        incomingInput
      );

      expect(result.id).toBeDefined();
      expect(result.availableBalance).toEqual(999999999);
      expect(
        monitoringEventProducer.sendMonitoringEventMessage
      ).toHaveBeenCalled();
    });

    it('should throw error if source balance cannot pay fee', async () => {
      // Given
      const transactionInput: ITransferTransactionInput = getITransferTransactionInput();
      const feeData = [{ feeAmount: 1 }];

      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        {
          ...getRecommendedService(),
          feeData,
          paymentServiceCode: transactionInput.paymentServiceCode
        }
      ]);

      (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValueOnce(
        transactionInput.transactionAmount
      );

      // When
      const incomingInput = {
        ...createExternalTransactionInput(),
        sourceAccountInfo: { accountNumber: '123' },
        sourceCIF: 'CIF-1',
        sourceBankCode: JAGO_BANK_CODE
      };

      const error = await externalTransactionService
        .createExternalTransaction(incomingInput)
        .catch(e => e);

      // Then
      expect(error.errorCode).toEqual(
        ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
      );
    });

    //
    // Not clear on the test intention. It is not clear why is the recommendationService
    // expected to be empty and generate an error...
    //
    // it('should throw error when recommendationServiceCode is not found', async () => {
    //   // Given
    //   (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
    //     getRecommendedService()
    //   ]);
    //   // When
    //   const incomingInput = {
    //     ...createExternalTransactionInput(),
    //     sourceAccountInfo: { accountNumber: '123' },
    //     sourceCIF: 'CIF-1',
    //     sourceBankCode: JAGO_BANK_CODE
    //   };

    //   const error = await externalTransactionService
    //     .createExternalTransaction(incomingInput)
    //     .catch(e => e);

    //   // Then
    //   expect(error).toBeInstanceOf(TransferAppError);
    //   expect(error.errorCode).toEqual(ERROR_CODE.BALANCE_IS_INSUFFICIENT);
    // });

    describe('submit transactions to core banking after saving data to db', () => {
      const paymentServiceCode = 'p01';

      beforeEach(() => {
        (transactionHelper.mapFeeData as jest.Mock).mockResolvedValue([
          { ...getRecommendedService(), paymentServiceCode }
        ]);
      });

      afterEach(() => {
        jest.resetAllMocks();
      });

      it('should submit transaction to core banking', async () => {
        // given
        const incomingInput = {
          ...createExternalTransactionInput(),
          sourceBankCode: '000124',
          beneficiaryBankCode: 'MASBIDJ1',
          sourceCIF: 'sourceCIF'
        };

        const expectedSourceBankCode = '000124'; // this is normalized bank code
        const expectedBeneficiaryBankCode = 'MASBIDJ1'; // this is normalized bank code

        (accountCache.getCachedInquiryAccountInfo as jest.Mock).mockImplementation(
          (_, accountNo) => {
            if (accountNo === sourceAccount.accountNumber) {
              return sourceAccount;
            }

            if (accountNo === beneficiaryAccount.accountNumber) {
              return beneficiaryAccount;
            }
            return;
          }
        );

        // When

        const result = await externalTransactionService.createExternalTransaction(
          incomingInput
        );
        // Then
        expect(
          configurationRepository.inquiryBankAndCurrencyByIncomingCodes
        ).toBeCalledWith(
          expectedSourceBankCode,
          expectedBeneficiaryBankCode,
          'currency'
        );
        expect(depositRepository.getAvailableBalance).toBeCalledWith(
          sourceAccount.accountNumber
        );
        expect(transactionRepository.createIfNotExist).toBeCalledWith(
          expect.objectContaining({
            externalId: createIncomingTransactionExternalId(
              incomingInput.externalId,
              incomingInput.transactionDate,
              incomingInput.sourceAccountNo
            )
          })
        );
        expect(transactionRepository.update).toHaveBeenNthCalledWith(
          1,
          'any_id',
          expect.objectContaining({
            secondaryExternalId: createIncomingTransactionExternalId(
              incomingInput.externalId,
              incomingInput.transactionDate,
              incomingInput.beneficiaryAccountNo
            )
          })
        );

        expect(result.id).toBeDefined();
        expect(result.availableBalance).toEqual(999999999);
      });

      it('should submit transaction to core banking with no beneficiary account name', async () => {
        //given
        let transactionInput = createExternalTransactionInput();
        transactionInput = omit(transactionInput, 'beneficiaryAccountNo');
        const incomingInput = {
          ...transactionInput,
          beneficiaryBankCode: '542',
          sourceCIF: 'sourceCIF'
        };
        const expectedSourceBankCode = '000124'; // this is normalized bank code
        const expectedbeneficiaryBankCode = 'BC191'; // this is normalized bank code

        (configurationRepository.inquiryBankAndCurrencyByIncomingCodes as jest.Mock).mockResolvedValue(
          {
            sourceBank: {
              ...getBankCodeData(),
              bankCodeId: 'BC002',
              isInternal: true,
              channel: BankChannelEnum.INTERNAL
            },
            beneficiaryBank: {
              ...getBankCodeData(),
              channel: BankChannelEnum.EXTERNAL
            },
            currency: {
              code: 'IDR'
            }
          }
        );

        (accountCache.getCachedInquiryAccountInfo as jest.Mock).mockImplementation(
          (_, accountNo) => {
            if (accountNo === sourceAccountForWithdrawal.accountNumber) {
              return sourceAccount;
            }

            if (accountNo === beneficiaryAccount.accountNumber) {
              return beneficiaryAccount;
            }
            return;
          }
        );

        // When
        const result = await externalTransactionService.createExternalTransaction(
          incomingInput
        );

        // Then
        expect(
          configurationRepository.inquiryBankAndCurrencyByIncomingCodes
        ).toBeCalledWith(
          expectedSourceBankCode,
          expectedbeneficiaryBankCode,
          'currency'
        );
        expect(depositRepository.getAvailableBalance).toBeCalledWith(
          incomingInput.sourceAccountNo
        );
        expect(transactionRepository.createIfNotExist).toBeCalledWith(
          expect.objectContaining({
            externalId: createIncomingTransactionExternalId(
              incomingInput.externalId,
              incomingInput.transactionDate,
              incomingInput.sourceAccountNo
            )
          })
        );
        expect(transactionRepository.update).toHaveBeenCalledTimes(2);

        expect(result.id).toBeDefined();
        expect(result.availableBalance).toEqual(999999999);
      });
      it.each(['0', '1', '2', '9', '5'])(
        'should combine bankCode with RTOL is 542 and start with of accountNo is %#',
        async prefix => {
          // given
          const incomingInput = {
            ...createExternalTransactionInput(),
            sourceBankCode: '542',

            beneficiaryBankCode: 'BC11111',
            sourceAccountNo: `${prefix}23456789`,
            sourceCIF: 'sourceCIF'
          };
          const newSourceAccount = {
            ...sourceAccount,
            accountNumber: incomingInput.sourceAccountNo
          };
          let expectedSourceBankCode;
          switch (prefix) {
            case '0':
              expectedSourceBankCode = 'BC191';
              break;
            case '1':
              expectedSourceBankCode = 'BC002';
              break;
            case '2':
              expectedSourceBankCode = 'BC002';
              break;
            case '9':
              expectedSourceBankCode = 'BC004';
              break;
            case '5':
              expectedSourceBankCode = 'BC777';
              break;
            default:
              break;
          }
          (accountCache.getCachedInquiryAccountInfo as jest.Mock).mockImplementation(
            (_, accountNo) => {
              if (accountNo === newSourceAccount.accountNumber) {
                return newSourceAccount;
              }

              if (accountNo === beneficiaryAccount.accountNumber) {
                return beneficiaryAccount;
              }
              return;
            }
          );

          // When
          await externalTransactionService.createExternalTransaction(
            incomingInput
          );
          // Then
          expect(
            configurationRepository.inquiryBankAndCurrencyByIncomingCodes
          ).toBeCalledWith(
            expectedSourceBankCode,
            incomingInput.beneficiaryBankCode,
            'currency'
          );
          expect(
            transactionRepository.getAllByExternalId
          ).toHaveBeenCalledTimes(1);
          expect(
            transactionRepository.getIncomingNonRefunded
          ).toHaveBeenCalledTimes(0);
        }
      );

      it('should save processingInfo and workflowId to db if exists in input', async () => {
        // given
        const incomingInput = {
          ...createExternalTransactionInput(),
          processingInfo: {
            partner: 'ATOME',
            method: ExecutionMethodEnum.ONE_BY_ONE
          },
          workflowId: 'workflowIdValue',
          sourceBankCode: '000124',
          beneficiaryBankCode: 'MASBIDJ1',
          sourceCIF: 'sourceCIF'
        };

        (accountCache.getCachedInquiryAccountInfo as jest.Mock).mockImplementation(
          (_, accountNo) => {
            if (accountNo === sourceAccount.accountNumber) {
              return sourceAccount;
            }

            if (accountNo === beneficiaryAccount.accountNumber) {
              return beneficiaryAccount;
            }
            return;
          }
        );

        // When

        const result = await externalTransactionService.createExternalTransaction(
          incomingInput
        );
        // Then
        expect(transactionRepository.createIfNotExist).toBeCalledWith(
          expect.objectContaining({
            processingInfo: {
              partner: 'ATOME',
              method: ExecutionMethodEnum.ONE_BY_ONE
            },
            workflowId: 'workflowIdValue'
          })
        );

        expect(result.id).toBeDefined();
      });

      it('should save processingInfo and correlationId to db if exists in input', async () => {
        // given
        const incomingInput = {
          ...createExternalTransactionInput(),
          processingInfo: {
            partner: 'ATOME',
            method: ExecutionMethodEnum.ONE_BY_ONE
          },
          correlationId: 'correlationIdValue',
          sourceBankCode: '000124',
          beneficiaryBankCode: 'MASBIDJ1',
          sourceCIF: 'sourceCIF'
        };

        (accountCache.getCachedInquiryAccountInfo as jest.Mock).mockImplementation(
          (_, accountNo) => {
            if (accountNo === sourceAccount.accountNumber) {
              return sourceAccount;
            }

            if (accountNo === beneficiaryAccount.accountNumber) {
              return beneficiaryAccount;
            }
            return;
          }
        );

        // When

        const result = await externalTransactionService.createExternalTransaction(
          incomingInput
        );
        // Then
        expect(transactionRepository.createIfNotExist).toBeCalledWith(
          expect.objectContaining({
            processingInfo: {
              partner: 'ATOME',
              method: ExecutionMethodEnum.ONE_BY_ONE
            },
            correlationId: 'correlationIdValue'
          })
        );

        expect(result.id).toBeDefined();
      });
    });

    describe('submit transactions with idempotency key', () => {
      const idempotencyKey = 'c754ec35-3f76-4c10-a5ca-8fcdc6479aae';
      const incomingInput = {
        ...createExternalTransactionInput(),
        sourceBankCode: '000124',
        beneficiaryBankCode: 'MASBIDJ1',
        sourceCIF: 'sourceCIF'
      };

      afterEach(() => {
        jest.resetAllMocks();
      });

      it('should return previous transaction result with existing idempotency key and the same externalId if previous transaction status SUCCEED', async () => {
        // given
        const transactionModel = {
          ...getTransactionModelFullData(),
          externalId: createIncomingTransactionExternalId(
            incomingInput.externalId,
            incomingInput.transactionDate,
            incomingInput.sourceAccountNo
          ),
          status: TransactionStatus.SUCCEED
        };

        (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
          {
            id: 1,
            ...transactionModel
          }
        );

        // when
        const result = await externalTransactionService.createExternalTransaction(
          incomingInput,
          idempotencyKey
        );

        // then
        expect(transactionRepository.findOneByQuery).toHaveBeenNthCalledWith(
          1,
          { idempotencyKey: idempotencyKey }
        );
        expect(transactionRepository.createIfNotExist).not.toHaveBeenCalled();
        expect(result.id).toBeDefined();
        expect(result.availableBalance).toEqual(999999999);
      });

      it('should throw an error taken from previous transaction with existing idempotency key and the same externalId if previous transaction status DECLINED', async () => {
        // given
        const expectedFailureReason: IFailureReasonModel = {
          detail: 'Insufficient account balance.',
          actor: TransferFailureReasonActor.SUBMITTER,
          type: ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
        };
        const transactionModel = {
          ...getTransactionModelFullData(),
          externalId: createIncomingTransactionExternalId(
            incomingInput.externalId,
            incomingInput.transactionDate,
            incomingInput.sourceAccountNo
          ),
          status: TransactionStatus.DECLINED,
          failureReason: expectedFailureReason
        };

        (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
          {
            id: 1,
            ...transactionModel
          }
        );

        // when
        const error = await externalTransactionService
          .createExternalTransaction(incomingInput, idempotencyKey)
          .catch(e => e);

        // then
        expect(error).toBeInstanceOf(TransferAppError);
        expect(error.errorCode).toEqual(expectedFailureReason.type);
        expect(error.actor).toEqual(expectedFailureReason.actor);
        expect(error.detail).toEqual(expectedFailureReason.detail);

        expect(transactionRepository.findOneByQuery).toHaveBeenNthCalledWith(
          1,
          { idempotencyKey: idempotencyKey }
        );
        expect(transactionRepository.createIfNotExist).not.toHaveBeenCalled();
      });

      it('should successfully submit as a new transaction with existing idempotency key and the different externalId', async () => {
        // given
        const transactionModel = {
          ...getTransactionModelFullData(),
          externalId: 'different_externalId'
        };

        (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
          {
            id: 1,
            ...transactionModel
          }
        );

        // when
        const result = await externalTransactionService.createExternalTransaction(
          incomingInput,
          idempotencyKey
        );

        // then
        expect(transactionRepository.findOneByQuery).toHaveBeenNthCalledWith(
          1,
          { idempotencyKey: idempotencyKey }
        );
        expect(transactionRepository.createIfNotExist).toHaveBeenCalled();
        expect(result.id).toBeDefined();
        expect(result.availableBalance).toEqual(999999999);
      });

      it('should successfully submit as a new transaction with undefined idempotency key', async () => {
        // given
        const undefinedIdempotencyKey = undefined;
        const transactionModel = getTransactionModelFullData();

        (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValueOnce(
          {
            id: 1,
            ...transactionModel
          }
        );

        // when
        const result = await externalTransactionService.createExternalTransaction(
          incomingInput,
          undefinedIdempotencyKey
        );

        // then
        expect(transactionRepository.findOneByQuery).not.toHaveBeenCalled();
        expect(transactionRepository.createIfNotExist).toHaveBeenCalled();
        expect(result.id).toBeDefined();
        expect(result.availableBalance).toEqual(999999999);
      });
    });

    describe('refunded transaction', () => {
      it('should exectue an euronet refunded transaction if all refund validations passed', async () => {
        // given
        const incomingInput = {
          ...createExternalTransactionInput(),
          sourceBankCode: '000124',
          beneficiaryBankCode: 'MASBIDJ1',
          paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT,
          notes: 'stringstring'
        };
        let newTransactionModel = getTransactionModelFullData();

        const refundedTransaction = {
          ...newTransactionModel,
          paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT,
          transactionAmount: 50000,
          id: 'test12345'
        };
        const originalTransaction = {
          ...newTransactionModel,
          paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT,
          transactionAmount: 100000,
          id: 'test1234'
        };

        (transactionRepository.getIncomingNonRefunded as jest.Mock).mockReturnValueOnce(
          originalTransaction
        );
        (refundValidator.validateRefundRequest as jest.Mock).mockReturnValueOnce(
          null
        );
        (transactionRepository.createIfNotExist as jest.Mock).mockResolvedValueOnce(
          refundedTransaction
        );
        (transactionRepository.update as jest.Mock)
          .mockResolvedValueOnce(refundedTransaction)
          .mockResolvedValueOnce(refundedTransaction)
          .mockResolvedValueOnce({
            ...originalTransaction,
            refund: {
              remainingAmount: 50000,
              transactions: [refundedTransaction.id]
            }
          })
          .mockResolvedValueOnce({
            ...refundedTransaction,
            refund: {
              originalTransactionId: originalTransaction.id,
              revertPaymentServiceCode: originalTransaction.paymentServiceCode
            }
          });
        (accountCache.getCachedInquiryAccountInfo as jest.Mock).mockImplementation(
          (_, accountNo) => {
            if (accountNo === sourceAccount.accountNumber) {
              return sourceAccount;
            }

            if (accountNo === beneficiaryAccount.accountNumber) {
              return beneficiaryAccount;
            }
            return;
          }
        );

        // When

        await externalTransactionService.createExternalTransaction(
          incomingInput
        );
        // Then
        expect(transactionRepository.createIfNotExist).toBeCalledWith(
          expect.objectContaining({
            externalId: createIncomingTransactionExternalId(
              incomingInput.externalId,
              incomingInput.transactionDate,
              incomingInput.sourceAccountNo
            )
          })
        );
        expect(
          transactionRepository.getIncomingNonRefunded
        ).toHaveBeenCalledWith(
          incomingInput.notes,
          incomingInput.beneficiaryAccountNo
        );
        expect(transactionRepository.update).toHaveBeenCalledTimes(4);
        expect(transactionRepository.update).toHaveBeenNthCalledWith(
          1,
          'test12345',
          expect.objectContaining({
            secondaryExternalId: createIncomingTransactionExternalId(
              incomingInput.externalId,
              incomingInput.transactionDate,
              incomingInput.beneficiaryAccountNo
            )
          })
        );
        expect(transactionRepository.update).toHaveBeenNthCalledWith(
          3,
          'test1234',
          {
            ...originalTransaction,
            refund: {
              remainingAmount: 50000,
              transactions: ['test12345']
            }
          }
        );
        expect(transactionRepository.update).toHaveBeenNthCalledWith(
          4,
          'test12345',
          {
            ...refundedTransaction,
            refund: {
              originalTransactionId: originalTransaction.id,
              revertPaymentServiceCode: originalTransaction.paymentServiceCode
            }
          }
        );
        expect(
          monitoringEventProducer.sendMonitoringEventMessage
        ).toHaveBeenCalled();
      });

      it('should exectue an jagopay refunded transaction if all refund validations passed', async () => {
        // given
        const incomingInput = {
          ...createExternalTransactionInput(),
          sourceBankCode: '000124',
          beneficiaryBankCode: 'MASBIDJ1',
          paymentServiceType: PaymentServiceTypeEnum.JAGOPAY_REFUND,
          notes: 'stringstring'
        };
        let newTransactionModel = getTransactionModelFullData();

        const refundedTransaction = {
          ...newTransactionModel,
          paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT,
          transactionAmount: 50000,
          id: 'test12345'
        };
        const originalTransaction = {
          ...newTransactionModel,
          paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT,
          transactionAmount: 100000,
          id: 'test1234'
        };

        (transactionRepository.getByExternalId as jest.Mock).mockReturnValueOnce(
          originalTransaction
        );
        (refundValidator.validateRefundRequest as jest.Mock).mockReturnValueOnce(
          null
        );
        (transactionRepository.createIfNotExist as jest.Mock).mockResolvedValueOnce(
          refundedTransaction
        );
        (transactionRepository.update as jest.Mock)
          .mockResolvedValueOnce(refundedTransaction)
          .mockResolvedValueOnce(refundedTransaction)
          .mockResolvedValueOnce({
            ...originalTransaction,
            refund: {
              remainingAmount: 50000,
              transactions: [refundedTransaction.id]
            }
          })
          .mockResolvedValueOnce({
            ...refundedTransaction,
            refund: {
              originalTransactionId: originalTransaction.id,
              revertPaymentServiceCode: originalTransaction.paymentServiceCode
            }
          });
        (accountCache.getCachedInquiryAccountInfo as jest.Mock).mockImplementation(
          (_, accountNo) => {
            if (accountNo === sourceAccount.accountNumber) {
              return sourceAccount;
            }

            if (accountNo === beneficiaryAccount.accountNumber) {
              return beneficiaryAccount;
            }
            return;
          }
        );

        // When

        await externalTransactionService.createExternalTransaction(
          incomingInput
        );
        // Then
        expect(transactionRepository.createIfNotExist).toBeCalledWith(
          expect.objectContaining({
            externalId: createIncomingTransactionExternalId(
              incomingInput.externalId,
              incomingInput.transactionDate,
              incomingInput.sourceAccountNo
            )
          })
        );
        expect(transactionRepository.getByExternalId).toHaveBeenCalledWith(
          incomingInput.notes
        );
        expect(transactionRepository.update).toHaveBeenCalledTimes(4);
        expect(transactionRepository.update).toHaveBeenNthCalledWith(
          1,
          'test12345',
          expect.objectContaining({
            secondaryExternalId: createIncomingTransactionExternalId(
              incomingInput.externalId,
              incomingInput.transactionDate,
              incomingInput.beneficiaryAccountNo
            )
          })
        );
        expect(transactionRepository.update).toHaveBeenNthCalledWith(
          3,
          'test1234',
          {
            ...originalTransaction,
            refund: {
              remainingAmount: 50000,
              transactions: ['test12345']
            }
          }
        );
        expect(transactionRepository.update).toHaveBeenNthCalledWith(
          4,
          'test12345',
          {
            ...refundedTransaction,
            refund: {
              originalTransactionId: originalTransaction.id,
              revertPaymentServiceCode: originalTransaction.paymentServiceCode
            }
          }
        );
        expect(
          monitoringEventProducer.sendMonitoringEventMessage
        ).toHaveBeenCalled();
      });
    });
  });

  describe('getExternalTransactionFee', () => {
    beforeEach(() => {
      (configurationRepository.inquiryBankAndCurrencyByIncomingCodes as jest.Mock).mockResolvedValue(
        {
          sourceBank: {
            ...getBankCodeData(),
            bankCodeId: Constant.GIN_BANK_CODE_ID
          },
          beneficiaryBank: getBankCodeData(),
          currency: {
            code: 'IDR'
          }
        }
      );
    });
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should throw error if source balance cannot pay fee', async () => {
      // Given
      const transactionInput: ITransferTransactionInput = getITransferTransactionInput();
      const feeData = [{ feeAmount: 1 }];

      (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
        { ...getRecommendedService(), feeData }
      ]);

      (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValueOnce(
        transactionInput.transactionAmount
      );

      // When
      const incomingInput = {
        ...createExternalTransactionInput(),
        sourceAccountInfo: { accountNumber: '123' },
        sourceCIF: 'CIF-1',
        sourceBankCode: JAGO_BANK_CODE
      };
      const error = await externalTransactionService
        .getExternalTransactionFee(incomingInput)
        .catch(e => e);

      // Then
      expect(error.errorCode).toEqual(
        ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
      );
    });

    //
    // Not clear on the test intention. It is not clear why is the recommendationService
    // expected to be empty and generate an error...
    //
    // it('should throw error when recommendationServiceCode is not found', async () => {
    //   // Given
    //   (transactionHelper.mapFeeData as jest.Mock).mockResolvedValueOnce([
    //     getRecommendedService()
    //   ]);
    //   // When
    //   const incomingInput = {
    //     ...createExternalTransactionInput(),
    //     sourceAccountInfo: { accountNumber: '123' },
    //     sourceCIF: 'CIF-1',
    //     sourceBankCode: JAGO_BANK_CODE
    //   };

    //   const error = await externalTransactionService
    //     .getExternalTransactionFee(incomingInput)
    //     .catch(e => e);

    //   // Then
    //   expect(error).toBeInstanceOf(TransferAppError);
    //   expect(error.errorCode).toEqual(ERROR_CODE.BALANCE_IS_INSUFFICIENT);
    // });

    describe('should get fee amount and checking for balance in core banking', () => {
      const paymentServiceCode = 'p01';

      const feeData = [
        {
          feeAmount: 100
        }
      ];

      afterEach(() => {
        jest.resetAllMocks();
      });

      beforeEach(() => {
        (accountCache.getCachedInquiryAccountInfo as jest.Mock).mockResolvedValueOnce(
          {
            cif: 'cif'
          }
        );
        (accountCache.getCachedInquiryAccountInfo as jest.Mock).mockResolvedValueOnce(
          {
            accountNumber: '123456789'
          }
        );
        (depositRepository.getAvailableBalance as jest.Mock).mockResolvedValueOnce(
          999999999
        );

        (transactionHelper.mapFeeData as jest.Mock).mockResolvedValue([
          { ...getRecommendedService(), paymentServiceCode, feeData }
        ]);
      });

      it('should get fee amount and checking for balance in core banking', async () => {
        // given
        const incomingInput = {
          ...createExternalTransactionInput(),
          sourceBankCode: '000124',
          beneficiaryBankCode: '542',
          sourceCIF: 'sourceCIF'
        };

        const expectedSourceBankCode = '000124'; // this is normalized bank code
        const expectedbeneficiaryBankCode = 'BC002'; // this is normalized bank code

        // When
        const result = await externalTransactionService.getExternalTransactionFee(
          incomingInput
        );
        // Then
        expect(
          configurationRepository.inquiryBankAndCurrencyByIncomingCodes
        ).toBeCalledWith(
          expectedSourceBankCode,
          expectedbeneficiaryBankCode,
          'currency'
        );

        expect(depositRepository.getAvailableBalance).toBeCalledWith(
          sourceAccount.accountNumber
        );
        expect(result.feeAmount).toEqual(feeData[0].feeAmount);
      });

      it('should return 0 fee if the feeData is empty', async () => {
        // given
        (transactionHelper.mapFeeData as jest.Mock).mockResolvedValue([
          { ...getRecommendedService(), paymentServiceCode, feeData: [] }
        ]);

        const incomingInput = {
          ...createExternalTransactionInput(),
          sourceBankCode: '000124',
          beneficiaryBankCode: '542',
          sourceCIF: 'sourceCIF'
        };

        // when
        const result = await externalTransactionService.getExternalTransactionFee(
          incomingInput
        );

        // then
        expect(result.feeAmount).toEqual(0);
      });
    });
  });
});
