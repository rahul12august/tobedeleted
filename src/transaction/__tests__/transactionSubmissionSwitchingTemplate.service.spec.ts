import switchingSubmissionTemplate from '../transactionSubmissionSwitchingTemplate.service';
import { BankChannelEnum, Rail, Util } from '@dk/module-common';
import { getTransactionDetail } from '../__mocks__/transaction.data';
import { mapToSwitchingTransaction } from '../transactionSwitching.helper';
import { ISwitchingTransaction } from '../../switching/switching.type';
import {
  getMockedSwitchingResponse,
  getMockedTransaction
} from '../../switching/__mocks__/switchingTransaction.data';
import switchingRepository from '../../switching/switching.repository';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import transactionRepository from '../transaction.repository';
import { TransferFailureReasonActor } from '../transaction.enum';

jest.mock('../transactionSwitching.helper');
jest.mock('../../switching/switching.repository');
jest.mock('../transaction.repository');

describe('switchingSubmissionTemplate', () => {
  class NetworkError extends Error {
    status: number;
    code?: string;
    constructor(status = 501, code?: string) {
      super();
      this.status = status;
      this.code = code;
    }
  }
  let mockedRetry = jest
    .spyOn(Util, 'retry')
    .mockImplementation(async (_, fn, ...args) => await fn(...args));

  // given
  const transactionModel = {
    ...getTransactionDetail(),
    beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
    requireThirdPartyOutgoingId: true,
    debitTransactionCode: 'TFD23',
    thirdPartyOutgoingId: '20085T000001'
  };

  describe('isEligible check', () => {
    it(`should return true when beneficiaryBankCodeChannel is ${BankChannelEnum.EXTERNAL}`, () => {
      // when
      const eligible = switchingSubmissionTemplate.isEligible(transactionModel);

      // then
      expect(eligible).toBeTruthy();
    });

    it(`should return false when beneficiaryBankCodeChannel is not ${BankChannelEnum.EXTERNAL}`, () => {
      // when
      const eligible = switchingSubmissionTemplate.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL
      });

      // then
      expect(eligible).not.toBeTruthy();
    });

    it('should return false when beneficiaryAccountNo is undefined', async () => {
      // when
      const eligible = switchingSubmissionTemplate.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        beneficiaryAccountNo: undefined
      });

      // then
      expect(eligible).not.toBeTruthy();
    });

    it('should return false when using BRANCH_SKN as the paymentServiceCode', () => {
      // when
      const eligible = switchingSubmissionTemplate.isEligible({
        ...transactionModel,
        paymentServiceCode: 'BRANCH_SKN'
      });

      // then
      expect(eligible).not.toBeFalsy();
    });

    it('should return false when using BRANCH_RTGS as the paymentServiceCode', () => {
      // when
      const eligible = switchingSubmissionTemplate.isEligible({
        ...transactionModel,
        paymentServiceCode: 'BRANCH_RTGS'
      });

      // then
      expect(eligible).not.toBeFalsy();
    });
  });

  describe('submitTransaction', () => {
    const mockedSwitchingModel: ISwitchingTransaction = getMockedTransaction();
    const mockedSwitchingResponse = getMockedSwitchingResponse();
    beforeEach(() => {
      (mapToSwitchingTransaction as jest.Mock).mockResolvedValue(
        mockedSwitchingModel
      );
      (switchingRepository.submitTransaction as jest.Mock).mockResolvedValue(
        mockedSwitchingResponse
      );
      (transactionRepository.findOneByQuery as jest.Mock).mockResolvedValue(
        transactionModel
      );
      (transactionRepository.update as jest.Mock).mockResolvedValue(
        transactionModel
      );
    });
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should submit transaction to switching with correct payload', async () => {
      const result = await switchingSubmissionTemplate.submitTransaction(
        transactionModel
      );
      expect(result).toBeDefined();
      expect(switchingRepository.submitTransaction).toHaveBeenCalledWith(
        mockedSwitchingModel
      );
    });
    it('should retry first time when switching repository throw network error', async () => {
      const networkError = new NetworkError();
      const expectedNetworkError = {
        ...networkError,
        code: 'ECONNABORTED'
      };
      mockedRetry.mockRestore();

      (switchingRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        expectedNetworkError
      );

      const result = await switchingSubmissionTemplate.submitTransaction(
        transactionModel
      );
      expect(result).toBeDefined();
      expect(switchingRepository.submitTransaction).toHaveBeenCalledTimes(1);
    });

    it('should throw error when switching returned bad request error', async () => {
      (switchingRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.ERROR_FROM_SWICHING
        )
      );
      const error = await switchingSubmissionTemplate

        .submitTransaction(transactionModel)
        .catch(err => err);

      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.detail).toEqual('Test error!');
      expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      expect(error.errorCode).toEqual(ERROR_CODE.ERROR_FROM_SWICHING);
      expect(switchingRepository.submitTransaction).toHaveBeenCalledTimes(1);
    });
  });

  describe('Rail check', () => {
    it(`should have rail property defined`, () => {
      // when
      const rail = switchingSubmissionTemplate.getRail?.();

      // then
      expect(rail).toEqual(Rail.EURONET);
    });
  });
});
