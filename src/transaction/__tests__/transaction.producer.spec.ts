import {
  NotificationTopicConstant,
  INotificationMessage,
  TransactionTopicConstant,
  ITransactionFailedMessage,
  PaymentInstructionTopicConstant,
  ITransactionSucceededMessage
} from '@dk/module-message';
import producer from '../../common/kafkaProducer';
import transactionProducer, {
  reversalRetryTopic,
  reversalRetryFailedTopic
} from '../transaction.producer';
import { ITransactionRetryReversalMessage } from '../transaction.producer.type';
import { NotificationCode } from '../transaction.producer.enum';
import {
  getITransactionInput,
  getTransactionModelFullData
} from '../__mocks__/transaction.data';
import {
  PaymentRequestStatus,
  TransferFailureReasonActor
} from '../transaction.enum';
import {
  TransactionExecutionTypeEnum,
  Constant,
  Formater,
  BankChannelEnum,
  PaymentServiceTypeEnum
} from '@dk/module-common';
import { getBillDetails } from '../../billPayment/billDetail/__mocks__/billDetail.data';
import { ERROR_CODE } from '../../common/errors';
import uuidv4 from 'uuid/v4';
import { getCustomerResponse } from '../../customer/__mocks__/customer.data';
import customerRepository from '../../customer/customer.repository';
import { toIndonesianRupiah } from '../../common/currencyFormatter';
import logger from '../../logger';
import { ITransactionModel } from '../transaction.model';
import faker from 'faker';

jest.mock('../../common/kafkaProducer', () => ({
  sendSerializedValue: jest.fn()
}));

jest.mock('../../customer/customer.repository.ts');
jest.mock('uuid/v4');
describe('transaction.producer', () => {
  const uuidMockValue = 'bb39f11d-f84c-40c2-b958-2fd103b544a8';
  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should produce failed transfer notification message', async () => {
    // Given
    const updatedAt = new Date();
    const transactionModel = {
      ...getTransactionModelFullData(),
      id: 'transactionId',
      beneficiaryBankCodeChannel: BankChannelEnum.IRIS,
      updatedAt
    };

    (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);

    const notificationMessage: INotificationMessage = {
      notificationCode: NotificationCode.NOTIFICATION_FAILED_TRANSFER,
      cif: transactionModel.sourceCIF || '',
      payload: {
        amount: Formater.formatMoney(transactionModel.transactionAmount, {
          currency: transactionModel.sourceTransactionCurrency || ''
        }),
        beneficiaryName: transactionModel.beneficiaryAccountName || '',
        accountNumber: transactionModel.beneficiaryAccountNo || ''
      }
    };

    await transactionProducer.sendFailedTransferNotification(transactionModel);

    expect(producer.sendSerializedValue).toBeCalledWith({
      topic: NotificationTopicConstant.NOTIFICATION_CREATED,
      messages: [
        {
          value: notificationMessage,
          headers: { 'x-idempotency-key': uuidMockValue }
        }
      ]
    });
  });

  it('should produce failed transfer notification message - QRIS Failed', async () => {
    // Given
    const updatedAt = new Date();
    const transactionModel = {
      ...getTransactionModelFullData(),
      id: 'transactionId',
      beneficiaryBankCodeChannel: BankChannelEnum.IRIS,
      updatedAt
    };
    transactionModel.paymentServiceType = PaymentServiceTypeEnum.QRIS;

    (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);
    const customerData = getCustomerResponse();
    (customerRepository.getCustomerInfoById as jest.Mock).mockResolvedValue(
      customerData
    );

    const notificationMessage: INotificationMessage = {
      notificationCode: NotificationCode.NOTIF_FAILED_QRIS,
      cif: transactionModel.sourceCIF || '',
      payload: {
        amount: Formater.formatMoney(transactionModel.transactionAmount, {
          currency: transactionModel.sourceTransactionCurrency || ''
        }),
        beneficiaryName: transactionModel.beneficiaryAccountName || '',
        accountNumber: transactionModel.beneficiaryAccountNo || '',
        customerName: customerData.fullName
      }
    };

    await transactionProducer.sendFailedTransferNotification(transactionModel);

    expect(producer.sendSerializedValue).toBeCalledWith({
      topic: NotificationTopicConstant.NOTIFICATION_CREATED,
      messages: [
        {
          value: notificationMessage,
          headers: { 'x-idempotency-key': uuidMockValue }
        }
      ]
    });
  });

  it('should not produce failed transfer notification message', async () => {
    // Given
    const updatedAt = new Date();
    const transactionModel = {
      ...getTransactionModelFullData(),
      beneficiaryBankCodeChannel: BankChannelEnum.TOKOPEDIA,
      id: 'transactionId',
      updatedAt
    };

    await transactionProducer.sendFailedTransferNotification(transactionModel);

    expect(producer.sendSerializedValue).not.toBeCalled();
  });

  it('can send Transaction Succeeded Message', async () => {
    // given
    const updatedAt = new Date();
    const transactionModel = {
      ...getTransactionModelFullData(),
      id: 'transactionId',
      executorCIF: 'executorCIF',
      updatedAt
    };
    (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);
    const succeedMessage: ITransactionSucceededMessage = {
      customerId: 'customerId',
      transactionId: 'transactionId',
      sourceCIF: transactionModel.sourceCIF,
      beneficiaryCIF: transactionModel.beneficiaryCIF,
      referenceId: transactionModel.referenceId,
      externalId: transactionModel.externalId,
      status: transactionModel.status,
      updatedAt: updatedAt.toISOString(),
      transactionAmount: transactionModel.transactionAmount,
      sourceAccountNo: transactionModel.sourceAccountNo,
      sourceAccountType: transactionModel.sourceAccountType,
      categoryCode: transactionModel.categoryCode,
      beneficiaryAccountNo: transactionModel.beneficiaryAccountNo,
      beneficiaryAccountType: transactionModel.beneficiaryAccountType,
      paymentRequestID: transactionModel.paymentRequestID,
      additionalInformation1: transactionModel.additionalInformation1,
      additionalInformation2: transactionModel.additionalInformation2,
      beneficiaryName: transactionModel.beneficiaryAccountName,
      executorCIF: 'executorCIF',
      paymentServiceCode: transactionModel.paymentServiceCode,
      notes: transactionModel.note
    };

    // when
    await transactionProducer.sendTransactionSucceededMessage(
      'customerId',
      transactionModel
    );

    // then
    expect(producer.sendSerializedValue).toBeCalledWith({
      topic: TransactionTopicConstant.TRANSFER_TRANSACTION_SUCCEEDED,
      messages: [
        {
          value: succeedMessage,
          headers: { 'x-idempotency-key': uuidMockValue }
        }
      ]
    });
  });

  it('can send Transaction Failed Message', async () => {
    // given
    const updatedAt = new Date();
    const failureReason = {
      detail: 'detail',
      actor: TransferFailureReasonActor.UNKNOWN,
      type: ERROR_CODE.UNEXPECTED_ERROR
    };
    const transactionModel = {
      ...getTransactionModelFullData(),
      id: 'transactionId',
      beneficiaryBankCodeChannel: BankChannelEnum.IRIS,
      updatedAt
    };
    transactionModel.failureReason = failureReason;
    (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);

    const failedMessage: ITransactionFailedMessage = {
      customerId: 'customerId',
      transactionId: 'transactionId',
      sourceCIF: transactionModel.sourceCIF,
      beneficiaryCIF: transactionModel.beneficiaryCIF,
      referenceId: transactionModel.referenceId,
      externalId: transactionModel.externalId,
      status: transactionModel.status,
      updatedAt: updatedAt.toISOString(),
      transactionAmount: transactionModel.transactionAmount,
      sourceAccountNo: transactionModel.sourceAccountNo,
      sourceAccountType: transactionModel.sourceAccountType,
      categoryCode: transactionModel.categoryCode,
      beneficiaryAccountNo: transactionModel.beneficiaryAccountNo,
      beneficiaryAccountType: transactionModel.beneficiaryAccountType,
      paymentRequestID: transactionModel.paymentRequestID,
      additionalInformation1: transactionModel.additionalInformation1,
      additionalInformation2: transactionModel.additionalInformation2,
      beneficiaryName: transactionModel.beneficiaryAccountName,
      paymentServiceCode: transactionModel.paymentServiceCode,
      failureReson: {
        actor: 'UNKNOWN',
        detail: 'detail',
        type: 'UNEXPECTED_ERROR'
      },
      notes: transactionModel.note
    };

    // when
    await transactionProducer.sendTransactionFailedMessage(
      'customerId',
      transactionModel
    );

    // then
    expect(producer.sendSerializedValue).toBeCalledWith({
      topic: TransactionTopicConstant.TRANSFER_TRANSACTION_FAILED,
      messages: [
        {
          value: failedMessage,
          headers: { 'x-idempotency-key': uuidMockValue }
        }
      ]
    });
  });

  it('should not send Transaction Failed Message', async () => {
    // given
    const failureReason = {
      detail: 'detail',
      actor: TransferFailureReasonActor.UNKNOWN,
      type: ERROR_CODE.UNEXPECTED_ERROR
    };
    const updatedAt = new Date();
    const transactionModel = {
      ...getTransactionModelFullData(),
      beneficiaryBankCodeChannel: BankChannelEnum.TOKOPEDIA,
      id: 'transactionId',
      updatedAt
    };
    transactionModel.failureReason = failureReason;
    // when
    await transactionProducer.sendTransactionFailedMessage(
      'customerId',
      transactionModel
    );

    // then
    expect(producer.sendSerializedValue).not.toBeCalled();
  });

  it('should produce retry exceeded notification message', async () => {
    // Given
    const notificationMessage: INotificationMessage = {
      notificationCode: NotificationCode.NOTIFICATION_PENDING_BILLPAYMENT,
      customerId: '123456',
      payload: {
        amount: '100.000',
        billKey: '1234567890',
        billerName: 'PLN',
        transactionId: 'TRAN01',
        transactionTime: '00:00'
      }
    };
    (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);

    await transactionProducer.sendPendingBillPaymentNotification({
      billDetails: getBillDetails(),
      transactionAmount: 100000,
      transactionId: 'TRAN01',
      transactionTime: new Date(`01 June 2020 17:00:00Z`)
    });

    expect(producer.sendSerializedValue).toBeCalledWith({
      topic: NotificationTopicConstant.NOTIFICATION_CREATED,
      messages: [
        {
          value: notificationMessage,
          headers: { 'x-idempotency-key': uuidMockValue }
        }
      ]
    });
  });

  describe('sendTransactionRetryReversalCommand', () => {
    it('should produce message successfully', async () => {
      const message: ITransactionRetryReversalMessage = {
        transactionId: '132434',
        retryCounter: 1
      };
      (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);
      await transactionProducer.sendTransactionRetryReversalCommand(message);

      expect(producer.sendSerializedValue).toBeCalledWith({
        topic: reversalRetryTopic,
        messages: [
          {
            value: message,
            headers: { 'x-idempotency-key': uuidMockValue }
          }
        ]
      });
    });
  });

  describe('sendTransactionReversalFailedEvent', () => {
    it('should produce message successfully', async () => {
      const message: ITransactionRetryReversalMessage = {
        transactionId: '132434',
        retryCounter: 1
      };
      (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);
      await transactionProducer.sendTransactionReversalFailedEvent(message);

      expect(producer.sendSerializedValue).toBeCalledWith({
        topic: reversalRetryFailedTopic,
        messages: [
          {
            value: message,
            headers: { 'x-idempotency-key': uuidMockValue }
          }
        ]
      });
    });
  });

  describe('sendTransactionMessage', () => {
    it('should produce message successfully', async () => {
      const message = {
        externalId: '132434',
        status: PaymentRequestStatus.SUCCEED
      };
      (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);
      await transactionProducer.sendTransactionMessage(
        PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE,
        message,
        TransactionExecutionTypeEnum.DIRECT_TRANSFER,
        ''
      );
      const expectedMessage = {
        headers: {
          [Constant.EXECUTION_TYPE]:
            TransactionExecutionTypeEnum.DIRECT_TRANSFER,
          [Constant.PI_KEY_HEADER]: '',
          'x-idempotency-key': uuidMockValue
        },
        value: message
      };
      expect(producer.sendSerializedValue).toBeCalledWith({
        topic: PaymentInstructionTopicConstant.DIRECT_TRANSACTION_RESPONSE,
        messages: [expectedMessage]
      });
    });
  });

  describe('sendPendingQrisTransactionNotification', () => {
    it('should produce Notif_Pending_Transfer notification message', async () => {
      // Given
      const transactionData = getTransactionModelFullData();
      const transactionModel = {
        ...transactionData,
        transactionAmount: 23456,
        id: '12345678890',
        createdAt: new Date(`01 June 2020 17:00:00Z`)
      };
      (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);

      await transactionProducer.sendQrisTransactionNotification(
        transactionModel,
        NotificationCode.NOTIF_PENDING_QRIS
      );

      const notification: INotificationMessage = {
        notificationCode: NotificationCode.NOTIF_PENDING_QRIS,
        customerId: transactionModel.sourceCustomerId,
        cif: transactionModel.sourceCIF,
        payload: {
          customerName: transactionModel.sourceAccountName,
          sourceAccountType: transactionModel.sourceAccountType,
          sourceAccountNo: transactionModel.sourceAccountNo,
          beneficiaryBankName: transactionModel.beneficiaryBankName,
          beneficiaryAccountNo: transactionModel.beneficiaryAccountNo,
          transactionDate: '02 June 2020, 00:00 WIB',
          amount: '23.456',
          beneficiaryName: transactionModel.beneficiaryAccountName
        }
      };

      expect(producer.sendSerializedValue).toBeCalledWith({
        topic: NotificationTopicConstant.NOTIFICATION_CREATED,
        messages: [
          {
            value: notification,
            headers: { 'x-idempotency-key': uuidMockValue }
          }
        ]
      });
    });

    it('should produce retry exceeded notification message', async () => {
      // Given
      const transactionModel: ITransactionModel = getTransactionModelFullData();
      delete transactionModel.sourceCustomerId;
      delete transactionModel.sourceCIF;
      const error = await transactionProducer
        .sendQrisTransactionNotification(
          transactionModel,
          NotificationCode.NOTIF_PENDING_QRIS
        )
        .catch(error => error);

      expect(producer.sendSerializedValue).not.toHaveBeenCalled();
      expect(error).toBeInstanceOf(Error);
      expect(error.message).toEqual(ERROR_CODE.CUSTOMER_NOT_FOUND);
    });
  });

  describe('sendPendingBlockedAmountNotification', () => {
    it('should produce Notif_Pending_Transfer notification message', async () => {
      // Given
      const transactionData = getTransactionModelFullData();
      const transactionModel = {
        ...transactionData,
        transactionAmount: 23456,
        id: '12345678890',
        createdAt: new Date(`01 June 2020 17:00:00Z`)
      };
      (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);

      await transactionProducer.sendPendingBlockedAmountNotification(
        transactionModel,
        NotificationCode.NOTIFICATION_PENDING_TRANSFER
      );

      const notification: INotificationMessage = {
        notificationCode: NotificationCode.NOTIFICATION_PENDING_TRANSFER,
        customerId: transactionModel.sourceCustomerId,
        cif: transactionModel.sourceCIF,
        payload: {
          amount: '23.456',
          beneficiaryName: transactionModel.beneficiaryAccountName,
          transactionId: transactionModel.id,
          transactionTime: '00:00'
        }
      };

      expect(producer.sendSerializedValue).toBeCalledWith({
        topic: NotificationTopicConstant.NOTIFICATION_CREATED,
        messages: [
          {
            value: notification,
            headers: { 'x-idempotency-key': uuidMockValue }
          }
        ]
      });
    });

    it('should produce Notif_Pending_Transfer notification message with date', async () => {
      // Given
      (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);
      const transactionData = getTransactionModelFullData();
      const transactionModel = {
        ...transactionData,
        transactionAmount: 23456,
        id: '12345678890'
      };

      await transactionProducer.sendPendingBlockedAmountNotification(
        transactionModel,
        NotificationCode.NOTIFICATION_PENDING_TRANSFER
      );

      const notification: INotificationMessage = {
        notificationCode: NotificationCode.NOTIFICATION_PENDING_TRANSFER,
        customerId: transactionModel.sourceCustomerId,
        cif: transactionModel.sourceCIF,
        payload: expect.objectContaining({
          amount: '23.456',
          beneficiaryName: transactionModel.beneficiaryAccountName,
          transactionId: transactionModel.id
        })
      };

      expect(producer.sendSerializedValue).toBeCalledWith({
        topic: NotificationTopicConstant.NOTIFICATION_CREATED,
        messages: [
          {
            value: notification,
            headers: { 'x-idempotency-key': uuidMockValue }
          }
        ]
      });
    });

    it('should produce NOTIF_PENIDNG_SKNRTGS notification message when sourceCustomerId missing', async () => {
      // Given
      const transactionModel: ITransactionModel = {
        ...getTransactionModelFullData(),
        transactionAmount: 4194,
        createdAt: new Date(`01 June 2020 15:30:00Z`)
      };
      (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);
      delete transactionModel.sourceCustomerId;
      await transactionProducer.sendPendingBlockedAmountNotification(
        transactionModel,
        NotificationCode.NOTIF_PENIDNG_SKNRTGS
      );

      const notification: INotificationMessage = {
        notificationCode: NotificationCode.NOTIF_PENIDNG_SKNRTGS,
        customerId: transactionModel.sourceCustomerId,
        cif: transactionModel.sourceCIF,
        payload: {
          transactionId: transactionModel.id,
          amount: '4.194',
          beneficiaryName: transactionModel.beneficiaryAccountName,
          transactionTime: '22:30'
        }
      };

      expect(producer.sendSerializedValue).toBeCalledWith({
        topic: NotificationTopicConstant.NOTIFICATION_CREATED,
        messages: [
          {
            value: notification,
            headers: { 'x-idempotency-key': uuidMockValue }
          }
        ]
      });
    });

    it('should produce retry exceeded notification message', async () => {
      // Given
      const transactionModel: ITransactionModel = getTransactionModelFullData();
      delete transactionModel.sourceCustomerId;
      delete transactionModel.sourceCIF;
      const error = await transactionProducer
        .sendPendingBlockedAmountNotification(
          transactionModel,
          NotificationCode.NOTIFICATION_PENDING_TRANSFER
        )
        .catch(error => error);

      expect(producer.sendSerializedValue).not.toHaveBeenCalled();
      expect(error).toBeInstanceOf(Error);
      expect(error.message).toEqual(ERROR_CODE.CUSTOMER_NOT_FOUND);
    });
  });

  describe('sendFailedDebitCardTransactionAmount', () => {
    it('should produce Notif_Debit_Card_Insufficient_Balance notification message', async () => {
      // Given
      const iTransactionInput = getITransactionInput();
      const transactionIput = {
        ...iTransactionInput,
        transactionAmount: 23456,
        sourceAccountName: 'any',
        beneficiaryAccountName: 'any'
      };
      (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);

      // When
      await transactionProducer.sendFailedDebitCardTransactionAmount(
        transactionIput,
        NotificationCode.NOTIF_DEBIT_CARD_INSUFFICIENT_BALANCE
      );

      // Then
      const notification: INotificationMessage = {
        notificationCode:
          NotificationCode.NOTIF_DEBIT_CARD_INSUFFICIENT_BALANCE,
        customerId: transactionIput.sourceCustomerId,
        cif: transactionIput.sourceCIF,
        payload: {
          amount: toIndonesianRupiah(transactionIput.transactionAmount),
          customerName: transactionIput.sourceAccountName,
          merchantName: transactionIput.beneficiaryAccountName
        }
      };

      expect(producer.sendSerializedValue).toBeCalledWith({
        topic: NotificationTopicConstant.NOTIFICATION_CREATED,
        messages: [
          {
            value: notification,
            headers: { 'x-idempotency-key': uuidMockValue }
          }
        ]
      });
    });

    it('should show logger error when produce Notif_Debit_Card_Insufficient_Balance notification message when sourceCustomerId missing', async () => {
      // Given
      const iTransactionInput = getITransactionInput();
      const transactionIput = {
        ...iTransactionInput,
        transactionAmount: 23456,
        sourceAccountName: 'any',
        beneficiaryAccountName: 'any',
        sourceCustomerId: undefined,
        sourceCIF: undefined
      };
      (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);

      const spyOn = jest.spyOn(logger, 'error');

      // When
      await transactionProducer
        .sendFailedDebitCardTransactionAmount(
          transactionIput,
          NotificationCode.NOTIF_DEBIT_CARD_INSUFFICIENT_BALANCE
        )
        .catch(error => error);

      // Then
      expect(producer.sendSerializedValue).toBeCalled();
      expect(spyOn).toBeCalledWith(
        'sendFailedDebitCardTransactionAmount: Cannot send notification: Customer is not found in transaction'
      );
    });
  });

  describe('sendFailedCashWithdrawalDailyLimit', () => {
    it('should produce exceeded atm withdrawal daily limit notification message', async () => {
      // Given
      const notificationMessage: INotificationMessage = {
        notificationCode: NotificationCode.NOTIF_DAILY_LIMIT_ATM_WITHDRAWAL,
        customerId: '123456',
        payload: {
          amount: 'Rp100.000',
          pocketName: 'Test Pocket'
        }
      };
      (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);

      await transactionProducer.sendFailedCashWithdrawalDailyLimit(
        {
          customerId: '123456',
          pocketName: 'Test Pocket',
          amount: 100000
        },
        NotificationCode.NOTIF_DAILY_LIMIT_ATM_WITHDRAWAL
      );

      expect(producer.sendSerializedValue).toBeCalledWith({
        topic: NotificationTopicConstant.NOTIFICATION_CREATED,
        messages: [
          {
            value: notificationMessage,
            headers: { 'x-idempotency-key': uuidMockValue }
          }
        ]
      });
    });
  });

  describe('sendFailedMoneyTransferViaAtmDailyLimit', () => {
    it('should produce exceeded atm transfer daily limit notification message', async () => {
      const beneficiaryAccountNo = faker.random.alphaNumeric(10);
      const beneficiaryAccountName = 'bene name';
      // Given
      const notificationMessage: INotificationMessage = {
        notificationCode: NotificationCode.NOTIF_DAILY_LIMIT_ATM_TRANSFER,
        customerId: '123456',
        payload: {
          accountNumber: beneficiaryAccountNo,
          beneficiaryName: beneficiaryAccountName,
          amount: 'Rp100.000'
        }
      };
      (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);

      await transactionProducer.sendFailedMoneyTransferViaAtmDailyLimit(
        {
          customerId: '123456',
          amount: 100000,
          beneficiaryAccountNo: beneficiaryAccountNo,
          beneficiaryName: beneficiaryAccountName
        },
        NotificationCode.NOTIF_DAILY_LIMIT_ATM_TRANSFER
      );

      expect(producer.sendSerializedValue).toBeCalledWith({
        topic: NotificationTopicConstant.NOTIFICATION_CREATED,
        messages: [
          {
            value: notificationMessage,
            headers: { 'x-idempotency-key': uuidMockValue }
          }
        ]
      });
    });
  });
});
