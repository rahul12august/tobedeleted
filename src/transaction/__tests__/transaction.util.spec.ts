import {
  formatBeneficiaryAccountNoForWallet,
  formatToUtcTime,
  generateUniqueId,
  mapToCoreBankingTransaction,
  validateManualAdjustmentInterchangeAndChannel,
  validateTransactionForManualAdjustment,
  toTransactionList,
  mapToDisbursementTransaction,
  mapToRepaymentTransaction,
  mapToGlIncomeTransaction,
  mapToPayOffTransaction,
  loanAdditionalPayloadValidator,
  checkTransferSameCifForMigratingAccounts,
  extractTransactionInfoByInterchange,
  isGlTransactionDetailFieldExists,
  qrisAdditionalPayloadValidator,
  mapToQrisTransaction,
  mapToQrisTransferDb,
  mapTransactionModelToRefundTransaction,
  mapTransactionResultToExternalResponse
} from '../transaction.util';
import {
  getFeeMapping,
  getNNSMappingData,
  getRefundDetails,
  getTransactionCodeMapping,
  getTransactionCodeMappingWithFeeRule,
  getTransactionDetail,
  getTransactionModelData,
  getTransactionWithCoreBankingTransactionIds
} from '../__mocks__/transaction.data';

import {
  getBankCodeData,
  getGeneralLedgersMockData
} from '../../configuration/__mocks__/configuration.data';
import {
  GENERATE_TRANSACTION_DATE_FORMAT,
  TRANSACTION_DATE_FORMAT_STRING
} from '../transaction.constant';
import {
  BankChannelEnum,
  BankNetworkEnum,
  AdminPortalUserPermissionCode,
  PaymentServiceTypeEnum,
  GeneralLedgerCode,
  qrisEncryption
} from '@dk/module-common';
import { ERROR_CODE } from '../../common/errors';
import {
  GlTransactionTypes,
  TransactionStatus,
  TransferFailureReasonActor
} from '../transaction.enum';
import { ITransactionModel } from '../transaction.model';
import { generateAccountsData } from '../../account/__mocks__/account.data';
import {
  DEFAULT_DATE_FORMAT,
  DEFAULT_MASKING_PLACEHOLDER,
  UTC_OFFSET_WESTERN_INDONESIAN
} from '../../common/constant';
import moment from 'moment';
import coreBankingConstant from '../../coreBanking/coreBanking.constant';
import { get } from 'lodash';
import { AccountStatusEnum } from '../../account/account.enum';
import {
  getTransactionAdditionalPayloadQrisPayment,
  getNotValidTransactionAdditionalPayloadQrisPayment
} from '../../alto/__mocks__/alto.data';
import { AltoAccountTypeEnum } from '../../alto/alto.enum';
import altoUtils from '../../alto/alto.utils';
import configurationRepository from '../../configuration/configuration.repository';
import { BIFAST_PROXY_BANK_CODE } from '../../bifast/bifast.constant';

jest.mock('../../configuration/configuration.repository');
jest.mock('../transaction.repository');

describe('transaction.util', () => {
  describe('generateUniqueId', () => {
    it('should return value for object id', () => {
      const uniqueId = generateUniqueId();
      expect(uniqueId).toHaveLength(36);
    });
  });

  describe('formatBeneficiaryAccountNoForWallet', () => {
    it('should add prefix if wallet have prefix', () => {
      const accountNo = formatBeneficiaryAccountNoForWallet(
        {
          ...getBankCodeData(),
          prefix: '+62'
        },
        '123'
      );
      expect(accountNo).toEqual('+62123');
    });

    it('should use identify number if wallet not have prefix', () => {
      const accountNo = formatBeneficiaryAccountNoForWallet(
        {
          ...getBankCodeData(),
          prefix: undefined
        },
        '123'
      );
      expect(accountNo).toEqual('123');
    });
  });

  describe('format date string 2 string', () => {
    it(`should success formating date`, () => {
      const dateString = '200921094306';
      const result = formatToUtcTime(
        dateString,
        TRANSACTION_DATE_FORMAT_STRING,
        GENERATE_TRANSACTION_DATE_FORMAT
      );
      expect(result).toEqual('20200921');
    });
  });

  describe('mapToCoreBankingTransaction', () => {
    it('should map to core banking fieds', () => {
      const transactionModel = { ...getTransactionModelData(), id: 'id' };

      const result = mapToCoreBankingTransaction(
        transactionModel,
        'TFD30',
        'GoBills'
      );

      expect(result).toBeDefined();
    });

    it('should have sourceName & TragetName as undefined', () => {
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        sourceAccountName: '',
        beneficiaryAccountName: ''
      };

      const result = mapToCoreBankingTransaction(
        transactionModel,
        'TFD30',
        'GoBills'
      );

      expect(
        result._Custom_Source_Information.Source_Account_Name
      ).toBeUndefined();
      expect(
        result._Custom_Target_Information.Target_Account_Name
      ).toBeUndefined();
    });

    it('should have sourceAccountNo as undefined', () => {
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        sourceAccountNo: ''
      };

      const result = mapToCoreBankingTransaction(
        transactionModel,
        'TFD30',
        'GoBills'
      );

      expect(
        result._Custom_Source_Information.Source_Account_No
      ).toBeUndefined();
    });

    it('should have transactionUUID and paymentInstructionId', () => {
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        sourceAccountName: '',
        beneficiaryAccountName: '',
        paymentInstructionId: '12345abcd12345'
      };

      const result = mapToCoreBankingTransaction(
        transactionModel,
        'TFD30',
        'GoBills'
      );

      expect(result._Custom_Transaction_Details.Transaction_Id).toEqual('id');
      expect(result._Custom_Transaction_Details.PI_Id).toEqual(
        '12345abcd12345'
      );
    });

    it('should map additionalInformation4 to target account number when BI fast paymentServiceCode', () => {
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        paymentServiceCode: 'BIFAST_OUTGOING',
        additionalInformation3: '02',
        additionalInformation4: 'user@user.com'
      };

      const result = mapToCoreBankingTransaction(
        transactionModel,
        'TFD30',
        'GoBills'
      );

      expect(result).toBeDefined();
      expect(result._Custom_Target_Information.Target_Account_No).toEqual(
        transactionModel.additionalInformation4
      );
      expect(
        result._Custom_Target_Information.Target_Bank_Name
      ).toBeUndefined();
      expect(result._Custom_Target_Information.Institutional_Bank_Code).toEqual(
        BIFAST_PROXY_BANK_CODE
      );
    });

    it('should map beneficiary to target account number when BI fast paymentServiceCode & additionalInformation4 is undefined', () => {
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        paymentServiceCode: 'BIFAST_OUTGOING'
      };

      const result = mapToCoreBankingTransaction(
        transactionModel,
        'TFD30',
        'GoBills'
      );

      expect(result).toBeDefined();
      expect(result._Custom_Target_Information.Target_Account_No).toEqual(
        transactionModel.beneficiaryAccountNo
      );
      expect(result._Custom_Target_Information.Target_Bank_Name).toEqual(
        transactionModel.beneficiaryBankName
      );
      expect(result._Custom_Target_Information.Institutional_Bank_Code).toEqual(
        transactionModel.beneficiaryBankCode
      );
    });

    it('should map beneficiary to target account number when paymentServiceCode is VOID_BIFAST', () => {
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        paymentServiceCode: 'REFUND_BIFAST',
        additionalInformation4: 'additionalInformation4'
      };

      const result = mapToCoreBankingTransaction(
        transactionModel,
        'TFD30',
        'GoBills'
      );

      expect(result).toBeDefined();
      expect(result._Custom_Target_Information.Target_Account_No).toEqual(
        transactionModel.beneficiaryAccountNo
      );
      expect(result._Custom_Target_Information.Target_Bank_Name).toEqual(
        transactionModel.beneficiaryBankName
      );
      expect(result._Custom_Target_Information.Institutional_Bank_Code).toEqual(
        transactionModel.beneficiaryBankCode
      );
    });

    it('should map notes to transaction details', () => {
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        notes: 'notes'
      };

      const result = mapToCoreBankingTransaction(
        transactionModel,
        'TFD30',
        'GoBills'
      );

      expect(result).toBeDefined();
      expect(result.notes).toEqual(transactionModel.note);
    });
  });

  describe('validateManualAdjustmentInterchangeAndChannel', () => {
    it('throw error if BankCodeChannel other than IRIS, GOBILLS, EXTERNAL, TOKOPEDIA, WINCOR and QRIS', () => {
      const bankCodeChannel = BankChannelEnum.GOJEK;

      let error;
      try {
        validateManualAdjustmentInterchangeAndChannel(bankCodeChannel);
      } catch (e) {
        error = e;
      }

      expect(error.errorCode).toBe(ERROR_CODE.INVALID_BANK_CHANNEL);
    });

    it('throw error if BankCodeChannel is IRIS | GOBILLS | TOKOPEDIA | QRIS and interchange is present', () => {
      const bankCodeChannels = [
        BankChannelEnum.IRIS,
        BankChannelEnum.GOBILLS,
        BankChannelEnum.TOKOPEDIA,
        BankChannelEnum.QRIS
      ];
      const interchange = BankNetworkEnum.ALTO;

      bankCodeChannels.map(bankCodeChannel => {
        let error;
        try {
          validateManualAdjustmentInterchangeAndChannel(
            bankCodeChannel,
            interchange
          );
        } catch (e) {
          error = e;
        }

        expect(error.errorCode).toBe(ERROR_CODE.INTERCHANGE_NOT_EXPECTED);
      });
    });

    it('not throw error if BankCodeChannel is EXTERNAL and interchange is present', () => {
      const bankCodeChannel = BankChannelEnum.EXTERNAL;
      const interchange = BankNetworkEnum.ALTO;

      let error;
      try {
        validateManualAdjustmentInterchangeAndChannel(
          bankCodeChannel,
          interchange
        );
      } catch (e) {
        error = e;
      }

      expect(error).toBeUndefined();
    });
  });

  describe('validateTransactionForManualAdjustment', () => {
    const transaction = getTransactionModelData();

    it('should fail if transaction status is same as requested status', () => {
      const transactionModel: ITransactionModel = {
        ...transaction,
        id: '523348fd-3d7d-410a-a54c-341b37ecbb7b',
        status: TransactionStatus.DECLINED
      };

      let error;
      try {
        validateTransactionForManualAdjustment(
          transactionModel,
          TransactionStatus.DECLINED,
          BankNetworkEnum.ALTO
        );
      } catch (e) {
        error = e;
      }

      expect(error).toBeDefined();
      expect(error.actor).toBe(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toBe(
        `Manual adjustment validation failed. Invalid transaction status to manually adjusted for txnId: ${transactionModel.id}!`
      );
      expect(error.errorCode).toBe(ERROR_CODE.INVALID_TRANSACTION_STATUS);
    });

    it('should fail if transaction date is invalid', () => {
      const transactionModel: ITransactionModel = {
        ...transaction,
        id: 'c7931b62-2ce1-46a4-b942-4dc693fbd66a',
        updatedAt: new Date('2019-11-11T11:40:44.786Z'),
        status: TransactionStatus.DECLINED
      };

      let error;
      try {
        validateTransactionForManualAdjustment(
          transactionModel,
          TransactionStatus.SUCCEED,
          BankNetworkEnum.ALTO
        );
      } catch (e) {
        error = e;
      }

      expect(error).toBeDefined();
      expect(error.actor).toBe(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toBe(
        `Manual adjustment validation failed. Transaction ${transactionModel.id} with external Id ${transactionModel.externalId} is not eligible for Manual adjustment as transaction date is not withing limits!`
      );
      expect(error.errorCode).toBe(ERROR_CODE.TRANSACTION_DATE_NOT_ALLOWED);
    });
  });

  describe('toTransactionList', () => {
    it('should return transactions without masking when admin has employee role', () => {
      // given
      const transactionModel1 = {
        ...getTransactionModelData(),
        id: 'id1',
        creditTransactionCode: 'PRC01',
        refund: { remainingAmount: 2222 }
      };

      const transactionModel2 = {
        ...getTransactionModelData(),
        id: 'id2'
      };

      const adminRoles = [AdminPortalUserPermissionCode.EMPLOYEE_INFO_READ];

      // when
      const response = toTransactionList(
        [transactionModel1, transactionModel2],
        adminRoles,
        [],
        false
      );

      // then
      const firstResponseElement = response[0];
      expect(firstResponseElement.transactionAmount).toEqual(
        transactionModel1.transactionAmount
      );
      expect(firstResponseElement.refund?.remainingAmount).toEqual(2222);
    });

    it('should return transaction without masking when user do not have employee role but transaction is not sensitive', () => {
      // Given
      const transactionModel1 = {
        ...getTransactionModelData(),
        id: 'id1'
      };
      const transactionModel2 = {
        ...getTransactionModelData(),
        id: 'id2'
      };
      const adminRoles = [AdminPortalUserPermissionCode.CUSTOMER_INFO_READ];
      // When
      const response = toTransactionList(
        [transactionModel1, transactionModel2],
        adminRoles,
        [],
        false
      );
      // Then
      expect(response[0].transactionAmount).toEqual(
        transactionModel1.transactionAmount
      );
    });

    it('should return transaction with masking when user do not have employee role and credit transaction is sensitive', () => {
      // Given
      const transaction = getTransactionModelData();
      transaction.fees = [
        {
          feeCode: 'feeCode',
          feeAmount: 1111,
          customerTc: 'customerTc',
          customerTcChannel: 'customerTcChannel'
        }
      ];
      transaction.refund = {
        remainingAmount: 2222
      };
      const transactionModel = {
        ...transaction,
        creditTransactionCode: 'PRC01',
        id: 'id'
      };

      const transactionModel2 = {
        ...getTransactionModelData(),
        id: 'id2'
      };
      const adminRoles = [AdminPortalUserPermissionCode.CUSTOMER_INFO_READ];
      // When
      const response = toTransactionList(
        [transactionModel, transactionModel2],
        adminRoles,
        [],
        false
      );
      // Then
      expect(response[0].transactionAmount).toEqual(
        DEFAULT_MASKING_PLACEHOLDER
      );
      expect(response[0].fees).toEqual([
        {
          feeAmount: DEFAULT_MASKING_PLACEHOLDER
        }
      ]);
      expect(response[0]?.refund?.remainingAmount).toEqual(
        DEFAULT_MASKING_PLACEHOLDER
      );
    });

    it('should return transaction with masking when user do not have employee role and debit transaction is sensitive', () => {
      // Given
      const transactionModel = {
        ...getTransactionModelData(),
        debitTransactionCode: 'PRD01',
        id: 'id'
      };
      const transactionModel2 = {
        ...getTransactionModelData(),
        id: 'id2'
      };
      const adminRoles = [AdminPortalUserPermissionCode.CUSTOMER_INFO_READ];
      // When
      const response = toTransactionList(
        [transactionModel, transactionModel2],
        adminRoles,
        [],
        false
      );
      // Then
      expect(response[0].transactionAmount).toEqual(
        DEFAULT_MASKING_PLACEHOLDER
      );
    });

    it('should return transaction with refund info when fetchRefund is requested gives SUCCEED refund details', () => {
      // Given
      const transactionModel: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...getTransactionModelData(),
        refund: getRefundDetails()
      };
      const adminRoles = [AdminPortalUserPermissionCode.CUSTOMER_INFO_READ];

      // When
      const response = toTransactionList(
        [transactionModel],
        adminRoles,
        [
          {
            id: '5f8f7a1c481f6a70b636bc85',
            status: TransactionStatus.SUCCEED,
            createdAt: new Date('2020-10-17T03:09:05.461Z'),
            updatedAt: new Date('2020-10-17T03:09:05.466Z'),
            amount: 10000
          },
          {
            id: '5f8f7a1c481f6a70b636bc86',
            status: TransactionStatus.PENDING,
            createdAt: new Date('2020-10-17T03:10:23.362Z'),
            updatedAt: new Date('2020-10-18T03:10:23.364Z'),
            amount: 10000
          }
        ],
        true
      );

      // Then
      expect(response[0].refund?.latestTransaction?.id).toBe(
        '5f8f7a1c481f6a70b636bc85'
      );
      expect(response[0].refund?.latestTransaction?.status).toBe('SUCCEED');
    });

    it('should return transaction with refund info when fetchRefund is requested gives latest updated one if not found SUCCEED refund details', () => {
      // Given
      const transactionModel: ITransactionModel = {
        id: '5e412688fd14310c4969099a',
        ...getTransactionModelData(),
        refund: getRefundDetails()
      };
      const adminRoles = [AdminPortalUserPermissionCode.CUSTOMER_INFO_READ];

      // When
      const response = toTransactionList(
        [transactionModel],
        adminRoles,
        [
          {
            id: '5f8f7a1c481f6a70b636bc85',
            status: TransactionStatus.SUBMITTED,
            createdAt: new Date('2020-10-17T03:09:05.461Z'),
            updatedAt: new Date('2020-10-17T03:09:05.466Z'),
            amount: 10000
          },
          {
            id: '5f8f7a1c481f6a70b636bc86',
            status: TransactionStatus.PENDING,
            createdAt: new Date('2020-10-17T03:10:23.362Z'),
            updatedAt: new Date('2020-10-18T03:10:23.364Z'),
            amount: 10000
          }
        ],
        true
      );

      // Then
      expect(response[0].refund?.latestTransaction?.id).toBe(
        '5f8f7a1c481f6a70b636bc86'
      );
      expect(response[0].refund?.latestTransaction?.status).toBe('PENDING');
    });
  });

  describe('mapToDisbursementTransaction', () => {
    const coreBankingExternalId = 'corebanking-external-id';
    it('should return disbursementPayload', () => {
      // Given
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        additionalPayload: { coreBankingExternalId }
      };
      const transactionChannel = 'testChannel';
      const expectedResult = {
        externalId: transactionModel.additionalPayload.coreBankingExternalId,
        transactionDetails: {
          transactionChannelId: transactionChannel
        }
      };

      // When
      const result = mapToDisbursementTransaction(
        transactionModel,
        transactionChannel
      );

      // Then
      expect(result).toEqual(expectedResult);
    });

    it('should return disbursementPayload with valueDate and bookingDate if the loanTransactionDate is before today date', () => {
      // Given
      const loanTransactionDate = moment()
        .subtract(7, 'days')
        .utcOffset(UTC_OFFSET_WESTERN_INDONESIAN)
        .format();
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        additionalPayload: {
          loanTransactionDate,
          coreBankingExternalId
        }
      };
      const transactionChannel = 'testChannel';
      const expectedResult = {
        externalId: transactionModel.additionalPayload.coreBankingExternalId,
        transactionDetails: {
          transactionChannelId: transactionChannel
        },
        valueDate: moment
          .utc(loanTransactionDate)
          .utcOffset(UTC_OFFSET_WESTERN_INDONESIAN)
          .format(coreBankingConstant.VALUE_DATE_MAMBU_FORMAT),
        bookingDate: moment
          .utc()
          .utcOffset(UTC_OFFSET_WESTERN_INDONESIAN)
          .format(coreBankingConstant.VALUE_DATE_MAMBU_FORMAT)
      };

      // When
      const result = mapToDisbursementTransaction(
        transactionModel,
        transactionChannel
      );

      // Then
      expect(result).toEqual(expectedResult);
    });

    it('should return disbursementPayload without valueDate and bookingDate if the loanTransactionDate is same or after today date', () => {
      // Given
      const loanTransactionDate = moment()
        .utcOffset(UTC_OFFSET_WESTERN_INDONESIAN)
        .format();
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        additionalPayload: { loanTransactionDate, coreBankingExternalId }
      };
      const transactionChannel = 'testChannel';
      const expectedResult = {
        externalId: transactionModel.additionalPayload.coreBankingExternalId,
        transactionDetails: {
          transactionChannelId: transactionChannel
        }
      };

      // When
      const result = mapToDisbursementTransaction(
        transactionModel,
        transactionChannel
      );

      // Then
      expect(result).toEqual(expectedResult);
    });

    it('should return disbursementPayload with amount field if shouldDisburseWithAmount is true', () => {
      // Given
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        additionalPayload: {
          shouldDisburseWithAmount: true,
          coreBankingExternalId
        }
      };
      const transactionChannel = 'testChannel';
      const expectedResult = {
        externalId: transactionModel.additionalPayload.coreBankingExternalId,
        transactionDetails: {
          transactionChannelId: transactionChannel
        },
        amount: transactionModel.transactionAmount
      };

      // When
      const result = mapToDisbursementTransaction(
        transactionModel,
        transactionChannel
      );

      // Then
      expect(result).toEqual(expectedResult);
    });
  });

  describe('mapToRepaymentTransaction', () => {
    const coreBankingExternalId = 'corebanking-external-id';
    it('should return repaymentPayload without customPaymentAmounts if loanRepaymentAllocation is not defined', () => {
      // Given
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        additionalPayload: {
          coreBankingExternalId
        }
      };
      const transactionChannel = 'testChannel';
      const expectedResult = {
        externalId: `${transactionModel.additionalPayload.coreBankingExternalId}-rp`,
        transactionDetails: {
          transactionChannelId: transactionChannel
        },
        amount: transactionModel.transactionAmount
      };

      // When
      const result = mapToRepaymentTransaction(
        transactionModel,
        transactionChannel,
        0
      );

      // Then
      expect(result).toEqual(expectedResult);
    });

    it('should return repaymentPayload with customPaymentAmounts if loanRepaymentAllocation is defined', () => {
      // Given
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        additionalPayload: {
          loanRepaymentAllocation: [{ type: 'PRINCIPAL', amount: 10000 }],
          coreBankingExternalId
        }
      };
      const transactionChannel = 'testChannel';
      const expectedResult = {
        externalId: `${transactionModel.additionalPayload.coreBankingExternalId}-rp`,
        transactionDetails: {
          transactionChannelId: transactionChannel
        },
        amount: transactionModel.transactionAmount,
        customPaymentAmounts: [
          { customPaymentAmountType: 'PRINCIPAL', amount: 10000 }
        ]
      };

      // When
      const result = mapToRepaymentTransaction(
        transactionModel,
        transactionChannel,
        0
      );

      // Then
      expect(result).toEqual(expectedResult);
    });

    it('should return repaymentPayload with valueDate if the loanTransactionDate is defined', () => {
      // Given
      const loanTransactionDate = moment()
        .subtract(7, 'days')
        .utcOffset(UTC_OFFSET_WESTERN_INDONESIAN)
        .format(DEFAULT_DATE_FORMAT);
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        additionalPayload: { loanTransactionDate, coreBankingExternalId }
      };
      const transactionChannel = 'testChannel';
      const expectedResult = {
        externalId: `${transactionModel.additionalPayload.coreBankingExternalId}-rp`,
        transactionDetails: {
          transactionChannelId: transactionChannel
        },
        amount: transactionModel.transactionAmount,
        valueDate: moment(loanTransactionDate)
          .utcOffset(UTC_OFFSET_WESTERN_INDONESIAN)
          .format(coreBankingConstant.VALUE_DATE_MAMBU_FORMAT)
      };

      // When
      const result = mapToRepaymentTransaction(
        transactionModel,
        transactionChannel,
        0
      );

      // Then
      expect(result).toEqual(expectedResult);
    });

    it('should return repaymentPayload with top up allowance', () => {
      // Given
      const allowance = 1000;
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        additionalPayload: {
          loanRepaymentAllocation: [{ type: 'PRINCIPAL', amount: 10000 }],
          allowance,
          coreBankingExternalId
        }
      };
      const transactionChannel = 'testChannel';
      const expectedResult = {
        externalId: `${transactionModel.additionalPayload.coreBankingExternalId}-rp`,
        transactionDetails: {
          transactionChannelId: transactionChannel
        },
        amount: transactionModel.transactionAmount - allowance,
        customPaymentAmounts: [
          { customPaymentAmountType: 'PRINCIPAL', amount: 10000 }
        ]
      };

      // When
      const result = mapToRepaymentTransaction(
        transactionModel,
        transactionChannel,
        0
      );

      // Then
      expect(result).toEqual(expectedResult);
    });

    it('should return repaymentPayload with allowance used', () => {
      // Given
      const allowance = -1000;
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        additionalPayload: {
          loanRepaymentAllocation: [{ type: 'PRINCIPAL', amount: 10000 }],
          allowance,
          coreBankingExternalId
        }
      };
      const transactionChannel = 'testChannel';
      const expectedResult = {
        externalId: `${transactionModel.additionalPayload.coreBankingExternalId}-rp`,
        transactionDetails: {
          transactionChannelId: transactionChannel
        },
        amount: transactionModel.transactionAmount - allowance,
        customPaymentAmounts: [
          { customPaymentAmountType: 'PRINCIPAL', amount: 10000 }
        ]
      };

      // When
      const result = mapToRepaymentTransaction(
        transactionModel,
        transactionChannel,
        0
      );

      // Then
      expect(result).toEqual(expectedResult);
    });
  });

  describe('mapToPayOffTransaction', () => {
    const coreBankingExternalId = 'corebanking-external-id';
    it('should return payoffPayload without payOffAdjustableAmounts if loanRepaymentAllocation is not defined', () => {
      // Given
      const note = 'deceased';
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        note,
        additionalPayload: {
          coreBankingExternalId
        }
      };
      const transactionChannel = 'testChannel';
      const expectedResult = {
        externalId: `${transactionModel.additionalPayload.coreBankingExternalId}-po`,
        transactionDetails: {
          transactionChannelId: transactionChannel
        },
        notes: note
      };

      // When
      const result = mapToPayOffTransaction(
        transactionModel,
        transactionChannel
      );

      // Then
      expect(result).toEqual(expectedResult);
    });

    it('should return payoffPayload with payOffAdjustableAmounts if loanRepaymentAllocation is defined', () => {
      // Given
      const note = 'deceased';
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        note,
        additionalPayload: {
          loanRepaymentAllocation: [
            { type: 'INTEREST', amount: 1000 },
            { type: 'PRINCIPAL', amount: 10000 }
          ],
          coreBankingExternalId
        }
      };
      const transactionChannel = 'testChannel';
      const expectedResult = {
        externalId: `${transactionModel.additionalPayload.coreBankingExternalId}-po`,
        transactionDetails: {
          transactionChannelId: transactionChannel
        },
        payOffAdjustableAmounts: {
          interestPaid: 1000,
          penaltyPaid: 0
        },
        notes: note
      };

      // When
      const result = mapToPayOffTransaction(
        transactionModel,
        transactionChannel
      );

      // Then
      expect(result).toEqual(expectedResult);
    });
  });

  describe('mapToGlIncomeTransaction', () => {
    it('should return journal entries GL payload successfully', () => {
      // Given
      const loanTransactionDate = moment()
        .utcOffset(UTC_OFFSET_WESTERN_INDONESIAN)
        .format(DEFAULT_DATE_FORMAT);
      const amount = 10000;
      const mockGeneralLedgers = getGeneralLedgersMockData();
      const repaymentGl = mockGeneralLedgers.find(glItem => {
        return glItem.code === GeneralLedgerCode.REPAYMENT;
      });
      const bankIncomeGl = mockGeneralLedgers.find(glItem => {
        return glItem.code === GeneralLedgerCode.REPAYMENT;
      });
      const glTransactionDetail = [
        {
          glAccountCode: GeneralLedgerCode.REPAYMENT,
          amount: amount,
          type: GlTransactionTypes.DEBIT,
          accountNumber: repaymentGl?.accountNumber
        },
        {
          glAccountCode: GeneralLedgerCode.BANK_INCOME,
          amount: amount,
          type: GlTransactionTypes.CREDIT,
          accountNumber: bankIncomeGl?.accountNumber
        }
      ];
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        additionalPayload: {
          glTransactionDetail,
          loanTransactionDate
        },
        note: 'note'
      };

      const expectedResult = {
        date: moment(loanTransactionDate)
          .utcOffset(UTC_OFFSET_WESTERN_INDONESIAN)
          .format(coreBankingConstant.VALUE_DATE_MAMBU_FORMAT),
        credits: [{ amount, glAccount: bankIncomeGl?.accountNumber }],
        debits: [{ amount, glAccount: repaymentGl?.accountNumber }],
        notes: 'note'
      };

      // When
      const result = mapToGlIncomeTransaction(transactionModel);

      // Then
      expect(result).toEqual(expectedResult);
    });

    it('should return journal entries GL payload without credits, debits and transactionId if bankIncomeGlNumber, repaymentGlNumber and transactionId are not defined', () => {
      // Given
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id'
      };
      const expectedResult = {
        date: moment()
          .utcOffset(UTC_OFFSET_WESTERN_INDONESIAN)
          .format(coreBankingConstant.VALUE_DATE_MAMBU_FORMAT)
      };

      // When
      const result = mapToGlIncomeTransaction(transactionModel);

      // Then
      expect(result).toEqual(expectedResult);
    });
  });

  describe('loanAdditionalPayloadValidator', () => {
    describe('disbursement and repayment transaction', () => {
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        paymentServiceType: PaymentServiceTypeEnum.LOAN_DIRECT_DISBURSEMENT
      };
      const mockGeneralLedgers = getGeneralLedgersMockData();

      it('should bypass the validation if the additionalPayload is not defined', () => {
        const transactionModelWithWrongAdditionalPayload = {
          ...transactionModel,
          additionalPayload: undefined
        };

        expect(() =>
          loanAdditionalPayloadValidator(
            transactionModelWithWrongAdditionalPayload
          )
        ).not.toThrow();
      });

      it('should transform field to the correct data type and add additional data', () => {
        const transactionModelWithWrongAdditionalPayload = {
          ...transactionModel,
          additionalPayload: {
            glTransactionDetail: [
              {
                glAccountCode: GeneralLedgerCode.BANK_INCOME,
                amount: '10000',
                type: GlTransactionTypes.DEBIT
              },
              {
                glAccountCode: GeneralLedgerCode.REPAYMENT,
                amount: '10000',
                type: GlTransactionTypes.CREDIT
              }
            ],
            shouldDisburseWithAmount: 'true',
            allowance: '1000',
            loanRepaymentAllocation: [{ type: 123, amount: '10000' }],
            repaymentAmountForPayOffInterestSettlement: '10000'
          }
        };
        const bankIncomeGl = mockGeneralLedgers.find(glItem => {
          return glItem.code === GeneralLedgerCode.BANK_INCOME;
        });
        const repaymentGl = mockGeneralLedgers.find(glItem => {
          return glItem.code === GeneralLedgerCode.REPAYMENT;
        });

        loanAdditionalPayloadValidator(
          transactionModelWithWrongAdditionalPayload,
          mockGeneralLedgers
        );

        const payload =
          transactionModelWithWrongAdditionalPayload.additionalPayload;

        expect(payload['shouldDisburseWithAmount']).toBe(true);
        expect(payload['loanRepaymentAllocation'][0].type).toBe('123');
        expect(payload['loanRepaymentAllocation'][0].amount).toBe(10000);
        expect(payload['allowance']).toBe(1000);
        expect(payload['repaymentAmountForPayOffInterestSettlement']).toBe(
          10000
        );
        expect(payload['glTransactionDetail'][0].glAccountCode).toBe(
          GeneralLedgerCode.BANK_INCOME
        );
        expect(get(payload, 'glTransactionDetail.0.accountNumber')).toBe(
          bankIncomeGl?.accountNumber
        );
        expect(payload['glTransactionDetail'][0].amount).toBe(10000);
        expect(payload['glTransactionDetail'][0].type).toBe(
          GlTransactionTypes.DEBIT
        );
        expect(payload['glTransactionDetail'][1].glAccountCode).toBe(
          GeneralLedgerCode.REPAYMENT
        );
        expect(get(payload, 'glTransactionDetail.1.accountNumber')).toBe(
          repaymentGl?.accountNumber
        );
        expect(payload['glTransactionDetail'][1].amount).toBe(10000);
        expect(payload['glTransactionDetail'][1].type).toBe(
          GlTransactionTypes.CREDIT
        );
      });

      it('should throw error if glTransactionDetail contain wrong type', () => {
        const transactionModelWithWrongAdditionalPayload = {
          ...transactionModel,
          additionalPayload: {
            glTransactionDetail: [
              {
                glAccount: '123',
                amount: 10000,
                type: 'test'
              }
            ],
            shouldDisburseWithAmount: 'true',
            loanRepaymentAllocation: [{ type: 123, amount: '10000' }]
          }
        };

        let error;
        try {
          loanAdditionalPayloadValidator(
            transactionModelWithWrongAdditionalPayload,
            mockGeneralLedgers
          );
        } catch (e) {
          error = e;
        }

        expect(error).toBeDefined();
        expect(error.actor).toBe(TransferFailureReasonActor.SUBMITTER);
        expect(error.detail).toBe('Invalid additional payload!');
        expect(error.errorCode).toBe(ERROR_CODE.INVALID_ADDITIONAL_PAYLOAD);
      });

      it('should throw error if additionalPayload mandatory fields are undefined', () => {
        const transactionModelWithWrongAdditionalPayload = {
          ...transactionModel,
          additionalPayload: {
            loanRepaymentAllocation: [{}]
          }
        };

        let error;
        try {
          loanAdditionalPayloadValidator(
            transactionModelWithWrongAdditionalPayload
          );
        } catch (e) {
          error = e;
        }

        expect(error).toBeDefined();
        expect(error.actor).toBe(TransferFailureReasonActor.SUBMITTER);
        expect(error.detail).toBe('Invalid additional payload!');
        expect(error.errorCode).toBe(ERROR_CODE.INVALID_ADDITIONAL_PAYLOAD);
      });

      it('should not throw error if errorList is empty', () => {
        const transactionModelWithAdditionalPayload = {
          ...transactionModel,
          additionalPayload: {
            loanRepaymentAllocation: [{ type: 'PRINCIPAL', amount: 10000 }]
          }
        };

        expect(() => {
          loanAdditionalPayloadValidator(transactionModelWithAdditionalPayload);
        }).not.toThrow();
      });
    });
  });

  describe('checkTransferSameCifForMigratingAccounts', () => {
    it('should return false when source & beneficiary account is ACTIVE and transfer to different cif', () => {
      // Given
      const mockSourceAccount = {
        ...generateAccountsData(),
        cif: 'cif'
      };
      const mockBeneficiaryAccount = {
        ...generateAccountsData(),
        cif: 'otherCif'
      };

      // When
      const isTransferSameCifForMigratingAccounts = checkTransferSameCifForMigratingAccounts(
        mockSourceAccount,
        mockBeneficiaryAccount
      );

      // Then
      expect(isTransferSameCifForMigratingAccounts).toEqual(true);
    });

    it('should return false when source/beneficiary account status is MIGRATING with same cif', () => {
      // Given
      const mockSourceAccount = {
        ...generateAccountsData(),
        cif: 'cif',
        status: AccountStatusEnum.MIGRATING
      };
      const mockBeneficiaryAccount = {
        ...generateAccountsData(),
        cif: 'cif'
      };

      // When
      const isTransferSameCifForMigratingAccounts = checkTransferSameCifForMigratingAccounts(
        mockSourceAccount,
        mockBeneficiaryAccount
      );

      // Then
      expect(isTransferSameCifForMigratingAccounts).toEqual(true);
    });

    it('should return true when either source account or beneficiary account status is MIGRATING with different cif', () => {
      // Given
      const mockSourceAccount = {
        ...generateAccountsData(),
        cif: 'cif'
      };
      const mockBeneficiaryAccount = {
        ...generateAccountsData(),
        cif: 'otherCif',
        status: AccountStatusEnum.MIGRATING
      };

      // When
      const isTransferSameCifForMigratingAccounts = checkTransferSameCifForMigratingAccounts(
        mockSourceAccount,
        mockBeneficiaryAccount
      );

      // Then
      expect(isTransferSameCifForMigratingAccounts).toEqual(false);
    });
  });

  describe('extractTransactionInfoByInterchange', () => {
    it('should return debitTransactionCode and creditTransactionCode based on debitPriority and creditPriority', () => {
      // Given
      const transactionModel = {
        ...getTransactionModelData(),
        ...getTransactionCodeMapping(),
        ...getFeeMapping(),
        debitPriority: BankNetworkEnum.ALTO,
        creditPriority: BankNetworkEnum.ALTO
      };

      // When
      const result = extractTransactionInfoByInterchange(
        transactionModel,
        undefined,
        undefined
      );

      // Then
      expect(result).toBeDefined();
      expect(result.debitTransactionCode).toEqual('d02');
      expect(result.creditTransactionCode).toEqual('c02');
    });

    it('should return debitTransactionCode, creditTransactionCode and feeCode based on debitPriority and creditPriority', () => {
      // Given
      const transactionModel = {
        ...getTransactionModelData(),
        ...getTransactionCodeMappingWithFeeRule(),
        ...getFeeMapping(),
        debitPriority: BankNetworkEnum.ALTO,
        creditPriority: BankNetworkEnum.ALTO
      };

      // When
      const result = extractTransactionInfoByInterchange(
        transactionModel,
        undefined,
        undefined
      );

      // Then
      expect(result).toBeDefined();
      expect(result.debitTransactionCode).toEqual('d02');
      expect(result.creditTransactionCode).toEqual('c02');
      expect(result.fees?.[0]?.feeCode).toEqual('CW004');
    });

    it('should return first debitTransactionCode and creditTransactionCode when no interchange provided', () => {
      // Given
      let tcMapping = getTransactionCodeMapping();
      tcMapping.creditTransactionCodeMapping.forEach(
        tc => (tc.interchange = undefined)
      );
      tcMapping.debitTransactionCodeMapping.forEach(
        tc => (tc.interchange = undefined)
      );
      let feeData = getFeeMapping();
      feeData.feeMapping.forEach(
        feeMapping => (feeMapping.interchange = undefined)
      );
      const transactionModel = {
        ...getTransactionModelData(),
        ...tcMapping,
        ...feeData,
        debitPriority: BankNetworkEnum.ARTAJASA,
        creditPriority: BankNetworkEnum.ARTAJASA
      };

      // When
      const result = extractTransactionInfoByInterchange(
        transactionModel,
        undefined,
        undefined
      );

      // Then
      expect(result).toBeDefined();
      expect(result.debitTransactionCode).toEqual('d01');
      expect(result.creditTransactionCode).toEqual('c01');
    });

    it('should return first debitTransactionCode, creditTransactionCode and feeCode when no interchange provided', () => {
      // Given
      let tcMapping = getTransactionCodeMappingWithFeeRule();
      tcMapping.creditTransactionCodeMapping.forEach(
        tc => (tc.interchange = undefined)
      );
      tcMapping.debitTransactionCodeMapping.forEach(
        tc => (tc.interchange = undefined)
      );
      let feeData = getFeeMapping();
      feeData.feeMapping.forEach(
        feeMapping => (feeMapping.interchange = undefined)
      );
      const transactionModel = {
        ...getTransactionModelData(),
        ...tcMapping,
        ...feeData,
        debitPriority: BankNetworkEnum.ARTAJASA,
        creditPriority: BankNetworkEnum.ARTAJASA
      };

      // When
      const result = extractTransactionInfoByInterchange(
        transactionModel,
        undefined,
        undefined
      );

      // Then
      expect(result).toBeDefined();
      expect(result.debitTransactionCode).toEqual('d01');
      expect(result.creditTransactionCode).toEqual('c01');
      expect(result.fees[0].feeCode).toEqual('CW003');
    });
  });

  describe('isGlTransactionDetailFieldExists', () => {
    // given
    const mockTransactionModel = {
      ...getTransactionModelData(),
      id: 'id',
      paymentServiceType: PaymentServiceTypeEnum.LOAN_DIRECT_DISBURSEMENT
    };

    it('should return false if additionalPayload is undefined', () => {
      // when
      const result = isGlTransactionDetailFieldExists(mockTransactionModel);

      // then
      expect(result).toEqual(false);
    });

    it('should return false if glTransactionDetail is undefined', () => {
      mockTransactionModel.additionalPayload = {};

      // when
      const result = isGlTransactionDetailFieldExists(mockTransactionModel);

      // then
      expect(result).toEqual(false);
    });

    it('should return true if glTransactionDetail is defined', () => {
      mockTransactionModel.additionalPayload = {
        glTransactionDetail: [
          {
            glAccountCode: GeneralLedgerCode.BANK_INCOME,
            amount: '10000',
            type: GlTransactionTypes.DEBIT
          },
          {
            glAccountCode: GeneralLedgerCode.REPAYMENT,
            amount: '10000',
            type: GlTransactionTypes.CREDIT
          }
        ]
      };

      // when
      const result = isGlTransactionDetailFieldExists(mockTransactionModel);

      // then
      expect(result).toEqual(true);
    });
  });

  describe('qrisAdditionalPayloadValidator', () => {
    describe('QRIS transaction', () => {
      const transactionModel = {
        ...getTransactionModelData(),
        id: 'id',
        paymentServiceType: PaymentServiceTypeEnum.QRIS
      };

      it('should throw error if additionalPayload does not exist', () => {
        let error;
        try {
          qrisAdditionalPayloadValidator(transactionModel.additionalPayload);
        } catch (e) {
          error = e;
        }

        expect(error).toBeDefined();
        expect(error.actor).toBe(TransferFailureReasonActor.SUBMITTER);
        expect(error.detail).toBe('Additional payload is empty!');
        expect(error.errorCode).toBe(ERROR_CODE.ADDITIONAL_PAYLOAD_NOT_EXIST);
      });

      it('should pass the validator', () => {
        // Given
        const additionalPayload: any = getTransactionAdditionalPayloadQrisPayment();
        const input = {
          ...getTransactionModelData(),
          id: 'id',
          paymentServiceType: PaymentServiceTypeEnum.QRIS,
          additionalPayload
        };

        // Then
        expect(() => {
          qrisAdditionalPayloadValidator(input.additionalPayload);
        }).not.toThrow();
      });

      it('should transform field to the correct data type', async () => {
        // Given
        const additionalPayload: any = getTransactionAdditionalPayloadQrisPayment();
        const sourceInput = {
          ...getTransactionModelData(),
          id: 'id',
          paymentServiceType: PaymentServiceTypeEnum.QRIS,
          additionalPayload
        };
        (configurationRepository.getNNSMapping as jest.Mock).mockResolvedValueOnce(
          getNNSMappingData()
        );

        const updatedInput = await mapToQrisTransferDb(sourceInput);
        const input = { ...sourceInput, ...updatedInput };

        // When
        const result = mapToQrisTransaction(input, additionalPayload);

        const mpan = '936011010099879098';
        const expectedMpan = `${mpan}${qrisEncryption.getCheckDigit(
          '936011010099879098'
        )}`;

        //Then
        expect(result).toEqual(
          expect.objectContaining({
            /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
            currency_code: 'IDR',
            amount: 100500,
            fee: 500,
            national_mid: 'IN1019000000024',
            additional_data: 'ABCDEFGHIJ',
            terminal_label: 'K19',
            merchant: {
              pan: expectedMpan,
              id: '999540008',
              criteria: 'UME',
              name: 'Somay Mangga Dua',
              city: 'Jakarta',
              mcc: '6600',
              postal_code: '10110',
              country_code: 'ID'
            },
            customer: {
              pan: altoUtils.generateCustomerPan('9360002319993788396'),
              name: 'Sebastian',
              account_type: AltoAccountTypeEnum.SAVING
            }
          })
        );
      });

      it('should transform data transaction to the correct fields', async () => {
        // Given
        const additionalPayload: any = getTransactionAdditionalPayloadQrisPayment();
        const input = {
          ...getTransactionModelData(),
          id: 'id',
          paymentServiceType: PaymentServiceTypeEnum.QRIS,
          externalId: '123123123',
          additionalPayload
        };
        (configurationRepository.getNNSMapping as jest.Mock).mockResolvedValueOnce(
          getNNSMappingData()
        );

        // When
        const result = await mapToQrisTransferDb(input);

        //Then
        expect(configurationRepository.getNNSMapping).toBeCalledWith(
          result.beneficiaryBankName
        );

        const beneficiaryAccountNumber = '936011010099879098';
        const expectedBeneficiaryAccountNumber = `${beneficiaryAccountNumber}${qrisEncryption.getCheckDigit(
          beneficiaryAccountNumber
        )}`;
        expect(result).toEqual({
          additionalInformation1: 'BRI',
          additionalInformation2: '9360002319993788398',
          additionalInformation3: 'Jakarta',
          beneficiaryAccountName: 'Somay Mangga Dua',
          beneficiaryAccountNo: expectedBeneficiaryAccountNumber,
          beneficiaryAccountType: '6600',
          beneficiaryBankName: '93601101',
          thirdPartyOutgoingId: '123123123'
        });
      });

      it('should throw an error when transform data transaction if NNS Mapping Not Found', async () => {
        // given
        const additionalPayload = getTransactionAdditionalPayloadQrisPayment();
        const input: any = {
          ...getTransactionModelData(),
          id: 'id',
          paymentServiceType: PaymentServiceTypeEnum.QRIS,
          paymentInstructionId: '123123123',
          additionalPayload
        };
        (configurationRepository.getNNSMapping as jest.Mock).mockResolvedValueOnce(
          undefined
        );

        // when
        const error = await mapToQrisTransferDb(input).catch(error => error);

        // then
        expect(error).toBeDefined();
        expect(error.actor).toBe(TransferFailureReasonActor.UNKNOWN);
        expect(error.detail).toBe('Qris NNS mapping data is undefined!');
        expect(error.errorCode).toBe(ERROR_CODE.QRIS_NNS_MAPPING_NOT_FOUND);
      });

      it('should throw error if additionalPayload mandatory fields are undefined', () => {
        // Given
        const additionalPayload: any = getNotValidTransactionAdditionalPayloadQrisPayment();
        const input = {
          ...getTransactionModelData(),
          id: 'id',
          paymentServiceType: PaymentServiceTypeEnum.QRIS,
          additionalPayload
        };

        // when
        let error;
        try {
          qrisAdditionalPayloadValidator(input.additionalPayload);
        } catch (e) {
          error = e;
        }

        // then
        expect(error).toBeDefined();
        expect(error.actor).toBe(TransferFailureReasonActor.SUBMITTER);
        expect(error.detail).toBe('Qris additional payload validation failed!');
        expect(error.errorCode).toBe(ERROR_CODE.INVALID_ADDITIONAL_PAYLOAD);
      });
    });
  });

  describe('mapTransactionModelToRefundTransaction', () => {
    it('should returnIRefundTransactionBasicDetail when ITransactionModel passed ', () => {
      const refundTransactions = mapTransactionModelToRefundTransaction([
        getTransactionDetail()
      ]);
      expect(refundTransactions.length).toBe(1);
    });
  });

  describe('mapTransactionResultToExternalResponse', () => {
    let transactionModel: ITransactionModel;

    beforeEach(() => {
      transactionModel = getTransactionWithCoreBankingTransactionIds();
    });

    it('should not populate coreBankingTransactions property if includeCoreBankingTransactions is false', () => {
      const response = mapTransactionResultToExternalResponse(
        transactionModel,
        false
      );

      expect(response.id).toBe(transactionModel.id);
      expect(response.externalId).toBe(transactionModel.externalId);
      expect(response.availableBalance).toBe(transactionModel.availableBalance);
      expect(response.coreBankingTransactions).toBeUndefined();
    });

    it('should populate coreBankingTransactions property if includeCoreBankingTransactions is true', () => {
      const response = mapTransactionResultToExternalResponse(
        transactionModel,
        true
      );

      expect(response.id).toBe(transactionModel.id);
      expect(response.externalId).toBe(transactionModel.externalId);
      expect(response.availableBalance).toBe(transactionModel.availableBalance);
      expect(response.coreBankingTransactions).toBe(
        transactionModel.coreBankingTransactions
      );
    });
  });
});
