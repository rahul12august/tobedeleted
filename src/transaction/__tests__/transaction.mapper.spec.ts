import transactionMapper from '../transaction.mapper';
import { ITransactionInput } from '../transaction.type';
import { BankChannelEnum, PaymentServiceTypeEnum } from '@dk/module-common';
import configurationRepository from '../../configuration/configuration.repository';
import {
  ILimitGroup,
  IPaymentConfigRule
} from '../../configuration/configuration.type';
import { getITransactionInput } from '../__mocks__/transaction.data';
import {
  createTransaction,
  getAutoRouteRecommendPaymentServiceTestCases,
  getBiFastRecommendPaymentServiceTestCases,
  getNonBiFastRecommendPaymentServiceTestCases,
  getRDNRecommendPaymentServiceTestCases
} from '../__mocks__/recommendPaymentServices.data';
import {
  createConfigRules,
  getLimitGroupCodes
} from '../__mocks__/transaction.mapper.data';
import {
  getBifastPaymentConfigRule,
  getBifastShariaPaymentConfigRule,
  getLimitGroupsData,
  getPaymentConfigRulesData,
  getRDNPaymentConfigRule,
  getRTGSAndBIFASTPaymentConfigRule,
  getRtolPaymentConfigRule,
  getSknAndRtgsPaymentConfigRule,
  getSknPaymentConfigRule,
  workingTimeData
} from '../../configuration/__mocks__/configuration.data';
import { UsageCounter } from '../../award/award.type';
import configurationService from '../../configuration/configuration.service';
import customerCache from '../../customer/customer.cache';
import { ERROR_CODE } from '../../common/errors';
import { RuleConditionStatus } from '../transaction.type';
import { TransferFailureReasonActor } from '../transaction.enum';
import { TransferAppError } from '../../errors/AppError';
jest.mock('../../common/featureFlag');
jest.mock('../../account/account.cache');
jest.mock('../../configuration/configuration.repository');
jest.mock('../../account/account.service');
jest.mock('../../account/account.repository');
jest.mock('../../fee/fee.service');
jest.mock('../../award/award.repository');
jest.mock('../transaction.util', () => {
  const original = require.requireActual('../transaction.util');
  return {
    ...original,
    generateUniqueId: jest.fn()
  };
});
jest.mock('../../coreBanking/deposit.repository');
jest.mock('../transaction.repository');
jest.mock('../../configuration/configuration.service');
jest.mock('../../customer/customer.cache');

describe('transactionMapper', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('mapAuthenticationRequired', () => {
    describe('getPaymentServiceMappings', () => {
      beforeEach(() => {
        jest.resetAllMocks();
        (configurationService.isBiFastEnabled as jest.Mock).mockResolvedValue(
          true
        );
        (customerCache.getCustomerType as jest.Mock).mockResolvedValue(
          'CUSTOMER_TYPE_01'
        );
      });
      describe('should return correct service codes for required cases', () => {
        const testCases = getBiFastRecommendPaymentServiceTestCases().concat(
          getRDNRecommendPaymentServiceTestCases()
        );
        const ruleConfigs = getPaymentConfigRulesData().concat(
          getRDNPaymentConfigRule()
        );
        const limitGroups = getLimitGroupsData();
        const defaultDailyUsage = [
          {
            limitGroupCode: 'L002',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          },
          {
            limitGroupCode: 'L003',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          },
          {
            limitGroupCode: 'L004',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          },
          {
            limitGroupCode: 'L019',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          }
        ];

        const OLD_ENV = process.env;

        beforeEach(() => {
          jest.resetModules();
          process.env = { ...OLD_ENV };
        });

        afterAll(() => {
          process.env = OLD_ENV;
        });

        it.each(testCases)('validations %#', async testCase => {
          // Given
          const input = testCase.input;
          const dailyUsage = testCase.dailyUsage || defaultDailyUsage;

          (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValue(
            []
          );
          (configurationRepository.getWorkingTime as jest.Mock).mockResolvedValue(
            workingTimeData()
          );
          // When
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            ruleConfigs,
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result.map(pm => pm.paymentServiceCode)).toEqual(
            testCase.output
          );
        });

        it(`validate RTOL payment config rule BIFAST_ENABLED is true`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
            sourceAccountType: 'MA',
            beneficiaryAccountType: 'ANY',
            transactionAmount: 2500000,
            beneficiarySupportedChannels: [
              BankChannelEnum.BIFAST,
              BankChannelEnum.EXTERNAL
            ]
          };
          const dailyUsage = defaultDailyUsage;

          // When
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            getRtolPaymentConfigRule(),
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result).toEqual([]);
        });

        it(`validate BIFAST_OUTGOING payment config rule when feature flag is false`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
            sourceAccountType: 'MA',
            beneficiaryAccountType: 'ANY',
            transactionAmount: 250000000,
            beneficiarySupportedChannels: [
              BankChannelEnum.BIFAST,
              BankChannelEnum.EXTERNAL
            ]
          };
          const dailyUsage = defaultDailyUsage;

          (configurationService.isBiFastEnabled as jest.Mock).mockResolvedValueOnce(
            false
          );
          // When
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            getBifastPaymentConfigRule(),
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result).toEqual([]);
        });

        it(`validate BIFAST_OUTGOING payment config rule when bank doesn't support Bi Fast`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
            sourceAccountType: 'MA',
            beneficiaryAccountType: 'ANY',
            transactionAmount: 250000000,
            beneficiarySupportedChannels: [BankChannelEnum.EXTERNAL]
          };
          const dailyUsage = defaultDailyUsage;

          (configurationService.isBiFastEnabled as jest.Mock).mockResolvedValueOnce(
            false
          );
          // When
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            getBifastPaymentConfigRule(),
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result).toEqual([]);
        });

        it(`validate INCOMING_BIFAST payment config rule`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            paymentServiceType: PaymentServiceTypeEnum.INCOMING_BIFAST,
            beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
            beneficiaryAccountType: 'MA',
            sourceAccountType: 'ANY',
            sourceBankCode: 'BC005',
            sourceBankCodeChannel: BankChannelEnum.EXTERNAL,
            transactionAmount: 250000000
          };
          const dailyUsage = defaultDailyUsage;

          (configurationService.isBiFastEnabled as jest.Mock).mockResolvedValueOnce(
            true
          );
          // When
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            getPaymentConfigRulesData(),
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result).toBeDefined();
          expect(result[0].paymentServiceCode).toEqual('BIFAST_INCOMING');
        });

        it(`validate BIFAST_OUTGOING payment daily limit when daily limit is exhausted`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
            sourceAccountType: 'MA',
            beneficiaryAccountType: 'ANY',
            transactionAmount: 250000000,
            beneficiarySupportedChannels: [
              BankChannelEnum.BIFAST,
              BankChannelEnum.EXTERNAL
            ]
          };
          const dailyUsage = [
            ...defaultDailyUsage,
            {
              limitGroupCode: 'L019',
              dailyAccumulationAmount: 250000000,
              monthlyAccumulationTransaction: 250000000
            }
          ];

          // When
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            getBifastPaymentConfigRule(),
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result).toEqual([]);
        });

        it(`validate SKN payment config rule`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
            sourceAccountType: 'MA',
            beneficiaryAccountType: 'ANY',
            transactionAmount: 60000000
          };
          const dailyUsage = [
            {
              limitGroupCode: 'L002',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 0
            },
            {
              limitGroupCode: 'L003',
              dailyAccumulationAmount: 10000,
              monthlyAccumulationTransaction: 0
            },
            {
              limitGroupCode: 'L004',
              dailyAccumulationAmount: 10000,
              monthlyAccumulationTransaction: 0
            }
          ];
          // When
          (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValue(
            []
          );
          (configurationRepository.getWorkingTime as jest.Mock).mockResolvedValueOnce(
            workingTimeData()
          );
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            getSknPaymentConfigRule(),
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result.map(pm => pm.paymentServiceCode)).toEqual(['SKN']);
        });

        it(`validate SKN or RTGS payment config rule if it is a holiday`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
            sourceAccountType: 'MA',
            beneficiaryAccountType: 'ANY',
            transactionAmount: 60000000
          };
          const dailyUsage = defaultDailyUsage;
          // When
          (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValue(
            [
              {
                name: `New Year's Day`,
                fromDate: new Date('2021-02-01T00:00:00.000Z'),
                toDate: new Date('2021-02-01T11:59:99.999Z')
              }
            ]
          );

          (configurationRepository.getWorkingTime as jest.Mock).mockResolvedValueOnce(
            workingTimeData()
          );
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            getSknAndRtgsPaymentConfigRule(),
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result).toEqual([]);
        });

        it(`validate SKN or RTGS payment config rule if day is not available in working time`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
            sourceAccountType: 'MA',
            beneficiaryAccountType: 'ANY',
            transactionAmount: 100000002
          };
          const dailyUsage = defaultDailyUsage;
          // When
          (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValueOnce(
            []
          );
          //this test return [] as working time is mocked to [] ... but why ?
          (configurationRepository.getWorkingTime as jest.Mock).mockResolvedValue(
            []
          );
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            getSknAndRtgsPaymentConfigRule(),
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result).toEqual([]);
        });

        it(`validate SKN or RTGS payment config rule if time is not matching from working time`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
            sourceAccountType: 'MA',
            beneficiaryAccountType: 'ANY',
            transactionAmount: 100000002
          };
          const dailyUsage = defaultDailyUsage;
          // When
          (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValueOnce(
            []
          );

          (configurationRepository.getWorkingTime as jest.Mock).mockResolvedValue(
            workingTimeData()
          );
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            getSknAndRtgsPaymentConfigRule(),
            limitGroups,
            dailyUsage,
            new Date('2021-01-01T08:00:00.000Z')
          );

          // Then
          expect(result).toEqual([]);
        });

        it(`validate RTGS payment config rule when BIFAST_ENABLED is false`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
            sourceAccountType: 'MA',
            beneficiaryAccountType: 'ANY',
            transactionAmount: 250000000,
            beneficiarySupportedChannels: [
              BankChannelEnum.BIFAST,
              BankChannelEnum.EXTERNAL
            ]
          };
          const dailyUsage = defaultDailyUsage;

          (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValue(
            []
          );
          (configurationRepository.getWorkingTime as jest.Mock).mockResolvedValue(
            workingTimeData()
          );

          (configurationService.isBiFastEnabled as jest.Mock).mockResolvedValueOnce(
            false
          );

          // When
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            getRTGSAndBIFASTPaymentConfigRule(),
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result).toBeDefined();
          expect(result[0]).toEqual(
            expect.objectContaining({ paymentServiceCode: 'RTGS' })
          );
        });

        it(`validate BIFAST_OUTGOING_SHARIA payment config rule when sharia feature flag is false`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
            sourceAccountType: 'MA',
            beneficiaryAccountType: 'ANY',
            transactionAmount: 250000000,
            beneficiarySupportedChannels: [
              BankChannelEnum.BIFAST,
              BankChannelEnum.EXTERNAL
            ]
          };
          const dailyUsage = defaultDailyUsage;

          (customerCache.getCustomerType as jest.Mock).mockResolvedValue(
            'INDIVIDUAL_SHARIA'
          );
          // When
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            getBifastShariaPaymentConfigRule(),
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result).toEqual([]);
        });

        it(`OCT transaction should be deined, if tranType is set.`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            tranType: 'OC'
          };
          const dailyUsage = defaultDailyUsage;
          // When
          const fn = async () => {
            await transactionMapper.getPaymentServiceMappings(
              input,
              getBifastShariaPaymentConfigRule(),
              limitGroups,
              dailyUsage,
              new Date('2021-02-01T00:00:00.000Z')
            );
          };

          // Then
          await expect(fn()).rejects.toThrow(
            new TransferAppError(
              `Original Credit Transaction is denied. externalId: ${input.externalId}`,
              TransferFailureReasonActor.SUBMITTER,
              ERROR_CODE.INVALID_REQUEST
            )
          );
        });

        it(`OCT transaction should be deined - with fallback.`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            paymentServiceType: PaymentServiceTypeEnum.THIRD_PARTY_TRANSFER,
            sourceAccountNo: '000000',
            sourceBankCode: '360005'
          };
          const dailyUsage = defaultDailyUsage;
          // When
          const fn = async () => {
            await transactionMapper.getPaymentServiceMappings(
              input,
              getBifastShariaPaymentConfigRule(),
              limitGroups,
              dailyUsage,
              new Date('2021-02-01T00:00:00.000Z')
            );
          };

          // Then
          await expect(fn()).rejects.toThrow(
            new TransferAppError(
              `Original Credit Transaction is denied. externalId: ${input.externalId}`,
              TransferFailureReasonActor.SUBMITTER,
              ERROR_CODE.INVALID_REQUEST
            )
          );
        });

        it(`OCT transaction should be allowed if feature toggle is false, event if tranType is set.`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            tranType: 'OC'
          };
          const dailyUsage = defaultDailyUsage;
          process.env.OCT = 'false';
          // When
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            getBifastShariaPaymentConfigRule(),
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result).toBeDefined();
        });

        it(`OCT transaction should be allowed if feature toggle is false, event if with fallback.`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            paymentServiceType: PaymentServiceTypeEnum.THIRD_PARTY_TRANSFER,
            sourceAccountNo: '000000',
            sourceBankCode: '360005'
          };
          const dailyUsage = defaultDailyUsage;
          process.env.OCT = 'false';

          // When
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            getBifastShariaPaymentConfigRule(),
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result).toBeDefined();
        });

        it(`validate SKN payment config rule when preferedBankChannel is EXTERNAL`, async () => {
          // Given
          const input = {
            ...createTransaction(),
            paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
            beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
            sourceAccountType: 'MA',
            beneficiaryAccountType: 'ANY',
            transactionAmount: 60000000,
            preferedBankChannel: BankChannelEnum.EXTERNAL
          };
          const dailyUsage = [
            {
              limitGroupCode: 'L002',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 0
            },
            {
              limitGroupCode: 'L003',
              dailyAccumulationAmount: 10000,
              monthlyAccumulationTransaction: 0
            },
            {
              limitGroupCode: 'L004',
              dailyAccumulationAmount: 10000,
              monthlyAccumulationTransaction: 0
            }
          ];
          // When
          (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValue(
            []
          );
          (configurationRepository.getWorkingTime as jest.Mock).mockResolvedValueOnce(
            workingTimeData()
          );
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            getSknPaymentConfigRule(),
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result.map(pm => pm.paymentServiceCode)).toEqual(['SKN']);
        });
      });

      describe('should return correct service codes for required cases when BiFast is disabled', () => {
        const testCases = getNonBiFastRecommendPaymentServiceTestCases();
        const ruleConfigs = getPaymentConfigRulesData();
        const limitGroups = getLimitGroupsData();
        const defaultDailyUsage = [
          {
            limitGroupCode: 'L002',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          },
          {
            limitGroupCode: 'L003',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          },
          {
            limitGroupCode: 'L004',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          },
          {
            limitGroupCode: 'L019',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          }
        ];
        beforeEach(() => {
          jest.resetAllMocks();
          (configurationService.isBiFastEnabled as jest.Mock).mockResolvedValue(
            false
          );
        });
        it.each(testCases)('validations %#', async testCase => {
          // Given
          const input = testCase.input;
          const dailyUsage = testCase.dailyUsage || defaultDailyUsage;

          (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValue(
            []
          );
          (configurationRepository.getWorkingTime as jest.Mock).mockResolvedValue(
            workingTimeData()
          );
          // When
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            ruleConfigs,
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result.map(pm => pm.paymentServiceCode)).toEqual(
            testCase.output
          );
        });
      });

      describe('should return correct service codes for required cases when BiFast is enable', () => {
        const testCases = getBiFastRecommendPaymentServiceTestCases();
        const ruleConfigs = getPaymentConfigRulesData();
        const limitGroups = getLimitGroupsData();
        const defaultDailyUsage = [
          {
            limitGroupCode: 'L002',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          },
          {
            limitGroupCode: 'L003',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          },
          {
            limitGroupCode: 'L004',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          },
          {
            limitGroupCode: 'L019',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          }
        ];
        it.each(testCases)('validations %#', async testCase => {
          // Given
          const input = testCase.input;
          const dailyUsage = testCase.dailyUsage || defaultDailyUsage;

          (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValue(
            []
          );
          (configurationRepository.getWorkingTime as jest.Mock).mockResolvedValue(
            workingTimeData()
          );
          // When
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            ruleConfigs,
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result.map(pm => pm.paymentServiceCode)).toEqual(
            testCase.output
          );
        });
      });

      describe('should return correct service codes for required cases when BiFast is enable but account inquiry route to Euronet', () => {
        const testCases = getAutoRouteRecommendPaymentServiceTestCases();
        const ruleConfigs = getPaymentConfigRulesData();
        const limitGroups = getLimitGroupsData();
        const defaultDailyUsage = [
          {
            limitGroupCode: 'L002',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          },
          {
            limitGroupCode: 'L003',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          },
          {
            limitGroupCode: 'L004',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          },
          {
            limitGroupCode: 'L019',
            dailyAccumulationAmount: 0,
            monthlyAccumulationTransaction: 0
          }
        ];
        it.each(testCases)('validations %#', async testCase => {
          // Given
          const input = testCase.input;
          const dailyUsage = testCase.dailyUsage || defaultDailyUsage;

          (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValue(
            []
          );
          (configurationRepository.getWorkingTime as jest.Mock).mockResolvedValue(
            workingTimeData()
          );
          // When
          const result = await transactionMapper.getPaymentServiceMappings(
            input,
            ruleConfigs,
            limitGroups,
            dailyUsage,
            new Date('2021-02-01T00:00:00.000Z')
          );

          // Then
          expect(result.map(pm => pm.paymentServiceCode)).toEqual(
            testCase.output
          );
        });
      });

      it('should return empty array if services are not found', async () => {
        const result = await transactionMapper.getPaymentServiceMappings(
          getITransactionInput(),
          [],
          [],
          []
        );

        // Then
        expect(result).toHaveLength(0);
      });

      describe('bill payment daily limit validation', () => {
        var transactionInput: ITransactionInput;
        var ruleConfigs: IPaymentConfigRule[];
        var limitGroups: ILimitGroup[];
        var usageCounters: UsageCounter[];

        beforeEach(() => {
          transactionInput = {
            ...createTransaction(),
            paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
            beneficiaryBankCodeChannel: BankChannelEnum.GOBILLS,
            sourceAccountType: 'MA',
            beneficiaryAccountType: 'ANY'
          };

          ruleConfigs = getPaymentConfigRulesData();
          limitGroups = getLimitGroupsData();

          usageCounters = [
            {
              limitGroupCode: 'L009',
              dailyAccumulationAmount: 99_900_000,
              monthlyAccumulationTransaction: 100_000_000
            }
          ];

          transactionInput.transactionAmount = 100_000;
          transactionInput.beneficiaryBankCodeChannel = BankChannelEnum.GOBILLS;
        });

        it('should pass bill payment validation when transaction amount + daily usage still within limit', async () => {
          const result = await transactionMapper.getPaymentServiceMappings(
            transactionInput,
            ruleConfigs,
            limitGroups,
            usageCounters
          );

          expect(result).toHaveLength(1);
        });

        it('should return empty if bill payment exceeds daily limit', async () => {
          // Given
          transactionInput.transactionAmount = 100_001; // Will exceed daily limit.
          // When
          const result = await transactionMapper.getPaymentServiceMappings(
            transactionInput,
            ruleConfigs,
            limitGroups,
            usageCounters
          );

          // Then
          expect(result).toHaveLength(0);
        });

        it('should not execute bill payment validation if flagged as so', async () => {
          const result = await transactionMapper.getPaymentServiceMappings(
            transactionInput,
            ruleConfigs,
            limitGroups,
            usageCounters
          );

          expect(result).toHaveLength(1);
        });

        it('should not execute bill payment validation when bank channel is not GOBILLS', async () => {
          transactionInput.paymentServiceType = PaymentServiceTypeEnum.WALLET;
          transactionInput.beneficiaryBankCodeChannel = BankChannelEnum.PARTNER;

          const result = await transactionMapper.getPaymentServiceMappings(
            transactionInput,
            ruleConfigs,
            limitGroups,
            usageCounters
          );

          expect(result).toHaveLength(1);
        });
      });
    });
  });

  describe('no auto route for business PSCs', () => {
    const createTransactionInput = (): ITransactionInput => {
      const transactionInput = {
        ...createTransaction(),
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY'
      };
      transactionInput.transactionAmount = 100000;
      return transactionInput;
    };

    const limitGroups = getLimitGroupCodes();
    const configRules = createConfigRules();

    let usageCounters;
    beforeEach(() => {
      jest.resetAllMocks();
      (configurationService.isBiFastEnabled as jest.Mock).mockResolvedValue(
        true
      );

      usageCounters = [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        }
      ];
    });

    it('should return the first matching service code if none is provided', async () => {
      //GIVEN
      const transactionInput = createTransactionInput();
      transactionInput.paymentServiceCode = undefined;

      //WHEN
      const result = await transactionMapper.getPaymentServiceMappings(
        transactionInput,
        configRules,
        limitGroups,
        usageCounters
      );

      //THEN
      // eslint-disable-next-line no-console
      console.log(result);
      expect(result).toHaveLength(1);
      expect(result[0].paymentServiceCode).toEqual('RTOL');
    });

    it('should do a forced matching for BIFAST_FOR_BUSINESS ', async () => {
      //GIVEN
      const transactionInput: ITransactionInput = {
        paymentServiceCode: 'BIFAST_FOR_BUSINESS',
        sourceAccountType: 'MBA',
        transactionAmount: 100000,
        beneficiaryAccountType: 'ANY',
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        beneficiarySupportedChannels: [BankChannelEnum.BIFAST],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        sourceBankCodeChannel: BankChannelEnum.INTERNAL
      };
      //WHEN:
      const result = await transactionMapper.getPaymentServiceMappings(
        transactionInput,
        configRules,
        limitGroups,
        usageCounters
      );

      //THEN
      expect(result).toHaveLength(1);
      expect(result[0].paymentServiceCode).toEqual('BIFAST_FOR_BUSINESS');
    });

    it('should fail a forced matching for BIFAST_FOR_BUSINESS if the daily limit is surpassed', async () => {
      //GIVEN
      const transactionInput: ITransactionInput = {
        paymentServiceCode: 'BIFAST_FOR_BUSINESS',
        sourceAccountType: 'MBA',
        transactionAmount: 100000,
        beneficiaryAccountType: 'ANY',
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        beneficiarySupportedChannels: [BankChannelEnum.BIFAST],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        sourceBankCodeChannel: BankChannelEnum.INTERNAL
      };
      //WHEN:
      const fn = async () => {
        await transactionMapper.getPaymentServiceMappings(
          transactionInput,
          configRules,
          limitGroups,
          [
            {
              limitGroupCode: 'L019',
              dailyAccumulationAmount: 249999999,
              monthlyAccumulationTransaction: 0
            }
          ]
        );
      };
      //THEN
      await expect(fn()).rejects.toThrow(
        new RuleConditionStatus(
          'The sum of the total daily amount plus the current transaction is: 250099999 which is beyond  the daily limit of BI Fast: 250000000. But you can try it tomorrow.',
          ERROR_CODE.RECOMMENDATION_OVER_DAILY_LIMIT_RETRY_TOMORROW
        ).toError()
      );
    });

    const okInput = {
      paymentServiceCode: 'BIFAST_FOR_BUSINESS',
      sourceAccountType: 'MBA',
      transactionAmount: 1000000,
      beneficiaryAccountType: 'ANY',
      beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
      beneficiarySupportedChannels: [BankChannelEnum.BIFAST],
      paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
      sourceBankCodeChannel: BankChannelEnum.INTERNAL
    };
    const testCases = [
      {
        input: { sourceAccountType: 'MA' },

        status: new RuleConditionStatus(
          'Source account type: MA does not match any of the required ones: MBA,SBA',
          ERROR_CODE.RECOMMENDATION_SOURCE_ACCOUNT_TYPE_MISMATCH
        )
      },
      {
        input: { transactionAmount: 1000000000 },
        status: new RuleConditionStatus(
          'transaction amount: 1000000000 is not in the expected range: [10000 , 250000000]',
          ERROR_CODE.RECOMMENDATION_TRANSACTION_AMOUNT_IS_OUT_OF_RANGE
        )
      },
      {
        input: { beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL },
        status: new RuleConditionStatus(
          'Beneficiary bank code channel check is failed. actual=INTERNAL expected=EXTERNAL',
          ERROR_CODE.RECOMMENDATION_BENEFICIARY_BANK_CODE_CHANNEL_MISMATCH
        )
      },
      {
        input: { beneficiarySupportedChannels: [BankChannelEnum.TOKOPEDIA] },
        status: new RuleConditionStatus(
          'BI Fast is Disabled or beneficiary bank does not support it.',
          ERROR_CODE.RECOMMENDATION_BIFAST_UNSUPPORTED
        )
      },
      {
        input: {
          paymentServiceType: PaymentServiceTypeEnum.BIBIT_INSTANT_REDEMPTION
        },
        status: new RuleConditionStatus(
          'Payment service type check is failed. actual=BIBIT_INSTANT_REDEMPTION expected=TRANSFER',
          ERROR_CODE.RECOMMENDATION_PAYMENT_SEVICE_TYPE_MISMATCH
        )
      },
      {
        input: { sourceBankCodeChannel: [BankChannelEnum.TOKOPEDIA] },
        status: new RuleConditionStatus(
          'Source bank code channel check is failed. actual=TOKOPEDIA expected=INTERNAL',
          ERROR_CODE.RECOMMENDATION_SOURCE_BANK_CODE_CHANNEL_MISMATCH
        )
      }
    ];

    it.each(testCases)('NoRecommendation: %#', async testCase => {
      const transactionInput = {
        ...okInput,
        ...testCase.input
      };
      const fn = async () => {
        await transactionMapper.getPaymentServiceMappings(
          transactionInput,
          configRules,
          limitGroups,
          usageCounters
        );
      };
      await expect(fn()).rejects.toThrow(testCase.status.toError());
    });

    it('should do a forced matching for RTOL_FOR_BUSINESS ', async () => {
      //GIVEN
      const transactionInput: ITransactionInput = {
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        paymentServiceCode: 'RTOL_FOR_BUSINESS',
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        transactionAmount: 100000,
        sourceAccountType: 'MBA',
        sourceBankCodeChannel: BankChannelEnum.INTERNAL
      };

      //WHEN:
      const result = await transactionMapper.getPaymentServiceMappings(
        transactionInput,
        configRules,
        limitGroups,
        usageCounters
      );

      //THEN
      expect(result).toHaveLength(1);
      expect(result[0].paymentServiceCode).toEqual('RTOL_FOR_BUSINESS');
    });

    it('should do a forced matching for SKN_FOR_BUSINESS ', async () => {
      //GIVEN
      const transactionInput: ITransactionInput = {
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        paymentServiceCode: 'SKN_FOR_BUSINESS',
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        transactionAmount: 100000,
        sourceAccountType: 'MBA',
        sourceBankCodeChannel: BankChannelEnum.INTERNAL
      };

      (configurationRepository.getWorkingTime as jest.Mock).mockResolvedValue(
        workingTimeData()
      );

      //WHEN:
      const result = await transactionMapper.getPaymentServiceMappings(
        transactionInput,
        configRules,
        limitGroups,
        usageCounters,
        new Date('2021-02-01T00:00:00.000Z')
      );

      //THEN
      expect(result).toHaveLength(1);
      expect(result[0].paymentServiceCode).toEqual('SKN_FOR_BUSINESS');
    });

    it('should fail a forced matching for SKN_FOR_BUSINESS due to invalid working time', async () => {
      //GIVEN
      const transactionInput: ITransactionInput = {
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        paymentServiceCode: 'SKN_FOR_BUSINESS',
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        transactionAmount: 100000,
        sourceAccountType: 'MBA',
        sourceBankCodeChannel: BankChannelEnum.INTERNAL
      };

      (configurationRepository.getWorkingTime as jest.Mock).mockResolvedValue(
        workingTimeData()
      );

      //WHEN:
      const fn = async () => {
        await transactionMapper.getPaymentServiceMappings(
          transactionInput,
          configRules,
          limitGroups,
          usageCounters,
          new Date('2021-01-01T012:00:00.000Z')
        );
      };
      //THEN
      await expect(fn()).rejects.toThrow(
        new RuleConditionStatus(
          'Transaction is rejected due to the working date and time rules. Try again tomorrow.',
          ERROR_CODE.RECOMMENDATION_REJECTED_BY_WORKING_DAY_RULES
        ).toError()
      );
    });

    it('should do a forced matching for RTGS_FOR_BUSINESS ', async () => {
      //GIVEN
      const transactionInput: ITransactionInput = {
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        paymentServiceCode: 'RTGS_FOR_BUSINESS',
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        transactionAmount: 100000001,
        sourceAccountType: 'MBA',
        sourceBankCodeChannel: BankChannelEnum.INTERNAL
      };

      (configurationRepository.getWorkingTime as jest.Mock).mockResolvedValue(
        workingTimeData()
      );

      //WHEN:
      const result = await transactionMapper.getPaymentServiceMappings(
        transactionInput,
        configRules,
        limitGroups,
        usageCounters,
        new Date('2021-02-01T00:00:00.000Z')
      );

      //THEN
      expect(result).toHaveLength(1);
      expect(result[0].paymentServiceCode).toEqual('RTGS_FOR_BUSINESS');
    });
  });
});
