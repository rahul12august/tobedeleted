import { MongoMemoryServer } from 'mongodb-memory-server';
import { ITransactionModel, TransactionModel } from '../transaction.model';
import { getTransactionModelFullData } from '../__mocks__/transaction.data';
import transactionRepository from '../transaction.repository';
import mongoose from 'mongoose';
import { TransactionStatus } from '../transaction.enum';
import { BankNetworkEnum, PaymentServiceTypeEnum } from '@dk/module-common';
import {
  IConfirmTransactionInput,
  TransactionListRequest
} from '../transaction.type';
import { mockConfirmTransactionRequest } from '../__mocks__/transactionConfirmation.data';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import moment from 'moment';
import faker from 'faker';
import { utilConstants } from '../../../api-automation/utils/constants';
import { ExecutionMethodEnum } from '../transaction.constant';

const dateUtil = require('../../common/dateUtils');

jest.mock('mongoose', () => {
  const mongoose = require.requireActual('mongoose');
  return new mongoose.Mongoose(); // new mongoose instance and connection for each test
});

const getCurrentDateInStringFormat = (increase?: boolean) => {
  let today = new Date();
  if (increase) {
    today.setDate(today.getDate() + 10);
  }
  let dd: any = today.getDate();

  let mm: any = today.getMonth() + 1;
  let yyyy = today.getFullYear();
  if (dd < 10) {
    dd = '0' + dd;
  }

  if (mm < 10) {
    mm = '0' + mm;
  }
  return yyyy + '-' + mm + '-' + dd;
};

describe('transaction repository', () => {
  let mongod: MongoMemoryServer;
  beforeAll(async () => {
    mongod = new MongoMemoryServer();
    const mongoDbUri = await mongod.getConnectionString();
    await mongoose.connect(mongoDbUri, {
      useNewUrlParser: true,
      useCreateIndex: true
    });
  });

  afterEach(async () => {
    expect.hasAssertions();
    await TransactionModel.deleteMany({});
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongod.stop();
  });

  describe('create method', () => {
    it('should create transaction successfully with customerId', async () => {
      // Given
      let transactionModel = getTransactionModelFullData();
      transactionModel.sourceCustomerId = 'customerId123';

      // When
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );

      // Then
      expect(createdTransactionModel.id).toBeDefined();
      expect(createdTransactionModel.sourceCustomerId).toEqual(
        transactionModel.sourceCustomerId
      );
    });

    it('should create transaction successfully', async () => {
      // Given
      const transactionModel = getTransactionModelFullData();

      // When
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );

      // Then
      expect(createdTransactionModel.id).toBeDefined();
    });

    it('should create transaction with blockingId field successfully', async () => {
      // Given
      const transactionModel = getTransactionModelFullData();
      transactionModel.blockingId = 'test-blocking-id';

      // When
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );

      // Then
      expect(createdTransactionModel.id).toBeDefined();
      expect(createdTransactionModel.blockingId).toEqual(
        transactionModel.blockingId
      );
    });
    it('should create transaction with promotionCode field successfully', async () => {
      // Given
      const transactionModel = getTransactionModelFullData();
      transactionModel.promotionCode = 'promotionCode123';

      // When
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );

      // Then
      expect(createdTransactionModel.id).toBeDefined();
      expect(createdTransactionModel.promotionCode).toEqual(
        transactionModel.promotionCode
      );
    });
    it('should create transaction successfully without beneficiaryCIF and beneficiaryAccountName', async () => {
      // Given
      const transactionModel = {
        ...getTransactionModelFullData(),
        beneficiaryCIF: null,
        beneficiaryAccountName: null
      };

      // When
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );

      // Then
      expect(createdTransactionModel).toMatchObject(transactionModel);
    });
  });

  describe('update transaction', () => {
    it('should update transaction successfully', async () => {
      // Given
      const transactionModel = getTransactionModelFullData();
      const createdTransaction = await transactionRepository.create(
        transactionModel
      );
      const interchangeFieldValue = BankNetworkEnum.ALTO;
      // When
      const updatedTransaction = await transactionRepository.update(
        createdTransaction.id,
        {
          status: TransactionStatus.DECLINED,
          interchange: interchangeFieldValue
        }
      );

      // Then
      expect(updatedTransaction.id).toBe(createdTransaction.id);
      expect(updatedTransaction.status).toEqual(TransactionStatus.DECLINED);
      expect(updatedTransaction.interchange).toEqual(interchangeFieldValue);
    });

    it('should throw error when transaction not found', async () => {
      // Given
      const transactionModel = getTransactionModelFullData();
      const createdTransaction = await transactionRepository.create(
        transactionModel
      );
      const interchangeFieldValue = BankNetworkEnum.ALTO;
      await TransactionModel.deleteMany({});
      // When
      let error;
      try {
        await transactionRepository.update(createdTransaction.id, {
          status: TransactionStatus.DECLINED,
          interchange: interchangeFieldValue
        });
      } catch (err) {
        error = err;
      }

      // Then
      expect(error).toBeDefined();
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toBe(ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION);
    });
  });

  describe('update multiple Transactions by referenceId', () => {
    it('should update status of multiple transactions', async () => {
      // Given
      const transactionModel1 = {
        ...getTransactionModelFullData(),
        referenceId: '1',
        status: TransactionStatus.SUCCEED
      };

      const transactionModel2 = {
        ...getTransactionModelFullData(),
        referenceId: '1',
        status: TransactionStatus.SUCCEED
      };

      const created1 = await transactionRepository.create(transactionModel1);

      await transactionRepository.create(transactionModel2);

      // When
      const result = await transactionRepository.updateTransactionsByReferenceId(
        '1',
        {
          status: TransactionStatus.REVERTED
        }
      );

      // Then
      expect(result).toBe(true);
      const updated1 = await transactionRepository.getByKey(created1.id);
      expect(updated1.status).toEqual(TransactionStatus.REVERTED);
    });
  });

  describe('getByKey', () => {
    it('should return transaction successfully by Id', async () => {
      // Given
      const transactionModel = getTransactionModelFullData();

      // When
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );

      //when
      const expected = await transactionRepository.getByKey(
        createdTransactionModel.id
      );

      //then
      expect(expected).toBeDefined();
      expect(expected).toMatchObject(createdTransactionModel);
    });
    it('should return transaction successfully by externalId', async () => {
      // Given
      const transactionModel = getTransactionModelFullData();
      transactionModel.externalId = 'externalId';
      // Whens
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );
      //when
      const expected = await transactionRepository.getByKey(
        transactionModel.externalId
      );
      //then
      expect(expected).toBeDefined();
      expect(expected).toMatchObject(createdTransactionModel);
    });
    it('should return null when id not found', async () => {
      // When
      const expected = await transactionRepository.getByKey('123');

      // Then
      expect(expected).toBeNull();
    });
  });

  describe('getByExternalId', () => {
    let createdDocument: ITransactionModel | undefined;
    beforeEach(async () => {
      const sampleTransactionDocument = getTransactionModelFullData();
      createdDocument = await transactionRepository.create(
        sampleTransactionDocument
      );
    });

    it('should return data when external-id is found', async () => {
      //when
      const expected = await transactionRepository.getByExternalId(
        createdDocument.externalId
      );

      //then
      expect(expected).toBeDefined();
      expect(expected).toMatchObject(createdDocument);
    });

    it('should return null when external-id is not found', async () => {
      //when
      const expected = await transactionRepository.getByExternalId(
        'dummy-key-12333'
      );

      //then
      expect(expected).toBeNull();
    });
  });

  describe('getIncomingNonRefunded', () => {
    let createdDocument: ITransactionModel | undefined;
    beforeEach(async () => {
      const sampleTransactionDocument = getTransactionModelFullData();
      createdDocument = await transactionRepository.create({
        ...sampleTransactionDocument,
        externalId: 'extrnlId_mmddyyyy_accNumber',
        beneficiaryAccountNo: 'accNumber'
      });
    });

    it('should return data when external-id and account regex is found', async () => {
      //when
      const expected = await transactionRepository.getIncomingNonRefunded(
        'extrnlId',
        'accNumber'
      );

      //then
      expect(expected).toBeDefined();
      expect(expected).toMatchObject(createdDocument);
    });

    it('should return null when external-id is not found', async () => {
      //when
      const expected = await transactionRepository.getIncomingNonRefunded(
        'ext-key-12333',
        'ext-acc-12333'
      );

      //then
      expect(expected).toBeNull();
    });
  });

  describe('getAtomeTxnTobeRefunded', () => {
    let createdDocument: ITransactionModel | undefined;
    beforeEach(async () => {
      const sampleTransactionDocument = getTransactionModelFullData();
      createdDocument = await transactionRepository.create({
        ...sampleTransactionDocument,
        thirdPartyOutgoingId: 'extrnlId',
        paymentServiceType: PaymentServiceTypeEnum.ATOME_PAYMENT
      });
    });

    it('should return data when outgoing-id is found', async () => {
      //when
      const expected = await transactionRepository.getAtomeTxnTobeRefunded(
        'extrnlId'
      );

      //then
      expect(expected).toBeDefined();
      expect(expected).toMatchObject(createdDocument);
    });

    it('should return null when outgoing-id is not found', async () => {
      //when
      const expected = await transactionRepository.getAtomeTxnTobeRefunded(
        'ext-key-12333'
      );

      //then
      expect(expected).toBeNull();
    });
  });

  describe('updateIfExists', () => {
    let createdDocument: ITransactionModel | undefined;
    let sampleTransactionDocument;
    beforeEach(async () => {
      sampleTransactionDocument = getTransactionModelFullData();
      createdDocument = await transactionRepository.create(
        sampleTransactionDocument
      );
    });
    const input: IConfirmTransactionInput = mockConfirmTransactionRequest();

    it('should return data when updateIfExists is found', async () => {
      const _input: IConfirmTransactionInput = { ...input };
      _input.externalId = createdDocument.thirdPartyOutgoingId;

      //when
      const expected = await transactionRepository.updateIfExists(_input);

      //then
      expect(expected).toBeDefined();
      expect(expected.beneficiaryBankName).toEqual(
        createdDocument.beneficiaryBankName
      );
    });

    it('should return null when getByThirdPartyOutgoingId is not found', async () => {
      //when
      const expected = await transactionRepository.updateIfExists(
        input,
        createdDocument.externalId
      );

      //then
      expect(expected).toBeDefined();
      expect(expected).toEqual(null);
    });

    it('should return null when updateIfExists is not found', async () => {
      //when
      const expected = await transactionRepository.updateIfExists(input);

      //then
      expect(expected).toBeNull();
    });

    it('should push add reason as empty if not initiated at saving', async () => {
      expect(createdDocument.reason).toEqual([]);
    });

    it('should push data reason when externalId is matched', async () => {
      //when
      const expected = await transactionRepository.updateIfExists(
        {
          ...input,
          externalId: createdDocument.thirdPartyOutgoingId
        },
        createdDocument.externalId
      );

      //then
      expect(expected).toBeDefined();
      expect(expected.reason[0].responseCode).toEqual(200);
    });

    it('should take record with correct externalId incase multiple records found', async () => {
      // Given
      const createdDocument2 = await transactionRepository.create({
        ...sampleTransactionDocument,
        externalId: '21238W002228_20210826_108800576581'
      });

      //when
      const expected = await transactionRepository.updateIfExists(
        {
          ...input,
          externalId: createdDocument2.thirdPartyOutgoingId,
          responseCode: 400
        },
        createdDocument2.externalId
      );

      //then
      expect(expected).toBeDefined();
      expect(expected.reason[0].responseCode).toEqual(400);
      expect(expected.externalId).toEqual('21238W002228_20210826_108800576581');
    });

    it('should take correct record incase multiple records found, only match externalId without evaluating secondaryExternalId', async () => {
      // Given
      const thirdPartyOutgoingId = 'thirdPartyOutgoingId';
      const sourceAccountNo = '108800576581';
      const beneficiaryAccountNo = '208800576581';
      const externalIdOfFirstRecord = `21238W002228_20210826_${sourceAccountNo}`;
      const externalIdOfSecondRecord = `21238W002228_20210826_${beneficiaryAccountNo}`;

      const result = await Promise.all([
        transactionRepository.create({
          ...sampleTransactionDocument,
          sourceAccountNo,
          beneficiaryAccountNo,
          externalId: externalIdOfFirstRecord,
          secondaryExternalId: externalIdOfSecondRecord,
          thirdPartyOutgoingId
        }),
        transactionRepository.create({
          ...sampleTransactionDocument,
          sourceAccountNo: beneficiaryAccountNo,
          beneficiaryAccountNo: 'GL account no',
          externalId: externalIdOfSecondRecord,
          secondaryExternalId: '21238W002228_20210826_GL account no',
          thirdPartyOutgoingId
        })
      ]);

      //when
      const expected = await transactionRepository.updateIfExists(
        {
          ...input,
          accountNo: beneficiaryAccountNo,
          externalId: thirdPartyOutgoingId,
          responseCode: 400
        },
        externalIdOfSecondRecord
      );

      //then
      expect(expected).toBeDefined();
      expect(expected.reason[0].responseCode).toEqual(400);
      expect(expected.externalId).toEqual(externalIdOfSecondRecord);
    });

    it('should take correct record incase multiple records found and evaluate secondaryExternalId', async () => {
      // Given
      const thirdPartyOutgoingId = 'thirdPartyOutgoingId';
      const sourceAccountNo = '108800576581';
      const beneficiaryAccountNo = '208800576581';
      const externalIdOfFirstRecord = `21238W002228_20210826_${sourceAccountNo}`;
      const externalIdOfSecondRecord = `21238W002228_20210826_${beneficiaryAccountNo}`;

      const result = await Promise.all([
        transactionRepository.create({
          ...sampleTransactionDocument,
          sourceAccountNo,
          beneficiaryAccountNo,
          externalId: externalIdOfFirstRecord,
          secondaryExternalId: externalIdOfSecondRecord,
          thirdPartyOutgoingId
        }),
        transactionRepository.create({
          ...sampleTransactionDocument,
          sourceAccountNo: beneficiaryAccountNo,
          beneficiaryAccountNo: 'GL account no',
          externalId: externalIdOfSecondRecord,
          secondaryExternalId: '21238W002228_20210826_',
          thirdPartyOutgoingId
        })
      ]);

      //when
      const expected = await transactionRepository.updateIfExists(
        {
          ...input,
          accountNo: '',
          externalId: thirdPartyOutgoingId,
          responseCode: 400
        },
        externalIdOfSecondRecord
      );

      //then
      expect(expected).toBeDefined();
      expect(expected.reason[0].responseCode).toEqual(400);
      expect(expected.externalId).toEqual(externalIdOfSecondRecord);
    });

    it('should return null if transaction is not of current day for reversal', async () => {
      // Given
      let createdAt = new Date();
      createdAt.setDate(createdAt.getDate() - 1);
      const createdDocument2 = await transactionRepository.create({
        ...sampleTransactionDocument,
        externalId: '21238W002228_20210826_108800576582',
        createdAt
      });

      //when
      const expected = await transactionRepository.updateIfExists(
        {
          ...input,
          externalId: createdDocument2.thirdPartyOutgoingId,
          responseCode: 400
        },
        createdDocument2.externalId
      );

      //then
      expect(expected).toBeDefined();
      expect(expected).toEqual(null);
    });

    it('should return data if transactions current date is UTC time with same date', async () => {
      // Given
      //Logic to convert UTC time
      var createdAt = new Date();
      createdAt.setDate(createdAt.getDate() - 1);
      createdAt.setHours(17, 59, 27, 899);
      var inputDate = createdAt.toLocaleString('en-US', {
        timeZone: 'Universal'
      });

      const spyCurrentDate = jest.spyOn(dateUtil, 'isCurrentDate');
      spyCurrentDate.mockImplementation(() => true);

      const createdDocument2 = await transactionRepository.create({
        ...sampleTransactionDocument,
        externalId: '21238W002228_20210826_108800576582',
        thirdPartyOutgoingId: '21238W002228_20210826_108800576582',
        createdAt: inputDate
      });

      //when
      const expected = await transactionRepository.updateIfExists(
        {
          ...input,
          externalId: createdDocument2.thirdPartyOutgoingId,
          responseCode: 400
        },
        createdDocument2.externalId
      );

      //then
      expect(expected).toBeDefined();
      expect(expected.beneficiaryBankName).toEqual(
        createdDocument.beneficiaryBankName
      );
      spyCurrentDate.mockRestore();
    });
  });

  describe('getTransactionListByCriteria', () => {
    it('should return transaction List successfully when transactionId is provided', async () => {
      // Given
      const transactionModel = getTransactionModelFullData();
      // When
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );
      const input: TransactionListRequest = {
        transactionId: createdTransactionModel.id
      };
      //when
      const expected = await transactionRepository.getTransactionListByCriteria(
        input
      );

      //then
      expect(expected).toBeDefined();
      expect(expected.list[0]).toMatchObject(createdTransactionModel);
    });

    it('should return transaction List successfully when customerId is provided', async () => {
      // Given
      const customerId = '23145678998';
      const transactionModel = {
        ...getTransactionModelFullData(),
        ownerCustomerId: customerId
      };

      // When
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );
      const input: TransactionListRequest = {
        customerId
      };
      //when
      const expected = await transactionRepository.getTransactionListByCriteria(
        input
      );

      //then
      expect(expected).toBeDefined();
      expect(expected.list[0]).toMatchObject(createdTransactionModel);
    });

    it('should return transaction List successfully when transactionAmount is provided', async () => {
      // Given
      const customerId = '23145678998';
      const transactionAmount = 60000;
      const transactionModel = {
        ...getTransactionModelFullData(),
        ownerCustomerId: customerId,
        transactionAmount: transactionAmount
      };

      // When
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );
      const input: TransactionListRequest = {
        customerId,
        transactionAmount
      };
      //when
      const expected = await transactionRepository.getTransactionListByCriteria(
        input
      );

      //then
      expect(expected).toBeDefined();
      expect(expected.list[0]).toMatchObject(createdTransactionModel);
    });

    it('should return transaction List successfully when accountNo is provided', async () => {
      // Given
      const customerId = '23145678998';
      const accountNo = '12334234233';
      const transactionModel = {
        ...getTransactionModelFullData(),
        ownerCustomerId: customerId,
        sourceAccountNo: accountNo
      };

      // When
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );
      const input: TransactionListRequest = {
        customerId,
        accountNo
      };
      //when
      const expected = await transactionRepository.getTransactionListByCriteria(
        input
      );

      //then
      expect(expected).toBeDefined();
      expect(expected.list[0]).toMatchObject(createdTransactionModel);
    });

    it('should return transaction List successfully when cif is provided', async () => {
      // Given
      const customerId = '23145678998';
      const transactionModel = {
        ...getTransactionModelFullData(),
        sourceCIF: 'cif'
      };

      // When
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );
      const input: TransactionListRequest = {
        customerId
      };
      //when
      const expected = await transactionRepository.getTransactionListByCriteria(
        input,
        'cif'
      );

      //then
      expect(expected).toBeDefined();
      expect(expected.list[0]).toMatchObject(createdTransactionModel);
    });

    it('should return empty transaction List successfully when no record found', async () => {
      // Given
      const transactionModel = getTransactionModelFullData();
      // When
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );
      const input: TransactionListRequest = {
        transactionId: createdTransactionModel.id,
        sourceBankCode: '123344'
      };
      //when
      const expected = await transactionRepository.getTransactionListByCriteria(
        input
      );

      //then
      expect(expected).toBeDefined();
      expect(expected.list).toEqual([]);
    });

    it('should return empty transaction List successfully when no record found', async () => {
      // Given
      const transactionModel = getTransactionModelFullData();
      // When
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );
      const input: TransactionListRequest = {
        sourceBankCode: createdTransactionModel.sourceBankCode,
        beneficiaryBankCode: createdTransactionModel.beneficiaryBankCode,
        beneficiaryAccountNo: createdTransactionModel.beneficiaryAccountNo,
        sourceAccountNo: createdTransactionModel.sourceAccountNo,
        startDate: getCurrentDateInStringFormat(),
        endDate: getCurrentDateInStringFormat(true),
        transactionStatus: [TransactionStatus.PENDING]
      };
      //when
      const expected = await transactionRepository.getTransactionListByCriteria(
        input,
        createdTransactionModel.sourceCIF
      );

      //then
      expect(expected).toBeDefined();
      expect(expected.list[0]).toMatchObject(createdTransactionModel);
    });
  });

  describe('findByIdAndUpdate', () => {
    it('should return transaction when transactionId is provided', async () => {
      // Given
      const transactionModel = getTransactionModelFullData();
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );

      //When
      const expected = await transactionRepository.findByIdAndUpdate(
        createdTransactionModel.id,
        { status: TransactionStatus.SUCCEED }
      );

      //then
      expect(expected).toBeDefined();
      expect(expected.status).toEqual(TransactionStatus.SUCCEED);
    });
  });

  describe('getTransactionByCriteria', () => {
    it('should return transactionModel when customerId, paymentInstructionId and transactionDate is provided', async () => {
      // Given
      const transactionDate = new Date();
      const customerId = '123';
      const paymentInstructionId = '123zxc';
      const transactionModel = {
        ...getTransactionModelFullData(),
        ownerCustomerId: customerId,
        createdAt: transactionDate,
        paymentInstructionId
      };
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );
      const input = {
        customerId,
        transactionDate,
        paymentInstructionId
      };

      // When
      const result = await transactionRepository.getTransactionStatusByCriteria(
        input
      );

      // Expect
      expect(result).toBeDefined();
      expect(result).toMatchObject(createdTransactionModel);
    });

    it('should return transactionModel when customerId and paymentInstructionId is provided', async () => {
      // Given
      const customerId = '123';
      const paymentInstructionId = '123zxc';
      const transactionModel = {
        ...getTransactionModelFullData(),
        ownerCustomerId: customerId,
        createdAt: new Date(),
        paymentInstructionId
      };
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );
      const input = {
        customerId,
        paymentInstructionId
      };

      // When
      const result = await transactionRepository.getTransactionStatusByCriteria(
        input
      );

      // Expect
      expect(result).toBeDefined();
      expect(result).toMatchObject(createdTransactionModel);
    });

    it('should return undefined when fetching transactionModel using customerId and paymentInstructionId got no result', async () => {
      // Given
      const transactionDate = new Date();
      const customerId = '123';
      const paymentInstructionId = '123zxc';
      const input = {
        customerId,
        transactionDate,
        paymentInstructionId
      };

      // When
      const result = await transactionRepository.getTransactionStatusByCriteria(
        input
      );

      // Expect
      expect(result).toBeUndefined();
    });

    it('should return transactionModel when fetching transactionModel using customerId and paymentInstructionId got more than 1 result', async () => {
      // Given
      const transactionDate = moment.utc().toDate();
      const customerId = '123';
      const paymentInstructionId = '123zxc';
      const input = {
        customerId,
        transactionDate,
        paymentInstructionId
      };
      const transactionModel1 = {
        ...getTransactionModelFullData(),
        ownerCustomerId: customerId,
        createdAt: transactionDate,
        paymentInstructionId
      };
      const transactionModel2 = {
        ...getTransactionModelFullData(),
        ownerCustomerId: customerId,
        createdAt: moment
          .utc()
          .subtract(1, 'day')
          .toDate(),
        paymentInstructionId
      };
      await transactionRepository.create(transactionModel2);
      await transactionRepository.create(transactionModel1);

      // When
      const result = await transactionRepository.getTransactionStatusByCriteria(
        input
      );

      // Then
      expect(result).toBeDefined();
      expect(result).toMatchObject(transactionModel2);
    });

    it('should return undefined when fetching transactionModel using correct customerId and paymentInstructionId but transactionDate is outrange the createdAt', async () => {
      // Given
      const transactionDate = new Date('2022-04-10');
      const customerId = '123';
      const paymentInstructionId = '123zxc';
      const input = {
        customerId,
        transactionDate,
        paymentInstructionId
      };
      const transactionModel = {
        ...getTransactionModelFullData(),
        ownerCustomerId: customerId,
        createdAt: new Date('2022-04-8'),
        paymentInstructionId
      };
      await transactionRepository.create(transactionModel);

      // When
      const result = await transactionRepository.getTransactionStatusByCriteria(
        input
      );

      // Then
      expect(result).toBeUndefined();
    });
  });

  describe('getAllByExternalId', () => {
    it('should return list of transaction when externalId is provided', async () => {
      // Given
      const transactionModel = getTransactionModelFullData();
      const createdTransactionModel = await transactionRepository.create(
        transactionModel
      );

      //When
      const expected = await transactionRepository.getAllByExternalId(
        createdTransactionModel.externalId
      );

      //then
      expect(expected).toBeDefined();
      expect(expected[0]).toMatchObject(createdTransactionModel);
    });
  });

  describe('getPendingTransactionList', () => {
    // Given
    const tasks: Promise<ITransactionModel>[] = [];
    const outOfBanTxAgeMin = 30;
    const outOfBanTxAgeMax = 10080; // 7 days
    const qrisValidRecordsNum = 4;
    const qrisInsufficientRecordsNum = 3;
    const qrisTooOldRecordsNum = 2;
    const bifastValidRecordsNum = 1;
    const newTransactionAge = 20;
    const oldTransactionAge = 10081; // 7 days + 1 min
    const recordLimit = 1000;

    beforeEach(async () => {
      // QRIS with sufficient age
      for (let i = 0; i < qrisValidRecordsNum; i++) {
        tasks.push(
          new TransactionModel({
            ...getTransactionModelFullData(),
            paymentServiceCode: utilConstants.paymentServiceCode.QRIS_PAYOUTS,
            status: faker.random.arrayElement([
              TransactionStatus.SUBMITTED,
              TransactionStatus.PENDING
            ]),
            createdAt: moment
              .utc()
              .subtract(outOfBanTxAgeMin, 'minutes')
              .toDate()
          }).save({ timestamps: false })
        );
      }

      // QRIS with insufficient age
      for (let i = 0; i < qrisInsufficientRecordsNum; i++) {
        tasks.push(
          new TransactionModel({
            ...getTransactionModelFullData(),
            paymentServiceCode: utilConstants.paymentServiceCode.QRIS_PAYOUTS,
            status: faker.random.arrayElement([
              TransactionStatus.SUBMITTED,
              TransactionStatus.PENDING
            ]),
            createdAt: moment
              .utc()
              .subtract(newTransactionAge, 'minutes')
              .toDate()
          }).save({ timestamps: false })
        );
      }

      // QRIS too old transactions
      for (let i = 0; i < qrisTooOldRecordsNum; i++) {
        tasks.push(
          new TransactionModel({
            ...getTransactionModelFullData(),
            paymentServiceCode: utilConstants.paymentServiceCode.QRIS_PAYOUTS,
            status: faker.random.arrayElement([
              TransactionStatus.SUBMITTED,
              TransactionStatus.PENDING
            ]),
            createdAt: moment
              .utc()
              .subtract(oldTransactionAge, 'minutes')
              .toDate()
          }).save({ timestamps: false })
        );
      }

      // BIFAST with sufficient age
      for (let i = 0; i < bifastValidRecordsNum; i++) {
        tasks.push(
          new TransactionModel({
            ...getTransactionModelFullData(),
            paymentServiceCode: faker.random.arrayElement([
              utilConstants.paymentServiceCode.BIFAST_OUTGOING,
              utilConstants.paymentServiceCode.BIFAST_OUTGOING_SHARIA
            ]),
            status: faker.random.arrayElement([
              TransactionStatus.SUBMITTED,
              TransactionStatus.PENDING
            ]),
            createdAt: moment
              .utc()
              .subtract(outOfBanTxAgeMin, 'minutes')
              .toDate()
          }).save({ timestamps: false })
        );
      }

      // create all records
      await Promise.all(tasks);
    });

    it('should return list of QRIS pending transactions', async () => {
      //When
      const qrisOutOfBanTransactions = await transactionRepository.getPendingTransactionList(
        [utilConstants.paymentServiceCode.QRIS_PAYOUTS],
        outOfBanTxAgeMin,
        outOfBanTxAgeMax,
        recordLimit
      );

      //then
      expect(qrisOutOfBanTransactions).toBeDefined();
      expect(qrisOutOfBanTransactions.length).toBe(qrisValidRecordsNum);
    });

    it('should return list of BIFAST pending transactions', async () => {
      //When
      const bifastOutOfBanTransactions = await transactionRepository.getPendingTransactionList(
        [
          utilConstants.paymentServiceCode.BIFAST_OUTGOING,
          utilConstants.paymentServiceCode.BIFAST_OUTGOING_SHARIA
        ],
        outOfBanTxAgeMin,
        outOfBanTxAgeMax,
        recordLimit
      );

      //then
      expect(bifastOutOfBanTransactions).toBeDefined();
      expect(bifastOutOfBanTransactions.length).toBe(bifastValidRecordsNum);
    });
  });

  describe('findTransactionByQuery', () => {
    it('should return list of transactions with required projections', async () => {
      // Given
      await transactionRepository.create(getTransactionModelFullData());
      const query = { sourceCustomerId: 'customer123' };
      const projections = { transactionAmount: 1, status: 1 };

      // When
      const refundTransactions = await transactionRepository.findTransactionByQuery(
        query,
        projections
      );

      // Then
      expect(refundTransactions).toBeDefined();
      expect(refundTransactions.length).toBe(1);
    });

    it('should return empty list if no transactions found', async () => {
      // Given
      await transactionRepository.create(getTransactionModelFullData());
      const query = { sourceCustomerId: 'customer' };
      const projections = { transactionAmount: 1, status: 1 };

      // When
      const refundTransactions = await transactionRepository.findTransactionByQuery(
        query,
        projections
      );

      // Then
      expect(refundTransactions).toBeDefined();
      expect(refundTransactions.length).toBe(0);
    });
  });

  describe('createIfNotExist method', () => {
    it('should create transaction successfully with customerId', async () => {
      // Given
      let transactionModel = getTransactionModelFullData();
      transactionModel.sourceCustomerId = 'customerId123';

      TransactionModel.findOneAndUpdate = jest.fn().mockResolvedValueOnce(null);

      // When
      const createdTransactionModel = await transactionRepository.createIfNotExist(
        transactionModel
      );

      // Then
      expect(createdTransactionModel.id).toBeDefined();
      expect(createdTransactionModel.sourceCustomerId).toEqual(
        transactionModel.sourceCustomerId
      );
      expect(createdTransactionModel.createdAt).toBeDefined();
      expect(createdTransactionModel.updatedAt).toBeDefined();

      const findOneAndUpdateOptions = (TransactionModel.findOneAndUpdate as jest.Mock)
        .mock.calls[0][2];
      expect(findOneAndUpdateOptions).toEqual({
        upsert: true,
        timestamps: false
      });
    });

    it('should throw UNIQUE_FIELD error for duplicate keys', async () => {
      // Given
      let transactionModel = getTransactionModelFullData();
      transactionModel.sourceCustomerId = 'customerId123';

      TransactionModel.findOneAndUpdate = jest
        .fn()
        .mockResolvedValueOnce(transactionModel);

      // When
      let error;
      try {
        await transactionRepository.createIfNotExist(transactionModel);
      } catch (e) {
        error = e;
      }

      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toBe(ERROR_CODE.DUPLICATE_EXTERNAL_ID);
    });

    it('should create transaction successfully with processingInfo and workflowId', async () => {
      // Given
      let transactionModel = {
        ...getTransactionModelFullData(),
        processingInfo: {
          paymentRail: 'paymentRailValue',
          channel: 'chanelValue',
          method: ExecutionMethodEnum.ONE_BY_ONE,
          partner: 'partnerValue'
        },
        workflowId: 'workflowIdValue'
      };

      TransactionModel.findOneAndUpdate = jest.fn().mockResolvedValueOnce(null);

      // When
      const createdTransactionModel = await transactionRepository.createIfNotExist(
        transactionModel
      );

      // Then
      expect(createdTransactionModel).toMatchObject({
        workflowId: 'workflowIdValue',
        processingInfo: {
          paymentRail: 'paymentRailValue',
          channel: 'chanelValue',
          method: ExecutionMethodEnum.ONE_BY_ONE,
          partner: 'partnerValue'
        }
      });
    });

    it('should create transaction successfully with processingInfo and correlationId', async () => {
      // Given
      let transactionModel = {
        ...getTransactionModelFullData(),
        processingInfo: {
          paymentRail: 'paymentRailValue',
          channel: 'chanelValue',
          method: ExecutionMethodEnum.ONE_BY_ONE,
          partner: 'partnerValue'
        },
        correlationId: 'correlationIdValue'
      };

      TransactionModel.findOneAndUpdate = jest.fn().mockResolvedValueOnce(null);

      // When
      const createdTransactionModel = await transactionRepository.createIfNotExist(
        transactionModel
      );

      // Then
      expect(createdTransactionModel).toMatchObject({
        correlationId: 'correlationIdValue',
        processingInfo: {
          paymentRail: 'paymentRailValue',
          channel: 'chanelValue',
          method: ExecutionMethodEnum.ONE_BY_ONE,
          partner: 'partnerValue'
        }
      });
    });
  });
});
