import transactionRepository from '../transaction.repository';
import {
  ConfirmTransactionRequest,
  ConfirmTransactionResponse
} from '../transaction.type';
import {
  mockConfirmTransactionRequest,
  mockTransactionFromDb
} from '../__mocks__/transactionConfirmation.data';
import { ITransactionModel } from '../transaction.model';
import transactionConfirmationHelper from '../transactionConfirmation.helper';

jest.mock('../transaction.repository');

describe('transactionConfirmation.helper', () => {
  const input: ConfirmTransactionRequest = mockConfirmTransactionRequest();
  const expectedResponse: ConfirmTransactionResponse = {
    transactionResponseID: input.transactionResponseID
  };
  const mockedTransactionFromDb: ITransactionModel = {
    ...mockTransactionFromDb(),
    beneficiaryAccountNo: input.accountNo,
    thirdPartyOutgoingId: input.externalId
  };
  describe('updateRefundAmount', () => {
    it('should update remaining amount of original transaction', async () => {
      // Given
      const txId = '1234';
      const refundAmount = 10000;

      (transactionRepository.getByKey as jest.Mock).mockResolvedValue({
        ...mockedTransactionFromDb,
        refund: {
          remainingAmount: 40000,
          transactions: ['5678']
        }
      });

      (transactionRepository.update as jest.Mock).mockResolvedValueOnce(null);
      // When
      const response = await transactionConfirmationHelper.updateRefundAmount(
        txId,
        refundAmount
      );

      // Then
      expect(response).toBeUndefined();
      expect(transactionRepository.getByKey).toHaveBeenCalledWith(txId);
      expect(transactionRepository.update).toHaveBeenCalledWith(
        mockedTransactionFromDb.id,
        {
          ...mockedTransactionFromDb,
          refund: {
            remainingAmount: 50000,
            transactions: ['5678']
          }
        }
      );
    });
  });
});
