import hapi from '@hapi/hapi';
import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import {
  ILimitGroup,
  IPaymentConfigRule,
  IBankCode,
  IPaymentServiceMapping
} from '../configuration/configuration.type';
import {
  recommendPaymentServiceRequestValidator,
  createDepositTransactionRequestValidator,
  createTransferTransactionRequestValidator,
  reverseRequestValidator,
  transactionIdValidator,
  transactionInfoResponseValidator,
  transactionWithRefundValidator,
  createWithdrawTransactionRequestValidator,
  transactionResponseValidator,
  blockingTransactionAdjustmentRequestValidator,
  cifValidator,
  topupInternalTransactionRequestPayload,
  topupExternalTransactionRequestPayload,
  confirmTransactionRequestValidator,
  confirmTransactionResponseValidator,
  externalTransactionRequestValidator,
  incomingFeeCheckResponseValidator,
  externalTransactionResponseValidator,
  externalTransactionFeeRequestValidator,
  refundTransactionRequestValidator,
  refundTransactionResponseValidator,
  transactionListRequestValidator,
  refundTransactionQueryValidator,
  transactionStatusResponseValidator,
  transactionStatusRequestValidator,
  confirmTransactionQueryValidator,
  bulkTransactionFeesRequestValidator,
  bulkTransactionFeesResponseValidator
} from './transaction.validator';
import { IFeeAmount } from '../fee/fee.type';
import {
  Rail,
  BankChannelEnum,
  BankNetworkEnum,
  InstitutionTypeEnum,
  PaymentServiceTypeEnum,
  AuthenticationType
} from '@dk/module-common';
import {
  TransactionStatus,
  TransferJourneyStatusEnum
} from './transaction.enum';
import { ErrorDetails } from '../errors/AppError';
import {
  ITransactionModel,
  IPaymentInstructionExecutionModel,
  IRefund,
  IEntitlementInfo
} from './transaction.model';
import { ExecutionMethodEnum } from './transaction.constant';
import { ERROR_CODE } from '../common/errors';
import { TransferAppError } from '../errors/AppError';
import { TransferFailureReasonActor } from './transaction.enum';

export interface IBlockingTransaction extends ITransactionModel {
  sourceCIF: string;
  sourceAccountNo: string;
  blockingId: string;
  cardId: string;
  idempotencyKey?: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ITransactionInfoByInterchange
  extends Pick<
    ITransactionModel,
    | 'debitTransactionCode'
    | 'debitTransactionChannel'
    | 'debitLimitGroupCode'
    | 'debitAwardGroupCounter'
    | 'creditTransactionCode'
    | 'creditTransactionChannel'
    | 'fees'
    | 'categoryCode'
  > {}

export type TransferTransactionPayload = Joi.extractType<
  typeof createTransferTransactionRequestValidator
>;

export interface ITransferTransactionInput extends TransferTransactionPayload {
  sourceCustomerId?: string;
  paymentInstructionExecution?: IPaymentInstructionExecutionModel;
}

export type CreateDepositTransactionPayload = Joi.extractType<
  typeof createDepositTransactionRequestValidator
>;

export interface IDepositTransactionInput
  extends CreateDepositTransactionPayload {
  sourceCustomerId?: string;
}

export type CreateWithdrawTransactionPayload = Joi.extractType<
  typeof createWithdrawTransactionRequestValidator
>;

export interface IWithdrawTransactionInput
  extends CreateWithdrawTransactionPayload {
  sourceCustomerId?: string;
}

export interface IRecommendPaymentServiceRequest extends hapi.Request {
  payload: RecommendPaymentServiceInput;
}

export interface ICreateTransactionRequest extends hapi.Request {
  query: { customerId: string };
  payload: TransferTransactionPayload;
}

export type TopupInternalTransactionPayload = Joi.extractType<
  typeof topupInternalTransactionRequestPayload
>;
export interface ITopupInternalTransactionInput
  extends TopupInternalTransactionPayload {
  sourceCustomerId?: string;
  paymentInstructionExecution?: IPaymentInstructionExecutionModel;
}
export interface ITopupInternalTransactionPayload extends hapi.Request {
  query: { customerId: string };
  payload: TopupInternalTransactionPayload;
}

export type TopupExternalTransactionPayload = Joi.extractType<
  typeof topupExternalTransactionRequestPayload
>;

export interface ITopupExternalTransactionPayload extends hapi.Request {
  payload: TopupExternalTransactionPayload;
}

export interface ICreateDepositTransactionRequest extends hapi.Request {
  query: { customerId: string };
  payload: CreateDepositTransactionPayload;
}

export interface ICreateWithdrawTransactionRequest extends hapi.Request {
  query: { customerId: string };
  payload: CreateWithdrawTransactionPayload;
}

export type RecommendPaymentServiceInput = Joi.extractType<
  typeof recommendPaymentServiceRequestValidator
>;

export interface DailyUsages {
  rtolDailyUsage: number;
  sknDailyUsage: number;
  rtgsDailyUsage: number;
  billDailyUsage: number;
  bifastDailyUsage: number;
}

export interface ExecutionTime {
  date: Date;
}
export interface ILimitGroupCodesMap {
  [key: string]: ILimitGroup;
}

export interface ITransferJourney {
  status: TransferJourneyStatusEnum;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface ITransaction
  extends Partial<Omit<ITransferTransactionInput, 'paymentServiceType'>> {
  referenceId?: string; // it's not need for principle transfer
  paymentRequestID?: string;
  sourceCIF?: string;
  sourceAccountType?: string;
  sourceAccountNo?: string;
  sourceAccountName?: string;
  sourceBankCode?: string;
  sourceRtolCode?: string;
  sourceRemittanceCode?: string;
  sourceBankCodeChannel?: BankChannelEnum;
  sourceTransactionCurrency?: string;
  transactionAmount: number;
  thirdPartyOutgoingId?: string;
  beneficiaryCIF?: string;
  beneficiaryBankCode?: string;
  beneficiaryAccountType?: string; // it's optional if beneficiary is external
  beneficiaryAccountNo?: string;
  beneficiaryAccountName?: string;
  beneficiaryBankName?: string;
  beneficiaryBankCodeChannel?: BankChannelEnum;
  beneficiaryRtolCode?: string;
  beneficiaryRemittanceCode?: string;
  beneficiaryIrisCode?: string;
  debitPriority?: BankNetworkEnum;
  debitLimitGroupCode?: string;
  creditPriority?: BankNetworkEnum;
  institutionalType?: InstitutionTypeEnum;
  note?: string;
  categoryCode?: string; // if category code is null, its value is get from default category code based on transaction code
  paymentServiceType: PaymentServiceTypeEnum;
  billerCode?: string;
  additionalInformation1?: string;
  additionalInformation2?: string;
  additionalInformation3?: string;
  additionalInformation4?: string;
  blockingId?: string;
  interchange?: BankNetworkEnum;
  externalTransactionDate?: string;
  externalId?: string;
  secondaryExternalId?: string;
  sourceCustomerId?: string;
  beneficiaryCustomerId?: string;
  debitAwardGroupCounter?: string;
  journey?: ITransferJourney[];
  additionalPayload?: Record<string, unknown>;
  beneficiarySupportedChannels?: BankChannelEnum[];
  sourceIdNumber?: string;
  sourceCustomerType?: string;
  sourceAccountTypeBI?: string;
  preferedBankChannel?: BankChannelEnum;
  processingInfo?: IProcessingInfo;
}

export interface IProcessingInfo {
  channel?: string;
  method?: ExecutionMethodEnum;
  partner?: string;
}

export interface IRefundInput {
  transactionId?: string;
  externalId?: string;
  fee?: boolean;
}

export interface IRefundInput {
  transactionId?: string;
  externalId?: string;
  fee?: boolean;
}
export interface ITransactionInput extends ITransaction {
  paymentServiceCode?: string;
  sourceBankCodeInfo?: IBankCode;
  beneficiaryBankCodeInfo?: IBankCode;
  sourceAccountInfo?: ITransactionAccount;
  beneficiaryAccountInfo?: ITransactionAccount;
  executorCIF?: string;
  beneficiaryAccountRole?: string;
  sourceAccountRole?: string;
  destinationCIF?: string;
  moverRemainingDailyUsage?: number;
  refund?: IRefund;
  debitAwardGroupCounter?: string;
  paymentInstructionId?: string;
  transactionId?: string;
  generalRefund?: IRefundInput;
  ownerCustomerId?: string;
  entitlementInfo?: IEntitlementInfo[];
  tranType?: string;
}

export interface IBiFastEnabled {
  isBiFastEnabled?: boolean;
  isShariaCustomer?: boolean;
}
export class RuleConditionStatus {
  message: string;
  errorCode?: ERROR_CODE;

  constructor(message: string, errorCode?: ERROR_CODE) {
    this.message = message;
    this.errorCode = errorCode;
  }

  isSuccess = () => {
    return !this.errorCode;
  };

  toError() {
    if (this.isSuccess()) {
      throw new TransferAppError(
        'toError cannot be called when status is success.',
        TransferFailureReasonActor.MS_TRANSFER,
        ERROR_CODE.UNEXPECTED_ERROR
      );
    }
    const code = this.errorCode || ERROR_CODE.UNEXPECTED_ERROR;
    return new TransferAppError(
      this.message,
      TransferFailureReasonActor.MS_TRANSFER,
      code,
      [
        {
          message: this.message,
          key: code,
          code: code
        }
      ]
    );
  }
}
export interface FilteredMappingResult {
  status: RuleConditionStatus;
  mappings: IPaymentServiceMapping[];
}

export type RuleConditionFn = (
  input: ITransaction & DailyUsages & ExecutionTime & IBiFastEnabled,
  paymentConfigRule: IPaymentConfigRule,
  limitGroupCodesMap: ILimitGroupCodesMap
) => RuleConditionStatus;

export interface RecommendationService extends IPaymentServiceMapping {
  beneficiaryBankCode?: string;
  feeData?: IFeeAmount[];
  authenticationType?: AuthenticationType;
}

export interface ITransactionLimitAmount {
  transactionCode?: string;
  transactionMinAmount: number;
  transactionMaxAmount: number;
}
export type ReverseTransactionInput = Joi.extractType<
  typeof reverseRequestValidator
>;

export interface IReverseTransactionRequest extends hapi.Request {
  params: Joi.extractType<typeof transactionIdValidator>;
  payload: ReverseTransactionInput;
}

export interface ITransactionConfiguration {
  readonly limitGroupCodes: ILimitGroup[];
  readonly paymentConfigRules: IPaymentConfigRule[];
}

export interface ITransactionAccount {
  accountNumber?: string;
  accountType?: string;
  accountName?: string;
  currency?: string;
  bankCode?: string;
  cif?: string;
  customerId?: string;
  role?: string;
  moverDailyUsage?: number;
  moverLimit?: number;
  moverRemainingDailyUsage?: number;
  status?: string;
  beneficiaryProxyType?: string;
  beneficiaryAccountType?: string;
  preferredChannel?: BankChannelEnum;
}

export type TransactionDetail = Joi.extractType<
  typeof transactionInfoResponseValidator
>;

export type IRefundTransactionBasicDetail = Joi.extractType<
  typeof transactionWithRefundValidator
>;

export interface ITransactionRequest extends hapi.Request {
  params: {
    transactionId: string;
  };
}

export type ITransactionResponseValidator = Joi.extractType<
  typeof transactionResponseValidator
>;

export type IExternalTransactionResponse = Joi.extractType<
  typeof externalTransactionResponseValidator
>;

export interface ITransactionError {
  details?: ErrorDetails[];
  errorCode: string;
}

export type BlockingTransactionAdjustmentInput = Joi.extractType<
  typeof blockingTransactionAdjustmentRequestValidator
>;

export interface IBlockingAmountTransactionRequest extends hapi.Request {
  query: Joi.extractType<typeof cifValidator>;
  params: Joi.extractType<typeof transactionIdValidator>;
  payload: BlockingTransactionAdjustmentInput;
}

export interface IExecuteBlockingTransactionRequest extends hapi.Request {
  query: Joi.extractType<typeof cifValidator>;
  params: Joi.extractType<typeof transactionIdValidator>;
  payload: BlockingTransactionAdjustmentInput;
}
export interface ICancelBlockingTransactionRequest extends hapi.Request {
  query: Joi.extractType<typeof cifValidator>;
  params: Joi.extractType<typeof transactionIdValidator>;
}

export interface IManualUnBlockingTransactionRequest extends hapi.Request {
  params: Joi.extractType<typeof transactionIdValidator>;
}

export interface ITransactionExecutionHooks {
  onFailedTransaction?: (model: ITransactionModel) => Promise<void>;

  onRecommendedServiceCode?: (code: RecommendationService) => Promise<void>;

  onSucceedTransaction?: (model: ITransactionModel) => Promise<void>;

  onNewCreatedTransaction?: (model: ITransactionModel) => Promise<void>;
}

export type ConfirmTransactionRequest = Joi.extractType<
  typeof confirmTransactionRequestValidator
>;
export type ConfirmTransactionResponse = Joi.extractType<
  typeof confirmTransactionResponseValidator
>;

export type ConfirmTransactionQueryInput = Joi.extractType<
  typeof confirmTransactionQueryValidator
>;

export interface IConfirmTransactionRequest extends hapi.Server {
  query: ConfirmTransactionQueryInput;
  payload: ConfirmTransactionRequest;
}

export type ExternalTransactionInput = Joi.extractType<
  typeof externalTransactionRequestValidator
>;

export type BaseExternalTransactionInput = Joi.extractType<
  typeof externalTransactionFeeRequestValidator
>;

export interface IIncomingTransactionRequest extends hapi.Request {
  query: {
    includeCoreBankingTransactions: string;
  };
  payload: ExternalTransactionInput;
}
export interface IIncomingTransactionFeeRequest extends hapi.Server {
  payload: BaseExternalTransactionInput;
}
export type IncomingFeeCheckResponse = Joi.extractType<
  typeof incomingFeeCheckResponseValidator
>;

export type RefundTransactionInput = Joi.extractType<
  typeof refundTransactionRequestValidator
>;

export type RefundTransactionQueryInput = Joi.extractType<
  typeof refundTransactionQueryValidator
>;

export interface IIncomingRefundTransactionRequest extends hapi.Server {
  params: RefundTransactionInput;
  query: RefundTransactionQueryInput;
}

export type IRefundTransactionResponse = Joi.extractType<
  typeof refundTransactionResponseValidator
>;

export interface ITransactionSubmissionTemplate {
  submitTransaction(model: ITransactionModel): Promise<ITransactionModel>;
  isEligible(model: ITransactionModel): boolean;
  getRail?(): Rail;
  declineOnSubmissionFailure?: boolean;
  shouldSendNotification?(): boolean;
}

export interface ITransactionStatusTemplate {
  getTransactionStatus(
    model: ITransactionModel
  ): Promise<ITransactionStatusResponse>;
  isEligible(model: ITransactionModel): boolean;
  shouldSendNotification?(): boolean;
}

export interface IConfirmTransactionInput {
  externalId: string;
  accountNo?: string; // optional if externalId is thirdPartyOutgoingId
  transactionDate?: string;
  responseCode: number;
  interchange?: BankNetworkEnum;
  additionalInformation1?: string;
  additionalInformation2?: string;
  additionalInformation3?: string;
  additionalInformation4?: string;
  transactionResponseID?: string;
  amount?: number;
  responseMessage?: string;
}

export type TransactionListRequest = Joi.extractType<
  typeof transactionListRequestValidator
>;

export interface ITransactionListRequest extends hapi.Request {
  payload: TransactionListRequest;
}

export interface IExtTxnManualAdjstmentRequest extends hapi.Request {
  payload: {
    transactionId: string;
    status: TransactionStatus;
    interchange: BankNetworkEnum;
  };
}

export type TransactionStatusInput = Joi.extractType<
  typeof transactionStatusRequestValidator
>;

export interface ITransactionStatusRequest extends hapi.Request {
  payload: TransactionStatusInput;
}

export type ITransactionStatusResponse = Joi.extractType<
  typeof transactionStatusResponseValidator
>;

export interface IPagingOptions {
  limit?: number;
  page?: number;
}

export interface IPagingResult<T> {
  list: T[];
  limit: number;
  page: number;
  totalPage: number;
}

export type BulkTransactionFeesInput = Joi.extractType<
  typeof bulkTransactionFeesRequestValidator
>;

export type BulkTransactionFeesResponse = Joi.extractType<
  typeof bulkTransactionFeesResponseValidator
>;

export interface IBulkTransactionFeesRequest extends hapi.Request {
  payload: BulkTransactionFeesInput;
}
