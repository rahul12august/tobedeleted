import { RetryConfig } from './transaction.util';
import { ERROR_CODE, isRetriableError } from '../common/errors';
import { ITransactionRetryReversalMessage } from './transaction.producer.type';
import { Util } from '@dk/module-common';
import logger, { wrapLogs } from '../logger';

import transactionReversalService from './transactionReversal.service';
import transactionHelper from './transaction.helper';
import transactionProducer from './transaction.producer';
import {
  RetryableTransferAppError,
  TransferAppError
} from '../errors/AppError';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from './transaction.enum';
import { ITransactionModel } from './transaction.model';
import { LOAN_PROCESSOR_PAYMENT_SERVICE_TYPE_LIST } from './transaction.constant';

const getReversalTemplate = (transaction: ITransactionModel | undefined) => {
  if (
    transaction &&
    LOAN_PROCESSOR_PAYMENT_SERVICE_TYPE_LIST.includes(
      transaction.paymentServiceType
    )
  ) {
    return transactionReversalService.reverseLoanTransaction;
  }

  return transactionReversalService.reverseTransactionsByInstructionId;
};

const revertFailedTransaction = async (
  transactionId: string,
  reversalReason?: string,
  transaction?: ITransactionModel
) => {
  try {
    await Util.retry(
      RetryConfig(),
      getReversalTemplate(transaction),
      transactionId,
      reversalReason,
      transaction
    );
  } catch (err) {
    const error =
      err instanceof TransferAppError
        ? err
        : new TransferAppError(
            err.message || err.status || err.errorCode,
            TransferFailureReasonActor.UNKNOWN,
            ERROR_CODE.UNEXPECTED_ERROR
          );
    await transactionHelper.updateFailedTransactionStatus(
      transactionId,
      TransactionStatus.REVERSAL_FAILED,
      error
    );
    const reversalMessage: ITransactionRetryReversalMessage = {
      transactionId: transactionId,
      retryCounter: 1
    };
    if (isRetriableError(err)) {
      transactionProducer.sendTransactionRetryReversalCommand(reversalMessage);
    } else {
      transactionProducer.sendTransactionReversalFailedEvent(reversalMessage);
      const detail = `Failed to reverse for transaction ID: ${transactionId}!`;
      logger.error(`revertFailedTransaction: ${detail}`);
      throw new RetryableTransferAppError(
        detail,
        TransferFailureReasonActor.UNKNOWN,
        ERROR_CODE.FAILED_TO_REVERSE,
        true
      );
    }
  }
};

const revertManuallyAdjustedTransaction = async (
  transactionId: string,
  transaction: ITransactionModel
) => {
  try {
    await Util.retry(
      RetryConfig(),
      transactionReversalService.reverseTransactionsByInstructionId,
      transactionId,
      'Failed to debit amount while manually adjusting transaction',
      transaction
    );
  } catch (err) {
    const reversalMessage: ITransactionRetryReversalMessage = {
      transactionId: transactionId,
      retryCounter: 1
    };
    transactionProducer.sendTransactionReversalFailedEvent(reversalMessage);
    const detail = `Failed to reverse for transaction ID: ${transactionId}!`;
    logger.error(`revertManuallyAdjustedTransaction: ${detail}`);
    throw new RetryableTransferAppError(
      detail,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.FAILED_TO_REVERSE,
      true
    );
  }
};

const revertFailedTransactionWithoutError = async (
  transactionId: string,
  reversalReason?: string,
  transaction?: ITransactionModel
): Promise<boolean> => {
  try {
    await Util.retry(
      RetryConfig(),
      getReversalTemplate(transaction),
      transactionId,
      reversalReason,
      transaction
    );
    return true;
  } catch (err) {
    const reversalMessage: ITransactionRetryReversalMessage = {
      transactionId: transactionId,
      retryCounter: 1
    };
    if (isRetriableError(err)) {
      transactionProducer.sendTransactionRetryReversalCommand(reversalMessage);
    } else {
      transactionProducer.sendTransactionReversalFailedEvent(reversalMessage);
      logger.error(
        `revertFailedQrisTransaction: failed to reverse for transaction Id: ${transactionId}`
      );
    }
    return false;
  }
};

export default wrapLogs({
  revertFailedTransaction,
  revertManuallyAdjustedTransaction,
  revertFailedTransactionWithoutError
});
