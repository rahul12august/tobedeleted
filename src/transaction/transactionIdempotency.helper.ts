import { ITransactionModel } from './transaction.model';
import transactionRepository from './transaction.repository';
import { wrapLogs } from '../logger';
import { NewEntity } from '../common/common.type';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from './transaction.enum';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';

const validateExistTransactionStatus = (
  existsTransaction: ITransactionModel
): ITransactionModel => {
  switch (existsTransaction.status) {
    case TransactionStatus.SUCCEED:
      return existsTransaction;
    case TransactionStatus.PENDING:
    case TransactionStatus.SUBMITTED:
    case TransactionStatus.SUBMITTING:
      throw new TransferAppError(
        'The transaction still in progress',
        TransferFailureReasonActor.MS_TRANSFER,
        ERROR_CODE.TRANSACTION_STILL_IN_PROGRESS
      );
    case TransactionStatus.REVERTED:
      throw new TransferAppError(
        `Transaction already reversed : ${existsTransaction.id}, externalId : ${existsTransaction.externalId}`,
        TransferFailureReasonActor.UNKNOWN,
        ERROR_CODE.TRANSACTION_ALREADY_REVERSED
      );
    case TransactionStatus.REVERSAL_FAILED:
    case TransactionStatus.DECLINED:
      const failureReason = existsTransaction.failureReason;
      const detail =
        failureReason && failureReason.detail
          ? failureReason.detail
          : 'Transaction declined';
      const actor =
        failureReason && failureReason.actor
          ? failureReason.actor
          : TransferFailureReasonActor.UNKNOWN;
      const errorCode =
        failureReason && failureReason.type
          ? failureReason.type
          : ERROR_CODE.UNEXPECTED_ERROR;

      throw new TransferAppError(detail, actor, errorCode);
    default:
      // Should never happen
      throw new TransferAppError(
        'Idempotency API error. Can not determine the response to answer',
        TransferFailureReasonActor.MS_TRANSFER,
        ERROR_CODE.UNEXPECTED_ERROR
      );
  }
};

const matchExistingTransactionByIdempotencyKey = async (
  transactionModel: NewEntity<ITransactionModel>
): Promise<ITransactionModel | null> => {
  if (transactionModel.idempotencyKey) {
    const existingTransaction = await transactionRepository.findOneByQuery({
      idempotencyKey: transactionModel.idempotencyKey
    });

    if (
      existingTransaction &&
      existingTransaction.externalId === transactionModel.externalId
    ) {
      return validateExistTransactionStatus(existingTransaction);
    }
  }
  return null;
};

const transactionIdempotencyHelper = {
  matchExistingTransactionByIdempotencyKey
};

export default wrapLogs(transactionIdempotencyHelper);
