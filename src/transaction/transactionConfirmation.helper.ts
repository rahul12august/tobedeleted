import transactionRepository from './transaction.repository';
import logger, { wrapLogs } from '../logger';

const updateRefundAmount = async (
  txId: string,
  amount: number
): Promise<void> => {
  logger.info(
    `updateRefundAmount: Updating amount on reversal of refund  for original transaction id : ${txId}`
  );
  const originalTx = await transactionRepository.getByKey(txId);
  if (
    originalTx &&
    originalTx.refund &&
    originalTx.refund.remainingAmount !== undefined &&
    originalTx.refund.remainingAmount !== null
  ) {
    const remainingAmount = originalTx.refund.remainingAmount + amount;
    const updatedTransaction = {
      ...originalTx,
      refund: {
        ...originalTx.refund,
        remainingAmount: remainingAmount
      }
    };
    await transactionRepository.update(originalTx.id, updatedTransaction);
  }
};

const transactionConfirmationHelper = wrapLogs({
  updateRefundAmount
});

export default transactionConfirmationHelper;
