import { ITransactionModel } from '../transaction.model';
import { IFeeExecutionTemplate } from './feeExecution.type';
import { PaymentServiceTypeEnum } from '@dk/module-common';
import { isEmpty } from 'lodash';
import coreBankingHelper from '../transactionCoreBanking.helper';
import depositRepository from '../../coreBanking/deposit.repository';
import logger from '../../logger';
import { TransferJourneyStatusEnum } from '../transaction.enum';

const updateCoreBankingTransactionIds = (
  model: ITransactionModel,
  ids: string[]
): void => {
  if (!isEmpty(ids) && model.coreBankingTransactionIds) {
    for (const id of ids) {
      model.coreBankingTransactionIds.push(id);
      model.coreBankingTransactions?.push({
        id: id,
        type: TransferJourneyStatusEnum.FEE_AMOUNT_DEBITED
      });
    }
  }
};

const checkIfFeeIsAvailable = (model: ITransactionModel): boolean =>
  !(!model.fees || model.fees.length <= 0);

const generalRefundFeeExecution: IFeeExecutionTemplate = {
  isApplicable: (model: ITransactionModel): boolean =>
    checkIfFeeIsAvailable(model) &&
    !isEmpty(model.beneficiaryAccountNo) &&
    model.paymentServiceType === PaymentServiceTypeEnum.GENERAL_REFUND,
  execute: async (model: ITransactionModel): Promise<void> => {
    logger.info(
      `generalRefundFeeExecution: executing general refund fee with beneficiary account no.`
    );
    if (model.beneficiaryAccountNo) {
      const feeTransactionIds = await coreBankingHelper.submitFeeTransactions(
        model.beneficiaryAccountNo,
        model,
        depositRepository.makeDeposit
      );
      updateCoreBankingTransactionIds(model, feeTransactionIds);
    }
  }
};

const offerFeeExecution: IFeeExecutionTemplate = {
  isApplicable: (model: ITransactionModel): boolean =>
    checkIfFeeIsAvailable(model) &&
    !isEmpty(model.beneficiaryAccountNo) &&
    (model.paymentServiceType === PaymentServiceTypeEnum.OFFER ||
      model.paymentServiceType === PaymentServiceTypeEnum.BONUS_INTEREST),

  execute: async (model: ITransactionModel): Promise<void> => {
    logger.info(
      `offerFeeExecution: executing offer fee with beneficiary account no.`
    );
    if (model.beneficiaryAccountNo) {
      const transaction = {
        ...model,
        sourceBankCode: model.beneficiaryBankCode,
        sourceAccountName: model.beneficiaryAccountName,
        sourceAccountType: model.beneficiaryAccountType,
        sourceAccountNo: model.beneficiaryAccountNo,
        beneficiaryAccountNo: model.sourceAccountNo,
        sourceCIF: model.beneficiaryCIF,
        beneficiaryCIF: undefined
      };
      const feeTransactionIds = await coreBankingHelper.submitFeeTransactions(
        model.beneficiaryAccountNo,
        transaction,
        depositRepository.makeWithdrawal
      );
      updateCoreBankingTransactionIds(model, feeTransactionIds);
    }
  }
};

const sourceAccountFeeExecution: IFeeExecutionTemplate = {
  isApplicable: (model: ITransactionModel): boolean =>
    checkIfFeeIsAvailable(model) && !isEmpty(model.sourceAccountNo),
  execute: async (model: ITransactionModel): Promise<void> => {
    logger.info(
      `sourceAccountFeeExecution: executing source account fee execution.`
    );
    if (model.sourceAccountNo) {
      const feeTransactionIds = await coreBankingHelper.submitFeeTransactions(
        model.sourceAccountNo,
        model,
        depositRepository.makeWithdrawal
      );
      updateCoreBankingTransactionIds(model, feeTransactionIds);
    }
  }
};

const cashbackFeeExecution: IFeeExecutionTemplate = {
  isApplicable: (model: ITransactionModel): boolean =>
    checkIfFeeIsAvailable(model) &&
    model.paymentServiceType === PaymentServiceTypeEnum.CASHBACK,

  execute: async (model: ITransactionModel): Promise<void> => {
    logger.info(`cashbackFeeExecution: executing cashback fee execution.`);
    await coreBankingHelper.submitGlFees(model);
  }
};

const availableTemplates: IFeeExecutionTemplate[] = [
  generalRefundFeeExecution,
  offerFeeExecution,
  cashbackFeeExecution,
  sourceAccountFeeExecution
];

export const getApplicable = (
  input: ITransactionModel
): IFeeExecutionTemplate | undefined =>
  availableTemplates.find(template => template.isApplicable(input));
