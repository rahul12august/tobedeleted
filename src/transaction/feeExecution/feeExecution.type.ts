import { ITransactionModel } from '../transaction.model';

export interface IFeeExecutionTemplate {
  isApplicable(input: ITransactionModel): boolean;
  execute(input: ITransactionModel): Promise<void>;
}
