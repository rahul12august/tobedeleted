import { IFeeModel, ITransactionModel } from './transaction.model';
import {
  IJournalEntriesGL,
  IFeeTransactionInput,
  IFeeCardTransactionInput
} from '../coreBanking/deposit.interface';
import {
  mapToCoreBankingTransaction,
  mapDataJournalEntriesGL,
  mapToCoreBankingCardTransaction
} from './transaction.util';

import { wrapLogs } from '../logger';
import { isNumber } from 'lodash';

const getFeeTransactionInputs = (
  transactionModel: ITransactionModel
): IFeeTransactionInput[] => {
  const fees = transactionModel.fees || [];
  const feeInputs: IFeeTransactionInput[] = [];
  for (let i = 0; i < fees.length; i++) {
    if (fees[i].feeAmount > 0) {
      const feeTransaction = mapToCoreBankingTransaction(
        {
          ...transactionModel,
          transactionAmount: fees[i].feeAmount,
          categoryCode: undefined // force enrichment get default category code for fee
        },
        fees[i].customerTc,
        fees[i].customerTcChannel
      );
      feeInputs.push({
        feeTransactionPayload: feeTransaction,
        feeIdempotencyKey: fees[i].idempotencyKey
      });
    }
  }
  return feeInputs;
};

const getFeeCardTransactionInputs = (
  transactionModel: ITransactionModel
): IFeeCardTransactionInput[] => {
  const fees = transactionModel.fees || [];
  const feeInputs: IFeeCardTransactionInput[] = [];
  for (let fee of fees) {
    if (fee.feeAmount > 0 && fee.blockingId) {
      const feeTransaction = mapToCoreBankingCardTransaction(
        {
          ...transactionModel,
          transactionAmount: fee.feeAmount,
          categoryCode: undefined // force enrichment get default category code for fee
        },
        fee.customerTc,
        fee.customerTcChannel,
        fee.blockingId
      );
      feeInputs.push({
        feeCardTransactionPayload: feeTransaction,
        feeCardIdempotencyKey: fee.idempotencyKey
      });
    }
  }
  return feeInputs;
};

const isGLTransaction = (fee: IFeeModel) =>
  fee.subsidiary && isNumber(fee.subsidiaryAmount) && fee.subsidiaryAmount > 0;

const getJournalEntriesGLTransactionInput = (
  model: ITransactionModel
): IJournalEntriesGL[] => {
  const fees = model.fees || [];
  const entries: IJournalEntriesGL[] = [];
  for (let fee of fees) {
    if (isGLTransaction(fee)) {
      entries.push(mapDataJournalEntriesGL(fee, model.id));
    }
  }
  return entries;
};

const feeHelper = {
  getFeeTransactionInputs,
  getJournalEntriesGLTransactionInput,
  getFeeCardTransactionInputs
};

export default wrapLogs(feeHelper);
