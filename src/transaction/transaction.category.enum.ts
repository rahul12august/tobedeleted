import { PaymentServiceTypeEnum } from '@dk/module-common';

import { ITransactionCategoryMetadata } from './transaction.category.type';

export const ATM_WITHDRAWAL_PAYMENT_SERVICE_TYPES = [
  PaymentServiceTypeEnum.THIRD_PARTY_ATM_WITHDRAWAL,
  PaymentServiceTypeEnum.INTERNATIONAL_ATM_WITHDRAWAL
];

export const ATM_WITHDRAWAL_JAGO: ITransactionCategoryMetadata = {
  paymentServiceType: PaymentServiceTypeEnum.THIRD_PARTY_ATM_WITHDRAWAL,
  paymentServiceCode: 'JAGOATM_CASH_WITHDRAW',
  interchange: '-',
  //transactionCode: 'CWD02',
  limitGroupCode: 'L015'
};

export const ATM_WITHDRAWAL_THIRD_PARTY_ALTO: ITransactionCategoryMetadata = {
  paymentServiceType: PaymentServiceTypeEnum.THIRD_PARTY_ATM_WITHDRAWAL,
  paymentServiceCode: 'THIRDPARTY_CASH_WITHDRAW',
  interchange: 'ALTO',
  //transactionCode: 'CWD03',
  limitGroupCode: 'L006'
};

export const ATM_WITHDRAWAL_THIRD_PARTY_ARTAJASA: ITransactionCategoryMetadata = {
  paymentServiceType: PaymentServiceTypeEnum.THIRD_PARTY_ATM_WITHDRAWAL,
  paymentServiceCode: 'THIRDPARTY_CASH_WITHDRAW',
  interchange: 'ARTAJASA',
  //transactionCode: 'CWD03',
  limitGroupCode: 'L006'
};

export const ATM_WITHDRAWAL_INTERNATIONAL: ITransactionCategoryMetadata = {
  paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_ATM_WITHDRAWAL,
  paymentServiceCode: 'VISA_WITHDRAW',
  interchange: 'VISA', //should be - or empty?
  //transactionCode: 'CWD05',
  limitGroupCode: 'L006'
};

export const ATM_WITHDRAWAL_PAYMENT_SERVICE_CODES = [
  ATM_WITHDRAWAL_JAGO.paymentServiceCode, //local - jago ATM
  ATM_WITHDRAWAL_THIRD_PARTY_ALTO.paymentServiceCode, //local - alto + artajasa share psc
  ATM_WITHDRAWAL_INTERNATIONAL.paymentServiceCode //international ATM withdrawal
];

export const TRANSACTION_CATEGORY_ATM_WITHDRAWAL: ITransactionCategoryMetadata[] = [
  ATM_WITHDRAWAL_JAGO,
  ATM_WITHDRAWAL_THIRD_PARTY_ARTAJASA,
  ATM_WITHDRAWAL_THIRD_PARTY_ALTO,
  ATM_WITHDRAWAL_INTERNATIONAL
];

export const ATM_TRANSFER_PAYMENT_SERVICE_TYPES = [
  PaymentServiceTypeEnum.THIRD_PARTY_TRANSFER,
  PaymentServiceTypeEnum.JAGOATM_TRANSFER
];

export const ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE: ITransactionCategoryMetadata = {
  paymentServiceType: PaymentServiceTypeEnum.JAGOATM_TRANSFER,
  paymentServiceCode: 'JAGOATM_JAGO_TO_JAGO',
  interchange: 'JAGO',
  //transactionCode: 'CWD02',
  limitGroupCode: 'L001'
};

export const ATM_TRANSFER_JAGO_ATM_ARTAJASA_INTERCHANGE: ITransactionCategoryMetadata = {
  paymentServiceType: PaymentServiceTypeEnum.JAGOATM_TRANSFER,
  paymentServiceCode: 'JAGOATM_JAGO_TO_OTH',
  interchange: 'ARTAJASA',
  //transactionCode: 'CWD02',
  limitGroupCode: 'L002'
};

export const ATM_TRANSFER_JAGO_ATM_ALTO_INTERCHANGE: ITransactionCategoryMetadata = {
  paymentServiceType: PaymentServiceTypeEnum.JAGOATM_TRANSFER,
  paymentServiceCode: 'JAGOATM_JAGO_TO_OTH',
  interchange: 'ALTO',
  //transactionCode: 'CWD02',
  limitGroupCode: 'L002'
};

export const ATM_TRANSFER_THIRD_PARTY_ATM: ITransactionCategoryMetadata = {
  paymentServiceType: PaymentServiceTypeEnum.THIRD_PARTY_TRANSFER,
  paymentServiceCode: 'THIRDPARTY_JAGO_TO_OTH',
  interchange: '',
  //transactionCode: 'CWD03',
  limitGroupCode: 'L008'
};

export const ATM_TRANSFER_PAYMENT_SERVICE_CODES = [
  ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode, //atm jago to jago
  ATM_TRANSFER_JAGO_ATM_ALTO_INTERCHANGE.paymentServiceCode, //atm jago to altajasa/artajasa
  ATM_TRANSFER_THIRD_PARTY_ATM.paymentServiceCode //third party atm
];

export const TRANSACTION_CATEGORY_ATM_TRANSFER: ITransactionCategoryMetadata[] = [
  ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE,
  ATM_TRANSFER_JAGO_ATM_ARTAJASA_INTERCHANGE,
  ATM_TRANSFER_JAGO_ATM_ALTO_INTERCHANGE,
  ATM_TRANSFER_THIRD_PARTY_ATM
];
