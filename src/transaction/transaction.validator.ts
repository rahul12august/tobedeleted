import Joi from '@hapi/joi';
import validations, { ExecutionMethodEnum } from './transaction.constant';
import {
  PaymentServiceTypeEnum,
  AuthenticationType,
  JoiValidator,
  BankNetworkEnum,
  BankChannelEnum
} from '@dk/module-common';
import transactionConstant from './transaction.constant';
const emptyString = '';
const BENEFICIARY_BANK_CODE_PROP_NAME = 'beneficiaryBankCode';
const matchEmptySchema = Joi.any().valid('', null);
import {
  TransactionStatus,
  ConfirmTransactionStatus,
  TransferJourneyStatusEnum
} from './transaction.enum';
import { HttpHeaders } from '../common/constant';
import { feeAmountResponseValidator } from '../fee/fee.validator';

const paymentServiceCode = JoiValidator.optionalString().description(
  'The payment service code'
);

const externalIdValidator = JoiValidator.optionalString()
  .max(transactionConstant.MAX_LENGTH_EXTERNAL_ID)
  .description('External ID');

const commonTransactionRequestPayload = {
  externalId: externalIdValidator.description('Unique identity of transaction'),
  categoryCode: Joi.string()
    .max(validations.MAX_LENGTH_CATEGORY_CODE)
    .optional()
    .allow(null)
    .description('Category Code'),
  note: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_NOTE)
    .empty(matchEmptySchema)
    .description('Note'),
  additionalInformation1: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
    .empty(matchEmptySchema)
    .description('additionalInformation1 used to save extra information'),
  additionalInformation2: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
    .empty(matchEmptySchema)
    .description('additionalInformation2 used to save extra information'),
  additionalInformation3: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
    .empty(matchEmptySchema)
    .description('additionalInformation3 used to save extra information'),
  additionalInformation4: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
    .empty(matchEmptySchema)
    .description('additionalInformation4 used to save extra information'),
  paymentRequestID: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_PAYMENT_REQUEST_ID)
    .description('An optional payment request or pay-me request identity'),
  generalRefund: Joi.object({
    transactionId: JoiValidator.optionalString().description(
      'Transaction Id of original transaction'
    ),
    externalId: JoiValidator.optionalString().description(
      'External Id of original transaction'
    ),
    fee: Joi.boolean().optional().description(`General refund with fee
      * TRUE: Refunds fees
      * False: Does not refund fees`),
    interchange: JoiValidator.optionalString()
      .valid(Object.values(BankNetworkEnum))
      .description('Interchange/Bank network for General refund')
  })
    .optional()
    .description(`Extra details required for general refund`)
};

export const transactionWithRefundValidator = {
  id: Joi.string()
    .required()
    .description('Refund Transaction Id'),
  status: Joi.string()
    .required()
    .description('Refund Transaction Status'),
  amount: Joi.number()
    .required()
    .description('Refund Transaction Amount'),
  createdAt: Joi.date()
    .required()
    .description('Refund Transacation Created'),
  updatedAt: Joi.date()
    .required()
    .description('Refund Transacation Last Updated')
};

export const transactionInfoResponseValidator = Joi.object({
  paymentRequestID: JoiValidator.optionalString().description(
    'An optional payment request or pay-me request identity'
  ),
  sourceCIF: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Source Cif'),
  sourceAccountType: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Source Account Type'),
  sourceAccountNo: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Source Account Number'),
  sourceAccountName: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Source Account Name'),
  sourceBankCode: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Source Bank Code'),
  sourceTransactionCurrency: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Source Tranasaction Currency'),
  sourceBankCodeChannel: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Source Bank Code Channel'),
  transactionAmount: [JoiValidator.currency(), JoiValidator.requiredString()],
  beneficiaryCIF: JoiValidator.optionalString().description('Beneficiary Cif'),
  beneficiaryBankCode: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Beneficiary Bank Code'),
  beneficiaryAccountType: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Beneficiary Account Type'),
  beneficiaryAccountNo: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Beneficiary Account number'),
  beneficiaryAccountName: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Beneficiary Account Name'),
  beneficiaryBankName: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Beneficiary Bank Name'),
  beneficiaryBankCodeChannel: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Beneficiary Bank Code Channel'),
  debitPriority: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Debit Priority'),
  creditPriority: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Credit Priority'),
  institutionalType: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Institutional Type'),
  note: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Note'),
  paymentServiceType: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Payment Service Type for rule evaluation'),
  extendedMessage: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Extended Message'),
  status: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Status of transaction'),
  externalId: externalIdValidator,
  referenceId: JoiValidator.requiredString().description('Reference ID'),
  fees: Joi.array()
    .items(
      Joi.object({
        feeAmount: [
          JoiValidator.currency().allow([0, null]),
          JoiValidator.requiredString()
        ]
      })
        .label('FeeAmount')
        .description('fees applied on transaction')
    )
    .label('FeeAmounts'),
  ...commonTransactionRequestPayload,
  categoryCode: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Category Code'),
  promotionCode: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Promotion Code'),
  sourceCustomerId: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Source Customer ID'),
  refund: Joi.object()
    .keys({
      remainingAmount: [
        JoiValidator.currency().allow([0, null]),
        JoiValidator.requiredString()
      ],
      originalTransactionId: JoiValidator.optionalString().description(
        'TransactionId of Original transaction'
      ),
      revertPaymentServiceCode: JoiValidator.optionalString()
        .empty(matchEmptySchema)
        .description('Revert Transaction Payment Service code'),
      transactions: Joi.array()
        .items(Joi.string().optional())
        .optional()
        .description('Transactions mapped to original transaction'),
      latestTransaction: Joi.object()
        .keys(transactionWithRefundValidator)
        .optional()
        .description('SUCCEED / Latest updated refund transaction')
    })
    .optional(),
  blockingId: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Blocking ID used in case of blocking transactions'),
  cardId: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Card ID used in case of blocking transactions'),
  transactionId: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Transaction ID of transaction'),
  confirmationCode: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Confirmation Code if we get while confirming transaction'),
  paymentServiceCode: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Payment Service Code used for evaluating rule'),
  createdAt: Joi.date()
    .description('Transaction date')
    .description('Transaction Created Date'),
  reason: Joi.array()
    .items(
      Joi.object()
        .keys({
          responseCode: Joi.number()
            .optional()
            .description('Declined Reason response code'),
          responseMessage: JoiValidator.optionalString().description(
            'Declined response Message code'
          ),
          transactionDate: JoiValidator.optionalString().description(
            'Declined response Transaction Date'
          )
        })
        .label('Declined-reason')
    )
    .optional(),
  journey: Joi.array()
    .items(
      Joi.object()
        .keys({
          status: Joi.string()
            .valid(Object.values(TransferJourneyStatusEnum))
            .required()
            .description('Journey Status'),
          updatedAt: Joi.date().description('Journey created date')
        })
        .label('Journey-of-the-transaction')
        .optional()
    )
    .optional()
    .label('Journey'),
  ownerCustomerId: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Owner Id of transaction')
}).label('TransactionDetail');

const baseRecommendPaymentServiceRequestValidator = {
  sourceCIF: JoiValidator.optionalString()
    .empty('')
    .description('Source Cif'),
  sourceAccountNo: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_SOURCE_ACCOUNT_NO)
    .empty(matchEmptySchema)
    .description('source Account Number'),
  sourceBankCode: JoiValidator.optionalString()
    .empty('')
    .description('source Bank Code'),
  transactionAmount: JoiValidator.currency()
    .not(null, emptyString)
    .required()
    .description('Transaction Amount'),
  beneficiaryCIF: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_BENEFICIARY_CIF)
    .description('Beneficiary Cif'),
  [BENEFICIARY_BANK_CODE_PROP_NAME]: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_BENEFICIARY_BANK_CODE)
    .description('Beneficiary Bank Code'),
  beneficiaryAccountNo: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
    .empty(matchEmptySchema)
    .description('Beneficiary Account No'),
  beneficiaryAccountName: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_BENEFICIARY_ACCOUNT_NAME)
    .empty(matchEmptySchema)
    .description('Beneficiary Account Name'),
  paymentServiceType: Joi.string()
    .valid(Object.values(PaymentServiceTypeEnum))
    .required()
    .description('Payment Service Type for rule evaluation'),
  externalId: externalIdValidator,
  paymentServiceCode: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Payment Service Code for rule evaluation'),
  preferredBankChannel: JoiValidator.optionalString()
    .valid(Object.values(BankChannelEnum))
    .description('Preferred Bank Channel')
};

export const recommendPaymentServiceRequestValidator = Joi.object({
  ...baseRecommendPaymentServiceRequestValidator
}).label('RecommendPaymentServicePayload');

export const transactionRequestBaseModelValidator = {
  ...baseRecommendPaymentServiceRequestValidator,
  ...commonTransactionRequestPayload
};
export const topupTransactionBaseRequestValidator = {
  [BENEFICIARY_BANK_CODE_PROP_NAME]: JoiValidator.requiredString()
    .max(validations.MAX_LENGTH_BENEFICIARY_BANK_CODE)
    .description('Beneficiary Bank Code'),
  transactionAmount: JoiValidator.currency()
    .not(null, emptyString)
    .required()
    .description('The topup amount'),
  paymentServiceCode,
  ...commonTransactionRequestPayload
};
export const topupExternalTransactionRequestPayload = Joi.object({
  ...topupTransactionBaseRequestValidator,
  beneficiaryBankCode: Joi.forbidden().description('Beneficiary Bank Code'),
  paymentServiceCode: Joi.forbidden().description(
    'Payment Service Code for rule evaluation'
  ),
  beneficiaryAccountNo: JoiValidator.requiredString()
    .max(validations.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
    .description('The wallet identifier number including partner prefix')
}).label('ExternalTopupTransactionPayload');

export const topupInternalTransactionRequestPayload = Joi.object({
  ...topupTransactionBaseRequestValidator,
  sourceCIF: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_SOURCE_CIF)
    .description('Source Cif'),
  sourceAccountNo: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_SOURCE_ACCOUNT_NO)
    .description('Source Account Number'),
  beneficiaryAccountNo: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
    .description('The wallet identifier number'),
  paymentInstructionId: JoiValidator.optionalString().description(
    'Payment Instruction ID'
  ),
  paymentServiceType: Joi.string()
    .valid(Object.values(PaymentServiceTypeEnum))
    .required()
    .description('Payment Service Type for rule evaluation'),
  feeAmount: JoiValidator.currency()
    .optional()
    .description('fee amount applied in transaction')
}).label('InternalTopupTransactionPayload');

const processingInfoValidator = Joi.object({
  channel: JoiValidator.optionalString().description('Channel of caller'),
  method: JoiValidator.optionalString()
    .valid(Object.values(ExecutionMethodEnum))
    .description('Execution method'),
  partner: JoiValidator.optionalString().description(
    'Partners who initiate the transaction'
  )
})
  .optional()
  .description(
    'Transaction processing information to indicate source channel, execution method, and partner'
  );

export const createTransferTransactionRequestValidator = Joi.object({
  ...transactionRequestBaseModelValidator,
  sourceCIF: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_SOURCE_CIF)
    .description('Source Cif'),
  sourceAccountNo: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_SOURCE_ACCOUNT_NO)
    .description('Source Account Number'),
  sourceBankCode: JoiValidator.requiredString().description('Source Bank Code'),
  beneficiaryCIF: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_BENEFICIARY_CIF)
    .description('Beneficiary Cif'),
  [BENEFICIARY_BANK_CODE_PROP_NAME]: JoiValidator.requiredString()
    .max(validations.MAX_LENGTH_BENEFICIARY_BANK_CODE)
    .description('Beneficiary Bank Code'),
  beneficiaryAccountNo: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
    .description('Beneficiary Account Number'),
  paymentServiceCode,
  paymentServiceType: Joi.string()
    .valid(Object.values(PaymentServiceTypeEnum))
    .required()
    .description('Payment Service Type for rule evaluation'),
  promotionCode: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_PROMOTION_CODE)
    .description('Promotion Code'),
  interchange: JoiValidator.optionalString()
    .valid(Object.values(BankNetworkEnum))
    .description('Interchange/Bank network'),
  paymentInstructionId: JoiValidator.optionalString().description(
    'Payment Instruction ID'
  ),
  feeAmount: JoiValidator.currency()
    .optional()
    .description('fee amount applied in transaction'),
  additionalPayload: Joi.object({})
    .unknown()
    .optional(),
  processingInfo: processingInfoValidator
}).label('TransferTransactionPayload');

export const createDepositTransactionRequestValidator = Joi.object({
  ...transactionRequestBaseModelValidator,
  paymentServiceType: Joi.string()
    .valid([PaymentServiceTypeEnum.WALLET, PaymentServiceTypeEnum.CASHBACK])
    .required()
    .description('Payment Service Type for rule evaluation'),
  sourceCIF: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Source Cif'),
  sourceAccountNo: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_SOURCE_ACCOUNT_NO)
    .empty(matchEmptySchema)
    .description('Source Account Number'),
  sourceAccountName: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Source Account Name'),
  beneficiaryCIF: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_BENEFICIARY_CIF)
    .description('Beneficiary Cif'),
  [BENEFICIARY_BANK_CODE_PROP_NAME]: JoiValidator.requiredString()
    .max(validations.MAX_LENGTH_BENEFICIARY_BANK_CODE)
    .description('Beneficiary Bank Code'),
  beneficiaryAccountNo: JoiValidator.requiredString()
    .max(validations.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
    .description('Beneficiary Account Number'),
  referenceId: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_REFERENCE_ID)
    .description(
      'An external referenceId of transaction. It could be used to link multiple transaction with the same reference id'
    ),
  paymentServiceCode
}).label('DepositTransactionPayload');

export const createWithdrawTransactionRequestValidator = Joi.object({
  ...transactionRequestBaseModelValidator,
  paymentServiceType: Joi.string()
    .valid([PaymentServiceTypeEnum.MDR])
    .required(),
  sourceCIF: JoiValidator.requiredString().max(
    validations.MAX_LENGTH_SOURCE_CIF
  ),
  sourceAccountNo: JoiValidator.requiredString().max(
    validations.MAX_LENGTH_SOURCE_ACCOUNT_NO
  ),
  sourceBankCode: JoiValidator.requiredString(),
  beneficiaryCIF: JoiValidator.optionalString().max(
    validations.MAX_LENGTH_BENEFICIARY_CIF
  ),
  [BENEFICIARY_BANK_CODE_PROP_NAME]: JoiValidator.optionalString().max(
    validations.MAX_LENGTH_BENEFICIARY_BANK_CODE
  ),
  beneficiaryAccountNo: JoiValidator.optionalString().max(
    validations.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO
  ),
  referenceId: JoiValidator.optionalString().max(
    validations.MAX_LENGTH_REFERENCE_ID
  ),
  paymentServiceCode
}).label('WithdrawTransactionPayload');

const baseTransactionResponseValidator = {
  id: Joi.string()
    .required()
    .description('Transaction id'),
  externalId: externalIdValidator
};

export const transactionResponseValidator = Joi.object({
  ...baseTransactionResponseValidator,
  referenceId: JoiValidator.optionalString().description(
    'Transaction reference ID. Used by consequence transaction to link with this transaction'
  ),
  paymentServiceType: Joi.string(),
  transactionIds: Joi.array()
    .items(Joi.string())
    .required()
    .label('CoreBankingTransactionIdentities')
    .description('It is used for debugging purpose'),
  transactionDate: Joi.date().description('Transaction execution date'),
  transactionStatus: Joi.string()
    .optional()
    .description('Status of the transaction')
}).label('TransactionResponse');

const coreBankingTransactionsValidator = Joi.array()
  .items(
    Joi.object()
      .keys({
        id: Joi.string()
          .required()
          .description('Core banking transaction Id'),
        type: Joi.string()
          .valid(Object.values(TransferJourneyStatusEnum))
          .required()
          .description('Transfer journey status')
      })
      .optional()
  )
  .optional()
  .label('CoreBankingTransactionInfo')
  .description('Core banking transaction IDs mapped to original transaction');

export const externalTransactionResponseValidator = Joi.object({
  ...baseTransactionResponseValidator,
  availableBalance: Joi.number()
    .optional()
    .description('Customer Available Balance'),
  coreBankingTransactions: coreBankingTransactionsValidator
}).label('ExternalTransactionResponse');

export const blockingTransactionResponseValidator = Joi.object({
  id: Joi.string()
    .required()
    .description('Blocking Transaction ID')
}).label('BlockingTransferTransaction');

export const recommendPaymentServiceResponseValidator = Joi.array()
  .items(
    Joi.object({
      paymentServiceCode: Joi.string()
        .required()
        .description('Payment Service Code for rule evaluation'),
      debitTransactionCode: Joi.string()
        .allow(null)
        .description('Debit Transaction Code'),
      creditTransactionCode: Joi.string()
        .allow(null)
        .description('Credit Transaction Code'),
      realBeneficiaryBankCode: Joi.string()
        .allow(null)
        .description('Beneficiary Bank Code'),
      feeAmount: Joi.number()
        .allow(null)
        .description('Fee Amount applied for transaction'),
      customerTc: Joi.string()
        .allow(null)
        .description('Customer Tc'),
      authenticationRequired: Joi.string()
        .valid(Object.values(AuthenticationType))
        .description(
          'Whether the customer need to authenticate for step-up transaction amount'
        )
    }).label('RecommendPaymentService')
  )
  .label('RecommendPaymentServices');

export const reverseRequestValidator = Joi.object({
  notes: JoiValidator.optionalString().description('Notes')
})
  .label('RevertTransactionPayload')
  .allow(null);

export const transactionIdValidator = Joi.object({
  transactionId: Joi.string()
    .required()
    .description('Transaction ID of transaction')
}).label('TransactionIdentities');

export const blockingTransactionAdjustmentRequestValidator = Joi.object({
  amount: JoiValidator.currency()
    .required()
    .description('Amount in blocking Transaction')
}).label('AdjustBlockingPayload');

export const cifValidator = Joi.object({
  cif: Joi.string()
    .required()
    .description('Cif')
}).label('CIF');

export const customerIdValidator = Joi.object({
  customerId: JoiValidator.optionalString().description('Customer ID')
}).label('CustomerId');

const confirmTransactionBaseModelValidator = {
  transactionResponseID: JoiValidator.optionalString()
    .regex(/^\d{6}/)
    .max(validations.MAX_LENGTH_TRANSACTION_RESPONSE_ID)
    .description('Accepted format: MMDDYYXXXXXX')
};

const confirmTransactionAdditionalModelValidator = {
  id: JoiValidator.optionalString().description(
    'Unique identity of transaction for internal'
  ),
  externalId: externalIdValidator.description(
    'Unique identity of transaction for external'
  ),
  coreBankingTransactions: coreBankingTransactionsValidator
};

export const incomingTransactionDate = JoiValidator.requiredString()
  .length(validations.LENGTH_TRANSACTION_DATE_CONFIRMATION)
  .regex(/\d{12}/)
  .description('Date time with format: YYMMDDhhmmss');

const transactionCreatedDate = JoiValidator.optionalString()
  .regex(/^\d{4}[\-](0?[1-9]|1[012])[\-](0?[1-9]|[12][0-9]|3[01])$/)
  .description('Date time with format: YYYY-MM-DD');

export const confirmTransactionRequestValidator = Joi.object({
  externalId: Joi.string()
    .required()
    .max(validations.MAX_LENGTH_EXTERNAL_ID)
    .description('Unique identity of transaction'),
  accountNo: JoiValidator.requiredString()
    .max(validations.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
    .description('Account Number with max length 40'),
  transactionDate: incomingTransactionDate,
  responseCode: Joi.number()
    .required()
    .valid(Object.values(ConfirmTransactionStatus))
    .description('Response code for confirming transaction'),
  responseMessage: Joi.string()
    .max(validations.MAX_LENGTH_TRANSACTION_RESPONSE_MESSAGE)
    .description('Response Message for confirming transaction'),
  interchange: JoiValidator.requiredString()
    .valid(Object.values(BankNetworkEnum))
    .description('Interchange while confirming transaction'),
  additionalInformation1: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
    .empty(matchEmptySchema)
    .description('additionalInformation1 used to save extra information'),
  additionalInformation2: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
    .empty(matchEmptySchema)
    .description('additionalInformation2 used to save extra information'),
  additionalInformation3: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
    .empty(matchEmptySchema)
    .description('additionalInformation3 used to save extra information'),
  additionalInformation4: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
    .empty(matchEmptySchema)
    .description('additionalInformation4 used to save extra information'),
  ...confirmTransactionBaseModelValidator
})
  .options({ stripUnknown: true })
  .label('ConfirmTransactionRequest');

export const confirmTransactionQueryValidator = Joi.object({
  includeTransactionId: Joi.boolean().optional()
    .description(`fetch transactionId for transaction
      * true: include transactionId in response payload
      * false: do not include transactionId in response payload`),
  includeExternalId: Joi.boolean().optional()
    .description(`fetch externalId for transaction
      * true: include in response payload
      * false: do not include in response payload`),
  includeCoreBankingTransactions: Joi.boolean().optional()
    .description(`fetch core banking transaction IDs for transaction 
      * true: include in response payload
      * false: do not include in response payload`)
});

export const confirmTransactionResponseValidator = Joi.object({
  ...confirmTransactionBaseModelValidator,
  ...confirmTransactionAdditionalModelValidator
}).label('ConfirmTransactionResponse');

const baseExternalTransactionPayloadValidator = {
  externalId: externalIdValidator,
  transactionDate: incomingTransactionDate,
  paymentServiceType: JoiValidator.requiredString()
    .valid(Object.values(PaymentServiceTypeEnum))
    .description('Payment Service Type for rule Evaluation'),
  // RTOL, remittance
  sourceBankCode: JoiValidator.requiredString()
    .max(validations.MAX_LENGTH_SOURCE_BANK_CODE)
    .description('Source Bank Code with max length 10'),
  sourceAccountNo: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_SOURCE_ACCOUNT_NO)
    .regex(/[^0-9]/, { invert: true })
    .description('Source Account Number with max length 40'),
  sourceAccountName: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_BENEFICIARY_ACCOUNT_NAME)
    .description('Source Account Name with max length 100'),
  // currency iso code
  currency: JoiValidator.requiredString()
    .max(validations.MAX_LENGTH_CURRENCY_ISO_NO)
    .description('Currency with max length 5'),
  transactionAmount: JoiValidator.currency()
    .required()
    .description('Transaction Amount'),
  // RTOL, remittance
  beneficiaryBankCode: JoiValidator.requiredString()
    .max(validations.MAX_LENGTH_BENEFICIARY_BANK_CODE)
    .description('Beneficiary Bank Code with max length 10'),
  beneficiaryAccountNo: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
    .description('Beneficiary Account Number with max length 40'),
  beneficiaryAccountName: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_BENEFICIARY_ACCOUNT_NAME)
    .description('Beneficiary Account Name with max length 100'),
  interchange: JoiValidator.optionalString()
    .valid(Object.values(BankNetworkEnum))
    .description('Interchange/ Bank Network'),
  notes: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_NOTE)
    .description('Notes for transaction'),
  additionalInformation1: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
    .empty(matchEmptySchema)
    .description('additionalInformation1 used to save extra information'),
  additionalInformation2: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
    .empty(matchEmptySchema)
    .description('additionalInformation2 used to save extra information'),
  additionalInformation3: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
    .empty(matchEmptySchema)
    .description('additionalInformation3 used to save extra information'),
  additionalInformation4: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_TRANSACTION_ADDITIONAL_INFORMATION)
    .empty(matchEmptySchema)
    .description('additionalInformation4 used to save extra information'),
  processingInfo: processingInfoValidator,
  workflowId: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_WORKFLOW_ID)
    .empty(matchEmptySchema)
    .description(
      'Deprecated, workflowId for transactions. In old Atome card transaction processing, one Atome operation creates 2 transactions. These 2 transactions must have the same workflowId'
    ),
  correlationId: JoiValidator.optionalString()
    .max(validations.MAX_LENGTH_WORKFLOW_ID)
    .empty(matchEmptySchema)
    .description(
      'correlationId for transactions. In some transaction processing (for example, Atome card transaction), there may be more than one transfer. We use this correlation id to link them together.'
    ),
  tranType: JoiValidator.optionalString().description(
    'Euronet should send this as OC when this is an Original Credit Transaction from VISA'
  )
};

export const externalTransactionRequestValidator = Joi.object({
  ...baseExternalTransactionPayloadValidator,
  sourceAccountNo: Joi.alternatives().when('paymentServiceType', {
    is: Joi.any().valid([
      PaymentServiceTypeEnum.INCOMING_SKN,
      PaymentServiceTypeEnum.INCOMING_RTGS,
      PaymentServiceTypeEnum.INCOMING_BIFAST
    ]),
    then: JoiValidator.optionalString()
      .max(validations.MAX_LENGTH_SOURCE_ACCOUNT_NO)
      .description('Source Account Number with max length 40'),
    otherwise: JoiValidator.optionalString()
      .max(validations.MAX_LENGTH_SOURCE_ACCOUNT_NO)
      .regex(/[^0-9]/, { invert: true })
      .description('Source Account Number with max length 40')
  }),
  externalId: Joi.string()
    .required()
    .max(validations.MAX_LENGTH_EXTERNAL_ID)
    .regex(/[^A-Za-z0-9\-_]/, { invert: true })
    .description('Unique identity of transaction')
})
  .options({ stripUnknown: true })
  .label('ExternalTransactionRequestValidator');

export const externalTransactionQueryValidator = Joi.object({
  includeCoreBankingTransactions: Joi.string()
    .optional()
    .description('fetch core banking transaction IDs for transaction')
});

export const externalTransactionHeaderValidator = Joi.object({
  [HttpHeaders.X_IDEMPOTENCY_KEY]: Joi.string()
    .optional()
    .description(
      'Key to differentiate requests, will return same response for identical keys'
    )
}).unknown();

export const externalTransactionFeeRequestValidator = Joi.object({
  ...baseExternalTransactionPayloadValidator
}).label('Request-external-transaction-fee');

export const incomingFeeCheckResponseValidator = Joi.object({
  externalId: externalIdValidator,
  feeAmount: Joi.number()
    .required()
    .description('fee amount for incoming fee check')
}).label('Response-external-transaction-fee');

export const refundTransactionRequestValidator = Joi.object({
  transactionId: JoiValidator.requiredString().description(
    'transaction ID for refund transaction'
  )
});

export const refundTransactionQueryValidator = Joi.object({
  refundFee: Joi.boolean()
    .optional()
    .description('refund fee for refunded transaction'),
  interchange: JoiValidator.optionalString()
    .valid(Object.values(BankNetworkEnum))
    .description('Interchange/ Bank Network')
});

export const refundTransactionResponseValidator = Joi.object({
  id: JoiValidator.requiredString().description(
    'ID for refund transaction response'
  )
});

export const transactionListQueryValidator = Joi.object({
  fetchRefund: Joi.string()
});

const pagingRequest = {
  page: Joi.number()
    .integer()
    .positive() // first page is 1
    .empty(1),
  limit: Joi.number()
    .positive()
    .integer()
    .max(validations.MAX_RECORD_PER_PAGE)
    .empty(validations.DEFAULT_RECORD_PER_PAGE)
};

export const transactionListRequestValidator = Joi.object({
  transactionId: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Transaction ID for the transaction'),
  customerId: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Transaction customer id'),
  startDate: transactionCreatedDate,
  endDate: transactionCreatedDate,
  transactionStatus: Joi.array()
    .items(
      Joi.string()
        .optional()
        .valid(Object.values(TransactionStatus))
        .description('Transaction Status')
    )
    .optional(),
  beneficiaryAccountNo: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Beneficiary Account number'),
  sourceAccountNo: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Source Account number'),
  sourceBankCode: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Source Bank code'),
  beneficiaryBankCode: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Beneficiary Bank code'),
  transactionAmount: JoiValidator.currency().description('Transaction Amount'),
  accountNo: JoiValidator.optionalString()
    .empty(matchEmptySchema)
    .description('Account number'),
  ...pagingRequest
}).label('TransactionListRequest');

const pagingResultResponse = {
  limit: Joi.number().required(),
  page: Joi.number().required(),
  totalPage: Joi.number().required()
};

export const transactionListResponseVaidator = Joi.object({
  ...pagingResultResponse,
  list: Joi.array()
    .items(transactionInfoResponseValidator)
    .label('TransactionList')
});

export const extTxnManualAdjstmentRequestValidator = Joi.object({
  transactionId: Joi.string()
    .required()
    .description('Transaction ID for the transaction'),
  status: Joi.string()
    .required()
    .valid(TransactionStatus.SUCCEED, TransactionStatus.DECLINED)
    .description('Status of the transaction'),
  interchange: Joi.string()
    .optional()
    .valid(Object.values(BankNetworkEnum))
    .description('Interchange / Bank Network')
}).label('ConfirmTransactionRequest');

export const transactionStatusRequestValidator = Joi.object({
  customerId: Joi.string()
    .required()
    .description('Owner Customer ID of transaction'),
  paymentInstructionId: Joi.string()
    .required()
    .description('Payment Instruction ID of transaction'),
  transactionDate: Joi.date()
    .optional()
    .description('Date of transaction')
}).label('TransactionIdentities');

export const transactionStatusResponseValidator = {
  status: Joi.string()
    .required()
    .description('Status of the transaction')
};

export const bulkTransactionFeesRequestValidator = Joi.object({
  transactions: Joi.array()
    .items({
      ...baseRecommendPaymentServiceRequestValidator,
      externalId: JoiValidator.requiredString()
        .max(transactionConstant.MAX_LENGTH_EXTERNAL_ID)
        .description('External ID')
    })
    .required()
    .max(transactionConstant.MAX_RECORD_PER_PAGE)
    .unique('externalId')
}).label(`bulkTransactionFeesRequest`);

export const bulkTransactionFeesResponseValidator = Joi.array()
  .items(
    feeAmountResponseValidator.keys({
      externalId: JoiValidator.requiredString()
        .max(transactionConstant.MAX_LENGTH_EXTERNAL_ID)
        .description('External ID')
    })
  )
  .label('bulkTransactionFeesResponse');
