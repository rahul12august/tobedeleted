/**
 * This module is to handle any transaction that come from 3rd application or switching
 *
 */

import logger, { wrapLogs } from '../logger';
import {
  BaseExternalTransactionInput,
  ExternalTransactionInput,
  IncomingFeeCheckResponse,
  ITransactionAccount,
  ITransactionExecutionHooks,
  ITransactionInput,
  TopupExternalTransactionPayload
} from './transaction.type';
import { ITransactionModel } from './transaction.model';
import configurationRepository from '../configuration/configuration.repository';
import { IBankCode } from '../configuration/configuration.type';
import transactionHelper from './transaction.helper';
import {
  BankChannelEnum,
  Constant,
  PaymentServiceTypeEnum
} from '@dk/module-common';
import virtualAccountRepository from '../virtualAccount/virtualAccount.repository';
import recommendedCodeService from './transactionRecommend.service';
import transactionExecutionService, {
  defaultExecutionHooks
} from './transactionExecution.service';
import {
  createIncomingTransactionExternalId,
  generateUniqueId
} from './transaction.util';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import transactionService from './transaction.service';
import depositRepository from '../coreBanking/deposit.repository';
import refundValidator from './refund/refund.validator';
import { IRefundTemplate } from './refund/refund.type';
import euronetRefundTemplate from './refund/euronet/euronetRefund.template';
import jagoPayRefundTemplate from './refund/jagoPay/jagoPayRefund.template';
import sknRtgsRefundTemplate from './refund/sknRtgs/sknRtgsRefund.template';
import bifastRefundTemplate from './refund/bifast/bifastRefund.template';
import accountRepository from '../account/account.repository';
import {
  JagoExternalBankCode,
  JagoShariaExternalBankCode,
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from './transaction.enum';
import { trimStringValuesFromObject } from '../common/common.util';
import transactionRepository from './transaction.repository';
import { NewEntity } from '../common/common.type';
import configurationService from '../configuration/configuration.service';
import { isEmpty } from 'lodash';
import monitoringEventProducer from '../monitoringEvent/monitoringEvent.producer';
import { MonitoringEventType } from '@dk/module-message';
import transactionIdempotencyHelper from './transactionIdempotency.helper';
import atomeRefundTemplate from './refund/atome/atomeRefund.template';

const jagoAccountNumberMapping: { [key: string]: string } = {
  '0': 'BC191', // ATOS
  '1': 'BC002', // JAGO
  '2': 'BC002', // JAGO - Corporate account
  '9': 'BC004', // GOPAY
  '5': 'BC777', // JAGO SYARIAH
  '7': 'BC002'
};

const jagoBankCodeDefaultMapping: { [key: string]: string } = {
  '542': 'BC191' //ARTOS
};

const goBizAccountReferenceGroup = 'goBizAccount';

const allowedBankCodeCheck = (bankCode: string): boolean => {
  const RTOL_CODES = [
    JagoExternalBankCode.RTOL_CODE,
    JagoShariaExternalBankCode.RTOL_CODE
  ];
  const REMITTANCE_CODES = [
    JagoExternalBankCode.REMITTANCE_CODE,
    JagoShariaExternalBankCode.REMITTANCE_CODE
  ];

  return ([...RTOL_CODES, ...REMITTANCE_CODES] as string[]).includes(bankCode);
};

const getEligibleBankCode = (bankCode: string, accountNo?: string): string => {
  const logMessage = (findBankCode: string) => {
    logger.info(
      `getEligibleBankCode foundbank code: ${findBankCode} from input: bankCode ${bankCode} & accountNo ${accountNo}`
    );
  };
  if (accountNo && allowedBankCodeCheck(bankCode)) {
    const findBankCode = jagoAccountNumberMapping[accountNo.charAt(0)];
    if (findBankCode) {
      logMessage(findBankCode);
      return findBankCode;
    }
  } else if (allowedBankCodeCheck(bankCode)) {
    const findBankCode = jagoBankCodeDefaultMapping[bankCode];
    if (findBankCode) {
      logMessage(findBankCode);
      return findBankCode;
    }
  }
  logMessage('none');
  return bankCode;
};

const populateExternalWalletTransactionInput = async (
  input: TopupExternalTransactionPayload
): Promise<ITransactionInput> => {
  const virtualAccount = await virtualAccountRepository.inquiryVirtualAccount(
    input.beneficiaryAccountNo
  );
  const [ginBankCodeInfo, beneficiaryBankCodeInfo] = await Promise.all([
    transactionHelper.getBankCodeMapping(Constant.GIN_BANK_CODE_ID),
    transactionHelper.getBankCodeMapping(virtualAccount.institution.bankCode)
  ]);
  const beneficiaryAccount = await transactionHelper.getAccountInfo(
    virtualAccount.institution.accountId,
    virtualAccount.institution.cif,
    ginBankCodeInfo
  );

  const transactionInput: ITransactionInput = {
    ...input,
    paymentServiceCode: '',
    beneficiaryBankCode: virtualAccount.institution.bankCode,
    paymentServiceType: PaymentServiceTypeEnum.WALLET
  };
  return transactionHelper.populateTransactionInput(
    transactionInput,
    undefined,
    undefined,
    beneficiaryAccount,
    beneficiaryBankCodeInfo
  );
};

export const populateIncomingAccount = async (
  bankCode: IBankCode,
  accountNo?: string,
  accountName?: string,
  cif?: string
): Promise<ITransactionAccount> => {
  const accountInfo: ITransactionAccount = {
    accountName: accountName,
    bankCode: bankCode.bankCodeId,
    accountNumber: accountNo
  };

  logger.info(`populateIncomingAccount with cif: ${cif}`);

  const inquiryInfo =
    accountNo &&
    Constant.JAGO_BANK_CODES.includes(bankCode.bankCodeId) &&
    (await accountRepository.getAccountInfo(accountNo));

  logger.info(
    `Inquiry Info: ${typeof inquiryInfo}, accountNo first char: ${
      accountNo ? accountNo[0] : undefined
    }, bankCode: ${bankCode.bankCodeId}}`
  );
  return {
    ...accountInfo,
    ...inquiryInfo
  };
};

const processIncomingTransaction = async (
  transactionInput: ITransactionInput,
  executionHooks: ITransactionExecutionHooks = defaultExecutionHooks
): Promise<ITransactionModel> => {
  if (transactionInput.externalId) {
    if (!transactionInput.externalTransactionDate) {
      logger.error(`processIncomingTransaction : Missing Transaction Date.`);
      throw new TransferAppError(
        'Missing Transaction Date.',
        TransferFailureReasonActor.UNKNOWN,
        ERROR_CODE.REQUIRE_TRANSACTION_DATE
      );
    }
    const oldExternalId = transactionInput.externalId;
    transactionInput.externalId = createIncomingTransactionExternalId(
      oldExternalId,
      transactionInput.externalTransactionDate,
      transactionInput.sourceAccountNo
    );
    if (transactionInput.beneficiaryAccountNo) {
      transactionInput.secondaryExternalId = createIncomingTransactionExternalId(
        oldExternalId,
        transactionInput.externalTransactionDate,
        transactionInput.beneficiaryAccountNo
      );
    }
    transactionInput.thirdPartyOutgoingId = oldExternalId;
  }
  return transactionExecutionService.executeTransaction(
    transactionInput,
    executionHooks
  );
};

const populateExternalTransactionInput = async (
  input: BaseExternalTransactionInput,
  transactionModel?: ITransactionModel
): Promise<ITransactionInput> => {
  try {
    const eligibleSourceBankCode = getEligibleBankCode(
      input.sourceBankCode,
      input.sourceAccountNo
    );
    const eligibleBeneBankCode = getEligibleBankCode(
      input.beneficiaryBankCode,
      input.beneficiaryAccountNo
    );
    const bankCodes = await configurationRepository.inquiryBankAndCurrencyByIncomingCodes(
      eligibleSourceBankCode,
      eligibleBeneBankCode,
      input.currency
    );

    const [sourceAccountInfo, beneficiaryAccountInfo] = await Promise.all([
      populateIncomingAccount(
        bankCodes.sourceBank,
        input.sourceAccountNo,
        input.sourceAccountName
      ),
      populateIncomingAccount(
        bankCodes.beneficiaryBank,
        input.beneficiaryAccountNo,
        input.beneficiaryAccountName
      )
    ]);

    let transactionInput: ITransactionInput = {
      ...input,
      paymentServiceCode: '',
      externalTransactionDate: input.transactionDate,
      sourceBankCode: bankCodes.sourceBank.bankCodeId,
      beneficiaryBankCode: bankCodes.beneficiaryBank.bankCodeId,
      note: input.notes,
      sourceTransactionCurrency: bankCodes.currency.code,
      transactionId: transactionModel && transactionModel.id
    };
    transactionInput = {
      ...transactionInput,
      journey: transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.INQUIRY_FETCHED,
        transactionInput
      )
    };

    logger.info(
      `transactionInput BankCodes: 
      source BankCode: ${transactionInput.sourceBankCode},
      beneficiary BankCode: ${transactionInput.beneficiaryBankCode},`
    );

    return transactionHelper.populateTransactionInput(
      transactionInput,
      sourceAccountInfo,
      bankCodes.sourceBank,
      beneficiaryAccountInfo,
      bankCodes.beneficiaryBank
    );
  } catch (err) {
    logger.error(
      `populateExternalTransactionInput: Error while making an inquiry ${err.message}`
    );
    transactionModel &&
      (await transactionService.updateFailedTransaction(transactionModel, err));

    throw err;
  }
};

const buildNewTransactionModel = (
  transaction: BaseExternalTransactionInput,
  idempotencyKey?: string
): NewEntity<ITransactionModel> => ({
  ...transaction,
  referenceId: generateUniqueId(),
  idempotencyKey: idempotencyKey,
  externalId: createIncomingTransactionExternalId(
    transaction.externalId || generateUniqueId(),
    transaction.transactionDate,
    transaction.sourceAccountNo
  ),
  paymentServiceCode: '',
  thirdPartyOutgoingId: transaction.externalId,
  paymentServiceType: transaction.paymentServiceType,
  beneficiaryRealBankCode: transaction.beneficiaryBankCode,
  status: TransactionStatus.PENDING,
  requireThirdPartyOutgoingId: false,
  journey: [
    {
      status: TransferJourneyStatusEnum.REQUEST_RECEIVED,
      updatedAt: new Date()
    }
  ]
});

const isAccountFromGojekBusiness = async (
  cif: string | undefined
): Promise<boolean> => {
  const env = process.env.NODE_ENV;
  // get eligible cif from parameter setting
  const parameterSetting = await configurationService.getParameterSettingByReferenceGroup(
    goBizAccountReferenceGroup
  );
  const goBizCif = parameterSetting?.find(param => {
    return param.name === env;
  });
  return goBizCif !== undefined && goBizCif.code === cif;
};

const validateIrisPayment = async (input: ITransactionInput) => {
  if (
    input.paymentServiceType === PaymentServiceTypeEnum.IRIS_TO_JAGO ||
    input.paymentServiceType === PaymentServiceTypeEnum.GOPAY_TO_JAGO
  ) {
    if (isEmpty(input.sourceAccountNo) || isEmpty(input.sourceCIF)) {
      throw new TransferAppError(
        'Input sourceAccountNo or sourceCIF is empty!',
        TransferFailureReasonActor.SUBMITTER,
        ERROR_CODE.SOURCE_ACCOUNT_NOT_FOUND
      );
    }

    const isEligibleAccount = await isAccountFromGojekBusiness(input.sourceCIF);
    if (!isEligibleAccount) {
      logger.error(
        `Account :${input.sourceAccountNo} is not eligible to disburse trx`
      );
      throw new TransferAppError(
        `Account :${input.sourceAccountNo} is not eligible to disburse transaction!`,
        TransferFailureReasonActor.SUBMITTER,
        ERROR_CODE.UNAUTHORIZED
      );
    }
  }
};

const executeTransaction = async (
  externalInput: BaseExternalTransactionInput,
  idempotencyKey?: string
): Promise<ITransactionModel> => {
  const transactionModel = buildNewTransactionModel(
    externalInput,
    idempotencyKey
  );

  const existingTransaction = await transactionIdempotencyHelper.matchExistingTransactionByIdempotencyKey(
    transactionModel
  );
  if (existingTransaction) {
    return existingTransaction;
  }

  await transactionHelper.validateExternalIdAndPaymentType(transactionModel);

  const model = await transactionRepository.createIfNotExist(transactionModel);

  monitoringEventProducer.sendMonitoringEventMessage(
    MonitoringEventType.PROCESSING_STARTED,
    model
  );

  const input = await populateExternalTransactionInput(externalInput, model);
  await validateIrisPayment(input);

  return processIncomingTransaction(input);
};

const processRefund = async (
  input: BaseExternalTransactionInput,
  template: IRefundTemplate
): Promise<ITransactionModel> => {
  logger.info('processRefund: Requested transaction for refund.');
  const sourceBankCode = getEligibleBankCode(
    input.sourceBankCode,
    input.sourceAccountNo
  );
  const beneficiaryBankCode = getEligibleBankCode(
    input.beneficiaryBankCode,
    input.beneficiaryAccountNo
  );

  const originalTransaction = await template.getOriginalTransaction(input);

  refundValidator.validateRefundRequest(
    {
      ...input,
      sourceBankCode,
      beneficiaryBankCode
    },
    originalTransaction
  );

  const refundTxn = await executeTransaction(input);

  logger.info(
    'processRefund: Updating Transaction records with refund details'
  );
  await transactionService.updateOriginalTransactionWithRefund(
    originalTransaction,
    refundTxn
  );
  await transactionService.updateRefundTransaction(
    originalTransaction,
    refundTxn
  );

  return refundTxn;
};

const refundTemplates: IRefundTemplate[] = [
  euronetRefundTemplate,
  jagoPayRefundTemplate,
  sknRtgsRefundTemplate,
  bifastRefundTemplate,
  atomeRefundTemplate
];

const matchRefundTemplate = (
  transactionInput: BaseExternalTransactionInput
): IRefundTemplate | undefined => {
  const template = refundTemplates.find(refundTemplate =>
    refundTemplate.isApplicable(transactionInput)
  );
  if (template) {
    logger.info(
      `Transaction Refund template found for paymentServiceType : ${transactionInput.paymentServiceType}`
    );
  }
  return template;
};

const getAvailableBalance = async (
  tx: ITransactionModel
): Promise<number | undefined> => {
  let accountNumber;
  if (tx.sourceBankCodeChannel === BankChannelEnum.INTERNAL) {
    accountNumber = tx.sourceAccountNo;
  } else if (tx.beneficiaryBankCodeChannel === BankChannelEnum.INTERNAL) {
    accountNumber = tx.beneficiaryAccountNo;
  }
  return accountNumber
    ? depositRepository.getAvailableBalance(accountNumber)
    : undefined;
};

/**
 * This will handle any transaction that come from 3rd parties or switching
 *
 * @param transactionInput
 * @param idempotencyKey
 */
const createExternalTransaction = async (
  transactionInput: ExternalTransactionInput,
  idempotencyKey?: string
): Promise<ITransactionModel> => {
  trimStringValuesFromObject(transactionInput);
  logger.info(
    `createExternalTransaction : external transaction payload ${transactionInput.paymentServiceType}, 
      idempotencyKey: ${idempotencyKey},
      externalId: ${transactionInput.externalId},
      sourceBankCode: ${transactionInput.sourceBankCode}, 
      sourceAccountNumber: ${transactionInput.sourceAccountNo},
      beneficiaryBankCode: ${transactionInput.beneficiaryBankCode},
      beneficiaryAccountNumber: ${transactionInput.beneficiaryAccountNo}, 
      interchange: ${transactionInput.interchange}`
  );
  const normalizedTrxInput = transactionInput;

  const template = matchRefundTemplate(normalizedTrxInput);
  if (template) {
    return processRefund(normalizedTrxInput, template);
  }

  const transaction = await executeTransaction(
    normalizedTrxInput,
    idempotencyKey
  );

  const availableBalance = await getAvailableBalance(transaction);
  return { ...transaction, availableBalance };
};

const getExternalTransactionFee = async (
  transactionInput: BaseExternalTransactionInput
): Promise<IncomingFeeCheckResponse> => {
  const input = await populateExternalTransactionInput(transactionInput);
  const recommendationService = await recommendedCodeService.getRecommendService(
    input
  );
  const result: IncomingFeeCheckResponse = {
    externalId: transactionInput.externalId,
    feeAmount:
      (recommendationService.feeData &&
        recommendationService.feeData[0] &&
        recommendationService.feeData[0].feeAmount) ||
      0
  };
  return result;
};

const createExternalTopupTransaction = async (
  input: TopupExternalTransactionPayload
): Promise<ITransactionModel> => {
  trimStringValuesFromObject(input);
  const depositTransactionInput: ITransactionInput = await populateExternalWalletTransactionInput(
    input
  );
  return processIncomingTransaction(depositTransactionInput);
};

const externalTransactionService = wrapLogs({
  createExternalTransaction,
  getExternalTransactionFee,
  createExternalTopupTransaction
});

export default externalTransactionService;
