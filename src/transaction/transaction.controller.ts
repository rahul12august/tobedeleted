import {
  AdminPortalUserPermissionCode,
  BankChannelEnum,
  Http
} from '@dk/module-common';
import hapi from '@hapi/hapi';
import _ from 'lodash';
import billPaymentService from '../billPayment/billPayment.service';
import logger from '../logger';
import generalRefundService from './refund/generalRefund.service';
import transactionService from './transaction.service';
import {
  IBlockingAmountTransactionRequest,
  IBulkTransactionFeesRequest,
  ICancelBlockingTransactionRequest,
  IConfirmTransactionRequest,
  ICreateDepositTransactionRequest,
  ICreateTransactionRequest,
  ICreateWithdrawTransactionRequest,
  IExecuteBlockingTransactionRequest,
  IExtTxnManualAdjstmentRequest,
  IIncomingRefundTransactionRequest,
  IIncomingTransactionFeeRequest,
  IIncomingTransactionRequest,
  IManualUnBlockingTransactionRequest,
  IRecommendPaymentServiceRequest,
  IReverseTransactionRequest,
  ITopupExternalTransactionPayload,
  ITopupInternalTransactionPayload,
  ITransactionListRequest,
  ITransactionRequest,
  ITransactionStatusRequest
} from './transaction.type';
import {
  mapConfirmTransactionResultToExternalResponse,
  mapTransactionResultToExternalResponse,
  mapTransactionResultToResponse,
  toTransactionDetail,
  toTransactionList,
  validateTransactionForManualAdjustment,
  withReadOnlyMode
} from './transaction.util';
import {
  blockingTransactionAdjustmentRequestValidator,
  blockingTransactionResponseValidator,
  bulkTransactionFeesRequestValidator,
  bulkTransactionFeesResponseValidator,
  cifValidator,
  confirmTransactionQueryValidator,
  confirmTransactionRequestValidator,
  confirmTransactionResponseValidator,
  createDepositTransactionRequestValidator,
  createTransferTransactionRequestValidator,
  createWithdrawTransactionRequestValidator,
  customerIdValidator,
  externalTransactionFeeRequestValidator,
  externalTransactionHeaderValidator,
  externalTransactionQueryValidator,
  externalTransactionRequestValidator,
  externalTransactionResponseValidator,
  extTxnManualAdjstmentRequestValidator,
  incomingFeeCheckResponseValidator,
  recommendPaymentServiceRequestValidator,
  recommendPaymentServiceResponseValidator,
  refundTransactionQueryValidator,
  refundTransactionRequestValidator,
  refundTransactionResponseValidator,
  reverseRequestValidator,
  topupExternalTransactionRequestPayload,
  topupInternalTransactionRequestPayload,
  transactionIdValidator,
  transactionInfoResponseValidator,
  transactionListQueryValidator,
  transactionListRequestValidator,
  transactionListResponseVaidator,
  transactionResponseValidator,
  transactionStatusRequestValidator,
  transactionStatusResponseValidator
} from './transaction.validator';
import transactionBlockingService from './transactionBlocking.service';
import switchingTransactionService from './transactionConfirmation.service';
import externalTransactionService from './transactionExternal.service';
import txnManualAdjustmentService from './transactionManualAdjustment.service';
import transactionReversalService from './transactionReversal.service';
import { updateRequestIdInContext } from '../common/contextHandler';
import { ExecutionTypeEnum, TransactionStatus } from './transaction.enum';
import { trimStringValuesFromObject } from '../common/common.util';
import { getAuthPayloadFromAdminToken } from '../common/authentication.util';
import entitlementService from '../entitlement/entitlement.service';
import entitlementFlag from '../common/entitlementFlag';
import transactionUsageService from '../transactionUsage/transactionUsage.service';
import { HttpHeaders } from '../common/constant';

const recommendPaymentServices: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/recommend-payment-services',
  options: {
    description: 'Recommend payment service code',
    notes:
      'The api is to calculate the transaction fee and to acquire a payment service code which is used to submit a transaction later',
    tags: ['api', 'transactions', 'admin'],
    validate: {
      payload: recommendPaymentServiceRequestValidator
    },
    response: {
      schema: recommendPaymentServiceResponseValidator
    },
    handler: async (
      hapiRequest: IRecommendPaymentServiceRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const startTime = new Date().getTime();

      logger.debug(
        `recommendPaymentServices: before calling recommendPaymentService : ${new Date().getTime() -
          startTime}ms to complete`
      );

      const serviceRecommendations = await transactionService.recommendPaymentService(
        hapiRequest.payload
      );

      logger.debug(
        `recommendPaymentServices: after calling recommendPaymentService : ${new Date().getTime() -
          startTime}ms to complete`
      );

      const response = serviceRecommendations.map(serviceRecommendation => {
        const feeData = serviceRecommendation.feeData;
        return {
          paymentServiceCode: serviceRecommendation.paymentServiceCode,
          debitTransactionCode: _.get(
            serviceRecommendation,
            ['debitTransactionCode', 0, 'transactionCode'],
            null
          ),
          creditTransactionCode: _.get(
            serviceRecommendation,
            ['creditTransactionCode', 0, 'transactionCode'],
            null
          ),
          realBeneficiaryBankCode: _.get(
            serviceRecommendation,
            'beneficiaryBankCode',
            null
          ),
          feeAmount: _.get(feeData, [0, 'feeAmount'], null),
          customerTc: _.get(feeData, [0, 'customerTc'], null),
          authenticationRequired: serviceRecommendation.authenticationType
        };
      });

      logger.debug(
        `recommendPaymentServices: takes  ${new Date().getTime() -
          startTime}ms to complete`
      );

      return hapiResponse.response(response).code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Success'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const recommendPaymentServicesForAdminPortal: hapi.ServerRoute = {
  ...recommendPaymentServices,
  path: '/admin/recommend-payment-services'
};

const recommendPaymentServicesWithoutAuth: hapi.ServerRoute = {
  ...recommendPaymentServices,
  path: '/private/recommend-payment-services',
  options: {
    ...recommendPaymentServices.options,
    description: 'Recommend payment service code',
    notes:
      'Private api to calculate the transaction fee and to acquire a payment service code which is used to submit a transaction later',
    tags: ['api', 'private', 'transactions'],
    auth: false
  }
};

const createTransferTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/transfer-transactions',
  options: {
    description: 'Create a transfer transaction',
    notes:
      'Private Api to do Transfer from sourceAccountNo to beneficiaryAccountNo',
    tags: ['api', 'transactions', 'private'],
    auth: false,
    validate: {
      query: customerIdValidator,
      payload: createTransferTransactionRequestValidator
    },
    response: {
      schema: transactionResponseValidator
    },
    handler: async (
      hapiRequest: ICreateTransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { payload, query } = hapiRequest;
      trimStringValuesFromObject(payload);
      const transaction = await transactionService.createTransferTransaction({
        ...payload,
        sourceCustomerId: query.customerId
      });
      let response = mapTransactionResultToResponse(transaction);
      if (transaction.executionType === ExecutionTypeEnum.NONE_BLOCKING) {
        response = {
          ...response,
          transactionStatus: transaction.status
        };
      }
      return hapiResponse.response(response).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Transfer transaction is created'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const privateCreateTransferTransaction: hapi.ServerRoute = {
  ...createTransferTransaction,
  path: '/private/transfer-transactions'
};

const createInternalTopupTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/top-up-internal-transactions',
  options: {
    description: 'Create internal top up transaction',
    notes: `Private api where internal topup will topup money from main account of customer to partner (GoPay) and none partner (Ovo) wallets`,
    tags: ['api', 'transactions', 'private'],
    auth: false,
    validate: {
      query: customerIdValidator,
      payload: topupInternalTransactionRequestPayload
    },
    response: {
      schema: transactionResponseValidator
    },
    handler: async (
      hapiRequest: ITopupInternalTransactionPayload,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const transaction = await transactionService.createInternalTopupTransaction(
        {
          ...hapiRequest.payload,
          sourceCustomerId: hapiRequest.query.customerId
        }
      );
      const response = mapTransactionResultToResponse(transaction);
      return hapiResponse.response(response).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Created'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const privateCreateInternalTopupTransaction: hapi.ServerRoute = {
  ...createInternalTopupTransaction,
  path: '/private/top-up-internal-transactions'
};

/**
 * @dikshitthakral
 * To be depreciated as not being used.
 */
const createExternalTopupTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/top-up-external-transactions',
  options: {
    description: 'Create top up transaction triggered from external/switching',
    notes: `An external topup will topup money to partner (GoPay) only`,
    tags: ['api', 'transactions', 'private'],
    auth: false,
    validate: {
      payload: topupExternalTransactionRequestPayload
    },
    response: {
      schema: transactionResponseValidator
    },
    handler: async (
      hapiRequest: ITopupExternalTransactionPayload,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const transaction = await externalTransactionService.createExternalTopupTransaction(
        hapiRequest.payload
      );
      const response = mapTransactionResultToResponse(transaction);
      return hapiResponse.response(response).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Created'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const privateCreateExternalTopupTransaction: hapi.ServerRoute = {
  ...createExternalTopupTransaction,
  path: '/private/top-up-external-transactions'
};

const createDepositTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/deposit-transactions',
  options: {
    description: 'Create deposit transaction',
    notes:
      'Private api to deposit a transaction amount to a given beneficiaryAccountNo',
    tags: ['api', 'transactions', 'private'],
    auth: false,
    validate: {
      query: customerIdValidator,
      payload: createDepositTransactionRequestValidator
    },
    response: {
      schema: transactionResponseValidator
    },
    handler: async (
      hapiRequest: ICreateDepositTransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const transaction = await transactionService.createDepositTransaction({
        ...hapiRequest.payload,
        sourceCustomerId: hapiRequest.query.customerId
      });
      const response = mapTransactionResultToResponse(transaction);
      return hapiResponse.response(response).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Created'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const privateCreateDepositTransaction: hapi.ServerRoute = {
  ...createDepositTransaction,
  path: '/private/deposit-transactions'
};

const createWithdrawTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/withdraw-transactions',
  options: {
    description: 'Create withdraw transaction',
    notes:
      'Private Api to withdraw a transaction amount from a given sourceAccountNo',
    tags: ['api', 'transactions', 'private'],
    auth: false,
    validate: {
      query: customerIdValidator,
      payload: createWithdrawTransactionRequestValidator
    },
    response: {
      schema: transactionResponseValidator
    },
    handler: async (
      hapiRequest: ICreateWithdrawTransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const transaction = await transactionService.createWithdrawTransaction({
        ...hapiRequest.payload,
        sourceCustomerId: hapiRequest.query.customerId
      });
      const response = mapTransactionResultToResponse(transaction);
      return hapiResponse.response(response).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Created'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const privateCreateWithdrawTransaction: hapi.ServerRoute = {
  ...createWithdrawTransaction,
  path: '/private/withdraw-transactions'
};

const reverseTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/transactions/{transactionId}/revert',
  options: {
    description: 'Private Api to Reverse a transaction',
    notes: 'All information must be valid',
    tags: ['api', 'transactions', 'private'],
    auth: false,
    validate: {
      params: transactionIdValidator,
      payload: reverseRequestValidator
    },
    handler: async (
      hapiRequest: IReverseTransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { params, payload } = hapiRequest;
      await transactionReversalService.reverseTransaction(
        params.transactionId,
        payload
      );
      return hapiResponse.response().code(Http.StatusCode.NO_CONTENT);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.NO_CONTENT]: {
            description: 'Revert transaction successfully'
          },
          [Http.StatusCode.NOT_FOUND]: {
            description: 'Not Found'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const privateReverseTransaction: hapi.ServerRoute = {
  ...reverseTransaction,
  path: '/private/transactions/{transactionId}/revert'
};

const getTransaction: hapi.ServerRoute = {
  method: Http.Method.GET,
  path: '/transactions/{transactionId}',
  options: {
    description:
      'Private Api to get transaction detail by transactionId or externalId',
    notes: 'All information must be valid',
    tags: ['api', 'transactions', 'private'],
    auth: false,
    validate: {
      params: transactionIdValidator
    },
    response: {
      schema: transactionInfoResponseValidator
    },
    handler: async (
      hapiRequest: ITransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { params } = hapiRequest;
      const transaction = await transactionService.getTransaction(
        params.transactionId
      );
      return hapiResponse
        .response(toTransactionDetail(transaction))
        .code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Success'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.NOT_FOUND]: {
            description: 'Not found'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const privateGetTransaction: hapi.ServerRoute = {
  ...getTransaction,
  path: '/private/transactions/{transactionId}'
};

const getTransactionList: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/admin/transactions',
  options: {
    description: 'Admin Api to get transaction list based on filter parameter',
    notes: 'All information must be valid',
    tags: ['api', 'transactions', 'private', 'admin'],
    validate: {
      query: transactionListQueryValidator,
      payload: transactionListRequestValidator
    },
    response: {
      schema: transactionListResponseVaidator
    },
    bind: {
      roles: [AdminPortalUserPermissionCode.CUSTOMER_INFO_WRITE]
    },
    handler: async (
      hapiRequest: ITransactionListRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { payload } = hapiRequest;
      const { roles } = getAuthPayloadFromAdminToken(
        hapiRequest as hapi.Request
      );
      const fetchRefund = hapiRequest.query.fetchRefund === 'true';
      const {
        transactions,
        refundTransactions
      } = await transactionService.getTransactionList(payload, fetchRefund);
      return hapiResponse
        .response({
          ...transactions,
          list: toTransactionList(
            transactions.list,
            roles,
            refundTransactions,
            fetchRefund
          )
        })
        .code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Success'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.NOT_FOUND]: {
            description: 'Not found'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const createBlockingTransferTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/blocking-transfer-transactions',
  options: {
    description: 'Private Api to create a blocking transfer transaction',
    notes:
      'Blocking transfer transaction will not execute the transaction yet but temporary block transaction amount. The available balance of account is adjusted based on the blocked amount. The blocked transaction amount can not be used until the transaction is unblocked',
    tags: ['api', 'transactions', 'private'],
    auth: false,
    validate: {
      query: customerIdValidator,
      payload: createTransferTransactionRequestValidator
    },
    response: {
      schema: blockingTransactionResponseValidator
    },
    handler: async (
      hapiRequest: ICreateTransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { payload, query } = hapiRequest;

      const blockingTransactionResponse = await transactionBlockingService.createBlockingTransferTransaction(
        {
          ...payload,
          sourceCustomerId: query.customerId
        }
      );
      return hapiResponse
        .response({
          id: blockingTransactionResponse.id
        })
        .code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description:
              'Submit blocking transfer transaction request successfully'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const privateCreateBlockingTransferTransaction: hapi.ServerRoute = {
  ...createBlockingTransferTransaction,
  path: '/private/blocking-transfer-transactions'
};

const adjustBlockingTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/blocking-transfer-transactions/{transactionId}/adjust',
  options: {
    description:
      'Private End point to adjust transaction amount of a blocking transfer transaction',
    notes: 'All information must be valid',
    tags: ['api', 'transactions', 'private'],
    auth: false,
    validate: {
      query: cifValidator,
      params: transactionIdValidator,
      payload: blockingTransactionAdjustmentRequestValidator
    },
    handler: async (
      hapiRequest: IBlockingAmountTransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { params, payload, query } = hapiRequest;
      await transactionBlockingService.adjustBlockingTransferTransaction(
        query.cif,
        params.transactionId,
        payload.amount
      );
      return hapiResponse.response().code(Http.StatusCode.NO_CONTENT);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.NO_CONTENT]: {
            description: 'Adjust blocking transaction successfully'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.NOT_FOUND]: {
            description: 'Not found'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const privateAdjustBlockingTransaction: hapi.ServerRoute = {
  ...adjustBlockingTransaction,
  path: '/private/blocking-transfer-transactions/{transactionId}/adjust'
};

const executeBlockingTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/blocking-transfer-transactions/{transactionId}/execute',
  options: {
    description: 'Private Api to execute a blocking transfer transaction',
    notes: 'All information must be valid',
    tags: ['api', 'transactions', 'private'],
    auth: false,
    validate: {
      query: cifValidator,
      params: transactionIdValidator,
      payload: blockingTransactionAdjustmentRequestValidator
    },
    handler: async (
      hapiRequest: IExecuteBlockingTransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { params, query, payload } = hapiRequest;
      await transactionBlockingService.executeBlockingTransferTransaction(
        query.cif,
        params.transactionId,
        payload.amount
      );
      return hapiResponse.response().code(Http.StatusCode.NO_CONTENT);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.NO_CONTENT]: {
            description: 'Execute blocking transaction successfully'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.NOT_FOUND]: {
            description: 'Not found'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const privateExecuteBlockingTransaction: hapi.ServerRoute = {
  ...executeBlockingTransaction,
  path: '/private/blocking-transfer-transactions/{transactionId}/execute'
};

const cancelBlockingTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/blocking-transfer-transactions/{transactionId}/cancel',
  options: {
    description: 'Private Api to cancel blocking transaction',
    notes: 'All information must be valid',
    tags: ['api', 'transactions', 'private'],
    auth: false,
    validate: {
      query: cifValidator,
      params: transactionIdValidator
    },
    handler: async (
      hapiRequest: ICancelBlockingTransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { params, query } = hapiRequest;
      await transactionBlockingService.cancelBlockingTransferTransaction(
        query.cif,
        params.transactionId
      );
      return hapiResponse.response().code(Http.StatusCode.NO_CONTENT);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.NO_CONTENT]: {
            description: 'cancel blocking transaction successfully'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.NOT_FOUND]: {
            description: 'Not found'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error.'
          }
        }
      }
    }
  }
};

const privateCancelBlockingTransaction: hapi.ServerRoute = {
  ...cancelBlockingTransaction,
  path: '/private/blocking-transfer-transactions/{transactionId}/cancel'
};

const manualAdjustmentUnBlockingTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/manual-adjustment/unblock/{transactionId}',
  options: {
    description: 'Admin Api to manually adjustment unblock amount',
    notes: 'All information must be valid',
    tags: ['api', 'transactions'],
    bind: {
      roles: [AdminPortalUserPermissionCode.CUSTOMER_INFO_WRITE]
    },
    validate: {
      params: transactionIdValidator
    },
    handler: async (
      hapiRequest: IManualUnBlockingTransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { params } = hapiRequest;
      await transactionBlockingService.manualUnblockingTransferTransaction(
        params.transactionId
      );
      return hapiResponse.response().code(Http.StatusCode.NO_CONTENT);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.NO_CONTENT]: {
            description: 'manual adjustment unblock amount successfully'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.NOT_FOUND]: {
            description: 'Not found'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error.'
          }
        }
      }
    }
  }
};

const confirmTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/transaction-confirmation',
  options: {
    description: 'Private Api to Confirm transaction',
    notes:
      'All information must be valid. This endpoint is for switching to callback when they finish processing the transaction request',
    tags: ['api', 'transactions'],
    auth: false,
    validate: {
      query: confirmTransactionQueryValidator,
      payload: confirmTransactionRequestValidator
    },
    response: {
      schema: confirmTransactionResponseValidator
    },
    handler: async (
      hapiRequest: IConfirmTransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      try {
        const { payload, query } = hapiRequest;

        await updateRequestIdInContext(payload.externalId);

        const confirmTransactionResponse = await switchingTransactionService.confirmTransaction(
          payload
        );

        //reversal for entitlement counterUsage (by transaction confirmation status)
        if (await entitlementFlag.isEnabled()) {
          await entitlementService.processReversalEntitlementCounterUsageOnTransactionConfirmation(
            payload
          );
        }

        //reversal for atm withdrawal - daily limit
        //it will check based on the transaction confirmation status - non successful status
        transactionUsageService.processReversalPocketLimitByTransactionConfirmationInput(
          payload
        );

        const response = mapConfirmTransactionResultToExternalResponse(
          confirmTransactionResponse,
          query
        );

        return hapiResponse.response(response).code(Http.StatusCode.OK);
      } catch (e) {
        //reversal for entitlement counterUsage due to error being thrown
        if (await entitlementFlag.isEnabled()) {
          await entitlementService.processReversalEntitlementCounterUsageBasedOnContext();
        }

        //reversal for entitlement counterUsage due to error being thrown
        transactionUsageService.processReversalPocketLimitByTransactionBasedOnContext();

        return e;
      }
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description:
              'Process the transaction confirmation request successfully'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const extTxnManualAdjstment: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/admin/manual-adjustment',
  options: {
    description:
      'Admin api is used by admin portal to mark pending / MANUAL_INTERVENTION transaction as successful / failed',
    notes: 'All information must be valid',
    tags: ['private', 'api', 'admin'],
    bind: {
      roles: [AdminPortalUserPermissionCode.CUSTOMER_INFO_WRITE]
    },
    validate: {
      payload: extTxnManualAdjstmentRequestValidator
    },
    handler: async (
      hapiRequest: IExtTxnManualAdjstmentRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const {
        transactionId,
        status: reqStatus,
        interchange: interchange
      } = hapiRequest.payload;
      logger.info(
        `extTxnManualAdjstment: Manually updating transaction ${transactionId}, status: ${reqStatus}, interchange: ${interchange}`
      );
      const txnObject = await transactionService.getTransaction(transactionId);
      logger.info(
        `extTxnManualAdjstment: Updating transaction ${transactionId}, 
        oldTransactionstatus: ${txnObject.status}, 
        channel ${txnObject.beneficiaryBankCodeChannel}, sourceAccountNo. ${txnObject.sourceAccountNo},
        beneficiaryAccountNo. ${txnObject.beneficiaryAccountName}, externalId : ${txnObject.externalId}`
      );
      validateTransactionForManualAdjustment(txnObject, reqStatus, interchange);
      const txn =
        interchange &&
        txnObject.beneficiaryBankCodeChannel === BankChannelEnum.EXTERNAL
          ? { ...txnObject, interchange: interchange }
          : txnObject;
      if (
        txn.beneficiaryBankCodeChannel === BankChannelEnum.EXTERNAL ||
        txn.beneficiaryBankCodeChannel === BankChannelEnum.IRIS ||
        txn.beneficiaryBankCodeChannel === BankChannelEnum.WINCOR ||
        txn.beneficiaryBankCodeChannel === BankChannelEnum.TOKOPEDIA ||
        txn.beneficiaryBankCodeChannel === BankChannelEnum.QRIS
      ) {
        await txnManualAdjustmentService.extTxnManualAdjstment(txn, reqStatus);
      }

      if (txn.beneficiaryBankCodeChannel === BankChannelEnum.GOBILLS) {
        await billPaymentService.txnManualAdjustment(
          transactionId,
          reqStatus,
          txnObject.status != TransactionStatus.DECLINED
        );
      }
      logger.info(
        `Manual adjustment completed for transaction id ${txn.id} and external id : ${txn.externalId}`
      );
      return hapiResponse.response({ success: true }).code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Transaction manually adjusted'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.NOT_FOUND]: {
            description: 'Not found'
          }
        }
      }
    }
  }
};

const createExternalTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/external-transactions',
  options: {
    description: 'Private Api to Create a external transfer transaction',
    notes:
      'This api is to handle any transaction that come from 3rd application',
    tags: ['api', 'transactions'],
    auth: false,
    validate: {
      headers: externalTransactionHeaderValidator,
      query: externalTransactionQueryValidator,
      payload: externalTransactionRequestValidator
    },
    response: {
      schema: externalTransactionResponseValidator
    },
    handler: async (
      hapiRequest: IIncomingTransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { payload, query, headers } = hapiRequest;

      try {
        const includeCoreBankingTransactions =
          query.includeCoreBankingTransactions === 'true';
        const idempotencyKey =
          headers[HttpHeaders.X_IDEMPOTENCY_KEY.toLowerCase()];

        const transaction = await externalTransactionService.createExternalTransaction(
          payload,
          idempotencyKey
        );
        const response = mapTransactionResultToExternalResponse(
          transaction,
          includeCoreBankingTransactions
        );

        return hapiResponse.response(response).code(Http.StatusCode.CREATED);
      } catch (e) {
        if (await entitlementFlag.isEnabled()) {
          await entitlementService.processReversalEntitlementCounterUsageBasedOnContext();
        }

        //check whether the flag is true for pocket level limit validation
        //perform reversal
        transactionUsageService.processReversalPocketLimitByTransactionBasedOnContext();

        return e;
      }
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Transfer transaction is created'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const getExternalTransactionFee: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/external-transaction-fee',
  options: {
    description: 'Private Api to calculate transaction fee',
    notes: 'All information must be valid',
    tags: ['api', 'transactions'],
    auth: false,
    validate: {
      payload: externalTransactionFeeRequestValidator
    },
    response: {
      schema: incomingFeeCheckResponseValidator
    },
    handler: async (
      hapiRequest: IIncomingTransactionFeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { payload } = hapiRequest;

      const response = await externalTransactionService.getExternalTransactionFee(
        payload
      );

      return hapiResponse.response(response).code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'OK'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const refundTransferTransation: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/admin/refund/{transactionId}',
  options: {
    description: 'Admin Api to Refund transaction by transaction id',
    notes: 'All information must be valid',
    tags: ['api', 'transactions', 'refund', 'admin'],
    bind: {
      roles: [AdminPortalUserPermissionCode.CUSTOMER_INFO_WRITE]
    },
    validate: {
      params: refundTransactionRequestValidator,
      query: refundTransactionQueryValidator
    },
    response: {
      schema: refundTransactionResponseValidator
    },
    handler: async (
      hapiRequest: IIncomingRefundTransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { params, query } = hapiRequest;

      const response = await generalRefundService.refundTransferTransaction(
        params.transactionId,
        query.refundFee
      );

      return hapiResponse.response(response).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Refund transaction successfully created'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const checkTransactionStatus: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/transaction-status',
  options: {
    description: 'Api to check transaction status',
    notes: 'All information must be valid',
    tags: ['api', 'check status', 'transactions'],
    validate: {
      payload: transactionStatusRequestValidator
    },
    response: {
      schema: transactionStatusResponseValidator
    },
    handler: async (
      hapiRequest: ITransactionStatusRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { payload } = hapiRequest;
      await updateRequestIdInContext(payload.paymentInstructionId);
      const result = await transactionService.checkTransactionStatus(payload);
      return hapiResponse.response(result).code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            describe: 'Transaction found'
          },
          [Http.StatusCode.NOT_FOUND]: {
            describe: 'Transaction not found'
          }
        }
      }
    }
  }
};

const confirmTransactionStatus: hapi.ServerRoute = {
  method: Http.Method.GET,
  path: '/private/confirm-transaction-status',
  options: {
    description: 'Api to check and resolve pending transaction status',
    notes: 'All information must be valid',
    tags: ['api', 'update pending status', 'transactions'],
    auth: false,
    handler: async (
      _hapiRequest: hapi.Request,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      logger.info('Endpoint /private/confirm-transaction-status is triggered.');
      logger.info('Query params: ' + JSON.stringify(_hapiRequest.query));
      let enable = _hapiRequest.query.enable === '1';
      let minTransactionAge = _hapiRequest.query.minTransactionAge
        ? parseInt(_hapiRequest.query.minTransactionAge as string)
        : undefined;
      let maxTransactionAge = _hapiRequest.query.maxTransactionAge
        ? parseInt(_hapiRequest.query.maxTransactionAge as string)
        : undefined;
      let recordLimit = _hapiRequest.query.recordLimit
        ? parseInt(_hapiRequest.query.recordLimit as string)
        : undefined;
      if (enable) {
        await transactionService.resolveTransactionStatus(
          minTransactionAge,
          maxTransactionAge,
          recordLimit
        );
      }
      return hapiResponse
        .response({
          response: 'success',
          enable: enable
        })
        .code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            describe: 'Transaction found'
          },
          [Http.StatusCode.NOT_FOUND]: {
            describe: 'Transaction not found'
          }
        }
      }
    }
  }
};

const checkBulkTransactionFees: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/private/bulk/transaction-fees/v1',
  options: {
    description:
      'Api to get transaction fees for a list of transaction amounts',
    notes: 'All information must be valid',
    tags: ['api', 'fees', 'transactions', 'bulk', 'private'],
    auth: false,
    validate: {
      payload: bulkTransactionFeesRequestValidator
    },
    response: {
      schema: bulkTransactionFeesResponseValidator
    },
    handler: async (
      hapiRequest: IBulkTransactionFeesRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { payload } = hapiRequest;
      const response = await withReadOnlyMode(() =>
        transactionService.checkBulkTransactionFees(payload)
      );

      return hapiResponse.response(response).code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'OK'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const transactionController: hapi.ServerRoute[] = [
  cancelBlockingTransaction,
  adjustBlockingTransaction,
  getTransaction,
  recommendPaymentServices,
  reverseTransaction,
  createTransferTransaction,
  createDepositTransaction,
  createWithdrawTransaction,
  createBlockingTransferTransaction,
  executeBlockingTransaction,
  createInternalTopupTransaction,
  createExternalTopupTransaction,
  confirmTransaction,
  extTxnManualAdjstment,
  createExternalTransaction,
  getExternalTransactionFee,
  recommendPaymentServicesWithoutAuth,
  refundTransferTransation,
  getTransactionList,
  manualAdjustmentUnBlockingTransaction,
  recommendPaymentServicesForAdminPortal,
  privateCreateTransferTransaction,
  privateCreateInternalTopupTransaction,
  privateCreateExternalTopupTransaction,
  privateCreateDepositTransaction,
  privateCreateWithdrawTransaction,
  privateReverseTransaction,
  privateGetTransaction,
  privateCreateBlockingTransferTransaction,
  privateAdjustBlockingTransaction,
  privateExecuteBlockingTransaction,
  privateCancelBlockingTransaction,
  checkTransactionStatus,
  confirmTransactionStatus,
  checkBulkTransactionFees
];

export default transactionController;
