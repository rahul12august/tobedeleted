import logger, { wrapLogs } from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { ITransferTransactionInput } from './transaction.type';
import {
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from './transaction.enum';
import transactionRepository from './transaction.repository';
import transactionHelper from './transaction.helper';
import transactionBlockingHelper from './transactionBlocking.helper';
import transactionService from './transaction.service';
import transactionExecutionService from './transactionExecution.service';
import { ITransactionModel } from './transaction.model';
import coreBankingHelper from './transactionCoreBanking.helper';
import { BankChannelEnum, Enum } from '@dk/module-common';
import blockingAmountService from '../blockingAmount/blockingAmount.service';
import monitoringEventProducer from '../monitoringEvent/monitoringEvent.producer';
import { MonitoringEventType } from '@dk/module-message';
import { REVERSAL_TC_SUFFIX } from './transaction.constant';

const submitBlockingTransaction = async (
  model: ITransactionModel
): Promise<ITransactionModel> => {
  const transaction = await transactionBlockingHelper.createBlockingTransaction(
    model,
    Enum.BlockingAmountType.GINPAY
  );
  return await transactionRepository.update(transaction.id, {
    ...transaction,
    status: TransactionStatus.SUBMITTED
  });
};

const createBlockingTransferTransaction = async (
  transactionInput: ITransferTransactionInput
): Promise<ITransactionModel> => {
  const transactionModel = transactionHelper.buildOutgoingTransactionModel(
    transactionInput
  );
  await transactionHelper.validateExternalIdAndPaymentType(transactionModel);
  const model = await transactionRepository.create(transactionModel);
  monitoringEventProducer.sendMonitoringEventMessage(
    MonitoringEventType.PROCESSING_STARTED,
    model
  );

  const input = await transactionService.populateTransactionInfo(
    { ...transactionInput, ...model, transactionId: model.id },
    bankCode => {
      return bankCode.channel !== BankChannelEnum.WINCOR;
    }
  );

  const createdTransaction = await transactionExecutionService.initTransaction(
    input,
    {
      submitTransaction: submitBlockingTransaction,
      isEligible: () => true
    }
  );

  return createdTransaction;
};

const adjustBlockingTransferTransaction = async (
  cif: string,
  transactionId: string,
  amount: number
): Promise<ITransactionModel> => {
  const transaction = await transactionHelper.getPendingBlockingTransaction(
    transactionId,
    cif
  );
  let updatedTransaction = await transactionRepository.update(transaction.id, {
    journey: transactionHelper.getStatusesToBeUpdated(
      TransferJourneyStatusEnum.ADJUST_BLOCKING_AMOUNT,
      transaction
    )
  });
  logger.info(
    `adjustBlockingTransferTransaction: Updating blocking amount for transactionId: ${transactionId}`
  );
  await blockingAmountService.updateBlockingAmount(
    transaction.sourceAccountNo,
    transaction.blockingId,
    amount,
    transaction.transactionAmount,
    transaction.cardId
  );
  updatedTransaction = await transactionRepository.update(transaction.id, {
    transactionAmount: amount,
    journey: transactionHelper.getStatusesToBeUpdated(
      TransferJourneyStatusEnum.BLOCKING_AMOUNT_ADJUSTED,
      updatedTransaction
    )
  });
  if (updatedTransaction === null) {
    logger.error(
      `adjustBlockingTransferTransaction: failed to update transaction info for txnId ${transaction.id}`
    );
    throw new TransferAppError(
      `Failed to update transaction info for txnId ${transaction.id}.`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION
    );
  }
  return updatedTransaction;
};

const confirmBlockingTransaction = async (
  transaction: ITransactionModel
): Promise<ITransactionModel> => {
  if (
    !(
      transaction.debitTransactionCode && transaction.debitTransactionChannel
    ) ||
    !transaction.beneficiaryAccountNo ||
    !transaction.cardId ||
    !transaction.blockingId ||
    !transaction.sourceAccountNo
  ) {
    logger.error(
      'confirmBlockingTransaction: invalid request, one of field is missing debitTransactionCode, debitTransactionChannel, beneficiaryAccountNo, cardId, blockingId, sourceAccountNo'
    );
    throw new TransferAppError(
      `Invalid request one of the field is missing debitTransactionCode, debitTransactionChannel, beneficiaryAccountNo, cardId, blockingId, sourceAccountNo!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.INVALID_REQUEST
    );
  }

  if (!transaction.debitTransactionCode.endsWith(REVERSAL_TC_SUFFIX)) {
    await coreBankingHelper.submitCardTransaction(
      transaction.cardId,
      transaction.blockingId,
      transaction,
      transaction.debitTransactionCode,
      transaction.debitTransactionChannel
    );
  }

  if (
    transaction.creditTransactionCode &&
    !transaction.creditTransactionCode.endsWith(REVERSAL_TC_SUFFIX) &&
    transaction.creditTransactionChannel
  ) {
    coreBankingHelper.submitCreditTransaction(
      transaction.beneficiaryAccountNo,
      transaction,
      transaction.creditTransactionCode,
      transaction.creditTransactionChannel
    );
  }

  return transaction;
};

const executeBlockingTransferTransaction = async (
  cif: string,
  transactionId: string,
  amount: number
): Promise<ITransactionModel> => {
  const transaction = await transactionHelper.getPendingBlockingTransaction(
    transactionId,
    cif,
    amount
  );

  const updatedTransaction = await transactionExecutionService.settleTransaction(
    transaction,
    {
      submitTransaction: confirmBlockingTransaction,
      isEligible: () => true,
      declineOnSubmissionFailure: false
    }
  );

  return updatedTransaction;
};

const cancelBlockingTransferTransaction = async (
  cif: string,
  transactionId: string
): Promise<void> => {
  const transaction = await transactionHelper.getPendingBlockingTransaction(
    transactionId,
    cif
  );

  await transactionBlockingHelper.cancelBlockingTransaction(transaction);

  // todo await transactionService.updateFailedTransaction(transaction, null);

  await transactionRepository.update(transaction.id, {
    status: TransactionStatus.DECLINED,
    journey: transactionHelper.getStatusesToBeUpdated(
      TransferJourneyStatusEnum.TRANSFER_FAILURE,
      transaction
    )
  });
};

const manualUnblockingTransferTransaction = async (
  transactionId: string
): Promise<void> => {
  const transaction = await transactionHelper.getDeclinedBlockingTransaction(
    transactionId
  );

  await transactionBlockingHelper.cancelBlockingTransaction(transaction);
};

const transactionBlockingService = wrapLogs({
  cancelBlockingTransferTransaction,
  adjustBlockingTransferTransaction,
  createBlockingTransferTransaction,
  executeBlockingTransferTransaction,
  manualUnblockingTransferTransaction
});

export default transactionBlockingService;
