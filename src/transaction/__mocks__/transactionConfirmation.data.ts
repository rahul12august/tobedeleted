import { ITransactionModel } from '../transaction.model';
import {
  TransactionStatus,
  ConfirmTransactionStatus
} from '../transaction.enum';
import { PaymentServiceTypeEnum, BankNetworkEnum } from '@dk/module-common';
import { ExecutionTypeEnum } from '../transaction.enum';
import { ConfirmTransactionRequest } from '../transaction.type';

export const mockConfirmTransactionRequest = (): ConfirmTransactionRequest => {
  return {
    externalId: '0093384848',
    accountNo: '100023993999400000',
    transactionDate: '200212092500',
    responseMessage: 'OK',
    transactionResponseID: '3499488888',
    responseCode: ConfirmTransactionStatus.SUCCESS,
    interchange: BankNetworkEnum.ARTAJASA
  };
};

export const mockTransactionFromDb = (): ITransactionModel => {
  return {
    externalId: 'externalId',
    referenceId: '34839845995-3093059-03434',
    beneficiaryAccountName: 'External Account Name From Switching',
    beneficiaryBankCode: '542',
    sourceAccountName: 'Jago account name',
    sourceBankCode: 'BC002',
    createdAt: new Date('2020-02-12T09:25:00.041Z'),
    status: TransactionStatus.SUBMITTED,
    paymentServiceCode: 'RTOL',
    paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
    transactionAmount: 900000,
    id: '5df72fa3490b56067991ae39',
    executionType: ExecutionTypeEnum.NONE_BLOCKING,
    requireThirdPartyOutgoingId: true,
    entitlementInfo: [
      {
        entitlementCodeRefId: 'entitlementCodeRefId',
        counterCode: 'counterCode',
        customerId: 'customerId'
      }
    ]
  };
};
