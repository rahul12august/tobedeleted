import { getITransactionInput } from './transaction.data';
import { ITransactionInput } from '../transaction.type';
import { BankChannelEnum, PaymentServiceTypeEnum } from '@dk/module-common';
import { Constant } from '@dk/module-common';
import { IPaymentServiceMapping } from '../../configuration/configuration.type';
import { getTransactionCodeInfoData } from '../../configuration/__mocks__/configuration.data';
import { BIFAST_PROXY_BANK_CODE } from '../../bifast/bifast.constant';

export const createTransaction = (): ITransactionInput => {
  const transaction: ITransactionInput = {
    ...getITransactionInput(),
    beneficiaryAccountName: 'invalid',
    beneficiaryBankCode: Constant.GIN_BANK_CODE_ID,
    beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
    beneficiaryBankName: 'bank',
    sourceAccountName: 'invalid',
    sourceAccountType: 'invalid', // should be invalid value by default
    sourceBankCodeChannel: BankChannelEnum.INTERNAL,
    sourceBankCode: Constant.GIN_BANK_CODE_ID,
    sourceTransactionCurrency: 'IDR',
    sourceCustomerId: 'sourceCustomerId'
  };
  return transaction;
};

export const getPaymentServiceMappingData = (): IPaymentServiceMapping => ({
  paymentServiceCode: 'SIT02',
  debitTransactionCode: [
    {
      transactionCode: 'WTD01',
      transactionCodeInfo: {
        ...getTransactionCodeInfoData(),
        code: 'WTD01'
      }
    }
  ],
  creditTransactionCode: [
    {
      transactionCode: 'WTC01',
      transactionCodeInfo: {
        ...getTransactionCodeInfoData(),
        code: 'WTD01'
      }
    }
  ],
  beneficiaryBankCodeType: 'RTOL',
  transactionAuthenticationChecking: true,
  blocking: false,
  requireThirdPartyOutgoingId: false
});

export const getRecommendPaymentServiceTestCases = (): {
  input: ITransactionInput;
  dailyUsage?: {
    limitGroupCode: string;
    dailyAccumulationAmount: number;
    monthlyAccumulationTransaction: number;
  }[];
  output: string[];
}[] => {
  return [
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.WALLET,
        beneficiaryBankCodeChannel: BankChannelEnum.PARTNER,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 7000000,
        externalId: 'dummy123'
      },
      output: ['SIT02']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.WALLET,
        beneficiaryBankCodeChannel: BankChannelEnum.PARTNER,
        sourceAccountType: 'DC',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 7000000,
        externalId: 'dummy123'
      },
      output: ['SIT02']
    },
    {
      input: {
        ...createTransaction(),
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'ANY',
        sourceAccountType: 'MA',
        beneficiaryCIF: '123',
        sourceCIF: '1234',
        transactionAmount: 7000000,
        externalId: 'dummy123',
        additionalInformation1: '1',
        additionalInformation2: '1',
        additionalInformation3: '01',
        additionalInformation4: 'test@jago.com'
      },
      output: ['SIT01']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        sourceAccountType: 'DC',
        beneficiaryAccountType: 'ANY',
        beneficiaryCIF: '123',
        sourceCIF: '1234',
        transactionAmount: 7000000,
        externalId: 'dummy123'
      },
      output: ['SIT01']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'FS',
        sourceAccountType: 'MA',
        beneficiaryCIF: '123',
        sourceCIF: '123',
        transactionAmount: 7000000
      },
      output: ['SAT01']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'FS',
        sourceAccountType: 'DC',
        beneficiaryCIF: '123',
        sourceCIF: '123',
        transactionAmount: 7000000
      },
      output: ['SAT01']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'MA',
        sourceAccountType: 'FS',
        beneficiaryCIF: '123',
        sourceCIF: '123',
        transactionAmount: 7000000
      },
      output: ['SAT02']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'DC',
        sourceAccountType: 'FS',
        beneficiaryCIF: '123',
        sourceCIF: '123',
        transactionAmount: 7000000
      },
      output: ['SAT02']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'DC',
        sourceAccountType: 'MA',
        beneficiaryCIF: '123',
        sourceCIF: '123',
        transactionAmount: 7000000
      },
      output: ['SAT03']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'MA',
        sourceAccountType: 'DC',
        beneficiaryCIF: '123',
        sourceCIF: '123',
        transactionAmount: 7000000
      },
      output: ['SAT04']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 110000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['BIFAST_OUTGOING']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'SMA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 110000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['BIFAST_OUTGOING_SHARIA']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 120000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 700000000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 130000000,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['BIFAST_OUTGOING']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'SMA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 120000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 700000000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 130000000,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['BIFAST_OUTGOING_SHARIA']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 60000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 70000000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['BIFAST_OUTGOING']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'SMA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 60000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 70000000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['BIFAST_OUTGOING_SHARIA']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 600000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 100000000,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['RTGS']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'SMA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 500000001
      },
      dailyUsage: [
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 100000000,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['RTGS_SHARIA']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.GOBILLS,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY'
      },
      output: ['BIL01']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'FS',
        sourceAccountType: 'FS',
        beneficiaryCIF: '123',
        sourceCIF: '123',
        transactionAmount: 7000000
      },
      output: ['SAT05']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'DC',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 30000000
      },
      output: ['BIFAST_OUTGOING']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 30000000
      },
      output: ['BIFAST_OUTGOING']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'FS',
        sourceAccountType: 'FS',
        beneficiaryCIF: '1234',
        sourceCIF: '1234',
        transactionAmount: 30000000
      },
      output: ['SAT05']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.GIN_PAY,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'MA',
        sourceAccountType: 'MA',
        beneficiaryCIF: '123',
        sourceCIF: '1234',
        transactionAmount: 30000000
      },
      output: ['SIT03']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.GIN_PAY,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'MA',
        sourceAccountType: 'DC',
        beneficiaryCIF: '123',
        sourceCIF: '1234',
        transactionAmount: 30000000
      },
      output: ['SIT03']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.MDR,
        beneficiaryAccountType: 'ANY',
        sourceAccountType: 'MA',
        transactionAmount: 30000000
      },
      output: ['MDR']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.CASHBACK,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        sourceAccountType: 'ANY',
        beneficiaryAccountType: 'MA',
        transactionAmount: 9000000
      },
      dailyUsage: [],
      output: ['CASHBACK']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.WALLET,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 5000000
      },
      output: ['RTOL_WALLET']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.PAYME,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        beneficiaryCIF: '123',
        sourceCIF: '1234',
        transactionAmount: 5000000
      },
      output: ['PAYME']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.CREDIT_CARD,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 5000000
      },
      output: ['RTOL_CC']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        sourceCIF: undefined,
        paymentServiceType: PaymentServiceTypeEnum.WALLET,
        beneficiaryBankCodeChannel: BankChannelEnum.PARTNER,
        sourceAccountType: 'ANY',
        transactionAmount: 9000000
      },
      output: ['VA_TOPUP_EXTERNAL']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        sourceCIF: '123',
        paymentServiceType: PaymentServiceTypeEnum.WALLET,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        sourceAccountType: 'ANY',
        beneficiaryAccountType: 'MA',
        transactionAmount: 9000000
      },
      output: ['VA_WITHDRAW']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TD_SHARIA_PLACEMENT,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        beneficiaryCIF: '123',
        sourceCIF: '123',
        transactionAmount: 7000000
      },
      output: ['SAT08']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TD_SHARIA_REPAYMENT,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'MA',
        beneficiaryCIF: '123',
        sourceCIF: '123',
        sourceAccountType: 'ANY',
        transactionAmount: 7000000
      },
      output: ['SAT09']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        beneficiaryBankCode: 'BC005',
        beneficiaryCIF: '123',
        sourceCIF: '123',
        sourceAccountType: 'MA',
        transactionAmount: 7000000
      },
      output: ['BIFAST_OUTGOING']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        beneficiaryBankCode: 'BC005',
        beneficiaryCIF: '123',
        sourceCIF: '123',
        sourceAccountType: 'SMA',
        transactionAmount: 7000000
      },
      output: ['BIFAST_OUTGOING_SHARIA']
    },
    {
      input: {
        ...createTransaction(),
        paymentServiceType: PaymentServiceTypeEnum.INCOMING_BIFAST,
        sourceBankCodeChannel: BankChannelEnum.EXTERNAL,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'MA',
        beneficiaryCIF: '123',
        sourceCIF: '123',
        transactionAmount: 7000000
      },
      output: ['BIFAST_INCOMING']
    },
    {
      input: {
        ...createTransaction(),
        paymentServiceType: PaymentServiceTypeEnum.INCOMING_BIFAST,
        sourceBankCodeChannel: BankChannelEnum.EXTERNAL,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'SMA',
        beneficiaryCIF: '123',
        sourceCIF: '123',
        transactionAmount: 7000000
      },
      output: ['BIFAST_INCOMING_SHARIA']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.VOID_BIFAST,
        sourceBankCodeChannel: BankChannelEnum.EXTERNAL,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'SMA',
        beneficiaryCIF: '123',
        sourceCIF: '123',
        transactionAmount: 7000000
      },
      output: ['REFUND_GENERAL']
    }
  ];
};

export const getNonBiFastRecommendPaymentServiceTestCases = (): {
  input: ITransactionInput;
  dailyUsage?: {
    limitGroupCode: string;
    dailyAccumulationAmount: number;
    monthlyAccumulationTransaction: number;
  }[];
  output: string[];
}[] => {
  return [
    {
      input: {
        ...createTransaction(),
        beneficiaryBankCode: BIFAST_PROXY_BANK_CODE,
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 110000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: []
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 110000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['SKN', 'RTGS']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'SMA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 110000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['SKN_SHARIA']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 120000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 700000000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 130000000,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['RTGS']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'SMA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 500000003
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 130000000,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['RTGS_SHARIA']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 60000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 70000000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['SKN']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'SMA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 100000002
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['SKN_SHARIA']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.GOBILLS,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY'
      },
      output: ['BIL01']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
        beneficiaryAccountType: 'FS',
        sourceAccountType: 'FS',
        beneficiaryCIF: '123',
        sourceCIF: '123',
        transactionAmount: 7000000
      },
      output: ['SAT05']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'DC',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 30000000
      },
      output: ['RTOL']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 30000000
      },
      output: ['RTOL']
    }
  ];
};

const getBiFastByAdditionalInformationRecommendPaymentServiceTestCases = (): {
  input: ITransactionInput;
  dailyUsage?: {
    limitGroupCode: string;
    dailyAccumulationAmount: number;
    monthlyAccumulationTransaction: number;
  }[];
  output: string[];
}[] => {
  return [
    {
      input: {
        ...createTransaction(),
        beneficiaryBankCode: 'BC005',
        additionalInformation3: '02',
        additionalInformation4: 'test@email.com',
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 110000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['BIFAST_OUTGOING']
    },
    {
      input: {
        ...createTransaction(),
        beneficiaryBankCode: 'BC005',
        additionalInformation3: '01',
        additionalInformation4: '+1234567890',
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 250000001
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 10
        }
      ],
      output: []
    },
    {
      input: {
        ...createTransaction(),
        beneficiaryBankCode: 'BC005',
        additionalInformation3: '00',
        additionalInformation4: '123123123123',
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 25000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 250000000,
          monthlyAccumulationTransaction: 10
        }
      ],
      output: ['RTOL']
    },
    {
      input: {
        ...createTransaction(),
        beneficiaryBankCode: 'BC005',
        additionalInformation3: '02',
        additionalInformation4: 'test@email.com',
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 110000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 250000000,
          monthlyAccumulationTransaction: 10
        }
      ],
      output: []
    },
    {
      input: {
        ...createTransaction(),
        beneficiaryBankCode: 'BC005',
        additionalInformation3: '',
        additionalInformation4: '',
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 110000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 100000000,
          monthlyAccumulationTransaction: 10
        }
      ],
      output: ['BIFAST_OUTGOING']
    },
    {
      input: {
        ...createTransaction(),
        beneficiaryBankCode: 'BC005',
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 50000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 250000000,
          monthlyAccumulationTransaction: 10
        }
      ],
      output: ['RTOL']
    },
    {
      input: {
        ...createTransaction(),
        beneficiaryBankCode: 'BC005',
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 100000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 250000000,
          monthlyAccumulationTransaction: 10
        }
      ],
      output: ['SKN']
    }
  ];
};

const getBiFastProxyRecommendPaymentServiceTestCases = (): {
  input: ITransactionInput;
  dailyUsage?: {
    limitGroupCode: string;
    dailyAccumulationAmount: number;
    monthlyAccumulationTransaction: number;
  }[];
  output: string[];
}[] => {
  return [
    {
      input: {
        ...createTransaction(),
        beneficiaryBankCode: BIFAST_PROXY_BANK_CODE,
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 110000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['BIFAST_OUTGOING']
    },
    {
      input: {
        ...createTransaction(),
        beneficiaryBankCode: BIFAST_PROXY_BANK_CODE,
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 250000001
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 10
        }
      ],
      output: []
    },
    {
      input: {
        ...createTransaction(),
        beneficiaryBankCode: 'BC005',
        additionalInformation3: '00',
        additionalInformation4: '123123123123',
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 25000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 250000000,
          monthlyAccumulationTransaction: 10
        }
      ],
      output: ['RTOL']
    },
    {
      input: {
        ...createTransaction(),
        beneficiaryBankCode: BIFAST_PROXY_BANK_CODE,
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 110000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 250000000,
          monthlyAccumulationTransaction: 10
        }
      ],
      output: []
    }
  ];
};

export const getBiFastRecommendPaymentServiceTestCases = (): {
  input: ITransactionInput;
  dailyUsage?: {
    limitGroupCode: string;
    dailyAccumulationAmount: number;
    monthlyAccumulationTransaction: number;
  }[];
  output: string[];
}[] => {
  return [
    ...getBiFastByAdditionalInformationRecommendPaymentServiceTestCases(),
    ...getBiFastProxyRecommendPaymentServiceTestCases()
  ];
};

export const getAutoRouteRecommendPaymentServiceTestCases = (): {
  input: ITransactionInput;
  dailyUsage?: {
    limitGroupCode: string;
    dailyAccumulationAmount: number;
    monthlyAccumulationTransaction: number;
  }[];
  output: string[];
}[] => {
  return [
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 25000,
        beneficiaryBankCode: 'BC005',
        additionalInformation3: '00',
        additionalInformation4: '123123123123',
        preferedBankChannel: BankChannelEnum.EXTERNAL
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 10
        }
      ],
      output: ['RTOL']
    },
    {
      input: {
        ...createTransaction(),
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 250000001,
        beneficiaryBankCode: 'BC005',
        additionalInformation3: '00',
        additionalInformation4: '123123123123',
        preferedBankChannel: BankChannelEnum.EXTERNAL
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['SKN', 'RTGS']
    },
    {
      input: {
        ...createTransaction(),
        beneficiaryBankCode: BIFAST_PROXY_BANK_CODE,
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.TRANSFER,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'MA',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 25000,
        preferedBankChannel: BankChannelEnum.EXTERNAL
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 10
        }
      ],
      output: []
    }
  ];
};

export const getRDNRecommendPaymentServiceTestCases = (): {
  input: ITransactionInput;
  dailyUsage?: {
    limitGroupCode: string;
    dailyAccumulationAmount: number;
    monthlyAccumulationTransaction: number;
  }[];
  output: string[];
}[] => {
  return [
    {
      input: {
        ...createTransaction(),
        beneficiaryBankCode: 'BC006',
        beneficiarySupportedChannels: [
          BankChannelEnum.BIFAST,
          BankChannelEnum.EXTERNAL
        ],
        paymentServiceType: PaymentServiceTypeEnum.RDN,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        sourceAccountType: 'RDS',
        beneficiaryAccountType: 'ANY',
        transactionAmount: 110000000
      },
      dailyUsage: [
        {
          limitGroupCode: 'L002',
          dailyAccumulationAmount: 0,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L003',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L004',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        },
        {
          limitGroupCode: 'L019',
          dailyAccumulationAmount: 10000,
          monthlyAccumulationTransaction: 0
        }
      ],
      output: ['SKN_SHARIA']
    }
  ];
};
