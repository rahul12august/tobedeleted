import { mockToken } from '../../plugins/__mocks__/tokenAuth.data';
import { PREFIX_TOKEN } from '../../common/constant';

export const invalidHeader = {
  Authorization: `Basic ${mockToken}`
};

export const validHeader = {
  Authorization: `${PREFIX_TOKEN}${mockToken}`
};
