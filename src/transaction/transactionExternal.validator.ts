import logger, { wrapLogs } from '../logger';
import { ITransactionModel } from './transaction.model';
import { BaseExternalTransactionInput } from './transaction.type';
import { ERROR_CODE } from '../common/errors';
import { AppError } from '../errors/AppError';
import { split, head, isNumber } from 'lodash';
import { TransactionStatus } from './transaction.enum';

const logAndThrow = (message: string): void => {
  logger.error(message);
  throw new AppError(ERROR_CODE.INVALID_REFUND_REQUEST);
};

const validateRefundRequest = (
  payload: BaseExternalTransactionInput,
  originalTransaction: ITransactionModel
): void => {
  if (
    head(split(originalTransaction.paymentServiceType, '_')) !==
    head(split(payload.paymentServiceType, '_'))
  ) {
    logAndThrow(
      `validateRefundRequest: paymentServiceType missmatch ${originalTransaction.paymentServiceType}`
    );
  }

  if (originalTransaction.status !== TransactionStatus.SUCCEED) {
    logAndThrow(
      `validateRefundRequest: Invalid Transaction Status ${originalTransaction.status}.`
    );
  }
  if (payload.beneficiaryAccountNo !== originalTransaction.sourceAccountNo) {
    logAndThrow(`validateRefundRequest: Account mismatch.`);
  }
  // TODO: (Dikshit)commenting it for now can be enabled later once we will get nore calrity
  // if (
  //   payload.beneficiaryBankCode !== originalTransaction.sourceBankCode ||
  //   payload.sourceBankCode !== originalTransaction.beneficiaryBankCode
  // ) {
  //   logAndThrow(`validateRefundRequest: Bank Code mismatch.`);
  // }
  if (payload.transactionAmount > originalTransaction.transactionAmount) {
    logAndThrow(
      `validateRefundRequest: Transaction Amount greater than original transaction Amount.`
    );
  }
  if (
    originalTransaction.refund &&
    isNumber(originalTransaction.refund.remainingAmount) &&
    payload.transactionAmount > originalTransaction.refund.remainingAmount
  ) {
    logAndThrow(
      `validateRefundRequest: Refund amount mismatch, refund.remainingAmount:${originalTransaction.refund.remainingAmount}`
    );
  }

  logger.info(
    `validateRefundRequest: Validated refunded transaction of originalTransaction:${originalTransaction.id} complete.`
  );
};

const transactionExternalValidator = wrapLogs({
  validateRefundRequest
});

export default transactionExternalValidator;
