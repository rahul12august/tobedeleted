import { BankChannelEnum } from '@dk/module-common';
import { isEmpty } from 'lodash';
import { wrapLogs } from '../logger';
import { UsageCounter } from '../award/award.type';
import configurationService from '../configuration/configuration.service';
import {
  ILimitGroup,
  IPaymentConfigRule,
  IPaymentServiceMapping
} from '../configuration/configuration.type';
import limitGroup from '../limitGroup/limitGroup';
import logger from '../logger';
import ruleConditionFns from './transaction.conditions';
import { filterByDateAndWorkingTime } from './transaction.conditions';
import { denyOCT } from './octVerifyer';

import {
  BILL_LIMIT_GROUP_CODE,
  BIFAST_LIMIT_GROUP_CODE
} from './transaction.constant';

import {
  DailyUsages,
  ExecutionTime,
  ILimitGroupCodesMap,
  ITransactionInput,
  IBiFastEnabled,
  ITransaction,
  RuleConditionStatus,
  FilteredMappingResult
} from './transaction.type';

import customerCache from '../customer/customer.cache';
import { CustomerType } from '../customer/customer.enum';
import { isReadOnlyMode } from './transaction.util';

const buildLimitGroupCodesMap = (
  limitGroups: ILimitGroup[]
): ILimitGroupCodesMap => {
  return limitGroups.reduce(
    (acc: ILimitGroupCodesMap, limitGroup: ILimitGroup) => {
      acc[limitGroup.code] = limitGroup;
      return acc;
    },
    {}
  );
};

const getDailyUsages = (
  usageCounters: UsageCounter[],
  paymentServiceCode?: string
): DailyUsages => {
  logger.info(
    `getDailyUsages: getting usage counter for paymentServiceCode: ${paymentServiceCode}`
  );
  const dailyUsages: DailyUsages = {
    rtgsDailyUsage: 0,
    rtolDailyUsage: 0,
    sknDailyUsage: 0,
    billDailyUsage: 0,
    bifastDailyUsage: 0
  };

  const {
    SKN_LIMIT_GROUP_CODE,
    RTOL_LIMIT_GROUP_CODE,
    RTGS_LIMIT_GROUP_CODE
  } = limitGroup.getLimitGroupCodes(paymentServiceCode);

  usageCounters.forEach(counter => {
    const usage = counter.dailyAccumulationAmount;
    switch (counter.limitGroupCode) {
      case SKN_LIMIT_GROUP_CODE:
        dailyUsages.sknDailyUsage = usage;
        break;
      case RTOL_LIMIT_GROUP_CODE:
        dailyUsages.rtolDailyUsage = usage;
        break;
      case RTGS_LIMIT_GROUP_CODE:
        dailyUsages.rtgsDailyUsage = usage;
        break;
      case BILL_LIMIT_GROUP_CODE:
        dailyUsages.billDailyUsage = usage;
        break;
      case BIFAST_LIMIT_GROUP_CODE:
        dailyUsages.bifastDailyUsage = usage;
        break;
      default:
        break;
    }
  });

  return dailyUsages;
};

const fetchIsShariaCustomer = async (
  data: ITransactionInput
): Promise<boolean> => {
  const customerType =
    data.sourceCustomerId &&
    (await customerCache.getCustomerType(data.sourceCustomerId));

  return customerType === CustomerType.INDIVIDUAL_SHARIA;
};

const isExternalChannel = (channelType?: BankChannelEnum): boolean => {
  return !isEmpty(channelType) && channelType == BankChannelEnum.EXTERNAL;
};

const calcBiFastEnabled = async (
  transactionInput: ITransactionInput
): Promise<boolean> => {
  let isBiFastEnabled = await configurationService.isBiFastEnabled();
  if (
    isBiFastEnabled &&
    isExternalChannel(transactionInput.preferedBankChannel)
  ) {
    logger.info(
      `Rerouting transaction from BIFAST to External for BankCode: ${transactionInput.beneficiaryBankCode}`
    );
    isBiFastEnabled = false;
  }
  return isBiFastEnabled;
};

enum NO_RECOMMENDATION_PAYMENT_SERVICE_CODE {
  SKN_FOR_BUSINESS = 'SKN_FOR_BUSINESS',
  RTOL_FOR_BUSINESS = 'RTOL_FOR_BUSINESS',
  RTGS_FOR_BUSINESS = 'RTGS_FOR_BUSINESS',
  BIFAST_FOR_BUSINESS = 'BIFAST_FOR_BUSINESS'
}

const isAutoRoutingEnabled = (paymentServiceCode?: string): boolean => {
  if (!paymentServiceCode) {
    return true;
  }
  const found = Object.values(NO_RECOMMENDATION_PAYMENT_SERVICE_CODE).find(
    businessPsc => businessPsc === paymentServiceCode
  );
  return !found;
};

const filterPaymentConfigRulesForBusinessPSCs = (
  paymentConfigRules: IPaymentConfigRule[],
  paymentServiceCode?: string
): IPaymentConfigRule[] => {
  if (isAutoRoutingEnabled(paymentServiceCode)) {
    return paymentConfigRules;
  }
  if (!paymentServiceCode) {
    throw new Error('Internal error at filterByPaymentServiceCode');
  }
  return paymentConfigRules.filter(pcr =>
    pcr.serviceRecommendation.includes(paymentServiceCode)
  );
};

/**
 * Retruns the IPaymentServiceMapping[] from first paymentConfigRule
 * that matches to all properties of the current transaction.
 */
const inferMatchingPaymentServiceMappings = async (
  paymentConfigRules: IPaymentConfigRule[],
  pscToDailyLimits: ILimitGroupCodesMap,
  currentTransaction: ITransaction &
    DailyUsages &
    ExecutionTime &
    IBiFastEnabled
): Promise<IPaymentServiceMapping[]> => {
  let configPassed;
  for (let paymentConfigRule of paymentConfigRules) {
    configPassed = true;
    for (let ruleFn of ruleConditionFns) {
      const status: RuleConditionStatus = ruleFn(
        currentTransaction,
        paymentConfigRule,
        pscToDailyLimits
      );
      if (!status.isSuccess()) {
        logger.info(
          `inferMatchingPaymentServiceMappings: Service rule evaluation failed for config: ${
            paymentConfigRule.paymentServiceType
          }, code: ${JSON.stringify(
            paymentConfigRule.serviceRecommendation
          )}, rule: ${ruleFn.name}, message: ${status.message}`
        );
        if (!isAutoRoutingEnabled(currentTransaction.paymentServiceCode)) {
          throw status.toError();
        }
        configPassed = false;
        break;
      }
    }
    if (configPassed) {
      if (isReadOnlyMode()) {
        return paymentConfigRule.paymentServiceMappings;
      }

      const result: FilteredMappingResult = await filterByDateAndWorkingTime(
        paymentConfigRule.paymentServiceMappings,
        currentTransaction.date
      );
      logger.info(
        `inferMatchingPaymentServiceMappings: Passed config serviceType: ${paymentConfigRule.paymentServiceType}, 
        service code: ${paymentConfigRule.paymentServiceMappings[0].paymentServiceCode}
          ${result.status.message}`
      );

      if (
        isEmpty(result.mappings) &&
        !isAutoRoutingEnabled(currentTransaction.paymentServiceCode)
      ) {
        throw result.status.toError();
      }

      return result.mappings;
    }
  }
  return [];
};

const getPaymentServiceMappings = async (
  transactionInput: ITransactionInput,
  paymentConfigRules: IPaymentConfigRule[],
  limitGroups: ILimitGroup[],
  usageCounters: UsageCounter[],
  executionDate: Date = new Date()
): Promise<IPaymentServiceMapping[]> => {
  denyOCT(transactionInput);

  const limitGroupCodesMap = buildLimitGroupCodesMap(limitGroups);
  const dailyUsages = getDailyUsages(
    usageCounters,
    transactionInput.paymentServiceCode
  );

  const isBiFastEnabled: boolean = await calcBiFastEnabled(transactionInput);
  const isShariaCustomer: boolean = await fetchIsShariaCustomer(
    transactionInput
  );

  const ruleInput = {
    ...transactionInput,
    ...dailyUsages,
    ...{ isBiFastEnabled, isShariaCustomer },
    date: executionDate
  };

  const paymentConfigRulesToUse = filterPaymentConfigRulesForBusinessPSCs(
    paymentConfigRules,
    transactionInput.paymentServiceCode
  );

  return inferMatchingPaymentServiceMappings(
    paymentConfigRulesToUse,
    limitGroupCodesMap,
    ruleInput
  );
};

export default wrapLogs({
  getPaymentServiceMappings
});
