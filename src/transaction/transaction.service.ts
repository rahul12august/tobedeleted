import logger, { wrapLogs } from '../logger';
import {
  BankChannelEnum,
  BankNetworkEnum,
  Constant,
  PaymentServiceTypeEnum
} from '@dk/module-common';
import {
  IBankCode,
  ITransactionCodeInfo,
  ITransactionCodeMapping
} from '../configuration/configuration.type';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE, ErrorList } from '../common/errors';
import {
  checkTransferSameCifForMigratingAccounts,
  getErrorMessage,
  isBIFastBank,
  isExternalBank,
  mapTransactionModelToRefundTransaction
} from './transaction.util';
import {
  BulkTransactionFeesInput,
  BulkTransactionFeesResponse,
  IDepositTransactionInput,
  IPagingResult,
  IRefundTransactionBasicDetail,
  ITopupInternalTransactionInput,
  ITransactionAccount,
  ITransactionExecutionHooks,
  ITransactionInput,
  ITransactionStatusResponse,
  ITransferTransactionInput,
  IWithdrawTransactionInput,
  RecommendationService,
  RecommendPaymentServiceInput,
  TransactionListRequest,
  TransactionStatusInput
} from './transaction.type';
import { ITransactionModel } from './transaction.model';
import transactionExecutionService, {
  defaultExecutionHooks
} from './transactionExecution.service';
import virtualAccountRepository from '../virtualAccount/virtualAccount.repository';
import recommendedCodeService from './transactionRecommend.service';
import transactionRepository from './transaction.repository';
import transactionHelper from './transaction.helper';
import transactionProducer from './transaction.producer';
import _, { isEmpty, startsWith } from 'lodash';
import {
  MonitoringEventType,
  PaymentInstructionTopicConstant
} from '@dk/module-message';
import {
  PhoneCode,
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from './transaction.enum';
import transferConstant, {
  BANK_KSEI,
  JAGO_BANK_CODE,
  SCHEDULED_RESOLVE_STATUS_FETCH_RECORD_LIMIT,
  SCHEDULED_RESOLVE_STATUS_MAX_TRANSACTION_AGE_IN_MINUTES,
  SCHEDULED_RESOLVE_STATUS_MIN_TRANSACTION_AGE_IN_MINUTES,
  SCHEDULED_RESOLVE_STATUS_TRANSACTION_CODES
} from './transaction.constant';
import { ICustomerInfoWithCif } from '../customer/customer.type';
import customerService from '../customer/customer.service';
import { toDate } from '../common/dateUtils';
import { BeneficiaryProxyType } from '../bifast/bifast.enum';
import { QRIS_PAYMENT_SERVICE_CODE } from '../alto/alto.constant';
import qrisService from '../alto/alto.service';
import monitoringEventProducer from '../monitoringEvent/monitoringEvent.producer';
import { FeeAmountInput, IFeeAmountResponse } from '../fee/fee.type';
import feeHelper from '../fee/fee.helper';
import feeBatch from '../fee/fee.batch';
import { UsageCounter } from '../award/award.type';

const {
  BENEFICIARY_BANK_CODE_COL,
  SOURCE_BANK_CODE_COL,
  SOURCE_CIF_COL,
  BENEFICIARY_ACCOUNT_NO_COL
} = transferConstant;

const findByIdAndUpdate = async (
  txnId: string,
  transactionUpdate: Partial<ITransactionModel>
): Promise<ITransactionModel | null> => {
  const transaction = await transactionRepository.findByIdAndUpdate(
    txnId,
    transactionUpdate
  );
  if (!transaction) {
    logger.error(`No transaction found with transactionId:${txnId}`);
    throw new TransferAppError(
      `No transaction found with transactionId:${txnId}`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.TRANSACTION_NOT_FOUND
    );
  }
  return transaction;
};

const updateFailedTransaction = async (
  transactionInput: ITransactionInput,
  error: TransferAppError,
  transactionStatus: TransactionStatus = TransactionStatus.DECLINED,
  transferJourneyStatus: TransferJourneyStatusEnum = TransferJourneyStatusEnum.TRANSFER_FAILURE
): Promise<ITransactionModel | null> => {
  let updatedTransaction = null;
  if (transactionInput.transactionId) {
    updatedTransaction = await findByIdAndUpdate(
      transactionInput.transactionId,
      {
        status: transactionStatus,
        journey: transactionHelper.getStatusesToBeUpdated(
          transferJourneyStatus,
          transactionInput
        ),
        failureReason: {
          type: ERROR_CODE[error.errorCode as keyof typeof ERROR_CODE],
          actor: error.actor,
          detail: error.detail
        },
        ownerCustomerId: transactionInput.ownerCustomerId
      }
    );
  }
  return updatedTransaction;
};

function buildRejectedHandler(
  actor: TransferFailureReasonActor,
  detail: string,
  key: string
) {
  return (error: { errorCode?: string }) => {
    throw new TransferAppError(
      detail,
      actor,
      ERROR_CODE[error.errorCode as keyof typeof ERROR_CODE],
      [getErrorMessage(key)]
    );
  };
}

const getBankInfo = (input: ITransactionInput) =>
  Promise.all([
    transactionHelper
      .getBankCodeInfo(input.sourceBankCode)
      .catch(
        buildRejectedHandler(
          TransferFailureReasonActor.MS_TRANSFER,
          `Failed to get source bank code info for: ${input.sourceBankCode}!`,
          SOURCE_BANK_CODE_COL
        )
      ),
    transactionHelper
      .getBankCodeInfo(input.beneficiaryBankCode)
      .catch(
        buildRejectedHandler(
          TransferFailureReasonActor.MS_TRANSFER,
          `Failed to get beneficiary bank code info for: ${input.beneficiaryBankCode}!`,
          BENEFICIARY_BANK_CODE_COL
        )
      )
  ]);

const getTransactionAmount = (
  transactionAmount: number,
  paymentServiceCode?: string
) =>
  paymentServiceCode &&
  (paymentServiceCode.includes('RTGS') || paymentServiceCode.includes('SKN'))
    ? 0
    : transactionAmount;

/**
 * Populate the transaction input before processing. The caller can decide if the inquiry beneficiary account is needed.
 * @param input Transaction input.
 * @param beneficiaryInquiryPredicate Predicate function to determine if we need to make a beneficiary account inquiry.
 * @param sourceInquiryPredicate Predicate function to determine if we need to make a source account inquiry.
 */
const populateTransactionInfo = async (
  input: ITransactionInput,
  beneficiaryInquiryPredicate: (
    bankCode: IBankCode,
    paymentServiceType?: PaymentServiceTypeEnum
  ) => boolean = () => true,
  sourceInquiryPredicate: (
    paymentServiceType: PaymentServiceTypeEnum
  ) => boolean = () => true
): Promise<ITransactionInput> => {
  try {
    input = {
      ...input,
      journey: transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.REQUEST_RECEIVED,
        input
      )
    };

    const [sourceBankInfo, beneficiaryBankInfo] = await getBankInfo(input);

    // Keep track input cif
    const executorCIF = input.sourceCIF;
    const destinationCIF = input.beneficiaryCIF;
    let sourceAccountInfo: ITransactionAccount = {},
      beneficiaryAccountInfo: ITransactionAccount = {};
    let sourceInquiryPredicateResult:
      | boolean
      | undefined = sourceInquiryPredicate(input.paymentServiceType);
    let beneficiaryInquiryPredicateResult: boolean | undefined =
      beneficiaryBankInfo &&
      beneficiaryInquiryPredicate(
        beneficiaryBankInfo,
        input.paymentServiceType
      );

    input = {
      ...input,
      journey: transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.INQUIRY_STARTED,
        input
      )
    };

    const [
      beneficiaryAccountNo,
      bifastBankCode
    ] = transactionHelper.getBifastBeneficiaryAdditionalInfo(input);
    if (bifastBankCode && beneficiaryBankInfo) {
      beneficiaryBankInfo.bankCodeId = bifastBankCode.bankCodeId;
    }

    try {
      [sourceAccountInfo, beneficiaryAccountInfo] = await Promise.all([
        transactionHelper.getSourceAccount(
          input.sourceAccountNo,
          executorCIF,
          sourceBankInfo,
          input.sourceAccountName,
          input.transactionAmount,
          sourceInquiryPredicateResult
        ),
        transactionHelper.getBeneficiaryAccount(
          beneficiaryAccountNo,
          executorCIF,
          beneficiaryBankInfo,
          input.beneficiaryAccountName,
          getTransactionAmount(
            input.transactionAmount,
            input.paymentServiceCode
          ),
          input.sourceAccountNo,
          beneficiaryInquiryPredicateResult
        )
      ]);
    } catch (err) {
      logger.error(
        `populateTransactionInfo: Error while doing account inquiry journey: ${JSON.stringify(
          input.journey
        )}`
      );
      throw err;
    }
    if (!sourceInquiryPredicateResult) {
      sourceAccountInfo = {
        cif: input.sourceCIF,
        ...sourceAccountInfo
      };
    }

    const isTransferSameCifForMigratingAccounts = checkTransferSameCifForMigratingAccounts(
      sourceAccountInfo,
      beneficiaryAccountInfo
    );

    if (!isTransferSameCifForMigratingAccounts) {
      logger.error(
        'populateTransactionInfo: source or beneficiary account is being migrated'
      );
      throw new TransferAppError(
        'Source or beneficiary account is being migrated!',
        TransferFailureReasonActor.JAGO,
        ERROR_CODE.SOURCE_OR_BENEFICIARY_ACCOUNT_IS_BEING_MIGRATED_TO_SHARIA
      );
    }

    // get back owner cif
    if (sourceAccountInfo) {
      input.sourceCIF = sourceAccountInfo.cif;
    }
    if (beneficiaryAccountInfo) {
      input.beneficiaryCIF = beneficiaryAccountInfo.cif;
    }

    let actualBeneficiaryBankInfo = undefined;
    if (bifastBankCode) {
      transactionHelper.validateProxyTypeAndBankCode(
        input,
        beneficiaryAccountInfo
      );
      if (beneficiaryAccountInfo.bankCode === JAGO_BANK_CODE) {
        /* 
          For Jago proxy,updating paymentServiceCode from BIFAST_OUTGOING to undefined. 
          Recommendation service can calculate service on its own. 
        */
        input.paymentServiceCode = undefined;
      }

      if (beneficiaryAccountInfo.bankCode) {
        actualBeneficiaryBankInfo = await transactionHelper.getBankCode(
          beneficiaryAccountInfo.bankCode
        );
        input.beneficiaryBankCode =
          actualBeneficiaryBankInfo?.bankCodeId || input.beneficiaryBankCode;
      }
    }

    return transactionHelper.populateTransactionInput(
      input,
      sourceAccountInfo,
      sourceBankInfo,
      beneficiaryAccountInfo,
      actualBeneficiaryBankInfo || beneficiaryBankInfo,
      executorCIF,
      destinationCIF
    );
  } catch (err) {
    logger.error(
      `populateTransactionInfo: Error while making an inquiry ${err.message}`
    );

    const failedTransaction = await updateFailedTransaction(input, err);

    failedTransaction &&
      monitoringEventProducer.sendMonitoringEventMessage(
        MonitoringEventType.PROCESSING_FINISHED,
        failedTransaction
      );
    throw err;
  }
};

const executeTransaction = async (
  transactionInput: ITransactionInput,
  executionHooks: ITransactionExecutionHooks = defaultExecutionHooks
): Promise<ITransactionModel> => {
  return transactionExecutionService.executeTransaction(
    transactionInput,
    executionHooks
  );
};

const generateValidationErrorForWallet = (
  input: ITransactionInput
): TransferAppError => {
  const missingKeys: string[] = [];
  if (!input.sourceCIF) {
    missingKeys.push(SOURCE_CIF_COL);
  }
  if (!input.beneficiaryAccountNo) {
    missingKeys.push(BENEFICIARY_ACCOUNT_NO_COL);
  }
  if (!input.beneficiaryBankCode) {
    missingKeys.push(BENEFICIARY_BANK_CODE_COL);
  }
  const code = ERROR_CODE.INVALID_MANDATORY_FIELDS;
  return new TransferAppError(
    'Invalid mandatory fields!',
    TransferFailureReasonActor.SUBMITTER,
    ERROR_CODE.INVALID_REQUEST,
    missingKeys.map(key => ({
      message: ErrorList[code].message,
      code: code,
      key: key
    }))
  );
};

const populateBeneficiaryAccountFromVirtualAccount = async (
  virtualAccountNumber: string,
  beneficiaryBankCode: string,
  ginBankCodeInfo: IBankCode
): Promise<ITransactionAccount> => {
  const virtualAccount = await virtualAccountRepository.inquiryVirtualAccount(
    virtualAccountNumber
  );

  if (virtualAccount.institution.bankCode !== beneficiaryBankCode) {
    logger.error(
      `Beneficiary bank code ${beneficiaryBankCode} does not match with virtual bankCode ${virtualAccount.institution.bankCode}.`
    );
    throw new TransferAppError(
      `Beneficiary bank code ${beneficiaryBankCode} does not match with virtual bankCode ${virtualAccount.institution.bankCode}.`,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_REGISTER_PARAMETER,
      [getErrorMessage(BENEFICIARY_BANK_CODE_COL)]
    );
  }
  return await transactionHelper.getAccountInfo(
    virtualAccount.institution.accountId,
    virtualAccount.institution.cif,
    ginBankCodeInfo
  );
};

const populateWalletTransactionInfo = async (
  input: ITransactionInput
): Promise<ITransactionInput> => {
  try {
    if (
      input.sourceCIF &&
      input.beneficiaryAccountNo &&
      input.beneficiaryBankCode
    ) {
      const [beneficiaryBankCodeInfo, ginBankCodeInfo] = await Promise.all([
        transactionHelper
          .getBankCodeMapping(input.beneficiaryBankCode)
          .catch(
            buildRejectedHandler(
              TransferFailureReasonActor.MS_TRANSFER,
              `Failed to get beneficiary bank code mapping for: ${input.sourceBankCode}!`,
              BENEFICIARY_BANK_CODE_COL
            )
          ),
        transactionHelper
          .getBankCodeMapping(Constant.GIN_BANK_CODE_ID)
          .catch(
            buildRejectedHandler(
              TransferFailureReasonActor.MS_TRANSFER,
              `Failed to get source bank code mapping for: ${Constant.GIN_BANK_CODE_ID}!`,
              SOURCE_BANK_CODE_COL
            )
          )
      ]);
      const sourceAccountInfo = await transactionHelper.getSourceAccount(
        input.sourceAccountNo,
        input.sourceCIF,
        ginBankCodeInfo,
        input.sourceAccountName,
        input.transactionAmount
      );
      let beneficiaryAccountNo = input.beneficiaryAccountNo;
      let beneficiaryAccountInfo;
      if (beneficiaryBankCodeInfo.channel === BankChannelEnum.PARTNER) {
        beneficiaryAccountInfo = await populateBeneficiaryAccountFromVirtualAccount(
          beneficiaryAccountNo,
          input.beneficiaryBankCode,
          ginBankCodeInfo
        );
      } else {
        beneficiaryAccountInfo = await transactionHelper.getAccountInfo(
          beneficiaryAccountNo,
          undefined,
          beneficiaryBankCodeInfo,
          undefined,
          input.transactionAmount
        );
      }

      return transactionHelper.populateTransactionInput(
        {
          ...input,
          sourceCIF: sourceAccountInfo.cif
        },
        {
          ...sourceAccountInfo,
          bankCode: Constant.GIN_BANK_CODE_ID // main account api don't provide bank code like inquiry
        },
        ginBankCodeInfo,
        beneficiaryAccountInfo,
        beneficiaryBankCodeInfo,
        input.sourceCIF // this one will similar executorCIF for keep track
      );
    } else {
      throw generateValidationErrorForWallet(input);
    }
  } catch (err) {
    logger.error(
      `populateWalletTransactionInfo: Error while making an inquiry ${err.message}`
    );
    input.transactionId &&
      (await findByIdAndUpdate(input.transactionId, {
        status: TransactionStatus.DECLINED,
        journey: transactionHelper.getStatusesToBeUpdated(
          TransferJourneyStatusEnum.TRANSFER_FAILURE,
          input
        )
      }));
    throw err;
  }
};

const getTcByInterchange = (
  transactionCodeMapping: ITransactionCodeMapping[] | undefined,
  priority: BankNetworkEnum | undefined
): ITransactionCodeMapping[] | undefined => {
  let transactionCode: ITransactionCodeMapping | undefined;
  if (transactionCodeMapping) {
    transactionCode = transactionCodeMapping.find(
      tc => tc.interchange === priority
    );
  }

  return transactionCode ? [transactionCode] : transactionCodeMapping;
};

const populateBifastAdditionalInfo = (
  transactionInput: ITransactionInput,
  input: RecommendPaymentServiceInput
) => {
  // if bank code is bifast, then populate additionalInformation3 and additionalInformation4
  if (
    transactionInput.beneficiaryBankCode &&
    isBIFastBank(transactionInput.beneficiaryBankCode)
  ) {
    transactionInput.additionalInformation3 = startsWith(
      input.beneficiaryAccountNo,
      PhoneCode.PLUS62
    )
      ? BeneficiaryProxyType.PHONE_NUMBER
      : BeneficiaryProxyType.EMAIL;
    transactionInput.additionalInformation4 = input.beneficiaryAccountNo;
  }
};

const getTransactionInfoForRecommendPaymentService = async (
  input: RecommendPaymentServiceInput
): Promise<ITransactionInput> => {
  let transactionInput: ITransactionInput = {
    ...input,
    paymentServiceCode: input.paymentServiceCode || ''
  };

  populateBifastAdditionalInfo(transactionInput, input);

  if (input.paymentServiceType === PaymentServiceTypeEnum.WALLET) {
    return await populateWalletTransactionInfo(transactionInput);
  } else {
    // if bank code is external, then we don't need to inquiry to switching.
    // if bank code is bifast, then we'll do inquiry to bifast channel.
    return await populateTransactionInfo(transactionInput, bankCode => {
      return !isExternalBank(bankCode) || isBIFastBank(bankCode.bankCodeId);
    });
  }
};

const recommendPaymentService = async (
  input: RecommendPaymentServiceInput
): Promise<RecommendationService[]> => {
  logger.info(`Entering recommendPaymentService with sourceAccountNo :${input.sourceAccountNo},
  sourceBankCode: ${input.sourceBankCode}, externalId: ${input.externalId}, beneficiaryAccountNumber: ${input.beneficiaryAccountNo}
  beneficiaryBankCode: ${input.beneficiaryBankCode}, paymentServiceType: ${input.paymentServiceType}`);
  const { transactionAmount, sourceAccountNo } = input;

  const transactionInput = await getTransactionInfoForRecommendPaymentService(
    input
  );

  let recommendedServices: RecommendationService[] = await recommendedCodeService.getRecommendServices(
    transactionInput
  );

  recommendedServices = await transactionHelper.mapAuthenticationRequired(
    recommendedServices,
    transactionAmount,
    sourceAccountNo
  );

  recommendedServices.map(serviceRecommendation => {
    serviceRecommendation.debitTransactionCode = getTcByInterchange(
      serviceRecommendation.debitTransactionCode,
      transactionInput.debitPriority
    );
    serviceRecommendation.creditTransactionCode = getTcByInterchange(
      serviceRecommendation.creditTransactionCode,
      transactionInput.creditPriority
    );
  });

  return recommendedServices;
};

const produceFailedTransferNotificationMessage = async (
  transaction: ITransactionModel
): Promise<void> => {
  logger.info('produce transfer failed message for failed transaction...');
  transactionProducer.sendFailedTransferNotification(transaction);
};

const validate = (input: ITransferTransactionInput): void => {
  if (
    input.paymentServiceType === PaymentServiceTypeEnum.PAYROLL &&
    !input.interchange
  ) {
    logger.error(
      'populateTransactionInfo: Interchange required for PAYROLL Transaction'
    );
    throw new TransferAppError(
      'Interchange required for payroll Transaction!',
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.PAYROLL_INTERCHANGE_REQUIRED
    );
  }
};

const isRdnTransaction = (model: ITransactionModel): boolean =>
  model.paymentServiceType === PaymentServiceTypeEnum.RDN;

const beneficiaryInquiryPredicate = (
  bankCode: IBankCode,
  paymentServiceType?: PaymentServiceTypeEnum
) =>
  bankCode.channel !== BankChannelEnum.WINCOR &&
  bankCode.channel !== BankChannelEnum.TOKOPEDIA &&
  bankCode.bankCodeId != BANK_KSEI &&
  paymentServiceType !== PaymentServiceTypeEnum.TD_PENALTY &&
  paymentServiceType !== PaymentServiceTypeEnum.BRANCH_WITHDRAWAL &&
  paymentServiceType !== PaymentServiceTypeEnum.CARD_OPENING &&
  paymentServiceType !== PaymentServiceTypeEnum.LOAN_ADMIN_FEE &&
  paymentServiceType !== PaymentServiceTypeEnum.OVERBOOKING &&
  paymentServiceType !== PaymentServiceTypeEnum.MAIN_POCKET_TO_GL_TRANSFER &&
  paymentServiceType !== PaymentServiceTypeEnum.LOAN_FORWARD_PAYMENT_DEPOSIT &&
  paymentServiceType !== PaymentServiceTypeEnum.TD_SHARIA_PLACEMENT &&
  paymentServiceType !== PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY &&
  paymentServiceType !== PaymentServiceTypeEnum.TD_PAYMENT_GL &&
  paymentServiceType !== PaymentServiceTypeEnum.LOAN_GL_DISBURSEMENT;

const sourceInquiryPredicate = (paymentServiceType: PaymentServiceTypeEnum) =>
  paymentServiceType !== PaymentServiceTypeEnum.OFFER &&
  paymentServiceType !== PaymentServiceTypeEnum.PAYROLL &&
  paymentServiceType !== PaymentServiceTypeEnum.CASHBACK &&
  paymentServiceType !== PaymentServiceTypeEnum.BRANCH_DEPOSIT &&
  paymentServiceType !== PaymentServiceTypeEnum.LOAN_DISBURSEMENT &&
  paymentServiceType !== PaymentServiceTypeEnum.SETTLEMENT &&
  paymentServiceType !==
    PaymentServiceTypeEnum.LOAN_FORWARD_PAYMENT_WITHDRAWAL &&
  paymentServiceType !== PaymentServiceTypeEnum.LOAN_REPAYMENT &&
  paymentServiceType !== PaymentServiceTypeEnum.TD_SHARIA_REPAYMENT &&
  paymentServiceType !== PaymentServiceTypeEnum.BONUS_INTEREST &&
  paymentServiceType !== PaymentServiceTypeEnum.MIGRATION_TRANSFER &&
  paymentServiceType !== PaymentServiceTypeEnum.LOAN_REFUND &&
  paymentServiceType !== PaymentServiceTypeEnum.LOAN_WRITEOFF_RECOVERY &&
  paymentServiceType !== PaymentServiceTypeEnum.TD_MANUAL_PAYMENT_GL &&
  paymentServiceType !== PaymentServiceTypeEnum.LOAN_GL_REPAYMENT;

const createTransferTransaction = async (
  transactionInput: ITransferTransactionInput
): Promise<ITransactionModel> => {
  validate(transactionInput);
  const transactionModel = transactionHelper.buildOutgoingTransactionModel(
    transactionInput
  );
  await transactionHelper.validateExternalIdAndPaymentType(transactionModel);
  const model = await transactionRepository.create(transactionModel);
  monitoringEventProducer.sendMonitoringEventMessage(
    MonitoringEventType.PROCESSING_STARTED,
    model
  );

  let input = await populateTransactionInfo(
    { ...transactionInput, ...model, transactionId: model.id },
    beneficiaryInquiryPredicate,
    sourceInquiryPredicate
  );
  input = {
    ...input,
    journey: transactionHelper.getStatusesToBeUpdated(
      TransferJourneyStatusEnum.INQUIRY_FETCHED,
      input
    )
  };
  return await executeTransaction(input, {
    onFailedTransaction: async (model: ITransactionModel) => {
      if (
        !model.paymentInstructionExecution?.topics.includes(
          PaymentInstructionTopicConstant.UPDATE_TRANSACTION_STATUS
        ) ||
        !isRdnTransaction(model)
      ) {
        produceFailedTransferNotificationMessage(model);
      }
    }
  });
};

const createDepositTransaction = async (
  transactionInput: IDepositTransactionInput
): Promise<ITransactionModel> => {
  const transactionModel = transactionHelper.buildOutgoingTransactionModel(
    transactionInput
  );
  await transactionHelper.validateExternalIdAndPaymentType(transactionModel);
  const model = await transactionRepository.create(transactionModel);
  monitoringEventProducer.sendMonitoringEventMessage(
    MonitoringEventType.PROCESSING_STARTED,
    model
  );

  const input = await populateTransactionInfo(
    { ...transactionInput, ...model, transactionId: model.id },
    beneficiaryInquiryPredicate,
    sourceInquiryPredicate
  );
  return executeTransaction(input, {
    onRecommendedServiceCode: async (code: RecommendationService) => {
      // a deposit transaction require the credit transaction must be existed and debit must be not existed
      if (!code.creditTransactionCode || !code.creditTransactionCode.length) {
        logger.error(
          'createDepositTransaction: missing credit transaction code'
        );
        throw new TransferAppError(
          'Missing credit transaction code while creating deposit transaction!',
          TransferFailureReasonActor.SUBMITTER,
          ERROR_CODE.MISSING_CREDIT_TRANSACTION_CODE
        );
      }

      if (code.debitTransactionCode && code.debitTransactionCode.length) {
        logger.error(
          'createDepositTransaction: missing debit transaction code'
        );
        throw new TransferAppError(
          'Missing debit transaction code while creating deposit transaction!',
          TransferFailureReasonActor.SUBMITTER,
          ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE
        );
      }
      return;
    }
  });
};

const createWithdrawTransaction = async (
  transactionInput: IWithdrawTransactionInput
): Promise<ITransactionModel> => {
  const transactionModel = transactionHelper.buildOutgoingTransactionModel(
    transactionInput
  );
  await transactionHelper.validateExternalIdAndPaymentType(transactionModel);
  const model = await transactionRepository.create(transactionModel);
  monitoringEventProducer.sendMonitoringEventMessage(
    MonitoringEventType.PROCESSING_STARTED,
    model
  );

  const input = await populateTransactionInfo({
    ...transactionInput,
    ...model,
    transactionId: model.id
  });
  return executeTransaction(input, {
    onRecommendedServiceCode: async (code: RecommendationService) => {
      // a withdraw transaction require the debit transaction must be existed and credit transaction must be not existed
      if (code.creditTransactionCode && code.creditTransactionCode.length) {
        logger.error(
          'createWithdrawTransaction: missing credit transaction code'
        );
        throw new TransferAppError(
          'Missing credit transaction code while creating withdraw transaction!',
          TransferFailureReasonActor.SUBMITTER,
          ERROR_CODE.MISSING_CREDIT_TRANSACTION_CODE
        );
      }

      if (!code.debitTransactionCode || !code.debitTransactionCode.length) {
        logger.error(
          'createWithdrawTransaction: missing debit transaction code'
        );
        throw new TransferAppError(
          'Missing debit transaction code while creating withdraw transaction!',
          TransferFailureReasonActor.SUBMITTER,
          ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE
        );
      }
      return;
    }
  });
};

const getTransaction = async (id: string): Promise<ITransactionModel> => {
  const transaction = await transactionRepository.getByKey(id);
  if (!transaction) {
    logger.error(
      `getTransaction: Matching txn was not found for transactionId: ${id}`
    );
    throw new TransferAppError(
      `getTransaction: Matching txn was not found for transactionId: ${id}`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.TRANSACTION_NOT_FOUND
    );
  }
  return transaction;
};

const createInternalTopupTransaction = async (
  input: ITopupInternalTransactionInput
): Promise<ITransactionModel> => {
  const transactionModel = transactionHelper.buildOutgoingTransactionModel(
    input
  );
  await transactionHelper.validateExternalIdAndPaymentType(transactionModel);
  const model = await transactionRepository.create(transactionModel);
  monitoringEventProducer.sendMonitoringEventMessage(
    MonitoringEventType.PROCESSING_STARTED,
    model
  );

  let transactionInput = await populateWalletTransactionInfo({
    ...input,
    ...model,
    transactionId: model.id
  });
  transactionInput = {
    ...transactionInput,
    journey: transactionHelper.getStatusesToBeUpdated(
      TransferJourneyStatusEnum.INQUIRY_FETCHED,
      transactionInput
    )
  };
  return executeTransaction(transactionInput);
};

const updateOriginalTransactionWithRefund = async (
  originalTxn: ITransactionModel,
  refundTxn: ITransactionModel
): Promise<ITransactionModel | null> => {
  const earlierRefund = originalTxn.refund;
  if (
    earlierRefund &&
    earlierRefund.remainingAmount &&
    earlierRefund.transactions
  ) {
    earlierRefund.remainingAmount =
      earlierRefund.remainingAmount - refundTxn.transactionAmount;
    earlierRefund.transactions.push(refundTxn.id);
  } else {
    originalTxn = {
      ...originalTxn,
      refund: {
        remainingAmount:
          originalTxn.transactionAmount - refundTxn.transactionAmount,
        transactions: [refundTxn.id]
      }
    };
  }
  logger.info(`Updating original transaction with refund details
  transactions: ${JSON.stringify(originalTxn.refund?.transactions)}`);
  return transactionRepository.update(originalTxn.id, originalTxn);
};

const updateRefundTransaction = async (
  originalTxn: ITransactionModel,
  refundTxn: ITransactionModel
): Promise<ITransactionModel | null> => {
  refundTxn = {
    ...refundTxn,
    refund: {
      originalTransactionId: originalTxn.id,
      revertPaymentServiceCode: originalTxn.paymentServiceCode
    }
  };
  logger.info(`Updating refund transaction with orginal transaction details
  originalTransactionId: ${JSON.stringify(
    refundTxn.refund?.originalTransactionId
  )}`);
  return transactionRepository.update(refundTxn.id, refundTxn);
};

const getTransactionListByCriteria = async (
  input: TransactionListRequest
): Promise<IPagingResult<ITransactionModel>> => {
  let customerInfo: ICustomerInfoWithCif | undefined;
  if (input.customerId) {
    customerInfo = await customerService.getCustomerById(input.customerId);
  }
  const transactions = await transactionRepository.getTransactionListByCriteria(
    input,
    customerInfo && customerInfo.cif
  );
  if (isEmpty(transactions.list)) {
    logger.error(`getTransactionList: Matching txn was not found`);
    throw new TransferAppError(
      'getTransactionListByCriteria: Matching txn was not found',
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.TRANSACTION_NOT_FOUND
    );
  }
  return transactions;
};

const shouldGetTransactionWithoutOwnerCustomerId = (
  input: TransactionListRequest,
  transactions: ITransactionModel[]
): boolean => {
  const lastCreatedAt: Date | undefined =
    transactions[transactions.length - 1].createdAt;

  const lastTransactionDate: number | undefined = lastCreatedAt
    ? new Date(lastCreatedAt).setHours(0, 0, 0, 0)
    : undefined;

  const startDate: number | undefined = input.startDate
    ? toDate(input.startDate).setHours(0, 0, 0, 0)
    : undefined;

  return lastTransactionDate && startDate
    ? startDate < lastTransactionDate
    : false;
};

const getTransactionList = async (
  input: TransactionListRequest,
  fetchRefund: boolean = false
): Promise<{
  transactions: IPagingResult<ITransactionModel>;
  refundTransactions: IRefundTransactionBasicDetail[];
}> => {
  transactionHelper.validateTransactionListRequest(input);
  let transactions = await transactionRepository.getTransactionListByCriteria(
    input
  );
  if (
    isEmpty(transactions.list) ||
    shouldGetTransactionWithoutOwnerCustomerId(input, transactions.list)
  ) {
    logger.info(
      `getTransactionList: Getting transactions without ownerCustomerId query`
    );
    transactions = await getTransactionListByCriteria(input);
  }
  if (!fetchRefund) {
    return { transactions: transactions, refundTransactions: [] };
  }
  const refundedTransactionIds: string[] = transactions.list
    .map(transaction => {
      if (!(transaction.refund && transaction.refund.transactions)) {
        return [];
      }
      return transaction.refund.transactions;
    })
    .reduce((prevValue, currValue) => {
      return prevValue.concat(currValue);
    }, []);

  let refundTransactions: IRefundTransactionBasicDetail[] = [];
  if (refundedTransactionIds.length > 0) {
    const refundTransactionQuery = {
      _id: { $in: refundedTransactionIds }
    };
    const refundTransactionProjection = {
      status: 1,
      transactionAmount: 1,
      createdAt: 1,
      updatedAt: 1
    };
    refundTransactions = mapTransactionModelToRefundTransaction(
      await transactionRepository.findTransactionByQuery(
        refundTransactionQuery,
        refundTransactionProjection
      )
    );
  }
  return {
    transactions: transactions,
    refundTransactions: refundTransactions
  };
};

const checkTransactionStatus = async (
  input: TransactionStatusInput
): Promise<ITransactionStatusResponse> => {
  try {
    // Check status to DB
    let transaction = await transactionRepository.getTransactionStatusByCriteria(
      input
    );
    if (!transaction) {
      logger.error(
        `checkTransactionStatus: Matching txn was not found for 
        ownerCustomerId: ${input.customerId},
        paymentInstructionId: ${input.paymentInstructionId},
        transactionDate: ${input.transactionDate}`
      );
      throw new TransferAppError(
        'Matching transaction was not found!',
        TransferFailureReasonActor.SUBMITTER,
        ERROR_CODE.TRANSACTION_NOT_FOUND
      );
    }

    if (
      [TransactionStatus.SUCCEED, TransactionStatus.DECLINED].includes(
        transaction.status
      )
    ) {
      return { status: transaction.status };
    }
    // Check status with third party
    return transactionExecutionService.executeCheckStatus(transaction);
  } catch (error) {
    if (error instanceof TransferAppError) throw error;
    logger.error(
      `Unexpected error while checkTransactionStatus: ${error.message}`
    );
    throw new TransferAppError(
      `Unexpected error while checking transaction status with message: ${error.message}!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.UNEXPECTED_ERROR
    );
  }
};

const resolveTransactionStatus = async (
  minTransactionAge: number = SCHEDULED_RESOLVE_STATUS_MIN_TRANSACTION_AGE_IN_MINUTES,
  maxTransactionAge: number = SCHEDULED_RESOLVE_STATUS_MAX_TRANSACTION_AGE_IN_MINUTES,
  recordLimit: number = SCHEDULED_RESOLVE_STATUS_FETCH_RECORD_LIMIT
) => {
  const paymentServiceCodes = SCHEDULED_RESOLVE_STATUS_TRANSACTION_CODES;
  logger.debug(`Resolving transaction status for ${paymentServiceCodes}`);

  const pendingTransactions = await transactionRepository.getPendingTransactionList(
    paymentServiceCodes,
    minTransactionAge,
    maxTransactionAge,
    recordLimit
  );

  for (const pendingTransaction of pendingTransactions) {
    try {
      logger.info(
        `Resolving status for transaction with:
          ID: '${pendingTransaction.id}'
          External ID: ${pendingTransaction.externalId}
          Payment Service Code '${pendingTransaction.paymentServiceCode}'`
      );
      if (pendingTransaction.paymentServiceCode === QRIS_PAYMENT_SERVICE_CODE) {
        await qrisService.executeCheckStatus(pendingTransaction);
      } else if (
        pendingTransaction.paymentServiceCode?.includes(BankChannelEnum.BIFAST)
      ) {
        await transactionExecutionService.executeCheckStatus(
          pendingTransaction
        );
      } else {
        logger.info(
          `Cannot resolve status for transaction ${pendingTransaction.id}. Reason: Not supported.`
        );
      }
    } catch (err) {
      logger.error(
        `Cannot resolve status for transaction ${pendingTransaction.id}. Reason: ${err.code}, ${err.message}`
      );
    }
  }
};

const getFeeAmountInputs = async (
  transactionInput: ITransactionInput,
  usageCounters: UsageCounter[]
): Promise<FeeAmountInput[]> => {
  const recommendedService = await recommendedCodeService.getRecommendedServiceWithoutFeeData(
    transactionInput,
    usageCounters
  );
  return await transactionHelper.getFeeAmountInput(
    transactionInput,
    recommendedService,
    usageCounters
  );
};

const getUpdatedUsageCounters = async (
  usageCounters: UsageCounter[],
  feeAmountInputs: FeeAmountInput[]
): Promise<UsageCounter[]> => {
  const updatedUsageCounters = _.cloneDeep(usageCounters);
  for (const feeAmountInput of feeAmountInputs) {
    const transactionCode: ITransactionCodeInfo = {
      code: '',
      minAmountPerTx: NaN,
      maxAmountPerTx: NaN,
      channel: '',
      limitGroupCode: feeAmountInput.limitGroupCode,
      awardGroupCounter: feeAmountInput.awardGroupCounter
    };
    const feeUsageCounter = await transactionHelper.getUsageCounter(
      transactionCode,
      updatedUsageCounters
    );
    if (!feeUsageCounter) {
      // No usage counter for this fee input
      continue;
    }
    const recordedUsageCounter = updatedUsageCounters.find(
      counter => counter.limitGroupCode == feeUsageCounter.limitGroupCode
    );
    if (recordedUsageCounter) {
      recordedUsageCounter.monthlyAccumulationTransaction++;
      recordedUsageCounter.dailyAccumulationAmount +=
        feeAmountInput.transactionAmount;
    }
  }
  return updatedUsageCounters;
};

const checkBulkTransactionFees = async (
  bulkInputs: BulkTransactionFeesInput
): Promise<BulkTransactionFeesResponse> => {
  const usageCountersRecord = new Map<string | undefined, UsageCounter[]>();

  // Generate the fee queries
  const allFeeAmountInputs: FeeAmountInput[][] = [];
  for (const transaction of bulkInputs.transactions) {
    const transactionInput = await getTransactionInfoForRecommendPaymentService(
      transaction
    );
    const sourceCustomerId = transactionInput.sourceCustomerId;
    let usageCounters: UsageCounter[] = [];
    if (!usageCountersRecord.has(sourceCustomerId)) {
      usageCounters = await recommendedCodeService.getCustomerUsageCounters(
        sourceCustomerId
      );
    } else {
      usageCounters = usageCountersRecord.get(sourceCustomerId) ?? [];
    }
    const feeAmountInputs = await getFeeAmountInputs(
      transactionInput,
      usageCounters
    );
    allFeeAmountInputs.push(feeAmountInputs);

    // At this point, usage counters is complete with both counters from ms-award and ms-entitlement
    // So when we record counters, it is a complete set
    const updatedUsageCounters = await getUpdatedUsageCounters(
      usageCounters,
      feeAmountInputs
    );
    usageCountersRecord.set(sourceCustomerId, updatedUsageCounters);
  }

  // Get the basic fees and try applying custom fees if applicable
  const feeAmountQueries = await transactionHelper.getFeeAmountQueries(
    _.flatten(allFeeAmountInputs)
  );
  await feeBatch.applyCustomerSpecificFeeIfAvailable(feeAmountQueries);

  // Now we can use this information to calculate the fee amounts
  // For each external ID, we select the first fee that is available
  const feesResult = new Map<string, IFeeAmountResponse>();
  const externalIds = new Set<string>();
  for (const feeAmountQuery of feeAmountQueries) {
    const { input: feeAmountInput, feeMappings } = feeAmountQuery;
    const externalId = feeAmountInput.externalId;
    if (!externalId) {
      const detail =
        'Expected External ID for fee amount input, but it was not set';
      logger.error(`checkBulkTransactionFees: ${detail}`);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.MS_TRANSFER,
        ERROR_CODE.MISSING_INQUIRY_ID
      );
    }
    externalIds.add(externalId);
    if (feesResult.has(externalId)) {
      continue;
    }

    if (feeMappings.length) {
      const feeData = await feeHelper.getFeeAmount(
        feeAmountInput,
        feeMappings[0]
      );
      delete feeData.interchange;
      delete feeData.feeCode;
      feesResult.set(externalId, feeData);
    }
  }

  // Fill in any missing data
  const defaultFeeDataResponse = {
    feeAmount: 0
  };
  for (const externalId of externalIds) {
    if (!feesResult.has(externalId)) {
      feesResult.set(externalId, defaultFeeDataResponse);
    }
  }

  return Array.from(feesResult, ([externalId, feeData]) => ({
    ...feeData,
    externalId
  }));
};

const transactionService = wrapLogs({
  getTransaction,
  recommendPaymentService,
  createTransferTransaction,
  createDepositTransaction,
  createWithdrawTransaction,
  createInternalTopupTransaction,
  populateTransactionInfo,
  updateRefundTransaction,
  updateOriginalTransactionWithRefund,
  getTransactionList,
  findByIdAndUpdate,
  updateFailedTransaction,
  checkTransactionStatus,
  resolveTransactionStatus,
  checkBulkTransactionFees
});

export default transactionService;
