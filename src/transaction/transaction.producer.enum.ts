export enum NotificationCode {
  NOTIFICATION_FAILED_TRANSFER = 'Notif_failed_transfer',
  NOTIFICATION_PENDING_BILLPAYMENT = 'Notif_Pending_BillPayment',
  NOTIF_BLOCK_AMOUNT = 'Notif_Block_Amount',
  NOTIFICATION_PENDING_TRANSFER = 'Notif_Pending_Transfer',
  NOTIF_PENIDNG_SKNRTGS = 'Notif_Pending_SKNRTGS',
  NOTIF_FAILED_QRIS = 'Notif_QRD01_FAILED',
  NOTIF_PENDING_QRIS = 'Notif_QRD01_PENDING',
  NOTIF_SUCCESS_QRIS = 'Notif_QRD01',
  NOTIF_DEBIT_CARD_INSUFFICIENT_BALANCE = 'Notif_Debit_Card_Insufficient_Balance',
  NOTIF_DAILY_LIMIT_ATM_WITHDRAWAL = 'Notif_Daily_Limit_Atm_Withdrawal_Exceeds_Limit',
  NOTIF_DAILY_LIMIT_ATM_TRANSFER = 'Notif_Daily_Limit_Atm_Transfer_Exceeds_Limit'
}
