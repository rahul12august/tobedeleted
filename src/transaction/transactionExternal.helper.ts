import { AppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { BaseExternalTransactionInput } from './transaction.type';
import { ITransactionModel } from './transaction.model';
import { includes } from 'lodash';
import { PaymentServiceTypeEnum } from '@dk/module-common';
import { EURONET_REFUND_EXTERNAL_ID_LENGTH } from './transaction.constant';
import logger, { wrapLogs } from '../logger';
import transactionRepository from './transaction.repository';

const isRefundRequest = (payload: BaseExternalTransactionInput): boolean =>
  includes(
    [
      PaymentServiceTypeEnum.DOMESTIC_VOID_PAYMENT,
      PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT
    ],
    payload.paymentServiceType
  );

const getOriginalTransaction = async (
  payload: BaseExternalTransactionInput
): Promise<ITransactionModel> => {
  if (
    !(
      payload.notes &&
      payload.notes.length === EURONET_REFUND_EXTERNAL_ID_LENGTH
    )
  ) {
    logger.error(
      'validateRefundRequest: notes is required for refund type req'
    );
    throw new AppError(ERROR_CODE.INVALID_REFUND_REQUEST);
  }

  const originalTransaction = await transactionRepository.getIncomingNonRefunded(
    payload.notes as string,
    payload.beneficiaryAccountNo as string
  );
  if (!originalTransaction) {
    logger.error(
      `validateRefundRequest: No transaction found for refund request with external Id : ${payload.notes}`
    );
    throw new AppError(ERROR_CODE.INVALID_REFUND_REQUEST);
  }
  return originalTransaction;
};

const transactionExternalHelper = wrapLogs({
  isRefundRequest,
  getOriginalTransaction
});

export default transactionExternalHelper;
