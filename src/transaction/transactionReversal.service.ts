import { Util } from '@dk/module-common';

import { ReverseTransactionInput } from './transaction.type';
import transactionRepository from './transaction.repository';
import logger, { wrapLogs } from '../logger';
import depositRepository from '../coreBanking/deposit.repository';
import { ERROR_CODE } from '../common/errors';
import {
  RetryableTransferAppError,
  TransferAppError,
  isRetryableError
} from '../errors/AppError';
import {
  isDepositTransactionNotReverted,
  RetryConfig
} from './transaction.util';
import {
  IDepositTransaction,
  IReverseTransactionInput
} from '../coreBanking/deposit.interface';
import {
  CoreBankTransactionType,
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from './transaction.enum';
import {
  ICoreBankingTransaction,
  ITransactionModel
} from './transaction.model';
import { AdjustLoanTransactionTypeEnum } from '../coreBanking/loan.enum';
import loanRepository from '../coreBanking/loan.repository';
import { ILoanTransactionAdjustment } from '../coreBanking/loan.interface';
import coreBankingTransactionHelper from './transactionCoreBanking.helper';
import transactionHelper from './transaction.helper';
import { isFeatureEnabled } from '../common/featureFlag';
import { REVERSAL_TC_SUFFIX } from './transaction.constant';
import { isEmpty } from 'lodash';
import { FEATURE_FLAG } from '../common/constant';

const reverseTransactionsByReferenceId = async (
  referenceId: string,
  reverseInput: ReverseTransactionInput
): Promise<void> => {
  let transactions = await depositRepository.getTransactionsByReferenceId(
    referenceId
  );
  transactions = transactions.filter(isDepositTransactionNotReverted);
  if (transactions.length > 0) {
    if (transactions[0].cardTransaction) {
      const cardTransaction = transactions[0].cardTransaction;
      await depositRepository.reverseCardTransaction(
        cardTransaction.cardToken,
        cardTransaction.externalReferenceId,
        {
          amount: cardTransaction.amount,
          externalReferenceId: cardTransaction.externalReferenceId
        }
      );
    } else {
      await depositRepository.reverseTransaction(
        transactions[0].id,
        reverseInput
      );
    }
    // mambu revert transaction by stack, id and encodedKey is changed after each revert
    // so we query and revert the updated ones
    if (transactions.length > 1)
      reverseTransactionsByReferenceId(referenceId, reverseInput);
  }
};

const getCoreBankingTransactionIds = async (
  id: string,
  creationDate: Date
): Promise<IDepositTransaction[]> => {
  try {
    const coreBankingTransactionId = await depositRepository.getTransactionsByInstructionId(
      id,
      creationDate
    );
    return coreBankingTransactionId.filter(isDepositTransactionNotReverted);
  } catch (error) {
    logger.error(
      `getCoreBankingTransactionIds: failed in fetching core banking ids for transactions: ${id}`
    );
    throw new TransferAppError(
      `Failed fetching core banking ids for transaction: ${id}`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.ERROR_WHILE_FETCHING_COREBANKING_ID
    );
  }
};

const mapToReverseTransaction = (
  transaction: ITransactionModel
): ITransactionModel => ({
  ...transaction,
  sourceAccountNo: transaction.beneficiaryAccountNo,
  beneficiaryAccountNo: transaction.sourceAccountNo,
  sourceAccountName: transaction.beneficiaryAccountName,
  beneficiaryAccountName: transaction.sourceAccountName,
  beneficiaryAccountType: transaction.sourceAccountType,
  sourceAccountType: transaction.beneficiaryAccountType,
  sourceBankCode: transaction.beneficiaryBankCode,
  beneficiaryBankCode: transaction.sourceBankCode,
  sourceCIF: transaction.beneficiaryCIF,
  beneficiaryCIF: transaction.sourceCIF
});

const setCategoryCode = (
  type: TransferJourneyStatusEnum,
  transactionModel: ITransactionModel
): string | undefined => {
  if (type.includes('CREDITED')) return 'C056';
  else if (type.includes('DEBITED')) return 'C057';
  else return transactionModel.categoryCode;
};

const invertWithdrawalTransaction = async (
  reverseTransaction: ITransactionModel,
  coreBankingTransaction: ICoreBankingTransaction
): Promise<string | undefined> => {
  let creditTransactionChannel, creditTransactionCode, transactionAmount;
  if (
    coreBankingTransaction &&
    [
      TransferJourneyStatusEnum.AMOUNT_CREDITED,
      TransferJourneyStatusEnum.AMOUNT_DEBITED
    ].includes(coreBankingTransaction.type)
  ) {
    creditTransactionChannel = reverseTransaction.creditTransactionChannel
      ? reverseTransaction.creditTransactionChannel
      : reverseTransaction.debitTransactionChannel;
    if (
      reverseTransaction.debitPriority &&
      reverseTransaction.creditTransactionCodeMapping &&
      reverseTransaction.creditTransactionCodeMapping?.length > 0
    ) {
      const creditTransactionCodeInfo = reverseTransaction.creditTransactionCodeMapping?.find(
        item => item.interchange === reverseTransaction.debitPriority
      );
      creditTransactionCode =
        creditTransactionCodeInfo && creditTransactionCodeInfo.code
          ? creditTransactionCodeInfo.code
          : reverseTransaction.debitTransactionCode + REVERSAL_TC_SUFFIX;
    } else {
      creditTransactionCode = reverseTransaction.creditTransactionCode
        ? reverseTransaction.creditTransactionCode
        : reverseTransaction.debitTransactionCode + REVERSAL_TC_SUFFIX;
    }
    transactionAmount = reverseTransaction.transactionAmount;
  } else if (
    coreBankingTransaction &&
    reverseTransaction.fees &&
    !isEmpty(reverseTransaction.fees) &&
    [
      TransferJourneyStatusEnum.FEE_AMOUNT_CREDITED,
      TransferJourneyStatusEnum.FEE_AMOUNT_DEBITED
    ].includes(coreBankingTransaction.type)
  ) {
    creditTransactionChannel = reverseTransaction.fees[0].customerTcChannel
      ? reverseTransaction.fees[0].customerTcChannel
      : reverseTransaction.creditTransactionChannel;
    creditTransactionCode = reverseTransaction.fees[0].customerTc
      ? reverseTransaction.fees[0].customerTc + REVERSAL_TC_SUFFIX
      : reverseTransaction.creditTransactionCode;
    transactionAmount = reverseTransaction.fees[0].feeAmount
      ? reverseTransaction.fees[0].feeAmount
      : reverseTransaction.transactionAmount;
  }

  reverseTransaction.categoryCode = setCategoryCode(
    coreBankingTransaction.type,
    reverseTransaction
  );

  logger.info(
    `invertWithdrawalTransaction: credit transaction for beneficiaryAccountNo ${reverseTransaction.beneficiaryAccountNo} with 
      creditTransactionChannel: ${creditTransactionChannel},
      creditTransactionCode: ${creditTransactionCode}`
  );

  if (
    transactionAmount &&
    creditTransactionCode &&
    creditTransactionChannel &&
    reverseTransaction.beneficiaryAccountNo
  ) {
    const creditTransactionId = await coreBankingTransactionHelper.submitCreditTransaction(
      reverseTransaction.beneficiaryAccountNo,
      { ...reverseTransaction, transactionAmount: transactionAmount },
      creditTransactionCode,
      creditTransactionChannel
    );
    return creditTransactionId;
  }
  logger.warn(
    `Ignoring invertWithdrawalTransaction as conditions does not matched`
  );
  return;
};

const addInverseTransactionToTransferJourney = (
  transactionModel: ITransactionModel,
  journeyType: TransferJourneyStatusEnum,
  coreBankingId: string
): void => {
  coreBankingId &&
    !transactionModel.coreBankingTransactionIds?.includes(coreBankingId) &&
    transactionModel.coreBankingTransactionIds?.push(coreBankingId);
  if (
    coreBankingId &&
    !transactionModel.coreBankingTransactions
      ?.map(item => item.id)
      .includes(coreBankingId)
  ) {
    coreBankingId &&
      transactionModel.coreBankingTransactions?.push({
        id: coreBankingId,
        type: (journeyType.toString() +
          '_REVERSED') as TransferJourneyStatusEnum
      });
  }

  transactionModel.journey = transactionHelper.getStatusesToBeUpdated(
    journeyType,
    transactionModel
  );
};

export const invertDepositTransaction = async (
  model: ITransactionModel
): Promise<string | undefined> => {
  model.debitTransactionChannel = model.debitTransactionChannel
    ? model.debitTransactionChannel
    : model.creditTransactionChannel;
  model.debitTransactionCode = model.debitTransactionCode
    ? model.debitTransactionCode
    : model.creditTransactionCode;
  logger.info(
    `invertDepositTransaction: debit transaction for sourceAccountNo ${model.sourceAccountNo} with 
      debitTransactionChannel: ${model.debitTransactionChannel},
      debitTransactionCode: ${model.debitTransactionChannel}`
  );
  try {
    if (
      model.debitTransactionCode &&
      model.debitTransactionChannel &&
      model.sourceAccountNo
    ) {
      const debitTransactionId = await coreBankingTransactionHelper.submitDebitTransaction(
        model.sourceAccountNo,
        model,
        model.debitTransactionCode,
        model.debitTransactionChannel
      );
      return debitTransactionId;
    }
    logger.warn(
      `Ignoring invertDepositTransaction as conditions does not matched`
    );
    return;
  } catch (err) {
    logger.error(
      `invertDepositTransaction: Error while debiting money, code: ${err.code}, message: ${err.message}`
    );
    throw new RetryableTransferAppError(
      `Error while debiting money, code: ${err.code}, message: ${err.message}!`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.COREBANKING_DEBIT_FAILED,
      isRetryableError(err.status)
    );
  }
  return;
};

const reverseTransactionsByInstructionId = async (
  transactionId: string,
  reversalReason?: string,
  transactionModel?: ITransactionModel
): Promise<void> => {
  if (!(transactionModel && transactionModel.createdAt)) {
    logger.error(
      `reverseTransactionsByInstructionId: creation date did not find for transactions: ${transactionId}`
    );
    throw new TransferAppError(
      `Creation date not found for transactions: ${transactionId}`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.ERROR_WHILE_FETCHING_COREBANKING_ID
    );
  }

  let coreBankingTransactionIds = await getCoreBankingTransactionIds(
    transactionId,
    transactionModel.createdAt
  );
  if (coreBankingTransactionIds.length > 0) {
    logger.info(
      `reverseTransactionsByInstructionId: number of transactions received from corebanking is ${coreBankingTransactionIds.length} for instructionId ${transactionId}`
    );
    const reverseTransaction = mapToReverseTransaction(transactionModel);
    const isNonBlockingInverse = isFeatureEnabled(
      FEATURE_FLAG.ENABLE_NON_BLOCKING_INVERSE_TRANSACTION_ON_REVERSAL
    );
    for (let i = 0; i < coreBankingTransactionIds.length; i++) {
      let reversalInput: IReverseTransactionInput | undefined = undefined;
      if (reversalReason) {
        reversalInput = {
          notes: reversalReason
        };
      }
      logger.info(
        `reverseTransactionsByInstructionId: reversing transaction corebanking id ${coreBankingTransactionIds[i].id} for instructionId ${transactionId}, isNonBlockingInverse: ${isNonBlockingInverse}`
      );
      const coreTransactionDetail = reverseTransaction.coreBankingTransactions?.find(
        item => item.id === coreBankingTransactionIds[i].id
      );

      if (
        isNonBlockingInverse &&
        coreTransactionDetail &&
        coreBankingTransactionIds[i].type == CoreBankTransactionType.WITHDRAWAL
      ) {
        // Creating inverse transaction of Withdrawal
        const creditTransactionId = await invertWithdrawalTransaction(
          reverseTransaction,
          coreTransactionDetail
        );
        logger.info(
          `reverseTransactionsByInstructionId: reverted original transaction for 
              instructionId: ${transactionId},
              transactionId: ${coreBankingTransactionIds[i].id}
           and got creditTransactionId ${creditTransactionId}`
        );
        creditTransactionId &&
          addInverseTransactionToTransferJourney(
            transactionModel,
            coreTransactionDetail.type,
            creditTransactionId
          );
      } else if (
        isNonBlockingInverse &&
        coreTransactionDetail &&
        coreBankingTransactionIds[i].type == CoreBankTransactionType.DEPOSIT
      ) {
        // Creating inverse transaction of Deposit
        const debitTransactionId = await invertDepositTransaction(
          reverseTransaction
        );
        logger.info(
          `reverseTransactionsByInstructionId: reverted original transaction for
              instructionId ${transactionId},
              transactionId: ${coreBankingTransactionIds[i].id}
           and got debitTransactionId ${debitTransactionId}`
        );
        coreTransactionDetail &&
          debitTransactionId &&
          addInverseTransactionToTransferJourney(
            transactionModel,
            coreTransactionDetail.type,
            debitTransactionId
          );
      } else {
        // Adjusting original transaction
        await depositRepository.reverseTransaction(
          coreBankingTransactionIds[i].id,
          reversalInput
        );
        logger.info(
          `reverseTransactionsByInstructionId: reverted settlementTransactionId ${coreBankingTransactionIds[i].id} for instructionId ${transactionId}`
        );
      }
    }
    await transactionRepository.update(transactionModel.id, {
      coreBankingTransactionIds: transactionModel.coreBankingTransactionIds,
      coreBankingTransactions: transactionModel.coreBankingTransactions,
      journey: transactionModel.journey
    });
    logger.info(
      `reverseTransactionsByInstructionId: reversed all transactions from coreBankingTransactionIds.`
    );
  }
};

const reverseTransaction = async (
  transactionId: string,
  reverseInput: ReverseTransactionInput
): Promise<void> => {
  const transaction = await transactionRepository.getByKey(transactionId);
  if (!transaction) {
    logger.error(
      `reverseTransaction: Matching txn was not found for transactionId: ${transactionId}`
    );
    throw new TransferAppError(
      `Matching txn was not found for transactionId: ${transactionId}`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.TRANSACTION_NOT_FOUND
    );
  }
  const referenceId = transaction.referenceId;
  logger.debug(
    `calling reverseTransactionsByReferenceId with retry, reference id: ${referenceId}`
  );
  try {
    await Util.retry(
      RetryConfig(),
      reverseTransactionsByReferenceId,
      referenceId,
      reverseInput
    );
    await transactionRepository.updateTransactionsByReferenceId(referenceId, {
      status: TransactionStatus.REVERTED
    });
  } catch (error) {
    // TODO: handle reverting failure, e.g update status for expected error, or retry reverting with cron job, or require manual intervention
    logger.error('error reverting transaction: ', error);
    throw new TransferAppError(
      `Error reverting transaction requires manual intervention!`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.CAN_NOT_REVERSE_TRANSACTION
    );
  }
};

const reverseLoanTransaction = async (
  transactionId: string,
  reversalReason?: string,
  transaction?: ITransactionModel
): Promise<void> => {
  const revertTransaction = [];
  if (!transaction) {
    throw new TransferAppError(
      'Transaction not provided!',
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.TRANSACTION_NOT_FOUND
    );
  }

  const coreBankingTransactionIds = transaction.coreBankingTransactions || [];
  if (coreBankingTransactionIds.length > 0) {
    logger.info(
      `reverseLoanTransaction: number of transactions received from corebanking ${coreBankingTransactionIds.length} for transactionId ${transactionId}`
    );
    for (const transactionIdData of coreBankingTransactionIds) {
      logger.info(
        `reverseLoanTransaction: reversing transaction corebanking id ${transactionIdData.id} for transactionId ${transactionId}`
      );
      switch (transactionIdData.type) {
        case TransferJourneyStatusEnum.AMOUNT_DISBURSED:
          const reversalInputDisbursement: ILoanTransactionAdjustment = {
            type: AdjustLoanTransactionTypeEnum.UNDO_DISBURSEMENT
          };
          if (reversalReason) {
            reversalInputDisbursement.notes = reversalReason;
          }
          revertTransaction.push(
            loanRepository.adjustLoanTransaction(
              transaction.sourceAccountNo || '',
              reversalInputDisbursement
            )
          );
          break;
        case TransferJourneyStatusEnum.AMOUNT_REPAID:
          const reversalInputRepayment: ILoanTransactionAdjustment = {
            type: AdjustLoanTransactionTypeEnum.UNDO_REPAYMENT,
            originalTransactionId: transactionIdData.id
          };
          if (reversalReason) {
            reversalInputRepayment.notes = reversalReason;
          }
          revertTransaction.push(
            loanRepository.adjustLoanTransaction(
              transaction.beneficiaryAccountNo || '',
              reversalInputRepayment
            )
          );
          break;
        case TransferJourneyStatusEnum.GL_TRANSACTION_SUCCESS:
          const reversalTransactionId = `${transactionIdData.id}-reversal`;
          coreBankingTransactionHelper.submitGlTransaction(
            transaction,
            true,
            reversalTransactionId
          );
          break;
        default:
          let reversalInput: IReverseTransactionInput | undefined = undefined;
          if (reversalReason) {
            reversalInput = {
              notes: reversalReason
            };
          }
          revertTransaction.push(
            depositRepository.reverseTransaction(
              transactionIdData.id,
              reversalInput
            )
          );
          break;
      }
    }
  }

  await Promise.all(revertTransaction);
};

const transactionReversalService = wrapLogs({
  reverseTransaction,
  reverseTransactionsByInstructionId,
  reverseLoanTransaction
});

export default transactionReversalService;
