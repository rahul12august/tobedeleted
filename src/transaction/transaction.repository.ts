import logger, { wrapLogs } from '../logger';
import moment from 'moment';
import {
  ITransactionModel,
  TransactionDocument,
  TransactionModel
} from './transaction.model';
import { ObjectId } from 'bson';
import { NewEntity } from '../common/common.type';
import { isEmpty, isNull } from 'lodash';
import {
  IConfirmTransactionInput,
  IPagingResult,
  TransactionDetail,
  TransactionListRequest,
  TransactionStatusInput
} from './transaction.type';
import { PaymentServiceTypeEnum } from '@dk/module-common';
import { isCurrentDate, toDate } from '../common/dateUtils';
import transactionHelper from './transaction.helper';
import { ERROR_CODE } from '../common/errors';
import { TransferAppError } from '../errors/AppError';
import {
  ConfirmTransactionStatus,
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from './transaction.enum';
import validations from './transaction.constant';

const convertTransactionDocumentToObject = (document: TransactionDocument) =>
  document.toObject({ getters: true }) as ITransactionModel;

const convertTransactionListDocumentToObject = (
  documents: TransactionDocument[]
) => documents.map(document => convertTransactionDocumentToObject(document));

const create = async (
  transactionModel: NewEntity<ITransactionModel>
): Promise<ITransactionModel> => {
  const newTransactionModel = new TransactionModel(transactionModel);

  await newTransactionModel.save();

  return convertTransactionDocumentToObject(newTransactionModel);
};

const update = async (
  id: string,
  transactionModel: Partial<ITransactionModel>
): Promise<ITransactionModel> => {
  const transaction = await TransactionModel.findOneAndUpdate(
    { _id: id },
    transactionModel,
    { new: true }
  );

  const response =
    transaction && convertTransactionDocumentToObject(transaction);
  if (!response) {
    const detail = `Failed to update transaction info for txnId: ${transactionModel.id}!`;
    logger.error(`update: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION
    );
  }
  return response;
};

const updateTransactionsByReferenceId = async (
  referenceId: string,
  transactionUpdate: Pick<ITransactionModel, 'status'> // should only extend field when needed
): Promise<boolean> => {
  const res = await TransactionModel.updateMany(
    { referenceId },
    {
      $set: transactionUpdate
    }
  );

  return res.n > 0;
};

const findByIdAndUpdate = async (
  txnId: string,
  transactionUpdate: Partial<ITransactionModel>
): Promise<ITransactionModel | null> => {
  const transaction = await TransactionModel.findByIdAndUpdate(
    txnId,
    {
      $set: transactionUpdate
    },
    { new: true }
  );
  return transaction && convertTransactionDocumentToObject(transaction);
};

const findOneByQuery = async (
  query: any
): Promise<ITransactionModel | null> => {
  const transaction = await TransactionModel.findOne(query);
  return transaction && convertTransactionDocumentToObject(transaction);
};

/**
 * @description
 * This method is used to find transactions by thirdparty outgoing and externalId if provided
 * for outgoing we will always have unique thirdpartyoutgoing id so we will always have one record
 * for incoming we may have duplicate external ids(thirdpartyOutgoingId)
 * so in case of multiple transactions we need to filter with externalId_date_accountId
 */
const findTransactionForConfirmation = async (
  input: IConfirmTransactionInput,
  externalId?: string
): Promise<TransactionDocument | undefined> => {
  const transactions = await TransactionModel.find({
    thirdPartyOutgoingId: input.externalId
  });
  let transaction: TransactionDocument | undefined = undefined;
  if (transactions && transactions.length === 1) {
    // For Outgoing transactions
    transaction = transactions[0];
  } else if (transactions && transactions.length > 1 && externalId) {
    // For Incoming transactions should match with externalId first
    transaction = transactions.find(trx => trx.externalId === externalId);
    // Will match with secondaryExternalId if with externalId didn't find any record
    if (isEmpty(transaction)) {
      transaction = transactions.find(
        trx => trx.secondaryExternalId === externalId
      );
    }
  }
  return transaction;
};

const updateIfExists = async (
  input: IConfirmTransactionInput,
  externalId?: string
): Promise<ITransactionModel | null> => {
  const transaction = await findTransactionForConfirmation(input, externalId);
  if (!transaction || isEmpty(transaction)) {
    return null;
  }

  if (
    (input.responseCode === ConfirmTransactionStatus.ERROR_EXPECTED ||
      input.responseCode === ConfirmTransactionStatus.ERROR_UNEXPECTED) &&
    transaction.createdAt &&
    !isCurrentDate(transaction.createdAt)
  ) {
    logger.info(
      `updateIfExists: ignoring reversal of transaction externalId ${input.externalId}, as original transaction is not of same day`
    );
    return null;
  }

  const updatedTransaction = await TransactionModel.findByIdAndUpdate(
    transaction.id,
    {
      $push: {
        reason: {
          responseCode: input.responseCode,
          responseMessage: input.responseMessage,
          transactionDate: input.transactionDate
        },
        journey: {
          status: TransferJourneyStatusEnum.TRANSFER_CONFIRMATION_RECEIVED,
          updatedAt: new Date()
        }
      }
    },
    { new: true }
  );
  return (
    updatedTransaction && convertTransactionDocumentToObject(updatedTransaction)
  );
};

const getByKey = async (id: string): Promise<ITransactionModel | null> => {
  const queryOr: Record<string, string>[] = [
    {
      externalId: id
    }
  ];
  if (ObjectId.isValid(id)) {
    queryOr.push({ _id: id });
  }
  return findOneByQuery({ $or: queryOr });
};

const getByThirdPartyOutgoingId = async (
  id: string
): Promise<ITransactionModel | null> => {
  return findOneByQuery({ thirdPartyOutgoingId: id });
};

const getByQRISRefundIdentifier = async (
  id: string
): Promise<ITransactionModel | null> => {
  return findOneByQuery({
    externalId: id,
    paymentServiceType: PaymentServiceTypeEnum.VOID_QRIS
  });
};

// TODO: deprecated
const getAllByExternalId = async (
  externalId: string
): Promise<ITransactionModel[]> => {
  const transactionList = await TransactionModel.find({
    externalId: externalId
  });
  return (
    transactionList && convertTransactionListDocumentToObject(transactionList)
  );
};

// TODO: deprecated
const getByExternalId = async (
  externalId: string
): Promise<ITransactionModel | null> => {
  return findOneByQuery({
    externalId: externalId
  });
};

const getIncomingNonRefunded = async (
  externalId: string,
  accountNumber: string
): Promise<ITransactionModel | null> =>
  findOneByQuery({
    externalId: { $regex: `${externalId}.*${accountNumber}` },
    paymentServiceType: {
      $nin: [
        PaymentServiceTypeEnum.DOMESTIC_VOID_PAYMENT,
        PaymentServiceTypeEnum.INTERNATIONAL_VOID_PAYMENT
      ]
    }
  });

const getAtomeTxnTobeRefunded = async (
  thirdPartyOutgoingId: string
): Promise<ITransactionModel | null> =>
  findOneByQuery({
    thirdPartyOutgoingId: thirdPartyOutgoingId,
    paymentServiceType: PaymentServiceTypeEnum.ATOME_PAYMENT
  });

const createOrCondition = (cif: string): Record<string, object> => {
  const searchCriteria: Record<string, string>[] = [];
  searchCriteria.push({ sourceCIF: cif });
  searchCriteria.push({ beneficiaryCIF: cif });
  return {
    $or: searchCriteria
  };
};

const getTransactionListQuery = (
  input: TransactionListRequest,
  cif?: string
): object => {
  const searchCriteria: Record<string, any>[] = [];
  if (!cif && input.customerId) {
    logger.info(
      `getTransactionListQuery: Adding ownerCustomerId in search query`
    );
    searchCriteria.push({ ownerCustomerId: input.customerId });
  }
  if (cif) {
    searchCriteria.push(createOrCondition(cif));
  }
  if (input.transactionId) {
    searchCriteria.push({ _id: input.transactionId });
  }
  if (input.startDate) {
    searchCriteria.push({
      createdAt: { $gte: toDate(input.startDate) }
    });
  }
  if (input.endDate) {
    searchCriteria.push({
      createdAt: { $lte: toDate(input.endDate).setHours(23, 59, 59, 999) }
    });
  }
  if (!input.startDate && !input.endDate) {
    const fromDate = transactionHelper.getTransactionSearchStartDate();
    searchCriteria.push({
      createdAt: { $gte: fromDate }
    });
    searchCriteria.push({
      createdAt: { $lte: new Date().setHours(23, 59, 59, 999) }
    });
  }
  if (input.transactionStatus) {
    searchCriteria.push({ status: { $in: input.transactionStatus } });
  }
  if (input.beneficiaryAccountNo) {
    searchCriteria.push({ beneficiaryAccountNo: input.beneficiaryAccountNo });
  }
  if (input.sourceAccountNo) {
    searchCriteria.push({ sourceAccountNo: input.sourceAccountNo });
  }
  if (input.sourceBankCode) {
    searchCriteria.push({ sourceBankCode: input.sourceBankCode });
  }
  if (input.beneficiaryBankCode) {
    searchCriteria.push({ beneficiaryBankCode: input.beneficiaryBankCode });
  }
  if (input.accountNo) {
    const account = new RegExp(input.accountNo, 'i');
    searchCriteria.push({
      $or: [{ sourceAccountNo: account }, { beneficiaryAccountNo: account }]
    });
  }
  if (input.transactionAmount) {
    searchCriteria.push({
      transactionAmount: {
        $gte: input.transactionAmount,
        $lt: input.transactionAmount + 1
      }
    });
  }

  return isEmpty(searchCriteria)
    ? {}
    : {
        $and: searchCriteria
      };
};

const getTransactionListByCriteria = async (
  input: TransactionListRequest,
  cif?: string
): Promise<IPagingResult<ITransactionModel>> => {
  const limit = input.limit || validations.DEFAULT_RECORD_PER_PAGE;
  const page = input.page || 1;
  const skip = (page - 1) * limit;

  const query = getTransactionListQuery(input, cif);
  const transactionList = await TransactionModel.find(query)
    .sort({
      createdAt: -1
    })
    .skip(skip)
    .limit(limit)
    .exec();

  const totalCount = await TransactionModel.countDocuments(query);
  return {
    list:
      transactionList &&
      convertTransactionListDocumentToObject(transactionList),
    limit,
    page,
    totalPage: Math.ceil(totalCount / limit)
  };
};

const getTransactionStatusQuery = (input: TransactionStatusInput): object => {
  const searchCriteria: Record<string, any>[] = [];
  if (input.customerId) {
    searchCriteria.push({
      ownerCustomerId: input.customerId
    });
  }
  if (input.paymentInstructionId) {
    searchCriteria.push({
      paymentInstructionId: input.paymentInstructionId
    });
  }
  const transactionDate = input.transactionDate
    ? input.transactionDate
    : new Date();
  const dateFrom = transactionDate.setDate(transactionDate.getDate() - 1);
  const dateUntil = transactionDate.setDate(transactionDate.getDate() + 2);
  searchCriteria.push({
    createdAt: {
      $gte: dateFrom,
      $lte: dateUntil
    }
  });
  return isEmpty(searchCriteria) ? {} : { $and: searchCriteria };
};

const getTransactionStatusByCriteria = async (
  input: TransactionStatusInput
): Promise<ITransactionModel | undefined> => {
  const query: object = getTransactionStatusQuery(input);
  const transaction: TransactionDocument | null = await TransactionModel.findOne(
    query
  ).exec();
  if (isNull(transaction)) {
    logger.error(`Transaction not found: 
      ownerCustomerId: ${input.customerId},
      paymentInstructionId: ${input.paymentInstructionId},
      transactionDate: ${input.transactionDate}`);
    return;
  }
  return convertTransactionDocumentToObject(transaction);
};

const getPendingTransactionList = async (
  paymentServiceCodes: string[],
  minTransactionAge: number,
  maxTransactionAge: number,
  recordLimit: number
): Promise<ITransactionModel[]> => {
  logger.debug(`outOfBanTxAgeMax: ${minTransactionAge}`);
  logger.debug(`outOfBanTxAgeMin: ${maxTransactionAge}`);
  logger.debug(`outOfBanTxLimit: ${recordLimit}`);
  const transactionList = await TransactionModel.find({
    paymentServiceCode: { $in: paymentServiceCodes },
    status: {
      $in: [
        TransactionStatus.SUBMITTED,
        TransactionStatus.PENDING,
        TransactionStatus.SUBMITTING
      ]
    },
    createdAt: {
      $lte: moment
        .utc()
        .subtract(minTransactionAge, 'minutes')
        .toDate(),
      $gte: moment
        .utc()
        .subtract(maxTransactionAge, 'minutes')
        .toDate()
    }
  })
    .limit(recordLimit)
    .exec();
  return (
    transactionList && convertTransactionListDocumentToObject(transactionList)
  );
};

const findTransactionByQuery = async (
  query: object,
  projection: { [x in keyof Partial<TransactionDetail>]: number }
): Promise<ITransactionModel[]> => {
  const transactions = await TransactionModel.find(query, projection).exec();
  return transactions && convertTransactionListDocumentToObject(transactions);
};

const createIfNotExist = async (
  transactionModel: NewEntity<ITransactionModel>
): Promise<ITransactionModel> => {
  const now = new Date();
  const newTransactionModel = new TransactionModel({
    ...transactionModel,
    createdAt: now,
    updatedAt: now
  });
  const result = await TransactionModel.findOneAndUpdate(
    {
      externalId: newTransactionModel.externalId,
      paymentServiceType: newTransactionModel.paymentServiceType
    },
    { $setOnInsert: newTransactionModel },
    { upsert: true, timestamps: false }
  );
  // result == null means successful insertion
  if (result) {
    throw new TransferAppError(
      `Error in createIfNotExist: Duplicate transaction found with same externalId = ${transactionModel.externalId} and paymentServiceType = ${transactionModel.paymentServiceType}`,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.DUPLICATE_EXTERNAL_ID
    );
  }
  return convertTransactionDocumentToObject(newTransactionModel);
};

const transactionRepository = {
  getByKey,
  findOneByQuery,
  create,
  update,
  updateTransactionsByReferenceId,
  getByExternalId,
  updateIfExists,
  getIncomingNonRefunded,
  getAtomeTxnTobeRefunded,
  getTransactionListByCriteria,
  findByIdAndUpdate,
  getAllByExternalId,
  getTransactionStatusByCriteria,
  getByThirdPartyOutgoingId,
  getByQRISRefundIdentifier,
  getPendingTransactionList,
  findTransactionByQuery,
  createIfNotExist
};

export default wrapLogs(transactionRepository);
