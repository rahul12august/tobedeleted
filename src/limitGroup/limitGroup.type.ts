export interface LimitGroupCodes {
  RTOL_LIMIT_GROUP_CODE: string;
  SKN_LIMIT_GROUP_CODE: string;
  RTGS_LIMIT_GROUP_CODE: string;
}

export interface LimitGroupTemplate {
  limitGroupCodes: (paymentServiceCode?: string) => LimitGroupCodes;
  isEligible: (paymentServiceCode: string) => boolean;
}
