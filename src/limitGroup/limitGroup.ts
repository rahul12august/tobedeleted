import { LimitGroupCodes } from './limitGroup.type';
import logger, { wrapLogs } from '../logger';
import {
  lfsTemplate,
  bfsTemplate,
  btsTemplate,
  shariaTemplate
} from './templates';

const templates = [lfsTemplate, bfsTemplate, btsTemplate, shariaTemplate];

const getLimitGroupCodes = (paymentServiceCode?: string): LimitGroupCodes => {
  if (paymentServiceCode) {
    const template = templates.find(template =>
      template.isEligible(paymentServiceCode)
    );
    if (template) {
      logger.info(
        `getLimitGroupCodes: limit group codes template found for paymentServiceCode:${paymentServiceCode}`
      );
      return template.limitGroupCodes(paymentServiceCode);
    }
  }
  logger.info(
    `getLimitGroupCodes: template not found, returning default LFS limit group codes`
  );
  return lfsTemplate.limitGroupCodes(paymentServiceCode);
};

export default wrapLogs({ getLimitGroupCodes });
