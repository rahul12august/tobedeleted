import { LimitGroupTemplate, LimitGroupCodes } from './limitGroup.type';

export const lfsTemplate: LimitGroupTemplate = {
  limitGroupCodes: (paymentServiceCode?: string): LimitGroupCodes => ({
    RTOL_LIMIT_GROUP_CODE: 'L002',
    SKN_LIMIT_GROUP_CODE: 'L003',
    RTGS_LIMIT_GROUP_CODE:
      paymentServiceCode && paymentServiceCode === 'RDN_KSEI' ? 'L017' : 'L004'
  }),
  isEligible: (paymentServiceCode: string): boolean =>
    paymentServiceCode === 'RTOL' ||
    paymentServiceCode === 'SKN' ||
    paymentServiceCode === 'RTGS' ||
    paymentServiceCode === 'RDN_KSEI' ||
    paymentServiceCode === 'RDN_WITHDRAW_RTGS' ||
    paymentServiceCode === 'RDN_WITHDRAW_SKN'
};

export const bfsTemplate: LimitGroupTemplate = {
  limitGroupCodes: (): LimitGroupCodes => ({
    RTOL_LIMIT_GROUP_CODE: 'L013',
    SKN_LIMIT_GROUP_CODE: 'L014',
    RTGS_LIMIT_GROUP_CODE: 'L012'
  }),
  isEligible: (paymentServiceCode: string): boolean =>
    paymentServiceCode === 'RTOL_FOR_BUSINESS' ||
    paymentServiceCode === 'SKN_FOR_BUSINESS' ||
    paymentServiceCode === 'RTGS_FOR_BUSINESS'
};

export const btsTemplate: LimitGroupTemplate = {
  limitGroupCodes: (): LimitGroupCodes => ({
    RTOL_LIMIT_GROUP_CODE: 'L002',
    SKN_LIMIT_GROUP_CODE: 'L003',
    RTGS_LIMIT_GROUP_CODE: 'L004'
  }),
  isEligible: (paymentServiceCode: string): boolean =>
    paymentServiceCode === 'BRANCH_RTGS' || paymentServiceCode === 'BRANCH_SKN'
};

export const shariaTemplate: LimitGroupTemplate = {
  limitGroupCodes: (): LimitGroupCodes => ({
    RTOL_LIMIT_GROUP_CODE: 'L002',
    SKN_LIMIT_GROUP_CODE: 'L003',
    RTGS_LIMIT_GROUP_CODE: 'L004'
  }),
  isEligible: (paymentServiceCode: string): boolean =>
    paymentServiceCode === 'SKN_SHARIA' || paymentServiceCode === 'RTGS_SHARIA'
};
