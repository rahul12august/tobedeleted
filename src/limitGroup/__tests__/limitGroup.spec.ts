import limitGroup from '../limitGroup';

describe('limitGroup.helper', () => {
  describe('getLimitGroupCodes', () => {
    test('get BFS limit group codes when paymentServiceCode is RTOL_FOR_BUSINESS', () => {
      const result = limitGroup.getLimitGroupCodes('RTOL_FOR_BUSINESS');

      expect(result).toEqual({
        RTOL_LIMIT_GROUP_CODE: 'L013',
        SKN_LIMIT_GROUP_CODE: 'L014',
        RTGS_LIMIT_GROUP_CODE: 'L012'
      });
    });

    test('get BFS limit group codes when paymentServiceCode is SKN_FOR_BUSINESS', () => {
      const result = limitGroup.getLimitGroupCodes('SKN_FOR_BUSINESS');

      expect(result).toEqual({
        RTOL_LIMIT_GROUP_CODE: 'L013',
        SKN_LIMIT_GROUP_CODE: 'L014',
        RTGS_LIMIT_GROUP_CODE: 'L012'
      });
    });

    test('get BFS limit group codes when paymentServiceCode is RTGS_FOR_BUSINESS', () => {
      const result = limitGroup.getLimitGroupCodes('RTGS_FOR_BUSINESS');

      expect(result).toEqual({
        RTOL_LIMIT_GROUP_CODE: 'L013',
        SKN_LIMIT_GROUP_CODE: 'L014',
        RTGS_LIMIT_GROUP_CODE: 'L012'
      });
    });

    test('get SHARIA limit group codes when paymentServiceCode is SKN_SHARIA', () => {
      const result = limitGroup.getLimitGroupCodes('SKN_SHARIA');

      expect(result).toEqual({
        RTOL_LIMIT_GROUP_CODE: 'L002',
        SKN_LIMIT_GROUP_CODE: 'L003',
        RTGS_LIMIT_GROUP_CODE: 'L004'
      });
    });

    test('get SHARIA limit group codes when paymentServiceCode is RTGS_SHARIA', () => {
      const result = limitGroup.getLimitGroupCodes('RTGS_SHARIA');

      expect(result).toEqual({
        RTOL_LIMIT_GROUP_CODE: 'L002',
        SKN_LIMIT_GROUP_CODE: 'L003',
        RTGS_LIMIT_GROUP_CODE: 'L004'
      });
    });

    test('get LFS limit group codes when paymentServiceCode is undefined', () => {
      const result = limitGroup.getLimitGroupCodes();

      expect(result).toEqual({
        RTOL_LIMIT_GROUP_CODE: 'L002',
        SKN_LIMIT_GROUP_CODE: 'L003',
        RTGS_LIMIT_GROUP_CODE: 'L004'
      });
    });

    test('get LFS limit group codes when paymentServiceCode is RTOL', () => {
      const result = limitGroup.getLimitGroupCodes('RTOL');

      expect(result).toEqual({
        RTOL_LIMIT_GROUP_CODE: 'L002',
        SKN_LIMIT_GROUP_CODE: 'L003',
        RTGS_LIMIT_GROUP_CODE: 'L004'
      });
    });

    test('get LFS limit group codes when paymentServiceCode is RTGS', () => {
      const result = limitGroup.getLimitGroupCodes('RTGS');

      expect(result).toEqual({
        RTOL_LIMIT_GROUP_CODE: 'L002',
        SKN_LIMIT_GROUP_CODE: 'L003',
        RTGS_LIMIT_GROUP_CODE: 'L004'
      });
    });

    test('get LFS limit group codes when paymentServiceCode is RDN_KSEI', () => {
      const result = limitGroup.getLimitGroupCodes('RDN_KSEI');

      expect(result).toEqual({
        RTOL_LIMIT_GROUP_CODE: 'L002',
        SKN_LIMIT_GROUP_CODE: 'L003',
        RTGS_LIMIT_GROUP_CODE: 'L017'
      });
    });

    test('get LFS limit group codes when paymentServiceCode is SKN', () => {
      const result = limitGroup.getLimitGroupCodes('SKN');

      expect(result).toEqual({
        RTOL_LIMIT_GROUP_CODE: 'L002',
        SKN_LIMIT_GROUP_CODE: 'L003',
        RTGS_LIMIT_GROUP_CODE: 'L004'
      });
    });
  });
});
