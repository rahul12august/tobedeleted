import { ERROR_CODE } from '../common/errors';
import { TransferAppError } from '../errors/AppError';
import logger, { wrapLogs } from '../logger';
import {
  ConfirmTransactionStatus,
  TransferFailureReasonActor
} from '../transaction/transaction.enum';
import {
  ConfirmTransactionResponse,
  IConfirmTransactionInput
} from '../transaction/transaction.type';
import transactionConfirmationService from '../transaction/transactionConfirmation.service';
import { ITokopediaPayoutStatusEnum } from './tokopedia.enum';
import { ConfirmTransactionRequest } from './tokopedia.type';
import billDetailService from '../billPayment/billDetail/billDetail.service';
import transactionRepository from '../transaction/transaction.repository';

const confirmTransaction = async (
  input: ConfirmTransactionRequest
): Promise<ConfirmTransactionResponse> => {
  logger.info(
    `Tokopedia: Transaction Confirmation with external id: ${input.id} status: ${input.attributes.status}`
  );
  let responseCode;
  switch (input.attributes.status) {
    case ITokopediaPayoutStatusEnum.FAILED:
      responseCode = ConfirmTransactionStatus.ERROR_EXPECTED;
      break;
    case ITokopediaPayoutStatusEnum.PENDING:
      responseCode = ConfirmTransactionStatus.REQUEST_TIMEOUT;
      break;
    case ITokopediaPayoutStatusEnum.SUCCESS:
      responseCode = ConfirmTransactionStatus.SUCCESS;
      break;
    default:
      const detail = `Unsupported confirm transaction status: ${input.attributes.status}!`;
      logger.error(`confirmTransaction: ${detail}`);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.TOKOPEDIA,
        ERROR_CODE.UNSUPPORTED_CONFIRM_TRANSACTION_STATUS
      );
  }

  await billDetailService.updateOrderDetails(input.id, input);

  const transactionInput: IConfirmTransactionInput = {
    externalId: input.id,
    amount: Number(input.attributes.sales_price),
    responseCode,
    responseMessage: input.attributes.error_detail
  };
  const response: ConfirmTransactionResponse = await transactionConfirmationService.confirmTransaction(
    transactionInput
  );
  const result = await transactionRepository.getByExternalId(input.id);
  if (!result) {
    const detail = `Failed to identify transaction by externalId: ${input.id}!`;
    logger.error(`confirmTransaction: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.TOKOPEDIA,
      ERROR_CODE.INVALID_EXTERNALID
    );
  }
  return response;
};

const tokopediaService = wrapLogs({
  confirmTransaction
});

export default tokopediaService;
