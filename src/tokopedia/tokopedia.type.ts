import hapi from '@hapi/hapi';
import * as Joi from '@hapi/joi';
import { ITokopediaPayoutStatusEnum } from './tokopedia.enum';
import { confirmTransactionFieldsValidator } from './tokopedia.validator';

export interface ITokopediaRequestFields {
  name?: string;
  value?: string;
}

export interface ITokopediaResponseAttributes {
  client_number: string;
  error_code?: string;
  error_detail?: string;
  fields?: [ITokopediaRequestFields];
  fulfilled_at: string | null;
  partner_fee: number;
  product_code: string;
  sales_price: number;
  status: ITokopediaPayoutStatusEnum;
}

export interface ITokopediaTransactionResponse {
  type: string;
  id: string;
  attributes: ITokopediaResponseAttributes;
}

export interface ITokopediaRequestAttributes {
  product_code: string;
  client_number: string;
  amount: number;
  fields?: [ITokopediaRequestFields];
}

export interface ITokopediaTransactionRequestPayload {
  type: string;
  id: string;
  attributes: ITokopediaRequestAttributes;
}

export type ConfirmTransactionRequest = Joi.extractType<
  typeof confirmTransactionFieldsValidator
>;

export interface IConfirmTransactionRequest extends hapi.Request {
  payload: {
    data: any;
  };
}

export interface ITokopediaTokenResult {
  access_token: string;
  expires_in: number;
  token_type: string;
}

export interface ISubmitPaymentTransactionRequest extends hapi.Request {
  payload: ITokopediaTransactionRequestPayload;
}
