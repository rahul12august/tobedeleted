import tokopediaSubmissionTemplate from '../tokopediaSubmissionTemplate.service';
import { BankChannelEnum, Util } from '@dk/module-common';
import {
  ITokopediaTransactionRequestPayload,
  ITokopediaTransactionResponse
} from '../tokopedia.type';
import {
  getTokopediaTransactionData,
  getTokopediaTransactionPayload,
  transactionModel
} from '../__mocks__/tokopedia.data';

import tokopediaRepository from '../tokopedia.repository';
import transactionRepository from '../../transaction/transaction.repository';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../../transaction/transaction.enum';
import { Rail } from '@dk/module-common';
import { ITokopediaPayoutStatusEnum } from '../tokopedia.enum';
import { ITransactionModel } from '../../transaction/transaction.model';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import { sumBy } from 'lodash';

jest.mock('../tokopedia.repository');
jest.mock('../../transaction/transaction.repository');
jest.mock('../tokopedia.service');
jest.mock('../../billPayment/billDetail/billDetail.service');

describe('tokopediaSubmissionTemplate', () => {
  class NetworkError extends Error {
    status: number;
    code?: string;
    constructor(status = 501, code?: string) {
      super();
      this.status = status;
      this.code = code;
    }
  }
  let mockedRetry = jest
    .spyOn(Util, 'retry')
    .mockImplementation(async (_, fn, ...args) => await fn(...args));
  describe('isEligible', () => {
    it(`should return true when beneficiaryBankCodeChannel is ${BankChannelEnum.TOKOPEDIA}`, () => {
      // when
      const eligible = tokopediaSubmissionTemplate.isEligible(transactionModel);

      // then
      expect(eligible).toBeTruthy();
    });

    it(`should return false when beneficiaryBankCodeChannel is not ${BankChannelEnum.TOKOPEDIA}`, () => {
      // when
      const eligible = tokopediaSubmissionTemplate.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL
      });

      // then
      expect(eligible).not.toBeTruthy();
    });
  });

  describe('submitTransaction', () => {
    const tokopediaTransactionPayload: ITokopediaTransactionRequestPayload = getTokopediaTransactionPayload();
    const tokopediaTransactionData = getTokopediaTransactionData();
    beforeEach(() => {
      (tokopediaRepository.submitTransaction as jest.Mock).mockResolvedValue(
        tokopediaTransactionData
      );
    });
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should submit transaction to tokopedia with correct payload', async () => {
      // Given
      const expectedPayload = {
        ...tokopediaTransactionPayload,
        id: transactionModel.externalId
      };
      const inputTransactionModel: ITransactionModel = {
        ...transactionModel,
        id: expectedPayload.id
      };
      const fee =
        inputTransactionModel.fees && inputTransactionModel.fees.length
          ? sumBy(inputTransactionModel.fees, 'feeAmount')
          : 0;
      expectedPayload.attributes.amount =
        expectedPayload.attributes.amount + fee;
      const tokopediaPayoutResponse: ITokopediaTransactionResponse = {
        id: transactionModel.externalId,
        type: tokopediaTransactionData.type,
        attributes: {
          ...tokopediaTransactionData.attributes,
          status: ITokopediaPayoutStatusEnum.SUCCESS
        }
      };
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({});
      (tokopediaRepository.submitTransaction as jest.Mock).mockResolvedValueOnce(
        tokopediaPayoutResponse
      );
      // When
      await tokopediaSubmissionTemplate.submitTransaction(
        inputTransactionModel
      );

      // Then
      expect(tokopediaRepository.submitTransaction).toHaveBeenCalledWith(
        expectedPayload
      );
      expect(transactionRepository.update).toHaveBeenCalledWith(
        inputTransactionModel.id,
        {
          status: TransactionStatus.SUBMITTED,
          thirdPartyOutgoingId: inputTransactionModel.externalId
        }
      );
    });

    it('should retry first times when TOKOPEDIA repository throw network error before giving response', async () => {
      const networkError = new NetworkError();
      const expectedNetworkError = {
        ...networkError,
        code: 'ECONNABORTED'
      };
      mockedRetry.mockRestore();
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});
      (tokopediaRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        expectedNetworkError
      );

      const result = await tokopediaSubmissionTemplate.submitTransaction(
        transactionModel
      );
      expect(result).toBeDefined();
      expect(tokopediaRepository.submitTransaction).toHaveBeenCalledTimes(1);
    });

    it(`should throw error ${ERROR_CODE.UNEXPECTED_ERROR} when tokopedia returned bad request error`, async () => {
      (tokopediaRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.UNEXPECTED_ERROR
        )
      );
      const error = await tokopediaSubmissionTemplate
        .submitTransaction(transactionModel)
        .catch(err => err);

      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.detail).toBe('Test error!');
      expect(error.actor).toBe(TransferFailureReasonActor.UNKNOWN);
      expect(error.errorCode).toBe(ERROR_CODE.UNEXPECTED_ERROR);
      expect(tokopediaRepository.submitTransaction).toHaveBeenCalledTimes(1);
    });

    it(`should throw error ${ERROR_CODE.ERROR_FROM_TOKOPEDIA} when tokopedia returned error detail`, async () => {
      (tokopediaRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.ERROR_FROM_TOKOPEDIA
        )
      );
      const error = await tokopediaSubmissionTemplate
        .submitTransaction(transactionModel)
        .catch(err => err);
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.detail).toBe('Test error!');
      expect(error.actor).toBe(TransferFailureReasonActor.UNKNOWN);
      expect(error.errorCode).toBe(ERROR_CODE.ERROR_FROM_TOKOPEDIA);
      expect(tokopediaRepository.submitTransaction).toHaveBeenCalledTimes(1);
    });
  });

  describe('Rail check', () => {
    it(`should have rail property defined`, () => {
      // when
      const rail = tokopediaSubmissionTemplate.getRail?.();

      // then
      expect(rail).toEqual(Rail.TOKOPEDIA);
    });
  });
});
