import { BankChannelEnum } from '@dk/module-common';
import switchingTransactionService from '../../transaction/transactionConfirmation.service';
import { getTransactionDetail } from '../../transaction/__mocks__/transaction.data';
import transactionRepository from '../../transaction/transaction.repository';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../../transaction/transaction.enum';
import tokopediaService from '../tokopedia.service';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import { getTokopediaTransactionData } from '../__mocks__/tokopedia.data';
import { ITokopediaTransactionResponse } from '../tokopedia.type';
import { ITokopediaPayoutStatusEnum } from '../tokopedia.enum';
import billDetailService from '../../billPayment/billDetail/billDetail.service';

jest.mock('../../transaction/transactionConfirmation.service');
jest.mock('../../transaction/transaction.repository');
jest.mock('../../billPayment/billDetail/billDetail.service');

describe('tokopedia.service', () => {
  // given
  const transactionModel = {
    ...getTransactionDetail(),
    beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
    requireThirdPartyOutgoingId: true,
    debitTransactionCode: 'TFD60',
    thirdPartyOutgoingId: '21042T000041',
    transactionAmount: 50000002,
    paymentServiceCode: 'BIL01'
  };

  const tokopediaTransactionData = getTokopediaTransactionData();
  describe('confirmTransaction', () => {
    it('should confirm the transaction to succeed when correct payload passed', async () => {
      const tokopediaPayoutResponse: ITokopediaTransactionResponse = {
        id: transactionModel.externalId,
        type: tokopediaTransactionData.type,
        attributes: {
          ...tokopediaTransactionData.attributes,
          status: ITokopediaPayoutStatusEnum.SUCCESS
        }
      };
      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (transactionRepository.getByExternalId as jest.Mock).mockResolvedValueOnce(
        {
          ...transactionModel,
          status: TransactionStatus.SUCCEED
        }
      );
      (billDetailService.updateOrderDetails as jest.Mock).mockResolvedValueOnce(
        null
      );

      const result = await tokopediaService.confirmTransaction(
        tokopediaPayoutResponse
      );

      expect(result).toBeUndefined();
      expect(
        switchingTransactionService.confirmTransaction
      ).toHaveBeenCalledWith({
        externalId: tokopediaPayoutResponse.id,
        amount: Number(tokopediaPayoutResponse.attributes.sales_price),
        responseCode: 200,
        responseMessage: undefined
      });
      expect(transactionRepository.getByExternalId).toHaveBeenCalledWith(
        transactionModel.externalId
      );
      expect(billDetailService.updateOrderDetails).toHaveBeenCalledWith(
        tokopediaPayoutResponse.id,
        tokopediaPayoutResponse
      );
    });

    it('should throw the error when transaction not found', async () => {
      const tokopediaPayoutResponse: ITokopediaTransactionResponse = {
        id: transactionModel.externalId,
        type: tokopediaTransactionData.type,
        attributes: {
          ...tokopediaTransactionData.attributes,
          status: ITokopediaPayoutStatusEnum.SUCCESS
        }
      };
      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (transactionRepository.getByExternalId as jest.Mock).mockResolvedValueOnce(
        null
      );
      const error = await tokopediaService
        .confirmTransaction(tokopediaPayoutResponse)
        .catch(err => err);

      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.actor).toEqual(TransferFailureReasonActor.TOKOPEDIA);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_EXTERNALID);
      expect(error.detail).toEqual(
        'Failed to identify transaction by externalId: externalId!'
      );
      expect(
        switchingTransactionService.confirmTransaction
      ).toHaveBeenCalledWith({
        externalId: tokopediaPayoutResponse.id,
        amount: Number(tokopediaPayoutResponse.attributes.sales_price),
        responseCode: 200,
        responseMessage: undefined
      });
    });

    it('should confirm the transaction to declined when correct payload passed', async () => {
      const tokopediaPayoutResponse: ITokopediaTransactionResponse = {
        id: transactionModel.externalId,
        type: tokopediaTransactionData.type,
        attributes: {
          ...tokopediaTransactionData.attributes,
          status: ITokopediaPayoutStatusEnum.FAILED,
          // eslint-disable-next-line @typescript-eslint/camelcase
          error_detail: 'third_party_error_message'
        }
      };
      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (transactionRepository.getByExternalId as jest.Mock).mockResolvedValueOnce(
        {
          ...transactionModel,
          status: TransactionStatus.DECLINED
        }
      );
      (billDetailService.updateOrderDetails as jest.Mock).mockResolvedValueOnce(
        null
      );

      const result = await tokopediaService.confirmTransaction(
        tokopediaPayoutResponse
      );

      expect(result).toBeUndefined();
      expect(
        switchingTransactionService.confirmTransaction
      ).toHaveBeenCalledWith({
        externalId: tokopediaPayoutResponse.id,
        amount: Number(tokopediaPayoutResponse.attributes.sales_price),
        responseCode: 400,
        responseMessage: 'third_party_error_message'
      });
      expect(transactionRepository.getByExternalId).toHaveBeenCalledWith(
        transactionModel.externalId
      );
      expect(billDetailService.updateOrderDetails).toHaveBeenCalledWith(
        tokopediaPayoutResponse.id,
        tokopediaPayoutResponse
      );
    });
  });
});
