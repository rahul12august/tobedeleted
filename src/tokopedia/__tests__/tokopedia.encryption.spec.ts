import crypto from 'crypto';
import { getTokopediaTransactionPayload } from '../__mocks__/tokopedia.data';
import { generateSignature, verifySignature } from '../tokopedia.encryption';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import NodeRSA from 'node-rsa';

process.env.TOKOPEDIA_PUBLIC_KEY = `-----BEGIN PUBLIC KEY-----
MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgFDV+vvLM2rDPolUDTKesquor3aY
zn/DCKwNz4jv8ZfiGFn9JySrgy/dmtiYveAW09W6zby4Z4hd1eIRlGQoDAXHxFee
jl0Ik2QPZiMhaoh6VIk82IhK4lAh4vCNbCFfsNOofzOKK/LUI/dtO7G3IsCHiyI5
edJQhjGzkeXgICnnAgMBAAE=
-----END PUBLIC KEY-----`;
jest.mock('crypto');

describe('tokopedia.encryption', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });
  const tokopediaTransactionPayload = getTokopediaTransactionPayload();
  describe('generateSignature', () => {
    it('Should return valid signature', async () => {
      const signature = 'OpH++zdKpBwQFx6medZnLWY+==';
      (crypto.sign as jest.Mock).mockResolvedValueOnce(signature);
      const stringifiedPayload = JSON.stringify(tokopediaTransactionPayload);
      const result = await generateSignature(stringifiedPayload);
      expect(result).toBeDefined();
      expect(result).toBe(signature);
    });
  });

  describe('verifySignature', () => {
    const strigifiedPayload = JSON.stringify(tokopediaTransactionPayload);
    it('Should return true when token is valid', async () => {
      const bufferSignature = Buffer.from(
        'OpH++zdKpBwQFx6medZnLWY+==',
        'base64'
      );
      const hashMock: any = {
        update: jest.fn().mockReturnThis(),
        digest: jest.fn(() => 'encrypt 123')
      };

      const createHashMock = jest
        .spyOn(crypto, 'createHash')
        .mockImplementationOnce(() => hashMock);
      const NodeRSAMock = jest
        .spyOn(NodeRSA.prototype, 'getKeySize')
        .mockImplementation(() => 1000);
      (crypto.verify as jest.Mock).mockResolvedValueOnce(true);
      const result = await verifySignature(
        strigifiedPayload,
        'OpH++zdKpBwQFx6medZnLWY+=='
      );
      expect(result).toBeUndefined();
      expect(crypto.verify).toHaveBeenCalledWith(
        'sha256',
        Buffer.from(strigifiedPayload),
        {
          key: process.env.TOKOPEDIA_PUBLIC_KEY,
          padding: crypto.constants.RSA_PKCS1_PSS_PADDING,
          saltLength: 112
        },
        bufferSignature
      );
      expect(createHashMock).toBeCalledWith('md5');
      expect(hashMock.update).toBeCalledWith(strigifiedPayload);
      expect(hashMock.digest).toBeCalledWith('hex');
      expect(NodeRSAMock).toBeDefined();
    });

    it('Should throw error when token is invalid', async () => {
      const bufferSignature = Buffer.from(
        'OpH++zdKpBwQFx6medZnLWY+==',
        'base64'
      );
      const hashMock: any = {
        update: jest.fn().mockReturnThis(),
        digest: jest.fn(() => 'encrypt 123')
      };

      const createHashMock = jest
        .spyOn(crypto, 'createHash')
        .mockImplementationOnce(() => hashMock);
      const NodeRSAMock = jest
        .spyOn(NodeRSA.prototype, 'getKeySize')
        .mockImplementation(() => 1000);
      (crypto.verify as jest.Mock).mockResolvedValueOnce(false);
      let error;
      try {
        await verifySignature(strigifiedPayload, 'OpH++zdKpBwQFx6medZnLWY+==');
      } catch (err) {
        error = err;
      }

      expect(error).toBeDefined();
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_SIGNATURE);
      expect(crypto.verify).toHaveBeenCalledWith(
        'sha256',
        Buffer.from(strigifiedPayload),
        {
          key: process.env.TOKOPEDIA_PUBLIC_KEY,
          padding: crypto.constants.RSA_PKCS1_PSS_PADDING,
          saltLength: 112
        },
        bufferSignature
      );
      expect(createHashMock).toBeCalledWith('md5');
      expect(hashMock.update).toBeCalledWith(strigifiedPayload);
      expect(hashMock.digest).toBeCalledWith('hex');
      expect(NodeRSAMock).toBeDefined();
    });
  });
});
