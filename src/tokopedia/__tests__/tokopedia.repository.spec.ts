import httpClient, { tokenHTTPClient } from '../httpClient';
import tokopediaRepository from '../tokopedia.repository';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import {
  getTokopediaTransactionData,
  getTokopediaTransactionPayload,
  getTokopediaErrorTransactionData
} from '../__mocks__/tokopedia.data';
import { config } from '../../config';
import { generateSignature } from '../tokopedia.encryption';
import redis from '../../common/redis';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

const { transactionUrl } = config.get('tokopedia');

jest.mock('../httpClient');
jest.mock('../tokopedia.encryption');

describe('tokopedia.repository', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('should submit transaction', () => {
    const tokopediaTransactionPayload = getTokopediaTransactionPayload();
    const tokopediaTransaction = getTokopediaTransactionData();
    const tokopediaErrorTransaction = getTokopediaErrorTransactionData();

    it('should return tokopedia transaction success', async () => {
      const signature = 'OpH++zdKpBwQFx6medZnLWY+==';
      const accessToken = '2krP4WcLxse1lCpAuYTwFeee';
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: { data: { ...tokopediaTransaction } }
      });
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);
      const tokenDetails = {
        /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
        access_token: accessToken,
        expires_in: 1234567,
        token_type: 'order'
      };
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);
      const expected = await tokopediaRepository.submitTransaction(
        tokopediaTransactionPayload
      );

      expect(httpClient.post).toBeCalledWith(
        transactionUrl,
        {
          data: {
            ...tokopediaTransactionPayload
          }
        },
        {
          headers: {
            signature: signature,
            Authorization: `Bearer ${accessToken}`
          }
        }
      );
      expect(expected).toEqual(tokopediaTransaction);
    });

    it('should return token success when invalid ttl because using default ttl', async () => {
      const accessToken = '2krP4WcLxse1lCpAuYTwFeee';
      const tokenDetails = {
        /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
        access_token: accessToken,
        expires_in: 100,
        token_type: 'order'
      };
      (redis.get as jest.Mock).mockResolvedValueOnce(null);
      (tokenHTTPClient.post as jest.Mock).mockResolvedValueOnce({
        data: tokenDetails
      });

      try {
        await tokopediaRepository.getToken();
      } catch (e) {
        expect(e.errorCode).toEqual(
          ERROR_CODE.TOKOPEDIA_AUTH_TOKEN_ACCESS_ERROR
        );
      }
    });

    it(`should throw error with code ${ERROR_CODE.ERROR_FROM_TOKOPEDIA} when result return error details`, async () => {
      const signature = 'OpH++zdKpBwQFx6medZnLWY+==';
      const accessToken = '2krP4WcLxse1lCpAuYTwFeee';
      (redis.get as jest.Mock).mockResolvedValueOnce({
        /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
        access_token: accessToken,
        expires_in: 1234567,
        token_type: 'order'
      });
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: tokopediaErrorTransaction
      });

      const error = await tokopediaRepository
        .submitTransaction(tokopediaTransactionPayload)
        .catch(error => error);

      expect(error).toBeDefined();
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.actor).toBe(TransferFailureReasonActor.TOKOPEDIA);
      expect(error.detail).toBe(
        'Error from TOKOPEDIA while transferring funds!'
      );
      expect(error.errorCode).toBe(ERROR_CODE.ERROR_FROM_TOKOPEDIA);
      expect(httpClient.post).toBeCalledWith(
        transactionUrl,
        {
          data: {
            ...tokopediaTransactionPayload
          }
        },
        {
          headers: {
            signature: signature,
            Authorization: `Bearer ${accessToken}`
          }
        }
      );
    });

    it(`should throw error with code ${ERROR_CODE.ERROR_FROM_TOKOPEDIA} when result is null`, async () => {
      const signature = 'OpH++zdKpBwQFx6medZnLWY+==';
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);
      (httpClient.post as jest.Mock).mockRejectedValueOnce({
        data: null
      });

      const error = await tokopediaRepository
        .submitTransaction(tokopediaTransactionPayload)
        .catch(error => error);

      expect(error).toBeDefined();
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.actor).toBe(TransferFailureReasonActor.MS_TRANSFER);
      expect(error.detail).toBe('Tokopedia Get Access Token error!');
      expect(error.errorCode).toBe(
        ERROR_CODE.TOKOPEDIA_AUTH_TOKEN_ACCESS_ERROR
      );
    });

    it(`should throw error with code ${ERROR_CODE.ERROR_FROM_TOKOPEDIA} when submit error`, async () => {
      (redis.get as jest.Mock).mockResolvedValueOnce({
        /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
        access_token: '2krP4WcLxse1lCpAuYTwFeee',
        expires_in: 1234567,
        token_type: 'order'
      });
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: null
      });

      const error = await tokopediaRepository
        .submitTransaction(tokopediaTransactionPayload)
        .catch(error => error);

      expect(error).toBeDefined();
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.actor).toBe(TransferFailureReasonActor.TOKOPEDIA);
      expect(error.detail).toBe(
        'Error from TOKOPEDIA while transferring funds!'
      );
      expect(error.errorCode).toBe(ERROR_CODE.ERROR_FROM_TOKOPEDIA);
    });

    it(`should throw error with code ${ERROR_CODE.ERROR_FROM_TOKOPEDIA} when token is invalid`, async () => {
      const signature = 'OpH++zdKpBwQFx6medZnLWY+==';
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          error: 'access_denied',
          error_description: 'ErrorFinishAuthorization',
          event_code: '',
          last_login_type: '8'
        }
      });
      const tokenDetails = {
        access_token: null,
        expires_in: 100,
        token_type: 'order'
      };
      (tokenHTTPClient.post as jest.Mock).mockResolvedValueOnce({
        data: tokenDetails
      });
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);
      (redis.get as jest.Mock).mockResolvedValueOnce(null);

      const error = await tokopediaRepository
        .submitTransaction(tokopediaTransactionPayload)
        .catch(error => error);

      expect(generateSignature).not.toHaveBeenCalled();

      expect(error).toBeDefined();
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.actor).toBe(TransferFailureReasonActor.TOKOPEDIA);
      expect(error.detail).toBe('Tokopedia Get Access Token error!');
      expect(error.errorCode).toBe(
        ERROR_CODE.TOKOPEDIA_AUTH_TOKEN_ACCESS_ERROR
      );
    });
  });
});
