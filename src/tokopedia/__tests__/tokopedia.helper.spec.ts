import { TransferAppError } from '../../errors/AppError';
import { mapToTokopediaTransaction } from '../tokopedia.helper';
import {
  inValidTransactionModel,
  validTransactionModel
} from '../__mocks__/tokopedia.data';
import { ERROR_CODE } from '../../common/errors';
import { sumBy } from 'lodash';

describe('tokopedia.helper', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });
  it('Should return valid payload', async () => {
    const transaction = validTransactionModel;
    const fees =
      transaction.fees && transaction.fees.length
        ? sumBy(transaction.fees, 'feeAmount')
        : 0;
    const payload = await mapToTokopediaTransaction(transaction);

    expect(payload).not.toBeNull();
    expect(payload.id).toBe(transaction.externalId);
    expect(payload.attributes.product_code).toBe(
      transaction.additionalInformation1
    );
    expect(payload.attributes.client_number).toBe(
      transaction.beneficiaryAccountNo
    );
    expect(payload.attributes.amount).toBe(
      transaction.transactionAmount + fees
    );
  });

  it('Should throw error if required field is missing', async () => {
    const error = await mapToTokopediaTransaction(
      inValidTransactionModel
    ).catch(err => err);

    expect(error).toBeInstanceOf(TransferAppError);
    expect(error.errorCode).toBe(ERROR_CODE.INVALID_MANDATORY_FIELDS);
  });
});
