import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import tokopediaController from '../tokopedia.controller';
import ResponseWrapper from '../../plugins/responseWrapper';
import errorHandler from '../../common/handleValidationErrors';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import tokopediaService from '../tokopedia.service';
import {
  getResponseFields,
  getTokopediaTransactionData
} from '../__mocks__/tokopedia.data';
import { verifySignature } from '../tokopedia.encryption';
import { ITokopediaPayoutStatusEnum } from '../tokopedia.enum';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

jest.mock('../tokopedia.service');
jest.mock('../tokopedia.encryption');

describe('tokopediaController', () => {
  let server: hapi.Server;
  beforeAll(() => {
    server = new hapi.Server({
      routes: {
        validate: {
          options: {
            abortEarly: false
          },
          failAction: errorHandler
        }
      }
    });
    server.register([ResponseWrapper]);
    server.route(tokopediaController);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('confirm transaction', () => {
    const confirmTransactionPayload = {
      ...getTokopediaTransactionData(),
      /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
      attributes: {
        admin_fee: 0,
        client_name: 'Tokopedia User',
        client_number: '2121212',
        product_code: 'pln-postpaid',
        error_code: undefined,
        error_detail: undefined,
        fulfilled_at: '2020-12-09T10:48:45.000Z',
        partner_fee: 500,
        sales_price: 12500,
        status: ITokopediaPayoutStatusEnum.SUCCESS,
        fields: null,
        fulfillment_result: getResponseFields(),
        serial_number: '5196 1584 0828 2085 4701',
        voucher_code: '5196 1584 0828 2085 4701'
      }
    };
    (verifySignature as jest.Mock).mockResolvedValueOnce(null);

    it('should response 204 with no payload', async () => {
      // arrange
      const options = {
        method: 'POST',
        url: '/tokopedia/transaction-confirmation',
        payload: { data: { ...confirmTransactionPayload } }
      };
      (verifySignature as jest.Mock).mockResolvedValueOnce(null);
      (tokopediaService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        {}
      );
      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(Http.StatusCode.NO_CONTENT);
      expect(tokopediaService.confirmTransaction).toBeCalledWith(
        confirmTransactionPayload
      );
    });

    it('should return bad request error (400) when service throw AppError', async () => {
      // arrange
      const options = {
        method: 'POST',
        url: '/tokopedia/transaction-confirmation',
        payload: { data: { ...confirmTransactionPayload } }
      };

      (verifySignature as jest.Mock).mockResolvedValueOnce(null);
      (tokopediaService.confirmTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.INVALID_AMOUNT
        )
      );

      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
      expect(response.result.error.code).toEqual(ERROR_CODE.INVALID_AMOUNT);
    });

    it('should return server error (500) when service throw server error', async () => {
      // arrange
      const options = {
        method: 'POST',
        url: '/tokopedia/transaction-confirmation',
        payload: { data: { ...confirmTransactionPayload } }
      };

      (verifySignature as jest.Mock).mockResolvedValueOnce(null);
      (tokopediaService.confirmTransaction as jest.Mock).mockRejectedValueOnce(
        new Error()
      );
      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(
        Http.StatusCode.INTERNAL_SERVER_ERROR
      );
    });
  });
});
