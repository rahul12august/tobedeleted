import httpClient, { tokenHTTPClient } from './httpClient';
import {
  ITokopediaTokenResult,
  ITokopediaTransactionRequestPayload,
  ITokopediaTransactionResponse
} from './tokopedia.type';
import logger, { wrapLogs } from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { config } from '../config';
import { HttpHeaders } from '../common/constant';
import { REDIS_TOKEN_KEY, TokenConstants } from './tokopedia.constant';
import { isEmpty } from 'lodash';
import redis from '../common/redis';
import { generateSignature } from './tokopedia.encryption';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const { transactionUrl } = config.get('tokopedia');
const configurationPrefixKey = 'ms-bill-payment';

const fetchAccessToken = async (): Promise<ITokopediaTokenResult> => {
  const { data: tokenResponse } = await tokenHTTPClient.post(
    `${TokenConstants.TOKEN_ENDPOINT}`
  );
  return tokenResponse;
};

const getToken = async (): Promise<string> => {
  const errorDetail = 'Tokopedia Get Access Token error';
  try {
    let result: ITokopediaTokenResult = await redis.get(
      REDIS_TOKEN_KEY,
      configurationPrefixKey
    );

    if (isEmpty(result)) {
      result = await fetchAccessToken();
      if (!result.access_token || isEmpty(result.access_token)) {
        logger.error(`${errorDetail}: ${JSON.stringify(result)}`);
        throw new TransferAppError(
          `${errorDetail}!`,
          TransferFailureReasonActor.TOKOPEDIA,
          ERROR_CODE.TOKOPEDIA_AUTH_TOKEN_ACCESS_ERROR
        );
      }
      let tokenExpiry: number =
        result.expires_in / 1000 - TokenConstants.TOKEN_EXPIRY_BUFFER;
      if (Number.isNaN(tokenExpiry) || tokenExpiry < 0) {
        tokenExpiry = TokenConstants.DEFAULT_TTL;
      }
      await redis.mSet({
        prefix: configurationPrefixKey,
        keyValueObjectList: [
          {
            key: REDIS_TOKEN_KEY,
            value: result,
            ttl: tokenExpiry,
            shouldStringifyValue: true
          }
        ]
      });
    }
    return result.access_token;
  } catch (error) {
    logger.error(`${errorDetail}: ${error && error.message}`);
    if (error instanceof TransferAppError) {
      throw error;
    } else {
      throw new TransferAppError(
        `${errorDetail}!`,
        TransferFailureReasonActor.MS_TRANSFER,
        ERROR_CODE.TOKOPEDIA_AUTH_TOKEN_ACCESS_ERROR
      );
    }
  }
};

const isValidResponse = (result: any): boolean =>
  !result.data ||
  !result.data.data ||
  (result.data.data.attributes &&
    result.data.data.attributes.error_code &&
    result.data.data.attributes.error_detail);

const submitTransaction = async (
  payload: ITokopediaTransactionRequestPayload
): Promise<ITokopediaTransactionResponse> => {
  const errorDetail = 'Error from TOKOPEDIA while transferring funds!';
  try {
    logger.info(
      `Submitting Tokopedia transaction Id : ${payload.id}, 
        beneficiary_account: ${payload.attributes.client_number},
        skuCode: ${payload.attributes.product_code}`
    );
    const accessToken = await getToken();
    const updatedPayload = {
      data: {
        ...payload
      }
    };
    const signature = await generateSignature(JSON.stringify(updatedPayload));
    const requestConfig = {
      headers: {
        Authorization: `Bearer ${accessToken}`,
        [HttpHeaders.SIGNATURE]: signature
      }
    };
    const result = await httpClient.post(
      transactionUrl,
      updatedPayload,
      requestConfig
    );
    if (isValidResponse(result)) {
      logger.error(
        `Tokopedia submitTransaction: ${errorDetail}
          Error Code : ${result?.data?.data?.attributes?.error_code},
          Error Details : ${result?.data?.data?.attributes?.error_detail},`
      );
      throw new TransferAppError(
        errorDetail,
        TransferFailureReasonActor.TOKOPEDIA,
        ERROR_CODE.ERROR_FROM_TOKOPEDIA
      );
    }
    logger.info(
      `Submit Tokopedia transaction response 
        id : ${result.data.data.id},
        beneficiaryAccount: ${result.data.data.attributes.client_number}`
    );
    return result.data.data;
  } catch (error) {
    logger.error(
      `submitTransaction catch: ${errorDetail}`,
      error && error.message
    );
    if (error instanceof TransferAppError) {
      throw error;
    } else {
      throw new TransferAppError(
        errorDetail,
        TransferFailureReasonActor.MS_TRANSFER,
        ERROR_CODE.ERROR_FROM_TOKOPEDIA
      );
    }
  }
};

const tokopediaRepository = wrapLogs({
  submitTransaction,
  getToken
});

export default tokopediaRepository;
