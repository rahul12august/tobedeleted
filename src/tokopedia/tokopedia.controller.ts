import _ from 'lodash';
import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import { IConfirmTransactionRequest } from './tokopedia.type';
//import { confirmTransactionRequestValidator } from './tokopedia.validator';
import { updateRequestIdInContext } from '../common/contextHandler';
import tokopediaService from './tokopedia.service';
import { verifySignature } from './tokopedia.encryption';
import logger from '../logger';

const confirmTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/tokopedia/transaction-confirmation',
  options: {
    description: 'Confirm Tokopedia transaction',
    notes:
      'Private Api endpoint is called from Tokopedia to confirm transaction',
    tags: ['api', 'transactions'],
    auth: false,
    // validate: {
    //   payload: confirmTransactionRequestValidator
    // },
    payload: {
      parse: false,
      output: 'data'
    },
    handler: async (
      hapiRequest: IConfirmTransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { payload } = hapiRequest;
      const jsonPayload = JSON.parse(payload.toString());
      await updateRequestIdInContext(jsonPayload.data.id);
      logger.debug(`Payload: ${payload.toString()}`);
      logger.debug(`Signature: ${hapiRequest.headers['signature']}`);
      await verifySignature(
        payload.toString(),
        hapiRequest.headers['signature']
      );
      await tokopediaService.confirmTransaction(jsonPayload.data);
      return hapiResponse.response().code(Http.StatusCode.NO_CONTENT);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Ok'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          },
          [Http.StatusCode.UNAUTHORIZED]: {
            description: 'UNAUTHORIZED'
          }
        }
      }
    }
  }
};

const tokopediaController: hapi.ServerRoute[] = [confirmTransaction];

export default tokopediaController;
