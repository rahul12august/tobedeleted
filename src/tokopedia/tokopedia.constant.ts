export const BASIC_PREFIX = 'Basic';

export default {
  MAX_LENGTH_TRANSACTION_RESPONSE_ID: 12,
  MAX_LENGTH_IRIS_REFERENCE_NO: 40,
  MAX_LENGTH_IRIS_ERROR_MESSAGE: 100,
  MAX_LENGTH_IRIS_ERROR_CODE: 3
};

export const ORDER_TYPE = 'order';

export const TokenConstants = {
  TOKEN_ENDPOINT: '/token?grant_type=client_credentials',
  TOKEN_EXPIRY_BUFFER: 600,
  DEFAULT_TTL: 3000
};

export const REDIS_TOKEN_KEY = 'TOKOPEDIA_TOKEN';
