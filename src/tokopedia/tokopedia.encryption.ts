import crypto from 'crypto';
import logger from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import NodeRSA from 'node-rsa';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const privateKey =
  (process.env.TOKOPEDIA_PRIVATE_KEY &&
    decodeURIComponent(process.env.TOKOPEDIA_PRIVATE_KEY)) ||
  '';
const getPublicKey = (): string =>
  (process.env.TOKOPEDIA_PUBLIC_KEY &&
    decodeURIComponent(process.env.TOKOPEDIA_PUBLIC_KEY)) ||
  '';
export const generateSignature = async (
  stringifiedPayload: string
): Promise<string> => {
  logger.info(`Generating Signature for tokopedia`);
  logger.debug(`Private key value : ${privateKey}`);
  const signature = await crypto.sign(
    'sha256',
    Buffer.from(stringifiedPayload),
    {
      key: privateKey,
      padding: crypto.constants.RSA_PKCS1_PSS_PADDING
    }
  );
  const encodedSignature = signature.toString('base64');
  logger.debug(`Signature value : ${encodedSignature}`);
  return encodedSignature;
};

export const verifySignature = async (
  payload: any,
  signature: string
): Promise<void> => {
  logger.info(`Verifying Signature for tokopedia`);
  const bufferSignature = Buffer.from(signature, 'base64');
  const publicKey = getPublicKey();
  const key = new NodeRSA(publicKey);
  logger.debug(`key size: ${key.getKeySize()}`);
  const stringifiedPayload = payload;
  const saltLength =
    key.getKeySize() / 8 -
    2 -
    crypto
      .createHash('md5')
      .update(stringifiedPayload)
      .digest('hex').length;
  logger.debug(`saltLength: ${saltLength}`);
  const isVerified = await crypto.verify(
    'sha256',
    Buffer.from(stringifiedPayload),
    {
      key: publicKey,
      padding: crypto.constants.RSA_PKCS1_PSS_PADDING,
      saltLength: saltLength
    },
    bufferSignature
  );
  if (!isVerified) {
    const detail = `Invalid Signature or payload details.`;
    logger.error(detail);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.TOKOPEDIA,
      ERROR_CODE.INVALID_SIGNATURE
    );
  }
};
