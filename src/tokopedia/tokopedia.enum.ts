export enum ITokopediaPayoutStatusEnum {
  PENDING = 'Pending',
  SUCCESS = 'Success',
  FAILED = 'Failed'
}
