import Joi from '@hapi/joi';
import { JoiValidator } from '@dk/module-common';
import { ITokopediaPayoutStatusEnum } from './tokopedia.enum';

/*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
const attributeValidator = Joi.object({
  client_number: Joi.string().description('Number inputted by end user'),
  admin_fee: Joi.number().description('Admin fee'),
  client_name: Joi.string()
    .allow(['', null])
    .description('Client name'),
  error_code: JoiValidator.optionalString().description(
    'Code that represents the error state'
  ),
  error_detail: JoiValidator.optionalString().description(
    'Error message from Tokopedia'
  ),
  fields: Joi.array()
    .items(
      Joi.object({
        name: Joi.string().optional(),
        value: Joi.string().optional()
      })
    )
    .optional()
    .allow(null)
    .description('Extra deatils require for bill payment transaction'),
  fulfilled_at: Joi.string()
    .allow(null)
    .description('Bill payment transaction date'),
  partner_fee: Joi.number().description('Fee amount charge by partner'),
  product_code: JoiValidator.requiredString().description('Product code'),
  sales_price: Joi.number().description('Total price of product'),
  status: JoiValidator.requiredString().valid(
    Object.values(ITokopediaPayoutStatusEnum)
  ).description(`Tokopedia status
    * PENDING
    * SUCCESS
    * FAILED`),
  serial_number: Joi.string()
    .allow(['', null])
    .description('Serial number'),
  voucher_code: Joi.string()
    .optional()
    .allow(['', null])
    .description('Voucher code'),
  fulfillment_result: Joi.array()
    .items(
      Joi.object({
        name: Joi.string().optional(),
        value: Joi.string().optional()
      })
    )
    .optional()
    .allow(null)
    .description(
      'Bill success information that can be used to generate the user receipt'
    )
}).required();

export const confirmTransactionFieldsValidator = Joi.object({
  type: Joi.string().required().description(`Tokopedia api request type
  * inquiry - product bill details inquiry
  * order - bill payment request`),
  id: Joi.string()
    .required()
    .description('Identifier for order request, generated by user'),
  attributes: attributeValidator.description(
    'Fields contain extra parameters for specific products. Fields are part of client_number parameters for some products that need more than 1 client number parameter to pay the bill.'
  )
}).required();

export const confirmTransactionRequestValidator = Joi.object({
  data: confirmTransactionFieldsValidator
}).label('confirmTransactionRequestValidator');
