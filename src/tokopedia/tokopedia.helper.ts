import { ITransactionModel } from '../transaction/transaction.model';
import { ITokopediaTransactionRequestPayload } from './tokopedia.type';
import logger, { wrapLogs } from '../logger';
import { ORDER_TYPE } from './tokopedia.constant';
import { TransferAppError } from '../errors/AppError';
import { isEmpty, sumBy } from 'lodash';
import { ERROR_CODE } from '../common/errors';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const validateMandatoryFields = (transaction: ITransactionModel): void => {
  let isValid = true;
  if (isEmpty(transaction.additionalInformation1)) {
    logger.error(
      'validateMandatoryFields: additionalInformation1 should be mandatory for Tokopedia transaction.'
    );
    isValid = false;
  }
  if (isEmpty(transaction.beneficiaryAccountNo)) {
    logger.error(
      'validateMandatoryFields: beneficiaryAccountNo should be mandatory for Tokopedia transaction.'
    );
    isValid = false;
  }
  if (!isValid) {
    throw new TransferAppError(
      'Validation of mandatory fields failed!',
      TransferFailureReasonActor.TOKOPEDIA,
      ERROR_CODE.INVALID_MANDATORY_FIELDS
    );
  }
  return;
};

export const mapToTokopediaTransaction = async (
  transaction: ITransactionModel
): Promise<ITokopediaTransactionRequestPayload> => {
  validateMandatoryFields(transaction);
  const fees =
    transaction.fees && transaction.fees.length
      ? sumBy(transaction.fees, 'feeAmount')
      : 0;
  return {
    type: ORDER_TYPE,
    id: transaction.externalId,
    attributes: {
      /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
      product_code: transaction.additionalInformation1 || '',
      client_number: transaction.beneficiaryAccountNo || '',
      amount: transaction.transactionAmount + fees
    }
  };
};

export default wrapLogs(mapToTokopediaTransaction);
