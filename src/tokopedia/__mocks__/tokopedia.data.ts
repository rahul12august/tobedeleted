import faker from 'faker';
import {
  ITokopediaRequestAttributes,
  ITokopediaRequestFields,
  ITokopediaTransactionRequestPayload,
  ITokopediaTransactionResponse,
  ITokopediaResponseAttributes
} from '../tokopedia.type';
import { ITokopediaPayoutStatusEnum } from '../tokopedia.enum';
import {
  getTransactionDetail,
  getTransactionModelFullData
} from '../../transaction/__mocks__/transaction.data';
import { ITransactionModel } from '../../transaction/transaction.model';
import { BankChannelEnum } from '@dk/module-common';
import { ORDER_TYPE } from '../tokopedia.constant';

const newTransactionModel = getTransactionModelFullData();

export const getResponseFields = (): [ITokopediaRequestFields] => [
  {
    name: 'Optional',
    value: 'Optional'
  }
];

export const validTransactionModel: ITransactionModel = {
  id: faker.random.uuid(),
  ...newTransactionModel
};

export const inValidTransactionModel: ITransactionModel = {
  ...newTransactionModel,
  id: faker.random.uuid(),
  additionalInformation1: undefined,
  beneficiaryAccountNo: undefined
};

const getResponseAttributes = (): ITokopediaResponseAttributes => ({
  /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
  client_number: '2121212',
  product_code: 'pln-postpaid',
  error_code: undefined,
  error_detail: undefined,
  fulfilled_at: '2020-12-09T10:48:45.000Z',
  partner_fee: 500,
  sales_price: 12500,
  status: ITokopediaPayoutStatusEnum.PENDING,
  fields: getResponseFields()
});

export const errorResponse = (): ITokopediaTransactionResponse => ({
  type: ORDER_TYPE,
  id: faker.random.uuid(),
  attributes: {
    ...getResponseAttributes(),
    /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
    error_code: 'mock_error_code',
    error_detail: 'mock_error_detail'
  }
});

export const getErrorResponseAttributes = (): ITokopediaResponseAttributes => ({
  /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
  client_number: '2121212',
  product_code: 'pln-postpaid',
  error_code: 'S02',
  error_detail: 'Product is not available',
  fulfilled_at: null,
  partner_fee: 500,
  sales_price: 12500,
  status: ITokopediaPayoutStatusEnum.PENDING,
  fields: getResponseFields()
});

export const getTokopediaTransactionData = (): ITokopediaTransactionResponse => ({
  type: ORDER_TYPE,
  id: faker.random.uuid(),
  attributes: getResponseAttributes()
});

export const getTokopediaErrorTransactionData = (): ITokopediaTransactionResponse => ({
  type: ORDER_TYPE,
  id: faker.random.uuid(),
  attributes: getErrorResponseAttributes()
});

const getRequestAttributes = (): ITokopediaRequestAttributes => ({
  /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
  client_number: '2121212',
  product_code: 'pln-postpaid',
  amount: 12500
});

export const getTokopediaTransactionPayload = (): ITokopediaTransactionRequestPayload => ({
  id: faker.random.uuid(),
  type: ORDER_TYPE,
  attributes: getRequestAttributes()
});

export const transactionModel: ITransactionModel = {
  ...getTransactionDetail(),
  beneficiaryBankCodeChannel: BankChannelEnum.TOKOPEDIA,
  beneficiaryAccountNo: '2121212',
  beneficiaryAccountName: 'beneficiary_name',
  beneficiaryBankCode: 'beneficiary_bank',
  beneficiaryIrisCode: 'beneficiary_bank',
  note: 'notes',
  transactionAmount: 12500,
  additionalInformation1: 'pln-postpaid'
};
