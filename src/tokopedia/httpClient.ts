import { config } from '../config';
import { HttpHeaders } from '../common/constant';
import { BASIC_PREFIX } from './tokopedia.constant';
import { context } from '../context';
import logger from '../logger';
import { createHttpClient, HttpClient } from '@dk/module-httpclient';
import { Http } from '@dk/module-common';
import { retryConfig } from '../common/constant';

const { baseUrl, timeout } = config.get('tokopedia');
const tokenBaseURL = config.get('tokopedia').tokenBaseUrl;
const apiKey = process.env.TOKOPEDIA_API_KEY || '';

const httpClient: HttpClient = Http.createHttpClientForward({
  baseURL: baseUrl,
  headers: {
    [HttpHeaders.CONTENT_TYPE]: 'application/json',
    [HttpHeaders.ACCEPT]: 'application/json',
    [HttpHeaders.AUTH]: `${BASIC_PREFIX} ${Buffer.from(apiKey).toString(
      'base64'
    )}:`
  },
  timeout: timeout,
  context,
  logger,
  retryConfig: {
    ...retryConfig,
    networkErrorCodesToRetry: ['ENOENT', 'ECONNREFUSED']
  }
});

const basicToken = process.env.TOKOPEDIA_BASIC_AUTH_TOKEN;
export const tokenHTTPClient: HttpClient = createHttpClient({
  baseURL: tokenBaseURL,
  headers: {
    Authorization: `Basic ${basicToken}`
  },
  timeout: timeout,
  context,
  logger
});

export default httpClient;
