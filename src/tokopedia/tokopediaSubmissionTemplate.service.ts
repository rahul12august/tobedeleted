/**
 * @module tokopediaSubmissionTemplate handle outgoing transaction to switching
 */

import { BankChannelEnum } from '@dk/module-common';
import { ITransactionModel } from '../transaction/transaction.model';
import logger, { wrapLogs } from '../logger';
import { ITransactionSubmissionTemplate } from '../transaction/transaction.type';
import tokopediaRepository from './tokopedia.repository';
import { mapToTokopediaTransaction } from './tokopedia.helper';
import transactionUtility from '../transaction/transaction.util';
import transactionRepository from '../transaction/transaction.repository';
import { TransactionStatus } from '../transaction/transaction.enum';
import { Rail } from '@dk/module-common';
import { updateRequestIdInRedis } from '../common/contextHandler';
import { httpClientTimeoutErrorCode } from '../common/constant';

const submitTransaction = async (
  transactionModel: ITransactionModel
): Promise<ITransactionModel> => {
  let status: TransactionStatus = TransactionStatus.SUBMITTED;
  try {
    const tokopediaTransactionPayload = await mapToTokopediaTransaction(
      transactionModel
    );
    const tokopediaTransaction = await transactionUtility.submitTransactionWithRetry(
      tokopediaRepository.submitTransaction,
      tokopediaTransactionPayload
    );
    if (tokopediaTransaction.id)
      updateRequestIdInRedis(tokopediaTransaction.id);

    return transactionModel;
  } catch (error) {
    logger.error(
      'failed submission to tokopedia after all retry or non-retriable condition',
      error
    );
    if (error.code && error.code === httpClientTimeoutErrorCode) {
      logger.info(
        `Sending pending RTGS blocked amount notification for : ${transactionModel.id}`
      );
      status = TransactionStatus.SUBMITTED;
      return transactionModel;
    }
    status = TransactionStatus.DECLINED;
    throw error;
  } finally {
    await transactionRepository.update(transactionModel.id, {
      status: status,
      thirdPartyOutgoingId: transactionModel.externalId
    });
  }
};

const isEligible = (model: ITransactionModel): boolean =>
  BankChannelEnum.TOKOPEDIA === model.beneficiaryBankCodeChannel;

const shouldSendNotification = () => false;

const getRail = () => Rail.TOKOPEDIA;

const tokopediaSubmissionTemplate: ITransactionSubmissionTemplate = {
  submitTransaction,
  isEligible,
  getRail,
  shouldSendNotification
};

export default wrapLogs(tokopediaSubmissionTemplate);
