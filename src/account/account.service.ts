import accountRepository from './account.repository';
import { AMOUNT_LIMIT_REJECT } from './account.constant';
import { wrapLogs } from '../logger';
import { IAccount } from './account.type';

const authenticateTransactionAmount = async (
  sourceAccountNo: string,
  amount: number
): Promise<boolean> => {
  const amountLimit = await accountRepository.checkAmountLimit(
    sourceAccountNo,
    amount
  );
  if (amountLimit && amountLimit.status === AMOUNT_LIMIT_REJECT) return true;
  return false;
};

const getAccountInfo = async (sourceAccountNo: string): Promise<IAccount> => {
  const accountInfo = await accountRepository.getAccountInfo(sourceAccountNo);
  return accountInfo;
};

export default wrapLogs({
  authenticateTransactionAmount,
  getAccountInfo
});
