import { config } from '../config';
import { IAccount } from './account.type';
import accountCacheUtil from './account.cache.util';
import accountRepository from './account.repository';

import redis from '../common/redis';
import redisRepository from '../common/redis.repository';
import { wrapLogs } from '../logger';

const basePrefixKey = config.get('servicePrefix').account;
const inquiryAccountPrefixKey = `${basePrefixKey}-inquiry-account`;

const getCachedInquiryAccountInfo = async (
  bankCode: string,
  accountNumber: string,
  transactionAmount?: number,
  sourceAccountNo?: string,
  cif?: string
): Promise<IAccount | {}> => {
  const cacheArgs = {
    bankCode,
    accountNumber,
    transactionAmount,
    sourceAccountNo,
    cif
  };

  const key = redis.addPrefix(
    accountCacheUtil.constructCacheKeyFromObject(cacheArgs),
    inquiryAccountPrefixKey
  );

  return redisRepository.getCacheFromIntermediateKey(
    key,
    accountRepository.inquiryAccountInfo,
    ...Object.values({ ...cacheArgs })
  );
};

const accountCache = {
  getCachedInquiryAccountInfo
};

export default wrapLogs(accountCache);
