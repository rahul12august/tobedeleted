import Enum from '@dk/module-common';

export interface IBasicAccountInfo {
  readonly accountNumber?: string;
  readonly accountType?: string;
  readonly currency?: string;
  readonly aliasName?: string;
  readonly accountName?: string;
}

export interface IAccount extends IBasicAccountInfo {
  readonly cif?: string;
  readonly customerId?: string;
  readonly role?: string;
  readonly moverDailyUsage?: number;
  readonly moverLimit?: number;
  readonly moverRemainingDailyUsage?: number;
  readonly cardId?: string;
  readonly status?: string;
}

export interface IAccountInfo extends IBasicAccountInfo {
  accountType: string;
  currency: string;
  cardId?: string;
  id?: string;
}
export interface IBlockingRequestPayload {
  amount: number;
  name: string;
  blockingPurposeCode: Enum.BlockingAmountType;
  mambuBlockingCode: string;
}

export interface IBlockingAmountResponse {
  id: string;
  status: string;
  cardId: string;
  idempotencyKey?: string;
}

export interface ITransactionAmountLimit {
  status: string;
}
