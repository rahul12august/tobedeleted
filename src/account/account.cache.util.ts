import { Dictionary } from 'lodash';

const constructCacheKeyFromObject = <T>(object: Dictionary<T>): string =>
  Object.entries(object)
    .filter(([, val]) => val)
    .map(([key, val]) => `${key}=${val}`)
    .join('&');

export default { constructCacheKeyFromObject };
