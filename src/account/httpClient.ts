import { config } from '../config';
import { context } from '../context';
import logger from '../logger';
import { HttpClient } from '@dk/module-httpclient';
import { Http } from '@dk/module-common';
import { retryConfig } from '../common/constant';

const MS_ACCOUNT_URL = config.get('ms').account;
const MS_ACCOUNT_DEFAULT_TIMEOUT = 45_000;
const accountTimeoutConfig: number =
  config.get('ms-account')?.timeout ?? MS_ACCOUNT_DEFAULT_TIMEOUT;

const httpClient: HttpClient = Http.createHttpClientForward({
  baseURL: MS_ACCOUNT_URL,
  context,
  logger,
  timeout: accountTimeoutConfig,
  retryConfig
});

export default httpClient;
