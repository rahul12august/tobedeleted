import {
  IAccount,
  IAccountInfo,
  IBasicAccountInfo,
  IBlockingAmountResponse,
  IBlockingRequestPayload,
  ITransactionAmountLimit
} from './account.type';
import { get } from 'lodash';
import httpClient from './httpClient';
import logger, { wrapLogs } from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { config } from '../config';
import { mapTransferAppError } from '../common/common.util';
import { Http } from '@dk/module-common';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const inquiryAccountInfo = async (
  bankCode: string,
  accountNumber: string,
  transactionAmount?: number,
  sourceAccountNo?: string,
  cif?: string
): Promise<IAccount | {}> => {
  const queryCif = cif ? `&cif=${cif}` : '';
  let endpoint = `/accounts/inquiry?bankCode=${bankCode}&inputAccount=${accountNumber}${queryCif}`;
  if (transactionAmount) {
    endpoint += `&transactionAmount=${transactionAmount}`;
  }
  if (sourceAccountNo) {
    endpoint += `&sourceAccount=${sourceAccountNo}`;
  }

  const response = await httpClient.get(endpoint);

  const info = get(response, ['data', 'data'], {});
  if (!info.accountNumber) {
    const detail = `Invalid account for account number: ${accountNumber}!`;
    logger.error(`inquiryAccountInfo: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_ACCOUNT
    );
  }
  return info;
};

const queryMainAccount = async (cif: string): Promise<IBasicAccountInfo> => {
  const response = await httpClient.get(
    `/customers/${cif}/accounts/main-account`,
    {
      timeout: config.get('coreBankingApiCall').account
    }
  );
  return get(response, ['data', 'data'], []);
};

const updateBlockingAmount = async (
  cif: string,
  accountNumber: string,
  blockId: string,
  amount: number
): Promise<void> => {
  await httpClient.put(
    `/private/accounts/${accountNumber}/blocking-amounts/${blockId}?cif=${cif}`,
    {
      amount
    },
    {
      timeout: config.get('coreBankingApiCall').account
    }
  );
};

const createBlockingAmount = async (
  cif: string,
  accountNumber: string,
  payload: IBlockingRequestPayload
): Promise<IBlockingAmountResponse> => {
  try {
    logger.info(
      `create blocking amount transaction for account: ${accountNumber} and mambuBlockingCode: ${payload.mambuBlockingCode}`
    );
    const result = await httpClient.post(
      `/accounts/${accountNumber}/blocking-amounts?cif=${cif}`,
      payload,
      {
        timeout: config.get('coreBankingApiCall').account
      }
    );
    const response = result.data.data;
    logger.info(
      `createBlockingAmount: response id: ${response.id}, status: ${response.status} and cardId: ${response.cardId}`
    );
    return response;
  } catch (err) {
    logger.error(
      `createBlockingAmount: Failed while blocking Amount code : ${err.code} message : ${err.message}`
    );
    const appError = err && err.error && err.error.error;
    return mapTransferAppError(
      appError || err,
      ERROR_CODE.COREBANKING_BLOCK_AMOUNT_FAILED,
      err.status >= 500 ? true : false
    );
  }
};

const checkAmountLimit = async (
  sourceAccountNo: string,
  amount: number
): Promise<ITransactionAmountLimit | undefined> => {
  logger.info(`checkAmountLimit: For sourceAccountNo ${sourceAccountNo}`);
  const result = await httpClient.post(
    `/private/accounts/auth-transaction-amount`,
    {
      authenticationAmount: amount,
      accountNumber: sourceAccountNo
    },
    {
      timeout: config.get('coreBankingApiCall').account
    }
  );
  const response = (result.data && result.data.data) || undefined;
  logger.info(
    `checkAmountLimit: Response for sourceAccountNo ${sourceAccountNo} is ${response}`
  );
  return response;
};

const getAccountInfo = async (
  accountNumber: string,
  cif?: string
): Promise<IAccount> => {
  try {
    logger.info(`getAccountInfo for account: ${accountNumber} and cif: ${cif}`);
    let accountInfoUrl = `/private/accounts/inquiry?accountNumber=${accountNumber}&includeMigratingAccounts=true`;
    if (cif) accountInfoUrl += `&cif=${cif}`;

    const result = await httpClient.get(accountInfoUrl);
    logger.info(
      `getAccountInfo: getting response for account: ${accountNumber} and cif: ${cif}`
    );
    return result.data.data;
  } catch (error) {
    logger.error(`getAccountInfo: returns an error: ${error}`);
    throw error && error.status === Http.StatusCode.BAD_REQUEST
      ? new TransferAppError(
          `Error while getting account info ${error}!`,
          TransferFailureReasonActor.MS_TRANSFER,
          ERROR_CODE.INVALID_REQUEST
        )
      : error;
  }
};

const getActivatedAccount = async (
  accountNumber: string
): Promise<IAccountInfo> => {
  logger.info(`getActivatedAccount for account: ${accountNumber}`);
  const result = await httpClient.get(
    `/private/accounts/${accountNumber}?fetchBalance=false`
  );
  logger.info(
    `getActivatedAccount: getting response for account: ${accountNumber}`
  );
  return result.data.data;
};

export default wrapLogs({
  inquiryAccountInfo,
  createBlockingAmount,
  updateBlockingAmount,
  queryMainAccount,
  checkAmountLimit,
  getAccountInfo,
  getActivatedAccount
});
