import _ from 'lodash';
import redis from '../../common/redis';
import redisRepository from '../../common/redis.repository';
import accountCache from '../account.cache';
import accountRepository from '../account.repository';

jest.mock('../../common/redis.repository');
jest.mock('../../common/redis', () => ({
  addPrefix: jest.fn((key, prefix) => `{${prefix}}:${key}`)
}));

describe('getCachedInquiryAccountInfo', () => {
  const bankCode = 'ABC01';
  const accountNumber = '30012009230335';
  const transactionAmount = 99000;
  const sourceAccountNo = '30012009230337';
  const cif = '8a85c1b37339967901733e36139a0ff8';

  const object = {
    bankCode: 'ABC01'
  };

  it('should call redis repository with correct arguments', async () => {
    // Given
    const key =
      'bankCode=ABC01&accountNumber=30012009230335&transactionAmount=99000&sourceAccountNo=30012009230337&cif=8a85c1b37339967901733e36139a0ff8';
    const prefixedKey = `{ms-account-inquiry-account}:${key}`;
    redisRepository.getCacheFromIntermediateKey = jest
      .fn()
      .mockResolvedValue(object);

    // When
    const result = await accountCache.getCachedInquiryAccountInfo(
      bankCode,
      accountNumber,
      transactionAmount,
      sourceAccountNo,
      cif
    );

    // Then
    expect(redis.addPrefix).toHaveBeenCalled();
    expect(redisRepository.getCacheFromIntermediateKey).toHaveBeenCalledWith(
      prefixedKey,
      accountRepository.inquiryAccountInfo,
      bankCode,
      accountNumber,
      transactionAmount,
      sourceAccountNo,
      cif
    );
    expect(result).toBe(object);
  });
});
