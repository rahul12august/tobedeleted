import accountService from '../account.service';
import accountRepository from '../account.repository';
import { getAccountInfo } from '../__mocks__/account.data';
import { IAccount } from '../account.type';

jest.mock('../httpClient');

describe('account.service', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('authenticateTransactionAmount', () => {
    const sourceAccountNo = '123456';
    const amount = 100000;
    it('should return true if services recommend transactionAuthenticationChecking is true and status amount limit is reject', async () => {
      accountRepository.checkAmountLimit = jest
        .fn()
        .mockResolvedValueOnce({ status: 'reject' });
      const expected = await accountService.authenticateTransactionAmount(
        sourceAccountNo,
        amount
      );
      expect(expected).toBeTruthy();
    });
    it('should return false if services recommend transactionAuthenticationChecking is true and status amount limit is success', async () => {
      accountRepository.checkAmountLimit = jest
        .fn()
        .mockResolvedValueOnce({ status: 'success' });
      const expected = await accountService.authenticateTransactionAmount(
        sourceAccountNo,
        amount
      );
      expect(expected).toBeFalsy();
    });
  });
  describe('getAccountInfo', () => {
    const sourceAccountNo = '123456';
    const accountInfo: IAccount = getAccountInfo();
    it('should return account info', async () => {
      accountRepository.getAccountInfo = jest
        .fn()
        .mockResolvedValueOnce(accountInfo);
      const expected = await accountService.getAccountInfo(sourceAccountNo);
      expect(expected).toEqual(accountInfo);
    });
  });
});
