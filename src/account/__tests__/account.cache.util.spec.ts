import _ from 'lodash';
import accountCacheUtil from '../account.cache.util';

describe('construct cache key', () => {
  it('should construct correct key prefix', () => {
    // Given
    const object = {
      bankCode: 'ABC01'
    };

    // When
    const cacheKey = accountCacheUtil.constructCacheKeyFromObject(object);

    // Then
    expect(cacheKey).toEqual('bankCode=ABC01');
  });

  it('should construct correct key from object', () => {
    // Given
    const object = {
      bankCode: 'ABC01',
      accountNumber: '30012009230335',
      transactionAmount: 99000,
      sourceAccountNo: '30012009230337',
      cif: '8a85c1b37339967901733e36139a0ff8'
    };

    // When
    const cacheKey = accountCacheUtil.constructCacheKeyFromObject(object);

    // Then
    expect(cacheKey).toEqual(
      'bankCode=ABC01&accountNumber=30012009230335&transactionAmount=99000&sourceAccountNo=30012009230337&cif=8a85c1b37339967901733e36139a0ff8'
    );
  });

  it('should construct correct key if the object is partially undefined', () => {
    // Given
    const object = {
      bankCode: 'ABC01',
      accountNumber: '30012009230335',
      transactionAmount: undefined,
      sourceAccountNo: undefined,
      cif: undefined
    };

    // When
    const cacheKey = accountCacheUtil.constructCacheKeyFromObject(object);

    // Then
    expect(cacheKey).toEqual('bankCode=ABC01&accountNumber=30012009230335');
  });
});
