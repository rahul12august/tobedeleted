import redis from '../../common/redis';
import redisRepository from '../../common/redis.repository';
jest.mock('../../common/redis');

describe('should get ', () => {
  const object = {
    bankCode: 'ABC01'
  };

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should return cached value', async () => {
    // Given
    const key = '28347254';
    const intermediateKey = 'usd96USGsjd978';
    const args = ['a', 'b'];
    const callbackFunction = jest.fn().mockResolvedValueOnce(object);
    redis.get = jest.fn(async k => {
      if (k == key) {
        return intermediateKey;
      }
      if (k == intermediateKey) {
        return object;
      }
    });

    // When
    const result = await redisRepository.getCacheFromIntermediateKey(
      key,
      callbackFunction,
      ...args
    );

    // Then
    expect(result).toEqual(object);
    expect(callbackFunction).not.toHaveBeenCalled();
  });

  it('should get from callback function if intermediate key is not found', async () => {
    // Given
    const key = '28347254';
    const intermediateKey = 'usd96USGsjd978';
    const args = ['a', 'b'];
    const callbackFunction = jest.fn().mockResolvedValueOnce(object);
    redis.get = jest.fn();

    // When
    const result = await redisRepository.getCacheFromIntermediateKey(
      key,
      callbackFunction,
      ...args
    );

    // Then
    expect(result).toEqual(object);
    expect(callbackFunction).toHaveBeenNthCalledWith(1, ...args);
  });
});
