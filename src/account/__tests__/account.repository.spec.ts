import httpClient from '../httpClient';
import accountRepository from '../account.repository';
import {
  IBlockingRequestPayload,
  IBlockingAmountResponse
} from '../account.type';
import { BlockingAmountType } from '@dk/module-common';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

jest.mock('../httpClient');

describe('account.repository', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('inquiry account info', () => {
    it('should call to accounts', async () => {
      // Given
      (httpClient.get as jest.Mock).mockResolvedValueOnce({
        data: {
          data: {
            accountNumber: 'accountNumber'
          }
        }
      });
      const bankCode = 'bankCode';
      const accountNumber = 'accountNumber';
      // When
      await accountRepository.inquiryAccountInfo(bankCode, accountNumber);

      // Then
      expect(httpClient.get).toHaveBeenCalledWith(
        `/accounts/inquiry?bankCode=${bankCode}&inputAccount=${accountNumber}`
      );
    });

    it('http client should have specified timeout', async () => {
      expect((httpClient as any).apiConfig.timeout).toEqual(40000);
    });

    it('should call to accounts with cif', async () => {
      // Given
      (httpClient.get as jest.Mock).mockResolvedValueOnce({
        data: {
          data: {
            accountNumber: 'accountNumber'
          }
        }
      });
      const bankCode = 'bankCode';
      const accountNumber = 'accountNumber';
      const cif = 'cif';
      // When
      await accountRepository.inquiryAccountInfo(
        bankCode,
        accountNumber,
        undefined,
        accountNumber,
        cif
      );

      // Then
      expect(httpClient.get).toHaveBeenCalledWith(
        `/accounts/inquiry?bankCode=${bankCode}&inputAccount=${accountNumber}&cif=${cif}&sourceAccount=${accountNumber}`
      );
    });

    it('should inquiry account with transaction amount', async () => {
      // Given
      (httpClient.get as jest.Mock).mockResolvedValueOnce({
        data: {
          data: {
            accountNumber: 'accountNumber'
          }
        }
      });
      const bankCode = 'bankCode';
      const accountNumber = 'accountNumber';
      const transactionAmount = 1000;
      // When
      await accountRepository.inquiryAccountInfo(
        bankCode,
        accountNumber,
        transactionAmount
      );

      // Then
      expect(httpClient.get).toHaveBeenCalledWith(
        `/accounts/inquiry?bankCode=${bankCode}&inputAccount=${accountNumber}&transactionAmount=${transactionAmount}`
      );
    });

    it('should throw if account number not returned', async () => {
      // Given
      (httpClient.get as jest.Mock).mockResolvedValueOnce({
        data: {
          data: {}
        }
      });
      const bankCode = 'bankCode';
      const accountNumber = 'accountNumber';
      // When
      const error = await accountRepository
        .inquiryAccountInfo(bankCode, accountNumber)
        .catch(e => e);

      // Then
      expect(httpClient.get).toHaveBeenCalledWith(
        `/accounts/inquiry?bankCode=${bankCode}&inputAccount=${accountNumber}`
      );
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_ACCOUNT);
    });
  });
  describe('query main account by CIF', () => {
    it('should call to get main-account endpoint with correct payload', async () => {
      (httpClient.get as jest.Mock).mockImplementationOnce(() => {
        Promise.resolve([]);
      });
      const cif = '123';

      // ACT
      await accountRepository.queryMainAccount(cif);

      // ASSERT
      expect(httpClient.get).toHaveBeenCalledWith(
        `/customers/${cif}/accounts/main-account`,
        {
          timeout: 75000
        }
      );
    });
  });

  describe('createBlockingAmount', () => {
    it('should call to accounts service to creat blocking amount', async () => {
      // Given
      const expectedBlockingAmountResponse: IBlockingAmountResponse = {
        id: 'blocking-id',
        status: 'ACTIVE',
        cardId: 'card-id'
      };
      (httpClient.post as jest.Mock).mockImplementationOnce(() =>
        Promise.resolve({
          data: {
            data: expectedBlockingAmountResponse
          }
        })
      );
      const cif = '123';
      const accountNumber = '123';
      const blockingAmountRequest: IBlockingRequestPayload = {
        amount: 1234,
        name: 'blocking-name',
        mambuBlockingCode: '10000007',
        blockingPurposeCode: BlockingAmountType.GINPAY
      };
      // When
      const result = await accountRepository.createBlockingAmount(
        cif,
        accountNumber,
        blockingAmountRequest
      );

      // Then
      expect(expectedBlockingAmountResponse).toMatchObject(result);
      expect(httpClient.post).toHaveBeenCalledWith(
        `/accounts/${accountNumber}/blocking-amounts?cif=${cif}`,
        blockingAmountRequest,
        {
          timeout: 75000
        }
      );
    });

    it('should return COREBANKING_BLOCK_AMOUNT_FAILED error when error from http', async () => {
      // Given
      (httpClient.post as jest.Mock).mockImplementationOnce(() =>
        Promise.reject(new Error('INVALID_REQUEST'))
      );
      const cif = '123';
      const accountNumber = '123';
      const blockingAmountRequest: IBlockingRequestPayload = {
        amount: 1234,
        name: 'blocking-name',
        mambuBlockingCode: '10000007',
        blockingPurposeCode: BlockingAmountType.GINPAY
      };
      // When
      let error;
      try {
        await accountRepository.createBlockingAmount(
          cif,
          accountNumber,
          blockingAmountRequest
        );
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(
        ERROR_CODE.COREBANKING_BLOCK_AMOUNT_FAILED
      );
    });
  });

  describe('update blocking amount', () => {
    it('should update blocking amount success', async () => {
      // Given
      (httpClient.put as jest.Mock).mockImplementationOnce(() => undefined);
      const cif = '123';
      const accountNumber = '123';
      const blockId = '123';
      const amount = 10000;
      // When
      await accountRepository.updateBlockingAmount(
        cif,
        accountNumber,
        blockId,
        amount
      );

      // Then
      expect(httpClient.put).toHaveBeenCalledWith(
        `/private/accounts/${accountNumber}/blocking-amounts/${blockId}?cif=${cif}`,
        {
          amount
        },
        {
          timeout: 75000
        }
      );
    });
  });

  describe('checkAmountLimit', () => {
    const sourceAccountNo = '123456';
    const amount = 100000;
    it('should return data undefined', async () => {
      const response = { data: { data: undefined } };
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);
      const expected = await accountRepository.checkAmountLimit(
        sourceAccountNo,
        amount
      );
      expect(httpClient.post).toHaveBeenCalledWith(
        `/private/accounts/auth-transaction-amount`,
        {
          authenticationAmount: amount,
          accountNumber: sourceAccountNo
        },
        {
          timeout: 75000
        }
      );
      expect(expected).toBeUndefined();
    });
    it('should return status success', async () => {
      const response = { data: { data: { status: 'success' } } };
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);
      const expected = await accountRepository.checkAmountLimit(
        sourceAccountNo,
        amount
      );
      expect(httpClient.post).toHaveBeenCalledWith(
        `/private/accounts/auth-transaction-amount`,
        {
          authenticationAmount: amount,
          accountNumber: sourceAccountNo
        },
        {
          timeout: 75000
        }
      );
      expect(expected).toBeDefined();
      expect(expected?.status).toEqual('success');
    });
    it('should return status reject', async () => {
      const response = { data: { data: { status: 'reject' } } };
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);
      const expected = await accountRepository.checkAmountLimit(
        sourceAccountNo,
        amount
      );
      expect(httpClient.post).toHaveBeenCalledWith(
        `/private/accounts/auth-transaction-amount`,
        {
          authenticationAmount: amount,
          accountNumber: sourceAccountNo
        },
        {
          timeout: 75000
        }
      );
      expect(expected).toBeDefined();
      expect(expected?.status).toEqual('reject');
    });
  });

  describe('getAccountInfo', () => {
    const accountNo = '123456';
    const cif = 'dummyCif';

    it('should throw error', async () => {
      (httpClient.get as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.INVALID_ACCOUNT
        )
      );
      await accountRepository.getAccountInfo(accountNo, cif).catch(_ => {
        expect(httpClient.get).toHaveBeenCalledWith(
          `/private/accounts/inquiry?accountNumber=${accountNo}&includeMigratingAccounts=true&cif=${cif}`
        );
      });
    });

    it('should return account info', async () => {
      (httpClient.get as jest.Mock).mockResolvedValueOnce({
        data: { data: 'success' }
      });
      const result = await accountRepository.getAccountInfo(accountNo, cif);
      expect(result).toBe('success');
      expect(httpClient.get).toHaveBeenCalledWith(
        `/private/accounts/inquiry?accountNumber=${accountNo}&includeMigratingAccounts=true&cif=${cif}`
      );
    });

    it('should return account info without having cif as query param', async () => {
      (httpClient.get as jest.Mock).mockResolvedValueOnce({
        data: { data: 'success' }
      });
      const result = await accountRepository.getAccountInfo(accountNo);
      expect(result).toBe('success');
      expect(httpClient.get).toHaveBeenCalledWith(
        `/private/accounts/inquiry?accountNumber=${accountNo}&includeMigratingAccounts=true`
      );
    });
  });

  describe('getActivatedAccount', () => {
    const accountNo = '123456';

    it('should throw error', async () => {
      (httpClient.get as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.INVALID_ACCOUNT
        )
      );
      await accountRepository.getActivatedAccount(accountNo).catch(_ => {
        expect(httpClient.get).toHaveBeenCalledWith(
          `/private/accounts/${accountNo}?fetchBalance=false`
        );
      });
    });

    it('should return activated account info', async () => {
      (httpClient.get as jest.Mock).mockResolvedValueOnce({
        data: { data: 'success' }
      });
      const result = await accountRepository.getActivatedAccount(accountNo);
      expect(result).toBe('success');
      expect(httpClient.get).toHaveBeenCalledWith(
        `/private/accounts/${accountNo}?fetchBalance=false`
      );
    });
  });
});
