export enum inDeliveryChannelEnum {
  DEFAULT = '02'
}
export enum transactionProcessingCodeEnum {
  FUND_TRANSFER = '401000'
}

export enum inSysOrgEnum {
  DEFAULT = 'MS ARTOS'
}

export enum inTermIdEnum {
  DEFAULT = 'JG8888'
}

export enum inTermLocEnum {
  DEFAULT = 'ARTOS DIGITAL BANK  HQ'
}
export enum inServiceIdEnum {
  FUND_TRANSFER = 'FundTrnsfr'
}
