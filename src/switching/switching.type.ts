export interface ISwitchingTransaction {
  inDeliveryChannel: string;
  inProcessingCode: string;
  inTxnAmt: string;
  inTxnDateTime: string;
  inSTAN: string;
  inAcqInstId: string;
  inRRN: string;
  inAddData: string;
  inCurrCode: number;
  inAcctId1: string;
  inAcctId2: string;
  inDestInstId: string;
  inSysOrg: string;
  inTermID: string;
  inTermLoc: string;
  inAuditData: ISwitchingTransactionAudit;
}
interface ISwitchingTransactionAudit {
  inApplication: string;
  inUserId: string;
  inPassword: string;
  inServiceID: string;
  inSequence: string;
}

export interface ISwitchingTransactionResponse {
  outRespCode: string;
  outRespMsg?: string;
  outAddData?: string;
  outSequence?: string;
}

export interface EuronetConfig {
  baseUrl: string;
  transactionUrl: string;
  accountInquiryUrl: string;
  userId: string;
  password: string;
  application: string;
  timeout: number;
}
