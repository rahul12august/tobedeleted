import logger, { wrapLogs } from '../logger';
import {
  EuronetConfig,
  ISwitchingTransaction,
  ISwitchingTransactionResponse
} from './switching.type';
import httpClient from './httpClient';
import { config } from '../config';
import { get } from 'lodash';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import circuitBreakerFactory from './circuitBreaker';
import { updateRequestIdInRedis } from '../common/contextHandler';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const SWITCHING_SUCCESS_RESPONSE_CODE = '00';

const switchingEuronetConfig: EuronetConfig = config.get('switching').euronet;

const doSubmitTransaction = async (
  transaction: ISwitchingTransaction
): Promise<ISwitchingTransactionResponse> => {
  updateRequestIdInRedis(transaction.inRRN);
  logger.info(
    `Submitting Euronet transaction with RRN : ${transaction.inRRN}, processingCode : ${transaction.inProcessingCode},
      sequence : ${transaction.inAuditData.inSequence}, 
      inAcqInstId: ${transaction.inAcqInstId},
      inDestInstId: ${transaction.inDestInstId},
      inAcctId1: ${transaction.inAcctId1},
      inAcctId2: ${transaction.inAcctId2},
      inAddData: ${transaction.inAddData}`
  );
  const response = await httpClient.post(
    switchingEuronetConfig.transactionUrl,
    transaction
  );
  const data: ISwitchingTransactionResponse = get(response, ['data'], {});
  if (data.outRespCode !== SWITCHING_SUCCESS_RESPONSE_CODE) {
    const detail = `Switching Euronet has returned error response with RRN: ${transaction.inRRN}`;
    logger.error(`${detail}, response: ${JSON.stringify(data)}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.EURONET,
      ERROR_CODE.ERROR_FROM_SWICHING
    );
  }
  logger.info(`Submit Euronet transaction response : ${JSON.stringify(data)}`);
  return data;
};

const submitTransaction = circuitBreakerFactory.create(doSubmitTransaction);

const switchingRepository = {
  submitTransaction
};

export default wrapLogs(switchingRepository);
