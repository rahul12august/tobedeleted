/*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
import httpClient from '../httpClient';

describe('Ueronet HTTP Client', () => {
  it('should have a timeout value of 50 seconds', function() {
    expect((httpClient as any).apiConfig.timeout).toEqual(93000);
  });
});
