import switchingRepository from '../switching.repository';
import {
  getMockedTransaction,
  getMockedSwitchingResponse
} from '../__mocks__/switchingTransaction.data';
import { ISwitchingTransactionResponse } from '../switching.type';
import { TransferAppError } from '../../errors/AppError';
import httpClient from '../httpClient';

jest.mock('../../logger');
jest.mock('../httpClient');

describe('submitTransaction', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });
  const testInput = getMockedTransaction();

  it('should sumbit transaction successfully with correct payload', async () => {
    const mockedSwitchingResponse = getMockedSwitchingResponse();
    (httpClient.post as jest.Mock).mockResolvedValueOnce({
      data: mockedSwitchingResponse
    });

    const response = await switchingRepository.submitTransaction(testInput);

    expect(response).toEqual(mockedSwitchingResponse);
    expect(httpClient.post).toHaveBeenCalledWith(`/fundtransfer`, testInput);
  });

  it('should throw when switching returned error response', async () => {
    const errorResponse: ISwitchingTransactionResponse = {
      outRespCode: '01',
      outRespMsg: '"inProcessingCode" must be a string',
      outAddData: '201174000003',
      outSequence: 'FROMVan Thi1'
    };
    (httpClient.post as jest.Mock).mockResolvedValueOnce({
      data: errorResponse
    });

    const error = await switchingRepository
      .submitTransaction(testInput)
      .catch(err => err);

    expect(error).toBeInstanceOf(TransferAppError);
  });
});
