import circuitBreakerFactory from '../circuitBreaker';

describe('ueronet Circuit Breaker', () => {
  it('should be opened when more than 50% of the requests are failed', async function() {
    const openCallback = jest.fn();
    const originalFunction = jest.fn();
    const wrappedFunction = circuitBreakerFactory.create(originalFunction, {
      open: openCallback
    });

    // Success request for 40 times.
    originalFunction.mockResolvedValue('success');
    for (let i = 0; i < 40; i++) {
      await wrappedFunction();
    }

    // Failed request for 40 times.
    originalFunction.mockRejectedValue('failure');
    for (let i = 0; i < 40; i++) {
      await wrappedFunction().catch(() => {});
      expect(openCallback).not.toBeCalled();
    }

    // If failed request count to exceed success request count, then circuit should be opened.
    await wrappedFunction().catch(() => {});
    expect(openCallback).toBeCalled();
  });

  it('should adhere minimum threshold of 50 requests before considering opening the circuit', async function() {
    const openCallback = jest.fn();
    const originalFunction = jest.fn();
    const wrappedFunction = circuitBreakerFactory.create(originalFunction, {
      open: openCallback
    });

    // Failed request for 49 times.
    originalFunction.mockRejectedValue('failure');
    for (let i = 0; i < 49; i++) {
      await wrappedFunction().catch(() => {});
      expect(openCallback).not.toBeCalled();
    }

    // Failed request for 50 times, then circuit should be opened.
    await wrappedFunction().catch(() => {});
    expect(openCallback).toBeCalled();
  });

  it('should close the circuit after receiving successful request in halfopen state', async function() {
    const resetTimeout = 50; // 0.05s
    const openCallback = jest.fn();
    const halfOpenCallback = jest.fn();
    const closeCallback = jest.fn();
    const originalFunction = jest.fn();
    const wrappedFunction = circuitBreakerFactory.create(originalFunction, {
      open: openCallback,
      halfOpen: halfOpenCallback,
      close: closeCallback,
      resetTimeout: resetTimeout
    });

    // Failed request for 50 times to open the circuit.
    originalFunction.mockRejectedValue('failure');
    for (let i = 0; i < 50; i++) {
      await wrappedFunction().catch(() => {});
    }

    expect(openCallback).toBeCalled();

    // Wait for reset timeout to switch to half open state.
    await new Promise(resolve => setTimeout(resolve, resetTimeout + 1));
    expect(halfOpenCallback).toBeCalled();

    // First success request in half open state to close the circuit.
    originalFunction.mockResolvedValue('success');
    await wrappedFunction();
    expect(closeCallback).toBeCalled();
  });

  it('should not consider HTTP status code 200..<500 as failure', async function() {
    const failureCallback = jest.fn();
    const originalFunction = jest.fn();

    const wrappedFunction = circuitBreakerFactory.create(originalFunction, {
      failure: failureCallback
    });

    failureCallback.mockClear();
    originalFunction.mockRejectedValue({ status: 200 });
    await wrappedFunction().catch(() => {});
    expect(failureCallback).not.toBeCalled();

    failureCallback.mockClear();
    originalFunction.mockRejectedValue({ status: 499 });
    await wrappedFunction().catch(() => {});
    expect(failureCallback).not.toBeCalled();

    failureCallback.mockClear();
    originalFunction.mockRejectedValue({ status: 500 });
    await wrappedFunction().catch(() => {});
    expect(failureCallback).toBeCalled();

    failureCallback.mockClear();
    originalFunction.mockRejectedValue(new Error('Not an HTTP error'));
    await wrappedFunction().catch(() => {});
    expect(failureCallback).toBeCalled();
  });
});
