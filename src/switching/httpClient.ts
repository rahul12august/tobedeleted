import { config } from '../config';
import { EuronetConfig } from './switching.type';
import { context } from '../context';
import logger from '../logger';
import { HttpClient } from '@dk/module-httpclient';
import { Http } from '@dk/module-common';
import https from 'https';
import { retryConfig } from '../common/constant';

const switchingEuronetConfig: EuronetConfig = config.get('switching').euronet;

const httpClient: HttpClient = Http.createHttpClientForward({
  baseURL: switchingEuronetConfig.baseUrl,
  timeout: switchingEuronetConfig.timeout,
  httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  context,
  logger,
  retryConfig: {
    ...retryConfig,
    networkErrorCodesToRetry: ['ENOENT', 'ECONNREFUSED']
  }
});

export default httpClient;
