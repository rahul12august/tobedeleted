import {
  inDeliveryChannelEnum,
  transactionProcessingCodeEnum,
  inSysOrgEnum,
  inTermIdEnum,
  inTermLocEnum
} from '../switching.enum';
import {
  ISwitchingTransaction,
  ISwitchingTransactionResponse
} from '../switching.type';

export const getMockedTransaction = (): ISwitchingTransaction => ({
  inDeliveryChannel: inDeliveryChannelEnum.DEFAULT,
  inAuditData: {
    inApplication: 'artos',
    inUserId: 'artos-user',
    inPassword: 'artos-password',
    inServiceID: 'string;',
    inSequence: '348384989'
  },
  inProcessingCode: transactionProcessingCodeEnum.FUND_TRANSFER,
  inTxnAmt: '250000',
  inTxnDateTime: '301090000', // numeric value based on datetime format MMDDhhmmss
  inSTAN: '123456',
  inAcqInstId: '542',
  inRRN: '12345690877',
  inAddData: `FROM JAGO ACCOUNT NAME`,
  inCurrCode: 0,
  inAcctId1: '9993883883',
  inAcctId2: '9083887477',
  inDestInstId: '542',
  inSysOrg: inSysOrgEnum.DEFAULT,
  inTermID: inTermIdEnum.DEFAULT,
  inTermLoc: inTermLocEnum.DEFAULT
});

export const getMockedSwitchingResponse = (): ISwitchingTransactionResponse => ({
  outRespCode: '00',
  outRespMsg: 'success',
  outAddData: '201174000003',
  outSequence: 'FROMVan Thi1'
});
