import { context } from '../context';
import redis from './redis';
import logger from '../logger';
import { Tracing } from './constant';
import { config } from '../config';
import { isEmpty } from 'lodash';

const prefixKey = `${config.get('serviceName')}-request-id`;
const expiresIn = 600;

/* 
   To have same trancing id across we are saving request id in redis for 10 minutes and when external 
   api calls to give status we will use same request id. 
*/
export const updateRequestIdInContext = async (
  referenceId: string
): Promise<void> => {
  logger.info(
    `updateRequestIdInContext : Get reference id from redis cache and set it in context for ${referenceId}.`
  );
  try {
    const requestId = await redis.get(referenceId, prefixKey);
    logger.info(
      `updateRequestIdInContext : Request Id ${requestId} for Reference Id : ${referenceId}`
    );
    if (requestId) {
      const contextObject: any = context.getStore();
      contextObject[Tracing.TRANSACTION_ID] = requestId;
    }
  } catch (err) {
    logger.error(
      `Error updateRequestIdInContext : error while updating request id - ${err}`
    );
    return;
  }
};

export const updateRequestIdInRedis = (referenceId: string): void => {
  logger.info(
    `updateRequestIdInRedis : Get request id from context and set it in redis for ${referenceId}.`
  );
  try {
    const contextObject: any = context.getStore(); // any becuase can return any object
    const requestId = contextObject && contextObject[Tracing.TRANSACTION_ID];
    if (requestId && !isEmpty(requestId)) {
      redis.mSet({
        prefix: prefixKey,
        keyValueObjectList: [
          {
            key: referenceId,
            value: requestId,
            ttl: expiresIn
          }
        ]
      });
    }
  } catch (err) {
    logger.error(
      `Error updateRequestIdInRedis : error while fetching request id from context - ${err}`
    );
    return;
  }
};
