const Tracing = {
  TRANSACTION_ID: 'x-request-id',
  TRACER_SESSION: 'TRACER_SESSION',
  RETRY_COUNT: 'RETRY_COUNT',
  TRANSACTION_INFO_TRACKER: 'TRANSACTION_INFO_TRACKER',
  PROGRESS_TRACKER: 'PROGRESS_TRACKER',
  TRANSACTION_USAGE_INFO_TRACKER: 'TRANSACTION_USAGE_INFO_TRACKER'
};

const HttpHeaders = {
  AUTH: 'authorization',
  X_IDEMPOTENCY_KEY: 'X-Idempotency-Key',
  CONTENT_TYPE: 'Content-Type',
  ACCEPT: 'Accept',
  SIGNATURE: 'signature'
};

const HTTP_HEADERS = {
  AUTH: 'authorization'
};

const FEATURE_FLAG = {
  ENABLE_AWARD_V2: 'enableAwardV2',
  ENABLE_NON_BLOCKING_INVERSE_TRANSACTION_ON_REVERSAL:
    'enableNonBlockingInverse',
  ENABLE_DAILY_LIMIT_ATM_WITHDRAWAL_POCKET_LIMIT_VALIDATION:
    'enableDailyLimitAtmWithdrawalPocketLevelValidation',
  ENABLE_DAILY_LIMIT_ATM_TRANSFER_POCKET_LIMIT_VALIDATION:
    'enableDailyLimitAtmTransferPocketLevelValidation',
  LEGACY_BALANCE_CHECK_KEY: 'useLegacyBalanceCheck'
};

const PREFIX_TOKEN = 'Bearer ';

const AUTH_STRATEGY = 'jwt';

const httpClientTimeoutErrorCode = 'ECONNABORTED';

export const AUTH_CREDENTIALS_PAYLOAD = 'auth.credentials.payload';

export const SENSITIVE_DEBIT_TRANSACTION_CODES = ['PRD01', 'PRD02', 'PRD03'];

export const SENSITIVE_CREDIT_TRANSACTION_CODES = ['PRC01', 'PRC02', 'PRC03'];

export const DEFAULT_MASKING_PLACEHOLDER = '*****';

export const UTC_OFFSET_WESTERN_INDONESIAN = 7;

export const DEFAULT_DATE_FORMAT = 'YYYY-MM-DD';

export const BI_FAST_DATE_FORMAT = 'YYMMDD';

const kafkaHeaders = {
  X_IDEMPOTENCY_KEY: 'x-idempotency-key'
};

export const retryConfig = {
  networkErrorCodesToRetry: [
    'ENOENT',
    'ECONNRESET',
    'ECONNREFUSED',
    'ECONNABORTED'
  ],
  maxRetriesOnError: 3,
  delay: 1000
};

export {
  PREFIX_TOKEN,
  HTTP_HEADERS,
  FEATURE_FLAG,
  Tracing,
  HttpHeaders,
  httpClientTimeoutErrorCode,
  AUTH_STRATEGY,
  kafkaHeaders
};

export const MICROSERVICE_NAME = 'ms-transfer';
