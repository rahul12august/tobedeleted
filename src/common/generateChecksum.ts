import crypto from 'crypto';

const algorithm = 'sha512';

//secret Key – Key shared between channel and Euronet
//Data String is pipe delimited values
const getSecureSHAHash = (secretKey: string, dataString: string) =>
  crypto
    .createHmac(algorithm, Buffer.from(secretKey, 'utf-8'))
    .update(Buffer.from(dataString, 'utf-8'))
    .digest('hex')
    .toUpperCase();

export { getSecureSHAHash };
