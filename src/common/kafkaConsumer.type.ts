import { VariadicFunction } from '@dk/module-redis/dist/type';

export interface IFormattedMessageType {
  key: string;
  value: any;
  headers: any;
}

export interface IMessageProcessorCallbackPayload {
  message: IFormattedMessageType;
  topicName: string;
  partition: number;
}

export type IFunctionWithPromiseReturn = (
  messageToConsume?: IMessageProcessorCallbackPayload
) => Promise<any>;

export interface ITopicsMap {
  [topicName: string]: IFunctionWithPromiseReturn;
}

export interface IConsumerConfig {
  groupId: string;
  topic: string;
  handler: VariadicFunction;
  isDqlNotRequired?: boolean;
  consumerRunConfig?: {
    partitionsConsumedConcurrently?: number;
    isLocal: boolean;
  };
  consumerInitConfig?: {
    maxBytes?: number;
    isLocal: boolean;
    useLocalRedis: boolean;
  };
}
