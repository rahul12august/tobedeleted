import { IErrorDetail } from '@dk/module-common/dist/error/error.interface';
import {
  RetryableTransferAppError,
  TransferAppError
} from '../errors/AppError';
import { ERROR_CODE } from './errors';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

export const mapTransferAppError = (
  err: any,
  code: ERROR_CODE,
  retry: boolean = false
) => {
  const errorDetail: IErrorDetail[] = [];
  errorDetail.push({
    message: (err && err.message) || '',
    key: code,
    code: (err && err.code) || ''
  });
  let detail, actor;
  if (err instanceof TransferAppError) {
    detail = err.detail;
    actor = err.actor;
  } else {
    detail = (err && err.message) || 'Unexpected error!';
    actor = TransferFailureReasonActor.UNKNOWN;
  }

  throw new RetryableTransferAppError(detail, actor, code, retry, errorDetail);
};

export const trimStringValuesFromObject = (payload: any): any => {
  if (typeof payload === 'object') {
    const keys: string[] = Object.keys(payload);
    for (let key of keys) {
      if (typeof payload[key] === 'string') {
        payload[key] = payload[key].trim();
      }
    }
  }
  return payload;
};

export const enforceStringFormat = (
  payload: Record<string, unknown>,
  field: string
) => {
  if (payload[field]) {
    payload[field] = String(payload[field]);
  }
};

export const enforceNumberFormat = (
  payload: Record<string, unknown>,
  field: string
) => {
  if (payload[field]) {
    payload[field] = Number(payload[field]);
  }
};

export const enforceBooleanFormat = (
  payload: Record<string, unknown>,
  field: string
) => {
  if (payload[field]) {
    payload[field] = Boolean(payload[field]);
  }
};
