import { config } from '@dk/module-config';
import logger from '../logger';

let configFlag = config.get('featureFlags');

export const isFeatureEnabled = (flag: string): boolean => {
  if (undefined === configFlag) {
    configFlag = config.get('featureFlags');
  }

  if (!configFlag) {
    return false;
  }

  let isEnabled = configFlag[flag] === 'true';
  logger.info(`is ${flag} Feature Flag enabled: ${isEnabled}`);
  return isEnabled;
};

export const refresh = () => {
  configFlag = undefined;
};
