import currencyFormatter from 'currency-formatter';
const IDR = 'id-ID';

export const toIndonesianRupiah = (number: number) =>
  currencyFormatter.format(number, {
    locale: IDR,
    symbol: ''
  });
