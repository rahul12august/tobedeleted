import connectRedis from '@dk/module-redis';
import logger from '../logger';
import { config } from '../config';

const redisConfig = {
  isLocal: config.get('redis').isLocal
};

const redis = connectRedis(redisConfig, logger);

export default redis;
