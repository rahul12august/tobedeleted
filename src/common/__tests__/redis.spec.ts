import redisClient from '../redis';

jest.unmock('../redis');

jest.mock('@dk/module-redis', () =>
  jest.fn(() => ({
    mSet: jest.fn()
  }))
);

describe('redis', () => {
  it('call redis init', async () => {
    expect(redisClient).toBeDefined();
  });
});
