import {
  generateOutgoingId,
  getNextCounterByPrefix
} from '../externalIdGenerator.util';
import { ERROR_CODE } from '../errors';
import outgoingCounterRepository from '../../outgoingCounter/outgoingCounter.repository';
import { advanceTo, clear } from 'jest-date-mock';
import { IOutgoingCounter } from '../../outgoingCounter/outgoingCounter.interface';
import { getITransactionInput } from '../../transaction/__mocks__/transaction.data';
import { BankChannelEnum, Constant } from '@dk/module-common';
import { ITransactionInput } from '../../transaction/transaction.type';

jest.mock('../../outgoingCounter/outgoingCounter.repository');
describe('externalIdGenerator util', () => {
  afterAll(() => {
    clear();
  });
  const mockedJulianDate = '20085';
  const mockJulianDateGeneration = () => {
    advanceTo('2020-03-25T08:14:34.662Z');
  };
  const mockOutgoingCounter = (value: IOutgoingCounter) => {
    (outgoingCounterRepository.getNextCounterByPrefix as jest.Mock).mockResolvedValue(
      value
    );
  };

  const transaction: ITransactionInput = {
    ...getITransactionInput(),
    beneficiaryAccountName: 'invalid',
    beneficiaryBankCode: 'BC005',
    beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL,
    beneficiaryBankName: 'bank',
    sourceAccountName: 'invalid',
    sourceAccountType: 'MA', // should be invalid value by default
    sourceBankCodeChannel: BankChannelEnum.INTERNAL,
    sourceBankCode: Constant.GIN_BANK_CODE_ID,
    sourceTransactionCurrency: 'IDR',
    beneficiarySupportedChannels: [
      BankChannelEnum.BIFAST,
      BankChannelEnum.EXTERNAL
    ]
  };

  describe('getNextCounterByPrefix', () => {
    it('should throw error if prefix is blank ', async () => {
      mockOutgoingCounter({
        prefix: '20123T',
        counter: 1000001
      });
      const counter = await getNextCounterByPrefix('');
      expect(counter).toBeDefined();
    });

    it('should throw error if prefix is any', async () => {
      mockOutgoingCounter({
        prefix: '20123T',
        counter: 1000001
      });
      const counter = await getNextCounterByPrefix('TEST1');
      expect(counter).toBeDefined();
    });
  });

  describe('generateOutgoingId', () => {
    beforeEach(() => {
      jest.resetAllMocks();
    });
    it('should throws error when external max limit counter is crossed', async () => {
      const newTransaction = {
        ...transaction,
        beneficiarySupportedChannels: undefined
      };
      (outgoingCounterRepository.getNextCounterByPrefix as jest.Mock).mockResolvedValueOnce(
        {
          prefix: '20123T',
          counter: 1000001
        }
      );
      mockJulianDateGeneration();
      await generateOutgoingId(newTransaction, 'TFD50').catch(e => {
        expect(e.errorCode).toEqual(ERROR_CODE.MAXIMUM_COUNTER_REACHED);
      });
    });

    it('should throws error when BIFAST max limit counter is crossed', async () => {
      const newTransaction = {
        ...transaction,
        sourceRemittanceCode: 'CENAIDJA'
      };
      (outgoingCounterRepository.getNextCounterByPrefix as jest.Mock).mockResolvedValueOnce(
        {
          prefix: '20123T',
          counter: 100000001
        }
      );
      mockJulianDateGeneration();
      await generateOutgoingId(newTransaction, 'TFD50').catch(e => {
        expect(e.errorCode).toEqual(ERROR_CODE.MAXIMUM_COUNTER_REACHED);
      });
    });

    it('should throws error when sourceRemittanceCode is undefined', async () => {
      const newTransaction = {
        ...transaction,
        paymentServiceCode: 'BIFAST_OUTGOING',
        sourceRemittanceCode: undefined
      };
      (outgoingCounterRepository.getNextCounterByPrefix as jest.Mock).mockResolvedValueOnce(
        {
          prefix: '20123T',
          counter: 100000001
        }
      );
      mockJulianDateGeneration();
      await generateOutgoingId(newTransaction).catch(e => {
        expect(e.errorCode).toEqual(ERROR_CODE.EMPTY_REMITTANCE_CODE);
      });
    });

    it('should return bifast outgoing id with right format', async () => {
      const newTransaction = {
        ...transaction,
        sourceRemittanceCode: 'CENAIDJA',
        paymentServiceCode: 'BIFAST_OUTGOING'
      };
      const mockedBiFastJulianDate = '200325';
      (outgoingCounterRepository.getNextCounterByPrefix as jest.Mock).mockResolvedValueOnce(
        {
          prefix: '20123T',
          counter: 12
        }
      );
      mockJulianDateGeneration();

      const result = await generateOutgoingId(newTransaction);

      expect(result).toEqual(`${mockedBiFastJulianDate}CENAIDJA00000012`);
    });

    it('should return external outgoing id with right format', async () => {
      const newTransaction = {
        ...transaction,
        beneficiarySupportedChannels: [BankChannelEnum.EXTERNAL]
      };
      (outgoingCounterRepository.getNextCounterByPrefix as jest.Mock)
        .mockResolvedValueOnce({
          prefix: '20123T',
          counter: 12
        })
        .mockResolvedValueOnce({
          prefix: '20123T',
          counter: 12
        });
      mockJulianDateGeneration();

      const result = await generateOutgoingId(newTransaction, 'TFD60');

      expect(result).toEqual(`${mockedJulianDate}T000011`);
    });

    it('should return external outgoing id when beneficiarySupportedChannels is undefined', async () => {
      const newTransaction = {
        ...transaction,
        beneficiarySupportedChannels: undefined
      };
      (outgoingCounterRepository.getNextCounterByPrefix as jest.Mock).mockResolvedValueOnce(
        {
          prefix: '20123T',
          counter: 12
        }
      );
      mockJulianDateGeneration();

      const result = await generateOutgoingId(newTransaction, 'TFD60');

      expect(result).toEqual(`${mockedJulianDate}T000011`);
    });

    it('should thow error debitTransactionCode and beneficiarySupportedChannels is undefined', async () => {
      const newTransaction = {
        ...transaction,
        beneficiarySupportedChannels: undefined,
        debitTransactionCode: undefined
      };
      (outgoingCounterRepository.getNextCounterByPrefix as jest.Mock).mockResolvedValueOnce(
        {
          prefix: '20123T',
          counter: 12
        }
      );
      mockJulianDateGeneration();

      await generateOutgoingId(newTransaction).catch(e => {
        expect(e.errorCode).toEqual(ERROR_CODE.EMPTY_TRANSACTION_CODE);
      });
    });
  });
});
