import { toDate, isDateWithinYear } from '../dateUtils';
import { ERROR_CODE } from '../errors';
import moment = require('moment');
import { TransferAppError } from '../../errors/AppError';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

describe('dateUtils', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('positive scenario', () => {
    it('should get proper proper date when string is correct format YYYY-MM-DD', async () => {
      // Given
      const dateString = '2020-10-20';
      // When
      const actualResponse = toDate(dateString);
      // Then
      expect(actualResponse).toBeDefined();
      expect(actualResponse).toEqual(new Date(2020, 9, 20));
    });

    it('should throw an error when string is incorrect format', async () => {
      // given
      const dateString = '20ab-10-20';
      let error;

      // when
      try {
        toDate(dateString);
      } catch (err) {
        error = err;
      }

      // then
      expect(error).toBeDefined();
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Invalid date format: 20ab-10-20!');
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_DATE_FORMAT);
    });
  });

  describe('isDateWithinYear', () => {
    it('should return true if date is exactly within an year', () => {
      const dateInput = moment()
        .subtract(1, 'year')
        .toDate();

      const result = isDateWithinYear(dateInput);

      expect(result).toEqual(true);
    });

    it('should return true if date is well within an year', () => {
      const dateInput = moment()
        .add(2, 'days')
        .subtract(1, 'year')
        .toDate();

      const result = isDateWithinYear(dateInput);

      expect(result).toEqual(true);
    });

    it('should return false if date is not within an year', () => {
      const dateInput = moment()
        .subtract(2, 'days')
        .subtract(1, 'year')
        .toDate();

      const result = isDateWithinYear(dateInput);

      expect(result).toEqual(false);
    });
  });
});
