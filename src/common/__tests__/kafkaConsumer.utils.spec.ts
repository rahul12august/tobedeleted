import consumerUtils from '../kafkaConsumer.utils';
import { kafkaConsumer } from '@dk/module-kafka';
import logger from '../../logger';
import { context } from '../../context';
jest.mock('@dk/module-kafka', () => {
  return {
    kafkaConsumer: {
      init: jest.fn(),
      connectSubscribeRun: jest.fn()
    }
  };
});
describe('kafkaConsumer.utils', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });
  const groupId = 'group-test';
  const topicName = 'test';
  const callback = async (): Promise<any> => {};
  describe('generateTopicsMapConsumer', () => {
    it('should return valid consumer', async () => {
      const expected = consumerUtils.generateTopicsMapConsumer(
        topicName,
        callback
      );
      expect(expected[topicName]).toEqual(callback);
    });
  });
  describe('initConsumer', () => {
    it('should init customer valid', async () => {
      const topicsMap = consumerUtils.generateTopicsMapConsumer(
        topicName,
        callback
      );
      const consumer = kafkaConsumer.init(
        { groupId: groupId },
        logger,
        context
      );

      await consumerUtils.initConsumer({
        groupId: groupId,
        topic: topicName,
        handler: callback
      });
      expect(kafkaConsumer.connectSubscribeRun).toBeCalledWith(
        consumer,
        topicsMap,
        undefined
      );
    });
    it('should init with config ', async () => {
      const topicsMap = consumerUtils.generateTopicsMapConsumer(
        topicName,
        callback
      );
      const consumer = kafkaConsumer.init(
        { groupId: groupId },
        logger,
        context
      );

      await consumerUtils.initConsumer({
        groupId: groupId,
        topic: topicName,
        handler: callback,
        consumerRunConfig: {
          partitionsConsumedConcurrently: 1,
          isLocal: false
        }
      });
      expect(kafkaConsumer.connectSubscribeRun).toBeCalledWith(
        consumer,
        topicsMap,
        {
          partitionsConsumedConcurrently: 1,
          isLocal: false
        }
      );
    });
  });
});
