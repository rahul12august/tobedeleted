import { punycodeUtil } from '../encodeString.util';

describe('encode string', () => {
  it('encode note contain emoji', async () => {
    const ExpectedResult = 'xn--I   !-pe1cr7gba70750cca';
    const notes = 'I ❤️ ❤️ ☕!';

    const result = punycodeUtil.encodeString(notes);
    expect(ExpectedResult).toEqual(result);
  });
  it('encode note contain emoji and emoji delimeter', async () => {
    const ExpectedResult = 'xn--:hey: I   !-6i8g53mba73453fca';
    const notes = ':hey: I ❤️ ❤️ ☕!';

    const result = punycodeUtil.encodeString(notes);
    expect(ExpectedResult).toEqual(result);
  });

  it('encode note if not contain emoji', async () => {
    const ExpectedResult = 'hei jude';
    const notes = 'hei jude';

    const result = punycodeUtil.encodeString(notes);
    expect(ExpectedResult).toEqual(result);
  });
});
