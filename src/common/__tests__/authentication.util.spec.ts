import { getAuthPayloadFromAdminToken } from '../authentication.util';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../errors';
import { AdminPortalUserPermissionCode } from '@dk/module-common';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

describe('authentication util', () => {
  describe('getAuthPayloadFromAdminToken', () => {
    it('should throw error if token do not have roles', () => {
      // Given
      const request: any = {
        auth: {}
      };

      // When and then
      expect(() => getAuthPayloadFromAdminToken(request)).toThrowError(
        new TransferAppError(
          "Auth credentials payload doesn't contain any roles!",
          TransferFailureReasonActor.SUBMITTER,
          ERROR_CODE.UNAUTHORIZED
        )
      );
    });

    it('should return roles', () => {
      // Given
      const request: any = {
        auth: {
          credentials: {
            payload: {
              roles: [
                AdminPortalUserPermissionCode.CUSTOMER_INFO_READ,
                AdminPortalUserPermissionCode.MS_ADMIN_PORTAL_EMPLOYEE_PAYROLL_MANAGER
              ]
            }
          }
        }
      };

      // When and then
      expect(getAuthPayloadFromAdminToken(request)).toEqual({
        roles: [
          AdminPortalUserPermissionCode.CUSTOMER_INFO_READ,
          AdminPortalUserPermissionCode.MS_ADMIN_PORTAL_EMPLOYEE_PAYROLL_MANAGER
        ]
      });
    });
  });
});
