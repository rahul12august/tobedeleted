import { getSecureSHAHash } from '../generateChecksum';

describe('generateChecksum utility', () => {
  beforeEach(() => {
    expect.hasAssertions();
  });
  describe('getSecureSHAHash', () => {
    it('should return expected hash', () => {
      const testCases = [
        {
          key: 'my secret key',
          stringData: 'Hello World!',
          expected:
            'A0D3C346671E38494B8461FA1F6F1F7E3BAD456EE74311EC814D50888C0D9E9953D448B6C109B67B9661783F7829CA74804234E187ADD09AE4EEE501EAC88CFD'
        },
        {
          key: 'secret tiếng Việt',
          stringData: 'Hello Thế giới!',
          expected:
            '9792804B0AC664CE10E18EED1357C19D7684A62AD830CDF449039515A3DD5E15A2327757D070CA5C0193E44F705E440530EEBAEFDDA3900855B46FD0A87E77FC'
        }
      ];
      testCases.forEach(testCase => {
        expect(getSecureSHAHash(testCase.key, testCase.stringData)).toEqual(
          testCase.expected
        );
      });
    });
  });
});
