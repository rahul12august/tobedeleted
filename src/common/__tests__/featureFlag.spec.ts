import { isFeatureEnabled, refresh } from '../featureFlag';
import { FEATURE_FLAG } from '../constant';

import { config } from '@dk/module-config';
jest.mock('@dk/module-config');

describe('isFeatureEnabled', () => {
  const withFeatureFlag = (key: string, value: string) => {
    (config.get as jest.Mock).mockImplementation(theKey => {
      if (theKey === 'featureFlags') {
        const result = {};
        result[key] = value;
        return result;
      }
    });
  };

  beforeEach(() => {
    refresh();
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should return false by default', () => {
    expect(isFeatureEnabled('')).toBeFalsy();
    expect(isFeatureEnabled('NO-SUCH-KEY')).toBeFalsy();
  });

  it('should return false when flag is not defined', () => {
    const result = isFeatureEnabled('enableAwardV2');

    expect(result).toBeFalsy();
  });

  it('Should properly detect ENABLE_AWARD_V2 flag set to true', () => {
    withFeatureFlag(FEATURE_FLAG.ENABLE_AWARD_V2, 'true');
    expect(isFeatureEnabled(FEATURE_FLAG.ENABLE_AWARD_V2)).toBe(true);
  });

  it('Should properly detect LEGACY_BALANCE_CHECK_KEY flag set to false', () => {
    withFeatureFlag(
      FEATURE_FLAG.ENABLE_DAILY_LIMIT_ATM_TRANSFER_POCKET_LIMIT_VALIDATION,
      'false'
    );
    expect(
      isFeatureEnabled(
        FEATURE_FLAG.ENABLE_DAILY_LIMIT_ATM_TRANSFER_POCKET_LIMIT_VALIDATION
      )
    ).toBe(false);
  });

  it('Should properly detect LEGACY_BALANCE_CHECK_KEY flag set to true', () => {
    withFeatureFlag(FEATURE_FLAG.LEGACY_BALANCE_CHECK_KEY, 'true');
    expect(isFeatureEnabled(FEATURE_FLAG.LEGACY_BALANCE_CHECK_KEY)).toBe(true);
  });

  it('Should properly detect any feature flag set to true', () => {
    for (const featureFlag in FEATURE_FLAG) {
      refresh();
      withFeatureFlag(FEATURE_FLAG[featureFlag], 'true');
      expect(isFeatureEnabled(FEATURE_FLAG[featureFlag])).toBeTruthy();
    }
  });

  it('Should properly detect any feature flag not set as false', () => {
    for (const featureFlag in FEATURE_FLAG) {
      expect(isFeatureEnabled(FEATURE_FLAG[featureFlag])).toBeFalsy();
    }
  });

  it('Should properly detect different value than "true" for ENABLE_DAILY_LIMIT_ATM_TRANSFER_POCKET_LIMIT_VALIDATION', () => {
    withFeatureFlag(
      FEATURE_FLAG.ENABLE_DAILY_LIMIT_ATM_TRANSFER_POCKET_LIMIT_VALIDATION,
      '1'
    );
    expect(
      isFeatureEnabled(
        FEATURE_FLAG.ENABLE_DAILY_LIMIT_ATM_TRANSFER_POCKET_LIMIT_VALIDATION
      )
    ).toBeFalsy();
  });
});
