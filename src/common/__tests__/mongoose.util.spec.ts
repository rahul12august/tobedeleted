import { handleDuplicationError } from '../mongoose.util';
import {
  MONGO_ERROR,
  MONGO_ERROR_CODE,
  ERROR_CODE,
  MONGO_TIMEOUT_ERROR
} from '../errors';
import {
  RetryableTransferAppError,
  TransferAppError
} from '../../errors/AppError';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

describe('mongoose.util', () => {
  describe('handleDuplicationError', () => {
    it('should call next method with AppError if MONGO_ERROR_CODE is DUPLICATED_KEY', () => {
      // given
      const error = {
        name: MONGO_ERROR,
        code: MONGO_ERROR_CODE.DUPLICATED_KEY
      };
      const next = jest.fn();

      // when
      handleDuplicationError(error, {}, next);

      // then
      const transferAppError = next.mock.calls?.[0]?.[0];
      expect(transferAppError).toBeInstanceOf(TransferAppError);
      expect(transferAppError.detail).toEqual(
        'Trying to save duplicated document in collection!'
      );
      expect(transferAppError.actor).toEqual(
        TransferFailureReasonActor.MS_TRANSFER
      );
      expect(transferAppError.errorCode).toEqual(ERROR_CODE.UNIQUE_FIELD);
    });

    it('should call next method with AppError if MONGO_ERROR_CODE is MONGO_TIMEOUT_ERROR', () => {
      // given
      const error = {
        name: MONGO_TIMEOUT_ERROR,
        message: `MongoTimeoutError: Server selection timed out after 30000 ms
        at Timeout._onTimeout (/app/node_modules/mongodb/lib/core/sdam/topology.js:850:16)
        at listOnTimeout (internal/timers.js:554:17)
        at processTimers (internal/timers.js:497:7)`
      };
      const next = jest.fn();

      // when
      handleDuplicationError(error, {}, next);

      // then
      const transferAppError = next.mock.calls?.[0]?.[0];
      expect(transferAppError).toBeInstanceOf(RetryableTransferAppError);
      expect(transferAppError.detail).toEqual('Mongo timeout error!');
      expect(transferAppError.actor).toEqual(
        TransferFailureReasonActor.MS_TRANSFER
      );
      expect(transferAppError.errorCode).toEqual(
        ERROR_CODE.MONGO_TIMEOUT_ERROR
      );
    });

    it('should call next method with AppError if MONGO_ERROR_CODE is not MONGO_TIMEOUT_ERROR', () => {
      // Given
      const error = {
        name: MONGO_TIMEOUT_ERROR,
        message: 'test'
      };
      const next = jest.fn();

      // When
      handleDuplicationError(error, {}, next);

      // Then
      expect(next).toHaveBeenCalledWith();
    });

    it('should call next method without AppError if MONGO_ERROR_CODE is not DUPLICATED_KEY', () => {
      // Given
      const error = {
        name: 'DocumentNotFoundError',
        code: 'test',
        message: undefined
      };
      const next = jest.fn();

      // When
      handleDuplicationError(error, {}, next);

      // Then
      expect(next).toHaveBeenCalledWith();
    });
  });
});
