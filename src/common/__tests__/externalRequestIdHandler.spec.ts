import redis from '../redis';
import {
  updateRequestIdInContext,
  updateRequestIdInRedis
} from '../externalRequestIdHandler';
import { context } from '../../context';

jest.mock('../redis', () => ({
  mSet: jest.fn(),
  get: jest.fn()
}));

jest.mock('../../context', () => ({
  context: {
    getStore: jest.fn(),
    enterWith: jest.fn()
  }
}));

describe('external Request id handler', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });
  const prefixKey = `ms-transfer-request-id`;
  describe('updateRequestIdInContext', () => {
    it('should get request id from redis and update in context', async () => {
      // Given
      const referenceId = '123';
      // when
      (redis.get as jest.Mock).mockResolvedValueOnce('test123');
      (context.getStore as jest.Mock).mockReturnValue({
        ['x-request-id']: 'abcd'
      });
      await updateRequestIdInContext(referenceId);

      // Then
      const contextObject: any = context.getStore();
      const requestId = contextObject && contextObject['x-request-id'];
      expect(requestId).toEqual('test123');
      expect(redis.get).toHaveBeenCalledWith('123', prefixKey);
    });

    it('should not get request id from redis', async () => {
      // Given
      const referenceId = '123';
      // when
      (redis.get as jest.Mock).mockResolvedValueOnce(null);
      await updateRequestIdInContext(referenceId);

      // Then
      expect(context.enterWith).not.toHaveBeenCalled();
      expect(redis.get).toHaveBeenCalledWith('123', prefixKey);
    });

    it('should throw error while getting request id from redis', async () => {
      // Given
      const referenceId = '123';
      // when
      (redis.get as jest.Mock).mockRejectedValueOnce('error');
      let error;
      try {
        await updateRequestIdInContext(referenceId);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeUndefined();
      expect(context.enterWith).not.toHaveBeenCalled();
      expect(redis.get).toHaveBeenCalledWith('123', prefixKey);
    });
  });

  describe('updateRequestIdInRedis', () => {
    it('should get request id from context and update in redis', async () => {
      // Given
      const referenceId = '123';
      // when
      (context.getStore as jest.Mock).mockReturnValueOnce({
        ['x-request-id']: 'test123'
      });
      redis.mSet as jest.Mock;
      await updateRequestIdInRedis(referenceId);

      // Then
      expect(context.getStore).toHaveBeenCalled();
      expect(redis.mSet).toBeCalledWith({
        prefix: prefixKey,
        keyValueObjectList: [
          {
            key: referenceId,
            value: 'test123',
            ttl: 600
          }
        ]
      });
    });

    it('should not get request id from redis', async () => {
      // Given
      const referenceId = '123';
      // when
      (redis.get as jest.Mock).mockResolvedValueOnce(null);
      await updateRequestIdInContext(referenceId);

      // Then
      expect(context.enterWith).not.toHaveBeenCalled();
      expect(redis.get).toHaveBeenCalledWith('123', prefixKey);
    });

    it('should throw error while getting request id from redis', async () => {
      // Given
      const referenceId = '123';
      // when
      (redis.get as jest.Mock).mockRejectedValueOnce('error');
      let error;
      try {
        await updateRequestIdInContext(referenceId);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeUndefined();
      expect(context.enterWith).not.toHaveBeenCalled();
      expect(redis.get).toHaveBeenCalledWith('123', prefixKey);
    });
  });
});
