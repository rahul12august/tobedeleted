import errorHandler, { validateHandler } from '../handleValidationErrors';
import { ERROR_CODE } from '../errors';
import { TransferAppError } from '../../errors/AppError';
import Joi from '@hapi/joi';
import { JoiValidator } from '@dk/module-common';

describe('handleValidationErrors.validateHandler', () => {
  const transactionResponseValidator = Joi.object({
    id: Joi.string().required(),
    referenceId: JoiValidator.optionalString(),
    paymentServiceType: Joi.string(),
    transactionIds: Joi.array()
      .items(Joi.string())
      .required()
  }).label('Transaction Response');

  it('should return payload if there is no error', () => {
    const payload = {
      id: '1244',
      referenceId: '',
      paymentServiceType: 'test',
      transactionIds: ['1']
    };
    const result = validateHandler(payload, transactionResponseValidator);
    expect(result).toEqual(payload);
  });

  it('should throw error', () => {
    const payload = {
      id: '1244',
      referenceId: '',
      paymentServiceType: '',
      transactionIds: ['1']
    };
    try {
      validateHandler(payload, transactionResponseValidator);
    } catch (error) {
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
    }
  });
});

describe('handleValidationErrors', () => {
  const request = {};
  const response = { continue: '' };

  it('should return continue if there is no error', () => {
    const result = errorHandler(request, response, null);
    expect(result).toBe(response.continue);
  });

  it('should return origin error if it is not Joi validation errors', () => {
    const someError = new Error();
    try {
      errorHandler(request, response, someError);
    } catch (err) {
      expect(err).toBe(someError);
    }
  });

  it('should filter duplicated path of errors', () => {
    class JoiError extends Error {
      isJoi = true;
      details = [
        { message: 'message', path: ['message'], type: 'type1' },
        { message: 'message', path: ['message'], type: 'min' }
      ];
    }
    const joiError = new JoiError();
    try {
      errorHandler(request, response, joiError);
    } catch (err) {
      expect(err).toBeInstanceOf(TransferAppError);
      expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
      expect(err.errors.length).toBe(1);
      expect(err.errors[0].key).toEqual('message');

      expect(err.errors[0].code).toEqual(ERROR_CODE.INCORRECT_FIELD);
    }
  });

  it('should return default code if default error for the constraint is not defined', () => {
    class JoiError extends Error {
      isJoi = true;
      details = [{ message: 'message', path: ['message'], type: 'type' }];
    }
    const joiError = new JoiError();
    try {
      errorHandler(request, response, joiError);
    } catch (err) {
      expect(err).toBeInstanceOf(TransferAppError);
      expect(err.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
      expect(err.errors[0].key).toEqual('message');

      expect(err.errors[0].code).toEqual(ERROR_CODE.INCORRECT_FIELD);
    }
  });
});
