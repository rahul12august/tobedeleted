import redis from '../redis';
import {
  getRetryCount,
  getTransactionInfoTracker,
  getTransactionUsageInfoTracker,
  setRetryCount,
  setTransactionInfoTracker,
  setTransactionUsageInfoTracker,
  updateRequestIdInContext,
  updateRequestIdInRedis
} from '../contextHandler';
import { context } from '../../context';
import faker from 'faker';
import { Tracing } from '../constant';
import { TransactionUsageContext } from '../../transactionUsage/transactionUsage.type';
import {
  TransactionUsageCategory,
  TransactionUsageGroupCode
} from '../../transactionUsage/transactionUsage.enum';
import { ATM_WITHDRAWAL_JAGO } from '../../transaction/transaction.category.enum';

jest.mock('../redis', () => ({
  mSet: jest.fn(),
  get: jest.fn()
}));

jest.mock('../../context', () => ({
  context: {
    getStore: jest.fn(),
    enterWith: jest.fn()
  }
}));

describe('external Request id handler', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });
  const prefixKey = `ms-transfer-request-id`;
  describe('updateRequestIdInContext', () => {
    it('should get request id from redis and update in context', async () => {
      // Given
      const referenceId = '123';
      // when
      (redis.get as jest.Mock).mockResolvedValueOnce('test123');
      (context.getStore as jest.Mock).mockReturnValue({
        ['x-request-id']: 'abcd'
      });
      await updateRequestIdInContext(referenceId);

      // Then
      const contextObject: any = context.getStore();
      const requestId = contextObject && contextObject['x-request-id'];
      expect(requestId).toEqual('test123');
      expect(redis.get).toHaveBeenCalledWith('123', prefixKey);
    });

    it('should not get request id from redis', async () => {
      // Given
      const referenceId = '123';
      // when
      (redis.get as jest.Mock).mockResolvedValueOnce(null);
      await updateRequestIdInContext(referenceId);

      // Then
      expect(context.enterWith).not.toHaveBeenCalled();
      expect(redis.get).toHaveBeenCalledWith('123', prefixKey);
    });

    it('should throw error while getting request id from redis', async () => {
      // Given
      const referenceId = '123';
      // when
      (redis.get as jest.Mock).mockRejectedValueOnce('error');
      let error;
      try {
        await updateRequestIdInContext(referenceId);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeUndefined();
      expect(context.enterWith).not.toHaveBeenCalled();
      expect(redis.get).toHaveBeenCalledWith('123', prefixKey);
    });
  });

  describe('updateRequestIdInRedis', () => {
    it('should get request id from context and update in redis', async () => {
      // Given
      const referenceId = '123';
      // when
      (context.getStore as jest.Mock).mockReturnValueOnce({
        ['x-request-id']: 'test123'
      });
      redis.mSet as jest.Mock;
      await updateRequestIdInRedis(referenceId);

      // Then
      expect(context.getStore).toHaveBeenCalled();
      expect(redis.mSet).toBeCalledWith({
        prefix: prefixKey,
        keyValueObjectList: [
          {
            key: referenceId,
            value: 'test123',
            ttl: 600
          }
        ]
      });
    });

    it('should not get request id from redis', async () => {
      // Given
      const referenceId = '123';
      // when
      (redis.get as jest.Mock).mockResolvedValueOnce(null);
      await updateRequestIdInContext(referenceId);

      // Then
      expect(context.enterWith).not.toHaveBeenCalled();
      expect(redis.get).toHaveBeenCalledWith('123', prefixKey);
    });

    it('should throw error while getting request id from redis', async () => {
      // Given
      const referenceId = '123';
      // when
      (redis.get as jest.Mock).mockRejectedValueOnce('error');
      let error;
      try {
        await updateRequestIdInContext(referenceId);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeUndefined();
      expect(context.enterWith).not.toHaveBeenCalled();
      expect(redis.get).toHaveBeenCalledWith('123', prefixKey);
    });
  });

  describe('setRetryCount', () => {
    it('update retry count in context', async () => {
      // Given
      const count = 1;
      // when
      (context.getStore as jest.Mock).mockReturnValue({
        ['RETRY_COUNT']: 1
      });
      await setRetryCount(count);

      // Then
      const contextObject: any = context.getStore();
      const retryCount = contextObject && contextObject['RETRY_COUNT'];
      expect(retryCount).toEqual(1);
    });
  });

  describe('getRetryCount', () => {
    it('get retry count from context', async () => {
      // Given
      const count = 1;
      // when
      (context.getStore as jest.Mock).mockReturnValue({
        ['RETRY_COUNT']: 1
      });
      const retryCount = getRetryCount();

      // Then
      expect(context.getStore).toHaveBeenCalled();
      expect(retryCount).toEqual(1);
    });
  });

  describe('TransactionInfoTracker', () => {
    it('setTransactionInfoTracker', async () => {
      // Given
      const info = [
        {
          counterCode: 'counterCode',
          entitlementCodeRefId: 'entitlementCodeRefId',
          customerId: 'customerId'
        }
      ];
      // when
      (context.getStore as jest.Mock).mockReturnValue({
        ['TRANSACTION_INFO_TRACKER']: [
          {
            counterCode: 'counterCode',
            entitlementCodeRefId: 'entitlementCodeRefId',
            customerId: 'customerId'
          }
        ]
      });
      await setTransactionInfoTracker(info);

      // Then
      const contextObject: any = context.getStore();
      const infoTracker =
        contextObject && contextObject['TRANSACTION_INFO_TRACKER'];
      expect(infoTracker).toEqual(info);
    });

    it('getTransactionInfoTracker', async () => {
      // Given
      const info = [
        {
          counterCode: 'counterCode',
          entitlementCodeRefId: 'entitlementCodeRefId',
          customerId: 'customerId'
        }
      ];
      // when
      (context.getStore as jest.Mock).mockReturnValue({
        ['TRANSACTION_INFO_TRACKER']: [
          {
            counterCode: 'counterCode',
            entitlementCodeRefId: 'entitlementCodeRefId',
            customerId: 'customerId'
          }
        ]
      });
      await getTransactionInfoTracker();

      // Then
      const contextObject: any = context.getStore();
      const infoTracker =
        contextObject && contextObject['TRANSACTION_INFO_TRACKER'];
      expect(infoTracker).toEqual(info);
    });
  });

  describe('TransactionUsageInfoTracker', () => {
    it('setTransactionUsageInfoTracker', async () => {
      // Given
      const info: TransactionUsageContext = {
        customerId: faker.random.alphaNumeric(10),
        accountNo: faker.random.alphaNumeric(10),
        transactionCategory: TransactionUsageCategory.ATM_WITHDRAWAL,
        currentTransactionAmount: 500000,
        paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode,
        limitGroupCode: TransactionUsageGroupCode.ATM_WITHDRAWAL
      };

      // when
      (context.getStore as jest.Mock).mockReturnValue({
        [Tracing.TRANSACTION_USAGE_INFO_TRACKER]: info
      });
      await setTransactionUsageInfoTracker(info);

      // Then
      const contextObject: any = context.getStore();
      const infoTracker =
        contextObject && contextObject[Tracing.TRANSACTION_USAGE_INFO_TRACKER];
      expect(infoTracker).toEqual(info);
    });

    it('getTransactionUsageInfoTracker', async () => {
      // Given
      const info: TransactionUsageContext = {
        customerId: faker.random.alphaNumeric(10),
        accountNo: faker.random.alphaNumeric(10),
        transactionCategory: TransactionUsageCategory.ATM_WITHDRAWAL,
        currentTransactionAmount: 500000,
        paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode,
        limitGroupCode: TransactionUsageGroupCode.ATM_WITHDRAWAL
      };

      // when
      (context.getStore as jest.Mock).mockReturnValue({
        [Tracing.TRANSACTION_USAGE_INFO_TRACKER]: info
      });
      await getTransactionUsageInfoTracker();

      // Then
      const contextObject: any = context.getStore();
      const infoTracker =
        contextObject && contextObject[Tracing.TRANSACTION_USAGE_INFO_TRACKER];
      expect(infoTracker).toEqual(info);
    });
  });
});
