import { Enum } from '@dk/module-common';

export type NewEntity<T> = Omit<T, 'id'>;

export interface IPollingConfig {
  type: Enum.BillingAggregatorPollingConfigType;
  delayTime: string;
}

export interface IBillingAggregatorPollingConfig {
  billingAggregator: Enum.BillingAggregator;
  configs: IPollingConfig[];
}
