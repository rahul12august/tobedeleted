import punycode from 'punycode';

const encodeString = (note: string) => {
  return punycode.toASCII(note);
};

export const punycodeUtil = {
  encodeString
};
