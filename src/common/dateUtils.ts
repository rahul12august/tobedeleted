import logger from '../logger';
import { ERROR_CODE } from './errors';
import moment from 'moment-timezone';
import {
  DAYS_IN_YEAR,
  FORMAT_HOUR_MINUS_24H,
  TMZ
} from '../transaction/transaction.constant';
import { TransferAppError } from '../errors/AppError';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

export const toDate = (dateString: string): Date => {
  const [year, month, day] = dateString.split('-');
  if (isNaN(Number(year)) || isNaN(Number(month)) || isNaN(Number(day))) {
    const detail = `Invalid date format: ${dateString}!`;
    logger.error(`toDate: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_DATE_FORMAT
    );
  }
  return new Date(Number(year), Number(month) - 1, Number(day));
};

export const isDateWithinYear = (dateInput: Date = new Date()): boolean => {
  const currentDate = moment().tz(TMZ);
  const compareDiff = currentDate.diff(moment(dateInput).tz(TMZ), `days`);
  return compareDiff <= DAYS_IN_YEAR;
};

export const to24HourFormat = (dateInput: Date = new Date()): string => {
  return moment(dateInput)
    .tz(TMZ)
    .format(FORMAT_HOUR_MINUS_24H);
};

export const toJKTDate = (
  dateInput: Date = new Date(),
  dateformat: string
): string => {
  return moment(dateInput)
    .tz(TMZ)
    .format(dateformat);
};

export const isCurrentDate = (date: Date): boolean => {
  const currentDate = moment().tz(TMZ);
  const transactionDate = moment(date).tz(TMZ);
  return (
    transactionDate.year() === currentDate.year() &&
    transactionDate.month() === currentDate.month() &&
    transactionDate.date() === currentDate.date()
  );
};
