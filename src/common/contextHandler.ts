import { context } from '../context';
import redis from './redis';
import logger from '../logger';
import { Tracing } from './constant';
import { config } from '../config';
import { isEmpty } from 'lodash';
import { EntitlementContext } from '../entitlement/entitlement.interface';
import { TransactionUsageContext } from '../transactionUsage/transactionUsage.type';

const prefixKey = `${config.get('serviceName')}-request-id`;
const expiresIn = 600;

/* 
   To have same trancing id across we are saving request id in redis for 10 minutes and when external 
   api calls to give status we will use same request id. 
*/
export const updateRequestIdInContext = async (
  referenceId: string
): Promise<void> => {
  logger.info(
    `updateRequestIdInContext : Get reference id from redis cache and set it in context for ${referenceId}.`
  );
  try {
    const requestId = await redis.get(referenceId, prefixKey);
    logger.info(
      `updateRequestIdInContext : Request Id ${requestId} for Reference Id : ${referenceId}`
    );
    if (requestId) {
      const contextObject: any = context.getStore();
      contextObject[Tracing.TRANSACTION_ID] = requestId;
    }
  } catch (err) {
    logger.error(
      `Error updateRequestIdInContext : error while updating request id - ${err}`
    );
    return;
  }
};

export const updateRequestIdInRedis = (referenceId: string): void => {
  logger.info(
    `updateRequestIdInRedis : Get request id from context and set it in redis for ${referenceId}.`
  );
  try {
    const contextObject: any = context.getStore(); // any becuase can return any object
    const requestId = contextObject && contextObject[Tracing.TRANSACTION_ID];
    if (requestId && !isEmpty(requestId)) {
      redis.mSet({
        prefix: prefixKey,
        keyValueObjectList: [
          {
            key: referenceId,
            value: requestId,
            ttl: expiresIn
          }
        ]
      });
    }
  } catch (err) {
    logger.error(
      `Error updateRequestIdInRedis : error while fetching request id from context - ${err}`
    );
    return;
  }
};

export const getRequestId = (): string => {
  logger.info(`getRequestId : Get request id from context.`);
  try {
    const contextObject: any = context.getStore(); // any becuase can return any object
    return contextObject && contextObject[Tracing.TRANSACTION_ID];
  } catch (err) {
    logger.error(
      `Error getRequestId : error while fetching request id from context - ${err}`
    );
    return '';
  }
};

export const setRetryCount = async (count: number): Promise<void> => {
  logger.info(`setRetryCount : set retry count in context for ${count}.`);
  const contextObject: any = context.getStore();
  if (isEmpty(contextObject)) {
    logger.error(`setRetryCount : Async Storage context is missing.`);
    return;
  }
  contextObject[Tracing.RETRY_COUNT] = count;
  logger.info(contextObject);
};

export const getRetryCount = (): number | undefined => {
  logger.info(`getRetryCount : get retry count from context.`);
  const contextObject: any = context.getStore();
  if (isEmpty(contextObject)) {
    logger.error(`getRetryCount : Async Storage context is missing.`);
    return undefined;
  }
  const retryCount = contextObject && contextObject[Tracing.RETRY_COUNT];
  return retryCount;
};

export const setTransactionInfoTracker = async (
  entitlement: EntitlementContext[]
): Promise<void> => {
  logger.info(
    `setTransactionInfoTracker : set transaction info in context for ${entitlement}.`
  );
  const contextObject: any = context.getStore();
  contextObject[Tracing.TRANSACTION_INFO_TRACKER] = entitlement;
  logger.info(contextObject);
};

export const getTransactionInfoTracker = ():
  | EntitlementContext[]
  | undefined => {
  logger.info(`getTransactionInfoTracker : get TRANSACTION info in context`);
  const contextObject: any = context.getStore();
  const entitlement = contextObject[Tracing.TRANSACTION_INFO_TRACKER];
  logger.info(`entitlement info: ${entitlement}`);
  return entitlement;
};

export const setTransactionUsageInfoTracker = async (
  transactionUsageInfo: TransactionUsageContext
): Promise<void> => {
  logger.info(
    `setTransactionUsageInfoTracker: set transaction limit info in context for ${transactionUsageInfo}`
  );

  const contextObject: any = context.getStore();
  contextObject[Tracing.TRANSACTION_USAGE_INFO_TRACKER] = transactionUsageInfo;
  logger.info(contextObject);
};

export const getTransactionUsageInfoTracker = ():
  | TransactionUsageContext
  | undefined => {
  logger.info(
    `getTransactionUsageInfoTracker: get TRANSACTION LIMIT info in context`
  );
  const contextObject: any = context.getStore();
  const transactionUsage =
    contextObject[Tracing.TRANSACTION_USAGE_INFO_TRACKER];
  logger.info(`transaction limit info: ${transactionUsage}`);
  return transactionUsage;
};

export const setFnProgressTracker = async (
  initialTime: number
): Promise<void> => {
  const contextObject: any = context.getStore();

  contextObject[Tracing.PROGRESS_TRACKER] = initialTime;
};

export const getFnProgressTracker = (): number | undefined => {
  const contextObject: any = context.getStore();

  const startTime = contextObject[Tracing.PROGRESS_TRACKER];
  return startTime;
};
