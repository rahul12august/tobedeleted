import moment from 'moment';
import outgoingCounterRepository from '../outgoingCounter/outgoingCounter.repository';
import logger from '../logger';
import { ERROR_CODE } from './errors';
import { TransferAppError } from '../errors/AppError';
import { isEmpty } from 'lodash';
import { BI_FAST_DATE_FORMAT } from './constant';
import { isBIFast } from '../transaction/transaction.util';
import { ITransactionModel } from '../transaction/transaction.model';
import { NewEntity } from './common.type';
import { TMZ } from '../transaction/transaction.constant';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const MAX_RRN_COUNTER = 999999; // 6 digit number
const MAX_BIFAST_COUNTER = 99999999; //8 digit number
const BIFAST_PADDING_ZEROS = 8;
const OUTGOING_PADDING_ZEROS = 6;
export const getJulianDate = (
  format: string = 'YYDDDD',
  isBIFAST: boolean = false
): string => {
  return isBIFAST
    ? moment()
        .tz(TMZ)
        .format(format)
    : moment().format(format); // moment will format padding 0 - https://momentjs.com/docs/#/displaying/
};

export const getNextCounterByPrefix = async (
  prefix: string,
  isBIFAST?: boolean
): Promise<number> => {
  const outgoingCounter = await outgoingCounterRepository.getNextCounterByPrefix(
    prefix
  );
  return isBIFAST ? outgoingCounter.counter : outgoingCounter.counter - 1; // transform to base 0
};

const generateId = async (
  code: string,
  maxCounter: number,
  paddingZeros: number,
  isBIFAST?: boolean,
  format?: string
): Promise<string> => {
  const julianDate = getJulianDate(format, isBIFAST);
  const prefix = `${julianDate}${code}`;
  const currentCounter = await getNextCounterByPrefix(prefix, isBIFAST);
  if (currentCounter > maxCounter) {
    const detail = `Maximum outgoing id counter is reached, cannot generate thirdPartyOutgoingId!`;
    logger.error(`generateOutgoingId: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.MAXIMUM_COUNTER_REACHED
    );
  }
  return `${prefix}${currentCounter.toString().padStart(paddingZeros, '0')}`;
};

const generateBifastExternalId = async (
  transaction: NewEntity<ITransactionModel>
): Promise<string | undefined> => {
  const isBIFAST = isBIFast(transaction.paymentServiceCode);
  if (!isBIFAST) {
    logger.info('generateBifastExternalId: supportedchannel is not bifast');
    return;
  }
  if (!transaction.sourceRemittanceCode) {
    const detail = `Cannot generate bifast thirdPartyOutgoingId!`;
    logger.error(`generateBifastExternalId: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.JAGO,
      ERROR_CODE.EMPTY_REMITTANCE_CODE
    );
  }
  return generateId(
    transaction.sourceRemittanceCode,
    MAX_BIFAST_COUNTER,
    BIFAST_PADDING_ZEROS,
    isBIFAST,
    BI_FAST_DATE_FORMAT
  );
};

export const generateOutgoingId = async (
  transaction: NewEntity<ITransactionModel>,
  debitTransactionCode?: string
): Promise<string | undefined> => {
  let externalId = await generateBifastExternalId(transaction);
  if (isEmpty(externalId)) {
    if (!(debitTransactionCode && debitTransactionCode.length)) {
      const detail = `Cannot generate external thirdPartyOutgoingId!`;
      logger.error(`generateOutgoingId: ${detail}`);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.JAGO,
        ERROR_CODE.EMPTY_TRANSACTION_CODE
      );
    }

    externalId = await generateId(
      debitTransactionCode[0],
      MAX_RRN_COUNTER,
      OUTGOING_PADDING_ZEROS
    );
  }
  return externalId;
};
