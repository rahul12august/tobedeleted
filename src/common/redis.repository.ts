import _ from 'lodash';
import { wrapLogs } from '../logger';
import redis from './redis';

const getCacheFromIntermediateKey = async <T>(
  key: string,
  onCacheNotFound: (...args: any[]) => Promise<T>,
  ...args: any[]
): Promise<T> => {
  const intermediateCache = await redis.get(key);

  if (_.isEmpty(intermediateCache)) {
    return await onCacheNotFound(...args);
  }

  return await redis.get(intermediateCache, onCacheNotFound);
};

const redisRepository = {
  getCacheFromIntermediateKey
};

export default wrapLogs(redisRepository);
