import uuidv4 from 'uuid/v4';

import { kafkaHeaders } from './constant';

const getHeaders = (key?: string) => ({
  [kafkaHeaders.X_IDEMPOTENCY_KEY]: key ?? uuidv4()
});

const kafkaUtils = {
  getHeaders
};

export default kafkaUtils;
