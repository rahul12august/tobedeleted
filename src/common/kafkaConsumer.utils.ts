import {
  kafkaConsumer,
  ITopicsMap,
  ITopicDLQOptionFields
} from '@dk/module-kafka';
import logger from '../logger';
import { context } from '../context';
import { IConsumerConfig } from './kafkaConsumer.type';
import { Consumer } from 'kafkajs';

type VariadicFunction = (...args: any[]) => any;

export const generateTopicsMapConsumer = (
  topicName: string,
  callback: VariadicFunction
): ITopicsMap => {
  const topicsMap: ITopicsMap = {};
  topicsMap[`${topicName}`] = callback;
  return topicsMap;
};

export const generateTopicsMapConsumerWithDLQOptions = (
  topicName: string,
  dlqOption: ITopicDLQOptionFields
): ITopicsMap => {
  const topicsMap: ITopicsMap = {};
  topicsMap[`${topicName}`] = dlqOption;
  return topicsMap;
};

export const initConsumer = async (
  consumerConfig: IConsumerConfig
): Promise<Consumer> => {
  const consumer = kafkaConsumer.init(
    {
      groupId: consumerConfig.groupId,
      ...consumerConfig.consumerInitConfig
    },
    logger,
    context
  );
  let topicsMap: ITopicsMap = {};
  if (consumerConfig.isDqlNotRequired != true) {
    topicsMap = generateTopicsMapConsumer(
      consumerConfig.topic,
      consumerConfig.handler
    );
  } else {
    topicsMap = generateTopicsMapConsumerWithDLQOptions(consumerConfig.topic, {
      isDqlNotRequired: consumerConfig.isDqlNotRequired,
      messageConsumingFunction: consumerConfig.handler
    });
  }
  await kafkaConsumer.connectSubscribeRun(
    consumer,
    topicsMap,
    consumerConfig.consumerRunConfig
  );
  return consumer;
};

export default {
  generateTopicsMapConsumer,
  initConsumer
};
