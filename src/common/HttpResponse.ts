import { ERROR_CODE } from './errors';
import { Http } from '@dk/module-common';

export class HttpResponse<T> extends Http.ResponseBase<T, ERROR_CODE> {
  constructor() {
    super();
  }
}
