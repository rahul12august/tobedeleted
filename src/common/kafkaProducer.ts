import { kafkaProducer } from '@dk/module-kafka';
import { TransactionGroupConstant } from '@dk/module-message';
import logger from '../logger';
import { context } from '../context';
import { config } from '../config';

const kafKaConfig = {
  serviceName: TransactionGroupConstant.TRANSACTION_HISTORY_GROUPID,
  idempotent: false,
  isLocal: config.get('kafka').isLocal
};

const producer = kafkaProducer.init(kafKaConfig, logger, context);

export default producer;
