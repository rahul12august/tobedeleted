import logger from '../logger';

const enableEntitlement = process.env['ENABLE_ENTITLEMENT']
  ? process.env['ENABLE_ENTITLEMENT']
  : false;

const isEnabled = async (): Promise<boolean> => {
  const isEnabled = enableEntitlement === 'true';

  logger.info(`Entitlement flag is ${isEnabled}`);
  return isEnabled;
};

export default {
  isEnabled
};
