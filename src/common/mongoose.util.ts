import {
  ERROR_CODE,
  MONGO_ERROR,
  MONGO_ERROR_CODE,
  MONGO_TIMEOUT_ERROR
} from './errors';
import {
  RetryableTransferAppError,
  TransferAppError
} from '../errors/AppError';
import logger from '../logger';
import { getErrorDetails } from '../errors/error.util';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

export const handleDuplicationError = (error: any, _doc: any, next: any) => {
  if (
    error.name === MONGO_ERROR &&
    error.code === MONGO_ERROR_CODE.DUPLICATED_KEY
  ) {
    const detail = `Trying to save duplicated document in collection!`;
    logger.error(`handleDuplicationError: ${detail}`);
    next(
      new TransferAppError(
        detail,
        TransferFailureReasonActor.MS_TRANSFER,
        ERROR_CODE.UNIQUE_FIELD
      )
    );
  } else if (
    (error.name === MONGO_ERROR || error.name === MONGO_TIMEOUT_ERROR) &&
    error.message &&
    error.message.includes(MONGO_TIMEOUT_ERROR)
  ) {
    const detail = 'Mongo timeout error!';
    logger.error(`handleDuplicationError: ${detail}`);
    next(
      new RetryableTransferAppError(
        detail,
        TransferFailureReasonActor.MS_TRANSFER,
        ERROR_CODE.MONGO_TIMEOUT_ERROR,
        true,
        getErrorDetails(
          ERROR_CODE.MONGO_TIMEOUT_ERROR,
          error.message,
          ERROR_CODE.MONGO_TIMEOUT_ERROR
        )
      )
    );
  } else {
    logger.error(
      `handleDuplicationError: Mongo error message : ${error.message ||
        error.name}`
    );
    next();
  }
};
