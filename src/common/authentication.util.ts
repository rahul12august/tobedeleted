import { get } from 'lodash';
import hapi from '@hapi/hapi';

import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from './errors';
import { AUTH_CREDENTIALS_PAYLOAD } from './constant';
import { AdminPortalUserPermissionCode } from '@dk/module-common';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

export const getAuthPayloadFromAdminToken = (
  request: hapi.Request
): {
  roles: AdminPortalUserPermissionCode[];
} => {
  const auth = get(request, AUTH_CREDENTIALS_PAYLOAD, {});
  if (!auth.roles) {
    throw new TransferAppError(
      "Auth credentials payload doesn't contain any roles!",
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.UNAUTHORIZED
    );
  }
  return auth;
};
