import { UploadFile } from '@dk/module-common';
import { config } from '../config';
import { UploadConfig } from '@dk/module-common/dist/uploadFile/uploadFile.type';

const partnerReportBucket = config.get('ossPartnerReport');

const partnerReportConfigOSS: UploadConfig = {
  ...partnerReportBucket,
  accessKeyId: process.env.OSS_PARTNER_REPORT_ACCESS_KEY_ID,
  accessKeySecret: process.env.OSS_PARTNER_REPORT_ACCESS_KEY_SECRET
};

const partnerReport = new UploadFile(partnerReportConfigOSS);

const fileUploader = {
  partnerReport
};

export default fileUploader;
