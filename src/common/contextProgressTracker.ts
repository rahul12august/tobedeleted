import logger from '../logger';
import { getFnProgressTracker, setFnProgressTracker } from './contextHandler';

export class ContextProgressTracker {
  private static instance: ContextProgressTracker;

  private constructor() {}

  initTracker() {
    //set timer to context
    setFnProgressTracker(new Date().getTime());
  }

  logProgress(baseMsg: string) {
    const initTime = getFnProgressTracker();
    if (initTime) {
      logger.debug(`${baseMsg} : ${new Date().getTime() - initTime}ms`);
    }
  }

  public static getInstance(): ContextProgressTracker {
    if (!ContextProgressTracker.instance) {
      ContextProgressTracker.instance = new ContextProgressTracker();
    }

    return ContextProgressTracker.instance;
  }
}
