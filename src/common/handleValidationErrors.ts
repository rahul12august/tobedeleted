import hapi from '@hapi/hapi';
import {
  Schema,
  validate,
  ValidationError,
  ValidationErrorItem,
  ValidationOptions
} from '@hapi/joi';
import { ERROR_CODE, ErrorList, JoiValidationErrors } from './errors';
import { ErrorDetails, TransferAppError } from '../errors/AppError';
import logger from '../logger';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

interface JoiValidationErrors {
  [index: string]: ERROR_CODE;
}

const getMappedDetails = (details: ValidationErrorItem[]): ErrorDetails[] => {
  const mappedDetails = details.reduce<ErrorDetails[]>((acc, detail, index) => {
    if (index !== 0 && detail.path[0] === details[index - 1].path[0]) {
      return acc;
    }

    const constraint = detail.type.split('.').pop() || '';
    const errorCode =
      (JoiValidationErrors as JoiValidationErrors)[constraint] ||
      ERROR_CODE.INCORRECT_FIELD;
    const defaultError = ErrorList[errorCode];
    acc.push({
      message: defaultError.message,
      code: errorCode,
      key: `${detail.path[0]}`
    });

    return acc;
  }, []);

  return mappedDetails;
};

export const validateHandler = <I, S extends Schema>(
  payload: I,
  schema: S,
  options: ValidationOptions = {}
): I => {
  const { error, value } = validate(payload, schema, options);
  if (error) {
    const mappedDetails = getMappedDetails(error.details);
    logger.error(
      `validateHandler: invalid request ${JSON.stringify(mappedDetails)}`
    );
    throw new TransferAppError(
      `Invalid request detected in validate handler ${JSON.stringify(
        mappedDetails
      )}`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.INVALID_REQUEST,
      mappedDetails
    );
  }
  return value;
};

const errorHandler: hapi.Lifecycle.Method = (
  _req: hapi.Request,
  res: hapi.ResponseToolkit,
  err?: Error
) => {
  if (!err) {
    return res.continue;
  }
  if ((err as ValidationError).isJoi) {
    const details = (err as ValidationError).details;
    const mappedDetails = getMappedDetails(details);
    logger.error(
      `errorHandler: invalid request ${JSON.stringify(mappedDetails)}`
    );
    throw new TransferAppError(
      `Invalid request detected in error handler ${JSON.stringify(
        mappedDetails
      )}`,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.INVALID_REQUEST,
      mappedDetails
    );
  }

  throw err;
};

export default errorHandler;
