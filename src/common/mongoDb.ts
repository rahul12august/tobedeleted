import { connect, connection } from 'mongoose';
import { config } from '@dk/module-config';
import logger from '../logger';
import { Util } from '@dk/module-common';

export const MONGO_USER_PARAM = 'MONGO_USER';
export const MONGO_PASS_PARAM = 'MONGO_PASS';
export const MONGO_DB_NAME = 'MONGO_DB_NAME';
export const MONGO_HOSTS = 'MONGO_HOSTS';

export const connectMongo = () =>
  new Promise<void>((resolve, reject) => {
    const user = config.get(MONGO_USER_PARAM);
    const pass = process.env.MONGO_PASSWORD || '';
    const dbName = config.get(MONGO_DB_NAME);
    const dbHosts = config.get(MONGO_HOSTS);
    logger.info(`MongoDb URL : ${dbHosts}`);
    const dbUri = Util.getMongoUri(user, pass, dbName, dbHosts, '');
    if (!dbName) {
      logger.error(`No ${MONGO_DB_NAME} is provided`);
      return reject(`No ${MONGO_DB_NAME} is provided`);
    }

    connection.once('open', () => resolve());

    connection.on('error', err => {
      logger.error('error while connecting to mongodb', err);
      reject(err);
    });

    connect(dbUri, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      user,
      pass,
      dbName
    });
  });
