export interface IAbuseDetectionConfig {
  readonly paymentServiceCode: string;
  readonly maxUniqueDestinations: number;
  readonly timeframe: number;
  readonly cooldown: number;
}
