import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import { SinBinModel } from '../sinbin.model';
import sinbinRepository from '../sinbin.repository';
import moment from 'moment';

jest.mock('mongoose', () => {
  const mongoose = jest.requireActual('mongoose');
  return new mongoose.Mongoose(); // new mongoose instance and connection for each test
});

const customerId = 'cust1234';
const paymentServiceCode = 'IRIS_JAGO_TO_OTH';
const trxId = 'trx5678';

describe('sinbin repository', () => {
  let mongod: MongoMemoryServer;
  beforeAll(async () => {
    mongod = new MongoMemoryServer();
    const mongoDbUri = await mongod.getConnectionString();
    await mongoose.connect(mongoDbUri, {
      useNewUrlParser: true,
      useCreateIndex: true
    });
  });

  beforeEach(() => {
    jest.clearAllMocks();
    Date.now = jest.fn().mockReturnValue(new Date('2023-02-10T08:03:04.169Z'));
  });

  afterEach(async () => {
    expect.hasAssertions();
    await SinBinModel.deleteMany({});
    jest.clearAllMocks();
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongod.stop();
  });

  describe('putInSinBin()', () => {
    it('adds the user to the SinBin with requested cooldown', async () => {
      await sinbinRepository.putInSinBin(
        customerId,
        paymentServiceCode,
        24,
        trxId
      );
      const saved = await SinBinModel.findOne();

      expect(saved?.customerId).toEqual(customerId);
      expect(saved?.paymentServiceCode).toEqual(paymentServiceCode);
      expect(saved?.offendingTransactionId).toEqual(trxId);
      const expectedCooldown = moment()
        .add(24, 'hours')
        .toDate();
      expect(saved?.cooldownExpiry).toEqual(expectedCooldown);
    });

    it('adds the user to the SinBin with requested cooldown without offendingTransactionId', async () => {
      await sinbinRepository.putInSinBin(
        customerId,
        paymentServiceCode,
        24,
        undefined
      );
      const saved = await SinBinModel.findOne();

      expect(saved?.customerId).toEqual(customerId);
      expect(saved?.paymentServiceCode).toEqual(paymentServiceCode);
      expect(saved?.offendingTransactionId).toBeUndefined();
      const expectedCooldown = moment()
        .add(24, 'hours')
        .toDate();
      expect(saved?.cooldownExpiry).toEqual(expectedCooldown);
    });
  });

  describe('isInSinBin', () => {
    it('says no, when no entry exists', async () => {
      expect(
        await sinbinRepository.isInSinBin(customerId, paymentServiceCode)
      ).toBeFalsy();
    });

    it('says yes, when future dated entry exists', async () => {
      await new SinBinModel({
        customerId,
        paymentServiceCode,
        cooldownExpiry: moment()
          .add(1, 'second')
          .toDate()
      }).save();
      expect(
        await sinbinRepository.isInSinBin(customerId, paymentServiceCode)
      ).toBeTruthy();
    });

    it('says no, when only past dated entry exists', async () => {
      await new SinBinModel({
        customerId,
        paymentServiceCode,
        cooldownExpiry: moment()
          .subtract(1, 'second')
          .toDate()
      }).save();
      expect(
        await sinbinRepository.isInSinBin(customerId, paymentServiceCode)
      ).toBeFalsy();
    });

    it('says no, when future dated entry exists for different paymentServiceCode', async () => {
      await new SinBinModel({
        customerId,
        paymentServiceCode: 'SOME_OTHER_CODE',
        cooldownExpiry: moment()
          .add(1, 'hour')
          .toDate()
      }).save();
      expect(
        await sinbinRepository.isInSinBin(customerId, paymentServiceCode)
      ).toBeFalsy();
    });
  });
});
