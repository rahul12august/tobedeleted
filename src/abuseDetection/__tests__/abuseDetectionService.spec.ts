import { isSuspectedAbuse } from '../abuseDetectionService';
import transactionRepo from '../../transaction/transaction.repository';
import sinbinRepo from '../sinbin.repository';
import moment from 'moment';

jest.mock('../../transaction/transaction.repository');
jest.mock('../sinbin.repository');

const getPrevTrx = (countUnique: number) => {
  const prevTrx = [];
  for (let i = 0; i < countUnique; i++) {
    prevTrx.push({
      beneficiaryAccountNo: `beneficiary-${i}`
    });
  }
  return prevTrx;
};

describe('abuseDetectionService', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    Date.now = jest.fn().mockReturnValue(new Date('2023-02-10T08:03:04.169Z'));
    (sinbinRepo.isInSinBin as jest.Mock).mockResolvedValue(false);
  });
  describe('not abusable transaction', () => {
    it('does not get analysed for abuse', async () => {
      expect(
        await isSuspectedAbuse(
          'SAFE_TRANSACTION',
          'custId-1234',
          'beneficiary-5678',
          'trx-91011'
        )
      ).toBeFalsy();
      expect(transactionRepo.findTransactionByQuery).not.toHaveBeenCalled();
      expect(sinbinRepo.putInSinBin).not.toHaveBeenCalled();
    });
  });

  describe('abusable transaction', () => {
    it('allows if no previous transactions in range', async () => {
      const searchTime = moment().subtract(60, 'minutes');
      (transactionRepo.findTransactionByQuery as jest.Mock).mockResolvedValue(
        []
      );
      expect(
        await isSuspectedAbuse(
          'IRIS_JAGO_TO_OTH',
          'custId-1234',
          'beneficiary-5678',
          'trx-91011'
        )
      ).toBeFalsy();
      // just doing this on the one test, to ensure the date range is set correctly
      expect(transactionRepo.findTransactionByQuery).toHaveBeenCalledWith(
        {
          paymentServiceCode: 'IRIS_JAGO_TO_OTH',
          ownerCustomerId: 'custId-1234',
          createdAt: { $gte: searchTime.toDate() }
        },
        {
          beneficiaryAccountNo: 1
        }
      );
      expect(sinbinRepo.putInSinBin).not.toHaveBeenCalled();
    });

    it('prevents if acceptable request made while user in SinBin', async () => {
      (transactionRepo.findTransactionByQuery as jest.Mock).mockResolvedValue(
        []
      );
      (sinbinRepo.isInSinBin as jest.Mock).mockResolvedValue(true);
      expect(
        await isSuspectedAbuse(
          'IRIS_JAGO_TO_OTH',
          'custId-1234',
          'beneficiary-5678',
          'trx-91011'
        )
      ).toBeTruthy();
      expect(sinbinRepo.isInSinBin).toHaveBeenCalledWith(
        'custId-1234',
        'IRIS_JAGO_TO_OTH'
      );
    });

    it('prevents and puts in sinbin if too many previous transactions with too many destinations', async () => {
      (transactionRepo.findTransactionByQuery as jest.Mock).mockResolvedValue(
        getPrevTrx(11)
      );
      expect(
        await isSuspectedAbuse(
          'IRIS_JAGO_TO_OTH',
          'custId-1234',
          'beneficiary-5678',
          'trx-91011'
        )
      ).toBeTruthy();
      expect(sinbinRepo.putInSinBin).toHaveBeenCalledWith(
        'custId-1234',
        'IRIS_JAGO_TO_OTH',
        24,
        'trx-91011'
      );
    });

    it('allows if many previous transactions with acceptable destinations', async () => {
      (transactionRepo.findTransactionByQuery as jest.Mock).mockResolvedValue(
        getPrevTrx(5)
      );
      expect(
        await isSuspectedAbuse(
          'IRIS_JAGO_TO_OTH',
          'custId-1234',
          'beneficiary-5678',
          'trx-91011'
        )
      ).toBeFalsy();
      expect(sinbinRepo.putInSinBin).not.toHaveBeenCalled();
    });

    it('allows if destinations in previous transactions have exact destination count and new destination included', async () => {
      const prevTrx = getPrevTrx(9);
      prevTrx.push(...getPrevTrx(9));
      prevTrx.push({
        beneficiaryAccountNo: `beneficiary-5678`
      });

      (transactionRepo.findTransactionByQuery as jest.Mock).mockResolvedValue(
        prevTrx
      );
      expect(
        await isSuspectedAbuse(
          'IRIS_JAGO_TO_OTH',
          'custId-1234',
          'beneficiary-5678',
          'trx-91011'
        )
      ).toBeFalsy();
      expect(sinbinRepo.putInSinBin).not.toHaveBeenCalled();
    });

    it('prevents if destinations in previous transactions have exact destination count and new destination excluded', async () => {
      (transactionRepo.findTransactionByQuery as jest.Mock).mockResolvedValue(
        getPrevTrx(10)
      );
      expect(
        await isSuspectedAbuse(
          'IRIS_JAGO_TO_OTH',
          'custId-1234',
          'beneficiary-5678',
          'trx-91011'
        )
      ).toBeTruthy();
      expect(sinbinRepo.putInSinBin).toHaveBeenCalledWith(
        'custId-1234',
        'IRIS_JAGO_TO_OTH',
        24,
        'trx-91011'
      );
    });
  });
});
