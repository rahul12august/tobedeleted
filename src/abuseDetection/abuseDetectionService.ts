import logger from '../logger';
import { config } from '../config';
import { IAbuseDetectionConfig } from './abuseDetection.type';
import transactionRepository from '../transaction/transaction.repository';
import sinbinRepository from './sinbin.repository';
import moment from 'moment';

const abuseConfig = config.get('abuseDetection') as IAbuseDetectionConfig[];
const configMap = new Map<string, IAbuseDetectionConfig>();
abuseConfig.forEach(item => {
  configMap.set(item.paymentServiceCode, item);
});

const previousTransactionProjection = {
  beneficiaryAccountNo: 1
};

export const isSuspectedAbuse = async (
  paymentServiceCode: string,
  ownerCustomerId: string,
  beneficiaryAccountNo: string,
  transactionId?: string
) => {
  const config = configMap.get(paymentServiceCode);

  if (!config) {
    logger.info(
      `Abuse check - skip :: customerId : ${ownerCustomerId} , paymentServiceCode : ${paymentServiceCode} , transactionId : ${transactionId}`,
      {
        ownerCustomerId,
        paymentServiceCode,
        beneficiaryAccountNo,
        transactionId
      }
    );
    return false;
  }

  const searchTime = moment().subtract(config.timeframe, 'minutes');

  const prevTransactions = await transactionRepository.findTransactionByQuery(
    {
      paymentServiceCode,
      ownerCustomerId,
      createdAt: { $gte: searchTime.toDate() }
    },
    previousTransactionProjection
  );

  const destinations = new Set(
    prevTransactions.map(i => i.beneficiaryAccountNo)
  );
  destinations.add(beneficiaryAccountNo);

  if (destinations.size > config.maxUniqueDestinations) {
    await sinbinRepository.putInSinBin(
      ownerCustomerId,
      paymentServiceCode,
      config.cooldown,
      transactionId
    );
    logger.info(
      `Abuse check - blocked :: customerId : ${ownerCustomerId} , paymentServiceCode : ${paymentServiceCode} , transactionId : ${transactionId} , beneficiaryAccountNo : ${beneficiaryAccountNo}`,
      {
        ownerCustomerId,
        paymentServiceCode,
        beneficiaryAccountNo,
        transactionId,
        uniqueDestinationsInRange: destinations.size,
        maxAllowedUniqueDestinations: config.maxUniqueDestinations,
        timeRangeInMinutes: config.timeframe
      }
    );
    return true;
  }

  if (await sinbinRepository.isInSinBin(ownerCustomerId, paymentServiceCode)) {
    logger.info(
      `Abuse check - on cooldown :: customerId : ${ownerCustomerId} , paymentServiceCode : ${paymentServiceCode} , transactionId : ${transactionId} , beneficiaryAccountNo : ${beneficiaryAccountNo}`,
      {
        ownerCustomerId,
        paymentServiceCode,
        beneficiaryAccountNo,
        transactionId,
        uniqueDestinationsInRange: destinations.size,
        maxAllowedUniqueDestinations: config.maxUniqueDestinations,
        timeRangeInMinutes: config.timeframe
      }
    );
    return true;
  }
  logger.info(
    `Abuse check - ok :: customerId : ${ownerCustomerId} , paymentServiceCode : ${paymentServiceCode} , transactionId : ${transactionId} , beneficiaryAccountNo : ${beneficiaryAccountNo}`,
    {
      ownerCustomerId,
      paymentServiceCode,
      beneficiaryAccountNo,
      transactionId,
      uniqueDestinationsInRange: destinations.size,
      maxAllowedUniqueDestinations: config.maxUniqueDestinations,
      timeRangeInMinutes: config.timeframe
    }
  );
  return false;
};
