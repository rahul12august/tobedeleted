import mongoose, { Schema, Model, Document } from 'mongoose';
import { IBaseModel } from '../common/baseModel.type';

interface ISinBinModel extends IBaseModel {
  customerId: string;
  paymentServiceCode: string;
  cooldownExpiry: Date;
}

type SinBinDocument = ISinBinModel & Document;

export const collectionName = 'sinbin';

const SinBinSchema = new Schema(
  {
    customerId: {
      type: String,
      required: true
    },
    paymentServiceCode: {
      type: String,
      required: true
    },
    cooldownExpiry: {
      type: Date,
      required: true
    },
    offendingTransactionId: {
      type: String,
      required: false
    }
  },
  {
    timestamps: true
  }
);

SinBinSchema.index({ customerId: 1, paymentServiceCode: 1, cooldownExpiry: 1 });

export const SinBinModel: Model<SinBinDocument> = mongoose.model(
  collectionName,
  SinBinSchema
);
