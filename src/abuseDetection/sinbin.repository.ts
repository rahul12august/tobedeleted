import logger, { wrapLogs } from '../logger';
import moment from 'moment';
import { SinBinModel } from './sinbin.model';

const putInSinBin = async (
  customerId: string,
  paymentServiceCode: string,
  cooldownDurationHours: number,
  offendingTransactionId?: string
) => {
  const cooldownExpiry = moment().add(cooldownDurationHours, 'hours');
  const savedEntry = await new SinBinModel({
    customerId,
    paymentServiceCode,
    cooldownExpiry,
    offendingTransactionId
  }).save();
  logger.info(
    `Added to Sin Bin :: customerId : ${customerId} , paymentServiceCode : ${paymentServiceCode} , cooldownExpiry : ${cooldownExpiry} , offendingTransactionId : ${offendingTransactionId}`,
    savedEntry
  );
};

const isInSinBin = async (
  customerId: string,
  paymentServiceCode: string
): Promise<boolean> => {
  return await SinBinModel.exists({
    customerId,
    paymentServiceCode,
    cooldownExpiry: { $gte: moment().toDate() }
  });
};

export default wrapLogs({
  putInSinBin,
  isInSinBin
});
