import mongoose, { Schema, Model, Document } from 'mongoose';
import { IBaseModel } from '../common/baseModel.type';

import { handleDuplicationError } from '../common/mongoose.util';

export const collectionName = 'transaction_usages';
export type TransactionUsageDocument = ITransactionUsageModel & Document;

export interface ITransactionUsageModel extends IBaseModel {
  customerId: string;
  accountNo: string;
  dailyUsages: IDailyUsages;
}

export interface IDailyUsages {
  atmWithdrawal?: IAtmWithdrawalDailyUsageInfo;
  atmTransfer?: IAtmTransferDailyUsageInfo;
}

export interface IAtmWithdrawalDailyUsageInfo {
  amount: number;
  lastUpdatedAt: Date;
}

export interface IAtmTransferDailyUsageInfo {
  amount: number;
  lastUpdatedAt: Date;
}

const ATMWithdrawalDailyUsageSubSchema = new Schema(
  {
    amount: {
      type: Number,
      required: true
    },
    lastUpdatedAt: {
      type: Date,
      required: true
    }
  },
  { _id: false }
);

const ATMTransferDailyUsageSubSchema = new Schema(
  {
    amount: {
      type: Number,
      required: true
    },
    lastUpdatedAt: {
      type: Date,
      required: true
    }
  },
  { _id: false }
);

const DailyUsageSubSchema = new Schema(
  {
    atmWithdrawal: {
      type: ATMWithdrawalDailyUsageSubSchema,
      required: false
    },
    atmTransfer: {
      type: ATMTransferDailyUsageSubSchema,
      required: false
    }
  },
  { _id: false }
);

const TransactionUsageSchema = new Schema({
  customerId: {
    type: String,
    required: true
  },
  accountNo: {
    type: String,
    required: true
  },
  dailyUsages: {
    type: DailyUsageSubSchema,
    required: true
  }
});

TransactionUsageSchema.index({ customerId: 1, accountNo: 1 }, { unique: true });
TransactionUsageSchema.post('save', handleDuplicationError);
TransactionUsageSchema.post('find', handleDuplicationError);

export const TransactionUsageModel: Model<TransactionUsageDocument> = mongoose.model(
  collectionName,
  TransactionUsageSchema
);
