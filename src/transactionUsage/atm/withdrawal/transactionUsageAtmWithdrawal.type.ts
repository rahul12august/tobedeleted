import { PaymentServiceTypeEnum } from '@dk/module-common';
import { ILimitGroup } from '../../../configuration/configuration.type';
import { RuleConditionStatus } from '../../../transaction/transaction.type';
import { IDailyUsageParamValidatedResponse } from '../../transactionUsage.type';

export interface IAtmWithdrawalDailyUsageParamValidatedResponse {
  customerId: string;
  accountNo: string;
  debitTransactionCode: string;
  debitLimitGroupCode: string;
  limitGroupInfo: ILimitGroup;
  transactionAmount: number;
  paymentServiceType: PaymentServiceTypeEnum;
  paymentServiceCode: string;
}

export interface IAtmWithdrawalDailyUsageProcessAcceptedStateRequest {
  validatedParams: IDailyUsageParamValidatedResponse;
  dailyAmountSoFar: number;
}

export interface IAtmWithdrawalDailyUsageProcessRejectedStateRequest {
  evalStatus: RuleConditionStatus;
  validatedParams: IDailyUsageParamValidatedResponse;
  dailyAmountSoFar: number;
}
