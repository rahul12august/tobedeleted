import { ErrorList, ERROR_CODE } from '../../../common/errors';
import { AppError, RetryableTransferAppError } from '../../../errors/AppError';
import logger from '../../../logger';
import { IATMWithdrawalExceedsDailyLimitMessage } from '../../../transaction/transaction.producer.type';

import { ITransactionUsageHandler } from '../../transactionUsageHandler';
import transactionUsageRepoService from '../../transactionUsageRepo.service';
import {
  IAtmWithdrawalDailyUsageParamValidatedResponse,
  IAtmWithdrawalDailyUsageProcessAcceptedStateRequest,
  IAtmWithdrawalDailyUsageProcessRejectedStateRequest
} from './transactionUsageAtmWithdrawal.type';
import _ from 'lodash';
import {
  IDailyUsageParamValidatedResponse,
  IEvalStatusError,
  IPocketLimitValidationRequestExt,
  ITransactionUsageSaveContextRequest,
  TransactionUsageContext
} from '../../transactionUsage.type';
import {
  getTransactionUsageInfoTracker,
  setTransactionUsageInfoTracker
} from '../../../common/contextHandler';
import {
  TransactionUsageCategory,
  TransactionUsageGroupCode
} from '../../transactionUsage.enum';
import transactionUsageCommonService from '../../transactionUsageCommon.service';
import transactionProducer from '../../../transaction/transaction.producer';
import { NotificationCode } from '../../../transaction/transaction.producer.enum';
import { TransferFailureReasonActor } from '../../../transaction/transaction.enum';
import accountRepository from '../../../account/account.repository';
import { ITransactionUsageModel } from '../../transactionUsage.model';
import { isFeatureEnabled } from '../../../common/featureFlag';
import { TRANSACTION_CATEGORY_ATM_WITHDRAWAL } from '../../../transaction/transaction.category.enum';
import { FEATURE_FLAG } from '../../../common/constant';

export class TransactionUsageAtmWithdrawalHandler
  implements ITransactionUsageHandler {
  isFeatureFlagEnabled(): boolean {
    return isFeatureEnabled(
      FEATURE_FLAG.ENABLE_DAILY_LIMIT_ATM_WITHDRAWAL_POCKET_LIMIT_VALIDATION
    );
  }

  getEvalStatusErrors(): IEvalStatusError {
    return {
      errorCodeDeclined:
        ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT,
      errorCodeTryTomorrow:
        ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT
    };
  }

  getTransactionUsageGroupCode(): TransactionUsageGroupCode {
    return TransactionUsageGroupCode.ATM_WITHDRAWAL;
  }

  getTransactionUsageCategory(): TransactionUsageCategory {
    return TransactionUsageCategory.ATM_WITHDRAWAL;
  }

  isValidTransactionCategoryByRecommendedService(
    validatedParams: IDailyUsageParamValidatedResponse
  ): boolean {
    //extract validated params
    const {
      debitLimitGroupCode: limitGroupCode,
      paymentServiceType,
      paymentServiceCode
    } = validatedParams;

    //validate the transaction's category whether it is ATM Withdrawal
    logger.info(`[isValidTransactionCategoryByRecommendedService]: 
      paymentServiceType: ${paymentServiceType}
      paymentServiceCode: ${paymentServiceCode}
      limitGroupCode: ${limitGroupCode}
    `);

    for (const metadata of TRANSACTION_CATEGORY_ATM_WITHDRAWAL) {
      if (
        metadata.paymentServiceType === paymentServiceType &&
        metadata.paymentServiceCode === paymentServiceCode &&
        metadata.limitGroupCode === limitGroupCode
      ) {
        return true;
      }
    }

    logger.info(
      `[isValidTransactionCategoryByRecommendedService] not ATM Withdrawal category`
    );
    return false;
  }

  validateDailyUsageParams(
    params: IPocketLimitValidationRequestExt
  ): IAtmWithdrawalDailyUsageParamValidatedResponse {
    const {
      customerId,
      accountNo,
      transactionAmount,
      paymentServiceType,
      recommendedService,
      limitGroupConfigs,
      transactionInterchange
    } = params;

    //validate customerId and accountNo must be EXIST
    if (_.isNil(customerId) || _.isNil(accountNo)) {
      throw new AppError(
        ERROR_CODE.ATM_WITHDRAWAL_EXECUTOR_MANDATORY_INFO_NOT_FOUND
      );
    }

    let debitTransactionCodes = recommendedService.debitTransactionCode;

    if (_.isNil(debitTransactionCodes) || _.isEmpty(debitTransactionCodes)) {
      throw new AppError(ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE);
    }

    const mappingInfoMatched = transactionUsageCommonService.extractInfoFromTransactionCodeMappings(
      {
        transactionCodesMappingInfo: debitTransactionCodes,
        currentTransactionInterchange: transactionInterchange
      }
    );

    let debitLimitGroupCode = mappingInfoMatched.limitGroupCode;
    let debitTransactionCode = mappingInfoMatched.transactionCode;

    const paymentServiceCode = recommendedService.paymentServiceCode;

    const limitGroupInfo = transactionUsageCommonService.getLimitGroupCodeInfo(
      this.getTransactionUsageGroupCode(),
      limitGroupConfigs
    );

    return {
      customerId,
      accountNo,
      transactionAmount,
      paymentServiceType,
      debitLimitGroupCode,
      debitTransactionCode,
      limitGroupInfo,
      paymentServiceCode
    };
  }

  getDailyLimitAmountSoFar = (
    existingTransactionUsage: ITransactionUsageModel | null
  ) => {
    //1. check if existing transactionUsage record is null
    if (
      existingTransactionUsage == null ||
      _.isUndefined(existingTransactionUsage.dailyUsages.atmWithdrawal)
    ) {
      return 0;
    }

    const {
      amount,
      lastUpdatedAt
    } = existingTransactionUsage.dailyUsages.atmWithdrawal;

    //2. check whether the record is expired (if it is in the past)
    const isExpired = transactionUsageCommonService.isDailyLimitExpired(
      lastUpdatedAt
    );

    if (isExpired) {
      //if expired, the dailyUsages should reset back to 0
      return 0;
    }

    //3. return latest dailyUsages amount
    return amount;
  };

  saveDailyLimitToContext(params: ITransactionUsageSaveContextRequest): void {
    const {
      customerId,
      accountNo,
      transactionAmount,
      transactionUsageCategory,
      paymentServiceCode,
      limitGroupCode
    } = params;

    if (
      _.isUndefined(customerId) ||
      _.isEmpty(customerId) ||
      _.isUndefined(accountNo) ||
      _.isEmpty(accountNo)
    ) {
      return;
    }

    const isSameTransactionUsageCategoryAsHandler =
      transactionUsageCategory === this.getTransactionUsageCategory();
    if (isSameTransactionUsageCategoryAsHandler) {
      //write it to context
      setTransactionUsageInfoTracker({
        customerId: customerId,
        accountNo: accountNo,
        transactionCategory: transactionUsageCategory,
        currentTransactionAmount: transactionAmount,
        paymentServiceCode: paymentServiceCode,
        limitGroupCode: limitGroupCode
      });
    }
  }

  async processAcceptedState(
    params: IAtmWithdrawalDailyUsageProcessAcceptedStateRequest
  ): Promise<void> {
    const { validatedParams, dailyAmountSoFar } = params;

    const {
      customerId,
      accountNo,
      transactionAmount,
      paymentServiceCode,
      debitLimitGroupCode
    } = validatedParams;

    const expectedAmountAfterUpdate = dailyAmountSoFar + transactionAmount;

    logger.debug(`[processAcceptedState] 
      customerId: ${customerId}
      accountNo: ${accountNo}
      dailyAmountSumSoFar: ${dailyAmountSoFar}
      expectedAmountAfterUpdate: ${expectedAmountAfterUpdate},
      paymentServiceCode: ${paymentServiceCode}
      debitLimitGroupCode: ${debitLimitGroupCode}
    `);

    //update  or insert new to db (Directly consume "quota")
    const transactionUsageRecord = await transactionUsageRepoService.insertOrUpdateDailyUsage(
      customerId,
      accountNo,
      expectedAmountAfterUpdate,
      TransactionUsageCategory.ATM_WITHDRAWAL
    );

    if (transactionUsageRecord == null) {
      throw new AppError(ERROR_CODE.FAILED_TO_SAVE_UPDATE_TRANSACTION_USAGE);
    }

    //save to context
    this.saveDailyLimitToContext({
      customerId: customerId,
      accountNo: accountNo,
      transactionAmount: transactionAmount,
      transactionUsageCategory: this.getTransactionUsageCategory(),
      paymentServiceCode,
      limitGroupCode: debitLimitGroupCode
    });
  }

  private async constructNotificationMessageParams(
    validatedParams: IDailyUsageParamValidatedResponse,
    dailyAmountSoFar: number
  ): Promise<IATMWithdrawalExceedsDailyLimitMessage> {
    const { customerId, accountNo, limitGroupInfo } = validatedParams;

    const accountDetail = await accountRepository.getAccountInfo(
      validatedParams.accountNo
    );

    return {
      customerId: customerId,
      pocketName: accountDetail?.aliasName ?? accountNo,
      amount: limitGroupInfo.dailyLimitAmount - dailyAmountSoFar
    };
  }

  async processRejectedState(
    params: IAtmWithdrawalDailyUsageProcessRejectedStateRequest
  ): Promise<void> {
    const { validatedParams, dailyAmountSoFar, evalStatus } = params;

    const { customerId, accountNo } = validatedParams;

    logger.debug(`[processRejectedState] 
    customerId: ${customerId}
    accountNo: ${accountNo}
    dailyAmountSumSoFar: ${dailyAmountSoFar}
  `);

    let errorCode = evalStatus.errorCode;
    if (_.isNil(errorCode)) {
      //change to generic error code
      errorCode =
        ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT;
    }

    logger.error(`[processRejectedState]: ${evalStatus.message}`);

    const notifMessage = await this.constructNotificationMessageParams(
      validatedParams,
      dailyAmountSoFar
    );

    //send notification
    await transactionProducer.sendFailedCashWithdrawalDailyLimit(
      notifMessage,
      NotificationCode.NOTIF_DAILY_LIMIT_ATM_WITHDRAWAL
    );

    //then throw error
    throw new RetryableTransferAppError(
      evalStatus.message,
      TransferFailureReasonActor.MS_TRANSFER,
      errorCode,
      false,
      [
        {
          message: ErrorList[errorCode].message,
          key: errorCode,
          code: errorCode
        }
      ]
    );
  }

  async processReversalBasedOnContext(): Promise<void> {
    logger.info(`Processing reversal for ATM Withdrawal Limit`);

    const transactionUsageInfo:
      | TransactionUsageContext
      | undefined = getTransactionUsageInfoTracker();

    //if the info does not exist in context
    if (_.isNil(transactionUsageInfo)) {
      return;
    }

    const isTransactionCategoryValid =
      transactionUsageInfo.transactionCategory ==
      this.getTransactionUsageCategory();

    //if the transaction category does not match ATM withdrawal, skip it
    if (!isTransactionCategoryValid) {
      return;
    }

    const {
      customerId,
      accountNo,
      currentTransactionAmount
    } = transactionUsageInfo;

    logger.info(
      `[processReversalBasedOnContext] atm withdrawal - checking if the dailyAmountSumSoFar should be set to 0`
    );
    let updatedTransLimit = await transactionUsageRepoService.revertDailyUsageToZero(
      customerId,
      accountNo,
      currentTransactionAmount,
      TransactionUsageCategory.ATM_WITHDRAWAL
    );

    if (updatedTransLimit == null) {
      logger.info(
        `[processReversalBasedOnContext] atm withdrawal - checking if the dailyAmountSumSoFar should NOT be set to 0, but 
            just perform revert the atm-withdrawal amount`
      );

      updatedTransLimit = await transactionUsageRepoService.revertDailyUsage(
        customerId,
        accountNo,
        currentTransactionAmount,
        TransactionUsageCategory.ATM_WITHDRAWAL
      );
    }

    if (
      !_.isNull(updatedTransLimit) &&
      !_.isUndefined(updatedTransLimit.dailyUsages.atmWithdrawal)
    ) {
      logger.debug(`[ATM WITHDRAWAL LIMIT REVERSAL] success: 
            customerId : ${updatedTransLimit.customerId}
            accountNo: ${updatedTransLimit.accountNo}
            currentAccumulatedAmount: ${updatedTransLimit.dailyUsages.atmWithdrawal.amount}
          `);
    }
  }
}
