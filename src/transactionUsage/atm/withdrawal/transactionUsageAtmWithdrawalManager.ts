import { ITransactionUsageHandler } from '../../transactionUsageHandler';
import { TransactionUsageManager } from '../../transactionUsageManager';
import { TransactionUsageAtmWithdrawalHandler } from './transactionUsageAtmWithdrawalHandler';

export class TransactionUsageAtmWithdrawalManager extends TransactionUsageManager {
  constructor(
    transactionUsageHandler: ITransactionUsageHandler = new TransactionUsageAtmWithdrawalHandler()
  ) {
    super();
    super.setTransactionUsageHandlerInstance(transactionUsageHandler);
  }
}
