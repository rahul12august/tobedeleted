import { ITransactionUsageHandler } from '../../transactionUsageHandler';
import { TransactionUsageManager } from '../../transactionUsageManager';
import { TransactionUsageAtmTransferHandler } from './transactionUsageAtmTransferHandler';

export class TransactionUsageAtmTransferManager extends TransactionUsageManager {
  constructor(
    transactionUsageHandler: ITransactionUsageHandler = new TransactionUsageAtmTransferHandler()
  ) {
    super();
    super.setTransactionUsageHandlerInstance(transactionUsageHandler);
  }
}
