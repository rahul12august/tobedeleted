import { ErrorList, ERROR_CODE } from '../../../common/errors';
import { isFeatureEnabled } from '../../../common/featureFlag';
import logger from '../../../logger';
import { TRANSACTION_CATEGORY_ATM_TRANSFER } from '../../../transaction/transaction.category.enum';
import {
  TransactionUsageCategory,
  TransactionUsageGroupCode
} from '../../transactionUsage.enum';
import {
  IDailyUsageParamValidatedResponse,
  IEvalStatusError,
  IPocketLimitValidationRequestExt,
  ITransactionUsageSaveContextRequest,
  TransactionUsageContext
} from '../../transactionUsage.type';
import { ITransactionUsageHandler } from '../../transactionUsageHandler';
import _ from 'lodash';
import { ITransactionUsageModel } from '../../transactionUsage.model';
import transactionUsageCommonService from '../../transactionUsageCommon.service';
import {
  IAtmTransferDailyUsageProcessRejectedStateRequest,
  IAtmTransferDailyUsageProcessAcceptedStateRequest,
  IAtmTransferDailyUsageParamValidatedResponse
} from './transactionUsageAtmTransfer.type';
import transactionUsageRepoService from '../../transactionUsageRepo.service';
import { AppError, RetryableTransferAppError } from '../../../errors/AppError';
import { IATMTransferExceedsDailyLimitMessage } from '../../../transaction/transaction.producer.type';
import transactionProducer from '../../../transaction/transaction.producer';
import { NotificationCode } from '../../../transaction/transaction.producer.enum';
import { TransferFailureReasonActor } from '../../../transaction/transaction.enum';
import {
  getTransactionUsageInfoTracker,
  setTransactionUsageInfoTracker
} from '../../../common/contextHandler';
import { FEATURE_FLAG } from '../../../common/constant';

export class TransactionUsageAtmTransferHandler
  implements ITransactionUsageHandler {
  isFeatureFlagEnabled(): boolean {
    return isFeatureEnabled(
      FEATURE_FLAG.ENABLE_DAILY_LIMIT_ATM_TRANSFER_POCKET_LIMIT_VALIDATION
    );
  }

  getEvalStatusErrors(): IEvalStatusError {
    return {
      errorCodeDeclined: ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT,
      errorCodeTryTomorrow: ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT
    };
  }

  getTransactionUsageGroupCode(): TransactionUsageGroupCode {
    return TransactionUsageGroupCode.ATM_TRANSFER;
  }

  getTransactionUsageCategory(): TransactionUsageCategory {
    return TransactionUsageCategory.ATM_TRANSFER;
  }

  isValidTransactionCategoryByRecommendedService(
    validatedParams: IDailyUsageParamValidatedResponse
  ): boolean {
    //extract validated params
    const {
      debitLimitGroupCode: limitGroupCode,
      paymentServiceType,
      paymentServiceCode
    } = validatedParams;

    //validate the transaction's category whether it is ATM TRANSFER
    logger.info(`[isValidTransactionCategoryByRecommendedService]: 
        paymentServiceType: ${paymentServiceType}
        paymentServiceCode: ${paymentServiceCode}
        limitGroupCode: ${limitGroupCode}
      `);

    for (const metadata of TRANSACTION_CATEGORY_ATM_TRANSFER) {
      if (
        metadata.paymentServiceType === paymentServiceType &&
        metadata.paymentServiceCode === paymentServiceCode &&
        metadata.limitGroupCode === limitGroupCode
      ) {
        return true;
      }
    }

    logger.info(
      `[isValidTransactionCategoryByRecommendedService] not ATM TRANSFER  category`
    );
    return false;
  }

  validateDailyUsageParams(
    params: IPocketLimitValidationRequestExt
  ): IAtmTransferDailyUsageParamValidatedResponse {
    const {
      customerId,
      accountNo,
      beneficiaryAccountNo,
      beneficiaryAccountName,
      transactionAmount,
      paymentServiceType,
      recommendedService,
      limitGroupConfigs,
      transactionInterchange
    } = params;

    //validate customerId and accountNo must be EXIST
    if (
      _.isNil(customerId) ||
      _.isNil(accountNo) ||
      _.isNil(beneficiaryAccountNo) ||
      _.isNil(beneficiaryAccountName)
    ) {
      throw new AppError(
        ERROR_CODE.ATM_TRANSFER_EXECUTOR_MANDATORY_INFO_NOT_FOUND
      );
    }

    let debitTransactionCodes = recommendedService.debitTransactionCode;

    if (_.isNil(debitTransactionCodes) || _.isEmpty(debitTransactionCodes)) {
      throw new AppError(ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE);
    }

    const mappingInfoMatched = transactionUsageCommonService.extractInfoFromTransactionCodeMappings(
      {
        transactionCodesMappingInfo: debitTransactionCodes,
        currentTransactionInterchange: transactionInterchange
      }
    );

    let debitLimitGroupCode = mappingInfoMatched.limitGroupCode;
    let debitTransactionCode = mappingInfoMatched.transactionCode;

    const paymentServiceCode = recommendedService.paymentServiceCode;

    const limitGroupInfo = transactionUsageCommonService.getLimitGroupCodeInfo(
      this.getTransactionUsageGroupCode(),
      limitGroupConfigs
    );

    return {
      customerId,
      accountNo,
      beneficiaryAccountNo,
      beneficiaryAccountName,
      transactionAmount,
      paymentServiceType,
      debitLimitGroupCode,
      debitTransactionCode,
      limitGroupInfo,
      paymentServiceCode
    };
  }

  getDailyLimitAmountSoFar = (
    existingTransactionUsage: ITransactionUsageModel | null
  ) => {
    //1. check if existing transactionUsage record is null
    if (
      existingTransactionUsage == null ||
      _.isUndefined(existingTransactionUsage.dailyUsages.atmTransfer)
    ) {
      return 0;
    }

    const {
      amount,
      lastUpdatedAt
    } = existingTransactionUsage.dailyUsages.atmTransfer;

    //2. check whether the record is expired (if it is in the past)
    const isExpired = transactionUsageCommonService.isDailyLimitExpired(
      lastUpdatedAt
    );

    if (isExpired) {
      //if expired, the dailyUsages should reset back to 0
      return 0;
    }

    //3. return latest dailyUsages amount
    return amount;
  };

  saveDailyLimitToContext(params: ITransactionUsageSaveContextRequest): void {
    const {
      customerId,
      accountNo,
      transactionAmount,
      transactionUsageCategory,
      paymentServiceCode,
      limitGroupCode
    } = params;

    if (
      _.isUndefined(customerId) ||
      _.isEmpty(customerId) ||
      _.isUndefined(accountNo) ||
      _.isEmpty(accountNo)
    ) {
      return;
    }

    const isSameTransactionUsageCategoryAsHandler =
      transactionUsageCategory === this.getTransactionUsageCategory();
    if (isSameTransactionUsageCategoryAsHandler) {
      //write it to context
      setTransactionUsageInfoTracker({
        customerId: customerId,
        accountNo: accountNo,
        transactionCategory: transactionUsageCategory,
        currentTransactionAmount: transactionAmount,
        paymentServiceCode,
        limitGroupCode
      });
    }
  }

  async processAcceptedState(
    params: IAtmTransferDailyUsageProcessAcceptedStateRequest
  ): Promise<void> {
    const { validatedParams, dailyAmountSoFar } = params;

    const {
      customerId,
      accountNo,
      beneficiaryAccountName,
      beneficiaryAccountNo,
      transactionAmount,
      paymentServiceCode,
      debitLimitGroupCode
    } = validatedParams;

    const expectedAmountAfterUpdate = dailyAmountSoFar + transactionAmount;

    logger.debug(`[processAcceptedState] 
      customerId: ${customerId}
      accountNo: ${accountNo}
      beneficiaryAccountNo: ${beneficiaryAccountNo}
      beneficiaryAccountName: ${beneficiaryAccountName}
      dailyAmountSumSoFar: ${dailyAmountSoFar}
      expectedAmountAfterUpdate: ${expectedAmountAfterUpdate}
      paymentServiceCode: ${paymentServiceCode}
      debitLimitGroupCode: ${debitLimitGroupCode}
    `);

    //update  or insert new to db (Directly consume "quota")
    const transactionUsageRecord = await transactionUsageRepoService.insertOrUpdateDailyUsage(
      customerId,
      accountNo,
      expectedAmountAfterUpdate,
      TransactionUsageCategory.ATM_TRANSFER
    );

    if (transactionUsageRecord == null) {
      throw new AppError(ERROR_CODE.FAILED_TO_SAVE_UPDATE_TRANSACTION_USAGE);
    }

    //save to context
    this.saveDailyLimitToContext({
      customerId: customerId,
      accountNo: accountNo,
      transactionAmount: transactionAmount,
      transactionUsageCategory: this.getTransactionUsageCategory(),
      paymentServiceCode,
      limitGroupCode: debitLimitGroupCode
    });
  }

  private async constructNotificationMessageParams(
    validatedParams: IAtmTransferDailyUsageParamValidatedResponse
  ): Promise<IATMTransferExceedsDailyLimitMessage> {
    const {
      beneficiaryAccountNo,
      beneficiaryAccountName,
      transactionAmount,
      customerId
    } = validatedParams;

    return {
      customerId,
      beneficiaryAccountNo: beneficiaryAccountNo,
      beneficiaryName: beneficiaryAccountName,
      amount: transactionAmount
    };
  }

  async processRejectedState(
    params: IAtmTransferDailyUsageProcessRejectedStateRequest
  ): Promise<void> {
    const { validatedParams, evalStatus, dailyAmountSoFar } = params;

    const {
      customerId,
      accountNo,
      beneficiaryAccountNo,
      beneficiaryAccountName
    } = validatedParams;

    logger.debug(`[processRejectedState] 
      customerId: ${customerId}
      accountNo: ${accountNo}
      beneficiaryAccountNo: ${beneficiaryAccountNo}
      beneficiaryAccountName: ${beneficiaryAccountName}
      dailyAmountSumSoFar: ${dailyAmountSoFar}
    `);

    let errorCode = evalStatus.errorCode;
    if (_.isNil(errorCode)) {
      //change to generic error code
      errorCode = ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT;
    }

    logger.error(`[processRejectedState]: ${evalStatus.message}`);

    const notifMessage = await this.constructNotificationMessageParams(
      validatedParams
    );

    logger.info(`[processRejectedState] 
      sending notification
    `);

    //send notification
    await transactionProducer.sendFailedMoneyTransferViaAtmDailyLimit(
      notifMessage,
      NotificationCode.NOTIF_DAILY_LIMIT_ATM_TRANSFER
    );

    //then throw error
    throw new RetryableTransferAppError(
      evalStatus.message,
      TransferFailureReasonActor.MS_TRANSFER,
      errorCode,
      false,
      [
        {
          message: ErrorList[errorCode].message,
          key: errorCode,
          code: errorCode
        }
      ]
    );
  }

  async processReversalBasedOnContext(): Promise<void> {
    logger.info(`Processing reversal for ATM Transfer Usage`);

    const transactionUsageInfo:
      | TransactionUsageContext
      | undefined = getTransactionUsageInfoTracker();

    //if the info does not exist in context
    if (_.isNil(transactionUsageInfo)) {
      return;
    }

    const isTransactionCategoryValid =
      transactionUsageInfo.transactionCategory ==
      this.getTransactionUsageCategory();

    //if the transaction category does not match ATM Transfer, skip it
    if (!isTransactionCategoryValid) {
      return;
    }

    const {
      customerId,
      accountNo,
      currentTransactionAmount
    } = transactionUsageInfo;

    logger.info(
      `[processReversalBasedOnContext] atm transfer - checking if the dailyAmountSumSoFar should be set to 0`
    );
    let updatedTransLimit = await transactionUsageRepoService.revertDailyUsageToZero(
      customerId,
      accountNo,
      currentTransactionAmount,
      TransactionUsageCategory.ATM_TRANSFER
    );

    if (updatedTransLimit == null) {
      logger.info(
        `[processReversalBasedOnContext] atm transfer - checking if the dailyAmountSumSoFar should NOT be set to 0, but 
            just perform revert the atm-transfer amount`
      );

      updatedTransLimit = await transactionUsageRepoService.revertDailyUsage(
        customerId,
        accountNo,
        currentTransactionAmount,
        TransactionUsageCategory.ATM_TRANSFER
      );
    }

    if (
      !_.isNull(updatedTransLimit) &&
      !_.isUndefined(updatedTransLimit.dailyUsages.atmTransfer)
    ) {
      logger.debug(`[ATM TRANSFER LIMIT REVERSAL] success: 
            customerId : ${updatedTransLimit.customerId}
            accountNo: ${updatedTransLimit.accountNo}
            currentAccumulatedAmount: ${updatedTransLimit.dailyUsages.atmTransfer.amount}
          `);
    }

    //need to revert rtol daily limit if it is L002 - transaction (RTOL LIMIT)
    // if (limitGroupCode === 'L002') {
    //   //revert RTOL daily limit
    //   awardRepository.updateUsageCounters(customerId);
    // }
  }
}
