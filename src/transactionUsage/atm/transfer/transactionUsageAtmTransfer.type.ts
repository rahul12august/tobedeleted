import { PaymentServiceTypeEnum } from '@dk/module-common';
import { ILimitGroup } from '../../../configuration/configuration.type';
import { RuleConditionStatus } from '../../../transaction/transaction.type';

export interface IAtmTransferDailyUsageParamValidatedResponse {
  customerId: string;
  accountNo: string;
  beneficiaryAccountNo: string;
  beneficiaryAccountName: string;
  debitTransactionCode: string;
  debitLimitGroupCode: string;
  limitGroupInfo: ILimitGroup;
  transactionAmount: number;
  paymentServiceType: PaymentServiceTypeEnum;
  paymentServiceCode: string;
}

export interface IAtmTransferDailyUsageProcessAcceptedStateRequest {
  validatedParams: IAtmTransferDailyUsageParamValidatedResponse;
  dailyAmountSoFar: number;
}

export interface IAtmTransferDailyUsageProcessRejectedStateRequest {
  evalStatus: RuleConditionStatus;
  validatedParams: IAtmTransferDailyUsageParamValidatedResponse;
  dailyAmountSoFar: number;
}
