import { BankNetworkEnum } from '@dk/module-common';
import faker from 'faker';
import { ITransactionCodeMapping } from '../../../../configuration/configuration.type';
import { ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE } from '../../../../transaction/transaction.category.enum';
import { TransactionUsageGroupCode } from '../../../transactionUsage.enum';
import {
  IPocketLimitValidationRequest,
  IPocketLimitValidationRequestExt
} from '../../../transactionUsage.type';

export const getTransactionUsageSampleRecordDB = (
  customerId: string,
  accountNo: string,
  id: string,
  lastUpdatedAt: Date
) => {
  return {
    _id: id,
    customerId: customerId,
    accountId: '',
    accountNo: accountNo,
    dailyUsages: {
      atmTransfer: {
        amount: faker.random.number({ min: 50000, max: 100000 }),
        lastUpdatedAt: lastUpdatedAt
      }
    }
  };
};

export const getTransactionUsageSampleRecordDB2 = (
  customerId: string,
  accountNo: string,
  id: string,
  lastUpdatedAt: Date,
  updatedAmount: number
) => {
  return {
    _id: id,
    customerId: customerId,
    accountId: '',
    accountNo: accountNo,
    dailyUsages: {
      atmTransfer: {
        amount: updatedAmount,
        lastUpdatedAt: lastUpdatedAt
      }
    }
  };
};

export const validateParamsValid = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountName: faker.random.alphaNumeric(10),
    paymentServiceType:
      ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
    recommendedService: {
      paymentServiceCode:
        ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'TFD24',
          transactionCodeInfo: {
            code: 'TFD24',
            limitGroupCode: 'L001',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L001',
        dailyLimitAmount: 999999999999
      },
      {
        code: TransactionUsageGroupCode.ATM_TRANSFER,
        dailyLimitAmount: 50000000
      }
    ]
  };
};

export const validateParamsCustomerIdUndefined = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: undefined,
    accountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountName: faker.random.alphaNumeric(10),
    paymentServiceType:
      ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
    recommendedService: {
      paymentServiceCode:
        ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'TFD24',
          transactionCodeInfo: {
            code: 'TFD24',
            limitGroupCode: 'L001',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L001',
        dailyLimitAmount: 999999999999
      },
      {
        code: TransactionUsageGroupCode.ATM_TRANSFER,
        dailyLimitAmount: 50000000
      }
    ]
  };
};

export const validateParamsAccountNoUndefined = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    beneficiaryAccountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountName: faker.random.alphaNumeric(10),
    accountNo: undefined,
    paymentServiceType:
      ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
    recommendedService: {
      paymentServiceCode:
        ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'TFD24',
          transactionCodeInfo: {
            code: 'TFD24',
            limitGroupCode: 'L001',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L001',
        dailyLimitAmount: 999999999999
      },
      {
        code: TransactionUsageGroupCode.ATM_TRANSFER,
        dailyLimitAmount: 50000000
      }
    ]
  };
};

export const validateParamsBeneficiaryAccountNoUndefined = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountNo: undefined,
    beneficiaryAccountName: faker.random.alphaNumeric(10),
    paymentServiceType:
      ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
    recommendedService: {
      paymentServiceCode:
        ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'TFD24',
          transactionCodeInfo: {
            code: 'TFD24',
            limitGroupCode: 'L001',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L001',
        dailyLimitAmount: 999999999999
      },
      {
        code: TransactionUsageGroupCode.ATM_TRANSFER,
        dailyLimitAmount: 50000000
      }
    ]
  };
};

export const validateParamsBeneficiaryAccountNameUndefined = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountName: undefined,
    paymentServiceType:
      ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
    recommendedService: {
      paymentServiceCode:
        ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'TFD24',
          transactionCodeInfo: {
            code: 'TFD24',
            limitGroupCode: 'L001',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L001',
        dailyLimitAmount: 999999999999
      },
      {
        code: TransactionUsageGroupCode.ATM_TRANSFER,
        dailyLimitAmount: 50000000
      }
    ]
  };
};

export const validateParamsLimitGroupCodeUndefined = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountName: faker.random.alphaNumeric(10),
    paymentServiceType:
      ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
    recommendedService: {
      paymentServiceCode:
        ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'TFD24',
          transactionCodeInfo: {
            code: 'TFD24',
            limitGroupCode: undefined,
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L001',
        dailyLimitAmount: 999999999999
      },
      {
        code: TransactionUsageGroupCode.ATM_TRANSFER,
        dailyLimitAmount: 50000000
      }
    ]
  };
};

export const validateParamsDebitTransactionCodeEmptyArray = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountName: faker.random.alphaNumeric(10),
    paymentServiceType:
      ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
    recommendedService: {
      paymentServiceCode:
        ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
      debitTransactionCode: [],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L001',
        dailyLimitAmount: 999999999999
      },
      {
        code: TransactionUsageGroupCode.ATM_TRANSFER,
        dailyLimitAmount: 50000000
      }
    ]
  };
};

export const validateParamsDebitTransactionCodeUndefined = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountName: faker.random.alphaNumeric(10),
    paymentServiceType:
      ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
    recommendedService: {
      paymentServiceCode:
        ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
      debitTransactionCode: undefined,
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L001',
        dailyLimitAmount: 999999999999
      },
      {
        code: TransactionUsageGroupCode.ATM_TRANSFER,
        dailyLimitAmount: 50000000
      }
    ]
  };
};

export const validateParamsTransactionInterchangeUndefined = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: undefined,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountName: faker.random.alphaNumeric(10),
    paymentServiceType:
      ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
    recommendedService: {
      paymentServiceCode:
        ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'TFD24',
          transactionCodeInfo: {
            code: 'TFD24',
            limitGroupCode: 'L001',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L001',
        dailyLimitAmount: 999999999999
      },
      {
        code: TransactionUsageGroupCode.ATM_TRANSFER,
        dailyLimitAmount: 50000000
      }
    ]
  };
};

export const validateParamsTransactionEmptyLimitGroupConfigs = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountName: faker.random.alphaNumeric(10),
    paymentServiceType:
      ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
    recommendedService: {
      paymentServiceCode:
        ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'TFD24',
          transactionCodeInfo: {
            code: 'TFD24',
            limitGroupCode: 'L001',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: []
  };
};

export const getJagoAtmTransferJagoInterchangeTransactionParamsSample = (): IPocketLimitValidationRequest => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountNo: faker.random.alphaNumeric(10),
    beneficiaryAccountName: faker.random.alphaNumeric(10),
    paymentServiceType:
      ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
    recommendedService: {
      paymentServiceCode:
        ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'TFD24',
          transactionCodeInfo: {
            code: 'TFD24',
            limitGroupCode: 'L001',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    }
  };
};

export const getTransactionCodesMappingJagoAtmToNonJagoAcc = (): ITransactionCodeMapping[] => {
  return [
    {
      interchange: BankNetworkEnum.ARTAJASA,
      transactionCode: 'TFD32',
      transactionCodeInfo: {
        maxAmountPerTx: 25000000,
        minAmountPerTx: 10000,
        limitGroupCode: 'L002',
        channel: 'TFD32',
        code: 'TFD32'
      }
    },

    {
      interchange: BankNetworkEnum.ALTO,
      transactionCode: 'TFD42',
      transactionCodeInfo: {
        maxAmountPerTx: 25000000,
        minAmountPerTx: 10000,
        limitGroupCode: 'L002',
        channel: 'TFD42',
        code: 'TFD42'
      }
    }
  ];
};

export const getTransactionCodesMappingsAll = (): ITransactionCodeMapping[] => {
  return [
    {
      interchange: BankNetworkEnum.JAGO,
      transactionCode: 'TFD24',
      transactionCodeInfo: {
        maxAmountPerTx: 9999999999.0,
        minAmountPerTx: 1,
        limitGroupCode: 'L001',
        channel: 'SAD01',
        code: 'TFD24'
      }
    },
    {
      interchange: BankNetworkEnum.ARTAJASA,
      transactionCode: 'TFD31',
      transactionCodeInfo: {
        maxAmountPerTx: 25000000,
        minAmountPerTx: 10000,
        limitGroupCode: 'L008',
        channel: 'TFD31',
        code: 'TFD31'
      }
    },

    {
      interchange: BankNetworkEnum.ARTAJASA,
      transactionCode: 'TFD41',
      transactionCodeInfo: {
        maxAmountPerTx: 25000000,
        minAmountPerTx: 10000,
        limitGroupCode: 'L008',
        channel: 'TFD41',
        code: 'TFD41'
      }
    },

    {
      interchange: BankNetworkEnum.ARTAJASA,
      transactionCode: 'TFD32',
      transactionCodeInfo: {
        maxAmountPerTx: 25000000,
        minAmountPerTx: 10000,
        limitGroupCode: 'L002',
        channel: 'TFD32',
        code: 'TFD32'
      }
    },

    {
      interchange: BankNetworkEnum.ALTO,
      transactionCode: 'TFD42',
      transactionCodeInfo: {
        maxAmountPerTx: 25000000,
        minAmountPerTx: 10000,
        limitGroupCode: 'L002',
        channel: 'TFD42',
        code: 'TFD42'
      }
    }
  ];
};
