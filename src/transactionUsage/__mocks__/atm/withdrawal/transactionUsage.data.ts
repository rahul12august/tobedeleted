import { BankNetworkEnum } from '@dk/module-common';
import faker from 'faker';
import { ITransactionCodeMapping } from '../../../../configuration/configuration.type';

import {
  ATM_WITHDRAWAL_INTERNATIONAL,
  ATM_WITHDRAWAL_JAGO,
  ATM_WITHDRAWAL_THIRD_PARTY_ALTO,
  ATM_WITHDRAWAL_THIRD_PARTY_ARTAJASA
} from '../../../../transaction/transaction.category.enum';
import { TransactionUsageGroupCode } from '../../../transactionUsage.enum';

import {
  IPocketLimitValidationRequestExt,
  IPocketLimitValidationRequest
} from '../../../transactionUsage.type';

export const getTransactionUsageSampleRecordDB = (
  customerId: string,
  accountNo: string,
  id: string,
  lastUpdatedAt: Date
) => {
  return {
    _id: id,
    customerId: customerId,
    accountId: '',
    accountNo: accountNo,
    dailyUsages: {
      atmWithdrawal: {
        amount: faker.random.number({ min: 50000, max: 100000 }),
        lastUpdatedAt: lastUpdatedAt
      }
    }
  };
};

export const getTransactionUsageSampleRecordDB2 = (
  customerId: string,
  accountNo: string,
  id: string,
  lastUpdatedAt: Date,
  updatedAmount: number
) => {
  return {
    _id: id,
    customerId: customerId,
    accountId: '',
    accountNo: accountNo,
    dailyUsages: {
      atmWithdrawal: {
        amount: updatedAmount,
        lastUpdatedAt: lastUpdatedAt
      }
    }
  };
};

export const getJagoAtmWithdrawalTransactionParamsSample = (): IPocketLimitValidationRequest => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    paymentServiceType: ATM_WITHDRAWAL_JAGO.paymentServiceType,
    recommendedService: {
      paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'CWD02',
          transactionCodeInfo: {
            code: 'CWD02',
            limitGroupCode: 'L015',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    }
  };
};

export const getThirdPartAltoAtmWithdrawalTransactionParamsSample = (): IPocketLimitValidationRequest => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.ALTO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    paymentServiceType: ATM_WITHDRAWAL_THIRD_PARTY_ALTO.paymentServiceType,
    recommendedService: {
      paymentServiceCode: ATM_WITHDRAWAL_THIRD_PARTY_ALTO.paymentServiceCode,
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC005', //non-jago
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.ALTO,
          transactionCode: 'CWD03',
          transactionCodeInfo: {
            code: 'CWD03',
            limitGroupCode: 'L006',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'ALTO'
          }
        }
      ]
    }
  };
};

export const getThirdPartyArtajasaAtmWithdrawalTransactionParamsSample = (): IPocketLimitValidationRequest => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.ARTAJASA,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    paymentServiceType: ATM_WITHDRAWAL_THIRD_PARTY_ARTAJASA.paymentServiceType,
    recommendedService: {
      paymentServiceCode:
        ATM_WITHDRAWAL_THIRD_PARTY_ARTAJASA.paymentServiceCode,
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC005', //non-jago
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.ARTAJASA,
          transactionCode: 'CWD03',
          transactionCodeInfo: {
            code: 'CWD03',
            limitGroupCode: 'L006',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'ARTAJASA'
          }
        }
      ]
    }
  };
};

export const getInternationalAtmWithdrawalTransactionParamsSample = (): IPocketLimitValidationRequest => {
  return {
    transactionAmount: faker.random.number({ min: 200000, max: 250000 }),
    transactionInterchange: BankNetworkEnum.VISA,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    paymentServiceType: ATM_WITHDRAWAL_INTERNATIONAL.paymentServiceType,
    recommendedService: {
      paymentServiceCode: ATM_WITHDRAWAL_INTERNATIONAL.paymentServiceCode,
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC005', //non-jago
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.VISA,
          transactionCode: 'CWD03',
          transactionCodeInfo: {
            code: 'CWD05',
            limitGroupCode: 'L006',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'VISA'
          }
        }
      ]
    }
  };
};

export const validateParamsValid = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    paymentServiceType: ATM_WITHDRAWAL_JAGO.paymentServiceType,
    recommendedService: {
      paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'CWD02',
          transactionCodeInfo: {
            code: 'CWD02',
            limitGroupCode: 'L015',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L006',
        dailyLimitAmount: 15000000
      },
      {
        code: 'L015',
        dailyLimitAmount: 15000000
      },
      {
        code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
        dailyLimitAmount: 15000000
      }
    ]
  };
};

export const validateParamsCustomerIdUndefined = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: undefined,
    accountNo: faker.random.alphaNumeric(10),
    paymentServiceType: ATM_WITHDRAWAL_JAGO.paymentServiceType,
    recommendedService: {
      paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'CWD02',
          transactionCodeInfo: {
            code: 'CWD02',
            limitGroupCode: 'L015',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L006',
        dailyLimitAmount: 15000000
      },
      {
        code: 'L015',
        dailyLimitAmount: 15000000
      },
      {
        code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
        dailyLimitAmount: 15000000
      }
    ]
  };
};

export const validateParamsAccountNoUndefined = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: undefined,
    paymentServiceType: ATM_WITHDRAWAL_JAGO.paymentServiceType,
    recommendedService: {
      paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'CWD02',
          transactionCodeInfo: {
            code: 'CWD02',
            limitGroupCode: 'L015',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L006',
        dailyLimitAmount: 15000000
      },
      {
        code: 'L015',
        dailyLimitAmount: 15000000
      },
      {
        code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
        dailyLimitAmount: 15000000
      }
    ]
  };
};

export const validateParamsLimitGroupCodeUndefined = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    paymentServiceType: ATM_WITHDRAWAL_JAGO.paymentServiceType,
    recommendedService: {
      paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'CWD02',
          transactionCodeInfo: {
            code: 'CWD02',
            limitGroupCode: undefined,
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L006',
        dailyLimitAmount: 15000000
      },
      {
        code: 'L015',
        dailyLimitAmount: 15000000
      },
      {
        code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
        dailyLimitAmount: 15000000
      }
    ]
  };
};

export const validateParamsDebitTransactionCodeEmptyArray = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    paymentServiceType: ATM_WITHDRAWAL_JAGO.paymentServiceType,
    recommendedService: {
      paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode,
      debitTransactionCode: [],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L006',
        dailyLimitAmount: 15000000
      },
      {
        code: 'L015',
        dailyLimitAmount: 15000000
      },
      {
        code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
        dailyLimitAmount: 15000000
      }
    ]
  };
};

export const validateParamsDebitTransactionCodeUndefined = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    paymentServiceType: ATM_WITHDRAWAL_JAGO.paymentServiceType,
    recommendedService: {
      paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode,
      debitTransactionCode: undefined,
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L006',
        dailyLimitAmount: 15000000
      },
      {
        code: 'L015',
        dailyLimitAmount: 15000000
      },
      {
        code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
        dailyLimitAmount: 15000000
      }
    ]
  };
};

export const validateParamsTransactionInterchangeUndefined = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: undefined,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    paymentServiceType: ATM_WITHDRAWAL_JAGO.paymentServiceType,
    recommendedService: {
      paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'CWD02',
          transactionCodeInfo: {
            code: 'CWD02',
            limitGroupCode: 'L015',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: [
      {
        code: 'L006',
        dailyLimitAmount: 15000000
      },
      {
        code: 'L015',
        dailyLimitAmount: 15000000
      },
      {
        code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
        dailyLimitAmount: 15000000
      }
    ]
  };
};

export const validateParamsTransactionEmptyLimitGroupConfigs = (): IPocketLimitValidationRequestExt => {
  return {
    transactionAmount: faker.random.number({ min: 50000, max: 150000 }),
    transactionInterchange: BankNetworkEnum.JAGO,
    customerId: faker.random.alphaNumeric(10),
    accountNo: faker.random.alphaNumeric(10),
    paymentServiceType: ATM_WITHDRAWAL_JAGO.paymentServiceType,
    recommendedService: {
      paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode,
      debitTransactionCode: [
        {
          interchange: BankNetworkEnum.JAGO,
          transactionCode: 'CWD02',
          transactionCodeInfo: {
            code: 'CWD02',
            limitGroupCode: 'L015',
            minAmountPerTx: 50000,
            maxAmountPerTx: 2500000,
            channel: 'JAGO'
          }
        }
      ],
      beneficiaryBankCodeType: '?',
      transactionAuthenticationChecking: true,
      blocking: false,
      requireThirdPartyOutgoingId: false,
      beneficiaryBankCode: 'BC191' //jago
    },
    limitGroupConfigs: []
  };
};

export const getTransactionCodesMapping = (): ITransactionCodeMapping[] => {
  return [
    {
      interchange: BankNetworkEnum.JAGO,
      transactionCode: 'CWD02',
      transactionCodeInfo: {
        maxAmountPerTx: 5000000,
        minAmountPerTx: 50000,
        limitGroupCode: 'L015',
        channel: 'CWD02',
        code: 'CWD02'
      }
    },
    {
      interchange: BankNetworkEnum.ARTAJASA,
      transactionCode: 'CWD03',
      transactionCodeInfo: {
        maxAmountPerTx: 2500000,
        minAmountPerTx: 50000,
        limitGroupCode: 'L006',
        channel: 'CWD03',
        code: 'CWD03'
      }
    },

    {
      interchange: BankNetworkEnum.ALTO,
      transactionCode: 'CWD04',
      transactionCodeInfo: {
        maxAmountPerTx: 2500000,
        minAmountPerTx: 50000,
        limitGroupCode: 'L006',
        channel: 'CWD04',
        code: 'CWD04'
      }
    },

    {
      interchange: BankNetworkEnum.VISA,
      transactionCode: 'CWD05',
      transactionCodeInfo: {
        maxAmountPerTx: 5000000,
        minAmountPerTx: 100000,
        limitGroupCode: 'L006',
        channel: 'CWD05',
        code: 'CWD05'
      }
    }
  ];
};
