import { FilterQuery, UpdateQuery } from 'mongoose';
import { wrapLogs } from '../logger';
import {
  ITransactionUsageModel,
  TransactionUsageDocument,
  TransactionUsageModel
} from './transactionUsage.model';

const convertTransactionUsageDocumentToObject = (
  document: TransactionUsageDocument
) =>
  document.toObject({
    getters: true
  }) as ITransactionUsageModel;

const getTransactionUsage = async (
  customerId: string,
  accountNo: string
): Promise<ITransactionUsageModel | null> => {
  const transactionUsage = await TransactionUsageModel.findOne({
    customerId: customerId,
    accountNo: accountNo
  });

  return (
    transactionUsage &&
    convertTransactionUsageDocumentToObject(transactionUsage)
  );
};

const insertOrUpdateTransactionUsage = async (
  query: FilterQuery<TransactionUsageDocument>,
  update: UpdateQuery<TransactionUsageDocument>
): Promise<ITransactionUsageModel | null> => {
  const transactionUsage = await TransactionUsageModel.findOneAndUpdate(
    query,
    update,
    { new: true, upsert: true }
  );

  return (
    transactionUsage &&
    convertTransactionUsageDocumentToObject(transactionUsage)
  );
};

const updateTransactionUsageCustom = async (
  query: FilterQuery<TransactionUsageDocument>,
  update: UpdateQuery<TransactionUsageDocument>
): Promise<ITransactionUsageModel | null> => {
  const data = await TransactionUsageModel.findOneAndUpdate(query, update, {
    new: true
  }).exec();

  return data && convertTransactionUsageDocumentToObject(data);
};

const transactionUsageRepository = {
  getTransactionUsage,
  updateTransactionUsageCustom,
  insertOrUpdateTransactionUsage
};

export default wrapLogs(transactionUsageRepository);
