import transactionUsageRepository from '../transactionUsage.repository';
import transactionUsageRepoService from '../transactionUsageRepo.service';
import faker from 'faker';
import {
  getTransactionUsageSampleRecordDB as getTransactionUsageSampleAtmWithdrawalRecordDB,
  getTransactionUsageSampleRecordDB2 as getTransactionUsageSampleAtmWithdrawalRecordDB2
} from '../__mocks__/atm/withdrawal/transactionUsage.data';

import { getTransactionUsageSampleRecordDB2 as getTransactionUsageSampleAtmTransferRecordDB2 } from '../__mocks__/atm/transfer/transactionUsage.data';

import { TransactionUsageCategory } from '../transactionUsage.enum';
import { AppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';

describe('transactionUsageRepo.service', () => {
  afterEach(() => {
    jest.resetAllMocks();
    expect.hasAssertions();
  });

  const customerId = 'testCust01';
  const accountNo = 'testAcc01';

  describe('findTransactionUsageByCustomerAndAccount', () => {
    it(`
        GIVEN transaction limit record not found,
        then ensure transactionUsageRepository.getTransactionUsage is called once
        and 
        it should return null
    `, async () => {
      const getTransactionUsageFn = jest.spyOn(
        transactionUsageRepository,
        'getTransactionUsage'
      );

      getTransactionUsageFn.mockResolvedValueOnce(null);

      const result = await transactionUsageRepoService.findTransactionUsageByCustomerAndAccount(
        customerId,
        accountNo
      );

      expect(getTransactionUsageFn).toBeCalledTimes(1);
      expect(result).toBeNull();
    });

    it(`
    GIVEN transaction limit record not found,
        then ensure transactionUsageRepository.getTransactionUsage is called once
        and
        it should NOT return null
    `, async () => {
      const getTransactionUsageFn = jest.spyOn(
        transactionUsageRepository,
        'getTransactionUsage'
      );

      const now = new Date();
      const id = faker.random.uuid();
      const existingRecord = getTransactionUsageSampleAtmWithdrawalRecordDB(
        customerId,
        accountNo,
        id,
        now
      );

      getTransactionUsageFn.mockResolvedValueOnce(existingRecord);

      const result = await transactionUsageRepoService.findTransactionUsageByCustomerAndAccount(
        customerId,
        accountNo
      );

      expect(getTransactionUsageFn).toBeCalledTimes(1);
      expect(result).not.toBeNull();
    });
  });

  describe('insertOrUpdateDailyUsage', () => {
    it(`GIVEN appropriate data (ATM WITHDRAWAL)
        THEN 
        - ensure transactionUsageRepository.insertOrUpdateTransactionUsage called once
 
    `, async () => {
      const now = new Date();
      const id = faker.random.uuid();
      const amount = faker.random.number({ min: 10000, max: 500000 });
      const finalRecord = getTransactionUsageSampleAtmWithdrawalRecordDB2(
        customerId,
        accountNo,
        id,
        now,
        amount
      );

      const insertOrUpdateTransactionUsageFn = jest.spyOn(
        transactionUsageRepository,
        'insertOrUpdateTransactionUsage'
      );

      insertOrUpdateTransactionUsageFn.mockResolvedValueOnce(finalRecord);

      const result = await transactionUsageRepoService.insertOrUpdateDailyUsage(
        customerId,
        accountNo,
        amount,
        TransactionUsageCategory.ATM_WITHDRAWAL
      );

      //expect(insertOrUpdateTransactionUsageFn).toBeCalledTimes(1);
      expect(insertOrUpdateTransactionUsageFn).toBeCalledWith(
        {
          accountNo,
          customerId
        },
        {
          $set: {
            'dailyUsages.atmWithdrawal': {
              amount: amount,
              lastUpdatedAt: expect.any(Date)
            }
          }
        }
      );
      expect(result).toEqual(finalRecord);
    });

    it(`GIVEN appropriate data (ATM TRANSFER)
    THEN 
    - ensure transactionUsageRepository.insertOrUpdateTransactionUsage called once

`, async () => {
      const now = new Date();
      const id = faker.random.uuid();
      const amount = faker.random.number({ min: 10000, max: 500000 });
      const finalRecord = getTransactionUsageSampleAtmTransferRecordDB2(
        customerId,
        accountNo,
        id,
        now,
        amount
      );

      const insertOrUpdateTransactionUsageFn = jest.spyOn(
        transactionUsageRepository,
        'insertOrUpdateTransactionUsage'
      );

      insertOrUpdateTransactionUsageFn.mockResolvedValueOnce(finalRecord);

      const result = await transactionUsageRepoService.insertOrUpdateDailyUsage(
        customerId,
        accountNo,
        amount,
        TransactionUsageCategory.ATM_TRANSFER
      );

      //expect(insertOrUpdateTransactionUsageFn).toBeCalledTimes(1);
      expect(insertOrUpdateTransactionUsageFn).toBeCalledWith(
        {
          accountNo,
          customerId
        },
        {
          $set: {
            'dailyUsages.atmTransfer': {
              amount: amount,
              lastUpdatedAt: expect.any(Date)
            }
          }
        }
      );
      expect(result).toEqual(finalRecord);
    });

    it(`GIVEN transactionUsageCategory with unspecified field (setup)
    THEN 
    - it will return default field value of dailyUsages.notImplemented
    - ensure error thrown ERROR_CODE.DAILY_USAGE_REPO_FIELD_TO_UPDATE_NOT_FOUND

`, async () => {
      try {
        const now = new Date();
        const id = faker.random.uuid();
        const amount = faker.random.number({ min: 10000, max: 500000 });
        const finalRecord = getTransactionUsageSampleAtmWithdrawalRecordDB2(
          customerId,
          accountNo,
          id,
          now,
          amount
        );

        const insertOrUpdateTransactionUsageFn = jest.spyOn(
          transactionUsageRepository,
          'insertOrUpdateTransactionUsage'
        );

        insertOrUpdateTransactionUsageFn.mockResolvedValueOnce(finalRecord);

        await transactionUsageRepoService.insertOrUpdateDailyUsage(
          customerId,
          accountNo,
          amount,
          'TEST' as TransactionUsageCategory
        );
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.DAILY_USAGE_REPO_FIELD_TO_UPDATE_NOT_FOUND
        );
      }
    });
  });

  describe('revertDailyUsage', () => {
    it(`
        GIVEN appropriate input (ATM WITHDRAWAL)
        THEN: 
        - ensure transactionUsageRepository.updateTransactionUsageCustom called once
 
    `, async () => {
      const now = new Date();
      const id = faker.random.uuid();
      const amount = faker.random.number({ min: 10000, max: 500000 });
      const finalRecord = getTransactionUsageSampleAtmWithdrawalRecordDB2(
        customerId,
        accountNo,
        id,
        now,
        amount
      );

      const updateTransactionUsageCustomFn = jest.spyOn(
        transactionUsageRepository,
        'updateTransactionUsageCustom'
      );

      updateTransactionUsageCustomFn.mockResolvedValueOnce(finalRecord);

      const result = await transactionUsageRepoService.revertDailyUsage(
        customerId,
        accountNo,
        123,
        TransactionUsageCategory.ATM_WITHDRAWAL
      );

      expect(updateTransactionUsageCustomFn).toBeCalledWith(
        {
          accountNo,
          customerId
        },
        {
          $set: {
            'dailyUsages.atmWithdrawal': {
              $inc: { amount: -123 },
              lastUpdatedAt: expect.any(Date)
            }
          }
        }
      );
      expect(result).toEqual(finalRecord);
    });

    it(`
    GIVEN appropriate input (ATM TRANSFER)
    THEN: 
    - ensure transactionUsageRepository.updateTransactionUsageCustom called once

`, async () => {
      const now = new Date();
      const id = faker.random.uuid();
      const amount = faker.random.number({ min: 10000, max: 500000 });
      const finalRecord = getTransactionUsageSampleAtmTransferRecordDB2(
        customerId,
        accountNo,
        id,
        now,
        amount
      );

      const updateTransactionUsageCustomFn = jest.spyOn(
        transactionUsageRepository,
        'updateTransactionUsageCustom'
      );

      updateTransactionUsageCustomFn.mockResolvedValueOnce(finalRecord);

      const result = await transactionUsageRepoService.revertDailyUsage(
        customerId,
        accountNo,
        123,
        TransactionUsageCategory.ATM_TRANSFER
      );

      expect(updateTransactionUsageCustomFn).toBeCalledWith(
        {
          accountNo,
          customerId
        },
        {
          $set: {
            'dailyUsages.atmTransfer': {
              $inc: { amount: -123 },
              lastUpdatedAt: expect.any(Date)
            }
          }
        }
      );
      expect(result).toEqual(finalRecord);
    });

    it(`
    GIVEN transactionUsageCategory with unspecified field (setup)
    THEN: 
    - it will return default field value of dailyUsages.notImplemented
    - ensure error thrown ERROR_CODE.DAILY_USAGE_REPO_FIELD_TO_UPDATE_NOT_FOUND

`, async () => {
      try {
        const now = new Date();
        const id = faker.random.uuid();
        const amount = faker.random.number({ min: 10000, max: 500000 });
        const finalRecord = getTransactionUsageSampleAtmWithdrawalRecordDB2(
          customerId,
          accountNo,
          id,
          now,
          amount
        );

        const updateTransactionUsageCustomFn = jest.spyOn(
          transactionUsageRepository,
          'updateTransactionUsageCustom'
        );

        updateTransactionUsageCustomFn.mockResolvedValueOnce(finalRecord);

        await transactionUsageRepoService.revertDailyUsage(
          customerId,
          accountNo,
          123,
          'TEST' as TransactionUsageCategory
        );
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.DAILY_USAGE_REPO_FIELD_TO_UPDATE_NOT_FOUND
        );
      }
    });
  });

  describe('revertDailyUsageToZero', () => {
    it(`
        GIVEN appropriate input (ATM WITHDRAWAL)
        THEN: 
        - ensure transactionUsageRepository.updateTransactionUsageCustom called once
 
    `, async () => {
      const now = new Date();
      const id = faker.random.uuid();
      const amount = 0;
      const finalRecord = getTransactionUsageSampleAtmWithdrawalRecordDB2(
        customerId,
        accountNo,
        id,
        now,
        amount
      );

      const updateTransactionUsageCustomFn = jest.spyOn(
        transactionUsageRepository,
        'updateTransactionUsageCustom'
      );

      updateTransactionUsageCustomFn.mockResolvedValueOnce(finalRecord);

      const result = await transactionUsageRepoService.revertDailyUsageToZero(
        customerId,
        accountNo,
        123,
        TransactionUsageCategory.ATM_WITHDRAWAL
      );

      expect(updateTransactionUsageCustomFn).toBeCalledWith(
        {
          accountNo,
          customerId,
          'dailyUsages.atmWithdrawal': {
            amount: {
              $and: [
                {
                  $gte: 0
                },
                {
                  $lte: 123
                }
              ]
            }
          }
        },
        {
          $set: {
            'dailyUsages.atmWithdrawal': {
              amount: 0,
              lastUpdatedAt: expect.any(Date)
            }
          }
        }
      );
      expect(result).toEqual(finalRecord);
    });

    it(`
        GIVEN appropriate input
        THEN: 
        - ensure transactionUsageRepository.updateTransactionUsageCustom called once
 
    `, async () => {
      const now = new Date();
      const id = faker.random.uuid();
      const amount = 0;
      const finalRecord = getTransactionUsageSampleAtmWithdrawalRecordDB2(
        customerId,
        accountNo,
        id,
        now,
        amount
      );

      const updateTransactionUsageCustomFn = jest.spyOn(
        transactionUsageRepository,
        'updateTransactionUsageCustom'
      );

      updateTransactionUsageCustomFn.mockResolvedValueOnce(finalRecord);

      const result = await transactionUsageRepoService.revertDailyUsageToZero(
        customerId,
        accountNo,
        123,
        TransactionUsageCategory.ATM_TRANSFER
      );

      expect(updateTransactionUsageCustomFn).toBeCalledWith(
        {
          accountNo,
          customerId,
          'dailyUsages.atmTransfer': {
            amount: {
              $and: [
                {
                  $gte: 0
                },
                {
                  $lte: 123
                }
              ]
            }
          }
        },
        {
          $set: {
            'dailyUsages.atmTransfer': {
              amount: 0,
              lastUpdatedAt: expect.any(Date)
            }
          }
        }
      );
      expect(result).toEqual(finalRecord);
    });

    it(`
       GIVEN transactionUsageCategory with unspecified field (setup)
        THEN: 
        - it will return default field value of dailyUsages.notImplemented
        - ensure error thrown ERROR_CODE.DAILY_USAGE_REPO_FIELD_TO_UPDATE_NOT_FOUND
 
    `, async () => {
      try {
        const now = new Date();
        const id = faker.random.uuid();
        const amount = 0;
        const finalRecord = getTransactionUsageSampleAtmWithdrawalRecordDB2(
          customerId,
          accountNo,
          id,
          now,
          amount
        );

        const updateTransactionUsageCustomFn = jest.spyOn(
          transactionUsageRepository,
          'updateTransactionUsageCustom'
        );

        updateTransactionUsageCustomFn.mockResolvedValueOnce(finalRecord);

        await transactionUsageRepoService.revertDailyUsageToZero(
          customerId,
          accountNo,
          123,
          'TEST' as TransactionUsageCategory
        );
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.DAILY_USAGE_REPO_FIELD_TO_UPDATE_NOT_FOUND
        );
      }
    });
  });
});
