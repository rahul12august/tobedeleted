import { MongoMemoryServer } from 'mongodb-memory-server';

import transactionUsageRepo from '../transactionUsage.repository';
import mongoose, { FilterQuery, UpdateQuery } from 'mongoose';
import {
  TransactionUsageDocument,
  TransactionUsageModel
} from '../transactionUsage.model';
import _ from 'lodash';
import faker from 'faker';

jest.mock('mongoose', () => {
  const mongoose = require.requireActual('mongoose');
  return new mongoose.Mongoose();
});

interface IPocketDailyLimitRecord {
  amount: number;
  lastUpdatedAt: Date;
}

const createNewRecord = async (
  customerId: string,
  accountNo: string,
  atmWithdrawalUpdateInput?: IPocketDailyLimitRecord,
  atmTransferUpdateInput?: IPocketDailyLimitRecord
) => {
  const filterQuery: FilterQuery<TransactionUsageDocument> = {
    customerId: customerId,
    accountNo: accountNo
  };

  const amount = 1000000;

  let atmWithdrawalDailyLimit = {};
  let atmTransferDailyLimit = {};

  if (!_.isUndefined(atmWithdrawalUpdateInput)) {
    atmWithdrawalDailyLimit = {
      'dailyUsages.atmWithdrawal': {
        amount: atmWithdrawalUpdateInput.amount,
        lastUpdatedAt: atmWithdrawalUpdateInput.lastUpdatedAt
      }
    };
  }

  if (!_.isUndefined(atmTransferUpdateInput)) {
    atmTransferDailyLimit = {
      'dailyUsages.atmTransfer': {
        amount: atmTransferUpdateInput.amount,
        lastUpdatedAt: atmTransferUpdateInput.lastUpdatedAt
      }
    };
  }

  const updateQuery: UpdateQuery<TransactionUsageDocument> = {
    $set: {
      ...atmWithdrawalDailyLimit,
      ...atmTransferDailyLimit
    }
  };

  return await transactionUsageRepo.insertOrUpdateTransactionUsage(
    filterQuery,
    updateQuery
  );
};

describe('transactionUsage repository', () => {
  let mongod: MongoMemoryServer;

  beforeAll(async () => {
    mongod = new MongoMemoryServer();
    const mongoDbUri = await mongod.getConnectionString();
    await mongoose.connect(mongoDbUri, {
      useNewUrlParser: true,
      useCreateIndex: true
    });
  });

  afterEach(async () => {
    expect.hasAssertions();
    await TransactionUsageModel.deleteMany({});
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongod.stop();
  });

  const customerId = 'cust01';
  const accountNo = 'acc01';

  describe('insertOrUpdateTransactionUsage', () => {
    it(`
        GIVEN the customerId and accountNo's record limit does not exist
        THEN it should create new record based on the given data
    `, async () => {
      const amount = faker.random.number({ min: 100000, max: 1000000 });
      const newlyCreatedTransactionUsage = await createNewRecord(
        customerId,
        accountNo,
        {
          amount: amount,
          lastUpdatedAt: new Date()
        }
      );

      if (_.isNull(newlyCreatedTransactionUsage)) {
        return;
      }

      expect(newlyCreatedTransactionUsage).not.toBeNull();
      expect(newlyCreatedTransactionUsage['_id']).not.toBeNull();
      expect(newlyCreatedTransactionUsage['_id']).not.toBeUndefined();
      expect(newlyCreatedTransactionUsage.dailyUsages).not.toBeNull();
      expect(
        newlyCreatedTransactionUsage.dailyUsages.atmWithdrawal
      ).not.toBeNull();
      expect(
        newlyCreatedTransactionUsage.dailyUsages.atmWithdrawal?.amount
      ).toEqual(amount);
    });

    it(`GIVEN the customerId and accountNo's record exist,
        THEN it should have the same id and data is updated appropriately
    `, async () => {
      const amount = faker.random.number({ min: 100000, max: 1000000 });

      const newlyCreatedTransactionUsage = await createNewRecord(
        customerId,
        accountNo,
        {
          amount: amount,
          lastUpdatedAt: new Date()
        }
      );

      const now = new Date();
      const newAmount = faker.random.number({ min: 1000000, max: 2000000 });
      const updatedTransactionUsage = await createNewRecord(
        customerId,
        accountNo,
        {
          amount: newAmount,
          lastUpdatedAt: now
        }
      );

      if (
        _.isNull(newlyCreatedTransactionUsage) ||
        _.isNull(updatedTransactionUsage) ||
        _.isUndefined(updatedTransactionUsage?.dailyUsages.atmWithdrawal)
      ) {
        return;
      }

      expect(newlyCreatedTransactionUsage['_id']).toEqual(
        updatedTransactionUsage['_id']
      );
      expect(updatedTransactionUsage.dailyUsages.atmWithdrawal.amount).toEqual(
        newAmount
      );
      expect(
        updatedTransactionUsage.dailyUsages.atmWithdrawal.lastUpdatedAt
      ).toEqual(now);
    });
  });

  describe('getTransactionUsage', () => {
    it(`GIVEN record does not exist,
    THEN return null
    `, async () => {
      const existingRecord = await transactionUsageRepo.getTransactionUsage(
        customerId,
        accountNo
      );

      expect(existingRecord).toBeNull();
    });

    it(`GIVEN record  exist,
    THEN return the record
    `, async () => {
      const newlyCreatedTransactionUsage = await createNewRecord(
        customerId,
        accountNo,
        {
          amount: faker.random.number({ min: 10000, max: 100000 }),
          lastUpdatedAt: new Date()
        }
      );

      const existingRecord = await transactionUsageRepo.getTransactionUsage(
        customerId,
        accountNo
      );

      expect(newlyCreatedTransactionUsage).toEqual(existingRecord);
    });
  });

  describe('updateTransactionUsageCustom', () => {
    it(`GIVEN the record does not exist,
        THEN it should not create new record
    `, async () => {
      const filterQuery: FilterQuery<TransactionUsageDocument> = {
        customerId: customerId,
        accountNo: accountNo
      };

      const updateQuery: UpdateQuery<TransactionUsageDocument> = {
        $set: {
          'dailyUsages.atmWithdrawal': {
            $inc: {
              amount: -Math.abs(-10000)
            },
            lastUpdatedAt: new Date()
          }
        }
      };

      const updatedRecord = await transactionUsageRepo.updateTransactionUsageCustom(
        filterQuery,
        updateQuery
      );

      expect(updatedRecord).toBeNull();
    });

    it(`GIVEN the record exist,
    THEN it should update appropriately
`, async () => {
      const newlyCreatedTransactionUsage = await createNewRecord(
        customerId,
        accountNo,
        {
          amount: faker.random.number({ min: 5000, max: 100000 }),
          lastUpdatedAt: new Date()
        }
      );

      const filterQuery: FilterQuery<TransactionUsageDocument> = {
        customerId: customerId,
        accountNo: accountNo
      };

      const amount = 1000000;

      const updateQuery: UpdateQuery<TransactionUsageDocument> = {
        $set: {
          'dailyUsages.atmWithdrawal': {
            amount: amount,
            lastUpdatedAt: new Date()
          }
        }
      };

      const updatedRecord = await transactionUsageRepo.updateTransactionUsageCustom(
        filterQuery,
        updateQuery
      );

      if (
        _.isNil(updatedRecord) ||
        _.isUndefined(updatedRecord?.dailyUsages.atmWithdrawal) ||
        _.isNil(newlyCreatedTransactionUsage)
      ) {
        return;
      }

      expect(updatedRecord).not.toBeNull();
      expect(updatedRecord['_id']).toEqual(newlyCreatedTransactionUsage['_id']);
      expect(updatedRecord?.dailyUsages.atmWithdrawal.amount).toEqual(amount);
    });
  });
});
