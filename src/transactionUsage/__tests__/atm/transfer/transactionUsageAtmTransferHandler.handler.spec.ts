import faker from 'faker';
import { ERROR_CODE } from '../../../../common/errors';
import {
  TransactionUsageCategory,
  TransactionUsageGroupCode
} from '../../../transactionUsage.enum';
import {
  IDailyUsageParamValidatedResponse,
  IEvalStatusError,
  IPocketLimitValidationRequestExt
} from '../../../transactionUsage.type';
import {
  validateParamsAccountNoUndefined,
  validateParamsCustomerIdUndefined,
  validateParamsBeneficiaryAccountNoUndefined,
  validateParamsBeneficiaryAccountNameUndefined,
  validateParamsDebitTransactionCodeEmptyArray,
  validateParamsDebitTransactionCodeUndefined,
  validateParamsLimitGroupCodeUndefined,
  validateParamsValid
} from '../../../__mocks__/atm/transfer/transactionUsage.data';
import { TransactionUsageAtmTransferHandler } from '../../../atm/transfer/transactionUsageAtmTransferHandler';
import {
  ATM_TRANSFER_JAGO_ATM_ALTO_INTERCHANGE,
  ATM_TRANSFER_JAGO_ATM_ARTAJASA_INTERCHANGE,
  ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE,
  ATM_TRANSFER_THIRD_PARTY_ATM
} from '../../../../transaction/transaction.category.enum';
import { PaymentServiceTypeEnum } from '@dk/module-common';
import moment from 'moment';
import transactionUsageRepoService from '../../../transactionUsageRepo.service';
import {
  AppError,
  RetryableTransferAppError
} from '../../../../errors/AppError';
import { ITransactionUsageModel } from '../../../transactionUsage.model';
import _ from 'lodash';
import transactionUsageCommonService from '../../../transactionUsageCommon.service';
import { ILimitGroup } from '../../../../configuration/configuration.type';
import transactionProducer from '../../../../transaction/transaction.producer';
import { RuleConditionStatus } from '../../../../transaction/transaction.type';
import { NotificationCode } from '../../../../transaction/transaction.producer.enum';
import { IAtmTransferDailyUsageParamValidatedResponse } from '../../../atm/transfer/transactionUsageAtmTransfer.type';

jest.mock('../../../transactionUsageRepo.service');
jest.mock('../../../../transaction/transaction.producer');
jest.mock('../../../../account/account.repository');
jest.mock('../../../transactionUsageCommon.service');

describe('TransactionUsageAtmTransferHandler [handler]', () => {
  const transactionUsageGroupCode = TransactionUsageGroupCode.ATM_TRANSFER;
  const transactionUsageCategory = TransactionUsageCategory.ATM_TRANSFER;
  const evalStatusErrors: IEvalStatusError = {
    errorCodeDeclined: ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT,
    errorCodeTryTomorrow: ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT
  };
  //based on BI regulation (50 million rupiah)
  const dailyLimitAmount = 50000000;

  //sample validated params
  const validatedParams: IAtmTransferDailyUsageParamValidatedResponse = {
    accountNo: faker.random.alphaNumeric(10),
    customerId: faker.random.alphaNumeric(10),
    beneficiaryAccountName: faker.random.alphaNumeric(10),
    beneficiaryAccountNo: faker.random.alphaNumeric(10),
    debitLimitGroupCode: ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.limitGroupCode,
    debitTransactionCode: faker.random.alphaNumeric(5),
    limitGroupInfo: {
      code: transactionUsageGroupCode,
      dailyLimitAmount: dailyLimitAmount
    },
    paymentServiceCode:
      ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
    paymentServiceType:
      ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
    transactionAmount: faker.random.number({ min: 100000, max: 1000000 })
  };

  let handlerInstance;
  beforeEach(() => {
    handlerInstance = new TransactionUsageAtmTransferHandler();
  });

  afterEach(() => {
    jest.resetAllMocks();
    expect.hasAssertions();
  });

  describe('TransactionUsageGroupCode', () => {
    it('should return transaction limit group code of ATM TRANSFER', () => {
      const result = handlerInstance.getTransactionUsageGroupCode();
      expect(result).toEqual(transactionUsageGroupCode);
    });
  });

  describe('TransactionUsageCategory', () => {
    it('should return transaction limit category of ATM TRANSFER', () => {
      const result = handlerInstance.getTransactionUsageCategory();
      expect(result).toEqual(transactionUsageCategory);
    });
  });

  describe('getEvalStatusErrors', () => {
    it(`should return 
      tryTomorrow: ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT
      declined: ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT,
    `, () => {
      const result = handlerInstance.getEvalStatusErrors();
      expect(result).toEqual(evalStatusErrors);
    });
  });

  describe('validateDailyUsageParams', () => {
    test.each([
      validateParamsCustomerIdUndefined(),
      validateParamsAccountNoUndefined(),
      validateParamsBeneficiaryAccountNameUndefined(),
      validateParamsBeneficiaryAccountNoUndefined()
    ])(
      `
        GIVEN invalid params customerId/accountNo/beneficiaryAccountNo/beneficiaryAccountName,
        THEN 
        - it should throw error ERROR_CODE.ATM_TRANSFER_EXECUTOR_MANDATORY_INFO_NOT_FOUND`,
      async (params: IPocketLimitValidationRequestExt) => {
        try {
          const getLimitGroupsConfigFn = transactionUsageCommonService.getLimitGroupsConfig as jest.Mock;

          getLimitGroupsConfigFn.mockResolvedValueOnce([]);

          const handlerInstance = new TransactionUsageAtmTransferHandler();
          await handlerInstance.validateDailyUsageParams(params);
        } catch (e) {
          expect(e).toBeInstanceOf(AppError);
          expect((e as AppError).errorCode).toEqual(
            ERROR_CODE.ATM_TRANSFER_EXECUTOR_MANDATORY_INFO_NOT_FOUND
          );
        }
      }
    );

    it(`
      GIVEN invalid params debit transaction code is empty array,
      THEN it should throw error ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE`, async () => {
      try {
        const getLimitGroupsConfigFn = transactionUsageCommonService.getLimitGroupsConfig as jest.Mock;

        getLimitGroupsConfigFn.mockResolvedValueOnce([]);

        const params = validateParamsDebitTransactionCodeEmptyArray();

        const handlerInstance = new TransactionUsageAtmTransferHandler();
        await handlerInstance.validateDailyUsageParams(params);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE
        );
      }
    });

    it(`
      GIVEN invalid params debit transaction code is undefined,
      THEN it should throw error ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE`, async () => {
      try {
        const getLimitGroupsConfigFn = transactionUsageCommonService.getLimitGroupsConfig as jest.Mock;

        getLimitGroupsConfigFn.mockResolvedValueOnce([]);

        const params = validateParamsDebitTransactionCodeUndefined();

        const handlerInstance = new TransactionUsageAtmTransferHandler();
        await handlerInstance.validateDailyUsageParams(params);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE
        );
      }
    });

    it(`
      GIVEN invalid params debit limit group code is undefined,
      THEN it should throw error ERROR_CODE.RECOMMENDED_LIMIT_GROUP_CODE_NOT_EXIST`, async () => {
      try {
        const getLimitGroupsConfigFn = transactionUsageCommonService.getLimitGroupsConfig as jest.Mock;
        getLimitGroupsConfigFn.mockResolvedValueOnce([]);

        const extractInfoFromTransactionCodeMappingsFn = transactionUsageCommonService.extractInfoFromTransactionCodeMappings as jest.Mock;
        extractInfoFromTransactionCodeMappingsFn.mockImplementationOnce(() => {
          throw new AppError(ERROR_CODE.RECOMMENDED_LIMIT_GROUP_CODE_NOT_EXIST);
        });

        const params = validateParamsLimitGroupCodeUndefined();

        const handlerInstance = new TransactionUsageAtmTransferHandler();
        await handlerInstance.validateDailyUsageParams(params);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.RECOMMENDED_LIMIT_GROUP_CODE_NOT_EXIST
        );
      }
    });

    it(`
      GIVEN invalid params specified limit group code is not found in limit group configs,
      THEN it should throw error ERROR_CODE.RECOMMENDED_LIMIT_GROUP_NOT_FOUND_IN_CONFIG`, async () => {
      try {
        const params = validateParamsValid();
        if (
          !_.isNil(params.recommendedService.debitTransactionCode) &&
          params.recommendedService.debitTransactionCode.length > 0
        ) {
          params.recommendedService.debitTransactionCode[0].transactionCodeInfo.limitGroupCode =
            'L99999';
        }

        const extractMappingInfoResult = {
          transactionCode: 'TFD24',
          limitGroupCode: 'L001'
        };

        const extractInfoFromTransactionCodeMappingsFn = transactionUsageCommonService.extractInfoFromTransactionCodeMappings as jest.Mock;
        extractInfoFromTransactionCodeMappingsFn.mockReturnValueOnce(
          extractMappingInfoResult
        );

        jest
          .spyOn(transactionUsageCommonService, 'getLimitGroupCodeInfo')
          .mockImplementationOnce(() => {
            throw new AppError(
              ERROR_CODE.RECOMMENDED_LIMIT_GROUP_NOT_FOUND_IN_CONFIG
            );
          });

        const getLimitGroupsConfigFn = transactionUsageCommonService.getLimitGroupsConfig as jest.Mock;
        getLimitGroupsConfigFn.mockResolvedValueOnce([]);

        const handlerInstance = new TransactionUsageAtmTransferHandler();
        await handlerInstance.validateDailyUsageParams(params);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.RECOMMENDED_LIMIT_GROUP_NOT_FOUND_IN_CONFIG
        );
      }
    });

    it(`
    GIVEN valid params,
    then it should the validated params`, async () => {
      const params = validateParamsValid();
      const pocketLimitAtmTransferLimitGroup: ILimitGroup = {
        code: TransactionUsageGroupCode.ATM_TRANSFER,
        dailyLimitAmount: dailyLimitAmount
      };
      params.limitGroupConfigs = [pocketLimitAtmTransferLimitGroup];

      const extractMappingInfoResult = {
        transactionCode: 'TFD24',
        limitGroupCode: 'L001'
      };

      const extractInfoFromTransactionCodeMappingsFn = transactionUsageCommonService.extractInfoFromTransactionCodeMappings as jest.Mock;
      extractInfoFromTransactionCodeMappingsFn.mockReturnValueOnce(
        extractMappingInfoResult
      );

      jest
        .spyOn(transactionUsageCommonService, 'getLimitGroupCodeInfo')
        .mockReturnValueOnce(pocketLimitAtmTransferLimitGroup);

      const handlerInstance = new TransactionUsageAtmTransferHandler();
      const result = await handlerInstance.validateDailyUsageParams(params);

      expect(result).toEqual({
        accountNo: params.accountNo,
        customerId: params.customerId,
        beneficiaryAccountNo: params.beneficiaryAccountNo,
        beneficiaryAccountName: params.beneficiaryAccountName,
        debitLimitGroupCode: extractMappingInfoResult.limitGroupCode,
        debitTransactionCode: extractMappingInfoResult.transactionCode,
        limitGroupInfo: pocketLimitAtmTransferLimitGroup,
        paymentServiceCode:
          ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
        paymentServiceType: params.paymentServiceType,
        transactionAmount: params.transactionAmount
      });
    });
  });

  describe('isValidTransactionCategoryByRecommendedService', () => {
    it(`
      Given the recommended service it does not categorized as ATM Withdrawal category
      THEN return false
    `, () => {
      const nonAtmWithdrawalParams: IDailyUsageParamValidatedResponse = {
        customerId: faker.random.alphaNumeric(10),
        accountNo: faker.random.alphaNumeric(10),
        debitTransactionCode: faker.random.alphaNumeric(5),
        debitLimitGroupCode:
          ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.limitGroupCode,
        limitGroupInfo: {
          code: transactionUsageGroupCode,
          dailyLimitAmount: dailyLimitAmount
        },
        transactionAmount: faker.random.number({
          min: 100000,
          max: 500000
        }),
        paymentServiceType: PaymentServiceTypeEnum.CASHBACK,
        paymentServiceCode:
          ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode
      };

      const result = handlerInstance.isValidTransactionCategoryByRecommendedService(
        nonAtmWithdrawalParams
      );
      expect(result).toBeFalsy();
    });

    it(`
      Given the recommended service is categorized as ATM TRANSFER (JAGO ATM to other)
      via JAGO interchange
      THEN return true
    `, () => {
      const atmWithdrawalParams: IDailyUsageParamValidatedResponse = {
        customerId: faker.random.alphaNumeric(10),
        accountNo: faker.random.alphaNumeric(10),
        debitTransactionCode: faker.random.alphaNumeric(5),
        debitLimitGroupCode:
          ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.limitGroupCode,
        limitGroupInfo: {
          code: transactionUsageGroupCode,
          dailyLimitAmount: dailyLimitAmount
        },
        transactionAmount: faker.random.number({
          min: 100000,
          max: 500000
        }),
        paymentServiceType:
          ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
        paymentServiceCode:
          ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode
      };

      const result = handlerInstance.isValidTransactionCategoryByRecommendedService(
        atmWithdrawalParams
      );
      expect(result).toBeTruthy();
    });

    it(`
    Given the recommended service is categorized as ATM TRANSFER (JAGO ATM to other)
    via ALTO interchange
    THEN return true
  `, () => {
      const atmWithdrawalParams: IDailyUsageParamValidatedResponse = {
        customerId: faker.random.alphaNumeric(10),
        accountNo: faker.random.alphaNumeric(10),
        debitTransactionCode: faker.random.alphaNumeric(5),
        debitLimitGroupCode:
          ATM_TRANSFER_JAGO_ATM_ALTO_INTERCHANGE.limitGroupCode,
        limitGroupInfo: {
          code: transactionUsageGroupCode,
          dailyLimitAmount: dailyLimitAmount
        },
        transactionAmount: faker.random.number({
          min: 100000,
          max: 500000
        }),
        paymentServiceType:
          ATM_TRANSFER_JAGO_ATM_ALTO_INTERCHANGE.paymentServiceType,
        paymentServiceCode:
          ATM_TRANSFER_JAGO_ATM_ALTO_INTERCHANGE.paymentServiceCode
      };

      const result = handlerInstance.isValidTransactionCategoryByRecommendedService(
        atmWithdrawalParams
      );
      expect(result).toBeTruthy();
    });

    it(`
    Given the recommended service is categorized as ATM TRANSFER (JAGO ATM to other)
    via ARTAJASA interchange
    THEN return true
  `, () => {
      const atmWithdrawalParams: IDailyUsageParamValidatedResponse = {
        customerId: faker.random.alphaNumeric(10),
        accountNo: faker.random.alphaNumeric(10),
        debitTransactionCode: faker.random.alphaNumeric(5),
        debitLimitGroupCode:
          ATM_TRANSFER_JAGO_ATM_ARTAJASA_INTERCHANGE.limitGroupCode,
        limitGroupInfo: {
          code: transactionUsageGroupCode,
          dailyLimitAmount: dailyLimitAmount
        },
        transactionAmount: faker.random.number({
          min: 100000,
          max: 500000
        }),
        paymentServiceType:
          ATM_TRANSFER_JAGO_ATM_ARTAJASA_INTERCHANGE.paymentServiceType,
        paymentServiceCode:
          ATM_TRANSFER_JAGO_ATM_ARTAJASA_INTERCHANGE.paymentServiceCode
      };

      const result = handlerInstance.isValidTransactionCategoryByRecommendedService(
        atmWithdrawalParams
      );
      expect(result).toBeTruthy();
    });

    it(`
    Given the recommended service is categorized as ATM TRANSFER (THIRD PARTY ATM to other)
    THEN return true
  `, () => {
      const atmWithdrawalParams: IDailyUsageParamValidatedResponse = {
        customerId: faker.random.alphaNumeric(10),
        accountNo: faker.random.alphaNumeric(10),
        debitTransactionCode: faker.random.alphaNumeric(5),
        debitLimitGroupCode: ATM_TRANSFER_THIRD_PARTY_ATM.limitGroupCode,
        limitGroupInfo: {
          code: transactionUsageGroupCode,
          dailyLimitAmount: dailyLimitAmount
        },
        transactionAmount: faker.random.number({
          min: 100000,
          max: 500000
        }),
        paymentServiceType: ATM_TRANSFER_THIRD_PARTY_ATM.paymentServiceType,
        paymentServiceCode: ATM_TRANSFER_THIRD_PARTY_ATM.paymentServiceCode
      };

      const result = handlerInstance.isValidTransactionCategoryByRecommendedService(
        atmWithdrawalParams
      );
      expect(result).toBeTruthy();
    });
  });

  describe(`getDailyLimitAmountSoFar`, () => {
    beforeEach(() => {
      global.Date.now = jest.fn(() =>
        new Date('2023-02-01T12:00:00.000Z').getTime()
      );
    });

    it(`
          GIVEN existing record is null,
          then it should return 0
        `, () => {
      const result = handlerInstance.getDailyLimitAmountSoFar(null);
      expect(result).toEqual(0);
    });

    it(`
          GIVEN existing record lastUpdateDate is still within the same day as NOW
          then it should return existing record's amount from db
        `, () => {
      const transDateCurrent = moment();
      const transDatePrevious = transDateCurrent.subtract(1, 'hour');

      (transactionUsageCommonService.isDailyLimitExpired as jest.Mock).mockReturnValueOnce(
        false
      );

      const amount = 1000000;
      const result = handlerInstance.getDailyLimitAmountSoFar({
        accountNo: faker.random.alphaNumeric(10),
        customerId: faker.random.alphaNumeric(10),
        dailyUsages: {
          atmTransfer: {
            amount: amount,
            lastUpdatedAt: transDatePrevious.toDate()
          }
        }
      });

      expect(result).toEqual(amount);
    });

    it(`
          GIVEN existing record lastUpdateDate is >= 1 day more than NOW
          then it should return existing record's amount from db
        `, () => {
      const transDateCurrent = moment();
      const transDatePrevious = transDateCurrent.subtract(1, 'day');

      (transactionUsageCommonService.isDailyLimitExpired as jest.Mock).mockReturnValueOnce(
        true
      );

      const amount = 1000000;
      const result = handlerInstance.getDailyLimitAmountSoFar({
        accountNo: faker.random.alphaNumeric(10),
        customerId: faker.random.alphaNumeric(10),
        dailyUsages: {
          atmTransfer: {
            amount: amount,
            lastUpdatedAt: transDatePrevious.toDate()
          }
        }
      });

      expect(result).toEqual(0);
    });
  });

  describe('processAcceptedState', () => {
    const now = new Date();

    it(`
    GIVEN 
      - valid parameters
      - insertOrUpdateDailyUsage returns null 
    THEN
      - it should throw error ERROR_CODE.FAILED_TO_SAVE_UPDATE_TRANSACTION_USAGE
  `, async () => {
      try {
        //update transaction_limit record
        const insertOrUpdateDailyUsageFn = transactionUsageRepoService.insertOrUpdateDailyUsage as jest.Mock;
        insertOrUpdateDailyUsageFn.mockResolvedValueOnce(null);

        const saveDailyLimitToContextFn = jest.spyOn(
          handlerInstance,
          'saveDailyLimitToContext'
        );
        saveDailyLimitToContextFn.mockImplementationOnce(_ => {});

        await handlerInstance.processAcceptedState({
          validatedParams,
          dailyAmountSoFar: 1000000
        });
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.FAILED_TO_SAVE_UPDATE_TRANSACTION_USAGE
        );
      }
    });

    it(`
      GIVEN 
        - valid parameters
        - Existing record not found
      THEN
        - it should save to db
        - it should call saveDailyLimitToContext
    `, async () => {
      const dailyAmountSoFar = 0;

      const expectedUpdatedTransactionUsageRecord: ITransactionUsageModel = {
        accountNo: validatedParams.accountNo as string,
        customerId: validatedParams.customerId as string,
        dailyUsages: {
          atmTransfer: {
            amount: validatedParams.transactionAmount,
            lastUpdatedAt: now
          }
        }
      };

      //update transaction_limit record
      const insertOrUpdateDailyUsageFn = transactionUsageRepoService.insertOrUpdateDailyUsage as jest.Mock;
      insertOrUpdateDailyUsageFn.mockResolvedValueOnce(
        expectedUpdatedTransactionUsageRecord
      );

      const saveDailyLimitToContextFn = jest.spyOn(
        handlerInstance,
        'saveDailyLimitToContext'
      );
      saveDailyLimitToContextFn.mockImplementationOnce(_ => {});

      await handlerInstance.processAcceptedState({
        validatedParams,
        dailyAmountSoFar
      });

      expect(insertOrUpdateDailyUsageFn).toBeCalledTimes(1);
      expect(saveDailyLimitToContextFn).toBeCalledTimes(1);
    });

    it(`
        GIVEN
          - valid parameters
          - Existing record found
        THEN
        - it should save to db
        - it should call saveDailyLimitToContext
      `, async () => {
      const transDateCurrent = moment('2023-02-01T08:00:00');
      const transDatePrevious = transDateCurrent.subtract(1, 'hour');

      const existingRecord: ITransactionUsageModel = {
        accountNo: validatedParams.accountNo as string,
        customerId: validatedParams.customerId as string,
        dailyUsages: {
          atmTransfer: {
            amount: 1000000,
            lastUpdatedAt: transDatePrevious.toDate()
          }
        }
      };

      //just for linting
      if (_.isUndefined(existingRecord.dailyUsages.atmTransfer)) {
        return;
      }

      const dailyAmountSoFar = existingRecord.dailyUsages.atmTransfer.amount;

      const expectedUpdatedTransactionUsageRecord = Object.assign(
        {},
        existingRecord
      );

      //for linting
      if (
        _.isUndefined(
          expectedUpdatedTransactionUsageRecord.dailyUsages.atmTransfer
        )
      ) {
        return;
      }

      //update amount
      expectedUpdatedTransactionUsageRecord.dailyUsages.atmTransfer.amount =
        existingRecord.dailyUsages.atmTransfer.amount +
        validatedParams.transactionAmount;

      //update last updated at
      expectedUpdatedTransactionUsageRecord.dailyUsages.atmTransfer.lastUpdatedAt = transDateCurrent.toDate();

      const insertOrUpdateDailyUsageFn = transactionUsageRepoService.insertOrUpdateDailyUsage as jest.Mock;
      insertOrUpdateDailyUsageFn.mockResolvedValueOnce(
        expectedUpdatedTransactionUsageRecord
      );

      //context
      const saveDailyLimitToContextFn = jest.spyOn(
        handlerInstance,
        'saveDailyLimitToContext'
      );
      saveDailyLimitToContextFn.mockImplementationOnce(_ => {});

      await handlerInstance.processAcceptedState({
        validatedParams,
        dailyAmountSoFar
      });

      expect(insertOrUpdateDailyUsageFn).toBeCalledTimes(1);
      expect(saveDailyLimitToContextFn).toBeCalledTimes(1);
    });

    it(`
        GIVEN
          - valid parameters
          - Existing record found (date was yesterday)
        THEN
          -  daily amount usage to be back to 0
          - it should update existing record in db
          - it should save to context
      `, async () => {
      //const params = validateParamsValid();
      const transDateCurrent = moment('2023-02-01T08:00:00');
      const transDatePrevious = transDateCurrent.subtract(1, 'day');
      const dailyAmountSoFar = 0;

      const existingRecord: ITransactionUsageModel = {
        accountNo: validatedParams.accountNo as string,
        customerId: validatedParams.customerId as string,
        dailyUsages: {
          atmTransfer: {
            amount: 1000000,
            lastUpdatedAt: transDatePrevious.toDate()
          }
        }
      };

      //just for linting
      if (_.isUndefined(existingRecord.dailyUsages.atmTransfer)) {
        return;
      }

      const expectedUpdatedTransactionUsageRecord = Object.assign(
        {},
        existingRecord
      );

      //for linting
      if (
        _.isUndefined(
          expectedUpdatedTransactionUsageRecord.dailyUsages.atmTransfer
        )
      ) {
        return;
      }

      //update amount
      //reset back
      expectedUpdatedTransactionUsageRecord.dailyUsages.atmTransfer.amount =
        validatedParams.transactionAmount;

      //update last updated at
      expectedUpdatedTransactionUsageRecord.dailyUsages.atmTransfer.lastUpdatedAt = transDateCurrent.toDate();

      const insertOrUpdateDailyUsageFn = transactionUsageRepoService.insertOrUpdateDailyUsage as jest.Mock;
      insertOrUpdateDailyUsageFn.mockResolvedValueOnce(
        expectedUpdatedTransactionUsageRecord
      );

      //context
      const saveDailyLimitToContextFn = jest.spyOn(
        handlerInstance,
        'saveDailyLimitToContext'
      );
      saveDailyLimitToContextFn.mockImplementationOnce(_ => {});

      await handlerInstance.processAcceptedState({
        validatedParams,
        dailyAmountSoFar
      });

      expect(insertOrUpdateDailyUsageFn).toBeCalledTimes(1);
      expect(saveDailyLimitToContextFn).toBeCalledTimes(1);
    });
  });

  describe('processRejectedState', () => {
    it(`
      GIVEN evalStatus.errorCode is null 
      THEN 
       - errorCode will be generic: ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT [temp]
       - ensure that transactionProducer.sendFailedMoneyTransferViaAtmDailyLimit called (notification sent)
       - and it should throw RetryableTransferAppError
    `, async () => {
      try {
        const sendFailedMoneyTransferViaAtmDailyLimitFn = transactionProducer.sendFailedMoneyTransferViaAtmDailyLimit as jest.Mock;
        sendFailedMoneyTransferViaAtmDailyLimitFn.mockImplementationOnce(
          _ => {}
        );

        await handlerInstance.processRejectedState({
          validatedParams,
          dailyAmountSoFar: 1000000,
          evalStatus: new RuleConditionStatus('ERROR_DUMMY')
        });

        expect(sendFailedMoneyTransferViaAtmDailyLimitFn).toBeCalledTimes(1);
      } catch (e) {
        expect(e).toBeInstanceOf(RetryableTransferAppError);
        expect((e as RetryableTransferAppError).errorCode).toEqual(
          ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT
        );
      }
    });

    it(`
    GIVEN evalStatus.errorCode is ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT
    And account alias exist
    THEN 
     - ensure that transactionProducer.sendFailedMoneyTransferViaAtmDailyLimit called (notification sent)
     - should  aliasName, as the pocketName
     - and it should throw RetryableTransferAppError
  `, async () => {
      const sendFailedMoneyTransferViaAtmDailyLimitFn = transactionProducer.sendFailedMoneyTransferViaAtmDailyLimit as jest.Mock;
      try {
        sendFailedMoneyTransferViaAtmDailyLimitFn.mockImplementationOnce(
          _ => {}
        );

        await handlerInstance.processRejectedState({
          validatedParams,
          dailyAmountSoFar: 1000000,
          evalStatus: new RuleConditionStatus(
            'ERROR_DUMMY',
            ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT
          )
        });
      } catch (e) {
        expect(sendFailedMoneyTransferViaAtmDailyLimitFn).toBeCalledWith(
          {
            amount: validatedParams.transactionAmount,
            customerId: validatedParams.customerId,
            beneficiaryAccountNo: validatedParams.beneficiaryAccountNo,
            beneficiaryName: validatedParams.beneficiaryAccountName
          },
          NotificationCode.NOTIF_DAILY_LIMIT_ATM_TRANSFER
        );
        expect(e).toBeInstanceOf(RetryableTransferAppError);
        expect((e as RetryableTransferAppError).errorCode).toEqual(
          ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT
        );
      }
    });

    it(`
    GIVEN evalStatus.errorCode is ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT
    THEN 
     - ensure that transactionProducer.sendFailedMoneyTransferViaAtmDailyLimit called (notification sent)
     - and it should throw RetryableTransferAppError
  `, async () => {
      const sendFailedMoneyTransferViaAtmDailyLimitFn = transactionProducer.sendFailedMoneyTransferViaAtmDailyLimit as jest.Mock;
      const dailyAmountSoFar = 1000000;
      try {
        sendFailedMoneyTransferViaAtmDailyLimitFn.mockImplementationOnce(
          _ => {}
        );

        await handlerInstance.processRejectedState({
          validatedParams,
          dailyAmountSoFar,
          evalStatus: new RuleConditionStatus(
            'ERROR_DUMMY',
            ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT
          )
        });
      } catch (e) {
        expect(sendFailedMoneyTransferViaAtmDailyLimitFn).toBeCalledWith(
          {
            amount: validatedParams.transactionAmount,
            customerId: validatedParams.customerId,
            beneficiaryAccountNo: validatedParams.beneficiaryAccountNo,
            beneficiaryName: validatedParams.beneficiaryAccountName
          },
          NotificationCode.NOTIF_DAILY_LIMIT_ATM_TRANSFER
        );
        expect(e).toBeInstanceOf(RetryableTransferAppError);
        expect((e as RetryableTransferAppError).errorCode).toEqual(
          ERROR_CODE.ATM_TRANSFER_AMOUNT_EXCEEDED_DAILY_LIMIT
        );
      }
    });
  });
});
