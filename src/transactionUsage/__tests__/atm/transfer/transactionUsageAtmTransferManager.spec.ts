import { validateParamsValid } from '../../../__mocks__/atm/transfer/transactionUsage.data';
import _ from 'lodash';

import transactionUsageRepoService from '../../../transactionUsageRepo.service';
import { RuleConditionState } from '../../../../transaction/transaction.conditions.enum';
import transactionConditionHelperService from '../../../../transaction/transaction.conditions.helper';
import { RuleConditionStatus } from '../../../../transaction/transaction.type';
import {
  TransactionUsageCategory,
  TransactionUsageGroupCode
} from '../../../transactionUsage.enum';
import { IDailyUsageParamValidatedResponse } from '../../../transactionUsage.type';
import { PaymentServiceTypeEnum } from '@dk/module-common';
import faker from 'faker';

import moment from 'moment';
import { ITransactionUsageModel } from '../../../transactionUsage.model';
import { TransactionUsageAtmTransferHandler } from '../../../atm/transfer/transactionUsageAtmTransferHandler';
import { TransactionUsageAtmTransferManager } from '../../../atm/transfer/transactionUsageAtmTransferManager';
import { ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE } from '../../../../transaction/transaction.category.enum';

jest.mock('../../../transactionUsageRepo.service');
jest.mock('../../../../transaction/transaction.conditions.helper');

describe('TransactionUsageAtmTransferManager', () => {
  const transCategory = TransactionUsageCategory.ATM_TRANSFER;
  const transLimitGroupCode = TransactionUsageGroupCode.ATM_TRANSFER;
  const dailyLimitAmount = 50000000;

  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('validateDailyUsageParams', () => {
    it('should call handler instance', async () => {
      const handlerInstance = new TransactionUsageAtmTransferHandler();

      const validateDailyLimitFn = jest.spyOn(
        handlerInstance,
        'validateDailyUsageParams'
      );
      validateDailyLimitFn.mockImplementationOnce(_ => {});

      const managerInstance = new TransactionUsageAtmTransferManager(
        handlerInstance
      );

      await managerInstance.validateDailyUsageParams(validateParamsValid());

      expect(validateDailyLimitFn).toBeCalledTimes(1);
    });
  });

  describe('processReversalBasedOnContext', () => {
    it('should call handlerInstance.processReversalBasedOnContext', async () => {
      const handlerInstance = new TransactionUsageAtmTransferHandler();
      const processReversalBasedOnContextFn = jest.spyOn(
        handlerInstance,
        'processReversalBasedOnContext'
      );
      processReversalBasedOnContextFn.mockImplementationOnce(_ => {});

      const managerInstance = new TransactionUsageAtmTransferManager(
        handlerInstance
      );

      await managerInstance.processReversalBasedOnContext();

      expect(processReversalBasedOnContextFn).toBeCalledTimes(1);
    });
  });

  describe('isFeatureFlagEnabled', () => {
    it('should call handlerInstance.isFeatureFlagEnabled', async () => {
      const handlerInstance = new TransactionUsageAtmTransferHandler();

      const isFeatureFlagEnabledFn = jest.spyOn(
        handlerInstance,
        'isFeatureFlagEnabled'
      );
      isFeatureFlagEnabledFn.mockImplementationOnce(_ => {});

      const managerInstance = new TransactionUsageAtmTransferManager(
        handlerInstance
      );

      await managerInstance.isFeatureFlagEnabled();

      expect(isFeatureFlagEnabledFn).toBeCalledTimes(1);
    });
  });

  describe('saveDailyLimitContext', () => {
    it('should call handlerInstance.saveDailyLimitContext', async () => {
      const handlerInstance = new TransactionUsageAtmTransferHandler();

      const saveDailyLimitContextFn = jest.spyOn(
        handlerInstance,
        'saveDailyLimitToContext'
      );
      saveDailyLimitContextFn.mockImplementationOnce(_ => {});

      const managerInstance = new TransactionUsageAtmTransferManager(
        handlerInstance
      );

      await managerInstance.saveDailyLimitContext({
        accountNo: faker.random.alphaNumeric(10),
        customerId: faker.random.alphaNumeric(10),
        transactionAmount: 1000000,
        transactionUsageCategory: transCategory,
        limitGroupCode: faker.random.alphaNumeric(5),
        paymentServiceCode: faker.random.alphaNumeric(5)
      });

      expect(saveDailyLimitContextFn).toBeCalledTimes(1);
    });
  });

  describe('validateTransactionAmountAgainstDailyLimit', () => {
    const getValidatedParams = (): IDailyUsageParamValidatedResponse => {
      return {
        accountNo: faker.random.alphaNumeric(10),
        customerId: faker.random.alphaNumeric(10),
        beneficiaryAccountName: faker.random.alphaNumeric(10),
        beneficiaryAccountNo: faker.random.alphaNumeric(10),
        debitLimitGroupCode:
          ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.limitGroupCode,
        debitTransactionCode: faker.random.alphaNumeric(5),
        limitGroupInfo: {
          code: transLimitGroupCode,
          dailyLimitAmount
        },
        paymentServiceCode:
          ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
        paymentServiceType:
          ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
        transactionAmount: 100000
      };
    };

    it(`
      GIVEN  recommended service cannot be categorized as ATM Transfer 
        (handlerInstance.isValidTransactionCategoryByRecommendedService = false) 
      THEN
        - it should return isValidated = false
        
    `, async () => {
      const invalidValidatedParams = getValidatedParams();
      invalidValidatedParams.paymentServiceType =
        PaymentServiceTypeEnum.CASHBACK;
      const dailyAmountSoFar = 0;

      const findTransactionUsageByCustomerAndAccountFn = transactionUsageRepoService.findTransactionUsageByCustomerAndAccount as jest.Mock;
      findTransactionUsageByCustomerAndAccountFn.mockImplementationOnce(
        _ => {}
      );

      const compareAmountWithDailyLimitFn = transactionConditionHelperService.compareAmountWithDailyLimit as jest.Mock;
      compareAmountWithDailyLimitFn.mockImplementationOnce(_ => {});

      jest.mock(
        '../../../atm/transfer/transactionUsageAtmTransferHandler',
        () => ({
          isValidTransactionCategoryByRecommendedService: jest
            .fn()
            .mockResolvedValueOnce(false),
          getDailyLimitAmountSoFar: jest
            .fn()
            .mockReturnValueOnce(dailyAmountSoFar)
        })
      );

      const managerInstance = new TransactionUsageAtmTransferManager();

      const result = await managerInstance.validateTransactionAmountAgainstDailyLimit(
        invalidValidatedParams
      );

      expect(result).toEqual({
        isValidated: false
      });
    });

    it(`
      GIVEN  recommended service categorized as ATM Transfer 
        (handlerInstance.isValidTransactionCategoryByRecommendedService = true) 
        - no transaction_limit record exist [dailyAmountSoFar = 0]
      THEN
        - it should return {
            is Validated: true 
            + dailyAmountSoFar = 0
            + resultState = ACCEPTED
         }
        
    `, async () => {
      const validatedParams = getValidatedParams();
      const dailyAmountSoFar = 0;
      const ruleConditionStateResult = RuleConditionState.ACCEPTED;

      const findTransactionUsageByCustomerAndAccountFn = transactionUsageRepoService.findTransactionUsageByCustomerAndAccount as jest.Mock;
      findTransactionUsageByCustomerAndAccountFn.mockResolvedValueOnce(null);

      const compareAmountWithDailyLimitFn = transactionConditionHelperService.compareAmountWithDailyLimit as jest.Mock;
      compareAmountWithDailyLimitFn.mockReturnValueOnce(
        ruleConditionStateResult
      );

      jest.mock(
        '../../../atm/transfer/transactionUsageAtmTransferHandler',
        () => ({
          isValidTransactionCategoryByRecommendedService: jest
            .fn()
            .mockResolvedValueOnce(true),
          getDailyLimitAmountSoFar: jest
            .fn()
            .mockReturnValueOnce(dailyAmountSoFar)
        })
      );

      const managerInstance = new TransactionUsageAtmTransferManager();

      const result = await managerInstance.validateTransactionAmountAgainstDailyLimit(
        validatedParams
      );

      expect(result).toEqual({
        isValidated: true,
        dailyAmountSoFar,
        ruleConditionStateResult
      });
    });

    it(`
      GIVEN  recommended service categorized as ATM Transfer 
        (handlerInstance.isValidTransactionCategoryByRecommendedService = true) 
        - transaction_limit record exist [dailyAmountSoFar = 50000000]
        - any transaction amount will exceeds daily limit of 50 million
      THEN
        - it should return {
            is Validated: false 
            + dailyAmountSoFar = 50000000
            + resultState = TRY_TOMORROW
         }
        
    `, async () => {
      const validatedParams = getValidatedParams();
      const dailyAmountSoFar = 50000000;
      const ruleConditionStateResult = RuleConditionState.TRY_TOMORROW;
      const transDateCurrent = moment('2023-02-01T08:00:00');
      const transDatePrevious = transDateCurrent.subtract(1, 'hour');

      const existingRecord: ITransactionUsageModel = {
        accountNo: validatedParams.accountNo as string,
        customerId: validatedParams.customerId as string,
        dailyUsages: {
          atmWithdrawal: {
            amount: dailyAmountSoFar,
            lastUpdatedAt: transDatePrevious.toDate()
          }
        }
      };

      const findTransactionUsageByCustomerAndAccountFn = transactionUsageRepoService.findTransactionUsageByCustomerAndAccount as jest.Mock;
      findTransactionUsageByCustomerAndAccountFn.mockResolvedValueOnce(
        existingRecord
      );

      const compareAmountWithDailyLimitFn = transactionConditionHelperService.compareAmountWithDailyLimit as jest.Mock;
      compareAmountWithDailyLimitFn.mockReturnValueOnce(
        ruleConditionStateResult
      );

      const handlerInstance = new TransactionUsageAtmTransferHandler();
      jest
        .spyOn(
          handlerInstance,
          'isValidTransactionCategoryByRecommendedService'
        )
        .mockResolvedValueOnce(true);
      jest
        .spyOn(handlerInstance, 'getDailyLimitAmountSoFar')
        .mockReturnValueOnce(dailyAmountSoFar);

      const managerInstance = new TransactionUsageAtmTransferManager(
        handlerInstance
      );

      const result = await managerInstance.validateTransactionAmountAgainstDailyLimit(
        validatedParams
      );

      expect(result).toEqual({
        isValidated: true,
        dailyAmountSoFar,
        ruleConditionStateResult
      });
    });

    it(`
    GIVEN  recommended service categorized as ATM Transfer 
      (handlerInstance.isValidTransactionCategoryByRecommendedService = true) 
      - transaction_limit record exist [dailyAmountSoFar = 1000000]
      - assume transaction amount is 16 million, where daily limit is 50 million
    THEN
      - it should return {
          is Validated: false 
          + dailyAmountSoFar = 1000000
          + resultState = DECLINED
       }
      
  `, async () => {
      const validatedParams = getValidatedParams();
      const dailyAmountSoFar = 1000000;
      const ruleConditionStateResult = RuleConditionState.DECLINED;
      const transDateCurrent = moment('2023-02-01T08:00:00');
      const transDatePrevious = transDateCurrent.subtract(1, 'hour');

      const existingRecord: ITransactionUsageModel = {
        accountNo: validatedParams.accountNo as string,
        customerId: validatedParams.customerId as string,
        dailyUsages: {
          atmWithdrawal: {
            amount: dailyAmountSoFar,
            lastUpdatedAt: transDatePrevious.toDate()
          }
        }
      };

      const findTransactionUsageByCustomerAndAccountFn = transactionUsageRepoService.findTransactionUsageByCustomerAndAccount as jest.Mock;
      findTransactionUsageByCustomerAndAccountFn.mockResolvedValueOnce(
        existingRecord
      );

      const compareAmountWithDailyLimitFn = transactionConditionHelperService.compareAmountWithDailyLimit as jest.Mock;
      compareAmountWithDailyLimitFn.mockReturnValueOnce(
        ruleConditionStateResult
      );

      const handlerInstance = new TransactionUsageAtmTransferHandler();
      jest
        .spyOn(
          handlerInstance,
          'isValidTransactionCategoryByRecommendedService'
        )
        .mockResolvedValueOnce(true);
      jest
        .spyOn(handlerInstance, 'getDailyLimitAmountSoFar')
        .mockReturnValueOnce(dailyAmountSoFar);

      const managerInstance = new TransactionUsageAtmTransferManager(
        handlerInstance
      );

      const result = await managerInstance.validateTransactionAmountAgainstDailyLimit(
        validatedParams
      );

      expect(result).toEqual({
        isValidated: true,
        dailyAmountSoFar,
        ruleConditionStateResult
      });
    });
  });

  describe('processAcceptedState', () => {
    const getValidatedParams = (): IDailyUsageParamValidatedResponse => {
      return {
        accountNo: faker.random.alphaNumeric(10),
        customerId: faker.random.alphaNumeric(10),
        beneficiaryAccountName: faker.random.alphaNumeric(10),
        beneficiaryAccountNo: faker.random.alphaNumeric(10),
        debitLimitGroupCode:
          ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.limitGroupCode,
        debitTransactionCode: faker.random.alphaNumeric(5),
        limitGroupInfo: {
          code: transLimitGroupCode,
          dailyLimitAmount
        },
        paymentServiceCode:
          ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
        paymentServiceType:
          ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
        transactionAmount: 100000
      };
    };

    it(`
    GIVEN the state is ACCEPTED
    then it should call handlerinstance.acceptedState 
    `, async () => {
      const handlerInstance = new TransactionUsageAtmTransferHandler();
      const processAcceptedStateFn = jest.spyOn(
        handlerInstance,
        'processAcceptedState'
      );
      processAcceptedStateFn.mockImplementationOnce(_ => {});

      const managerInstance = new TransactionUsageAtmTransferManager(
        handlerInstance
      );
      await managerInstance.processAcceptedState({
        dailyAmountSoFar: 1000000,
        validatedParams: getValidatedParams(),
        ruleConditionStateResult: RuleConditionState.ACCEPTED
      });

      expect(processAcceptedStateFn).toBeCalledTimes(1);
    });
  });

  describe('processRejectedState', () => {
    const getValidatedParams = (): IDailyUsageParamValidatedResponse => {
      return {
        accountNo: faker.random.alphaNumeric(10),
        customerId: faker.random.alphaNumeric(10),
        debitLimitGroupCode:
          ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.limitGroupCode,
        debitTransactionCode: faker.random.alphaNumeric(5),
        limitGroupInfo: {
          code: transLimitGroupCode,
          dailyLimitAmount
        },
        paymentServiceCode:
          ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode,
        paymentServiceType:
          ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceType,
        transactionAmount: 100000
      };
    };

    it(` 
    GIVEN the state is non accpted,
    THEN
    ensure:
      - it called transactionConditionHelperService.constructDailyLimitEvaluatedRuleConditionStatusRefined
      - it called handlerinstance.processRejectedState
     `, async () => {
      const handlerInstance = new TransactionUsageAtmTransferHandler();

      const constructDailyLimitEvaluatedRuleConditionStatusRefinedFn = transactionConditionHelperService.constructDailyLimitEvaluatedRuleConditionStatusRefined as jest.Mock;
      constructDailyLimitEvaluatedRuleConditionStatusRefinedFn.mockReturnValueOnce(
        new RuleConditionStatus('declined')
      );
      const processRejectedStateFn = jest.spyOn(
        handlerInstance,
        'processRejectedState'
      );
      processRejectedStateFn.mockImplementationOnce(_ => {});

      const managerInstance = new TransactionUsageAtmTransferManager(
        handlerInstance
      );
      await managerInstance.processRejectedState({
        dailyAmountSoFar: 50000000,
        validatedParams: getValidatedParams(),
        ruleConditionStateResult: RuleConditionState.DECLINED
      });

      expect(
        constructDailyLimitEvaluatedRuleConditionStatusRefinedFn
      ).toBeCalledTimes(1);
      expect(processRejectedStateFn).toBeCalledTimes(1);
    });
  });

  describe('setTransactionUsageHandlerInstance', () => {
    it(`ensure when getting the handler instance, it does not return null`, () => {
      const managerInstance = new TransactionUsageAtmTransferManager();
      const handlerInstance = new TransactionUsageAtmTransferHandler();
      managerInstance.setTransactionUsageHandlerInstance(handlerInstance);

      const result = managerInstance.getTransactionUsageHandlerInstance();

      expect(result).not.toBeNull();
      expect(result).toEqual(handlerInstance);
    });
  });
});
