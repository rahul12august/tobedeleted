import { isFeatureEnabled } from '../../../../common/featureFlag';
import { TransactionUsageAtmTransferHandler } from '../../../atm/transfer/transactionUsageAtmTransferHandler';

jest.mock('../../../../common/featureFlag');

const setupFeatureFlag = ({ dailyLimitAtmWTransferConfigFlag }) => {
  const confIsFeatureEnabled = isFeatureEnabled as jest.Mock;
  confIsFeatureEnabled.mockImplementationOnce(flags => {
    switch (flags) {
      case 'enableDailyLimitAtmTransferPocketLevelValidation':
        return dailyLimitAtmWTransferConfigFlag;
      default:
        return false;
    }
  });
};

describe('transactionUsageAtmTransferHandler [feature Flag]', () => {
  afterEach(() => {
    jest.resetAllMocks();
    expect.hasAssertions();
  });

  describe(`isFeatureFlagEnabled`, () => {
    let handlerInstance: TransactionUsageAtmTransferHandler;
    beforeEach(() => {
      handlerInstance = new TransactionUsageAtmTransferHandler();
    });

    it(`
        GIVEN ATM-TRANSFER daily limit pocket validation flag is set to true,
        THEN return true
      `, () => {
      setupFeatureFlag({ dailyLimitAtmWTransferConfigFlag: true });

      const result = handlerInstance.isFeatureFlagEnabled();

      expect(result).toEqual(true);
    });

    it(`
        GIVEN ATM-TRANSFER daily limit pocket validation flag is set to false,
        THEN return false
      `, () => {
      setupFeatureFlag({ dailyLimitAtmWTransferConfigFlag: false });

      const result = handlerInstance.isFeatureFlagEnabled();

      expect(result).toEqual(false);
    });
  });
});
