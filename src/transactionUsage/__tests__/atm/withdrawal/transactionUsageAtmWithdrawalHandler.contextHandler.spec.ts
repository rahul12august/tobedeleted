import faker from 'faker';

import {
  getTransactionUsageInfoTracker,
  setTransactionUsageInfoTracker
} from '../../../../common/contextHandler';

import { TransactionUsageAtmWithdrawalHandler } from '../../../atm/withdrawal/transactionUsageAtmWithdrawalHandler';
import { TransactionUsageCategory } from '../../../transactionUsage.enum';
import {
  ITransactionUsageSaveContextRequest,
  TransactionUsageContext
} from '../../../transactionUsage.type';
import transactionUsageRepoService from '../../../transactionUsageRepo.service';

jest.mock('../../../../common/contextHandler');
jest.mock('../../../transactionUsageRepo.service');

describe('transactionUsageAtmWithdrawalHandler [context handler]', () => {
  const limitCategory = TransactionUsageCategory.ATM_WITHDRAWAL;
  let handlerInstance;
  afterEach(() => {
    jest.resetAllMocks();
    expect.hasAssertions();
  });

  beforeEach(() => {
    handlerInstance = new TransactionUsageAtmWithdrawalHandler();
  });

  describe('saveDailyLimitToContext', () => {
    describe('invalid params', () => {
      const params: ITransactionUsageSaveContextRequest[] = [
        {
          customerId: undefined,
          accountNo: faker.random.alphaNumeric(10),
          transactionAmount: faker.random.number({ min: 100000, max: 500000 }),
          transactionUsageCategory: limitCategory,
          limitGroupCode: faker.random.alphaNumeric(5),
          paymentServiceCode: faker.random.alphaNumeric(5)
        },
        {
          customerId: '',
          accountNo: faker.random.alphaNumeric(10),
          transactionAmount: faker.random.number({ min: 100000, max: 500000 }),
          transactionUsageCategory: limitCategory,
          limitGroupCode: faker.random.alphaNumeric(5),
          paymentServiceCode: faker.random.alphaNumeric(5)
        },
        {
          customerId: faker.random.alphaNumeric(10),
          accountNo: undefined,
          transactionAmount: faker.random.number({ min: 100000, max: 500000 }),
          transactionUsageCategory: limitCategory,
          limitGroupCode: faker.random.alphaNumeric(5),
          paymentServiceCode: faker.random.alphaNumeric(5)
        },
        {
          customerId: faker.random.alphaNumeric(10),
          accountNo: '',
          transactionAmount: faker.random.number({ min: 100000, max: 500000 }),
          transactionUsageCategory: limitCategory,
          limitGroupCode: faker.random.alphaNumeric(5),
          paymentServiceCode: faker.random.alphaNumeric(5)
        }
      ];

      it.each(params)(
        `
      GIVEN param is invalid
      THEN ensure setTransactionUsageInfoTracker is not being called`,
        param => {
          const setTransactionUsageInfoTrackerFn = setTransactionUsageInfoTracker as jest.Mock;
          setTransactionUsageInfoTrackerFn.mockImplementationOnce(_ => {});

          handlerInstance.saveDailyLimitToContext(param);

          expect(setTransactionUsageInfoTrackerFn).not.toBeCalled();
        }
      );
    });

    it(`
    GIVEN category is not ATM_WITHDRAWAL
    THEN ensure setTransactionUsageInfoTracker is not being called
    `, () => {
      const setTransactionUsageInfoTrackerFn = setTransactionUsageInfoTracker as jest.Mock;
      setTransactionUsageInfoTrackerFn.mockImplementationOnce(_ => {});

      handlerInstance.saveDailyLimitToContext({
        customerId: faker.random.alphaNumeric(10),
        accountNo: faker.random.alphaNumeric(10),
        transactionAmount: faker.random.number({ min: 100000, max: 500000 }),
        transactionUsageCategory: 'invalid_category' as TransactionUsageCategory,
        limitGroupCode: faker.random.alphaNumeric(5),
        paymentServiceCode: faker.random.alphaNumeric(5)
      });

      expect(setTransactionUsageInfoTrackerFn).not.toBeCalled();
    });

    it(`
    GIVEN valid customerId, accountNo, and ATM-Withdrawal category
    THEN ensure setTransactionUsageInfoTracker is called
    `, () => {
      const setTransactionUsageInfoTrackerFn = setTransactionUsageInfoTracker as jest.Mock;
      setTransactionUsageInfoTrackerFn.mockImplementationOnce(_ => {});

      handlerInstance.saveDailyLimitToContext({
        customerId: faker.random.alphaNumeric(10),
        accountNo: faker.random.alphaNumeric(10),
        transactionAmount: faker.random.number({ min: 100000, max: 500000 }),
        transactionUsageCategory: limitCategory,
        limitGroupCode: faker.random.alphaNumeric(5),
        paymentServiceCode: faker.random.alphaNumeric(5)
      });

      expect(setTransactionUsageInfoTrackerFn).toBeCalledTimes(1);
    });
  });

  describe('processReversalBasedOnContext', () => {
    it(`
      GIVEN transaction limit info does not exist in the context,
      THEN 
      - ensure the transactionUsageRepoService.revertDailyUsageToZero not called
      - ensure the transactionUsageRepoService.revertDailyUsage not called
    `, async () => {
      const getTransactionUsageInfoTrackerFn = getTransactionUsageInfoTracker as jest.Mock;
      getTransactionUsageInfoTrackerFn.mockReturnValueOnce(null);

      const revertDailyUsageToZeroFn = transactionUsageRepoService.revertDailyUsageToZero as jest.Mock;
      revertDailyUsageToZeroFn.mockImplementationOnce(_ => {});

      const revertDailyUsageFn = transactionUsageRepoService.revertDailyUsage as jest.Mock;
      revertDailyUsageFn.mockImplementationOnce(_ => {});

      await handlerInstance.processReversalBasedOnContext();

      expect(revertDailyUsageFn).not.toBeCalledWith();
      expect(revertDailyUsageToZeroFn).not.toBeCalledWith();
    });

    it(`
      GIVEN 
      - transaction limit info exist in the context
      - category is not ATM-withdrawal,
      THEN 
      - ensure the transactionUsageRepoService.revertDailyUsageToZero not called
      - ensure the transactionUsageRepoService.revertDailyUsage not called
    `, async () => {
      const contextVal: TransactionUsageContext = {
        customerId: 'custO1',
        accountNo: 'acc01',
        transactionCategory: 'not_atm_withdrawal' as TransactionUsageCategory,
        currentTransactionAmount: 1000000,
        limitGroupCode: faker.random.alphaNumeric(5),
        paymentServiceCode: faker.random.alphaNumeric(5)
      };

      const getTransactionUsageInfoTrackerFn = getTransactionUsageInfoTracker as jest.Mock;
      getTransactionUsageInfoTrackerFn.mockReturnValueOnce(contextVal);

      const revertDailyUsageToZeroFn = transactionUsageRepoService.revertDailyUsageToZero as jest.Mock;
      revertDailyUsageToZeroFn.mockImplementationOnce(_ => {});

      const revertDailyUsageFn = transactionUsageRepoService.revertDailyUsage as jest.Mock;
      revertDailyUsageFn.mockImplementationOnce(_ => {});

      await handlerInstance.processReversalBasedOnContext();

      expect(revertDailyUsageFn).not.toBeCalledWith();
      expect(revertDailyUsageToZeroFn).not.toBeCalledWith();
    });

    it(`
      GIVEN 
      - transaction limit info exist in the context
      - category is ATM withdrawal
      - dailyAmountSumSoFar <= transactionSumAmount; 
          transactionUsageRepoService.revertDailyUsageToZero return non-null value
      THEN 
      - ensure the transactionUsageRepoService.revertDailyUsageToZero called
      - ensure the transactionUsageRepoService.revertDailyUsage not called
    `, async () => {
      const contextVal: TransactionUsageContext = {
        customerId: 'custO1',
        accountNo: 'acc01',
        transactionCategory: limitCategory,
        currentTransactionAmount: 1000000,
        limitGroupCode: faker.random.alphaNumeric(5),
        paymentServiceCode: faker.random.alphaNumeric(5)
      };

      const getTransactionUsageInfoTrackerFn = getTransactionUsageInfoTracker as jest.Mock;
      getTransactionUsageInfoTrackerFn.mockReturnValueOnce(contextVal);

      const revertDailyUsageToZeroFn = transactionUsageRepoService.revertDailyUsageToZero as jest.Mock;
      revertDailyUsageToZeroFn.mockResolvedValueOnce({
        customerId: contextVal.customerId,
        accountNo: contextVal.accountNo,
        dailyUsages: {
          atmWithdrawal: {
            amount: 0
          }
        }
      });

      const revertDailyUsageFn = transactionUsageRepoService.revertDailyUsage as jest.Mock;
      revertDailyUsageFn.mockImplementationOnce(_ => {});

      await handlerInstance.processReversalBasedOnContext();

      expect(revertDailyUsageToZeroFn).toBeCalledTimes(1);
      expect(revertDailyUsageFn).not.toBeCalledWith();
    });

    it(`
    GIVEN 
    - transaction limit info exist in the context
    - category is ATM withdrawal
    - dailyAmountSumSoFar > transactionSumAmount; 
        transactionUsageRepoService.revertDailyUsageToZero return null value
    THEN 
    - ensure the transactionUsageRepoService.revertDailyUsageToZero called
    - ensure the transactionUsageRepoService.revertDailyUsage called
  `, async () => {
      const contextVal: TransactionUsageContext = {
        customerId: 'custO1',
        accountNo: 'acc01',
        transactionCategory: limitCategory,
        currentTransactionAmount: 1000000,
        limitGroupCode: faker.random.alphaNumeric(5),
        paymentServiceCode: faker.random.alphaNumeric(5)
      };

      const getTransactionUsageInfoTrackerFn = getTransactionUsageInfoTracker as jest.Mock;
      getTransactionUsageInfoTrackerFn.mockReturnValueOnce(contextVal);

      const revertDailyUsageToZeroFn = transactionUsageRepoService.revertDailyUsageToZero as jest.Mock;
      revertDailyUsageToZeroFn.mockResolvedValueOnce(null);

      const revertDailyUsageFn = transactionUsageRepoService.revertDailyUsage as jest.Mock;
      revertDailyUsageFn.mockResolvedValueOnce({
        customerId: contextVal.customerId,
        accountNo: contextVal.accountNo,
        dailyUsages: {
          atmWithdrawal: {
            amount: 10000000
          }
        }
      });

      await handlerInstance.processReversalBasedOnContext();

      expect(revertDailyUsageToZeroFn).toBeCalledTimes(1);
      expect(revertDailyUsageFn).toBeCalledTimes(1);
    });
  });
});
