import { isFeatureEnabled } from '../../../../common/featureFlag';
import { TransactionUsageAtmWithdrawalHandler } from '../../../atm/withdrawal/transactionUsageAtmWithdrawalHandler';

jest.mock('../../../../common/featureFlag');

const setupFeatureFlag = ({ dailyLimitAtmWithdrawalConfigFlag }) => {
  const confIsFeatureEnabled = isFeatureEnabled as jest.Mock;
  confIsFeatureEnabled.mockImplementationOnce(flags => {
    switch (flags) {
      case 'enableDailyLimitAtmWithdrawalPocketLevelValidation':
        return dailyLimitAtmWithdrawalConfigFlag;
      default:
        return false;
    }
  });
};

describe('transactionUsageAtmWithdrawalHandler [featureFlag]', () => {
  afterEach(() => {
    jest.resetAllMocks();
    expect.hasAssertions();
  });

  describe(`isFeatureFlagEnabled`, () => {
    let handlerInstance: TransactionUsageAtmWithdrawalHandler;
    beforeEach(() => {
      handlerInstance = new TransactionUsageAtmWithdrawalHandler();
    });

    it(`
        GIVEN ATM-WITHDRAWAL daily limit pocket validation flag is set to true,
        THEN return true
      `, () => {
      setupFeatureFlag({ dailyLimitAtmWithdrawalConfigFlag: true });

      const result = handlerInstance.isFeatureFlagEnabled();

      expect(result).toEqual(true);
    });

    it(`
        GIVEN ATM-WITHDRAWAL daily limit pocket validation flag is set to false,
        THEN return false
      `, () => {
      setupFeatureFlag({ dailyLimitAtmWithdrawalConfigFlag: false });

      const result = handlerInstance.isFeatureFlagEnabled();

      expect(result).toEqual(false);
    });
  });
});
