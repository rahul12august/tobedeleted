import faker from 'faker';
import { getTransactionUsageInfoTracker } from '../../../../common/contextHandler';
import { ERROR_CODE } from '../../../../common/errors';

import { ILimitGroup } from '../../../../configuration/configuration.type';
import { AppError } from '../../../../errors/AppError';
import { ATM_WITHDRAWAL_JAGO } from '../../../../transaction/transaction.category.enum';
import { RuleConditionState } from '../../../../transaction/transaction.conditions.enum';
import { ConfirmTransactionStatus } from '../../../../transaction/transaction.enum';
import { ITransactionModel } from '../../../../transaction/transaction.model';
import { IConfirmTransactionInput } from '../../../../transaction/transaction.type';
import { getTransactionModelFullData } from '../../../../transaction/__mocks__/transaction.data';
import { TransactionUsageAtmWithdrawalHandler } from '../../../atm/withdrawal/transactionUsageAtmWithdrawalHandler';
import { TransactionUsageAtmWithdrawalManager } from '../../../atm/withdrawal/transactionUsageAtmWithdrawalManager';
import {
  TransactionUsageCategory,
  reversalTransactionConfirmationResponseCode,
  TransactionUsageGroupCode
} from '../../../transactionUsage.enum';
import transactionUsageService from '../../../transactionUsage.service';
import {
  IPocketLimitPreValidationResponse,
  IValidatedTransactionAmountDailyLimitResponse,
  TransactionUsageContext
} from '../../../transactionUsage.type';
import transactionUsageCommonService from '../../../transactionUsageCommon.service';
import { TransactionUsageManagerFactory } from '../../../transactionUsageManagerFactory';
import {
  getJagoAtmWithdrawalTransactionParamsSample,
  validateParamsValid
} from '../../../__mocks__/atm/withdrawal/transactionUsage.data';

jest.mock('../../../transactionUsageCommon.service');
jest.mock('../../../transactionUsageManagerFactory');
jest.mock('../../../transactionUsageManager');
jest.mock('../../../atm/withdrawal/transactionUsageAtmWithdrawalManager');
jest.mock('../../../atm/withdrawal/transactionUsageAtmWithdrawalHandler');
jest.mock('../../../../common/contextHandler');

describe('transactionUsageService [ATM-WITHDRAWAL]', () => {
  const transactionCategory = TransactionUsageCategory.ATM_WITHDRAWAL;
  const transLimitGroupCode = TransactionUsageGroupCode.ATM_WITHDRAWAL;
  const dailyLimitAmount = 15000000;

  beforeEach(() => {
    jest.resetAllMocks();
  });
  describe('pocketLevelLimitValidation', () => {
    it(`
    GIVEN transactionUsageCommonService.isToProceedWithPocketLimitValidation' toProceed == false,
    then
    - ensure it will never call validateTransactionAmountAgainstDailyLimit
  `, async () => {
      const preValidationResponse: IPocketLimitPreValidationResponse = {
        isToProceed: false
      };

      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
      isToProceedWithPocketLimitValidationFn.mockReturnValueOnce(
        preValidationResponse
      );

      const validateTransactionAmountAgainstDailyLimitFn = managerInstance.validateTransactionAmountAgainstDailyLimit as jest.Mock;
      validateTransactionAmountAgainstDailyLimitFn.mockImplementationOnce(
        _ => {}
      );

      await transactionUsageService.pocketLevelLimitValidation(
        getJagoAtmWithdrawalTransactionParamsSample()
      );

      expect(validateTransactionAmountAgainstDailyLimitFn).not.toBeCalled();
    });

    it(`
    GIVEN transactionUsageCommonService.isToProceedWithPocketLimitValidation':
      - toProceed == true,
      - transaction category is undefined
       - transaction manager factory instance exists
    then
    - ensure that it will never call validateTransactionAmountAgainstDailyLimit
  `, async () => {
      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      const preValidationResponse: IPocketLimitPreValidationResponse = {
        isToProceed: true,
        transactionUsageManagerInstance: managerInstance
      };

      const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
      isToProceedWithPocketLimitValidationFn.mockReturnValueOnce(
        preValidationResponse
      );

      const validateTransactionAmountAgainstDailyLimitFn = managerInstance.validateTransactionAmountAgainstDailyLimit as jest.Mock;
      validateTransactionAmountAgainstDailyLimitFn.mockImplementationOnce(
        _ => {}
      );

      await transactionUsageService.pocketLevelLimitValidation(
        getJagoAtmWithdrawalTransactionParamsSample()
      );

      expect(validateTransactionAmountAgainstDailyLimitFn).not.toBeCalled();
    });

    it(`
    GIVEN transactionUsageCommonService.isToProceedWithPocketLimitValidation':
      - toProceed == true,
      - transaction category is exists
       - transaction manager factory instance undefined
    then
    - ensure that it will never call validateTransactionAmountAgainstDailyLimit
  `, async () => {
      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      const preValidationResponse: IPocketLimitPreValidationResponse = {
        isToProceed: true,
        transactionUsageCategory: transactionCategory
      };

      const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
      isToProceedWithPocketLimitValidationFn.mockReturnValueOnce(
        preValidationResponse
      );

      const validateTransactionAmountAgainstDailyLimitFn = managerInstance.validateTransactionAmountAgainstDailyLimit as jest.Mock;
      validateTransactionAmountAgainstDailyLimitFn.mockImplementationOnce(
        _ => {}
      );

      await transactionUsageService.pocketLevelLimitValidation(
        getJagoAtmWithdrawalTransactionParamsSample()
      );

      expect(validateTransactionAmountAgainstDailyLimitFn).not.toBeCalled();
    });

    it(`
    GIVEN
      1. transactionUsageCommonService.isToProceedWithPocketLimitValidation':
        - toProceed == true,
        - transaction category ATM-Withdrawal
      2. transaction limit manager factory - no setup
    then
    - ensure that it throw error ERROR_CODE.TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND
  `, async () => {
      try {
        const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
        isToProceedWithPocketLimitValidationFn.mockImplementationOnce(() => {
          throw new AppError(
            ERROR_CODE.TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND
          );
        });

        await transactionUsageService.pocketLevelLimitValidation(
          getJagoAtmWithdrawalTransactionParamsSample()
        );
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND
        );
      }
    });

    it(`
    GIVEN
      - valid inputs and feature flag is enabled
      - limit group configs empty
    then
    - ensure that transactionUsageCommonService.isToProceedWithPocketLimitValidation is called
    - ensure validatedDailyLimitPArams throw ERROR_CODE.LIMIT_GROUPS_CONF_NOT_FOUND)
    - ensure validateTransactionAmountAgainstDailyLimit not called
  `, async () => {
      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
      const getLimitGroupsConfigFn = transactionUsageCommonService.getLimitGroupsConfig as jest.Mock;
      const validateDailyUsageParamsFn = managerInstance.validateDailyUsageParams as jest.Mock;
      const validateTransactionAmountAgainstDailyLimitFn = managerInstance.validateTransactionAmountAgainstDailyLimit as jest.Mock;

      try {
        const preValidationResponse: IPocketLimitPreValidationResponse = {
          isToProceed: true,
          transactionUsageCategory: TransactionUsageCategory.ATM_WITHDRAWAL,
          transactionUsageManagerInstance: managerInstance
        };

        isToProceedWithPocketLimitValidationFn.mockReturnValueOnce(
          preValidationResponse
        );

        getLimitGroupsConfigFn.mockResolvedValueOnce([]);

        validateDailyUsageParamsFn.mockImplementationOnce(() => {
          throw new AppError(ERROR_CODE.LIMIT_GROUPS_CONF_NOT_FOUND);
        });

        validateTransactionAmountAgainstDailyLimitFn.mockImplementationOnce(
          _ => {}
        );

        await transactionUsageService.pocketLevelLimitValidation(
          getJagoAtmWithdrawalTransactionParamsSample()
        );
      } catch (e) {
        expect(isToProceedWithPocketLimitValidationFn).toBeCalledTimes(1);
        expect(validateDailyUsageParamsFn).toBeCalledTimes(1);
        expect(validateTransactionAmountAgainstDailyLimitFn).not.toBeCalled();
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.LIMIT_GROUPS_CONF_NOT_FOUND
        );
      }
    });

    it(`
    GIVEN
      - valid inputs and feature flag is enabled
      - limit group configs exists
      - validatedDailyLimitParams output is valid
      - validateTransactionAmountAgainstDailyLimit output
        isValidated: false (validating category against recommended service failed)
    then
    - ensure that transactionUsageCommonService.isToProceedWithPocketLimitValidation is called
    - ensure validatedDailyLimitPArams is called
    - ensure validateTransactionAmountAgainstDailyLimit is called
    - ensure processAcceptedState not called
    - ensure processRejectedState not called
  `, async () => {
      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
      const getLimitGroupsConfigFn = transactionUsageCommonService.getLimitGroupsConfig as jest.Mock;
      const validateDailyUsageParamsFn = managerInstance.validateDailyUsageParams as jest.Mock;
      const validateTransactionAmountAgainstDailyLimitFn = managerInstance.validateTransactionAmountAgainstDailyLimit as jest.Mock;
      const processAcceptedStateFn = managerInstance.processAcceptedState as jest.Mock;
      const processRejectedStateFn = managerInstance.processRejectedState as jest.Mock;

      const atmWithdrawalLimitGroupInfo: ILimitGroup = {
        code: transLimitGroupCode,
        dailyLimitAmount
      };

      const limitGroupConfigs: ILimitGroup[] = [atmWithdrawalLimitGroupInfo];

      const validatedParams = validateParamsValid();

      const validatedTransactionResponse: IValidatedTransactionAmountDailyLimitResponse = {
        isValidated: false
      };

      const preValidationResponse: IPocketLimitPreValidationResponse = {
        isToProceed: true,
        transactionUsageCategory: TransactionUsageCategory.ATM_WITHDRAWAL,
        transactionUsageManagerInstance: managerInstance
      };

      isToProceedWithPocketLimitValidationFn.mockReturnValueOnce(
        preValidationResponse
      );

      getLimitGroupsConfigFn.mockResolvedValueOnce(limitGroupConfigs);

      validateDailyUsageParamsFn.mockReturnValueOnce(validatedParams);

      validateTransactionAmountAgainstDailyLimitFn.mockResolvedValueOnce({
        validatedTransactionResponse
      });

      processAcceptedStateFn.mockImplementationOnce(_ => {});
      processRejectedStateFn.mockImplementationOnce(_ => {});

      await transactionUsageService.pocketLevelLimitValidation(
        getJagoAtmWithdrawalTransactionParamsSample()
      );

      expect(isToProceedWithPocketLimitValidationFn).toBeCalledTimes(1);
      expect(validateDailyUsageParamsFn).toBeCalledTimes(1);
      expect(validateTransactionAmountAgainstDailyLimitFn).toBeCalledTimes(1);
      expect(processAcceptedStateFn).not.toBeCalled();
      expect(processRejectedStateFn).not.toBeCalled();
    });

    it(`
      GIVEN
        - valid inputs and feature flag is enabled
        - limit group configs exists
        - validatedDailyLimitParams output is valid
        - validateTransactionAmountAgainstDailyLimit output
          isValidated: true
          dailyAmountSoFar: undefined
          ruleConditionState: undefined
      then
      - ensure that transactionUsageCommonService.isToProceedWithPocketLimitValidation is called
      - ensure validatedDailyLimitPArams is called
      - ensure validateTransactionAmountAgainstDailyLimit is called
      - ensure processAcceptedState not called
      - ensure processRejectedState not called
    `, async () => {
      const atmWithdrawalLimitGroupInfo: ILimitGroup = {
        code: transLimitGroupCode,
        dailyLimitAmount
      };

      const limitGroupConfigs: ILimitGroup[] = [atmWithdrawalLimitGroupInfo];

      const validatedParams = validateParamsValid();

      const validatedTransactionResponse: IValidatedTransactionAmountDailyLimitResponse = {
        isValidated: true
      };

      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      const preValidationResponse: IPocketLimitPreValidationResponse = {
        isToProceed: true,
        transactionUsageCategory: TransactionUsageCategory.ATM_WITHDRAWAL,
        transactionUsageManagerInstance: managerInstance
      };

      const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
      const getLimitGroupsConfigFn = transactionUsageCommonService.getLimitGroupsConfig as jest.Mock;
      const validateDailyUsageParamsFn = managerInstance.validateDailyUsageParams as jest.Mock;
      const validateTransactionAmountAgainstDailyLimitFn = managerInstance.validateTransactionAmountAgainstDailyLimit as jest.Mock;
      const processAcceptedStateFn = managerInstance.processAcceptedState as jest.Mock;
      const processRejectedStateFn = managerInstance.processRejectedState as jest.Mock;

      isToProceedWithPocketLimitValidationFn.mockReturnValueOnce(
        preValidationResponse
      );

      getLimitGroupsConfigFn.mockResolvedValueOnce(limitGroupConfigs);

      validateDailyUsageParamsFn.mockReturnValueOnce(validatedParams);

      validateTransactionAmountAgainstDailyLimitFn.mockResolvedValueOnce({
        validatedTransactionResponse
      });

      processAcceptedStateFn.mockImplementationOnce(_ => {});
      processRejectedStateFn.mockImplementationOnce(_ => {});

      await transactionUsageService.pocketLevelLimitValidation(
        getJagoAtmWithdrawalTransactionParamsSample()
      );

      expect(isToProceedWithPocketLimitValidationFn).toBeCalledTimes(1);
      expect(validateDailyUsageParamsFn).toBeCalledTimes(1);
      expect(validateTransactionAmountAgainstDailyLimitFn).toBeCalledTimes(1);
      expect(processAcceptedStateFn).not.toBeCalled();
      expect(processRejectedStateFn).not.toBeCalled();
    });

    it(`
      GIVEN
        - valid inputs and feature flag is enabled
        - limit group configs exists
        - validatedDailyLimitParams output is valid
        - validateTransactionAmountAgainstDailyLimit output
          isValidated: true
          dailyAmountSoFar: valid amount
          ruleConditionState: rejected
      then
      - ensure that transactionUsageCommonService.isToProceedWithPocketLimitValidation is called
      - ensure validatedDailyLimitPArams is called
      - ensure validateTransactionAmountAgainstDailyLimit is called
      - ensure processAcceptedState not called
      - ensure processRejectedState called
    `, async () => {
      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;

      const getLimitGroupsConfigFn = transactionUsageCommonService.getLimitGroupsConfig as jest.Mock;
      const validateDailyUsageParamsFn = managerInstance.validateDailyUsageParams as jest.Mock;
      const validateTransactionAmountAgainstDailyLimitFn = managerInstance.validateTransactionAmountAgainstDailyLimit as jest.Mock;
      const processAcceptedStateFn = managerInstance.processAcceptedState as jest.Mock;
      const processRejectedStateFn = managerInstance.processRejectedState as jest.Mock;

      const dailyAmountSoFar = 1000000;
      const resultState = RuleConditionState.DECLINED;
      const atmWithdrawalLimitGroupInfo: ILimitGroup = {
        code: transLimitGroupCode,
        dailyLimitAmount
      };

      const limitGroupConfigs: ILimitGroup[] = [atmWithdrawalLimitGroupInfo];
      const validatedParams = validateParamsValid();
      const preValidationResponse: IPocketLimitPreValidationResponse = {
        isToProceed: true,
        transactionUsageCategory: TransactionUsageCategory.ATM_WITHDRAWAL,
        transactionUsageManagerInstance: managerInstance
      };

      isToProceedWithPocketLimitValidationFn.mockReturnValueOnce(
        preValidationResponse
      );

      const validatedTransactionResponse: IValidatedTransactionAmountDailyLimitResponse = {
        isValidated: true,
        dailyAmountSoFar,
        ruleConditionStateResult: resultState
      };

      isToProceedWithPocketLimitValidationFn.mockReturnValueOnce(
        preValidationResponse
      );

      getLimitGroupsConfigFn.mockResolvedValueOnce(limitGroupConfigs);

      validateDailyUsageParamsFn.mockReturnValueOnce(validatedParams);

      validateTransactionAmountAgainstDailyLimitFn.mockResolvedValueOnce(
        validatedTransactionResponse
      );
      processAcceptedStateFn.mockImplementationOnce(_ => {});
      processRejectedStateFn.mockImplementationOnce(_ => {});

      await transactionUsageService.pocketLevelLimitValidation(
        getJagoAtmWithdrawalTransactionParamsSample()
      );

      expect(isToProceedWithPocketLimitValidationFn).toBeCalledTimes(1);

      expect(validateDailyUsageParamsFn).toBeCalledTimes(1);
      expect(validateTransactionAmountAgainstDailyLimitFn).toBeCalledTimes(1);
      expect(processAcceptedStateFn).not.toBeCalled();
      expect(processRejectedStateFn).toBeCalledTimes(1);
    });

    it(`
    GIVEN
      - valid inputs and feature flag is enabled
      - limit group configs exists
      - validatedDailyLimitParams output is valid
      - validateTransactionAmountAgainstDailyLimit output
        isValidated: true
        dailyAmountSoFar: valid amount
        ruleConditionState: accepted
    then
    - ensure that transactionUsageCommonService.isToProceedWithPocketLimitValidation is called
    - ensure validatedDailyLimitPArams is called
    - ensure validateTransactionAmountAgainstDailyLimit is called
    - ensure processAcceptedState  called
    - ensure processRejectedState not called
  `, async () => {
      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
      const getLimitGroupsConfigFn = transactionUsageCommonService.getLimitGroupsConfig as jest.Mock;
      const validateDailyUsageParamsFn = managerInstance.validateDailyUsageParams as jest.Mock;
      const validateTransactionAmountAgainstDailyLimitFn = managerInstance.validateTransactionAmountAgainstDailyLimit as jest.Mock;
      const processAcceptedStateFn = managerInstance.processAcceptedState as jest.Mock;
      const processRejectedStateFn = managerInstance.processRejectedState as jest.Mock;

      const dailyAmountSoFar = 1000000;
      const resultState = RuleConditionState.ACCEPTED;

      const atmWithdrawalLimitGroupInfo: ILimitGroup = {
        code: transLimitGroupCode,
        dailyLimitAmount
      };

      const limitGroupConfigs: ILimitGroup[] = [atmWithdrawalLimitGroupInfo];

      const validatedParams = validateParamsValid();

      const preValidationResponse: IPocketLimitPreValidationResponse = {
        isToProceed: true,
        transactionUsageCategory: TransactionUsageCategory.ATM_WITHDRAWAL,
        transactionUsageManagerInstance: managerInstance
      };

      isToProceedWithPocketLimitValidationFn.mockReturnValueOnce(
        preValidationResponse
      );

      const validatedTransactionResponse: IValidatedTransactionAmountDailyLimitResponse = {
        isValidated: true,
        dailyAmountSoFar,
        ruleConditionStateResult: resultState
      };

      isToProceedWithPocketLimitValidationFn.mockReturnValueOnce(
        preValidationResponse
      );

      getLimitGroupsConfigFn.mockResolvedValueOnce(limitGroupConfigs);

      validateDailyUsageParamsFn.mockReturnValueOnce(validatedParams);

      validateTransactionAmountAgainstDailyLimitFn.mockResolvedValueOnce(
        validatedTransactionResponse
      );

      processAcceptedStateFn.mockImplementationOnce(_ => {});
      processRejectedStateFn.mockImplementationOnce(_ => {});

      await transactionUsageService.pocketLevelLimitValidation(
        getJagoAtmWithdrawalTransactionParamsSample()
      );

      expect(isToProceedWithPocketLimitValidationFn).toBeCalledTimes(1);

      expect(validateDailyUsageParamsFn).toBeCalledTimes(1);
      expect(validateTransactionAmountAgainstDailyLimitFn).toBeCalledTimes(1);
      expect(processAcceptedStateFn).toBeCalledTimes(1);
      expect(processRejectedStateFn).not.toBeCalled();
    });
  });

  describe('mapTransactionInfoToTransactionUsageInfoContext', () => {
    it(`
          GIVEN transaction is null
          THEN
            - ensure transactionUsageCommonService.isToProceedWithPocketLimitValidation is never being called
        `, async () => {
      const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
      isToProceedWithPocketLimitValidationFn.mockImplementationOnce(_ => {});

      await transactionUsageService.mapTransactionInfoToTransactionUsageInfoContext(
        null
      );

      expect(isToProceedWithPocketLimitValidationFn).not.toBeCalled();
    });

    it(`
        GIVEN paymentServiceCode is undefined
        THEN
          - ensure transactionUsageCommonService.isToProceedWithPocketLimitValidation is never being called

      `, async () => {
      const transModel = getTransactionModelFullData() as ITransactionModel;
      transModel.paymentServiceCode = undefined;

      const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
      isToProceedWithPocketLimitValidationFn.mockImplementationOnce(_ => {});

      await transactionUsageService.mapTransactionInfoToTransactionUsageInfoContext(
        transModel
      );

      expect(isToProceedWithPocketLimitValidationFn).not.toBeCalled();
    });

    it(`
          GIVEN customerId is undefined
          THEN
            - ensure ctory.getTransactionUsageManagerInstance is never being called

        `, async () => {
      const transModel = getTransactionModelFullData() as ITransactionModel;
      transModel.paymentServiceCode = ATM_WITHDRAWAL_JAGO.paymentServiceCode;
      transModel.sourceCustomerId = undefined;

      const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
      isToProceedWithPocketLimitValidationFn.mockImplementationOnce(_ => {});

      await transactionUsageService.mapTransactionInfoToTransactionUsageInfoContext(
        transModel
      );

      expect(isToProceedWithPocketLimitValidationFn).not.toBeCalled();
    });

    it(`
          GIVEN accountNo is undefined
          THEN
            - ensure transactionUsageCommonService.isToProceedWithPocketLimitValidation is never being called

        `, async () => {
      const transModel = getTransactionModelFullData() as ITransactionModel;
      transModel.paymentServiceCode = ATM_WITHDRAWAL_JAGO.paymentServiceCode;
      transModel.sourceAccountNo = undefined;

      const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
      isToProceedWithPocketLimitValidationFn.mockImplementationOnce(_ => {});

      await transactionUsageService.mapTransactionInfoToTransactionUsageInfoContext(
        transModel
      );

      expect(isToProceedWithPocketLimitValidationFn).not.toBeCalled();
    });

    it(`
        GIVEN
          - input is valid
          - transactionUsageCommonService.isToProceedWithPocketLimitValidation:
            - isToProceed = false
        THEN
          - ensure transactionUsageCommonService.isToProceedWithPocketLimitValidation is called
          - ensure saveDailyLimitContextFn is never being called
      `, async () => {
      const transModel = getTransactionModelFullData() as ITransactionModel;
      transModel.paymentServiceCode = 'not_inclusive_psc';

      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      const saveDailyLimitContextFn = managerInstance.saveDailyLimitContext as jest.Mock;
      saveDailyLimitContextFn.mockImplementationOnce(() => {});

      const preProcessValidationResponse: IPocketLimitPreValidationResponse = {
        isToProceed: false
      };

      const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
      isToProceedWithPocketLimitValidationFn.mockReturnValueOnce(
        preProcessValidationResponse
      );

      await transactionUsageService.mapTransactionInfoToTransactionUsageInfoContext(
        transModel
      );

      expect(isToProceedWithPocketLimitValidationFn).toBeCalledTimes(1);
      expect(saveDailyLimitContextFn).not.toBeCalled();
    });

    it(`
        GIVEN
          - input is valid
          - transactionUsageCommonService.isToProceedWithPocketLimitValidation:
            - isToProceed = true
            - transactionUsageCategory is undefined
        THEN
        - ensure transactionUsageCommonService.isToProceedWithPocketLimitValidation is called
        - ensure  saveDailyLimitContextFn is never being called
      `, async () => {
      const transModel = getTransactionModelFullData() as ITransactionModel;
      transModel.paymentServiceCode = ATM_WITHDRAWAL_JAGO.paymentServiceCode;

      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      const preValidationResponse: IPocketLimitPreValidationResponse = {
        isToProceed: true,
        transactionUsageCategory: undefined
      };

      const saveDailyLimitContextFn = managerInstance.saveDailyLimitContext as jest.Mock;
      saveDailyLimitContextFn.mockImplementationOnce(() => {});

      const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
      isToProceedWithPocketLimitValidationFn.mockReturnValueOnce(
        preValidationResponse
      );

      await transactionUsageService.mapTransactionInfoToTransactionUsageInfoContext(
        transModel
      );

      expect(isToProceedWithPocketLimitValidationFn).toBeCalledTimes(1);
      expect(saveDailyLimitContextFn).not.toBeCalled();
    });

    it(`
        GIVEN
          - input is valid
          - transactionUsageCommonService.isToProceedWithPocketLimitValidation:
            - isToProceed = true
            - transactionUsageCategory is no_setup_manager
          - but TransactionUsageManagerFactory.getTransactionUsageManagerInstance for some reason doesn't have the implementation
        THEN
        - ensure that it throw error ERROR_CODE.TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND
        `, async () => {
      try {
        const transModel = getTransactionModelFullData() as ITransactionModel;
        transModel.paymentServiceCode = ATM_WITHDRAWAL_JAGO.paymentServiceCode;

        const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
        isToProceedWithPocketLimitValidationFn.mockImplementationOnce(() => {
          throw new AppError(
            ERROR_CODE.TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND
          );
        });

        await transactionUsageService.mapTransactionInfoToTransactionUsageInfoContext(
          transModel
        );
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND
        );
      }
    });

    it(`
      GIVEN
        - input is valid
        - transactionUsageCommonService.isToProceedWithPocketLimitValidation:
          - isToProceed = true
          - transactionUsageCategory is ATM-WITHDRAWAL
        - TransactionUsageManagerFactory.getTransactionUsageManagerInstance result in atmWithdrawalHandler instance
        - but the feature flag is true
      THEN
      - ensure transactionUsageCommonService.isToProceedWithPocketLimitValidation is called
      - ensure managerInstance.saveDailyLimitContext is being called
    `, async () => {
      const transModel = getTransactionModelFullData() as ITransactionModel;
      transModel.paymentServiceCode = ATM_WITHDRAWAL_JAGO.paymentServiceCode;

      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      const preValidationResponse: IPocketLimitPreValidationResponse = {
        isToProceed: true,
        transactionUsageCategory: TransactionUsageCategory.ATM_WITHDRAWAL,
        transactionUsageManagerInstance: managerInstance
      };

      const isToProceedWithPocketLimitValidationFn = transactionUsageCommonService.isToProceedWithPocketLimitValidation as jest.Mock;
      isToProceedWithPocketLimitValidationFn.mockReturnValueOnce(
        preValidationResponse
      );

      const saveDailyLimitToContextFn = managerInstance.saveDailyLimitContext as jest.Mock;
      saveDailyLimitToContextFn.mockImplementationOnce(_ => {});

      await transactionUsageService.mapTransactionInfoToTransactionUsageInfoContext(
        transModel
      );

      expect(isToProceedWithPocketLimitValidationFn).toBeCalledTimes(1);
      expect(saveDailyLimitToContextFn).toBeCalledTimes(1);
    });
  });

  describe('processReversalPocketLimitByTransactionBasedOnContext', () => {
    it(`
          GIVEN context information is undefined
          Then
            - ensure that is should not call TransactionUsageManagerFactory.getTransactionUsageManagerInstance
        `, () => {
      const getTransactionUsageInfoTrackerFn = getTransactionUsageInfoTracker as jest.Mock;
      getTransactionUsageInfoTrackerFn.mockReturnValueOnce(undefined);

      const getTransactionUsageManagerInstanceFn = TransactionUsageManagerFactory.getTransactionUsageManagerInstance as jest.Mock;
      getTransactionUsageManagerInstanceFn.mockImplementationOnce(_ => {});

      transactionUsageService.processReversalPocketLimitByTransactionBasedOnContext();

      expect(getTransactionUsageManagerInstanceFn).not.toBeCalled();
    });

    it(`
          GIVEN
          - context information exist
          - TransactionUsageManagerFactory.getTransactionUsageManagerInstance does not have implementation for the category
         Then
          - ensure that it throw error ERROR_CODE.TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND
      `, () => {
      const contextVal: TransactionUsageContext = {
        customerId: 'custO1',
        accountNo: 'acc01',
        transactionCategory: TransactionUsageCategory.ATM_WITHDRAWAL,
        currentTransactionAmount: 1000000,
        limitGroupCode: 'L015',
        paymentServiceCode: 'JAGOATM_CASH_WITHDRAW'
      };
      try {
        const getTransactionUsageInfoTrackerFn = getTransactionUsageInfoTracker as jest.Mock;
        getTransactionUsageInfoTrackerFn.mockReturnValueOnce(contextVal);

        const getTransactionUsageManagerInstanceFn = TransactionUsageManagerFactory.getTransactionUsageManagerInstance as jest.Mock;

        getTransactionUsageManagerInstanceFn.mockImplementationOnce(() => {
          throw new AppError(
            ERROR_CODE.TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND
          );
        });

        transactionUsageService.processReversalPocketLimitByTransactionBasedOnContext();
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND
        );
      }
    });

    it(`
          GIVEN
          - context information exist
          - TransactionUsageManagerFactory.getTransactionUsageManagerInstance has the implementation
          - feature flag is true
          Then
          - ensure that TransactionUsageManagerFactory.getTransactionUsageManagerInstance is called
          - ensure that processReversalBasedOnContext is called
    `, () => {
      const contextVal: TransactionUsageContext = {
        customerId: 'custO1',
        accountNo: 'acc01',
        transactionCategory: TransactionUsageCategory.ATM_WITHDRAWAL,
        currentTransactionAmount: 1000000,
        limitGroupCode: 'L015',
        paymentServiceCode: 'JAGOATM_CASH_WITHDRAW'
      };

      const getTransactionUsageInfoTrackerFn = getTransactionUsageInfoTracker as jest.Mock;
      getTransactionUsageInfoTrackerFn.mockReturnValueOnce(contextVal);

      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      const getTransactionUsageManagerInstanceFn = TransactionUsageManagerFactory.getTransactionUsageManagerInstance as jest.Mock;
      getTransactionUsageManagerInstanceFn.mockReturnValueOnce(managerInstance);

      const processReversalBasedOnContextFn = managerInstance.processReversalBasedOnContext as jest.Mock;
      processReversalBasedOnContextFn.mockImplementationOnce(_ => {});

      transactionUsageService.processReversalPocketLimitByTransactionBasedOnContext();

      expect(getTransactionUsageManagerInstanceFn).toBeCalledTimes(1);

      expect(processReversalBasedOnContextFn).toBeCalledTimes(1);
    });
  });

  describe('processReversalPocketLimitByTransactionBasedOnContext', () => {
    const getSampleInputTransactionUsageInfoContextByTransConfirmation = (
      responseCode: ConfirmTransactionStatus
    ): IConfirmTransactionInput => {
      return {
        externalId: faker.random.uuid(),
        accountNo: faker.random.alphaNumeric(10),
        responseCode: responseCode,
        amount: faker.random.number({ min: 7, max: 8 })
      };
    };

    const getNonSuccessFullInputTransactionUsageInfoContextByTransConfirmation = (): IConfirmTransactionInput[] => {
      const result: IConfirmTransactionInput[] = [];
      for (
        let i = 0;
        i < reversalTransactionConfirmationResponseCode.length;
        i++
      ) {
        result[
          i
        ] = getSampleInputTransactionUsageInfoContextByTransConfirmation(
          reversalTransactionConfirmationResponseCode[i]
        );
      }
      return result;
    };

    it.each(
      getNonSuccessFullInputTransactionUsageInfoContextByTransConfirmation()
    )(
      `
        GIVEN input code is included within reversal list
        Then
          ensure that processReversalBasedOnContextFn is called
      `,
      input => {
        const contextVal = {
          customerId: faker.random.alphaNumeric(10),
          accountNo: input.accountNo,
          transactionCategory: TransactionUsageCategory.ATM_WITHDRAWAL,
          currentTransactionAmount: input.amount
        };

        const getTransactionUsageInfoTrackerFn = getTransactionUsageInfoTracker as jest.Mock;
        getTransactionUsageInfoTrackerFn.mockReturnValueOnce(contextVal);

        const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
        const managerInstance = new TransactionUsageAtmWithdrawalManager(
          handlerInstance
        );

        const getTransactionUsageManagerInstanceFn = TransactionUsageManagerFactory.getTransactionUsageManagerInstance as jest.Mock;
        getTransactionUsageManagerInstanceFn.mockReturnValueOnce(
          managerInstance
        );

        const processReversalBasedOnContextFn = managerInstance.processReversalBasedOnContext as jest.Mock;
        processReversalBasedOnContextFn.mockImplementationOnce(_ => {});

        transactionUsageService.processReversalPocketLimitByTransactionConfirmationInput(
          input
        );

        expect(getTransactionUsageManagerInstanceFn).toBeCalledTimes(1);
        expect(processReversalBasedOnContextFn).toBeCalledTimes(1);
      }
    );

    it(`
        GIVEN input code is not included within reversal list
        Then
          ensure that processReversalPocketLimitByTransactionBasedOnContext is not called
      `, () => {
      const processReversalPocketLimitByTransactionBasedOnContextFn = jest.spyOn(
        transactionUsageService,
        'processReversalPocketLimitByTransactionBasedOnContext'
      );

      // processReversalPocketLimitByTransactionBasedOnContextFn.mockReturnValueOnce();
      transactionUsageService.processReversalPocketLimitByTransactionConfirmationInput(
        getSampleInputTransactionUsageInfoContextByTransConfirmation(
          ConfirmTransactionStatus.SUCCESS
        )
      );

      expect(
        processReversalPocketLimitByTransactionBasedOnContextFn
      ).not.toBeCalled();
    });
  });
});
