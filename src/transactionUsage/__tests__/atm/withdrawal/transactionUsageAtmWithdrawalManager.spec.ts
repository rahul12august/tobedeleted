import { TransactionUsageAtmWithdrawalHandler } from '../../../atm/withdrawal/transactionUsageAtmWithdrawalHandler';
import { TransactionUsageAtmWithdrawalManager } from '../../../atm/withdrawal/transactionUsageAtmWithdrawalManager';

import { validateParamsValid } from '../../../__mocks__/atm/withdrawal/transactionUsage.data';
import _ from 'lodash';

import transactionUsageRepoService from '../../../transactionUsageRepo.service';
import { RuleConditionState } from '../../../../transaction/transaction.conditions.enum';
import transactionConditionHelperService from '../../../../transaction/transaction.conditions.helper';
import { RuleConditionStatus } from '../../../../transaction/transaction.type';
import {
  TransactionUsageCategory,
  TransactionUsageGroupCode
} from '../../../transactionUsage.enum';
import { IDailyUsageParamValidatedResponse } from '../../../transactionUsage.type';
import { PaymentServiceTypeEnum } from '@dk/module-common';
import faker from 'faker';
import { ATM_WITHDRAWAL_JAGO } from '../../../../transaction/transaction.category.enum';
import moment from 'moment';
import { ITransactionUsageModel } from '../../../transactionUsage.model';

jest.mock('../../../transactionUsageRepo.service');
jest.mock('../../../../transaction/transaction.conditions.helper');

describe('TransactionUsageAtmWithdrawalManager', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('validateDailyUsageParams', () => {
    it('should call handler instance', async () => {
      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();

      const validateDailyLimitFn = jest.spyOn(
        handlerInstance,
        'validateDailyUsageParams'
      );
      validateDailyLimitFn.mockImplementationOnce(_ => {});

      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      await managerInstance.validateDailyUsageParams(validateParamsValid());

      expect(validateDailyLimitFn).toBeCalledTimes(1);
    });
  });

  describe('processReversalBasedOnContext', () => {
    it('should call handlerInstance.processReversalBasedOnContext', async () => {
      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const processReversalBasedOnContextFn = jest.spyOn(
        handlerInstance,
        'processReversalBasedOnContext'
      );
      processReversalBasedOnContextFn.mockImplementationOnce(_ => {});

      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      await managerInstance.processReversalBasedOnContext();

      expect(processReversalBasedOnContextFn).toBeCalledTimes(1);
    });
  });

  describe('isFeatureFlagEnabled', () => {
    it('should call handlerInstance.isFeatureFlagEnabled', async () => {
      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();

      const isFeatureFlagEnabledFn = jest.spyOn(
        handlerInstance,
        'isFeatureFlagEnabled'
      );
      isFeatureFlagEnabledFn.mockImplementationOnce(_ => {});

      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      await managerInstance.isFeatureFlagEnabled();

      expect(isFeatureFlagEnabledFn).toBeCalledTimes(1);
    });
  });

  describe('saveDailyLimitContext', () => {
    it('should call handlerInstance.saveDailyLimitContext', async () => {
      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();

      const saveDailyLimitContextFn = jest.spyOn(
        handlerInstance,
        'saveDailyLimitToContext'
      );
      saveDailyLimitContextFn.mockImplementationOnce(_ => {});

      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      await managerInstance.saveDailyLimitContext({
        accountNo: faker.random.alphaNumeric(10),
        customerId: faker.random.alphaNumeric(10),
        transactionAmount: 1000000,
        transactionUsageCategory: TransactionUsageCategory.ATM_WITHDRAWAL,
        limitGroupCode: faker.random.alphaNumeric(5),
        paymentServiceCode: faker.random.alphaNumeric(5)
      });

      expect(saveDailyLimitContextFn).toBeCalledTimes(1);
    });
  });

  describe('validateTransactionAmountAgainstDailyLimit', () => {
    const getValidatedParams = (): IDailyUsageParamValidatedResponse => {
      return {
        accountNo: faker.random.alphaNumeric(10),
        customerId: faker.random.alphaNumeric(10),
        debitLimitGroupCode: ATM_WITHDRAWAL_JAGO.limitGroupCode,
        debitTransactionCode: faker.random.alphaNumeric(5),
        limitGroupInfo: {
          code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
          dailyLimitAmount: 15000000
        },
        paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode,
        paymentServiceType: ATM_WITHDRAWAL_JAGO.paymentServiceType,
        transactionAmount: 100000
      };
    };

    it(`
      GIVEN  recommended service cannot be categorized as ATM Withdrawal 
        (handlerInstance.isValidTransactionCategoryByRecommendedService = false) 
      THEN
        - it should return isValidated = false
        
    `, async () => {
      const invalidValidatedParams = getValidatedParams();
      invalidValidatedParams.paymentServiceType =
        PaymentServiceTypeEnum.CASHBACK;
      const dailyAmountSoFar = 0;

      const findTransactionUsageByCustomerAndAccountFn = transactionUsageRepoService.findTransactionUsageByCustomerAndAccount as jest.Mock;
      findTransactionUsageByCustomerAndAccountFn.mockImplementationOnce(
        _ => {}
      );

      const compareAmountWithDailyLimitFn = transactionConditionHelperService.compareAmountWithDailyLimit as jest.Mock;
      compareAmountWithDailyLimitFn.mockImplementationOnce(_ => {});

      jest.mock(
        '../../../atm/withdrawal/transactionUsageAtmWithdrawalHandler',
        () => ({
          isValidTransactionCategoryByRecommendedService: jest
            .fn()
            .mockResolvedValueOnce(false),
          getDailyLimitAmountSoFar: jest
            .fn()
            .mockReturnValueOnce(dailyAmountSoFar)
        })
      );

      const managerInstance = new TransactionUsageAtmWithdrawalManager();

      const result = await managerInstance.validateTransactionAmountAgainstDailyLimit(
        invalidValidatedParams
      );

      expect(result).toEqual({
        isValidated: false
      });
    });

    it(`
      GIVEN  recommended service categorized as ATM Withdrawal 
        (handlerInstance.isValidTransactionCategoryByRecommendedService = true) 
        - no transaction_limit record exist [dailyAmountSoFar = 0]
      THEN
        - it should return {
            is Validated: true 
            + dailyAmountSoFar = 0
            + resultState = ACCEPTED
         }
        
    `, async () => {
      const validatedParams = getValidatedParams();
      const dailyAmountSoFar = 0;
      const ruleConditionStateResult = RuleConditionState.ACCEPTED;

      const findTransactionUsageByCustomerAndAccountFn = transactionUsageRepoService.findTransactionUsageByCustomerAndAccount as jest.Mock;
      findTransactionUsageByCustomerAndAccountFn.mockResolvedValueOnce(null);

      const compareAmountWithDailyLimitFn = transactionConditionHelperService.compareAmountWithDailyLimit as jest.Mock;
      compareAmountWithDailyLimitFn.mockReturnValueOnce(
        ruleConditionStateResult
      );

      jest.mock(
        '../../../atm/withdrawal/transactionUsageAtmWithdrawalHandler',
        () => ({
          isValidTransactionCategoryByRecommendedService: jest
            .fn()
            .mockResolvedValueOnce(true),
          getDailyLimitAmountSoFar: jest
            .fn()
            .mockReturnValueOnce(dailyAmountSoFar)
        })
      );

      const managerInstance = new TransactionUsageAtmWithdrawalManager();

      const result = await managerInstance.validateTransactionAmountAgainstDailyLimit(
        validatedParams
      );

      expect(result).toEqual({
        isValidated: true,
        dailyAmountSoFar,
        ruleConditionStateResult
      });
    });

    it(`
      GIVEN  recommended service categorized as ATM Withdrawal 
        (handlerInstance.isValidTransactionCategoryByRecommendedService = true) 
        - transaction_limit record exist [dailyAmountSoFar = 15000000]
        - any transaction amount will exceeds daily limit of 15 million
      THEN
        - it should return {
            is Validated: false 
            + dailyAmountSoFar = 15000000
            + resultState = TRY_TOMORROW
         }
        
    `, async () => {
      const validatedParams = getValidatedParams();
      const dailyAmountSoFar = 15000000;
      const ruleConditionStateResult = RuleConditionState.TRY_TOMORROW;
      const transDateCurrent = moment('2023-02-01T08:00:00');
      const transDatePrevious = transDateCurrent.subtract(1, 'hour');

      const existingRecord: ITransactionUsageModel = {
        accountNo: validatedParams.accountNo as string,
        customerId: validatedParams.customerId as string,
        dailyUsages: {
          atmWithdrawal: {
            amount: dailyAmountSoFar,
            lastUpdatedAt: transDatePrevious.toDate()
          }
        }
      };

      const findTransactionUsageByCustomerAndAccountFn = transactionUsageRepoService.findTransactionUsageByCustomerAndAccount as jest.Mock;
      findTransactionUsageByCustomerAndAccountFn.mockResolvedValueOnce(
        existingRecord
      );

      const compareAmountWithDailyLimitFn = transactionConditionHelperService.compareAmountWithDailyLimit as jest.Mock;
      compareAmountWithDailyLimitFn.mockReturnValueOnce(
        ruleConditionStateResult
      );

      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      jest
        .spyOn(
          handlerInstance,
          'isValidTransactionCategoryByRecommendedService'
        )
        .mockResolvedValueOnce(true);
      jest
        .spyOn(handlerInstance, 'getDailyLimitAmountSoFar')
        .mockReturnValueOnce(dailyAmountSoFar);

      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      const result = await managerInstance.validateTransactionAmountAgainstDailyLimit(
        validatedParams
      );

      expect(result).toEqual({
        isValidated: true,
        dailyAmountSoFar,
        ruleConditionStateResult
      });
    });

    it(`
    GIVEN  recommended service categorized as ATM Withdrawal 
      (handlerInstance.isValidTransactionCategoryByRecommendedService = true) 
      - transaction_limit record exist [dailyAmountSoFar = 1000000]
      - assume transaction amount is 16 million, where daily limit is 15 million
    THEN
      - it should return {
          is Validated: false 
          + dailyAmountSoFar = 1000000
          + resultState = DECLINED
       }
      
  `, async () => {
      const validatedParams = getValidatedParams();
      const dailyAmountSoFar = 1000000;
      const ruleConditionStateResult = RuleConditionState.DECLINED;
      const transDateCurrent = moment('2023-02-01T08:00:00');
      const transDatePrevious = transDateCurrent.subtract(1, 'hour');

      const existingRecord: ITransactionUsageModel = {
        accountNo: validatedParams.accountNo as string,
        customerId: validatedParams.customerId as string,
        dailyUsages: {
          atmWithdrawal: {
            amount: dailyAmountSoFar,
            lastUpdatedAt: transDatePrevious.toDate()
          }
        }
      };

      const findTransactionUsageByCustomerAndAccountFn = transactionUsageRepoService.findTransactionUsageByCustomerAndAccount as jest.Mock;
      findTransactionUsageByCustomerAndAccountFn.mockResolvedValueOnce(
        existingRecord
      );

      const compareAmountWithDailyLimitFn = transactionConditionHelperService.compareAmountWithDailyLimit as jest.Mock;
      compareAmountWithDailyLimitFn.mockReturnValueOnce(
        ruleConditionStateResult
      );

      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      jest
        .spyOn(
          handlerInstance,
          'isValidTransactionCategoryByRecommendedService'
        )
        .mockResolvedValueOnce(true);
      jest
        .spyOn(handlerInstance, 'getDailyLimitAmountSoFar')
        .mockReturnValueOnce(dailyAmountSoFar);

      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      const result = await managerInstance.validateTransactionAmountAgainstDailyLimit(
        validatedParams
      );

      expect(result).toEqual({
        isValidated: true,
        dailyAmountSoFar,
        ruleConditionStateResult
      });
    });
  });

  describe('processAcceptedState', () => {
    const getValidatedParams = (): IDailyUsageParamValidatedResponse => {
      return {
        accountNo: faker.random.alphaNumeric(10),
        customerId: faker.random.alphaNumeric(10),
        debitLimitGroupCode: ATM_WITHDRAWAL_JAGO.limitGroupCode,
        debitTransactionCode: faker.random.alphaNumeric(5),
        limitGroupInfo: {
          code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
          dailyLimitAmount: 15000000
        },
        paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode,
        paymentServiceType: ATM_WITHDRAWAL_JAGO.paymentServiceType,
        transactionAmount: 100000
      };
    };

    it(`
    GIVEN the state is ACCEPTED
    then it should call handlerinstance.acceptedState 
    `, async () => {
      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const processAcceptedStateFn = jest.spyOn(
        handlerInstance,
        'processAcceptedState'
      );
      processAcceptedStateFn.mockImplementationOnce(_ => {});

      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );
      await managerInstance.processAcceptedState({
        dailyAmountSoFar: 1000000,
        validatedParams: getValidatedParams(),
        ruleConditionStateResult: RuleConditionState.ACCEPTED
      });

      expect(processAcceptedStateFn).toBeCalledTimes(1);
    });
  });

  describe('processRejectedState', () => {
    const getValidatedParams = (): IDailyUsageParamValidatedResponse => {
      return {
        accountNo: faker.random.alphaNumeric(10),
        customerId: faker.random.alphaNumeric(10),
        debitLimitGroupCode: ATM_WITHDRAWAL_JAGO.limitGroupCode,
        debitTransactionCode: faker.random.alphaNumeric(5),
        limitGroupInfo: {
          code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
          dailyLimitAmount: 15000000
        },

        paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode,
        paymentServiceType: ATM_WITHDRAWAL_JAGO.paymentServiceType,
        transactionAmount: 100000
      };
    };

    it(` 
    GIVEN the state is non accpted,
    THEN
    ensure:
      - it called transactionConditionHelperService.constructDailyLimitEvaluatedRuleConditionStatusRefined
      - it called handlerinstance.processRejectedState
     `, async () => {
      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();

      const constructDailyLimitEvaluatedRuleConditionStatusRefinedFn = transactionConditionHelperService.constructDailyLimitEvaluatedRuleConditionStatusRefined as jest.Mock;
      constructDailyLimitEvaluatedRuleConditionStatusRefinedFn.mockReturnValueOnce(
        new RuleConditionStatus('declined')
      );
      const processRejectedStateFn = jest.spyOn(
        handlerInstance,
        'processRejectedState'
      );
      processRejectedStateFn.mockImplementationOnce(_ => {});

      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );
      await managerInstance.processRejectedState({
        dailyAmountSoFar: 15000000,
        validatedParams: getValidatedParams(),
        ruleConditionStateResult: RuleConditionState.DECLINED
      });

      expect(
        constructDailyLimitEvaluatedRuleConditionStatusRefinedFn
      ).toBeCalledTimes(1);
      expect(processRejectedStateFn).toBeCalledTimes(1);
    });
  });

  describe('setTransactionUsageHandlerInstance', () => {
    it(`ensure when getting the handler instance, it does not return null`, () => {
      const managerInstance = new TransactionUsageAtmWithdrawalManager();
      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      managerInstance.setTransactionUsageHandlerInstance(handlerInstance);

      const result = managerInstance.getTransactionUsageHandlerInstance();

      expect(result).not.toBeNull();
      expect(result).toEqual(handlerInstance);
    });
  });
});
