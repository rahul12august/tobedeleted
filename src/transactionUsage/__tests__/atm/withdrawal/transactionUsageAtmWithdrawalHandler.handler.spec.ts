import { PaymentServiceTypeEnum } from '@dk/module-common';
import faker from 'faker';
import moment from 'moment';

import { ERROR_CODE } from '../../../../common/errors';

import {
  ATM_WITHDRAWAL_INTERNATIONAL,
  ATM_WITHDRAWAL_JAGO,
  ATM_WITHDRAWAL_THIRD_PARTY_ALTO,
  ATM_WITHDRAWAL_THIRD_PARTY_ARTAJASA
} from '../../../../transaction/transaction.category.enum';

import { TransactionUsageAtmWithdrawalHandler } from '../../../atm/withdrawal/transactionUsageAtmWithdrawalHandler';

import {
  TransactionUsageCategory,
  TransactionUsageGroupCode
} from '../../../transactionUsage.enum';
import {
  IDailyUsageParamValidatedResponse,
  IEvalStatusError,
  IPocketLimitValidationRequestExt
} from '../../../transactionUsage.type';

import { ITransactionUsageModel } from '../../../transactionUsage.model';
import transactionUsageRepoService from '../../../transactionUsageRepo.service';
import _ from 'lodash';

import { AppError } from '@dk/module-common/dist/error/AppError';
import { RetryableTransferAppError } from '../../../../errors/AppError';
import { RuleConditionStatus } from '../../../../transaction/transaction.type';
import transactionProducer from '../../../../transaction/transaction.producer';
import accountRepository from '../../../../account/account.repository';
import { NotificationCode } from '../../../../transaction/transaction.producer.enum';
import {
  validateParamsAccountNoUndefined,
  validateParamsCustomerIdUndefined,
  validateParamsDebitTransactionCodeEmptyArray,
  validateParamsDebitTransactionCodeUndefined,
  validateParamsLimitGroupCodeUndefined,
  validateParamsValid
} from '../../../__mocks__/atm/withdrawal/transactionUsage.data';

import { ILimitGroup } from '../../../../configuration/configuration.type';
import transactionUsageCommonService from '../../../transactionUsageCommon.service';

jest.mock('../../../transactionUsageRepo.service');
jest.mock('../../../../transaction/transaction.producer');
jest.mock('../../../../account/account.repository');
jest.mock('../../../transactionUsageCommon.service');

const validatedParams: IDailyUsageParamValidatedResponse = {
  accountNo: faker.random.alphaNumeric(10),
  customerId: faker.random.alphaNumeric(10),
  debitLimitGroupCode: ATM_WITHDRAWAL_JAGO.limitGroupCode,
  debitTransactionCode: faker.random.alphaNumeric(5),
  limitGroupInfo: {
    code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
    dailyLimitAmount: 15000000
  },
  paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode,
  paymentServiceType: ATM_WITHDRAWAL_JAGO.paymentServiceType,
  transactionAmount: 100000
};

describe('transactionUsageAtmWithdrawalHandler [handler]', () => {
  const transactionUsageGroupCode = TransactionUsageGroupCode.ATM_WITHDRAWAL;
  const transactionUsageCategory = TransactionUsageCategory.ATM_WITHDRAWAL;
  const evalStatusErrors: IEvalStatusError = {
    errorCodeDeclined: ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT,
    errorCodeTryTomorrow: ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT
  };

  let handlerInstance;
  beforeEach(() => {
    handlerInstance = new TransactionUsageAtmWithdrawalHandler();
  });

  afterEach(() => {
    jest.resetAllMocks();
    expect.hasAssertions();
  });

  describe('TransactionUsageGroupCode', () => {
    it('should return transaction limit group code of ATM WITHDRAWAL', () => {
      const result = handlerInstance.getTransactionUsageGroupCode();
      expect(result).toEqual(transactionUsageGroupCode);
    });
  });

  describe('TransactionUsageCategory', () => {
    it('should return transaction limit category of ATM WITHDRAWAL', () => {
      const result = handlerInstance.getTransactionUsageCategory();
      expect(result).toEqual(transactionUsageCategory);
    });
  });

  describe('getEvalStatusErrors', () => {
    it(`should return 
      tryTomorrow: ERROR_CODE.ATM_WITHDRAWAL_ACCUMULATION_SUM_EXCEEDS_DAILY_LIMIT
      declined: ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT,
    `, () => {
      const result = handlerInstance.getEvalStatusErrors();
      expect(result).toEqual(evalStatusErrors);
    });
  });

  describe('validateDailyUsageParams', () => {
    test.each([
      validateParamsCustomerIdUndefined(),
      validateParamsAccountNoUndefined()
    ])(
      `
        GIVEN invalid params customerId or accountNo,
        THEN 
        - it should throw error ERROR_CODE.ATM_WITHDRAWAL_EXECUTOR_MANDATORY_INFO_NOT_FOUND`,
      async (params: IPocketLimitValidationRequestExt) => {
        try {
          const getLimitGroupsConfigFn = transactionUsageCommonService.getLimitGroupsConfig as jest.Mock;

          getLimitGroupsConfigFn.mockResolvedValueOnce([]);

          const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
          await handlerInstance.validateDailyUsageParams(params);
        } catch (e) {
          expect(e).toBeInstanceOf(AppError);
          expect((e as AppError).errorCode).toEqual(
            ERROR_CODE.ATM_WITHDRAWAL_EXECUTOR_MANDATORY_INFO_NOT_FOUND
          );
        }
      }
    );
    it(`
      GIVEN invalid params debit transaction code is empty array,
      THEN it should throw error ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE`, async () => {
      try {
        const getLimitGroupsConfigFn = transactionUsageCommonService.getLimitGroupsConfig as jest.Mock;

        getLimitGroupsConfigFn.mockResolvedValueOnce([]);

        const params = validateParamsDebitTransactionCodeEmptyArray();

        const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
        await handlerInstance.validateDailyUsageParams(params);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE
        );
      }
    });
    it(`
      GIVEN invalid params debit transaction code is undefined,
      THEN it should throw error ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE`, async () => {
      try {
        const getLimitGroupsConfigFn = transactionUsageCommonService.getLimitGroupsConfig as jest.Mock;

        getLimitGroupsConfigFn.mockResolvedValueOnce([]);

        const params = validateParamsDebitTransactionCodeUndefined();

        const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
        await handlerInstance.validateDailyUsageParams(params);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.MISSING_DEBIT_TRANSACTION_CODE
        );
      }
    });
    it(`
      GIVEN invalid params debit limit group code is undefined,
      THEN it should throw error ERROR_CODE.RECOMMENDED_LIMIT_GROUP_CODE_NOT_EXIST`, async () => {
      try {
        const getLimitGroupsConfigFn = transactionUsageCommonService.getLimitGroupsConfig as jest.Mock;
        getLimitGroupsConfigFn.mockResolvedValueOnce([]);

        const extractInfoFromTransactionCodeMappingsFn = transactionUsageCommonService.extractInfoFromTransactionCodeMappings as jest.Mock;
        extractInfoFromTransactionCodeMappingsFn.mockImplementationOnce(() => {
          throw new AppError(ERROR_CODE.RECOMMENDED_LIMIT_GROUP_CODE_NOT_EXIST);
        });

        const params = validateParamsLimitGroupCodeUndefined();

        const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
        await handlerInstance.validateDailyUsageParams(params);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.RECOMMENDED_LIMIT_GROUP_CODE_NOT_EXIST
        );
      }
    });

    it(`
      GIVEN invalid params specified limit group code is not found in limit group configs,
      THEN it should throw error ERROR_CODE.RECOMMENDED_LIMIT_GROUP_NOT_FOUND_IN_CONFIG`, async () => {
      try {
        const params = validateParamsValid();
        if (
          !_.isNil(params.recommendedService.debitTransactionCode) &&
          params.recommendedService.debitTransactionCode.length > 0
        ) {
          params.recommendedService.debitTransactionCode[0].transactionCodeInfo.limitGroupCode =
            'L99999';
        }

        const extractMappingInfoResult = {
          transactionCode: 'CWD02',
          limitGroupCode: 'L015'
        };

        const extractInfoFromTransactionCodeMappingsFn = transactionUsageCommonService.extractInfoFromTransactionCodeMappings as jest.Mock;
        extractInfoFromTransactionCodeMappingsFn.mockReturnValueOnce(
          extractMappingInfoResult
        );

        jest
          .spyOn(transactionUsageCommonService, 'getLimitGroupCodeInfo')
          .mockImplementationOnce(() => {
            throw new AppError(
              ERROR_CODE.RECOMMENDED_LIMIT_GROUP_NOT_FOUND_IN_CONFIG
            );
          });

        const getLimitGroupsConfigFn = transactionUsageCommonService.getLimitGroupsConfig as jest.Mock;
        getLimitGroupsConfigFn.mockResolvedValueOnce([]);

        const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
        await handlerInstance.validateDailyUsageParams(params);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.RECOMMENDED_LIMIT_GROUP_NOT_FOUND_IN_CONFIG
        );
      }
    });

    it(`
    GIVEN valid params,
    then it should the validated params`, async () => {
      const params = validateParamsValid();
      const pocketLimitAtmWithdrawalLimitGroup: ILimitGroup = {
        code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
        dailyLimitAmount: 150000000
      };
      params.limitGroupConfigs = [pocketLimitAtmWithdrawalLimitGroup];

      const extractMappingInfoResult = {
        transactionCode: 'CWD02',
        limitGroupCode: 'L015'
      };

      const extractInfoFromTransactionCodeMappingsFn = transactionUsageCommonService.extractInfoFromTransactionCodeMappings as jest.Mock;
      extractInfoFromTransactionCodeMappingsFn.mockReturnValueOnce(
        extractMappingInfoResult
      );

      jest
        .spyOn(transactionUsageCommonService, 'getLimitGroupCodeInfo')
        .mockReturnValueOnce(pocketLimitAtmWithdrawalLimitGroup);

      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const result = await handlerInstance.validateDailyUsageParams(params);

      expect(result).toEqual({
        accountNo: params.accountNo,
        customerId: params.customerId,
        debitLimitGroupCode: extractMappingInfoResult.limitGroupCode,
        debitTransactionCode: extractMappingInfoResult.transactionCode,
        limitGroupInfo: pocketLimitAtmWithdrawalLimitGroup,
        paymentServiceCode: 'JAGOATM_CASH_WITHDRAW',
        paymentServiceType: params.paymentServiceType,
        transactionAmount: params.transactionAmount
      });
    });
  });

  describe('isValidTransactionCategoryByRecommendedService', () => {
    it(`
      Given the recommended service it does not categorized as ATM Withdrawal category
      THEN return false
    `, () => {
      const nonAtmWithdrawalParams: IDailyUsageParamValidatedResponse = {
        customerId: faker.random.alphaNumeric(10),
        accountNo: faker.random.alphaNumeric(10),
        debitTransactionCode: faker.random.alphaNumeric(5),
        debitLimitGroupCode: ATM_WITHDRAWAL_JAGO.limitGroupCode,
        limitGroupInfo: {
          code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
          dailyLimitAmount: 15000000
        },
        transactionAmount: faker.random.number({
          min: 100000,
          max: 500000
        }),
        paymentServiceType: PaymentServiceTypeEnum.CASHBACK,
        paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode
      };

      const result = handlerInstance.isValidTransactionCategoryByRecommendedService(
        nonAtmWithdrawalParams
      );
      expect(result).toBeFalsy();
    });

    it(`
      Given the recommended service is categorized as ATM Withdrawal category
      THEN return true
    `, () => {
      const atmWithdrawalParams: IDailyUsageParamValidatedResponse = {
        customerId: faker.random.alphaNumeric(10),
        accountNo: faker.random.alphaNumeric(10),
        debitTransactionCode: faker.random.alphaNumeric(5),
        debitLimitGroupCode: ATM_WITHDRAWAL_JAGO.limitGroupCode,
        limitGroupInfo: {
          code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
          dailyLimitAmount: 15000000
        },
        transactionAmount: faker.random.number({
          min: 100000,
          max: 500000
        }),
        paymentServiceType: ATM_WITHDRAWAL_JAGO.paymentServiceType,
        paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode
      };

      const result = handlerInstance.isValidTransactionCategoryByRecommendedService(
        atmWithdrawalParams
      );
      expect(result).toBeTruthy();
    });

    it(`
      Given the recommended service is categorized as ATM Withdrawal category (jago)
      THEN return true
    `, () => {
      const atmWithdrawalParams: IDailyUsageParamValidatedResponse = {
        customerId: faker.random.alphaNumeric(10),
        accountNo: faker.random.alphaNumeric(10),
        debitTransactionCode: faker.random.alphaNumeric(5),
        debitLimitGroupCode: ATM_WITHDRAWAL_JAGO.limitGroupCode,
        limitGroupInfo: {
          code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
          dailyLimitAmount: 15000000
        },
        transactionAmount: faker.random.number({
          min: 100000,
          max: 500000
        }),
        paymentServiceType: ATM_WITHDRAWAL_JAGO.paymentServiceType,
        paymentServiceCode: ATM_WITHDRAWAL_JAGO.paymentServiceCode
      };

      const result = handlerInstance.isValidTransactionCategoryByRecommendedService(
        atmWithdrawalParams
      );
      expect(result).toBeTruthy();
    });

    it(`
      Given the recommended service is categorized as ATM Withdrawal category (alto)
      THEN return true
    `, () => {
      const atmWithdrawalParams: IDailyUsageParamValidatedResponse = {
        customerId: faker.random.alphaNumeric(10),
        accountNo: faker.random.alphaNumeric(10),
        debitTransactionCode: faker.random.alphaNumeric(5),
        debitLimitGroupCode: ATM_WITHDRAWAL_THIRD_PARTY_ALTO.limitGroupCode,
        limitGroupInfo: {
          code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
          dailyLimitAmount: 15000000
        },
        transactionAmount: faker.random.number({
          min: 100000,
          max: 500000
        }),
        paymentServiceType: ATM_WITHDRAWAL_THIRD_PARTY_ALTO.paymentServiceType,
        paymentServiceCode: ATM_WITHDRAWAL_THIRD_PARTY_ALTO.paymentServiceCode
      };

      const result = handlerInstance.isValidTransactionCategoryByRecommendedService(
        atmWithdrawalParams
      );
      expect(result).toBeTruthy();
    });

    it(`
    Given the recommended service is categorized as ATM Withdrawal category (artajasa)
    THEN return true
  `, () => {
      const atmWithdrawalParams: IDailyUsageParamValidatedResponse = {
        customerId: faker.random.alphaNumeric(10),
        accountNo: faker.random.alphaNumeric(10),
        debitTransactionCode: faker.random.alphaNumeric(5),
        debitLimitGroupCode: ATM_WITHDRAWAL_THIRD_PARTY_ARTAJASA.limitGroupCode,
        limitGroupInfo: {
          code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
          dailyLimitAmount: 15000000
        },
        transactionAmount: faker.random.number({
          min: 100000,
          max: 500000
        }),
        paymentServiceType:
          ATM_WITHDRAWAL_THIRD_PARTY_ARTAJASA.paymentServiceType,
        paymentServiceCode:
          ATM_WITHDRAWAL_THIRD_PARTY_ARTAJASA.paymentServiceCode
      };

      const result = handlerInstance.isValidTransactionCategoryByRecommendedService(
        atmWithdrawalParams
      );
      expect(result).toBeTruthy();
    });

    it(`
    Given the recommended service is categorized as ATM Withdrawal category (international)
    THEN return true
  `, () => {
      const atmWithdrawalParams: IDailyUsageParamValidatedResponse = {
        customerId: faker.random.alphaNumeric(10),
        accountNo: faker.random.alphaNumeric(10),
        debitTransactionCode: faker.random.alphaNumeric(5),
        debitLimitGroupCode: ATM_WITHDRAWAL_INTERNATIONAL.limitGroupCode,
        limitGroupInfo: {
          code: TransactionUsageGroupCode.ATM_WITHDRAWAL,
          dailyLimitAmount: 15000000
        },
        transactionAmount: faker.random.number({
          min: 100000,
          max: 500000
        }),
        paymentServiceType: ATM_WITHDRAWAL_INTERNATIONAL.paymentServiceType,
        paymentServiceCode: ATM_WITHDRAWAL_INTERNATIONAL.paymentServiceCode
      };

      const result = handlerInstance.isValidTransactionCategoryByRecommendedService(
        atmWithdrawalParams
      );
      expect(result).toBeTruthy();
    });
  });

  describe(`getDailyLimitAmountSoFar`, () => {
    beforeEach(() => {
      global.Date.now = jest.fn(() =>
        new Date('2023-02-01T12:00:00.000Z').getTime()
      );
    });

    it(`
          GIVEN existing record is null,
          then it should return 0
        `, () => {
      const result = handlerInstance.getDailyLimitAmountSoFar(null);
      expect(result).toEqual(0);
    });

    it(`
          GIVEN existing record lastUpdateDate is still within the same day as NOW
          then it should return existing record's amount from db
        `, () => {
      const transDateCurrent = moment();
      const transDatePrevious = transDateCurrent.subtract(1, 'hour');

      //it should not indicate as expired
      (transactionUsageCommonService.isDailyLimitExpired as jest.Mock).mockReturnValueOnce(
        false
      );

      const amount = 1000000;
      const result = handlerInstance.getDailyLimitAmountSoFar({
        accountNo: faker.random.alphaNumeric(10),
        customerId: faker.random.alphaNumeric(10),
        dailyUsages: {
          atmWithdrawal: {
            amount: amount,
            lastUpdatedAt: transDatePrevious.toDate()
          }
        }
      });

      expect(result).toEqual(amount);
    });

    it(`
          GIVEN existing record lastUpdateDate is >= 1 day more than NOW
          then it should return existing record's amount from db
        `, () => {
      const transDateCurrent = moment();
      const transDatePrevious = transDateCurrent.subtract(1, 'day');

      //it should not indicate as expired
      (transactionUsageCommonService.isDailyLimitExpired as jest.Mock).mockReturnValueOnce(
        true
      );

      const amount = 1000000;
      const result = handlerInstance.getDailyLimitAmountSoFar({
        accountNo: faker.random.alphaNumeric(10),
        customerId: faker.random.alphaNumeric(10),
        dailyUsages: {
          atmWithdrawal: {
            amount: amount,
            lastUpdatedAt: transDatePrevious.toDate()
          }
        }
      });

      expect(result).toEqual(0);
    });
  });

  describe('processAcceptedState', () => {
    const now = new Date();

    it(`
    GIVEN 
      - valid parameters
      - insertOrUpdateDailyUsage returns null 
    THEN
      - it should throw error ERROR_CODE.FAILED_TO_SAVE_UPDATE_TRANSACTION_USAGE
  `, async () => {
      try {
        //update transaction_limit record
        const insertOrUpdateDailyUsageFn = transactionUsageRepoService.insertOrUpdateDailyUsage as jest.Mock;
        insertOrUpdateDailyUsageFn.mockResolvedValueOnce(null);

        const saveDailyLimitToContextFn = jest.spyOn(
          handlerInstance,
          'saveDailyLimitToContext'
        );
        saveDailyLimitToContextFn.mockImplementationOnce(_ => {});

        await handlerInstance.processAcceptedState({
          validatedParams,
          dailyAmountSoFar: 1000000
        });
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.FAILED_TO_SAVE_UPDATE_TRANSACTION_USAGE
        );
      }
    });

    it(`
      GIVEN 
        - valid parameters
        - Existing record not found
      THEN
        - it should save to db
        - it should call saveDailyLimitToContext
    `, async () => {
      const dailyAmountSoFar = 0;

      const expectedUpdatedTransactionUsageRecord: ITransactionUsageModel = {
        accountNo: validatedParams.accountNo as string,
        customerId: validatedParams.customerId as string,
        dailyUsages: {
          atmWithdrawal: {
            amount: validatedParams.transactionAmount,
            lastUpdatedAt: now
          }
        }
      };

      //update transaction_limit record
      const insertOrUpdateDailyUsageFn = transactionUsageRepoService.insertOrUpdateDailyUsage as jest.Mock;
      insertOrUpdateDailyUsageFn.mockResolvedValueOnce(
        expectedUpdatedTransactionUsageRecord
      );

      const saveDailyLimitToContextFn = jest.spyOn(
        handlerInstance,
        'saveDailyLimitToContext'
      );
      saveDailyLimitToContextFn.mockImplementationOnce(_ => {});

      await handlerInstance.processAcceptedState({
        validatedParams,
        dailyAmountSoFar
      });

      expect(insertOrUpdateDailyUsageFn).toBeCalledTimes(1);
      expect(saveDailyLimitToContextFn).toBeCalledTimes(1);
    });

    it(`
        GIVEN
          - valid parameters
          - Existing record found
        THEN
        - it should save to db
        - it should call saveDailyLimitToContext
      `, async () => {
      const transDateCurrent = moment('2023-02-01T08:00:00');
      const transDatePrevious = transDateCurrent.subtract(1, 'hour');

      const existingRecord: ITransactionUsageModel = {
        accountNo: validatedParams.accountNo as string,
        customerId: validatedParams.customerId as string,
        dailyUsages: {
          atmWithdrawal: {
            amount: 1000000,
            lastUpdatedAt: transDatePrevious.toDate()
          }
        }
      };

      //just for linting
      if (_.isUndefined(existingRecord.dailyUsages.atmWithdrawal)) {
        return;
      }

      const dailyAmountSoFar = existingRecord.dailyUsages.atmWithdrawal.amount;

      const expectedUpdatedTransactionUsageRecord = Object.assign(
        {},
        existingRecord
      );

      //for linting
      if (
        _.isUndefined(
          expectedUpdatedTransactionUsageRecord.dailyUsages.atmWithdrawal
        )
      ) {
        return;
      }

      //update amount
      expectedUpdatedTransactionUsageRecord.dailyUsages.atmWithdrawal.amount =
        existingRecord.dailyUsages.atmWithdrawal.amount +
        validatedParams.transactionAmount;

      //update last updated at
      expectedUpdatedTransactionUsageRecord.dailyUsages.atmWithdrawal.lastUpdatedAt = transDateCurrent.toDate();

      const insertOrUpdateDailyUsageFn = transactionUsageRepoService.insertOrUpdateDailyUsage as jest.Mock;
      insertOrUpdateDailyUsageFn.mockResolvedValueOnce(
        expectedUpdatedTransactionUsageRecord
      );

      //context
      const saveDailyLimitToContextFn = jest.spyOn(
        handlerInstance,
        'saveDailyLimitToContext'
      );
      saveDailyLimitToContextFn.mockImplementationOnce(_ => {});

      await handlerInstance.processAcceptedState({
        validatedParams,
        dailyAmountSoFar
      });

      expect(insertOrUpdateDailyUsageFn).toBeCalledTimes(1);
      expect(saveDailyLimitToContextFn).toBeCalledTimes(1);
    });

    it(`
        GIVEN
          - valid parameters
          - Existing record found (date was yesterday)
        THEN
          -  daily amount usage to be back to 0
          - it should update existing record in db
          - it should save to context
      `, async () => {
      //const params = validateParamsValid();
      const transDateCurrent = moment('2023-02-01T08:00:00');
      const transDatePrevious = transDateCurrent.subtract(1, 'day');
      const dailyAmountSoFar = 0;

      const existingRecord: ITransactionUsageModel = {
        accountNo: validatedParams.accountNo as string,
        customerId: validatedParams.customerId as string,
        dailyUsages: {
          atmWithdrawal: {
            amount: 1000000,
            lastUpdatedAt: transDatePrevious.toDate()
          }
        }
      };

      //just for linting
      if (_.isUndefined(existingRecord.dailyUsages.atmWithdrawal)) {
        return;
      }

      const expectedUpdatedTransactionUsageRecord = Object.assign(
        {},
        existingRecord
      );

      //for linting
      if (
        _.isUndefined(
          expectedUpdatedTransactionUsageRecord.dailyUsages.atmWithdrawal
        )
      ) {
        return;
      }

      //update amount
      //reset back
      expectedUpdatedTransactionUsageRecord.dailyUsages.atmWithdrawal.amount =
        validatedParams.transactionAmount;

      //update last updated at
      expectedUpdatedTransactionUsageRecord.dailyUsages.atmWithdrawal.lastUpdatedAt = transDateCurrent.toDate();

      const insertOrUpdateDailyUsageFn = transactionUsageRepoService.insertOrUpdateDailyUsage as jest.Mock;
      insertOrUpdateDailyUsageFn.mockResolvedValueOnce(
        expectedUpdatedTransactionUsageRecord
      );

      //context
      const saveDailyLimitToContextFn = jest.spyOn(
        handlerInstance,
        'saveDailyLimitToContext'
      );
      saveDailyLimitToContextFn.mockImplementationOnce(_ => {});

      await handlerInstance.processAcceptedState({
        validatedParams,
        dailyAmountSoFar
      });

      expect(insertOrUpdateDailyUsageFn).toBeCalledTimes(1);
      expect(saveDailyLimitToContextFn).toBeCalledTimes(1);
    });
  });

  describe('processRejectedState', () => {
    it(`
      GIVEN evalStatus.errorCode is null 
      THEN 
       - errorCode will be generic: ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT
       - ensure that transactionProducer.sendFailedCashWithdrawalDailyLimit called (notification sent)
       - and it should throw RetryableTransferAppError
    `, async () => {
      try {
        const sendFailedCashWithdrawalDailyLimitFn = transactionProducer.sendFailedCashWithdrawalDailyLimit as jest.Mock;
        sendFailedCashWithdrawalDailyLimitFn.mockImplementationOnce(_ => {});

        const getAccountInfoFn = accountRepository.getAccountInfo as jest.Mock;
        getAccountInfoFn.mockResolvedValueOnce({
          customerId: validatedParams.customerId,
          pocketName: 'dummy pocket',
          amount: 150000
        });

        await handlerInstance.processRejectedState({
          validatedParams,
          dailyAmountSoFar: 1000000,
          evalStatus: new RuleConditionStatus('ERROR_DUMMY')
        });

        expect(sendFailedCashWithdrawalDailyLimitFn).toBeCalledTimes(1);
      } catch (e) {
        expect(e).toBeInstanceOf(RetryableTransferAppError);
        expect((e as RetryableTransferAppError).errorCode).toEqual(
          ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT
        );
      }
    });

    it(`
    GIVEN evalStatus.errorCode is ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT
    And account alias exist
    THEN 
     - ensure that transactionProducer.sendFailedCashWithdrawalDailyLimit called (notification sent)
     - should  aliasName, as the pocketName
     - and it should throw RetryableTransferAppError
  `, async () => {
      const sendFailedCashWithdrawalDailyLimitFn = transactionProducer.sendFailedCashWithdrawalDailyLimit as jest.Mock;
      const aliasName = 'dummy pocket';
      const dailyAmountSoFar = 1000000;
      try {
        sendFailedCashWithdrawalDailyLimitFn.mockImplementationOnce(_ => {});

        const getAccountInfoFn = accountRepository.getAccountInfo as jest.Mock;
        getAccountInfoFn.mockResolvedValueOnce({
          customerId: validatedParams.customerId,
          aliasName
        });

        await handlerInstance.processRejectedState({
          validatedParams,
          dailyAmountSoFar: 1000000,
          evalStatus: new RuleConditionStatus(
            'ERROR_DUMMY',
            ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT
          )
        });
      } catch (e) {
        expect(sendFailedCashWithdrawalDailyLimitFn).toBeCalledWith(
          {
            amount:
              validatedParams.limitGroupInfo.dailyLimitAmount -
              dailyAmountSoFar,
            customerId: validatedParams.customerId,
            pocketName: aliasName
          },
          NotificationCode.NOTIF_DAILY_LIMIT_ATM_WITHDRAWAL
        );
        expect(e).toBeInstanceOf(RetryableTransferAppError);
        expect((e as RetryableTransferAppError).errorCode).toEqual(
          ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT
        );
      }
    });

    it(`
    GIVEN evalStatus.errorCode is ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT
    AND
    account is undefined
    THEN 
     - ensure that transactionProducer.sendFailedCashWithdrawalDailyLimit called (notification sent)
     - should use accountNo, in place of aliasName, as the pocketName; when account is undefined
     - and it should throw RetryableTransferAppError
  `, async () => {
      const sendFailedCashWithdrawalDailyLimitFn = transactionProducer.sendFailedCashWithdrawalDailyLimit as jest.Mock;
      const dailyAmountSoFar = 1000000;
      try {
        sendFailedCashWithdrawalDailyLimitFn.mockImplementationOnce(_ => {});

        const getAccountInfoFn = accountRepository.getAccountInfo as jest.Mock;
        getAccountInfoFn.mockResolvedValueOnce(undefined);

        await handlerInstance.processRejectedState({
          validatedParams,
          dailyAmountSoFar,
          evalStatus: new RuleConditionStatus(
            'ERROR_DUMMY',
            ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT
          )
        });
      } catch (e) {
        expect(sendFailedCashWithdrawalDailyLimitFn).toBeCalledWith(
          {
            amount:
              validatedParams.limitGroupInfo.dailyLimitAmount -
              dailyAmountSoFar,
            customerId: validatedParams.customerId,
            pocketName: validatedParams.accountNo
          },
          NotificationCode.NOTIF_DAILY_LIMIT_ATM_WITHDRAWAL
        );
        expect(e).toBeInstanceOf(RetryableTransferAppError);
        expect((e as RetryableTransferAppError).errorCode).toEqual(
          ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT
        );
      }
    });

    it(`
    GIVEN evalStatus.errorCode is ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT
    AND
    account aliasName is undefined 
    THEN 
     - ensure that transactionProducer.sendFailedCashWithdrawalDailyLimit called (notification sent)
     - should use accountNo, in place of aliasName, as the pocketName; when account.AliasName is undefined
     - and it should throw RetryableTransferAppError
  `, async () => {
      const sendFailedCashWithdrawalDailyLimitFn = transactionProducer.sendFailedCashWithdrawalDailyLimit as jest.Mock;
      const dailyAmountSoFar = 1000000;
      try {
        sendFailedCashWithdrawalDailyLimitFn.mockImplementationOnce(_ => {});

        const getAccountInfoFn = accountRepository.getAccountInfo as jest.Mock;
        getAccountInfoFn.mockResolvedValueOnce({
          customerId: validatedParams.customerId
        });

        await handlerInstance.processRejectedState({
          validatedParams,
          dailyAmountSoFar,
          evalStatus: new RuleConditionStatus(
            'ERROR_DUMMY',
            ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT
          )
        });
      } catch (e) {
        expect(sendFailedCashWithdrawalDailyLimitFn).toBeCalledWith(
          {
            amount:
              validatedParams.limitGroupInfo.dailyLimitAmount -
              dailyAmountSoFar,
            customerId: validatedParams.customerId,
            pocketName: validatedParams.accountNo
          },
          NotificationCode.NOTIF_DAILY_LIMIT_ATM_WITHDRAWAL
        );
        expect(e).toBeInstanceOf(RetryableTransferAppError);
        expect((e as RetryableTransferAppError).errorCode).toEqual(
          ERROR_CODE.DAILY_USAGE_ATM_WITHDRAWAL_AMOUNT_EXCEEDED_DAILY_LIMIT
        );
      }
    });
  });
});
