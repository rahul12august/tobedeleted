import { ERROR_CODE } from '../../common/errors';
import { AppError } from '../../errors/AppError';
import { TransactionUsageAtmTransferManager } from '../atm/transfer/transactionUsageAtmTransferManager';
import { TransactionUsageAtmWithdrawalManager } from '../atm/withdrawal/transactionUsageAtmWithdrawalManager';
import { TransactionUsageCategory } from '../transactionUsage.enum';

import { TransactionUsageManagerFactory } from '../transactionUsageManagerFactory';

jest.mock('../atm/withdrawal/transactionUsageAtmWithdrawalManager');
jest.mock('../atm/transfer/transactionUsageAtmTransferManager');

describe('TransactionUsageManagerFactory', () => {
  beforeEach(() => {
    TransactionUsageManagerFactory.resetTransactionUsageManagerInstance();
    jest.clearAllMocks();
  });

  it(`
    Specified category does not have implementation
    then throw error ERROR_CODE.TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND
  `, () => {
    try {
      TransactionUsageManagerFactory.getTransactionUsageManagerInstance(
        'not_implemented' as TransactionUsageCategory
      );
    } catch (e) {
      expect(e).toBeInstanceOf(AppError);
      expect((e as AppError).errorCode).toEqual(
        ERROR_CODE.TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND
      );
    }
  });

  it(`
    GIVEN category is atm withdrawal
    then it should return TransactionUsageAtmWithdrawalManager
  `, () => {
    const result = TransactionUsageManagerFactory.getTransactionUsageManagerInstance(
      TransactionUsageCategory.ATM_WITHDRAWAL
    );

    expect(result).toBeInstanceOf(TransactionUsageAtmWithdrawalManager);
  });

  it(`
    GIVEN category is atm transfer
    then it should return TransactionUsageAtmWithdrawalManager
  `, () => {
    const result = TransactionUsageManagerFactory.getTransactionUsageManagerInstance(
      TransactionUsageCategory.ATM_TRANSFER
    );

    expect(result).toBeInstanceOf(TransactionUsageAtmTransferManager);
  });
});
