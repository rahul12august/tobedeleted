import { BankNetworkEnum } from '@dk/module-common';
import moment from 'moment';

import { ERROR_CODE } from '../../common/errors';
import { isFeatureEnabled } from '../../common/featureFlag';

import configurationRepository from '../../configuration/configuration.repository';
import { ILimitGroup } from '../../configuration/configuration.type';
import { AppError } from '../../errors/AppError';
import {
  ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE,
  ATM_WITHDRAWAL_JAGO
} from '../../transaction/transaction.category.enum';
import { TransactionUsageAtmTransferHandler } from '../atm/transfer/transactionUsageAtmTransferHandler';
import { TransactionUsageAtmTransferManager } from '../atm/transfer/transactionUsageAtmTransferManager';
import { TransactionUsageAtmWithdrawalHandler } from '../atm/withdrawal/transactionUsageAtmWithdrawalHandler';
import { TransactionUsageAtmWithdrawalManager } from '../atm/withdrawal/transactionUsageAtmWithdrawalManager';
import { TransactionUsageCategory } from '../transactionUsage.enum';

import transactionUsageCommonService from '../transactionUsageCommon.service';
import { TransactionUsageManagerFactory } from '../transactionUsageManagerFactory';

import { getTransactionCodesMappingJagoAtmToNonJagoAcc } from '../__mocks__/atm/transfer/transactionUsage.data';
import _ from 'lodash';
import { IExtractInfoFromTransactionCodeMappingResponse } from '../transactionUsage.type';
import limitGroup from '../../limitGroup/limitGroup';

jest.mock('../../common/featureFlag');
jest.mock('../transactionUsageManagerFactory');

const setupFeatureFlag = ({
  dailyLimitAtmWithdrawalConfigFlag,
  dailyLimitAtmTransferConfigFlag
}) => {
  const confIsFeatureEnabled = isFeatureEnabled as jest.Mock;
  confIsFeatureEnabled.mockImplementation(flags => {
    switch (flags) {
      case 'enableDailyLimitAtmWithdrawalPocketLevelValidation':
        return dailyLimitAtmWithdrawalConfigFlag;
      case 'enableDailyLimitAtmTransferPocketLevelValidation':
        return dailyLimitAtmTransferConfigFlag;
      default:
        return false;
    }
  });
};

describe('transactionUsageCommonService', () => {
  afterEach(() => {
    jest.resetAllMocks();
    expect.hasAssertions();
  });

  describe('isDailyLimitExpired', () => {
    it(`
        GIVEN the lastUpdatedAt is yesterday's date
        THEN it should return true
    `, () => {
      const todayMoment = moment();
      todayMoment.subtract(1, 'days');

      const isExpired = transactionUsageCommonService.isDailyLimitExpired(
        todayMoment.toDate()
      );

      expect(isExpired).toBeTruthy();
    });

    it(`
        GIVEN the lastUpdatedAt is today's date
        THEN it should return false
    `, () => {
      const isExpired = transactionUsageCommonService.isDailyLimitExpired(
        new Date()
      );

      expect(isExpired).toBeFalsy();
    });

    it(`
        GIVEN the lastUpdatedAt is behind the reset date
        THEN it should return true
    `, () => {
      const isExpired = transactionUsageCommonService.isDailyLimitExpired(
        new Date('2023-01-09 16:59:47.944Z')
      );

      expect(isExpired).toBeTruthy();
    });

    it(`
        GIVEN the lastUpdatedAt is ahead the reset date
        THEN it should return false
    `, () => {
      const isExpired = transactionUsageCommonService.isDailyLimitExpired(
        moment()
          .endOf('day')
          .toDate()
      );

      expect(isExpired).toBeFalsy();
    });
  });

  describe('getLimitGroupCodeInfo', () => {
    const limitGroupConfigs: ILimitGroup[] = [
      {
        code: 'L006',
        dailyLimitAmount: 15000000
      }
    ];
    it(`
        GIVEN limitGroupConfigs is empty
        THEN it should throw ERROR_CODE.LIMIT_GROUPS_CONF_NOT_FOUND
    `, () => {
      try {
        transactionUsageCommonService.getLimitGroupCodeInfo('L009', []);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.LIMIT_GROUPS_CONF_NOT_FOUND
        );
      }
    });

    it(`
        GIVEN limitGroupConfigs is not empty but limitGroupCode is not found within limitGroupConfigs
        Then it should throw ERROR_CODE.RECOMMENDED_LIMIT_GROUP_NOT_FOUND_IN_CONFIG)
    `, () => {
      try {
        transactionUsageCommonService.getLimitGroupCodeInfo(
          'L009',
          limitGroupConfigs
        );
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.RECOMMENDED_LIMIT_GROUP_NOT_FOUND_IN_CONFIG
        );
      }
    });

    it(`
        GIVEN limitGroupConfigs is not empty and limitGroupCode is found
        THEN return the info
    `, () => {
      const result = transactionUsageCommonService.getLimitGroupCodeInfo(
        'L006',
        limitGroupConfigs
      );

      expect(result.code).toEqual('L006');
      expect(result.dailyLimitAmount).toEqual(15000000);
    });
  });

  describe(`getLimitGroupsConfig`, () => {
    it(`
      GIVEN the configRepo return empty array
      THEN throw error ERROR_CODE.LIMIT_GROUPS_CONF_NOT_FOUND
    `, async () => {
      try {
        const getLimitGroupsByCodesFn = jest.spyOn(
          configurationRepository,
          'getLimitGroupsByCodes'
        );

        getLimitGroupsByCodesFn.mockResolvedValueOnce([]);

        await transactionUsageCommonService.getLimitGroupsConfig();
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.LIMIT_GROUPS_CONF_NOT_FOUND
        );
      }
    });

    it(`
      GIVEN the configRepo return valid values
      THEN
      ensured configurationRepository.getLimitGroupsByCodes called once
    `, async () => {
      const limitGroupConfigs: ILimitGroup[] = [
        {
          code: 'L006',
          dailyLimitAmount: 15000000
        }
      ];

      const getLimitGroupsByCodesFn = jest.spyOn(
        configurationRepository,
        'getLimitGroupsByCodes'
      );

      getLimitGroupsByCodesFn.mockResolvedValueOnce(limitGroupConfigs);

      const result = await transactionUsageCommonService.getLimitGroupsConfig();

      expect(getLimitGroupsByCodesFn).toBeCalledTimes(1);
      expect(result).toEqual(limitGroupConfigs);
    });
  });

  describe('isToProceedWithPocketLimitValidation', () => {
    it(`
        GIVEN payment service code is not of within the predefined list
        THEN ensure that:
        -  result.isToProceed is false
        - transactionUsageCategory to undefined
        - transactionUsageManagerInstance to be undefined
    `, () => {
      setupFeatureFlag({
        dailyLimitAtmTransferConfigFlag: false,
        dailyLimitAtmWithdrawalConfigFlag: false
      });

      const result = transactionUsageCommonService.isToProceedWithPocketLimitValidation(
        'PSC_TEST'
      );

      expect(result.isToProceed).toBe(false);
      expect(result.transactionUsageCategory).toBeUndefined();
      expect(result.transactionUsageManagerInstance).toBeUndefined();
    });

    it(`
    GIVEN 
    - payment service code is within the predefined list (ATM TRANSFER)
    - but feature flag is disabled
    THEN ensure that:
     -  the result.isToProceed is false 
     - transactionUsageCategory to undefined
     - transactionUsageManagerInstance to be undefined
`, () => {
      setupFeatureFlag({
        dailyLimitAtmTransferConfigFlag: false,
        dailyLimitAtmWithdrawalConfigFlag: false
      });

      const handlerInstance = new TransactionUsageAtmTransferHandler();
      const managerInstance = new TransactionUsageAtmTransferManager(
        handlerInstance
      );

      (TransactionUsageManagerFactory.getTransactionUsageManagerInstance as jest.Mock).mockReturnValueOnce(
        managerInstance
      );

      const result = transactionUsageCommonService.isToProceedWithPocketLimitValidation(
        ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode
      );

      expect(result.isToProceed).toBe(false);
      expect(result.transactionUsageCategory).toBeUndefined();
      expect(result.transactionUsageManagerInstance).toBeUndefined();
    });

    it(`
    GIVEN payment service code is within the predefined list (ATM TRANSFER)
    THEN ensure that:
     -  the result.isToProceed is true 
     -  transactionUsageCategory ATM-TRANSFER
`, () => {
      setupFeatureFlag({
        dailyLimitAtmTransferConfigFlag: true,
        dailyLimitAtmWithdrawalConfigFlag: false
      });

      const handlerInstance = new TransactionUsageAtmTransferHandler();
      const managerInstance = new TransactionUsageAtmTransferManager(
        handlerInstance
      );

      (TransactionUsageManagerFactory.getTransactionUsageManagerInstance as jest.Mock).mockReturnValueOnce(
        managerInstance
      );

      const result = transactionUsageCommonService.isToProceedWithPocketLimitValidation(
        ATM_TRANSFER_JAGO_ATM_JAGO_INTERCHANGE.paymentServiceCode
      );

      expect(result.isToProceed).toBe(true);
      expect(result.transactionUsageCategory).not.toBeUndefined();
      expect(result.transactionUsageCategory).toEqual(
        TransactionUsageCategory.ATM_TRANSFER
      );
      expect(result.transactionUsageManagerInstance).not.toBeUndefined();
      expect(result.transactionUsageManagerInstance).toEqual(managerInstance);
    });

    it(`
        GIVEN payment service code is within the predefined list (ATM WITHDRAWAL)
        THEN ensure that:
         -  the result.isToProceed is true 
         -  transactionUsageCategory ATM-WITHDRAWAL
    `, () => {
      setupFeatureFlag({
        dailyLimitAtmTransferConfigFlag: false,
        dailyLimitAtmWithdrawalConfigFlag: true
      });

      const handlerInstance = new TransactionUsageAtmWithdrawalHandler();
      const managerInstance = new TransactionUsageAtmWithdrawalManager(
        handlerInstance
      );

      (TransactionUsageManagerFactory.getTransactionUsageManagerInstance as jest.Mock).mockReturnValueOnce(
        managerInstance
      );

      const result = transactionUsageCommonService.isToProceedWithPocketLimitValidation(
        ATM_WITHDRAWAL_JAGO.paymentServiceCode
      );

      expect(result.isToProceed).toBe(true);
      expect(result.transactionUsageCategory).not.toBeUndefined();
      expect(result.transactionUsageCategory).toEqual(
        TransactionUsageCategory.ATM_WITHDRAWAL
      );
      expect(result.transactionUsageManagerInstance).not.toBeUndefined();
      expect(result.transactionUsageManagerInstance).toEqual(managerInstance);
    });
  });

  describe('extractInfoFromTransactionCodeMappings', () => {
    it(`
      GIVEN transactionCodeInterchange is not provided,
      THEN return the first array value of transaction code from ITransactionCodeMapping[] 
    `, () => {
      const tcMappingInfos = getTransactionCodesMappingJagoAtmToNonJagoAcc();

      const result = transactionUsageCommonService.extractInfoFromTransactionCodeMappings(
        { transactionCodesMappingInfo: tcMappingInfos }
      );

      const expectedResult: IExtractInfoFromTransactionCodeMappingResponse = {
        transactionCode: tcMappingInfos[0].transactionCode,
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        limitGroupCode: tcMappingInfos[0].transactionCodeInfo.limitGroupCode!
      };

      expect(result).toEqual(expectedResult);
    });

    it(`
    GIVEN 
      - transactionCodeInterchange provided
      - the limitGroupCode is undefined within ITransactionCodeMapping[]
    THEN return the first array value of transaction code from ITransactionCodeMapping[] 
  `, () => {
      try {
        const tcMappingInfos = getTransactionCodesMappingJagoAtmToNonJagoAcc();
        tcMappingInfos.forEach(tcMappingInfo => {
          tcMappingInfo.transactionCodeInfo.limitGroupCode = undefined;
        });

        transactionUsageCommonService.extractInfoFromTransactionCodeMappings({
          transactionCodesMappingInfo: tcMappingInfos,
          currentTransactionInterchange: BankNetworkEnum.ARTAJASA
        });
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.RECOMMENDED_LIMIT_GROUP_CODE_NOT_EXIST
        );
      }
    });

    it(`
      GIVEN 
        - transactionCodeInterchange provided
        - the interchange is undefined within ITransactionCodeMapping[]
      THEN return the first array value of transaction code from ITransactionCodeMapping[] 
    `, () => {
      const tcMappingInfos = getTransactionCodesMappingJagoAtmToNonJagoAcc();
      tcMappingInfos.forEach(tcMappingInfo => {
        tcMappingInfo.interchange = undefined;
      });

      const result = transactionUsageCommonService.extractInfoFromTransactionCodeMappings(
        {
          transactionCodesMappingInfo: tcMappingInfos,
          currentTransactionInterchange: BankNetworkEnum.ALTO
        }
      );

      const expectedResult: IExtractInfoFromTransactionCodeMappingResponse = {
        transactionCode: tcMappingInfos[0].transactionCode,
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        limitGroupCode: tcMappingInfos[0].transactionCodeInfo.limitGroupCode!
      };

      expect(result).toEqual(expectedResult);
    });

    it(`
      GIVEN 
        - transactionCodeInterchange provided
        - the currentTransactionCodeInterchange does not match any of the interchange is defined within ITransactionCodeMapping[]
      THEN return the first array value of transaction code from ITransactionCodeMapping[] 
    `, () => {
      const tcMappingInfos = getTransactionCodesMappingJagoAtmToNonJagoAcc();

      const result = transactionUsageCommonService.extractInfoFromTransactionCodeMappings(
        {
          transactionCodesMappingInfo: tcMappingInfos,
          currentTransactionInterchange: BankNetworkEnum.TAX
        }
      );

      const expectedResult: IExtractInfoFromTransactionCodeMappingResponse = {
        transactionCode: tcMappingInfos[0].transactionCode,
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        limitGroupCode: tcMappingInfos[0].transactionCodeInfo.limitGroupCode!
      };

      expect(result).toEqual(expectedResult);
    });

    it(`
    GIVEN 
      - transactionCodeInterchange provided
      - the limitGroupCode is undefined within ITransactionCodeMapping[] (not in the first array)
    THEN return the first array value of transaction code from ITransactionCodeMapping[] 
  `, () => {
      try {
        const tcMappingInfos = getTransactionCodesMappingJagoAtmToNonJagoAcc();
        tcMappingInfos.forEach(tcMappingInfo => {
          if (
            !_.isUndefined(tcMappingInfo.interchange) &&
            tcMappingInfo.interchange === BankNetworkEnum.ALTO
          ) {
            tcMappingInfo.transactionCodeInfo.limitGroupCode = undefined;
          }
        });

        transactionUsageCommonService.extractInfoFromTransactionCodeMappings({
          transactionCodesMappingInfo: tcMappingInfos,
          currentTransactionInterchange: BankNetworkEnum.ALTO
        });
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect((e as AppError).errorCode).toEqual(
          ERROR_CODE.RECOMMENDED_LIMIT_GROUP_CODE_NOT_EXIST
        );
      }
    });

    it(`
    GIVEN 
      - transactionCodeInterchange provided
      - the currentTransactionCodeInterchange  match one of the interchange is defined within ITransactionCodeMapping[]
    THEN return the matched interchange's transactionCode 
  `, () => {
      const tcMappingInfos = getTransactionCodesMappingJagoAtmToNonJagoAcc();

      const currentTransInterchange = BankNetworkEnum.ALTO;
      const result = transactionUsageCommonService.extractInfoFromTransactionCodeMappings(
        {
          transactionCodesMappingInfo: tcMappingInfos,
          currentTransactionInterchange: currentTransInterchange
        }
      );
      const filteredTcMappingInfo = tcMappingInfos.find(
        mapInfo => mapInfo.interchange === currentTransInterchange
      );

      if (_.isUndefined(filteredTcMappingInfo)) {
        return;
      }

      const expectedResult: IExtractInfoFromTransactionCodeMappingResponse = {
        transactionCode: filteredTcMappingInfo.transactionCode,
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        limitGroupCode: filteredTcMappingInfo.transactionCodeInfo
          .limitGroupCode!
      };

      expect(result).toEqual(expectedResult);
    });
  });
});
