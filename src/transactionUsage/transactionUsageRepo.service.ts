import { FilterQuery, UpdateQuery } from 'mongoose';
import { ERROR_CODE } from '../common/errors';
import { AppError } from '../errors/AppError';
import {
  DailyLimitField,
  getDailyLimitFieldRepoByCategory,
  TransactionUsageCategory
} from './transactionUsage.enum';
import {
  ITransactionUsageModel,
  TransactionUsageDocument
} from './transactionUsage.model';
import transactionUsageRepository from './transactionUsage.repository';

const findTransactionUsageByCustomerAndAccount = async (
  customerId: string,
  accountNo: string
): Promise<ITransactionUsageModel | null> => {
  return transactionUsageRepository.getTransactionUsage(customerId, accountNo);
};

const insertOrUpdateDailyUsage = async (
  customerId: string,
  accountNo: string,
  expectedAmountAfterUpdate: number,
  transactionCategory: TransactionUsageCategory
) => {
  const filterQuery: FilterQuery<TransactionUsageDocument> = {
    customerId: customerId,
    accountNo: accountNo
  };

  const fieldToUpdate = getDailyLimitFieldRepoByCategory(transactionCategory);
  if (fieldToUpdate === DailyLimitField.NOT_IMPLEMENTED) {
    throw new AppError(ERROR_CODE.DAILY_USAGE_REPO_FIELD_TO_UPDATE_NOT_FOUND);
  }

  const updateQuery: UpdateQuery<TransactionUsageDocument> = {
    $set: {
      [fieldToUpdate]: {
        amount: expectedAmountAfterUpdate,
        lastUpdatedAt: new Date()
      }
    }
  };

  return await transactionUsageRepository.insertOrUpdateTransactionUsage(
    filterQuery,
    updateQuery
  );
};

const revertDailyUsage = async (
  customerId: string,
  accountNo: string,
  transactionAmountToBeReverted: number,
  transactionCategory: TransactionUsageCategory
) => {
  const filterQuery: FilterQuery<TransactionUsageDocument> = {
    customerId: customerId,
    accountNo: accountNo
  };

  const fieldToUpdate = getDailyLimitFieldRepoByCategory(transactionCategory);
  if (fieldToUpdate === DailyLimitField.NOT_IMPLEMENTED) {
    throw new AppError(ERROR_CODE.DAILY_USAGE_REPO_FIELD_TO_UPDATE_NOT_FOUND);
  }

  const updateQuery: UpdateQuery<TransactionUsageDocument> = {
    $set: {
      [fieldToUpdate]: {
        $inc: {
          amount: -Math.abs(transactionAmountToBeReverted)
        },
        lastUpdatedAt: new Date()
      }
    }
  };

  return await transactionUsageRepository.updateTransactionUsageCustom(
    filterQuery,
    updateQuery
  );
};

const revertDailyUsageToZero = async (
  customerId: string,
  accountNo: string,
  transactionAmountToBeReverted: number,
  transactionCategory: TransactionUsageCategory
) => {
  const fieldToUpdate = getDailyLimitFieldRepoByCategory(transactionCategory);
  if (fieldToUpdate === DailyLimitField.NOT_IMPLEMENTED) {
    throw new AppError(ERROR_CODE.DAILY_USAGE_REPO_FIELD_TO_UPDATE_NOT_FOUND);
  }

  const filterQuery: FilterQuery<TransactionUsageDocument> = {
    customerId: customerId,
    accountNo: accountNo,
    [fieldToUpdate]: {
      amount: {
        $and: [{ $gte: 0 }, { $lte: transactionAmountToBeReverted }]
      }
    }
  };

  const updateQuery: UpdateQuery<TransactionUsageDocument> = {
    $set: {
      [fieldToUpdate]: {
        amount: 0,
        lastUpdatedAt: new Date()
      }
    }
  };

  return await transactionUsageRepository.updateTransactionUsageCustom(
    filterQuery,
    updateQuery
  );
};

const transactionUsageRepoService = {
  findTransactionUsageByCustomerAndAccount,
  insertOrUpdateDailyUsage,
  revertDailyUsage,
  revertDailyUsageToZero
};

export default transactionUsageRepoService;
