import { ERROR_CODE } from '../common/errors';
import { AppError } from '../errors/AppError';
import { TransactionUsageAtmTransferManager } from './atm/transfer/transactionUsageAtmTransferManager';
import { TransactionUsageAtmWithdrawalManager } from './atm/withdrawal/transactionUsageAtmWithdrawalManager';
import { TransactionUsageCategory } from './transactionUsage.enum';
import { TransactionUsageManager } from './transactionUsageManager';

export class TransactionUsageManagerFactory {
  private static transactionUsageManager: TransactionUsageManager | null = null;

  static getTransactionUsageManagerInstance(
    category: TransactionUsageCategory
  ): TransactionUsageManager {
    switch (category) {
      case TransactionUsageCategory.ATM_WITHDRAWAL:
        this.transactionUsageManager = new TransactionUsageAtmWithdrawalManager();
        return this.transactionUsageManager;
      case TransactionUsageCategory.ATM_TRANSFER:
        this.transactionUsageManager = new TransactionUsageAtmTransferManager();
        return this.transactionUsageManager;
      default:
        throw new AppError(
          ERROR_CODE.TRANSACTION_USAGE_CATEGORY_HANDLER_IMPLEMENTATION_NOT_FOUND
        );
    }
  }

  static resetTransactionUsageManagerInstance() {
    this.transactionUsageManager = null;
  }
}
