import moment, { Moment } from 'moment';
import { ERROR_CODE } from '../common/errors';
import { ILimitGroup } from '../configuration/configuration.type';
import { AppError } from '../errors/AppError';
import _ from 'lodash';
import configurationRepository from '../configuration/configuration.repository';

import { config } from '../config';

import { TransactionUsageIncludedPaymentServiceCodes } from './transactionUsage.enum';
import {
  IExtractInfoFromTransactionCodeMappingRequest,
  IExtractInfoFromTransactionCodeMappingResponse,
  IPocketLimitPreValidationResponse
} from './transactionUsage.type';
import { TransactionUsageManagerFactory } from './transactionUsageManagerFactory';
import logger from '../logger';
import { IEntitlementResponse } from '../entitlement/entitlement.interface';
import entitlementService from '../entitlement/entitlement.service';

const timezone = config.get('timeZone');

const momentTimeZone = (date?: Moment | Date) => {
  return moment(date).utcOffset(timezone);
};

const isDailyLimitExpired = (dailyLimitLastUpdatedAt: Date): boolean => {
  const resetDate = momentTimeZone().startOf('day');

  const dailyLimitMomentDate = momentTimeZone(dailyLimitLastUpdatedAt);
  return !moment(dailyLimitMomentDate).isAfter(resetDate);
};

const getLimitGroupCodeInfo = (
  limitGroupCode: string,
  limitGroupConfigs: ILimitGroup[]
) => {
  if (_.isEmpty(limitGroupConfigs)) {
    throw new AppError(ERROR_CODE.LIMIT_GROUPS_CONF_NOT_FOUND);
  }

  const foundLimitGroupCode = limitGroupConfigs.find(
    limitGroup => limitGroup.code === limitGroupCode
  );
  if (_.isUndefined(foundLimitGroupCode)) {
    throw new AppError(ERROR_CODE.RECOMMENDED_LIMIT_GROUP_NOT_FOUND_IN_CONFIG);
  }
  return foundLimitGroupCode;
};

function overrideDailyLimitAmounts(
  customerId: string,
  limitGroupEntitlementCodeMap: Map<string, ILimitGroup>,
  entitlementsMap: Map<string, IEntitlementResponse>
) {
  for (const entitlementKeyPair of entitlementsMap) {
    const [entitlementCode, entitlement] = entitlementKeyPair;
    const limitGroup = limitGroupEntitlementCodeMap.get(entitlementCode);

    if (!limitGroup) {
      continue;
    }
    const customDailyLimitAmount = Number(entitlement.quota);
    if (isNaN(customDailyLimitAmount) || customDailyLimitAmount < 0) {
      logger.error(
        `Expected the entitlement '${entitlement.entitlement}'` +
          `for customer '${customerId}' to be a positive numerical value, but it was '${entitlement.quota}'`
      );
      continue;
    }

    limitGroup.dailyLimitAmount = customDailyLimitAmount;
  }
}

const applyCustomLimitGroupsIfApplicable = async (
  customerId: string,
  limitGroups: ILimitGroup[]
): Promise<void> => {
  if (_.isEmpty(limitGroups)) {
    return;
  }

  const limitGroupEntitlementCodeMap = new Map(
    limitGroups.map(limitGroup => [
      `limit.tx.${limitGroup.code}.daily`,
      limitGroup
    ])
  );
  const limitGroupEntitlementCodes = Array.from(
    limitGroupEntitlementCodeMap.keys()
  );
  const entitlementsMap = await entitlementService.getEntitlementsMap(
    customerId,
    limitGroupEntitlementCodes
  );

  overrideDailyLimitAmounts(
    customerId,
    limitGroupEntitlementCodeMap,
    entitlementsMap
  );
};

const getLimitGroupsConfig = async (
  customerId?: string
): Promise<ILimitGroup[]> => {
  const limitGroupsInConf = await configurationRepository.getLimitGroupsByCodes();
  if (_.isUndefined(limitGroupsInConf) || _.isEmpty(limitGroupsInConf)) {
    throw new AppError(ERROR_CODE.LIMIT_GROUPS_CONF_NOT_FOUND);
  }

  if (customerId) {
    await applyCustomLimitGroupsIfApplicable(customerId, limitGroupsInConf);
  }

  return limitGroupsInConf;
};

const isToProceedWithPocketLimitValidation = (
  paymentServiceCode: string
): IPocketLimitPreValidationResponse => {
  for (const [
    category,
    paymentServiceCodes
  ] of TransactionUsageIncludedPaymentServiceCodes.entries()) {
    if (paymentServiceCodes.includes(paymentServiceCode)) {
      //get the associated manager instance
      const transactionUsageManagerInstance = TransactionUsageManagerFactory.getTransactionUsageManagerInstance(
        category
      );

      const isToProceed = transactionUsageManagerInstance.isFeatureFlagEnabled();

      if (!isToProceed) {
        return {
          isToProceed: false
        };
      }

      return {
        isToProceed: true,
        transactionUsageCategory: category,
        transactionUsageManagerInstance: transactionUsageManagerInstance
      };
    }
  }

  return {
    isToProceed: false
  };
};

const extractInfoFromTransactionCodeMappings = (
  params: IExtractInfoFromTransactionCodeMappingRequest
): IExtractInfoFromTransactionCodeMappingResponse => {
  //assumption transactionCodeMappingInfo is not empty array
  const { transactionCodesMappingInfo, currentTransactionInterchange } = params;

  logger.info(`extractInfoFromTransactionCodeMappings: 
    currentTransactionInterchange: ${currentTransactionInterchange}
  `);

  const defaultLimitGroupCode =
    transactionCodesMappingInfo[0].transactionCodeInfo.limitGroupCode;

  logger.info(`[extractInfoFromTransactionCodeMappings]:
  defaultLimitGroupCode: ${defaultLimitGroupCode}
  `);

  if (_.isNil(defaultLimitGroupCode)) {
    throw new AppError(ERROR_CODE.RECOMMENDED_LIMIT_GROUP_CODE_NOT_EXIST);
  }

  //this is the fallback in case it can't found the match
  const defaultResult: IExtractInfoFromTransactionCodeMappingResponse = {
    transactionCode: transactionCodesMappingInfo[0].transactionCode,
    limitGroupCode: defaultLimitGroupCode
  };

  logger.info(`[extractInfoFromTransactionCodeMappings] defaultResult: 
    transactionCode: ${defaultResult.transactionCode}
    limitGroupCode: ${defaultResult.limitGroupCode}
  `);

  //if current transaction interchange is not provided,
  //then just return the first array value of transaction code
  if (_.isUndefined(currentTransactionInterchange)) {
    return defaultResult;
  }

  for (const tcMappingInfo of transactionCodesMappingInfo) {
    if (_.isUndefined(tcMappingInfo.interchange)) {
      continue;
    }

    if (tcMappingInfo.interchange === currentTransactionInterchange) {
      let matchedLimitGroupCode =
        tcMappingInfo.transactionCodeInfo.limitGroupCode;

      logger.info(`[extractInfoFromTransactionCodeMappings] interchange matched: 
      currentTransactionInterchange: ${currentTransactionInterchange}
      mappingInfo transactionCode: ${tcMappingInfo.transactionCode}
      mappingInfo limitGroupCode: ${matchedLimitGroupCode}
    `);

      if (_.isNil(matchedLimitGroupCode)) {
        logger.info(`[extractInfoFromTransactionCodeMappings] interchange matched: 
        but limitGroup inside the mapping is undefined
      `);
        throw new AppError(ERROR_CODE.RECOMMENDED_LIMIT_GROUP_CODE_NOT_EXIST);
      }

      return {
        transactionCode: tcMappingInfo.transactionCode,
        limitGroupCode: matchedLimitGroupCode
      };
    }
  }

  return defaultResult;
};

const transactionUsageCommonService = {
  isDailyLimitExpired,
  getLimitGroupsConfig,
  getLimitGroupCodeInfo,
  isToProceedWithPocketLimitValidation,
  extractInfoFromTransactionCodeMappings,
  applyCustomLimitGroupsIfApplicable
};

export default transactionUsageCommonService;
