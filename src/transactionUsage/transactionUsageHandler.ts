import {
  TransactionUsageCategory,
  TransactionUsageGroupCode
} from './transactionUsage.enum';
import { ITransactionUsageModel } from './transactionUsage.model';
import {
  IDailyUsageParamValidatedResponse,
  IDailyUsageProcessAcceptedStateRequest,
  IDailyUsageProcessRejectedStateRequest,
  IEvalStatusError,
  IPocketLimitValidationRequestExt,
  ITransactionUsageSaveContextRequest
} from './transactionUsage.type';

export interface ITransactionUsageHandler {
  isFeatureFlagEnabled(): boolean;

  validateDailyUsageParams(
    params: IPocketLimitValidationRequestExt
  ): IDailyUsageParamValidatedResponse;

  getDailyLimitAmountSoFar(
    existingTransactionUsage: ITransactionUsageModel | null
  ): number;

  isValidTransactionCategoryByRecommendedService(
    validatedParams: IDailyUsageParamValidatedResponse
  ): boolean;

  processAcceptedState(
    params: IDailyUsageProcessAcceptedStateRequest
  ): Promise<void>;

  processRejectedState(
    params: IDailyUsageProcessRejectedStateRequest
  ): Promise<void>;

  getEvalStatusErrors(): IEvalStatusError;
  getTransactionUsageGroupCode(): TransactionUsageGroupCode;
  getTransactionUsageCategory(): TransactionUsageCategory;

  saveDailyLimitToContext(params: ITransactionUsageSaveContextRequest): void;

  processReversalBasedOnContext(): Promise<void>;
}
