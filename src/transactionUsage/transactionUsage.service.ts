import logger from '../logger';

import { IConfirmTransactionInput } from '../transaction/transaction.type';

import transactionUsageCommonService from './transactionUsageCommon.service';
import { IPocketLimitValidationRequest } from './transactionUsage.type';

import { TransactionUsageManagerFactory } from './transactionUsageManagerFactory';
import _ from 'lodash';
import { reversalTransactionConfirmationResponseCode } from './transactionUsage.enum';
import { ITransactionModel } from '../transaction/transaction.model';
import { getTransactionUsageInfoTracker } from '../common/contextHandler';
import { RuleConditionState } from '../transaction/transaction.conditions.enum';

//pocket-level limit validation feature
const pocketLevelLimitValidation = async (
  params: IPocketLimitValidationRequest
): Promise<void> => {
  const {
    recommendedService,
    customerId,
    accountNo,
    beneficiaryAccountNo,
    beneficiaryAccountName,
    paymentServiceType
  } = params;

  const paymentServiceCode = recommendedService.paymentServiceCode;

  logger.info(`[pocketLevelLimitValidation] start:
    customerId: ${customerId}
    accountNo: ${accountNo}
    beneficiaryAccountNo: ${beneficiaryAccountNo}
    beneficiaryAccountName: ${beneficiaryAccountName}
    paymentServiceType: ${paymentServiceType}
    recommendedService paymentServiceCode: ${paymentServiceCode}
  `);

  //perform prevalidation against payment service code, currently only accepting Atm Withdrawal and Atm Transfer
  const preValidationResponse = transactionUsageCommonService.isToProceedWithPocketLimitValidation(
    paymentServiceCode
  );

  const {
    isToProceed,
    transactionUsageCategory,
    transactionUsageManagerInstance
  } = preValidationResponse;

  //if prevalidation for pocket limit is invalid, then skip it
  if (
    !isToProceed ||
    _.isUndefined(transactionUsageCategory) ||
    _.isUndefined(transactionUsageManagerInstance)
  ) {
    return;
  }

  //get the limit groups
  const limitGroupConfigs = await transactionUsageCommonService.getLimitGroupsConfig(
    params.customerId
  );

  //validate the params, making sure that subsequent functions will use validated params
  const validatedParams = transactionUsageManagerInstance.validateDailyUsageParams(
    {
      ...params,
      limitGroupConfigs
    }
  );

  //validate transaction amount against daily limit
  const validatedTransactionResponse = await transactionUsageManagerInstance.validateTransactionAmountAgainstDailyLimit(
    validatedParams
  );

  //if not validated then skip it
  if (!validatedTransactionResponse.isValidated) {
    return;
  }

  //if dailyAmountSoFar or ruleState don't exist, then skip it
  if (
    _.isUndefined(validatedTransactionResponse.dailyAmountSoFar) ||
    _.isUndefined(validatedTransactionResponse.ruleConditionStateResult)
  ) {
    return;
  }

  if (
    validatedTransactionResponse.ruleConditionStateResult ===
    RuleConditionState.ACCEPTED
  ) {
    await transactionUsageManagerInstance.processAcceptedState({
      validatedParams,
      dailyAmountSoFar: validatedTransactionResponse.dailyAmountSoFar,
      ruleConditionStateResult:
        validatedTransactionResponse.ruleConditionStateResult
    });
  } else {
    await transactionUsageManagerInstance.processRejectedState({
      validatedParams,
      dailyAmountSoFar: validatedTransactionResponse.dailyAmountSoFar,
      ruleConditionStateResult:
        validatedTransactionResponse.ruleConditionStateResult
    });
  }
};

const mapTransactionInfoToTransactionUsageInfoContext = async (
  transaction: ITransactionModel | null
): Promise<void> => {
  logger.info(`[mapTransactionInfoToTransactionUsageInfoContext] start`);

  //if transaction is null or payment service code does not exist, then skip it
  if (_.isNull(transaction) || _.isUndefined(transaction.paymentServiceCode)) {
    return;
  }

  logger.info(`[mapTransactionInfoToTransactionUsageInfoContext] params: 
      sourceCustomerId: ${transaction.sourceCustomerId},
      sourceAccountNo: ${transaction.sourceAccountNo},
      paymentServiceCode: ${transaction.paymentServiceCode}
      debitLimitGroupCode: ${transaction.debitLimitGroupCode}
    `);

  const {
    sourceCustomerId,
    sourceAccountNo,
    transactionAmount,
    paymentServiceCode,
    debitLimitGroupCode
  } = transaction;

  const isSourceCustomerIdValid =
    !_.isUndefined(sourceCustomerId) && !_.isEmpty(sourceCustomerId);

  const isSourceAccountNoValid =
    !_.isUndefined(sourceAccountNo) && !_.isEmpty(sourceAccountNo);

  //if required value are not valid, then skip it
  if (!isSourceCustomerIdValid || !isSourceAccountNoValid) {
    return;
  }

  const preValidationResponse = transactionUsageCommonService.isToProceedWithPocketLimitValidation(
    paymentServiceCode
  );

  const {
    isToProceed,
    transactionUsageCategory,
    transactionUsageManagerInstance
  } = preValidationResponse;

  //if prevalidation for pocket limit is invalid, then skip it
  if (
    !isToProceed ||
    _.isUndefined(transactionUsageCategory) ||
    _.isUndefined(transactionUsageManagerInstance)
  ) {
    return;
  }

  transactionUsageManagerInstance.saveDailyLimitContext({
    customerId: sourceCustomerId,
    accountNo: sourceAccountNo,
    transactionUsageCategory,
    transactionAmount: transactionAmount,
    paymentServiceCode: paymentServiceCode,
    limitGroupCode: debitLimitGroupCode ?? ''
  });
};

const processReversalPocketLimitByTransactionBasedOnContext = () => {
  //currently there is only one context being used - transactionUsageInfoTracker
  const transactionUsageInfoContext = getTransactionUsageInfoTracker();

  //if there is no context information found, then skip it
  if (_.isUndefined(transactionUsageInfoContext)) {
    return;
  }

  //extract transaction  category
  const transactionCategory = transactionUsageInfoContext.transactionCategory;

  logger.info(`[processReversalPocketLimitByTransactionBasedOnContext] transactionCategory: ${transactionCategory}
  `);

  //based ont the category derived the managerHandler
  const transactionUsageManagerInstance = TransactionUsageManagerFactory.getTransactionUsageManagerInstance(
    transactionCategory
  );

  transactionUsageManagerInstance.processReversalBasedOnContext();

  logger.info(`[processReversalPocketLimitByTransactionBasedOnContext] transactionCategory: ${transactionCategory}
  COMPLETED`);
};

const processReversalPocketLimitByTransactionConfirmationInput = (
  input: IConfirmTransactionInput
) => {
  logger.info(`[processReversalPocketLimitByTransactionConfirmationInput]
    
     transConfirmation responseCode: ${input.responseCode}
    `);

  const isToReverse = reversalTransactionConfirmationResponseCode.includes(
    input.responseCode
  );

  logger.info(`[processReversalPocketLimitByTransactionConfirmationInput] isToReverse: ${isToReverse}
  `);

  if (!isToReverse) {
    return;
  }

  processReversalPocketLimitByTransactionBasedOnContext();

  logger.info(`[processReversalPocketLimitByTransactionConfirmationInput] COMPLETED
  `);
};

const transactionUsageService = {
  pocketLevelLimitValidation,
  mapTransactionInfoToTransactionUsageInfoContext,
  processReversalPocketLimitByTransactionBasedOnContext,
  processReversalPocketLimitByTransactionConfirmationInput
};

export default transactionUsageService;
