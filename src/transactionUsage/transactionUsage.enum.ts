import {
  ATM_TRANSFER_PAYMENT_SERVICE_CODES,
  ATM_WITHDRAWAL_PAYMENT_SERVICE_CODES
} from '../transaction/transaction.category.enum';
import { ConfirmTransactionStatus } from '../transaction/transaction.enum';

export const enum TransactionUsageCategory {
  ATM_WITHDRAWAL = 'atm-withdrawal',
  ATM_TRANSFER = 'atm-transfer'
}

export const enum TransactionUsageGroupCode {
  ATM_WITHDRAWAL = 'ATM_WITHDRAWAL_POCKET_LEVEL_DAILY_LIMIT',
  ATM_TRANSFER = 'ATM_TRANSFER_POCKET_LEVEL_DAILY_LIMIT'
}

export const TransactionUsageIncludedPaymentServiceCodes = new Map([
  [
    TransactionUsageCategory.ATM_WITHDRAWAL,
    [...ATM_WITHDRAWAL_PAYMENT_SERVICE_CODES]
  ],
  [
    TransactionUsageCategory.ATM_TRANSFER,
    [...ATM_TRANSFER_PAYMENT_SERVICE_CODES]
  ]
]);

export const reversalTransactionConfirmationResponseCode = [
  ConfirmTransactionStatus.REQUEST_TIMEOUT,
  ConfirmTransactionStatus.ERROR_EXPECTED,
  ConfirmTransactionStatus.ERROR_UNEXPECTED
];

export enum DailyLimitField {
  ATM_WITHDRAWAL = 'dailyUsages.atmWithdrawal',
  ATM_TRANSFER = 'dailyUsages.atmTransfer',
  NOT_IMPLEMENTED = 'dailyUsages.notImplemented'
}

export const getDailyLimitFieldRepoByCategory = (
  category: TransactionUsageCategory
): string => {
  switch (category) {
    case TransactionUsageCategory.ATM_WITHDRAWAL:
      return DailyLimitField.ATM_WITHDRAWAL;
    case TransactionUsageCategory.ATM_TRANSFER:
      return DailyLimitField.ATM_TRANSFER;
    default:
      return DailyLimitField.NOT_IMPLEMENTED;
  }
};
