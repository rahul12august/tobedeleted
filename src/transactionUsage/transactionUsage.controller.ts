import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import transactionUsageCommonService from './transactionUsageCommon.service';

const customerIdParameter = 'customerId';

const getLimitGroups: hapi.ServerRoute = {
  method: Http.Method.GET,
  path: `/private/limit-groups/{${customerIdParameter}?}`,
  options: {
    description: 'Api to check limit groups',
    notes:
      "If 'customerId' is specified, we return custom limits set for that customer",
    tags: ['api', 'transaction usage', 'limits'],
    auth: false,
    handler: async (
      hapiRequest: hapi.Request,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const customerId = hapiRequest.params[customerIdParameter];
      const limitGroups = await transactionUsageCommonService.getLimitGroupsConfig(
        customerId
      );

      return hapiResponse.response(limitGroups).code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'OK'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const transactionUsageController: hapi.ServerRoute[] = [getLimitGroups];
export default transactionUsageController;
