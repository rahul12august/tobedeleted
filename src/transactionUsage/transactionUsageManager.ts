import {
  IDailyUsageParamValidatedResponse,
  IDailyLimitProcessStateRequest,
  IPocketLimitValidationRequestExt,
  ITransactionUsageSaveContextRequest,
  IValidatedTransactionAmountDailyLimitResponse
} from './transactionUsage.type';
import { ITransactionUsageHandler } from './transactionUsageHandler';
import _ from 'lodash';

import logger from '../logger';

import transactionUsageRepoService from './transactionUsageRepo.service';
import transactionConditionHelperService from '../transaction/transaction.conditions.helper';

export abstract class TransactionUsageManager {
  private transactionUsageHandlerInstance!: ITransactionUsageHandler;

  setTransactionUsageHandlerInstance(
    transactionUsageHandlerInstance: ITransactionUsageHandler
  ) {
    this.transactionUsageHandlerInstance = transactionUsageHandlerInstance;
  }

  getTransactionUsageHandlerInstance() {
    return this.transactionUsageHandlerInstance;
  }

  public validateDailyUsageParams(
    params: IPocketLimitValidationRequestExt
  ): IDailyUsageParamValidatedResponse {
    const handlerInstance = this.transactionUsageHandlerInstance;

    return handlerInstance.validateDailyUsageParams(params);
  }

  public async processReversalBasedOnContext(): Promise<void> {
    const handlerInstance = this.transactionUsageHandlerInstance;

    handlerInstance.processReversalBasedOnContext();
  }

  public isFeatureFlagEnabled(): boolean {
    const handlerInstance = this.transactionUsageHandlerInstance;

    return handlerInstance.isFeatureFlagEnabled();
  }

  public saveDailyLimitContext(
    params: ITransactionUsageSaveContextRequest
  ): void {
    const handlerInstance = this.transactionUsageHandlerInstance;
    handlerInstance.saveDailyLimitToContext(params);
  }

  public async validateTransactionAmountAgainstDailyLimit(
    validatedParams: IDailyUsageParamValidatedResponse
  ): Promise<IValidatedTransactionAmountDailyLimitResponse> {
    const handlerInstance = this.transactionUsageHandlerInstance;

    const {
      customerId,
      accountNo,
      debitTransactionCode,
      debitLimitGroupCode,
      paymentServiceCode,
      paymentServiceType,
      limitGroupInfo,
      transactionAmount
    } = validatedParams;

    logger.info(`[validateDailyLimit] validated params:
      customerId: ${customerId}
      accountNo: ${accountNo}
      debitTransactionCode: ${debitTransactionCode}
      debitLimitGroupCode: ${debitLimitGroupCode}
      paymentServiceType: ${paymentServiceType}
      paymentServiceCode: ${paymentServiceCode}
    `);

    //3. check transaction category whether it is valid by recommend service as the input
    const isValidTransCategory = handlerInstance.isValidTransactionCategoryByRecommendedService(
      validatedParams
    );

    //if transaction category cannot be derived from recommended service (and not part of pocket level withdrawal)
    //then should skip the check
    if (!isValidTransCategory) {
      return {
        isValidated: false
      };
    }

    //4.query the dailyAmount from transaction collection (pocket level)
    const existingTransactionUsage = await transactionUsageRepoService.findTransactionUsageByCustomerAndAccount(
      customerId,
      accountNo
    );

    //2. get current dailyUsages accumulation
    const dailyAmountSoFar = handlerInstance.getDailyLimitAmountSoFar(
      existingTransactionUsage
    );

    logger.info(
      `[validateTransactionAmountAgainstDailyLimit] dailyAmountSoFar: ${dailyAmountSoFar}`
    );

    //3.validate the amount and output the state (ACCEPTED, DECLINED, TRY_TOMORROW)
    const resultState = transactionConditionHelperService.compareAmountWithDailyLimit(
      limitGroupInfo,
      transactionAmount,
      dailyAmountSoFar
    );

    logger.info(
      `[validateTransactionAmountAgainstDailyLimit] validationResult: ${resultState}`
    );

    return {
      isValidated: true,
      dailyAmountSoFar,
      ruleConditionStateResult: resultState
    };
  }

  public async processAcceptedState(
    params: IDailyLimitProcessStateRequest
  ): Promise<void> {
    const handlerInstance = this.transactionUsageHandlerInstance;

    await handlerInstance.processAcceptedState({
      dailyAmountSoFar: params.dailyAmountSoFar,
      validatedParams: params.validatedParams
    });
  }

  public async processRejectedState(
    params: IDailyLimitProcessStateRequest
  ): Promise<void> {
    const handlerInstance = this.transactionUsageHandlerInstance;

    const {
      validatedParams,
      dailyAmountSoFar,
      ruleConditionStateResult
    } = params;

    const { transactionAmount, limitGroupInfo } = validatedParams;

    //generate evalStatus
    const resultStatus = transactionConditionHelperService.constructDailyLimitEvaluatedRuleConditionStatusRefined(
      transactionAmount,
      dailyAmountSoFar,
      handlerInstance.getTransactionUsageCategory(),
      limitGroupInfo.dailyLimitAmount,
      handlerInstance.getEvalStatusErrors().errorCodeDeclined,
      handlerInstance.getEvalStatusErrors().errorCodeTryTomorrow,
      ruleConditionStateResult
    );

    //6. handle when
    await handlerInstance.processRejectedState({
      evalStatus: resultStatus,
      validatedParams,
      dailyAmountSoFar
    });
  }
}
