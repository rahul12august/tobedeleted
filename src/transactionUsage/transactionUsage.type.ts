import { BankNetworkEnum, PaymentServiceTypeEnum } from '@dk/module-common';
import { ERROR_CODE } from '../common/errors';
import {
  ILimitGroup,
  ITransactionCodeMapping
} from '../configuration/configuration.type';
import {
  IATMTransferExceedsDailyLimitMessage,
  IATMWithdrawalExceedsDailyLimitMessage
} from '../transaction/transaction.producer.type';

import { RecommendationService } from '../transaction/transaction.type';
import {
  IAtmWithdrawalDailyUsageParamValidatedResponse,
  IAtmWithdrawalDailyUsageProcessAcceptedStateRequest,
  IAtmWithdrawalDailyUsageProcessRejectedStateRequest
} from './atm/withdrawal/transactionUsageAtmWithdrawal.type';

import {
  IAtmTransferDailyUsageParamValidatedResponse,
  IAtmTransferDailyUsageProcessAcceptedStateRequest,
  IAtmTransferDailyUsageProcessRejectedStateRequest
} from './atm/transfer/transactionUsageAtmTransfer.type';
import { TransactionUsageCategory } from './transactionUsage.enum';

import { RuleConditionState } from '../transaction/transaction.conditions.enum';
import { TransactionUsageManager } from './transactionUsageManager';

export interface TransactionUsageContext {
  customerId: string;
  accountNo: string;
  transactionCategory: TransactionUsageCategory;
  paymentServiceCode: string;
  limitGroupCode: string;
  currentTransactionAmount: number;
}

export interface IPocketLimitValidationRequest {
  transactionAmount: number;
  transactionInterchange: BankNetworkEnum | undefined;
  customerId: string | undefined;
  accountNo: string | undefined;
  beneficiaryAccountNo?: string;
  beneficiaryAccountName?: string;
  paymentServiceType: PaymentServiceTypeEnum;
  recommendedService: RecommendationService;
}

export interface IPocketLimitValidationRequestExt
  extends IPocketLimitValidationRequest {
  limitGroupConfigs: ILimitGroup[];
}

export interface ITransactionUsageSaveContextRequest {
  customerId: string | undefined;
  accountNo: string | undefined;
  transactionAmount: number;
  transactionUsageCategory: TransactionUsageCategory;
  paymentServiceCode: string;
  limitGroupCode: string;
}

export interface IPocketLimitPreValidationResponse {
  isToProceed: boolean;
  transactionUsageCategory?: TransactionUsageCategory;
  transactionUsageManagerInstance?: TransactionUsageManager;
}

export interface IEvalStatusError {
  errorCodeTryTomorrow: ERROR_CODE;
  errorCodeDeclined: ERROR_CODE;
}

export interface IValidatedTransactionAmountDailyLimitResponse {
  isValidated: boolean;
  dailyAmountSoFar?: number;
  ruleConditionStateResult?: RuleConditionState;
}

export interface IDailyLimitProcessStateRequest {
  dailyAmountSoFar: number;
  validatedParams: IDailyUsageParamValidatedResponse;
  ruleConditionStateResult: RuleConditionState;
}

export interface IExtractInfoFromTransactionCodeMappingRequest {
  transactionCodesMappingInfo: ITransactionCodeMapping[];
  currentTransactionInterchange?: string;
}

export interface IExtractInfoFromTransactionCodeMappingResponse {
  limitGroupCode: string;
  transactionCode: string;
}

export type IDailyUsageParamValidatedResponse =
  | IAtmWithdrawalDailyUsageParamValidatedResponse
  | IAtmTransferDailyUsageParamValidatedResponse;

export type ITransactionExceedsDailyLimitMessage =
  | IATMWithdrawalExceedsDailyLimitMessage
  | IATMTransferExceedsDailyLimitMessage;

export type IDailyUsageProcessAcceptedStateRequest =
  | IAtmWithdrawalDailyUsageProcessAcceptedStateRequest
  | IAtmTransferDailyUsageProcessAcceptedStateRequest;

export type IDailyUsageProcessRejectedStateRequest =
  | IAtmWithdrawalDailyUsageProcessRejectedStateRequest
  | IAtmTransferDailyUsageProcessRejectedStateRequest;
