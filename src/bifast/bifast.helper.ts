import { ITransactionModel } from '../transaction/transaction.model';
import {
  IBifastPaymentStatusRequestPayload,
  IBifastRequestPayload
} from './bifast.type';
import logger from '../logger';
import {
  BeneficiaryProxyType,
  CustomerType,
  ResidentialStatus
} from './bifast.enum';
import { BIFAST_PAYMENT_SERVICE_TYPE, CHANNEL_TYPE } from './bifast.constant';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import ruleConditionFns from './bifast.condition';
import { toJKTDate } from '../common/dateUtils';
import {
  PhoneCode,
  TransferFailureReasonActor
} from '../transaction/transaction.enum';

const getBiFastTransaction = (transaction: ITransactionModel): string =>
  (transaction.thirdPartyOutgoingId &&
    transaction.thirdPartyOutgoingId.substr(
      transaction.thirdPartyOutgoingId.length - 8
    )) ||
  '';

export const mapToBifastTransaction = (
  transaction: ITransactionModel
): IBifastRequestPayload => {
  const beneficiaryProxyAlias =
    transaction.additionalInformation3 &&
    [BeneficiaryProxyType.PHONE_NUMBER].includes(
      transaction.additionalInformation3 as BeneficiaryProxyType
    )
      ? transaction.additionalInformation4 &&
        transaction.additionalInformation4.includes(PhoneCode.PLUS62)
        ? transaction.additionalInformation4.replace(PhoneCode.PLUS62, '62')
        : transaction.additionalInformation4
      : transaction.additionalInformation4;
  return {
    transactionId: getBiFastTransaction(transaction),
    transactionDate: toJKTDate(
      transaction.createdAt || new Date(),
      'YYMMDDHHmmss'
    ),
    paymentServiceType: BIFAST_PAYMENT_SERVICE_TYPE,
    channelType: CHANNEL_TYPE,
    transactionAmount: transaction.transactionAmount.toString(),
    beneficiaryBankCode: transaction.beneficiaryRemittanceCode || '',
    beneficiaryAccountName: transaction.beneficiaryAccountName || '',
    beneficiaryAccountNo: transaction.beneficiaryAccountNo || 'null',
    beneficiaryAccountType: transaction.beneficiaryAccountType || null,
    beneficiaryCustomerType:
      (transaction.additionalInformation1 &&
        (transaction.additionalInformation1?.length > 1
          ? transaction.additionalInformation1
          : '0' + transaction.additionalInformation1)) ||
      '',
    beneficiaryResidentialStatus:
      (transaction.additionalInformation2 &&
        '0' + transaction.additionalInformation2) ||
      '',
    sourceNik: transaction.sourceIdNumber || '',
    sourceBankCode: transaction.sourceRemittanceCode || '',
    sourceAccountNo: transaction.sourceAccountNo || '',
    sourceAccountName: transaction.sourceAccountName || '',
    sourceResidentialStatus: '0' + ResidentialStatus.RESIDENT,
    sourceAccountType: transaction.sourceAccountTypeBI || '',
    sourceCustomerType: transaction.sourceCustomerType || '',
    notes: transaction.note || null,
    beneficiaryProxyType: transaction.additionalInformation3 || null,
    beneficiaryProxyAlias: beneficiaryProxyAlias || null
  };
};

export const populateDefaultBiFastMandatoryFields = (
  transaction: ITransactionModel
): ITransactionModel => {
  logger.info(
    `populateDefaultBiFastMandatoryFields: transaction model with additionalInformation1 ${transaction.additionalInformation1} additionalInformation2 ${transaction.additionalInformation2} additionalInformation3 ${transaction.additionalInformation3} additionalInformation4 ${transaction.additionalInformation4}`
  );
  return {
    ...transaction,
    additionalInformation1:
      transaction.additionalInformation1 ?? CustomerType.INDIVIDUAL,
    additionalInformation2:
      transaction.additionalInformation2 ?? ResidentialStatus.RESIDENT,
    additionalInformation3:
      transaction.additionalInformation3 ?? BeneficiaryProxyType.ACCOUNT_NUMBER,
    additionalInformation4:
      transaction.additionalInformation4 ?? transaction.beneficiaryAccountNo
  };
};

export const validateBiFastMandatoryFields = (
  transaction: ITransactionModel
): void => {
  let isValid = true;
  for (let rule of ruleConditionFns) {
    const passed = rule(transaction);
    if (!passed) {
      logger.error(
        `validateBiFastMandatoryFields: Bifast validation failed for rule: ${rule.name}`
      );
      isValid = false;
      break;
    }
  }
  if (!isValid) {
    throw new TransferAppError(
      'Bifast validation failed!',
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_MANDATORY_FIELDS
    );
  }
  return;
};

export const mapBIFastTransactionStatus = (
  transactionModel: ITransactionModel
): IBifastPaymentStatusRequestPayload => {
  const biFastId = getBiFastTransaction(transactionModel);
  return {
    transactionId: biFastId,
    sourceBankCode: transactionModel.sourceRemittanceCode || '',
    oriTransactionId: biFastId,
    oriTransactionDate: toJKTDate(transactionModel.createdAt, 'YYMMDDHHmmss'),
    sourceAccountNumber: transactionModel.sourceAccountNo || '',
    channelType: CHANNEL_TYPE
  };
};
