import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import logger from '../logger';
import biFastService from './bifast.service';
import { IConfirmTransactionRequest } from '../transaction/transaction.type';
import {
  biFastConfirmTransactionRequestValidator,
  biFastConfirmTransactionResponseValidator
} from './bifast.validator';
import { updateRequestIdInContext } from '../common/contextHandler';
import entitlementFlag from '../common/entitlementFlag';
import entitlementService from '../entitlement/entitlement.service';

const confirmTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/bi-fast/transaction-confirmation',
  options: {
    description: 'Confirm BI Fast transaction',
    notes: 'Private Api endpoint is called from BI Fast to confirm transaction',
    tags: ['api', 'transactions'],
    auth: false,
    validate: {
      payload: biFastConfirmTransactionRequestValidator
    },
    response: {
      schema: biFastConfirmTransactionResponseValidator
    },
    handler: async (
      hapiRequest: IConfirmTransactionRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      try {
        const { payload } = hapiRequest;
        logger.debug(`Payload: ${JSON.stringify(payload)}`);
        logger.info(
          `confirmTransaction: BiFast transaction confirmation call initiated`
        );
        await updateRequestIdInContext(payload.externalId);
        const response = await biFastService.confirmTransaction(payload);
        if (await entitlementFlag.isEnabled()) {
          await entitlementService.processReversalEntitlementCounterUsageOnTransactionConfirmation(
            payload
          );
        }
        return hapiResponse.response(response).code(Http.StatusCode.OK);
      } catch (e) {
        if (await entitlementFlag.isEnabled()) {
          await entitlementService.processReversalEntitlementCounterUsageBasedOnContext();
        }
        return e;
      }
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Ok'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          },
          [Http.StatusCode.UNAUTHORIZED]: {
            description: 'UNAUTHORIZED'
          }
        }
      }
    }
  }
};

const bifastController: hapi.ServerRoute[] = [confirmTransaction];

export default bifastController;
