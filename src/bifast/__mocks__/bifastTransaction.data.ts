import { BankChannelEnum } from '@dk/module-common';
import { getTransactionDetail } from '../../transaction/__mocks__/transaction.data';
import {
  AccountType,
  BeneficiaryProxyType,
  BifastResponseCode
} from '../bifast.enum';
import {
  IBifastRequestPayload,
  IBifastPaymentStatusRequestPayload
} from '../bifast.type';

export const getMockedTransactionResponse = () => ({
  creditTransferResponse: {
    transactionId: '000000200',
    komiUniqueId: 'fe8fd11d-19eb-4361-82d9-0d8efa54b828',
    responseCode: BifastResponseCode.SUCCESS,
    responseMessage: 'SUCCESS'
  }
});

export const getMockedTransactionRequest = (): IBifastRequestPayload => ({
  transactionId: '000000200',
  transactionDate: '20220215T135959',
  paymentServiceType: 'OUTGOING_BIFAST',
  sourceBankCode: 'ATOSIDJ1',
  channelType: '02',
  sourceAccountType: 'SVGS',
  sourceCustomerType: 'I',
  sourceResidentialStatus: '01',
  sourceAccountNo: '0411920000100',
  sourceNik: '3273071111110001',
  beneficiaryAccountNo: '770179107',
  beneficiaryBankCode: 'CENAIDJA',
  beneficiaryAccountName: 'JOHN SMITH',
  beneficiaryAccountType: 'SVGS',
  beneficiaryCustomerType: '01',
  beneficiaryResidentialStatus: '01',
  beneficiaryProxyType: '01',
  beneficiaryProxyAlias: '770179107',
  transactionAmount: '100000.0',
  notes: 'Transfer',
  sourceAccountName: 'JOHN SMITH'
});

export const transactionModel = {
  ...getTransactionDetail(),
  beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
  requireThirdPartyOutgoingId: true,
  debitTransactionCode: 'TFD60',
  thirdPartyOutgoingId: '2T00000041',
  transactionAmount: 50000002,
  paymentServiceCode: 'SKN',
  additionalInformation1: '1',
  additionalInformation2: '1',
  beneficiaryProxyType: BeneficiaryProxyType.ACCOUNT_NUMBER,
  beneficiaryProxyAlias: '30',
  sourceIdNumber: '123344544232',
  beneficiaryAccountType: AccountType.SAVINGS_ACCOUNT
};

export const getTransactionStatusRequest = (): IBifastPaymentStatusRequestPayload => ({
  transactionId: '00000004',
  sourceBankCode: 'ATOSIDJ1',
  oriTransactionId: '00000004',
  oriTransactionDate: '220420062013',
  sourceAccountNumber: '100043860249',
  channelType: '02'
});

export const getTransactionStatusResponse = () => ({
  paymentStatusResponse: {
    transactionId: '12312351',
    transactionDate: '2022-04-13 16:38:51',
    sourceAccountNo: '0411920000100',
    interchange: 'BIFAST',
    transactionAmount: '100000.00',
    transactionResponseId: '20220418ATOSIDJ1110O0212312351',
    responseCode: '200',
    responseMessage: 'U000',
    komiUniqueId: 'fc650736-ef34-48cb-9ca1-b22aed249164'
  }
});
