import httpClient from './httpClient';
import logger, { wrapLogs } from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { httpClientTimeoutErrorCode } from '../common/constant';
import {
  IBifastPaymentStatusRequest,
  IBifastPaymentStatusRequestPayload,
  IBifastPaymentStatusResponse,
  IBifastPaymentStatusResponsePayload,
  IBifastRequestPayload,
  IBifastTransactionRequest,
  IBifastTransactionResponse
} from './bifast.type';
import { config } from '../config';
import { get } from 'lodash';
import { BifastResponseCode } from './bifast.enum';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const { transactionUrl } = config.get('bifast');
const paymentStatusUrl = ':paymentStatus';

const submitTransaction = async (
  transaction: IBifastRequestPayload
): Promise<IBifastTransactionResponse> => {
  try {
    const bifastRequest: IBifastTransactionRequest = {
      creditTransferRequest: transaction
    };
    logger.info(
      `Submitting BI-FAST transaction 
      externalId : ${transaction.transactionId}, 
      beneficiaryBankCode: ${transaction.beneficiaryBankCode}, 
      beneficiaryAccountNo: ${transaction.beneficiaryAccountNo},
      sourceAccountNo: ${transaction.sourceAccountNo},
      sourceNik: ${transaction.sourceNik}`
    );
    logger.debug(`submitTransaction: BIFAST transactionUrl: ${transactionUrl}`);
    logger.debug(
      `submitTransaction: BIFAST transfer request: ${JSON.stringify(
        bifastRequest
      )}`
    );
    const response = await httpClient.post(transactionUrl, bifastRequest);
    logger.debug(
      `submitTransaction: got response from bifast ${response.status}`
    );
    const data: IBifastTransactionResponse = get(response, ['data'], {});
    if (
      data.creditTransferResponse.responseCode !== BifastResponseCode.SUCCESS
    ) {
      const detail = `Error from BI-FAST while submitting transaction`;
      logger.error(
        `submitTransaction: ${detail}, response :
        ${JSON.stringify(data)}`
      );
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.BIFAST,
        ERROR_CODE.ERROR_FROM_BIFAST
      );
    }
    logger.info(
      `submitTransaction: submit BI-FAST transaction response : ${JSON.stringify(
        data
      )}`
    );
    return data;
  } catch (error) {
    let detail = 'Error from BI-FAST while submitting transaction',
      actor = TransferFailureReasonActor.BIFAST,
      errorCode = ERROR_CODE.ERROR_FROM_BIFAST;

    logger.error(`submitTransaction catch: ${detail}`, error && error.message);

    if (error.code == httpClientTimeoutErrorCode) {
      errorCode = ERROR_CODE.BIFAST_REQUEST_TIMEOUT;
    }

    if (error instanceof TransferAppError) {
      detail = error.detail;
      actor = error.actor;
    }

    throw new TransferAppError(detail, actor, errorCode);
  }
};

const getPaymentStatus = async (
  paymentStatusRequest: IBifastPaymentStatusRequestPayload
): Promise<IBifastPaymentStatusResponsePayload> => {
  try {
    logger.info(
      `getPaymentStatus: payment status payload: ${JSON.stringify(
        paymentStatusRequest
      )}`
    );
    const bifastPaymentStatusRequest: IBifastPaymentStatusRequest = {
      paymentStatusRequest: paymentStatusRequest
    };
    logger.info(
      `getPaymentStatus: Checking BI-FAST transaction payment status`
    );
    const response = await httpClient.post(
      paymentStatusUrl,
      bifastPaymentStatusRequest
    );

    const data: IBifastPaymentStatusResponse = get(response, ['data'], {});
    logger.info(
      `getPaymentStatus: got response from BI Fast reponse : 
        transactionId: ${data.paymentStatusResponse.transactionId},
        transactionDate: ${data.paymentStatusResponse.transactionDate},
        komiUniqueId: ${data.paymentStatusResponse.komiUniqueId},
        transactionResponseId: ${data.paymentStatusResponse.transactionResponseId},
        responseCode: ${data.paymentStatusResponse.responseCode},
        responseMessage:${data.paymentStatusResponse.responseMessage}`
    );
    return data.paymentStatusResponse;
  } catch (error) {
    const details = 'Error from BI-FAST while getting transaction status!';
    logger.error(`getPaymentStatus: ${details}`, error && error.message);
    throw new TransferAppError(
      details,
      TransferFailureReasonActor.BIFAST,
      ERROR_CODE.ERROR_FROM_BIFAST
    );
  }
};

const bifastRepository = wrapLogs({
  submitTransaction,
  getPaymentStatus
});

export default bifastRepository;
