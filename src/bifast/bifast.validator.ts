import Joi from '@hapi/joi';
import validations from './bifast.constant';
import transaction from '../transaction/transaction.constant';
import { JoiValidator } from '@dk/module-common';
import { ConfirmTransactionStatus } from '../transaction/transaction.enum';
import { incomingTransactionDate } from '../transaction/transaction.validator';

export const biFastConfirmTransactionRequestValidator = Joi.object({
  externalId: Joi.string()
    .required()
    .max(validations.MAX_LENGTH_BI_FAST_EXTERNAL_ID)
    .description('Unique identity of transaction'),
  accountNo: JoiValidator.requiredString()
    .max(transaction.MAX_LENGTH_BENEFICIARY_ACCOUNT_NO)
    .description('Account Number with max length 40'),
  transactionDate: incomingTransactionDate,
  responseCode: Joi.number()
    .required()
    .valid(Object.values(ConfirmTransactionStatus))
    .description('Response code for confirming transaction'),
  responseMessage: Joi.string()
    .max(transaction.MAX_LENGTH_TRANSACTION_RESPONSE_MESSAGE)
    .description('Response Message for confirming transaction'),
  komiUniqueId: Joi.string()
    .optional()
    .max(validations.MAX_LENGTH_BI_FAST_KOMI_ID)
    .description('Unique identity of BI Fast Komi')
}).label('BIFastConfirmTransactionRequest');

export const biFastConfirmTransactionResponseValidator = Joi.object({
  externalId: Joi.string()
    .required()
    .max(validations.MAX_LENGTH_BI_FAST_EXTERNAL_ID)
    .description('External unique identity of transaction')
}).label('BIFastConfirmTransactionResponses');
