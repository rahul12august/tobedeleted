export enum CustomerType {
  INDIVIDUAL = '1',
  CORPORATE = '2',
  GOVERNMENT = '3',
  REMITTANCE = '4',
  OTHERS = '99'
}

export enum ResidentialStatus {
  RESIDENT = '1',
  NON_RESIDENT = '2'
}

export enum ChannelType {
  INTERNET_BANKING = '01',
  MOBILE_BANKING = '02',
  OVER_THE_COUNTER = '03',
  OTHER = '99'
}

export enum AccountType {
  CURRENT_ACCOUNT = 'CACC',
  SAVINGS_ACCOUNT = 'SVGS',
  LOAN = 'LOAN',
  CREDIT_CARD = 'CCRD',
  E_MONEY = 'UESB',
  OTHER = 'OTHR'
}

export enum BeneficiaryProxyType {
  ACCOUNT_NUMBER = '00',
  PHONE_NUMBER = '01',
  EMAIL = '02',
  IPT_ID = '03'
}

export enum BifastResponseCode {
  SUCCESS = '200',
  FAILED = '400',
  TIMEDOUT = '408'
}
