import { BeneficiaryProxyType } from './bifast.enum';

export const SOURCE_ACCOUNT_TYPE = 'SVGS';
export const CHANNEL_TYPE = '02';
export const BIFAST_PAYMENT_SERVICE_TYPE = 'OUTGOING_BIFAST';
export const BIFAST_VOID_PAYMENT_SERVICE_CODE = 'REFUND_BIFAST';
export const BIFAST_OUTGOING_PAYMENT_SERVICE_CODE = 'BIFAST_OUTGOING';
export const BIFAST_PROXY_BANK_CODE = 'BC000';
export const CUSTOMER_TYPE_CORPORATE = 'CORPORATE';
export const BIFAST_PROXY_TYPES = [
  BeneficiaryProxyType.PHONE_NUMBER,
  BeneficiaryProxyType.EMAIL,
  BeneficiaryProxyType.IPT_ID
];
export default {
  MAX_LENGTH_BI_FAST_EXTERNAL_ID: 24,
  MAX_LENGTH_BI_FAST_KOMI_ID: 36
};
