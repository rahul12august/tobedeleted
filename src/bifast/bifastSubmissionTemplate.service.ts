import { ITransactionSubmissionTemplate } from '../transaction/transaction.type';
import { ITransactionModel } from '../transaction/transaction.model';
import { BankChannelEnum, Rail } from '@dk/module-common';
import { TransferAppError } from '../errors/AppError';
import transactionUtility, { isBIFast } from '../transaction/transaction.util';
import logger, { wrapLogs } from '../logger';
import {
  mapToBifastTransaction,
  populateDefaultBiFastMandatoryFields,
  validateBiFastMandatoryFields
} from './bifast.helper';
import { ERROR_CODE } from '../common/errors';
import { isEmpty } from 'lodash';
import bifastRepository from './bifast.repository';
import customerService from '../customer/customer.service';
import { updateRequestIdInRedis } from '../common/contextHandler';
import transactionRepository from '../transaction/transaction.repository';
import { AccountType, CustomerType } from './bifast.enum';
import { CUSTOMER_TYPE_CORPORATE } from './bifast.constant';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const submitTransaction = async (
  transactionModel: ITransactionModel
): Promise<ITransactionModel> => {
  try {
    if (!transactionModel.sourceCustomerId) {
      throw new TransferAppError(
        "Transaction model doesn't contain source customer ID!",
        TransferFailureReasonActor.SUBMITTER,
        ERROR_CODE.INVALID_MANDATORY_FIELDS
      );
    }
    const customerInfo = await customerService.getCustomerById(
      transactionModel.sourceCustomerId
    );

    transactionModel.sourceIdNumber = customerInfo && customerInfo.idNumber;
    if (customerInfo && customerInfo.customerType == CUSTOMER_TYPE_CORPORATE) {
      transactionModel.sourceIdNumber = customerInfo.npwpNumber;
    }

    transactionModel.sourceAccountTypeBI = AccountType.SAVINGS_ACCOUNT;
    transactionModel.sourceCustomerType = '0' + CustomerType.INDIVIDUAL;
    transactionModel = populateDefaultBiFastMandatoryFields(transactionModel);
    validateBiFastMandatoryFields(transactionModel);
    if (!transactionModel.thirdPartyOutgoingId) {
      const detail =
        'Bifast third party outgoing id is missing while doing transaction!';
      logger.error(detail);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.SUBMITTER,
        ERROR_CODE.INVALID_EXTERNALID
      );
    }
    const bifastTransactionPayload = mapToBifastTransaction(transactionModel);

    if (transactionModel.thirdPartyOutgoingId)
      updateRequestIdInRedis(transactionModel.thirdPartyOutgoingId);

    if (transactionModel.paymentInstructionId)
      updateRequestIdInRedis(transactionModel.paymentInstructionId);

    const {
      updatedTransaction
    } = await transactionUtility.submitThirdPartyTransactionWithRetryAndUpdate(
      bifastRepository.submitTransaction,
      bifastTransactionPayload,
      transactionModel
    );
    transactionModel = updatedTransaction;
  } catch (error) {
    logger.error(
      `failed submission to paymentServiceType: ${
        transactionModel.paymentServiceType
      }, paymentServiceCode: ${
        transactionModel.paymentServiceCode
      } error: ${JSON.stringify(error)}`
    );

    await transactionRepository.update(transactionModel.id, {
      sourceIdNumber: transactionModel.sourceIdNumber,
      sourceCustomerType: transactionModel.sourceCustomerType,
      sourceAccountTypeBI: transactionModel.sourceAccountTypeBI
    });

    if (error.errorCode !== ERROR_CODE.BIFAST_REQUEST_TIMEOUT) {
      throw error;
    }
  }

  return transactionModel;
};

const isEligible = (model: ITransactionModel): boolean => {
  if (model.paymentServiceCode) {
    return (
      isBIFast(model.paymentServiceCode) &&
      BankChannelEnum.EXTERNAL === model.beneficiaryBankCodeChannel &&
      !isEmpty(model.beneficiaryAccountNo)
    );
  }
  return false;
};

const shouldSendNotification = () => true;

const getRail = () => Rail.BI_FAST;

const bifastSubmissionTemplate: ITransactionSubmissionTemplate = {
  submitTransaction,
  isEligible,
  getRail,
  shouldSendNotification
};

export default wrapLogs(bifastSubmissionTemplate);
