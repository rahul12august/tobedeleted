import logger, { wrapLogs } from '../logger';
import { IConfirmTransactionInput } from '../transaction/transaction.type';
import transactionConfirmationService from '../transaction/transactionConfirmation.service';
import {
  BiFastConfirmTransactionRequest,
  BiFastConfirmTransactionResponse
} from './bifast.type';

const confirmTransaction = async (
  input: BiFastConfirmTransactionRequest
): Promise<BiFastConfirmTransactionResponse> => {
  logger.info(
    `BI Fast: Transaction Confirmation with external id: ${input.externalId} status: ${input.responseCode}`
  );
  const transactionInput: IConfirmTransactionInput = {
    ...input
  };
  logger.info(
    `BI Fast Confirmation: Sending request to transaction confirmation.`
  );
  await transactionConfirmationService.confirmTransaction(transactionInput);

  return {
    externalId: input.externalId
  };
};

const biFastService = wrapLogs({
  confirmTransaction
});

export default biFastService;
