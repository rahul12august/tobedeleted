import * as Joi from '@hapi/joi';
import {
  biFastConfirmTransactionRequestValidator,
  biFastConfirmTransactionResponseValidator
} from './bifast.validator';
import { BifastResponseCode } from './bifast.enum';
import { BankChannelEnum } from '@dk/module-common';

export type BiFastConfirmTransactionRequest = Joi.extractType<
  typeof biFastConfirmTransactionRequestValidator
>;

export type BiFastConfirmTransactionResponse = Joi.extractType<
  typeof biFastConfirmTransactionResponseValidator
>;

export interface IBifastTransactionRequest {
  creditTransferRequest: IBifastRequestPayload;
}

export interface IBifastRequestPayload {
  transactionId: string;
  transactionDate: string;
  paymentServiceType: string;
  channelType: string;
  sourceBankCode: string;
  sourceAccountNo: string;
  sourceAccountName: string;
  sourceCustomerType: string;
  sourceAccountType: string;
  sourceResidentialStatus: string;
  sourceNik: string;
  transactionAmount: string;
  beneficiaryBankCode: string;
  beneficiaryAccountNo: string;
  beneficiaryAccountName: string;
  beneficiaryAccountType: string | null;
  beneficiaryCustomerType: string;
  beneficiaryResidentialStatus: string;
  beneficiaryProxyType: string | null;
  notes?: string | null;
  beneficiaryProxyAlias?: string | null;
}

export interface IBifastTransactionResponse {
  creditTransferResponse: IBifastResponsePayload;
}

export interface IBifastResponsePayload {
  transactionId: string;
  responseCode: BifastResponseCode;
  responseMessage: string;
  komiUniqueId: string;
}

export interface IBifastPaymentStatusRequest {
  paymentStatusRequest: IBifastPaymentStatusRequestPayload;
}

export interface IBifastPaymentStatusRequestPayload {
  transactionId: string;
  sourceBankCode: string;
  oriTransactionId: string;
  oriTransactionDate: string;
  sourceAccountNumber: string;
  channelType: string;
}

export interface IBifastPaymentStatusResponse {
  paymentStatusResponse: IBifastPaymentStatusResponsePayload;
}

export interface IBifastPaymentStatusResponsePayload {
  transactionId: string;
  transactionDate: string;
  sourceAccountNo: string;
  interchange: BankChannelEnum;
  transactionAmount: string;
  transactionResponseId: string;
  responseCode: string;
  responseMessage: string;
  komiUniqueId: string;
}
