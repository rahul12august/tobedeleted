import bifastRepository from '../bifast.repository';
import { BankChannelEnum, Rail, Util } from '@dk/module-common';
import { getTransactionDetail } from '../../transaction/__mocks__/transaction.data';
import bifastSubmissionTemplate from '../bifastSubmissionTemplate.service';

import { getMockedTransactionRequest } from '../__mocks__/bifastTransaction.data';
import { IBifastTransactionRequest } from '../bifast.type';
import transactionRepository from '../../transaction/transaction.repository';
import {
  mapToBifastTransaction,
  populateDefaultBiFastMandatoryFields,
  validateBiFastMandatoryFields
} from '../bifast.helper';
import customerService from '../../customer/customer.service';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';
import { BeneficiaryProxyType, CustomerType, ResidentialStatus } from '../bifast.enum';

jest.mock('../bifast.repository');
jest.mock('../bifast.helper');
jest.mock('../../transaction/transaction.repository');
jest.mock('../../transaction/transaction.producer');
jest.mock('../../customer/customer.service');

describe('bifastSubmissionTemplate', () => {
  class NetworkError extends Error {
    status: number;
    code?: string;
    constructor(status = 501, code?: string) {
      super();
      this.status = status;
      this.code = code;
    }
  }

  let mockedRetry = jest
    .spyOn(Util, 'retry')
    .mockImplementation(async (_, fn, ...args) => await fn(...args));

  // given
  const transactionModel = {
    ...getTransactionDetail(),
    beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
    requireThirdPartyOutgoingId: true,
    debitTransactionCode: 'TFD60',
    thirdPartyOutgoingId: '21042T000041',
    transactionAmount: 150000000,
    paymentServiceCode: 'BIFAST_OUTGOING',
    sourceCustomerId: 'sourceCustomerId'
  };

  describe('isEligible check', () => {
    it(`should return true when paymentServiceCode is BIFAST_OUTGOING`, () => {
      // when
      const eligible = bifastSubmissionTemplate.isEligible(transactionModel);

      // then
      expect(eligible).toBeTruthy();
    });

    it(`should return true when paymentServiceCode is BIFAST_OUTGOING_SHARIA`, () => {
      // when
      const eligible = bifastSubmissionTemplate.isEligible({
        ...transactionModel,
        paymentServiceCode: 'BIFAST_OUTGOING_SHARIA'
      });

      // then
      expect(eligible).toBeTruthy();
    });

    it(`should return true when paymentServiceCode is RTGS_FOR_BUSINESS`, () => {
      // when
      const eligible = bifastSubmissionTemplate.isEligible({
        ...transactionModel,
        paymentServiceCode: 'RTGS_FOR_BUSINESS'
      });

      // then
      expect(eligible).toBeFalsy();
    });

    it(`should return false when beneficiaryBankCodeChannel is not ${BankChannelEnum.EXTERNAL}`, () => {
      // when
      const eligible = bifastSubmissionTemplate.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL
      });

      // then
      expect(eligible).toBeFalsy();
    });

    it('should return false when beneficiaryAccountNo is undefined', async () => {
      // when
      const eligible = bifastSubmissionTemplate.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        beneficiaryAccountNo: undefined
      });

      // then
      expect(eligible).toBeFalsy();
    });

    it('should return false when paymentServiceCode is undefined', async () => {
      // when
      const eligible = bifastSubmissionTemplate.isEligible({
        ...transactionModel,
        paymentServiceCode: undefined
      });

      // then
      expect(eligible).toBeFalsy();
    });
  });

  describe('submitTransaction', () => {
    const mockedBIFASTModel: IBifastTransactionRequest = {
      creditTransferRequest: getMockedTransactionRequest()
    };
    const mockedBIFASTResponse = {};
    beforeEach(() => {
      (mapToBifastTransaction as jest.Mock).mockReturnValueOnce(
        mockedBIFASTModel
      );
      (bifastRepository.submitTransaction as jest.Mock).mockResolvedValue(
        mockedBIFASTResponse
      );
      (populateDefaultBiFastMandatoryFields as jest.Mock).mockImplementation(
        transactionModel => {
          return {
            ...transactionModel,
            additionalInformation1:
              transactionModel.additionalInformation1 ??
              CustomerType.INDIVIDUAL,
            additionalInformation2:
              transactionModel.additionalInformation2 ??
              ResidentialStatus.RESIDENT,
            additionalInformation3:
              transactionModel.additionalInformation3 ?? BeneficiaryProxyType.ACCOUNT_NUMBER,
            additionalInformation4:
              transactionModel.additionalInformation4 ?? transactionModel.beneficiaryAccountNo
          };
        }
      );
      (validateBiFastMandatoryFields as jest.Mock).mockReturnValueOnce(null);
      (customerService.getCustomerById as jest.Mock).mockReturnValueOnce({
        cif: 'cif',
        id: 'id',
        idNumber: 'idNumber'
      });
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should submit transaction to bifast with correct payload', async () => {
      // Given
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});

      // When
      const result = await bifastSubmissionTemplate.submitTransaction(
        transactionModel
      );

      // Then
      expect(result).toBeDefined();
      expect(bifastRepository.submitTransaction).toHaveBeenCalledWith(
        mockedBIFASTModel
      );
      expect(customerService.getCustomerById).toBeCalledTimes(1);
    });

    it('should throw error when sourceCustomerId not provided on transactionModel', async () => {
      // Given
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});
      let invalidTransactionModel = {
        ...transactionModel,
        sourceCustomerId: undefined
      };

      // When
      const error = await bifastSubmissionTemplate
        .submitTransaction(invalidTransactionModel)
        .catch(err => err);

      // Then
      expect(error).toBeDefined();
      expect(customerService.getCustomerById).not.toBeCalled();
      expect(mapToBifastTransaction).not.toBeCalled();
      expect(bifastRepository.submitTransaction).not.toBeCalled();
    });

    it('should throw error when thirdparty outgoing id is missing', async () => {
      // Given
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});
      const model = {
        ...transactionModel,
        thirdPartyOutgoingId: undefined
      };
      let error, result;

      // When
      try {
        result = await bifastSubmissionTemplate.submitTransaction(model);
      } catch (e) {
        error = e;
      }

      // Then
      expect(result).toBeUndefined();
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_EXTERNALID);
      expect(transactionRepository.update).toHaveBeenCalledTimes(1);
    });

    it('should retry first times when BIFAST repository throw network error before giving response', async () => {
      // Given
      const networkError = new NetworkError();
      const expectedNetworkError = {
        ...networkError,
        code: 'ECONNABORTED'
      };
      mockedRetry.mockRestore();
      (transactionRepository.update as jest.Mock).mockReturnValue({});
      (transactionRepository.getByKey as jest.Mock).mockReturnValueOnce({
        ...transactionModel,
        thirdPartyOutgoingId: transactionModel.thirdPartyOutgoingId.padStart(
          16,
          '0'
        )
      });
      (transactionRepository.findOneByQuery as jest.Mock).mockReturnValueOnce(
        {}
      );
      (bifastRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        expectedNetworkError
      );
      let error, result;

      // When
      try {
        result = await bifastSubmissionTemplate.submitTransaction(
          transactionModel
        );
      } catch (e) {
        error = e;
      }

      // Then
      expect(result).toBeUndefined();
      expect(bifastRepository.submitTransaction).toHaveBeenCalledTimes(1);
      expect(transactionRepository.update).toHaveBeenCalledTimes(2);
      expect(transactionRepository.findOneByQuery).not.toHaveBeenCalled();
      expect(error.code).toEqual('ECONNABORTED');
      expect(error.status).toEqual(501);
    });

    it('should be in submitted the transaction when bifast returned bad request error', async () => {
      // Given
      (bifastRepository.submitTransaction as jest.Mock).mockResolvedValue(
        transactionModel
      );

      // When
      const result = await bifastSubmissionTemplate.submitTransaction(
        transactionModel
      );

      // Then
      expect(result).toBeDefined();
      expect(bifastRepository.submitTransaction).toHaveBeenCalledTimes(1);
      expect(transactionRepository.update).toHaveBeenCalledTimes(1);
    });

    it('should update the transaction to submitted when bifast returned timeout error 408', async () => {
      // given
      (bifastRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.BIFAST_REQUEST_TIMEOUT
        )
      );
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});
      (transactionRepository.findOneByQuery as jest.Mock).mockReturnValueOnce(
        {}
      );

      // when
      const result = await bifastSubmissionTemplate
        .submitTransaction(transactionModel)
        .catch(err => err);

      // then
      expect(result).not.toBeInstanceOf(TransferAppError);
      expect(bifastRepository.submitTransaction).toHaveBeenCalledTimes(1);
      expect(transactionRepository.update).toHaveBeenCalledTimes(2);
      expect(transactionRepository.findOneByQuery).not.toHaveBeenCalled();
    });
  });

  describe('Rail check', () => {
    it(`should have rail property defined`, () => {
      // when
      const rail = bifastSubmissionTemplate.getRail?.();

      // then
      expect(rail).toEqual(Rail.BI_FAST);
    });
  });
});
