import {
  validateBiFastMandatoryFields,
  mapToBifastTransaction,
  mapBIFastTransactionStatus,
  populateDefaultBiFastMandatoryFields
} from '../bifast.helper';
import { ERROR_CODE } from '../../common/errors';
import { ITransactionModel } from '../../transaction/transaction.model';
import { TransferAppError } from '../../errors/AppError';
import {
  CustomerType,
  AccountType,
  BeneficiaryProxyType,
  ResidentialStatus
} from '../bifast.enum';
import { BIFAST_PAYMENT_SERVICE_TYPE, CHANNEL_TYPE } from '../bifast.constant';
import { transactionModel } from '../__mocks__/bifastTransaction.data';
import { toJKTDate } from '../../common/dateUtils';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

describe('bifast.helper', () => {
  beforeEach(() => { });
  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('validateBiFastMandatoryFields', () => {
    it('should return error when beneficiaryAccountNo is undefined', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        beneficiaryAccountNo: undefined
      };
      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Bifast validation failed!');
    });

    it('should return error when beneficiaryAccountName is undefined', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        beneficiaryAccountName: undefined
      };
      // when
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Bifast validation failed!');
    });

    it('should return error when beneficiaryBankCode is undefined', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        beneficiaryBankCode: undefined
      };
      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Bifast validation failed!');
    });

    it('should return error when beneficiaryAccountType is undefined', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        beneficiaryAccountType: undefined
      };

      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }

      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Bifast validation failed!');
    });

    it('should return error when sourceAccountNo is undefined', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        sourceAccountNo: undefined
      };
      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Bifast validation failed!');
    });

    it('should return error when sourceAccountName is undefined', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        sourceAccountName: undefined
      };
      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Bifast validation failed!');
    });

    it('should return error when additionalInformation1 is undefined', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation1: undefined
      };
      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Bifast validation failed!');
    });

    it('should return error when additionalInformation1 is invalid', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation1: '9'
      };
      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Bifast validation failed!');
    });

    it('should return error when additionalInformation2 is undefined', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation2: undefined
      };
      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Bifast validation failed!');
    });

    it('should return error when additionalInformation2 is invalid', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation2: '9'
      };
      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Bifast validation failed!');
    });

    it('should return error when sourceIdNumber (sourceNik) is invalid', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        sourceIdNumber: undefined
      };

      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }

      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Bifast validation failed!');
    });

    it('should return error when additionalInformation3 is IPT_ID & additionalInformation4 is undefined', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation3: BeneficiaryProxyType.IPT_ID,
        additionalInformation4: undefined
      };
      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Bifast validation failed!');
    });

    it('should return error when additionalInformation3 is PHONE_NUMBER & additionalInformation4 is undefined', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation3: BeneficiaryProxyType.PHONE_NUMBER,
        additionalInformation4: undefined
      };
      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Bifast validation failed!');
    });

    it('should return error when additionalInformation3 is Email & additionalInformation4 is undefined', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation3: BeneficiaryProxyType.EMAIL,
        additionalInformation4: undefined
      };
      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual('Bifast validation failed!');
    });

    it('should return void when additionalInformation3 is Email & additionalInformation4 is proxy value', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation3: BeneficiaryProxyType.EMAIL,
        additionalInformation4: 'dummy@gmail.com'
      };
      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeUndefined();
    });

    it('should return void when additionalInformation3 is Account & additionalInformation4 is undefined', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation3: BeneficiaryProxyType.ACCOUNT_NUMBER,
        additionalInformation4: undefined
      };
      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeUndefined();
    });

    it('should return void when additionalInformation3 & additionalInformation4 is undefined', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation3: undefined,
        additionalInformation4: undefined
      };
      // When
      let error;
      try {
        validateBiFastMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeUndefined();
    });

    it('should return void when mandatory validations passed', () => {
      // When
      let error;
      try {
        validateBiFastMandatoryFields(transactionModel);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeUndefined();
    });
  });

  describe('mapToBifastTransaction', () => {
    it('should return mapToBifastTransaction', () => {
      // Given
      transactionModel.additionalInformation3 = '01'; // beneficiaryProxyType
      transactionModel.additionalInformation4 = '+628112233'; // beneficiaryProxyAlias
      transactionModel.sourceAccountTypeBI = 'SVGS'; // sourceCustomerTypeBI
      transactionModel.sourceCustomerType = '01'; // sourceCustomerType

      // When
      const transactionEntity = mapToBifastTransaction(transactionModel);

      // Then
      expect(transactionEntity).toBeDefined();
      expect(transactionEntity).toEqual({
        transactionId: transactionModel.thirdPartyOutgoingId.substr(
          transactionModel.thirdPartyOutgoingId.length - 8
        ),
        transactionDate: toJKTDate(transactionModel.createdAt, 'YYMMDDHHmmss'),
        paymentServiceType: BIFAST_PAYMENT_SERVICE_TYPE,
        channelType: CHANNEL_TYPE,
        transactionAmount: transactionModel.transactionAmount.toString(),
        beneficiaryBankCode: transactionModel.beneficiaryRemittanceCode || '',
        beneficiaryAccountName: transactionModel.beneficiaryAccountName || '',
        beneficiaryAccountNo: transactionModel.beneficiaryAccountNo || '',
        beneficiaryAccountType: AccountType.SAVINGS_ACCOUNT,
        beneficiaryCustomerType: '01',
        beneficiaryResidentialStatus: '01',
        sourceNik: transactionModel.sourceIdNumber,
        sourceAccountNo: transactionModel.sourceAccountNo || '',
        sourceAccountName: transactionModel.sourceAccountName || '',
        sourceAccountType: AccountType.SAVINGS_ACCOUNT,
        sourceCustomerType: '01',
        sourceBankCode: transactionModel.sourceRemittanceCode,
        sourceResidentialStatus: '01',
        notes: null,
        beneficiaryProxyType: transactionModel.additionalInformation3,
        beneficiaryProxyAlias: '628112233'
      });
    });

    it('should return mapToBifastTransaction when beneficiaryCustomerType is OTHERS', () => {
      // Given
      transactionModel.additionalInformation1 = '99'; // beneficiaryCustomerType
      transactionModel.additionalInformation3 = '01'; // beneficiaryProxyType
      transactionModel.additionalInformation4 = '+628112233'; // beneficiaryProxyAlias

      // When
      const transactionEntity = mapToBifastTransaction(transactionModel);

      // Then
      expect(transactionEntity).toBeDefined();
      expect(transactionEntity).toEqual({
        transactionId: transactionModel.thirdPartyOutgoingId.substr(
          transactionModel.thirdPartyOutgoingId.length - 8
        ),
        transactionDate: toJKTDate(transactionModel.createdAt, 'YYMMDDHHmmss'),
        paymentServiceType: BIFAST_PAYMENT_SERVICE_TYPE,
        channelType: CHANNEL_TYPE,
        transactionAmount: transactionModel.transactionAmount.toString(),
        beneficiaryBankCode: transactionModel.beneficiaryRemittanceCode || '',
        beneficiaryAccountName: transactionModel.beneficiaryAccountName || '',
        beneficiaryAccountNo: transactionModel.beneficiaryAccountNo || '',
        beneficiaryAccountType: AccountType.SAVINGS_ACCOUNT,
        beneficiaryCustomerType: CustomerType.OTHERS,
        beneficiaryResidentialStatus: '01',
        sourceNik: transactionModel.sourceIdNumber,
        sourceAccountNo: transactionModel.sourceAccountNo || '',
        sourceAccountName: transactionModel.sourceAccountName || '',
        sourceAccountType: AccountType.SAVINGS_ACCOUNT,
        sourceCustomerType: '01',
        sourceBankCode: transactionModel.sourceRemittanceCode,
        sourceResidentialStatus: '01',
        notes: null,
        beneficiaryProxyType: transactionModel.additionalInformation3,
        beneficiaryProxyAlias: '628112233'
      });
    });

    it('should return mapToBifastTransaction with beneficiaryProxyType, beneficiaryProxyAlias, beneficiaryCustomerType, beneficiaryResidentialStatus are undefined', () => {
      // When
      const transactionEntity = mapToBifastTransaction({
        ...transactionModel,
        additionalInformation1: undefined, // beneficiaryCustomerType
        additionalInformation2: undefined, // beneficiaryResidentialStatus
        additionalInformation3: undefined, // beneficiaryProxyType
        additionalInformation4: undefined, // beneficiaryProxyAlias
        sourceAccountName: '',
        beneficiaryAccountName: ''
      });
      // Then
      expect(transactionEntity).toBeDefined();
      expect(transactionEntity).toEqual({
        transactionId: transactionModel.thirdPartyOutgoingId.substr(
          transactionModel.thirdPartyOutgoingId.length - 8
        ),
        transactionDate: toJKTDate(transactionModel.createdAt, 'YYMMDDHHmmss'),
        paymentServiceType: BIFAST_PAYMENT_SERVICE_TYPE,
        channelType: CHANNEL_TYPE,
        transactionAmount: transactionModel.transactionAmount.toString(),
        beneficiaryBankCode: transactionModel.beneficiaryRemittanceCode || '',
        beneficiaryAccountName: '',
        beneficiaryAccountNo: transactionModel.beneficiaryAccountNo || '',
        beneficiaryAccountType: AccountType.SAVINGS_ACCOUNT,
        beneficiaryCustomerType: '',
        beneficiaryResidentialStatus: '',
        sourceNik: transactionModel.sourceIdNumber,
        sourceAccountNo: transactionModel.sourceAccountNo || '',
        sourceAccountName: '',
        sourceAccountType: AccountType.SAVINGS_ACCOUNT,
        sourceCustomerType: '01',
        sourceBankCode: transactionModel.sourceRemittanceCode,
        sourceResidentialStatus: '01',
        notes: transactionModel.note || null,
        beneficiaryProxyType: null,
        beneficiaryProxyAlias: null
      });
    });

    it('should return mapToBifastTransaction with some data are undefined', () => {
      // When
      const transactionEntity = mapToBifastTransaction({
        ...transactionModel,
        additionalInformation1: '99', // beneficiaryCustomerType
        additionalInformation2: undefined, // beneficiaryResidentialStatus
        additionalInformation3: undefined, // beneficiaryProxyType
        additionalInformation4: undefined, // beneficiaryProxyAlias
        sourceIdNumber: undefined,
        sourceAccountName: undefined,
        beneficiaryAccountName: undefined,
        sourceAccountTypeBI: undefined,
        sourceCustomerType: undefined,
        sourceRemittanceCode: undefined,
        sourceAccountNo: undefined,
        createdAt: undefined,
        beneficiaryRemittanceCode: undefined,
        beneficiaryAccountType: undefined,
        beneficiaryAccountNo: ''
      });

      // Then
      expect(transactionEntity).toBeDefined();
      expect(transactionEntity).toEqual({
        transactionId: transactionModel.thirdPartyOutgoingId.substr(
          transactionModel.thirdPartyOutgoingId.length - 8
        ),
        transactionDate: toJKTDate(new Date(), 'YYMMDDHHmmss'),
        paymentServiceType: BIFAST_PAYMENT_SERVICE_TYPE,
        channelType: CHANNEL_TYPE,
        transactionAmount: transactionModel.transactionAmount.toString(),
        beneficiaryBankCode: '',
        beneficiaryAccountName: '',
        beneficiaryAccountNo: 'null',
        beneficiaryAccountType: null,
        beneficiaryCustomerType: '99',
        beneficiaryResidentialStatus: '',
        sourceNik: '',
        sourceAccountNo: '',
        sourceAccountName: '',
        sourceAccountType: '',
        sourceCustomerType: '',
        sourceBankCode: '',
        sourceResidentialStatus: '01',
        notes: transactionModel.note || null,
        beneficiaryProxyType: null,
        beneficiaryProxyAlias: null
      });
    });

    it('should return mapToBifastTransaction without plus sign when additionalInformation4 is phone number without + sign', () => {
      // When
      const transactionEntity = mapToBifastTransaction({
        ...transactionModel,
        additionalInformation1: '1', // beneficiaryCustomerType
        additionalInformation2: '1', // beneficiaryResidentialStatus
        additionalInformation3: '01', // beneficiaryProxyType
        additionalInformation4: '628112233' // beneficiaryProxyAlias
      });
      // Then
      expect(transactionEntity).toBeDefined();
      expect(transactionEntity).toEqual({
        transactionId: transactionModel.thirdPartyOutgoingId.substr(
          transactionModel.thirdPartyOutgoingId.length - 8
        ),
        transactionDate: toJKTDate(transactionModel.createdAt, 'YYMMDDHHmmss'),
        paymentServiceType: BIFAST_PAYMENT_SERVICE_TYPE,
        channelType: CHANNEL_TYPE,
        transactionAmount: transactionModel.transactionAmount.toString(),
        beneficiaryBankCode: transactionModel.beneficiaryRemittanceCode || '',
        beneficiaryAccountName: transactionModel.beneficiaryAccountName || '',
        beneficiaryAccountNo: transactionModel.beneficiaryAccountNo || '',
        beneficiaryAccountType: AccountType.SAVINGS_ACCOUNT,
        beneficiaryCustomerType: '01',
        beneficiaryResidentialStatus: '01',
        sourceNik: transactionModel.sourceIdNumber,
        sourceAccountNo: transactionModel.sourceAccountNo || '',
        sourceAccountName: transactionModel.sourceAccountName || '',
        sourceAccountType: AccountType.SAVINGS_ACCOUNT,
        sourceCustomerType: '01',
        sourceBankCode: transactionModel.sourceRemittanceCode,
        sourceResidentialStatus: '01',
        notes: null,
        beneficiaryProxyType: transactionModel.additionalInformation3,
        beneficiaryProxyAlias: '628112233'
      });
    });

    it('should return mapToBifastTransaction with plus sign when additionalInformation4 is email address with + sign', () => {
      // When
      const transactionEntity = mapToBifastTransaction({
        ...transactionModel,
        additionalInformation1: '1', // beneficiaryCustomerType
        additionalInformation2: '1', // beneficiaryResidentialStatus
        additionalInformation3: '02', // beneficiaryProxyType
        additionalInformation4: 'test+@gmail.com' // beneficiaryProxyAlias
      });
      // Then
      expect(transactionEntity).toBeDefined();
      expect(transactionEntity).toEqual({
        transactionId: transactionModel.thirdPartyOutgoingId.substr(
          transactionModel.thirdPartyOutgoingId.length - 8
        ),
        transactionDate: toJKTDate(transactionModel.createdAt, 'YYMMDDHHmmss'),
        paymentServiceType: BIFAST_PAYMENT_SERVICE_TYPE,
        channelType: CHANNEL_TYPE,
        transactionAmount: transactionModel.transactionAmount.toString(),
        beneficiaryBankCode: transactionModel.beneficiaryRemittanceCode || '',
        beneficiaryAccountName: transactionModel.beneficiaryAccountName || '',
        beneficiaryAccountNo: transactionModel.beneficiaryAccountNo || '',
        beneficiaryAccountType: AccountType.SAVINGS_ACCOUNT,
        beneficiaryCustomerType: '01',
        beneficiaryResidentialStatus: '01',
        sourceNik: transactionModel.sourceIdNumber,
        sourceAccountNo: transactionModel.sourceAccountNo || '',
        sourceAccountName: transactionModel.sourceAccountName || '',
        sourceAccountType: AccountType.SAVINGS_ACCOUNT,
        sourceCustomerType: '01',
        sourceBankCode: transactionModel.sourceRemittanceCode,
        sourceResidentialStatus: '01',
        notes: null,
        beneficiaryProxyType: '02',
        beneficiaryProxyAlias: 'test+@gmail.com'
      });
    });
  });

  describe('mapBIFastTransactionStatus', () => {
    test('should return BI Fast transaction status payload', () => {
      const mappedPayload = mapBIFastTransactionStatus({
        ...transactionModel
      });

      expect(mappedPayload).toEqual(
        expect.objectContaining({
          channelType: '02',
          oriTransactionId: '00000041',
          sourceAccountNumber: '1079253293',
          sourceBankCode: 'ATOSIDJ1',
          transactionId: '00000041'
        })
      );
    });

    test('should return BI Fast transaction status payload when sourceAccountNumber is undefined', () => {
      const mappedPayload = mapBIFastTransactionStatus({
        ...transactionModel,
        sourceAccountNo: undefined
      });

      expect(mappedPayload).toEqual(
        expect.objectContaining({
          channelType: '02',
          oriTransactionId: '00000041',
          sourceAccountNumber: '',
          sourceBankCode: 'ATOSIDJ1',
          transactionId: '00000041'
        })
      );
    });
  });

  describe('populateDefaultBiFastMandatoryFields', () => {
    it('should return default additionalInformation fields when undefined', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation1: undefined,
        additionalInformation2: undefined,
        additionalInformation3: undefined,
        additionalInformation4: undefined,
      };
      // When
      const populatedTransaction = populateDefaultBiFastMandatoryFields(
        originalTransaction
      );

      // Then
      expect(populatedTransaction.additionalInformation1).toEqual(
        CustomerType.INDIVIDUAL
      );
      expect(populatedTransaction.additionalInformation2).toEqual(
        ResidentialStatus.RESIDENT
      );
      expect(populatedTransaction.additionalInformation3).toEqual(
        BeneficiaryProxyType.ACCOUNT_NUMBER
      );
      expect(populatedTransaction.additionalInformation4).toEqual(
        transactionModel.beneficiaryAccountNo
      );
    });

    it('should return additionalInformation fields when set properly', () => {
      // Given
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation1: CustomerType.CORPORATE,
        additionalInformation2: ResidentialStatus.NON_RESIDENT,
        additionalInformation3: BeneficiaryProxyType.PHONE_NUMBER,
        additionalInformation4: transactionModel.beneficiaryAccountNo,
      };
      // When
      const populatedTransaction = populateDefaultBiFastMandatoryFields(
        originalTransaction
      );

      // Then
      expect(populatedTransaction.additionalInformation1).toEqual(
        CustomerType.CORPORATE
      );
      expect(populatedTransaction.additionalInformation2).toEqual(
        ResidentialStatus.NON_RESIDENT
      );
      expect(populatedTransaction.additionalInformation3).toEqual(
        BeneficiaryProxyType.PHONE_NUMBER
      );
      expect(populatedTransaction.additionalInformation4).toEqual(
        transactionModel.beneficiaryAccountNo
      );
    });
  });
});
