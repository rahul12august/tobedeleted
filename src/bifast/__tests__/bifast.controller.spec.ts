import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import ResponseWrapper from '../../plugins/responseWrapper';
import errorHandler from '../../common/handleValidationErrors';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import bifastService from '../bifast.service';
import bifastController from '../bifast.controller';
import entitlementFlag from '../../common/entitlementFlag';
import entitlementService from '../../entitlement/entitlement.service';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

jest.mock('../bifast.service');

describe('bifastController', () => {
  let server: hapi.Server;
  beforeAll(() => {
    server = new hapi.Server({
      routes: {
        validate: {
          options: {
            abortEarly: false
          },
          failAction: errorHandler
        }
      }
    });
    server.register([ResponseWrapper]);
    server.route(bifastController);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('confirm transaction', () => {
    const confirmTransactionPayload = {
      externalId: '132221971879112932032',
      accountNo: '1234567890',
      transactionDate: '210317141902',
      responseCode: 200,
      responseMessage: 'SUCCEED'
    };

    it('should response 200 with no payload', async () => {
      // Given
      const options = {
        method: 'POST',
        url: '/bi-fast/transaction-confirmation',
        payload: confirmTransactionPayload
      };
      (bifastService.confirmTransaction as jest.Mock).mockResolvedValueOnce({
        externalId: 'externalId'
      });

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(bifastService.confirmTransaction).toBeCalledWith(
        confirmTransactionPayload
      );
    });

    it('should return bad request error (400) when service throw AppError', async () => {
      // Given
      const options = {
        method: 'POST',
        url: '/bi-fast/transaction-confirmation',
        payload: confirmTransactionPayload
      };
      (bifastService.confirmTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.INVALID_EXTERNALID
        )
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
      expect(response.result.error.code).toEqual(ERROR_CODE.INVALID_EXTERNALID);
    });

    it('should return server error (500) when service throw server error', async () => {
      // Given
      const options = {
        method: 'POST',
        url: '/bi-fast/transaction-confirmation',
        payload: confirmTransactionPayload
      };
      (bifastService.confirmTransaction as jest.Mock).mockRejectedValueOnce(
        new Error()
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(
        Http.StatusCode.INTERNAL_SERVER_ERROR
      );
    });

    it('should call entitlement reversal service throw Error', async () => {
      // Given
      const options = {
        method: 'POST',
        url: '/bi-fast/transaction-confirmation',
        payload: confirmTransactionPayload
      };
      (bifastService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        new Error()
      );
      entitlementFlag.isEnabled = jest.fn().mockResolvedValue(true);
      entitlementService.processReversalEntitlementCounterUsageOnTransactionConfirmation = jest
        .fn()
        .mockResolvedValue({});

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(
        Http.StatusCode.INTERNAL_SERVER_ERROR
      );
      expect(
        entitlementService.processReversalEntitlementCounterUsageOnTransactionConfirmation
      ).toBeCalled();
    });

    it('should call entitlement reversal service when something went wrong', async () => {
      // Given
      const options = {
        method: 'POST',
        url: '/bi-fast/transaction-confirmation',
        payload: confirmTransactionPayload
      };
      (bifastService.confirmTransaction as jest.Mock).mockRejectedValueOnce(
        new Error()
      );
      entitlementFlag.isEnabled = jest.fn().mockResolvedValue(true);
      entitlementService.processReversalEntitlementCounterUsageOnTransactionConfirmation = jest
        .fn()
        .mockResolvedValue({});
      entitlementService.processReversalEntitlementCounterUsageBasedOnContext = jest
        .fn()
        .mockResolvedValue({});

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(
        Http.StatusCode.INTERNAL_SERVER_ERROR
      );
      expect(
        entitlementService.processReversalEntitlementCounterUsageBasedOnContext
      ).toBeCalled();
    });
  });
});
