import httpClient from '../httpClient';
import bifastRepository from '../bifast.repository';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import {
  getMockedTransactionResponse,
  getMockedTransactionRequest,
  getTransactionStatusRequest,
  getTransactionStatusResponse
} from '../__mocks__/bifastTransaction.data';
import { config } from '../../config';
import { httpClientTimeoutErrorCode } from '../../common/constant';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';
const { transactionUrl } = config.get('bifast');

jest.mock('../httpClient');

describe('bifast.repository', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('submitTransaction', () => {
    const bifastTransaction = getMockedTransactionRequest();
    const mockedResponse = getMockedTransactionResponse();
    it('should return bifast transaction success', async () => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: mockedResponse,
        status: 200
      });

      const expected = await bifastRepository.submitTransaction(
        bifastTransaction
      );

      expect(httpClient.post).toBeCalledWith(transactionUrl, {
        creditTransferRequest: bifastTransaction
      });
      expect(expected).toEqual(mockedResponse);
    });

    it(`should throw error ${ERROR_CODE.ERROR_FROM_BIFAST} when submit error`, async () => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: null
      });

      const expected = await bifastRepository
        .submitTransaction(bifastTransaction)
        .catch(err => err);

      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toBe(ERROR_CODE.ERROR_FROM_BIFAST);
      expect(expected.actor).toBe(TransferFailureReasonActor.BIFAST);
      expect(expected.detail).toBe(
        'Error from BI-FAST while submitting transaction'
      );
    });

    it(`should throw error when bifast api throws 408`, async () => {
      (httpClient.post as jest.Mock).mockRejectedValueOnce({
        message: 'HttpClient Error: Error: Request failed with status code 408',
        code: httpClientTimeoutErrorCode
      });

      const expected = await bifastRepository
        .submitTransaction(bifastTransaction)
        .catch(err => err);

      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.message).toBe(ERROR_CODE.BIFAST_REQUEST_TIMEOUT);
      expect(expected.actor).toBe(TransferFailureReasonActor.BIFAST);
      expect(expected.detail).toBe(
        'Error from BI-FAST while submitting transaction'
      );
    });
  });

  describe('getPaymentStatus', () => {
    const bifastTransaction = getTransactionStatusRequest();
    const mockedResponse = getTransactionStatusResponse();
    const paymentStatusUrl = ':paymentStatus';
    it('should return bifast transaction status', async () => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: mockedResponse,
        status: 200
      });

      const expected = await bifastRepository.getPaymentStatus(
        bifastTransaction
      );

      expect(httpClient.post).toBeCalledWith(paymentStatusUrl, {
        paymentStatusRequest: bifastTransaction
      });
      expect(expected).toEqual(mockedResponse.paymentStatusResponse);
    });

    it(`should throw error ${ERROR_CODE.ERROR_FROM_BIFAST} when data is null`, async () => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: null
      });

      const expected = await bifastRepository
        .getPaymentStatus(bifastTransaction)
        .catch(err => err);

      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toBe(ERROR_CODE.ERROR_FROM_BIFAST);
      expect(expected.actor).toBe(TransferFailureReasonActor.BIFAST);
      expect(expected.detail).toBe(
        'Error from BI-FAST while getting transaction status!'
      );
      expect(httpClient.post).toBeCalledWith(paymentStatusUrl, {
        paymentStatusRequest: bifastTransaction
      });
    });

    it(`should throw error when bifast api throws 408`, async () => {
      (httpClient.post as jest.Mock).mockRejectedValueOnce({
        message: 'HttpClient Error: Error: Request failed with status code 408',
        code: httpClientTimeoutErrorCode
      });

      const expected = await bifastRepository
        .getPaymentStatus(bifastTransaction)
        .catch(err => err);

      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.message).toBe('ERROR_FROM_BIFAST');
      expect(expected.errorCode).toBe(ERROR_CODE.ERROR_FROM_BIFAST);
      expect(expected.actor).toBe(TransferFailureReasonActor.BIFAST);
      expect(expected.detail).toBe(
        'Error from BI-FAST while getting transaction status!'
      );
      expect(httpClient.post).toBeCalledWith(paymentStatusUrl, {
        paymentStatusRequest: bifastTransaction
      });
    });
  });
});
