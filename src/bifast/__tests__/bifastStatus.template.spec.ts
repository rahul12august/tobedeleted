import bifastRepository from '../bifast.repository';
import { BankChannelEnum, Util } from '@dk/module-common';
import { getTransactionDetail } from '../../transaction/__mocks__/transaction.data';
import {
  getTransactionStatusRequest,
  getTransactionStatusResponse
} from '../__mocks__/bifastTransaction.data';
import { IBifastPaymentStatusRequestPayload } from '../bifast.type';
import { httpClientTimeoutErrorCode } from '../../common/constant';
import bifastStatusTemplate from '../bifastStatus.template';
import {
  mapBIFastTransactionStatus,
  populateDefaultBiFastMandatoryFields,
  validateBiFastMandatoryFields
} from '../bifast.helper';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../../transaction/transaction.enum';
import { BeneficiaryProxyType, CustomerType, ResidentialStatus } from '../bifast.enum';

jest.mock('../bifast.repository');
jest.mock('../bifast.helper');

describe('bifastStatusTemplate', () => {
  class NetworkError extends Error {
    status: number;
    code?: string;
    constructor(status = 501, code?: string) {
      super();
      this.status = status;
      this.code = code;
    }
  }
  let mockedRetry = jest
    .spyOn(Util, 'retry')
    .mockImplementation(async (_, fn, ...args) => await fn(...args));

  // given
  const transactionModel = {
    ...getTransactionDetail(),
    beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
    requireThirdPartyOutgoingId: true,
    debitTransactionCode: 'TFD60',
    thirdPartyOutgoingId: '21042T000041',
    transactionAmount: 150000000,
    paymentServiceCode: 'BIFAST_OUTGOING',
    sourceCustomerId: 'sourceCustomerId'
  };

  describe('isEligible check', () => {
    it(`should return true when paymentServiceCode is BIFAST_OUTGOING`, () => {
      // when
      const eligible = bifastStatusTemplate.isEligible(transactionModel);

      // then
      expect(eligible).toBeTruthy();
    });

    it(`should return true when paymentServiceCode is BIFAST_OUTGOING_SHARIA`, () => {
      // when
      const eligible = bifastStatusTemplate.isEligible({
        ...transactionModel,
        paymentServiceCode: 'BIFAST_OUTGOING_SHARIA'
      });

      // then
      expect(eligible).toBeTruthy();
    });

    it(`should return true when paymentServiceCode is RTGS_FOR_BUSINESS`, () => {
      // when
      const eligible = bifastStatusTemplate.isEligible({
        ...transactionModel,
        paymentServiceCode: 'RTGS_FOR_BUSINESS'
      });

      // then
      expect(eligible).toBeFalsy();
    });

    it(`should return false when beneficiaryBankCodeChannel is not ${BankChannelEnum.EXTERNAL}`, () => {
      // when
      const eligible = bifastStatusTemplate.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL
      });

      // then
      expect(eligible).toBeFalsy();
    });

    it('should return false when beneficiaryAccountNo is undefined', async () => {
      // when
      const eligible = bifastStatusTemplate.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
        beneficiaryAccountNo: undefined
      });

      // then
      expect(eligible).toBeFalsy();
    });

    it('should return false when paymentServiceCode is undefined', async () => {
      // when
      const eligible = bifastStatusTemplate.isEligible({
        ...transactionModel,
        paymentServiceCode: undefined
      });

      // then
      expect(eligible).toBeFalsy();
    });
  });

  describe('getTransactionStatus', () => {
    const mockedPaymentStatusRequest: IBifastPaymentStatusRequestPayload = getTransactionStatusRequest();
    const mockPaymentStatusResponse = getTransactionStatusResponse()
      .paymentStatusResponse;

    beforeEach(() => {
      jest.clearAllMocks();
      jest.resetAllMocks();
    });

    beforeEach(() => {
      (populateDefaultBiFastMandatoryFields as jest.Mock).mockImplementation(
        transactionModel => {
          return {
            ...transactionModel,
            additionalInformation1:
              transactionModel.additionalInformation1 ??
              CustomerType.INDIVIDUAL,
            additionalInformation2:
              transactionModel.additionalInformation2 ??
              ResidentialStatus.RESIDENT,
            additionalInformation3:
              transactionModel.additionalInformation3 ?? BeneficiaryProxyType.ACCOUNT_NUMBER,
            additionalInformation4:
              transactionModel.additionalInformation4 ?? transactionModel.beneficiaryAccountNo
          };
        }
      );
      (validateBiFastMandatoryFields as jest.Mock).mockImplementation(
        () => null
      );
      (mapBIFastTransactionStatus as jest.Mock).mockResolvedValue(
        mockedPaymentStatusRequest
      );
    });

    it('should get transaction status from bifast with correct payload', async () => {
      // Given
      (bifastRepository.getPaymentStatus as jest.Mock).mockResolvedValueOnce(
        mockPaymentStatusResponse
      );

      // When
      const result = await bifastStatusTemplate.getTransactionStatus(
        transactionModel
      );

      // Then
      expect(result).toEqual({ status: TransactionStatus.SUCCEED });
    });

    it('should DECLINED when responseCode is 400', async () => {
      // Given
      (bifastRepository.getPaymentStatus as jest.Mock).mockImplementationOnce(
        () => ({
          ...mockPaymentStatusResponse,
          responseCode: '400'
        })
      );

      // When
      const result = await bifastStatusTemplate.getTransactionStatus(
        transactionModel
      );

      // Then
      expect(result).toEqual({ status: TransactionStatus.DECLINED });
    });

    it('should SUBMITTED when responseCode is null', async () => {
      // Given
      (bifastRepository.getPaymentStatus as jest.Mock).mockResolvedValueOnce({
        ...getTransactionStatusResponse().paymentStatusResponse,
        responseCode: null
      });

      // When
      const result = await bifastStatusTemplate.getTransactionStatus(
        transactionModel
      );

      // Then
      expect(result).toEqual({ status: TransactionStatus.SUBMITTED });
    });

    it('should SUBMITTED when responseCode is 408', async () => {
      // Given
      (bifastRepository.getPaymentStatus as jest.Mock).mockResolvedValueOnce({
        ...getTransactionStatusResponse().paymentStatusResponse,
        responseCode: 408
      });

      // When
      const result = await bifastStatusTemplate.getTransactionStatus(
        transactionModel
      );

      // Then
      expect(result).toEqual({ status: TransactionStatus.SUBMITTED });
    });

    it('should SUBMITTED when responseCode is 404', async () => {
      // Given
      (bifastRepository.getPaymentStatus as jest.Mock).mockResolvedValueOnce({
        ...getTransactionStatusResponse().paymentStatusResponse,
        responseCode: 404
      });

      // When
      const result = await bifastStatusTemplate.getTransactionStatus(
        transactionModel
      );

      // Then
      expect(result).toEqual({ status: TransactionStatus.SUBMITTED });
    });

    it('should throw error when validation fails', async () => {
      // Given
      (validateBiFastMandatoryFields as jest.Mock).mockImplementationOnce(
        () => {
          throw new TransferAppError(
            'Test error!',
            TransferFailureReasonActor.UNKNOWN,
            ERROR_CODE.INVALID_MANDATORY_FIELDS
          );
        }
      );

      // When

      const error = await bifastStatusTemplate
        .getTransactionStatus(transactionModel)
        .catch(err => err);

      // Then
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
      expect(bifastRepository.getPaymentStatus).not.toHaveBeenCalled();
    });

    it('should retry first times when BIFAST repository throw network error before giving response', async () => {
      // Given
      const networkError = new NetworkError();
      const expectedNetworkError = {
        ...networkError,
        code: 'ECONNABORTED'
      };
      mockedRetry.mockRestore();
      (bifastRepository.getPaymentStatus as jest.Mock).mockRejectedValueOnce(
        expectedNetworkError
      );
      let error, result;

      // When
      try {
        result = await bifastStatusTemplate.getTransactionStatus(
          transactionModel
        );
      } catch (e) {
        error = e;
      }

      // Then
      expect(result).toBeUndefined();
      expect(bifastRepository.getPaymentStatus).toHaveBeenCalledTimes(1);
      expect(error.code).toEqual('ECONNABORTED');
      expect(error.status).toEqual(501);
    });

    it('should update the transaction to submitted when bifast returned timeout error 408', async () => {
      // Given
      (bifastRepository.getPaymentStatus as jest.Mock).mockRejectedValueOnce({
        message: 'HttpClient Error: Error: Request failed with status code 408',
        code: httpClientTimeoutErrorCode
      });

      // When
      const error = await bifastStatusTemplate
        .getTransactionStatus(transactionModel)
        .catch(err => err);

      // Then
      expect(bifastRepository.getPaymentStatus).toHaveBeenCalledTimes(1);
      expect(error).toBeDefined();
      expect(error).toBeInstanceOf(Object);
      expect(error.message).toBe(
        'HttpClient Error: Error: Request failed with status code 408'
      );
      expect(error.code).toBe(httpClientTimeoutErrorCode);
    });
  });
});
