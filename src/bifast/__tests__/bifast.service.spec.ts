import biFastService from '../bifast.service';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import transactionConfirmationService from '../../transaction/transactionConfirmation.service';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

jest.mock('../../transaction/transactionConfirmation.service');

describe('bifast.service', () => {
  const input = {
    externalId: '132221971879112932032',
    accountNo: '1234567890',
    transactionDate: '210317141902',
    responseCode: 200,
    responseMessage: 'SUCCEED'
  };

  test('should throw error when externalId not found by transaction confirmation', async () => {
    // Given
    (transactionConfirmationService.confirmTransaction as jest.Mock).mockRejectedValueOnce(
      new TransferAppError(
        'Test error!',
        TransferFailureReasonActor.UNKNOWN,
        ERROR_CODE.INVALID_EXTERNALID
      )
    );

    // When
    const error = await biFastService
      .confirmTransaction(input)
      .catch(err => err);

    // Then
    expect(error).toBeDefined();
    expect(error.errorCode).toEqual(ERROR_CODE.INVALID_EXTERNALID);
    expect(
      transactionConfirmationService.confirmTransaction
    ).toHaveBeenCalledWith(input);
  });

  test('should return confirm transaction response', async () => {
    // Given
    (transactionConfirmationService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
      {}
    );

    // When
    const result = await biFastService.confirmTransaction(input);

    // Then
    expect(result).toBeDefined();
    expect(result).toEqual({
      externalId: input.externalId
    });
    expect(
      transactionConfirmationService.confirmTransaction
    ).toHaveBeenCalledWith(input);
  });
});
