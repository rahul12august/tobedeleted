import { ITransactionModel } from '../transaction/transaction.model';
import {
  AccountType,
  CustomerType,
  ResidentialStatus,
  BeneficiaryProxyType
} from './bifast.enum';
import { isUndefined, isEmpty } from 'lodash';
import { BIFAST_PROXY_TYPES } from './bifast.constant';

const checkBeneficiaryAccountName = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.beneficiaryAccountName);

const checkBeneficiaryAccountNo = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.beneficiaryAccountNo);

const checkBeneficiaryBankCode = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.beneficiaryBankCode);

const checkSourceAccountNo = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.sourceAccountNo);

const checkSourceAccountName = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.sourceAccountName);

const checkSourceBankCode = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.sourceAccountName);

const checkAdditionalInformation1 = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.additionalInformation1) &&
  Object.values(CustomerType).includes(
    transaction.additionalInformation1 as CustomerType
  );

const checkAdditionalInformation2 = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.additionalInformation2) &&
  Object.values(ResidentialStatus).includes(
    transaction.additionalInformation2 as ResidentialStatus
  );

const checkSourceNik = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.sourceIdNumber);

const checkBeneficiaryBifastAccountType = (
  transaction: ITransactionModel
): boolean =>
  !isUndefined(transaction.beneficiaryAccountType) &&
  Object.values(AccountType).includes(
    transaction.beneficiaryAccountType as AccountType
  );

const checkProxyTypeAndAlias = (transaction: ITransactionModel): boolean =>
  transaction.additionalInformation3 &&
  BIFAST_PROXY_TYPES.includes(
    transaction.additionalInformation3 as BeneficiaryProxyType
  )
    ? !isEmpty(transaction.additionalInformation4)
    : true;

export const conditions = {
  checkBeneficiaryAccountName,
  checkBeneficiaryAccountNo,
  checkBeneficiaryBankCode,
  checkBeneficiaryBifastAccountType,
  checkSourceAccountNo,
  checkSourceAccountName,
  checkSourceBankCode,
  checkSourceNik,
  checkAdditionalInformation1,
  checkAdditionalInformation2,
  checkProxyTypeAndAlias
};

export default Object.values(conditions);
