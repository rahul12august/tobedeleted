import { config } from '../config';
import { context } from '../context';
import logger from '../logger';
import { createHttpClient, HttpClient } from '@dk/module-httpclient';
import { retryConfig } from '../common/constant';
import https from 'https';

const token = process.env.BIFAST_TOKEN || '';
const baseUrlFromVault = process.env.BIFAST_URL;

logger.debug(`BIFAST token: ${token}`);
logger.debug(`BIFAST URL from vault: ${baseUrlFromVault}`);

const { timeout } = config.get('bifast');

const httpClient: HttpClient = createHttpClient({
  baseURL: baseUrlFromVault,
  headers: {
    Authorization: `Basic ${token}`
  },
  timeout: timeout,
  context,
  logger,
  retryConfig: {
    ...retryConfig,
    networkErrorCodesToRetry: ['ENOENT', 'ECONNREFUSED']
  },
  httpsAgent: new https.Agent({ rejectUnauthorized: false })
});

export default httpClient;
