import {
  ITransactionStatusResponse,
  ITransactionStatusTemplate
} from '../transaction/transaction.type';
import { ITransactionModel } from '../transaction/transaction.model';
import { BankChannelEnum } from '@dk/module-common';
import { isBIFast } from '../transaction/transaction.util';
import logger, { wrapLogs } from '../logger';
import bifastRepository from './bifast.repository';
import {
  mapBIFastTransactionStatus,
  populateDefaultBiFastMandatoryFields,
  validateBiFastMandatoryFields
} from './bifast.helper';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../transaction/transaction.enum';
import { isEmpty } from 'lodash';
import { IBifastPaymentStatusResponsePayload } from './bifast.type';

const getResponseStatus = (result: IBifastPaymentStatusResponsePayload) => {
  switch (result.responseCode) {
    case '200':
      return TransactionStatus.SUCCEED;
    case '400':
      return TransactionStatus.DECLINED;
    default:
      return TransactionStatus.SUBMITTED;
  }
};

const getTransactionStatus = async (
  transactionModel: ITransactionModel
): Promise<ITransactionStatusResponse> => {
  try {
    transactionModel = populateDefaultBiFastMandatoryFields(transactionModel);
    validateBiFastMandatoryFields(transactionModel);

    const input = mapBIFastTransactionStatus(transactionModel);
    const result = await bifastRepository.getPaymentStatus(input);

    if (!result) {
      throw new TransferAppError(
        'Failed get bifast transaction status!',
        TransferFailureReasonActor.BIFAST,
        ERROR_CODE.ERROR_FROM_BIFAST
      );
    }
    const status = getResponseStatus(result);

    return { status };
  } catch (error) {
    logger.error(
      `failed get bi fast transaction status to paymentServiceType: ${
        transactionModel.paymentServiceType
      }, paymentServiceCode: ${
        transactionModel.paymentServiceCode
      } error: ${JSON.stringify(error)}`
    );

    throw error;
  }
};

const isEligible = (model: ITransactionModel): boolean => {
  if (model.paymentServiceCode) {
    return (
      isBIFast(model.paymentServiceCode) &&
      BankChannelEnum.EXTERNAL === model.beneficiaryBankCodeChannel &&
      !isEmpty(model.beneficiaryAccountNo)
    );
  }
  return false;
};

const shouldSendNotification = () => true;

const bifastStatusTemplate: ITransactionStatusTemplate = {
  getTransactionStatus,
  isEligible,
  shouldSendNotification
};

export default wrapLogs(bifastStatusTemplate);
