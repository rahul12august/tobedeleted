import faker from 'faker';
import {
  IAltoCreatePaymentRequestData,
  IAltoCreatePaymentRequestPayload,
  IAltoCreatePaymentResponse,
  ITransactionInputAdditionalPayloadData,
  IQrisCheckStatusResponse,
  IQrisCheckStatusRequestPayload,
  IQrisCheckStatusDataPayload,
  IAltoCheckStatusResponse,
  IQrisCreatePaymentResponse,
  IAltoRefundResponse
} from '../alto.type';
import {
  getTransactionDetail,
  getTransactionModelFullData
} from '../../transaction/__mocks__/transaction.data';
import { ITransactionModel } from '../../transaction/transaction.model';
import { BankChannelEnum } from '@dk/module-common';
import {
  AltoAccountTypeEnum,
  AltoCommandEnum,
  AltoRefundResponseCodeEnum,
  AltoRefundResponseTextEnum,
  AltoResponseCodeEnum,
  QrisTransactionStatusEnum
} from '../alto.enum';
import altoUtils from '../alto.utils';
import { RecommendationService } from '../../transaction/transaction.type';
import { BankCodeTypes } from '../../transaction/transaction.enum';

const newTransactionModel = getTransactionModelFullData();

export const validTransactionModel: ITransactionModel = {
  id: faker.random.uuid(),
  ...newTransactionModel
};

export const inValidTransactionModel: ITransactionModel = {
  ...newTransactionModel,
  id: faker.random.uuid(),
  additionalInformation1: undefined,
  beneficiaryAccountNo: undefined
};

export const getQrisPaymentRequestAttributesData = (): IAltoCreatePaymentRequestData => ({
  /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
  date_time: '2022-02-22T08:23:08.168Z',
  customer_reference_number: 'PAY20220223001',
  authorization_id: altoUtils.generateAuthorizationId(),
  currency_code: 'IDR',
  amount: 100500,
  fee: 500,
  issuer_nns: '93600919',
  national_mid: 'IN1019000000024',
  additional_data: 'ABCDEFGHIJ',
  terminal_label: 'K19',
  merchant: {
    pan: '936011010099879098',
    id: '999540008',
    criteria: 'UME',
    name: 'Somay Mangga Dua',
    city: 'Jakarta',
    mcc: '6600',
    postal_code: '10110',
    country_code: 'ID'
  },
  customer: {
    pan: '9360002319993788396',
    name: 'Sebastian',
    account_type: AltoAccountTypeEnum.SAVING
  }
});

export const getQrisPaymentRequestAttributes = (): IAltoCreatePaymentRequestPayload => ({
  command: 'qr-payment-credit',
  data: getQrisPaymentRequestAttributesData()
});

export const transactionModel: ITransactionModel = {
  ...getTransactionDetail(),
  beneficiaryBankCodeChannel: BankChannelEnum.QRIS,
  beneficiaryAccountNo: '2121212',
  beneficiaryAccountName: 'beneficiary_name',
  beneficiaryBankCode: 'beneficiary_bank',
  beneficiaryIrisCode: 'beneficiary_bank',
  note: 'notes',
  transactionAmount: 12500
};

export const mockQrisPaymentInquiryResponse = (): IAltoCreatePaymentResponse => ({
  command: 'qr-payment-credit',
  response_code: '001',
  response_text: 'Success',
  data: {
    customer_reference_number: 'PAY20220223001',
    forwarding_customer_reference_number: '077879547900',
    invoice_no: '007',
    currency_code: 'IDR',
    amount: 100500,
    fee: 500
  }
});

export const mockSuccessQrisPaymentResponse = (): IQrisCreatePaymentResponse => ({
  customerReferenceNumber: 'PAY20220223001',
  forwardingCustomerReferenceNumber: '077879547900',
  invoiceNo: '007',
  currencyCode: 'IDR',
  amount: 100500,
  fee: 500,
  transactionStatus: QrisTransactionStatusEnum.SUCCESS
});

export const mockErrorQrisPaymentResponse = (): IQrisCreatePaymentResponse => ({
  customerReferenceNumber: 'PAY20220223001',
  forwardingCustomerReferenceNumber: '077879547900',
  invoiceNo: '007',
  currencyCode: 'IDR',
  amount: 100500,
  fee: 500,
  transactionStatus: QrisTransactionStatusEnum.SHOULD_CHECK_STATUS
});

export const qrisCreatePaymentResponseExpectation = (): IQrisCreatePaymentResponse => ({
  customerReferenceNumber: 'PAY20220223001',
  forwardingCustomerReferenceNumber: '077879547900',
  invoiceNo: '007',
  currencyCode: 'IDR',
  amount: 100500,
  fee: 500,
  transactionStatus: QrisTransactionStatusEnum.SUCCESS
});

export const getTransactionAdditionalPayloadQrisPayment = (): ITransactionInputAdditionalPayloadData => ({
  /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
  dateTime: '2022-02-22T08:23:08.168Z',
  currencyCode: 'IDR',
  amount: 100500,
  fee: 500,
  issuerNns: '93600919',
  nationalMid: 'IN1019000000024',
  additionalData: 'ABCDEFGHIJ',
  terminalLabel: 'K19',
  merchant: {
    pan: '936011010099879098',
    id: '999540008',
    criteria: 'UME',
    name: 'Somay Mangga Dua',
    city: 'Jakarta',
    mcc: '6600',
    postalCode: '10110',
    countryCode: 'ID'
  },
  customer: {
    pan: '9360002319993788396',
    name: 'Sebastian',
    accountType: AltoAccountTypeEnum.SAVING
  }
});

export const getNotValidTransactionAdditionalPayloadQrisPayment = () => ({
  /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
  dateTime: '2022-02-22T08:23:08.168Z',
  customerReferenceNumber: undefined,
  authorizationId: undefined,
  currencyCode: undefined,
  amount: undefined,
  fee: undefined,
  issuerNns: undefined,
  acquirerNns: undefined,
  nationalMid: undefined,
  additionalData: undefined,
  terminalLabel: undefined,
  forwardingCustomerReferenceNumber: undefined,
  merchant: {
    pan: undefined,
    id: undefined,
    criteria: undefined,
    name: undefined,
    city: undefined,
    mcc: undefined,
    postalCode: undefined,
    countryCode: undefined
  },
  customer: {
    pan: undefined,
    name: undefined,
    accountType: undefined
  }
});

export const mockQrisCheckStatusDataRequest = (retryAttempt: number = 1) => {
  return {
    customerId: '0435808014',
    paymentInstructionId: '6255003eaf1a288846cc5b7a',
    retryAttempt: retryAttempt
  };
};

export const mockAltoCheckStatusResponse = (): IAltoCheckStatusResponse => ({
  command: 'qr-check-status',
  response_code: '001',
  response_text: 'Success',
  data: {
    transaction_response_code: '001',
    transaction_status: 'Success',
    customer_reference_number: '0013284239ZFX21',
    forwarding_customer_reference_number: '42234093892',
    invoice_no: '73283923681238390213'
  }
});

export const mockAltoCheckStatusNotFoundResponse = (): IAltoCheckStatusResponse => ({
  command: 'qr-check-status',
  response_code: '096',
  response_text: 'Requested data not found',
  data: {
    customer_reference_number: '0013284239ZFX21'
  }
});

export const mockAltoCheckStatusRejected = (): IAltoCheckStatusResponse => ({
  command: 'qr-check-status',
  response_code: 'A00',
  response_text: 'Rejected',
  data: {
    customer_reference_number: '0013284239ZFX21'
  }
});

export const mockAltoCheckStatusSuspend = (): IAltoCheckStatusResponse => ({
  command: 'qr-check-status',
  response_code: '001',
  response_text: 'Success',
  data: {
    transaction_response_code: '002',
    transaction_status: 'Suspect',
    customer_reference_number: 'ad5ade04-db27-42bc-9a6f-85096b924ce3',
    forwarding_customer_reference_number: '914932025574',
    invoice_no: '00000000000000000000'
  }
});

export const mockQrisCheckStatusResponse = (): IQrisCheckStatusResponse => {
  return {
    transactionResponseCode: AltoResponseCodeEnum.SUCCESS,
    transactionStatus: QrisTransactionStatusEnum.SUCCESS,
    customerReferenceNumber: '0013284239ZFX21',
    forwardingCustomerReferenceNumber: '42234093892',
    invoiceNo: '73283923681238390213'
  };
};

export const mockQrisCheckStatusSuspectResponse = (): IQrisCheckStatusResponse => {
  return {
    transactionResponseCode: AltoResponseCodeEnum.SUSPECT,
    transactionStatus: QrisTransactionStatusEnum.SUSPECT,
    customerReferenceNumber: '0013284239ZFX21',
    forwardingCustomerReferenceNumber: '42234093892',
    invoiceNo: '73283923681238390213'
  };
};

export const mockCheckStatusResponseRejected = (): IQrisCheckStatusResponse => {
  return {
    customerReferenceNumber: 'QRIS625d179c15622f5ca75931ac',
    forwardingCustomerReferenceNumber: '976418600378',
    invoiceNo: '00000000990000000099',
    transactionResponseCode: AltoResponseCodeEnum.REJECTED,
    transactionStatus: QrisTransactionStatusEnum.FAILED
  };
};

export const mockCheckStatusResponseApproved = (): IQrisCheckStatusResponse => {
  return {
    customerReferenceNumber: 'QRIS625d179c15622f5ca75931ac',
    forwardingCustomerReferenceNumber: '976418600378',
    invoiceNo: '00000000990000000099',
    transactionResponseCode: AltoResponseCodeEnum.SUCCESS,
    transactionStatus: QrisTransactionStatusEnum.SUCCESS
  };
};

export const mockCheckStatusResponseUnknown = (): IQrisCheckStatusResponse => {
  return {
    customerReferenceNumber: 'QRIS625d179c15622f5ca75931ac',
    forwardingCustomerReferenceNumber: '976418600378',
    invoiceNo: '00000000990000000099',
    transactionResponseCode: AltoResponseCodeEnum.UNKNOWN,
    transactionStatus: QrisTransactionStatusEnum.FAILED
  };
};

export const mockCheckStatusResponseSuspended = (): IQrisCheckStatusResponse => {
  return {
    customerReferenceNumber: 'QRIS625d179c15622f5ca75931ac',
    forwardingCustomerReferenceNumber: '976418600378',
    invoiceNo: '00000000990000000099',
    transactionResponseCode: AltoResponseCodeEnum.SUSPECT,
    transactionStatus: QrisTransactionStatusEnum.SUSPEND
  };
};

export const mockRefundResponse = (): IAltoRefundResponse => {
  return {
    command: AltoCommandEnum.QRIS_REFUND,
    response_code: AltoRefundResponseCodeEnum.SUCCESS,
    response_text: AltoRefundResponseTextEnum.SUCCESS,
    data: {
      authorization_id: '1234567',
      customer_reference_number: 'QRIS625d179c15622f5ca75931ac',
      invoice_no_refund: 'generatedNumber',
      reference_number: '6418600378'
    }
  };
};

export const getQrisCheckPaymentAttributesData = (): IQrisCheckStatusDataPayload => ({
  datetime: '2022-02-22T08:23:08.168Z',
  customerReferenceNumber: 'PAY20220223001'
});

export const getQrisCheckStatusRequestAttributes = (): IQrisCheckStatusRequestPayload => ({
  command: 'qr-check-status',
  data: getQrisCheckPaymentAttributesData()
});

export const getQrisPayoutsRecommendedService = (): RecommendationService => {
  const recommendationService: RecommendationService = {
    paymentServiceCode: 'QRIS_PAYOUTS',
    beneficiaryBankCodeType: BankCodeTypes.RTOL,
    transactionAuthenticationChecking: false,
    debitTransactionCode: [
      {
        transactionCode: 'QRD001',
        transactionCodeInfo: {
          code: 'QRD001',
          channel: 'QRIS',
          defaultCategoryCode: 'QRIS',
          minAmountPerTx: 1,
          maxAmountPerTx: 9999999,
          limitGroupCode: 'QRIS'
        }
      }
    ],
    blocking: false,
    requireThirdPartyOutgoingId: true
  };
  return recommendationService;
};

export const getQrisWithdrawMambuResponse = () => ({
  id: 'mbu_001',
  encodedKey: '123',
  type: 'type',
  amount: 10000
});
