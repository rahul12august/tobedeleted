/* eslint-disable @typescript-eslint/camelcase */
import hapi from '@hapi/hapi';
import { Http, NO_WRAPPING } from '@dk/module-common';
import { IAltoRefundRequest, IQrisCheckStatusRequest } from './alto.type';
import {
  altoRefundRequestValidator,
  altoRefundResponseValidator,
  qrisCheckStatusRequestValidator,
  qrisCheckStatusResponseValidator
} from './alto.validator';
import { updateRequestIdInContext } from '../common/contextHandler';
import altoService from './alto.service';
import logger from '../logger';
import altoUtil from './alto.utils';
import { RESPONSE_DELAY } from './alto.constant';

const checkStatus: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/qris/status',
  options: {
    description: 'Checking and updating status for QRIS payment',
    notes:
      'Public Api endpoint called from Jago app to check with Alto and update QRIS payment status',
    tags: ['api', 'transactions', 'QRIS', 'payment'],
    validate: {
      payload: qrisCheckStatusRequestValidator
    },
    response: {
      schema: qrisCheckStatusResponseValidator
    },
    handler: async (
      hapiRequest: IQrisCheckStatusRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { payload } = hapiRequest;
      await updateRequestIdInContext(payload.paymentInstructionId);
      logger.info(`Payload: ${JSON.stringify(payload)}`);
      const statusResponse = await altoService.checkStatus(
        payload.customerId,
        payload.paymentInstructionId
      );
      return hapiResponse.response(statusResponse).code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Ok'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          },
          [Http.StatusCode.UNAUTHORIZED]: {
            description: 'UNAUTHORIZED'
          }
        }
      }
    }
  }
};

const refundTransaction: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/alto/refund',
  options: {
    description: 'Refund Transaction for QRIS payment',
    notes: 'Webhook API for ALTO to refund QRIS Transaction',
    tags: ['api', 'transactions', 'QRIS', 'refund'],
    auth: false,
    validate: {
      payload: altoRefundRequestValidator
    },
    response: {
      schema: altoRefundResponseValidator
    },
    handler: async (
      hapiRequest: IAltoRefundRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { payload } = hapiRequest;
      await updateRequestIdInContext(payload.data.customer_reference_number);
      logger.info(`refundTransaction: Payload: ${JSON.stringify(payload)}`);

      const processRefund = await altoService.refundTransaction(payload.data);

      logger.info(
        `refundTransaction (delayed ${RESPONSE_DELAY}ms) : Response: ${JSON.stringify(
          processRefund
        )}`
      );

      await altoUtil.sleep(RESPONSE_DELAY);

      return hapiResponse
        .response(processRefund)
        .code(Http.StatusCode.OK)
        .header(NO_WRAPPING, 'true');
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Ok'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const altoController: hapi.ServerRoute[] = [checkStatus, refundTransaction];

export default altoController;
