import logger from '../logger';
import { qrisEncryption } from '@dk/module-common';

const apiKey = process.env.ALTO_API_KEY || '';
const validationKey = process.env.ALTO_VALIDATION_KEY || '';

export const generateSignature = async (
  httpMethod: string,
  relativeURL: string,
  stringifiedPayload: string,
  timestamp: string
): Promise<string> => {
  logger.info(`generateSignature`);

  const payload = stringifiedPayload;
  const hashedPayload = qrisEncryption.bodyEncryption(payload).toLowerCase();
  logger.debug(`generateSignature: hashedPayload: ${hashedPayload}`);
  const stringToSign = `${httpMethod.toUpperCase()}:${relativeURL}:${apiKey}:${hashedPayload}:${timestamp}`;
  const signature = qrisEncryption.signatureEncryption(
    stringToSign,
    validationKey
  );

  logger.debug(`generateSignature: ${signature}`);
  return signature;
};
