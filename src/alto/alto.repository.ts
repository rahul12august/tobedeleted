/* eslint-disable @typescript-eslint/camelcase */
import moment from 'moment';
import { config } from '../config';
import httpClient, { tokenHTTPClient } from './httpClient';
import {
  IAltoCheckStatusRequestPayload,
  IAltoCreatePaymentRequestData,
  IAltoRequestPayload,
  IAltoTokenResult,
  IQrisCheckStatusResponse,
  IQrisCreatePaymentResponse,
  QrisError
} from './alto.type';
import logger, { wrapLogs } from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import {
  AltoHttpHeaders,
  EMPTY,
  PAYMENT_ENDPOINT_METHOD,
  REDIS_TOKEN_KEY,
  STATUS_ENDPOINT_METHOD,
  TIMESTAMP_FORMAT,
  TokenConstants
} from './alto.constant';
import { QrisTransactionStatusEnum } from './alto.enum';
import { isEmpty } from 'lodash';
import redis from '../common/redis';
import { generateSignature } from './alto.encryption';
import altoUtil from './alto.utils';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const { transactionPath, tokenPath, statusPath } = config.get('qrisAlto');

const configurationPrefixKey = 'ms-bill-payment';

const getTokenPayload = 'grant_type=client_credentials';

const apiKey = process.env.ALTO_API_KEY || '';

const getTokenFromCache = async (): Promise<IAltoTokenResult | undefined> => {
  try {
    let result: IAltoTokenResult = await redis.get(
      REDIS_TOKEN_KEY,
      configurationPrefixKey
    );
    return result;
  } catch (error) {
    logger.info('Token is missing in cache.');
    return;
  }
};

const setTokenToCache = async (result: IAltoTokenResult): Promise<void> => {
  try {
    let ttl = result.expires_in - TokenConstants.TOKEN_EXPIRY_BUFFER;
    if (Number.isNaN(ttl) || ttl < 0) {
      ttl = TokenConstants.DEFAULT_TTL;
    }

    await redis.mSet({
      prefix: configurationPrefixKey,
      keyValueObjectList: [
        {
          key: REDIS_TOKEN_KEY,
          value: result,
          ttl: ttl,
          shouldStringifyValue: true
        }
      ]
    });
  } catch (error) {
    logger.warn(`Failed to update token in cache. ${error}`);
    // not throwing error, as process can still continue if token is missing from cache
  }
};

const requestTokenFromAlto = async (): Promise<IAltoTokenResult> => {
  try {
    const response = await tokenHTTPClient.post(tokenPath, getTokenPayload);
    return response.data;
  } catch (error) {
    logger.error(`Error fetching access token from Alto. ${error}`);
    throw new TransferAppError(
      'Error fetching access token from Alto.',
      TransferFailureReasonActor.ALTO,
      ERROR_CODE.ALTO_AUTH_TOKEN_ACCESS_ERROR
    );
  }
};

const getToken = async (): Promise<string> => {
  let result = await getTokenFromCache();
  if (result && !isEmpty(result)) {
    return result.access_token;
  }

  result = await requestTokenFromAlto();
  if (!result.access_token || isEmpty(result.access_token)) {
    logger.error(`Cannot get token from Alto. ${JSON.stringify(result)}`);
    throw new TransferAppError(
      'Cannot get token from Alto.',
      TransferFailureReasonActor.ALTO,
      ERROR_CODE.ALTO_AUTH_TOKEN_ACCESS_ERROR
    );
  }

  await setTokenToCache(result);
  return result.access_token;
};

const sendRequestToAlto = async (
  targetUrl: string,
  accessToken: string,
  signature: string,
  payload: IAltoRequestPayload
): Promise<any> => {
  const requestHeaders = {
    Authorization: `Bearer ${accessToken}`,
    [AltoHttpHeaders.X_ALTO_KEY]: apiKey,
    [AltoHttpHeaders.X_ALTO_SIGNATURE]: signature,
    [AltoHttpHeaders.X_ALTO_TIMESTAMP]: payload.data.date_time
  };

  logger.info(
    `Sending request to Alto ${targetUrl}.
      Request Headers: ${JSON.stringify(requestHeaders)},
      Request Payload: ${JSON.stringify(payload)}`
  );

  try {
    const result = await httpClient.post(targetUrl, payload, {
      headers: requestHeaders
    });
    logger.info(
      `Receiving response from Alto ${targetUrl}:
        Response Code: ${result.data.response_code},
        Response Type: ${result.data.response_text},
        Response Data: ${JSON.stringify(result.data.data)}`
    );

    return result.data;
  } catch (error) {
    logger.error(`Error while sending request to Alto. ${error}`);
    throw altoUtil.getAltoHttpClientError(error);
  }
};

const submitTransaction = async (
  payload: IAltoCreatePaymentRequestData
): Promise<IQrisCreatePaymentResponse> => {
  logger.info(
    `Submitting QRIS transaction.
      Issuer NNS : ${payload.issuer_nns}, 
      National MID: ${payload.national_mid},
      Customer Reference Number: ${payload.customer_reference_number}`
  );

  const updatedPayload = altoUtil.generatePaymentPayload(payload);
  const accessToken = await getToken();
  const signature = await generateSignature(
    PAYMENT_ENDPOINT_METHOD,
    transactionPath,
    JSON.stringify(updatedPayload),
    payload.date_time
  );
  const result = await sendRequestToAlto(
    transactionPath,
    accessToken,
    signature,
    updatedPayload
  );

  if (!altoUtil.isValidResponse(result)) {
    logger.info(`QRIS transaction failed with code ${result.response_code}`);
    throw altoUtil.getQrisResponseError(result);
  }
  return altoUtil.buildCreatePaymentResponse(result);
};

const checkPaymentStatus = async (
  customerReferenceNumber: string
): Promise<IQrisCheckStatusResponse> => {
  logger.info(
    `Status inquiry for Customer Reference Number: ${customerReferenceNumber}`
  );

  const dynamicTimestamp = moment().format(TIMESTAMP_FORMAT);
  const accessToken = await getToken();
  const inqPayload: IAltoCheckStatusRequestPayload = altoUtil.generateStatusInquiryPayload(
    dynamicTimestamp,
    customerReferenceNumber
  );
  const signature = await generateSignature(
    STATUS_ENDPOINT_METHOD,
    statusPath,
    JSON.stringify(inqPayload),
    dynamicTimestamp
  );

  try {
    const result = await sendRequestToAlto(
      statusPath,
      accessToken,
      signature,
      inqPayload
    );
    return altoUtil.buildCheckStatusResponse(result);
  } catch (error) {
    if (altoUtil.isQrisTimeoutError(error)) {
      const qrisError = error as QrisError;
      return {
        customerReferenceNumber: customerReferenceNumber,
        transactionStatus: QrisTransactionStatusEnum.SUSPEND,
        invoiceNo: EMPTY,
        forwardingCustomerReferenceNumber:
          qrisError.forwardingCustomerReferenceNumber || EMPTY
      };
    }
    throw error;
  }
};

const altoRepository = wrapLogs({
  getToken,
  submitTransaction,
  checkPaymentStatus
});

export default altoRepository;
