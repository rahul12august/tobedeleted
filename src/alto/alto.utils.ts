import {
  ALTO_REFERENCE_NUMBER_LENGTH,
  CURRENCY_CODE,
  CUSTOMER_PAN_MAX_LENGTH,
  EMPTY,
  ISSUER_NNS,
  JAKARTA_TIMEZONE,
  MAX_LENGTH_CUSTOMER_NAME,
  VALID_RESPONSES
} from './alto.constant';
import logger from '../logger';
import {
  AltoCommandEnum,
  AltoResponseCodeEnum,
  QrisTransactionStatusEnum
} from './alto.enum';
import { PaymentServiceTypeEnum, qrisEncryption } from '@dk/module-common';
import { ERROR_CODE } from '../common/errors';
import {
  IAltoCheckStatusRequestPayload,
  IAltoCreatePaymentRequestData,
  IAltoCreatePaymentRequestPayload,
  IAltoRefundDataPayload,
  IQrisCheckStatusResponse,
  IQrisCreatePaymentResponse,
  IQrisResponse,
  QrisError
} from './alto.type';
import { isEmpty } from 'lodash';
import { ITransactionModel } from '../transaction/transaction.model';
import moment from 'moment';
import {
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from '../transaction/transaction.enum';
import transactionHelper from '../transaction/transaction.helper';
import { TransactionHistoryStatusEnum } from '../transactionHistory/transactionHistory.enum';
import { NotificationCode } from '../transaction/transaction.producer.enum';
import { toJKTDate } from '../common/dateUtils';
import { FORMAT_DATETIME_EMAIL_DETAIL } from '../transaction/transaction.constant';
import { toIndonesianRupiah } from '../common/currencyFormatter';
import { HttpError } from '@dk/module-httpclient';

export const shouldRetryQrisTransaction = (error: QrisError) => {
  return error.errorCode === ERROR_CODE.ALTO_SERVER_ERROR;
};

const maskName = (fullName: string): string => {
  let nameArray = fullName
    .toUpperCase()
    .substring(0, MAX_LENGTH_CUSTOMER_NAME)
    .trim()
    .split(' ');
  return nameArray
    .map(name => name[0] + name.slice(1).replace(/./g, 'X'))
    .join(' ');
};

const generatePaymentPayload = (
  payload: IAltoCreatePaymentRequestData
): IAltoCreatePaymentRequestPayload => {
  const updatedPayload: IAltoCreatePaymentRequestPayload = {
    command: AltoCommandEnum.PAYMENT_CREDIT,
    data: {
      ...payload
    }
  };
  updatedPayload.data.customer.name = maskName(payload.customer.name);

  return updatedPayload;
};

const generateStatusInquiryPayload = (
  timestamp: string,
  customerReferenceNumber: string
): IAltoCheckStatusRequestPayload => {
  /* eslint-disable @typescript-eslint/camelcase */
  return {
    command: AltoCommandEnum.CHECK_STATUS,
    data: {
      date_time: timestamp,
      customer_reference_number: customerReferenceNumber
    }
  };
};

const generateAuthorizationId = (): string => {
  const stringifiedDate = Date.now().toString(36);
  const stringifiedRandomMath = Math.random()
    .toString(36)
    .slice(2);

  return (
    stringifiedDate.slice(stringifiedDate.length - 3).toUpperCase() +
    stringifiedRandomMath.slice(stringifiedRandomMath.length - 3).toUpperCase()
  );
};

const generateCustomerPan = (pan: string): string => {
  const concatPan = `${ISSUER_NNS}${pan}`;
  const getMaxPan =
    concatPan.length >= CUSTOMER_PAN_MAX_LENGTH
      ? concatPan.slice(0, CUSTOMER_PAN_MAX_LENGTH - 1)
      : concatPan;

  return `${getMaxPan}${qrisEncryption.getCheckDigit(getMaxPan)}`;
};

const isValidResponse = (result: any): boolean =>
  result &&
  result.response_code &&
  VALID_RESPONSES.includes(result.response_code);

const getExceptionErrorCode = (error: any): ERROR_CODE => {
  if (error.errorCode) {
    return error.errorCode;
  } else if (error.toString().includes('Error: timeout')) {
    return ERROR_CODE.QRIS_TRANSACTION_TIMEOUT;
  } else {
    return ERROR_CODE.ERROR_FROM_ALTO;
  }
};

const getQrisResponseError = (result: any): QrisError => {
  const responseCode = result.response_code;
  const detail = `Request failed with response status code: ${responseCode}!`;
  if (
    responseCode === AltoResponseCodeEnum.SUSPECT ||
    responseCode === AltoResponseCodeEnum.IDENTICAL_TRANSACTION_STILL_ON_PROCESS
  ) {
    return new QrisError(
      detail,
      TransferFailureReasonActor.QRIS,
      ERROR_CODE.QRIS_TRANSACTION_TIMEOUT,
      QrisTransactionStatusEnum.SUSPECT,
      result.data.forwarding_customer_reference_number,
      responseCode
    );
  } else {
    return new QrisError(
      detail,
      TransferFailureReasonActor.ALTO,
      ERROR_CODE.ERROR_FROM_ALTO,
      QrisTransactionStatusEnum.FAILED,
      result.data.forwarding_customer_reference_number,
      responseCode
    );
  }
};

const getAltoHttpClientError = (error: any): QrisError => {
  if (error.toString().includes('Error: timeout')) {
    return new QrisError(
      'Transaction request timeout error!',
      TransferFailureReasonActor.QRIS,
      ERROR_CODE.QRIS_TRANSACTION_TIMEOUT,
      QrisTransactionStatusEnum.SUSPECT,
      EMPTY
    );
  } else if (error instanceof HttpError && error.status >= 500) {
    return new QrisError(
      'Alto server encountered an unexpected condition!',
      TransferFailureReasonActor.ALTO,
      ERROR_CODE.ALTO_SERVER_ERROR,
      QrisTransactionStatusEnum.FAILED,
      EMPTY
    );
  } else {
    return new QrisError(
      'Unknown issue with alto!',
      TransferFailureReasonActor.ALTO,
      ERROR_CODE.ERROR_FROM_ALTO,
      QrisTransactionStatusEnum.FAILED,
      EMPTY
    );
  }
};

const isQrisTimeoutError = (error: any): boolean =>
  !isEmpty(error) &&
  error instanceof QrisError &&
  error.errorCode === ERROR_CODE.QRIS_TRANSACTION_TIMEOUT;

const getQrisPaymentStatusOnCreatePayment = (altoResponseCode: string) => {
  logger.info(
    `getQrisPaymentStatusOnCreatePayment: getting QRIS transaction status by altoResponseCode: ${altoResponseCode}`
  );
  if (altoResponseCode === AltoResponseCodeEnum.SUCCESS) {
    return QrisTransactionStatusEnum.SUCCESS;
  } else if (
    altoResponseCode === AltoResponseCodeEnum.SUSPECT ||
    altoResponseCode ===
      AltoResponseCodeEnum.IDENTICAL_TRANSACTION_STILL_ON_PROCESS
  ) {
    return QrisTransactionStatusEnum.SUSPEND;
  } else if (
    altoResponseCode ===
      AltoResponseCodeEnum.IDENTICAL_TRANSACTION_HAS_BEEN_SUCCESS ||
    altoResponseCode ===
      AltoResponseCodeEnum.IDENTICAL_TRANSACTION_HAS_BEEN_FAILED
  ) {
    return QrisTransactionStatusEnum.SHOULD_CHECK_STATUS;
  } else {
    return QrisTransactionStatusEnum.FAILED;
  }
};

const buildCreatePaymentResponse = (
  result: any
): IQrisCreatePaymentResponse => {
  const data = result.data;
  return {
    amount: data.amount,
    currencyCode: data.currency_code,
    customerReferenceNumber: data.customer_reference_number,
    fee: data.fee,
    forwardingCustomerReferenceNumber:
      data.forwarding_customer_reference_number,
    invoiceNo: data.invoice_no,
    transactionStatus: getQrisPaymentStatusOnCreatePayment(result.response_code)
  };
};

const getQrisPaymentStatusOnCheckStatus = (altoResponseCode: string) => {
  logger.info(
    `getQrisPaymentStatusOnCheckStatus: getting QRIS transaction status by altoResponseCode: ${altoResponseCode}`
  );
  if (altoResponseCode === AltoResponseCodeEnum.SUCCESS) {
    return QrisTransactionStatusEnum.SUCCESS;
  } else if (altoResponseCode === AltoResponseCodeEnum.REJECTED) {
    return QrisTransactionStatusEnum.FAILED;
  } else {
    return QrisTransactionStatusEnum.SUSPEND;
  }
};

const buildCheckStatusResponse = (
  altoResponse: any
): IQrisCheckStatusResponse => {
  let transactionStatus: QrisTransactionStatusEnum;
  let responseCode: AltoResponseCodeEnum;
  let acquirerResponse = altoResponse.data;
  if (altoResponse.response_code === AltoResponseCodeEnum.SUCCESS) {
    // Success from Alto
    // Need to check the response from Acquirer

    responseCode = acquirerResponse.transaction_response_code;
    transactionStatus = getQrisPaymentStatusOnCheckStatus(responseCode);
  } else if (altoResponse.response_code === AltoResponseCodeEnum.REJECTED) {
    // Rejected from Alto

    responseCode = altoResponse.response_code;
    transactionStatus = QrisTransactionStatusEnum.FAILED;
  } else {
    // Suspect from Alto or another failed with specific code
    // This will make the transaction refundable from Admin Portal

    responseCode = altoResponse.response_code;
    transactionStatus = QrisTransactionStatusEnum.SUSPEND;
  }

  return {
    transactionStatus: transactionStatus,
    transactionResponseCode: responseCode,
    customerReferenceNumber: acquirerResponse.customer_reference_number,
    forwardingCustomerReferenceNumber:
      acquirerResponse.forwarding_customer_reference_number,
    invoiceNo: acquirerResponse.invoice_no
  };
};

const isSuccessQrisPayment = (qrisTransactionStatus: string): boolean =>
  qrisTransactionStatus === QrisTransactionStatusEnum.SUCCESS;

const isFailedQrisPayment = (qrisTransactionStatus: string): boolean =>
  qrisTransactionStatus === QrisTransactionStatusEnum.FAILED;

const shouldAutoRefundQrisPayment = (
  response: IQrisCheckStatusResponse
): boolean =>
  isFailedQrisPayment(response.transactionStatus) &&
  response.transactionResponseCode === AltoResponseCodeEnum.REJECTED;

const shouldUpdateTransaction = (
  response: IQrisCheckStatusResponse
): boolean => {
  return (
    !isEmpty(response.transactionStatus) &&
    (isFailedQrisPayment(response.transactionStatus) ||
      isSuccessQrisPayment(response.transactionStatus))
  );
};

const buildRefundRequestData = (
  checkStatusResponse: IQrisCheckStatusResponse,
  originalTransaction: ITransactionModel
): IAltoRefundDataPayload => {
  return {
    customer_reference_number: checkStatusResponse.customerReferenceNumber,
    invoice_no: checkStatusResponse.invoiceNo,
    reference_number: originalTransaction.thirdPartyOutgoingId || '',
    date_time: moment()
      .utc()
      .toISOString(),
    currency_code: CURRENCY_CODE,
    amount_refund: originalTransaction.transactionAmount
  };
};

const getAltoReferenceNumber = (forwardingCRN: string): string =>
  forwardingCRN.slice(forwardingCRN.length - ALTO_REFERENCE_NUMBER_LENGTH);

const setQrisResponseToTransactionData = (
  transactionModel: ITransactionModel,
  forwardingCRN: string
) => {
  transactionModel.additionalInformation4 = forwardingCRN;
  if (forwardingCRN.length > ALTO_REFERENCE_NUMBER_LENGTH) {
    transactionModel.thirdPartyOutgoingId = getAltoReferenceNumber(
      forwardingCRN
    );
  } else {
    transactionModel.thirdPartyOutgoingId = forwardingCRN;
  }
};

const updateTransactionDataOnQrisPaymentSuccess = (
  transactionModel: ITransactionModel,
  qrisResponse: IQrisResponse
): void => {
  transactionModel.coreBankingTransactions?.push({
    id: qrisResponse.invoiceNo,
    type: TransferJourneyStatusEnum.AMOUNT_CREDITED
  });

  if (transactionModel.coreBankingTransactionIds && qrisResponse.invoiceNo) {
    transactionModel.coreBankingTransactionIds.push(qrisResponse.invoiceNo);
  }

  transactionModel.journey = transactionHelper.getStatusesToBeUpdated(
    TransferJourneyStatusEnum.AMOUNT_CREDITED,
    transactionModel
  );

  setQrisResponseToTransactionData(
    transactionModel,
    qrisResponse.forwardingCustomerReferenceNumber
  );
};

const isAmountCredited = (
  transactionModel: ITransactionModel
): boolean | undefined =>
  !isEmpty(transactionModel.journey) &&
  transactionModel.journey?.some(
    journeyItem =>
      journeyItem.status === TransferJourneyStatusEnum.AMOUNT_CREDITED
  );

const generateBeneficiaryAccountNumberWithLundCheckDigit = (
  merchantPan: string
): string => {
  const checkDigit = qrisEncryption.getCheckDigit(merchantPan);
  return `${merchantPan}${checkDigit}`;
};

const sleep = (timeout: number) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};

const isQrisTransaction = (transaction: ITransactionModel): boolean =>
  transaction.paymentServiceType === PaymentServiceTypeEnum.QRIS;

const getTransactionHistoryStatus = (
  qrisResponseStatus: QrisTransactionStatusEnum
): TransactionHistoryStatusEnum => {
  let transactionHistoryStatus: TransactionHistoryStatusEnum;

  if (qrisResponseStatus == QrisTransactionStatusEnum.SUCCESS) {
    transactionHistoryStatus = TransactionHistoryStatusEnum.SUCCEED;
  } else if (qrisResponseStatus == QrisTransactionStatusEnum.FAILED) {
    transactionHistoryStatus = TransactionHistoryStatusEnum.FAILED;
  } else {
    transactionHistoryStatus = TransactionHistoryStatusEnum.PENDING;
  }

  return transactionHistoryStatus;
};

const getQrisNotificationCode = (
  qrisResponseStatus: QrisTransactionStatusEnum
): NotificationCode => {
  let notificationCode;

  if (qrisResponseStatus == QrisTransactionStatusEnum.SUCCESS) {
    notificationCode = NotificationCode.NOTIF_SUCCESS_QRIS;
  } else if (qrisResponseStatus == QrisTransactionStatusEnum.FAILED) {
    notificationCode = NotificationCode.NOTIF_FAILED_QRIS;
  } else {
    notificationCode = NotificationCode.NOTIF_PENDING_QRIS;
  }

  return notificationCode;
};

const buildQrisNotificationMessage = (transaction: ITransactionModel) => {
  const jakartaDate =
    toJKTDate(
      transaction.createdAt || transaction.updatedAt,
      FORMAT_DATETIME_EMAIL_DETAIL
    ) +
    ' ' +
    JAKARTA_TIMEZONE;

  return {
    customerName: transaction.sourceAccountName,
    sourceAccountType: transaction.sourceAccountType,
    sourceAccountNo: transaction.sourceAccountNo,
    beneficiaryBankName: transaction.beneficiaryBankName,
    beneficiaryAccountNo: transaction.beneficiaryAccountNo,
    beneficiaryName: transaction.beneficiaryAccountName,
    amount: toIndonesianRupiah(transaction.transactionAmount),
    transactionDate: jakartaDate
  };
};

const getQrisTransactionStatus = (transactionStatus: TransactionStatus) =>
  transactionStatus === TransactionStatus.SUCCEED
    ? QrisTransactionStatusEnum.SUCCESS
    : QrisTransactionStatusEnum.FAILED;

const isTransactionSettled = (transactionStatus: TransactionStatus) =>
  transactionStatus !== TransactionStatus.SUBMITTED &&
  transactionStatus !== TransactionStatus.SUBMITTING;

const updateMambuEnabled = () => process.env.QRIS_PATCH_MAMBU_ENABLED == 'true';

const altoUtil = {
  maskName,
  generatePaymentPayload,
  generateStatusInquiryPayload,
  generateAuthorizationId,
  generateBeneficiaryAccountNumberWithLundCheckDigit,
  generateCustomerPan,
  getAltoHttpClientError,
  getAltoReferenceNumber,
  getExceptionErrorCode,
  getQrisNotificationCode,
  getQrisResponseError,
  getQrisTransactionStatus,
  getTransactionHistoryStatus,
  buildCreatePaymentResponse,
  buildCheckStatusResponse,
  buildQrisNotificationMessage,
  buildRefundRequestData,
  isAmountCredited,
  isSuccessQrisPayment,
  isTransactionSettled,
  isFailedQrisPayment,
  isValidResponse,
  isQrisTimeoutError,
  isQrisTransaction,
  setQrisResponseToTransactionData,
  shouldUpdateTransaction,
  shouldAutoRefundQrisPayment,
  sleep,
  updateTransactionDataOnQrisPaymentSuccess,
  updateMambuEnabled
};

export default altoUtil;
