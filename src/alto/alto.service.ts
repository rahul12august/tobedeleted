/* eslint-disable @typescript-eslint/camelcase */
import {
  IAltoRefundDataPayload,
  IAltoRefundDataResponse,
  IAltoRefundResponse,
  IQrisCheckStatusResponse
} from './alto.type';
import logger, { wrapLogs } from '../logger';
import uuidv4 from 'uuid/v4';
import qrisRepository from './alto.repository';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import transactionRepository from '../transaction/transaction.repository';
import { ITransactionModel } from '../transaction/transaction.model';
import generalRefundService from '../transaction/refund/generalRefund.service';
import { qrisUtil } from '@dk/module-common';
import {
  AltoCommandEnum,
  AltoDefaultAuthorizationId,
  AltoRefundResponseCodeEnum,
  AltoRefundResponseTextEnum,
  QrisTransactionStatusEnum
} from './alto.enum';
import { ICreateRefundParam } from '../transaction/refund/refund.type';
import altoUtil from './alto.utils';
import {
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from '../transaction/transaction.enum';
import transactionHelper from '../transaction/transaction.helper';
import { updateTransactionHistoryReferenceIdWithRetry } from '../transactionHistory/transactionHistory.util';
import transactionHistoryRepository from '../transactionHistory/transactionHistory.repository';
import transactionProducer from '../transaction/transaction.producer';
import coreBankingDepositRepository from '../coreBanking/deposit.repository';
import { IPatchTransactionDetail } from '../coreBanking/deposit.interface';
import { PatchOperation } from '../coreBanking/deposit.enum';

const createAltoRefundResponse = (
  payloadData: IAltoRefundDataPayload,
  responseCode: AltoRefundResponseCodeEnum
): IAltoRefundResponse => {
  const responseDataIdentifier: IAltoRefundDataResponse = {
    authorization_id: qrisUtil.generateAuthorizationId(),
    invoice_no_refund: payloadData.invoice_no,
    customer_reference_number: payloadData.customer_reference_number,
    reference_number: payloadData.reference_number.toString()
  };

  let responseText: AltoRefundResponseTextEnum;
  switch (responseCode) {
    case AltoRefundResponseCodeEnum.SUCCESS:
      responseText = AltoRefundResponseTextEnum.SUCCESS;
      break;
    case AltoRefundResponseCodeEnum.FAILED:
      responseDataIdentifier.authorization_id =
        AltoDefaultAuthorizationId.FAILED;
      responseText = AltoRefundResponseTextEnum.FAILED;
      break;
    case AltoRefundResponseCodeEnum.INVALID_AMOUNT:
      responseDataIdentifier.authorization_id =
        AltoDefaultAuthorizationId.FAILED;
      responseText = AltoRefundResponseTextEnum.INVALID_AMOUNT;
      break;
  }

  return {
    command: AltoCommandEnum.QRIS_REFUND,
    response_code: responseCode,
    response_text: responseText,
    data: responseDataIdentifier
  };
};

const markQrisTxnDeclineManually = async (
  txn: ITransactionModel
): Promise<void> => {
  logger.info(
    `markQrisTxnDeclineManually: VOID_QRIS for reference_number: ${txn.thirdPartyOutgoingId}`
  );
  const createRefundParam: ICreateRefundParam = {
    transactionId: txn.id,
    originalTransaction: {
      ...txn,
      status: TransactionStatus.SUCCEED
    },
    amountRefund: txn.transactionAmount,
    refundFees: false,
    refundIdentifier: uuidv4(),
    refundAddInfo1: txn.thirdPartyOutgoingId
  };

  try {
    await generalRefundService.createRefundTransaction(createRefundParam);
  } catch (err) {
    logger.error(
      `markQrisTxnDeclineManually: failed to execute refund for: ${txn.id}.`
    );
    throw new TransferAppError(
      `Failed to execute refund for: ${txn.id}`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.QRIS_REFUND_TRANSACTION_FAILED
    );
  }

  const updatedTransaction = await transactionRepository.update(txn.id, {
    ...txn,
    status: TransactionStatus.DECLINED,
    journey: transactionHelper.getStatusesToBeUpdated(
      TransferJourneyStatusEnum.TRANSFER_FAILURE,
      txn
    )
  });

  if (!updatedTransaction) {
    logger.error(
      `markQrisTxnDeclineManually: failed to update transaction status for transaction id: ${txn.id}.`
    );
    throw new TransferAppError(
      `Failed to update transaction status for transaction id: ${txn.id}.`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION_STATUS
    );
  }
};

const refundTransaction = async (
  payloadData: IAltoRefundDataPayload
): Promise<IAltoRefundResponse> => {
  logger.info(
    `refundTransaction: VOID_QRIS for reference_number: ${payloadData.reference_number}`
  );
  const originalTransaction = await transactionRepository.getByThirdPartyOutgoingId(
    payloadData.reference_number.toString()
  );
  if (!originalTransaction) {
    logger.error(
      `refundTransaction : Original transaction not found for reference_number : ${payloadData.reference_number}`
    );

    const failedRefundResponse: IAltoRefundResponse = createAltoRefundResponse(
      payloadData,
      AltoRefundResponseCodeEnum.FAILED
    );

    return failedRefundResponse;
  }

  const existingQrisTransaction = await transactionRepository.getByQRISRefundIdentifier(
    payloadData.customer_reference_number
  );
  if (existingQrisTransaction) {
    logger.error(
      `refundTransaction : duplicate QRIS refund request for CRN : ${payloadData.customer_reference_number}`
    );

    return createAltoRefundResponse(
      payloadData,
      AltoRefundResponseCodeEnum.FAILED
    );
  }

  if (originalTransaction.transactionAmount < payloadData.amount_refund) {
    return createAltoRefundResponse(
      payloadData,
      AltoRefundResponseCodeEnum.INVALID_AMOUNT
    );
  }

  if (originalTransaction.refund) {
    if (
      originalTransaction.refund.remainingAmount !== undefined &&
      payloadData.amount_refund > originalTransaction.refund.remainingAmount
    ) {
      return createAltoRefundResponse(
        payloadData,
        AltoRefundResponseCodeEnum.INVALID_AMOUNT
      );
    }
  }

  const createRefundParam: ICreateRefundParam = {
    transactionId: originalTransaction.id,
    originalTransaction: originalTransaction,
    amountRefund: payloadData.amount_refund,
    refundFees: false,
    refundIdentifier: payloadData.customer_reference_number,
    refundAddInfo1: payloadData.reference_number
  };

  try {
    await generalRefundService.createRefundTransaction(createRefundParam);
  } catch (err) {
    return createAltoRefundResponse(
      payloadData,
      AltoRefundResponseCodeEnum.FAILED
    );
  }

  return createAltoRefundResponse(
    payloadData,
    AltoRefundResponseCodeEnum.SUCCESS
  );
};

const updateTransactionHistory = async (
  coreBankingTransactionId: string,
  qrisTransactionStatus: QrisTransactionStatusEnum,
  forwardingCustomerReferenceNumber: string | undefined = undefined
) => {
  try {
    await updateTransactionHistoryReferenceIdWithRetry(
      transactionHistoryRepository.updateTransactionHistory,
      coreBankingTransactionId,
      altoUtil.getTransactionHistoryStatus(qrisTransactionStatus),
      forwardingCustomerReferenceNumber
    );
  } catch (error) {
    logger.warn(
      `Failed to update transaction history for ${coreBankingTransactionId}`
    );
  }
};

const patchQrisCustomerReferenceNumberInMambu = async (
  coreBankingTransactionId: string,
  forwardingCustomerReferenceNumber: string
) => {
  const patchPayload: IPatchTransactionDetail = {
    op: PatchOperation.REPLACE,
    path: '/_Custom_Transaction_Details/Third_Party_Outgoing_Id',
    value: altoUtil.getAltoReferenceNumber(forwardingCustomerReferenceNumber)
  };

  try {
    await coreBankingDepositRepository.patchTransactionDetails(
      coreBankingTransactionId,
      [patchPayload]
    );
  } catch (error) {
    logger.warn(
      `Failed to update Mambu transaction details for ${coreBankingTransactionId}`
    );
  }
};

/**
 * Update transaction details in Mambu and MS-TH to include Forwarding Customer Reference Number (FCRN).
 * FCRN will be used as reference number in MS-TH and ThirdPartyOutgoingId in Mambu.
 *
 * Note: The update will be processed asynchronously, as the result of it won't affect the transaction.
 */
const updateTransactionDetailsInExternalService = (
  transactionModel: ITransactionModel,
  qrisTransactionStatus: QrisTransactionStatusEnum,
  forwardingCustomerReferenceNumber: string | undefined = undefined
) => {
  if (
    transactionModel.coreBankingTransactionIds &&
    transactionModel.coreBankingTransactionIds[0]
  ) {
    const coreBankingTransactionId =
      transactionModel.coreBankingTransactionIds[0];
    logger.info(`Updating transaction details for ${coreBankingTransactionId}`);

    updateTransactionHistory(
      coreBankingTransactionId,
      qrisTransactionStatus,
      forwardingCustomerReferenceNumber
    );

    if (altoUtil.updateMambuEnabled() && forwardingCustomerReferenceNumber) {
      patchQrisCustomerReferenceNumberInMambu(
        coreBankingTransactionId,
        forwardingCustomerReferenceNumber
      );
    }
  } else {
    logger.warn(
      `Cannot update transaction details. No coreBankingTransactionId`
    );
  }
};

const publishQrisTransactionNotification = (
  transaction: ITransactionModel,
  qrisTransactionStatus: QrisTransactionStatusEnum
) => {
  const notificationCode = altoUtil.getQrisNotificationCode(
    qrisTransactionStatus
  );
  transactionProducer.sendQrisTransactionNotification(
    transaction,
    notificationCode
  );
};

const updateTransactionStatus = (
  transaction: ITransactionModel,
  isSuccessQrisPayment: boolean,
  shouldAutoRefund: boolean
): ITransactionModel => {
  if (isSuccessQrisPayment) {
    transaction.status = TransactionStatus.SUCCEED;
    transaction.journey = transactionHelper.getStatusesToBeUpdated(
      TransferJourneyStatusEnum.TRANSFER_SUCCESS,
      transaction
    );
  } else if (shouldAutoRefund) {
    // SUCCESS transaction status is required for refund process
    transaction.status = TransactionStatus.SUCCEED;
    transaction.journey = transactionHelper.getStatusesToBeUpdated(
      TransferJourneyStatusEnum.TRANSFER_FAILURE,
      transaction
    );
  } else {
    // any FAILED payment that don't need refund automatically
    transaction.status = TransactionStatus.DECLINED;
    transaction.journey = transactionHelper.getStatusesToBeUpdated(
      TransferJourneyStatusEnum.TRANSFER_FAILURE,
      transaction
    );
    transaction.failureReason = {
      type: ERROR_CODE.ALTO_DECLINED_NO_REFUND_REQUIRED,
      actor: TransferFailureReasonActor.MS_TRANSFER,
      detail: `Payment that don't need refund automatically.`
    };
  }

  return transaction;
};

const autoRefundQrisTransaction = async (
  checkStatusResponse: IQrisCheckStatusResponse,
  transaction: ITransactionModel
): Promise<IAltoRefundResponse> => {
  const refundRequestData: IAltoRefundDataPayload = altoUtil.buildRefundRequestData(
    checkStatusResponse,
    transaction
  );
  try {
    return await refundTransaction(refundRequestData);
  } catch (err) {
    logger.error(
      `Cannot refund transaction ${transaction.id}. ${JSON.stringify(err)}`
    );
    return {
      command: AltoCommandEnum.QRIS_REFUND,
      response_code: AltoRefundResponseCodeEnum.FAILED,
      response_text: AltoRefundResponseTextEnum.FAILED
    };
  }
};

const updateTransaction = async (
  checkStatusResponse: IQrisCheckStatusResponse,
  transaction: ITransactionModel
): Promise<void> => {
  logger.info(`Updating QRIS transaction for status response.
    Customer Reference Number: ${checkStatusResponse.customerReferenceNumber}
    Forwarding Customer Reference Number: ${checkStatusResponse.forwardingCustomerReferenceNumber}
    Invoice No: ${checkStatusResponse.invoiceNo}
    Transaction Status: ${checkStatusResponse.transactionStatus}`);

  const isSuccessQrisPayment = altoUtil.isSuccessQrisPayment(
    checkStatusResponse.transactionStatus
  );
  const shouldAutoRefund = altoUtil.shouldAutoRefundQrisPayment(
    checkStatusResponse
  );
  if (isSuccessQrisPayment) {
    altoUtil.updateTransactionDataOnQrisPaymentSuccess(
      transaction,
      checkStatusResponse
    );
  }
  transaction = updateTransactionStatus(
    transaction,
    isSuccessQrisPayment,
    shouldAutoRefund
  );

  let updatedTransaction: ITransactionModel = await transactionRepository.update(
    transaction.id,
    transaction
  );

  if (shouldAutoRefund) {
    const refundResponse = await autoRefundQrisTransaction(
      checkStatusResponse,
      updatedTransaction
    );

    if (refundResponse.response_code === AltoRefundResponseCodeEnum.SUCCESS) {
      updatedTransaction = await transactionRepository.update(transaction.id, {
        status: TransactionStatus.DECLINED,
        journey: transactionHelper.getStatusesToBeUpdated(
          TransferJourneyStatusEnum.REFUND_CREDITED,
          transaction
        )
      });
    }
  }

  updateTransactionDetailsInExternalService(
    updatedTransaction,
    checkStatusResponse.transactionStatus,
    checkStatusResponse.forwardingCustomerReferenceNumber
  );

  publishQrisTransactionNotification(
    updatedTransaction,
    checkStatusResponse.transactionStatus
  );

  logger.info(`QRIS transaction is updated.
    Transaction ID: ${transaction.id}
    Reference Number: ${transaction.externalId}
    Third Party Outgoing Id: ${transaction.thirdPartyOutgoingId}
    Transaction Status: ${transaction.status}`);
};

const executeCheckStatus = async (
  transaction: ITransactionModel
): Promise<IQrisCheckStatusResponse> => {
  logger.info(`Check status of QRIS transaction.
    Transaction ID: ${transaction.id}
    External ID: ${transaction.externalId}`);

  try {
    const checkStatusResponse: IQrisCheckStatusResponse = await qrisRepository.checkPaymentStatus(
      transaction.externalId
    );

    if (altoUtil.shouldUpdateTransaction(checkStatusResponse)) {
      await updateTransaction(checkStatusResponse, transaction);
    } else {
      await transactionRepository.update(transaction.id, {
        journey: transaction.journey
      });
    }

    logger.info(`Check status result:
      Customer Reference Number: ${checkStatusResponse.customerReferenceNumber}
      Forwarding Customer Reference Number: ${checkStatusResponse.forwardingCustomerReferenceNumber}
      Invoice Number: ${checkStatusResponse.invoiceNo}
      Payment Status: ${checkStatusResponse.transactionStatus}
      Payment Response Code: ${checkStatusResponse.transactionResponseCode}`);

    return checkStatusResponse;
  } catch (error) {
    logger.error(
      `Error while checking status: ${error.errorCode | error.errorMessage}`
    );
    throw new TransferAppError(
      `Error while checking status: ${error.errorCode | error.errorMessage}`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.QRIS_CHECK_STATUS_FAILED
    );
  }
};

const retrieveTransactionToCheck = async (
  customerId: string,
  paymentInstructionId: string
): Promise<ITransactionModel> => {
  const query = {
    ownerCustomerId: customerId,
    paymentInstructionId: paymentInstructionId
  };
  const transaction: ITransactionModel | null = await transactionRepository.findOneByQuery(
    query
  );

  if (transaction == null) {
    logger.error(`checkStatus: Alto failed to get externalId`);
    throw new TransferAppError(
      'Alto failed to get externalId.',
      TransferFailureReasonActor.ALTO,
      ERROR_CODE.QRIS_PAYMENT_CHECK_STATUS_FAILED
    );
  }

  return transaction;
};

const checkStatus = async (
  customerId: string,
  paymentInstructionId: string
): Promise<IQrisCheckStatusResponse> => {
  logger.info(
    `Checking status for transaction with paymentInstructionId: ${paymentInstructionId}, customerId: ${customerId}`
  );
  const transaction: ITransactionModel = await retrieveTransactionToCheck(
    customerId,
    paymentInstructionId
  );

  if (altoUtil.isTransactionSettled(transaction.status)) {
    logger.info(`Cannot update transaction with:
      Id: ${transaction.id}
      Status: ${transaction.status}`);

    return {
      customerReferenceNumber: transaction.externalId,
      forwardingCustomerReferenceNumber:
        transaction.additionalInformation4 || '',
      invoiceNo: transaction.coreBankingTransactionIds
        ? transaction.coreBankingTransactionIds[1]
        : '',
      transactionStatus: altoUtil.getQrisTransactionStatus(transaction.status)
    };
  }

  transaction.journey = transactionHelper.getStatusesToBeUpdated(
    TransferJourneyStatusEnum.CHECK_STATUS,
    transaction
  );

  return await executeCheckStatus(transaction);
};

const qrisService = wrapLogs({
  checkStatus,
  executeCheckStatus,
  refundTransaction,
  markQrisTxnDeclineManually,
  updateTransactionDetailsInExternalService,
  updateTransaction // used for testing
});

export default qrisService;
