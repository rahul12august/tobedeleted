import { IErrorDetail } from '@dk/module-common/dist/error/error.interface';
import hapi from '@hapi/hapi';
import * as Joi from '@hapi/joi';
import { ERROR_CODE } from '../common/errors';
import { TransferAppError } from '../errors/AppError';
import {
  AltoCommandEnum,
  AltoRefundResponseCodeEnum,
  AltoRefundResponseTextEnum,
  AltoResponseCodeEnum,
  QrisTransactionStatusEnum
} from './alto.enum';
import {
  altoRefundRequestValidator,
  qrisCheckStatusRequestValidator
} from './alto.validator';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

export interface IAltoRequestPayload {
  command: string;
  data: IAltoRequestData;
}

export interface IAltoRequestData {
  date_time: string;
  customer_reference_number: string;
}

export interface IAltoCreatePaymentRequestPayload extends IAltoRequestPayload {
  data: IAltoCreatePaymentRequestData;
}

export interface IAltoCreatePaymentRequestData extends IAltoRequestData {
  authorization_id: string;
  currency_code: string;
  amount: number;
  fee: number;
  issuer_nns: string;
  national_mid: string;
  additional_data?: string;
  terminal_label: string;
  merchant: IAltoCreatePaymentRequestPayloadDataMerchant;
  customer: IAltoCreatePaymentRequestPayloadDataCustomer;
}

export interface IAltoCreatePaymentRequestPayloadDataMerchant {
  pan: string;
  id: string;
  criteria: string;
  name: string;
  city: string;
  mcc: string;
  postal_code: string;
  country_code: string;
}

export interface IAltoCreatePaymentRequestPayloadDataCustomer {
  pan: string;
  name: string;
  account_type: string;
}

export interface IAltoCheckStatusRequestPayload extends IAltoRequestPayload {
  data: IAltoCheckStatusRequestData;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IAltoCheckStatusRequestData extends IAltoRequestData {}

export interface IAltoCreatePaymentResponse {
  /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
  id?: string;
  command: string;
  response_code: string;
  response_text: string;
  data: IAltoCreatePaymentResponseData;
}

export interface IAltoCreatePaymentResponseData {
  /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
  customer_reference_number: string;
  forwarding_customer_reference_number: string;
  invoice_no: string;
  currency_code: string;
  amount: number;
  fee: number;
}

export interface IAltoTokenResult {
  access_token: string;
  expires_in: number;
  token_type: string;
}

export interface IQrisTransactionPayload {
  amount?: number;
  externalId: string;
  transactionDetails: {
    transactionChannelId: string;
  };
  qrisData: IAltoCreatePaymentRequestData;
}

export interface ITransactionInputAdditionalPayloadData {
  dateTime: string;
  currencyCode: string;
  amount: number;
  fee: number;
  issuerNns: string;
  nationalMid: string;
  additionalData?: string;
  terminalLabel: string;
  merchant: ITransactionInputAdditionalPayloadDataMerchant;
  customer: ITransactionInputAdditionalPayloadDataCustomer;
}

export interface ITransactionInputAdditionalPayloadDataMerchant {
  pan: string;
  id: string;
  criteria: string;
  name: string;
  city: string;
  mcc: string;
  postalCode: string;
  countryCode: string;
}

export interface ITransactionInputAdditionalPayloadDataCustomer {
  pan: string;
  name: string;
  accountType: string;
}

export interface IAltoCheckStatusDataResponse {
  transaction_response_code?: string;
  transaction_status?: string;
  customer_reference_number: string;
  forwarding_customer_reference_number?: string;
  invoice_no?: string;
}

export interface IAltoCheckStatusResponse {
  /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
  id?: string;
  command: string;
  response_code: string;
  response_text: string;
  data: IAltoCheckStatusDataResponse;
}

export interface IQrisResponse {
  customerReferenceNumber: string;
  forwardingCustomerReferenceNumber: string;
  invoiceNo: string;
  transactionStatus: QrisTransactionStatusEnum;
}

export interface IQrisCreatePaymentResponse extends IQrisResponse {
  currencyCode: string;
  amount: number;
  fee: number;
}

export interface IQrisCheckStatusResponse extends IQrisResponse {
  transactionResponseCode?: AltoResponseCodeEnum;
}

export type QrisCheckStatusRequest = Joi.extractType<
  typeof qrisCheckStatusRequestValidator
>;

export type AltoRefundRequest = Joi.extractType<
  typeof altoRefundRequestValidator
>;

export interface IQrisCheckStatusRequest extends hapi.Request {
  payload: QrisCheckStatusRequest;
}
export interface IQrisCheckStatusRequestPayload {
  command: string;
  data: IQrisCheckStatusDataPayload;
}

export interface IQrisCheckStatusDataPayload {
  datetime: string;
  customerReferenceNumber: string;
}

export interface IAltoRefundDataPayload {
  date_time: string;
  customer_reference_number: string;
  invoice_no: string;
  currency_code: string;
  amount_refund: number;
  reference_number: string;
}

export interface IAltoRefundRequestPayload {
  command: string;
  data: IAltoRefundDataPayload;
}

export interface IAltoRefundDataResponse {
  authorization_id: string;
  customer_reference_number: string;
  invoice_no_refund: string;
  reference_number: string;
}

export interface IAltoRefundResponse {
  command: AltoCommandEnum;
  response_code: AltoRefundResponseCodeEnum;
  response_text: AltoRefundResponseTextEnum;
  data?: IAltoRefundDataResponse;
}

export interface IAltoRefundRequest extends hapi.Request {
  payload: AltoRefundRequest;
}

export class QrisError extends TransferAppError {
  qrisPaymentStatus: QrisTransactionStatusEnum;
  forwardingCustomerReferenceNumber?: string;
  responseCode?: AltoResponseCodeEnum;
  constructor(
    detail: string,
    actor: TransferFailureReasonActor,
    errorCode: ERROR_CODE,
    status: QrisTransactionStatusEnum,
    forwardingCustomerReferenceNumber?: string,
    responseCode?: AltoResponseCodeEnum,
    errors?: IErrorDetail[]
  ) {
    super(detail, actor, errorCode, errors);
    this.qrisPaymentStatus = status;
    this.responseCode = responseCode;
    this.forwardingCustomerReferenceNumber = forwardingCustomerReferenceNumber;
  }
}
