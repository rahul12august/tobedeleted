/* eslint-disable @typescript-eslint/camelcase */
import Joi from '@hapi/joi';

export const qrisCheckStatusRequestValidator = Joi.object({
  customerId: Joi.string()
    .required()
    .description('Customer Id'),
  paymentInstructionId: Joi.string()
    .required()
    .description('Payment instruction Id'),
  retryAttempt: Joi.number()
    .optional()
    .description('Retry attempt counter')
}).label('qrisCheckStatusRequestValidator');

export const qrisCheckStatusResponseValidator = Joi.object({
  transactionResponseCode: Joi.string()
    .optional()
    .description('Response code of original transaction'),
  transactionStatus: Joi.string()
    .optional()
    .description('Status of original transaction'),
  customerReferenceNumber: Joi.string()
    .required()
    .description('Customer reference number from original transaction'),
  forwardingCustomerReferenceNumber: Joi.string()
    .optional()
    .allow('')
    .description(
      'Forwarding customer reference number of original transactions'
    ),
  invoiceNo: Joi.string()
    .optional()
    .allow('')
    .description('Invoice number of original transactions'),
  altoResponseCode: Joi.string()
    .optional()
    .description('Response code from Alto')
}).label('qrisCheckStatusRequestValidator');

export const altoRefundRequestValidator = Joi.object({
  command: Joi.string()
    .required()
    .valid('qr-refund')
    .description('ALTO webhook command'),
  data: Joi.object({
    date_time: Joi.string().required(),
    customer_reference_number: Joi.string()
      .min(6)
      .max(100)
      .required(),
    invoice_no: Joi.string().required(),
    currency_code: Joi.string()
      .valid('IDR')
      .required(),
    amount_refund: Joi.number()
      .min(1)
      .required(),
    reference_number: Joi.string().required()
  }).required()
}).label('altoRefundRequestValidator');

export const altoRefundResponseValidator = Joi.object({
  command: Joi.string()
    .required()
    .valid('qr-refund')
    .description('ALTO webhook command'),
  response_code: Joi.string().required(),
  response_text: Joi.string().required(),
  data: Joi.object({
    authorization_id: Joi.string().required(),
    customer_reference_number: Joi.string()
      .min(6)
      .max(100)
      .required(),
    invoice_no_refund: Joi.string().required(),
    reference_number: Joi.string().required()
  })
}).label('altoRefundResponseValidator');
