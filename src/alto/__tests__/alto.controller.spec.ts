/* eslint-disable @typescript-eslint/camelcase */
import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import altoController from '../alto.controller';
import ResponseWrapper from '../../plugins/responseWrapper';
import errorHandler from '../../common/handleValidationErrors';
import altoService from '../alto.service';
import {
  mockQrisCheckStatusDataRequest,
  mockQrisCheckStatusResponse
} from '../__mocks__/alto.data';
import {
  IAltoRefundDataPayload,
  IAltoRefundRequestPayload,
  IAltoRefundResponse
} from '../alto.type';
import {
  AltoCommandEnum,
  AltoRefundResponseCodeEnum,
  AltoRefundResponseTextEnum
} from '../alto.enum';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

jest.mock('../alto.service');

describe('altoController', () => {
  let server: hapi.Server;

  beforeAll(() => {
    server = new hapi.Server({
      routes: {
        validate: {
          options: {
            abortEarly: false
          },
          failAction: errorHandler
        }
      }
    });
    server.register([ResponseWrapper]);
    server.route(altoController);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('check status', () => {
    const checkStatusPayload = mockQrisCheckStatusDataRequest(3);

    it('should response 200 with payload', async () => {
      // arrange
      const options = {
        method: 'POST',
        url: '/qris/status',
        payload: checkStatusPayload
      };

      (altoService.checkStatus as jest.Mock).mockResolvedValueOnce(
        mockQrisCheckStatusResponse()
      );

      // act
      const response: hapi.ServerInjectResponse = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(altoService.checkStatus).toBeCalledWith(
        checkStatusPayload.customerId,
        checkStatusPayload.paymentInstructionId
      );
    });

    it('should return bad request error (400) when QRIS payment check status failed', async () => {
      // arrange
      const options = {
        method: 'POST',
        url: '/qris/status',
        payload: checkStatusPayload
      };

      (altoService.checkStatus as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.QRIS_PAYMENT_CHECK_STATUS_FAILED
        )
      );

      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
      expect(response.result.error.code).toEqual(
        ERROR_CODE.QRIS_PAYMENT_CHECK_STATUS_FAILED
      );
    });

    it('should return server error (500) when service throw server error', async () => {
      // arrange
      const options = {
        method: 'POST',
        url: '/qris/status',
        payload: checkStatusPayload
      };

      (altoService.checkStatus as jest.Mock).mockRejectedValueOnce(new Error());
      // act
      const response: any = await server.inject(options);

      // assert
      expect(response.statusCode).toEqual(
        Http.StatusCode.INTERNAL_SERVER_ERROR
      );
    });
  });

  describe('handle refund transaction', () => {
    const altoMockPayload: IAltoRefundDataPayload = {
      date_time: '2019-02-19 04:24:27.000Z',
      customer_reference_number: 'QRIS624d46a846e15b7cfe509759',
      invoice_no: '00000000990000000099',
      currency_code: 'IDR',
      amount_refund: 50000,
      reference_number: '542989150736'
    };
    const altoMockRequest: IAltoRefundRequestPayload = {
      command: 'qr-refund',
      data: altoMockPayload
    };
    it('should response 200 with valid payload', async () => {
      // Given
      const options = {
        method: 'POST',
        url: `/alto/refund`,
        payload: altoMockRequest
      };

      const expectedResponse: IAltoRefundResponse = {
        command: AltoCommandEnum.QRIS_REFUND,
        response_code: AltoRefundResponseCodeEnum.SUCCESS,
        response_text: AltoRefundResponseTextEnum.SUCCESS,
        data: {
          authorization_id: 'ABCDEF',
          invoice_no_refund: altoMockPayload.invoice_no,
          customer_reference_number: altoMockPayload.customer_reference_number,
          reference_number: altoMockPayload.reference_number.toString()
        }
      };

      (altoService.refundTransaction as jest.Mock).mockResolvedValueOnce(
        expectedResponse
      );

      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
      expect(response.result).toEqual(expectedResponse);
      expect(altoService.refundTransaction).toBeCalledWith(
        altoMockRequest.data
      );
    });
  });
});
