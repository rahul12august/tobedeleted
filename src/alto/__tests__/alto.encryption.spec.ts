import { getQrisPaymentRequestAttributes } from '../__mocks__/alto.data';
import { generateSignature } from '../alto.encryption';

jest.mock('crypto');

describe('alto.encryption', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });
  const qrisRequestAttributes = getQrisPaymentRequestAttributes();
  describe('generateSignature', () => {
    it('should return signature', async () => {
      process.env.ALTO_VALIDATION_KEY = 'vkey_BoHm104Kb5Tr3xqoHxFS';
      process.env.ALTO_API_KEY = 'akey_PBqzVliT24EnOKEJBsd3';

      // eslint-disable-next-line @typescript-eslint/camelcase
      qrisRequestAttributes.data.authorization_id = 'ABC123';

      const signature = await generateSignature(
        'POST',
        '/v2/qr-payment-sit/payment',
        qrisRequestAttributes.data.date_time,
        JSON.stringify(qrisRequestAttributes)
      );

      const arrayTrue = [
        '117c4428acb8347813eca2906808ed8304bad7a54083fad5559351ced4846124',
        'c8a3843b35d1d29586e557486cbde60ded001017c580f07a803159bc52e9a868'
      ];

      expect(arrayTrue).toContain(signature);
    });
  });
});
