import httpClient, { tokenHTTPClient } from '../httpClient';
import altoRepository from '../alto.repository';
import altoUtil from '../alto.utils';
import {
  getQrisPaymentRequestAttributes,
  mockQrisPaymentInquiryResponse,
  getQrisCheckStatusRequestAttributes,
  mockQrisCheckStatusResponse,
  mockAltoCheckStatusResponse,
  mockAltoCheckStatusNotFoundResponse,
  mockAltoCheckStatusRejected,
  mockAltoCheckStatusSuspend,
  qrisCreatePaymentResponseExpectation
} from '../__mocks__/alto.data';
import { generateSignature } from '../alto.encryption';
import redis from '../../common/redis';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import { QrisTransactionStatusEnum } from '../alto.enum';
import { config } from '../../config';
import { QrisError } from '../alto.type';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

process.env.ALTO_API_KEY = 'akey_PBqzVliT24EnOKEJBsd3';
process.env.ALTO_AUTHORIZATION_USERNAME = 'bankhv7fh90dq0';
process.env.ALTO_AUTHORIZATION_PASSWORD = 'JZwLhmfknmvZweTbWlns';

jest.mock('../httpClient');
jest.mock('../alto.encryption');
jest.mock('../../logger');

describe('alto.repository', () => {
  const { transactionPath, statusPath } = config.get('qrisAlto');

  class NetworkError extends Error {
    status: number;
    code?: string;
    constructor(status = 501, code?: string) {
      super();
      this.status = status;
      this.code = code;
    }
  }

  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('generateCustomerPan', () => {
    it('should return correct PAN number with Luhn', async () => {
      const expected = altoUtil.generateCustomerPan('93600542102838418251');
      expect(expected).toEqual('9360054210283841822');
    });
  });

  describe('maskName', () => {
    it('should return masked name with 1 character in last name', async () => {
      const expected = altoUtil.maskName('JUSTIN B');
      expect(expected).toEqual('JXXXXX B');
    });

    it('should return masked name with 1 character in first name', async () => {
      const expected = altoUtil.maskName('J Bieber');
      expect(expected).toEqual('J BXXXXX');
    });

    it('should return masked name with multiple characters in last and name', async () => {
      const expected = altoUtil.maskName('JUSTIN BIEBER');
      expect(expected).toEqual('JXXXXX BXXXXX');
    });

    it('should return masked name without last name', async () => {
      const expected = altoUtil.maskName('JUSTIN');
      expect(expected).toEqual('JXXXXX');
    });

    it('should return masked name with upper case', async () => {
      const expected = altoUtil.maskName('justin bieber');
      expect(expected).toEqual('JXXXXX BXXXXX');
    });

    it('should return masked name with 30 chars length when the name length is more than 30 chars', async () => {
      const expected = altoUtil.maskName(
        'Hubert Blaine Wolfeschlegelsteinhausenbergerdorff Sr'
      );
      expect(expected).toEqual('HXXXXX BXXXXX WXXXXXXXXXXXXXXX');
    });
  });

  describe('getToken', () => {
    const tokenDetails = {
      data: {
        access_token: '123abcdef798',
        expires_in: 3600,
        token_type: 'Bearer'
      }
    };

    it(`should throw error ERROR_CODE.ALTO_AUTH_TOKEN_ACCESS_ERROR when receiving failed response`, async () => {
      // Given
      (redis.get as jest.Mock).mockResolvedValueOnce(null);
      (tokenHTTPClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          title: 'Unauthorized',
          status: 401,
          detail: 'Bad credentials',
          path: '/api/auth/token',
          message: 'error.http.401'
        }
      });

      // When
      const expected = await altoRepository.getToken().catch(err => err);

      // Then
      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toBe(ERROR_CODE.ALTO_AUTH_TOKEN_ACCESS_ERROR);
      expect(expected.actor).toBe(TransferFailureReasonActor.ALTO);
      expect(expected.detail).toBe('Cannot get token from Alto.');
    });

    it(`should throw error ERROR_CODE.ALTO_AUTH_TOKEN_ACCESS_ERROR} when request timeout`, async () => {
      // Given
      (redis.get as jest.Mock).mockResolvedValueOnce(null);
      (tokenHTTPClient.post as jest.Mock).mockImplementation(() => {
        throw new Error('timeout');
      });

      // When
      const expected = await altoRepository.getToken().catch(err => err);

      // Then
      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toBe(ERROR_CODE.ALTO_AUTH_TOKEN_ACCESS_ERROR);
      expect(expected.actor).toBe(TransferFailureReasonActor.ALTO);
      expect(expected.detail).toBe('Error fetching access token from Alto.');
    });

    it(`should return access_token when request successful`, async () => {
      // Given
      (redis.get as jest.Mock).mockResolvedValueOnce(null);
      (tokenHTTPClient.post as jest.Mock).mockResolvedValueOnce(tokenDetails);
      (redis.set as jest.Mock).mockResolvedValueOnce(tokenDetails);

      // When
      const expected = await altoRepository.getToken().catch(err => err);

      // Then
      expect(expected).toBe('123abcdef798');
    });

    it(`should return access_token when request successful when ttl is invalid because using default ttl`, async () => {
      // Given
      const tokenDefaultTTL = { ...tokenDetails };
      tokenDefaultTTL.data.expires_in = 100;

      (redis.get as jest.Mock).mockResolvedValueOnce(null);
      (tokenHTTPClient.post as jest.Mock).mockResolvedValueOnce(
        tokenDefaultTTL
      );
      (redis.set as jest.Mock).mockResolvedValueOnce(tokenDefaultTTL);

      // When
      const expected = await altoRepository.getToken().catch(err => err);

      // Then
      expect(expected).toBe('123abcdef798');
    });

    it(`should return access_token when failed to access cache`, async () => {
      // Given
      (redis.get as jest.Mock).mockImplementation(() => {
        throw new Error('Failed');
      });
      (tokenHTTPClient.post as jest.Mock).mockResolvedValueOnce(tokenDetails);
      (redis.set as jest.Mock).mockImplementation(() => {
        throw new Error('Failed');
      });

      // When
      const expected = await altoRepository.getToken().catch(err => err);

      // Then
      expect(expected).toBe('123abcdef798');
    });
  });

  describe('submitTransaction', () => {
    const altoRequestPaymentPayload = getQrisPaymentRequestAttributes();
    const altoResponsePayment = mockQrisPaymentInquiryResponse();
    const tokenDetails = {
      /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
      access_token: '123abcdef798',
      expires_in: 1234567,
      token_type: 'order'
    };

    it('should return qr payment success', async () => {
      // Given
      const signature = 'signature';
      const httpClientMock = httpClient.post as jest.Mock;
      httpClientMock.mockResolvedValueOnce({
        data: altoResponsePayment
      });
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);

      // When
      const expected = await altoRepository.submitTransaction(
        altoRequestPaymentPayload.data
      );

      // Then
      const expectedResponse = qrisCreatePaymentResponseExpectation();
      expect(expected).toEqual(expectedResponse);

      expect(httpClientMock.mock.calls[0][0]).toBe(transactionPath);
    });

    it('should catch an error', async () => {
      // Given
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);

      // When
      const expected = await altoRepository
        .submitTransaction(altoRequestPaymentPayload.data)
        .catch(err => err);

      // Then
      expect(expected).toBeInstanceOf(QrisError);
      expect(expected.errorCode).toBe(ERROR_CODE.ERROR_FROM_ALTO);
    });

    it(`should throw error ERROR_FROM_ALTO when result is null`, async () => {
      // Given
      const signature = 'OpH++zdKpBwQFx6medZnLWY+==';
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);
      (httpClient.post as jest.Mock).mockResolvedValueOnce(tokenDetails);
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);
      (httpClient.post as jest.Mock).mockRejectedValueOnce({
        data: null
      });

      // When
      const expected = await altoRepository
        .submitTransaction(altoRequestPaymentPayload.data)
        .catch(err => err);

      // Then
      expect(expected).toBeInstanceOf(QrisError);
      expect(expected.errorCode).toBe(ERROR_CODE.ERROR_FROM_ALTO);
    });

    it(`should throw error ERROR_CODE.ERROR_FROM_ALTO when token is invalid`, async () => {
      // Given
      const signature = 'OpH++zdKpBwQFx6medZnLWY+==';
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        title: 'Unauthorized',
        status: 401,
        detail: 'Bad credentials',
        path: '/api/auth/token',
        message: 'error.http.401'
      });
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);

      // When
      const expected = await altoRepository
        .submitTransaction(altoRequestPaymentPayload.data)
        .catch(err => err);

      // Then
      expect(expected).toBeInstanceOf(QrisError);
      expect(expected.errorCode).toBe(ERROR_CODE.ERROR_FROM_ALTO);
    });

    it(`should throw error ERROR_CODE.ERROR_FROM_ALTO when result is null`, async () => {
      // Given
      const signature = 'OpH++zdKpBwQFx6medZnLWY+==';
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);
      (httpClient.post as jest.Mock).mockRejectedValueOnce({
        data: null
      });

      // When
      const expected = await altoRepository
        .submitTransaction(altoRequestPaymentPayload.data)
        .catch(err => err);

      // Then
      expect(expected).toBeInstanceOf(QrisError);
      expect(expected.errorCode).toBe(ERROR_CODE.ERROR_FROM_ALTO);
    });

    it(`should throw error ERROR_CODE.ERROR_FROM_ALTO when result is showing error`, async () => {
      // Given
      const signature = 'OpH++zdKpBwQFx6medZnLWY+==';
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          command: 'qr-payment-credit',
          response_code: '070',
          response_text: 'Message Format Error',
          data: {
            customer_reference_number: '208237947',
            forwarding_customer_reference_number: '',
            invoice_no: '00000000000000000000',
            currency_code: 'IDR',
            amount: 100500,
            fee: 500
          }
        }
      });

      // When
      const expected = await altoRepository
        .submitTransaction(altoRequestPaymentPayload.data)
        .catch(err => err);

      // Then
      expect(expected).toBeInstanceOf(QrisError);
      expect(expected.errorCode).toBe(ERROR_CODE.ERROR_FROM_ALTO);
    });

    it(`should throw error ERROR_CODE.ERROR_FROM_ALTO when cannot reach alto host`, async () => {
      // Given
      (httpClient.post as jest.Mock).mockImplementation(() => {
        throw new NetworkError();
      });
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);

      // When
      const expected = await altoRepository
        .submitTransaction(altoRequestPaymentPayload.data)
        .catch(err => err);

      // Then
      expect(expected).toBeInstanceOf(QrisError);
      expect(expected.errorCode).toBe(ERROR_CODE.ERROR_FROM_ALTO);
    });

    it(`should throw exception when receiving RC 002 from Alto`, async () => {
      // Given
      const signature = 'OpH++zdKpBwQFx6medZnLWY+==';
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          command: 'qr-payment-credit',
          response_code: '002',
          response_text: 'Suspend',
          data: {
            customer_reference_number: '208237947',
            forwarding_customer_reference_number: '123456789012',
            invoice_no: '00000000000000000000',
            currency_code: 'IDR',
            amount: 100000,
            fee: 0,
            forwardingCustomerReferenceNumber: '123456789012'
          }
        }
      });

      // When
      const result = await altoRepository
        .submitTransaction(altoRequestPaymentPayload.data)
        .catch(err => err);

      // Then
      expect(result).toBeInstanceOf(QrisError);
      expect(result.errorCode).toBe(ERROR_CODE.QRIS_TRANSACTION_TIMEOUT);
    });

    it(`should throw error ERROR_CODE.QRIS_TRANSACTION_TIMEOUT when http client timeout`, async () => {
      // Given
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);
      (httpClient.post as jest.Mock).mockImplementation(() => {
        throw new Error('timeout in 1000 exceeds');
      });

      // When
      const expected = await altoRepository
        .submitTransaction(altoRequestPaymentPayload.data)
        .catch(err => err);

      // Then
      expect(expected).toBeInstanceOf(QrisError);
      expect(expected.errorCode).toBe(ERROR_CODE.QRIS_TRANSACTION_TIMEOUT);
    });
  });

  describe('checkPaymentStatus', () => {
    const altoRequestStatusPayload = getQrisCheckStatusRequestAttributes();
    const altoResponseStatus = mockQrisCheckStatusResponse();
    const tokenDetails = {
      /*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
      access_token: '123abcdef798',
      expires_in: 1234567,
      token_type: 'order'
    };

    it('should return qr payment success', async () => {
      // Given
      const signature = 'signature';
      const httpClientMock = httpClient.post as jest.Mock;
      httpClientMock.mockResolvedValueOnce({
        data: mockAltoCheckStatusResponse()
      });
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);

      // When
      const expected = await altoRepository.checkPaymentStatus(
        altoRequestStatusPayload.data.customerReferenceNumber
      );

      // Then
      expect(expected).toEqual(altoResponseStatus);
      expect(httpClientMock.mock.calls[0][0]).toBe(statusPath);
    });

    it('should throw error ERROR_FROM_ALTO when result is undefined', async () => {
      // Given
      const signature = 'signature';
      (httpClient.post as jest.Mock).mockResolvedValueOnce({});
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);

      // When
      const expected = await altoRepository
        .checkPaymentStatus(
          altoRequestStatusPayload.data.customerReferenceNumber
        )
        .catch(err => err);

      // Then
      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toBe(ERROR_CODE.ERROR_FROM_ALTO);
      expect(expected.actor).toBe(TransferFailureReasonActor.ALTO);
      expect(expected.detail).toBe('Unknown issue with alto!');
    });

    it('should return FAILED when response code is A00', async () => {
      // Given
      const signature = 'signature';
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: mockAltoCheckStatusRejected()
      });
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);

      // When
      const expected = await altoRepository
        .checkPaymentStatus(
          altoRequestStatusPayload.data.customerReferenceNumber
        )
        .catch(err => err);

      // Then
      expect(expected.transactionStatus).toEqual(
        QrisTransactionStatusEnum.FAILED
      );
    });

    it('should return SUSPEND when response code is 096', async () => {
      // Given
      const signature = 'signature';
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: mockAltoCheckStatusNotFoundResponse()
      });
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);

      // When
      const expected = await altoRepository
        .checkPaymentStatus(
          altoRequestStatusPayload.data.customerReferenceNumber
        )
        .catch(err => err);

      // Then
      expect(expected.transactionStatus).toEqual(
        QrisTransactionStatusEnum.SUSPEND
      );
    });

    it('should return SUSPEND when response code is 002', async () => {
      // Given
      const signature = 'signature';
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: mockAltoCheckStatusSuspend()
      });
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);

      // When
      const expected = await altoRepository
        .checkPaymentStatus(
          altoRequestStatusPayload.data.customerReferenceNumber
        )
        .catch(err => err);

      // Then
      expect(expected.transactionStatus).toEqual(
        QrisTransactionStatusEnum.SUSPEND
      );
    });

    it('should return SUSPEND when http client timeout', async () => {
      // Given
      const signature = 'signature';
      (httpClient.post as jest.Mock).mockImplementation(() => {
        throw new Error('timeout in 1000 exceeds');
      });
      (generateSignature as jest.Mock).mockResolvedValueOnce(signature);
      (redis.get as jest.Mock).mockResolvedValueOnce(tokenDetails);

      // When
      const expected = await altoRepository
        .checkPaymentStatus(
          altoRequestStatusPayload.data.customerReferenceNumber
        )
        .catch(err => err);

      // Then
      expect(expected.transactionStatus).toEqual(
        QrisTransactionStatusEnum.SUSPEND
      );
    });
  });
});
