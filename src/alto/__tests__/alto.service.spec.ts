/* eslint-disable @typescript-eslint/camelcase */
import altoService from '../alto.service';
import {
  mockCheckStatusResponseApproved,
  mockCheckStatusResponseRejected,
  mockCheckStatusResponseSuspended,
  mockCheckStatusResponseUnknown,
  mockErrorQrisPaymentResponse,
  mockQrisCheckStatusDataRequest,
  mockQrisCheckStatusResponse,
  mockQrisCheckStatusSuspectResponse
} from '../__mocks__/alto.data';
import altoRepository from '../alto.repository';
import transactionRepository from '../../transaction/transaction.repository';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import { PaymentServiceTypeEnum } from '@dk/module-common';
import transactionHelper from '../../transaction/transaction.helper';
import refundValidator from '../../transaction/refund/refund.validator';
import faker from 'faker';
import transactionExecutionService from '../../transaction/transactionExecution.service';
import transactionService from '../../transaction/transaction.service';
import {
  getTransactionDetail,
  getTransactionMockValueObject,
  getTransactionModelFullData
} from '../../transaction/__mocks__/transaction.data';
import { IAltoRefundDataPayload, QrisError } from '../alto.type';
import configurationRepository from '../../configuration/configuration.repository';
import {
  AltoDefaultAuthorizationId,
  AltoRefundResponseCodeEnum,
  AltoResponseCodeEnum,
  QrisTransactionStatusEnum
} from '../alto.enum';
import altoUtils from '../alto.utils';
import {
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from '../../transaction/transaction.enum';
import { ITransactionModel } from '../../transaction/transaction.model';
import { mockTransactionFromDb } from '../../transaction/__mocks__/transactionConfirmation.data';
import helper from '../../transaction/transactionCoreBanking.helper';
import { getRetryCount } from '../../common/contextHandler';

jest.mock('../alto.repository');
jest.mock('../../transaction/transaction.repository');
jest.mock('../../configuration/configuration.repository');
jest.mock('../../account/account.cache');
jest.mock('../../transaction/transaction.producer');
jest.mock('../../virtualAccount/virtualAccount.repository');
jest.mock('../../transaction/transaction.repository');
jest.mock('../../transaction/transactionExecution.service');
jest.mock('../../transaction/transaction.service');
jest.mock('../../transaction/refund/refund.validator');
jest.mock('../../transaction/transaction.helper');
jest.mock('../../decisionEngine/decisionEngine.service.ts');
jest.mock('../../common/contextHandler.ts');

describe('alto.service', () => {
  afterEach(async () => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('checkStatus', () => {
    const input = mockQrisCheckStatusDataRequest(3);
    const transactionModel: ITransactionModel = {
      ...getTransactionModelFullData(),
      id: '12345'
    };

    it('should post checking status of QRIS payment through ALTO repo', async () => {
      // Given
      const mockSuccessResponse = mockQrisCheckStatusResponse();

      altoRepository.checkPaymentStatus = jest
        .fn()
        .mockReturnValueOnce(mockSuccessResponse);

      transactionRepository.findOneByQuery = jest.fn().mockReturnValueOnce({
        ...transactionModel,
        status: TransactionStatus.SUBMITTED
      });

      transactionRepository.update = jest.fn().mockReturnValueOnce({
        ...transactionModel,
        status: TransactionStatus.SUCCEED
      });

      // When
      const actualResult = await altoService.checkStatus(
        input.customerId,
        input.paymentInstructionId
      );

      // Then
      expect(actualResult).toEqual(mockSuccessResponse);
    });

    it('should return status SUSPECT on third attempt', async () => {
      // Given
      const mockSuccessResponse = mockQrisCheckStatusSuspectResponse();

      altoRepository.checkPaymentStatus = jest
        .fn()
        .mockReturnValueOnce(mockSuccessResponse);

      transactionRepository.findOneByQuery = jest.fn().mockReturnValueOnce({
        ...transactionModel,
        status: TransactionStatus.SUBMITTED
      });

      transactionRepository.update = jest.fn().mockReturnValueOnce({
        ...transactionModel,
        status: TransactionStatus.SUCCEED
      });

      // When
      const actualResult = await altoService.checkStatus(
        input.customerId,
        input.paymentInstructionId
      );

      // Then
      expect(actualResult).toEqual(mockSuccessResponse);
    });

    it('should throw error when no QRIS Transaction found', async () => {
      // Given
      transactionRepository.findOneByQuery = jest
        .fn()
        .mockReturnValueOnce(null);

      // When
      const expected = await altoService
        .checkStatus(input.customerId, input.paymentInstructionId)
        .catch(err => err);

      // Then
      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toBe(
        ERROR_CODE.QRIS_PAYMENT_CHECK_STATUS_FAILED
      );
    });

    it('should throw QRIS_CHECK_STATUS_FAILED when catching error from repository', async () => {
      // Given
      altoRepository.checkPaymentStatus = jest
        .fn()
        .mockImplementationOnce(() => {
          throw new TransferAppError(
            'Test error.',
            TransferFailureReasonActor.UNKNOWN,
            ERROR_CODE.ALTO_SERVER_ERROR
          );
        });

      transactionRepository.findOneByQuery = jest.fn().mockReturnValueOnce({
        ...transactionModel,
        status: TransactionStatus.SUBMITTED
      });

      // When
      const expected = await altoService
        .checkStatus(input.customerId, input.paymentInstructionId)
        .catch(err => err);

      // Then
      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toBe(ERROR_CODE.QRIS_CHECK_STATUS_FAILED);
    });

    it('should update transaction journey when QRIS payment status is Suspend', async () => {
      const mockTransactionRepositoryGetByKey = transactionRepository.getByKey as jest.Mock;
      const mockTransactionUpdate = transactionRepository.update as jest.Mock;

      transactionRepository.findOneByQuery = jest.fn().mockReturnValueOnce({
        ...transactionModel,
        status: TransactionStatus.SUBMITTED
      });

      altoRepository.checkPaymentStatus = jest
        .fn()
        .mockReturnValueOnce(mockCheckStatusResponseSuspended());

      const result = await altoService.checkStatus(
        input.customerId,
        input.paymentInstructionId
      );

      expect(result).toEqual(mockCheckStatusResponseSuspended());
      expect(mockTransactionRepositoryGetByKey).not.toBeCalled();
      expect(mockTransactionUpdate).toBeCalledTimes(1);
      expect(mockTransactionUpdate).toBeCalledWith('12345', {
        journey: transactionHelper.getStatusesToBeUpdated(
          TransferJourneyStatusEnum.CHECK_STATUS,
          transactionModel
        )
      });
    });

    it('should not check status when transaction already settled', async () => {
      const mockTransactionUpdate = transactionRepository.update as jest.Mock;

      altoRepository.checkPaymentStatus = jest
        .fn()
        .mockImplementationOnce(() => {
          throw new TransferAppError(
            'Test error.',
            TransferFailureReasonActor.UNKNOWN,
            ERROR_CODE.ALTO_SERVER_ERROR
          );
        });

      transactionRepository.findOneByQuery = jest.fn().mockReturnValueOnce({
        status: TransactionStatus.DECLINED,
        externalId: 'externalId123',
        additionalInformation4: 'fcrn123',
        coreBankingTransactionIds: ['mambu123', 'invoice123']
      });

      const result = await altoService.checkStatus(
        input.customerId,
        input.paymentInstructionId
      );

      expect(result).toEqual({
        customerReferenceNumber: 'externalId123',
        forwardingCustomerReferenceNumber: 'fcrn123',
        invoiceNo: 'invoice123',
        transactionStatus: QrisTransactionStatusEnum.FAILED
      });
      expect(altoRepository.checkPaymentStatus).not.toBeCalled();
      expect(mockTransactionUpdate).not.toBeCalled();
    });
  });

  describe('refundTransaction', () => {
    const altoMockPayload: IAltoRefundDataPayload = {
      date_time: '2019-02-19 04:24:27.000Z',
      customer_reference_number: 'QRIS624d46a846e15b7cfe509759',
      invoice_no: '00000000990000000099',
      currency_code: 'IDR',
      amount_refund: 50000,
      reference_number: '542989150736'
    };
    const {
      sourceBankCode: bankCode,
      transactionCategory,
      ruleConfigs,
      limitGroups
    } = getTransactionMockValueObject();

    const original = require.requireActual(
      '../../transaction/transaction.helper'
    ).default;

    const serviceCodes = ['p01'];

    const feeRule = {
      basicFeeMapping: { basicFee: { code: 'f01', refundFees: 1 } }
    };

    beforeEach(() => {
      (configurationRepository.getTransactionCategory as jest.Mock).mockResolvedValue(
        transactionCategory
      );

      // setup bankCode
      (configurationRepository.getBankCodeMapping as jest.Mock).mockResolvedValue(
        bankCode
      );

      (transactionHelper.mapAuthenticationRequired as jest.Mock).mockImplementation(
        value => value
      );

      (transactionHelper.mapFeeData as jest.Mock).mockImplementation(
        value => value
      );

      (transactionHelper.getPaymentServiceMappings as jest.Mock).mockReturnValue(
        serviceCodes
      );
      (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValue(
        ruleConfigs
      );
      (configurationRepository.getLimitGroupsByCodes as jest.Mock).mockResolvedValue(
        limitGroups
      );
      (configurationRepository.getFeeRule as jest.Mock).mockResolvedValue(
        feeRule
      );
      (transactionHelper.getBankCodeInfo as jest.Mock).mockResolvedValue({
        ...original,
        mapAuthenticationRequired: jest.fn(),
        mapFeeData: jest.fn(),
        getAccountInfo: jest.fn(),
        getSourceAccount: jest.fn(),
        validateTransactionLimitAmount: jest.fn(),
        getPaymentServiceMappings: jest.fn()
      });
      (transactionHelper.populateTransactionInput as jest.Mock).mockResolvedValue(
        {
          ...original,
          mapAuthenticationRequired: jest.fn(),
          mapFeeData: jest.fn(),
          getAccountInfo: jest.fn(),
          getSourceAccount: jest.fn(),
          validateTransactionLimitAmount: jest.fn(),
          getPaymentServiceMappings: jest.fn()
        }
      );
    });
    afterEach(() => {
      jest.resetAllMocks();
    });
    it('should throw error INVALID_BANK_CODE Bank info not found', async () => {
      //Given
      let newTransactionModel = getTransactionModelFullData();

      const originalTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.QRIS,
        transactionAmount: 50000,
        id: 'test12345'
      };

      (transactionRepository.getByThirdPartyOutgoingId as jest.Mock).mockResolvedValueOnce(
        originalTransaction
      );
      (transactionHelper.getBankCodeInfo as jest.Mock).mockResolvedValue(null);

      // When
      const response = await altoService
        .refundTransaction(altoMockPayload)
        .catch(e => e);

      // Then
      expect(response).toBeDefined();
      expect(response.response_code).toEqual(AltoRefundResponseCodeEnum.FAILED);
    });

    it('should throw error when original transaction not found', async () => {
      //Given
      (transactionRepository.getByThirdPartyOutgoingId as jest.Mock).mockResolvedValueOnce(
        null
      );

      // When
      const response = await altoService
        .refundTransaction(altoMockPayload)
        .catch(e => e);

      // Then
      expect(response).toBeDefined();
      expect(response.response_code).toEqual(AltoRefundResponseCodeEnum.FAILED);
    });

    it('should throw error when validation fails', async () => {
      //Given
      let newTransactionModel = getTransactionModelFullData();

      const originalTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.QRIS,
        transactionAmount: 50000,
        id: 'test12345'
      };

      (transactionRepository.getByThirdPartyOutgoingId as jest.Mock).mockResolvedValueOnce(
        originalTransaction
      );

      (refundValidator.validateGenaralRefundRequest as jest.Mock).mockImplementationOnce(
        () => {
          throw new TransferAppError(
            'Test error.',
            TransferFailureReasonActor.UNKNOWN,
            ERROR_CODE.INVALID_REFUND_REQUEST
          );
        }
      );

      // When
      const response = await altoService
        .refundTransaction(altoMockPayload)
        .catch(e => e);

      // Then
      expect(response).toBeDefined();
      expect(response.response_code).toEqual(AltoRefundResponseCodeEnum.FAILED);
    });

    it('should throw error when bankcode not found', async () => {
      //Given
      let newTransactionModel = getTransactionModelFullData();

      const originalTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.QRIS,
        transactionAmount: 50000,
        id: 'test12345'
      };

      (transactionRepository.getByThirdPartyOutgoingId as jest.Mock).mockResolvedValueOnce(
        originalTransaction
      );

      (transactionHelper.getBankCodeInfo as jest.Mock).mockRejectedValue(
        new TransferAppError(
          'Test error.',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.INVALID_REGISTER_PARAMETER
        )
      );

      // When
      const response = await altoService
        .refundTransaction(altoMockPayload)
        .catch(e => e);

      // Then
      expect(response).toBeDefined();
      // expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REGISTER_PARAMETER);
      expect(response.response_code).toEqual(AltoRefundResponseCodeEnum.FAILED);
    });

    describe('refund general transaction success function', () => {
      afterEach(() => {
        jest.resetAllMocks();
      });
      let newTransactionModel = getTransactionModelFullData();

      const originalTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.QRIS,
        transactionAmount: 50000,
        id: 'test12345',
        fees: [
          {
            feeCode: faker.random.alphaNumeric(4),
            customerTc: faker.random.alphaNumeric(4),
            customerTcChannel: faker.random.alphaNumeric(4),
            feeAmount: faker.random.number({ min: 0, max: 5000 })
          },
          {
            feeCode: faker.random.alphaNumeric(4),
            customerTc: faker.random.alphaNumeric(4),
            customerTcChannel: faker.random.alphaNumeric(4),
            feeAmount: faker.random.number({ min: 0, max: 5000 })
          }
        ]
      };

      const refundedTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.VOID_QRIS,
        transactionAmount: 50000,
        id: 'test1234',
        additionalInformation4: originalTransaction.id
      };
      it('should successfully refund transaction', async () => {
        (transactionRepository.getByThirdPartyOutgoingId as jest.Mock).mockResolvedValueOnce(
          originalTransaction
        );
        (transactionRepository.getByQRISRefundIdentifier as jest.Mock).mockResolvedValueOnce(
          null
        );

        (transactionExecutionService.executeTransaction as jest.Mock).mockResolvedValueOnce(
          refundedTransaction
        );

        (refundValidator.validateGenaralRefundRequest as jest.Mock).mockReturnValueOnce(
          null
        );
        (transactionService.updateOriginalTransactionWithRefund as jest.Mock).mockResolvedValueOnce(
          {
            ...originalTransaction,
            refund: {
              remainingAmount: 0,
              transactions: [refundedTransaction.id]
            }
          }
        );
        (transactionService.updateRefundTransaction as jest.Mock).mockResolvedValueOnce(
          {
            ...refundedTransaction,
            refund: {
              originalTransactionId: originalTransaction.id,
              revertPaymentServiceCode: originalTransaction.paymentServiceCode
            }
          }
        );

        await altoService.refundTransaction(altoMockPayload);

        expect(transactionRepository.getByThirdPartyOutgoingId).toBeCalledWith(
          altoMockPayload.reference_number.toString()
        );
        expect(refundValidator.validateGenaralRefundRequest).toBeCalled();
        expect(
          transactionService.updateOriginalTransactionWithRefund
        ).toHaveBeenCalled();
        expect(transactionService.updateRefundTransaction).toHaveBeenCalled();
        expect(refundedTransaction.additionalInformation4).toBeDefined();
      });

      it('should success if refund value is less than original transaction amount if partial', async () => {
        const origRefundTrx = { ...originalTransaction };
        origRefundTrx.refund = {
          remainingAmount: originalTransaction.transactionAmount / 2
        };
        (transactionRepository.getByThirdPartyOutgoingId as jest.Mock).mockResolvedValueOnce(
          origRefundTrx
        );
        (transactionRepository.getByQRISRefundIdentifier as jest.Mock).mockResolvedValueOnce(
          null
        );

        const cpAltoMockPayload = { ...altoMockPayload };
        cpAltoMockPayload.amount_refund =
          originalTransaction.transactionAmount / 2;
        const response = await altoService.refundTransaction(cpAltoMockPayload);

        expect(transactionRepository.getByThirdPartyOutgoingId).toBeCalledWith(
          cpAltoMockPayload.reference_number.toString()
        );
        expect(response.response_code).toEqual(
          AltoRefundResponseCodeEnum.SUCCESS
        );
      });

      it('should failed if refund value is greater than original transaction', async () => {
        (transactionRepository.getByThirdPartyOutgoingId as jest.Mock).mockResolvedValueOnce(
          originalTransaction
        );
        (transactionRepository.getByQRISRefundIdentifier as jest.Mock).mockResolvedValueOnce(
          null
        );

        const cpAltoMockPayload = { ...altoMockPayload };
        cpAltoMockPayload.amount_refund =
          originalTransaction.transactionAmount + 1000;
        const response = await altoService.refundTransaction(cpAltoMockPayload);

        expect(transactionRepository.getByThirdPartyOutgoingId).toBeCalledWith(
          cpAltoMockPayload.reference_number.toString()
        );
        expect(response.response_code).toEqual(
          AltoRefundResponseCodeEnum.INVALID_AMOUNT
        );
        expect(response.data?.authorization_id).toEqual(
          AltoDefaultAuthorizationId.FAILED
        );
      });

      it('should failed if refund value is greater than original transaction using partial', async () => {
        const origRefundTrx = { ...originalTransaction };
        origRefundTrx.refund = {
          remainingAmount: originalTransaction.transactionAmount / 2
        };
        (transactionRepository.getByThirdPartyOutgoingId as jest.Mock).mockResolvedValueOnce(
          origRefundTrx
        );
        (transactionRepository.getByQRISRefundIdentifier as jest.Mock).mockResolvedValueOnce(
          null
        );

        const response = await altoService.refundTransaction(altoMockPayload);

        expect(transactionRepository.getByThirdPartyOutgoingId).toBeCalledWith(
          altoMockPayload.reference_number.toString()
        );
        expect(response.response_code).toEqual(
          AltoRefundResponseCodeEnum.INVALID_AMOUNT
        );
        expect(response.data?.authorization_id).toEqual(
          AltoDefaultAuthorizationId.FAILED
        );
      });

      it('should failed if transaction not found', async () => {
        const origRefundTrx = { ...originalTransaction };
        origRefundTrx.refund = {
          remainingAmount: originalTransaction.transactionAmount / 2
        };
        (transactionRepository.getByThirdPartyOutgoingId as jest.Mock).mockResolvedValueOnce(
          null
        );
        (transactionRepository.getByQRISRefundIdentifier as jest.Mock).mockResolvedValueOnce(
          null
        );

        const response = await altoService.refundTransaction(altoMockPayload);

        expect(transactionRepository.getByThirdPartyOutgoingId).toBeCalledWith(
          altoMockPayload.reference_number.toString()
        );
        expect(response.response_code).toEqual(
          AltoRefundResponseCodeEnum.FAILED
        );
        expect(response.data?.authorization_id).toEqual(
          AltoDefaultAuthorizationId.FAILED
        );
      });

      it('should failed if got multiple refund request (based on CRN)', async () => {
        const origRefundTrx = { ...originalTransaction };
        origRefundTrx.refund = {
          remainingAmount: originalTransaction.transactionAmount / 2
        };
        (transactionRepository.getByThirdPartyOutgoingId as jest.Mock).mockResolvedValueOnce(
          origRefundTrx
        );
        (transactionRepository.getByQRISRefundIdentifier as jest.Mock).mockResolvedValueOnce(
          refundedTransaction
        );

        const response = await altoService.refundTransaction(altoMockPayload);

        expect(transactionRepository.getByThirdPartyOutgoingId).toBeCalledWith(
          altoMockPayload.reference_number.toString()
        );
        expect(transactionRepository.getByQRISRefundIdentifier).toBeCalledWith(
          altoMockPayload.customer_reference_number
        );
        expect(response.response_code).toEqual(
          AltoRefundResponseCodeEnum.FAILED
        );
        expect(response.data?.authorization_id).toEqual(
          AltoDefaultAuthorizationId.FAILED
        );
      });
    });

    describe('markQrisTxnDeclineManually', () => {
      it('markQrisTxnDeclineManually success', async () => {
        let txn = {
          ...getTransactionDetail(),
          status: TransactionStatus.SUBMITTED
        };

        (transactionRepository.update as jest.Mock).mockReturnValue({
          ...txn,
          status: TransactionStatus.DECLINED
        });

        await altoService.markQrisTxnDeclineManually(txn);

        expect(transactionRepository.update).toBeCalled();
      });

      it('should throw error QRIS_REFUND_TRANSACTION_FAILED when create refund failed', async () => {
        let txn = {
          ...getTransactionDetail(),
          status: TransactionStatus.SUBMITTED
        };

        (transactionRepository.update as jest.Mock).mockReturnValue({
          ...txn,
          status: TransactionStatus.DECLINED
        });

        (transactionHelper.getBankCodeInfo as jest.Mock).mockResolvedValue(
          null
        );

        const expected = await altoService
          .markQrisTxnDeclineManually(txn)
          .catch(err => err);

        // Then
        expect(expected).toBeInstanceOf(TransferAppError);
        expect(expected.errorCode).toBe(
          ERROR_CODE.QRIS_REFUND_TRANSACTION_FAILED
        );
      });

      it('should throw error FAILED_TO_UPDATE_TRANSACTION_STATUS when failed to update transaction status', async () => {
        let txn = {
          ...getTransactionDetail(),
          status: TransactionStatus.SUBMITTED
        };

        (transactionRepository.update as jest.Mock).mockReturnValue(null);

        const expected = await altoService
          .markQrisTxnDeclineManually(txn)
          .catch(err => err);

        // Then
        expect(expected).toBeInstanceOf(TransferAppError);
        expect(expected.errorCode).toBe(
          ERROR_CODE.FAILED_TO_UPDATE_TRANSACTION_STATUS
        );
        expect(transactionRepository.update).toBeCalled();
      });
    });
  });

  describe('update transaction after check status', () => {
    const mockedTransactionDetail = getTransactionDetail();

    it('should update transaction to SUCCESS when QRIS payment is Approved', async () => {
      const mockTransactionUpdate = transactionRepository.update as jest.Mock;
      mockTransactionUpdate.mockReturnValue({
        ...mockedTransactionDetail,
        status: TransactionStatus.SUCCEED
      });

      const result = await altoService
        .updateTransaction(
          mockCheckStatusResponseApproved(),
          mockedTransactionDetail
        )
        .catch(error => error);

      expect(result).toEqual(undefined);
      expect(mockTransactionUpdate).toBeCalledWith(
        mockedTransactionDetail.id,
        expect.objectContaining({ status: TransactionStatus.SUCCEED })
      );
    });

    it('should update transaction to FAILED when QRIS payment status is general failure', async () => {
      const mockTransactionUpdate = transactionRepository.update as jest.Mock;
      mockTransactionUpdate.mockReturnValue({
        ...mockedTransactionDetail,
        status: TransactionStatus.DECLINED
      });

      const result = await altoService
        .updateTransaction(
          mockCheckStatusResponseUnknown(),
          mockedTransactionDetail
        )
        .catch(error => error);

      expect(result).toEqual(undefined);
      expect(mockTransactionUpdate).toBeCalledWith(
        mockedTransactionDetail.id,
        expect.objectContaining({ status: TransactionStatus.DECLINED })
      );
    });

    it('should update transaction to SUCCESS and execute Refund when QRIS payment is Rejected', async () => {
      const mockTransactionUpdate = transactionRepository.update as jest.Mock;
      mockTransactionUpdate.mockReturnValue({
        ...mockedTransactionDetail,
        thirdPartyOutgoingId: '123456',
        status: TransactionStatus.SUCCEED
      });

      const spyOnShouldRefund = spyOn(
        altoUtils,
        'shouldAutoRefundQrisPayment'
      ).and.callThrough();
      const spyOnBuildRefundData = spyOn(
        altoUtils,
        'buildRefundRequestData'
      ).and.callThrough();
      const spyOnGetByThirdPartyOutgoingId = spyOn(
        transactionRepository,
        'getByThirdPartyOutgoingId'
      ).and.callThrough();

      const result = await altoService
        .updateTransaction(
          mockCheckStatusResponseRejected(),
          mockedTransactionDetail
        )
        .catch(error => error);

      expect(result).toEqual(undefined);

      // check update status is called
      expect(mockTransactionUpdate).toBeCalledWith(
        mockedTransactionDetail.id,
        expect.objectContaining({ status: TransactionStatus.SUCCEED })
      );

      // check refund is called with specific data
      expect(spyOnShouldRefund).toBeCalled();
      expect(spyOnShouldRefund.calls.mostRecent().returnValue).toEqual(true);

      expect(spyOnBuildRefundData).toBeCalled();
      const builtRefundData = spyOnBuildRefundData.calls.mostRecent()
        .returnValue;
      expect(builtRefundData).toEqual(
        expect.objectContaining({
          customer_reference_number: 'QRIS625d179c15622f5ca75931ac',
          invoice_no: '00000000990000000099',
          reference_number: '123456',
          currency_code: 'IDR',
          amount_refund: 2333
        })
      );

      expect(spyOnGetByThirdPartyOutgoingId).toBeCalled();
      expect(spyOnGetByThirdPartyOutgoingId.calls.mostRecent().args[0]).toEqual(
        builtRefundData.reference_number.toString()
      );
    });
  });

  describe('submit QRIS transaction', () => {
    const mockedTransactionFromDb: ITransactionModel = mockTransactionFromDb();
    const qrisTransaction = {
      ...mockedTransactionFromDb,
      coreBankingTransactionIds: ['mbu_001'],
      additionalPayload: {
        amount: 1000,
        fee: 0,
        merchant: {
          id: '12345',
          criteria: 'abcdefg',
          name: 'merchant001',
          city: 'Jakarta'
        },
        customer: {
          pan: 'abcd1234',
          name: 'customer123'
        }
      }
    };

    it('should retry three times when get 500 error', async () => {
      (altoRepository.submitTransaction as jest.Mock).mockImplementation(() => {
        throw new QrisError(
          'Test error!',
          TransferFailureReasonActor.ALTO,
          ERROR_CODE.ALTO_SERVER_ERROR,
          QrisTransactionStatusEnum.FAILED
        );
      });
      (getRetryCount as jest.Mock).mockReturnValue(3);
      let response;
      try {
        response = await helper.submitQrisTransaction(
          qrisTransaction,
          qrisTransaction.additionalPayload
        );
      } catch (err) {
        expect(err.errorCode).toBe(ERROR_CODE.ALTO_SERVER_ERROR);
        expect(err.actor).toBe(TransferFailureReasonActor.ALTO);
        expect(err.detail).toBe('Test error!');
      }

      expect(response).toBeUndefined();
      expect(altoRepository.submitTransaction).toBeCalledTimes(3);
    });

    it('should send failed error', async () => {
      (altoRepository.submitTransaction as jest.Mock).mockResolvedValue(
        mockErrorQrisPaymentResponse()
      );
      altoRepository.checkPaymentStatus = jest.fn().mockReturnValueOnce({
        currencyCode: '123',
        amount: 123,
        fee: 123,
        transactionResponseCode: AltoResponseCodeEnum.FAILED
      });
      (getRetryCount as jest.Mock).mockReturnValue(1);
      let response;
      try {
        response = await helper.submitQrisTransaction(
          qrisTransaction,
          qrisTransaction.additionalPayload
        );
      } catch (err) {
        expect(err.errorCode).toBe(ERROR_CODE.ERROR_FROM_ALTO);
        expect(err.actor).toBe(TransferFailureReasonActor.QRIS);
        expect(err.detail).toBe(
          `Qris payment is failed: ${QrisTransactionStatusEnum.FAILED}`
        );
      }

      expect(response).toBeDefined();
      expect(altoRepository.submitTransaction).toBeCalledTimes(1);
      expect(altoRepository.checkPaymentStatus).toBeCalledTimes(1);
    });
  });
});
