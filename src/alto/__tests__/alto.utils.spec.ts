/* eslint-disable @typescript-eslint/camelcase */
import { PaymentServiceTypeEnum } from '@dk/module-common';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../../transaction/transaction.enum';
import { ITransactionModel } from '../../transaction/transaction.model';
import { QrisTransactionStatusEnum } from '../alto.enum';
import { IQrisCheckStatusResponse, QrisError } from '../alto.type';
import altoUtil from '../alto.utils';
import altoUtils from '../alto.utils';
import {
  mockCheckStatusResponseApproved,
  mockCheckStatusResponseRejected
} from '../__mocks__/alto.data';

describe('check generateCustomerPan', () => {
  it('should add check digit and substring to MAX_LENGTH, when the given string is longer than MAX_LENGTH', async () => {
    const pan = 'ABCDEFGHIJKLMNOPQRST';
    expect(altoUtils.generateCustomerPan(pan)).toEqual('ABCDEFGHIJKLMNOPQR0');
  });

  it('should add check digit, when not longer than MAX_LENGTH', async () => {
    const pan = 'ABCDEFGHIJKLMNOP';
    expect(altoUtils.generateCustomerPan(pan)).toEqual('ABCDEFGHIJKLMNOP0');
  });
});

describe('process failed transaction on error', () => {
  it('should process when error object is null', async () => {
    expect(altoUtils.isQrisTimeoutError(null)).toEqual(false);
  });

  it('should process when error object is undefined', async () => {
    expect(altoUtils.isQrisTimeoutError(undefined)).toEqual(false);
  });

  it('should process when error code is not QRIS_TRANSACTION_TIMEOUT', async () => {
    expect(
      altoUtils.isQrisTimeoutError(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.ERROR_FROM_ALTO
        )
      )
    ).toEqual(false);
  });

  it('should skip when error code is QRIS_TRANSACTION_TIMEOUT', async () => {
    expect(
      altoUtils.isQrisTimeoutError(
        new QrisError(
          'Test error!',
          TransferFailureReasonActor.ALTO,
          ERROR_CODE.QRIS_TRANSACTION_TIMEOUT,
          QrisTransactionStatusEnum.SUSPECT
        )
      )
    ).toEqual(true);
  });
});

describe('should auto refund transaction', () => {
  it('should return TRUE for Rejected transaction', () => {
    const rejectedStatusResponse: IQrisCheckStatusResponse = mockCheckStatusResponseRejected();
    const result = altoUtils.shouldAutoRefundQrisPayment(
      rejectedStatusResponse
    );

    expect(result).toEqual(true);
  });

  it('should return FALSE for Approved transaction', () => {
    const approvedStatusResponse: IQrisCheckStatusResponse = mockCheckStatusResponseApproved();
    const result = altoUtils.shouldAutoRefundQrisPayment(
      approvedStatusResponse
    );

    expect(result).toEqual(false);
  });
});

describe('check buildCheckStatusResponse', () => {
  it('should return status = SUCCESS when Alto response status is SUCCESS and Acquirer status is SUCCESS', () => {
    const altoResponse = {
      command: 'qr-check-status',
      response_code: '001',
      response_text: 'Success',
      data: {
        transaction_response_code: '001',
        transaction_status: 'Success',
        customer_reference_number: '0013284239ZFX21',
        forwarding_customer_reference_number: '42234093892',
        invoice_no: '73283923681238390213'
      }
    };

    const builtResponse = altoUtil.buildCheckStatusResponse(altoResponse);

    expect(builtResponse.transactionStatus).toBe(
      QrisTransactionStatusEnum.SUCCESS
    );
    expect(builtResponse.transactionResponseCode).toBe('001');
  });

  it('should return status = SUSPEND when Alto response status is SUSPECT', () => {
    const altoResponse = {
      command: 'qr-check-status',
      response_code: '002',
      response_text: 'Suspect',
      data: {
        forwarding_customer_reference_number: '42234093892'
      }
    };

    const builtResponse = altoUtil.buildCheckStatusResponse(altoResponse);

    expect(builtResponse.transactionStatus).toBe(
      QrisTransactionStatusEnum.SUSPEND
    );
    expect(builtResponse.transactionResponseCode).toBe('002');
  });

  it('should return status = SUSPEND when Alto response status is SUCCESS and Acquirer status is SUSPECT', () => {
    const altoResponse = {
      command: 'qr-check-status',
      response_code: '001',
      response_text: 'Success',
      data: {
        transaction_response_code: '002',
        transaction_status: 'Suspect',
        customer_reference_number: '0013284239ZFX21',
        forwarding_customer_reference_number: '42234093892',
        invoice_no: '73283923681238390213'
      }
    };

    const builtResponse = altoUtil.buildCheckStatusResponse(altoResponse);

    expect(builtResponse.transactionStatus).toBe(
      QrisTransactionStatusEnum.SUSPEND
    );
    expect(builtResponse.transactionResponseCode).toBe('002');
  });

  it('should return status = SUSPEND when Alto response status is other than SUCCESS, SUSPECT, and REJECTED', () => {
    const altoResponse = {
      command: 'qr-check-status',
      response_code: '096',
      response_text: 'Requested data not found',
      data: {
        customer_reference_number: '0013284239ZFX21'
      }
    };

    const builtResponse = altoUtil.buildCheckStatusResponse(altoResponse);

    expect(builtResponse.transactionStatus).toBe(
      QrisTransactionStatusEnum.SUSPEND
    );
    expect(builtResponse.transactionResponseCode).toBe('096');
  });

  it('should return status = SUSPEND when Alto response status is SUCCESS and Acquirer status other than SUCCESS, SUSPECT, and REJECTED', () => {
    const altoResponse = {
      command: 'qr-check-status',
      response_code: '001',
      response_text: 'Success',
      data: {
        transaction_response_code: '003',
        transaction_status: 'Failed',
        customer_reference_number: '0013284239ZFX21',
        forwarding_customer_reference_number: '42234093892',
        invoice_no: '73283923681238390213'
      }
    };

    const builtResponse = altoUtil.buildCheckStatusResponse(altoResponse);

    expect(builtResponse.transactionStatus).toBe(
      QrisTransactionStatusEnum.SUSPEND
    );
    expect(builtResponse.transactionResponseCode).toBe('003');
  });

  it('should return status = FAILED when Alto response status is REJECTED', () => {
    const altoResponse = {
      command: 'qr-check-status',
      response_code: 'A00',
      response_text: 'Rejected',
      data: {
        customer_reference_number: '0013284239ZFX21'
      }
    };

    const builtResponse = altoUtil.buildCheckStatusResponse(altoResponse);

    expect(builtResponse.transactionStatus).toBe(
      QrisTransactionStatusEnum.FAILED
    );
    expect(builtResponse.transactionResponseCode).toBe('A00');
  });

  it('should return status = FAILED when Alto response status is SUCCESS and Acquirer status is REJECTED', () => {
    const altoResponse = {
      command: 'qr-check-status',
      response_code: '001',
      response_text: 'Success',
      data: {
        transaction_response_code: 'A00',
        transaction_status: 'Rejected',
        customer_reference_number: '0013284239ZFX21',
        forwarding_customer_reference_number: '42234093892',
        invoice_no: '73283923681238390213'
      }
    };

    const builtResponse = altoUtil.buildCheckStatusResponse(altoResponse);

    expect(builtResponse.transactionStatus).toBe(
      QrisTransactionStatusEnum.FAILED
    );
    expect(builtResponse.transactionResponseCode).toBe('A00');
  });
});

describe('check buildCreatePaymentResponse', () => {
  it('should return status = SUCCESS when Alto response status is SUCCESS', () => {
    const altoResponse = {
      command: 'qr-payment-credit',
      response_code: '001',
      response_text: 'Success',
      data: {
        customer_reference_number: '0013284239ZFX21',
        forwarding_customer_reference_number: '42234093892',
        invoice_no: '0123456789',
        currency_code: 'IDR',
        amount: 100500,
        fee: 500
      }
    };

    const builtResponse = altoUtil.buildCreatePaymentResponse(altoResponse);

    expect(builtResponse.transactionStatus).toBe(
      QrisTransactionStatusEnum.SUCCESS
    );
  });

  it('should return status = SUSPEND when Alto response status is SUSPECT', () => {
    const altoResponse = {
      command: 'qr-payment-credit',
      response_code: '002',
      response_text: 'Suspect',
      data: {
        customer_reference_number: '0013284239ZFX21',
        forwarding_customer_reference_number: '42234093892',
        invoice_no: '0123456789'
      }
    };

    const builtResponse = altoUtil.buildCreatePaymentResponse(altoResponse);

    expect(builtResponse.transactionStatus).toBe(
      QrisTransactionStatusEnum.SUSPEND
    );
  });

  it('should return status = SUSPEND when Alto response status is IDENTICAL_TRANSACTION_HAS_BEEN_SUCCESS', () => {
    const altoResponse = {
      command: 'qr-payment-credit',
      response_code: '030',
      response_text: 'Identical transaction has been success',
      data: {
        customer_reference_number: '0013284239ZFX21',
        forwarding_customer_reference_number: '42234093892',
        invoice_no: '0123456789'
      }
    };

    const builtResponse = altoUtil.buildCreatePaymentResponse(altoResponse);

    expect(builtResponse.transactionStatus).toBe(
      QrisTransactionStatusEnum.SHOULD_CHECK_STATUS
    );
  });

  it('should return status = SUSPEND when Alto response status is IDENTICAL_TRANSACTION_HAS_BEEN_FAILED', () => {
    const altoResponse = {
      command: 'qr-payment-credit',
      response_code: '032',
      response_text: 'Identical transaction has been failed',
      data: {
        customer_reference_number: '0013284239ZFX21',
        forwarding_customer_reference_number: '42234093892',
        invoice_no: '0123456789'
      }
    };

    const builtResponse = altoUtil.buildCreatePaymentResponse(altoResponse);

    expect(builtResponse.transactionStatus).toBe(
      QrisTransactionStatusEnum.SHOULD_CHECK_STATUS
    );
  });

  it('should return status = SUSPEND when Alto response status is IDENTICAL_TRANSACTION_STILL_ON_PROCESS', () => {
    const altoResponse = {
      command: 'qr-payment-credit',
      response_code: '002',
      response_text: 'Identical transaction still on process',
      data: {
        customer_reference_number: '0013284239ZFX21',
        forwarding_customer_reference_number: '42234093892',
        invoice_no: '0123456789'
      }
    };

    const builtResponse = altoUtil.buildCreatePaymentResponse(altoResponse);

    expect(builtResponse.transactionStatus).toBe(
      QrisTransactionStatusEnum.SUSPEND
    );
  });

  it('should return status = FAILED when Alto response status is REJECTED', () => {
    const altoResponse = {
      command: 'qr-payment-credit',
      response_code: 'A00',
      response_text: 'Rejected',
      data: {
        customer_reference_number: '0013284239ZFX21'
      }
    };

    const builtResponse = altoUtil.buildCreatePaymentResponse(altoResponse);

    expect(builtResponse.transactionStatus).toBe(
      QrisTransactionStatusEnum.FAILED
    );
  });

  it('should return status = FAILED when Alto response status is other than above', () => {
    const altoResponse = {
      command: 'qr-payment-credit',
      response_code: '003',
      response_text: 'Failed',
      data: {
        customer_reference_number: '0013284239ZFX21',
        forwarding_customer_reference_number: '42234093892',
        invoice_no: '73283923681238390213'
      }
    };

    const builtResponse = altoUtil.buildCreatePaymentResponse(altoResponse);

    expect(builtResponse.transactionStatus).toBe(
      QrisTransactionStatusEnum.FAILED
    );
  });
});

describe('check isValidResponse', () => {
  it('should return TRUE when response_code = 001', () => {
    const altoReponse = {
      command: 'qr-payment',
      response_code: '001',
      response_text: 'Success',
      data: {
        customer_reference_number: 'e3cbd65b-e665-4c00-9eb3-db7b2e480213',
        forwarding_customer_reference_number: '612795715087',
        invoice_no: '00000000000000000000',
        currency_code: 'IDR',
        amount: 2668681,
        fee: 0
      }
    };
    expect(altoUtil.isValidResponse(altoReponse)).toBe(true);
  });

  it('should return FALSE when response_code = 002', () => {
    const altoReponse = {
      command: 'qr-payment',
      response_code: '002',
      response_text: 'Suspect',
      data: {
        customer_reference_number: 'e3cbd65b-e665-4c00-9eb3-db7b2e480213',
        forwarding_customer_reference_number: '612795715087',
        invoice_no: '00000000000000000000',
        currency_code: 'IDR',
        amount: 2668681,
        fee: 0
      }
    };
    expect(altoUtil.isValidResponse(altoReponse)).toBe(false);
  });

  it('should return FALSE when response_code is not 001', () => {
    const altoReponse = {
      command: 'qr-payment',
      response_code: '003',
      response_text: 'Failed',
      data: {
        customer_reference_number: 'e3cbd65b-e665-4c00-9eb3-db7b2e480213',
        forwarding_customer_reference_number: '612795715087',
        invoice_no: '00000000000000000000',
        currency_code: 'IDR',
        amount: 2668681,
        fee: 0
      }
    };
    expect(altoUtil.isValidResponse(altoReponse)).toBe(false);
  });
});

describe('check build refund request data', () => {
  it('should return valid object when some field not exists', () => {
    const originalTransaction: ITransactionModel = {
      status: TransactionStatus.SUCCEED,
      referenceId: '123',
      externalId: '123',
      requireThirdPartyOutgoingId: false,
      transactionAmount: 5000,
      paymentServiceType: PaymentServiceTypeEnum.QRIS,
      id: '123'
    };
    const checkStatusResponse: IQrisCheckStatusResponse = {
      transactionStatus: QrisTransactionStatusEnum.SUCCESS,
      customerReferenceNumber: '12345678',
      forwardingCustomerReferenceNumber: 'abcdefgh',
      invoiceNo: 'abcd1234'
    };

    const refundRequestData = altoUtil.buildRefundRequestData(
      checkStatusResponse,
      originalTransaction
    );

    expect(refundRequestData.reference_number).toEqual('');
    expect(refundRequestData.currency_code).toEqual('IDR');
  });
});

describe('get exception error code', () => {
  it('should return timeout error when error message contains Timeout keyword', () => {
    const error = new Error('Error: timeout xxx');
    const errorCode = altoUtil.getExceptionErrorCode(error);
    expect(errorCode).toEqual(ERROR_CODE.QRIS_TRANSACTION_TIMEOUT);
  });

  it('should return alto generic error', () => {
    const error = new Error('Generic error');
    const errorCode = altoUtil.getExceptionErrorCode(error);
    expect(errorCode).toEqual(ERROR_CODE.ERROR_FROM_ALTO);
  });
});
