import { AltoResponseCodeEnum } from '../alto/alto.enum';

export const BASIC_PREFIX = 'Basic';

export const ORDER_TYPE = 'order';

export const TokenConstants = {
  TOKEN_EXPIRY_BUFFER: 600,
  DEFAULT_TTL: 3000
};

export const REDIS_TOKEN_KEY = 'ALTO_TOKEN';

export const PAYMENT_ENDPOINT_METHOD = 'POST';
export const STATUS_ENDPOINT_METHOD = 'POST';

export const AltoHttpHeaders = {
  CONTENT_TYPE: 'Content-Type',
  ACCEPT: 'Accept',
  X_ALTO_KEY: 'X-Alto-Key',
  X_ALTO_TIMESTAMP: 'X-Alto-Timestamp',
  X_ALTO_SIGNATURE: 'X-Alto-Signature'
};

export const DEFAULT_RESPONSE_DELAY = 0;
export const RESPONSE_DELAY = process.env.ALTO_RESPONSE_DELAY
  ? parseInt(process.env.ALTO_RESPONSE_DELAY)
  : DEFAULT_RESPONSE_DELAY;

export const DEFAULT_REQUEST_TIMEOUT = 45000;
export const REQUEST_TIMEOUT = process.env.ALTO_REQUEST_TIMEOUT
  ? parseInt(process.env.ALTO_REQUEST_TIMEOUT)
  : DEFAULT_REQUEST_TIMEOUT;

export const MAX_LENGTH_CUSTOMER_NAME = 30;

export const retryConfig = {
  networkErrorCodesToRetry: ['ENOENT', 'ECONNREFUSED'],
  maxRetriesOnError: 3,
  delay: 1000
};

export const ISSUER_NNS = process.env.ALTO_ISSUER_NNS || '';

export const CUSTOMER_PAN_MAX_LENGTH = 19;

export const TIMESTAMP_FORMAT = 'YYYY-MM-DD hh:mm:ss.SSS[Z]';

export const CURRENCY_CODE = 'IDR';

export const EMPTY = '';

export const MAX_CHECK_STATUS_RETRY_ATTEMPT = 3;

export const ALTO_REFERENCE_NUMBER_LENGTH = 10;

export const VALID_RESPONSES = [
  AltoResponseCodeEnum.SUCCESS,
  AltoResponseCodeEnum.IDENTICAL_TRANSACTION_HAS_BEEN_SUCCESS,
  AltoResponseCodeEnum.IDENTICAL_TRANSACTION_HAS_BEEN_FAILED
];

export const JAKARTA_TIMEZONE = 'WIB';

export const QRIS_PAYMENT_SERVICE_CODE = 'QRIS_PAYOUTS';
