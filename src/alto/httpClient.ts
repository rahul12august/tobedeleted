import { config } from '../config';
import { context } from '../context';
import logger from '../logger';
import { createHttpClient, HttpClient } from '@dk/module-httpclient';
import { REQUEST_TIMEOUT, retryConfig } from '../alto/alto.constant';

const { host } = config.get('qrisAlto');
const bearerValue = `Basic ${Buffer.from(
  process.env.ALTO_USERNAME + ':' + process.env.ALTO_PASSWORD
).toString('base64')}`;

const httpClient: HttpClient = createHttpClient({
  baseURL: host,
  timeout: REQUEST_TIMEOUT,
  context,
  logger,
  retryConfig
});

export const tokenHTTPClient: HttpClient = createHttpClient({
  baseURL: host,
  headers: {
    Authorization: bearerValue
  },
  timeout: REQUEST_TIMEOUT,
  context,
  logger,
  retryConfig
});

export default httpClient;
