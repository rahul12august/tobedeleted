import FormData from 'form-data';
import httpClient from './httpClient';
import { ISendEmailPayload } from '../report/report.type';
import { wrapLogs } from '../logger';

const sendEmailReport = async (
  sendEmailPayload: ISendEmailPayload
): Promise<void> => {
  const form = new FormData();
  for (const [key, value] of Object.entries(sendEmailPayload)) {
    form.append(key, value);
  }

  await httpClient.post(`/send-email-report`, form, {
    headers: form.getHeaders()
  });
};

const notificationRepository = {
  sendEmailReport
};

export default wrapLogs(notificationRepository);
