import httpClient from '../httpClient';
import notificationRepository from '../notification.repository';
import fs from 'fs';

describe('notification.repository', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('send email report', () => {
    it('should call to send email report', async () => {
      // Given
      httpClient.post = jest.fn().mockReturnValueOnce({});
      const stream = fs.createReadStream('../__mocks__/mockData.csv');
      const notifBody = {
        recipientEmails: 'test@gmail.com,second@gmail.com',
        notificationCode: 'code1',
        attachedFile: stream,
        params: '{"jobName":"job","transactionDate":"2020-10-10"}'
      };

      // When
      await notificationRepository.sendEmailReport(notifBody);

      // Then
      expect(httpClient.post).toBeCalledWith(
        `/send-email-report`,
        expect.any(Object),
        {
          headers: {
            'content-type': expect.stringContaining('multipart/form-data')
          }
        }
      );
    });
  });
});
