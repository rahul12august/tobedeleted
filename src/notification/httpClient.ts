import { config } from '../config';
import { context } from '../context';
import logger from '../logger';
import { HttpClient } from '@dk/module-httpclient';
import { Http } from '@dk/module-common';
import { retryConfig } from '../common/constant';

const MS_NOTIFICATION_URL = config.get('ms').notification;

const httpClient: HttpClient = Http.createHttpClientForward({
  baseURL: MS_NOTIFICATION_URL,
  context,
  logger,
  retryConfig
});

export default httpClient;
