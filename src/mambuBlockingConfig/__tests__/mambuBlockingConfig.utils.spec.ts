import configurationRepository from '../../configuration/configuration.repository';
import { getMambuBlockingDay } from '../mambuBlockingConfig.utils';
import { getHolidaysWithDateRangeDataWithDate } from '../../configuration/__mocks__/configuration.data';

jest.mock('../../configuration/configuration.repository');

describe('mambuBlockingConfigUtils', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  describe('getMambuBlockingDay', () => {
    it('should return 3 days when no holidays in next 30 days', async () => {
      //Given
      const today = new Date('2020-02-01T00:00:00.000Z');
      (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValueOnce(
        null
      );

      //When
      const result = await getMambuBlockingDay(today);

      //Then
      expect(result).toEqual(3);
      expect(configurationRepository.getHolidaysByDateRange).toHaveBeenCalled();
    });

    it('should return 6 days when holidays occur in next 2 days', async () => {
      //Given
      const today = new Date('2020-02-01T00:00:00.000Z');
      const tomorrow = new Date('2020-02-02T00:00:00.000Z');
      (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValueOnce(
        getHolidaysWithDateRangeDataWithDate(tomorrow)
      );

      //When
      const result = await getMambuBlockingDay(today);

      //Then
      expect(result).toEqual(6);
      expect(configurationRepository.getHolidaysByDateRange).toHaveBeenCalled();
    });

    it('should return 3 days when holidays occur after three days', async () => {
      //Given
      const today = new Date('2020-02-01T00:00:00.000Z');
      const tomorrow = new Date('2020-02-05T00:00:00.000Z');
      (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValueOnce(
        getHolidaysWithDateRangeDataWithDate(tomorrow)
      );

      //When
      const result = await getMambuBlockingDay(today);

      //Then
      expect(result).toEqual(3);
      expect(configurationRepository.getHolidaysByDateRange).toHaveBeenCalled();
    });
  });
});
