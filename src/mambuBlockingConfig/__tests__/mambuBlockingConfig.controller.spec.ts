import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import ResponseWrapper from '../../plugins/responseWrapper';
import errorHandler from '../../common/handleValidationErrors';
import mambuBlockingConfigController from '../mambuBlockingConfig.controller';
import mambuBlockingConfigService from '../mambuBlockingConfig.service';

jest.mock('../mambuBlockingConfig.service');

describe('mambuBlockingConfig Controller', () => {
  let server: hapi.Server;
  beforeAll(async () => {
    server = new hapi.Server({
      routes: {
        validate: {
          options: {
            abortEarly: false
          },
          failAction: errorHandler
        }
      }
    });
    server.register([ResponseWrapper]);
    server.route(mambuBlockingConfigController);
  });

  describe('update mambu blocking days', () => {
    it('should respond 202', async () => {
      // Given
      const options = {
        method: Http.Method.PUT,
        url: '/private/jobs-execution/update-mambu-blocking-days'
      };

      (mambuBlockingConfigService.updateMambuBlockingDays as jest.Mock).mockResolvedValueOnce(
        null
      );
      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.NO_CONTENT);
    });
  });
});
