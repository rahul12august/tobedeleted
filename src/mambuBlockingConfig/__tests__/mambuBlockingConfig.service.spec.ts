import configurationRepository from '../../configuration/configuration.repository';
import {
  getMambuBlockingDays,
  getHolidaysWithDateRangeDataWithDate
} from '../../configuration/__mocks__/configuration.data';
import mambuBlockingConfigService from '../mambuBlockingConfig.service';
import { ERROR_CODE } from '../../common/errors';
import { mambuBlockingCounterCode } from '../mambuBlockingConfig.constant';

jest.mock('../../configuration/configuration.repository');

describe('mambuBlockingConfig', () => {
  describe('update mambu blocking days', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    it('should update mambu blocking days with 4 when Holiday list is empty', async () => {
      //Given
      const today = new Date('2020-02-01T00:00:00.000Z');
      const mambuBlockingDays = getMambuBlockingDays();
      (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValueOnce(
        []
      );
      (configurationRepository.getMambuBlockingCodeByDays as jest.Mock).mockResolvedValueOnce(
        mambuBlockingDays
      );
      (configurationRepository.updateCounter as jest.Mock).mockResolvedValueOnce(
        null
      );
      //When
      await mambuBlockingConfigService.updateMambuBlockingDays(today);
      //Then
      expect(
        configurationRepository.getHolidaysByDateRange
      ).toHaveBeenCalledTimes(1);
      expect(
        configurationRepository.getMambuBlockingCodeByDays
      ).toHaveBeenCalledWith(3);
      expect(configurationRepository.updateCounter).toHaveBeenCalledWith({
        code: mambuBlockingCounterCode,
        value: parseInt(mambuBlockingDays.code)
      });
    });

    it('should throw error when mambu blocking days is null', async () => {
      //Given
      const today = new Date('2020-02-01T00:00:00.000Z');
      (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValueOnce(
        []
      );
      (configurationRepository.getMambuBlockingCodeByDays as jest.Mock).mockResolvedValueOnce(
        null
      );

      //When
      const error = await mambuBlockingConfigService
        .updateMambuBlockingDays(today)
        .catch(err => err);

      //Then
      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MAMBU_BLOCKING_DAY);
      expect(
        configurationRepository.getHolidaysByDateRange
      ).toHaveBeenCalledTimes(1);
      expect(
        configurationRepository.getMambuBlockingCodeByDays
      ).toHaveBeenCalledWith(3);
      expect(configurationRepository.updateCounter).not.toBeCalled();
    });

    it('should update 6 mambublocking days code when holidays in next two days', async () => {
      //Given
      const today = new Date('2020-02-01T00:00:00.000Z');
      const tomorrow = new Date('2020-02-02T00:00:00.000Z');
      const mambuBlockingDays = getMambuBlockingDays();
      (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValueOnce(
        getHolidaysWithDateRangeDataWithDate(tomorrow)
      );
      (configurationRepository.getMambuBlockingCodeByDays as jest.Mock).mockResolvedValueOnce(
        mambuBlockingDays
      );

      //When
      await mambuBlockingConfigService.updateMambuBlockingDays(today);

      //Then
      expect(
        configurationRepository.getHolidaysByDateRange
      ).toHaveBeenCalledTimes(1);
      expect(
        configurationRepository.getMambuBlockingCodeByDays
      ).toHaveBeenCalledWith(6);
      expect(configurationRepository.updateCounter).toHaveBeenCalledWith({
        code: mambuBlockingCounterCode,
        value: parseInt(mambuBlockingDays.code)
      });
    });
  });
});
