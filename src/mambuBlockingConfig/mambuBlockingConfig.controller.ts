import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import mambuBlockingConfigService from './mambuBlockingConfig.service';

const executeAutoUpdateMambuBlockingDays: hapi.ServerRoute = {
  method: Http.Method.PUT,
  path: '/private/jobs-execution/update-mambu-blocking-days',
  options: {
    description: 'Job Execution mambu blocking days',
    notes: 'Private Api to Auto update mambu blocking days',
    tags: ['api', 'mambu', 'jobs', 'private'],
    auth: false,
    handler: async (_: hapi.Request, hapiResponse: hapi.ResponseToolkit) => {
      mambuBlockingConfigService.updateMambuBlockingDays(new Date());
      return hapiResponse.response().code(Http.StatusCode.NO_CONTENT);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.NO_CONTENT]: {
            description: 'No Content'
          }
        }
      }
    }
  }
};

const mambuBlockingConfigController: hapi.ServerRoute[] = [
  executeAutoUpdateMambuBlockingDays
];

export default mambuBlockingConfigController;
