import configurationRepository from '../configuration/configuration.repository';
import { TransferAppError } from '../errors/AppError';
import logger, { wrapLogs } from '../logger';
import { getMambuBlockingDay } from './mambuBlockingConfig.utils';
import { mambuBlockingCounterCode } from './mambuBlockingConfig.constant';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';
import { ERROR_CODE } from '../common/errors';

const updateMambuBlockingDays = async (date: Date): Promise<void> => {
  const noOfDays = await getMambuBlockingDay(date);
  const mambuBlockingDays = await configurationRepository.getMambuBlockingCodeByDays(
    noOfDays
  );

  if (!mambuBlockingDays) {
    const detail = `Mambu blocking days code for number of days: ${noOfDays} not found!`;
    logger.error(`updateMambuBlockingConfig: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.JAGO,
      ERROR_CODE.INVALID_MAMBU_BLOCKING_DAY
    );
  }

  logger.info(
    `updateMambuBlockingConfig: Updating counter code ${mambuBlockingCounterCode} with value: ${mambuBlockingDays.code}`
  );
  configurationRepository.updateCounter({
    code: mambuBlockingCounterCode,
    value: parseInt(mambuBlockingDays.code)
  });
};

const mambuBlockingConfigService = wrapLogs({
  updateMambuBlockingDays
});

export default mambuBlockingConfigService;
