import { HOLIDAY_DATE_RANGE } from '../billPayment/billPayment.constant';
import configurationRepository from '../configuration/configuration.repository';
import { IHoliday } from '../configuration/configuration.type';
import { MINIMUM_MAMBU_BLOCKING_DAYS } from './mambuBlockingConfig.constant';
import { isEmpty, isUndefined } from 'lodash';
import moment from 'moment';

const addDays = (date: Date, days: number): Date => {
  const result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
};

const isDateBetween = (date: Date, fromDate: Date, toDate: Date): boolean => {
  const day = moment(date).format('YYYY-MM-DD');
  const fromDay = moment(fromDate).format('YYYY-MM-DD');
  const toDay = moment(toDate).format('YYYY-MM-DD');
  return moment(day).isBetween(fromDay, toDay, undefined, '[]');
};

const getHolidayFromDate = (
  date: Date,
  holidayList: IHoliday[]
): IHoliday | undefined => {
  for (let holiday of holidayList) {
    if (isDateBetween(date, holiday.fromDate, holiday.toDate)) {
      return holiday;
    }
  }
  return;
};

export const getMambuBlockingDay = async (
  currentDate: Date
): Promise<number> => {
  let lastDate = currentDate;
  const holidayList: IHoliday[] = await configurationRepository.getHolidaysByDateRange(
    currentDate,
    addDays(lastDate, HOLIDAY_DATE_RANGE)
  );

  if (!isEmpty(holidayList)) {
    let workingDays = 0;
    let holiday;
    while (workingDays < MINIMUM_MAMBU_BLOCKING_DAYS) {
      lastDate = addDays(holiday ? holiday.toDate : lastDate, 1);
      holiday = getHolidayFromDate(lastDate, holidayList);
      if (isUndefined(holiday)) {
        workingDays++;
      }
    }

    return moment(lastDate).diff(moment(currentDate), 'days');
  }
  return MINIMUM_MAMBU_BLOCKING_DAYS;
};
