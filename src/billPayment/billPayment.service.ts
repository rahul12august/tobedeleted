import paymentStatusPollingService from './paymentStatusPolling/paymentStatusPolling.service';
import logger, { wrapLogs } from '../logger';
import billingAggregatorService from '../billingAggregator/billingAggregator.service';
import { IPaymentStatusPolling } from './paymentStatusPolling/paymentStatusPolling.type';
import { isEmpty } from 'lodash';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { BillPaymentTransactionStatusEnum } from './billPaymentTransaction/billPaymentTransaction.enum';
import goBillsService from '../billingAggregator/goBills/goBills.service';
import { IBillPaymentStatus } from '../billingAggregator/goBills/goBills.type';
import { emptyString } from '../decisionEngine/decisionEngine.constant';
import billPaymentTxnService from './billPaymentTransaction/billPaymentTransaction.service';
import transactionService from '../transaction/transaction.service';
import { updateRequestIdInContext } from '../common/contextHandler';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../transaction/transaction.enum';
import { MAP_TXN_STATUS_TO_GOBILL } from '../transaction/transaction.constant';

const updatePendingTransactions = async (): Promise<void> => {
  const startDate = new Date();
  const pendingStatusRecords = await paymentStatusPollingService.getPendingRecords(
    startDate
  );
  if (isEmpty(pendingStatusRecords)) {
    logger.info(`Payment Statuses for startDate ${startDate} not found.`);
  }
  logger.warn(
    `Bill Payment Statuses for startDate ${startDate} with count of pending records : ${pendingStatusRecords.length}`
  );
  const paymentStatusPromises = pendingStatusRecords.map(
    async (paymentStatus: IPaymentStatusPolling) => {
      await updateRequestIdInContext(paymentStatus.orderId);
      return billingAggregatorService.updateBillPaymentStatus(
        paymentStatus.orderId
      );
    }
  );
  await Promise.all(paymentStatusPromises);
};

const txnManualAdjustment = async (
  txnId: string,
  reqStatus: TransactionStatus,
  updateBillDetails: boolean = true
): Promise<void> => {
  logger.info(`Manual adjustment started for transaction id ${txnId} `);
  const status = MAP_TXN_STATUS_TO_GOBILL[
    reqStatus
  ] as BillPaymentTransactionStatusEnum;
  const billPaymentTxn = await billPaymentTxnService.getByTxnId(txnId);
  if (!billPaymentTxn) {
    const detail = `Matching txn was not found for txnId: ${txnId}!`;
    logger.error(`txnManualAdjustment: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.MS_BILL_PAYMENT,
      ERROR_CODE.TRANSACTION_NOT_FOUND
    );
  }

  const res: IBillPaymentStatus = {
    orderId: billPaymentTxn.orderId,
    status,
    voucherCode: emptyString,
    amount: billPaymentTxn.amount.toString(),
    trxDate: emptyString,
    skuTag: emptyString,
    refNumber: billPaymentTxn.referenceId
  };
  try {
    await goBillsService.updateTransactionWithResult(
      res,
      billPaymentTxn.orderId,
      updateBillDetails
    );
  } catch (error) {
    logger.error(
      `txnManualAdjustment: error while manually adjusting transaction: ${txnId}, status: ${error &&
        error.status}, message: ${error && error.message}`
    );
    throw error;
  } finally {
    const transactionUpdate = updateBillDetails
      ? { manuallyAdjusted: true }
      : { manuallyAdjusted: true, status: reqStatus };
    await transactionService.findByIdAndUpdate(
      billPaymentTxn.transactionId,
      transactionUpdate
    );
    if (updateBillDetails)
      await paymentStatusPollingService.updatePaymentStatusPollingConfig(
        billPaymentTxn.transactionId,
        status
      );
  }
};

const billPaymentService = {
  updatePendingTransactions,
  txnManualAdjustment
};

export default wrapLogs(billPaymentService);
