import Joi from '@hapi/joi';
import { BillPaymentTransactionStatusEnum } from './billPaymentTransaction/billPaymentTransaction.enum';

export const manualAdjustmentPayloadValidator = Joi.object({
  transactionId: Joi.string().required(),
  status: Joi.string()
    .required()
    .valid(
      BillPaymentTransactionStatusEnum.FAILURE,
      BillPaymentTransactionStatusEnum.SUCCESS
    )
}).label('gobillsTransactionManualAdjustment');
