import httpClient from '../httpClient';
import billDetailRepository from '../billDetail.repository';
import {
  adminFeeResponse,
  billDetailsResponse
} from '../__mocks__/billDetail.data';
import {
  ADMIN_FEE_PATH,
  GET_BILL_DETAIL_URL,
  PATCH_ORDER_DETAILS_URL
} from '../billDetail.constant';
import { TransferAppError } from '../../../errors/AppError';
import { ERROR_CODE } from '../../../common/errors';
import { BillInfoMappedRowType } from '../billDetails.type';
import { getGoBillsData } from '../../../billingAggregator/goBills/__mocks__/goBills.data';
import { TransferFailureReasonActor } from '../../../transaction/transaction.enum';

jest.mock('../httpClient');

describe('Go bills repository', () => {
  beforeEach(() => {
    jest.resetAllMocks();
    expect.hasAssertions();
  });

  describe('getAdminFee', () => {
    it('should call billPayments and return data sucessfully', async () => {
      // Given
      (httpClient.get as jest.Mock).mockResolvedValueOnce(adminFeeResponse());
      const externalId = 'dummy123';

      // When
      const result = await billDetailRepository.getAdminFee(externalId);

      // Then
      expect(result).toEqual(
        parseFloat(adminFeeResponse().data.data['admin-fee'])
      );
      expect(typeof result).toEqual('number');
      expect(httpClient.get).toHaveBeenCalledWith(
        `${GET_BILL_DETAIL_URL}/${externalId}/${ADMIN_FEE_PATH}`
      );
    });

    it('should throw unexpected error in case of failure', async () => {
      // Given
      const externalId = 'dummy123';
      (httpClient.get as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.UNEXPECTED_ERROR
        )
      );

      // When
      await billDetailRepository.getAdminFee(externalId).catch(err => {
        expect(err.detail).toEqual('Test error!');
        expect(err.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
        expect(err.errorCode).toEqual(ERROR_CODE.UNEXPECTED_ERROR);
        expect(httpClient.get).toHaveBeenCalledWith(
          `${GET_BILL_DETAIL_URL}/${externalId}/${ADMIN_FEE_PATH}`
        );
      });
    });

    it('should throw unexpected error in case of admin fee not present in bill details', async () => {
      // Given
      const externalId = 'dummy123';
      (httpClient.get as jest.Mock).mockResolvedValueOnce({
        data: {
          data: {}
        }
      });

      // When
      await billDetailRepository.getAdminFee(externalId).catch(err => {
        // Then
        expect(err.errorCode).toBe(ERROR_CODE.ADMIN_FEE_NOT_FOUND);
        expect(httpClient.get).toHaveBeenCalledWith(
          `${GET_BILL_DETAIL_URL}/${externalId}/${ADMIN_FEE_PATH}`
        );
      });
    });
  });
  describe('getBillDetails', () => {
    it('should call billPayments getBillDetails and return data sucessfully', async () => {
      // Given
      (httpClient.get as jest.Mock).mockResolvedValueOnce(
        billDetailsResponse()
      );
      const externalId = 'dummy123';

      // When
      const result = await billDetailRepository.getBillDetails(externalId);

      // Then
      expect(result.customerId).toEqual('123456');
      expect(result.billerName).toEqual('PLN');
      expect(result.transactionId).toEqual('TRAN01');
      expect(httpClient.get).toHaveBeenCalledWith(
        `${GET_BILL_DETAIL_URL}/${externalId}?mapped_row_type=${BillInfoMappedRowType.ALL}`
      );
    });

    it('should throw unexpected error in case of failure', async () => {
      // Given
      const externalId = 'dummy123';
      (httpClient.get as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.UNEXPECTED_ERROR
        )
      );

      // When
      await billDetailRepository.getBillDetails(externalId).catch(err => {
        expect(err.detail).toEqual('Test error!');
        expect(err.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
        expect(err.errorCode).toEqual(ERROR_CODE.UNEXPECTED_ERROR);
        expect(httpClient.get).toHaveBeenCalledWith(
          `${GET_BILL_DETAIL_URL}/${externalId}?mapped_row_type=${BillInfoMappedRowType.ALL}`
        );
      });
    });
  });

  describe('updateOrderDetails', () => {
    it('should update order details successfully', async () => {
      // Given
      const paymentResponse = getGoBillsData();
      const body = {
        inquiryId: 'referenceId',
        order: paymentResponse.orderDetail
      };
      (httpClient.patch as jest.Mock).mockImplementationOnce(() =>
        Promise.resolve({
          data: {
            data: paymentResponse.orderDetail
          }
        })
      );

      // When
      await billDetailRepository.updateOrderDetails(body);

      // Then
      expect(httpClient.patch).toHaveBeenCalledWith(
        PATCH_ORDER_DETAILS_URL,
        body
      );
    });

    it('should throw error while update order details', async () => {
      // Given
      const paymentResponse = getGoBillsData();
      const body = {
        inquiryId: 'referenceId',
        order: paymentResponse.orderDetail
      };
      (httpClient.patch as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.UNEXPECTED_ERROR
        )
      );

      // When
      const error = await billDetailRepository
        .updateOrderDetails(body)
        .catch(err => err);

      // Then
      expect(error.detail).toEqual('Test error!');
      expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      expect(error.errorCode).toEqual(ERROR_CODE.UNEXPECTED_ERROR);
      expect(httpClient.patch).toHaveBeenCalledWith(
        PATCH_ORDER_DETAILS_URL,
        body
      );
    });
  });
});
