import {
  feeRuleTemplatePayloadForThirdPartyFee,
  getBillDetails
} from '../__mocks__/billDetail.data';
import billDetailRepository from '../billDetail.repository';
import billDetailService from '../billDetail.service';
import { TransferAppError } from '../../../errors/AppError';
import { ERROR_CODE } from '../../../common/errors';
import { getGoBillsData } from '../../../billingAggregator/goBills/__mocks__/goBills.data';
import { TransferFailureReasonActor } from '../../../transaction/transaction.enum';

jest.mock('../billDetail.repository');

describe('billDetailService', () => {
  beforeEach(() => {
    jest.resetAllMocks();
    expect.hasAssertions();
  });

  describe('getAdminFee', () => {
    it('should return correct data when all input details are correct', async () => {
      // Given
      const thirdPartyFeeData = feeRuleTemplatePayloadForThirdPartyFee();
      (billDetailRepository.getAdminFee as jest.Mock).mockResolvedValueOnce(
        1000
      );
      // When
      const feeAmount = await billDetailService.getAdminFee(
        thirdPartyFeeData.externalId
      );

      // Then
      expect(feeAmount).toEqual(1000);
      expect(billDetailRepository.getAdminFee).toHaveBeenCalledWith(
        thirdPartyFeeData.externalId
      );
    });

    it('should return internal server error goBill api fails', async () => {
      // given
      const thirdPartyFeeData = feeRuleTemplatePayloadForThirdPartyFee();
      const externalId: string = thirdPartyFeeData.externalId;

      (billDetailRepository.getAdminFee as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.UNEXPECTED_ERROR
        )
      );

      // when
      const error = await billDetailService
        .getAdminFee(externalId)
        .catch(error => error);

      // then
      expect(error.detail).toEqual('Test error!');
      expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      expect(error.errorCode).toEqual(ERROR_CODE.UNEXPECTED_ERROR);
    });
  });
  describe('getBillDetails', () => {
    it('should return correct data when all input details are correct', async () => {
      // Given
      const billDetailData = getBillDetails();
      (billDetailRepository.getBillDetails as jest.Mock).mockResolvedValueOnce(
        billDetailData
      );
      // When
      const billDetails = await billDetailService.getBillDetails('referenceId');

      // Then
      expect(billDetails.customerId).toEqual('123456');
      expect(billDetails.billerName).toEqual('PLN');
      expect(billDetails.transactionId).toEqual('TRAN01');
      expect(billDetailRepository.getBillDetails).toHaveBeenCalledWith(
        'referenceId'
      );
    });

    it('should return internal server error goBill api fails', async () => {
      // given
      const thirdPartyFeeData = feeRuleTemplatePayloadForThirdPartyFee();
      const externalId: string = thirdPartyFeeData.externalId;

      (billDetailRepository.getBillDetails as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.UNEXPECTED_ERROR
        )
      );

      // when
      const error = await billDetailService
        .getBillDetails(externalId)
        .catch(error => error);

      // then
      expect(error.detail).toEqual('Test error!');
      expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      expect(error.errorCode).toEqual(ERROR_CODE.UNEXPECTED_ERROR);
    });

    it('should throw error in case of bill details not present', async () => {
      // Given
      (billDetailRepository.getBillDetails as jest.Mock).mockResolvedValueOnce(
        null
      );
      // When
      await billDetailService.getBillDetails('referenceId').catch(err => {
        // Then
        expect(err.errorCode).toBe(ERROR_CODE.BILL_DETAILS_NOT_FOUND);
        expect(billDetailRepository.getBillDetails).toHaveBeenCalledWith(
          'referenceId'
        );
      });
    });
  });

  describe('updateOrderDetails', () => {
    it('should update order details successfully', async () => {
      // Given
      const paymentResponse = getGoBillsData();
      const body = {
        inquiryId: 'referenceId',
        order: paymentResponse
      };
      (billDetailRepository.updateOrderDetails as jest.Mock).mockResolvedValueOnce(
        null
      );
      // When
      await billDetailService.updateOrderDetails(
        'referenceId',
        paymentResponse
      );

      // Then
      expect(billDetailRepository.updateOrderDetails).toHaveBeenCalledWith(
        body
      );
    });

    it('should throw error while update order details', async () => {
      // given
      const paymentResponse = getGoBillsData();
      const body = {
        inquiryId: 'referenceId',
        order: paymentResponse
      };
      (billDetailRepository.updateOrderDetails as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.UNEXPECTED_ERROR
        )
      );
      // when
      const error = await billDetailService
        .updateOrderDetails('referenceId', paymentResponse)
        .catch(err => err);

      // then
      expect(error.detail).toEqual('Test error!');
      expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      expect(error.errorCode).toEqual(ERROR_CODE.UNEXPECTED_ERROR);
      expect(billDetailRepository.updateOrderDetails).toHaveBeenCalledWith(
        body
      );
    });

    it('should update order details successfully without orderDetails', async () => {
      // Given
      (billDetailRepository.updateOrderDetails as jest.Mock).mockResolvedValueOnce(
        null
      );
      // When
      await billDetailService.updateOrderDetails('referenceId', undefined);

      // Then
      expect(billDetailRepository.updateOrderDetails).not.toHaveBeenCalled();
    });
  });
});
