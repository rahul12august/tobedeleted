import httpClient from './httpClient';
import logger, { wrapLogs } from '../../logger';
import { get } from 'lodash';
import {
  ADMIN_FEE_PATH,
  GET_BILL_DETAIL_URL,
  PATCH_ORDER_DETAILS_URL
} from './billDetail.constant';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import {
  BillInfoMappedRowType,
  IBillDetailsResponse,
  IOrderDetails
} from './billDetails.type';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

const getAdminFee = async (referenceId: string): Promise<number> => {
  logger.info(
    `Bill Payment : Fetching admin fee for referenceId : ${referenceId}`
  );
  const response = await httpClient.get(
    `${GET_BILL_DETAIL_URL}/${referenceId}/${ADMIN_FEE_PATH}`
  );
  const adminFee = get(response, ['data', 'data', ADMIN_FEE_PATH], null);
  if (!adminFee) {
    const detail = `Admin fee is not present in bill details for : ${referenceId}!`;
    logger.error(`Bill Payment: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.JAGO,
      ERROR_CODE.ADMIN_FEE_NOT_FOUND
    );
  }
  return parseFloat(adminFee);
};

const getBillDetails = async (
  referenceId: string
): Promise<IBillDetailsResponse> => {
  logger.info(
    `Bill Payment : Fetching bill details for referenceId : ${referenceId}`
  );
  const response = await httpClient.get(
    `${GET_BILL_DETAIL_URL}/${referenceId}?mapped_row_type=${BillInfoMappedRowType.ALL}`
  );
  return get(response, ['data', 'data'], null);
};

const updateOrderDetails = async (order: IOrderDetails) =>
  httpClient.patch(`${PATCH_ORDER_DETAILS_URL}`, order);

const billDetailRepository = {
  getAdminFee,
  getBillDetails,
  updateOrderDetails
};

export default wrapLogs(billDetailRepository);
