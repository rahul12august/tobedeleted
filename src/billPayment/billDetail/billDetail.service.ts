import billDetailRepository from './billDetail.repository';
import logger, { wrapLogs } from '../../logger';
import { IBillDetailsResponse } from './billDetails.type';
import { ERROR_CODE } from '../../common/errors';
import { isEmpty } from 'lodash';
import { TransferAppError } from '../../errors/AppError';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

const getAdminFee = async (referenceId: string): Promise<number> =>
  billDetailRepository.getAdminFee(referenceId);

const getBillDetails = async (
  referenceId: string
): Promise<IBillDetailsResponse> => {
  const billDetails = await billDetailRepository.getBillDetails(referenceId);
  if (!billDetails) {
    const detail = `Bill detail is not present for: ${referenceId}!`;
    logger.error(`Bill Payment: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.MS_BILL_PAYMENT,
      ERROR_CODE.BILL_DETAILS_NOT_FOUND
    );
  }
  return billDetails;
};

const updateOrderDetails = async (referenceId: string, order: any) => {
  try {
    if (!isEmpty(order)) {
      const body = {
        inquiryId: referenceId,
        order: order
      };
      logger.info(
        `Update Order Details : Fetching order details for referenceId : ${referenceId} with orderDetails: ${JSON.stringify(
          body
        )}`
      );
      await billDetailRepository.updateOrderDetails(body);
    }
  } catch (err) {
    logger.error(
      `Error: While updating order details for referenceId: ${referenceId} with payload: ${JSON.stringify(
        order
      )}`
    );
    throw err;
  }
};

const billDetailService = {
  getAdminFee,
  getBillDetails,
  updateOrderDetails
};

export default wrapLogs(billDetailService);
