const GET_BILL_DETAIL_URL = '/private/bill-detail';
const PATCH_ORDER_DETAILS_URL = '/private/order-details';
const ADMIN_FEE_PATH = 'admin-fee';

export { GET_BILL_DETAIL_URL, PATCH_ORDER_DETAILS_URL, ADMIN_FEE_PATH };
