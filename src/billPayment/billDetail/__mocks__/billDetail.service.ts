export default {
  getAdminFee: jest.fn(),
  getBillDetails: jest.fn(),
  updateOrderDetails: jest.fn()
};
