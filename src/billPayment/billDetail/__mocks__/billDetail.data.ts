import { IBillDetailsResponse, IBillKey } from '../billDetails.type';
import { BillKeyType, BankChannelEnum } from '@dk/module-common';
import { IFeeRuleTemplatePayload } from '../../../fee/fee.type';

const getPrimaryBillKey = (): IBillKey => ({
  key: BillKeyType.PHONE_NUMBER,
  value: '1234567890'
});

export const getBillDetails = (): IBillDetailsResponse => ({
  customerId: '123456',
  primaryBillKey: getPrimaryBillKey(),
  transactionId: 'TRAN01',
  billerName: 'PLN'
});

export const billDetailsResponse = () => ({
  data: {
    data: getBillDetails()
  }
});

export const feeRuleTemplatePayloadForThirdPartyFee = (): IFeeRuleTemplatePayload => ({
  basicFee: {
    thirdPartyFee: true,
    code: 'BC144',
    customerTc: 'dummy',
    customerTcInfo: {
      channel: 'dummy'
    },
    subsidiary: false
  },
  transactionAmount: 1000,
  externalId: 'dummy123',
  beneficiaryBankCodeChannel: BankChannelEnum.GOBILLS
});

export const adminFeeResponse = () => ({
  data: {
    data: {
      'admin-fee': '100'
    }
  }
});
