import { createHttpClient, HttpClient } from '@dk/module-httpclient';

import { config } from '../../config';
import logger from '../../logger';
import { context } from '../../context';
import { retryConfig } from '../../common/constant';

const URL = config.get('ms').billPayment;
const httpClient: HttpClient = createHttpClient({
  baseURL: URL,
  context,
  logger,
  retryConfig
});

logger.info(`Setting up http-client with bill payments on address : ${URL}`);

export default httpClient;
