import { BillKeyType } from '@dk/module-common';

interface IBillKey {
  key: BillKeyType;
  value: string;
  primary?: boolean;
}

export interface IBillRowMapping {
  rowType: string;
  outputName: string;
  value: string | number;
}

interface IInquiryParams {
  skuCode: string;
  [K: string]: string;
}

interface IBillDetailsResponse {
  customerId: string;
  primaryBillKey: IBillKey;
  transactionId?: string;
  billerName: string;
  mappedBillInformation?: IBillRowMapping[];
  inquiryParams?: IInquiryParams;
}

interface IOrderDetails {
  inquiryId: string;
  order: any;
}

enum BillInfoMappedRowType {
  VISIBLE = 'visible',
  ALL = 'all'
}

export { IBillDetailsResponse, IBillKey, BillInfoMappedRowType, IOrderDetails };
