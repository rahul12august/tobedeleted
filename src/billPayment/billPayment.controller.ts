import hapi from '@hapi/hapi';
import { AdminPortalUserPermissionCode, Http } from '@dk/module-common';
import billPaymentService from './billPayment.service';
import { IManualAdjustmentRequest } from './billPayment.type';
import { manualAdjustmentPayloadValidator } from './billPayment.validator';
import { MAP_GOBILL_STATUS_TO_TXN } from '../transaction/transaction.constant';
import { TransactionStatus } from '../transaction/transaction.enum';

const updateBillPaymentStatus: hapi.ServerRoute = {
  method: Http.Method.PUT,
  path: '/private/jobs/bill-payment/status-update',
  options: {
    description:
      'Private api is used by the scheduler to trigger update for all pending transaction from gobills',
    notes: 'Trigger update for all pending gobills transactions.',
    tags: ['private'],
    auth: false,
    handler: async (
      _hapiRequest: hapi.Request,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      await billPaymentService.updatePendingTransactions();
      return hapiResponse.response().code(Http.StatusCode.ACCEPTED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.ACCEPTED]: {
            description: 'Update process triggered'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request.'
          }
        }
      }
    }
  }
};

const updateTransactionStatus: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/bill-payment/manual-adjustment',
  options: {
    description:
      'This api is used by admin portal to mark pending / MANUAL_INTERVENTION transaction as successful / failed',
    tags: ['private', 'api'],
    bind: {
      roles: [AdminPortalUserPermissionCode.CUSTOMER_INFO_WRITE]
    },
    validate: {
      payload: manualAdjustmentPayloadValidator
    },
    handler: async (
      _hapiRequest: IManualAdjustmentRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { transactionId, status } = _hapiRequest.payload;
      const reqStatus = MAP_GOBILL_STATUS_TO_TXN[status] as TransactionStatus;
      await billPaymentService.txnManualAdjustment(transactionId, reqStatus);
      return hapiResponse.response({ success: true }).code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Transaction manually adjusted'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error occured'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Invalid request'
          },
          [Http.StatusCode.NOT_FOUND]: {
            description: 'Transaction not found'
          }
        }
      }
    }
  }
};

const billPaymentController: hapi.ServerRoute[] = [
  updateBillPaymentStatus,
  updateTransactionStatus
];

export default billPaymentController;
