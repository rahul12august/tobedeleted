export default {
  create: jest.fn(),
  update: jest.fn(),
  getRecentTransactionStatus: jest.fn(),
  getStatusHistory: jest.fn(),
  getByTxnId: jest.fn(),
  updateGobillsTransaction: jest.fn()
};
