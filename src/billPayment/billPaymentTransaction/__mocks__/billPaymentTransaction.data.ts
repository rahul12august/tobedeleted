import {
  IBillPaymentTransactionStatus,
  IBillPaymentTransactionModel
} from '../billPaymentTransaction.type';
import { BillPaymentTransactionStatusEnum } from '../billPaymentTransaction.enum';
import { NewEntity } from '../../../common/common.type';

export const getBillPaymentTransaction = (): NewEntity<IBillPaymentTransactionModel> => ({
  orderId: 'order123',
  transactionId: 'transactionId',
  referenceId: 'REF123',
  status: [
    {
      status: BillPaymentTransactionStatusEnum.SUCCESS,
      capturedAt: new Date()
    }
  ],
  accountNo: '28205596',
  createdAt: new Date(`01 June 2020 00:00:00`),
  amount: 10000
});

export const getBillPaymentTransactionWithError = (): NewEntity<IBillPaymentTransactionModel> => ({
  orderId: 'order123',
  transactionId: 'transactionId',
  referenceId: 'REF123',
  status: [
    {
      status: BillPaymentTransactionStatusEnum.FAILURE,
      capturedAt: new Date(),
      reason: 'Error'
    }
  ],
  accountNo: '28205596',
  createdAt: new Date(`01 June 2020 00:00:00`),
  amount: 10000
});

export const getBillPaymentTransactionStatus = (): IBillPaymentTransactionStatus => ({
  status: BillPaymentTransactionStatusEnum.SUCCESS,
  capturedAt: new Date()
});

export const getBillPaymentTransactionStatusWithReason = (): IBillPaymentTransactionStatus => ({
  status: BillPaymentTransactionStatusEnum.TRANSACTION_SUBMIT_FAILED,
  capturedAt: new Date(),
  reason: 'Error'
});

export const getMultipleStatusForTransaction = (): NewEntity<IBillPaymentTransactionModel> => ({
  orderId: 'order123',
  transactionId: 'transactionId',
  referenceId: 'REF123',
  status: [
    {
      status: BillPaymentTransactionStatusEnum.REQUEST_SENT,
      capturedAt: new Date()
    },
    {
      status: BillPaymentTransactionStatusEnum.WAITING,
      capturedAt: new Date()
    },
    {
      status: BillPaymentTransactionStatusEnum.SUCCESS,
      capturedAt: new Date()
    }
  ],
  accountNo: '28205596',
  createdAt: new Date(`01 June 2020 00:00:00`),
  amount: 10000
});
