export default {
  createTransaction: jest.fn(),
  updateStatus: jest.fn(),
  getTransactionStatusHistory: jest.fn(),
  getTransactionStatus: jest.fn(),
  getByTxnId: jest.fn(),
  updateGobillsTransaction: jest.fn()
};
