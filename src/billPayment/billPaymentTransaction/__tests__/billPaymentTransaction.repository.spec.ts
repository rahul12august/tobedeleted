import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { BillPaymentTransactionModel } from '../billPaymentTransaction.model';
import {
  getBillPaymentTransaction,
  getBillPaymentTransactionStatus,
  getMultipleStatusForTransaction,
  getBillPaymentTransactionWithError,
  getBillPaymentTransactionStatusWithReason
} from '../__mocks__/billPaymentTransaction.data';
import billPaymentTransactionRepository from '../billPaymentTransaction.repository';
import { BillPaymentTransactionStatusEnum } from '../billPaymentTransaction.enum';

jest.mock('mongoose', () => {
  const mongoose = require.requireActual('mongoose');
  return new mongoose.Mongoose(); // new mongoose instance and connection for each test
});

describe('bill payment transaction repository', () => {
  let mongod: MongoMemoryServer;
  beforeAll(async () => {
    mongod = new MongoMemoryServer();
    const mongoDbUri = await mongod.getConnectionString();
    await mongoose.connect(mongoDbUri, {
      useNewUrlParser: true,
      useCreateIndex: true
    });
  });

  afterEach(async () => {
    expect.hasAssertions();
    await BillPaymentTransactionModel.deleteMany({});
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongod.stop();
  });

  describe('create method', () => {
    it('should create transaction successfully', async () => {
      // Given
      const transactionModel = getBillPaymentTransaction();

      // When
      const createdTransactionModel = await billPaymentTransactionRepository.create(
        transactionModel
      );

      // Then
      expect(createdTransactionModel.transactionId).toBeDefined();
      expect(createdTransactionModel.status.length).toEqual(1);
      expect(createdTransactionModel.status[0].status).toEqual(
        transactionModel.status[0].status
      );
      expect(createdTransactionModel.status[0].capturedAt).toEqual(
        transactionModel.status[0].capturedAt
      );
      expect(createdTransactionModel.status[0].reason).toBeUndefined();
    });

    it('should create transaction successfully with error', async () => {
      // Given
      let transactionModel = getBillPaymentTransactionWithError();

      // When
      const createdTransactionModel = await billPaymentTransactionRepository.create(
        transactionModel
      );

      // Then
      expect(createdTransactionModel.transactionId).toBeDefined();
      expect(createdTransactionModel.status.length).toEqual(1);
      expect(createdTransactionModel.status[0].status).toEqual(
        transactionModel.status[0].status
      );
      expect(createdTransactionModel.status[0].capturedAt).toEqual(
        transactionModel.status[0].capturedAt
      );
      expect(createdTransactionModel.status[0].reason).toEqual(
        transactionModel.status[0].reason
      );
    });

    it('should throw error when account no not provided', async () => {
      // Given
      const transactionModel = getBillPaymentTransaction();
      delete transactionModel.accountNo;
      // When
      let error = '';
      await billPaymentTransactionRepository
        .create(transactionModel)
        .catch(err => (error = err));

      // Then
      expect(error).toBeDefined();
    });

    it('should throw error when referenceId not provided', async () => {
      // Given
      const transactionModel = getBillPaymentTransaction();
      delete transactionModel.referenceId;
      // When
      let error = '';
      await billPaymentTransactionRepository
        .create(transactionModel)
        .catch(err => (error = err));

      // Then
      expect(error).toBeDefined();
    });
    it('should throw error when transactionId not provided', async () => {
      // Given
      const transactionModel = getBillPaymentTransaction();
      delete transactionModel.transactionId;
      // When
      let error = '';
      await billPaymentTransactionRepository
        .create(transactionModel)
        .catch(err => (error = err));

      // Then
      expect(error).toBeDefined();
    });
    it('should throw error when status not provided', async () => {
      // Given
      const transactionModel = getBillPaymentTransaction();
      delete transactionModel.status;
      // When
      let error = '';
      await billPaymentTransactionRepository
        .create(transactionModel)
        .catch(err => (error = err));

      // Then
      expect(error).toBeDefined();
    });
  });

  describe('update method', () => {
    it('should update transaction successfully', async () => {
      // Given
      const transactionModel = getBillPaymentTransaction();
      const transactionStatus = getBillPaymentTransactionStatus();
      const createdTransactionModel = await billPaymentTransactionRepository.create(
        transactionModel
      );

      // When
      const updatedTransaction = await billPaymentTransactionRepository.update(
        transactionModel.orderId,
        transactionStatus
      );

      // Then
      expect(updatedTransaction.transactionId).toBe(
        createdTransactionModel.transactionId
      );
      expect(updatedTransaction.status.length).toEqual(1);
      expect(updatedTransaction.status[0].status).toEqual(
        BillPaymentTransactionStatusEnum.SUCCESS
      );
      expect(updatedTransaction.status[0].capturedAt).toEqual(
        transactionStatus.capturedAt
      );
      expect(updatedTransaction.status[0].reason).toBeUndefined();
    });

    it('should update transaction successfully with error status', async () => {
      // Given
      const transactionModel = getBillPaymentTransaction();
      const transactionStatus = getBillPaymentTransactionStatusWithReason();
      const createdTransactionModel = await billPaymentTransactionRepository.create(
        transactionModel
      );

      // When
      const updatedTransaction = await billPaymentTransactionRepository.update(
        transactionModel.orderId,
        transactionStatus
      );

      // Then
      expect(updatedTransaction.transactionId).toBe(
        createdTransactionModel.transactionId
      );
      expect(updatedTransaction.status.length).toEqual(1);
      expect(updatedTransaction.status[0].status).toEqual(
        transactionStatus.status
      );
      expect(updatedTransaction.status[0].capturedAt).toEqual(
        transactionStatus.capturedAt
      );
      expect(updatedTransaction.status[0].reason).toEqual(
        transactionStatus.reason
      );
    });

    it('should return null when id not found', async () => {
      // Given
      const transactionStatus = getBillPaymentTransactionStatus();

      // When
      const updatedTransaction = await billPaymentTransactionRepository.update(
        'order123',
        transactionStatus
      );

      // Then
      expect(updatedTransaction).toBeNull();
    });
  });

  describe('find last status', () => {
    it('should find transaction successfully', async () => {
      // Given
      const transactionModel = getMultipleStatusForTransaction();
      const createdTransactionModel = await billPaymentTransactionRepository.create(
        transactionModel
      );

      // When
      const result = await billPaymentTransactionRepository.getRecentTransactionStatus(
        transactionModel.orderId
      );

      // Then
      expect(result.orderId).toBe(createdTransactionModel.orderId);
      expect(result.status.length).toEqual(1);
      expect(result.status[0].status).toEqual(
        BillPaymentTransactionStatusEnum.SUCCESS
      );
      expect(result.status[0].capturedAt).toEqual(
        transactionModel.status[transactionModel.status.length - 1].capturedAt
      );
    });

    it('should return null when id not found', async () => {
      // Given

      // When
      const result = await billPaymentTransactionRepository.getRecentTransactionStatus(
        'order123'
      );

      // Then
      expect(result).toBeNull();
    });
  });

  describe('find status history', () => {
    it('should find transaction successfully', async () => {
      // Given
      const transactionModel = getMultipleStatusForTransaction();
      const createdTransactionModel = await billPaymentTransactionRepository.create(
        transactionModel
      );

      // When
      const result = await billPaymentTransactionRepository.getStatusHistory(
        transactionModel.orderId
      );

      // Then
      expect(result.orderId).toBe(createdTransactionModel.orderId);
      expect(result.status.length).toEqual(3);
    });

    it('should return null when id not found', async () => {
      // Given

      // When
      const result = await billPaymentTransactionRepository.getStatusHistory(
        'referenceId'
      );

      // Then
      expect(result).toBeNull();
    });
  });

  describe('getByTxnId', () => {
    it('should return transaction', async () => {
      const transactionModel = getBillPaymentTransaction();
      const createdTransactionModel = await billPaymentTransactionRepository.create(
        transactionModel
      );

      const result = await billPaymentTransactionRepository.getByTxnId(
        createdTransactionModel.transactionId
      );

      expect(result.orderId).toEqual(transactionModel.orderId);
    });

    it('should throw an error', async () => {
      const transactionModel = getBillPaymentTransaction();
      await billPaymentTransactionRepository.create(transactionModel);

      const result = await billPaymentTransactionRepository.getByTxnId(
        'dummy123'
      );

      expect(result).toBeNull();
    });
  });

  describe('updateGobillsTransaction', () => {
    it('should update transaction', async () => {
      const transactionModel = getBillPaymentTransaction();
      const createdTransactionModel = await billPaymentTransactionRepository.create(
        transactionModel
      );
      transactionModel.manuallyAdjusted = true;

      const result = await billPaymentTransactionRepository.updateGobillsTransaction(
        createdTransactionModel.transactionId,
        transactionModel
      );

      expect(result.manuallyAdjusted).toEqual(
        transactionModel.manuallyAdjusted
      );
    });

    it('should update transaction', async () => {
      const transactionModel = getBillPaymentTransaction();

      await billPaymentTransactionRepository.create(transactionModel);
      transactionModel.manuallyAdjusted = true;
      const result = await billPaymentTransactionRepository.updateGobillsTransaction(
        'dummy123',
        transactionModel
      );

      expect(result).toBeNull();
    });
  });
});
