import billPaymentTransactionRepository from '../billPaymentTransaction.repository';
import billPaymentTransactionService from '../billPaymentTransaction.service';
import {
  getBillPaymentTransaction,
  getBillPaymentTransactionStatus,
  getMultipleStatusForTransaction
} from '../__mocks__/billPaymentTransaction.data';
import {
  IBillPaymentTransactionStatus,
  IBillPaymentTransactionModel
} from '../billPaymentTransaction.type';
import { BillPaymentTransactionStatusEnum } from '../billPaymentTransaction.enum';

jest.mock('../billPaymentTransaction.repository');

afterEach(() => {
  (billPaymentTransactionRepository.create as jest.Mock).mockReset();
  (billPaymentTransactionRepository.update as jest.Mock).mockReset();
  (billPaymentTransactionRepository.getRecentTransactionStatus as jest.Mock).mockReset();
  (billPaymentTransactionRepository.getStatusHistory as jest.Mock).mockReset();
});

describe('billPaymentTransactionService', () => {
  describe('createTransaction', () => {
    it('Should create transaction successfully', async () => {
      //Given
      const billTransferInput = getBillPaymentTransaction();
      (billPaymentTransactionRepository.create as jest.Mock).mockResolvedValue(
        billTransferInput
      );

      //When
      const result = await billPaymentTransactionService.createTransaction(
        billTransferInput
      );

      //Then
      expect(result.referenceId).toBeDefined();
    });

    it('Should return null when response null', async () => {
      //Given
      const billTransferInput = getBillPaymentTransaction();
      (billPaymentTransactionRepository.create as jest.Mock).mockResolvedValue(
        null
      );

      //When
      const result = await billPaymentTransactionService.createTransaction(
        billTransferInput
      );

      //Then
      expect(result).toBeNull();
    });
  });

  describe('updateStatus', () => {
    it('Should update transaction status successfully', async () => {
      //Given
      const billTransferInput = getBillPaymentTransaction();
      (billPaymentTransactionRepository.update as jest.Mock).mockResolvedValue(
        billTransferInput
      );

      //When
      const result = await billPaymentTransactionService.updateStatus(
        getBillPaymentTransactionStatus(),
        'REF123'
      );

      //Then
      expect(result.status[0].status).toEqual(
        BillPaymentTransactionStatusEnum.SUCCESS
      );
      expect(result.status[0].capturedAt).toEqual(
        billTransferInput.status[0].capturedAt
      );
    });

    it('Should return null if referenceId not found', async () => {
      //Given
      const billTransferUpdate: IBillPaymentTransactionStatus = getBillPaymentTransactionStatus();
      (billPaymentTransactionRepository.update as jest.Mock).mockResolvedValue(
        null
      );

      //When
      const result = await billPaymentTransactionService.updateStatus(
        billTransferUpdate,
        'referenceId'
      );

      //Then
      expect(result).toBeNull();
    });
  });

  describe('getTransactionStatusHistory', () => {
    it('Should get status history array successfully', async () => {
      //Given
      const billPaymentStatusResult: IBillPaymentTransactionModel = getMultipleStatusForTransaction();
      (billPaymentTransactionRepository.getStatusHistory as jest.Mock).mockResolvedValue(
        billPaymentStatusResult
      );

      //When
      const result = await billPaymentTransactionService.getTransactionStatusHistory(
        'REF123'
      );

      //Then
      expect(result.referenceId).toEqual('REF123');
      expect(result.status.length).toEqual(3);
    });

    it('Should return null if referenceId not found', async () => {
      //Given
      (billPaymentTransactionRepository.getStatusHistory as jest.Mock).mockResolvedValue(
        null
      );

      //When
      const result = await billPaymentTransactionService.getTransactionStatusHistory(
        'referenceId'
      );

      //Then
      expect(result).toBeNull();
    });
  });

  describe('getRecentTransactionStatus', () => {
    it('Should get last status successfully', async () => {
      //Given
      const billPaymentStatusResult: IBillPaymentTransactionModel = getBillPaymentTransaction();
      (billPaymentTransactionRepository.getRecentTransactionStatus as jest.Mock).mockResolvedValue(
        billPaymentStatusResult
      );

      //When
      const result = await billPaymentTransactionService.getTransactionStatus(
        'REF123'
      );

      //Then
      expect(result.referenceId).toEqual('REF123');
      expect(result.status.length).toEqual(1);
      expect(result.status[0].status).toEqual(
        BillPaymentTransactionStatusEnum.SUCCESS
      );
      expect(result.status[0].capturedAt).toEqual(
        billPaymentStatusResult.status[0].capturedAt
      );
    });

    it('Should return null if referenceId not found', async () => {
      //Given
      (billPaymentTransactionRepository.getRecentTransactionStatus as jest.Mock).mockResolvedValue(
        null
      );

      //When
      const result = await billPaymentTransactionService.getTransactionStatus(
        'referenceId'
      );

      //Then
      expect(result).toBeNull();
    });
  });

  describe('getByTxnId', () => {
    it('Should get transaction successfully', async () => {
      //Given
      const billPaymentStatusResult: IBillPaymentTransactionModel = getBillPaymentTransaction();
      (billPaymentTransactionRepository.getByTxnId as jest.Mock).mockResolvedValue(
        billPaymentStatusResult
      );

      //When
      const result = await billPaymentTransactionService.getByTxnId(
        'transactionId'
      );

      //Then
      expect(result.transactionId).toEqual('transactionId');
      expect(result.status.length).toEqual(1);
      expect(result.status[0].status).toEqual(
        BillPaymentTransactionStatusEnum.SUCCESS
      );
      expect(result.status[0].capturedAt).toEqual(
        billPaymentStatusResult.status[0].capturedAt
      );
    });

    it('Should return null if transactionId not found', async () => {
      //Given
      (billPaymentTransactionRepository.getByTxnId as jest.Mock).mockResolvedValue(
        null
      );

      //When
      const result = await billPaymentTransactionService.getByTxnId('txnId');

      //Then
      expect(result).toBeNull();
    });
  });

  describe('updateGobillsTransaction', () => {
    it('Should update transaction successfully', async () => {
      //Given
      const billPaymentStatusResult: IBillPaymentTransactionModel = getBillPaymentTransaction();
      (billPaymentTransactionRepository.updateGobillsTransaction as jest.Mock).mockResolvedValue(
        billPaymentStatusResult
      );

      //When
      const result = await billPaymentTransactionService.updateGobillsTransaction(
        'transactionId',
        {
          manuallyAdjusted: true
        }
      );

      //Then
      expect(result.transactionId).toEqual('transactionId');
      expect(result.status.length).toEqual(1);
      expect(result.status[0].status).toEqual(
        BillPaymentTransactionStatusEnum.SUCCESS
      );
      expect(result.status[0].capturedAt).toEqual(
        billPaymentStatusResult.status[0].capturedAt
      );
    });

    it('Should return null if transactionId not found', async () => {
      //Given
      (billPaymentTransactionRepository.updateGobillsTransaction as jest.Mock).mockResolvedValue(
        null
      );

      //When
      const result = await billPaymentTransactionService.updateGobillsTransaction(
        'transactionId',
        {
          manuallyAdjusted: true
        }
      );

      //Then
      expect(result).toBeNull();
    });
  });
});
