import mongoose, { Schema, Model, Document } from 'mongoose';
import { handleDuplicationError } from '../../common/mongoose.util';
import { BillPaymentTransactionStatusEnum } from './billPaymentTransaction.enum';
import { IBillPaymentTransactionModel } from './billPaymentTransaction.type';

export type BillPaymentTransactionDocument = IBillPaymentTransactionModel &
  Document;

export const collectionName = 'gobills_transactions';

const transactionStatus = new Schema({
  status: {
    type: String,
    enum: Object.values(BillPaymentTransactionStatusEnum),
    required: true
  },
  reason: {
    type: String,
    required: false
  },
  capturedAt: {
    type: Date,
    required: true
  }
});

const BillPaymentTransactionSchema = new Schema(
  {
    orderId: {
      type: String,
      required: true
    },
    transactionId: {
      type: String,
      required: true
    },
    referenceId: {
      type: String,
      required: true
    },
    status: {
      type: [transactionStatus],
      required: true
    },
    accountNo: {
      type: String,
      required: false
    },
    createdAt: {
      type: Date,
      required: true
    },
    amount: {
      type: Number,
      required: true
    },
    manuallyAdjusted: {
      type: Boolean,
      required: false
    }
  },
  {
    timestamps: true
  }
);

BillPaymentTransactionSchema.post('save', handleDuplicationError);

export const BillPaymentTransactionModel: Model<BillPaymentTransactionDocument> = mongoose.model(
  collectionName,
  BillPaymentTransactionSchema
);
