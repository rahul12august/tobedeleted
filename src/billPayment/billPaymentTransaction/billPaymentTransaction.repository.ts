import {
  BillPaymentTransactionModel,
  BillPaymentTransactionDocument
} from './billPaymentTransaction.model';
import { NewEntity } from '../../common/common.type';
import logger, { wrapLogs } from '../../logger';
import {
  IBillPaymentTransactionStatus,
  IBillPaymentTransactionModel
} from './billPaymentTransaction.type';

const convertTransactionDocumentToObject = (
  document: BillPaymentTransactionDocument
) => document.toObject({ getters: true }) as IBillPaymentTransactionModel;

const create = async (
  billPaymentTransactionModel: NewEntity<IBillPaymentTransactionModel>
): Promise<IBillPaymentTransactionModel> => {
  logger.info(
    `Bill Payment : Creating Bill Payment Transaction for transactionId : ${billPaymentTransactionModel.transactionId}, 
     referenceId : ${billPaymentTransactionModel.referenceId}, orderId : ${billPaymentTransactionModel.orderId}`
  );
  const newBillPaymentTransactionModel = new BillPaymentTransactionModel(
    billPaymentTransactionModel
  );

  await newBillPaymentTransactionModel.save();

  return convertTransactionDocumentToObject(newBillPaymentTransactionModel);
};

const update = async (
  orderId: string,
  billPaymentTransactionStatus: IBillPaymentTransactionStatus
): Promise<IBillPaymentTransactionModel | null> => {
  logger.info(
    `Bill Payment : Updating Bill Payment Transaction for payload : ${billPaymentTransactionStatus.status} for orderId : ${orderId}`
  );
  const billPaymentTransaction = await BillPaymentTransactionModel.findOneAndUpdate(
    { orderId: orderId },
    { $push: { status: billPaymentTransactionStatus } },
    { fields: { status: { $slice: -1 } }, new: true }
  );

  return (
    billPaymentTransaction &&
    convertTransactionDocumentToObject(billPaymentTransaction)
  );
};

const findOneByQuery = async (
  query: any
): Promise<IBillPaymentTransactionModel | null> => {
  logger.info(
    `Bill Payment : Fetching Bill Payment Transaction for query : ${JSON.stringify(
      query
    )}`
  );
  const billPaymentTransaction = await BillPaymentTransactionModel.findOne(
    query
  );
  return (
    billPaymentTransaction &&
    convertTransactionDocumentToObject(billPaymentTransaction)
  );
};

const getRecentTransactionStatus = async (
  orderId: string
): Promise<IBillPaymentTransactionModel | null> => {
  logger.info(
    `Bill Payment : Fetching recent Bill Payment Transaction for orderId : ${orderId}`
  );
  const billPaymentTransaction = await BillPaymentTransactionModel.findOne(
    { orderId: orderId },
    { status: { $slice: -1 } }
  );
  return (
    billPaymentTransaction &&
    convertTransactionDocumentToObject(billPaymentTransaction)
  );
};

const getStatusHistory = async (
  orderId: string
): Promise<IBillPaymentTransactionModel | null> => findOneByQuery({ orderId });

const getByTxnId = async (
  transactionId: string
): Promise<IBillPaymentTransactionModel | null> => {
  logger.info(
    `Bill Payment : Fetching recent Bill Payment Transaction for orderId : ${transactionId}`
  );
  const billPaymentTransaction = await BillPaymentTransactionModel.findOne({
    transactionId
  });
  return (
    billPaymentTransaction &&
    convertTransactionDocumentToObject(billPaymentTransaction)
  );
};

const updateGobillsTransaction = async (
  transactionId: string,
  transactionModel: Partial<IBillPaymentTransactionModel>
): Promise<IBillPaymentTransactionModel | null> => {
  const transaction = await BillPaymentTransactionModel.findOneAndUpdate(
    { transactionId },
    transactionModel,
    { new: true }
  );

  return transaction && convertTransactionDocumentToObject(transaction);
};

const billPaymentTransactionRepository = {
  create,
  update,
  getRecentTransactionStatus,
  getStatusHistory,
  getByTxnId,
  updateGobillsTransaction
};

export default wrapLogs(billPaymentTransactionRepository);
