import { BillPaymentTransactionStatusEnum } from './billPaymentTransaction.enum';

export interface IBillPaymentTransactionModel {
  orderId: string;
  transactionId: string;
  referenceId: string;
  status: IBillPaymentTransactionStatus[];
  accountNo?: string;
  createdAt?: Date;
  amount: number;
  manuallyAdjusted?: boolean;
}

export interface IBillPaymentTransactionStatus {
  status: BillPaymentTransactionStatusEnum;
  reason?: string;
  capturedAt: Date;
}
