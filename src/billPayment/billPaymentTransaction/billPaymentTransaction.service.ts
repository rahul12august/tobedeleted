import billPaymentTransactionRepository from './billPaymentTransaction.repository';
import {
  IBillPaymentTransactionStatus,
  IBillPaymentTransactionModel
} from './billPaymentTransaction.type';
import { wrapLogs } from '../../logger';

const createTransaction = async (
  billpaymentTransaction: IBillPaymentTransactionModel
): Promise<IBillPaymentTransactionModel> =>
  billPaymentTransactionRepository.create(billpaymentTransaction);

const updateStatus = async (
  billpaymentTransaction: IBillPaymentTransactionStatus,
  orderId: string
): Promise<IBillPaymentTransactionModel | null> =>
  billPaymentTransactionRepository.update(orderId, billpaymentTransaction);

const getTransactionStatusHistory = async (
  orderId: string
): Promise<IBillPaymentTransactionModel | null> =>
  billPaymentTransactionRepository.getStatusHistory(orderId);

const getTransactionStatus = async (
  orderId: string
): Promise<IBillPaymentTransactionModel | null> =>
  billPaymentTransactionRepository.getRecentTransactionStatus(orderId);

const getByTxnId = async (
  txnId: string
): Promise<IBillPaymentTransactionModel | null> =>
  billPaymentTransactionRepository.getByTxnId(txnId);

const updateGobillsTransaction = async (
  transactionId: string,
  transactionModel: Partial<IBillPaymentTransactionModel>
): Promise<IBillPaymentTransactionModel | null> =>
  billPaymentTransactionRepository.updateGobillsTransaction(
    transactionId,
    transactionModel
  );

const billPaymentTxnService = wrapLogs({
  createTransaction,
  updateStatus,
  getTransactionStatusHistory,
  getTransactionStatus,
  getByTxnId,
  updateGobillsTransaction
});

export default billPaymentTxnService;
