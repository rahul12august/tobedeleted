import {
  IPaymentStatusPolling,
  IUpdatePaymentStatusPolling
} from './paymentStatusPolling.type';
import { BillingAggregatorPollingConfigType } from '@dk/module-common';
import repository from './paymentStatusPolling.repository';
import { BillPaymentTransactionStatusEnum } from '../billPaymentTransaction/billPaymentTransaction.enum';
import { MINUTES } from './paymentStatusPolling.constant';
import { getNextPollTime } from '../billPayment.utils';
import configurationService from '../../configuration/configuration.service';
import { IPollingConfig } from '../../common/common.type';
import logger, { wrapLogs } from '../../logger';
import billPaymentTransactionService from '../billPaymentTransaction/billPaymentTransaction.service';
import transactionProducer from '../../transaction/transaction.producer';
import billDetailService from '../billDetail/billDetail.service';

const updatePaymentStatusPollingConfig = async (
  orderId: string,
  status?: BillPaymentTransactionStatusEnum,
  nextPollTime?: Date
): Promise<IPaymentStatusPolling | null> => {
  const newPollStatusEntry: IUpdatePaymentStatusPolling = {
    orderId: orderId,
    status: status,
    nextPollTime: nextPollTime
  };
  const response = await repository.updateWithIncrement(newPollStatusEntry);
  return response;
};

const calculateEndDate = (startDate: Date): Date => {
  const minutesToAddInMs = MINUTES * 60000;
  return new Date(startDate.getTime() + minutesToAddInMs);
};

const getPendingRecords = async (
  startDate: Date
): Promise<IPaymentStatusPolling[]> =>
  repository.getPendingRecords(
    BillPaymentTransactionStatusEnum.WAITING,
    calculateEndDate(startDate)
  );

const updateManualIntervention = async (orderId: string): Promise<void> => {
  logger.error(
    `Bill Payment : Updating bill payment status for orderId : ${orderId} to ${BillPaymentTransactionStatusEnum.MANUAL_INTERVENTION}`
  );
  updatePaymentStatusPollingConfig(
    orderId,
    BillPaymentTransactionStatusEnum.MANUAL_INTERVENTION
  );
};

const isMarkedForNextDayProcessesing = (nextTime: IPollingConfig): boolean =>
  nextTime.type === BillingAggregatorPollingConfigType.ABSOLUTE;

const sendBillPaymentWaitingNotif = async (orderId: string): Promise<void> => {
  const transactionStatus = await billPaymentTransactionService.getTransactionStatus(
    orderId
  );
  if (transactionStatus) {
    const billDetails = await billDetailService.getBillDetails(
      transactionStatus.referenceId
    );
    transactionProducer.sendPendingBillPaymentNotification({
      billDetails: billDetails,
      transactionAmount: transactionStatus.amount,
      transactionId: transactionStatus.transactionId,
      transactionTime: transactionStatus.createdAt || new Date()
    });
  }
};

const updatePaymentStatusConfig = async (
  paymentStatusConfig: IPaymentStatusPolling,
  status: BillPaymentTransactionStatusEnum,
  pollingConfigs: IPollingConfig[]
): Promise<void> => {
  switch (status) {
    case BillPaymentTransactionStatusEnum.SUCCESS: {
      updatePaymentStatusPollingConfig(paymentStatusConfig.orderId, status);
      break;
    }
    case BillPaymentTransactionStatusEnum.FAILURE: {
      logger.error(
        `Bill Payment : Updating bill payment failure status for orderId : ${paymentStatusConfig.orderId}`
      );
      updatePaymentStatusPollingConfig(paymentStatusConfig.orderId, status);
      break;
    }
    case BillPaymentTransactionStatusEnum.WAITING: {
      const nextTime = pollingConfigs[paymentStatusConfig.statusRetryCount];
      logger.info(
        `Bill Payment : Polling config for next time : ${JSON.stringify(
          nextTime
        )} for orderId: ${paymentStatusConfig.orderId}, RetryCount: ${
          paymentStatusConfig.statusRetryCount
        }`
      );

      if (nextTime) {
        if (isMarkedForNextDayProcessesing(nextTime)) {
          logger.info(
            `Bill Payment : Sending notification for pending billpayment for orderId: ${paymentStatusConfig.orderId}`
          );
          sendBillPaymentWaitingNotif(paymentStatusConfig.orderId);
        }
        updatePaymentStatusPollingConfig(
          paymentStatusConfig.orderId,
          status,
          await getNextPollTime(nextTime)
        );
      } else {
        return updateManualIntervention(paymentStatusConfig.orderId);
      }
      break;
    }
  }
};

const updatePollingConfig = async (
  orderId: string,
  status: BillPaymentTransactionStatusEnum
): Promise<IPollingConfig> => {
  const pollingConfig = await configurationService.getGoBillsPollingConfig();
  const paymentStatusPolling = await repository.getPaymentStatusPollingFromDB(
    orderId
  );

  if (paymentStatusPolling) {
    await updatePaymentStatusConfig(
      paymentStatusPolling,
      status,
      pollingConfig
    );
    return pollingConfig[paymentStatusPolling.statusRetryCount];
  }

  if (
    !paymentStatusPolling &&
    status === BillPaymentTransactionStatusEnum.WAITING
  ) {
    await repository.create({
      orderId: orderId,
      statusRetryCount: 1,
      nextPollTime: await getNextPollTime(pollingConfig[0]),
      status: status
    });
    return pollingConfig[0];
  }

  return pollingConfig[0];
};

const paymentStatusPollingService = {
  getPendingRecords,
  updatePollingConfig,
  updatePaymentStatusPollingConfig
};

export default wrapLogs(paymentStatusPollingService);
