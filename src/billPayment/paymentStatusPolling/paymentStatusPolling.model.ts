import mongoose, { Document, Model, Schema } from 'mongoose';
import { IPaymentStatusPolling } from './paymentStatusPolling.type';
import { BillPaymentTransactionStatusEnum } from '../billPaymentTransaction/billPaymentTransaction.enum';

export type PaymentStatusPollingDocument = IPaymentStatusPolling & Document;
export const collectionName = 'gobills_polling_state';

const PaymentStatusPollingSchema = new Schema(
  {
    orderId: {
      type: String,
      required: true,
      unique: true
    },
    status: {
      type: String,
      enum: Object.values(BillPaymentTransactionStatusEnum),
      required: true
    },
    nextPollTime: {
      type: Date,
      required: true
    },
    statusRetryCount: {
      type: Number,
      required: true,
      default: 0
    }
  },
  {
    timestamps: true
  }
);
PaymentStatusPollingSchema.index({ nextPollTime: 1, status: 1 });

export const PaymentStatusPollingModel: Model<PaymentStatusPollingDocument> = mongoose.model(
  collectionName,
  PaymentStatusPollingSchema
);
