import {
  PaymentStatusPollingDocument,
  PaymentStatusPollingModel
} from './paymentStatusPolling.model';
import {
  IPaymentStatusPolling,
  IUpdatePaymentStatusPolling
} from './paymentStatusPolling.type';
import { omitBy, isUndefined } from 'lodash';
import { BillPaymentTransactionStatusEnum } from '../billPaymentTransaction/billPaymentTransaction.enum';
import logger, { wrapLogs } from '../../logger';

const toDocumentObject = (document: PaymentStatusPollingDocument) =>
  document.toObject({
    getters: true
  }) as IPaymentStatusPolling;

const create = async (
  statusPollEntry: IPaymentStatusPolling
): Promise<IPaymentStatusPolling> => {
  logger.info(
    `Bill Payment : Creating bill payment status polling entry : 
    ${JSON.stringify(statusPollEntry)}`
  );
  const object = await PaymentStatusPollingModel.create(statusPollEntry);
  return toDocumentObject(object);
};

const updateWithIncrement = async (
  updatedPaymentStatusPolling: IUpdatePaymentStatusPolling
): Promise<IPaymentStatusPolling | null> => {
  logger.info(
    `Bill Payment : Updating bill payment status polling entry with increment : 
    ${JSON.stringify(updatedPaymentStatusPolling)}`
  );
  const { orderId, ...fieldsToUpdate } = updatedPaymentStatusPolling;
  const result = await PaymentStatusPollingModel.findOneAndUpdate(
    { orderId: orderId },
    {
      $set: omitBy(fieldsToUpdate, isUndefined),
      $inc: { statusRetryCount: 1 }
    },
    { new: true }
  );

  return result && toDocumentObject(result);
};

const getPaymentStatusPollingFromDB = async (
  orderId: string
): Promise<IPaymentStatusPolling | null> => {
  logger.info(
    `Bill Payment : Fetching bill payment status for orderId : 
    ${orderId}`
  );
  const paymentStatusPolling = await PaymentStatusPollingModel.findOne({
    orderId
  });
  return paymentStatusPolling && toDocumentObject(paymentStatusPolling);
};

const getPendingRecords = async (
  status: BillPaymentTransactionStatusEnum,
  endDateTime: Date
): Promise<IPaymentStatusPolling[]> => {
  logger.info(
    `Bill Payment : Fetching pending bill payment entries for endDateTime : 
    ${endDateTime}`
  );
  const documents = await PaymentStatusPollingModel.find({
    status: status,
    nextPollTime: {
      $lte: endDateTime
    }
  });
  return documents.map(entry => toDocumentObject(entry));
};

const paymentStatusPollingRepository = {
  create,
  updateWithIncrement,
  getPaymentStatusPollingFromDB,
  getPendingRecords
};

export default wrapLogs(paymentStatusPollingRepository);
