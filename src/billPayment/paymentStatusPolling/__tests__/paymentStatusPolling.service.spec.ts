import paymentStatusPollingRepository from '../paymentStatusPolling.repository';
import {
  createPaymentStatusPollingData,
  createPaymentStatusPollingData2,
  pollingConfigs
} from '../__mocks__/paymentStatusPolling.data';
import paymentStatusPollingService from '../paymentStatusPolling.service';
import { BillPaymentTransactionStatusEnum } from '../../billPaymentTransaction/billPaymentTransaction.enum';
import { getNextTime } from '../../billPayment.utils';
import billPaymentTransactionService from '../../billPaymentTransaction/billPaymentTransaction.service';
import {
  getBillPaymentTransaction,
  getMultipleStatusForTransaction
} from '../../billPaymentTransaction/__mocks__/billPaymentTransaction.data';
import configurationRepository from '../../../configuration/configuration.repository';
import { getHolidaysWithDateRangeDataWithDate } from '../../../configuration/__mocks__/configuration.data';
import configurationService from '../../../configuration/configuration.service';
import billDetailService from '../../billDetail/billDetail.service';
import { getBillDetails } from '../../billDetail/__mocks__/billDetail.data';
import transactionProducer from '../../../transaction/transaction.producer';

jest.mock('../paymentStatusPolling.repository');
jest.mock('../../billPaymentTransaction/billPaymentTransaction.service');
jest.mock('../../../configuration/configuration.repository');
jest.mock('../../../configuration/configuration.service');
jest.mock('../../billDetail/billDetail.service');
jest.mock('../../../transaction/transaction.producer');

describe('payment status polling service', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('#getPendingRecords', () => {
    it('should return pending records within range and proper status', async () => {
      // Given
      const pendingRecords = [
        createPaymentStatusPollingData(),
        createPaymentStatusPollingData2()
      ];
      (paymentStatusPollingRepository.getPendingRecords as jest.Mock).mockResolvedValueOnce(
        pendingRecords
      );

      // When
      const startDate = new Date(2020, 3, 20, 10, 33, 0, 0);

      const result = await paymentStatusPollingService.getPendingRecords(
        startDate
      );

      // Then
      expect(result).toBeDefined();
      expect(result.length).toEqual(2);
      for (const pendingStatus of result) {
        expect(pendingStatus.status).toEqual(
          BillPaymentTransactionStatusEnum.WAITING
        );
      }
    });

    it('should return empty when pending records not found', async () => {
      // Given
      const pendingRecords = [];
      (paymentStatusPollingRepository.getPendingRecords as jest.Mock).mockResolvedValueOnce(
        pendingRecords
      );

      // When
      const startDate = new Date(2020, 3, 20, 10, 40, 0, 0);

      const result = await paymentStatusPollingService.getPendingRecords(
        startDate
      );

      // Then
      expect(result).toBeDefined();
      expect(result.length).toEqual(0);
    });
  });

  describe('#updatePollingConfig', () => {
    it('should create payment status polling config successfully if Status is Waiting and record not found', async () => {
      // Given
      const paymentStatusPollingData = createPaymentStatusPollingData();
      (paymentStatusPollingRepository.getPaymentStatusPollingFromDB as jest.Mock).mockResolvedValueOnce(
        null
      );
      (paymentStatusPollingRepository.create as jest.Mock).mockResolvedValueOnce(
        paymentStatusPollingData
      );
      (configurationService.getGoBillsPollingConfig as jest.Mock).mockResolvedValueOnce(
        pollingConfigs
      );

      // When
      await paymentStatusPollingService.updatePollingConfig(
        'REF123',
        BillPaymentTransactionStatusEnum.WAITING
      );

      // Then
      expect(
        paymentStatusPollingRepository.getPaymentStatusPollingFromDB
      ).toHaveBeenCalledWith('REF123');

      expect(paymentStatusPollingRepository.create).toHaveBeenCalledTimes(1);
    });

    it('should not do anything if Status is Success and record not found', async () => {
      // Given
      (paymentStatusPollingRepository.getPaymentStatusPollingFromDB as jest.Mock).mockResolvedValueOnce(
        null
      );
      (configurationService.getGoBillsPollingConfig as jest.Mock).mockResolvedValueOnce(
        pollingConfigs
      );

      // When
      await paymentStatusPollingService.updatePollingConfig(
        'REF123',
        BillPaymentTransactionStatusEnum.SUCCESS
      );

      // Then
      expect(
        paymentStatusPollingRepository.getPaymentStatusPollingFromDB
      ).toHaveBeenCalledWith('REF123');

      expect(paymentStatusPollingRepository.create).not.toHaveBeenCalled();
    });

    it('should not create payment status polling config if Status is not Waiting and record not found', async () => {
      // Given
      const paymentStatusPollingData = createPaymentStatusPollingData();
      (paymentStatusPollingRepository.getPaymentStatusPollingFromDB as jest.Mock).mockResolvedValueOnce(
        null
      );
      (paymentStatusPollingRepository.create as jest.Mock).mockResolvedValueOnce(
        paymentStatusPollingData
      );
      (configurationService.getGoBillsPollingConfig as jest.Mock).mockResolvedValueOnce(
        pollingConfigs
      );

      // When
      await paymentStatusPollingService.updatePollingConfig(
        'REF123',
        BillPaymentTransactionStatusEnum.SUCCESS
      );

      // Then
      expect(
        paymentStatusPollingRepository.getPaymentStatusPollingFromDB
      ).toHaveBeenCalledWith('REF123');

      expect(paymentStatusPollingRepository.create).toHaveBeenCalledTimes(0);
    });

    it('should update payment status polling config if Status is Success and record found', async () => {
      // Given
      const getPaymentDataForCreate = createPaymentStatusPollingData();
      (paymentStatusPollingRepository.getPaymentStatusPollingFromDB as jest.Mock).mockResolvedValueOnce(
        getPaymentDataForCreate
      );
      getPaymentDataForCreate.status = BillPaymentTransactionStatusEnum.SUCCESS;
      (paymentStatusPollingRepository.updateWithIncrement as jest.Mock).mockResolvedValue(
        getPaymentDataForCreate
      );
      (configurationService.getGoBillsPollingConfig as jest.Mock).mockResolvedValueOnce(
        pollingConfigs
      );
      // When
      await paymentStatusPollingService.updatePollingConfig(
        'REF123',
        BillPaymentTransactionStatusEnum.SUCCESS
      );

      // Then
      expect(
        paymentStatusPollingRepository.getPaymentStatusPollingFromDB
      ).toHaveBeenCalledWith('REF123');

      expect(paymentStatusPollingRepository.create).toHaveBeenCalledTimes(0);
      expect(
        paymentStatusPollingRepository.updateWithIncrement
      ).toHaveBeenLastCalledWith({
        orderId: 'REF123',
        status: BillPaymentTransactionStatusEnum.SUCCESS,
        nextPollTime: undefined
      });
    });

    it('should update payment status polling config if Status is Failure and record found', async () => {
      // Given
      const getPaymentDataForCreate = createPaymentStatusPollingData();
      (paymentStatusPollingRepository.getPaymentStatusPollingFromDB as jest.Mock).mockResolvedValueOnce(
        getPaymentDataForCreate
      );
      getPaymentDataForCreate.status = BillPaymentTransactionStatusEnum.SUCCESS;
      (paymentStatusPollingRepository.updateWithIncrement as jest.Mock).mockResolvedValueOnce(
        getPaymentDataForCreate
      );
      (configurationService.getGoBillsPollingConfig as jest.Mock).mockResolvedValueOnce(
        pollingConfigs
      );
      // When
      await paymentStatusPollingService.updatePollingConfig(
        'REF123',
        BillPaymentTransactionStatusEnum.FAILURE
      );

      // Then
      expect(
        paymentStatusPollingRepository.getPaymentStatusPollingFromDB
      ).toHaveBeenCalledWith('REF123');

      expect(paymentStatusPollingRepository.create).toHaveBeenCalledTimes(0);
      expect(
        paymentStatusPollingRepository.updateWithIncrement
      ).toHaveBeenLastCalledWith({
        orderId: 'REF123',
        status: BillPaymentTransactionStatusEnum.FAILURE,
        nextPollTime: undefined
      });
    });

    it('should update payment status polling config if Status is Waiting but retry count exceeded', async () => {
      // Given
      const getPaymentDataForCreate = createPaymentStatusPollingData();
      (paymentStatusPollingRepository.getPaymentStatusPollingFromDB as jest.Mock).mockResolvedValueOnce(
        getPaymentDataForCreate
      );
      getPaymentDataForCreate.statusRetryCount = 3;
      (paymentStatusPollingRepository.updateWithIncrement as jest.Mock).mockResolvedValueOnce(
        getPaymentDataForCreate
      );
      (billPaymentTransactionService.updateStatus as jest.Mock).mockResolvedValueOnce(
        getBillPaymentTransaction()
      );
      (configurationService.getGoBillsPollingConfig as jest.Mock).mockResolvedValueOnce(
        pollingConfigs
      );
      // When
      await paymentStatusPollingService.updatePollingConfig(
        'REF123',
        BillPaymentTransactionStatusEnum.WAITING
      );

      // Then
      expect(
        paymentStatusPollingRepository.getPaymentStatusPollingFromDB
      ).toHaveBeenCalledWith('REF123');

      expect(paymentStatusPollingRepository.create).toHaveBeenCalledTimes(0);
      expect(
        paymentStatusPollingRepository.updateWithIncrement
      ).toHaveBeenCalledWith({
        orderId: 'REF123',
        status: BillPaymentTransactionStatusEnum.MANUAL_INTERVENTION,
        nextPollTime: undefined
      });
    });

    it('should update transactions to pending with payment config type absolute in the given range', async () => {
      // Given
      const getPaymentDataForCreate = createPaymentStatusPollingData();
      const today = new Date();
      const tomorrow = new Date(new Date(today).setHours(0, 0, 0, 0));
      tomorrow.setDate(tomorrow.getDate() + 1);
      (paymentStatusPollingRepository.getPaymentStatusPollingFromDB as jest.Mock).mockResolvedValueOnce(
        getPaymentDataForCreate
      );
      getPaymentDataForCreate.statusRetryCount = 2;
      (configurationService.getGoBillsPollingConfig as jest.Mock).mockResolvedValueOnce(
        pollingConfigs
      );
      (paymentStatusPollingRepository.updateWithIncrement as jest.Mock).mockResolvedValueOnce(
        getPaymentDataForCreate
      );
      (billPaymentTransactionService.updateStatus as jest.Mock).mockResolvedValueOnce(
        getBillPaymentTransaction()
      );
      (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValueOnce(
        getHolidaysWithDateRangeDataWithDate(tomorrow)
      );

      (billPaymentTransactionService.getTransactionStatus as jest.Mock).mockResolvedValueOnce(
        getMultipleStatusForTransaction()
      );

      (billDetailService.getBillDetails as jest.Mock).mockResolvedValueOnce(
        getBillDetails()
      );

      // When
      await paymentStatusPollingService.updatePollingConfig(
        'REF123',
        BillPaymentTransactionStatusEnum.WAITING
      );

      // Then
      expect(
        paymentStatusPollingRepository.getPaymentStatusPollingFromDB
      ).toHaveBeenCalledWith('REF123');

      expect(paymentStatusPollingRepository.create).toHaveBeenCalledTimes(0);
      expect(billPaymentTransactionService.updateStatus).toHaveBeenCalledTimes(
        0
      );
      tomorrow.setDate(tomorrow.getDate() + 3);
      expect(
        billPaymentTransactionService.getTransactionStatus
      ).toHaveBeenCalledWith('REF123');
      expect(billDetailService.getBillDetails).toHaveBeenCalledWith('REF123');
      expect(
        paymentStatusPollingRepository.updateWithIncrement
      ).toHaveBeenCalledWith({
        orderId: 'REF123',
        status: BillPaymentTransactionStatusEnum.WAITING,
        nextPollTime: getNextTime(tomorrow, 17, 15, 10)
      });
    });

    it('should update transactions to pending with payment config type absolute check isBefore condition', async () => {
      // Given
      const getPaymentDataForCreate = createPaymentStatusPollingData();
      const today = new Date();
      const tomorrow = new Date(new Date(today).setHours(0, 0, 0, 0));
      tomorrow.setDate(tomorrow.getDate() + 2);
      (paymentStatusPollingRepository.getPaymentStatusPollingFromDB as jest.Mock).mockResolvedValueOnce(
        getPaymentDataForCreate
      );
      getPaymentDataForCreate.statusRetryCount = 2;
      (paymentStatusPollingRepository.updateWithIncrement as jest.Mock).mockResolvedValueOnce(
        getPaymentDataForCreate
      );
      (billPaymentTransactionService.updateStatus as jest.Mock).mockResolvedValueOnce(
        getBillPaymentTransaction()
      );
      (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValueOnce(
        getHolidaysWithDateRangeDataWithDate(tomorrow)
      );
      (configurationService.getGoBillsPollingConfig as jest.Mock).mockResolvedValueOnce(
        pollingConfigs
      );

      // When
      await paymentStatusPollingService.updatePollingConfig(
        'REF123',
        BillPaymentTransactionStatusEnum.WAITING
      );

      // Then
      expect(
        paymentStatusPollingRepository.getPaymentStatusPollingFromDB
      ).toHaveBeenCalledWith('REF123');

      expect(paymentStatusPollingRepository.create).toHaveBeenCalledTimes(0);
      expect(billPaymentTransactionService.updateStatus).toHaveBeenCalledTimes(
        0
      );
      tomorrow.setDate(tomorrow.getDate() - 1);
      expect(
        paymentStatusPollingRepository.updateWithIncrement
      ).toHaveBeenCalledWith({
        orderId: 'REF123',
        status: BillPaymentTransactionStatusEnum.WAITING,
        nextPollTime: getNextTime(tomorrow, 17, 15, 10)
      });
    });
    it('should send notification when status is pending with payment config type absolute in the given range', async () => {
      // Given
      const getPaymentDataForCreate = createPaymentStatusPollingData();
      const today = new Date();
      const tomorrow = new Date(new Date(today).setHours(0, 0, 0, 0));
      tomorrow.setDate(tomorrow.getDate() + 1);
      (paymentStatusPollingRepository.getPaymentStatusPollingFromDB as jest.Mock).mockResolvedValueOnce(
        getPaymentDataForCreate
      );
      getPaymentDataForCreate.statusRetryCount = 2;
      (configurationService.getGoBillsPollingConfig as jest.Mock).mockResolvedValueOnce(
        pollingConfigs
      );
      (paymentStatusPollingRepository.updateWithIncrement as jest.Mock).mockResolvedValueOnce(
        getPaymentDataForCreate
      );
      (billPaymentTransactionService.updateStatus as jest.Mock).mockResolvedValueOnce(
        getBillPaymentTransaction()
      );
      (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValueOnce(
        getHolidaysWithDateRangeDataWithDate(tomorrow)
      );

      (billPaymentTransactionService.getTransactionStatus as jest.Mock).mockResolvedValueOnce(
        getMultipleStatusForTransaction()
      );

      (billDetailService.getBillDetails as jest.Mock).mockResolvedValueOnce(
        getBillDetails()
      );

      // When
      await paymentStatusPollingService.updatePollingConfig(
        'REF123',
        BillPaymentTransactionStatusEnum.WAITING
      );

      // Then
      expect(
        paymentStatusPollingRepository.getPaymentStatusPollingFromDB
      ).toHaveBeenCalledWith('REF123');

      expect(paymentStatusPollingRepository.create).toHaveBeenCalledTimes(0);
      expect(billPaymentTransactionService.updateStatus).toHaveBeenCalledTimes(
        0
      );
      tomorrow.setDate(tomorrow.getDate() + 3);
      expect(
        paymentStatusPollingRepository.updateWithIncrement
      ).toHaveBeenCalledWith({
        orderId: 'REF123',
        status: BillPaymentTransactionStatusEnum.WAITING,
        nextPollTime: getNextTime(tomorrow, 17, 15, 10)
      });
      expect(
        billPaymentTransactionService.getTransactionStatus
      ).toHaveBeenCalledWith('REF123');
      expect(billDetailService.getBillDetails).toHaveBeenCalledWith('REF123');
      expect(
        transactionProducer.sendPendingBillPaymentNotification
      ).toBeCalledWith({
        billDetails: getBillDetails(),
        transactionAmount: 10000,
        transactionId: 'transactionId',
        transactionTime: getMultipleStatusForTransaction().createdAt
      });
    });
    it('should not send notification when status is pending and retry count is not exceeded', async () => {
      // Given
      const getPaymentDataForCreate = createPaymentStatusPollingData();
      const today = new Date();
      const tomorrow = new Date(new Date(today).setHours(0, 0, 0, 0));
      tomorrow.setDate(tomorrow.getDate() + 1);
      (paymentStatusPollingRepository.getPaymentStatusPollingFromDB as jest.Mock).mockResolvedValueOnce(
        getPaymentDataForCreate
      );
      getPaymentDataForCreate.statusRetryCount = 1;
      (configurationService.getGoBillsPollingConfig as jest.Mock).mockResolvedValueOnce(
        pollingConfigs
      );
      (paymentStatusPollingRepository.updateWithIncrement as jest.Mock).mockResolvedValueOnce(
        getPaymentDataForCreate
      );
      (billPaymentTransactionService.updateStatus as jest.Mock).mockResolvedValueOnce(
        getBillPaymentTransaction()
      );
      (configurationRepository.getHolidaysByDateRange as jest.Mock).mockResolvedValueOnce(
        getHolidaysWithDateRangeDataWithDate(tomorrow)
      );

      (billPaymentTransactionService.getTransactionStatus as jest.Mock).mockResolvedValueOnce(
        getMultipleStatusForTransaction()
      );

      (billDetailService.getBillDetails as jest.Mock).mockResolvedValueOnce(
        getBillDetails()
      );

      // When
      await paymentStatusPollingService.updatePollingConfig(
        'REF123',
        BillPaymentTransactionStatusEnum.WAITING
      );

      // Then
      expect(
        paymentStatusPollingRepository.getPaymentStatusPollingFromDB
      ).toHaveBeenCalledWith('REF123');

      expect(paymentStatusPollingRepository.create).toHaveBeenCalledTimes(0);
      expect(billPaymentTransactionService.updateStatus).toHaveBeenCalledTimes(
        0
      );
      expect(
        billPaymentTransactionService.getTransactionStatus
      ).toHaveBeenCalledTimes(0);
      expect(billDetailService.getBillDetails).toHaveBeenCalledTimes(0);
      expect(
        transactionProducer.sendPendingBillPaymentNotification
      ).toHaveBeenCalledTimes(0);
    });
  });
});
