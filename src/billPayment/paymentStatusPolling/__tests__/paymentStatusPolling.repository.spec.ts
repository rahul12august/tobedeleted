import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { PaymentStatusPollingModel } from '../paymentStatusPolling.model';
import {
  createPaymentStatusPollingData,
  createPaymentStatusPollingData2,
  createPaymentStatusPollingData3,
  createPaymentStatusPollingData4
} from '../__mocks__/paymentStatusPolling.data';
import paymentStatusPollingRepository from '../paymentStatusPolling.repository';
import { IUpdatePaymentStatusPolling } from '../paymentStatusPolling.type';
import { BillPaymentTransactionStatusEnum } from '../../billPaymentTransaction/billPaymentTransaction.enum';

jest.mock('mongoose', () => {
  const mongoose = require.requireActual('mongoose');
  return new mongoose.Mongoose(); // new mongoose instance and connection for each test
});

describe('paymentStatusPolling.Repository', () => {
  let mongoMemory: MongoMemoryServer;
  beforeAll(async () => {
    mongoMemory = new MongoMemoryServer();
    const mongoDbUri = await mongoMemory.getConnectionString();
    await mongoose.connect(mongoDbUri, { useNewUrlParser: true });
  });

  afterEach(async () => {
    await PaymentStatusPollingModel.deleteMany({});
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongoMemory.stop();
  });

  describe('create', () => {
    it('should not throw error if valid', async () => {
      // Given
      const paymentStatusPollingData = createPaymentStatusPollingData();

      // When
      const createdPaymentStatusPolling = await paymentStatusPollingRepository.create(
        paymentStatusPollingData
      );

      // Then
      expect(createdPaymentStatusPolling.orderId).toBeDefined();
      expect(createdPaymentStatusPolling.orderId).toEqual('REF123');
      expect(createdPaymentStatusPolling.status).toEqual(
        paymentStatusPollingData.status
      );
      expect(createdPaymentStatusPolling.statusRetryCount).toEqual(
        paymentStatusPollingData.statusRetryCount
      );
      expect(createdPaymentStatusPolling.nextPollTime).toEqual(
        paymentStatusPollingData.nextPollTime
      );
    });

    it('should throw error if not valid', async () => {
      // Given
      const paymentStatusPollingData = {
        ...createPaymentStatusPollingData(),
        orderId: ''
      };

      // When
      try {
        await paymentStatusPollingRepository.create(paymentStatusPollingData);
      } catch (e) {
        expect(e).toBeDefined();
        expect(e.toString()).toEqual(
          'ValidationError: orderId: Path `orderId` is required.'
        );
      }
    });
  });

  describe('get', () => {
    it('should get payment status polling detail', async () => {
      // Given
      const paymentStatusPollingData = createPaymentStatusPollingData();
      await paymentStatusPollingRepository.create(paymentStatusPollingData);

      // When
      const result = await paymentStatusPollingRepository.getPaymentStatusPollingFromDB(
        paymentStatusPollingData.orderId
      );

      // Then
      expect(result?.orderId).toBeDefined();
      expect(result?.status).toEqual(paymentStatusPollingData.status);
      expect(result?.statusRetryCount).toEqual(
        paymentStatusPollingData.statusRetryCount
      );
      expect(result?.nextPollTime).toEqual(
        paymentStatusPollingData.nextPollTime
      );
    });

    it('should give null result when order id is not present', async () => {
      // Given
      const paymentStatusPollingData = createPaymentStatusPollingData();
      await paymentStatusPollingRepository.create(paymentStatusPollingData);

      // When
      const result = await paymentStatusPollingRepository.getPaymentStatusPollingFromDB(
        'any'
      );

      // Then
      expect(result).toBeNull();
    });
  });

  describe('update', () => {
    it('should not throw error if valid', async () => {
      // Given
      const paymentStatusPollingData = createPaymentStatusPollingData();
      await paymentStatusPollingRepository.create(paymentStatusPollingData);

      //when
      const input: IUpdatePaymentStatusPolling = {
        orderId: paymentStatusPollingData.orderId,
        status: BillPaymentTransactionStatusEnum.SUCCESS,
        nextPollTime: new Date(2020, 2, 10, 10, 33, 30, 0)
      };

      const result = await paymentStatusPollingRepository.updateWithIncrement(
        input
      );

      // Then
      expect(result?.orderId).toBeDefined();
      expect(result?.status).toEqual(input.status);
      expect(result?.statusRetryCount).toEqual(
        paymentStatusPollingData.statusRetryCount + 1
      );
      expect(result?.nextPollTime).toEqual(input.nextPollTime);
    });

    it('should not update anything', async () => {
      // Given
      const paymentStatusPollingData = createPaymentStatusPollingData();
      await paymentStatusPollingRepository.create(paymentStatusPollingData);

      //when
      const input: IUpdatePaymentStatusPolling = {
        orderId: paymentStatusPollingData.orderId
      };

      const result = await paymentStatusPollingRepository.updateWithIncrement(
        input
      );

      // Then
      expect(result?.orderId).toBeDefined();
      expect(result?.status).toEqual(paymentStatusPollingData.status);
      expect(result?.statusRetryCount).toEqual(
        paymentStatusPollingData.statusRetryCount + 1
      );
      expect(result?.nextPollTime).toEqual(
        paymentStatusPollingData.nextPollTime
      );
    });

    it('should return null when payment status polling config not found', async () => {
      // Given
      const paymentStatusPollingData = createPaymentStatusPollingData();
      await paymentStatusPollingRepository.create(paymentStatusPollingData);

      //when
      const input: IUpdatePaymentStatusPolling = {
        orderId: 'any',
        status: BillPaymentTransactionStatusEnum.SUCCESS
      };

      const result = await paymentStatusPollingRepository.updateWithIncrement(
        input
      );

      // Then
      expect(result).toBeNull();
    });
  });

  describe('getPendingRecords', () => {
    it('should get payment status polling detail within date range with Waiting status', async () => {
      // Given
      await Promise.all([
        paymentStatusPollingRepository.create(createPaymentStatusPollingData()),
        paymentStatusPollingRepository.create(
          createPaymentStatusPollingData2()
        ),
        paymentStatusPollingRepository.create(
          createPaymentStatusPollingData3()
        ),
        await paymentStatusPollingRepository.create(
          createPaymentStatusPollingData4()
        )
      ]);
      // When
      const endDate = new Date(2020, 3, 20, 10, 40, 0, 0);

      const result = await paymentStatusPollingRepository.getPendingRecords(
        BillPaymentTransactionStatusEnum.WAITING,
        endDate
      );

      // Then
      expect(result).toBeDefined();
      expect(result.length).toEqual(2);
      for (const paymentStatus of result) {
        expect(paymentStatus.status).toEqual(
          BillPaymentTransactionStatusEnum.WAITING
        );
      }
    });

    it('should give empty result when pending orders not found', async () => {
      // Given
      const paymentStatusPollingData = createPaymentStatusPollingData();
      await paymentStatusPollingRepository.create(paymentStatusPollingData);

      // When
      const endDate = new Date(2020, 3, 20, 10, 40, 0, 0);

      const result = await paymentStatusPollingRepository.getPendingRecords(
        BillPaymentTransactionStatusEnum.SUCCESS,
        endDate
      );

      // Then
      expect(result.length).toEqual(0);
    });
  });
});
