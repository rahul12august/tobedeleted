export default {
  create: jest.fn(),
  getPaymentStatusPollingFromDB: jest.fn(),
  updateWithIncrement: jest.fn(),
  getPendingRecords: jest.fn()
};
