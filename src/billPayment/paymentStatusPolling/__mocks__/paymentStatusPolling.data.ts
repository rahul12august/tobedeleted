import { IPaymentStatusPolling } from '../paymentStatusPolling.type';
import { BillPaymentTransactionStatusEnum } from '../../billPaymentTransaction/billPaymentTransaction.enum';
import { Enum } from '@dk/module-common';
import { IPollingConfig } from '../../../common/common.type';

export const createPaymentStatusPollingData = (): IPaymentStatusPolling => ({
  orderId: 'REF123',
  status: BillPaymentTransactionStatusEnum.WAITING,
  statusRetryCount: 0,
  nextPollTime: new Date(2020, 3, 20, 10, 33, 30, 0)
});

export const createPaymentStatusPollingData2 = (): IPaymentStatusPolling => ({
  orderId: 'REF1234',
  nextPollTime: new Date(2020, 3, 20, 10, 35, 30, 0),
  status: BillPaymentTransactionStatusEnum.WAITING,
  statusRetryCount: 1
});

export const createPaymentStatusPollingData3 = (): IPaymentStatusPolling => ({
  orderId: 'REF12345',
  status: BillPaymentTransactionStatusEnum.FAILURE,
  statusRetryCount: 1,
  nextPollTime: new Date(2020, 3, 20, 10, 38, 30, 0)
});

export const createPaymentStatusPollingData4 = (): IPaymentStatusPolling => ({
  orderId: 'REF123456',
  status: BillPaymentTransactionStatusEnum.WAITING,
  statusRetryCount: 1,
  nextPollTime: new Date(2020, 3, 20, 10, 40, 30, 0)
});

export const updatePaymentStatusPollingData = (): IPaymentStatusPolling => ({
  orderId: 'REF123',
  status: BillPaymentTransactionStatusEnum.SUCCESS,
  statusRetryCount: 2,
  nextPollTime: new Date(2020, 4, 25, 4, 30, 30, 0)
});

export const pollingConfigs: IPollingConfig[] = [
  {
    type: Enum.BillingAggregatorPollingConfigType.RELATIVE,
    delayTime: '300'
  },
  {
    type: Enum.BillingAggregatorPollingConfigType.RELATIVE,
    delayTime: '300'
  },
  {
    type: Enum.BillingAggregatorPollingConfigType.ABSOLUTE,
    delayTime: '17 15 10'
  }
];
