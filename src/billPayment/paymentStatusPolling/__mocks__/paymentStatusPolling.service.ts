export default {
  updatePollingConfig: jest.fn(),
  getPendingRecords: jest.fn(),
  updatePaymentStatusPollingConfig: jest.fn()
};
