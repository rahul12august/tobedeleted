import { BillPaymentTransactionStatusEnum } from '../billPaymentTransaction/billPaymentTransaction.enum';

interface IPaymentStatusPolling {
  orderId: string;
  statusRetryCount: number;
  nextPollTime: Date;
  status: BillPaymentTransactionStatusEnum;
}

interface IUpdatePaymentStatusPolling {
  orderId: string;
  nextPollTime?: Date;
  status?: BillPaymentTransactionStatusEnum;
}

export { IPaymentStatusPolling, IUpdatePaymentStatusPolling };
