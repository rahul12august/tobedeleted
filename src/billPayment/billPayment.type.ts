import hapi from '@hapi/hapi';
import { BillPaymentTransactionStatusEnum } from './billPaymentTransaction/billPaymentTransaction.enum';

export interface IManualAdjustmentRequest extends hapi.Request {
  payload: {
    transactionId: string;
    status: BillPaymentTransactionStatusEnum;
  };
}
