import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import ResponseWrapper from '../../plugins/responseWrapper';
import errorHandler from '../../common/handleValidationErrors';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import billPaymentController from '../billPayment.controller';
import billPaymentService from '../billPayment.service';
import { BillPaymentTransactionStatusEnum } from '../billPaymentTransaction/billPaymentTransaction.enum';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

jest.mock('../billPayment.service');

describe('BillPayment Controller', () => {
  let server: hapi.Server;
  beforeAll(async () => {
    server = new hapi.Server({
      routes: {
        validate: {
          options: {
            abortEarly: false
          },
          failAction: errorHandler
        }
      }
    });
    server.register([ResponseWrapper]);
    server.route(billPaymentController);
  });

  describe('update bill payment status', () => {
    it('should respond 202', async () => {
      // Given
      const options = {
        method: Http.Method.PUT,
        url: '/private/jobs/bill-payment/status-update'
      };

      (billPaymentService.updatePendingTransactions as jest.Mock).mockResolvedValueOnce(
        true
      );
      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.ACCEPTED);
    });

    it('should respond 400', async () => {
      // Given
      const options = {
        method: Http.Method.PUT,
        url: '/private/jobs/bill-payment/status-update'
      };

      (billPaymentService.updatePendingTransactions as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          `Test request.`,
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.INVALID_REQUEST
        )
      );
      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.BAD_REQUEST);
    });
  });

  describe('manual adjustment', async () => {
    it('should respond 200', async () => {
      // Given
      const options = {
        method: Http.Method.POST,
        url: '/bill-payment/manual-adjustment',
        payload: {
          transactionId: 'dummy123',
          status: BillPaymentTransactionStatusEnum.FAILURE
        }
      };

      (billPaymentService.txnManualAdjustment as jest.Mock).mockResolvedValueOnce(
        true
      );
      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toEqual(Http.StatusCode.OK);
    });

    it('should respond 400', async () => {
      // Given
      const options = {
        method: Http.Method.POST,
        url: '/bill-payment/manual-adjustment',
        payload: {
          transactionId: 'dummy123',
          status: BillPaymentTransactionStatusEnum.FAILURE
        }
      };

      (billPaymentService.txnManualAdjustment as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          `Test request.`,
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.INVALID_REQUEST
        )
      );
      // When
      const response: any = await server.inject(options);

      // Then
      expect(response.statusCode).toBe(Http.StatusCode.BAD_REQUEST);
    });
  });
});
