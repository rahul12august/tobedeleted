import billPaymentService from '../billPayment.service';
import paymentStatusPollingService from '../paymentStatusPolling/paymentStatusPolling.service';
import billingAggregatorService from '../../billingAggregator/billingAggregator.service';
import {
  createPaymentStatusPollingData,
  createPaymentStatusPollingData2
} from '../paymentStatusPolling/__mocks__/paymentStatusPolling.data';
import { BillPaymentTransactionStatusEnum } from '../billPaymentTransaction/billPaymentTransaction.enum';
import { getBillPaymentTransaction } from '../billPaymentTransaction/__mocks__/billPaymentTransaction.data';
import { ERROR_CODE } from '../../common/errors';
import goBillsService from '../../billingAggregator/goBills/goBills.service';
import billPaymentTxnService from '../billPaymentTransaction/billPaymentTransaction.service';
import transactionService from '../../transaction/transaction.service';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../../transaction/transaction.enum';
import { IPaymentStatusPolling } from '../paymentStatusPolling/paymentStatusPolling.type';
import { TransferAppError } from '../../errors/AppError';

jest.mock('../../transaction/transaction.service');
jest.mock('../paymentStatusPolling/paymentStatusPolling.service');
jest.mock('../../billingAggregator/billingAggregator.service');
jest.mock('../billPaymentTransaction/billPaymentTransaction.service');
jest.mock('../../billingAggregator/goBills/goBills.service');

describe('bill payment service', () => {
  afterEach(() => {
    jest.resetAllMocks();
    expect.hasAssertions();
  });

  describe('#updatePendingTransactions', () => {
    it('should not update anything if no pending transactions in given range', async () => {
      // Given
      const pendingRecords: IPaymentStatusPolling[] = [];
      (paymentStatusPollingService.getPendingRecords as jest.Mock).mockResolvedValueOnce(
        pendingRecords
      );
      // When
      await billPaymentService.updatePendingTransactions();

      // Then
      expect(paymentStatusPollingService.getPendingRecords).toHaveBeenCalled();
      expect(
        billingAggregatorService.updateBillPaymentStatus
      ).not.toHaveBeenCalled();
    });

    it('should update pending transactions to success in the given range', async () => {
      // Given
      const pendingRecords = [
        createPaymentStatusPollingData(),
        createPaymentStatusPollingData2()
      ];
      (paymentStatusPollingService.getPendingRecords as jest.Mock).mockResolvedValueOnce(
        pendingRecords
      );
      (billingAggregatorService.updateBillPaymentStatus as jest.Mock).mockResolvedValue(
        getBillPaymentTransaction()
      );
      // When
      await billPaymentService.updatePendingTransactions();

      // Then
      expect(paymentStatusPollingService.getPendingRecords).toHaveBeenCalled();
      expect(
        billingAggregatorService.updateBillPaymentStatus
      ).toHaveBeenCalledTimes(2);
    });

    it('should throw an error when updtaing transaction fails', async () => {
      // Given
      const pendingRecords = [
        createPaymentStatusPollingData(),
        createPaymentStatusPollingData2()
      ];
      (paymentStatusPollingService.getPendingRecords as jest.Mock).mockResolvedValueOnce(
        pendingRecords
      );
      (billingAggregatorService.updateBillPaymentStatus as jest.Mock)
        .mockResolvedValueOnce(getBillPaymentTransaction())
        .mockRejectedValueOnce('bad request!!');
      // When
      await billPaymentService.updatePendingTransactions().catch(err => err);

      // Then
      expect(paymentStatusPollingService.getPendingRecords).toHaveBeenCalled();
      expect(
        billingAggregatorService.updateBillPaymentStatus
      ).toHaveBeenCalledTimes(2);
    });
  });

  describe('txnManualAdjustment', () => {
    it('should update the txn to success', async () => {
      // Given
      const txnId = 'dummy123';
      const txn = { ...getBillPaymentTransaction(), transactionId: txnId };
      txn.status[txn.status.length - 1].status =
        BillPaymentTransactionStatusEnum.WAITING;
      (billPaymentTxnService.getByTxnId as jest.Mock).mockResolvedValueOnce(
        txn
      );
      (goBillsService.updateTransactionWithResult as jest.Mock).mockResolvedValueOnce(
        txn
      );
      (transactionService.findByIdAndUpdate as jest.Mock).mockResolvedValueOnce(
        txn
      );
      (paymentStatusPollingService.updatePaymentStatusPollingConfig as jest.Mock).mockResolvedValueOnce(
        {}
      );
      // When
      await billPaymentService.txnManualAdjustment(
        txnId,
        TransactionStatus.SUCCEED
      );
      // Then
      expect(billPaymentTxnService.getByTxnId).toHaveBeenCalledWith(txnId);
      expect(goBillsService.updateTransactionWithResult).toHaveBeenCalledWith(
        expect.objectContaining({
          orderId: txn.orderId,
          status: BillPaymentTransactionStatusEnum.SUCCESS,
          amount: txn.amount.toString(),
          refNumber: txn.referenceId
        }),
        txn.orderId,
        true
      );
      expect(transactionService.findByIdAndUpdate).toBeCalledWith(txnId, {
        manuallyAdjusted: true
      });
      expect(
        paymentStatusPollingService.updatePaymentStatusPollingConfig
      ).toBeCalledWith(txnId, BillPaymentTransactionStatusEnum.SUCCESS);
    });

    it('should update failed txn to success', async () => {
      // Given
      const txnId = 'dummy123';
      const txn = { ...getBillPaymentTransaction(), transactionId: txnId };
      txn.status[txn.status.length - 1].status =
        BillPaymentTransactionStatusEnum.FAILURE;
      (billPaymentTxnService.getByTxnId as jest.Mock).mockResolvedValueOnce(
        txn
      );
      (goBillsService.updateTransactionWithResult as jest.Mock).mockResolvedValueOnce(
        txn
      );
      (transactionService.findByIdAndUpdate as jest.Mock).mockResolvedValueOnce(
        txn
      );
      (paymentStatusPollingService.updatePaymentStatusPollingConfig as jest.Mock).mockResolvedValueOnce(
        {}
      );
      // When
      await billPaymentService.txnManualAdjustment(
        txnId,
        TransactionStatus.SUCCEED,
        false
      );
      // Then
      expect(billPaymentTxnService.getByTxnId).toHaveBeenCalledWith(txnId);
      expect(goBillsService.updateTransactionWithResult).toHaveBeenCalledWith(
        expect.objectContaining({
          orderId: txn.orderId,
          status: BillPaymentTransactionStatusEnum.SUCCESS,
          amount: txn.amount.toString(),
          refNumber: txn.referenceId
        }),
        txn.orderId,
        false
      );
      expect(transactionService.findByIdAndUpdate).toBeCalledWith(txnId, {
        manuallyAdjusted: true,
        status: TransactionStatus.SUCCEED
      });
      expect(
        paymentStatusPollingService.updatePaymentStatusPollingConfig
      ).not.toHaveBeenCalled();
    });

    it('should update the txn to failed', async () => {
      // Given
      const txnId = 'dummy123';
      const txn = { ...getBillPaymentTransaction(), transactionId: txnId };
      txn.status[txn.status.length - 1].status =
        BillPaymentTransactionStatusEnum.WAITING;
      (billPaymentTxnService.getByTxnId as jest.Mock).mockResolvedValueOnce(
        txn
      );
      (goBillsService.updateTransactionWithResult as jest.Mock).mockResolvedValueOnce(
        txn
      );
      (transactionService.findByIdAndUpdate as jest.Mock).mockResolvedValueOnce(
        txn
      );
      (paymentStatusPollingService.updatePaymentStatusPollingConfig as jest.Mock).mockResolvedValueOnce(
        {}
      );
      // When
      await billPaymentService.txnManualAdjustment(
        txnId,
        TransactionStatus.DECLINED
      );
      // Then
      expect(billPaymentTxnService.getByTxnId).toHaveBeenCalledWith(txnId);
      expect(goBillsService.updateTransactionWithResult).toHaveBeenCalledWith(
        expect.objectContaining({
          orderId: txn.orderId,
          status: BillPaymentTransactionStatusEnum.FAILURE,
          amount: txn.amount.toString(),
          refNumber: txn.referenceId
        }),
        txn.orderId,
        true
      );
      expect(transactionService.findByIdAndUpdate).toBeCalledWith(txnId, {
        manuallyAdjusted: true
      });
      expect(
        paymentStatusPollingService.updatePaymentStatusPollingConfig
      ).toBeCalledWith(txnId, BillPaymentTransactionStatusEnum.FAILURE);
    });

    it('should throw txn not found error', async () => {
      (billPaymentTxnService.getByTxnId as jest.Mock).mockResolvedValueOnce(
        null
      );
      try {
        await billPaymentService.txnManualAdjustment(
          'dummy123',
          TransactionStatus.SUCCEED
        );
      } catch (error) {
        expect(error.errorCode).toBe(ERROR_CODE.TRANSACTION_NOT_FOUND);
      }
    });

    it('should throw error when transaction update fails', async () => {
      const transactionId = 'test0001';
      const transaction = {
        ...getBillPaymentTransaction(),
        transactionId: transactionId
      };
      (billPaymentTxnService.getByTxnId as jest.Mock).mockResolvedValueOnce(
        transaction
      );
      (goBillsService.updateTransactionWithResult as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.UNEXPECTED_ERROR
        )
      );
      const error = await billPaymentService
        .txnManualAdjustment(transactionId, TransactionStatus.SUCCEED)
        .catch(error => error);

      expect(error).toBeDefined();
      expect(error.actor).toBe(TransferFailureReasonActor.UNKNOWN);
      expect(error.detail).toBe('Test error!');
      expect(error.errorCode).toBe(ERROR_CODE.UNEXPECTED_ERROR);
    });
  });
});
