import { Enum } from '@dk/module-common';
import { HOLIDAY_DATE_RANGE } from './billPayment.constant';
import configurationRepository from '../configuration/configuration.repository';
import { IHoliday } from '../configuration/configuration.type';
import { IPollingConfig } from '../common/common.type';

const getStartOfTheDay = (date: Date): Date => {
  date.setHours(0, 0, 0, 0);
  return date;
};

const isBefore = (fromDate: Date, toDate: Date): boolean =>
  getStartOfTheDay(fromDate) < getStartOfTheDay(toDate);

const getNextDate = (): Date => {
  const nextDate = getStartOfTheDay(new Date());
  nextDate.setDate(nextDate.getDate() + 1);
  return nextDate;
};

const addDays = (time: number, noOfDaysToAdd: number): Date => {
  const days = noOfDaysToAdd * 60 * 60 * 24 * 1000;
  return new Date(time + days);
};

const getNextWorkingDay = async (startDate: Date): Promise<Date> => {
  const endDate = addDays(startDate.getTime(), HOLIDAY_DATE_RANGE);

  const holidayList: IHoliday[] = await configurationRepository.getHolidaysByDateRange(
    startDate,
    endDate
  );

  for (let i = 0; i < holidayList.length; i++) {
    if (isBefore(startDate, new Date(holidayList[i].fromDate))) {
      return startDate;
    }
    startDate = addDays(new Date(holidayList[i].toDate).getTime(), 1);
  }

  return startDate;
};

export const getNextTime = (
  baseDate: Date,
  hours: number,
  minutes: number,
  seconds: number
): Date =>
  new Date(
    baseDate.getFullYear(),
    baseDate.getMonth(),
    baseDate.getDate(),
    hours,
    minutes,
    seconds
  );

const getNextRelativePollTime = (delayToBeAdded: number): Date => {
  const delayedMs = 1000 * delayToBeAdded;
  return new Date(new Date().getTime() + delayedMs);
};

const getNextAbsolutePollTime = async (delay: string): Promise<Date> => {
  const time = delay.split(' ');
  const nextDate = getNextDate();
  const nextWorkingDate = await getNextWorkingDay(nextDate);
  return getNextTime(
    nextWorkingDate,
    Number(time[0]),
    Number(time[1]),
    Number(time[2])
  );
};

export const getNextPollTime = async (
  pollingConfig: IPollingConfig
): Promise<Date> =>
  pollingConfig.type === Enum.BillingAggregatorPollingConfigType.RELATIVE
    ? getNextRelativePollTime(Number(pollingConfig.delayTime))
    : await getNextAbsolutePollTime(pollingConfig.delayTime);
