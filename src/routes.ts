import healthcheck from './healthcheck/healthcheck.controller';
import feeController from './fee/fee.controller';
import transferController from './transaction/transaction.controller';
import billPaymentController from './billPayment/billPayment.controller';
import irisController from './iris/iris.controller';
import mambuBlockingConfigController from './mambuBlockingConfig/mambuBlockingConfig.controller';
import tokopediaController from './tokopedia/tokopedia.controller';
import altoController from './alto/alto.controller';
import bifastController from './bifast/bifast.controller';
import transactionUsageController from './transactionUsage/transactionUsage.controller';

const routes = [
  ...healthcheck,
  ...feeController,
  ...transferController,
  ...billPaymentController,
  ...irisController,
  ...mambuBlockingConfigController,
  ...tokopediaController,
  ...altoController,
  ...bifastController,
  ...transactionUsageController
];

export { routes };
