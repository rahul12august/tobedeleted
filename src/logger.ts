import { createLogger, LevelEnum } from '@dk/module-logger';
import { Logger } from '@dk/module-common';

import { config } from './config';
import { context } from './context';
import { MICROSERVICE_NAME } from './common/constant';

const level = config.get('logLevel') || LevelEnum.warn;

const logger = createLogger({
  defaultMeta: {
    service: MICROSERVICE_NAME
  },
  tracing: context,
  level
});
export const wrapLogs = new Logger.BaseLogWrapper(logger).getWrapper();

export default logger;
