import { prefixValueCol } from './decisionEngine.constant';
import { IDecisionEngineRule } from './decisionEngine.type';
import { get } from 'lodash';

export const isValueGetFromField = (value?: string): any =>
  value && value.indexOf(prefixValueCol) > -1;

export const getValueFormOtherField = (input: object, value?: any): any =>
  value && get(input, value.replace(prefixValueCol, ''));

export const getValue = ({ value }: IDecisionEngineRule, input: object): any =>
  isValueGetFromField(String(value))
    ? getValueFormOtherField(input, String(value))
    : value;

export const getValueWithInCondition = ({ values }: IDecisionEngineRule): any =>
  values;
