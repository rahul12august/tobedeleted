import { IDecisionEngineRule } from './decisionEngine.type';
import {
  decisionEngineGetValueFunction,
  DecisionEngineOperator,
  decisionEngineVerifyFunction
} from './decisionEngine.constant';
import { ERROR_CODE } from '../common/errors';
import _ from 'lodash';
import { TransferAppError } from '../errors/AppError';
import logger from '../logger';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

export class DecisionEngineRules<T> {
  obj: T;
  role: IDecisionEngineRule;
  propertyValue?: any;
  valueData?: any;

  constructor(obj: T, role: IDecisionEngineRule) {
    this.obj = obj;
    this.role = role;
  }

  checkOperator() {
    if (Object.values(DecisionEngineOperator).includes(this.role.operator)) {
      return this;
    }
    const detail = `Decision engine operator: ${this.role.operator} is not supported!`;
    logger.error(`checkOperator: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.DECISION_ENGINE_OPERATOR_NOT_SUPPORT
    );
  }

  getPropertyValue() {
    this.propertyValue = _.get(this.obj, this.role.property);
    return this;
  }

  getValueData() {
    this.valueData = decisionEngineGetValueFunction[this.role.operator](
      this.role,
      this.obj
    );
    return this;
  }

  swapParams() {
    if (this.role.operator === DecisionEngineOperator.IN) {
      const temp = this.propertyValue;
      this.propertyValue = this.valueData;
      this.valueData = temp;
    }
    return this;
  }

  executeRule() {
    if (!this.role.operator || !this.propertyValue || !this.valueData) {
      return false;
    }
    return decisionEngineVerifyFunction[this.role.operator](
      this.propertyValue,
      this.valueData
    );
  }

  run() {
    return this.checkOperator()
      .getPropertyValue()
      .getValueData()
      .swapParams()
      .executeRule();
  }
}
