import { isEqual, includes, lte } from 'lodash';
import { IDecisionEngineFunction } from './decisionEngine.type';
import { getValue, getValueWithInCondition } from './decisionEngine.util';

export const DecisionEngineOperator = {
  EQUAL: 'EQUAL',
  IN: 'IN',
  LTE: 'LTE'
};

export const decisionEngineVerifyFunction: IDecisionEngineFunction = {
  [DecisionEngineOperator.EQUAL]: isEqual,
  [DecisionEngineOperator.IN]: includes,
  [DecisionEngineOperator.LTE]: lte
};

export const decisionEngineGetValueFunction: IDecisionEngineFunction = {
  [DecisionEngineOperator.EQUAL]: getValue,
  [DecisionEngineOperator.IN]: getValueWithInCondition,
  [DecisionEngineOperator.LTE]: getValue
};

export const prefixValueCol = '#';
export const emptyString = '';
