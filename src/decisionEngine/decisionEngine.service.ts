import logger, { wrapLogs } from '../logger';
import { IDecisionEngineConfigRole } from './decisionEngine.type';
import { DecisionEngineRules } from './decisionEngine.class';

const runRole = <T>(
  configRole: IDecisionEngineConfigRole,
  input: T
): boolean => {
  logger.info('Run DC engine Rule', configRole);
  const verifiedRules = configRole.rules.map(rule =>
    new DecisionEngineRules<T>(input, rule).run()
  );
  logger.info('Result DC engine Rule ' + configRole.description, verifiedRules);
  return verifiedRules.every(verifiedItem => verifiedItem === true);
};

const runMultipleRoles = <T>(
  configRoles: IDecisionEngineConfigRole[],
  input: T
): boolean => {
  logger.info(`start run DC engine with ${configRoles.length} configRoles`);
  const verifiedRoles = configRoles.map(configRole =>
    runRole(configRole, input)
  );
  logger.info('result run DC engine', verifiedRoles);
  return verifiedRoles.some(verifiedItem => verifiedItem === true);
};

export default wrapLogs({
  runRole,
  runMultipleRoles
});
