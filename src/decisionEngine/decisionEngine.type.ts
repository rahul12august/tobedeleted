export interface IDecisionEngineFunction {
  [index: string]: Function;
}

export interface IDecisionEngineRule {
  property: string;
  operator: string;
  value?: any;
  values?: any[];
}

export interface IDecisionEngineConfigRole {
  description: string;
  serviceCode: string;
  rules: IDecisionEngineRule[];
}
