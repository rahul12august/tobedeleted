import { getMockTransactionInput } from '../__mocks__/decisionEngine.data';
import decisionEngineService from '../decisionEngine.service';
import { ITransactionInput } from '../../transaction/transaction.type';
import { getDecisionEngineConfiguration } from '../../configuration/__mocks__/configuration.data';
import _ from 'lodash';
import { ERROR_CODE } from '../../common/errors';
import { IDecisionEngineConfigRole } from '../decisionEngine.type';

describe('decisionEngine.service', () => {
  let transactionInput;

  afterEach(() => {
    jest.resetAllMocks();
  });

  beforeEach(() => {
    transactionInput = getMockTransactionInput();
  });

  describe('runRole', () => {
    it('Should return true when transaction input is correct', () => {
      // Given
      const decisionEngineConfiguration = getDecisionEngineConfiguration();

      // When
      const result = decisionEngineService.runRole<ITransactionInput>(
        decisionEngineConfiguration,
        transactionInput
      );

      // Then
      expect(result).toEqual(true);
    });

    it('Should return false when transaction input is not matching with all roles', () => {
      // Given
      const decisionEngineConfiguration = getDecisionEngineConfiguration();
      transactionInput.sourceAccountType = 'DC';

      // When
      const result = decisionEngineService.runRole<ITransactionInput>(
        decisionEngineConfiguration,
        transactionInput
      );

      // Then
      expect(result).toEqual(false);
    });

    it.each([
      [
        false,
        [
          {
            property: 'transactionAmount',
            operator: 'LTE',
            value: false
          }
        ]
      ],
      [
        true,
        [
          {
            property: 'transactionAmount',
            operator: 'LTE',
            value: 1000
          }
        ]
      ],
      [
        false,
        [
          {
            property: 'transactionAmount',
            operator: 'LTE',
            value: undefined
          }
        ]
      ],
      [
        false,
        [
          {
            property: 'transactionAmount',
            operator: 'LTE',
            value: 'tests'
          }
        ]
      ]
    ])('Should return %s when config is %j', (expected: any, rules: any) => {
      // Given
      const decisionEngineConfiguration = {
        rules: rules
      };

      // When
      const result = decisionEngineService.runRole<ITransactionInput>(
        decisionEngineConfiguration as IDecisionEngineConfigRole,
        transactionInput
      );

      // Then
      expect(result).toEqual(expected);
    });

    it('Should throw error when config return operator is not exists', () => {
      // Given
      const decisionEngineConfiguration = {
        functionality: 'TRANSFER',
        description: 'Internal JAGO Transfer',
        serviceCode: 'RTOL_WALLET',
        rules: [
          {
            property: 'sourceAccountType',
            operator: 'TEST',
            value: 'FS'
          }
        ]
      };
      transactionInput.sourceAccountType = 'DC';

      // When
      try {
        decisionEngineService.runRole<ITransactionInput>(
          decisionEngineConfiguration,
          transactionInput
        );
      } catch (error) {
        // Then
        expect(error.errorCode).toEqual(
          ERROR_CODE.DECISION_ENGINE_OPERATOR_NOT_SUPPORT
        );
      }
    });
  });

  describe('runMultipleRoles', () => {
    it('Should return true when transaction input is correct one of rules', () => {
      // Given
      const decisionEngineConfiguration = getDecisionEngineConfiguration();
      const failRules = [...decisionEngineConfiguration.rules];
      failRules.push({
        property: 'sourceAccountName',
        operator: 'EQUAL',
        value: 'TEST'
      });
      const decisionEngineConfigurations = [
        decisionEngineConfiguration,
        {
          ...decisionEngineConfiguration,
          rules: failRules
        }
      ];

      // When
      const result = decisionEngineService.runMultipleRoles<ITransactionInput>(
        decisionEngineConfigurations,
        transactionInput
      );

      // Then
      expect(result).toEqual(true);
    });

    it('Should return false when transaction input is not matching with all rules', () => {
      // Given
      const decisionEngineConfigurations = [getDecisionEngineConfiguration()];
      transactionInput.sourceAccountType = 'DC';

      // When
      const result = decisionEngineService.runMultipleRoles<ITransactionInput>(
        decisionEngineConfigurations,
        transactionInput
      );

      // Then
      expect(result).toEqual(false);
    });
  });
});
