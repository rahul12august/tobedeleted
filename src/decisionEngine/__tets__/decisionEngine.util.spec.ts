import {
  getValueFormOtherField,
  getValue,
  getValueWithInCondition
} from '../decisionEngine.util';
import { IDecisionEngineRule } from '../decisionEngine.type';

describe('decisionEngine.util', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  beforeEach(() => {});

  describe('getValueFormOtherField', () => {
    it.each([
      ['FS', { sourceAccountType: 'FS' }, 'sourceAccountType'],
      [undefined, { sourceAccountType: 'FS' }, 'sourceAccountName'],
      [
        'sourceAccountType',
        { sourceAccountType: 'sourceAccountType', sourceAccountName: 'FS' },
        '#sourceAccountType'
      ],
      [10, { transactionAmount: 10 }, '#transactionAmount']
    ])(
      'Should return %s when transaction input is %s and property is %s',
      (expectResult: any, input, property) => {
        // When
        const result = getValueFormOtherField(
          input as object,
          property as string
        );

        // Then
        expect(result).toEqual(expectResult);
      }
    );
  });

  describe('getValue', () => {
    it.each([
      [
        'test',
        { sourceAccountType: 'FS' },
        {
          property: 'sourceAccountType',
          operator: 'EQUAL',
          value: 'test'
        }
      ],
      [
        undefined,
        { sourceAccountType: 'FS' },
        {
          property: 'sourceAccountType',
          operator: 'EQUAL',
          value: undefined
        }
      ],
      [
        undefined,
        { sourceAccountType: 'FS' },
        {
          property: 'sourceAccountName',
          operator: 'EQUAL',
          value: undefined
        }
      ],
      [
        1,
        { sourceAccountType: 'FS' },
        {
          property: 'sourceAccountName',
          operator: 'LTE',
          value: 1
        }
      ]
    ])(
      'Should return %s when transaction input is %s and role is %s',
      (expectResult: any, input, role) => {
        // When
        const result = getValue(role as IDecisionEngineRule, input as object);

        // Then
        expect(result).toEqual(expectResult);
      }
    );
  });

  describe('getValueWithInCondition', () => {
    it.each([
      [
        ['test'],
        {
          property: 'sourceAccountType',
          operator: 'IN',
          values: ['test']
        }
      ],
      [
        undefined,
        {
          property: 'sourceAccountType',
          operator: 'IN',
          values: undefined
        }
      ]
    ])(
      'Should return %s when transaction input is %s and role is %s',
      (expectResult, role) => {
        // When
        const result = getValueWithInCondition(role as IDecisionEngineRule);

        // Then
        expect(result).toEqual(expectResult);
      }
    );
  });
});
