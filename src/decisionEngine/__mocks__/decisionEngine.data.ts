import { getITransactionInput } from '../../transaction/__mocks__/transaction.data';
import { BankChannelEnum, Constant } from '@dk/module-common';

export const getMockTransactionInput = () => ({
  ...getITransactionInput(),
  sourceAccountType: 'FS',
  sourceBankCodeChannel: BankChannelEnum.INTERNAL,
  sourceBankCode: Constant.GIN_BANK_CODE_ID,
  sourceTransactionCurrency: 'IDR',
  sourceCIF: 'cif',
  beneficiaryCIF: 'cif',
  transactionAmount: 999,
  limitAmount: 1001
});
