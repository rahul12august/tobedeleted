export default {
  runRole: jest.fn(),
  runMultipleRoles: jest.fn()
};
