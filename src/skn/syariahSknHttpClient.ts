import { config } from '../config';
import { context } from '../context';
import logger from '../logger';
import { HttpClient } from '@dk/module-httpclient';
import { Http } from '@dk/module-common';
import https from 'https';
import { retryConfig } from '../common/constant';
const { baseUrlSharia } = config.get('skn');

const syariahSknHttpClient: HttpClient = Http.createHttpClientForward({
  baseURL: baseUrlSharia,
  httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  context,
  logger,
  retryConfig: {
    ...retryConfig,
    networkErrorCodesToRetry: ['ENOENT', 'ECONNREFUSED']
  }
});

logger.info(`Base URL for SKN SHARIA: ${baseUrlSharia}`);

export default syariahSknHttpClient;
