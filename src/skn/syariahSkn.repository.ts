import syariahSknHttpClient from './syariahSknHttpClient';
import logger, { wrapLogs } from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import uuidv4 from 'uuid/v4';
import { updateRequestIdInRedis } from '../common/contextHandler';
import { ISknResponse, ISknTransaction } from './skn.type';
import { config } from '../config';
import { httpClientTimeoutErrorCode } from '../common/constant';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const { transactionUrl } = config.get('skn');

const submitTransaction = async (
  transaction: ISknTransaction
): Promise<ISknResponse> => {
  try {
    const externalKey = transaction.externalId || uuidv4();
    updateRequestIdInRedis(externalKey);
    logger.info(
      `Submitting SKN Syariah transaction external id : ${transaction.externalId}, paymentServiceType : ${transaction.paymentServiceType},
      transactionType: ${transaction.transactionType}, transactionChannel: ${transaction.transactionChannel}, beneficiaryBankCode: ${transaction.beneficiaryBankCode},
      beneficiaryAccountNo: ${transaction.beneficiaryAccountNo}, beneficiaryAccountName: ${transaction.beneficiaryAccountName},
      beneficiaryCustomerType: ${transaction.beneficiaryCustomerType}, sourceAccountNo: ${transaction.sourceAccountNo},
      sourceAccountName: ${transaction.sourceAccountName}`
    );
    const result = await syariahSknHttpClient.post(transactionUrl, transaction);
    if (!result.data && result.status !== 200) {
      const detail = `Error while transferring funds with status ${result.status}.`;
      logger.error(`SKN Syariah submitTransaction: ${detail}`);
      throw new TransferAppError(
        detail,
        result.status === 400 || result.status === 404
          ? TransferFailureReasonActor.MS_TRANSFER
          : TransferFailureReasonActor.SKN_SYARIAH,
        ERROR_CODE.ERROR_FROM_SKN_SHARIA
      );
    }
    return result.data;
  } catch (error) {
    const detail = `Error while transferring funds code : ${error.code} message: ${error.message}.`;
    logger.error(`SKN Syariah submitTransaction catch: ${detail}`);
    // only throw error if it's a timeout error
    if (
      (error.code && error.code === httpClientTimeoutErrorCode) ||
      error instanceof TransferAppError
    ) {
      throw error;
    }
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.ERROR_FROM_SKN_SHARIA
    );
  }
};

const syariahSknRepository = wrapLogs({
  submitTransaction
});

export default syariahSknRepository;
