import syariahSknHttpClient from '../syariahSknHttpClient';
import syariahSknRepository from '../syariahSkn.repository';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import { getMockedTransaction } from '../__mocks__/sknTransaction.data';
import { config } from '../../config';
import { httpClientTimeoutErrorCode } from '../../common/constant';
const { transactionUrl } = config.get('skn');

jest.mock('../syariahSknHttpClient');

describe('syariahSkn.repository', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('submitTransaction', () => {
    const sknTransaction = getMockedTransaction();
    it('should return skn syariah transaction success', async () => {
      (syariahSknHttpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          responseCode: 200,
          responseMessage: 'SUCCESS'
        }
      });

      const expected = await syariahSknRepository.submitTransaction(
        sknTransaction
      );

      expect(syariahSknHttpClient.post).toBeCalledWith(
        transactionUrl,
        sknTransaction
      );
      expect(expected).toEqual({
        responseCode: 200,
        responseMessage: 'SUCCESS'
      });
    });

    it(`should throw error from SKN when submit transaction`, async () => {
      (syariahSknHttpClient.post as jest.Mock).mockResolvedValueOnce({
        data: null
      });

      const expected = await syariahSknRepository
        .submitTransaction(sknTransaction)
        .catch(err => err);

      expect(expected).toBeInstanceOf(TransferAppError);
      expect(expected.errorCode).toBe(ERROR_CODE.ERROR_FROM_SKN_SHARIA);
    });

    it(`should throw error when skn api throws 408`, async () => {
      (syariahSknHttpClient.post as jest.Mock).mockRejectedValueOnce({
        message: 'HttpClient Error: Error: Request failed with status code 408',
        code: httpClientTimeoutErrorCode
      });

      const expected = await syariahSknRepository
        .submitTransaction(sknTransaction)
        .catch(err => err);

      expect(expected).not.toBeInstanceOf(TransferAppError);
      expect(expected.message).toBe(
        'HttpClient Error: Error: Request failed with status code 408'
      );
      expect(expected.code).toBe(httpClientTimeoutErrorCode);
    });
  });
});
