import { BankChannelEnum, BankNetworkEnum } from '@dk/module-common';
import switchingTransactionService from '../../transaction/transactionConfirmation.service';
import { getTransactionDetail } from '../../transaction/__mocks__/transaction.data';
import transactionRepository from '../../transaction/transaction.repository';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../../transaction/transaction.enum';
import sknService from '../skn.service';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import { formatTransactionDate } from '../../transaction/transaction.util';

jest.mock('../../transaction/transactionConfirmation.service');
jest.mock('../../transaction/transaction.repository');

describe('skn.service', () => {
  // given
  const transactionModel = {
    ...getTransactionDetail(),
    beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
    requireThirdPartyOutgoingId: true,
    debitTransactionCode: 'TFD60',
    thirdPartyOutgoingId: '21042T000041',
    transactionAmount: 50000002,
    paymentServiceCode: 'SKN'
  };

  const mockedSKNResponse = {
    responseCode: 200,
    responseMessage: 'SUCCESS'
  };
  describe('confirmSknTransaction', () => {
    it('should confirm the transaction to succeed when correct payload passed', async () => {
      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce({
        ...transactionModel,
        status: TransactionStatus.SUCCEED
      });
      const result = await sknService.confirmSknTransaction(
        transactionModel,
        true,
        mockedSKNResponse
      );

      expect(result).toBeDefined();
      expect(
        switchingTransactionService.confirmTransaction
      ).toHaveBeenCalledWith({
        externalId: transactionModel.thirdPartyOutgoingId,
        accountNo: transactionModel.sourceAccountNo,
        transactionDate: formatTransactionDate(transactionModel.createdAt),
        responseCode: 200,
        responseMessage: 'SUCCESS',
        interchange: BankNetworkEnum.SKN,
        transactionResponseID: undefined,
        amount: transactionModel.transactionAmount
      });
      expect(transactionRepository.getByKey).toHaveBeenCalledWith(
        transactionModel.id
      );
    });

    it('should throw the error when transaction not found', async () => {
      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(null);
      const error = await sknService
        .confirmSknTransaction(transactionModel, true, mockedSKNResponse)
        .catch(error => error);

      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual(
        `Failed to get transaction info for txnId ${transactionModel.id}`
      );
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_TRANSACTION_ID);
      expect(
        switchingTransactionService.confirmTransaction
      ).toHaveBeenCalledWith({
        externalId: transactionModel.thirdPartyOutgoingId,
        accountNo: transactionModel.sourceAccountNo,
        transactionDate: formatTransactionDate(transactionModel.createdAt),
        responseCode: 200,
        responseMessage: 'SUCCESS',
        interchange: BankNetworkEnum.SKN,
        transactionResponseID: undefined,
        amount: transactionModel.transactionAmount
      });
      expect(transactionRepository.getByKey).toHaveBeenCalledWith(
        transactionModel.id
      );
    });

    it('should confirm the transaction to declined when correct payload passed', async () => {
      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce({
        ...transactionModel,
        status: TransactionStatus.DECLINED
      });
      const result = await sknService.confirmSknTransaction(
        transactionModel,
        false
      );

      expect(result).toBeDefined();
      expect(
        switchingTransactionService.confirmTransaction
      ).toHaveBeenCalledWith({
        externalId: transactionModel.thirdPartyOutgoingId,
        accountNo: transactionModel.sourceAccountNo,
        transactionDate: formatTransactionDate(transactionModel.createdAt),
        responseCode: 400,
        interchange: BankNetworkEnum.SKN,
        amount: transactionModel.transactionAmount,
        transactionResponseID: undefined
      });
      expect(transactionRepository.getByKey).toHaveBeenCalledWith(
        transactionModel.id
      );
    });
    it('should confirm the transaction to SUCCESS when correct payload passed', async () => {
      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce({
        ...transactionModel,
        status: TransactionStatus.SUCCEED
      });
      const result = await sknService.confirmSknTransaction(
        transactionModel,
        true
      );

      expect(result).toBeDefined();
      expect(
        switchingTransactionService.confirmTransaction
      ).toHaveBeenCalledWith({
        externalId: transactionModel.thirdPartyOutgoingId,
        accountNo: transactionModel.sourceAccountNo,
        transactionDate: formatTransactionDate(transactionModel.createdAt),
        responseCode: 200,
        interchange: BankNetworkEnum.SKN,
        amount: transactionModel.transactionAmount,
        transactionResponseID: undefined
      });
      expect(transactionRepository.getByKey).toHaveBeenCalledWith(
        transactionModel.id
      );
    });
  });
});
