import syariahSknRepository from '../syariahSkn.repository';
import { BankChannelEnum, Rail, Util } from '@dk/module-common';
import { getTransactionDetail } from '../../transaction/__mocks__/transaction.data';
import syariahSknSubmissionTemplateService from '../syariahSknSubmissionTemplate.service';
import { mapToSknTransaction, validateMandatoryFields } from '../skn.helper';
import { getMockedTransaction } from '../__mocks__/sknTransaction.data';
import { ISknTransaction } from '../skn.type';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import transactionRepository from '../../transaction/transaction.repository';
import transactionProducer from '../../transaction/transaction.producer';
import { httpClientTimeoutErrorCode } from '../../common/constant';
import sknService from '../skn.service';
import {
  TransactionStatus,
  TransferFailureReasonActor
} from '../../transaction/transaction.enum';

jest.mock('../syariahSkn.repository');
jest.mock('../skn.helper');
jest.mock('../../transaction/transaction.repository');
jest.mock('../../transaction/transaction.producer');
jest.mock('../skn.service');

describe('syariahSknSubmissionTemplate', () => {
  class NetworkError extends Error {
    status: number;
    code?: string;
    constructor(status = 501, code?: string) {
      super();
      this.status = status;
      this.code = code;
    }
  }
  let mockedRetry = jest
    .spyOn(Util, 'retry')
    .mockImplementation(async (_, fn, ...args) => await fn(...args));

  // given
  const transactionModel = {
    ...getTransactionDetail(),
    beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
    requireThirdPartyOutgoingId: true,
    debitTransactionCode: 'TFD60',
    thirdPartyOutgoingId: '21042T000041',
    transactionAmount: 50000002,
    paymentServiceCode: 'SKN_SHARIA'
  };

  describe('isEligible check', () => {
    it(`should return true when paymentServiceCode is SKN`, () => {
      // when
      const eligible = syariahSknSubmissionTemplateService.isEligible(
        transactionModel
      );

      // then
      expect(eligible).toBeTruthy();
    });

    it(`should return false when beneficiaryBankCodeChannel is not ${BankChannelEnum.EXTERNAL}`, () => {
      // when
      const eligible = syariahSknSubmissionTemplateService.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL
      });

      // then
      expect(eligible).not.toBeTruthy();
    });

    it('should return false when beneficiaryAccountNo is undefined', async () => {
      // when
      const eligible = syariahSknSubmissionTemplateService.isEligible({
        ...transactionModel,
        beneficiaryAccountNo: undefined
      });

      // then
      expect(eligible).not.toBeTruthy();
    });

    it('should return false when paymentServiceCode is other than SKN_SHARIA', async () => {
      // when
      const eligible = syariahSknSubmissionTemplateService.isEligible({
        ...transactionModel,
        paymentServiceCode: 'SKN'
      });

      // then
      expect(eligible).not.toBeTruthy();
    });
  });

  describe('submitTransaction', () => {
    const mockedSKNModel: ISknTransaction = getMockedTransaction();
    const mockedSKNResponse = {
      externalId: '000021123T000039',
      accountNo: '100455054110',
      transactionDate: '210503000000',
      responseCode: 200,
      responseMessage: 'SUCCESS',
      interchange: 'SKN',
      transactionResponseID: '000000000001'
    };
    beforeEach(() => {
      (mapToSknTransaction as jest.Mock).mockReturnValue(mockedSKNModel);
      (syariahSknRepository.submitTransaction as jest.Mock).mockResolvedValue(
        mockedSKNResponse
      );
      (validateMandatoryFields as jest.Mock).mockReturnValue(null);
    });
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should submit transaction to skn with correct payload', async () => {
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...transactionModel
      });
      (sknService.confirmSknTransaction as jest.Mock).mockResolvedValueOnce({
        ...transactionModel,
        status: TransactionStatus.SUCCEED
      });
      const result = await syariahSknSubmissionTemplateService.submitTransaction(
        transactionModel
      );

      expect(result).toBeDefined();
      expect(syariahSknRepository.submitTransaction).toHaveBeenCalledWith(
        mockedSKNModel
      );
      expect(transactionRepository.update).toHaveBeenCalledWith(
        transactionModel.id,
        {
          status: TransactionStatus.SUBMITTED,
          thirdPartyOutgoingId: transactionModel.thirdPartyOutgoingId.padStart(
            16,
            '0'
          ),
          journey: [
            {
              status: 'THIRD_PARTY_SUBMITTED',
              updatedAt: expect.anything()
            }
          ]
        }
      );
      expect(sknService.confirmSknTransaction).toHaveBeenCalledWith(
        { ...transactionModel, thirdPartyOutgoingId: '21042T000041' },
        true,
        mockedSKNResponse
      );
    });

    it('should throw invalid transaction to skn when thirdparty outgoing id is missing', async () => {
      const model = {
        ...transactionModel,
        thirdPartyOutgoingId: undefined
      };
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce(model);
      (sknService.confirmSknTransaction as jest.Mock).mockResolvedValueOnce({
        ...transactionModel,
        status: TransactionStatus.DECLINED
      });
      const result = await syariahSknSubmissionTemplateService.submitTransaction(
        model
      );
      expect(result).toBeDefined();
      expect(validateMandatoryFields).toHaveBeenCalledWith(model);
      expect(mapToSknTransaction).not.toBeCalled();
      expect(syariahSknRepository.submitTransaction).not.toBeCalled();
      expect(transactionRepository.update).toHaveBeenCalledWith(
        transactionModel.id,
        {
          status: TransactionStatus.SUBMITTED,
          thirdPartyOutgoingId: model.thirdPartyOutgoingId,
          journey: [
            {
              status: 'THIRD_PARTY_SUBMITTED',
              updatedAt: expect.anything()
            }
          ]
        }
      );
      expect(sknService.confirmSknTransaction).toHaveBeenCalledWith(
        { ...transactionModel, thirdPartyOutgoingId: undefined },
        false,
        undefined
      );
    });

    it('should retry first times when SKN repository throw network error', async () => {
      const networkError = new NetworkError();
      const expectedNetworkError = {
        ...networkError,
        code: 'ECONNABORTED'
      };

      mockedRetry.mockRestore();
      (sknService.confirmSknTransaction as jest.Mock).mockResolvedValueOnce({
        ...transactionModel,
        status: TransactionStatus.SUCCEED
      });
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({
        ...transactionModel
      });
      (syariahSknRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        expectedNetworkError
      );

      const result = await syariahSknSubmissionTemplateService.submitTransaction(
        transactionModel
      );

      expect(result).toBeDefined();
      expect(syariahSknRepository.submitTransaction).toHaveBeenCalledTimes(1);
      expect(sknService.confirmSknTransaction).toHaveBeenCalledWith(
        { ...transactionModel, thirdPartyOutgoingId: '000021042T000041' },
        true,
        undefined
      );
    });

    it('should throw error when skn returned bad request error', async () => {
      (syariahSknRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.ERROR_FROM_SKN_SHARIA
        )
      );
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({
        ...transactionModel
      });

      (sknService.confirmSknTransaction as jest.Mock).mockResolvedValueOnce({
        ...transactionModel,
        status: TransactionStatus.DECLINED
      });

      const result = await syariahSknSubmissionTemplateService
        .submitTransaction({ ...transactionModel })
        .catch(err => err);

      expect(result).toBeDefined();
      expect(sknService.confirmSknTransaction).toHaveBeenCalledWith(
        { ...transactionModel, thirdPartyOutgoingId: '000021042T000041' },
        false,
        undefined
      );
      expect(syariahSknRepository.submitTransaction).toHaveBeenCalledTimes(1);
    });

    it('should throw error and send pending notification when skn returned timeout error', async () => {
      (syariahSknRepository.submitTransaction as jest.Mock).mockRejectedValueOnce(
        {
          message:
            'HttpClient Error: Error: Request failed with status code 408',
          code: httpClientTimeoutErrorCode
        }
      );
      (transactionRepository.update as jest.Mock).mockReturnValueOnce({});
      (transactionProducer.sendPendingBlockedAmountNotification as jest.Mock).mockResolvedValueOnce(
        null
      );

      await syariahSknSubmissionTemplateService
        .submitTransaction(transactionModel)
        .catch(err => err);

      expect(syariahSknRepository.submitTransaction).toHaveBeenCalledTimes(1);
    });
  });

  describe('Rail check', () => {
    it(`should have rail property defined`, () => {
      // when
      const rail = syariahSknSubmissionTemplateService.getRail?.();

      // then
      expect(rail).toEqual(Rail.SYARIAH_SKN);
    });
  });
});
