import { BankChannelEnum } from '@dk/module-common';
import { getTransactionDetail } from '../../transaction/__mocks__/transaction.data';
import { validateMandatoryFields, mapToSknTransaction } from '../skn.helper';
import { ERROR_CODE } from '../../common/errors';
import { ITransactionModel } from '../../transaction/transaction.model';
import { formatTransactionDate } from '../../transaction/transaction.util';
import { sknOutgoingParameters } from '../skn.constant';
import { SourceCustomerType, BeneficiaryCustomerType } from '../skn.enum';
import { TransferAppError } from '../../errors/AppError';

describe('skn.helper', () => {
  beforeEach(() => {});
  afterEach(() => {
    jest.resetAllMocks();
  });
  // given
  const transactionModel = {
    ...getTransactionDetail(),
    beneficiaryBankCodeChannel: BankChannelEnum.EXTERNAL,
    requireThirdPartyOutgoingId: true,
    debitTransactionCode: 'TFD60',
    thirdPartyOutgoingId: '21042T000041',
    transactionAmount: 50000002,
    paymentServiceCode: 'SKN',
    additionalInformation1: '1',
    additionalInformation2: '1'
  };

  describe('validateMandatoryFields', () => {
    it('should return error when beneficiaryAccountNo is undefined', () => {
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        beneficiaryAccountNo: undefined
      };
      // when
      let error;
      try {
        validateMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
    });

    it('should return error when beneficiaryAccountName is undefined', () => {
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        beneficiaryAccountName: undefined
      };
      // when
      let error;
      try {
        validateMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
    });

    it('should return error when beneficiaryBankCode is undefined', () => {
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        beneficiaryBankCode: undefined
      };
      // when
      let error;
      try {
        validateMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
    });

    it('should return error when sourceAccountNo is undefined', () => {
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        sourceAccountNo: undefined
      };
      // when
      let error;
      try {
        validateMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
    });

    it('should return error when sourceAccountName is undefined', () => {
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        sourceAccountName: undefined
      };
      // when
      let error;
      try {
        validateMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
    });

    it('should return error when additionalInformation1 is undefined', () => {
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation1: undefined
      };
      // when
      let error;
      try {
        validateMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
    });

    it('should return error when additionalInformation1 is invalid', () => {
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation1: '9'
      };
      // when
      let error;
      try {
        validateMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
    });

    it('should return error when additionalInformation2 is undefined', () => {
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation2: undefined
      };
      // when
      let error;
      try {
        validateMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
    });

    it('should return error when additionalInformation2 is invalid value', () => {
      const originalTransaction: ITransactionModel = {
        ...transactionModel,
        additionalInformation2: '9'
      };
      // when
      let error;
      try {
        validateMandatoryFields(originalTransaction);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_MANDATORY_FIELDS);
    });

    it('should return void when mandatory validations passed', () => {
      // when
      let error;
      try {
        validateMandatoryFields(transactionModel);
      } catch (err) {
        error = err;
      }
      // Then
      expect(error).toBeUndefined();
    });
  });

  describe('mapToSknTransaction', () => {
    it('should return mapToSknTransaction', () => {
      // when
      const transactionEntity = mapToSknTransaction(
        transactionModel,
        'externalId'
      );
      expect(transactionEntity).toBeDefined();
      expect(transactionEntity).toEqual({
        externalId: 'externalId',
        transactionDate: formatTransactionDate(transactionModel.createdAt),
        transactionAmount: transactionModel.transactionAmount,
        transactionType: sknOutgoingParameters.transactionType,
        transactionChannel: sknOutgoingParameters.transactionChannel,
        beneficiaryBankCode: transactionModel.beneficiaryRemittanceCode || '',
        beneficiaryAccountName: transactionModel.beneficiaryAccountName || '',
        beneficiaryAccountNo: transactionModel.beneficiaryAccountNo || '',
        beneficiaryCustomerType: BeneficiaryCustomerType.INDIVIDUAL,
        beneficiaryCustomerCitizenship: 1,
        sourceAccountNo: transactionModel.sourceAccountNo || '',
        sourceAccountName: transactionModel.sourceAccountName || '',
        sourceCity: sknOutgoingParameters.sourceCity,
        sourceCustomerType: SourceCustomerType.INDIVIDUAL,
        sourceCustomerCitizenship: 1,
        notes: transactionModel.note || '',
        paymentServiceType: sknOutgoingParameters.paymentServiceType
      });
    });

    it('should return mapped transaction with sourceCustomerType as CORPORATE when paymentServiceCode is SKN_FOR_BUSINESS', () => {
      // when
      const transactionEntity = mapToSknTransaction(
        { ...transactionModel, paymentServiceCode: 'SKN_FOR_BUSINESS' },
        'externalId'
      );
      expect(transactionEntity).toBeDefined();
      expect(transactionEntity).toEqual({
        externalId: 'externalId',
        transactionDate: formatTransactionDate(transactionModel.createdAt),
        transactionAmount: transactionModel.transactionAmount,
        transactionType: sknOutgoingParameters.transactionType,
        transactionChannel: sknOutgoingParameters.transactionChannel,
        beneficiaryBankCode: transactionModel.beneficiaryRemittanceCode || '',
        beneficiaryAccountName: transactionModel.beneficiaryAccountName || '',
        beneficiaryAccountNo: transactionModel.beneficiaryAccountNo || '',
        beneficiaryCustomerType: BeneficiaryCustomerType.INDIVIDUAL,
        beneficiaryCustomerCitizenship: 1,
        sourceAccountNo: transactionModel.sourceAccountNo || '',
        sourceAccountName: transactionModel.sourceAccountName || '',
        sourceCity: sknOutgoingParameters.sourceCity,
        sourceCustomerType: SourceCustomerType.CORPORATE,
        sourceCustomerCitizenship: 1,
        notes: transactionModel.note || '',
        paymentServiceType: sknOutgoingParameters.paymentServiceType
      });
    });
  });
});
