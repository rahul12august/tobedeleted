import { ITransactionModel } from '../transaction/transaction.model';
import {
  BeneficiaryCustomerType,
  BeneficiaryCustomerCitizenship
} from './skn.enum';
import { isUndefined } from 'lodash';

const checkBeneficiaryAccountName = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.beneficiaryAccountName);

const checkBeneficiaryAccountNo = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.beneficiaryAccountNo);

const checkBeneficiaryBankCode = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.beneficiaryBankCode);

const checkSourceAccountNo = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.sourceAccountNo);

const checkSourceAccountName = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.sourceAccountName);

const checkAdditionalInformation1 = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.additionalInformation1) &&
  Object.values(BeneficiaryCustomerType).includes(
    transaction.additionalInformation1 as BeneficiaryCustomerType
  );

const checkAdditionalInformation2 = (transaction: ITransactionModel): boolean =>
  !isUndefined(transaction.additionalInformation2) &&
  Object.values(BeneficiaryCustomerCitizenship).includes(
    transaction.additionalInformation2 as BeneficiaryCustomerCitizenship
  );

export const conditions = {
  checkBeneficiaryAccountName,
  checkBeneficiaryAccountNo,
  checkBeneficiaryBankCode,
  checkSourceAccountNo,
  checkSourceAccountName,
  checkAdditionalInformation1,
  checkAdditionalInformation2
};

export default Object.values(conditions);
