export enum BeneficiaryCustomerType {
  INDIVIDUAL = '1',
  CORPORATE = '2',
  GOVERNMENT = '3',
  REMITTANCE = '4'
}

export enum BeneficiaryCustomerCitizenship {
  RESIDENT = '1',
  NON_RESIDENT = '2',
  REMITTANCE = '3'
}

export enum SourceCustomerType {
  INDIVIDUAL = '1',
  CORPORATE = '2',
  GOVERNMENT = '3',
  REMITTANCE = '4'
}

export enum SourceCustomerCitizenship {
  RESIDENT = '1',
  NON_RESIDENT = '2',
  REMITTANCE = '3'
}
