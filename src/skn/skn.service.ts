import { IConfirmTransactionInput } from '../transaction/transaction.type';
import { ITransactionModel } from '../transaction/transaction.model';
import {
  ConfirmTransactionStatus,
  TransferFailureReasonActor
} from '../transaction/transaction.enum';
import { formatTransactionDate } from '../transaction/transaction.util';
import { BankNetworkEnum } from '@dk/module-common';
import logger, { wrapLogs } from '../logger';
import switchingTransactionService from '../transaction/transactionConfirmation.service';
import transactionRepository from '../transaction/transaction.repository';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { ISknResponse } from './skn.type';

const getResponseCode = (status: boolean, data?: ISknResponse): number => {
  if (data) {
    return data.responseCode;
  }
  return status
    ? ConfirmTransactionStatus.SUCCESS
    : ConfirmTransactionStatus.ERROR_EXPECTED;
};

const mapConfirmTransaction = (
  transaction: ITransactionModel,
  status: boolean,
  data?: ISknResponse
): IConfirmTransactionInput => ({
  externalId: transaction.thirdPartyOutgoingId || '',
  accountNo: transaction.sourceAccountNo,
  transactionDate: transaction.createdAt
    ? formatTransactionDate(transaction.createdAt)
    : '',
  responseCode: getResponseCode(status, data),
  interchange: BankNetworkEnum.SKN,
  amount: transaction.transactionAmount,
  transactionResponseID: undefined,
  responseMessage: data && data.responseMessage
});

const confirmSknTransaction = async (
  transaction: ITransactionModel,
  status: boolean,
  data?: ISknResponse
): Promise<ITransactionModel> => {
  const payload: IConfirmTransactionInput = mapConfirmTransaction(
    transaction,
    status,
    data
  );
  logger.info(
    `SKN: Transaction Confirmation for transactionId : ${transaction.id} with inquiry id : ${transaction.thirdPartyOutgoingId} 
        with responseCode : ${payload.responseCode}`
  );
  await switchingTransactionService.confirmTransaction(payload);
  const result = await transactionRepository.getByKey(transaction.id);
  if (!result) {
    const detail = `Failed to get transaction info for txnId ${transaction.id}`;
    logger.error(`confirmSknTransaction: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_TRANSACTION_ID
    );
  }
  return result;
};

const sknService = {
  confirmSknTransaction
};

export default wrapLogs(sknService);
