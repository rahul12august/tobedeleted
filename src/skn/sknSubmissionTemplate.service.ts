import { ITransactionSubmissionTemplate } from '../transaction/transaction.type';
import { ITransactionModel } from '../transaction/transaction.model';
import { BankChannelEnum, BankNetworkEnum, Rail } from '@dk/module-common';
import { TransferAppError } from '../errors/AppError';
import transactionRepository from '../transaction/transaction.repository';
import transactionUtility from '../transaction/transaction.util';
import logger, { wrapLogs } from '../logger';
import { validateMandatoryFields } from '../skn/skn.helper';
import sknRepository from './skn.repository';
import { mapToSknTransaction } from './skn.helper';
import { ERROR_CODE } from '../common/errors';
import { isEmpty } from 'lodash';
import { httpClientTimeoutErrorCode } from '../common/constant';
import {
  TransactionStatus,
  TransferFailureReasonActor,
  TransferJourneyStatusEnum
} from '../transaction/transaction.enum';
import sknService from './skn.service';
import transactionHelper from '../transaction/transaction.helper';

const submitTransaction = async (
  transactionModel: ITransactionModel
): Promise<ITransactionModel> => {
  let status = false;
  let data = undefined;
  try {
    validateMandatoryFields(transactionModel);
    if (!transactionModel.thirdPartyOutgoingId) {
      const detail =
        'Third party outgoing id is missing while doing SKN transaction.';
      logger.error(detail);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.SUBMITTER,
        ERROR_CODE.INVALID_EXTERNALID
      );
    }
    const externalId = transactionModel.thirdPartyOutgoingId;
    transactionModel.thirdPartyOutgoingId = externalId.padStart(16, '0');
    const sknTransactionPayload = mapToSknTransaction(
      transactionModel,
      transactionModel.thirdPartyOutgoingId
    );
    data = await transactionUtility.submitTransactionWithRetry(
      sknRepository.submitTransaction,
      sknTransactionPayload
    );
    status = true;
  } catch (error) {
    logger.error('failed submission to skn payment', error);
    if (error.code && error.code === httpClientTimeoutErrorCode) {
      logger.error(`Timeout error from SKN with code : ${error.code}`);
      status = true;
    }
  }
  const updatedTransaction = await transactionRepository.update(
    transactionModel.id,
    {
      status: TransactionStatus.SUBMITTED,
      thirdPartyOutgoingId: transactionModel.thirdPartyOutgoingId,
      journey: transactionHelper.getStatusesToBeUpdated(
        TransferJourneyStatusEnum.THIRD_PARTY_SUBMITTED,
        transactionModel
      )
    }
  );
  return sknService.confirmSknTransaction(updatedTransaction, status, data);
};

const isEligible = (model: ITransactionModel): boolean => {
  if (model.paymentServiceCode) {
    return (
      [
        BankNetworkEnum.SKN,
        'BRANCH_SKN',
        'SKN_FOR_BUSINESS',
        'RDN_WITHDRAW_SKN'
      ].includes(model.paymentServiceCode) &&
      BankChannelEnum.EXTERNAL === model.beneficiaryBankCodeChannel &&
      !isEmpty(model.beneficiaryAccountNo)
    );
  }
  return false;
};

const shouldSendNotification = () => true;

const getRail = () => Rail.SKN;

const sknSubmissionTemplate: ITransactionSubmissionTemplate = {
  submitTransaction,
  isEligible,
  getRail,
  shouldSendNotification
};

export default wrapLogs(sknSubmissionTemplate);
