import { config } from '../config';
import { context } from '../context';
import logger from '../logger';
import { HttpClient } from '@dk/module-httpclient';
import { Http } from '@dk/module-common';
import https from 'https';
const { baseUrl } = config.get('skn');
import { retryConfig } from '../common/constant';

const httpClient: HttpClient = Http.createHttpClientForward({
  baseURL: baseUrl,
  httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  context,
  logger,
  retryConfig: {
    ...retryConfig,
    networkErrorCodesToRetry: ['ENOENT', 'ECONNREFUSED']
  }
});

logger.info(`Base URL for SKN: ${baseUrl}`);

export default httpClient;
