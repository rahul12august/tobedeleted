export interface ISknTransaction {
  externalId: string;
  paymentServiceType: string;
  transactionAmount: number;
  transactionDate: string;
  transactionType: number;
  transactionChannel: number;
  beneficiaryBankCode: string;
  beneficiaryAccountNo: string;
  beneficiaryAccountName: string;
  beneficiaryCustomerType: string;
  beneficiaryCustomerCitizenship: number;
  sourceAccountNo: string;
  sourceAccountName: string;
  sourceCity: string;
  sourceCustomerType: string;
  sourceCustomerCitizenship: number;
  notes?: string;
  additionalInformation1?: string;
  additionalInformation2?: string;
  additionalInformation3?: string;
  additionalInformation4?: string;
}

export interface ISknResponse {
  responseCode: number;
  responseMessage?: string;
}
