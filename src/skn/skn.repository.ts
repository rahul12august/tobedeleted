import httpClient from './httpClient';
import logger, { wrapLogs } from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import uuidv4 from 'uuid/v4';
import { updateRequestIdInRedis } from '../common/contextHandler';
import { ISknResponse, ISknTransaction } from './skn.type';
import { config } from '../config';
import { httpClientTimeoutErrorCode } from '../common/constant';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const { transactionUrl } = config.get('skn');

const submitTransaction = async (
  transaction: ISknTransaction
): Promise<ISknResponse> => {
  try {
    const externalKey = transaction.externalId || uuidv4();
    updateRequestIdInRedis(externalKey);
    logger.info(
      `Submitting SKN transaction external id : ${transaction.externalId}, paymentServiceType : ${transaction.paymentServiceType},
      transactionType: ${transaction.transactionType}, transactionChannel: ${transaction.transactionChannel}, beneficiaryBankCode: ${transaction.beneficiaryBankCode},
      beneficiaryAccountNo: ${transaction.beneficiaryAccountNo}, beneficiaryAccountName: ${transaction.beneficiaryAccountName},
      beneficiaryCustomerType: ${transaction.beneficiaryCustomerType}, sourceAccountNo: ${transaction.sourceAccountNo},
      sourceAccountName: ${transaction.sourceAccountName}`
    );
    const result = await httpClient.post(transactionUrl, transaction);
    if (!result.data && result.status !== 200) {
      logger.error('SKN submitTransaction: Error while transferring funds.');
      throw new TransferAppError(
        `SKN Error with status: ${result.status}!`,
        result.status === 400 || result.status === 404
          ? TransferFailureReasonActor.MS_TRANSFER
          : TransferFailureReasonActor.SKN,
        ERROR_CODE.ERROR_FROM_SKN
      );
    }
    return result.data;
  } catch (error) {
    const detail = `Error while transferring funds code : ${error.code} message: ${error.message}.`;
    logger.error(`SKN submitTransaction catch: ${detail}`);
    // only throw error if it's a timeout error
    if (
      (error.code && error.code === httpClientTimeoutErrorCode) ||
      error instanceof TransferAppError
    ) {
      throw error;
    }
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.UNKNOWN,
      ERROR_CODE.ERROR_FROM_SKN
    );
  }
};

const sknRepository = wrapLogs({
  submitTransaction
});

export default sknRepository;
