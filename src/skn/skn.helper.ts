import { ITransactionModel } from '../transaction/transaction.model';
import { ISknTransaction } from './skn.type';
import { formatTransactionDate } from '../transaction/transaction.util';
import logger from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { sknOutgoingParameters } from './skn.constant';
import { SourceCustomerCitizenship, SourceCustomerType } from './skn.enum';
import ruleConditionFns from './skn.validator';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

export const mapToSknTransaction = (
  transaction: ITransactionModel,
  externalId: string
): ISknTransaction => ({
  externalId: externalId,
  paymentServiceType: sknOutgoingParameters.paymentServiceType,
  transactionType: sknOutgoingParameters.transactionType,
  transactionChannel: sknOutgoingParameters.transactionChannel,
  transactionDate: formatTransactionDate(transaction.createdAt || new Date()),
  transactionAmount: transaction.transactionAmount,
  beneficiaryBankCode: transaction.beneficiaryRemittanceCode || '',
  beneficiaryAccountName: transaction.beneficiaryAccountName || '',
  beneficiaryAccountNo: transaction.beneficiaryAccountNo || '',
  beneficiaryCustomerType: transaction.additionalInformation1 || '',
  beneficiaryCustomerCitizenship: Number(transaction.additionalInformation2),
  sourceAccountNo: transaction.sourceAccountNo || '',
  sourceAccountName: transaction.sourceAccountName || '',
  sourceCity: sknOutgoingParameters.sourceCity,
  sourceCustomerCitizenship: Number(SourceCustomerCitizenship.RESIDENT),
  sourceCustomerType:
    transaction.paymentServiceCode === 'SKN_FOR_BUSINESS'
      ? SourceCustomerType.CORPORATE
      : SourceCustomerType.INDIVIDUAL,
  notes: transaction.note || ''
});

export const validateMandatoryFields = (
  transaction: ITransactionModel
): void => {
  let isValid = true;
  for (let rule of ruleConditionFns) {
    const passed = rule(transaction);
    if (!passed) {
      logger.error(
        `validateMandatoryFields: Skn validation failed for rule: ${rule.name}`
      );
      isValid = false;
      break;
    }
  }
  if (!isValid) {
    throw new TransferAppError(
      'Skn transaction validation failed!',
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.INVALID_MANDATORY_FIELDS
    );
  }
  return;
};
