import { formatTransactionDate } from '../../transaction/transaction.util';
import { ISknTransaction } from '../skn.type';
import { sknOutgoingParameters } from '../skn.constant';

export const getMockedTransaction = (): ISknTransaction => ({
  externalId: 'externalId',
  paymentServiceType: sknOutgoingParameters.paymentServiceType,
  transactionAmount: 50000002,
  transactionDate: formatTransactionDate(new Date()),
  transactionType: sknOutgoingParameters.transactionType,
  transactionChannel: sknOutgoingParameters.transactionChannel,
  beneficiaryBankCode: 'BC005',
  beneficiaryAccountNo: '1234567890',
  beneficiaryAccountName: 'Test123',
  beneficiaryCustomerType: sknOutgoingParameters.customerType,
  beneficiaryCustomerCitizenship: 1,
  sourceAccountNo: '11110000',
  sourceAccountName: 'Test321',
  sourceCity: sknOutgoingParameters.sourceCity,
  sourceCustomerType: sknOutgoingParameters.customerType,
  sourceCustomerCitizenship: 1
});
