export interface ICustomerInfo {
  readonly cif?: string;
  readonly id: string;
  readonly fullName?: string;
  readonly languagePreferred?: string;
  readonly customerType?: string;
  readonly idNumber?: string;
  readonly npwpNumber?: string;
}

export interface ICustomerInfoWithCif extends ICustomerInfo {
  readonly cif: string;
}
