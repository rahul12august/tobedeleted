import faker from 'faker';
import { CustomerType } from '../customer.enum';
import { ICustomerInfo } from '../customer.type';

export const getCustomerResponse = (): ICustomerInfo => {
  return {
    cif: faker.random.alphaNumeric(40),
    id: faker.random.alphaNumeric(40),
    fullName: 'Customer Name',
    languagePreferred: 'en',
    customerType: CustomerType.BUSINESS_INDIVIDUAL
  };
};
