import redis from '../common/redis';
import customerRepository from './customer.repository';
import { ICustomerInfo } from './customer.type';

const customerPrefixKey = 'ms-customer';

const getCustomerInfoById = async (
  customerId: string
): Promise<ICustomerInfo | undefined> =>
  redis.get(
    customerId,
    customerPrefixKey,
    customerRepository.getCustomerInfoById,
    customerId
  );

const getCustomerType = async (
  customerId: string
): Promise<string | undefined> => {
  const customer = await getCustomerInfoById(customerId);
  return customer?.customerType;
};

const customerCache = {
  getCustomerType
};

export default customerCache;
