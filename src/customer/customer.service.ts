import { ICustomerInfoWithCif } from './customer.type';
import customerRepository from './customer.repository';
import logger, { wrapLogs } from '../logger';
import { ERROR_CODE } from '../common/errors';
import { TransferAppError } from '../errors/AppError';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const getCustomerById = async (
  customerId: string
): Promise<ICustomerInfoWithCif> => {
  const customer = await customerRepository.getCustomerInfoById(customerId);
  if (!customer || !customer.cif) {
    let detail = `CIF not found for customerId: ${customerId}!`;
    logger.error(detail);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.SUBMITTER,
      ERROR_CODE.CUSTOMER_NOT_FOUND
    );
  }
  return customer as ICustomerInfoWithCif;
};

const customerService = {
  getCustomerById
};

export default wrapLogs(customerService);
