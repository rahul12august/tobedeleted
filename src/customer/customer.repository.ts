import { getCustomerByIdUrl } from './customer.constants';
import { ICustomerInfo } from './customer.type';
import httpClient from './httpClient';
import logger, { wrapLogs } from '../logger';

const getCustomerInfoById = async (
  customerId: string
): Promise<ICustomerInfo | null> => {
  logger.info('Calling get customer info by customerId : ', customerId);
  const {
    data: { data: customerInfo }
  } = await httpClient.get(getCustomerByIdUrl(customerId));
  return customerInfo;
};

const customerRepository = {
  getCustomerInfoById
};

export default wrapLogs(customerRepository);
