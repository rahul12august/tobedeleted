const getCustomerByIdUrl = (customerId: string): string =>
  `/customers/${customerId}`;

export { getCustomerByIdUrl };
