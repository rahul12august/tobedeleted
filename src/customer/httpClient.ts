import { HttpClient } from '@dk/module-httpclient';
import { config } from '../config';
import logger from '../logger';
import { context } from '../context';
import { Http } from '@dk/module-common';
import { retryConfig } from '../common/constant';

const MS_CUSTOMER_URL = config.get('ms').customer;

const httpClient: HttpClient = Http.createHttpClientForward({
  baseURL: MS_CUSTOMER_URL,
  context,
  logger,
  retryConfig
});

logger.info(
  `Setting up http-client with ms-customer on address : ${MS_CUSTOMER_URL}`
);

export default httpClient;
