import redis from '../../common/redis';

import customerCache from '../customer.cache';
import customerRepository from '../customer.repository';
import { getCustomerResponse } from '../__mocks__/customer.data';

describe('customer cache', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('#getCustomerType', () => {
    it('should get customer by ID', async () => {
      const sampleCustomerData = getCustomerResponse();
      redis.get = jest.fn().mockResolvedValueOnce(sampleCustomerData);

      const customerId = '1';

      const type = await customerCache.getCustomerType(customerId);

      expect(redis.get).toBeCalledTimes(1);
      expect(type).toEqual(sampleCustomerData.customerType);
    });

    it('should return undefined when customer not found', async () => {
      redis.get = jest.fn().mockResolvedValueOnce(null);

      const customerId = '1';

      const type = await customerCache.getCustomerType(customerId);

      expect(redis.get).toBeCalledTimes(1);
      expect(type).toEqual(undefined);
    });

    it('should return undefined if customer does not exists', async () => {
      const customerId = '1';

      redis.get = jest.fn().mockResolvedValueOnce(null);

      customerRepository.getCustomerInfoById = jest
        .fn()
        .mockResolvedValue(null);

      const type = await customerCache.getCustomerType(customerId);

      expect(redis.get).toBeCalledTimes(1);
      expect(type).toEqual(undefined);
    });
  });
});
