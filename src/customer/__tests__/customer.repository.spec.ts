import httpClient from '../httpClient';
import customerRepository from '../customer.repository';

jest.mock('../httpClient');

describe('customer.repository', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('get customer info by cutomerId', () => {
    it('should get proper response when call customer/me api', async () => {
      // Given
      const customerResponse = {
        data: { data: { cif: 'string', id: 'string', fullName: 'string' } }
      };

      (httpClient.get as jest.Mock).mockImplementationOnce(() =>
        Promise.resolve(customerResponse)
      );
      const customerId = '12345';
      // When
      const actualResponse = await customerRepository.getCustomerInfoById(
        customerId
      );

      // Then
      expect(httpClient.get).toHaveBeenCalledWith(`/customers/${customerId}`);
      expect(actualResponse).toEqual(customerResponse.data.data);
    });
  });
});
