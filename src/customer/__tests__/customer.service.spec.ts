import customerRepository from '../customer.repository';
import customerService from '../customer.service';
import { ERROR_CODE } from '../../common/errors';
import { TransferAppError } from '../../errors/AppError';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

jest.mock('../customer.repository');

describe('customer service', () => {
  afterEach(() => {
    jest.resetAllMocks();
    expect.hasAssertions();
  });
  describe('get customer info by customer id', () => {
    it('should call to get customer info of given customer Id', async () => {
      const customerId = '12345';
      const customerResponse = {
        cif: 'string',
        id: 'string',
        fullName: 'string'
      };

      (customerRepository.getCustomerInfoById as jest.Mock).mockResolvedValueOnce(
        customerResponse
      );

      const customerInfo = await customerService.getCustomerById(customerId);

      expect(customerRepository.getCustomerInfoById).toBeCalledWith(customerId);
      expect(customerInfo).toEqual(customerResponse);
    });

    it('should call to get customer info of given customer Id and throw error if customer info not found', async () => {
      // given
      const customerId = '12345';

      (customerRepository.getCustomerInfoById as jest.Mock).mockResolvedValueOnce(
        {}
      );

      // when
      let error = await customerService
        .getCustomerById(customerId)
        .catch(error => error);

      // then
      expect(error).toBeDefined();
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.actor).toEqual(TransferFailureReasonActor.SUBMITTER);
      expect(error.detail).toEqual(
        `CIF not found for customerId: ${customerId}!`
      );
      expect(error.errorCode).toEqual(ERROR_CODE.CUSTOMER_NOT_FOUND);
    });
  });

  //   describe('get customerCIF by customer id', () => {
  //     test('should call to get customer cif of given customer Id', async () => {
  //       const customerId = '12345';
  //       const customerResponse = {
  //         cif: 'string'
  //       };

  //       (customerRepository.getCustomerInfoById as jest.Mock).mockResolvedValueOnce(
  //         customerResponse
  //       );

  //       const customerCIF = await customerService.getCustomerCIFById(customerId);

  //       expect(customerRepository.getCustomerInfoById).toBeCalledWith(customerId);
  //       expect(customerCIF).toEqual(customerResponse.cif);
  //     });

  //     test('should throw error if customer info not found of given customer Id', async () => {
  //       //Given
  //       const customerId = '12345';

  //       (customerRepository.getCustomerInfoById as jest.Mock).mockResolvedValueOnce(
  //         null
  //       );

  //       //When
  //       try {
  //         await customerService.getCustomerCIFById(customerId);
  //       } catch (error) {
  //         //Then
  //         expect(error).toBeDefined();
  //         expect(error.errorCode).toEqual(ERROR_CODE.CUSTOMER_CIF_NOT_FOUND);
  //       }
  //     });

  //     test('should throw error if customer info found but cif not found of given customer Id', async () => {
  //       //Given
  //       const customerId = '12345';

  //       (customerRepository.getCustomerInfoById as jest.Mock).mockResolvedValueOnce(
  //         {
  //           name: 'string'
  //         }
  //       );

  //       //When
  //       try {
  //         await customerService.getCustomerCIFById(customerId);
  //       } catch (error) {
  //         //Then
  //         expect(error).toBeDefined();
  //         expect(error.errorCode).toEqual(ERROR_CODE.CUSTOMER_CIF_NOT_FOUND);
  //       }
  //     });
  //   });
});
