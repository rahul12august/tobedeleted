export const enum CustomerType {
  CUSTOMER_TYPE_01 = 'CUSTOMER_TYPE_01',
  CORPORATE = 'CORPORATE',
  CUSTOMER_TYPE_03 = 'CUSTOMER_TYPE_03',
  CUSTOMER_TYPE_04 = 'CUSTOMER_TYPE_04',
  CUSTOMER_TYPE_05 = 'CUSTOMER_TYPE_05',
  INDIVIDUAL_SHARIA = 'INDIVIDUAL_SHARIA',
  BUSINESS_INDIVIDUAL = 'BUSINESS_INDIVIDUAL'
}
