import { IErrorDetail } from '@dk/module-common/dist/error/error.interface';
import { ERROR_CODE } from '../../common/errors';
import { getErrorDetails } from '../error.util';

describe('error.util', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('getErrorDetails', () => {
    it('should called error', async () => {
      const errorCode = ERROR_CODE.INVALID_ACCOUNT;
      const errorKey = 'errorKey';
      const errorMessage = 'Invalid account';
      const expectedErrorCodeList: IErrorDetail[] = [
        {
          message: errorMessage,
          code: ERROR_CODE.INVALID_ACCOUNT,
          key: errorKey
        }
      ];

      const actual = await getErrorDetails(errorCode, errorKey);

      expect(actual).toEqual(expectedErrorCodeList);
    });
  });
});
