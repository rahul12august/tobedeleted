import { AppError } from '@dk/module-common/dist/error/AppError';
import { IErrorDetail } from '@dk/module-common/dist/error/error.interface';
import { ERROR_CODE } from '../common/errors';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';
import { ITransactionModel } from '../transaction/transaction.model';

export { AppError } from '@dk/module-common/dist/error/AppError';
export { ErrorDetails } from '@dk/module-common/dist/idm/idm.error';

export class TransferAppError extends AppError {
  detail: string;
  actor: TransferFailureReasonActor;
  transaction?: ITransactionModel;

  constructor(
    detail: string,
    actor: TransferFailureReasonActor,
    errorCode: ERROR_CODE,
    errors?: IErrorDetail[],
    transaction?: ITransactionModel
  ) {
    super(errorCode, errors);
    this.detail = detail;
    this.actor = actor;
    this.transaction = transaction;
  }
}

export class RetryableTransferAppError extends TransferAppError {
  retry?: boolean;

  constructor(
    detail: string,
    actor: TransferFailureReasonActor,
    errorCode: ERROR_CODE,
    retry?: boolean,
    errors?: IErrorDetail[],
    transaction?: ITransactionModel
  ) {
    super(detail, actor, errorCode, errors, transaction);
    this.retry = retry;
  }
}

export const isRetryableError = (status: number) => status >= 500;
