import { config, loadConfig } from '@dk/module-config';
import { merge } from 'lodash';

/* To overirde any config value call loadConfig with respective config object
ex: loadConfig({ 'server': { port: 9090 } });
*/

// ENV config will apply on reload
// JSON config must be rebuilt to apply
const defaultConf = require('../configs/default.json');
const env = process.env.NODE_ENV;
const envConf = env ? require(`../configs/${env}.json`) : {};

loadConfig(merge(defaultConf, envConf));
export { config };
