import * as Joi from '@hapi/joi';
import { JoiValidator } from '@dk/module-common';
JoiValidator.inject(Joi);
