import { ITransferJourney } from '../transaction/transaction.type';
import {
  IFailureReasonModel,
  IFeeModel,
  IProcessingInfo
} from '../transaction/transaction.model';
import { ExecutionTypeEnum } from '../transaction/transaction.enum';

export interface ITransactionMonitoringMessage {
  requestId?: string;
  beneficiaryAccountType?: string;
  beneficiaryBankCodeChannel?: string;
  customerId?: string;
  referenceId?: string;
  externalId?: string;
  status?: string;
  paymentRequestID?: string;
  transactionAmount?: number;
  fees?: IFeeModel[];
  categoryCode?: string;
  sourceAccountType?: string;
  sourceBankCodeChannel?: string;
  sourceTransactionCurrency?: string;
  paymentServiceType?: string;
  paymentServiceCode?: string;
  creditPriority?: string;
  creditTransactionChannel?: string;
  creditTransactionCode?: string;
  debitTransactionChannel?: string;
  debitTransactionCode?: string;
  debitPriority?: string;
  journey?: ITransferJourney[];
  processingInfo?: IProcessingInfo;
  kind?: ExecutionTypeEnum;
  failureReason?: IFailureReasonModel;
  createdAt?: Date;
  updatedAt?: Date;
}
