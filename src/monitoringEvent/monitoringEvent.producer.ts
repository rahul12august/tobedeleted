import { ITransactionModel } from '../transaction/transaction.model';
import logger, { wrapLogs } from '../logger';
import producer from '../common/kafkaProducer';
import kafkaUtils from '../common/kafka.util';
import {
  EventMonitoringTopicConstant,
  IMonitoringEventMessage,
  MonitoringEntityType,
  MonitoringEventType
} from '@dk/module-message';
import { ITransferJourney } from '../transaction/transaction.type';
import { getRequestId } from '../common/contextHandler';
import { ITransactionMonitoringMessage } from './transactionMonitoringMessage';
import { MICROSERVICE_NAME } from '../common/constant';
import transactionHelper from '../transaction/transaction.helper';
import moment from 'moment';
import { config } from '../config';

const getJourney = (model: ITransactionModel): ITransferJourney[] => {
  const journey =
    model.journey &&
    model.journey.map(data => {
      return {
        status: data.status,
        updatedAt: data.updatedAt
      };
    });
  return journey as ITransferJourney[];
};

const toMonitoringEventMessage = (
  eventType: MonitoringEventType,
  model: ITransactionModel
): IMonitoringEventMessage<
  MonitoringEventType,
  ITransactionMonitoringMessage
> => {
  const eventPayload: ITransactionMonitoringMessage = {
    requestId: getRequestId(),
    beneficiaryAccountType: model.beneficiaryAccountType,
    referenceId: model.referenceId,
    externalId: model.externalId,
    status: model.status,
    sourceBankCodeChannel: model.sourceBankCodeChannel,
    customerId: transactionHelper.getCustomerIdByModel(model),
    categoryCode: model.categoryCode,
    transactionAmount: model.transactionAmount,
    fees: model.fees,
    paymentRequestID: model.paymentRequestID,
    paymentServiceCode: model.paymentServiceCode,
    processingInfo: model.processingInfo,
    kind: model.executionType,
    journey: getJourney(model),
    failureReason: model.failureReason,
    createdAt: model.createdAt || new Date(),
    updatedAt: model.updatedAt || new Date()
  };

  const monitoringEventMessage: IMonitoringEventMessage<
    MonitoringEventType,
    ITransactionMonitoringMessage
  > = {
    entityId: model.id,
    entityType: MonitoringEntityType.TRANSACTION,
    eventType: eventType,
    timestampMillis: moment.now(),
    source: MICROSERVICE_NAME,
    payload: eventPayload
  };
  return monitoringEventMessage;
};

const sendMonitoringEventMessage = async (
  eventType: MonitoringEventType,
  transaction: ITransactionModel
) => {
  // Proceed only if monitoring is enabled for environment
  if (!config.get('monitoring').isEnabled) {
    return;
  }

  const monitoringEventMessage = toMonitoringEventMessage(
    eventType,
    transaction
  );
  logger.info(`sendMonitoringEventMessage : sending monitoring event message
      entityId: ${monitoringEventMessage.entityId}
      entityType: ${monitoringEventMessage.entityType}
      eventType: ${monitoringEventMessage.eventType}`);

  await producer.sendSerializedValue({
    topic: EventMonitoringTopicConstant.EVENT_MONITORING,
    messages: [
      {
        value: monitoringEventMessage,
        headers: kafkaUtils.getHeaders()
      }
    ]
  });
};

export default wrapLogs({
  sendMonitoringEventMessage
});
