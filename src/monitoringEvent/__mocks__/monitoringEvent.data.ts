import {
  BankChannelEnum,
  PaymentServiceTypeEnum,
  Rail
} from '@dk/module-common';
import faker from 'faker';
import {
  ExecutionTypeEnum,
  TransactionStatus,
  TransferJourneyStatusEnum
} from '../../transaction/transaction.enum';
import {
  IFeeModel,
  IProcessingInfo,
  ITransactionModel
} from '../../transaction/transaction.model';
import { ITransferJourney } from '../../transaction/transaction.type';

export const getTransactionModelData = (): ITransactionModel => {
  let fees: IFeeModel[] = [
    {
      feeCode: faker.random.alphaNumeric(4),
      feeAmount: faker.random.number({ min: 1, max: 100 }),
      customerTc: faker.random.alphaNumeric(4),
      customerTcChannel: faker.random.alphaNumeric(4)
    }
  ];

  let journey: ITransferJourney[] = [
    {
      status: TransferJourneyStatusEnum.REQUEST_RECEIVED,
      updatedAt: new Date()
    },
    {
      status: TransferJourneyStatusEnum.INQUIRY_STARTED,
      updatedAt: new Date()
    }
  ];

  const processingInfo: IProcessingInfo = {
    paymentRail: Rail.EURONET.toString()
  };

  return {
    id: faker.random.alphaNumeric(24),
    beneficiaryAccountType: faker.random.alphaNumeric(4),
    referenceId: faker.random.alphaNumeric(20),
    externalId: faker.random.alphaNumeric(20),
    status: TransactionStatus.SUCCEED,
    sourceBankCodeChannel: BankChannelEnum.EURONET,
    categoryCode: faker.random.alphaNumeric(10),
    transactionAmount: faker.random.number({ min: 1, max: 100000 }),
    fees: fees,
    paymentRequestID: faker.random.alphaNumeric(20),
    paymentServiceCode: faker.random.alphaNumeric(4),
    paymentServiceType: PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT,
    requireThirdPartyOutgoingId: true,
    journey: journey,
    processingInfo: processingInfo,
    executionType: ExecutionTypeEnum.BLOCKING,
    createdAt: new Date(),
    updatedAt: new Date()
  };
};
