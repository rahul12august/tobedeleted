import {
  EventMonitoringTopicConstant,
  IMonitoringEventMessage,
  MonitoringEntityType,
  MonitoringEventType
} from '@dk/module-message';
import producer from '../../common/kafkaProducer';
import { MICROSERVICE_NAME } from '../../common/constant';
import { getTransactionModelData } from '../__mocks__/monitoringEvent.data';
import monitoringEventProducer from '../monitoringEvent.producer';
import { ITransactionMonitoringMessage } from '../transactionMonitoringMessage';
import uuidv4 from 'uuid/v4';
import faker from 'faker';
import transactionHelper from '../../transaction/transaction.helper';
import { getRequestId } from '../../common/contextHandler';
import moment from 'moment';
import { ITransactionModel } from '../../transaction/transaction.model';
import { config } from '@dk/module-config';
import { ERROR_CODE } from '../../common/errors';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

jest.mock('../../common/kafkaProducer', () => ({
  sendSerializedValue: jest.fn()
}));

jest.mock('../../common/contextHandler', () => ({
  getRequestId: jest.fn()
}));

jest.mock('uuid/v4');
jest.mock('../../transaction/transaction.helper');

describe('monitoringEvent.producer', () => {
  const uuidMockValue = faker.random.alphaNumeric(20);
  const transactionModel: ITransactionModel = {
    ...getTransactionModelData(),
    id: 'transactionId',
    failureReason: {
      type: ERROR_CODE.INVALID_ACCOUNT,
      actor: TransferFailureReasonActor.SUBMITTER,
      detail: `Given incorrect account`
    }
  };
  const mockedDateNow = moment('2022-01-01T14:48:48.123Z').valueOf();

  afterEach(() => {
    jest.resetAllMocks();
  });

  beforeEach(() => {
    moment.now = jest.fn().mockReturnValue(mockedDateNow);

    (transactionHelper.getCustomerIdByModel as jest.Mock).mockReturnValueOnce(
      'customerId'
    );
    (getRequestId as jest.Mock).mockReturnValueOnce('x-request-id');
    (uuidv4 as jest.Mock).mockReturnValueOnce(uuidMockValue);
  });

  const getMonitoringEventMessageByModel = (
    model: ITransactionModel
  ): IMonitoringEventMessage<
    MonitoringEventType,
    ITransactionMonitoringMessage
  > => {
    let payload = {
      beneficiaryAccountType: model.beneficiaryAccountType,
      categoryCode: model.categoryCode,
      externalId: model.externalId,
      requestId: 'x-request-id',
      customerId: 'customerId',
      paymentRequestID: model.paymentRequestID,
      paymentServiceCode: model.paymentServiceCode,
      processingInfo: model.processingInfo,
      kind: model.executionType,
      referenceId: model.referenceId,
      sourceBankCodeChannel: model.sourceBankCodeChannel,
      status: model.status,
      transactionAmount: model.transactionAmount,
      journey: model.journey,
      fees: model.fees,
      failureReason: model.failureReason,
      createdAt: model.createdAt || new Date(),
      updatedAt: model.updatedAt || new Date()
    };

    return {
      entityId: model.id,
      entityType: MonitoringEntityType.TRANSACTION,
      eventType: MonitoringEventType.PROCESSING_STARTED,
      source: MICROSERVICE_NAME,
      timestampMillis: 1641048528123,
      payload: payload
    };
  };

  it('should produce transaction monitoring event', async () => {
    // Given
    const expectedMonitoringEvent = getMonitoringEventMessageByModel(
      transactionModel
    );

    // When
    await monitoringEventProducer.sendMonitoringEventMessage(
      MonitoringEventType.PROCESSING_STARTED,
      transactionModel
    );

    // Then
    expect(producer.sendSerializedValue).toBeCalledWith({
      topic: EventMonitoringTopicConstant.EVENT_MONITORING,
      messages: [
        {
          value: expectedMonitoringEvent,
          headers: { 'x-idempotency-key': uuidMockValue }
        }
      ]
    });
  });

  it('should not produce transaction monitoring event if feature flag disabled', async () => {
    config.get = jest.fn().mockImplementation(() => false);
    await monitoringEventProducer.sendMonitoringEventMessage(
      MonitoringEventType.PROCESSING_STARTED,
      transactionModel
    );

    expect(producer.sendSerializedValue).not.toHaveBeenCalled();
  });
});
