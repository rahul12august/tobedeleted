export interface IDisbursementTransaction {
  id: string;
}
export interface IRepaymentTransaction {
  id: string;
}

export interface IDisbursementPayload {
  amount?: number;
  externalId: string;
  valueDate?: string;
  bookingDate?: string;
  transactionDetails: {
    transactionChannelId: string;
  };
}

interface ICustomPaymentAmount {
  amount: number;
  customPaymentAmountType: string;
}
export interface IRepaymentPayload {
  externalId: string;
  amount: number;
  transactionDetails: {
    transactionChannelId: string;
  };
  customPaymentAmounts?: ICustomPaymentAmount[];
  valueDate?: string;
}

export interface IPayOffAdjustableAmount {
  interestPaid?: number;
  penaltyPaid?: number;
}

export interface IPayOffPayload {
  externalId: string;
  payOffAdjustableAmounts?: IPayOffAdjustableAmount;
  notes?: string;
  transactionDetails: {
    transactionChannelId: string;
  };
}

export interface ILoanTransactionAdjustment {
  type: string;
  originalTransactionId?: string;
  notes?: string;
}
