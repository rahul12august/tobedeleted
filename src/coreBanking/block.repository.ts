import { ICoreBankingBlockingInput } from './block.interface';
import httpClient from './httpClient';
import Const from './coreBanking.constant';
import circuitBreakerFactory from './circuitBreaker';
import { wrapLogs } from '../logger';

const baseCardPath = '/cards';

const createBlockingAmount = async (
  cardReferenceToken: string,
  inputBlockingAmount: ICoreBankingBlockingInput,
  idempotencyKey: string
): Promise<void> => {
  await httpClient.post(
    `${baseCardPath}/${cardReferenceToken}/authorizationholds`,
    inputBlockingAmount,
    {
      headers: {
        [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
      }
    }
  );
  inputBlockingAmount.idempotencyKey = idempotencyKey;
};

const increaseAuthorizationHold = async (
  cardReferenceToken: string,
  authHoldExtReferenceId: string,
  amount: number,
  idempotencyKey: string
): Promise<void> => {
  await httpClient.post(
    `${baseCardPath}/${cardReferenceToken}/authorizationholds/${authHoldExtReferenceId}:increase`,
    {
      amount
    },
    {
      headers: {
        [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
      }
    }
  );
};

const decreaseAuthorizationHold = async (
  cardReferenceToken: string,
  authHoldExtReferenceId: string,
  amount: number,
  idempotencyKey: string
): Promise<void> => {
  await httpClient.post(
    `${baseCardPath}/${cardReferenceToken}/authorizationholds/${authHoldExtReferenceId}:decrease`,
    {
      amount
    },
    {
      headers: {
        [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
      }
    }
  );
};

const coreBankingRepository = {
  createBlockingAmount: circuitBreakerFactory.create(createBlockingAmount),
  increaseAuthorizationHold: circuitBreakerFactory.create(
    increaseAuthorizationHold
  ),
  decreaseAuthorizationHold: circuitBreakerFactory.create(
    decreaseAuthorizationHold
  )
};
export default wrapLogs(coreBankingRepository);
