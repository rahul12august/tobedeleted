import { Http } from '@dk/module-common';
import logger, { wrapLogs } from '../logger';
import httpClient from './httpClient';
import {
  IWithdrawalInput,
  IDepositInput,
  IReverseTransactionInput,
  IReverseCardTransactionInput,
  IDepositTransaction,
  ICardWithdrawalInput,
  ICardTransaction,
  ICoreBankingDepositAccount,
  IJournalEntriesGL,
  IGLJournalEntriesV2,
  IGLJournalEntriesResult,
  IPatchTransactionDetail
} from './deposit.interface';
import Const from './coreBanking.constant';
import circuitBreakerFactory from './circuitBreaker';
import mambuErrorUtil from '../blockingAmount/mambu.error.util';
import { stringifiedMambuError } from './coreBanking.util';
import { toJKTDate } from '../common/dateUtils';
import { FORMAT_YYYY_MM_DD } from '../transaction/transaction.constant';

const doGetAvailableBalance = async (
  accountNumber: string
): Promise<number> => {
  try {
    logger.info(`doGetAvailableBalance accountId : ${accountNumber}`);
    const result = await httpClient.get<ICoreBankingDepositAccount>(
      `/deposits/${accountNumber}`
    );
    logger.info(
      `doGetAvailableBalance completed and available balance ${result.data.balances.availableBalance}.`
    );
    return result.data.balances.availableBalance;
  } catch (error) {
    logger.error(`Failed to getAvailableBalance of account ${accountNumber}`, {
      error
    });
    throw error;
  }
};

const doMakeWithdrawal = async (
  depositAccountId: string,
  input: IWithdrawalInput,
  idempotencyKey: string = Http.generateIdempotencyKey()
): Promise<IDepositTransaction> => {
  try {
    logger.info(
      `Submitting Mambu Withdrwal transaction accountId : ${depositAccountId} and idempotencyKey: ${idempotencyKey}`
    );
    const result = await httpClient.post<IDepositTransaction>(
      `/deposits/${depositAccountId}/withdrawal-transactions`,
      input,
      {
        headers: {
          [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
        }
      }
    );
    logger.info(
      `doMakeWithdrawal with depositAccountId : ${depositAccountId} and mambuId : ${result.data.id}`
    );
    logger.debug(
      `doMakeWithdrawal with depositAccountId : ${depositAccountId} and response : ${JSON.stringify(
        result.data
      )}`
    );
    return result.data;
  } catch (error) {
    logger.error(
      `Failed to call to withdrawal-transactions error :${stringifiedMambuError(
        error
      )}`
    );
    mambuErrorUtil.tryThrowingProperTransferAppError(error, input);
    throw error;
  }
};

const doMakeDeposit = async (
  depositAccountId: string,
  input: IDepositInput,
  idempotencyKey: string = Http.generateIdempotencyKey()
): Promise<IDepositTransaction> => {
  try {
    logger.info(
      `Submitting Mambu Deposit transaction accountId : ${depositAccountId} and idempotencyKey: ${idempotencyKey}`
    );
    const result = await httpClient.post<IDepositTransaction>(
      `/deposits/${depositAccountId}/deposit-transactions`,
      input,
      {
        headers: {
          [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
        }
      }
    );
    logger.debug(
      `doMakeDeposit with depositAccountId : ${depositAccountId} and response : ${JSON.stringify(
        result.data
      )}`
    );
    logger.info(
      `doMakeDeposit with depositAccountId : ${depositAccountId} and response with mambu id : ${result.data.id}`
    );
    return result.data;
  } catch (error) {
    logger.error(
      `Failed to makeDeposit with error : ${stringifiedMambuError(error)}`
    );
    throw error;
  }
};

const doReverseTransaction = async (
  transactionId: string,
  reverseInput?: IReverseTransactionInput,
  idempotencyKey: string = Http.generateIdempotencyKey()
): Promise<void> => {
  try {
    logger.debug(
      `Submitting Reversal transaction transactionId : ${transactionId} and payload : ${JSON.stringify(
        reverseInput
      )}`
    );
    logger.info(
      `Submitting Reversal transaction transactionId : ${transactionId}`
    );
    const result = await httpClient.post(
      `/deposits/transactions/${transactionId}:adjust`,
      {
        notes:
          (reverseInput && reverseInput.notes) ||
          `Adjust transaction ${transactionId}`
      },
      {
        headers: {
          [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
        }
      }
    );
    logger.info(
      `doReverseTransaction completed with transactionId: ${transactionId}, corebankingId: ${result.data.id}`
    );
  } catch (error) {
    logger.error(
      `Failed to reverseTransaction error : 
      ${stringifiedMambuError(error)}`
    );
    throw error;
  }
};

const doReverseCardTransaction = async (
  cardId: string,
  transactionId: string,
  reverseInput: IReverseCardTransactionInput,
  idempotencyKey: string = Http.generateIdempotencyKey()
): Promise<void> => {
  try {
    logger.debug(
      `Submitting Reversal card transaction with cardId : ${cardId} ,transactionId : ${transactionId} and payload : ${JSON.stringify(
        reverseInput
      )}`
    );
    logger.info(
      `Submitting Reversal card transaction with cardId : ${cardId} ,transactionId : ${transactionId}`
    );
    await httpClient.post(
      `/cards/${cardId}/financialtransactions/${transactionId}:decrease`,
      reverseInput,
      {
        headers: {
          [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
        }
      }
    );
    logger.info(
      `doReverseCardTransaction completed for cardId: ${cardId},transactionId : ${transactionId}`
    );
  } catch (error) {
    logger.error(
      `Failed to reverseCardTransaction error : 
      ${stringifiedMambuError(error)}`
    );
    throw error;
  }
};

const doPatchTransactionDetails = async (
  transactionId: string,
  payload: IPatchTransactionDetail[]
) => {
  try {
    const editTransactionDetailsUrl = `/deposits/transactions/${transactionId}`;
    logger.info(
      `calling patch transaction details on ${editTransactionDetailsUrl}`
    );
    const result = await httpClient.patch(editTransactionDetailsUrl, payload);
    logger.info(`edit transaction response status: ${result.status}`);
  } catch (error) {
    logger.error(`Failed to patch transaction details for ${transactionId}`, {
      error
    });
    throw error;
  }
};

const searchDepositTransactionsUrl = '/deposits/transactions:search';
const transactionsSortingCriteria = {
  field: 'creationDate',
  order: 'DESC' // order latest to revert latest first
};
const transactionIdSortingCriteria = {
  field: 'id',
  order: 'DESC' // order latest to revert latest first
};

const doGetTransactionsByInstructionId = async (
  id: string,
  creationDate: Date
): Promise<IDepositTransaction[]> => {
  try {
    logger.info(
      `get transactions by instructionId : ${id}, date: ${creationDate}`
    );
    const input = {
      sortingCriteria: transactionIdSortingCriteria,
      filterCriteria: [
        {
          field: '_Custom_Transaction_Details.Payment_Instruction_ID',
          operator: 'EQUALS_CASE_SENSITIVE',
          value: id
        },
        {
          field: 'creationDate',
          operator: 'ON',
          value: toJKTDate(creationDate, FORMAT_YYYY_MM_DD)
        }
      ]
    };

    const result = await httpClient.post<IDepositTransaction[]>(
      searchDepositTransactionsUrl,
      input
    );
    logger.info(`doGetTransactionsByInstructionId with instructionId : ${id}`);
    logger.debug(
      `doGetTransactionsByInstructionId with instructionId : ${id} and response length : ${result.data.length}`
    );
    return result.data;
  } catch (error) {
    logger.error(`Failed to get core banking ids for transaction ${id}`, {
      error
    });
    throw error;
  }
};

const doGetTransactionsByReferenceId = async (
  referenceId: string
): Promise<IDepositTransaction[]> => {
  logger.info(`get transactions by referenceId : ${referenceId}`);
  const input = {
    sortingCriteria: transactionsSortingCriteria,
    filterCriteria: [
      {
        field: '_Custom_Transaction_Details.Reference_ID',
        operator: 'EQUALS_CASE_SENSITIVE',
        value: referenceId
      }
    ]
  };

  const result = await httpClient.post<IDepositTransaction[]>(
    `${searchDepositTransactionsUrl}?detailsLevel=FULL`,
    input
  );

  logger.debug(
    `doGetTransactionsByReferenceId with referenceId : ${referenceId} and response : ${JSON.stringify(
      result.data
    )}`
  );
  logger.info(
    `doGetTransactionsByReferenceId with referenceId : ${referenceId}`
  );
  return result.data;
};

const doMakeCardWithdrawal = async (
  cardId: string,
  input: ICardWithdrawalInput,
  idempotencyKey: string = Http.generateIdempotencyKey()
): Promise<ICardTransaction> => {
  try {
    logger.info(
      `Submitting Card Withdrawal transaction with cardId : ${cardId} and payload : ${input.externalReferenceId}`
    );
    const result = await httpClient.post(
      `/cards/${cardId}/financialtransactions`,
      {
        ...input,
        advice: false
      },
      {
        headers: {
          [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
        }
      }
    );
    logger.debug(
      `doMakeCardWithdrawal transaction with cardId : ${cardId} and response : ${JSON.stringify(
        result.data
      )}`
    );
    logger.info(`doMakeCardWithdrawal transaction with cardId : ${cardId}`);
    return result.data;
  } catch (error) {
    logger.error(
      `Failed to makeCardWithdrawal error : 
      ${stringifiedMambuError(error)}`
    );
    return mambuErrorUtil.checkErrorReverseAuthorizationHold(error);
  }
};

const doReverseAuthorizationHold = async (
  cardReferenceToken: string,
  authHoldExtReferenceId: string
) => {
  try {
    logger.info(
      `Deleting reverse authorization hold with cardReferenceToken : ${cardReferenceToken} and authHoldExtReferenceId : ${authHoldExtReferenceId}`
    );
    await httpClient.delete(
      `/cards/${cardReferenceToken}/authorizationholds/${authHoldExtReferenceId}`
    );
    logger.info(
      `Deleted reverse authorization hold with cardReferenceToken : ${cardReferenceToken} and authHoldExtReferenceId : ${authHoldExtReferenceId}`
    );
  } catch (error) {
    logger.error(
      `Failed to reverseAuthorizationHold error : 
      ${stringifiedMambuError(error)}`
    );
    mambuErrorUtil.checkErrorReverseAuthorizationHold(error);
  }
};

const doExecuteJournalEntriesGL = async (
  input: IJournalEntriesGL,
  idempotencyKey: string = Http.generateIdempotencyKey()
): Promise<void> => {
  logger.debug(
    `Submitting journal entries GL with payload : ${JSON.stringify(input)}`
  );
  logger.info(
    `Submitting journal entries GL with transaction id : ${input.transactionID}`
  );
  await httpClient.post(`/gljournalentries`, input, {
    headers: {
      Accept: Const.ApiVersion.V1,
      [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
    }
  });
  logger.info(`doExecuteJournalEntriesGL: completed`);
};

const doCreateGlJournalEntries = async (
  input: IGLJournalEntriesV2,
  idempotencyKey: string = Http.generateIdempotencyKey()
): Promise<IGLJournalEntriesResult> => {
  logger.debug(
    `Creating journal entries GL with payload : ${JSON.stringify(input)}`
  );
  logger.info(
    `Creating journal entries GL with transaction id : ${input.transactionId}`
  );
  const result = await httpClient.post(`/gljournalentries`, input, {
    headers: { [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey }
  });
  logger.info(`doCreateGlJournalEntries: completed`);

  return { id: result.data[0].transactionId };
};

const getAvailableBalance = circuitBreakerFactory.create(doGetAvailableBalance);
const makeCardWithdrawal = circuitBreakerFactory.create(doMakeCardWithdrawal);
const makeWithdrawal = circuitBreakerFactory.create(doMakeWithdrawal);
const makeDeposit = circuitBreakerFactory.create(doMakeDeposit);
const reverseTransaction = circuitBreakerFactory.create(doReverseTransaction);
const reverseCardTransaction = circuitBreakerFactory.create(
  doReverseCardTransaction
);
const executeJournalEntriesGL = circuitBreakerFactory.create(
  doExecuteJournalEntriesGL
);
const createGlJournalEntries = circuitBreakerFactory.create(
  doCreateGlJournalEntries
);
const reverseAuthorizationHold = circuitBreakerFactory.create(
  doReverseAuthorizationHold
);
const getTransactionsByInstructionId = circuitBreakerFactory.create(
  doGetTransactionsByInstructionId
);
const getTransactionsByReferenceId = circuitBreakerFactory.create(
  doGetTransactionsByReferenceId
);
const patchTransactionDetails = circuitBreakerFactory.create(
  doPatchTransactionDetails
);

const coreBankingDepositRepository = {
  executeJournalEntriesGL,
  reverseAuthorizationHold,
  getAvailableBalance,
  makeCardWithdrawal,
  makeWithdrawal,
  makeDeposit,
  reverseTransaction,
  reverseCardTransaction,
  getTransactionsByInstructionId,
  getTransactionsByReferenceId,
  createGlJournalEntries,
  patchTransactionDetails
};

export default wrapLogs(coreBankingDepositRepository);
