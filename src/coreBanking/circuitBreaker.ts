import { Util } from '@dk/module-common';
import { config } from '../config';
import logger from '../logger';
import { HttpError } from '@dk/module-httpclient';

const coreBankingTimeout: number = config.get('coreBanking').timeout;

export default Util.createCircuitBreakerFactory(
  {
    // When timeout set as `false` then circuit breaker will not set the timeout
    timeout: false,
    resetTimeout: coreBankingTimeout,
    name: 'mambu',
    // When >50% requests in a rolling window failing, open the circuit.
    errorThresholdPercentage: 50,

    // When number of requests in a rolling window is <50, don't consider to open circuit.
    volumeThreshold: 50,

    // Don't break the circuit on HTTP status codes 200..<500.
    errorFilter: (error): boolean => {
      const httpError = (error as unknown) as HttpError;
      return httpError.status >= 200 && httpError.status < 500;
    }
  },
  logger
);
