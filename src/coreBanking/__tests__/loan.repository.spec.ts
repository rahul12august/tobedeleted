/*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
import { Http } from '@dk/module-common';
import httpClient from '../httpClient';
import Const from '../coreBanking.constant';
import coreBankingLoanRepository from '../loan.repository';
import {
  IDisbursementPayload,
  ILoanTransactionAdjustment,
  IPayOffPayload,
  IRepaymentPayload
} from '../loan.interface';

jest.mock('../../logger');
jest.mock('../httpClient');

describe('coreBankingLoanRepository', () => {
  const idempotencyKey = 'anyValue';
  beforeEach(() => {
    Http.generateIdempotencyKey = jest.fn().mockResolvedValue(idempotencyKey);
  });

  afterEach(() => {
    (Http.generateIdempotencyKey as jest.Mock).mockRestore();
  });

  describe('make disbursement transaction', () => {
    const input: IDisbursementPayload = {
      externalId: 'test123',
      valueDate: '2021-01-01',
      transactionDetails: {
        transactionChannelId: 'channelTest'
      }
    };
    const id = 'id';
    const loanAccountId = 'loanAccountId';

    beforeEach(() => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          id
        }
      });
    });

    afterEach(() => {
      jest.resetAllMocks();
    });
    it('should call coreBanking API to submit disbursement and return transaction id', async () => {
      const transaction = await coreBankingLoanRepository.makeDisbursement(
        loanAccountId,
        input,
        idempotencyKey
      );
      expect(httpClient.post).toBeCalledWith(
        `/loans/${loanAccountId}/disbursement-transactions`,
        input,
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
      expect(transaction).toEqual({ id });
    });

    it('should auto generate idempotencyKey if it is undefined', async () => {
      await coreBankingLoanRepository.makeDisbursement(loanAccountId, input);
      expect(Http.generateIdempotencyKey).toBeCalledTimes(1);
    });

    it('should throw mambu error if there is error from mambu', async () => {
      jest.resetAllMocks();
      (httpClient.post as jest.Mock).mockRejectedValueOnce(new Error('test'));

      const error = await coreBankingLoanRepository
        .makeDisbursement(loanAccountId, input, idempotencyKey)
        .catch(err => err);
      expect(httpClient.post).toBeCalledWith(
        `/loans/${loanAccountId}/disbursement-transactions`,
        input,
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
      expect(error).toBeDefined();
    });
  });

  describe('make repayment transaction', () => {
    const input: IRepaymentPayload = {
      externalId: 'test123',
      valueDate: '2021-01-01',
      transactionDetails: {
        transactionChannelId: 'channelTest'
      },
      amount: 1000000,
      customPaymentAmounts: [
        { amount: 1000000, customPaymentAmountType: 'PRINCIPAL' }
      ]
    };
    const id = 'id';
    const loanAccountId = 'loanAccountId';

    beforeEach(() => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          id
        }
      });
    });

    afterEach(() => {
      jest.resetAllMocks();
    });
    it('should call coreBanking API to submit repayment and return transaction id', async () => {
      const transaction = await coreBankingLoanRepository.makeRepayment(
        loanAccountId,
        input,
        idempotencyKey
      );
      expect(httpClient.post).toBeCalledWith(
        `/loans/${loanAccountId}/repayment-transactions`,
        input,
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
      expect(transaction).toEqual({ id });
    });

    it('should auto generate idempotencyKey if it is undefined', async () => {
      await coreBankingLoanRepository.makeRepayment(loanAccountId, input);
      expect(Http.generateIdempotencyKey).toBeCalledTimes(1);
    });

    it('should throw mambu error if there is error from mambu', async () => {
      jest.resetAllMocks();
      (httpClient.post as jest.Mock).mockRejectedValueOnce(new Error('test'));

      const error = await coreBankingLoanRepository
        .makeRepayment(loanAccountId, input, idempotencyKey)
        .catch(err => err);
      expect(httpClient.post).toBeCalledWith(
        `/loans/${loanAccountId}/repayment-transactions`,
        input,
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
      expect(error).toBeDefined();
    });
  });

  describe('make payoff transaction', () => {
    const input: IPayOffPayload = {
      externalId: 'test123',
      transactionDetails: {
        transactionChannelId: 'channelTest'
      },
      payOffAdjustableAmounts: { interestPaid: 1000 },
      notes: 'deceased'
    };
    const id = 'id';
    const loanAccountId = 'loanAccountId';

    it('should call coreBanking API to submit payoff', async () => {
      // Given
      const payOffIdempotencyKey = '1a2b3c4d5e';
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          id
        }
      });

      // When
      await coreBankingLoanRepository.makePayOff(
        loanAccountId,
        input,
        payOffIdempotencyKey
      );

      // Then
      expect(httpClient.post).toBeCalledWith(
        `/loans/${loanAccountId}:payOff`,
        input,
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: payOffIdempotencyKey
          }
        }
      );
    });

    it('should auto generate idempotencyKey if it is undefined', async () => {
      // Given
      const payOffIdempotencyKey = undefined;
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          id
        }
      });

      // When
      await coreBankingLoanRepository.makePayOff(
        loanAccountId,
        input,
        payOffIdempotencyKey
      );

      // Then
      expect(Http.generateIdempotencyKey).toBeCalledTimes(1);
    });

    it('should throw mambu error if there is error from mambu', async () => {
      // Given
      (httpClient.post as jest.Mock).mockRejectedValueOnce(new Error('test'));

      // When
      const error = await coreBankingLoanRepository
        .makePayOff(loanAccountId, input, idempotencyKey)
        .catch(err => err);

      // Then
      expect(httpClient.post).toBeCalledWith(
        `/loans/${loanAccountId}:payOff`,
        input,
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
      expect(error).toBeDefined();
    });
  });

  describe('adjust loan transaction', () => {
    const input: ILoanTransactionAdjustment = {
      type: 'TEST',
      originalTransactionId: '12345',
      notes: 'undo transaction'
    };
    const id = 'id';
    const loanAccountId = 'loanAccountId';

    beforeEach(() => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          id
        }
      });
    });

    afterEach(() => {
      jest.resetAllMocks();
    });
    it('should call coreBanking API to submit adjust loan transaction', async () => {
      await coreBankingLoanRepository.adjustLoanTransaction(
        loanAccountId,
        input,
        idempotencyKey
      );
      expect(httpClient.post).toBeCalledWith(
        `/loans/${loanAccountId}/transactions`,
        input,
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey,
            Accept: Const.ApiVersion.V1
          }
        }
      );
    });

    it('should auto generate idempotencyKey if it is undefined', async () => {
      await coreBankingLoanRepository.adjustLoanTransaction(
        loanAccountId,
        input
      );
      expect(Http.generateIdempotencyKey).toBeCalledTimes(1);
    });

    it('should throw mambu error if there is error from mambu', async () => {
      jest.resetAllMocks();
      (httpClient.post as jest.Mock).mockRejectedValueOnce(new Error('test'));

      const error = await coreBankingLoanRepository
        .adjustLoanTransaction(loanAccountId, input, idempotencyKey)
        .catch(err => err);

      expect(httpClient.post).toBeCalledWith(
        `/loans/${loanAccountId}/transactions`,
        input,
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey,
            Accept: Const.ApiVersion.V1
          }
        }
      );
      expect(error).toBeDefined();
    });
  });
});
