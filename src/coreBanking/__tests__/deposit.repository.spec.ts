/*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
import { Http } from '@dk/module-common';
import coreBankingDepositRepository from '../deposit.repository';
import httpClient from '../httpClient';
import {
  IWithdrawalInput,
  IDepositInput,
  IReverseTransactionInput,
  ICardWithdrawalInput
} from '../deposit.interface';
import Const from '../coreBanking.constant';
import { MambuRestError } from '../../blockingAmount/mambu.error.interface';
import { TransferAppError } from '../../errors/AppError';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';
import { ERROR_CODE } from '../../common/errors';

import logger from '../../logger';

jest.mock('../../logger');
jest.mock('../httpClient');

const withdrawalInput: IWithdrawalInput = {
  transactionDetails: {
    transactionChannelId: 'TFD20'
  },
  amount: 2,
  _Custom_Transaction_Details: {
    Transaction_Code: 'TC01',
    Reference_ID: 'REF01',
    Third_Party_Incoming_Id: 'EX01',
    Category_Code: 'C001',
    Payment_Service_Code: 'RTOL',
    Payment_Instruction_ID: '201910010000002',
    Additional_Information_1: 'Test',
    Additional_Information_2: 'Test',
    Additional_Information_3: 'Test',
    Additional_Information_4: 'Test'
  },
  _Custom_Source_Information: {
    Source_Account_Type: 'MA',
    Source_Account_No: '2',
    Source_CIF: 'CIF001'
  },
  _Custom_Target_Information: {
    Institutional_Bank_Code: 'BC002',
    Real_Target_Bank_Code: '542',
    Target_Account_Type: 'FS',
    Target_Account_No: 'TUBM658',
    Target_Account_Name: 'Mahesh',
    Target_Bank_Name: 'BANK ARTOS'
  }
};

describe('coreBankingDepositRepository', () => {
  const idempotencyKey = 'anyValue';
  beforeEach(() => {
    Http.generateIdempotencyKey = jest.fn().mockResolvedValue(idempotencyKey);
  });

  afterEach(() => {
    (Http.generateIdempotencyKey as jest.Mock).mockRestore();
  });

  describe('make new withdrawal', () => {
    const encodedKey = 'encodedKey';
    const depositAccountId = 'depositAccountId';

    beforeEach(() => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          encodedKey
        }
      });
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should call to coreBanking to create and return transaction id', async () => {
      const transaction = await coreBankingDepositRepository.makeWithdrawal(
        depositAccountId,
        withdrawalInput,
        idempotencyKey
      );
      expect(httpClient.post).toBeCalledWith(
        `/deposits/${depositAccountId}/withdrawal-transactions`,
        withdrawalInput,
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
      expect(transaction).toEqual({
        encodedKey
      });
    });

    it('should auto generate idempotencyKey if it is undefined', async () => {
      await coreBankingDepositRepository.makeWithdrawal(
        depositAccountId,
        withdrawalInput
      );

      expect(Http.generateIdempotencyKey).toBeCalledTimes(1);
    });

    it('should throw error from mambu', async () => {
      (httpClient.post as jest.Mock).mockRejectedValueOnce(new Error());

      const error = await coreBankingDepositRepository
        .makeWithdrawal(depositAccountId, withdrawalInput, idempotencyKey)
        .catch(err => err);

      expect(httpClient.post).toBeCalledWith(
        `/deposits/${depositAccountId}/withdrawal-transactions`,
        withdrawalInput,
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
      expect(error).toBeDefined();
    });
  });

  describe('Proper error correspondence', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('Should throw proper exception when Mambu is throwing BALANCE_BELOW_ZERO', async () => {
      const mambuExceptionNegativeBalance: MambuRestError = {
        errorCode: 401,
        errorReason: 'BALANCE_BELOW_ZERO'
      };

      (httpClient.post as jest.Mock).mockImplementation(() => {
        throw {
          error: {
            errors: [mambuExceptionNegativeBalance]
          }
        };
      });

      const result = await coreBankingDepositRepository
        .makeWithdrawal('DEP-1', withdrawalInput, idempotencyKey)
        .catch(e => e);

      expect(result).toBeInstanceOf(TransferAppError);
      expect((result as TransferAppError).actor).toBe(
        TransferFailureReasonActor.SUBMITTER
      );
      expect((result as TransferAppError).errorCode).toBe(
        ERROR_CODE.TRANSFER_AMOUNT_IS_INSUFFICIENT
      );
    });
  });

  describe('make new card withdrawal', () => {
    const input: ICardWithdrawalInput = {
      transactionChannelId: 'TFD20',
      externalAuthorizationReferenceId: 'block01',
      externalReferenceId: 'block01',
      amount: 2,
      _Custom_Transaction_Details: {
        Transaction_Code: 'TC01',
        Reference_ID: 'REF01',
        Third_Party_Incoming_Id: 'EX01',
        Category_Code: 'C001',
        Payment_Service_Code: 'RTOL',
        Payment_Instruction_ID: '201910010000002',
        Additional_Information_1: 'Test',
        Additional_Information_2: 'Test',
        Additional_Information_3: 'Test',
        Additional_Information_4: 'Test'
      },
      _Custom_Source_Information: {
        Source_Account_Type: 'MA',
        Source_Account_No: '2',
        Source_CIF: 'CIF001'
      },
      _Custom_Target_Information: {
        Institutional_Bank_Code: 'BC002',
        Real_Target_Bank_Code: '542',
        Target_Account_Type: 'FS',
        Target_Account_No: 'TUBM658',
        Target_Account_Name: 'Mahesh',
        Target_Bank_Name: 'BANK ARTOS',
        Target_CIF: 'cif'
      }
    };
    const idempotencyKey = 'anyValue';
    const encodedKey = 'encodedKey';

    beforeEach(() => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          encodedKey
        }
      });
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should call to coreBanking to create and return transaction id', async () => {
      const cardId = 'cardId01';

      const transaction = await coreBankingDepositRepository.makeCardWithdrawal(
        cardId,
        input,
        idempotencyKey
      );
      expect(httpClient.post).toBeCalledWith(
        `/cards/${cardId}/financialtransactions`,
        { ...input, advice: false },
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
      expect(transaction).toEqual({
        encodedKey
      });
    });

    it('should auto generate idempotencyKey if it is undefined', async () => {
      const cardId = 'cardId01';

      await coreBankingDepositRepository.makeCardWithdrawal(cardId, input);

      expect(Http.generateIdempotencyKey).toBeCalledTimes(1);
    });

    it('should throw error from http client', async () => {
      const cardId = 'cardId01';
      (httpClient.post as jest.Mock).mockRejectedValueOnce(new Error());

      const error = await coreBankingDepositRepository
        .makeCardWithdrawal(cardId, input, idempotencyKey)
        .catch(err => err);

      expect(httpClient.post).toBeCalledWith(
        `/cards/${cardId}/financialtransactions`,
        { ...input, advice: false },
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
      expect(error).toBeDefined();
    });
  });

  describe('make new deposit', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });
    const depositAccountId = 'depositAccountId';
    const input: IDepositInput = {
      transactionDetails: {
        transactionChannelId: 'TFC20'
      },
      amount: 2,
      _Custom_Transaction_Details: {
        Transaction_Code: 'TC01',
        Reference_ID: 'REF01',
        Third_Party_Incoming_Id: 'EX01',
        Category_Code: 'C001',
        Payment_Service_Code: 'RTOL',
        Payment_Instruction_ID: '201910010000002',
        Additional_Information_1: 'Test',
        Additional_Information_2: 'Test',
        Additional_Information_3: 'Test',
        Additional_Information_4: 'Test'
      },
      _Custom_Source_Information: {
        Source_Account_Type: 'MA',
        Source_Account_No: 'TUBM658',
        Source_CIF: 'CIF002'
      },
      _Custom_Target_Information: {
        Institutional_Bank_Code: 'BC002',
        Real_Target_Bank_Code: '542',
        Target_Account_Type: 'FS',
        Target_Account_No: '2',
        Target_Account_Name: 'Mahesh',
        Target_Bank_Name: 'BANK ARTOS'
      }
    };
    const encodedKey = 'encodedKey';

    it('should call to coreBanking to create and return transaction id', async () => {
      const idempotencyKey = 'anyValue';
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          encodedKey
        }
      });

      const transaction = await coreBankingDepositRepository.makeDeposit(
        depositAccountId,
        input,
        idempotencyKey
      );
      expect(httpClient.post).toBeCalledWith(
        `/deposits/${depositAccountId}/deposit-transactions`,
        input,
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
      expect(transaction).toEqual({
        encodedKey
      });
    });

    it('should auto generate idempotencyKey if it is undefined', async () => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          encodedKey
        }
      });

      await coreBankingDepositRepository.makeDeposit(depositAccountId, input);

      expect(Http.generateIdempotencyKey).toBeCalledTimes(1);
    });

    it('should throw error from http client', async () => {
      const idempotencyKey = 'anyValue';
      (httpClient.post as jest.Mock).mockRejectedValueOnce(new Error());

      const error = await coreBankingDepositRepository
        .makeDeposit(depositAccountId, input, idempotencyKey)
        .catch(err => err);

      expect(httpClient.post).toBeCalledWith(
        `/deposits/${depositAccountId}/deposit-transactions`,
        input,
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
      expect(error).toBeDefined();
    });
  });

  describe('reverseTransaction', () => {
    beforeEach(() => {
      jest.resetAllMocks();
    });

    const id = '1234';
    it('should call to coreBanking success', async () => {
      const idempotencyKey = 'anyValue';
      const transactionId = '123456';
      const inputRequest = {
        notes: '222'
      };
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          id
        }
      });
      const expected = await coreBankingDepositRepository.reverseTransaction(
        transactionId,
        inputRequest,
        idempotencyKey
      );

      expect(expected).toBeUndefined();
      expect(httpClient.post).toBeCalledWith(
        `/deposits/transactions/${transactionId}:adjust`,
        {
          notes: inputRequest.notes
        },
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
    });

    it('should call to coreBanking success with default notes if it is not defined', async () => {
      const idempotencyKey = 'anyValue';
      const transactionId = '123456';
      const inputRequest: IReverseTransactionInput = {};
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          id
        }
      });
      const expected = await coreBankingDepositRepository.reverseTransaction(
        transactionId,
        inputRequest,
        idempotencyKey
      );
      expect(expected).toBeUndefined();
      expect(httpClient.post).toBeCalledWith(
        `/deposits/transactions/${transactionId}:adjust`,
        {
          notes: `Adjust transaction ${transactionId}`
        },
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
    });

    it('should auto generate idempotencyKey if it is undefined', async () => {
      const transactionId = '123456';
      const encodedKey = 'encodedKey';
      const inputRequest: IReverseTransactionInput = {};
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          encodedKey
        }
      });

      await coreBankingDepositRepository.reverseTransaction(
        transactionId,
        inputRequest,
        idempotencyKey
      );

      expect(httpClient.post).toBeCalledWith(
        `/deposits/transactions/${transactionId}:adjust`,
        {
          notes: `Adjust transaction ${transactionId}`
        },
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
    });

    it('should throw error from http client', async () => {
      (httpClient.post as jest.Mock).mockRejectedValueOnce(new Error());
      const idempotencyKey = 'anyValue';
      const transactionId = '123456';
      const inputRequest = {
        notes: '222'
      };
      const error = await coreBankingDepositRepository
        .reverseTransaction(transactionId, inputRequest, idempotencyKey)
        .catch(err => err);

      expect(error).toBeDefined();
      expect(httpClient.post).toBeCalledWith(
        `/deposits/transactions/${transactionId}:adjust`,
        {
          notes: inputRequest.notes
        },
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
    });
  });

  describe('reverseCardTransaction', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should call to coreBanking success', async () => {
      const idempotencyKey = 'anyValue';
      const transactionId = '123456';
      const inputRequest = {
        amount: 10,
        externalReferenceId: 'ref01'
      };
      const cardId = 'card01';
      await coreBankingDepositRepository.reverseCardTransaction(
        cardId,
        transactionId,
        inputRequest,
        idempotencyKey
      );

      expect(httpClient.post).toBeCalledWith(
        `/cards/${cardId}/financialtransactions/${transactionId}:decrease`,
        inputRequest,
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
    });

    it('should auto generate idempotencyKey if it is undefined', async () => {
      const transactionId = '123456';
      const inputRequest = {
        amount: 10,
        externalReferenceId: 'ref01'
      };
      const cardId = 'card01';
      (Http.generateIdempotencyKey as jest.Mock).mockReturnValueOnce(
        idempotencyKey
      );
      await coreBankingDepositRepository.reverseCardTransaction(
        cardId,
        transactionId,
        inputRequest
      );

      expect(httpClient.post).toBeCalledWith(
        `/cards/${cardId}/financialtransactions/${transactionId}:decrease`,
        inputRequest,
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
    });

    it('should throw error from http client', async () => {
      const idempotencyKey = 'anyValue';
      const transactionId = '123456';
      const inputRequest = {
        amount: 10,
        externalReferenceId: 'ref01'
      };
      const cardId = 'card01';
      (httpClient.post as jest.Mock).mockRejectedValueOnce(new Error());

      const error = await coreBankingDepositRepository
        .reverseCardTransaction(
          cardId,
          transactionId,
          inputRequest,
          idempotencyKey
        )
        .catch(err => err);

      expect(httpClient.post).toBeCalledWith(
        `/cards/${cardId}/financialtransactions/${transactionId}:decrease`,
        inputRequest,
        {
          headers: {
            [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
          }
        }
      );
      expect(error).toBeDefined();
    });
  });

  describe('get transactions by instruction id', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should call core banking api and return list of transactions', async () => {
      const id = '1';
      const transactions = [{ id: '1' }];
      const creationDate = new Date('2022-03-23');
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: transactions
      });

      const result = await coreBankingDepositRepository.getTransactionsByInstructionId(
        id,
        creationDate
      );

      expect(httpClient.post).toBeCalledWith('/deposits/transactions:search', {
        sortingCriteria: {
          field: 'id',
          order: 'DESC'
        },
        filterCriteria: [
          {
            field: '_Custom_Transaction_Details.Payment_Instruction_ID',
            operator: 'EQUALS_CASE_SENSITIVE',
            value: id
          },
          {
            field: 'creationDate',
            operator: 'ON',
            value: '2022-03-23'
          }
        ]
      });
      expect(result).toBe(transactions);
    });
  });

  describe('get transactions by reference id', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should call core banking api and return list of transactions by referenceId', async () => {
      const id = '1';
      const transactions = [{ id: '1' }];
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: transactions
      });

      const result = await coreBankingDepositRepository.getTransactionsByReferenceId(
        id
      );

      expect(httpClient.post).toBeCalledWith(
        '/deposits/transactions:search?detailsLevel=FULL',
        {
          sortingCriteria: {
            field: 'creationDate',
            order: 'DESC'
          },
          filterCriteria: [
            {
              field: '_Custom_Transaction_Details.Reference_ID',
              operator: 'EQUALS_CASE_SENSITIVE',
              value: id
            }
          ]
        }
      );
      expect(result).toBe(transactions);
    });
  });

  describe('get available balance', () => {
    let infoMsgs: string[] = [];

    beforeEach(() => {
      (logger.info as jest.Mock).mockImplementation((msg: string) => {
        infoMsgs.push(msg);
      });
    });

    afterEach(() => {
      jest.resetAllMocks();
      infoMsgs = [];
    });

    it('should call to corebanking and return available balance', async () => {
      const availableBalance = 12;
      (httpClient.get as jest.Mock).mockResolvedValueOnce({
        data: {
          balances: {
            availableBalance
          }
        }
      });
      const accountNumber = 'accountNumber';
      const result = await coreBankingDepositRepository.getAvailableBalance(
        accountNumber
      );
      expect(httpClient.get).toBeCalledWith(`/deposits/${accountNumber}`);
      expect(result).toEqual(availableBalance);
    });

    it('should throw error from http client', async () => {
      (httpClient.get as jest.Mock).mockRejectedValueOnce(new Error());
      const accountNumber = 'accountNumber';

      const error = await coreBankingDepositRepository
        .getAvailableBalance(accountNumber)
        .catch(err => err);

      expect(httpClient.get).toBeCalledWith(`/deposits/${accountNumber}`);
      expect(error).toBeDefined();
    });

    it('Should print the resulting balance with info level', async () => {
      const availableBalance = 12;
      (httpClient.get as jest.Mock).mockResolvedValueOnce({
        data: {
          balances: {
            availableBalance
          }
        }
      });

      await coreBankingDepositRepository.getAvailableBalance('123456');
      expect(infoMsgs).toContain(
        `doGetAvailableBalance completed and available balance ${availableBalance}.`
      );
    });
  });

  describe('reverseAuthorizationHold', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should call to coreBanking success', async () => {
      // give
      const cardId = 'BLOCK_test123';
      const blockId = 'card01';

      (httpClient.delete as jest.Mock).mockResolvedValueOnce({});
      // when
      await coreBankingDepositRepository.reverseAuthorizationHold(
        cardId,
        blockId
      );

      // then
      const expectedPath = `/cards/${cardId}/authorizationholds/${blockId}`;
      expect(httpClient.delete).toBeCalledWith(expectedPath);
    });

    it('should call to coreBanking success', async () => {
      // give
      const cardId = 'BLOCK_test123';
      const blockId = 'card01';
      (httpClient.delete as jest.Mock).mockRejectedValueOnce(new Error());

      // when
      const error = await coreBankingDepositRepository
        .reverseAuthorizationHold(cardId, blockId)
        .catch(err => err);

      // then
      const expectedPath = `/cards/${cardId}/authorizationholds/${blockId}`;
      expect(httpClient.delete).toBeCalledWith(expectedPath);
      expect(error).toBeDefined();
    });
  });

  describe('executeJournalEntriesGL', () => {
    it('should call to coreBanking success', async () => {
      //given
      const input = {
        date: '2020-02-24',
        debitAccount1: '01.01.00.00.00.00',
        debitAmount1: 30000,
        creditAccount1: '02.06.01.01.00.00',
        creditAmount1: 30000,
        transactionID: 'test'
      };
      const expectedConfig = {
        headers: {
          Accept: Const.ApiVersion.V1,
          [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
        }
      };
      (httpClient.post as jest.Mock).mockResolvedValueOnce({});
      //when
      const expected = await coreBankingDepositRepository.executeJournalEntriesGL(
        input,
        idempotencyKey
      );
      //then
      expect(expected).toBeUndefined();
      expect(httpClient.post).toBeCalledWith(
        `/gljournalentries`,
        input,
        expectedConfig
      );
    });
  });

  describe('doCreateGlJournalEntries', () => {
    it('should call to coreBanking success', async () => {
      const transactionId = 'test123';
      //given
      const input = {
        date: '2020-02-24',
        debits: [
          {
            amount: 30000,
            glAccount: '01.01.00.00.00.00'
          }
        ],
        credits: [
          {
            amount: 3000,
            glAccount: '02.06.01.01.00.00'
          }
        ],
        transactionId: 'test'
      };
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: [{ transactionId }]
      });
      //when
      const expected = await coreBankingDepositRepository.createGlJournalEntries(
        input
      );
      //then
      expect(expected.id).toEqual(transactionId);
      expect(httpClient.post).toBeCalled();
    });
  });
});
