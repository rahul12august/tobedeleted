/*eslint @typescript-eslint/camelcase: ["error", {properties: "never"}]*/
import httpClient from '../httpClient';

describe('CoreBanking HTTP Client', () => {
  it('should have a timeout value of 60 seconds', function() {
    expect((httpClient as any).apiConfig.timeout).toEqual(60000);
  });
});
