import { HttpError } from '@dk/module-httpclient';
import repository from '../block.repository';
import service from '../block.service';

jest.mock('../block.repository');

describe('block.service', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('createBlockingAmount', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should save blocking amount', async () => {
      const cardReferenceToken = 'cardReferenceToken1';
      const inputBlockingAmount = {
        externalReferenceId: 'externalReferenceId1',
        amount: 1000,
        advice: true,
        currencyCode: 'currencyCode1'
      };

      (repository.createBlockingAmount as jest.Mock).mockResolvedValueOnce(
        inputBlockingAmount
      );

      const result = await service.createBlockingAmount(
        cardReferenceToken,
        inputBlockingAmount
      );

      expect(result).toBe(inputBlockingAmount);
    });
  });

  describe('decreaseAuthorizationHold', () => {
    it('should retry decreaseAuthorizationHold first time if fail with 500', async () => {
      (repository.decreaseAuthorizationHold as jest.Mock).mockRejectedValue(
        new HttpError(500, 'error')
      );

      await service
        .decreaseAuthorizationHold('token', 'blockingAmountId', 123)
        .catch(() => {
          expect(repository.decreaseAuthorizationHold).toBeCalledTimes(3);
        });
    });

    it('should not retry decreaseAuthorizationHold first time if fail with 500 and shouldRetry false', async () => {
      (repository.decreaseAuthorizationHold as jest.Mock).mockRejectedValue(
        new HttpError(500, 'error')
      );

      await service
        .decreaseAuthorizationHold('token', 'blockingAmountId', 123, false)
        .catch(() => {
          expect(repository.decreaseAuthorizationHold).toBeCalledTimes(1);
        });
    });
  });

  describe('increaseAuthorizationHold', () => {
    it('should save increaseAuthorizationHold', async () => {
      const cardReferenceToken = 'cardReferenceToken1';
      const authHoldExtReferenceId = 'authHoldExtReferenceId1';
      const amount = 1000;

      (repository.increaseAuthorizationHold as jest.Mock).mockResolvedValueOnce(
        undefined
      );

      const result = await service.increaseAuthorizationHold(
        cardReferenceToken,
        authHoldExtReferenceId,
        amount
      );

      expect(result).toBe(undefined);
    });
  });
});
