import httpClient from '../httpClient';
import Const from '../coreBanking.constant';
import blockRepository from '../block.repository';

jest.mock('../httpClient');

describe('block.repository', () => {
  beforeEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  const idempotencyKey = 'idempotencyKey1';
  const baseCardPath = '/cards';
  const expectHttpConfig = {
    headers: {
      [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey
    }
  };

  describe('createBlockingAmount', () => {
    const cardReferenceToken = 'cardReferenceToken1';
    const inputBlockingAmount = {
      externalReferenceId: 'externalReferenceId1',
      amount: 1000,
      advice: true,
      currencyCode: 'currencyCode1'
    };

    it('should return void for creating blocking amount', async () => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({});
      const result = await blockRepository.createBlockingAmount(
        cardReferenceToken,
        inputBlockingAmount,
        idempotencyKey
      );
      expect(result).toBeUndefined();
      expect(httpClient.post).toHaveBeenCalledWith(
        `${baseCardPath}/${cardReferenceToken}/authorizationholds`,
        inputBlockingAmount,
        expectHttpConfig
      );
    });
  });

  describe('increaseAuthorizationHold', () => {
    it('should call to core banking api to increase the hold', async () => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({});

      await blockRepository.increaseAuthorizationHold(
        'token',
        'id',
        1000,
        idempotencyKey
      );

      expect(httpClient.post).toBeCalledWith(
        `${baseCardPath}/token/authorizationholds/id:increase`,
        {
          amount: 1000
        },
        expectHttpConfig
      );
    });
  });

  describe('decreaseAuthorizationHold', () => {
    it('should call to core banking api to increase the hold', async () => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce({});

      await blockRepository.decreaseAuthorizationHold(
        'token',
        'id',
        1000,
        idempotencyKey
      );

      expect(httpClient.post).toBeCalledWith(
        `${baseCardPath}/token/authorizationholds/id:decrease`,
        {
          amount: 1000
        },
        expectHttpConfig
      );
    });
  });
});
