import { HttpError } from '@dk/module-httpclient';
import { getRetryCount } from '../common/contextHandler';

export const RetryConfig = () => ({
  shouldRetry: (error: HttpError) => {
    return error.status >= 500;
  },
  numberOfTries: getRetryCount() || 1
});

export const stringifiedMambuError = (error: any) =>
  (error &&
    error.error &&
    error.error.errors &&
    JSON.stringify(error.error.errors)) ||
  error.message;
