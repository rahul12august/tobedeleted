import { Http, Util } from '@dk/module-common';
import { getRetryCount } from '../common/contextHandler';
import { wrapLogs } from '../logger';
import { ICoreBankingBlockingInput } from './block.interface';
import blockRepository from './block.repository';
import { RetryConfig } from './coreBanking.util';

const createBlockingAmount = (
  cardReferenceToken: string,
  inputBlockingAmount: ICoreBankingBlockingInput
) =>
  Util.retry(
    RetryConfig(),
    blockRepository.createBlockingAmount,
    cardReferenceToken,
    inputBlockingAmount,
    Http.generateIdempotencyKey()
  );

const increaseAuthorizationHold = (
  cardReferenceToken: string,
  authHoldExtReferenceId: string,
  amount: number
) =>
  blockRepository.increaseAuthorizationHold(
    cardReferenceToken,
    authHoldExtReferenceId,
    amount,
    Http.generateIdempotencyKey()
  );

const decreaseAuthorizationHold = (
  cardReferenceToken: string,
  authHoldExtReferenceId: string,
  amount: number,
  shouldRetry: boolean = true
) =>
  Util.retry(
    {
      ...RetryConfig(),
      ...(shouldRetry && { numberOfTries: getRetryCount() })
    },
    blockRepository.decreaseAuthorizationHold,
    cardReferenceToken,
    authHoldExtReferenceId,
    amount,
    Http.generateIdempotencyKey()
  );

const blockService = {
  createBlockingAmount,
  increaseAuthorizationHold,
  decreaseAuthorizationHold
};

export default wrapLogs(blockService);
