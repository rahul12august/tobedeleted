export enum AmountTypeEnum {
  PRINCIPAL = 'PRINCIPAL',
  INTEREST = 'INTEREST',
  PENALTY = 'PENALTY',
  FEE = 'FEE'
}

export enum AdjustLoanTransactionTypeEnum {
  UNDO_REPAYMENT = 'REPAYMENT_ADJUSTMENT',
  UNDO_DISBURSEMENT = 'DISBURSMENT_ADJUSTMENT' //the parameter for the disbursement adjustment call is currently misspelt (i.e. DISBURSMENT_ADJUSTMENT instead of DISBURSEMENT_ADJUSTMENT), need to check the documentation when there is new mambu update version to fix the misspelt.
}
