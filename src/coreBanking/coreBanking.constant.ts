import { CoreBanking } from '@dk/module-common';
const ApiVersion = CoreBanking.ApiVersion;

export default {
  IDEMPOTENCY_KEY_HEADER: 'Idempotency-Key',
  VALUE_DATE_MAMBU_FORMAT: 'YYYY-MM-DDT00:00:00Z',
  ApiVersion
};
