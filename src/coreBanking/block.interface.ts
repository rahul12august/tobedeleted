export interface ICoreBankingBlockingInput {
  externalReferenceId: string;
  amount: number;
  cardAcceptor?: {
    mcc: string;
  };
  advice: boolean;
  currencyCode: string;
  idempotencyKey?: string;
}
