import { CoreBanking } from '@dk/module-common';
import { config } from '../config';
import { context } from '../context';
import logger from '../logger';
import { retryConfig } from '../common/constant';
import { AxiosRequestConfig } from 'axios';
import Const from './coreBanking.constant';

const coreBankingConfig = config.get('coreBanking');

export const logRequestInterceptor = (
  config: AxiosRequestConfig
): AxiosRequestConfig => {
  try {
    const idempotencyKey = config.headers
      ? config.headers[Const.IDEMPOTENCY_KEY_HEADER]
      : null;
    logger.info(
      `Intercepting ${config.method} ${config.url} with idempotencyKey: ${idempotencyKey}`
    );
  } finally {
    return config;
  }
};

const httpClient = CoreBanking.createCoreBankingHttpClient(
  {
    ...coreBankingConfig,
    headers: {
      apiKey: process.env.MAMBU_PASSWORD
    },
    retryConfig: {
      ...retryConfig,
      networkErrorCodesToRetry: [
        'ENOENT',
        'ECONNRESET',
        'ECONNREFUSED',
        'ECONNABORTED',
        'ETIMEDOUT'
      ],
      maxRetriesOnError: 5,
      isBackOffRetry: true,
      statusCodesToRetry: ['409', '^5']
    }
  },
  logger,
  context
);

logger.info(
  `Setting up http-client with coreBanking on address : ${coreBankingConfig.baseURL}`
);

httpClient.setRequestInterceptor(logRequestInterceptor);

export default httpClient;
