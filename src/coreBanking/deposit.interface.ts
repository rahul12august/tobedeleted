/*eslint @typescript-eslint/camelcase: [error, {properties: never}]*/

import { PatchOperation } from './deposit.enum';

export interface ICustomTransactionDetails {
  Category_Code?: string;
  Payment_Service_Code: string;
  Payment_Instruction_ID: string;
  Reference_ID: string;
  Third_Party_Incoming_Id: string;
  Additional_Information_1?: string;
  Additional_Information_2?: string;
  Additional_Information_3?: string;
  Additional_Information_4?: string;
  Original_Amount?: number;
  Transaction_Code: string;
  Notes?: string;
  Currency?: string;
  Original_Currency?: string;
  Exchange_Rate?: number;
  Third_Party_Outgoing_Id?: string;
  Transaction_Id: string;
  PI_Id?: string;
  Request_Id: string;
}

export interface ICustomSourceInformation {
  Source_Account_Type?: string;
  Source_Account_No?: string;
  Source_Account_Name?: string;
  Source_CIF?: string;
  Source_Bank_Code?: string;
  Executor_CIF?: string;
}

export interface ICustomTargetInformation {
  Institutional_Bank_Code?: string;
  Real_Target_Bank_Code?: string;
  Target_Account_Type?: string;
  Target_Account_No?: string;
  Target_Account_Name?: string;
  Target_Bank_Name?: string;
  Target_CIF?: string;
}

export interface ITransactionInputCustomFields {
  _Custom_Transaction_Details: ICustomTransactionDetails;
  _Custom_Source_Information: ICustomSourceInformation;
  _Custom_Target_Information: ICustomTargetInformation;
}

interface ITransactionInputBasicFields {
  transactionDetails: {
    transactionChannelId: string; // transactionCode channel
  };
  amount: number;
  notes?: string;
}

export type ITransactionInput = ITransactionInputCustomFields &
  ITransactionInputBasicFields;

export interface IFeeTransactionInput {
  feeTransactionPayload: ITransactionInput;
  feeIdempotencyKey?: string;
}

export interface IWithdrawalInput extends ITransactionInput {}

export interface IDepositInput extends ITransactionInput {}

export interface ICardWithdrawalInput {
  amount: number;
  externalAuthorizationReferenceId: string;
  externalReferenceId: string;
  transactionChannelId: string;
  _Custom_Transaction_Details: ICustomTransactionDetails;
  _Custom_Source_Information: ICustomSourceInformation;
  _Custom_Target_Information: ICustomTargetInformation;
}

export interface IFeeCardTransactionInput {
  feeCardTransactionPayload: ICardWithdrawalInput;
  feeCardIdempotencyKey?: string;
}

export interface ICardTransaction {
  encodedKey: string;
  linkedTransaction: {
    linkedTransactionKey: string;
  };
}

export interface IDepositTransaction {
  id: string;
  encodedKey: string;
  adjustmentTransactionKey?: string;
  type: string;
  amount: number;
  cardTransaction?: {
    cardToken: string;
    externalReferenceId: string;
    amount: number;
  };
}
export interface IReverseTransactionInput {
  notes?: string;
}

export interface IReverseCardTransactionInput {
  amount: number;
  externalReferenceId: string;
}

export interface ICoreBankingDepositAccount {
  balances: {
    availableBalance: number;
  };
}
export interface IJournalEntriesGL {
  date: string; // string because coreBanking format sort date: YYYY-MM-DD
  debitAccount1?: string;
  debitAmount1?: number;
  creditAccount1?: string;
  creditAmount1?: number;
  notes?: string;
  transactionID: string;
}

export interface GLAccountAmount {
  amount: number;
  glAccount: string;
}

export interface IGLJournalEntriesV2 {
  date: string;
  transactionId?: string;
  branchId?: string;
  credits?: GLAccountAmount[];
  debits?: GLAccountAmount[];
  notes?: string;
}

export interface IGLJournalEntriesResult {
  id: string;
}

export interface IPatchTransactionDetail {
  op: PatchOperation;
  path: string;
  from?: string;
  value?: string;
}
