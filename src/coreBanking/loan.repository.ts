import { Http } from '@dk/module-common';
import logger, { wrapLogs } from '../logger';
import httpClient from './httpClient';
import {
  IDisbursementPayload,
  IDisbursementTransaction,
  IRepaymentPayload,
  IRepaymentTransaction,
  ILoanTransactionAdjustment,
  IPayOffPayload
} from './loan.interface';
import Const from './coreBanking.constant';
import { stringifiedMambuError } from './coreBanking.util';
import circuitBreakerFactory from './circuitBreaker';

const doMakeDisbursement = async (
  loanAccountId: string,
  input: IDisbursementPayload,
  idempotencyKey: string = Http.generateIdempotencyKey()
): Promise<IDisbursementTransaction> => {
  try {
    logger.info(
      `Submitting Mambu Disbursement transaction accountId: ${loanAccountId}`
    );
    const result = await httpClient.post<IDisbursementTransaction>(
      `/loans/${loanAccountId}/disbursement-transactions`,
      input,
      { headers: { [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey } }
    );
    logger.debug(
      `doMakeDisbursement with accountId: ${loanAccountId} and response: ${JSON.stringify(
        result.data
      )}`
    );
    logger.info(
      `doMakeDisbursement with accountId: ${loanAccountId} and response with mambu id: ${result.data.id}`
    );
    return result.data;
  } catch (error) {
    logger.error(
      `Failed to makeDisbursement with error : ${stringifiedMambuError(error)}`
    );
    throw error;
  }
};

const doMakeRepayment = async (
  loanAccountId: string,
  input: IRepaymentPayload,
  idempotencyKey: string = Http.generateIdempotencyKey()
): Promise<IRepaymentTransaction> => {
  try {
    logger.info(
      `Submitting Mambu Repayment transaction accountId: ${loanAccountId}`
    );

    const result = await httpClient.post<IRepaymentTransaction>(
      `/loans/${loanAccountId}/repayment-transactions`,
      input,
      { headers: { [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey } }
    );
    logger.debug(
      `doMakeRepayment with accountId: ${loanAccountId} and response ${JSON.stringify(
        result.data
      )}`
    );
    logger.info(
      `doMakeRepayment with accountId: ${loanAccountId} and response with mambu id: ${result.data.id}`
    );
    return result.data;
  } catch (error) {
    logger.error(
      `Failed to makeRepayment with error : ${stringifiedMambuError(error)}`
    );
    throw error;
  }
};

const doMakePayOff = async (
  loanAccountId: string,
  input: IPayOffPayload,
  idempotencyKey: string = Http.generateIdempotencyKey()
): Promise<void> => {
  try {
    logger.info(
      `Submitting Mambu pay off transaction accountId: ${loanAccountId}`
    );

    await httpClient.post(`/loans/${loanAccountId}:payOff`, input, {
      headers: { [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey }
    });
  } catch (error) {
    logger.error(
      `Failed to makePayOff with error : ${stringifiedMambuError(
        error
      )} , payload: ${JSON.stringify(input)}`
    );
    throw error;
  }
};

const doAdjustLoanTransaction = async (
  loanAccountId: string,
  input: ILoanTransactionAdjustment,
  idempotencyKey: string = Http.generateIdempotencyKey()
): Promise<void> => {
  try {
    logger.debug(
      `Adjust loan transaction with payload: ${JSON.stringify(input)}`
    );
    logger.info(`Adjust loan transaction with account id : ${loanAccountId}`);
    const result = await httpClient.post(
      `/loans/${loanAccountId}/transactions`,
      input,
      {
        headers: {
          [Const.IDEMPOTENCY_KEY_HEADER]: idempotencyKey,
          Accept: Const.ApiVersion.V1
        }
      }
    );
    logger.debug(
      `doAdjustLoanTransaction with accountId: ${loanAccountId} and response ${JSON.stringify(
        result.data
      )}`
    );
    logger.info(
      `doAdjustLoanTransaction with accountId: ${loanAccountId} and response with transaction id: ${result.data.transactionId}`
    );
  } catch (error) {
    logger.error(
      `Failed to adjustLoanTransaction with error : ${stringifiedMambuError(
        error
      )}`
    );
    throw error;
  }
};

const makeDisbursement = circuitBreakerFactory.create(doMakeDisbursement);
const makeRepayment = circuitBreakerFactory.create(doMakeRepayment);
const makePayOff = circuitBreakerFactory.create(doMakePayOff);
const adjustLoanTransaction = circuitBreakerFactory.create(
  doAdjustLoanTransaction
);

const coreBankingLoanRepository = {
  makeDisbursement,
  makeRepayment,
  makePayOff,
  adjustLoanTransaction
};

export default wrapLogs(coreBankingLoanRepository);
