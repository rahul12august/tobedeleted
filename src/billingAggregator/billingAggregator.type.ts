export interface IBillingAggregatorRetryPolicy {
  maxRetries: number;
  delayTimeInSeconds: number;
  firstUpdateTimeInSeconds?: number;
}
