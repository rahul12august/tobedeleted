export default {
  getBillPaymentStatus: jest.fn(),
  submitPayment: jest.fn()
};
