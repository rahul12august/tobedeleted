export default {
  submitPayment: jest.fn(),
  updateBillPaymentStatus: jest.fn(),
  updateTransactionWithResult: jest.fn()
};
