import { IPaymentResponse } from '../goBills.type';
import { BillPaymentTransactionStatusEnum } from '../../../billPayment/billPaymentTransaction/billPaymentTransaction.enum';

export const submitPaymentInput = {
  orderId: 'ORDER123',
  inquiryId: 'INQUIRY123',
  accountNo: 'ACCOUNT123',
  transactionId: 'TRANSACTION123',
  createdAt: new Date('01 June 2020 00:00:00'),
  amount: 10000
};

export const postPaymentResponse = (): IPaymentResponse => ({
  orderId: 'ORDER123',
  amount: 50000,
  trxDate: '1579681608',
  skuTag: 'pln_prepaid_50k'
});

export const getGoBillsData = () => ({
  amount: 13500,
  orderId: 'test121dekat',
  productTag: 'pln_prepaid',
  refNumber: '4906d7c0-f395-40cb-920d-a06ed186bfb4',
  skuTag: 'pln_prepaid_10',
  status: BillPaymentTransactionStatusEnum.SUCCESS,
  trxDate: '1586943163',
  voucherCode: '8081-7887-1847-4059-2081',
  orderDetail: {
    name: 'pln_prepaid'
  }
});

export const getGoBillsDataWithFailureReason = () => ({
  amount: 13500,
  orderId: 'test121dekat',
  productTag: 'pln_prepaid',
  refNumber: '4906d7c0-f395-40cb-920d-a06ed186bfb4',
  skuTag: 'pln_prepaid_10',
  status: 'SUCCESS',
  trxDate: '1586943163',
  voucherCode: '8081-7887-1847-4059-2081',
  failureReason: 'Failure',
  orderDetail: {
    name: 'pln_prepaid'
  }
});
