import { IPaymentRequest, IPaymentResponse } from './goBills.type';
import { IBillPaymentTransactionModel } from '../../billPayment/billPaymentTransaction/billPaymentTransaction.type';
import { BillPaymentTransactionStatusEnum } from '../../billPayment/billPaymentTransaction/billPaymentTransaction.enum';

export const getTransactionPayload = (
  input: IPaymentRequest,
  result: IPaymentResponse | null,
  error?: string
): IBillPaymentTransactionModel => ({
  orderId: input.orderId,
  transactionId: input.transactionId,
  referenceId: input.inquiryId,
  status: [
    {
      status: result
        ? BillPaymentTransactionStatusEnum.TRANSACTION_SUBMITTED
        : BillPaymentTransactionStatusEnum.TRANSACTION_SUBMIT_FAILED,
      reason: error,
      capturedAt: new Date()
    }
  ],
  accountNo: input.accountNo,
  createdAt: input.createdAt || new Date(),
  amount: input.amount
});
