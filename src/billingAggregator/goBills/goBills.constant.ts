const UrlConstants = {
  GET_PAYMENT_STATUS: '/order',
  POST_PAYMENT: '/payment'
};

export { UrlConstants };
