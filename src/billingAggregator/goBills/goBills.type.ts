import { BillPaymentTransactionStatusEnum } from '../../billPayment/billPaymentTransaction/billPaymentTransaction.enum';

export interface IPaymentResponse {
  orderId: string;
  amount: number;
  trxDate: string;
  skuTag: string;
}

export interface IPaymentRequest {
  orderId: string;
  inquiryId: string;
  accountNo?: string;
  transactionId: string;
  createdAt: Date;
  amount: number;
}

export interface IBillPaymentStatus {
  orderId: string;
  status: BillPaymentTransactionStatusEnum;
  voucherCode: string;
  amount: string;
  trxDate: string;
  skuTag: string;
  refNumber: string;
  failureReason?: string;
  orderDetail?: any;
}
