import { createHttpClient, HttpClient } from '@dk/module-httpclient';
import { config } from '../../config';
import logger from '../../logger';
import { context } from '../../context';

const token = process.env.GOBILLS_BEARER_TOKEN;
const baseURL = config.get('billingAggregator').goBills;
const httpClient: HttpClient = createHttpClient({
  baseURL,
  headers: {
    Authorization: `Bearer ${token}`
  },
  timeout: config.get('goBills').timeout,
  context,
  logger
});
logger.info(
  `Setting up http-client with billing aggregator on address: ${baseURL}`
);
export default httpClient;
