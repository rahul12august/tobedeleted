import logger, { wrapLogs } from '../../logger';
import { get } from 'lodash';
import httpClient from './goBillsHttpClient';
import { UrlConstants } from './goBills.constant';
import { IBillPaymentStatus } from './goBills.type';
import { IPaymentResponse } from './goBills.type';
import circuitBreakerFactory from './circuitBreaker';
import { updateRequestIdInRedis } from '../../common/contextHandler';

const log = (httpMethod: string, response: any): void => {
  logger.debug(
    `Response from goBills ${httpMethod} payment ${
      response.status
    } and body ${JSON.stringify(response.data)}`
  );
};

const doSubmitPayment = async (
  orderId: string,
  inquiryId: string
): Promise<IPaymentResponse> => {
  logger.info(
    `Bill Payment : Executing goBills POST payment for orderId : ${orderId}, inquiryId : ${inquiryId}`
  );
  try {
    updateRequestIdInRedis(orderId);
    const result = await httpClient.post(UrlConstants.POST_PAYMENT, {
      orderId: orderId,
      inquiryId: inquiryId
    });
    log('POST', result);
    const response = get(result, ['data', 'data']);
    logger.info(
      `doSubmitPayment: Response from goBills
        orderId: ${response.orderId},
        skuTag: ${response.skuTag}`
    );
    return response;
  } catch (err) {
    logger.error(
      `Bill Payment : Error in submitting bill payment status : ${JSON.stringify(
        err
      )} with orderId : ${orderId}, inquiryId : ${inquiryId}`
    );
    throw err;
  }
};

const doGetBillPaymentStatus = async (
  orderId: string
): Promise<IBillPaymentStatus> => {
  logger.info(
    `Bill Payment : Executing goBills GET payment status API : ${orderId}`
  );
  try {
    const response = await httpClient.get(
      `${UrlConstants.GET_PAYMENT_STATUS}/${orderId}`
    );
    log('GET', response);
    const result = get(response, ['data', 'data'], []);
    logger.info(
      `doGetBillPaymentStatus: Response from goBills with
        orderId: ${result.orderId},
        skuTag: ${result.skuTag},
        status: ${result.status},
        refNumber: ${result.refNumber},
        failureReason: ${result.failureReason}`
    );
    return result;
  } catch (error) {
    logger.error(
      `Bill Payment : Error in goBills GET payment status API : ${error.message} and ${error.code} with orderId : ${orderId}`
    );
    throw error;
  }
};

const goBillsRepository = {
  submitPayment: circuitBreakerFactory.create(doSubmitPayment),
  getBillPaymentStatus: circuitBreakerFactory.create(doGetBillPaymentStatus)
};

export default wrapLogs(goBillsRepository);
