import repository from './goBills.repository';
import {
  IBillPaymentStatus,
  IPaymentRequest,
  IPaymentResponse
} from './goBills.type';
import billPaymentTxnService from '../../billPayment/billPaymentTransaction/billPaymentTransaction.service';
import { IBillPaymentTransactionModel } from '../../billPayment/billPaymentTransaction/billPaymentTransaction.type';
import { getTransactionPayload } from './goBills.payload';
import { BillPaymentTransactionStatusEnum } from '../../billPayment/billPaymentTransaction/billPaymentTransaction.enum';
import logger, { wrapLogs } from '../../logger';
import {
  DEBOUNCE_MAX_ATTEMPTS,
  debounceWhile,
  getInitialDelayForUpdate,
  goBillsErrorTranslator,
  RetryConfig
} from './goBills.util';
import { IConfirmTransactionInput } from '../../transaction/transaction.type';
import switchingTransactionService from '../../transaction/transactionConfirmation.service';
import paymentStatusPollingService from '../../billPayment/paymentStatusPolling/paymentStatusPolling.service';
import { TransferAppError } from '../../errors/AppError';
import { Util } from '@dk/module-common';
import { formatTransactionDate } from '../../transaction/transaction.util';
import {
  ConfirmTransactionStatus,
  TransferFailureReasonActor
} from '../../transaction/transaction.enum';
import billDetailService from '../../billPayment/billDetail/billDetail.service';
import { ERROR_CODE } from '../../common/errors';

const updateCoreBanking = async (
  inquiryId: string,
  orderId: string,
  status: boolean,
  refNumber?: string,
  voucherCode?: string
): Promise<void> => {
  logger.info(
    `Bill Payment : Updating Core Banking for inquiryId : ${inquiryId}, status:${status}, refNumber:${refNumber}, voucherCode:${voucherCode}`
  );
  const transaction = await billPaymentTxnService.getTransactionStatus(orderId);
  const payload: IConfirmTransactionInput = {
    externalId: inquiryId,
    accountNo: transaction?.accountNo,
    transactionDate: transaction?.createdAt
      ? formatTransactionDate(transaction?.createdAt)
      : '',
    responseCode: status
      ? ConfirmTransactionStatus.SUCCESS
      : ConfirmTransactionStatus.ERROR_UNEXPECTED,
    additionalInformation1: refNumber,
    additionalInformation2: voucherCode,
    amount: transaction?.amount
  };
  logger.info(
    `Bill Payment: Transaction Confirmation for transactionId : ${transaction?.transactionId} with inquiry id : ${inquiryId}`
  );
  await switchingTransactionService.confirmTransaction(payload);
};

const updateTransactionDB = async (
  status: BillPaymentTransactionStatusEnum,
  orderId: string,
  error?: string
): Promise<IBillPaymentTransactionModel | null> =>
  billPaymentTxnService.updateStatus(
    { status: status, reason: error, capturedAt: new Date() },
    orderId
  );

const getStatusFromGoBills = (orderId: string): Promise<IBillPaymentStatus> =>
  repository.getBillPaymentStatus(orderId);

const updateTransactionWithResult = async (
  res: IBillPaymentStatus,
  orderId: string,
  updateBillDetails: boolean = true
): Promise<IBillPaymentTransactionModel | null> => {
  logger.info(`Updating ${res.status} for orderId: ${orderId}`);
  const result = await updateTransactionDB(
    res.status,
    orderId,
    res.failureReason
  );
  if (!result) {
    let detail = `Error while updating bill payment transaction for orderId: ${orderId}!`;
    logger.error(
      `Bill Payment : ${detail} for payload : ${JSON.stringify(res)}`
    );
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.INVALID_ORDER_ID
    );
  }
  if (
    (res.status == BillPaymentTransactionStatusEnum.SUCCESS ||
      res.status == BillPaymentTransactionStatusEnum.FAILURE) &&
    updateBillDetails
  ) {
    await billDetailService.updateOrderDetails(result.referenceId, res);
    await updateCoreBanking(
      result.referenceId,
      orderId,
      res.status == BillPaymentTransactionStatusEnum.SUCCESS,
      res.refNumber,
      res.voucherCode
    );
  }

  return result;
};

const retryUpdate = async (orderId: string): Promise<IBillPaymentStatus> =>
  await Util.retry(RetryConfig(), getStatusFromGoBills, orderId);

const updateBillPaymentStatus = async (
  orderId: string
): Promise<IBillPaymentTransactionModel | null> =>
  retryUpdate(orderId)
    .then(async res => {
      logger.info(
        `Bill Payment : Updating bill payment status for orderId : ${orderId}`
      );
      const paymentConfig = await paymentStatusPollingService.updatePollingConfig(
        orderId,
        res.status
      );
      if (
        !paymentConfig &&
        res.status === BillPaymentTransactionStatusEnum.WAITING
      ) {
        logger.error(
          `Bill Payment : Updating bill payment status for orderId : ${orderId} to ${BillPaymentTransactionStatusEnum.MANUAL_INTERVENTION}`
        );
        res.status = BillPaymentTransactionStatusEnum.MANUAL_INTERVENTION;
      }
      return updateTransactionWithResult(res, orderId);
    })
    .catch(errorResponse => {
      logger.error(
        `Bill Payment : Retries exceeded for error in getting bill payment status : ${JSON.stringify(
          errorResponse
        )} for orderId : ${orderId}`
      );
      return updateTransactionDB(
        BillPaymentTransactionStatusEnum.WAITING,
        orderId,
        goBillsErrorTranslator(errorResponse)
      );
    });

const createBillPaymentTransaction = (
  input: IPaymentRequest,
  result: IPaymentResponse | null = null,
  error?: string
): Promise<IBillPaymentTransactionModel> =>
  billPaymentTxnService.createTransaction(
    getTransactionPayload(input, result, error)
  );

const submitPaymentRequest = (
  input: IPaymentRequest
): Promise<IPaymentResponse> =>
  repository.submitPayment(input.orderId, input.inquiryId);

const retrySubmit = async (input: IPaymentRequest): Promise<IPaymentResponse> =>
  await Util.retry(RetryConfig(), submitPaymentRequest, input);

const isPaymentPending = async (orderId: string) => {
  const billPaymentStatus = await getStatusFromGoBills(orderId);
  return billPaymentStatus.status === BillPaymentTransactionStatusEnum.WAITING;
};

const submitPayment = async (input: IPaymentRequest) =>
  retrySubmit(input)
    .then(result => {
      logger.info(
        `Bill Payment : Creating bill payment record for referenceId : ${input.inquiryId}`
      );
      createBillPaymentTransaction(input, result);
      debounceWhile(
        isPaymentPending,
        DEBOUNCE_MAX_ATTEMPTS,
        getInitialDelayForUpdate(),
        updateBillPaymentStatus,
        input.orderId
      );
    })
    .catch(async errorResponse => {
      logger.error(
        `Bill Payment : Retries exceeded for error submitting bill payment status : ${JSON.stringify(
          errorResponse
        )} with payload : ${JSON.stringify(input)}`
      );
      await createBillPaymentTransaction(
        input,
        null,
        goBillsErrorTranslator(errorResponse)
      );
      updateCoreBanking(input.inquiryId, input.orderId, false);
    });

const gobillsService = {
  submitPayment,
  updateBillPaymentStatus,
  updateTransactionWithResult,
  isPaymentPending
};

export default wrapLogs(gobillsService);
