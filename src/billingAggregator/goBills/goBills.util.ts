import { HttpError } from '@dk/module-httpclient';
import { getRetryCount } from '../../common/contextHandler';
import { config } from '../../config';

export const DEBOUNCE_MAX_ATTEMPTS = 3;

export const getInitialDelayForUpdate = () =>
  config.get('goBills').firstUpdateTimeInSeconds;

export const RetryConfig = () => ({
  shouldRetry: (error: HttpError) => {
    return error.status == 408 || (error.status > 499 && error.status < 600);
  },
  numberOfTries:
    getRetryCount() || config.get('goBills').retryPolicy.maxRetries,
  delayInMS: config.get('goBills').retryPolicy.delayTimeInSeconds
});

export const goBillsErrorTranslator = (errorResponse: any) => {
  let error;
  if (errorResponse.name !== 'AppError' && errorResponse.error) {
    error = JSON.stringify(errorResponse.error.errors);
  }
  return error;
};

export const debounceWhile = async (
  predicate: (...any: any[]) => Promise<boolean>,
  maxAttempts: number,
  initialDelay: number,
  functionToExecute: (...any: any[]) => Promise<any>,
  ...args: any[]
): Promise<any> => {
  const delay = (seconds: number) =>
    new Promise(resolve => setTimeout(resolve, seconds * 1000));

  const backOff = (offset: number) =>
    Math.round((Math.pow(2, offset) + offset) * initialDelay);

  for (let i = 0; i < maxAttempts; i++) {
    const shouldContinue = await predicate(...args);
    if (!shouldContinue) {
      break;
    }
    await delay(backOff(i));
  }
  return await functionToExecute(...args);
};
