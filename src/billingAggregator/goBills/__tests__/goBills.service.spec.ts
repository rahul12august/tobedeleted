import goBillsRepository from '../goBills.repository';
import goBillsService from '../goBills.service';
import transactionService from '../../../billPayment/billPaymentTransaction/billPaymentTransaction.service';
import billPaymentTxnService from '../../../billPayment/billPaymentTransaction/billPaymentTransaction.service';
import {
  getGoBillsData,
  getGoBillsDataWithFailureReason,
  postPaymentResponse,
  submitPaymentInput
} from '../__mocks__/goBills.data';
import {
  getBillPaymentTransaction,
  getBillPaymentTransactionWithError,
  getMultipleStatusForTransaction
} from '../../../billPayment/billPaymentTransaction/__mocks__/billPaymentTransaction.data';
import switchingTransactionService from '../../../transaction/transactionConfirmation.service';
import paymentStatusPollingService from '../../../billPayment/paymentStatusPolling/paymentStatusPolling.service';
import { TransferAppError } from '../../../errors/AppError';
import { ConfirmTransactionStatus } from '../../../transaction/transaction.enum';
import { formatTransactionDate } from '../../../transaction/transaction.util';
import { BillPaymentTransactionStatusEnum } from '../../../billPayment/billPaymentTransaction/billPaymentTransaction.enum';
import billDetailService from '../../../billPayment/billDetail/billDetail.service';
import { IBillPaymentStatus } from '../goBills.type';
import { emptyString } from '../../../decisionEngine/decisionEngine.constant';
import _ from 'lodash';

jest.mock('lodash/delay');
jest.mock('../goBills.repository');
jest.mock('../../../transaction/transactionConfirmation.service');
jest.mock(
  '../../../billPayment/billPaymentTransaction/billPaymentTransaction.repository'
);
jest.mock(
  '../../../billPayment/billPaymentTransaction/billPaymentTransaction.service'
);
jest.mock(
  '../../../billPayment/paymentStatusPolling/paymentStatusPolling.service'
);
jest.mock('../../../billPayment/billDetail/billDetail.service');

describe('goBillsService', () => {
  describe('updatePayment', () => {
    beforeEach(() => {
      jest.resetAllMocks();
      expect.hasAssertions();
    });

    it('Should get goBills and update payment status to Success', async () => {
      //Given
      const confirmTransactionPayload = {
        accountNo: '28205596',
        additionalInformation1: '4906d7c0-f395-40cb-920d-a06ed186bfb4',
        additionalInformation2: '8081-7887-1847-4059-2081',
        externalId: 'REF123',
        responseCode: 200,
        transactionDate: formatTransactionDate(
          getMultipleStatusForTransaction().createdAt
        ),
        amount: 10000
      };
      const updatePollingResponse = {
        type: 'relative',
        delayTime: 3
      };
      const billDetail = getBillPaymentTransaction();
      const paymentResponse = getGoBillsData();
      (goBillsRepository.getBillPaymentStatus as jest.Mock).mockResolvedValue(
        paymentResponse
      );
      (transactionService.updateStatus as jest.Mock).mockResolvedValue(
        billDetail
      );
      (transactionService.getTransactionStatus as jest.Mock).mockResolvedValue(
        billDetail
      );
      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValue(
        null
      );
      (paymentStatusPollingService.updatePollingConfig as jest.Mock).mockResolvedValue(
        updatePollingResponse
      );
      //When
      await goBillsService.updateBillPaymentStatus('123');
      //Then
      expect(goBillsRepository.getBillPaymentStatus).toBeCalledTimes(1);
      expect(transactionService.updateStatus).toBeCalled();
      expect(transactionService.getTransactionStatus).toHaveBeenCalledWith(
        '123'
      );
      expect(
        switchingTransactionService.confirmTransaction
      ).toHaveBeenCalledWith(confirmTransactionPayload);
      expect(
        paymentStatusPollingService.updatePollingConfig
      ).toHaveBeenCalledWith('123', paymentResponse.status);
      expect(billDetailService.updateOrderDetails).toBeCalledWith(
        billDetail.referenceId,
        paymentResponse
      );
    });

    it('Should get goBills and update payment status to Failed', async () => {
      //Given
      const paymentResponse = getGoBillsDataWithFailureReason();
      paymentResponse.status = BillPaymentTransactionStatusEnum.FAILURE;
      const updatePollingResponse = {
        type: 'relative',
        delayTime: 3
      };
      const billDetail = getBillPaymentTransactionWithError();
      (goBillsRepository.getBillPaymentStatus as jest.Mock).mockResolvedValue(
        paymentResponse
      );
      (transactionService.updateStatus as jest.Mock).mockResolvedValue(
        billDetail
      );
      (paymentStatusPollingService.updatePollingConfig as jest.Mock).mockResolvedValue(
        updatePollingResponse
      );
      (billDetailService.updateOrderDetails as jest.Mock).mockResolvedValueOnce(
        null
      );
      //When
      await goBillsService.updateBillPaymentStatus('123');
      //Then
      expect(goBillsRepository.getBillPaymentStatus).toBeCalledTimes(1);
      expect(transactionService.updateStatus).toBeCalled();
      expect(
        paymentStatusPollingService.updatePollingConfig
      ).toHaveBeenCalledWith('123', paymentResponse.status);
      expect(transactionService.updateStatus).toHaveBeenCalledWith(
        {
          status: paymentResponse.status,
          reason: paymentResponse.failureReason,
          capturedAt: expect.anything()
        },
        '123'
      );
      expect(billDetailService.updateOrderDetails).toBeCalledWith(
        billDetail.referenceId,
        paymentResponse
      );
    });

    it('Should get goBills and update payment status to Manual Intervention', async () => {
      //Given
      const paymentResponse = getGoBillsData();
      paymentResponse.status = BillPaymentTransactionStatusEnum.WAITING;
      (goBillsRepository.getBillPaymentStatus as jest.Mock).mockResolvedValue(
        paymentResponse
      );
      (transactionService.updateStatus as jest.Mock).mockResolvedValue(
        getBillPaymentTransaction()
      );
      (paymentStatusPollingService.updatePollingConfig as jest.Mock).mockResolvedValue(
        null
      );
      //When
      await goBillsService.updateBillPaymentStatus('123');
      //Then
      expect(goBillsRepository.getBillPaymentStatus).toBeCalledTimes(1);
      expect(transactionService.updateStatus).toHaveBeenCalledWith(
        {
          status: 'MANUAL_INTERVENTION',
          capturedAt: expect.anything()
        },
        '123'
      );
      expect(
        paymentStatusPollingService.updatePollingConfig
      ).toHaveBeenCalledWith('123', 'PENDING');
    });

    it('should not retry when response throws error with status 404', async () => {
      //Given
      (goBillsRepository.getBillPaymentStatus as jest.Mock).mockRejectedValue({
        status: 400,
        error: {
          errors: [
            {
              code: 'INVALID_PAYLOAD_CREDENTIAL',
              message: 'invalid payload credential'
            }
          ]
        }
      });
      transactionService.updateStatus as jest.Mock;
      //When
      await goBillsService.updateBillPaymentStatus('08512232');
      //Then
      expect(goBillsRepository.getBillPaymentStatus).toBeCalledTimes(1);
      expect(transactionService.updateStatus).toBeCalled();
    });

    it('Should throw error when inquiryId not found', async () => {
      //Given
      const paymentResponse = getGoBillsData();
      const updatePollingResponse = {
        type: 'relative',
        delayTime: 3
      };
      (goBillsRepository.getBillPaymentStatus as jest.Mock).mockResolvedValue(
        paymentResponse
      );
      (paymentStatusPollingService.updatePollingConfig as jest.Mock).mockResolvedValue(
        updatePollingResponse
      );
      (transactionService.updateStatus as jest.Mock).mockResolvedValue(null);
      //When
      await goBillsService.updateBillPaymentStatus('123');
      //Then
      expect(goBillsRepository.getBillPaymentStatus).toBeCalledTimes(1);
      expect(transactionService.updateStatus).toBeCalled();
      expect(TransferAppError).toBeDefined();
    });

    it('Should get empty pollingConfig and update payment status to SUCCESS if we get SUCCESS from gobills', async () => {
      //Given
      const paymentResponse = getGoBillsData();
      (goBillsRepository.getBillPaymentStatus as jest.Mock).mockResolvedValue(
        paymentResponse
      );
      (transactionService.updateStatus as jest.Mock).mockResolvedValue(
        getBillPaymentTransaction()
      );
      (paymentStatusPollingService.updatePollingConfig as jest.Mock).mockResolvedValue(
        null
      );
      //When
      await goBillsService.updateBillPaymentStatus('123');
      //Then
      expect(goBillsRepository.getBillPaymentStatus).toBeCalledTimes(1);
      expect(transactionService.updateStatus).toHaveBeenCalledWith(
        {
          status: 'SUCCESS',
          capturedAt: expect.anything()
        },
        '123'
      );
      expect(
        paymentStatusPollingService.updatePollingConfig
      ).toHaveBeenCalledWith('123', 'SUCCESS');
    });
  });

  describe('submitPayment', () => {
    beforeEach(() => {
      jest.resetAllMocks();
      expect.hasAssertions();
      jest.spyOn(_, 'delay');
    });

    it('Should post to goBills and update status', async () => {
      //Given
      const paymentResponse = postPaymentResponse();
      (goBillsRepository.submitPayment as jest.Mock).mockResolvedValue(
        paymentResponse
      );
      (transactionService.createTransaction as jest.Mock).mockResolvedValue(
        getBillPaymentTransaction()
      );
      (goBillsRepository.getBillPaymentStatus as jest.Mock).mockResolvedValue(
        getGoBillsData()
      );

      //When
      await goBillsService.submitPayment(submitPaymentInput);

      //Then
      expect(goBillsRepository.submitPayment).toHaveBeenCalled();
      expect(transactionService.createTransaction).toHaveBeenCalled();
      expect(goBillsRepository.getBillPaymentStatus).toHaveBeenCalledTimes(1);
    });

    it('should not retry when response throw with status code 404', async () => {
      //Given

      const paymentTransaction = getBillPaymentTransaction();
      const createdDate = new Date();
      paymentTransaction.createdAt = createdDate;

      const confirmTransactionPayload = {
        accountNo: '28205596',
        additionalInformation1: undefined,
        additionalInformation2: undefined,
        externalId: 'INQUIRY123',
        responseCode: ConfirmTransactionStatus.ERROR_UNEXPECTED,
        transactionDate: formatTransactionDate(createdDate),
        amount: 10000
      };
      (goBillsRepository.submitPayment as jest.Mock).mockRejectedValue({
        status: 400,
        error: {
          errors: [
            {
              code: 'INVALID_PAYLOAD_CREDENTIAL',
              message: 'invalid payload credential'
            }
          ]
        }
      });
      (transactionService.createTransaction as jest.Mock).mockResolvedValue(
        getBillPaymentTransactionWithError()
      );

      (transactionService.getTransactionStatus as jest.Mock).mockResolvedValue(
        paymentTransaction
      );
      (switchingTransactionService.confirmTransaction as jest.Mock).mockResolvedValue(
        null
      );
      //When
      await goBillsService.submitPayment(submitPaymentInput);
      //Then
      expect(goBillsRepository.submitPayment).toBeCalledTimes(1);
      expect(transactionService.createTransaction).toHaveBeenCalled();
      expect(transactionService.getTransactionStatus).toHaveBeenCalledWith(
        'ORDER123'
      );
      expect(
        switchingTransactionService.confirmTransaction
      ).toHaveBeenCalledWith(confirmTransactionPayload);
    });
  });

  describe('updateTransactionWithResult', () => {
    it('should not call billing detail service', async () => {
      const billDetail = getBillPaymentTransaction();
      const status: IBillPaymentStatus = {
        orderId: billDetail.orderId,
        status: BillPaymentTransactionStatusEnum.SUCCESS,
        voucherCode: emptyString,
        amount: '1',
        trxDate: emptyString,
        skuTag: emptyString,
        refNumber: billDetail.referenceId
      };

      (transactionService.updateStatus as jest.Mock).mockResolvedValueOnce(
        billDetail
      );

      await goBillsService.updateTransactionWithResult(
        status,
        billDetail.orderId,
        false
      );

      expect(transactionService.updateStatus).toHaveBeenCalledWith(
        expect.objectContaining({
          status: BillPaymentTransactionStatusEnum.SUCCESS
        }),
        billDetail.orderId
      );
      expect(billDetailService.updateOrderDetails).not.toHaveBeenCalled();
    });
  });
});
