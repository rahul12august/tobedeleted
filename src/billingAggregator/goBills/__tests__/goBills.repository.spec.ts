import goBillsRepository from '../goBills.repository';
import {
  getGoBillsData,
  postPaymentResponse,
  submitPaymentInput
} from '../__mocks__/goBills.data';
import httpClient from '../goBillsHttpClient';
import { TransferAppError } from '../../../errors/AppError';
import { ERROR_CODE } from '../../../common/errors';
import { UrlConstants } from '../goBills.constant';
import { TransferFailureReasonActor } from '../../../transaction/transaction.enum';

jest.mock('../goBillsHttpClient');

describe('Go bills repository', () => {
  afterEach(() => {
    jest.resetAllMocks();
    expect.hasAssertions();
  });

  describe('submitPayment', () => {
    it('should call goBills return data sucessfully', async () => {
      // Given
      const paymentResponse = postPaymentResponse();
      (httpClient.post as jest.Mock).mockImplementationOnce(() =>
        Promise.resolve({
          data: {
            data: paymentResponse
          }
        })
      );

      // When
      const result = await goBillsRepository.submitPayment(
        submitPaymentInput.orderId,
        submitPaymentInput.inquiryId
      );

      // Then
      expect(paymentResponse).toMatchObject(result);
      expect(httpClient.post).toHaveBeenCalledWith(UrlConstants.POST_PAYMENT, {
        orderId: submitPaymentInput.orderId,
        inquiryId: submitPaymentInput.inquiryId
      });
    });

    it('should throw error from goBills', async () => {
      // given
      (httpClient.post as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.UNEXPECTED_ERROR
        )
      );

      // when
      const error = await goBillsRepository
        .submitPayment(submitPaymentInput.orderId, submitPaymentInput.inquiryId)
        .catch(error => error);

      // then
      expect(error).toBeDefined();
      expect(error.detail).toEqual('Test error!');
      expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      expect(error.errorCode).toEqual(ERROR_CODE.UNEXPECTED_ERROR);
      expect(httpClient.post).toHaveBeenCalledWith(UrlConstants.POST_PAYMENT, {
        orderId: submitPaymentInput.orderId,
        inquiryId: submitPaymentInput.inquiryId
      });
    });
  });

  describe('getBillPaymentStatus', () => {
    it('should return response when orderId correct', async () => {
      //Given
      const result = getGoBillsData();
      (httpClient.get as jest.Mock).mockImplementationOnce(() =>
        Promise.resolve({
          data: {
            data: result
          }
        })
      );
      //When
      const res = await goBillsRepository.getBillPaymentStatus('123');
      //Then
      expect(res).toEqual(result);
    });

    it('should throw error from goBills', async () => {
      // given
      (httpClient.get as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.UNEXPECTED_ERROR
        )
      );

      // when
      const error = await goBillsRepository
        .getBillPaymentStatus('123')
        .catch(e => e);

      // then
      expect(error.detail).toEqual('Test error!');
      expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      expect(error.errorCode).toEqual(ERROR_CODE.UNEXPECTED_ERROR);
    });
  });
});
