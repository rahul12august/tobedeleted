import { ITransactionModel } from '../transaction/transaction.model';
import { ITransactionSubmissionTemplate } from '../transaction/transaction.type';
import { wrapLogs } from '../logger';
import billingAggregatorService from './billingAggregator.service';
import { IPaymentRequest } from './goBills/goBills.type';
import { BankChannelEnum } from '@dk/module-common';
import transactionRepository from '../transaction/transaction.repository';
import { Rail } from '@dk/module-common';

const submitTransaction = async (
  transactionModel: ITransactionModel
): Promise<ITransactionModel> => {
  const payload: IPaymentRequest = {
    orderId: transactionModel.id,
    inquiryId: transactionModel.externalId,
    accountNo: transactionModel.beneficiaryAccountNo,
    transactionId: transactionModel.id,
    createdAt: transactionModel.createdAt || new Date(),
    amount: transactionModel.transactionAmount
  };
  await transactionRepository.update(transactionModel.id, {
    thirdPartyOutgoingId: transactionModel.externalId
  });
  await billingAggregatorService.submitPayment(payload);
  return transactionModel;
};

const isEligible = (model: ITransactionModel): boolean =>
  BankChannelEnum.GOBILLS === model.beneficiaryBankCodeChannel;

const shouldSendNotification = () => true;

const getRail = () => Rail.GOBILLS;

const submitPaymentTemplate: ITransactionSubmissionTemplate = {
  submitTransaction,
  isEligible,
  getRail,
  shouldSendNotification
};

export default wrapLogs(submitPaymentTemplate);
