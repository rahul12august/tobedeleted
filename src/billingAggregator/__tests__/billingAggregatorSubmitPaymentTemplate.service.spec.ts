import { BankChannelEnum } from '@dk/module-common/dist/enum/bankChannel.enum';
import submitPaymentTemplate from '../billingAggregatorSubmitPaymentTemplate.service';
import { getTransactionDetail } from '../../transaction/__mocks__/transaction.data';
import billingAggregatorService from '../billingAggregator.service';
import transactionRepository from '../../transaction/transaction.repository';
import { Rail } from '@dk/module-common';

jest.mock('../billingAggregator.service');
jest.mock('../../transaction/transaction.repository');

describe('submitPaymentTemplate', () => {
  // given
  const transactionModel = {
    ...getTransactionDetail(),
    beneficiaryBankCodeChannel: BankChannelEnum.GOBILLS,
    requireThirdPartyOutgoingId: true,
    debitTransactionCode: 'TFD23',
    thirdPartyOutgoingId: '20085T000001'
  };

  describe('isEligible check', () => {
    it(`should return true when beneficiaryBankCodeChannel is ${BankChannelEnum.GOBILLS}`, () => {
      // when
      const eligible = submitPaymentTemplate.isEligible(transactionModel);

      // then
      expect(eligible).toBeTruthy();
    });

    it(`should return false when beneficiaryBankCodeChannel is not ${BankChannelEnum.GOBILLS}`, () => {
      // when
      const eligible = submitPaymentTemplate.isEligible({
        ...transactionModel,
        beneficiaryBankCodeChannel: BankChannelEnum.INTERNAL
      });

      // then
      expect(eligible).not.toBeTruthy();
    });
  });

  describe('submitTransaction', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should submit transaction to switching with correct payload', async () => {
      // Given
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({});
      // When
      const result = await submitPaymentTemplate.submitTransaction(
        transactionModel
      );
      const input = {
        orderId: transactionModel.id,
        inquiryId: transactionModel.externalId,
        accountNo: transactionModel.beneficiaryAccountNo,
        transactionId: transactionModel.id,
        createdAt: new Date('01 June 2020 00:00:00'),
        amount: transactionModel.transactionAmount
      };
      // Then
      expect(result).toBeDefined();
      expect(transactionRepository.update).toHaveBeenCalledWith(
        transactionModel.id,
        {
          thirdPartyOutgoingId: transactionModel.externalId
        }
      );
      expect(billingAggregatorService.submitPayment).toHaveBeenCalledWith(
        input
      );
    });

    it('should submit transaction to switching without createdAt', async () => {
      // Given
      (transactionRepository.update as jest.Mock).mockResolvedValueOnce({});
      // When
      const result = await submitPaymentTemplate.submitTransaction({
        ...transactionModel,
        createdAt: undefined
      });
      const input = {
        orderId: transactionModel.id,
        inquiryId: transactionModel.externalId,
        accountNo: transactionModel.beneficiaryAccountNo,
        transactionId: transactionModel.id,
        amount: transactionModel.transactionAmount
      };
      // Then
      expect(result).toBeDefined();
      expect(transactionRepository.update).toHaveBeenCalledWith(
        transactionModel.id,
        {
          thirdPartyOutgoingId: transactionModel.externalId
        }
      );
      expect(billingAggregatorService.submitPayment).toHaveBeenCalledWith(
        expect.objectContaining(input)
      );
    });
  });

  describe('Rail check', () => {
    it(`should have rail property defined`, () => {
      // when
      const rail = submitPaymentTemplate.getRail?.();

      // then
      expect(rail).toEqual(Rail.GOBILLS);
    });
  });
});
