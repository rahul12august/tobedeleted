import billingAggregatorService from '../billingAggregator.service';
import goBillsService from '../goBills/goBills.service';

jest.mock('../goBills/goBills.service');
jest.mock(
  '../../billPayment/billPaymentTransaction/billPaymentTransaction.service'
);

describe('billing aggregator service', () => {
  afterEach(() => {
    expect.hasAssertions();
    jest.resetAllMocks();
  });

  describe('updateBillPaymentStatus', () => {
    it('should return valid response when input is correct', async () => {
      //Given
      goBillsService.updateBillPaymentStatus as jest.Mock;

      //When
      await billingAggregatorService.updateBillPaymentStatus('123');

      //Then
      expect(goBillsService.updateBillPaymentStatus).toBeCalled();
    });
  });

  describe('submitPayment', () => {
    it('should return valid response when input is correct', async () => {
      //Given
      goBillsService.submitPayment as jest.Mock;

      //When
      await billingAggregatorService.submitPayment({
        orderId: '123',
        inquiryId: 'inquiryId',
        accountNo: 'accountNo',
        transactionId: 'transactionId'
      });

      //Then
      expect(goBillsService.submitPayment).toBeCalled();
    });
  });
});
