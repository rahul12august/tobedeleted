import { IPaymentRequest } from './goBills/goBills.type';
import goBillsService from './goBills/goBills.service';
import { IBillPaymentTransactionModel } from '../billPayment/billPaymentTransaction/billPaymentTransaction.type';
import { wrapLogs } from '../logger';

const updateBillPaymentStatus = async (
  orderId: string
): Promise<IBillPaymentTransactionModel | null> =>
  await goBillsService.updateBillPaymentStatus(orderId);

const submitPayment = async (input: IPaymentRequest) =>
  await goBillsService.submitPayment(input);

const billingAggregatorService = { updateBillPaymentStatus, submitPayment };

export default wrapLogs(billingAggregatorService);
