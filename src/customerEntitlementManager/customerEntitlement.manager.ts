import _ from 'lodash';
import { AwardHandler } from '../award/award.handler';
import { UsageCounter } from '../award/award.type';
import { EntitlementHandler } from '../entitlement/entitlement.handler';
import {
  ICounterUsageByTransCodeRequest,
  ICustomerEntitlementByTransCodeRequest,
  ICustomerEntitlementByTransCodeResponse
} from '../entitlement/entitlement.interface';
import { BasicFeeMapping, FeeRuleInfo } from '../fee/fee.type';

import { CustomerEntitlementHandler } from './customerEntitlement.interface';

export abstract class CustomerEntitlementManager {
  public isEntitlementEnabled: boolean = false;
  public customerEntitlementHandlerInstance: CustomerEntitlementHandler | null = null;
  public customerEntitlementHandlerFallbackInstance: CustomerEntitlementHandler | null = null;

  public abstract getHandlerInstanceProcessFeeRuleInfo(
    currentFeeRuleInfo: FeeRuleInfo,
    feeRuleType: string | undefined,
    awardGroupCounter: string | undefined,
    usageCounters: UsageCounter[] | undefined
  ): CustomerEntitlementHandler | null;

  public abstract getHandlerInstanceDetermineBasicFeeMapping(
    currentFeeRuleInfo: FeeRuleInfo
  ): CustomerEntitlementHandler | null;

  public async preProcessCustomerEntitlementBasedOnTransactionCodes(
    params: ICustomerEntitlementByTransCodeRequest
  ): Promise<ICustomerEntitlementByTransCodeResponse | null> {
    if (this.customerEntitlementHandlerInstance !== null) {
      return this.customerEntitlementHandlerInstance.preProcessCustomerEntitlementBasedOnTransactionCodes(
        params
      );
    }

    return null;
  }

  public mapEntitlementUsageCounterByLimitGroupCode(
    usageCounters: UsageCounter[],
    entitlementResultResponse: ICustomerEntitlementByTransCodeResponse | null
  ): UsageCounter[] {
    if (this.customerEntitlementHandlerInstance !== null) {
      return this.customerEntitlementHandlerInstance.mapEntitlementUsageCounterByLimitGroupCode(
        usageCounters,
        entitlementResultResponse
      );
    }

    //return as is
    return usageCounters;
  }

  public async determineBasicFeeMapping(
    feeRuleObject: FeeRuleInfo
  ): Promise<BasicFeeMapping[] | undefined> {
    if (this.customerEntitlementHandlerInstance !== null) {
      const handler = this.getHandlerInstanceDetermineBasicFeeMapping(
        feeRuleObject
      );

      if (handler !== null) {
        return await handler.determineBasicFeeMapping(feeRuleObject);
      }
    }

    return Promise.resolve(undefined);
  }

  public processFeeRuleInfo(
    currentFeeRuleInfo: FeeRuleInfo,
    feeRuleType: string | undefined,
    awardGroupCounter: string | undefined,
    usageCounters: UsageCounter[] | undefined
  ): FeeRuleInfo {
    if (this.customerEntitlementHandlerInstance !== null) {
      const handler = this.getHandlerInstanceProcessFeeRuleInfo(
        currentFeeRuleInfo,
        feeRuleType,
        awardGroupCounter,
        usageCounters
      );

      if (handler !== null) {
        return handler.processFeeRuleInfo(
          currentFeeRuleInfo,
          feeRuleType,
          awardGroupCounter,
          usageCounters
        );
      }
    }

    //return as is
    return currentFeeRuleInfo;
  }

  public generateMissingUsageCounterBasedOnTransactionCodes(
    params: ICounterUsageByTransCodeRequest
  ) {
    if (this.customerEntitlementHandlerInstance !== null) {
      return this.customerEntitlementHandlerInstance.generateMissingUsageCounterBasedOnTransactionCodes(
        params
      );
    }

    //return as is
    return params.usageCounters;
  }

  public getCurrentInstanceType(): string {
    return this.constructor.name;
  }

  public getCurrentInstanceHandlerTypeDesc(): string {
    if (this.customerEntitlementHandlerInstance != null) {
      if (
        this.customerEntitlementHandlerInstance instanceof EntitlementHandler
      ) {
        return 'CustomerEntitlementManagerFactory: using EntitlementManager - EntitlementHandler';
      } else if (
        this.customerEntitlementHandlerInstance instanceof AwardHandler
      ) {
        return 'CustomerEntitlementManagerFactory: using AwardManager - AwardHandler';
      }
      return 'CustomerEntitlementManagerFactory: no instance type detected';
    }
    return 'CustomerEntitlementManagerFactory: handler is null';
  }
}
