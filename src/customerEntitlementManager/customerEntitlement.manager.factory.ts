import entitlementFlag from '../common/entitlementFlag';

import { CustomerEntitlementManager } from './customerEntitlement.manager';
import {
  AwardManager,
  EntitlementManager
} from './customerEntitlement.manager.impl';

export class CustomerEntitlementManagerFactory {
  private static customerEntitlementManager: CustomerEntitlementManager | null = null;

  static async getCustomerEntitlementManagerByFlag(): Promise<
    CustomerEntitlementManager
  > {
    const isEntitlementEnabled = await entitlementFlag.isEnabled();

    if (this.customerEntitlementManager == null) {
      if (isEntitlementEnabled) {
        this.customerEntitlementManager = new EntitlementManager(
          isEntitlementEnabled
        );
      } else {
        this.customerEntitlementManager = new AwardManager(
          isEntitlementEnabled
        );
      }
    }

    return this.customerEntitlementManager;
  }

  //for jest to reset singleton instance
  static resetCustomerEntitlementManager() {
    this.customerEntitlementManager = null;
  }
}
