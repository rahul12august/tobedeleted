import { AwardHandler } from '../award/award.handler';
import { UsageCounter } from '../award/award.type';
import { EntitlementHandler } from '../entitlement/entitlement.handler';
import { FeeRuleInfo } from '../fee/fee.type';
import logger from '../logger';
import { CustomerEntitlementHandler } from './customerEntitlement.interface';
import { CustomerEntitlementManager } from './customerEntitlement.manager';
import { isUndefined } from 'lodash';

export class EntitlementManager extends CustomerEntitlementManager {
  constructor(
    isEntitlementEnabled: boolean,
    customerEntitlementHandlerInstance: CustomerEntitlementHandler | null = new EntitlementHandler(),
    customerEntitlementHandlerFallbackInstance: CustomerEntitlementHandler | null = new AwardHandler()
  ) {
    super();
    super.isEntitlementEnabled = isEntitlementEnabled;
    super.customerEntitlementHandlerInstance = customerEntitlementHandlerInstance;

    //this is the default
    super.customerEntitlementHandlerFallbackInstance = customerEntitlementHandlerFallbackInstance;
  }

  public getHandlerInstanceProcessFeeRuleInfo(
    _: FeeRuleInfo,
    feeRuleType: string | undefined,
    awardGroupCounter: string | undefined,
    usageCounters: UsageCounter[] | undefined
  ): CustomerEntitlementHandler | null {
    const isWithEntitlement =
      !isUndefined(awardGroupCounter) &&
      !isUndefined(feeRuleType) &&
      !isUndefined(usageCounters);

    logger.info(
      `[EntitlementManager] handlerInstanceValidationProcessFeeRuleInfo isWithEntitlement: [${isWithEntitlement}]`
    );

    if (isWithEntitlement) {
      return this.customerEntitlementHandlerInstance;
    } else {
      return this.customerEntitlementHandlerFallbackInstance;
    }
  }

  public getHandlerInstanceDetermineBasicFeeMapping(
    currentFeeRuleInfo: FeeRuleInfo
  ): CustomerEntitlementHandler | null {
    const {
      quota,
      isQuotaExceeded,
      monthlyNoTransaction: usedQuota
    } = currentFeeRuleInfo;

    const isWithEntitlement =
      !isUndefined(usedQuota) &&
      !isUndefined(quota) &&
      !isUndefined(isQuotaExceeded);

    logger.info(
      `[EntitlementManager]: handlerInstanceValidationDetermineBasicFeeMapping isWithEntitlement: [${isWithEntitlement}]`
    );

    if (isWithEntitlement) {
      return this.customerEntitlementHandlerInstance;
    } else {
      return this.customerEntitlementHandlerFallbackInstance;
    }
  }
}

export class AwardManager extends CustomerEntitlementManager {
  public toUseHandlerFallbackInstance(): boolean {
    throw new Error('Method not implemented.');
  }
  constructor(
    isEntitlementEnabled: boolean,
    customerEntitlementHandlerInstance:
      | CustomerEntitlementHandler
      | undefined = new AwardHandler()
  ) {
    super();
    super.isEntitlementEnabled = isEntitlementEnabled;
    super.customerEntitlementHandlerInstance = customerEntitlementHandlerInstance;
    super.customerEntitlementHandlerFallbackInstance = null;
  }

  public getHandlerInstanceProcessFeeRuleInfo(): CustomerEntitlementHandler | null {
    return this.customerEntitlementHandlerInstance;
  }
  public getHandlerInstanceDetermineBasicFeeMapping(): CustomerEntitlementHandler | null {
    return this.customerEntitlementHandlerInstance;
  }
}
