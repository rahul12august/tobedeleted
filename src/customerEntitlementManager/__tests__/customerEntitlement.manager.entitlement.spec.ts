import { BankNetworkEnum } from '@dk/module-common';
import entitlementFlag from '../../common/entitlementFlag';
import {
  AwardLimitGroupCodes,
  EntitlementCode
} from '../../entitlement/entitlement.enum';
import { EntitlementHandler } from '../../entitlement/entitlement.handler';
import {
  ICustomerEntitlementByTransCodeRequest,
  IUpdateUsageCounterEntitlementResponse
} from '../../entitlement/entitlement.interface';
import { EntitlementManager } from '../customerEntitlement.manager.impl';
import uuidv4 from 'uuid/v4';
import { UsageCounter } from '../../award/award.type';
import { FeeRuleType, COUNTER } from '../../fee/fee.constant';
import { FeeRuleInfo } from '../../fee/fee.type';
import { CustomerEntitlementManagerFactory } from '../customerEntitlement.manager.factory';
import { ITransactionCodeMapping } from '../../configuration/configuration.type';
import { AwardHandler } from '../../award/award.handler';

describe('customerEntitlementManager ENTITLEMENT', () => {
  entitlementFlag.isEnabled = jest.fn().mockResolvedValue(true);

  describe('preProcessCustomerEntitlementBasedOnTransactionCodes', () => {
    const params: ICustomerEntitlementByTransCodeRequest = {
      transactionCodes: [
        {
          interchange: BankNetworkEnum.ALTO,
          transactionCode: 'TFD30',
          transactionCodeInfo: {
            maxAmountPerTx: 50000000,
            minAmountPerTx: 10000,
            limitGroupCode: 'L002',
            awardGroupCounter: AwardLimitGroupCodes.BONUS_TRANSFER,
            entitlementCode: EntitlementCode.BONUS_TRANSFER,
            channel: 'TFD30',
            code: 'TFD30'
          }
        },
        {
          interchange: BankNetworkEnum.ARTAJASA,
          transactionCode: 'TFD40',
          transactionCodeInfo: {
            maxAmountPerTx: 50000000,
            minAmountPerTx: 10000,
            limitGroupCode: 'L002',
            awardGroupCounter: AwardLimitGroupCodes.BONUS_TRANSFER,
            entitlementCode: EntitlementCode.BONUS_TRANSFER,
            channel: 'TFD40',
            code: 'TFD40'
          }
        }
      ],
      customerId: 'customer1',
      transactionId: undefined
    };

    it('when entitlementHandler is null return null', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const customerEntitlementManager = new EntitlementManager(
        isEntitlementEnabled,
        null,
        null
      );

      const result = await customerEntitlementManager.preProcessCustomerEntitlementBasedOnTransactionCodes(
        params
      );

      expect(result).toBeNull();
    });

    it('when entitlementHandler exists, should expect that the call to the preProcessCustomerEntitlementBasedOnTransactionCodes made 1 time with the correct parameters ', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const entitlementHandler = new EntitlementHandler();
      const awardHandler = new AwardHandler();

      const customerEntitlementManager = new EntitlementManager(
        isEntitlementEnabled,
        entitlementHandler,
        awardHandler
      );

      const preProcessFn = jest
        .spyOn(
          customerEntitlementManager,
          'preProcessCustomerEntitlementBasedOnTransactionCodes'
        )
        .mockResolvedValue(null);

      await customerEntitlementManager.preProcessCustomerEntitlementBasedOnTransactionCodes(
        params
      );

      expect(preProcessFn).toBeCalledTimes(1);
      expect(preProcessFn).toBeCalledWith(params);
    });
  });

  describe('mapEntitlementUsageCounterByLimitGroupCode', () => {
    const usageCounters: UsageCounter[] = [
      {
        limitGroupCode: 'L002',
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      },
      {
        limitGroupCode: AwardLimitGroupCodes.BONUS_TRANSFER,
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      }
    ];

    const updatedEntitlementMapResponse: Map<
      string,
      IUpdateUsageCounterEntitlementResponse
    > = new Map();

    const entitlementCodeRefId = uuidv4();

    updatedEntitlementMapResponse.set('L002', {
      entitlements: [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 25,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 1,
          value: 'value'
        }
      ],
      counters: [
        {
          id: entitlementCodeRefId,
          recorded: true
        }
      ]
    });

    updatedEntitlementMapResponse.set(AwardLimitGroupCodes.BONUS_TRANSFER, {
      entitlements: [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 25,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 1,
          value: 'value'
        }
      ],
      counters: [
        {
          id: entitlementCodeRefId,
          recorded: true
        }
      ]
    });

    it('when entitlementHandler is null return usage counters as is', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const customerEntitlementManager = new EntitlementManager(
        isEntitlementEnabled,
        null
      );

      const result = customerEntitlementManager.mapEntitlementUsageCounterByLimitGroupCode(
        usageCounters,
        {
          updatedEntitlementMapResponse: updatedEntitlementMapResponse
        }
      );

      expect(result).toBe(usageCounters);
    });

    it('when entitlementHandler exists, should expect that the call to the mapEntitlementUsageCounterByLimitGroupCode made 1 time with the correct parameters ', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const entitlementHandler = new EntitlementHandler();

      const customerEntitlementManager = new EntitlementManager(
        isEntitlementEnabled,
        entitlementHandler
      );

      const mapFn = jest.spyOn(
        customerEntitlementManager,
        'mapEntitlementUsageCounterByLimitGroupCode'
      );

      await customerEntitlementManager.mapEntitlementUsageCounterByLimitGroupCode(
        usageCounters,
        {
          updatedEntitlementMapResponse: updatedEntitlementMapResponse
        }
      );

      expect(mapFn).toBeCalledTimes(1);
      expect(mapFn).toBeCalledWith(usageCounters, {
        updatedEntitlementMapResponse: updatedEntitlementMapResponse
      });
    });
  });

  describe('determineBasicFeeMapping', () => {
    const feeRuleInfoTemplate: FeeRuleInfo = {
      code: FeeRuleType.RTOL_TRANSFER_FEE_RULE,
      counterCode: COUNTER.COUNTER_01,
      basicFeeCode1: [
        {
          interchange: BankNetworkEnum.ARTAJASA,
          basicFeeCode: 'TF015'
        },
        {
          interchange: BankNetworkEnum.ALTO,
          basicFeeCode: 'TF008'
        }
      ],
      basicFeeCode2: [
        {
          interchange: BankNetworkEnum.ARTAJASA,
          basicFeeCode: 'TF010'
        },
        {
          interchange: BankNetworkEnum.ALTO,
          basicFeeCode: 'TF009'
        }
      ],
      monthlyNoTransaction: 0,
      quota: 25,
      interchange: BankNetworkEnum.ALTO,
      isQuotaExceeded: false
    };

    it('when entitlementHandler is null return null', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const customerEntitlementManager = new EntitlementManager(
        isEntitlementEnabled,
        null
      );

      const result = await customerEntitlementManager.determineBasicFeeMapping(
        feeRuleInfoTemplate
      );

      expect(result).toBeUndefined();
    });

    it('when entitlementHandler exists, should expect that the call to the determineBasicFeeMapping made 1 time with the correct parameters ', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const entitlementHandler = new EntitlementHandler();

      const customerEntitlementManager = new EntitlementManager(
        isEntitlementEnabled,
        entitlementHandler
      );

      const getHandlerInstanceDetermineBasicFeeMappingFn = jest.spyOn(
        customerEntitlementManager,
        'getHandlerInstanceDetermineBasicFeeMapping'
      );

      const basicFeeMappingFn = jest.spyOn(
        customerEntitlementManager,
        'determineBasicFeeMapping'
      );

      await customerEntitlementManager.determineBasicFeeMapping(
        feeRuleInfoTemplate
      );

      expect(basicFeeMappingFn).toBeCalledTimes(1);
      expect(basicFeeMappingFn).toBeCalledWith(feeRuleInfoTemplate);
      expect(getHandlerInstanceDetermineBasicFeeMappingFn).toBeCalledTimes(1);
    });
  });

  describe('processFeeRuleInfo', () => {
    const feeRuleInfoTemplate: FeeRuleInfo = {
      code: FeeRuleType.RTOL_TRANSFER_FEE_RULE,
      counterCode: COUNTER.COUNTER_01,
      basicFeeCode1: [
        {
          interchange: BankNetworkEnum.ARTAJASA,
          basicFeeCode: 'TF015'
        },
        {
          interchange: BankNetworkEnum.ALTO,
          basicFeeCode: 'TF008'
        }
      ],
      basicFeeCode2: [
        {
          interchange: BankNetworkEnum.ARTAJASA,
          basicFeeCode: 'TF010'
        },
        {
          interchange: BankNetworkEnum.ALTO,
          basicFeeCode: 'TF009'
        }
      ],
      monthlyNoTransaction: 0,
      quota: 25,
      interchange: BankNetworkEnum.ALTO,
      isQuotaExceeded: false
    };

    const awardGroupCounter = 'Bonus_Transfer';
    const usageCounters: UsageCounter[] = [
      {
        limitGroupCode: 'L002',
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      },
      {
        limitGroupCode: AwardLimitGroupCodes.BONUS_TRANSFER,
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      }
    ];

    it('when entitlementHandler is null return as is', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const customerEntitlementManager = new EntitlementManager(
        isEntitlementEnabled,
        null,
        null
      );

      const result = customerEntitlementManager.processFeeRuleInfo(
        feeRuleInfoTemplate,
        FeeRuleType.BIFAST_FEE_RULE,
        awardGroupCounter,
        usageCounters
      );

      expect(result).toBe(feeRuleInfoTemplate);
    });

    it('when entitlementHandler exists, should expect that the call to the processFeeRuleInfo made 1 time with the correct parameters ', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const entitlementHandler = new EntitlementHandler();

      const customerEntitlementManager = new EntitlementManager(
        isEntitlementEnabled,
        entitlementHandler
      );

      const basicFeeMappingFn = jest.spyOn(
        customerEntitlementManager,
        'processFeeRuleInfo'
      );

      const getHandlerInstanceProcessFeeRuleInfo = jest.spyOn(
        customerEntitlementManager,
        'getHandlerInstanceProcessFeeRuleInfo'
      );

      await customerEntitlementManager.processFeeRuleInfo(
        feeRuleInfoTemplate,
        FeeRuleType.BIFAST_FEE_RULE,
        awardGroupCounter,
        usageCounters
      );

      expect(basicFeeMappingFn).toBeCalledTimes(1);
      expect(basicFeeMappingFn).toBeCalledWith(
        feeRuleInfoTemplate,
        FeeRuleType.BIFAST_FEE_RULE,
        awardGroupCounter,
        usageCounters
      );
      expect(getHandlerInstanceProcessFeeRuleInfo).toBeCalledTimes(1);
    });
  });

  describe('getCurrentInstanceType', () => {
    const isEntitlementFlagEnabledFn = (entitlementFlag.isEnabled = jest.fn());

    beforeEach(() => {
      isEntitlementFlagEnabledFn.mockClear();
      CustomerEntitlementManagerFactory.resetCustomerEntitlementManager();
      jest.clearAllMocks();
    });

    it(`when entitlementHandler is null return description that handler is null`, async () => {
      isEntitlementFlagEnabledFn.mockResolvedValue(true);

      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const customerEntitlementManager = new EntitlementManager(
        isEntitlementEnabled,
        null
      );

      const result = customerEntitlementManager.getCurrentInstanceHandlerTypeDesc();

      expect(result).toBe('CustomerEntitlementManagerFactory: handler is null');
    });

    it(`when entitlementHandler exists [entitlementFlag = true], should expect that the call to the getCurrentInstanceType made 1 time `, async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const entitlementHandler = new EntitlementHandler();

      const customerEntitlementManager = new EntitlementManager(
        isEntitlementEnabled,
        entitlementHandler
      );

      const getCurrentInstanceHandlerTypeDesc = jest.spyOn(
        customerEntitlementManager,
        'getCurrentInstanceHandlerTypeDesc'
      );

      await customerEntitlementManager.getCurrentInstanceHandlerTypeDesc();

      expect(getCurrentInstanceHandlerTypeDesc).toBeCalledTimes(1);
    });
  });

  describe('generateMissingUsageCounterBasedOnTransactionCodes', () => {
    const updatedEntitlementMapResponse: Map<
      string,
      IUpdateUsageCounterEntitlementResponse
    > = new Map();

    const entitlementCodeRefId = uuidv4();

    updatedEntitlementMapResponse.set('L002', {
      entitlements: [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 25,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 1,
          value: 'value'
        }
      ],
      counters: [
        {
          id: entitlementCodeRefId,
          recorded: true
        }
      ]
    });

    updatedEntitlementMapResponse.set(AwardLimitGroupCodes.BONUS_TRANSFER, {
      entitlements: [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 25,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 1,
          value: 'value'
        }
      ],
      counters: [
        {
          id: entitlementCodeRefId,
          recorded: true
        }
      ]
    });

    const transactionCodes: ITransactionCodeMapping[] = [
      {
        interchange: BankNetworkEnum.ALTO,
        transactionCode: 'TFD30',
        transactionCodeInfo: {
          maxAmountPerTx: 50000000,
          minAmountPerTx: 10000,
          limitGroupCode: 'L002',
          awardGroupCounter: AwardLimitGroupCodes.BONUS_TRANSFER,
          entitlementCode: EntitlementCode.BONUS_TRANSFER,
          channel: 'TFD30',
          code: 'TFD30'
        }
      },
      {
        interchange: BankNetworkEnum.ARTAJASA,
        transactionCode: 'TFD40',
        transactionCodeInfo: {
          maxAmountPerTx: 50000000,
          minAmountPerTx: 10000,
          limitGroupCode: 'L002',
          awardGroupCounter: AwardLimitGroupCodes.BONUS_TRANSFER,
          entitlementCode: EntitlementCode.BONUS_TRANSFER,
          channel: 'TFD40',
          code: 'TFD40'
        }
      }
    ];

    const usageCounters: UsageCounter[] = [];

    it('when entitlementHandler is null return as is', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const customerEntitlementManager = new EntitlementManager(
        isEntitlementEnabled,
        null
      );

      const result = customerEntitlementManager.generateMissingUsageCounterBasedOnTransactionCodes(
        {
          transactionCodes,
          usageCounters,
          entitlementResultResponse: {
            updatedEntitlementMapResponse: updatedEntitlementMapResponse
          }
        }
      );

      expect(result).toBe(usageCounters);
    });

    it('when entitlementHandler exists, should expect that the call to the generateMissingUsageCounterBasedOnTransactionCodes made 1 time with the correct parameters ', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const entitlementHandler = new EntitlementHandler();

      const customerEntitlementManager = new EntitlementManager(
        isEntitlementEnabled,
        entitlementHandler
      );

      const generateMissingUsageCounter = jest.spyOn(
        customerEntitlementManager,
        'generateMissingUsageCounterBasedOnTransactionCodes'
      );

      await customerEntitlementManager.generateMissingUsageCounterBasedOnTransactionCodes(
        {
          transactionCodes,
          usageCounters,
          entitlementResultResponse: {
            updatedEntitlementMapResponse: updatedEntitlementMapResponse
          }
        }
      );

      expect(generateMissingUsageCounter).toBeCalledTimes(1);
      expect(generateMissingUsageCounter).toBeCalledWith({
        transactionCodes,
        usageCounters,
        entitlementResultResponse: {
          updatedEntitlementMapResponse: updatedEntitlementMapResponse
        }
      });
    });
  });
});
