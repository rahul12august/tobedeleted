import { BankNetworkEnum } from '@dk/module-common';
import entitlementFlag from '../../common/entitlementFlag';
import {
  AwardLimitGroupCodes,
  EntitlementCode
} from '../../entitlement/entitlement.enum';

import {
  ICustomerEntitlementByTransCodeRequest,
  IUpdateUsageCounterEntitlementResponse
} from '../../entitlement/entitlement.interface';
import uuidv4 from 'uuid/v4';
import { UsageCounter } from '../../award/award.type';
import { FeeRuleType, COUNTER } from '../../fee/fee.constant';
import { FeeRuleInfo } from '../../fee/fee.type';
import { CustomerEntitlementManagerFactory } from '../customerEntitlement.manager.factory';
import { AwardHandler } from '../../award/award.handler';
import { ITransactionCodeMapping } from '../../configuration/configuration.type';
import { AwardManager } from '../customerEntitlement.manager.impl';
import { getCountersList } from '../../configuration/__mocks__/configuration.data';
import configurationRepository from '../../configuration/configuration.repository';

jest.mock('../../configuration/configuration.repository');

describe('customerAwardManager AWARD', () => {
  entitlementFlag.isEnabled = jest.fn().mockResolvedValue(false);

  describe('preProcessCustomerEntitlementBasedOnTransactionCodes', () => {
    const params: ICustomerEntitlementByTransCodeRequest = {
      transactionCodes: [
        {
          interchange: BankNetworkEnum.ALTO,
          transactionCode: 'TFD30',
          transactionCodeInfo: {
            maxAmountPerTx: 50000000,
            minAmountPerTx: 10000,
            limitGroupCode: 'L002',
            awardGroupCounter: AwardLimitGroupCodes.BONUS_TRANSFER,
            entitlementCode: EntitlementCode.BONUS_TRANSFER,
            channel: 'TFD30',
            code: 'TFD30'
          }
        },
        {
          interchange: BankNetworkEnum.ARTAJASA,
          transactionCode: 'TFD40',
          transactionCodeInfo: {
            maxAmountPerTx: 50000000,
            minAmountPerTx: 10000,
            limitGroupCode: 'L002',
            awardGroupCounter: AwardLimitGroupCodes.BONUS_TRANSFER,
            entitlementCode: EntitlementCode.BONUS_TRANSFER,
            channel: 'TFD40',
            code: 'TFD40'
          }
        }
      ],
      customerId: 'customer1',
      transactionId: undefined
    };

    it('when awardHandler is null return null', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const customerAwardManager = new AwardManager(isEntitlementEnabled);

      const result = await customerAwardManager.preProcessCustomerEntitlementBasedOnTransactionCodes(
        params
      );

      expect(result).toBeNull();
    });
  });

  describe('mapEntitlementUsageCounterByLimitGroupCode', () => {
    const usageCounters: UsageCounter[] = [
      {
        limitGroupCode: 'L002',
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0
      },
      {
        limitGroupCode: AwardLimitGroupCodes.BONUS_TRANSFER,
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0
      }
    ];

    it('when awardHandler is null return usage counters as is', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const customerAwardManager = new AwardManager(isEntitlementEnabled);

      const result = customerAwardManager.mapEntitlementUsageCounterByLimitGroupCode(
        usageCounters,
        null
      );

      expect(result).toBe(usageCounters);
    });

    it('when awardHandler exists, should expect that the call to the mapEntitlementUsageCounterByLimitGroupCode made 1 time with the correct parameters ', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const awardHandler = new AwardHandler();

      const customerAwardManager = new AwardManager(
        isEntitlementEnabled,
        awardHandler
      );

      const mapFn = jest.spyOn(
        customerAwardManager,
        'mapEntitlementUsageCounterByLimitGroupCode'
      );

      await customerAwardManager.mapEntitlementUsageCounterByLimitGroupCode(
        usageCounters,
        null
      );

      expect(mapFn).toBeCalledTimes(1);
      expect(mapFn).toBeCalledWith(usageCounters, null);
    });
  });

  describe('determineBasicFeeMapping', () => {
    const feeRuleInfoTemplate: FeeRuleInfo = {
      code: FeeRuleType.RTOL_TRANSFER_FEE_RULE,
      counterCode: COUNTER.COUNTER_01,
      basicFeeCode1: [
        {
          interchange: BankNetworkEnum.ARTAJASA,
          basicFeeCode: 'TF015'
        },
        {
          interchange: BankNetworkEnum.ALTO,
          basicFeeCode: 'TF008'
        }
      ],
      basicFeeCode2: [
        {
          interchange: BankNetworkEnum.ARTAJASA,
          basicFeeCode: 'TF010'
        },
        {
          interchange: BankNetworkEnum.ALTO,
          basicFeeCode: 'TF009'
        }
      ],
      monthlyNoTransaction: 0,
      quota: 25,
      interchange: BankNetworkEnum.ALTO
    };

    it('when awardHandler exists, should expect that the call to the determineBasicFeeMapping made 1 time with the correct parameters ', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const awardHandler = new AwardHandler();

      const customerAwardManager = new AwardManager(
        isEntitlementEnabled,
        awardHandler
      );

      (configurationRepository.getCounterByCode as jest.Mock).mockImplementation(
        counter => getCountersList(counter)
      );

      const getHandlerInstanceDetermineBasicFeeMappingFn = jest.spyOn(
        customerAwardManager,
        'getHandlerInstanceDetermineBasicFeeMapping'
      );

      const basicFeeMappingFn = jest.spyOn(
        customerAwardManager,
        'determineBasicFeeMapping'
      );

      await customerAwardManager.determineBasicFeeMapping(feeRuleInfoTemplate);

      expect(basicFeeMappingFn).toBeCalledTimes(1);
      expect(basicFeeMappingFn).toBeCalledWith(feeRuleInfoTemplate);
      expect(getHandlerInstanceDetermineBasicFeeMappingFn).toBeCalledTimes(1);
    });
  });

  describe('processFeeRuleInfo', () => {
    const feeRuleInfoTemplate: FeeRuleInfo = {
      code: FeeRuleType.RTOL_TRANSFER_FEE_RULE,
      counterCode: COUNTER.COUNTER_01,
      basicFeeCode1: [
        {
          interchange: BankNetworkEnum.ARTAJASA,
          basicFeeCode: 'TF015'
        },
        {
          interchange: BankNetworkEnum.ALTO,
          basicFeeCode: 'TF008'
        }
      ],
      basicFeeCode2: [
        {
          interchange: BankNetworkEnum.ARTAJASA,
          basicFeeCode: 'TF010'
        },
        {
          interchange: BankNetworkEnum.ALTO,
          basicFeeCode: 'TF009'
        }
      ],
      monthlyNoTransaction: 0,
      quota: 25,
      interchange: BankNetworkEnum.ALTO
    };

    const awardGroupCounter = 'Bonus_Transfer';
    const usageCounters: UsageCounter[] = [
      {
        limitGroupCode: 'L002',
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0
      },
      {
        limitGroupCode: AwardLimitGroupCodes.BONUS_TRANSFER,
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0
      }
    ];

    it('when awardHandler is null return as is', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const customerAwardManager = new AwardManager(isEntitlementEnabled);

      const getHandlerInstanceProcessFeeRuleInfo = jest.spyOn(
        customerAwardManager,
        'getHandlerInstanceProcessFeeRuleInfo'
      );

      const result = customerAwardManager.processFeeRuleInfo(
        feeRuleInfoTemplate,
        FeeRuleType.BIFAST_FEE_RULE,
        awardGroupCounter,
        usageCounters
      );

      expect(result).toBe(feeRuleInfoTemplate);
      expect(getHandlerInstanceProcessFeeRuleInfo).toBeCalledTimes(1);
    });

    it('when awardHandler exists, should expect that the call to the processFeeRuleInfo made 1 time with the correct parameters ', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const awardHandler = new AwardHandler();

      const customerAwardManager = new AwardManager(
        isEntitlementEnabled,
        awardHandler
      );

      const basicFeeMappingFn = jest.spyOn(
        customerAwardManager,
        'processFeeRuleInfo'
      );

      const getHandlerInstanceProcessFeeRuleInfo = jest.spyOn(
        customerAwardManager,
        'getHandlerInstanceProcessFeeRuleInfo'
      );

      await customerAwardManager.processFeeRuleInfo(
        feeRuleInfoTemplate,
        FeeRuleType.BIFAST_FEE_RULE,
        awardGroupCounter,
        usageCounters
      );

      expect(basicFeeMappingFn).toBeCalledTimes(1);
      expect(basicFeeMappingFn).toBeCalledWith(
        feeRuleInfoTemplate,
        FeeRuleType.BIFAST_FEE_RULE,
        awardGroupCounter,
        usageCounters
      );
      expect(getHandlerInstanceProcessFeeRuleInfo).toBeCalledTimes(1);
    });
  });

  describe('getCurrentInstanceHandlerTypeDesc', () => {
    const isEntitlementFlagEnabledFn = (entitlementFlag.isEnabled = jest.fn());

    beforeEach(() => {
      isEntitlementFlagEnabledFn.mockClear();
      CustomerEntitlementManagerFactory.resetCustomerEntitlementManager();
      jest.clearAllMocks();
    });

    it(`when awardHandler exists [entitlementFlag = false], should expect that the call to the getCurrentInstanceType made 1 time `, async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const awardHandler = new AwardHandler();

      const customerAwardManager = new AwardManager(
        isEntitlementEnabled,
        awardHandler
      );

      const getCurrentInstanceHandlerTypeDesc = jest.spyOn(
        customerAwardManager,
        'getCurrentInstanceHandlerTypeDesc'
      );

      await customerAwardManager.getCurrentInstanceHandlerTypeDesc();

      expect(getCurrentInstanceHandlerTypeDesc).toBeCalledTimes(1);
    });
  });

  describe('generateMissingUsageCounterBasedOnTransactionCodes', () => {
    const updatedEntitlementMapResponse: Map<
      string,
      IUpdateUsageCounterEntitlementResponse
    > = new Map();

    const entitlementCodeRefId = uuidv4();

    updatedEntitlementMapResponse.set('L002', {
      entitlements: [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 25,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 1,
          value: 'value'
        }
      ],
      counters: [
        {
          id: entitlementCodeRefId,
          recorded: true
        }
      ]
    });

    updatedEntitlementMapResponse.set(AwardLimitGroupCodes.BONUS_TRANSFER, {
      entitlements: [
        {
          entitlement: 'entitlement',
          type: 'type',
          dataType: 'dataType',
          description: 'description',
          quota: 25,
          comments: 'comments',
          counterCode: 'counterCode',
          usedQuota: 1,
          value: 'value'
        }
      ],
      counters: [
        {
          id: entitlementCodeRefId,
          recorded: true
        }
      ]
    });

    const transactionCodes: ITransactionCodeMapping[] = [
      {
        interchange: BankNetworkEnum.ALTO,
        transactionCode: 'TFD30',
        transactionCodeInfo: {
          maxAmountPerTx: 50000000,
          minAmountPerTx: 10000,
          limitGroupCode: 'L002',
          awardGroupCounter: AwardLimitGroupCodes.BONUS_TRANSFER,
          entitlementCode: EntitlementCode.BONUS_TRANSFER,
          channel: 'TFD30',
          code: 'TFD30'
        }
      },
      {
        interchange: BankNetworkEnum.ARTAJASA,
        transactionCode: 'TFD40',
        transactionCodeInfo: {
          maxAmountPerTx: 50000000,
          minAmountPerTx: 10000,
          limitGroupCode: 'L002',
          awardGroupCounter: AwardLimitGroupCodes.BONUS_TRANSFER,
          entitlementCode: EntitlementCode.BONUS_TRANSFER,
          channel: 'TFD40',
          code: 'TFD40'
        }
      }
    ];

    const usageCounters: UsageCounter[] = [];

    it('when awardHandler exists, should expect that the call to the generateMissingUsageCounterBasedOnTransactionCodes made 1 time with the correct parameters ', async () => {
      const isEntitlementEnabled = await entitlementFlag.isEnabled();

      const awardHandler = new AwardHandler();

      const customerAwardManager = new AwardManager(
        isEntitlementEnabled,
        awardHandler
      );

      const generateMissingUsageCounter = jest.spyOn(
        customerAwardManager,
        'generateMissingUsageCounterBasedOnTransactionCodes'
      );

      await customerAwardManager.generateMissingUsageCounterBasedOnTransactionCodes(
        {
          transactionCodes,
          usageCounters
        }
      );

      expect(generateMissingUsageCounter).toBeCalledTimes(1);
      expect(generateMissingUsageCounter).toBeCalledWith({
        transactionCodes,
        usageCounters
      });
    });
  });
});
