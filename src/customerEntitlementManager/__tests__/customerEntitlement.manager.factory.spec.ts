import { AwardHandler } from '../../award/award.handler';
import entitlementFlag from '../../common/entitlementFlag';
import { EntitlementHandler } from '../../entitlement/entitlement.handler';
import { CustomerEntitlementManagerFactory } from '../customerEntitlement.manager.factory';
import {
  AwardManager,
  EntitlementManager
} from './../customerEntitlement.manager.impl';

const isEntitlementFlagEnabledFn = (entitlementFlag.isEnabled = jest.fn());

describe('CustomerEntitlementManagerFactory', () => {
  beforeEach(() => {
    isEntitlementFlagEnabledFn.mockClear();
    CustomerEntitlementManagerFactory.resetCustomerEntitlementManager();
    jest.clearAllMocks();
  });

  it('if entitlementEnable flag is true, then should return entitlementManager', async () => {
    isEntitlementFlagEnabledFn.mockResolvedValue(true);

    const custEntitlementManager = await CustomerEntitlementManagerFactory.getCustomerEntitlementManagerByFlag();

    expect(custEntitlementManager).not.toBeNull();
    expect(custEntitlementManager).toBeInstanceOf(EntitlementManager);
    expect(custEntitlementManager.getCurrentInstanceType()).toBe(
      'EntitlementManager'
    );
    expect(custEntitlementManager.isEntitlementEnabled).toBeTruthy();
    expect(
      custEntitlementManager.customerEntitlementHandlerInstance
    ).toBeInstanceOf(EntitlementHandler);
    expect(
      custEntitlementManager.customerEntitlementHandlerFallbackInstance
    ).toBeInstanceOf(AwardHandler);
  });

  it('if entitlementEnable flag is false, then should return awardManager', async () => {
    isEntitlementFlagEnabledFn.mockResolvedValue(false);

    const custEntitlementManager2 = await CustomerEntitlementManagerFactory.getCustomerEntitlementManagerByFlag();
    expect(custEntitlementManager2).not.toBeNull();
    expect(custEntitlementManager2).toBeInstanceOf(AwardManager);
    expect(custEntitlementManager2.getCurrentInstanceType()).toBe(
      'AwardManager'
    );
    expect(custEntitlementManager2.isEntitlementEnabled).toBeFalsy();
    expect(
      custEntitlementManager2.customerEntitlementHandlerInstance
    ).toBeInstanceOf(AwardHandler);
    expect(
      custEntitlementManager2.customerEntitlementHandlerFallbackInstance
    ).toBeNull();
  });
});
