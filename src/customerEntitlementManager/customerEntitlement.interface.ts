import { UsageCounter } from '../award/award.type';
import {
  ICounterUsageByTransCodeRequest,
  ICustomerEntitlementByTransCodeRequest,
  ICustomerEntitlementByTransCodeResponse
} from '../entitlement/entitlement.interface';

import { BasicFeeMapping, FeeRuleInfo } from '../fee/fee.type';

export interface CustomerEntitlementHandler {
  preProcessCustomerEntitlementBasedOnTransactionCodes(
    params: ICustomerEntitlementByTransCodeRequest
  ): Promise<ICustomerEntitlementByTransCodeResponse | null>;

  generateMissingUsageCounterBasedOnTransactionCodes(
    params: ICounterUsageByTransCodeRequest
  ): Promise<UsageCounter[]>;

  mapEntitlementUsageCounterByLimitGroupCode(
    usageCounters: UsageCounter[],
    entitlementResultResponse: ICustomerEntitlementByTransCodeResponse | null
  ): UsageCounter[];

  determineBasicFeeMapping(
    feeRuleObject: FeeRuleInfo
  ): Promise<BasicFeeMapping[] | undefined>;

  processFeeRuleInfo(
    currentFeeRuleInfo: FeeRuleInfo,
    feeRuleType: string | undefined,
    awardGroupCounter: string | undefined,
    usageCounters: UsageCounter[] | undefined
  ): FeeRuleInfo;
}
