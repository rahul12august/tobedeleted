import hapi from '@hapi/hapi';

const ping: hapi.ServerRoute = {
  method: 'GET',
  path: '/ping',
  options: {
    description: 'Pongs back',
    notes: 'Private Api to check if service pongs on a ping',
    tags: ['api'],
    auth: false,
    handler: (_request: hapi.Request, _h: hapi.ResponseToolkit) => 'pong!'
  }
};
const healthController: hapi.ServerRoute[] = [ping];
export default healthController;
