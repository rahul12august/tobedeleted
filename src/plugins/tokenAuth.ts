import { tokenAuth } from '@dk/module-common';

import logger from '../logger';
import { config } from '../config';
const { jwksUri } = config.get('idm');

export default {
  plugin: tokenAuth,
  options: {
    logger,
    jwksUri
  }
};
