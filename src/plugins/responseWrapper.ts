import ResponseWrapper from '@dk/module-common/dist/hapiPlugin/responseWrapper.plugin';
import logger from '../logger';
import { ErrorList } from '../common/errors';
import { context } from '../context';

const responseWrapper = {
  plugin: new ResponseWrapper(context).responseWrapper,
  options: {
    logger,
    errorList: ErrorList
  }
};

export default responseWrapper;
