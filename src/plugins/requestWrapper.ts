import RequestWrapperPlugin from '@dk/module-common/dist/hapiPlugin/requestWrapper.plugin';
import { context } from '../context';
import { ErrorList } from '../common/errors';
import logger from '../logger';

const requestWrapper = {
  plugin: new RequestWrapperPlugin(context).requestWrapper,
  options: {
    logger,
    errorList: ErrorList
  }
};

export default requestWrapper;
