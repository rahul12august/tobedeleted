import * as HapiSwagger from 'hapi-swagger';
import { displayErrorsDescription } from '@dk/module-common/dist/error';
import * as Package from '../../package.json';
import { ErrorList } from '../common/errors';
import * as fs from 'fs';
import * as path from 'path';

const apiDocPath = path.join(__dirname, '..', '..', 'api-doc.md');
const apiDoc = fs.readFileSync(apiDocPath, 'utf8');
const errorDesc = displayErrorsDescription(ErrorList);

const swaggerOptions: HapiSwagger.RegisterOptions = {
  info: {
    title: 'Transfer API Documentation',
    version: Package.version,
    description: apiDoc + '\n' + errorDesc
  },
  cors: process.env.NODE_ENV === 'dev',
  grouping: 'tags',
  securityDefinitions: {
    jwt: {
      type: 'apiKey',
      name: 'Authorization',
      in: 'header'
    }
  },
  security: [{ jwt: [] }]
};

export default {
  plugin: HapiSwagger,
  options: swaggerOptions
};
