export const mockToken =
  'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjZ2MkwrYllJNmszV3ZsSHhmRmJSYStZSW8zOD0ifQ.eyJjdXN0b21lcklkIjoiY3VzdG9tZXJJZCIsImNpZiI6ImNpZiJ9.Ma1XUOpR7igW6n_d1cZNSKWl1g0QynmSfP9-zEuMsBm-We9z4eBcvzvxPfdIKIo1XFcXRqorlmSecJE_jgpRBxmEetKPasILASfBkBnsn8hZVi550pfEI1V9GvEVK7Me612q30Jj-x4Y5zYpSZnF7OYfPuBgZz3cAyZ-VtScVvRVs57ui_3kHbDdrlPcrTeZ3N0a1aqe8FFxSbb4i0yHd_c6zAh0cggDyZydh7qZ_MNVwrqmwheLsQ0pqApSCI9oqCS9sCaaSL3M2Sq_eiIpqF6oKEQR8cKh8aQ_w80qaZ7YFj-PVuPrk1YCyMdgNiYpc8ykuQ8KnG_gbJ_NLqGudQ';

export const mockPublicKey = [
  `-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnzyis1ZjfNB0bBgKFMSv\nvkTtwlvBsaJq7S5wA+kzeVOVpVWwkWdVha4s38XM/pa/yr47av7+z3VTmvDRyAHc\naT92whREFpLv9cj5lTeJSibyr/Mrm/YtjCZVWgaOYIhwrXwKLqPr/11inWsAkfIy\ntvHWTxZYEcXLgAXFuUuaS3uF9gEiNQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0\ne+lf4s4OxQawWD79J9/5d3Ry0vbV3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWb\nV6L11BWkpzGXSW4Hv43qa+GSYOD2QU68Mb59oSk2OB+BtOLpJofmbGEGgvmwyCI9\nMwIDAQAB\n-----END PUBLIC KEY-----`
];

export const mockJwksUri =
  'https://am.fr.dk.io:30443/am/oauth2/customers/connect/jwk_uri';
