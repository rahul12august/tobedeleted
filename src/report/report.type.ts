import hapi from '@hapi/hapi';
import { ReadStream } from 'fs';
import { TransactionStatus } from '../transaction/transaction.enum';
import { ITransactionModel } from '../transaction/transaction.model';
import { ReconcileReportTxnType } from './report.enum';
import { BillPaymentTransactionStatusEnum } from '../billPayment/billPaymentTransaction/billPaymentTransaction.enum';

export interface IReconcileReportRequest extends hapi.Request {
  query: {
    fromDate: string;
    toDate: string;
    paymentServiceCode: string[];
    accountNo: string[];
    type: string;
  };
}

export interface IReconcileReportCondition {
  fromDate?: Date;
  toDate?: Date;
  paymentServiceCodes?: string[];
  accountNos?: string[];
  additionalInformation4?: string;
}

export interface IReconcileReport {
  fromDate?: Date;
  toDate?: Date;
  paymentServiceCodes?: string[];
  accountNos?: string[];
  type?: string;
  preferedId?: string;
  additionalInformation4?: string;
}

export interface IRunningDateRange {
  start: Date;
  end: Date;
}

export interface ISendEmailPayload {
  recipientEmails: string;
  notificationCode: string;
  attachedFile: ReadStream;
  params: string;
}

export interface IReconcileReportCSV {
  referenceId: string;
  amount: number;
  transactionDate?: Date;
  status: TransactionStatus | BillPaymentTransactionStatusEnum;
  sourceAccount?: string;
  beneficiaryAccount?: string;
  bankId?: string;
  bankName?: string | undefined;
  channel: string;
  detail?: string;
  type: ReconcileReportTxnType;
  additionalInformation3?: string;
  additionalInformation4?: string;
}

export enum ReconcileReportJobType {
  DEFAULT = 'DEFAULT',
  GOBILL_REPORT = 'GOBILL_REPORT'
}

export type IReportTxnMapper = (
  transaction: ITransactionModel,
  type: string,
  reference: string
) => Promise<IReconcileReportCSV>;

export type IReportMapper = {
  [K in ReconcileReportJobType]: IReportTxnMapper;
};
