export enum AccountSideEnum {
  Source = 'Source',
  Beneficiary = 'Beneficiary'
}

export enum PreferenceEnum {
  thirdPartyIncomingId = 'externalId',
  thirdPartyOutgoingId = 'thirdPartyOutgoingId'
}

export enum ReconcileReportTxnType {
  TRANSACTION = 'Transaction',
  REFUND = 'Refund'
}
