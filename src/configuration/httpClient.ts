import { config } from '../config';
import logger from '../logger';
import { context } from '../context';
import { Http } from '@dk/module-common';
import { HttpClient } from '@dk/module-httpclient';
import { retryConfig } from '../common/constant';

const MS_CONFIGURATION_URL = config.get('ms').configuration;

const httpClient: HttpClient = Http.createHttpClientForward({
  baseURL: `${MS_CONFIGURATION_URL}/graphql`,
  context,
  logger,
  retryConfig
});

export default httpClient;
