import _, { isEmpty } from 'lodash';
import { Enum, PaymentServiceTypeEnum } from '@dk/module-common';
import { IPollingConfig } from '../common/common.type';
import configurationRepository from './configuration.repository';
import logger, { wrapLogs } from '../logger';
import {
  IParameterSetting,
  IPaymentConfigRule,
  ISavingProduct,
  ITransactionCodeInfo
} from './configuration.type';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import {
  BIFAST_ENABLED_CODE,
  BIFAST_REFERENCE_GROUP,
  DEBIT_CARD_TRANSACTION_GROUP
} from './configuration.constant';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';
import entitlementService from '../entitlement/entitlement.service';
import { IEntitlementResponse } from '../entitlement/entitlement.interface';
import configurationOverride, {
  ITxLimitOverrideStrategy
} from './configuration.override';

const getGoBillsPollingConfig = async (): Promise<IPollingConfig[]> => {
  const pollingConfigs = await configurationRepository.getBillingAggregatorPollingConfig(
    Enum.BillingAggregator.GOBILLS
  );
  return pollingConfigs && !isEmpty(pollingConfigs)
    ? pollingConfigs[0].configs
    : [];
};

const filterConfigRulesByServiceType = async (
  paymentConfigRules: IPaymentConfigRule[] | undefined,
  paymentServiceType: PaymentServiceTypeEnum
): Promise<IPaymentConfigRule[] | undefined> =>
  (paymentConfigRules || []).filter(
    configRule => configRule.paymentServiceType === paymentServiceType
  );

const filterConfigRulesByServiceCode = async (
  paymentConfigRules: IPaymentConfigRule[] | undefined,
  paymentServiceCode: string
): Promise<IPaymentConfigRule[] | undefined> =>
  (paymentConfigRules || []).filter(configRule =>
    configRule.serviceRecommendation.includes(paymentServiceCode)
  );

const filterConfigRulesByServiceTypeAndCode = async (
  paymentConfigRules: IPaymentConfigRule[] | undefined,
  paymentServiceType: PaymentServiceTypeEnum,
  paymentServiceCode: string
): Promise<IPaymentConfigRule[] | undefined> =>
  (paymentConfigRules || []).filter(
    configRule =>
      configRule.paymentServiceType === paymentServiceType &&
      configRule.serviceRecommendation.includes(paymentServiceCode)
  );

const getTransactionCodeInfo = (
  paymentConfigRules: IPaymentConfigRule[]
): ITransactionCodeInfo[] => {
  const allTransactionCodeInfo = paymentConfigRules.map(rule => {
    return rule.paymentServiceMappings.map(service => {
      let transactionCodeInfo: ITransactionCodeInfo[] = [];
      if (service.creditTransactionCode) {
        transactionCodeInfo = transactionCodeInfo.concat(
          service.creditTransactionCode.map(code => code.transactionCodeInfo)
        );
      }
      if (service.debitTransactionCode) {
        transactionCodeInfo = transactionCodeInfo.concat(
          service.debitTransactionCode.map(code => code.transactionCodeInfo)
        );
      }
      return transactionCodeInfo;
    });
  });
  return _.flatten(_.flatten(allTransactionCodeInfo));
};

function overrideConfigAmount<CONFIG_TYPE>(
  customerId: string,
  limitGroupCode: string,
  entitlementsMap: Map<string, IEntitlementResponse>,
  config: CONFIG_TYPE,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  overrideStrategy: ITxLimitOverrideStrategy<CONFIG_TYPE>
) {
  const entitlement = entitlementsMap.get(
    overrideStrategy.mapToEntitlementCode(limitGroupCode)
  );
  if (entitlement) {
    const entitlementAmount = Number(entitlement.quota);
    if (isNaN(entitlementAmount) || entitlementAmount < 0) {
      logger.error(
        `Expected the entitlement '${entitlement.entitlement}'` +
          `for customer '${customerId}' to be a positive numerical value, but it was '${entitlement.quota}'`
      );
      return;
    }

    overrideStrategy.overrideLimit(customerId, config, entitlementAmount);
  }
}

function applyCustomLimitsToTransactionCodeInfo(
  transactionCodeInfo: ITransactionCodeInfo[],
  customerId: string,
  entitlementsMap: Map<string, IEntitlementResponse>
) {
  for (const codeInfo of transactionCodeInfo) {
    const limitGroupCode = codeInfo.limitGroupCode;
    if (!limitGroupCode) continue;
    overrideConfigAmount(
      customerId,
      limitGroupCode,
      entitlementsMap,
      codeInfo,
      configurationOverride.minAmountPerTxOverrider
    );
    overrideConfigAmount(
      customerId,
      limitGroupCode,
      entitlementsMap,
      codeInfo,
      configurationOverride.maxAmountPerTxOverrider
    );
  }
}

const getLimitGroupCodesForPaymentConfigRule = (
  paymentConfigRule: IPaymentConfigRule
): string[] => {
  const transactionCodeInfo = getTransactionCodeInfo([paymentConfigRule]);
  return _.compact(
    _.uniqBy(
      transactionCodeInfo.map(codeInfo => codeInfo.limitGroupCode),
      code => code
    )
  );
};

const applyCustomLimitsToPaymentConfigRules = (
  customerId: string,
  paymentConfigRules: IPaymentConfigRule[],
  entitlementsMap: Map<string, IEntitlementResponse>
) => {
  for (const configRule of paymentConfigRules) {
    const limitGroupCodes = getLimitGroupCodesForPaymentConfigRule(configRule);
    if (limitGroupCodes.length != 1) {
      logger.debug(
        'Unable to apply custom limits to payment config rules that do not have a singular limit group code'
      );
      continue;
    }
    overrideConfigAmount(
      customerId,
      limitGroupCodes[0],
      entitlementsMap,
      configRule,
      configurationOverride.paymentConfigRuleRangeFromOverrider
    );
    overrideConfigAmount(
      customerId,
      limitGroupCodes[0],
      entitlementsMap,
      configRule,
      configurationOverride.paymentConfigRuleRangeToOverrider
    );
  }
};

const applyCustomLimits = async (
  customerId: string,
  paymentConfigRules: IPaymentConfigRule[]
): Promise<void> => {
  const transactionCodeInfo = getTransactionCodeInfo(paymentConfigRules);
  const limitGroupCodes = _.compact(
    _.uniqBy(
      transactionCodeInfo.map(codeInfo => codeInfo.limitGroupCode),
      code => code
    )
  );

  const entitlementCodes = [
    ...limitGroupCodes.map(code =>
      configurationOverride.minAmountPerTxOverrider.mapToEntitlementCode(code)
    ),
    ...limitGroupCodes.map(code =>
      configurationOverride.maxAmountPerTxOverrider.mapToEntitlementCode(code)
    )
  ];
  const entitlementsMap = await entitlementService.getEntitlementsMap(
    customerId,
    entitlementCodes
  );

  applyCustomLimitsToTransactionCodeInfo(
    transactionCodeInfo,
    customerId,
    entitlementsMap
  );
  applyCustomLimitsToPaymentConfigRules(
    customerId,
    paymentConfigRules,
    entitlementsMap
  );
};

const filterPaymentConfigRules = async (
  paymentServiceType?: PaymentServiceTypeEnum,
  paymentServiceCode?: string,
  paymentConfigRules?: IPaymentConfigRule[]
) => {
  if (paymentServiceType && paymentServiceCode) {
    return await filterConfigRulesByServiceTypeAndCode(
      paymentConfigRules,
      paymentServiceType,
      paymentServiceCode
    );
  }
  if (paymentServiceType) {
    return await filterConfigRulesByServiceType(
      paymentConfigRules,
      paymentServiceType
    );
  }
  if (paymentServiceCode) {
    return await filterConfigRulesByServiceCode(
      paymentConfigRules,
      paymentServiceCode
    );
  }
  return paymentConfigRules;
};

const getPaymentConfigRules = async (
  paymentServiceType?: PaymentServiceTypeEnum,
  paymentServiceCode?: string,
  customerId?: string
): Promise<IPaymentConfigRule[] | undefined> => {
  const paymentConfigRules = await configurationRepository.getPaymentConfigRules();
  const filteredRules = await filterPaymentConfigRules(
    paymentServiceType,
    paymentServiceCode,
    paymentConfigRules
  );
  if (customerId && filteredRules) {
    await applyCustomLimits(customerId, filteredRules);
  }
  return filteredRules;
};

const getSavingProduct = async (
  productCode: string
): Promise<ISavingProduct> => {
  const savingProduct = await configurationRepository.getSavingProduct(
    productCode
  );

  if (!savingProduct) {
    throw new TransferAppError(
      `Saving product not found for: ${productCode}!`,
      TransferFailureReasonActor.JAGO,
      ERROR_CODE.SAVING_PRODUCT_NOT_FOUND
    );
  }

  return savingProduct;
};

const getParameterSettingByReferenceGroup = async (
  referenceGroup: string
): Promise<IParameterSetting[]> => {
  const parameterSetting = await configurationRepository.getParameterSettingByReferenceGroup(
    referenceGroup
  );
  return parameterSetting && !isEmpty(parameterSetting) ? parameterSetting : [];
};

const isBiFastEnabled = async (): Promise<boolean> => {
  try {
    const parameterSetting = await configurationRepository.getParameterSettingByReferenceGroup(
      BIFAST_REFERENCE_GROUP
    );
    if (isEmpty(parameterSetting)) {
      logger.warn(
        `isBiFastEnabled: ${BIFAST_REFERENCE_GROUP} parameter setting not found`
      );
      return false;
    }
    const isBiFastEnabled =
      parameterSetting.find(param => param.code === BIFAST_ENABLED_CODE)
        ?.value === 'true';

    logger.info(
      `isBiFastEnabled: returning BIFAST_ENABLED flag value: ${isBiFastEnabled}`
    );

    return isBiFastEnabled;
  } catch (error) {
    logger.error(
      `getParameterSettingByReferenceGroup: Error while fetching parameter setting.`
    );
    return false;
  }
};

const isDebitCardTransactionGroup = async (
  paymentServiceType: string
): Promise<boolean> => {
  const parameterSettings = await configurationRepository.getParameterSettingByReferenceGroup(
    DEBIT_CARD_TRANSACTION_GROUP
  );

  if (parameterSettings) {
    for (let parameterSetting of parameterSettings) {
      if (parameterSetting.code === paymentServiceType) {
        return true;
      }
    }
  }

  return false;
};

const configurationService = {
  getGoBillsPollingConfig,
  getPaymentConfigRules,
  getSavingProduct,
  getParameterSettingByReferenceGroup,
  isBiFastEnabled,
  isDebitCardTransactionGroup,
  applyCustomLimits
};

export default wrapLogs(configurationService);
