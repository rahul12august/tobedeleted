import {
  BankChannelEnum,
  BankNetworkEnum,
  GeneralLedgerCode,
  PaymentServiceTypeEnum
} from '@dk/module-common';
import { ReconcileReportJobType } from '../report/report.type';
import { TransactionCodeType } from './configuration.enum';

export interface ITransactionCodeInfo {
  code: string;
  limitGroupCode?: string;
  feeRules?: string;
  minAmountPerTx: number;
  maxAmountPerTx: number;
  defaultCategoryCode?: string;
  channel: string;
  awardGroupCounter?: string;
  entitlementCode?: string;
  counterCode?: string;
}

export interface ITransactionCodeMapping {
  interchange?: BankNetworkEnum;
  transactionCode: string;
  transactionCodeInfo: ITransactionCodeInfo;
}

export interface IPaymentServiceMapping {
  paymentServiceCode: string;
  beneficiaryBankCodeType: string;
  debitTransactionCode?: ITransactionCodeMapping[];
  creditTransactionCode?: ITransactionCodeMapping[];
  transactionAuthenticationChecking: boolean;
  inquiryFeeRule?: string;
  blocking: boolean;
  requireThirdPartyOutgoingId: boolean;
  cutOffTime?: string;
}

export interface IPaymentConfigRule {
  paymentServiceType: PaymentServiceTypeEnum;
  source: BankChannelEnum;
  target: BankChannelEnum;
  sourceAccType: string[];
  targetAccType: string[];
  sameCIF: string;
  amountRangeFrom: number;
  amountRangeTo: number;
  rtolDailyUsage: string;
  sknDailyUsage: string;
  rtgsDailyUsage: string;
  serviceRecommendation: string[];
  paymentServiceMappings: IPaymentServiceMapping[];
}
export interface ILimitGroup {
  code: string;
  dailyLimitAmount: number;
}

export interface IBasicFee {
  code: string;
  feeAmountFixed?: number | null;
  feeAmountPercentage?: number | null;
  customerTc: string;
  customerTcInfo: {
    channel: string;
  };
  subsidiary: boolean;
  subsidiaryAmount?: number;
  subsidiaryAmountPercentage?: number;
  debitGlNumber?: string;
  creditGlNumber?: string;
  thirdPartyFee?: boolean;
}
export interface IBankCode {
  bankCodeId: string;
  name: string;
  rtolCode?: string;
  remittanceCode?: string;
  billerCode?: string;
  companyName: string;
  isBersamaMember: boolean;
  isAltoMember: boolean;
  firstPriority?: BankNetworkEnum;
  channel: BankChannelEnum;
  type: string;
  ordering?: number;
  images?: string;
  prefix?: string;
  isInternal: boolean;
  irisCode?: string;
  supportedChannels?: BankChannelEnum[];
}

export interface IInquiryBankInfoResult {
  sourceBank: IBankCode;
  beneficiaryBank: IBankCode;
  currency: {
    code: string;
  };
}

export interface ITransactionCategory {
  code: string;
  isIncoming: boolean;
  isOutgoing: boolean;
}

export interface ICurrency {
  code: string;
  isoNumber: number;
  name: string;
}

export interface IHoliday {
  name: string;
  toDate: Date;
  fromDate: Date;
}

export interface IPaymentServiceMappingInput {
  paymentServiceCodes: [string];
  monthlyNoTransaction?: number;
  targetBankCode?: string;
}

export enum RunningDate {
  DAILY = 'Daily',
  MONTHLY = 'Monthly'
}

export enum ReferenceAccountEnum {
  BENEFICIARY = 'Beneficiary',
  SOURCE = 'Source'
}

export interface ITransactionReportConfig {
  job: ReconcileReportJobType;
  paymentServiceCode: string[];
  accountNo?: string[];
  periodOfReport: number;
  type: string;
  runningDate: RunningDate;
  runningTime: string;
  fileLocation: string;
  referenceBank: ReferenceAccountEnum;
  mailingList?: string[];
  notificationTemplate?: string;
  preferedId: string;
  additionalInformation4?: string;
}

export enum UploadTypeEnum {
  ASSET = 'ASSET',
  CUSTOMER = 'CUSTOMER',
  MERCHANT = 'MERCHANT',
  REPORT = 'REPORT'
}

export interface IFileUploadParameter {
  code: string;
  description: string;
  type: UploadTypeEnum;
  location: string;
  prefixFileName: string;
  fileType: string[];
}

export interface IBiller {
  bankCode: string;
  code: string;
  name: string;
  inquireFeeInfo: boolean;
}

export interface IMambuBlockingDays {
  code: string;
  name: string;
  days: number;
}

export interface IUpdateCounterInput {
  code: string;
  type?: string;
  name?: string;
  value?: number;
}

export interface Counter {
  code: string;
  type?: string;
  name: string;
  value: number;
}

export interface WorkingTimeModel {
  dayName: string;
  startTime: string;
  endTime: string;
}
export interface IBasicInterest {
  code: string;
  value: number;
}
export interface ISavingProductTenors {
  code: string;
  month: number;
  dayFrom: number;
  dayTo: number;
}
export interface InterestRateMapping {
  isVip: boolean;
  savingProductTenor: ISavingProductTenors;
  basicInterest: IBasicInterest;
}
export interface ISavingProductAmounts {
  tieringCode: string;
  tieringAmount: number;
  interestRateMappings?: InterestRateMapping[];
}
export interface IInterestRule {
  code: string;
  rule: string;
  basicInterests?: IBasicInterest[];
  savingProductAmounts?: ISavingProductAmounts[];
  savingProductTenors?: ISavingProductTenors[];
}

export interface ISavingProduct {
  encodedKey: string;
  code: string;
  blockingCapability: boolean;
  deletionCapability: boolean;
  goalCapability: boolean;
  maximumAccountPerUser: number;
  monthlyFeeCode: string;
  currency: string;
  type: string;
  initialAmount: number;
  maximumBalancePerAccount: number;
}
export interface ITransactionCode {
  code: string;
  ownershipCode: string;
  transactionType: TransactionCodeType;
  defaultCategoryCode?: string;
  splitBill: boolean;
  isExternal: boolean;
  downloadReceipt: boolean;
  allowedToDispute: boolean;
  currency?: string;
  minAmountPerTx: number;
  maxAmountPerTx: number;
  limitGroupCode?: string;
  feeRules?: string;
  allowedToRepeat: boolean;
  addToContact: boolean;
  allowedToInstallment: boolean;
  loyaltyPoint: boolean;
  cashBack: boolean;
  image?: string;
  notificationTemplate?: string;
  showInConsolidatedTrxHistory: boolean;
  neutralizeSignage: boolean;
  reversalCodeReference?: string;
  maxReversalDay: number;
  channel: string;
  awardGroupCounter?: string;
}

export interface IParameterSetting {
  code: string;
  name: string;
  value: string;
}

export interface IGeneralLedger {
  code: GeneralLedgerCode;
  accountNumber: string;
}

export interface INNSMapping {
  code: string;
  institutionName: string;
  brandName: string;
  switchingName: string;
  reservedNnsPrefix: string;
}
