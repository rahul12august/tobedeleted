const holidaysByDateRangeQuery = `        
  query getHolidaysByDateRange($fromDate:Date, $toDate:Date) {
    holidays(fromDate: $fromDate, toDate: $toDate) {
        name
        toDate
        fromDate
    }
  }
`;

export default holidaysByDateRangeQuery;
