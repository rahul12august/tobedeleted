const transactionCategoryQuery = `
query transactionCategoryQuery($code: String!) {
  transactionCategory(code: $code) {
    code
    isIncoming
    isOutgoing
  }
}
`;

export default transactionCategoryQuery;
