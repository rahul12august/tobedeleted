const feeRuleQuery = `
query getFeeRule($feeRuleInput: FeeRuleInput!) {
  feeRule(feeRuleInput: $feeRuleInput) {
    description
    basicFeeMapping {
      interchange
      basicFeeCode
      basicFee {
        feeAmountPercentage
        feeAmountFixed
        customerTc
        customerTcInfo {
          channel
        }
        subsidiary
        subsidiaryAmount
        subsidiaryAmountPercentage
        debitGlNumber
        creditGlNumber,
        thirdPartyFee
      }
    }
  }
}
`;

export default feeRuleQuery;
