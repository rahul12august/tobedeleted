const generalLedgerQuery = `
query generalLedgers {
  generalLedgers {
    code
    accountNumber
  }
}
`;

export default generalLedgerQuery;
