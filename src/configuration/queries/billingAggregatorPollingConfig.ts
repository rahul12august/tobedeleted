const billingAggregatorPollingConfigQuery = `
query billingAggregatorPollingConfig($billingAggregator: String) {
  billingAggregatorPollingConfig(billingAggregator: $billingAggregator){
    billingAggregator
    configs {
      type
      delayTime
    }
  }
}`;

export { billingAggregatorPollingConfigQuery };
