const basicFeeQuery = `
query basicFee($code: String!) {
    basicFee(code:  $code) {
        feeAmountPercentage
        feeAmountFixed
        customerTc
        customerTcInfo {
            channel
        }
        subsidiary
        subsidiaryAmount
        subsidiaryAmountPercentage
        debitGlNumber
        creditGlNumber,
        thirdPartyFee
    }
}
`;

export default basicFeeQuery;
