const getBillersQuery = `
query billerByBankCode($bankCode: String!) {
    getbillerByBankCode(bankCode: $bankCode){
      bankCode
      code
      name
      inquireFeeInfo
  }
}
`;

export { getBillersQuery };
