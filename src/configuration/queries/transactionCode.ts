const transactionCodeQuery = `
query transactionCode ($code: String!) {
    transactionCode(code: $code) {
        code
        ownershipCode
        transactionType
        defaultCategoryCode
        splitBill
        isExternal
        downloadReceipt
        allowedToDispute
        currency
        minAmountPerTx
        maxAmountPerTx
        limitGroupCode
        feeRules
        allowedToRepeat
        addToContact
        allowedToInstallment
        loyaltyPoint
        cashBack
        image
        notificationTemplate
        showInConsolidatedTrxHistory
        neutralizeSignage
        reversalCodeReference
        maxReversalDay
        channel
        awardGroupCounter
    }
}
`;

export default transactionCodeQuery;
