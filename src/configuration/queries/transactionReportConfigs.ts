const transactionReportConfigs = `
query{
  transactionReportConfigs{
    job
    accountNo
    paymentServiceCode
    periodOfReport
    type
    runningDate
    runningTime
    fileLocation
    referenceBank
    mailingList
    notificationTemplate
    preferedId
    additionalInformation4
  }
  
  fileUploadParameters{
    code
    prefixFileName
    location
    fileType
  }
}
`;

export default transactionReportConfigs;
