const currencyByCodeQuery = `        
  query currency($code:String!) {
    currency(code:$code) {
      code
      isoNumber
      name
    }
  }
`;

const currencyByIsoNumberQuery = `        
  query currencyByIsoNumber($isoNumber:String!) {
    currencyByIsoNumber(isoNumber:$isoNumber) {
      code
      isoNumber
      name
    }
  }
`;

export { currencyByCodeQuery, currencyByIsoNumberQuery };
