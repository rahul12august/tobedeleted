export const queryWorkingTimeTemplate = `
query getWorkingTimes($category: WorkingScheduleCategory!){
    workingTimes(category:$category) {
        dayName
        startTime
        endTime
    }
}
`;
