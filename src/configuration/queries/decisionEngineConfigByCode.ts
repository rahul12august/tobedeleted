const getDecisionEngineConfigByCodeQuery = `        
  query decisionEngineRules($code:String!) {
      decisionEngineRules(serviceCode: $code) {
        functionality
        description
        serviceCode
        rules {
          property
          operator
          value
          values
        }
      }
  }
`;

export default getDecisionEngineConfigByCodeQuery;
