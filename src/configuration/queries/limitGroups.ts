const getlimitGroupsQuery = `        
  query getAllLimitGroups {
    limitGroups {
      code
      dailyLimitAmount
    }
  }
`;

export default getlimitGroupsQuery;
