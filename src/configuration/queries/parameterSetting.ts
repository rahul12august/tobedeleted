const parameterSettingQuery = `
query parameterSettings($type: ParameterSettingType!) {
  parameterSettings(type: $type){
    code
    name
    value
  }
}
`;

export default parameterSettingQuery;
