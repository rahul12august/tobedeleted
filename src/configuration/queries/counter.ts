const updateCounterMutation = `
mutation updateCounter($counterInput: UpdateCounterInput!){
  updateCounter(updateCounterInput: $counterInput) {
    code
    value
    name
  }
}`;

const getCounterQuery = `
query getCounter($code: String!) {
    counter(code: $code) {
        code
      value
      name
    }
}`;

export { updateCounterMutation, getCounterQuery };
