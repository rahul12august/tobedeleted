const getNNSMappingQuery = `
query getNNSMappingByNNS ($reservedNnsPrefix: String!) {
    getNNSMappingByNNS(reservedNnsPrefix: $reservedNnsPrefix) {
        code
        institutionName
        brandName
        switchingName
        reservedNnsPrefix
    }
}
`;

export default getNNSMappingQuery;
