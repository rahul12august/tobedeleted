const bankCodeMappingQuery = `
query bankCodeMappingQuery($bankCodeId: String!) {
  bankCode(bankCodeId: $bankCodeId){
    id
    bankCodeId
    name
    rtolCode
    remittanceCode
    billerCode
    isBersamaMember
    isAltoMember
    firstPriority
    channel
    companyName
    type
    prefix
    isInternal
    irisCode
    supportedChannels
  }
}
`;

export default bankCodeMappingQuery;
