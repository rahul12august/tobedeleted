const mambuBlockingDayQuery = `
query getMambuBlockingDay($days: Float!) {
  mambuBlockingDay(days: $days) {
    code
    name
    days
  }
}
`;

export { mambuBlockingDayQuery };
