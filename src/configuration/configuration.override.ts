import { IPaymentConfigRule, ITransactionCodeInfo } from './configuration.type';
import logger from '../logger';

export interface ITxLimitOverrideStrategy<CONFIG_TYPE> {
  mapToEntitlementCode: (limitGroupCode: string) => string;
  overrideLimit: (
    customerId: string,
    config: CONFIG_TYPE,
    entitlementValue: number
  ) => void;
}

const minAmountPerTxOverrider: ITxLimitOverrideStrategy<ITransactionCodeInfo> = {
  mapToEntitlementCode(limitGroupCode: string): string {
    return `value.tx.${limitGroupCode}.min_amount`;
  },
  overrideLimit(
    customerId: string,
    config: ITransactionCodeInfo,
    entitlementValue: number
  ): void {
    config.minAmountPerTx = entitlementValue;
    logger.info(
      `Minimum amount per transaction has been changed to '${entitlementValue}', ` +
        `for transaction code '${config.code}', for customerId '${customerId}'`
    );
  }
};

const maxAmountPerTxOverrider: ITxLimitOverrideStrategy<ITransactionCodeInfo> = {
  mapToEntitlementCode(limitGroupCode: string): string {
    return `value.tx.${limitGroupCode}.max_amount`;
  },
  overrideLimit(
    customerId: string,
    config: ITransactionCodeInfo,
    entitlementValue: number
  ): void {
    config.maxAmountPerTx = entitlementValue;
    logger.info(
      `Maximum amount per transaction has been changed to '${entitlementValue}', ` +
        `for transaction code '${config.limitGroupCode}', for customerId '${customerId}'`
    );
  }
};

const paymentConfigRuleRangeFromOverrider: ITxLimitOverrideStrategy<IPaymentConfigRule> = {
  mapToEntitlementCode: minAmountPerTxOverrider.mapToEntitlementCode,
  overrideLimit(
    customerId: string,
    config: IPaymentConfigRule,
    entitlementValue: number
  ): void {
    config.amountRangeFrom = entitlementValue;
    logger.info(
      `range from amount has been changed to '${entitlementValue}' for payment config rule '${JSON.stringify(
        config
      )}', for customerId '${customerId}'`
    );
  }
};

const paymentConfigRuleRangeToOverrider: ITxLimitOverrideStrategy<IPaymentConfigRule> = {
  mapToEntitlementCode: maxAmountPerTxOverrider.mapToEntitlementCode,
  overrideLimit(
    customerId: string,
    config: IPaymentConfigRule,
    entitlementValue: number
  ): void {
    config.amountRangeTo = entitlementValue;
    logger.info(
      `range to amount has been changed to '${entitlementValue}' for payment config rule '${JSON.stringify(
        config
      )}', for customerId '${customerId}'`
    );
  }
};

const configurationOverride = {
  minAmountPerTxOverrider,
  maxAmountPerTxOverrider,
  paymentConfigRuleRangeFromOverrider,
  paymentConfigRuleRangeToOverrider
};

export default configurationOverride;
