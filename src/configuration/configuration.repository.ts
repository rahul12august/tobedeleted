import { get, isEmpty } from 'lodash';
import {
  BankNetworkEnum,
  Enum,
  WorkingScheduleCategory
} from '@dk/module-common';
import logger, { wrapLogs } from '../logger';
import httpClient from './httpClient';
import {
  Counter,
  IBankCode,
  IBasicFee,
  IBiller,
  ICurrency,
  IFileUploadParameter,
  IGeneralLedger,
  IHoliday,
  IInquiryBankInfoResult,
  ILimitGroup,
  IMambuBlockingDays,
  INNSMapping,
  IParameterSetting,
  IPaymentConfigRule,
  ISavingProduct,
  ITransactionCategory,
  ITransactionCode,
  ITransactionReportConfig,
  IUpdateCounterInput,
  WorkingTimeModel
} from './configuration.type';
import feeRuleQuery from './queries/feeRule';
import {
  getServiceRecommendationsQuery,
  paymentConfigRulesQuery
} from './queries/paymentConfigRules';
import limitGroupsQuery from './queries/limitGroups';
import transactionCategoryQuery from './queries/transactionCategory';
import bankCodeMappingQuery from './queries/bankCodeMapping';
import {
  currencyByCodeQuery,
  currencyByIsoNumberQuery
} from './queries/currency';
import holidaysByDateRangeQuery from './queries/holiday';
import decisionEngineConfigByCode from './queries/decisionEngineConfigByCode';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE, ErrorList } from '../common/errors';
import { billingAggregatorPollingConfigQuery } from './queries/billingAggregatorPollingConfig';
import { IBillingAggregatorPollingConfig } from '../common/common.type';
import { IDecisionEngineConfigRole } from '../decisionEngine/decisionEngine.type';
import transactionReportConfigs from './queries/transactionReportConfigs';
import { getBillersQuery } from './queries/billers';
import redis from '../common/redis';
import { mambuBlockingDayQuery } from './queries/mambuBlockingDays';
import { getCounterQuery, updateCounterMutation } from './queries/counter';
import { queryWorkingTimeTemplate } from './queries/workingTimeTemplate';
import { savingProductBaseQuery } from './queries/savingProduct';
import { IFeeRule } from '../fee/fee.type';
import basicFeeQuery from './queries/basicFee';
import transactionCodeQuery from './queries/transactionCode';
import { IErrorDetail } from '@dk/module-common/dist/error/error.interface';
import { standarizeRTOL } from './configuration.util';
import { bankByIncomingCodeQuery } from './queries/bankCodeIncomingCode';
import parameterSettingQuery from './queries/parameterSetting';
import generalLedgerQuery from './queries/generalLedger';
import getNNSMappingQuery from './queries/nnsMapping';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const pathOfConfiguration = '';
const paymentConfigRuleAllKey = '{ms-configuration}-paymentConfigRuleAll';
const billingAggregatorPrefix = 'ms-configuration';
const bankCodeIncomingKey = `{ms-configuration-bankCode}:getByIncomingCode-`;
const bankCodeMappingKey = `{ms-configuration-bankCode}:getByBankCodeIdOrPrefix-`;
const transactionCategoryKey = `{ms-configuration-transactionCategory}:`;
const currencyKey = `{ms-configuration-currency}:`;
const limitGroupAllKey = '{ms-configuration-limitGroup}:all?';

interface GQLlimitGroupByCodesResponse {
  data?: {
    limitGroups: ILimitGroup[];
  };
}
interface GQLResponse {
  data?: {
    feeRule: IFeeRule;
    bankCode: IBankCode;
    transactionCategory: ITransactionCategory;
    billingAggregatorPollingConfig: IBillingAggregatorPollingConfig[];
    decisionEngineRules: IDecisionEngineConfigRole[];
    transactionReportConfigs: ITransactionReportConfig[];
    fileUploadParameters: IFileUploadParameter[];
    getBankByIncomingCode: IBankCode;
    parameterSettings: IParameterSetting[];
    generalLedgers: IGeneralLedger[];
  };
}

interface GQLPaymentConfigRulesResponse {
  data?: {
    paymentConfigRules: IPaymentConfigRule[];
  };
}

interface GQLCurrencyByCodeQueryResponse {
  data?: {
    currency: ICurrency;
  };
}

interface GQLCurrencyByIsoNumberQueryResponse {
  data?: {
    currencyByIsoNumber: string;
  };
}

interface GQLHolidaysByDateRangeQueryResponse {
  data?: {
    holidays: IHoliday[];
  };
}

interface GQLWOrkingTimeResponse {
  data?: {
    workingTimes: WorkingTimeModel[];
  };
}
interface GQLGetBillersQueryResponse {
  data?: {
    getbillerByBankCode: IBiller;
  };
}

interface GQLGetMambuBlockingDayQueryResponse {
  data?: {
    mambuBlockingDay: IMambuBlockingDays;
  };
}

interface GQLGetCounterQueryResponse {
  data?: {
    updateCounter: Counter;
  };
}

interface GQLResponseCounter {
  data?: {
    counter?: Counter;
  };
}

interface GQLResponseBasicFee {
  data?: {
    basicFee?: IBasicFee;
  };
}

interface GQLResponseTransactionCode {
  data?: {
    transactionCode?: ITransactionCode;
  };
}

interface GQLResponseServiceRecommendations {
  data: {
    getServiceRecommendations: string[];
  };
}

interface GQLResponseSavingProduct {
  data?: {
    savingProduct?: ISavingProduct;
  };
}

interface GQLResponseNNSMapping {
  data?: {
    getNNSMappingByNNS?: INNSMapping;
  };
}

const throwGraphQLError = (apiName: string, errors: any[]): Promise<void> => {
  logger.error(
    `Error from API ${apiName} and errors are ${JSON.stringify(errors)}`
  );
  throw new TransferAppError(
    `Error from API ${apiName} and errors are ${JSON.stringify(errors)}!`,
    TransferFailureReasonActor.MS_TRANSFER,
    ERROR_CODE.INVALID_REQUEST,
    errors.map(error => ({
      code: ERROR_CODE.INVALID_REQUEST,
      key: error.path[0],
      message: error.message
    }))
  );
};

const getConfigurationCacheKey = (objectType: string, key: string): string =>
  `{ms-configuration-${objectType}}:${key}`;

const getFeeRule = async (
  feeRuleCode: string,
  monthlyNoTransaction?: number,
  interchange?: BankNetworkEnum,
  targetBankCode?: string,
  thresholdCounter?: string
): Promise<IFeeRule | undefined> => {
  const body = {
    query: feeRuleQuery,
    variables: {
      feeRuleInput: {
        code: feeRuleCode,
        monthlyNoTransaction,
        interchange,
        targetBankCode,
        thresholdCounter
      }
    }
  };
  logger.info(
    `getFeeRule : Payload details feeRuleCode : ${feeRuleCode}, monthlyNoTransaction : ${monthlyNoTransaction}, interchange : ${interchange}, targetBankCode : ${targetBankCode}`
  );
  const result = await httpClient.post<GQLResponse>('', body);
  const data = result.data.data;
  return data ? data.feeRule : undefined;
};

const getTransactionCategory = async (
  categoryCode: string
): Promise<ITransactionCategory | undefined> => {
  logger.info(
    `getTransactionCategory : Payload details categoryCode : ${categoryCode}`
  );
  const key = transactionCategoryKey + categoryCode;
  const cachedObject = await redis.get(key);
  if (cachedObject) {
    return cachedObject;
  }
  logger.info(
    `getTransactionCategory : Not available in cache fetching from configuration.`
  );
  const body = {
    query: transactionCategoryQuery,
    variables: {
      code: categoryCode
    }
  };
  const result = await httpClient.post<GQLResponse>(pathOfConfiguration, body);
  const data = result.data.data;
  return data ? data.transactionCategory : undefined;
};

const getBankByRtolCode = async (
  bankCode: string
): Promise<IBankCode | undefined> => {
  logger.info(`getBankByRtolCode : Payload details bankCode : ${bankCode}`);
  const key = bankCodeIncomingKey + standarizeRTOL(bankCode);
  const cachedObject = await redis.get(key);
  if (!isEmpty(cachedObject) && !isEmpty(cachedObject[0])) {
    return cachedObject[0];
  }
  logger.info(
    `getBankByRtolCode : Not available in cache fetching from configuration.`
  );
  const body = {
    query: bankByIncomingCodeQuery,
    variables: {
      incomingCode: standarizeRTOL(bankCode)
    }
  };
  const result = await httpClient.post<GQLResponse>(pathOfConfiguration, body);
  const data = result.data.data;
  return data ? data.getBankByIncomingCode : undefined;
};

const getBankCodeMapping = async (
  bankCode: string
): Promise<IBankCode | undefined> => {
  logger.info(`getBankCodeMapping : Payload details bankCode : ${bankCode}`);
  const key = bankCodeMappingKey + bankCode;
  const cachedObject = await redis.get(key);
  if (cachedObject) {
    return cachedObject;
  }
  logger.info(
    `getBankCodeMapping : Not available in cache fetching from configuration.`
  );
  const body = {
    query: bankCodeMappingQuery,
    variables: {
      bankCodeId: bankCode
    }
  };
  const result = await httpClient.post<GQLResponse>(pathOfConfiguration, body);
  const data = result.data.data;
  return data ? data.bankCode : undefined;
};

const getPaymentConfigRules = async (): Promise<
  IPaymentConfigRule[] | undefined
> => {
  logger.info(`getPaymentConfigRules : Fetching all config rules.`);
  const cachedObject = await redis.get(paymentConfigRuleAllKey);
  if (cachedObject) {
    return cachedObject;
  }
  logger.info(
    `getPaymentConfigRules : Not available in cache fetching from configuration.`
  );
  const body = {
    query: paymentConfigRulesQuery
  };
  const result = await httpClient.post<GQLPaymentConfigRulesResponse>('', body);
  const data = result.data.data;
  await redis.set(
    paymentConfigRuleAllKey,
    data ? data.paymentConfigRules : undefined,
    ''
  );

  return data ? data.paymentConfigRules : undefined;
};

const getLimitGroupsByCodes = async (): Promise<ILimitGroup[] | undefined> => {
  logger.info(`getLimitGroupsByCodes : Payload for limit group codes`);
  const cachedObjects = await redis.get(limitGroupAllKey);
  // Does cached objects exist each of them are truthy?
  if (cachedObjects) {
    return cachedObjects;
  }
  logger.info(
    `getLimitGroupsByCodes : Not available in cache fetching from configuration.`
  );
  const body = {
    query: limitGroupsQuery
  };
  const result = await httpClient.post<GQLlimitGroupByCodesResponse>('', body);
  const data = result.data.data;
  return data ? data.limitGroups : undefined;
};

const getCurrencyByIsoNumber = async (
  isoNumber: string
): Promise<string | undefined> => {
  logger.info(
    `getCodeCurrencyByIsoNumber : Payload details currency : ${isoNumber}`
  );
  const key = currencyKey + isoNumber;
  const cachedObject = await redis.get(key);
  if (cachedObject) {
    return cachedObject.code;
  }
  logger.info(
    `getCodeCurrencyByIsoNumber : Not available in cache fetching from configuration.`
  );
  const body = {
    query: currencyByIsoNumberQuery,
    variables: {
      isoNumber: isoNumber
    }
  };
  const result = await httpClient.post<GQLCurrencyByIsoNumberQueryResponse>(
    '',
    body
  );
  const data = result.data.data;
  return data ? data.currencyByIsoNumber : undefined;
};

const inquiryBankAndCurrencyByIncomingCodes = async (
  sourceBankCode: string,
  beneficiaryBankCode: string,
  isoNumber: string
): Promise<IInquiryBankInfoResult> => {
  logger.info(
    `inquiryBankAndCurrencyByIncomingCodes : Payload sourceBankCode ${sourceBankCode}, beneficiaryBankCode ${beneficiaryBankCode}, isoNumber ${isoNumber}`
  );
  let errors: IErrorDetail[] = [];
  let result;
  const [sourceBank, beneficiaryBank, currencyData] = await Promise.all([
    getBankByRtolCode(sourceBankCode),
    getBankByRtolCode(beneficiaryBankCode),
    getCurrencyByIsoNumber(isoNumber)
  ]);

  if (
    sourceBank &&
    !isEmpty(sourceBank) &&
    beneficiaryBank &&
    !isEmpty(beneficiaryBank) &&
    currencyData
  ) {
    result = {
      sourceBank,
      beneficiaryBank,
      currency: {
        code: currencyData
      }
    };
  } else {
    if (isEmpty(sourceBank)) {
      errors.push({
        code: ERROR_CODE.INVALID_REGISTER_PARAMETER,
        key: 'sourceBankCode',
        message: ErrorList[ERROR_CODE.INVALID_REGISTER_PARAMETER].message
      });
    }
    if (isEmpty(beneficiaryBank)) {
      errors.push({
        code: ERROR_CODE.INVALID_REGISTER_PARAMETER,
        key: 'beneficiaryBankCode',
        message: ErrorList[ERROR_CODE.INVALID_REGISTER_PARAMETER].message
      });
    }
    if (!currencyData) {
      errors.push({
        code: ERROR_CODE.INVALID_REGISTER_PARAMETER,
        key: 'currency',
        message: ErrorList[ERROR_CODE.INVALID_REGISTER_PARAMETER].message
      });
    }
    const detail = `Error while fetching bank information and currency!`;
    logger.error(`inquiryBankAndCurrencyByIncomingCodes: ${detail}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.JAGO,
      ERROR_CODE.INVALID_REQUEST,
      errors
    );
  }
  return result;
};

const getCurrencyByCode = async (
  currencyCode: string
): Promise<ICurrency | undefined> => {
  logger.info(`getCurrencyByCode : Payload details currency : ${currencyCode}`);
  const key = currencyKey + currencyCode;
  const cachedObject = await redis.get(key);
  if (cachedObject) {
    return cachedObject;
  }
  logger.info(
    `getCurrencyByCode : Not available in cache fetching from configuration.`
  );
  const body = {
    query: currencyByCodeQuery,
    variables: {
      code: currencyCode
    }
  };
  const result = await httpClient.post<GQLCurrencyByCodeQueryResponse>(
    '',
    body
  );
  const data = result.data.data;
  return data ? data.currency : undefined;
};

const getHolidaysByDateRange = async (
  fromDate: Date,
  toDate: Date
): Promise<IHoliday[]> => {
  logger.info(
    `getHolidaysByDateRange : Payload details fromDate : ${fromDate}, toDate : ${toDate}`
  );
  const body = {
    query: holidaysByDateRangeQuery,
    variables: {
      fromDate,
      toDate
    }
  };
  const response = await httpClient.post<GQLHolidaysByDateRangeQueryResponse>(
    '',
    body
  );
  const errors = get(response, ['data', 'errors'], []);
  if (errors.length > 0) {
    throwGraphQLError('getHolidaysByDateRange', errors);
  }
  return get(response, ['data', 'data', 'holidays'], []);
};

const getBillingAggregatorPollingConfig = async (
  billingAggregator: Enum.BillingAggregator
): Promise<IBillingAggregatorPollingConfig[] | undefined> => {
  logger.info(
    `getBillingAggregatorPollingConfig : Payload details billingAggregator : ${billingAggregator}`
  );
  const cachedObject = await redis.mGet(
    [billingAggregator.toString()],
    billingAggregatorPrefix,
    true
  );
  const billingAggregatorCachedObject = cachedObject && cachedObject[0];
  if (!isEmpty(billingAggregatorCachedObject)) {
    return billingAggregatorCachedObject;
  }
  const body = {
    query: billingAggregatorPollingConfigQuery,
    variables: {
      billingAggregator
    }
  };
  const { data } = await httpClient.post<GQLResponse>('', body);
  return data.data ? data.data.billingAggregatorPollingConfig : undefined;
};

const getDecisionEngineConfig = async (
  code: string
): Promise<IDecisionEngineConfigRole[] | undefined> => {
  logger.info(`getDecisionEngineConfig : Payload details code : ${code}`);
  const cachedObject = await redis.get(
    `{ms-configuration:decisionEngineRule}:${code}`
  );
  if (cachedObject) {
    let decisionRules: IDecisionEngineConfigRole[] = [];
    for (let data of cachedObject) {
      delete data._id;
      for (let i = 0; i < data.rules.length; i++) {
        if (!data.rules[i].value) {
          data.rules[i].value = null;
        }
        if (!data.rules[i].values) {
          data.rules[i].values = null;
        }
      }
      decisionRules.push(data);
    }
    if (decisionRules.length > 0) {
      return decisionRules;
    }
  }
  const body = {
    query: decisionEngineConfigByCode,
    variables: {
      code: code
    }
  };
  const result = await httpClient.post<GQLResponse>(pathOfConfiguration, body);
  const data = result.data.data;
  return data ? data.decisionEngineRules : undefined;
};

const getTransactionReportConfigsAndFileUpload = async (): Promise<
  [ITransactionReportConfig[], IFileUploadParameter[]] | undefined
> => {
  logger.info(
    `getTransactionReportConfigsAndFileUpload : Transaction report config and file upload fetching.`
  );
  const body = {
    query: transactionReportConfigs
  };
  const result = await httpClient.post<GQLResponse>(pathOfConfiguration, body);
  const data = result.data.data;
  return data
    ? [data.transactionReportConfigs, data.fileUploadParameters]
    : undefined;
};

/**
 * Get a biller identified by the given bank code.
 * @param bankCode
 */
const getBiller = async (bankCode: string): Promise<IBiller | undefined> => {
  logger.info(`getBiller : Fetching billers payload : ${bankCode}`);
  const cacheKey = getConfigurationCacheKey(
    'biller',
    `billerByBankCode-${bankCode}`
  );
  const cachedBiller = await redis.get(cacheKey);

  if (cachedBiller) {
    return cachedBiller;
  }
  logger.info(
    `getBiller : Not available in cache fetching from configuration.`
  );
  const requestBody = {
    query: getBillersQuery,
    variables: { bankCode: bankCode }
  };

  const result = await httpClient.post<GQLGetBillersQueryResponse>(
    pathOfConfiguration,
    requestBody
  );

  const biller = result.data.data?.getbillerByBankCode ?? undefined;

  return biller;
};

const getMambuBlockingCodeByDays = async (
  days: number
): Promise<IMambuBlockingDays | undefined> => {
  const requestBody = {
    query: mambuBlockingDayQuery,
    variables: { days }
  };

  const result = await httpClient.post<GQLGetMambuBlockingDayQueryResponse>(
    pathOfConfiguration,
    requestBody
  );
  const mambuBlockingDays = result.data.data?.mambuBlockingDay;
  return mambuBlockingDays;
};

const updateCounter = async (
  counterInput: IUpdateCounterInput
): Promise<IUpdateCounterInput | undefined> => {
  const requestBody = {
    query: updateCounterMutation,
    variables: { counterInput }
  };

  const result = await httpClient.post<GQLGetCounterQueryResponse>(
    pathOfConfiguration,
    requestBody
  );

  const counter = result.data.data?.updateCounter;
  return counter;
};

const getCounterByCode = async (code: string): Promise<Counter | undefined> => {
  const prefixKey = `ms-configuration-counter`;
  const cachedCounter = await redis.get(code, prefixKey);
  if (cachedCounter) {
    return cachedCounter;
  }
  const body = {
    query: getCounterQuery,
    variables: { code }
  };
  const result = await httpClient.post<GQLResponseCounter>('', body);
  const data = result.data.data;
  return data ? data.counter : undefined;
};

const getServiceRecommendations = async (
  refundable: boolean
): Promise<string[]> => {
  const body = {
    query: getServiceRecommendationsQuery,
    variables: { refundable }
  };
  const result = await httpClient.post<GQLResponseServiceRecommendations>(
    '',
    body
  );
  const data = result.data.data;
  return data ? data.getServiceRecommendations : [];
};

const getWorkingTime = async (
  category: WorkingScheduleCategory
): Promise<WorkingTimeModel[]> => {
  try {
    const body = {
      query: queryWorkingTimeTemplate,
      variables: { category }
    };
    const result = await httpClient.post<GQLWOrkingTimeResponse>('', body);
    const errors = get(result, ['data', 'errors'], []);
    if (errors.length > 0) {
      throwGraphQLError('getWorkingTime', errors);
    }
    return result && result.data.data ? result.data.data.workingTimes : [];
  } catch (err) {
    const detail = `Missing working time for category: ${category}`;
    logger.error(`${detail} with exception ${err}`);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.JAGO,
      ERROR_CODE.CANNOT_GET_CONFIG_PARAMETER
    );
  }
};

const getSavingProduct = async (
  code: string
): Promise<ISavingProduct | undefined> => {
  const prefixKey = `ms-configuration-savingProduct`;
  const cachedSavingProduct = await redis.get(code, prefixKey);
  if (cachedSavingProduct) {
    return cachedSavingProduct;
  }
  const body = {
    query: savingProductBaseQuery,
    variables: { code }
  };
  const result = await httpClient.post<GQLResponseSavingProduct>('', body);

  const data = result.data.data;

  return data ? data.savingProduct : undefined;
};

const getBasicFees = async (code: string): Promise<IBasicFee | undefined> => {
  const prefixKey = `ms-configuration-basicFee`;
  logger.info(`getBasicFees: fetching basic fee ${code}`);
  const cachedBasicFee = await redis.get(code, prefixKey);
  if (cachedBasicFee) {
    return cachedBasicFee;
  }
  logger.info(
    `getBasicFees: Not available in cache fetching from configuration.`
  );
  const body = {
    query: basicFeeQuery,
    variables: { code }
  };
  const result = await httpClient.post<GQLResponseBasicFee>('', body);
  const data = result.data.data;
  return data ? data.basicFee : undefined;
};

const getTransactionCode = async (
  code: string
): Promise<ITransactionCode | undefined> => {
  const prefixKey = `ms-configuration`;
  logger.info(`getTransactionCode: fetching Transaction code ${code}`);
  const cachedTransactionCode = await redis.get(code, prefixKey);
  if (cachedTransactionCode) {
    return cachedTransactionCode;
  }
  logger.info(
    `getTransactionCode: Not available in cache fetching from configuration.`
  );
  const body = {
    query: transactionCodeQuery,
    variables: { code }
  };
  const result = await httpClient.post<GQLResponseTransactionCode>('', body);
  const data = result.data.data;
  return data ? data.transactionCode : undefined;
};

const getParameterSettingByReferenceGroup = async (
  type: string
): Promise<IParameterSetting[]> => {
  const prefixKey = `ms-configuration-parameter-setting`;
  logger.info(`getParameterSetting: fetching Parameter setting ${type}`);
  const cachedParameterSetting = await redis.get(type, prefixKey);
  if (cachedParameterSetting) {
    return cachedParameterSetting;
  }
  logger.info(
    `getParameterSetting: Not available in cache fetching from configuration.`
  );
  const body = {
    query: parameterSettingQuery,
    variables: { type }
  };
  const result = await httpClient.post<GQLResponse>('', body);
  const data = result.data.data;
  return (data && data.parameterSettings) || [];
};

const getAllGeneralLedgers = async (): Promise<IGeneralLedger[]> => {
  const prefixKey = `{ms-configuration-generalLedger}:all?`;
  logger.info(`getGeneralLedgers: fetching General Ledgers`);
  const cachedGeneralLedgers = await redis.get(prefixKey);
  if (!isEmpty(cachedGeneralLedgers)) {
    return cachedGeneralLedgers;
  }
  logger.info(
    `getGeneralLedgers: Not available in cache fetching from configuration.`
  );
  const body = {
    query: generalLedgerQuery
  };
  const result = await httpClient.post<GQLResponse>('', body);
  const data = result.data.data;
  return (data && data.generalLedgers) || [];
};

const getNNSMapping = async (
  reservedNnsPrefix: string
): Promise<INNSMapping | undefined> => {
  const prefixKey = `ms-configuration-nnsMapping`;
  logger.info(
    `getNNSMapping: fetching NNS Mapping with nns ${reservedNnsPrefix}`
  );
  const cachedTransactionCode = await redis.get(reservedNnsPrefix, prefixKey);
  if (cachedTransactionCode) {
    return cachedTransactionCode;
  }
  logger.info(
    `getNNSMapping: Not available in cache fetching from configuration.`
  );
  const body = {
    query: getNNSMappingQuery,
    variables: { reservedNnsPrefix }
  };
  const result = await httpClient.post<GQLResponseNNSMapping>('', body);
  const data = result.data.data;
  return data ? data.getNNSMappingByNNS : undefined;
};

const configurationRepository = wrapLogs({
  getLimitGroupsByCodes,
  getFeeRule,
  getTransactionCategory,
  getBankByRtolCode,
  getBankCodeMapping,
  getPaymentConfigRules,
  inquiryBankAndCurrencyByIncomingCodes,
  getCurrencyByCode,
  getHolidaysByDateRange,
  getBillingAggregatorPollingConfig,
  getDecisionEngineConfig,
  getTransactionReportConfigsAndFileUpload,
  getBiller,
  getMambuBlockingCodeByDays,
  updateCounter,
  getCounterByCode,
  getServiceRecommendations,
  getWorkingTime,
  getSavingProduct,
  getBasicFees,
  getTransactionCode,
  getParameterSettingByReferenceGroup,
  getAllGeneralLedgers,
  getNNSMapping
});

export default configurationRepository;
