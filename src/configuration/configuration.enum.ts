export enum TransactionCodeType {
  DEBIT = 'DEBIT',
  CREDIT = 'CREDIT'
}
