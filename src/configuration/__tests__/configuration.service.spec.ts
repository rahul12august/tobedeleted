import { Enum, PaymentServiceTypeEnum } from '@dk/module-common';
import { isEmpty } from 'lodash';
import {
  getBillingAggregatorPollingConfig,
  getParameterSettingCardTransactionMockData,
  getPaymentConfigRulesData,
  getSavingProductConfig
} from '../__mocks__/configuration.data';
import configurationRepository from '../configuration.repository';
import configurationService from '../configuration.service';
import { IParameterSetting } from '../configuration.type';
import {
  BIFAST_REFERENCE_GROUP,
  DEBIT_CARD_TRANSACTION_GROUP
} from '../configuration.constant';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

jest.mock('../configuration.repository');
jest.mock('lodash');

describe('configuration.service', () => {
  describe('getGoBillsPollingConfig', () => {
    beforeEach(() => {
      expect.hasAssertions();
    });

    afterEach(() => {
      jest.resetAllMocks();
      jest.clearAllTimers();
    });

    it('should call getBillingAggregatorPollingConfig and get the empty data', async () => {
      //Given

      (configurationRepository.getBillingAggregatorPollingConfig as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (isEmpty as jest.Mock).mockReturnValue(true);

      // When
      const result = await configurationService.getGoBillsPollingConfig();

      //Then
      expect(
        configurationRepository.getBillingAggregatorPollingConfig
      ).toHaveBeenCalledWith(Enum.BillingAggregator.GOBILLS);
      expect(result.length).toEqual(0);
      expect(result).toEqual([]);
    });

    it('should call getBillingAggregatorPollingConfig and get the data', async () => {
      //Given
      const configs = getBillingAggregatorPollingConfig();

      (configurationRepository.getBillingAggregatorPollingConfig as jest.Mock).mockResolvedValueOnce(
        configs
      );
      (isEmpty as jest.Mock).mockReturnValueOnce(false);

      // When
      const result = await configurationService.getGoBillsPollingConfig();

      //Then
      expect(
        configurationRepository.getBillingAggregatorPollingConfig
      ).toHaveBeenCalledWith(Enum.BillingAggregator.GOBILLS);
      expect(result.length).toEqual(configs[0].configs.length);
      expect(result).toEqual(configs[0].configs);
    });
  });

  describe('getPaymentConfigRules', () => {
    beforeEach(() => {
      expect.hasAssertions();
    });

    afterEach(() => {
      jest.resetAllMocks();
      jest.clearAllTimers();
    });

    it('should get filtered array of paymentConfigRules when paymentServiceType is TRANSFER', async () => {
      // given
      const paymentConfigRules = getPaymentConfigRulesData();
      (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValueOnce(
        paymentConfigRules
      );

      // when
      const result = await configurationService.getPaymentConfigRules(
        PaymentServiceTypeEnum.TRANSFER
      );

      // then
      expect(result).toBeDefined();
      expect(result?.length).toEqual(18);
      expect(result?.[0]).toEqual(
        paymentConfigRules.find(
          x => x.paymentServiceType === PaymentServiceTypeEnum.TRANSFER
        )
      );
    });

    it('should get array of paymentConfigRules when paymentServiceType is undefined', async () => {
      // given
      const paymentConfigRules = getPaymentConfigRulesData();
      (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValueOnce(
        paymentConfigRules
      );

      // when
      const result = await configurationService.getPaymentConfigRules();

      // then
      expect(result).toBeDefined();
      expect(result?.length).toEqual(33);
      expect(result?.[0]).toEqual(paymentConfigRules[0]);
    });
    it('should get array of paymentConfigRules as undefined when paymentServiceType is TRANSFER', async () => {
      //Given
      (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValueOnce(
        undefined
      );

      // When
      const result = await configurationService.getPaymentConfigRules(
        PaymentServiceTypeEnum.TRANSFER
      );

      //Then
      expect(result).toBeDefined();
      expect(result?.length).toEqual(0);
    });

    it('should get array of paymentConfigRules as undefined when paymentServiceType is undefined', async () => {
      //Given
      (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValueOnce(
        undefined
      );

      // When
      const result = await configurationService.getPaymentConfigRules();

      //Then
      expect(result).toBeUndefined();
    });

    it('should get filtered array of paymentConfigRules when paymentServiceType is TRANSFER & paymentServiceCode is SKN', async () => {
      // given
      const paymentConfigRules = getPaymentConfigRulesData();
      (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValueOnce(
        paymentConfigRules
      );

      // when
      const result = await configurationService.getPaymentConfigRules(
        PaymentServiceTypeEnum.TRANSFER,
        'SKN'
      );

      // then
      expect(result).toBeDefined();
      expect(result?.length).toEqual(3);
      expect(result?.[0]).toEqual(
        paymentConfigRules.find(
          x =>
            x.paymentServiceType === PaymentServiceTypeEnum.TRANSFER &&
            x.serviceRecommendation.includes('SKN')
        )
      );
    });
    it('should get filtered array of paymentConfigRules when paymentServiceType is Undefined & paymentServiceCode is RTGS', async () => {
      //Given
      const paymentConfigRules = getPaymentConfigRulesData();
      (configurationRepository.getPaymentConfigRules as jest.Mock).mockResolvedValueOnce(
        paymentConfigRules
      );

      // When
      const result = await configurationService.getPaymentConfigRules(
        undefined,
        'RTGS'
      );

      //Then
      expect(result).toBeDefined();
      expect(result?.length).toEqual(3);
      expect(result?.[0]).toEqual(
        paymentConfigRules.find(x => x.serviceRecommendation.includes('RTGS'))
      );
    });
  });

  describe('isBiFastEnabled', () => {
    const parameterSetting: IParameterSetting = {
      code: 'BIFAST_ENABLED',
      name: 'BI Fast enable flag',
      value: 'true'
    };

    beforeEach(() => {
      expect.hasAssertions();
    });

    afterEach(() => {
      jest.resetAllMocks();
      jest.clearAllTimers();
    });

    it('should return true if BIFAST is enabled', async () => {
      // Given
      const parameterSettingsResponse: IParameterSetting[] = [parameterSetting];
      (configurationRepository.getParameterSettingByReferenceGroup as jest.Mock).mockResolvedValueOnce(
        parameterSettingsResponse
      );

      // When
      const result = await configurationService.isBiFastEnabled();

      //Then
      expect(
        configurationRepository.getParameterSettingByReferenceGroup
      ).toBeCalledWith(BIFAST_REFERENCE_GROUP);
      expect(result).toEqual(true);
    });

    it('should return false if BIFAST is disabled', async () => {
      // Given
      const parameterSettingsResponse: IParameterSetting[] = [
        { ...parameterSetting, value: 'false' }
      ];
      (configurationRepository.getParameterSettingByReferenceGroup as jest.Mock).mockResolvedValueOnce(
        parameterSettingsResponse
      );
      // When
      const result = await configurationService.isBiFastEnabled();

      //Then
      expect(
        configurationRepository.getParameterSettingByReferenceGroup
      ).toBeCalledWith(BIFAST_REFERENCE_GROUP);
      expect(result).toEqual(false);
    });
  });

  describe('isDebitCardTransactionGroup', () => {
    const parameterSetting: IParameterSetting[] = getParameterSettingCardTransactionMockData();

    beforeEach(() => {
      expect.hasAssertions();
    });

    afterEach(() => {
      jest.resetAllMocks();
      jest.clearAllTimers();
    });

    it('should return true if paymentServiceType is included in parameter setting response', async () => {
      // Given
      const parameterSettingsResponse: IParameterSetting[] = parameterSetting;
      const paymentServiceType = PaymentServiceTypeEnum.INTERNATIONAL_PAYMENT;
      (configurationRepository.getParameterSettingByReferenceGroup as jest.Mock).mockResolvedValueOnce(
        parameterSettingsResponse
      );

      // When
      const result = await configurationService.isDebitCardTransactionGroup(
        paymentServiceType
      );

      //Then
      expect(
        configurationRepository.getParameterSettingByReferenceGroup
      ).toBeCalledWith(DEBIT_CARD_TRANSACTION_GROUP);
      expect(result).toEqual(true);
    });

    it('should return false if if paymentServiceType is included in parameter setting response', async () => {
      // Given
      const parameterSettingsResponse: IParameterSetting[] = parameterSetting;
      const paymentServiceType = PaymentServiceTypeEnum.JAGOPAY_REFUND;
      (configurationRepository.getParameterSettingByReferenceGroup as jest.Mock).mockResolvedValueOnce(
        parameterSettingsResponse
      );

      // When
      const result = await configurationService.isDebitCardTransactionGroup(
        paymentServiceType
      );

      //Then
      expect(
        configurationRepository.getParameterSettingByReferenceGroup
      ).toBeCalledWith(DEBIT_CARD_TRANSACTION_GROUP);
      expect(result).toEqual(false);
    });
  });
  describe('getSavingProduct', () => {
    beforeEach(() => {
      expect.hasAssertions();
    });

    afterEach(() => {
      jest.resetAllMocks();
      jest.clearAllTimers();
    });

    it('should call getSavingProduct and throw error on the empty data', async () => {
      // given
      (configurationRepository.getSavingProduct as jest.Mock).mockResolvedValueOnce(
        undefined
      );
      (isEmpty as jest.Mock).mockReturnValue(true);

      // when
      const error = await configurationService
        .getSavingProduct('MA')
        .catch(err => err);

      // then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.actor).toEqual(TransferFailureReasonActor.JAGO);
      expect(error.detail).toEqual('Saving product not found for: MA!');
      expect(error.errorCode).toEqual(ERROR_CODE.SAVING_PRODUCT_NOT_FOUND);
    });

    it('should call getSavingProductConfig and get the data', async () => {
      // given
      const configs = getSavingProductConfig();

      (configurationRepository.getSavingProduct as jest.Mock).mockResolvedValueOnce(
        configs
      );
      (isEmpty as jest.Mock).mockReturnValueOnce(false);

      // when
      const result = await configurationService.getSavingProduct('MA');

      // then
      expect(configurationRepository.getSavingProduct).toHaveBeenCalledWith(
        'MA'
      );
      // @ts-ignore
      expect(result.length).toEqual(configs.length);
    });
  });
});
