import { Enum, WorkingScheduleCategory } from '@dk/module-common';
import httpClient from '../httpClient';
import feeRuleQuery from '../queries/feeRule';
import limitGroupsQuery from '../queries/limitGroups';
import configurationRepository from '../configuration.repository';
import transactionCategoryQuery from '../queries/transactionCategory';
import bankCodeMappingQuery from '../queries/bankCodeMapping';
import getNNSMappingQuery from '../queries/nnsMapping';
import {
  getBankCodeData,
  getBillingAggregatorPollingConfig,
  getBillingAggregatorPollingConfigErrorResponse,
  getCounterData,
  getCurrencyData,
  getDecisionEngineConfiguration,
  getDecisionEngineConfigurationExpected,
  getDecisionEngineConfigurationRedis,
  getGeneralLedgersMockData,
  getHolidaysWithDateRangeData,
  getLimitGroupRedis,
  getLimitGroupsData,
  getMambuBlockingDays,
  getPaymentConfigRulesData,
  getServiceReccommendation,
  getTransactionReportConfigsAndFileUploadParameters
} from '../__mocks__/configuration.data';
import { ERROR_CODE } from '../../common/errors';
import holidaysByDateRangeQuery from '../queries/holiday';
import { currencyByCodeQuery } from '../queries/currency';
import { billingAggregatorPollingConfigQuery } from '../queries/billingAggregatorPollingConfig';
import { TransferAppError } from '../../errors/AppError';
import decisionEngineConfigByCode from '../queries/decisionEngineConfigByCode';
import transactionReportConfigs from '../queries/transactionReportConfigs';
import redis from '../../common/redis';
import { IBiller } from '../configuration.type';
import { getBillersQuery } from '../queries/billers';
import { mambuBlockingDayQuery } from '../queries/mambuBlockingDays';
import { getCounterQuery, updateCounterMutation } from '../queries/counter';
import { getServiceRecommendationsQuery } from '../queries/paymentConfigRules';
import { queryWorkingTimeTemplate } from '../queries/workingTimeTemplate';
import { savingProductBaseQuery } from '../queries/savingProduct';
import {
  getBasicFeeData,
  getTransactionCode
} from '../../fee/__mocks__/fee.data';
import basicFeeQuery from '../queries/basicFee';
import transactionCodeQuery from '../queries/transactionCode';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

jest.mock('../httpClient');

describe('configuration.repository', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('getFeeRule', () => {
    it('should get Fee Rule data correctly if found', async () => {
      // Given
      const code = 'RTOL_TRANSFER_ON_OTHER_ATM_FEE_RULE';
      const thresholdCounter = 'AWARD_TRANSFER_001';
      const body = {
        query: feeRuleQuery,
        variables: {
          feeRuleInput: {
            code,
            interchange: undefined,
            monthlyNoTransaction: undefined,
            targetBankCode: undefined,
            thresholdCounter
          }
        }
      };
      const expectedFeeRule = {
        code,
        thresholdCounter
      };
      const response = { data: { data: { feeRule: expectedFeeRule } } };
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const actualFeeRule = await configurationRepository.getFeeRule(
        code,
        undefined,
        undefined,
        undefined,
        thresholdCounter
      );

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(actualFeeRule).toBe(expectedFeeRule);
    });

    it('should return undefined when feeRule not found', async () => {
      // Given
      const code = 'RTOL_TRANSFER_ON_OTHER_ATM_FEE_RULE';
      const response = { data: {} };
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const actualFeeRule = await configurationRepository.getFeeRule(code);

      // Then
      expect(actualFeeRule).toBeUndefined();
    });
  });

  describe('bankCodeMapping query', () => {
    it('should get bankCode mapping data correctly', async () => {
      // Given
      const bankCode = 'BC002';
      const body = {
        query: bankCodeMappingQuery,
        variables: {
          bankCodeId: bankCode
        }
      };
      const expectedBankCode = { bankCodeId: bankCode };
      const response = {
        data: {
          data: {
            bankCode: expectedBankCode
          }
        }
      };
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const actual = await configurationRepository.getBankCodeMapping(bankCode);

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(actual).toEqual(expectedBankCode);
    });

    it('should get bankCode mapping data from redis', async () => {
      // Given
      const bankCode = 'BC002';
      const expectedBankCode = { bankCodeId: bankCode };

      (redis.get as jest.Mock).mockResolvedValueOnce(expectedBankCode);

      // When
      const actual = await configurationRepository.getBankCodeMapping(bankCode);

      // Then
      expect(redis.get).toHaveBeenCalledTimes(1);
      expect(httpClient.post).not.toHaveBeenCalled();
      expect(actual).toEqual(expectedBankCode);
    });

    it('should return undefined when bankCode not found', async () => {
      // Given
      const bankCode = 'BC00000';
      const expectedTransactionCategories = [{ code: 'C001' }];
      const response = {
        data: { transactionCategories: expectedTransactionCategories }
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const actual = await configurationRepository.getBankCodeMapping(bankCode);

      // Then
      expect(actual).toBeUndefined();
    });
  });

  describe('transactionCategory query', () => {
    it('should get data correctly', async () => {
      // Given
      const code = 'C001';
      const body = {
        query: transactionCategoryQuery,
        variables: {
          code
        }
      };
      const expectedData = { code };
      const response = {
        data: {
          data: {
            transactionCategory: expectedData
          }
        }
      };
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const actual = await configurationRepository.getTransactionCategory(code);

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(actual).toEqual(expectedData);
    });

    it('should get data correctly by redis without hitting configuration', async () => {
      // Given
      const code = 'C001';
      const expectedData = { code };
      (redis.get as jest.Mock).mockResolvedValueOnce(expectedData);

      // When
      const actual = await configurationRepository.getTransactionCategory(code);

      // Then
      expect(httpClient.post).not.toHaveBeenCalled();
      expect(actual).toEqual(expectedData);
    });

    it('should return undefined if data is not found', async () => {
      // Given
      const code = 'C001';
      const response = {
        data: {}
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const actual = await configurationRepository.getTransactionCategory(code);

      // Then
      expect(actual).toBeUndefined();
    });
  });

  describe('paymentConfigRule.repository', () => {
    describe('getpaymentConfigRules', () => {
      it('should return data if found', async () => {
        // given
        const data = getPaymentConfigRulesData();
        const response = {
          data: {
            data: {
              paymentConfigRules: data
            }
          }
        };

        // when
        (redis.get as jest.Mock).mockResolvedValueOnce(data);
        (httpClient.post as jest.Mock).mockResolvedValueOnce(response);
        const expected = await configurationRepository.getPaymentConfigRules();

        // then
        expect(expected).toEqual(data);
        expect(redis.get).toBeCalled();
      });
      it('should return data if found when cache is empty', async () => {
        // given
        const data = getPaymentConfigRulesData();
        const response = {
          data: {
            data: {
              paymentConfigRules: data
            }
          }
        };

        // when
        (redis.get as jest.Mock).mockResolvedValueOnce(undefined);
        (httpClient.post as jest.Mock).mockResolvedValueOnce(response);
        const expected = await configurationRepository.getPaymentConfigRules();

        // given
        expect(redis.get).toBeCalled();
        expect(expected).toEqual(data);
      });

      it('should return undefined if not found', async () => {
        // given
        const response = { data: {} };
        (redis.get as jest.Mock).mockResolvedValueOnce(undefined);
        (redis.set as jest.Mock).mockResolvedValue(undefined);
        (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

        // when
        const expected = await configurationRepository.getPaymentConfigRules();

        // then
        expect(redis.set).toHaveBeenCalled();
        expect(expected).toBeUndefined();
      });
    });
  });

  describe('getLimitGroupsByCodes', () => {
    it('should return data if found', async () => {
      // given
      const expected = getLimitGroupsData();
      const cacheDate = getLimitGroupRedis;

      // when
      (redis.get as jest.Mock).mockResolvedValueOnce(cacheDate);

      const data = await configurationRepository.getLimitGroupsByCodes();

      // then
      expect(redis.get).toBeCalled();
      expect(data).toEqual(
        expect.arrayContaining(
          expected.map(data => expect.objectContaining(data))
        )
      );
    });

    it('should return data if found when cache is empty', async () => {
      // given
      const expected = getLimitGroupsData();
      const body = {
        query: limitGroupsQuery
      };
      const response = { data: { data: { limitGroups: expected } } };

      // when
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);
      (redis.get as jest.Mock).mockResolvedValueOnce(undefined);
      const data = await configurationRepository.getLimitGroupsByCodes();

      // then
      expect(redis.get).toBeCalled();
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(data).toEqual(
        expect.arrayContaining(
          expected.map(data => expect.objectContaining(data))
        )
      );
    });

    it('should return undefined if not found', async () => {
      // given
      const body = {
        query: limitGroupsQuery
      };
      const response = { data: {} };

      // when
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);
      (redis.get as jest.Mock).mockResolvedValueOnce(undefined);

      // then
      const data = await configurationRepository.getLimitGroupsByCodes();
      expect(redis.get).toBeCalled();
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(data).toBeUndefined();
    });
  });

  describe('inquiry BankInfo By IncomingCodes', () => {
    it('should call to ms-configuration', async () => {
      const sourceBankCode = '0123';
      const beneficiaryBankCode = '011';
      const isoNumber = '460';

      const sourceResponse = {
        data: {
          data: {
            getBankByIncomingCode: getBankCodeData()
          }
        }
      };
      const beneficiaryResponse = {
        data: {
          data: {
            getBankByIncomingCode: getBankCodeData()
          }
        }
      };
      const currencyrResponse = {
        data: {
          data: {
            currencyByIsoNumber: getCurrencyData().isoNumber
          }
        }
      };
      const expectedBody = {
        sourceBank: sourceResponse.data.data.getBankByIncomingCode,
        beneficiaryBank: beneficiaryResponse.data.data.getBankByIncomingCode,
        currency: {
          code: currencyrResponse.data.data.currencyByIsoNumber
        }
      };
      (redis.get as jest.Mock)
        .mockResolvedValueOnce([sourceResponse.data.data.getBankByIncomingCode])
        .mockResolvedValueOnce(null)
        .mockResolvedValueOnce(null);
      (httpClient.post as jest.Mock)
        .mockResolvedValueOnce(beneficiaryResponse)
        .mockResolvedValueOnce(currencyrResponse);

      const result = await configurationRepository.inquiryBankAndCurrencyByIncomingCodes(
        sourceBankCode,
        beneficiaryBankCode,
        isoNumber
      );
      expect(redis.get).toHaveBeenCalledTimes(3);
      expect(httpClient.post).toHaveBeenCalledTimes(2);
      expect(result).toEqual(expectedBody);
    });

    it('should get data from redis', async () => {
      const sourceBankCode = '0123';
      const beneficiaryBankCode = '011';
      const isoNumber = '460';

      const bankCodeData = getBankCodeData();
      const currencyrData = getCurrencyData();

      const expectedBody = {
        sourceBank: bankCodeData,
        beneficiaryBank: bankCodeData,
        currency: {
          code: currencyrData.code
        }
      };
      (redis.get as jest.Mock)
        .mockResolvedValueOnce([bankCodeData])
        .mockResolvedValueOnce([bankCodeData])
        .mockResolvedValueOnce(currencyrData);

      const result = await configurationRepository.inquiryBankAndCurrencyByIncomingCodes(
        sourceBankCode,
        beneficiaryBankCode,
        isoNumber
      );
      expect(redis.get).toHaveBeenCalledTimes(3);
      expect(httpClient.post).toHaveBeenCalledTimes(0);
      expect(result).toEqual(expectedBody);
    });

    it('should throw error when bankcode and currency not found', async () => {
      const sourceBankCode = '0123';
      const beneficiaryBankCode = '011';
      const isoNumber = '460';
      (redis.get as jest.Mock)
        .mockResolvedValueOnce(null)
        .mockResolvedValueOnce(null)
        .mockResolvedValueOnce(null);
      (httpClient.post as jest.Mock)
        .mockResolvedValueOnce({ data: { data: undefined } })
        .mockResolvedValueOnce({ data: { data: undefined } })
        .mockResolvedValueOnce({ data: { data: undefined } });

      const error = await configurationRepository
        .inquiryBankAndCurrencyByIncomingCodes(
          sourceBankCode,
          beneficiaryBankCode,
          isoNumber
        )
        .catch(e => e);

      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
      expect(error.errors.length).toEqual(3);
      expect(httpClient.post).toHaveBeenCalledTimes(3);
      expect(redis.get).toHaveBeenCalledTimes(3);
    });
  });

  describe('inquiry get currency', () => {
    it('should call to ms-configuration and get currency data', async () => {
      const currencyCode = 'USD';

      const expectedBody = {
        query: currencyByCodeQuery,
        variables: {
          code: currencyCode
        }
      };

      const response = {
        data: {
          data: {
            currency: getCurrencyData()
          }
        }
      };
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      const expected = await configurationRepository.getCurrencyByCode(
        currencyCode
      );
      expect(httpClient.post).toHaveBeenCalledWith('', expectedBody);
      expect(expected).toBe(response.data.data.currency);
    });

    it('should not call to ms-configuration and get currency data', async () => {
      const currencyCode = 'USD';
      const response = getCurrencyData();
      (redis.get as jest.Mock).mockResolvedValueOnce(response);

      const expected = await configurationRepository.getCurrencyByCode(
        currencyCode
      );
      expect(httpClient.post).not.toHaveBeenCalled();
      expect(expected).toBe(response);
    });
  });

  describe('get holidays for specific date range', () => {
    it('should get Holiday list data correctly if found', async () => {
      // Given
      const holidaysData = getHolidaysWithDateRangeData();
      const fromDate = new Date('2020-01-01T00:00:00.000Z'),
        toDate = new Date('2020-01-20T00:00:00.000Z');
      const body = {
        query: holidaysByDateRangeQuery,
        variables: { fromDate, toDate }
      };
      const response = {
        data: { data: { holidays: holidaysData } }
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const holidays = await configurationRepository.getHolidaysByDateRange(
        fromDate,
        toDate
      );

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(holidays).toEqual(holidaysData);
    });

    it('should return empty array when holidays not found in given range', async () => {
      // Given
      const fromDate = new Date('2020-01-01T00:00:00.000Z'),
        toDate = new Date('2020-01-20T00:00:00.000Z');
      const body = {
        query: holidaysByDateRangeQuery,
        variables: { fromDate, toDate }
      };
      const response = {
        data: { data: { holidays: [] } }
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const holidays = await configurationRepository.getHolidaysByDateRange(
        fromDate,
        toDate
      );

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(holidays).toEqual([]);
    });

    it('should return error when holidays api thorws error, access denied', async () => {
      // Given
      const fromDate = new Date('2020-01-01T00:00:00.000Z'),
        toDate = new Date('2020-01-20T00:00:00.000Z');
      const body = {
        query: holidaysByDateRangeQuery,
        variables: { fromDate, toDate }
      };

      const response = {
        data: {
          errors: [
            {
              message: 'Access denied!',
              path: ['holiday']
            }
          ]
        }
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // Whe
      let error: any;
      try {
        await configurationRepository.getHolidaysByDateRange(fromDate, toDate);
      } catch (err) {
        error = err;
      }
      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(error).toBeDefined();
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REQUEST);
      expect(error.errors[0].message).toEqual('Access denied!');
      expect(error.errors[0].key).toEqual('holiday');
    });
  });

  describe('getBillingAggregatorPollingConfig', () => {
    it('should get billing aggregator polling configs', async () => {
      // Given
      const configs = getBillingAggregatorPollingConfig();
      const body = {
        query: billingAggregatorPollingConfigQuery,
        variables: { billingAggregator: Enum.BillingAggregator.GOBILLS }
      };
      const response = {
        data: { data: { billingAggregatorPollingConfig: configs } }
      };
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const result = await configurationRepository.getBillingAggregatorPollingConfig(
        Enum.BillingAggregator.GOBILLS
      );

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(result).toEqual(configs);
    });

    it('should get billing aggregator polling configs from cache', async () => {
      // Given
      const configs = getBillingAggregatorPollingConfig();

      (redis.mGet as jest.Mock).mockResolvedValueOnce([configs]);

      // When
      const result = await configurationRepository.getBillingAggregatorPollingConfig(
        Enum.BillingAggregator.GOBILLS
      );

      // Then
      expect(httpClient.post).not.toHaveBeenCalled();
      expect(redis.mGet).toHaveBeenCalledWith(
        ['GoBills'],
        'ms-configuration',
        true
      );
      expect(result).toEqual(configs);
    });

    it('should return undefined when api throws an error', async () => {
      // Given
      const errorResponse = getBillingAggregatorPollingConfigErrorResponse();
      const body = {
        query: billingAggregatorPollingConfigQuery,
        variables: { billingAggregator: Enum.BillingAggregator.GOBILLS }
      };
      const response = {
        data: { errorResponse }
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const result = await configurationRepository.getBillingAggregatorPollingConfig(
        Enum.BillingAggregator.GOBILLS
      );

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(result).toBeUndefined();
    });

    it('should throw error when exception is there', async () => {
      // given
      const body = {
        query: billingAggregatorPollingConfigQuery,
        variables: { billingAggregator: Enum.BillingAggregator.GOBILLS }
      };

      (httpClient.post as jest.Mock).mockRejectedValueOnce(
        new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.UNEXPECTED_ERROR
        )
      );

      // when
      const error = await configurationRepository
        .getBillingAggregatorPollingConfig(Enum.BillingAggregator.GOBILLS)
        .catch(error => error);

      // then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(error).toBeDefined();
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
      expect(error.detail).toEqual('Test error!');
      expect(error.errorCode).toEqual(ERROR_CODE.UNEXPECTED_ERROR);
    });
  });

  describe('getDecisionEngineConfig query', () => {
    it('get data from cache', async () => {
      // Given
      const code = 'ST001';
      const redisData = getDecisionEngineConfigurationRedis;
      (redis.get as jest.Mock).mockResolvedValueOnce(redisData);
      const expectedData = getDecisionEngineConfigurationExpected;
      const response = {
        data: {
          data: {
            decisionEngineRules: expectedData
          }
        }
      };
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const actual = await configurationRepository.getDecisionEngineConfig(
        code
      );

      // Then
      expect(redis.get).toBeCalled();
      expect(expectedData).toEqual(actual);
    });
    it('should get data correctly', async () => {
      // Given
      const code = 'ST001';
      const body = {
        query: decisionEngineConfigByCode,
        variables: {
          code
        }
      };
      const expectedData = [getDecisionEngineConfiguration()];
      const response = {
        data: {
          data: {
            decisionEngineRules: expectedData
          }
        }
      };
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const actual = await configurationRepository.getDecisionEngineConfig(
        code
      );

      // Then
      expect(redis.get).toBeCalled();
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(actual).toEqual(expectedData);
    });

    it('should return undefined if data is not found', async () => {
      // Given
      const code = 'ST001';
      const response = {
        data: {}
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const actual = await configurationRepository.getDecisionEngineConfig(
        code
      );

      // Then
      expect(redis.get).toBeCalled();
      expect(actual).toBeUndefined();
    });
  });

  describe('getTransactionReportConfigsAndFileUpload query', () => {
    it('should return all transaction report configs and file upload parameters', async () => {
      // Given
      const body = {
        query: transactionReportConfigs
      };
      const configData = getTransactionReportConfigsAndFileUploadParameters();
      const response = {
        data: {
          data: {
            ...configData
          }
        }
      };
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const actual = await configurationRepository.getTransactionReportConfigsAndFileUpload();

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(actual).toEqual([
        configData.transactionReportConfigs,
        configData.fileUploadParameters
      ]);
    });

    it('should return undefined if data is not found', async () => {
      // Given
      const response = {
        data: {}
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const actual = await configurationRepository.getTransactionReportConfigsAndFileUpload();

      // Then
      expect(actual).toBeUndefined();
    });
  });

  describe('getBiller', () => {
    it('should not fetch biller from remote repository if available in cache', async () => {
      // Given
      const biller = {
        bankCode: 'BC160',
        code: 'PDAM2'
      };

      (redis.get as jest.Mock).mockResolvedValueOnce(biller);

      // When
      const result = await configurationRepository.getBiller('BC160');

      expect(redis.get).toHaveBeenCalledWith(
        `{ms-configuration-biller}:billerByBankCode-BC160`
      );
      expect(httpClient.post).not.toHaveBeenCalled();
      expect(result).toBe(biller);
    });

    it('should fetch biller from remote repository if no cache is available', async () => {
      // Given
      const biller: IBiller = {
        bankCode: 'BC160',
        code: 'PDAM2',
        name: 'Aerta',
        inquireFeeInfo: true
      };

      (redis.get as jest.Mock).mockResolvedValueOnce(undefined);
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          data: {
            getbillerByBankCode: biller
          }
        }
      });

      // When
      const result = await configurationRepository.getBiller('BC160');

      // Then
      expect(redis.get).toHaveBeenCalled();
      expect(httpClient.post).toHaveBeenCalledWith(expect.anything(), {
        query: getBillersQuery,
        variables: { bankCode: 'BC160' }
      });
      expect(result).toBe(biller);
    });

    it('should return nothing when no cache is available and the remote repo returns nothing', async () => {
      // Given
      (redis.get as jest.Mock).mockResolvedValueOnce(undefined);
      (httpClient.post as jest.Mock).mockResolvedValueOnce({
        data: {
          data: {
            billers: []
          }
        }
      });

      // When
      const result = await configurationRepository.getBiller('BC160');

      // Then
      expect(redis.get).toHaveBeenCalled();
      expect(httpClient.post).toHaveBeenCalled();
      expect(result).toBeUndefined();
    });
  });

  describe('get mambu blocking code by days', () => {
    it('should get mambu blocking code by days', async () => {
      // Given
      const blockingDaysData = getMambuBlockingDays();
      const days = 4;
      const body = {
        query: mambuBlockingDayQuery,
        variables: { days }
      };
      const response = {
        data: { data: { mambuBlockingDay: blockingDaysData } }
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const holidays = await configurationRepository.getMambuBlockingCodeByDays(
        days
      );

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(holidays).toEqual(blockingDaysData);
    });

    it('should get undefined when data not found', async () => {
      // Given
      const days = 4;
      const body = {
        query: mambuBlockingDayQuery,
        variables: { days }
      };
      const response = {
        data: { data: {} }
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const holidays = await configurationRepository.getMambuBlockingCodeByDays(
        days
      );

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(holidays).toBeUndefined();
    });
  });

  describe('update mambu blocking days in counter', () => {
    it('should update Counter', async () => {
      // Given
      const counterData = getCounterData();
      const input = { code: 'counter', value: 4 };
      const body = {
        query: updateCounterMutation,
        variables: { counterInput: input }
      };
      const response = {
        data: { data: { updateCounter: counterData } }
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const counter = await configurationRepository.updateCounter(input);

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(counter).toEqual(counterData);
    });

    it('should return undefined when counter not found', async () => {
      //Given
      const input = { code: 'counter', value: 4 };
      const body = {
        query: updateCounterMutation,
        variables: { counterInput: input }
      };
      const response = {
        data: { data: {} }
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const counter = await configurationRepository.updateCounter(input);

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(counter).toBeUndefined();
    });
  });

  describe('getCounterByCode', () => {
    it('should get Counter', async () => {
      // Given
      const counterData = getCounterData();
      const body = {
        query: getCounterQuery,
        variables: { code: 'counter' }
      };
      const response = {
        data: { data: { counter: counterData } }
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const counter = await configurationRepository.getCounterByCode('counter');

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(counter).toEqual(counterData);
    });

    it('should return undefined when counter not found', async () => {
      //Given
      const body = {
        query: getCounterQuery,
        variables: { code: 'counter' }
      };
      const response = {
        data: { data: {} }
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const counter = await configurationRepository.getCounterByCode('counter');

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(counter).toBeUndefined();
    });
  });

  describe('getServiceRecommendations', () => {
    it('should get Service Recommendations', async () => {
      // Given
      const serviceReccommendationData = getServiceReccommendation();
      const body = {
        query: getServiceRecommendationsQuery,
        variables: { refundable: false }
      };
      const response = {
        data: {
          data: { getServiceRecommendations: serviceReccommendationData }
        }
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const serviceReccommendations = await configurationRepository.getServiceRecommendations(
        false
      );

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(serviceReccommendations).toEqual(serviceReccommendationData);
    });

    it('should return undefined when service recommendations not found', async () => {
      //Given
      const body = {
        query: getServiceRecommendationsQuery,
        variables: { refundable: false }
      };
      const response = {
        data: { data: {} }
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const serviceReccommendations = await configurationRepository.getServiceRecommendations(
        false
      );

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(serviceReccommendations).toBeUndefined();
    });
  });

  describe('getWorkingTime', () => {
    it('should get working time data success', async () => {
      const body = {
        query: queryWorkingTimeTemplate,
        variables: {
          category: WorkingScheduleCategory.SKN
        }
      };
      const data = [
        {
          dayName: 'Monday',
          startTime: '8:00 AM',
          endTime: '5:00 PM'
        }
      ];
      const response = {
        data: {
          data: {
            workingTimes: data
          }
        }
      };
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);
      const result = await configurationRepository.getWorkingTime(
        WorkingScheduleCategory.SKN
      );

      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(result).toEqual(data);
    });

    it('should throw error when call api not success', async () => {
      (httpClient.post as jest.Mock).mockRejectedValueOnce({});
      const error = await configurationRepository
        .getWorkingTime(WorkingScheduleCategory.SKN)
        .catch(error => error);
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.detail).toEqual('Missing working time for category: SKN');
      expect(error.actor).toEqual(TransferFailureReasonActor.JAGO);
      expect(error.errorCode).toEqual(ERROR_CODE.CANNOT_GET_CONFIG_PARAMETER);
    });

    it('should throw empty when not get result', async () => {
      (httpClient.post as jest.Mock).mockResolvedValueOnce(null);
      const result = await configurationRepository.getWorkingTime(
        WorkingScheduleCategory.SKN
      );
      expect(result).toEqual([]);
    });
  });

  describe('getSavingProduct', () => {
    const code = 'MA';
    const body = {
      query: savingProductBaseQuery,
      variables: {
        code
      }
    };
    it('should return a successful response if a saving product is for given account code', async () => {
      const response = {
        data: {
          data: {
            savingProduct: {
              code: code,
              maximumAccountPerUser: 1,
              currency: 'IDR',
              maximumBalancePerAccount: 10000000000000,
              type: 'CURRENT_ACCOUNT',
              blockingCapability: true,
              initialAmount: 0
            }
          }
        }
      };

      httpClient.post = jest.fn().mockResolvedValueOnce(response);

      const result = await configurationRepository.getSavingProduct(code);
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(result).toEqual(response.data.data.savingProduct);
    });

    it('should return undefined if saving product is not found for given account code', async () => {
      httpClient.post = jest.fn().mockResolvedValueOnce({ data: {} });

      const result = await configurationRepository.getSavingProduct(code);
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(result).toBeUndefined();
    });

    it('should get data correctly by redis without hitting configuration', async () => {
      //Given
      const expectedData = { code };
      (redis.get as jest.Mock).mockResolvedValueOnce(expectedData);

      // When
      const actual = await configurationRepository.getSavingProduct(code);

      // Then
      expect(httpClient.post).not.toHaveBeenCalled();
      expect(actual).toEqual(expectedData);
    });
  });

  describe('getBasicFees', () => {
    const code = 'MA';
    const body = {
      query: basicFeeQuery,
      variables: {
        code
      }
    };
    it('should return a successful response if a basic fee', async () => {
      const response = {
        data: {
          data: {
            basicFee: getBasicFeeData()
          }
        }
      };

      httpClient.post = jest.fn().mockResolvedValueOnce(response);

      const result = await configurationRepository.getBasicFees(code);
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(result).toEqual(response.data.data.basicFee);
    });

    it('should return undefined if basicFee is not found', async () => {
      httpClient.post = jest.fn().mockResolvedValueOnce({ data: {} });

      const result = await configurationRepository.getBasicFees(code);
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(result).toBeUndefined();
    });

    it('should get data correctly by redis without hitting configuration', async () => {
      //Given
      const expectedData = { code };
      (redis.get as jest.Mock).mockResolvedValueOnce(expectedData);

      // When
      const actual = await configurationRepository.getBasicFees(code);

      // Then
      expect(httpClient.post).not.toHaveBeenCalled();
      expect(actual).toEqual(expectedData);
    });
  });

  describe('getTransactionCode', () => {
    const code = 'MA';
    const body = {
      query: transactionCodeQuery,
      variables: {
        code
      }
    };
    it('should return a successful response if a basic fee', async () => {
      const response = {
        data: {
          data: {
            transactionCode: getTransactionCode()
          }
        }
      };

      httpClient.post = jest.fn().mockResolvedValueOnce(response);

      const result = await configurationRepository.getTransactionCode(code);
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(result).toEqual(response.data.data.transactionCode);
    });

    it('should return undefined if transactionCode is not found', async () => {
      httpClient.post = jest.fn().mockResolvedValueOnce({ data: {} });

      const result = await configurationRepository.getTransactionCode(code);
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(result).toBeUndefined();
    });

    it('should get data correctly by redis without hitting configuration', async () => {
      //Given
      const expectedData = { code };
      (redis.get as jest.Mock).mockResolvedValueOnce(expectedData);

      // When
      const actual = await configurationRepository.getTransactionCode(code);

      // Then
      expect(httpClient.post).not.toHaveBeenCalled();
      expect(actual).toEqual(expectedData);
    });
  });

  describe('getAllGeneralLedgers', () => {
    const mockGeneralLedgers = getGeneralLedgersMockData();

    it('should return all general ledgers from redis', async () => {
      // given
      (redis.get as jest.Mock).mockResolvedValueOnce(mockGeneralLedgers);

      // when
      const result = await configurationRepository.getAllGeneralLedgers();

      // then
      expect(result).toEqual(mockGeneralLedgers);
    });

    it('should return all general ledgers from configuration', async () => {
      // given
      const response = {
        data: {
          data: {
            generalLedgers: mockGeneralLedgers
          }
        }
      };
      httpClient.post = jest.fn().mockResolvedValueOnce(response);

      // when
      const result = await configurationRepository.getAllGeneralLedgers();

      // then
      expect(result).toEqual(mockGeneralLedgers);
    });

    it('should empty array if get no result', async () => {
      // given
      const response = {
        data: {
          data: undefined
        }
      };
      httpClient.post = jest.fn().mockResolvedValueOnce(response);

      // when
      const result = await configurationRepository.getAllGeneralLedgers();

      // then
      expect(result).toEqual([]);
    });
  });

  describe('getNNSMapping query', () => {
    it('should get data correctly', async () => {
      // Given
      const reservedNnsPrefix = '002';
      const body = {
        query: getNNSMappingQuery,
        variables: {
          reservedNnsPrefix
        }
      };
      const expectedData = { reservedNnsPrefix };
      const response = {
        data: {
          data: {
            getNNSMappingByNNS: expectedData
          }
        }
      };
      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const actual = await configurationRepository.getNNSMapping(
        reservedNnsPrefix
      );

      // Then
      expect(httpClient.post).toHaveBeenCalledWith('', body);
      expect(actual).toEqual(expectedData);
    });

    it('should get data correctly by redis without hitting configuration', async () => {
      // Given
      const reservedNnsPrefix = 'C001';
      const expectedData = { reservedNnsPrefix };
      (redis.get as jest.Mock).mockResolvedValueOnce(expectedData);

      // When
      const actual = await configurationRepository.getNNSMapping(
        reservedNnsPrefix
      );

      // Then
      expect(httpClient.post).not.toHaveBeenCalled();
      expect(actual).toEqual(expectedData);
    });

    it('should return undefined if data is not found', async () => {
      // Given
      const code = '002';
      const response = {
        data: {}
      };

      (httpClient.post as jest.Mock).mockResolvedValueOnce(response);

      // When
      const actual = await configurationRepository.getNNSMapping(code);

      // Then
      expect(actual).toBeUndefined();
    });
  });
});
