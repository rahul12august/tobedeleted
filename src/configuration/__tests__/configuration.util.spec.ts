import { standarizeRTOL } from '../configuration.util';

describe('configuration.util', () => {
  describe('standarizeRTOL', () => {
    it('should return 014 when 14 is rtol code is send', () => {
      const result = standarizeRTOL('14');

      expect(result).toEqual('014');
    });

    it('should return 004 when 4 is rtol code is send', () => {
      const result = standarizeRTOL('4');

      expect(result).toEqual('004');
    });

    it('should return 542 when 542 is rtol code is send', () => {
      const result = standarizeRTOL('542');

      expect(result).toEqual('542');
    });

    it('should return 00000004 when 004 is rtol code is send', () => {
      const result = standarizeRTOL('00000004');

      expect(result).toEqual('004');
    });

    it('should return 00000044 when 044 is rtol code is send', () => {
      const result = standarizeRTOL('00000044');

      expect(result).toEqual('044');
    });

    it('should return 00000444 when 444 is rtol code is send', () => {
      const result = standarizeRTOL('00000444');

      expect(result).toEqual('444');
    });

    it('should return 360000004 when 360000004 is rtol code is send', () => {
      const result = standarizeRTOL('360000004');

      expect(result).toEqual('360000004');
    });
  });
});
