const standarizeRTOL = (code: string): string => {
  code = code.replace(/^0+/, '');
  if (code.length === 1) return '00' + code;
  else if (code.length === 2) return '0' + code;
  return code;
};

export { standarizeRTOL };
