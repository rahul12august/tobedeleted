import logger, { wrapLogs } from '../logger';
import httpClient from './httpClient';
import { UpdateTransactionHistoryRequest } from './transactionHistory.type';

const updateTransactionHistory = async (
  coreBankingId: string,
  status: string,
  referenceId?: string | undefined
) => {
  logger.info(
    `Updating transaction history: ${coreBankingId}.
      Reference ID: ${referenceId}
      Status: ${status}`
  );
  const thUrl = `/private/transactionHistory/${coreBankingId}`;
  const payload: UpdateTransactionHistoryRequest = { status: status };
  if (referenceId) {
    payload.referenceId = referenceId;
  }
  await httpClient.put(thUrl, payload);
};

const transactionHistoryRepository = wrapLogs({
  updateTransactionHistory
});

export default transactionHistoryRepository;
