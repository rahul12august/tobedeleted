import { HttpError } from '@dk/module-httpclient';
import { updateTransactionHistoryReferenceIdWithRetry } from '../transactionHistory.util';

describe('transactionHistory.util', () => {
  describe('updateTransactionHistoryReferenceIdWithRetry', () => {
    it('should return undefined when throwing error', async () => {
      const result = await updateTransactionHistoryReferenceIdWithRetry(() => {
        throw new Error();
      });
      expect(result).toBeUndefined();
    });

    it('should return undefined when throwing error', () => {
      const result = updateTransactionHistoryReferenceIdWithRetry(() => {
        throw new HttpError(400, {
          error: {
            code: 'INVALID_TRANSACTION_ID'
          }
        });
      });
      expect(result).not.toBeUndefined();
    });
  });
});
