import { Util } from '@dk/module-common';
import { HttpError } from '@dk/module-httpclient';
import logger from '../logger';

const RETRIABLE_ERROR_CODE = 'INVALID_TRANSACTION_ID';

const DEFAULT_RETRY_COUNT = '3';

const DEFAULT_RETRY_DELAY_IN_MS = '2000';

const getRetryCount = () => process.env.TH_RETRY_COUNT || DEFAULT_RETRY_COUNT;

const getDelayInMS = () =>
  process.env.TH_RETRY_DELAY || DEFAULT_RETRY_DELAY_IN_MS;

const getRetryConfig = () => ({
  shouldRetry: (error: HttpError) => {
    logger.info(
      `Failed to update TH reference ID. ${JSON.stringify(error.error)}`
    );
    return error?.error?.error?.code === RETRIABLE_ERROR_CODE;
  },
  delayInMS: Number(getDelayInMS()),
  numberOfTries: Number(getRetryCount())
});

export const updateTransactionHistoryReferenceIdWithRetry = async <
  T extends (...args: any[]) => any
>(
  fn: T,
  ...args: Parameters<T>
): Promise<ReturnType<T> | undefined> => {
  try {
    const retryConfig = getRetryConfig();
    logger.debug(
      `updateTransactionHistoryReferenceIdWithRetry: RetryConfig: 
      ${JSON.stringify(retryConfig)}`
    );
    return await Util.retry(retryConfig, fn, ...args);
  } catch (err) {
    logger.error(`Failed updating TH reference ID. ${err}`);
    return undefined;
  }
};
