export interface UpdateTransactionHistoryRequest {
  status: string;
  referenceId?: string;
}
