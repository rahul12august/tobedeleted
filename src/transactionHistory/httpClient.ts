import { createHttpClient, HttpClient } from '@dk/module-httpclient';
import { retryConfig } from '../common/constant';
import { config } from '../config';
import { context } from '../context';
import logger from '../logger';

const baseUrl = config.get('ms').transactionHistory;

const httpClient: HttpClient = createHttpClient({
  baseURL: baseUrl,
  context,
  logger,
  retryConfig
});

export default httpClient;
