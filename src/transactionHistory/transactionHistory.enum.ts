export enum TransactionHistoryStatusEnum {
  SUCCEED = 'SUCCEED',
  PENDING = 'PENDING',
  FAILED = 'FAILED'
}
