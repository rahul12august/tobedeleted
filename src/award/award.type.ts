export interface UsageCounter {
  limitGroupCode: string;
  dailyAccumulationAmount: number;
  monthlyAccumulationTransaction: number;
  quota?: number;
  isQuotaExceeded?: boolean;
}
export interface UpdateUsageCounterPayload {
  limitGroupCode: string;
  adjustmentLimitAmount: number;
}
export interface ThresholdCounter {
  thresholdCounter: string;
}
