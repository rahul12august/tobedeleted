import { get } from 'lodash';

import httpClient from './httpClient';
import { UsageCounter, UpdateUsageCounterPayload } from './award.type';
import logger, { wrapLogs } from '../logger';
const pathUsageCounters = '/private/usage-counters';

const getUsageCounters = async (
  customerId: string,
  limitGroupCodes?: string[]
): Promise<UsageCounter[]> => {
  logger.info(
    `getUsageCounters repository: get usage counter for customerId: ${customerId}.`
  );
  const result = await httpClient.get(
    `/private/customers/${customerId}/usage-counters`,
    limitGroupCodes
      ? {
          params: {
            limitGroupCodes: JSON.stringify(limitGroupCodes)
          }
        }
      : {}
  );
  return get(result, ['data', 'data'], []);
};

const getThresholdCounter = async (
  customerId: string,
  groupType: string
): Promise<string> => {
  logger.info(
    `getThresholdCounter repository: get threshold counter for customerId: ${customerId} and groupType: ${groupType}.`
  );
  const result = await httpClient.get(
    `/private/customers/${customerId}/threshold-counter/${groupType}`
  );
  return get(result, ['data', 'data', 'thresholdCounter'], {});
};

const updateUsageCounters = async (
  customerId: string,
  data: UpdateUsageCounterPayload[]
): Promise<void> => {
  logger.info(
    `updateUsageCounters repository: update usage counter for customerId: ${customerId}.`
  );
  const payload = data.map(updateCounterPayload => ({
    limitGroupCode: updateCounterPayload.limitGroupCode,
    adjustmentPoint: 1,
    adjustmentLimitAmount: updateCounterPayload.adjustmentLimitAmount
  }));
  await httpClient.put(`${pathUsageCounters}`, payload, {
    params: {
      customerId
    }
  });
};

const awardRepository = wrapLogs({
  getUsageCounters,
  getThresholdCounter,
  updateUsageCounters
});

export default awardRepository;
