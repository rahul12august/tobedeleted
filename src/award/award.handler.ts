import { isEmpty, isUndefined } from 'lodash';
import { ERROR_CODE } from '../common/errors';
import configurationRepository from '../configuration/configuration.repository';
import { CustomerEntitlementHandler } from '../customerEntitlementManager/customerEntitlement.interface';
import {
  ICounterUsageByTransCodeRequest,
  ICustomerEntitlementByTransCodeResponse
} from '../entitlement/entitlement.interface';
import { TransferAppError } from '../errors/AppError';
import { BasicFeeMapping, FeeRuleInfo } from '../fee/fee.type';
import logger from '../logger';
import { IValidatedFeeRuleObject } from './award.interface';
import { UsageCounter } from './award.type';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

export class AwardHandler implements CustomerEntitlementHandler {
  async generateMissingUsageCounterBasedOnTransactionCodes(
    params: ICounterUsageByTransCodeRequest
  ): Promise<UsageCounter[]> {
    //return as is
    return params.usageCounters;
  }

  async validateFeeRuleObject(
    feeRuleObject: FeeRuleInfo
  ): Promise<IValidatedFeeRuleObject> {
    logger.info(`[AwardHandler] validateFeeRuleObject: 
      monthlyNoTransaction: ${feeRuleObject.monthlyNoTransaction}
      counterCode: ${feeRuleObject.counterCode}
    `);

    if (
      typeof feeRuleObject.monthlyNoTransaction !== 'number' ||
      isUndefined(feeRuleObject.counterCode)
    ) {
      const detail = `monthlyNoTransaction or counter code not found!`;
      logger.error(`[AwardHandler] validateFeeRuleObject: ${detail}`);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.MS_ENTITLEMENT,
        ERROR_CODE.TRANSFER_FEE_RULE_INVALID_MANDATORY_FIELDS
      );
    }
    logger.info(
      ` [AwardHandler] validateFeeRuleObject: Counter code: ${feeRuleObject.counterCode}`
    );
    const counter = await configurationRepository.getCounterByCode(
      feeRuleObject.counterCode
    );
    if (isEmpty(counter) || (counter && isUndefined(counter.value))) {
      const detail = `Counter code ${feeRuleObject.counterCode} not found!`;
      logger.error(`[AwardHandler] validateFeeRuleObject: ${detail}`);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.MS_ENTITLEMENT,
        ERROR_CODE.TRANSFER_FEE_RULE_INVALID_REGISTER_PARAMETER
      );
    }
    logger.info(
      `[AwardHandler] validateFeeRuleObject: monthlyNoTransaction: ${feeRuleObject.monthlyNoTransaction}`
    );

    //in most cases
    //basicFeeCode1 is for free transaction
    //basicFeeCode2 is for paid transaction

    return {
      usedQuota: feeRuleObject.monthlyNoTransaction,
      quota: counter ? counter.value : 0,
      freeFeeCode: feeRuleObject.basicFeeCode1,
      paidFeeCode: feeRuleObject.basicFeeCode2
    };
  }

  async determineBasicFeeMapping(
    feeRuleObject: FeeRuleInfo
  ): Promise<BasicFeeMapping[] | undefined> {
    const {
      usedQuota,
      quota,
      freeFeeCode,
      paidFeeCode
    } = await this.validateFeeRuleObject(feeRuleObject);

    logger.info(
      `[AwardHandler] determineBasicFeeMapping: 
        quota: [${quota}]
        usedQuota: [${usedQuota}]
      `
    );

    const basicFeeMapping = usedQuota <= quota ? freeFeeCode : paidFeeCode;

    return basicFeeMapping;
  }

  async preProcessCustomerEntitlementBasedOnTransactionCodes(): Promise<ICustomerEntitlementByTransCodeResponse | null> {
    logger.info(
      `[AwardHandler] preProcessCustomerEntitlementBasedOnTransactionCodes - no preprocessing
      `
    );

    //no preprocessing for awards
    return null;
  }
  mapEntitlementUsageCounterByLimitGroupCode(
    usageCounters: UsageCounter[]
  ): UsageCounter[] {
    logger.info(
      `[AwardHandler] mapEntitlementUsageCounterByLimitGroupCode - no mapping required [return as is]
      `
    );
    //just return as it is
    return usageCounters;
  }

  processFeeRuleInfo(currentFeeRuleInfo: FeeRuleInfo): FeeRuleInfo {
    logger.info(
      `[AwardHandler] processFeeRuleInfo - return as is
      `
    );
    //return as is
    return currentFeeRuleInfo;
  }
}
