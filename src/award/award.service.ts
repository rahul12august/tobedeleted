import { UsageCounter } from './award.type';
import awardRepository from './award.repository';
import logger, { wrapLogs } from '../logger';

const getUsageCounter = async (
  customerId: string,
  limitGroupCode: string
): Promise<UsageCounter | null> => {
  logger.info(
    `getUsageCounter service: get usage counter for customerId: ${customerId}.`
  );
  const usageCounters = await awardRepository.getUsageCounters(customerId, [
    limitGroupCode
  ]);
  return (usageCounters && usageCounters[0]) || null;
};

const getThresholdCounter = async (
  customerId: string,
  groupType: string
): Promise<string | undefined> => {
  logger.info(
    `getThresholdCounter service: get threshold counter for customerId: ${customerId} groupType: ${groupType}.`
  );
  const thresholdCounters = await awardRepository.getThresholdCounter(
    customerId,
    groupType
  );
  return thresholdCounters;
};

const awardService = wrapLogs({
  getUsageCounter,
  getThresholdCounter
});

export default awardService;
