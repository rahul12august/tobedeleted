import faker from 'faker';
import { ThresholdCounter } from '../award.type';

export const getUsageCountersData = () => {
  return [
    {
      limitGroupCode: 'L001',
      dailyAccumulationAmount: 2322,
      monthlyAccumulationAmount: 2322,
      totalAccumulationAmount: 2322,
      dailyAccumulationTransaction: 100,
      monthlyAccumulationTransaction: 100,
      totalAccumulationTransaction: 100
    }
  ];
};

export const anyCustomerId = (): string => {
  return faker.random.alphaNumeric(20);
};

export const anyCode = (): string => {
  return faker.random.alphaNumeric(8);
};

export const anyGroupType = (): string => {
  return faker.random.arrayElement([
    'Bonus_Transfer',
    'Bonus_Overseas_CashWithdrawal',
    'Bonus_Local_CashWithdrawal'
  ]);
};

export const usageCountersInput = () => {
  return {
    limitGroupCode: 'L001',
    adjustmentPoint: 1,
    adjustmentLimitAmount: 10000
  };
};

export const getThresholdCountersData = (): ThresholdCounter => {
  return { thresholdCounter: 'AWARD_TRANSFER_001' };
};
