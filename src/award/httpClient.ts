import { config } from '../config';
import { context } from '../context';
import logger from '../logger';
import { HttpClient } from '@dk/module-httpclient';
import { Http } from '@dk/module-common';
import { retryConfig } from '../common/constant';

const MS_AWARD_URL = config.get('ms').award;

const httpClient: HttpClient = Http.createHttpClientForward({
  baseURL: `${MS_AWARD_URL}`,
  context,
  logger,
  retryConfig
});

export default httpClient;
