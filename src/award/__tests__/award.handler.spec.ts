import { BankNetworkEnum } from '@dk/module-common';
import { ERROR_CODE } from '../../common/errors';

import { TransferAppError } from '../../errors/AppError';
import { FeeRuleType, COUNTER } from '../../fee/fee.constant';
import { FeeRuleInfo } from '../../fee/fee.type';
import { AwardHandler } from '../award.handler';
import configurationRepository from '../../configuration/configuration.repository';
import { getCountersList } from '../../configuration/__mocks__/configuration.data';
import { AwardLimitGroupCodes } from '../../entitlement/entitlement.enum';
import { UsageCounter } from '../award.type';

describe('awardHandler', () => {
  describe('determineBasicFeeMapping', () => {
    afterEach(() => {
      expect.hasAssertions();
      jest.clearAllMocks();
    });
    const feeRuleInfoTemplate: FeeRuleInfo = {
      code: FeeRuleType.RTOL_TRANSFER_FEE_RULE,
      counterCode: COUNTER.COUNTER_01,
      basicFeeCode1: [
        {
          interchange: BankNetworkEnum.ARTAJASA,
          basicFeeCode: 'TF015'
        },
        {
          interchange: BankNetworkEnum.ALTO,
          basicFeeCode: 'TF008'
        }
      ],
      basicFeeCode2: [
        {
          interchange: BankNetworkEnum.ARTAJASA,
          basicFeeCode: 'TF010'
        },
        {
          interchange: BankNetworkEnum.ALTO,
          basicFeeCode: 'TF009'
        }
      ],
      monthlyNoTransaction: 0,
      interchange: BankNetworkEnum.ALTO
    };

    it('should be failed when required fields are undefined', async () => {
      const feeRuleInfo = {
        ...feeRuleInfoTemplate
      };

      feeRuleInfo.quota = undefined;
      feeRuleInfo.monthlyNoTransaction = undefined;
      feeRuleInfo.counterCode = undefined;

      const awardHandler = new AwardHandler();

      const output = await awardHandler
        .determineBasicFeeMapping(feeRuleInfo)
        .catch(err => err);

      // Then
      expect(output).toBeInstanceOf(TransferAppError);
      expect(output.errorCode).toBe(
        ERROR_CODE.TRANSFER_FEE_RULE_INVALID_MANDATORY_FIELDS
      );
    });

    it('should return basicCode1 when usedQuota < quota', async () => {
      const feeRuleInfo = {
        ...feeRuleInfoTemplate
      };

      feeRuleInfo.quota = 25;
      feeRuleInfo.monthlyNoTransaction = 1;
      feeRuleInfo.counterCode = 'COUNTER_01';

      jest
        .spyOn(configurationRepository, 'getCounterByCode')
        .mockResolvedValue(getCountersList('COUNTER_1'));

      const awardHandler = new AwardHandler();

      const output = await awardHandler.determineBasicFeeMapping(feeRuleInfo);

      // Then
      expect(output).toEqual(feeRuleInfo.basicFeeCode1);
    });

    it('should return basicCode1 when usedQuota == quota', async () => {
      const feeRuleInfo = {
        ...feeRuleInfoTemplate
      };

      feeRuleInfo.quota = 25;
      feeRuleInfo.monthlyNoTransaction = 25;
      feeRuleInfo.counterCode = 'COUNTER_01';

      jest
        .spyOn(configurationRepository, 'getCounterByCode')
        .mockResolvedValue(getCountersList('COUNTER_1'));

      const awardHandler = new AwardHandler();

      const output = await awardHandler.determineBasicFeeMapping(feeRuleInfo);

      // Then
      expect(output).toEqual(feeRuleInfo.basicFeeCode1);
    });

    it('should return basicCode2 when usedQuota == quota, but it exceeds the quota', async () => {
      const feeRuleInfo = {
        ...feeRuleInfoTemplate
      };

      feeRuleInfo.quota = 25;
      feeRuleInfo.monthlyNoTransaction = 26;
      feeRuleInfo.counterCode = 'COUNTER_01';

      jest
        .spyOn(configurationRepository, 'getCounterByCode')
        .mockResolvedValue({
          code: 'COUNTER_01',
          type: 'AWARDS',
          name: 'name 1',
          value: 25
        });

      const awardHandler = new AwardHandler();

      const output = await awardHandler.determineBasicFeeMapping(feeRuleInfo);

      // Then
      expect(output).toEqual(feeRuleInfo.basicFeeCode2);
    });
  });

  describe('generateMissingUsageCounterBasedOnTransactionCodes', () => {
    const usageCounters: UsageCounter[] = [
      {
        limitGroupCode: 'L002',
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      },
      {
        limitGroupCode: AwardLimitGroupCodes.BONUS_TRANSFER,
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      }
    ];

    it('it should return usage counter as is', async () => {
      const awardHandler = new AwardHandler();
      const result = await awardHandler.generateMissingUsageCounterBasedOnTransactionCodes(
        {
          usageCounters
        }
      );

      expect(result).toBe(usageCounters);
    });
  });

  describe('preProcessCustomerEntitlementBasedOnTransactionCodes', () => {
    it('should return null', async () => {
      const awardHandler = new AwardHandler();
      const result = await awardHandler.preProcessCustomerEntitlementBasedOnTransactionCodes();
      expect(result).toBeNull();
    });
  });

  describe('mapEntitlementUsageCounterByLimitGroupCode', () => {
    const usageCounters: UsageCounter[] = [
      {
        limitGroupCode: 'L002',
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      },
      {
        limitGroupCode: AwardLimitGroupCodes.BONUS_TRANSFER,
        dailyAccumulationAmount: 50000,
        monthlyAccumulationTransaction: 0,
        quota: 0,
        isQuotaExceeded: false
      }
    ];

    it('should return usageCounter as is', () => {
      const awardHandler = new AwardHandler();
      const result = awardHandler.mapEntitlementUsageCounterByLimitGroupCode(
        usageCounters
      );
      expect(result).toBe(usageCounters);
    });
  });

  describe('processFeeRuleInfo', () => {
    const feeRuleInfoTemplate: FeeRuleInfo = {
      code: FeeRuleType.RTOL_TRANSFER_FEE_RULE,
      counterCode: COUNTER.COUNTER_01,
      basicFeeCode1: [
        {
          interchange: BankNetworkEnum.ARTAJASA,
          basicFeeCode: 'TF015'
        },
        {
          interchange: BankNetworkEnum.ALTO,
          basicFeeCode: 'TF008'
        }
      ],
      basicFeeCode2: [
        {
          interchange: BankNetworkEnum.ARTAJASA,
          basicFeeCode: 'TF010'
        },
        {
          interchange: BankNetworkEnum.ALTO,
          basicFeeCode: 'TF009'
        }
      ],
      monthlyNoTransaction: 0,
      quota: 25,
      interchange: BankNetworkEnum.ALTO,
      isQuotaExceeded: false
    };

    it('should return current feeRule as is', () => {
      const awardHandler = new AwardHandler();
      const result = awardHandler.processFeeRuleInfo(feeRuleInfoTemplate);
      expect(result).toBe(feeRuleInfoTemplate);
    });
  });
});
