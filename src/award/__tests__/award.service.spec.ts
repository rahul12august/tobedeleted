import repository from '../award.repository';
import service from '../award.service';
import { anyCustomerId, anyCode, anyGroupType } from '../__mocks__/award.data';
import { ThresholdCounter, UsageCounter } from '../award.type';

jest.mock('../award.repository');

describe('getUsageCounter', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should return null if repository return null', async () => {
    (repository.getUsageCounters as jest.Mock).mockResolvedValueOnce(null);

    const usageCounter = await service.getUsageCounter(
      anyCustomerId(),
      anyCode()
    );

    expect(usageCounter).toBeNull();
    expect(repository.getUsageCounters).toBeCalledTimes(1);
  });

  it('should return null if repository return empty', async () => {
    (repository.getUsageCounters as jest.Mock).mockResolvedValueOnce([]);

    const usageCounter = await service.getUsageCounter(
      anyCustomerId(),
      anyCode()
    );

    expect(usageCounter).toBeNull();
    expect(repository.getUsageCounters).toBeCalledTimes(1);
  });

  it('should return usage counter', async () => {
    const CustomerId = anyCustomerId();
    const limitGroupCode = anyCode();

    const usageCounter: UsageCounter = {
      limitGroupCode,
      dailyAccumulationAmount: 1,
      monthlyAccumulationTransaction: 2
    };

    (repository.getUsageCounters as jest.Mock).mockResolvedValueOnce([
      usageCounter
    ]);

    const foundUsageCounter = await service.getUsageCounter(
      CustomerId,
      limitGroupCode
    );

    expect(foundUsageCounter).toEqual(usageCounter);
  });
});

describe('getThresholdCounter', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should return null if repository return null', async () => {
    (repository.getThresholdCounter as jest.Mock).mockResolvedValueOnce(null);

    const thresholdCounter = await service.getThresholdCounter(
      anyCustomerId(),
      anyGroupType()
    );

    expect(thresholdCounter).toBeNull();
    expect(repository.getThresholdCounter).toBeCalledTimes(1);
  });

  it('should return empty Object if repository return empty object', async () => {
    (repository.getThresholdCounter as jest.Mock).mockResolvedValueOnce({});

    const thresholdCounter = await service.getThresholdCounter(
      anyCustomerId(),
      anyGroupType()
    );

    expect(thresholdCounter).toEqual({});
    expect(repository.getThresholdCounter).toBeCalledTimes(1);
  });

  it('should return threshold counter', async () => {
    const customerId = anyCustomerId();
    const groupType = anyGroupType();

    const thresholdCounter: ThresholdCounter = {
      thresholdCounter: 'AWARD_TRANSFER_001'
    };

    (repository.getThresholdCounter as jest.Mock).mockResolvedValueOnce(
      thresholdCounter
    );

    const foundthresholCounter = await service.getThresholdCounter(
      customerId,
      groupType
    );

    expect(foundthresholCounter).toEqual(thresholdCounter);
  });
});
