import {
  getThresholdCountersData,
  getUsageCountersData,
  usageCountersInput
} from '../__mocks__/award.data';
import httpClient from '../httpClient';
import repository from '../award.repository';

jest.mock('../httpClient');

describe('award.repository', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('getUsageCounters', () => {
    it('should return data if found', async () => {
      const data = getUsageCountersData();
      const customerId = '123456';
      const limitGroupCodes = ['L001', 'L002'];
      (httpClient.get as jest.Mock).mockResolvedValueOnce({ data: { data } });
      const expected = await repository.getUsageCounters(
        customerId,
        limitGroupCodes
      );
      expect(expected).toHaveLength(1);
      expect(httpClient.get).toBeCalledWith(
        `/private/customers/${customerId}/usage-counters`,
        {
          params: {
            limitGroupCodes: JSON.stringify(limitGroupCodes)
          }
        }
      );
    });
    it('should return [] if not found', async () => {
      const data = [];
      (httpClient.get as jest.Mock).mockResolvedValueOnce({ data: { data } });
      const expected = await repository.getUsageCounters('123456', [
        'L001',
        'L002'
      ]);
      expect(expected).toHaveLength(0);
    });
  });

  describe('updateUsageCounters', () => {
    it('should update usage counters success', async () => {
      const data = usageCountersInput();
      const customerId = '123456';
      httpClient.post = jest.fn().mockResolvedValueOnce(undefined);
      const expected = await repository.updateUsageCounters(customerId, [
        {
          limitGroupCode: data.limitGroupCode,
          adjustmentLimitAmount: data.adjustmentLimitAmount
        }
      ]);
      expect(expected).toBeUndefined();
      expect(httpClient.put).toBeCalledWith(`/private/usage-counters`, [data], {
        params: {
          customerId
        }
      });
    });
  });

  describe('getThresholdCounter', () => {
    it('should return data if found', async () => {
      const data = getThresholdCountersData();
      const customerId = '123456';
      const groupType = 'Bonus_Transfer';
      (httpClient.get as jest.Mock).mockResolvedValueOnce({ data: { data } });
      const expected = await repository.getThresholdCounter(
        customerId,
        groupType
      );
      expect(expected).toEqual(data.thresholdCounter);
      expect(httpClient.get).toBeCalledWith(
        `/private/customers/${customerId}/threshold-counter/${groupType}`
      );
    });
    it('should return [] if not found', async () => {
      (httpClient.get as jest.Mock).mockResolvedValueOnce({ data: {} });
      const expected = await repository.getThresholdCounter(
        '123456',
        'Bonus_Transfer'
      );
      expect(expected).toEqual({});
    });
  });
});
