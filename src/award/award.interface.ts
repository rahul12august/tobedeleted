import { BasicFeeMapping } from '../fee/fee.type';

export interface IValidatedFeeRuleObject {
  usedQuota: number;
  quota: number;
  freeFeeCode: BasicFeeMapping[] | undefined;
  paidFeeCode: BasicFeeMapping[] | undefined;
}
