import faker from 'faker';
import { IFeeAmount, IFeeRuleTemplatePayload } from '../fee.type';
import { BankChannelEnum } from '@dk/module-common';
import {
  IBasicFee,
  ITransactionCode
} from '../../configuration/configuration.type';
import { TransactionCodeType } from '../../configuration/configuration.enum';

export const getFeeAmountData = (): IFeeAmount => {
  return {
    feeCode: 'CW003',
    feeAmount: 25000,
    customerTc: 'FCD05',
    customerTcChannel: 'FCD05_channel',
    subsidiary: true,
    subsidiaryAmount: 7500,
    debitGlNumber: '01.01.00.00.00.00',
    creditGlNumber: '01.02.00.00.00.00'
  };
};

export const getFeeAmountDataWithoutGlNumber = (): IFeeAmount => {
  return {
    feeCode: 'CW003',
    feeAmount: 25000,
    customerTc: 'FCD05',
    customerTcChannel: 'FCD05_channel',
    subsidiary: false,
    subsidiaryAmount: 0,
    debitGlNumber: '',
    creditGlNumber: ''
  };
};

export const feeRuleTemplatePayloadForThirdPartyFee = (): IFeeRuleTemplatePayload => ({
  basicFee: {
    thirdPartyFee: true,
    code: 'BC144',
    customerTc: 'dummy',
    customerTcInfo: {
      channel: 'dummy'
    },
    subsidiary: false
  },
  transactionAmount: 1000,
  externalId: 'dummy123',
  beneficiaryBankCodeChannel: BankChannelEnum.GOBILLS
});

export const feeRuleTemplatePayloadForThirdPartyFeeWithAdminFee = (): IFeeRuleTemplatePayload => ({
  basicFee: {
    thirdPartyFee: true,
    code: 'BC144',
    customerTc: 'dummy',
    customerTcInfo: {
      channel: 'dummy'
    },
    subsidiary: false
  },
  transactionAmount: 1000,
  externalId: 'dummy123',
  beneficiaryBankCodeChannel: BankChannelEnum.GOBILLS,
  feeAmount: 2500
});

export const feeRuleTemplatePayloadForTokopedia = (): IFeeRuleTemplatePayload => ({
  basicFee: {
    thirdPartyFee: true,
    code: 'BC144',
    customerTc: 'dummy',
    customerTcInfo: {
      channel: 'dummy'
    },
    subsidiary: false
  },
  transactionAmount: 1000,
  externalId: 'dummy123',
  beneficiaryBankCodeChannel: BankChannelEnum.TOKOPEDIA
});

export const feeRuleTemplatePayloadForFixedFee = (): IFeeRuleTemplatePayload => ({
  basicFee: {
    feeAmountFixed: 100,
    code: 'BC144',
    customerTc: 'dummy',
    customerTcInfo: {
      channel: 'dummy'
    },
    subsidiary: true,
    subsidiaryAmount: 7500
  },
  transactionAmount: 1000,
  externalId: 'dummy123'
});

export const feeRuleTemplatePayloadForPercentageFee = (): IFeeRuleTemplatePayload => ({
  basicFee: {
    feeAmountPercentage: 2,
    code: 'BC144',
    customerTc: 'dummy',
    customerTcInfo: {
      channel: 'dummy'
    },
    subsidiary: true,
    subsidiaryAmountPercentage: 20
  },
  transactionAmount: 1000,
  externalId: 'dummy123'
});

export const getBasicFeeData = (): IBasicFee => ({
  code: faker.random.word().substr(0, 40),
  feeAmountPercentage: faker.random.number({ min: 1, max: 100 }),
  customerTc: faker.random.word().substr(0, 40),
  subsidiary: true,
  subsidiaryAmount: 35000,
  debitGlNumber: '7170232320.00.00',
  creditGlNumber: '71702323202.00.00',
  thirdPartyFee: false,
  customerTcInfo: {
    channel: faker.random.word().substr(0, 40)
  }
});

export const getTransactionCode = (): ITransactionCode => {
  return {
    code: 'SAD01',
    ownershipCode: 'MA',
    transactionType: TransactionCodeType.DEBIT,
    defaultCategoryCode: 'C011',
    splitBill: false,
    isExternal: false,
    downloadReceipt: false,
    allowedToDispute: false,
    currency: 'Any',
    minAmountPerTx: 1,
    maxAmountPerTx: 99999999999,
    limitGroupCode: 'L001',
    feeRules: 'ZERO_FEE_RULE',
    allowedToRepeat: false,
    addToContact: false,
    allowedToInstallment: false,
    loyaltyPoint: false,
    cashBack: false,
    image: '/picture/Transaction_code/SAD01.png',
    notificationTemplate: 'Notif_SAD01',
    showInConsolidatedTrxHistory: false,
    neutralizeSignage: false,
    reversalCodeReference: 'IVC01',
    maxReversalDay: 1,
    channel: 'SAD01'
  };
};
