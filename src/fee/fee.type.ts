import hapi from '@hapi/hapi';
import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import {
  feeAmountRequestValidator,
  feeAmountResponseValidator
} from './fee.validator';
import { BankNetworkEnum } from '@dk/module-common';
import { IRefund } from '../transaction/transaction.model';
import { FeeRuleType } from './fee.constant';
import { IBasicFee } from '../configuration/configuration.type';
import { UsageCounter } from '../award/award.type';

export type FeeAmountRequest = Joi.extractType<
  typeof feeAmountRequestValidator
>;

export interface IFeeAmountRequest extends hapi.Request {
  payload: FeeAmountRequest;
}

export type IFeeAmountResponse = Joi.extractType<
  typeof feeAmountResponseValidator
>;

export interface IFeeAmount extends Required<IFeeAmountResponse> {
  interchange?: BankNetworkEnum;
  feeCode: string;
}

export type TransactionFeeAmounts = IFeeAmount[];

export interface IFeeRuleTemplatePayload {
  basicFee: IBasicFee;
  transactionAmount: number;
  feeAmount?: number;
  externalId?: string;
  beneficiaryBankCodeChannel?: string;
  refund?: IRefund;
}

export interface IFeeRuleTemplate {
  isApplicable(input: IFeeRuleTemplatePayload): boolean;
  getFee(input: IFeeRuleTemplatePayload): Promise<number> | number;
}

export interface FeeRuleInput {
  code: FeeRuleType;
  transactionAmount: number;
  monthlyNoTransaction?: number;
  interchange?: BankNetworkEnum;
  targetBankCode?: string;
  thresholdCounter?: string;
  quota?: number;
}

export interface FeeAmountInput {
  feeRuleCode: string;
  transactionAmount: number;
  feeAmount?: number;
  monthlyNoTransaction?: number;
  interchange?: BankNetworkEnum;
  targetBankCode?: string;
  externalId?: string;
  beneficiaryBankCodeChannel?: string;
  refund?: IRefund;
  thresholdCounter?: string;
  customerId?: string;
  awardGroupCounter?: string | undefined;
  limitGroupCode?: string | undefined;
  usageCounters?: UsageCounter[];
}

export interface BasicFeeMapping {
  interchange?: BankNetworkEnum;
  basicFeeCode: string;
  basicFee?: IBasicFee;
}

export interface IBasicFeeMapping {
  interchange?: BankNetworkEnum;
  basicFeeCode: string;
  basicFee: IBasicFee;
}

export interface IFeeRule {
  description: string;
  basicFeeMapping?: IBasicFeeMapping[];
}

export interface IFeeRuleType {
  description: string;
  basicFeeMapping?: BasicFeeMapping[];
}

export interface FeeRuleInfo {
  code: FeeRuleType;
  counterCode?: string;
  basicFeeCode1?: BasicFeeMapping[];
  basicFeeCode2?: BasicFeeMapping[];
  monthlyNoTransaction?: number;
  quota?: number;
  interchange?: BankNetworkEnum;
  isQuotaExceeded?: boolean;
}

export interface BasicFeeRecordKey {
  basicFeeCode: string;
  customerId?: string;
}
export interface FeeAmountQuery {
  input: FeeAmountInput;
  feeMappings: IBasicFeeMapping[];
}
