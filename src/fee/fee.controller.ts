import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';

import {
  feeAmountResponseValidator,
  feeAmountRequestValidator
} from './fee.validator';
import { IFeeAmountRequest } from './fee.type';
import feeService from './fee.service';

const getFeeAmount: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/fees',
  options: {
    description: 'Calculate fee for transaction',
    notes: 'All information must be valid',
    tags: ['api', 'fees'],
    validate: {
      payload: feeAmountRequestValidator
    },
    response: {
      schema: feeAmountResponseValidator
    },
    handler: async (
      hapiRequest: IFeeAmountRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const {
        feeRuleCode,
        transactionAmount,
        monthlyNoTransaction,
        customerId
      } = hapiRequest.payload;
      const feeData = await feeService.getFeeAmount({
        feeRuleCode: feeRuleCode,
        transactionAmount: transactionAmount,
        monthlyNoTransaction: monthlyNoTransaction,
        customerId: customerId
      });
      const defaultFeeDataResponse = {
        feeAmount: 0
      };
      const feeDataResponse = (feeData && feeData[0]) || defaultFeeDataResponse; // get the first found regardless interchange
      delete feeDataResponse.interchange;
      delete feeDataResponse.feeCode;
      return hapiResponse.response(feeDataResponse).code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'OK'
          },
          [Http.StatusCode.BAD_REQUEST]: {
            description: 'Bad request'
          },
          [Http.StatusCode.INTERNAL_SERVER_ERROR]: {
            description: 'Internal server error'
          }
        }
      }
    }
  }
};

const privateGetFeeAmount: hapi.ServerRoute = {
  ...getFeeAmount,
  path: '/private/fees'
};

const feeController: hapi.ServerRoute[] = [getFeeAmount, privateGetFeeAmount];
export default feeController;
