import * as Joi from '@hapi/joi';
import { JoiValidator } from '@dk/module-common';

const MAX_NO_TRANSACTION = 999;
const ZERO_AMOUNT = 0;
const noTractionValidator = Joi.number()
  .min(0)
  .max(MAX_NO_TRANSACTION)
  .required();

export const feeAmountRequestValidator = Joi.object({
  feeRuleCode: Joi.string()
    .max(40)
    .required()
    .description('Fee rule code'),
  transactionAmount: JoiValidator.currency()
    .required()
    .description('Transaction AMount'),
  todayAccumulationAmount: JoiValidator.currency()
    .allow(ZERO_AMOUNT)
    .required()
    .description(
      'Total amount of transactions done against particular limit group code for a day'
    ),
  monthlyAccumulationAmount: JoiValidator.currency()
    .allow(ZERO_AMOUNT)
    .required()
    .description(
      'Total amount of transactions done against particular limit group code for a month'
    ),
  totalAccumulationAmount: JoiValidator.currency()
    .allow(ZERO_AMOUNT)
    .required()
    .description(
      'Total amount of transactions done against particular limit group code till date'
    ),
  todayAccumulationNoTransaction: noTractionValidator.description(
    'Total number of transactions done for a day'
  ),
  monthlyNoTransaction: noTractionValidator.description(
    'Total number of transactions done for a month'
  ),
  totalAccumulationNoTransaction: noTractionValidator.description(
    'Total number of transactions done till date'
  ),
  customerId: JoiValidator.optionalString().description('Customer ID')
}).label('FeePayload');

export const feeAmountResponseValidator = Joi.object({
  feeAmount: Joi.number()
    .required()
    .description('Fee amount'),
  customerTc: Joi.string().description('Fee transaction code'),
  customerTcChannel: Joi.string().description('Fee Transaction code'),
  subsidiaryAmount: JoiValidator.currency().description(
    'Amount paid by Jago Bank'
  ),
  debitGlNumber: JoiValidator.optionalString().description(
    'General ledger debit account number'
  ),
  creditGlNumber: JoiValidator.optionalString().description(
    'General ledger credit account number'
  ),
  subsidiary: Joi.boolean().description(
    `Subsidiary 
      * True
      * False`
  )
}).label('Fee');
