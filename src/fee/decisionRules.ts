import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { BankNetworkEnum } from '@dk/module-common';
import {
  BasicFeeMapping,
  FeeRuleInfo,
  FeeRuleInput,
  IFeeRule,
  IFeeRuleType
} from './fee.type';
import { COUNTER, feeRuleMapping, FeeRuleType } from './fee.constant';
import configurationRepository from '../configuration/configuration.repository';
import logger, { wrapLogs } from '../logger';
import customerCache from '../customer/customer.cache';
import { CustomerType } from '../customer/customer.enum';
import { UsageCounter } from '../award/award.type';

import { CustomerEntitlementManagerFactory } from '../customerEntitlementManager/customerEntitlement.manager.factory';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

const getBasicFee = async (fees: IFeeRuleType): Promise<IFeeRule> => {
  if (fees.basicFeeMapping) {
    await Promise.all(
      fees.basicFeeMapping.map(async basicFeeMapping => {
        const basicFeeCode = await configurationRepository.getBasicFees(
          basicFeeMapping.basicFeeCode
        );
        if (!basicFeeCode) {
          const detail = `Basic fee code: ${basicFeeMapping.basicFeeCode} not found!`;
          logger.error(`getBasicFee: ${detail}`);
          throw new TransferAppError(
            detail,
            TransferFailureReasonActor.MS_TRANSFER,
            ERROR_CODE.INVALID_REGISTER_PARAMETER
          );
        }
        basicFeeMapping.basicFee = basicFeeCode;
      })
    );
    await Promise.all(
      fees.basicFeeMapping.map(async basicFeeMapping => {
        if (
          basicFeeMapping.basicFee &&
          !basicFeeMapping.basicFee.customerTcInfo
        ) {
          const transctionCode = await configurationRepository.getTransactionCode(
            basicFeeMapping.basicFee.customerTc
          );
          if (!transctionCode) {
            const detail = `Transaction code: ${basicFeeMapping.basicFee.customerTc} not found!`;
            logger.error(`getBasicFee: ${detail}`);
            throw new TransferAppError(
              detail,
              TransferFailureReasonActor.MS_TRANSFER,
              ERROR_CODE.INVALID_REGISTER_PARAMETER
            );
          }
          basicFeeMapping.basicFee.customerTcInfo = {
            channel: transctionCode.channel
          };
        }
      })
    );
  }
  return fees as IFeeRule;
};

const getFeeRuleBase = async (
  feeRuleObject: FeeRuleInfo,
  basicFeeMapping?: BasicFeeMapping[]
): Promise<IFeeRule> => {
  // request all available
  if (
    !feeRuleObject.interchange ||
    !basicFeeMapping ||
    !basicFeeMapping.length
  ) {
    return getBasicFee({
      ...feeRuleMapping[feeRuleObject.code],
      basicFeeMapping
    });
  }
  // get by specific interchange
  let returnBasicFeeMapping = basicFeeMapping.filter(
    mappingInfo => feeRuleObject.interchange === mappingInfo.interchange
  );
  // fall back to default if interchange not found
  if (!returnBasicFeeMapping.length) {
    returnBasicFeeMapping = basicFeeMapping.filter(
      mappingInfo => !mappingInfo.interchange
    );
  }
  if (!returnBasicFeeMapping.length) {
    throw new TransferAppError(
      `Transfer interchange fee rule not configured!`,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.TRANSFER_INTERCHANGE_FEE_RULE_NOT_CONFIGURED
    );
  }
  return getBasicFee({
    ...feeRuleMapping[feeRuleObject.code],
    basicFeeMapping: returnBasicFeeMapping
  });
};

const getFeeRuleBasedOnMonthlyNoTransaction = async (
  feeRuleObject: FeeRuleInfo
): Promise<IFeeRule> => {
  const custEntitlementMgr = await CustomerEntitlementManagerFactory.getCustomerEntitlementManagerByFlag();

  const basicFeeMapping = await custEntitlementMgr.determineBasicFeeMapping(
    feeRuleObject
  );

  return getFeeRuleBase(feeRuleObject, basicFeeMapping);
};

const getRtolAndWalletCounterCode = async (
  thresholdCounter: string | undefined,
  customerId?: string
): Promise<string> => {
  if (
    customerId &&
    (await customerCache.getCustomerType(customerId)) ===
      CustomerType.BUSINESS_INDIVIDUAL
  ) {
    return COUNTER.COUNTER_MFS_01;
  }
  return thresholdCounter || COUNTER.COUNTER_01;
};

const calculateFeeRule = async (
  feeRuleInput: FeeRuleInput,
  customerId?: string,
  awardGroupCounter?: string | undefined,
  usageCounters?: UsageCounter[]
): Promise<IFeeRule> => {
  logger.info(`calculateFeeRule: getting fee rule`);

  //based on the feature flag, get the appropriate customerEntitlementManager
  const custEntitlementManager = await CustomerEntitlementManagerFactory.getCustomerEntitlementManagerByFlag();

  const {
    code,
    monthlyNoTransaction,
    interchange,
    targetBankCode,
    thresholdCounter
  } = feeRuleInput;

  switch (code) {
    case FeeRuleType.LOCAL_BALANCE_INQUIRY_FEE:
      let localBalanceInquiryFeeObject: FeeRuleInfo = {
        code,
        basicFeeCode2: [
          {
            interchange: BankNetworkEnum.ARTAJASA,
            basicFeeCode: 'BL001'
          },
          {
            interchange: BankNetworkEnum.ALTO,
            basicFeeCode: 'BL002'
          }
        ],
        interchange
      };

      return await getFeeRuleBase(
        localBalanceInquiryFeeObject,
        localBalanceInquiryFeeObject.basicFeeCode2
      );
    case FeeRuleType.RTOL_TRANSFER_ON_OTHER_ATM_FEE_RULE:
      let rtolTransferOnOtherAtmFeeRuleObject: FeeRuleInfo = {
        code,
        basicFeeCode2: [
          {
            interchange: BankNetworkEnum.ARTAJASA,
            basicFeeCode: 'TF003'
          },
          {
            interchange: BankNetworkEnum.ALTO,
            basicFeeCode: 'TF004'
          }
        ],
        interchange
      };

      return await getFeeRuleBase(
        rtolTransferOnOtherAtmFeeRuleObject,
        rtolTransferOnOtherAtmFeeRuleObject.basicFeeCode2
      );
    case FeeRuleType.BILL_PAYMENT_FEE:
      if (!targetBankCode) {
        const detail = `Calculating fee rule targetBankCode not provided!`;
        logger.error(`calculateFeeRule: ${detail}`);
        throw new TransferAppError(
          detail,
          TransferFailureReasonActor.SUBMITTER,
          ERROR_CODE.TRANSFER_FEE_RULE_INVALID_MANDATORY_FIELDS
        );
      }
      return getBasicFee({
        ...feeRuleMapping[code],
        basicFeeMapping: [
          {
            basicFeeCode: targetBankCode
          }
        ]
      });
    case FeeRuleType.RTOL_TRANSFER_FEE_RULE:
      let rtolTransferFeeRuleObject: FeeRuleInfo = {
        code,
        counterCode: await getRtolAndWalletCounterCode(
          thresholdCounter,
          customerId
        ),
        basicFeeCode1: [
          {
            interchange: BankNetworkEnum.ARTAJASA,
            basicFeeCode: 'TF015'
          },
          {
            interchange: BankNetworkEnum.ALTO,
            basicFeeCode: 'TF008'
          }
        ],
        basicFeeCode2: [
          {
            interchange: BankNetworkEnum.ARTAJASA,
            basicFeeCode: 'TF010'
          },
          {
            interchange: BankNetworkEnum.ALTO,
            basicFeeCode: 'TF009'
          }
        ],
        monthlyNoTransaction,
        interchange
      };

      rtolTransferFeeRuleObject = custEntitlementManager.processFeeRuleInfo(
        rtolTransferFeeRuleObject,
        code,
        awardGroupCounter,
        usageCounters
      );

      return await getFeeRuleBasedOnMonthlyNoTransaction(
        rtolTransferFeeRuleObject
      );
    case FeeRuleType.LOCAL_ATM_WITHDRAWAL_RULE:
      let localAtmFeeRuleInfo: FeeRuleInfo = {
        code,
        counterCode: thresholdCounter || COUNTER.COUNTER_07,
        basicFeeCode1: [
          {
            interchange: BankNetworkEnum.ARTAJASA,
            basicFeeCode: 'CW007'
          },
          {
            interchange: BankNetworkEnum.ALTO,
            basicFeeCode: 'CW005'
          }
        ],
        basicFeeCode2: [
          {
            interchange: BankNetworkEnum.ARTAJASA,
            basicFeeCode: 'CW001'
          },
          {
            interchange: BankNetworkEnum.ALTO,
            basicFeeCode: 'CW002'
          }
        ],
        monthlyNoTransaction,
        interchange
      };

      localAtmFeeRuleInfo = custEntitlementManager.processFeeRuleInfo(
        localAtmFeeRuleInfo,
        code,
        awardGroupCounter,
        usageCounters
      );

      return await getFeeRuleBasedOnMonthlyNoTransaction(localAtmFeeRuleInfo);
    case FeeRuleType.OVERSEAS_ATM_WITHDRAWAL_RULE:
      let overseasAtmFeeRuleInfo: FeeRuleInfo = {
        code,
        counterCode: thresholdCounter,
        basicFeeCode1: [
          {
            basicFeeCode: 'CW004'
          }
        ],
        basicFeeCode2: [
          {
            basicFeeCode: 'CW003'
          }
        ],
        monthlyNoTransaction,
        interchange
      };

      return thresholdCounter
        ? getFeeRuleBasedOnMonthlyNoTransaction(overseasAtmFeeRuleInfo)
        : getFeeRuleBase(overseasAtmFeeRuleInfo, [
            {
              basicFeeCode: 'CW003'
            }
          ]);
    case FeeRuleType.WALLET_FEE_RULE:
      let walletTransferFeeRuleObject: FeeRuleInfo = {
        code,
        counterCode: await getRtolAndWalletCounterCode(
          thresholdCounter,
          customerId
        ),
        basicFeeCode1: [
          {
            interchange: BankNetworkEnum.ARTAJASA,
            basicFeeCode: 'BF011'
          },
          {
            interchange: BankNetworkEnum.ALTO,
            basicFeeCode: 'BF004'
          }
        ],
        basicFeeCode2: [
          {
            interchange: BankNetworkEnum.ARTAJASA,
            basicFeeCode: 'BF005'
          },
          {
            interchange: BankNetworkEnum.ALTO,
            basicFeeCode: 'BF003'
          }
        ],
        monthlyNoTransaction,
        interchange
      };

      walletTransferFeeRuleObject = custEntitlementManager.processFeeRuleInfo(
        walletTransferFeeRuleObject,
        code,
        awardGroupCounter,
        usageCounters
      );

      return await getFeeRuleBasedOnMonthlyNoTransaction(
        walletTransferFeeRuleObject
      );
    case FeeRuleType.IRIS_TRANSFER_RULE:
      let irisTransferFeeRuleObject: FeeRuleInfo = {
        code,
        counterCode: thresholdCounter || COUNTER.COUNTER_01,
        basicFeeCode1: [
          {
            basicFeeCode: 'TF011'
          }
        ],
        basicFeeCode2: [
          {
            basicFeeCode: 'TF012'
          }
        ],
        monthlyNoTransaction
      };

      return await getFeeRuleBasedOnMonthlyNoTransaction(
        irisTransferFeeRuleObject
      );
    case FeeRuleType.SKN_TRANSFER_FEE_RULE:
      let sknFeeRuleInfo: FeeRuleInfo = {
        code,
        counterCode: thresholdCounter || COUNTER.COUNTER_08,
        basicFeeCode1: [
          {
            basicFeeCode: 'TF013'
          }
        ],
        basicFeeCode2: [
          {
            basicFeeCode: 'TF005'
          }
        ],
        monthlyNoTransaction,
        interchange
      };

      return await getFeeRuleBasedOnMonthlyNoTransaction(sknFeeRuleInfo);
    case FeeRuleType.RTGS_TRANSFER_FEE_RULE:
      let rtgsFeeRuleInfo: FeeRuleInfo = {
        code,
        counterCode: thresholdCounter || COUNTER.COUNTER_09,
        basicFeeCode1: [
          {
            basicFeeCode: 'TF014'
          }
        ],
        basicFeeCode2: [
          {
            basicFeeCode: 'TF006'
          }
        ],
        monthlyNoTransaction,
        interchange
      };

      return await getFeeRuleBasedOnMonthlyNoTransaction(rtgsFeeRuleInfo);
    case FeeRuleType.CASHBACK_TAX_RULE:
      let cashbackFeeRuleObject: FeeRuleInfo = {
        code,
        basicFeeCode2: [
          {
            interchange: BankNetworkEnum.JAGO,
            basicFeeCode: 'CBC01'
          },
          {
            interchange: BankNetworkEnum.PARTNER,
            basicFeeCode: 'CBC02'
          },
          {
            interchange: BankNetworkEnum.VISA,
            basicFeeCode: 'CBC03'
          },
          {
            interchange: BankNetworkEnum.GIVEAWAY,
            basicFeeCode: 'CBC04'
          }
        ],
        interchange
      };

      return await getFeeRuleBase(
        cashbackFeeRuleObject,
        cashbackFeeRuleObject.basicFeeCode2
      );
    case FeeRuleType.OFFER_TAX_RULE:
      let offerFeeRuleObject: FeeRuleInfo = {
        code,
        basicFeeCode2: [
          {
            interchange: BankNetworkEnum.TAX,
            basicFeeCode: 'OFC01'
          },
          {
            interchange: BankNetworkEnum.NOTAX,
            basicFeeCode: 'OFC02'
          },
          {
            interchange: BankNetworkEnum.PARTNER,
            basicFeeCode: 'OFC03'
          },
          {
            interchange: BankNetworkEnum.OPERATION,
            basicFeeCode: 'OFC04'
          }
        ],
        interchange
      };

      return await getFeeRuleBase(
        offerFeeRuleObject,
        offerFeeRuleObject.basicFeeCode2
      );
    case FeeRuleType.SKN_SHARIA_FEE_RULE:
      let sknShariaFeeRuleInfo: FeeRuleInfo = {
        code,
        counterCode: thresholdCounter || COUNTER.COUNTER_08,
        basicFeeCode1: [
          {
            basicFeeCode: 'TF017'
          }
        ],
        basicFeeCode2: [
          {
            basicFeeCode: 'TF018'
          }
        ],
        monthlyNoTransaction,
        interchange
      };

      return await getFeeRuleBasedOnMonthlyNoTransaction(sknShariaFeeRuleInfo);

    case FeeRuleType.RTOL_TRANSFER_FOR_BUSINESS_FEE_RULE:
      let rtolTransferForBusinessFeeRuleObject: FeeRuleInfo = {
        code,
        counterCode: thresholdCounter || COUNTER.COUNTER_01,
        basicFeeCode2: [
          {
            interchange: BankNetworkEnum.ARTAJASA,
            basicFeeCode: 'TF010'
          },
          {
            interchange: BankNetworkEnum.ALTO,
            basicFeeCode: 'TF009'
          }
        ],
        monthlyNoTransaction,
        interchange
      };

      return await getFeeRuleBase(
        rtolTransferForBusinessFeeRuleObject,
        rtolTransferForBusinessFeeRuleObject.basicFeeCode2
      );
    case FeeRuleType.BIFAST_FEE_RULE:
      let biFastFeeRuleObject: FeeRuleInfo = {
        code,
        counterCode: await getRtolAndWalletCounterCode(
          thresholdCounter,
          customerId
        ),
        basicFeeCode1: [
          {
            basicFeeCode: 'TF022'
          }
        ],
        basicFeeCode2: [
          {
            basicFeeCode: 'TF020'
          }
        ],
        monthlyNoTransaction,
        interchange
      };

      biFastFeeRuleObject = custEntitlementManager.processFeeRuleInfo(
        biFastFeeRuleObject,
        code,
        awardGroupCounter,
        usageCounters
      );

      return await getFeeRuleBasedOnMonthlyNoTransaction(biFastFeeRuleObject);

    case FeeRuleType.BIFAST_SHARIA_FEE_RULE:
      let biFastShariaFeeRuleObject: FeeRuleInfo = {
        code,
        counterCode: await getRtolAndWalletCounterCode(
          thresholdCounter,
          customerId
        ),
        basicFeeCode1: [
          {
            basicFeeCode: 'TF023'
          }
        ],
        basicFeeCode2: [
          {
            basicFeeCode: 'TF021'
          }
        ],
        monthlyNoTransaction,
        interchange
      };

      biFastShariaFeeRuleObject = custEntitlementManager.processFeeRuleInfo(
        biFastShariaFeeRuleObject,
        code,
        awardGroupCounter,
        usageCounters
      );

      return await getFeeRuleBasedOnMonthlyNoTransaction(
        biFastShariaFeeRuleObject
      );
    case FeeRuleType.INTEREST_TAX_RULE:
      let interestTaxFeeRuleObject: FeeRuleInfo = {
        code,
        basicFeeCode2: [
          {
            interchange: BankNetworkEnum.LFS,
            basicFeeCode: 'IBC01'
          },
          {
            interchange: BankNetworkEnum.MFS,
            basicFeeCode: 'IBC02'
          }
        ],
        monthlyNoTransaction,
        interchange
      };

      return await getFeeRuleBase(
        interestTaxFeeRuleObject,
        interestTaxFeeRuleObject.basicFeeCode2
      );
  }
  return getBasicFee(feeRuleMapping[code]);
};

const getFeeRule = async (
  feeRuleCode: string,
  transactionAmount: number,
  monthlyNoTransaction?: number,
  interchange?: BankNetworkEnum,
  targetBankCode?: string,
  thresholdCounter?: string,
  customerId?: string,
  awardGroupCounter?: string | undefined,
  usageCounters?: UsageCounter[]
): Promise<IFeeRule | undefined> => {
  const code: FeeRuleType = feeRuleCode as FeeRuleType;
  let transactionCount = monthlyNoTransaction || 0;
  ++transactionCount;

  return calculateFeeRule(
    {
      code,
      monthlyNoTransaction: transactionCount,
      interchange,
      targetBankCode,
      thresholdCounter,
      transactionAmount
    },
    customerId,
    awardGroupCounter,
    usageCounters
  );
};

const decisionRules = {
  getFeeRule
};

export default wrapLogs(decisionRules);
