import { fixedSubsidiary, percentageSubsidiary } from '../subsidiary.template';
import { ERROR_CODE } from '../../common/errors';
import {
  feeRuleTemplatePayloadForFixedFee,
  feeRuleTemplatePayloadForPercentageFee
} from '../__mocks__/fee.data';

jest.mock('../../billPayment/billDetail/billDetail.service');
jest.mock('../../transaction/transaction.repository');

describe('subsidiary template', () => {
  describe('fixedSubsidiary', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });
    describe('check if isApplicable', () => {
      it(`should return true when subsidiaryAmount is number`, () => {
        //Given
        const fixedFeeData = feeRuleTemplatePayloadForFixedFee();
        // when
        const isApplicable = fixedSubsidiary.isApplicable(fixedFeeData);

        // then
        expect(isApplicable).toBeTruthy();
      });

      it(`should return false when subsidiaryAmount is not present`, () => {
        //Given
        const fixedFeeData = feeRuleTemplatePayloadForFixedFee();
        delete fixedFeeData.basicFee.subsidiaryAmount;
        // when
        const isApplicable = fixedSubsidiary.isApplicable(fixedFeeData);

        // then
        expect(isApplicable).toBeFalsy();
      });
    });

    describe('get fixed fee', () => {
      it(`should return subsidiaryAmount when present`, async () => {
        //Given
        const fixedFeeData = feeRuleTemplatePayloadForFixedFee();
        // when
        const result = await fixedSubsidiary.getFee(fixedFeeData);

        // then
        expect(result).toEqual(fixedFeeData.basicFee.subsidiaryAmount);
      });

      it(`should throw INVALID_SUBSIDIARY_VALUE error `, async () => {
        //Given
        const fixedFeeData = feeRuleTemplatePayloadForFixedFee();
        delete fixedFeeData.basicFee.subsidiaryAmount;
        // when
        try {
          await fixedSubsidiary.getFee(fixedFeeData);
        } catch (error) {
          // Then
          expect(error.errorCode).toEqual(ERROR_CODE.INVALID_SUBSIDIARY_VALUE);
        }
      });
    });
  });

  describe('percentageFee', () => {
    describe('check if isApplicable', () => {
      it(`should return true when subsidiaryAmountPercentage is number`, () => {
        //Given
        const percentageFeeData = feeRuleTemplatePayloadForPercentageFee();
        // when
        const applicable = percentageSubsidiary.isApplicable(percentageFeeData);

        // Then
        expect(applicable).toBeTruthy();
      });

      it(`should return false when subsidiaryAmountPercentage is not present`, () => {
        //Given
        const percentageFeeData = feeRuleTemplatePayloadForPercentageFee();
        delete percentageFeeData.basicFee.subsidiaryAmountPercentage;
        // when
        const applicable = percentageSubsidiary.isApplicable(percentageFeeData);

        // Then
        expect(applicable).toBeFalsy();
      });
    });

    describe('get percentage fee', () => {
      it(`should return fixed fee when present`, async () => {
        //Given
        const percentageFeeData = feeRuleTemplatePayloadForPercentageFee();
        const expectedResult = 250;
        // when
        const result = await percentageSubsidiary.getFee(percentageFeeData);

        // Then
        expect(result).toEqual(expectedResult);
      });

      it(`should throw  INVALID_SUBSIDIARY_VALUE error`, async () => {
        //Given
        const percentageFeeData = feeRuleTemplatePayloadForPercentageFee();
        delete percentageFeeData.basicFee.subsidiaryAmountPercentage;
        try {
          // When
          await percentageSubsidiary.getFee(percentageFeeData);
        } catch (error) {
          // Then
          expect(error.errorCode).toEqual(ERROR_CODE.INVALID_SUBSIDIARY_VALUE);
        }
      });
    });
  });
});
