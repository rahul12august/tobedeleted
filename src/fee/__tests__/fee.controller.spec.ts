import hapi = require('@hapi/hapi');
import feeController from '../fee.controller';
import { FeeAmountRequest } from '../fee.type';
import feeService from '../fee.service';
import { getFeeAmountData } from '../__mocks__/fee.data';

jest.mock('../fee.service');

describe('fee.controller', () => {
  let server: hapi.Server;
  beforeAll(async () => {
    server = new hapi.Server();
    server.route(feeController);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('POST / fees/calculations', () => {
    const feeAmountRequest: FeeAmountRequest = {
      feeRuleCode: 'OVERSEAS_ATM_WITHDRAWAL_RULE',
      transactionAmount: 5000,
      todayAccumulationAmount: 5000,
      monthlyAccumulationAmount: 5000,
      totalAccumulationAmount: 5000,
      todayAccumulationNoTransaction: 0,
      monthlyNoTransaction: 0,
      totalAccumulationNoTransaction: 0
    };

    it('should return 200 on valid payload and success service call', async () => {
      // Given
      const feeData = [getFeeAmountData()];
      (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(feeData);

      // When
      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: '/fees',
        payload: feeAmountRequest
      });

      // Then
      expect(response.statusCode).toBe(200);
      expect(feeService.getFeeAmount).toHaveBeenCalled();
    });
    it('should return 200 and default fee response if service return undefined response', async () => {
      // Given
      const defaultFeeDataResponse = {
        feeAmount: 0
      };
      (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(undefined);

      // When
      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: '/fees',
        payload: feeAmountRequest
      });

      // Then
      expect(response.statusCode).toBe(200);
      expect(response.result).toEqual(defaultFeeDataResponse);
      expect(feeService.getFeeAmount).toHaveBeenCalled();
    });

    describe('should return error if payload is wrong', () => {
      const testCases = [
        {
          ...feeAmountRequest,
          transactionAmount: 5001.123
        },
        {
          ...feeAmountRequest,
          todayAccumulationAmount: 5000.123
        },
        {
          ...feeAmountRequest,
          todayAccumulationAmount: -5000.12
        },
        {
          ...feeAmountRequest,
          monthlyAccumulationAmount: 5000.123
        },
        {
          ...feeAmountRequest,
          monthlyAccumulationAmount: -5000.12
        },
        {
          ...feeAmountRequest,
          totalAccumulationAmount: 5000.123
        },
        {
          ...feeAmountRequest,
          totalAccumulationAmount: -5000.12
        },
        {
          ...feeAmountRequest,
          todayAccumulationNoTransaction: 1000
        },
        {
          ...feeAmountRequest,
          todayAccumulationNoTransaction: -1000
        },
        {
          ...feeAmountRequest,
          monthlyNoTransaction: 1000
        },
        {
          ...feeAmountRequest,
          monthlyNoTransaction: -1000
        },
        {
          ...feeAmountRequest,
          totalAccumulationNoTransaction: 1000
        },
        {
          ...feeAmountRequest,
          totalAccumulationNoTransaction: -1000
        }
      ];

      it.each(testCases)('validations %#', async feeAmountRequestParam => {
        (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(
          getFeeAmountData()
        );

        // When
        const response: hapi.ServerInjectResponse = await server.inject({
          method: 'POST',
          url: '/fees',
          payload: feeAmountRequestParam
        });

        // Then
        expect(response.statusCode).toBe(400);
        expect(feeService.getFeeAmount).not.toBeCalled();
      });
    });
  });

  describe('POST /private/fees/calculations', () => {
    const feeAmountRequest: FeeAmountRequest = {
      feeRuleCode: 'OVERSEAS_ATM_WITHDRAWAL_RULE',
      transactionAmount: 5000,
      todayAccumulationAmount: 5000,
      monthlyAccumulationAmount: 5000,
      totalAccumulationAmount: 5000,
      todayAccumulationNoTransaction: 0,
      monthlyNoTransaction: 0,
      totalAccumulationNoTransaction: 0
    };

    it('should return 200 on valid payload and success service call', async () => {
      // Given
      const feeData = [getFeeAmountData()];
      (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(feeData);

      // When
      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: '/private/fees',
        payload: feeAmountRequest
      });

      // Then
      expect(response.statusCode).toBe(200);
      expect(feeService.getFeeAmount).toHaveBeenCalled();
    });
    it('should return 200 and default fee response if service return undefined response', async () => {
      // Given
      const defaultFeeDataResponse = {
        feeAmount: 0
      };
      (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(undefined);

      // When
      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: '/private/fees',
        payload: feeAmountRequest
      });

      // Then
      expect(response.statusCode).toBe(200);
      expect(response.result).toEqual(defaultFeeDataResponse);
      expect(feeService.getFeeAmount).toHaveBeenCalled();
    });

    describe('should return error if payload is wrong', () => {
      const testCases = [
        {
          ...feeAmountRequest,
          transactionAmount: 5001.123
        },
        {
          ...feeAmountRequest,
          todayAccumulationAmount: 5000.123
        },
        {
          ...feeAmountRequest,
          todayAccumulationAmount: -5000.12
        },
        {
          ...feeAmountRequest,
          monthlyAccumulationAmount: 5000.123
        },
        {
          ...feeAmountRequest,
          monthlyAccumulationAmount: -5000.12
        },
        {
          ...feeAmountRequest,
          totalAccumulationAmount: 5000.123
        },
        {
          ...feeAmountRequest,
          totalAccumulationAmount: -5000.12
        },
        {
          ...feeAmountRequest,
          todayAccumulationNoTransaction: 1000
        },
        {
          ...feeAmountRequest,
          todayAccumulationNoTransaction: -1000
        },
        {
          ...feeAmountRequest,
          monthlyNoTransaction: 1000
        },
        {
          ...feeAmountRequest,
          monthlyNoTransaction: -1000
        },
        {
          ...feeAmountRequest,
          totalAccumulationNoTransaction: 1000
        },
        {
          ...feeAmountRequest,
          totalAccumulationNoTransaction: -1000
        }
      ];

      it.each(testCases)('validations %#', async feeAmountRequestParam => {
        (feeService.getFeeAmount as jest.Mock).mockResolvedValueOnce(
          getFeeAmountData()
        );

        // When
        const response: hapi.ServerInjectResponse = await server.inject({
          method: 'POST',
          url: '/private/fees',
          payload: feeAmountRequestParam
        });

        // Then
        expect(response.statusCode).toBe(400);
        expect(feeService.getFeeAmount).not.toBeCalled();
      });
    });
  });
});
