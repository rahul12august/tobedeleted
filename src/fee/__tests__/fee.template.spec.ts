import faker from 'faker';
import { ERROR_CODE } from '../../common/errors';
import {
  billPaymentAdminFee,
  fixedFee,
  percentageFee,
  refundFee
} from '../fee.template';
import { getTransactionModelFullData } from '../../transaction/__mocks__/transaction.data';
import { PaymentServiceTypeEnum } from '@dk/module-common';
import transactionRepository from '../../transaction/transaction.repository';
import { sumBy } from 'lodash';
import billDetailService from '../../billPayment/billDetail/billDetail.service';
import { TransferAppError } from '../../errors/AppError';
import {
  feeRuleTemplatePayloadForFixedFee,
  feeRuleTemplatePayloadForPercentageFee,
  feeRuleTemplatePayloadForThirdPartyFee,
  feeRuleTemplatePayloadForThirdPartyFeeWithAdminFee,
  feeRuleTemplatePayloadForTokopedia
} from '../__mocks__/fee.data';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

jest.mock('../../billPayment/billDetail/billDetail.service');
jest.mock('../../transaction/transaction.repository');

describe('fee template', () => {
  describe('fixedFee', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });
    describe('check if isApplicable', () => {
      it(`should return true when feeAmountFixed is true`, () => {
        //Given
        const fixedFeeData = feeRuleTemplatePayloadForFixedFee();
        // when
        const isApplicable = fixedFee.isApplicable(fixedFeeData);

        // then
        expect(isApplicable).toBeTruthy();
      });

      it(`should return false when fixedFeeData is not present`, () => {
        //Given
        const fixedFeeData = feeRuleTemplatePayloadForFixedFee();
        delete fixedFeeData.basicFee.feeAmountFixed;
        // when
        const isApplicable = fixedFee.isApplicable(fixedFeeData);

        // then
        expect(isApplicable).toBeFalsy();
      });
    });

    describe('get fixed fee', () => {
      it(`should return fixed fee when present`, async () => {
        //Given
        const fixedFeeData = feeRuleTemplatePayloadForFixedFee();
        // when
        const result = await fixedFee.getFee(fixedFeeData);

        // then
        expect(result).toEqual(fixedFeeData.basicFee.feeAmountFixed);
      });

      it(`should throw MISSING_FIXED_FEE_CONFIGURATION error `, async () => {
        //Given
        const fixedFeeData = feeRuleTemplatePayloadForFixedFee();
        delete fixedFeeData.basicFee.feeAmountFixed;
        // when
        try {
          await fixedFee.getFee(fixedFeeData);
        } catch (error) {
          // Then
          expect(error.errorCode).toEqual(
            ERROR_CODE.MISSING_FIXED_FEE_CONFIGURATION
          );
        }
      });
    });
  });

  describe('percentageFee', () => {
    describe('check if isApplicable', () => {
      it(`should return true when feeAmountPercentage is true`, () => {
        //Given
        const percentageFeeData = feeRuleTemplatePayloadForPercentageFee();
        // when
        const applicable = percentageFee.isApplicable(percentageFeeData);

        // Then
        expect(applicable).toBeTruthy();
      });

      it(`should return false when feeAmountPercentage is not present`, () => {
        //Given
        const percentageFeeData = feeRuleTemplatePayloadForPercentageFee();
        delete percentageFeeData.basicFee.feeAmountPercentage;
        // when
        const applicable = percentageFee.isApplicable(percentageFeeData);

        // Then
        expect(applicable).toBeFalsy();
      });
    });

    describe('get percentage fee', () => {
      it(`should return fixed fee when present`, async () => {
        //Given
        const percentageFeeData = feeRuleTemplatePayloadForPercentageFee();
        // when
        const result = await percentageFee.getFee(percentageFeeData);

        // Then
        expect(result).toEqual(20);
      });

      it(`should throw  MISSING_PERCENTAGE_FEE_CONFIGURATION error`, async () => {
        //Given
        const percentageFeeData = feeRuleTemplatePayloadForPercentageFee();
        delete percentageFeeData.basicFee.feeAmountPercentage;
        try {
          // When
          await percentageFee.getFee(percentageFeeData);
        } catch (error) {
          // Then
          expect(error.errorCode).toEqual(
            ERROR_CODE.MISSING_PERCENTAGE_FEE_CONFIGURATION
          );
        }
      });
    });
  });

  describe('refundTemplate', () => {
    beforeEach(() => {
      jest.resetAllMocks();
    });

    describe('isApplicable', () => {
      it('should send true if refund is send', async () => {
        // Given
        const input = {
          ...feeRuleTemplatePayloadForFixedFee(),
          refund: { originalTransactionId: 'originalTransactionId' }
        };

        const result = refundFee.isApplicable(input);

        expect(result).toBeTruthy();
      });
    });

    describe('getFee', () => {
      let newTransactionModel = getTransactionModelFullData();
      const originalTransaction = {
        ...newTransactionModel,
        paymentServiceType: PaymentServiceTypeEnum.JAGOPAY_REFUND,
        transactionAmount: 50000,
        id: 'test12345',
        fees: [
          {
            feeCode: faker.random.alphaNumeric(4),
            customerTc: faker.random.alphaNumeric(4),
            customerTcChannel: faker.random.alphaNumeric(4),
            feeAmount: faker.random.number({ min: 0, max: 5000 })
          }
        ]
      };

      it('should send refund fees as fee amount', async () => {
        // Given
        const input = {
          ...feeRuleTemplatePayloadForFixedFee(),
          refund: { originalTransactionId: 'test12345' }
        };

        (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
          originalTransaction
        );

        const result = await refundFee.getFee(input);

        expect(result).toEqual(sumBy(originalTransaction.fees, 'feeAmount'));
      });

      it('should throw error when original transaction not found', async () => {
        // Given
        const input = {
          ...feeRuleTemplatePayloadForFixedFee(),
          refund: { originalTransactionId: 'test12345' }
        };
        (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
          null
        );

        try {
          await refundFee.getFee(input);
        } catch (err) {
          expect(err).toBeDefined();
          expect(err.errorCode).toEqual(ERROR_CODE.TRANSACTION_NOT_FOUND);
        }
      });

      it('should throw error when original transaction not send', async () => {
        // Given
        const input = {
          ...feeRuleTemplatePayloadForFixedFee()
        };
        (transactionRepository.getByKey as jest.Mock).mockResolvedValueOnce(
          null
        );

        const result = await refundFee.getFee(input);

        expect(result).toEqual(0);
        expect(transactionRepository.getByKey).not.toBeCalled();
      });
    });
  });

  describe('billPaymentAdminFee or tokopedia fee', () => {
    describe('check if isApplicable', () => {
      it(`should return true when thirdPartyFee is true`, () => {
        //Given
        const thirdPartyFeeData = feeRuleTemplatePayloadForThirdPartyFee();
        // when
        const applicable = billPaymentAdminFee.isApplicable(thirdPartyFeeData);

        // then
        expect(applicable).toBeTruthy();
      });
      it(`should return true when thirdPartyFee is true and channel is tokopedia`, () => {
        //Given
        const thirdPartyFeeData = feeRuleTemplatePayloadForTokopedia();
        // when
        const applicable = billPaymentAdminFee.isApplicable(thirdPartyFeeData);

        // then
        expect(applicable).toBeTruthy();
      });

      it(`should return false when beneficiaryBankCodeChannel is not of billDetail`, () => {
        //Given
        const thirdPartyFeeData = feeRuleTemplatePayloadForThirdPartyFee();
        thirdPartyFeeData.basicFee.thirdPartyFee = false;
        // when
        const applicable = billPaymentAdminFee.isApplicable(thirdPartyFeeData);

        // then
        expect(applicable).toBeFalsy();
      });
    });

    describe('get admin fee billDetail', () => {
      beforeEach(() => {
        jest.resetAllMocks();
      });
      it(`should return success when called with correct payload`, async () => {
        //Given
        const thirdPartyFeeData = feeRuleTemplatePayloadForThirdPartyFee();
        (billDetailService.getAdminFee as jest.Mock).mockResolvedValueOnce(
          1000
        );
        // when
        const result = await billPaymentAdminFee.getFee(thirdPartyFeeData);

        // then
        expect(result).toEqual(1000);
      });

      it(`should return unexpected error when called with wrong payload`, async () => {
        //Given
        const thirdPartyFeeData = feeRuleTemplatePayloadForThirdPartyFee();
        const transferAppError = new TransferAppError(
          'Test error!',
          TransferFailureReasonActor.UNKNOWN,
          ERROR_CODE.UNEXPECTED_ERROR
        );
        (billDetailService.getAdminFee as jest.Mock).mockRejectedValueOnce(
          transferAppError
        );

        // when
        try {
          await billPaymentAdminFee.getFee(thirdPartyFeeData);
        } catch (error) {
          // then
          expect(error).toEqual(transferAppError);
          expect(error.actor).toEqual(TransferFailureReasonActor.UNKNOWN);
          expect(error.detail).toEqual('Test error!');
          expect(error.errorCode).toEqual(ERROR_CODE.UNEXPECTED_ERROR);
        }
      });

      it(`should return missing external id error when called without external id`, async () => {
        //Given
        const thirdPartyFeeData = feeRuleTemplatePayloadForThirdPartyFee();
        thirdPartyFeeData.externalId = '';
        try {
          // when
          await billPaymentAdminFee.getFee(thirdPartyFeeData);
        } catch (error) {
          // then
          expect(error.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
          expect(error.detail).toEqual(
            'externalId is not provided for getFee!'
          );
          expect(error.errorCode).toEqual(ERROR_CODE.MISSING_INQUIRY_ID);
        }
      });

      it(`should not call bill payment service if admin fee already exist`, async () => {
        //Given
        const thirdPartyFeeData = feeRuleTemplatePayloadForThirdPartyFeeWithAdminFee();

        // when
        const result = await billPaymentAdminFee.getFee(thirdPartyFeeData);

        // then
        expect(billDetailService.getAdminFee).not.toBeCalled();
        expect(result).toEqual(2500);
      });
    });
  });
});
