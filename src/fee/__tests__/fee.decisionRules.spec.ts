import { BankNetworkEnum } from '@dk/module-common';
import configurationRepository from '../../configuration/configuration.repository';
import {
  getCounterData,
  getCountersList
} from '../../configuration/__mocks__/configuration.data';
import { FeeRuleInput } from '../fee.type';
import { ERROR_CODE } from '../../common/errors';
import decisionRules from '../decisionRules';
import { COUNTER, FeeRuleType } from '../fee.constant';
import { getBasicFeeData, getTransactionCode } from '../__mocks__/fee.data';
import customerCache from '../../customer/customer.cache';
import { CustomerType } from '../../customer/customer.enum';
import entitlementFlag from '../../common/entitlementFlag';

jest.mock('../../configuration/configuration.repository');
jest.mock('../../customer/customer.cache');

describe('feeRule service', () => {
  const basicFee = getBasicFeeData();
  delete basicFee.customerTcInfo;
  const transactionCode = getTransactionCode();
  const expectedBasicFee = {
    ...basicFee,
    customerTcInfo: { channel: transactionCode.channel }
  };
  afterEach(() => {
    expect.hasAssertions();
    jest.clearAllMocks();
  });
  describe('getFeeRule method', () => {
    afterEach(() => {
      expect.hasAssertions();
      jest.clearAllMocks();
    });

    entitlementFlag.isEnabled = jest.fn().mockResolvedValue(false);

    const testCases = [
      [
        { code: 'ZERO_FEE_RULE', monthlyNoTransaction: 0 },
        {
          description: 'Zero Fee Rule'
        }
      ],
      [
        { code: 'RTOL_TRANSFER_FEE_RULE', monthlyNoTransaction: 0 },
        {
          description: 'RTOL Transfer Fee Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF015',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            },
            {
              basicFeeCode: 'TF008',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTOL_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 0,
          interchange: BankNetworkEnum.ALTO
        },
        {
          description: 'RTOL Transfer Fee Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF008',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTOL_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 31,
          interchange: BankNetworkEnum.ALTO
        },
        {
          description: 'RTOL Transfer Fee Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF009',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTOL_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 31,
          interchange: BankNetworkEnum.ARTAJASA
        },
        {
          description: 'RTOL Transfer Fee Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF010',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'LOCAL_ATM_WITHDRAWAL_RULE',
          monthlyNoTransaction: 0,
          interchange: BankNetworkEnum.ALTO
        },
        {
          description: 'Local ATM Withdrawal Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'CW005',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'LOCAL_ATM_WITHDRAWAL_RULE',
          monthlyNoTransaction: 0,
          interchange: BankNetworkEnum.ARTAJASA,
          thresholdCounter: 'AWARD_TRANSFER_001'
        },
        {
          description: 'Local ATM Withdrawal Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'CW007',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'LOCAL_ATM_WITHDRAWAL_RULE',
          monthlyNoTransaction: 6,
          interchange: BankNetworkEnum.ARTAJASA,
          thresholdCounter: 'AWARD_TRANSFER_001'
        },
        {
          description: 'Local ATM Withdrawal Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'CW001',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'LOCAL_ATM_WITHDRAWAL_RULE',
          monthlyNoTransaction: 6,
          interchange: BankNetworkEnum.ALTO,
          thresholdCounter: 'AWARD_TRANSFER_001'
        },
        {
          description: 'Local ATM Withdrawal Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'CW002',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        { code: 'GOPAY_TOP_UP_RULE', monthlyNoTransaction: undefined },
        {
          description: 'Gopay Top up Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'BF001',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTOL_TRANSFER_ON_OTHER_ATM_FEE_RULE',
          interchange: BankNetworkEnum.ALTO
        },
        {
          description: 'RTOL Transfer on other ATM Fee Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF004',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTOL_TRANSFER_ON_OTHER_ATM_FEE_RULE',
          interchange: BankNetworkEnum.ARTAJASA
        },
        {
          description: 'RTOL Transfer on other ATM Fee Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF003',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'OVERSEAS_ATM_WITHDRAWAL_RULE',
          monthlyNoTransaction: 3,
          thresholdCounter: 'AWARD_OVERSEAS_CASHWITHDRAWAL_001'
        },
        {
          description: 'Overseas ATM Withdrawal Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'CW004',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'OVERSEAS_ATM_WITHDRAWAL_RULE',
          monthlyNoTransaction: 6,
          thresholdCounter: 'AWARD_OVERSEAS_CASHWITHDRAWAL_001'
        },
        {
          description: 'Overseas ATM Withdrawal Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'CW003',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'OVERSEAS_ATM_WITHDRAWAL_RULE',
          monthlyNoTransaction: 3,
          thresholdCounter: ''
        },
        {
          description: 'Overseas ATM Withdrawal Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'CW003',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'LOCAL_BALANCE_INQUIRY_FEE',
          interchange: BankNetworkEnum.ALTO
        },
        {
          description: 'Local Balance Inquiry Fee',
          basicFeeMapping: [
            {
              basicFeeCode: 'BL002',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'LOCAL_BALANCE_INQUIRY_FEE',
          interchange: BankNetworkEnum.ARTAJASA
        },
        {
          description: 'Local Balance Inquiry Fee',
          basicFeeMapping: [
            {
              basicFeeCode: 'BL001',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'OVERSEAS_BALANCE_INQUIRY_FEE',
          monthlyNoTransaction: undefined
        },
        {
          description: 'Overseas Balance Inquiry Fee',
          basicFeeMapping: [
            {
              basicFeeCode: 'BL003',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        { code: 'PENALTY_FEE', monthlyNoTransaction: undefined },
        {
          description: 'TD Penalty',
          basicFeeMapping: [
            {
              basicFeeCode: 'PF001',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'BILL_PAYMENT_FEE',
          monthlyNoTransaction: undefined,
          targetBankCode: 'BCtest'
        },
        {
          description: 'Bill payment fee',
          basicFeeMapping: [
            {
              basicFeeCode: 'BCtest',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        { code: 'WALLET_FEE_RULE', monthlyNoTransaction: 0 },
        {
          description: 'Wallet Top up Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'BF011',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            },
            {
              basicFeeCode: 'BF004',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'WALLET_FEE_RULE',
          monthlyNoTransaction: 0,
          interchange: BankNetworkEnum.ALTO
        },
        {
          description: 'Wallet Top up Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'BF004',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'WALLET_FEE_RULE',
          monthlyNoTransaction: 31,
          interchange: BankNetworkEnum.ALTO
        },
        {
          description: 'Wallet Top up Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'BF003',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'WALLET_FEE_RULE',
          monthlyNoTransaction: 31,
          interchange: BankNetworkEnum.ARTAJASA
        },
        {
          description: 'Wallet Top up Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'BF005',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'IRIS_TRANSFER_RULE',
          monthlyNoTransaction: 0
        },
        {
          description: 'IRIS Transfer Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF011',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'IRIS_TRANSFER_RULE',
          monthlyNoTransaction: 26
        },
        {
          description: 'IRIS Transfer Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF012',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'SKN_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 0
        },
        {
          description: 'SKN transfer fee rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF013',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'SKN_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 6
        },
        {
          description: 'SKN transfer fee rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF005',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTGS_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 0
        },
        {
          description: 'RTGS transfer fee rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF014',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTGS_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 4
        },
        {
          description: 'RTGS transfer fee rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF006',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'CASHBACK_TAX_RULE',
          monthlyNoTransaction: undefined,
          interchange: BankNetworkEnum.JAGO
        },
        {
          description: 'Tax on Cashback',
          basicFeeMapping: [
            {
              basicFeeCode: 'CBC01',
              interchange: BankNetworkEnum.JAGO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'CASHBACK_TAX_RULE',
          monthlyNoTransaction: undefined,
          interchange: BankNetworkEnum.PARTNER
        },
        {
          description: 'Tax on Cashback',
          basicFeeMapping: [
            {
              basicFeeCode: 'CBC02',
              interchange: BankNetworkEnum.PARTNER,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'CASHBACK_TAX_RULE',
          monthlyNoTransaction: undefined,
          interchange: BankNetworkEnum.VISA
        },
        {
          description: 'Tax on Cashback',
          basicFeeMapping: [
            {
              basicFeeCode: 'CBC03',
              interchange: BankNetworkEnum.VISA,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'CASHBACK_TAX_RULE',
          monthlyNoTransaction: undefined,
          interchange: BankNetworkEnum.GIVEAWAY
        },
        {
          description: 'Tax on Cashback',
          basicFeeMapping: [
            {
              basicFeeCode: 'CBC04',
              interchange: BankNetworkEnum.GIVEAWAY,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'OFFER_TAX_RULE',
          interchange: BankNetworkEnum.TAX
        },
        {
          description: 'Offer Tax Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'OFC01',
              interchange: BankNetworkEnum.TAX,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'OFFER_TAX_RULE',
          interchange: BankNetworkEnum.PARTNER
        },
        {
          description: 'Offer Tax Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'OFC03',
              interchange: BankNetworkEnum.PARTNER,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'OFFER_TAX_RULE',
          interchange: BankNetworkEnum.OPERATION
        },
        {
          description: 'Offer Tax Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'OFC04',
              interchange: BankNetworkEnum.OPERATION,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'OFFER_TAX_RULE',
          interchange: BankNetworkEnum.NOTAX
        },
        {
          description: 'Offer Tax Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'OFC02',
              interchange: BankNetworkEnum.NOTAX,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'OFFER_TAX_RULE'
        },
        {
          description: 'Offer Tax Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'OFC01',
              interchange: BankNetworkEnum.TAX,
              basicFee: expectedBasicFee
            },
            {
              basicFeeCode: 'OFC02',
              interchange: BankNetworkEnum.NOTAX,
              basicFee: expectedBasicFee
            },
            {
              basicFeeCode: 'OFC03',
              interchange: BankNetworkEnum.PARTNER,
              basicFee: expectedBasicFee
            },
            {
              basicFeeCode: 'OFC04',
              interchange: BankNetworkEnum.OPERATION,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'BRANCH_SKN_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 6
        },
        {
          description: 'Branch SKN transfer fee rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF005',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'BRANCH_RTGS_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 4
        },
        {
          description: 'Branch RTGS transfer fee rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF006',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'SKN_SHARIA_FEE_RULE',
          monthlyNoTransaction: 6
        },
        {
          description: 'Fee Rules for Sharia SKN Transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF018',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'SKN_SHARIA_FEE_RULE',
          monthlyNoTransaction: 0
        },
        {
          description: 'Fee Rules for Sharia SKN Transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF017',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTGS_SHARIA_FEE_RULE',
          monthlyNoTransaction: 0
        },
        {
          description: 'Fee Rules for Sharia RTGS Transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF019',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTGS_SHARIA_FEE_RULE',
          monthlyNoTransaction: 6
        },
        {
          description: 'Fee Rules for Sharia RTGS Transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF019',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'DIGITAL_GOODS_FEE',
          monthlyNoTransaction: 6
        },
        {
          description: 'Fee Rules for Digital Goods',
          basicFeeMapping: [
            {
              basicFeeCode: 'DG00',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTOL_TRANSFER_FOR_BUSINESS_FEE_RULE',
          monthlyNoTransaction: 0
        },
        {
          description: 'Fee rules for BFS RTOL transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF010',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            },
            {
              basicFeeCode: 'TF009',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTOL_TRANSFER_FOR_BUSINESS_FEE_RULE',
          monthlyNoTransaction: 0,
          interchange: BankNetworkEnum.ALTO
        },
        {
          description: 'Fee rules for BFS RTOL transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF009',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTOL_TRANSFER_FOR_BUSINESS_FEE_RULE',
          monthlyNoTransaction: 31,
          interchange: BankNetworkEnum.ALTO
        },
        {
          description: 'Fee rules for BFS RTOL transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF009',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTOL_TRANSFER_FOR_BUSINESS_FEE_RULE',
          monthlyNoTransaction: 31,
          interchange: BankNetworkEnum.ARTAJASA
        },
        {
          description: 'Fee rules for BFS RTOL transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF010',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'SKN_TRANSFER_FOR_BUSINESS_FEE_RULE'
        },
        {
          description: 'Fee rules for BFS SKN transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF005',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTGS_TRANSFER_FOR_BUSINESS_FEE_RULE'
        },
        {
          description: 'Fee rules for BFS RTGS transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF006',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'BIFAST_FEE_RULE',
          transactionAmount: 50000001
        },
        {
          description: 'Fee Rules for BI Fast Transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF022',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'BIFAST_FEE_RULE',
          transactionAmount: 4000000,
          monthlyNoTransaction: 30
        },
        {
          description: 'Fee Rules for BI Fast Transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF020',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'BIFAST_FEE_RULE',
          transactionAmount: 4000000,
          monthlyNoTransaction: 20
        },
        {
          description: 'Fee Rules for BI Fast Transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF022',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'BIFAST_SHARIA_FEE_RULE',
          transactionAmount: 50000001
        },
        {
          description: 'Fee Rules for BI Fast Sharia Transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF023',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'BIFAST_SHARIA_FEE_RULE',
          transactionAmount: 4000000,
          monthlyNoTransaction: 30
        },
        {
          description: 'Fee Rules for BI Fast Sharia Transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF021',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'BIFAST_SHARIA_FEE_RULE',
          transactionAmount: 4000000,
          monthlyNoTransaction: 20
        },
        {
          description: 'Fee Rules for BI Fast Sharia Transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF023',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'BIFAST_TRANSFER_FOR_BUSINESS_FEE_RULE'
        },
        {
          description: 'Fee rules for BFS BIFAST Transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF022',
              basicFee: expectedBasicFee
            }
          ]
        }
      ]
    ];

    test.each(testCases)(
      'when feeRuleInput is %o return %o',
      async (feeRuleInput, expectedFeeRule) => {
        // Given
        const {
          code,
          monthlyNoTransaction,
          transactionAmount,
          interchange,
          targetBankCode,
          thresholdCounter
        } = feeRuleInput as FeeRuleInput;
        (configurationRepository.getCounterByCode as jest.Mock).mockImplementation(
          counter => getCountersList(counter)
        );
        (configurationRepository.getBasicFees as jest.Mock).mockImplementation(
          () => basicFee
        );
        (configurationRepository.getTransactionCode as jest.Mock).mockImplementation(
          () => transactionCode
        );

        // When
        const actualFeeRule = await decisionRules.getFeeRule(
          code,
          transactionAmount,
          monthlyNoTransaction,
          interchange,
          targetBankCode,
          thresholdCounter
        );

        // Then
        expect(actualFeeRule).toEqual(expectedFeeRule);
      }
    );

    test.each([
      [
        {
          code: 'RTOL_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 0
        },
        { customerId: '1' },
        {
          description: 'RTOL Transfer Fee Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF015',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            },
            {
              basicFeeCode: 'TF008',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTOL_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 0,
          interchange: BankNetworkEnum.ALTO
        },
        { customerId: '1' },
        {
          description: 'RTOL Transfer Fee Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF008',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTOL_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 101,
          interchange: BankNetworkEnum.ALTO
        },
        { customerId: '1' },
        {
          description: 'RTOL Transfer Fee Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF009',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTOL_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 101,
          interchange: BankNetworkEnum.ARTAJASA
        },
        { customerId: '1' },
        {
          description: 'RTOL Transfer Fee Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF010',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        { code: 'WALLET_FEE_RULE', monthlyNoTransaction: 0 },
        { customerId: '1' },
        {
          description: 'Wallet Top up Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'BF011',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            },
            {
              basicFeeCode: 'BF004',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'WALLET_FEE_RULE',
          monthlyNoTransaction: 0,
          interchange: BankNetworkEnum.ALTO
        },
        { customerId: '1' },
        {
          description: 'Wallet Top up Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'BF004',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'WALLET_FEE_RULE',
          monthlyNoTransaction: 101,
          interchange: BankNetworkEnum.ALTO
        },
        { customerId: '1' },
        {
          description: 'Wallet Top up Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'BF003',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'WALLET_FEE_RULE',
          monthlyNoTransaction: 101,
          interchange: BankNetworkEnum.ARTAJASA
        },
        { customerId: '1' },
        {
          description: 'Wallet Top up Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'BF005',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            }
          ]
        }
      ]
    ] as any[][])(
      'when feeRuleInput is %o customerData %o return %o',
      async (feeRuleInput, customerData, expectedFeeRule) => {
        // Given
        const {
          code,
          monthlyNoTransaction,
          interchange,
          targetBankCode,
          thresholdCounter
        } = feeRuleInput as FeeRuleInput;

        (customerCache.getCustomerType as jest.Mock).mockResolvedValueOnce(
          CustomerType.BUSINESS_INDIVIDUAL
        );

        (configurationRepository.getCounterByCode as jest.Mock).mockImplementation(
          counter => getCountersList(counter)
        );
        (configurationRepository.getBasicFees as jest.Mock).mockImplementation(
          () => basicFee
        );
        (configurationRepository.getTransactionCode as jest.Mock).mockImplementation(
          () => transactionCode
        );

        // When
        const actualFeeRule = await decisionRules.getFeeRule(
          code,
          50000,
          monthlyNoTransaction,
          interchange,
          targetBankCode,
          thresholdCounter,
          customerData.customerId
        );

        // Then
        expect(configurationRepository.getCounterByCode).toHaveBeenCalledWith(
          COUNTER.COUNTER_MFS_01
        );
        expect(actualFeeRule).toEqual(expectedFeeRule);
      }
    );
  });

  //error
  it('should throw error for unsupported interchange', async () => {
    // Given
    (configurationRepository.getCounterByCode as jest.Mock).mockResolvedValueOnce(
      getCounterData()
    );

    // When
    const error = await decisionRules
      .getFeeRule(FeeRuleType.WALLET_FEE_RULE, 50000, 31, BankNetworkEnum.IRIS)
      .catch(err => err);

    expect(error.errorCode).toEqual(
      ERROR_CODE.TRANSFER_INTERCHANGE_FEE_RULE_NOT_CONFIGURED
    );
  });

  it('should throw error for BILL_PAYMENT_FEE when targetBankCode is undefined', async () => {
    // Given
    (configurationRepository.getCounterByCode as jest.Mock).mockResolvedValueOnce(
      getCounterData()
    );

    // When
    const error = await decisionRules
      .getFeeRule(
        FeeRuleType.BILL_PAYMENT_FEE,
        50000,
        31,
        BankNetworkEnum.IRIS,
        undefined
      )
      .catch(err => err);

    expect(error.errorCode).toEqual(
      ERROR_CODE.TRANSFER_FEE_RULE_INVALID_MANDATORY_FIELDS
    );
  });

  it('should throw error when counter not found', async () => {
    // Given
    (configurationRepository.getCounterByCode as jest.Mock).mockResolvedValueOnce(
      null
    );

    // When
    const error = await decisionRules
      .getFeeRule(
        FeeRuleType.RTOL_TRANSFER_FEE_RULE,
        50000,
        31,
        BankNetworkEnum.IRIS
      )
      .catch(err => err);

    expect(error.errorCode).toEqual(
      ERROR_CODE.TRANSFER_INTERCHANGE_FEE_RULE_NOT_CONFIGURED
    );
  });

  it('should throw error when basic fee not found', async () => {
    // Given
    (configurationRepository.getCounterByCode as jest.Mock).mockResolvedValueOnce(
      getCounterData()
    );
    (configurationRepository.getBasicFees as jest.Mock).mockResolvedValueOnce(
      null
    );

    // When
    const error = await decisionRules
      .getFeeRule(
        FeeRuleType.CASHBACK_TAX_RULE,
        50000,
        undefined,
        BankNetworkEnum.VISA
      )
      .catch(error => error);

    // Then
    expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REGISTER_PARAMETER);
  });

  it('should throw error when transaction code not found', async () => {
    // Given
    const basicFeeData = { ...basicFee };
    delete basicFeeData.customerTcInfo;
    (configurationRepository.getCounterByCode as jest.Mock).mockResolvedValueOnce(
      getCounterData()
    );
    (configurationRepository.getBasicFees as jest.Mock).mockImplementation(
      () => basicFeeData
    );
    (configurationRepository.getTransactionCode as jest.Mock).mockResolvedValueOnce(
      null
    );

    // When
    const error = await decisionRules
      .getFeeRule(
        FeeRuleType.CASHBACK_TAX_RULE,
        50000,
        undefined,
        BankNetworkEnum.VISA
      )
      .catch(error => error);

    // Then
    expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REGISTER_PARAMETER);
  });

  test.each([
    [
      {
        code: 'INTEREST_TAX_RULE'
      },
      {
        description: 'Tax on interest',
        basicFeeMapping: [
          {
            interchange: BankNetworkEnum.LFS,
            basicFeeCode: 'IBC01',
            basicFee: expectedBasicFee
          },
          {
            interchange: BankNetworkEnum.MFS,
            basicFeeCode: 'IBC02',
            basicFee: expectedBasicFee
          }
        ]
      }
    ],
    [
      {
        code: 'INTEREST_TAX_RULE',
        interchange: BankNetworkEnum.LFS
      },
      {
        description: 'Tax on interest',
        basicFeeMapping: [
          {
            interchange: BankNetworkEnum.LFS,
            basicFeeCode: 'IBC01',
            basicFee: expectedBasicFee
          }
        ]
      }
    ],
    [
      {
        code: 'INTEREST_TAX_RULE',
        interchange: BankNetworkEnum.MFS
      },
      {
        description: 'Tax on interest',
        basicFeeMapping: [
          {
            interchange: BankNetworkEnum.MFS,
            basicFeeCode: 'IBC02',
            basicFee: expectedBasicFee
          }
        ]
      }
    ]
  ])(
    'when feeRuleInput is %o return %o for fee rule INTEREST_TAX_RULE',
    async (feeRuleInput, expectedFeeRule) => {
      // Given
      const {
        code,
        monthlyNoTransaction,
        transactionAmount,
        interchange,
        targetBankCode,
        thresholdCounter
      } = feeRuleInput as FeeRuleInput;
      (configurationRepository.getCounterByCode as jest.Mock).mockImplementation(
        counter => getCountersList(counter)
      );
      (configurationRepository.getBasicFees as jest.Mock).mockImplementation(
        () => basicFee
      );
      (configurationRepository.getTransactionCode as jest.Mock).mockImplementation(
        () => transactionCode
      );

      // When
      const actualFeeRule = await decisionRules.getFeeRule(
        code,
        transactionAmount,
        monthlyNoTransaction,
        interchange,
        targetBankCode,
        thresholdCounter
      );

      // Then
      expect(actualFeeRule).toEqual(expectedFeeRule);
    }
  );
});
