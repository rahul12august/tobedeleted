import { BankNetworkEnum } from '@dk/module-common';
import configurationRepository from '../../configuration/configuration.repository';
import { getCountersList } from '../../configuration/__mocks__/configuration.data';
import { FeeRuleInput } from '../fee.type';
import decisionRules from '../decisionRules';
import { getBasicFeeData, getTransactionCode } from '../__mocks__/fee.data';
import entitlementFlag from '../../common/entitlementFlag';

jest.mock('../../configuration/configuration.repository');
jest.mock('../../customer/customer.cache');

describe('feeRule service with entitlement', () => {
  const basicFee = getBasicFeeData();
  delete basicFee.customerTcInfo;
  const transactionCode = getTransactionCode();
  const expectedBasicFee = {
    ...basicFee,
    customerTcInfo: { channel: transactionCode.channel }
  };

  afterEach(() => {
    expect.hasAssertions();
    jest.clearAllMocks();
  });
  describe('entitlement getFeeRule method', () => {
    afterEach(() => {
      expect.hasAssertions();
      jest.clearAllMocks();
    });

    entitlementFlag.isEnabled = jest.fn().mockResolvedValue(true);

    const testCases = [
      [
        {
          code: 'RTOL_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 0
        },
        {
          customerId: '123456',
          awardGroupCounter: 'Bonus_Transfer',
          usageCounters: [
            {
              limitGroupCode: 'Bonus_Transfer',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 0,
              quota: 25,
              isQuotaExceeded: false
            }
          ]
        },
        {
          description: 'RTOL Transfer Fee Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF015',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            },
            {
              basicFeeCode: 'TF008',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTOL_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 0,
          interchange: BankNetworkEnum.ALTO
        },
        {
          customerId: '123456',
          awardGroupCounter: 'Bonus_Transfer',
          usageCounters: [
            {
              limitGroupCode: 'Bonus_Transfer',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 0,
              quota: 25,
              isQuotaExceeded: false
            }
          ]
        },
        {
          description: 'RTOL Transfer Fee Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF008',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTOL_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 31,
          interchange: BankNetworkEnum.ALTO
        },
        {
          customerId: '123456',
          awardGroupCounter: 'Bonus_Transfer',
          usageCounters: [
            {
              limitGroupCode: 'Bonus_Transfer',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 31,
              quota: 25,
              isQuotaExceeded: true
            }
          ]
        },
        {
          description: 'RTOL Transfer Fee Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF009',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'RTOL_TRANSFER_FEE_RULE',
          monthlyNoTransaction: 31,
          interchange: BankNetworkEnum.ARTAJASA
        },
        {
          customerId: '123456',
          awardGroupCounter: 'Bonus_Transfer',
          usageCounters: [
            {
              limitGroupCode: 'Bonus_Transfer',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 31,
              quota: 25,
              isQuotaExceeded: true
            }
          ]
        },
        {
          description: 'RTOL Transfer Fee Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF010',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'LOCAL_ATM_WITHDRAWAL_RULE',
          monthlyNoTransaction: 0,
          interchange: BankNetworkEnum.ALTO
        },
        {
          customerId: '123456',
          awardGroupCounter: 'Bonus_Local_CashWithdrawal',
          usageCounters: [
            {
              limitGroupCode: 'Bonus_Local_CashWithdrawal',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 0,
              quota: 5,
              isQuotaExceeded: false
            }
          ]
        },
        {
          description: 'Local ATM Withdrawal Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'CW005',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'LOCAL_ATM_WITHDRAWAL_RULE',
          monthlyNoTransaction: 0,
          interchange: BankNetworkEnum.ARTAJASA,
          thresholdCounter: 'AWARD_TRANSFER_001'
        },
        {
          customerId: '123456',
          awardGroupCounter: 'Bonus_Local_CashWithdrawal',
          usageCounters: [
            {
              limitGroupCode: 'Bonus_Local_CashWithdrawal',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 0,
              quota: 5,
              isQuotaExceeded: false
            }
          ]
        },
        {
          description: 'Local ATM Withdrawal Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'CW007',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'LOCAL_ATM_WITHDRAWAL_RULE',
          monthlyNoTransaction: 6,
          interchange: BankNetworkEnum.ARTAJASA,
          thresholdCounter: 'AWARD_TRANSFER_001'
        },
        {
          customerId: '123456',
          awardGroupCounter: 'Bonus_Local_CashWithdrawal',
          usageCounters: [
            {
              limitGroupCode: 'Bonus_Local_CashWithdrawal',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 6,
              quota: 5,
              isQuotaExceeded: true
            }
          ]
        },
        {
          description: 'Local ATM Withdrawal Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'CW001',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'LOCAL_ATM_WITHDRAWAL_RULE',
          monthlyNoTransaction: 6,
          interchange: BankNetworkEnum.ALTO,
          thresholdCounter: 'AWARD_TRANSFER_001'
        },
        {
          customerId: '123456',
          awardGroupCounter: 'Bonus_Local_CashWithdrawal',
          usageCounters: [
            {
              limitGroupCode: 'Bonus_Local_CashWithdrawal',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 6,
              quota: 5,
              isQuotaExceeded: true
            }
          ]
        },
        {
          description: 'Local ATM Withdrawal Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'CW002',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        { code: 'WALLET_FEE_RULE', monthlyNoTransaction: 0 },
        {
          customerId: '123456',
          awardGroupCounter: 'Bonus_Transfer',
          usageCounters: [
            {
              limitGroupCode: 'Bonus_Transfer',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 0,
              quota: 25,
              isQuotaExceeded: false
            }
          ]
        },
        {
          description: 'Wallet Top up Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'BF011',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            },
            {
              basicFeeCode: 'BF004',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'WALLET_FEE_RULE',
          monthlyNoTransaction: 0,
          interchange: BankNetworkEnum.ALTO
        },
        {
          customerId: '123456',
          awardGroupCounter: 'Bonus_Transfer',
          usageCounters: [
            {
              limitGroupCode: 'Bonus_Transfer',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 0,
              quota: 25,
              isQuotaExceeded: false
            }
          ]
        },
        {
          description: 'Wallet Top up Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'BF004',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'WALLET_FEE_RULE',
          monthlyNoTransaction: 31,
          interchange: BankNetworkEnum.ALTO
        },
        {
          customerId: '123456',
          awardGroupCounter: 'Bonus_Transfer',
          usageCounters: [
            {
              limitGroupCode: 'Bonus_Transfer',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 31,
              quota: 25,
              isQuotaExceeded: true
            }
          ]
        },
        {
          description: 'Wallet Top up Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'BF003',
              interchange: BankNetworkEnum.ALTO,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'WALLET_FEE_RULE',
          monthlyNoTransaction: 31,
          interchange: BankNetworkEnum.ARTAJASA
        },
        {
          customerId: '123456',
          awardGroupCounter: 'Bonus_Transfer',
          usageCounters: [
            {
              limitGroupCode: 'Bonus_Transfer',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 31,
              quota: 25,
              isQuotaExceeded: true
            }
          ]
        },
        {
          description: 'Wallet Top up Rule',
          basicFeeMapping: [
            {
              basicFeeCode: 'BF005',
              interchange: BankNetworkEnum.ARTAJASA,
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'BIFAST_FEE_RULE',
          transactionAmount: 50000001
        },
        {
          customerId: '123456',
          awardGroupCounter: 'Bonus_Transfer',
          usageCounters: [
            {
              limitGroupCode: 'Bonus_Transfer',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 0,
              quota: 25,
              isQuotaExceeded: false
            }
          ]
        },
        {
          description: 'Fee Rules for BI Fast Transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF022',
              basicFee: expectedBasicFee
            }
          ]
        }
      ],
      [
        {
          code: 'BIFAST_FEE_RULE',
          transactionAmount: 4000000,
          monthlyNoTransaction: 30
        },
        {
          customerId: '123456',
          awardGroupCounter: 'Bonus_Transfer',
          usageCounters: [
            {
              limitGroupCode: 'Bonus_Transfer',
              dailyAccumulationAmount: 0,
              monthlyAccumulationTransaction: 30,
              quota: 25,
              isQuotaExceeded: true
            }
          ]
        },
        {
          description: 'Fee Rules for BI Fast Transfer',
          basicFeeMapping: [
            {
              basicFeeCode: 'TF020',
              basicFee: expectedBasicFee
            }
          ]
        }
      ]
    ];

    test.each(testCases)(
      'when feeRuleInput is %o return %o',
      async (feeRuleInput, entitlementParams, expectedFeeRule) => {
        // Given
        const {
          code,
          monthlyNoTransaction,
          transactionAmount,
          interchange,
          targetBankCode,
          thresholdCounter
        } = feeRuleInput as FeeRuleInput;

        const {
          customerId,
          awardGroupCounter,
          usageCounters
        } = entitlementParams;

        (configurationRepository.getCounterByCode as jest.Mock).mockImplementation(
          counter => getCountersList(counter)
        );
        (configurationRepository.getBasicFees as jest.Mock).mockImplementation(
          () => basicFee
        );
        (configurationRepository.getTransactionCode as jest.Mock).mockImplementation(
          () => transactionCode
        );

        const actualFeeRule = await decisionRules.getFeeRule(
          code,
          transactionAmount,
          monthlyNoTransaction,
          interchange,
          targetBankCode,
          thresholdCounter,
          customerId,
          awardGroupCounter,
          usageCounters
        );

        // Then
        expect(actualFeeRule).toEqual(expectedFeeRule);
      }
    );
  });
});
