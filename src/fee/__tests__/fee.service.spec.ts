import feeService from '../fee.service';
import {
  getFeeRuleData,
  getFeeRuleDataWithoutAnyFeeType,
  getFeeRuleDataWithoutAnySubsidiaryType,
  getFeeRuleDataWithoutGLNumber
} from '../../configuration/__mocks__/configuration.data';
import {
  getFeeAmountData,
  getFeeAmountDataWithoutGlNumber
} from '../__mocks__/fee.data';
import { TransferAppError } from '../../errors/AppError';
import { ERROR_CODE } from '../../common/errors';
import { BankNetworkEnum } from '@dk/module-common';
import { FeeAmountInput, IFeeAmount, IFeeRule } from '../fee.type';
import decisionRules from '../decisionRules';
import { TransferFailureReasonActor } from '../../transaction/transaction.enum';

jest.mock('../decisionRules');

describe('fee service', () => {
  describe('getFeeAmount method', () => {
    const FEE_CODE = 'OVERSEAS_ATM_WITHDRAWAL_RULE';
    const thresholdCounter = 'AWARD_TRANSFER_001';

    it('should return fee amount data if fee rule code is BILL_PAYMENT_FEE and input targetBankCode', async () => {
      // Given
      const FEE_CODE = 'BILL_PAYMENT_FEE';
      const targetBankCode = 'BC002';
      const feeData = getFeeAmountData();
      const expectedAmountData = [feeData];
      const mockFeeRuleData = getFeeRuleData();
      (decisionRules.getFeeRule as jest.Mock).mockResolvedValueOnce(
        mockFeeRuleData
      );
      const input: FeeAmountInput = {
        targetBankCode,
        feeRuleCode: FEE_CODE,
        transactionAmount: 50000,
        monthlyNoTransaction: 30
      };

      // When
      const actualFeeAmount = await feeService.getFeeAmount(input);

      // Then
      expect(actualFeeAmount).toEqual(expectedAmountData);
      expect(decisionRules.getFeeRule).toHaveBeenCalledWith(
        FEE_CODE,
        input.transactionAmount,
        30,
        undefined,
        targetBankCode,
        undefined,
        undefined,
        undefined,
        undefined
      );
    });

    it('should return fee amount data if fee rule code and feeAmountFixed found', async () => {
      // Given
      const targetBankCode = 'BC002';
      const feeData = getFeeAmountData();
      const expectedAmountData = [feeData];
      const mockFeeRuleData = getFeeRuleData();
      (decisionRules.getFeeRule as jest.Mock).mockResolvedValueOnce(
        mockFeeRuleData
      );
      const input: FeeAmountInput = {
        feeRuleCode: FEE_CODE,
        transactionAmount: 50000,
        monthlyNoTransaction: 30,
        interchange: BankNetworkEnum.ARTAJASA,

        externalId: undefined,
        beneficiaryBankCodeChannel: undefined,
        refund: undefined,
        targetBankCode,
        thresholdCounter
      };
      // When
      const actualFeeAmount = await feeService.getFeeAmount(input);

      // Then
      expect(actualFeeAmount).toEqual(expectedAmountData);
      expect(decisionRules.getFeeRule).toHaveBeenCalledWith(
        FEE_CODE,
        input.transactionAmount,
        30,
        BankNetworkEnum.ARTAJASA,
        targetBankCode,
        thresholdCounter,
        undefined,
        undefined,
        undefined
      );
    });

    it('should return fee amount data if fee rule code and feeAmountPercentage found', async () => {
      // Given
      const targetBankCode = 'BC002';
      const feeData = getFeeAmountData();
      const expectedAmountData: IFeeAmount[] = [
        {
          ...feeData,
          feeAmount: 1111.12
        }
      ];
      const mockFeeRuleData: IFeeRule = getFeeRuleData();
      const basicFeeMapping = mockFeeRuleData.basicFeeMapping?.[0];
      if (basicFeeMapping) {
        basicFeeMapping.basicFee = {
          ...basicFeeMapping.basicFee,
          feeAmountFixed: null,
          feeAmountPercentage: 1,
          customerTc: 'FCD05'
        };
      }
      (decisionRules.getFeeRule as jest.Mock).mockResolvedValueOnce(
        mockFeeRuleData
      );
      const input: FeeAmountInput = {
        feeRuleCode: FEE_CODE,
        transactionAmount: 111111.11,
        monthlyNoTransaction: 30,
        interchange: undefined,
        externalId: undefined,
        beneficiaryBankCodeChannel: undefined,
        refund: undefined,
        targetBankCode,
        thresholdCounter
      };

      // When
      const actualFeeAmount = await feeService.getFeeAmount(input);

      // Then
      expect(actualFeeAmount).toEqual(expectedAmountData);
      expect(decisionRules.getFeeRule).toHaveBeenCalledWith(
        FEE_CODE,
        input.transactionAmount,
        30,
        undefined,
        targetBankCode,
        thresholdCounter,
        undefined,
        undefined,
        undefined
      );
    });

    it('should return fee amount data if fee rule without gl number and subsidiary', async () => {
      // Given
      const targetBankCode = 'BC002';
      const feeData = getFeeAmountDataWithoutGlNumber();
      const expectedAmountData = [feeData];
      const mockFeeRuleData = getFeeRuleDataWithoutGLNumber();
      (decisionRules.getFeeRule as jest.Mock).mockResolvedValueOnce(
        mockFeeRuleData
      );

      const input: FeeAmountInput = {
        feeRuleCode: FEE_CODE,
        transactionAmount: 50000,
        monthlyNoTransaction: 30,
        interchange: BankNetworkEnum.ARTAJASA,
        externalId: undefined,
        beneficiaryBankCodeChannel: undefined,
        refund: undefined,
        targetBankCode,
        thresholdCounter
      };

      // When
      const actualFeeAmount = await feeService.getFeeAmount(input);

      // Then
      expect(actualFeeAmount).toEqual(expectedAmountData);
      expect(decisionRules.getFeeRule).toHaveBeenCalledWith(
        FEE_CODE,
        input.transactionAmount,
        30,
        BankNetworkEnum.ARTAJASA,
        targetBankCode,
        thresholdCounter,
        undefined,
        undefined,
        undefined
      );
    });

    it('should return undefined if fee rule has no basicFeeCode', async () => {
      // Given
      const targetBankCode = 'BC002';
      const mockFeeRuleData = getFeeRuleData();
      mockFeeRuleData.basicFeeMapping = undefined;
      (decisionRules.getFeeRule as jest.Mock).mockResolvedValueOnce(
        mockFeeRuleData
      );
      const input: FeeAmountInput = {
        feeRuleCode: FEE_CODE,
        transactionAmount: 50000,
        monthlyNoTransaction: 30,

        targetBankCode
      };

      // When
      const actualFeeAmount = await feeService.getFeeAmount(input);

      // Then
      expect(actualFeeAmount).toEqual([]);
      expect(decisionRules.getFeeRule).toHaveBeenCalledWith(
        FEE_CODE,
        input.transactionAmount,
        30,
        undefined,
        targetBankCode,
        undefined,
        undefined,
        undefined,
        undefined
      );
    });

    it('should throw error if there are no feeAmountFixed and feeAmountPercentage', async () => {
      // given
      const mockFeeRuleData: IFeeRule = getFeeRuleData();
      const feeMapping = mockFeeRuleData.basicFeeMapping?.[0];
      if (feeMapping) {
        feeMapping.basicFee = {
          ...feeMapping.basicFee,
          feeAmountFixed: null,
          feeAmountPercentage: null,
          customerTc: 'FCD05'
        };
      }
      (decisionRules.getFeeRule as jest.Mock).mockResolvedValueOnce(
        mockFeeRuleData
      );
      const input: FeeAmountInput = {
        feeRuleCode: 'any',
        transactionAmount: 0,
        monthlyNoTransaction: 0
      };

      // when
      const error = await feeService.getFeeAmount(input).catch(e => e);

      // then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.MISSING_FEE_RULE);
      expect(error.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
      expect(error.detail).toEqual(
        'Fee template could not be found for fee rule!'
      );
    });

    it('should throw error if getFeeRule not found', async () => {
      // given
      (decisionRules.getFeeRule as jest.Mock).mockResolvedValueOnce(null);
      const input: FeeAmountInput = {
        feeRuleCode: 'any',
        transactionAmount: 0,
        monthlyNoTransaction: 0
      };

      // when
      const error = await feeService.getFeeAmount(input).catch(error => error);

      // then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.INVALID_REGISTER_PARAMETER);
      expect(error.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
      expect(error.detail).toEqual('No fee rule found for feeRuleCode: any!');
    });

    it('should throw error if matching fee template is not found', async () => {
      // given
      const mockFeeRuleData = getFeeRuleDataWithoutAnyFeeType();
      (decisionRules.getFeeRule as jest.Mock).mockResolvedValueOnce(
        mockFeeRuleData
      );
      const input: FeeAmountInput = {
        feeRuleCode: 'any',
        transactionAmount: 0,
        monthlyNoTransaction: 0
      };

      // when
      const error = await feeService.getFeeAmount(input).catch(error => error);

      // then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.MISSING_FEE_RULE);
      expect(error.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
      expect(error.detail).toEqual(
        'Fee template could not be found for fee rule!'
      );
    });

    it('should throw error if matching subsidiary template is not found', async () => {
      // given
      const mockFeeRuleData = getFeeRuleDataWithoutAnySubsidiaryType();
      (decisionRules.getFeeRule as jest.Mock).mockResolvedValueOnce(
        mockFeeRuleData
      );
      const input: FeeAmountInput = {
        feeRuleCode: 'any',
        transactionAmount: 0,
        monthlyNoTransaction: 0
      };

      // when
      const error = await feeService.getFeeAmount(input).catch(error => error);

      // then
      expect(error).toBeInstanceOf(TransferAppError);
      expect(error.errorCode).toEqual(ERROR_CODE.MISSING_SUBSIDIARY_RULE);
      expect(error.actor).toEqual(TransferFailureReasonActor.MS_TRANSFER);
      expect(error.detail).toEqual(
        'Subsidiary template could not be found for fee rule!'
      );
    });

    it('should return fee amount data if fee rule code is BILL_PAYMENT_FEE with customerId', async () => {
      // Given
      const FEE_CODE = 'BILL_PAYMENT_FEE';
      const targetBankCode = 'BC002';
      const feeData = getFeeAmountData();
      const expectedAmountData = [feeData];
      const mockFeeRuleData = getFeeRuleData();
      (decisionRules.getFeeRule as jest.Mock).mockResolvedValueOnce(
        mockFeeRuleData
      );
      const input: FeeAmountInput = {
        targetBankCode,
        feeRuleCode: FEE_CODE,
        transactionAmount: 50000,
        monthlyNoTransaction: 30,
        customerId: '1'
      };

      // When
      const actualFeeAmount = await feeService.getFeeAmount(input);

      // Then
      expect(actualFeeAmount).toEqual(expectedAmountData);
      expect(decisionRules.getFeeRule).toHaveBeenCalledWith(
        FEE_CODE,
        input.transactionAmount,
        30,
        undefined,
        targetBankCode,
        undefined,
        input.customerId,
        undefined,
        undefined
      );
    });
  });
});
