import {
  FeeAmountInput,
  IBasicFeeMapping,
  IFeeAmount,
  IFeeRuleTemplate,
  IFeeRuleTemplatePayload
} from './fee.type';
import decisionRules from './decisionRules';
import logger, { wrapLogs } from '../logger';
import { TransferAppError } from '../errors/AppError';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';
import { ERROR_CODE } from '../common/errors';
import { feeTemplates } from './fee.template';
import { subsidiaryTemplates } from './subsidiary.template';

const getFeeRuleTemplatePayload = (
  input: FeeAmountInput,
  basicFeeMap: IBasicFeeMapping
): IFeeRuleTemplatePayload => {
  return {
    basicFee: basicFeeMap.basicFee,
    transactionAmount: input.transactionAmount,
    feeAmount: input.feeAmount,
    externalId: input.externalId,
    beneficiaryBankCodeChannel: input.beneficiaryBankCodeChannel,
    refund: input.refund
  };
};

const getFeeMappings = async (
  input: FeeAmountInput
): Promise<IBasicFeeMapping[]> => {
  const feeRule = await decisionRules.getFeeRule(
    input.feeRuleCode,
    input.transactionAmount,
    input.monthlyNoTransaction,
    input.interchange,
    input.targetBankCode,
    input.thresholdCounter,
    input.customerId,
    input.awardGroupCounter,
    input.usageCounters
  );
  if (!feeRule) {
    const detail = `No fee rule found for feeRuleCode: ${input.feeRuleCode}!`;
    logger.error(detail);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.INVALID_REGISTER_PARAMETER
    );
  }

  return feeRule.basicFeeMapping ?? [];
};

const getApplicableTemplate = (
  feeRuleTemplatePayload: IFeeRuleTemplatePayload
): IFeeRuleTemplate => {
  const template = feeTemplates().find(tmpl =>
    tmpl.isApplicable(feeRuleTemplatePayload)
  );
  if (!template) {
    const detail = 'Fee template could not be found for fee rule!';
    logger.error(detail, feeRuleTemplatePayload);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.MISSING_FEE_RULE
    );
  }
  return template;
};

const getSubsidiaryTemplate = (
  feeRuleTemplatePayload: IFeeRuleTemplatePayload
): IFeeRuleTemplate => {
  const template = subsidiaryTemplates().find(tmpl =>
    tmpl.isApplicable(feeRuleTemplatePayload)
  );
  if (!template) {
    const detail = 'Subsidiary template could not be found for fee rule!';
    logger.error(detail, feeRuleTemplatePayload);
    throw new TransferAppError(
      detail,
      TransferFailureReasonActor.MS_TRANSFER,
      ERROR_CODE.MISSING_SUBSIDIARY_RULE
    );
  }
  return template;
};

const calculateFeeAmount = async (
  feePayload: IFeeRuleTemplatePayload
): Promise<number> => getApplicableTemplate(feePayload).getFee(feePayload);

const calculateSubsidiaryAmount = async (
  feePayload: IFeeRuleTemplatePayload
): Promise<number> =>
  feePayload.basicFee.subsidiary
    ? getSubsidiaryTemplate(feePayload).getFee(feePayload)
    : 0;

const getFeeAmount = async (
  input: FeeAmountInput,
  basicFeeMapping: IBasicFeeMapping
): Promise<IFeeAmount> => {
  {
    const { basicFee, basicFeeCode } = basicFeeMapping;
    const feePayload = getFeeRuleTemplatePayload(input, basicFeeMapping);
    const fee = await calculateFeeAmount(feePayload);
    const subsidiaryAmount = await calculateSubsidiaryAmount(feePayload);
    return {
      feeCode: basicFeeCode,
      interchange: basicFeeMapping.interchange,
      feeAmount: fee,
      customerTc: basicFee.customerTc,
      debitGlNumber: basicFee.debitGlNumber || '',
      creditGlNumber: basicFee.creditGlNumber || '',
      subsidiary: basicFee.subsidiary,
      subsidiaryAmount: subsidiaryAmount,
      customerTcChannel: basicFee.customerTcInfo.channel
    };
  }
};

const feeHelper = {
  getFeeRuleTemplatePayload,
  getFeeMappings,
  getFeeAmount
};

export default wrapLogs(feeHelper);
