import { IFeeRuleTemplate, IFeeRuleTemplatePayload } from './fee.type';
import { BankChannelEnum, Formater } from '@dk/module-common';
import { PERCENTAGE } from './fee.constant';
import logger, { wrapLogs } from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { isUndefined, sumBy } from 'lodash';
import transactionRepository from '../transaction/transaction.repository';
import billDetailService from '../billPayment/billDetail/billDetail.service';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

export const fixedFee: IFeeRuleTemplate = wrapLogs({
  isApplicable: (input: IFeeRuleTemplatePayload) =>
    typeof input.basicFee.feeAmountFixed === 'number',

  getFee: (input: IFeeRuleTemplatePayload) => {
    const { feeAmountFixed } = input.basicFee;
    if (feeAmountFixed === null || feeAmountFixed === undefined) {
      let detail = "Can't continue feeAmountFixed is absent in basic fee!";
      logger.error(detail, input);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.MS_TRANSFER,
        ERROR_CODE.MISSING_FIXED_FEE_CONFIGURATION
      );
    }
    return feeAmountFixed;
  }
});

export const percentageFee: IFeeRuleTemplate = wrapLogs({
  isApplicable: (input: IFeeRuleTemplatePayload) =>
    typeof input.basicFee.feeAmountPercentage === 'number',

  getFee: (input: IFeeRuleTemplatePayload) => {
    const { feeAmountPercentage } = input.basicFee;
    if (feeAmountPercentage === null || feeAmountPercentage === undefined) {
      const detail = 'Fee amount percentage is absent in basic fee!';
      logger.error(detail, input);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.SUBMITTER,
        ERROR_CODE.MISSING_PERCENTAGE_FEE_CONFIGURATION
      );
    }
    return Formater.roundUp(
      (input.transactionAmount * feeAmountPercentage) / PERCENTAGE
    );
  }
});

export const refundFee: IFeeRuleTemplate = wrapLogs({
  isApplicable: (input: IFeeRuleTemplatePayload) =>
    !isUndefined(input.refund) &&
    !isUndefined(input.refund.originalTransactionId),

  getFee: async (input: IFeeRuleTemplatePayload) => {
    let feeAmount = 0;
    if (input.refund && input.refund.originalTransactionId) {
      const originalTransaction = await transactionRepository.getByKey(
        input.refund.originalTransactionId
      );
      if (!originalTransaction) {
        const detail = `Original transaction not found for transactionId: ${input.refund.originalTransactionId}`;
        logger.error(`General refund getFee: ${detail}`);
        throw new TransferAppError(
          detail,
          TransferFailureReasonActor.SUBMITTER,
          ERROR_CODE.TRANSACTION_NOT_FOUND
        );
      }
      if (originalTransaction.fees) {
        feeAmount = sumBy(originalTransaction.fees, 'feeAmount');
      }
    }
    return feeAmount;
  }
});

export const billPaymentAdminFee: IFeeRuleTemplate = wrapLogs({
  isApplicable: (input: IFeeRuleTemplatePayload): boolean =>
    input.basicFee.thirdPartyFee === true &&
    (input.beneficiaryBankCodeChannel === BankChannelEnum.GOBILLS ||
      input.beneficiaryBankCodeChannel === BankChannelEnum.TOKOPEDIA),

  getFee: async (input: IFeeRuleTemplatePayload): Promise<number> => {
    if (input.feeAmount) {
      logger.info(
        `billPaymentAdminFee : getFee : Using feeAmount provided in transfer input.`
      );
      return input.feeAmount;
    }
    if (!input.externalId) {
      const detail = 'externalId is not provided for getFee';
      logger.error(
        `billPaymentAdminFee : ${detail}, payload: `,
        JSON.stringify(input)
      );
      throw new TransferAppError(
        detail + '!',
        TransferFailureReasonActor.MS_TRANSFER,
        ERROR_CODE.MISSING_INQUIRY_ID
      );
    }
    return billDetailService.getAdminFee(input.externalId);
  }
});

export const feeTemplates = (): IFeeRuleTemplate[] => [
  refundFee,
  billPaymentAdminFee,
  fixedFee,
  percentageFee
];
