import {
  FeeAmountInput,
  FeeAmountQuery,
  IBasicFeeMapping,
  IFeeAmount
} from './fee.type';
import { wrapLogs } from '../logger';
import feeHelper from './fee.helper';
import feeBatchHelper from './fee.batch';

const getFeeAmount = async (input: FeeAmountInput): Promise<IFeeAmount[]> => {
  const feeMappings = await feeHelper.getFeeMappings(input);
  const feeQuery: FeeAmountQuery = {
    input,
    feeMappings
  };
  await feeBatchHelper.applyCustomerSpecificFeeIfAvailable([feeQuery]);
  return Promise.all(
    feeMappings.map(async (basicFeeMapping: IBasicFeeMapping) => {
      return feeHelper.getFeeAmount(input, basicFeeMapping);
    })
  );
};

const feeService = {
  getFeeAmount
};

export default wrapLogs(feeService);
