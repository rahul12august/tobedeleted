import { IFeeRuleTemplate, IFeeRuleTemplatePayload } from './fee.type';
import logger, { wrapLogs } from '../logger';
import { TransferAppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
import { PERCENTAGE } from './fee.constant';
import { Formater } from '@dk/module-common';
import { TransferFailureReasonActor } from '../transaction/transaction.enum';

export const fixedSubsidiary: IFeeRuleTemplate = wrapLogs({
  isApplicable: (input: IFeeRuleTemplatePayload) =>
    typeof input.basicFee.subsidiaryAmount === 'number',

  getFee: (input: IFeeRuleTemplatePayload) => {
    const amount = input.basicFee.subsidiaryAmount;
    if (amount === null || amount === undefined) {
      const detail = 'Invalid subsidiary amount in fee rule!';
      logger.error(detail);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.SUBMITTER,
        ERROR_CODE.INVALID_SUBSIDIARY_VALUE
      );
    }
    return amount;
  }
});

export const percentageSubsidiary: IFeeRuleTemplate = wrapLogs({
  isApplicable: (input: IFeeRuleTemplatePayload) =>
    typeof input.basicFee.subsidiaryAmountPercentage === 'number',

  getFee: (input: IFeeRuleTemplatePayload) => {
    const percentage = input.basicFee.subsidiaryAmountPercentage;
    if (percentage === null || percentage === undefined) {
      const detail = 'Invalid subsidiary percentage in fee rule!';
      logger.error(detail);
      throw new TransferAppError(
        detail,
        TransferFailureReasonActor.SUBMITTER,
        ERROR_CODE.INVALID_SUBSIDIARY_VALUE
      );
    }
    const grossAmount = Formater.roundUp(
      input.transactionAmount / (1 - percentage / PERCENTAGE)
    );
    return grossAmount - input.transactionAmount;
  }
});

export const subsidiaryTemplates = (): IFeeRuleTemplate[] => [
  fixedSubsidiary,
  percentageSubsidiary
];
