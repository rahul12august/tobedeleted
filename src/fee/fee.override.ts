import { fixedFee, percentageFee } from './fee.template';
import { IFeeRuleTemplatePayload } from './fee.type';
import entitlementRepository from '../entitlement/entitlement.repository';
import logger, { wrapLogs } from '../logger';
import _ from 'lodash';

// Regex for percentage or fixed fees
const overrideFeeRegex = new RegExp('^[0-9]+(\\.[0-9]+)?%?$');

const tryGetCustomFeeFromEntitlement = async (
  customerId: string,
  feeEntitlementCode: string
): Promise<string | null> => {
  try {
    const response = await entitlementRepository.getEntitlements(customerId, [
      feeEntitlementCode
    ]);

    if (!_.isEmpty(response)) {
      // Should only have 1 item, as we only have a single request
      return String(response[0].quota);
    } else {
      return null;
    }
  } catch (error) {
    // We ignore any error. This just means no override is available from ms-entitlement.
    // Error already logged by entitlementRepository
    return null;
  }
};

const applyCustomerSpecificFeeIfAvailable = async (
  customerId: string | undefined,
  feePayload: IFeeRuleTemplatePayload
): Promise<void> => {
  if (
    !customerId ||
    !(
      fixedFee.isApplicable(feePayload) ||
      percentageFee.isApplicable(feePayload)
    )
  ) {
    // We currently only allow custom fee overrides for fixed or percentage fees
    return;
  }

  // Try to get custom fee from ms-entitlement
  const basicFee = feePayload.basicFee;
  const feeCode = basicFee.code;
  const feeEntitlementCode = `value.tx.${feeCode}.fee`;
  const overrideFee = await tryGetCustomFeeFromEntitlement(
    customerId,
    feeEntitlementCode
  );
  if (!overrideFee) return;
  if (!overrideFeeRegex.test(overrideFee)) {
    const detail =
      `Could not parse fee amount from entitlement: '${feeEntitlementCode}', for customer ID: '${customerId}'! ` +
      `Value was '${overrideFee}'`;
    logger.error(`applyCustomerSpecificFeeIfAvailable: ${detail}`);
    return;
  }

  // Replace the existing fee values
  const overrideFeeAmount = parseFloat(overrideFee);
  if (overrideFee.includes('%')) {
    basicFee.feeAmountPercentage = overrideFeeAmount;
    basicFee.feeAmountFixed = undefined;
  } else {
    basicFee.feeAmountFixed = overrideFeeAmount;
    basicFee.feeAmountPercentage = undefined;
  }
  logger.info(
    `The fee amount for fee code: '${feeCode}' has been changed to '${overrideFee}', for customer ID: '${customerId}'`
  );
};

const feeOverride = {
  applyCustomerSpecificFeeIfAvailable
};

export default wrapLogs(feeOverride);
