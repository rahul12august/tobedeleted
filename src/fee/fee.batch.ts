import {
  BasicFeeRecordKey,
  FeeAmountInput,
  FeeAmountQuery,
  IBasicFeeMapping
} from './fee.type';
import feeHelper from './fee.helper';
import feeOverride from './fee.override';
import logger, { wrapLogs } from '../logger';
import { IBasicFee } from '../configuration/configuration.type';

const computeRecordedFeeIfAbsent = async (
  basicFeeRecord: Map<string, IBasicFee>,
  input: FeeAmountInput,
  basicFeeMapping: IBasicFeeMapping
): Promise<IBasicFee> => {
  const recordKey: BasicFeeRecordKey = {
    basicFeeCode: basicFeeMapping.basicFeeCode,
    customerId: input.customerId
  };
  const keyString = JSON.stringify(recordKey);
  const cachedFee = basicFeeRecord.get(keyString);
  if (cachedFee) {
    return cachedFee;
  }

  const feePayload = feeHelper.getFeeRuleTemplatePayload(
    input,
    basicFeeMapping
  );
  await feeOverride.applyCustomerSpecificFeeIfAvailable(
    input.customerId,
    feePayload
  );
  const recordedFee = basicFeeMapping.basicFee;
  basicFeeRecord.set(keyString, recordedFee);

  logger.info(
    `Recorded the following fee for customer ID: '${input.customerId}': ${recordedFee}`
  );

  return recordedFee;
};

const applyCustomerSpecificFeeIfAvailable = async (
  feeQueries: FeeAmountQuery[]
): Promise<void> => {
  const basicFeeRecord = new Map<string, IBasicFee>();
  for (const query of feeQueries) {
    const { input, feeMappings } = query;
    for (const basicFeeMapping of feeMappings) {
      basicFeeMapping.basicFee = await computeRecordedFeeIfAbsent(
        basicFeeRecord,
        input,
        basicFeeMapping
      );
    }
  }
};

const feeBatchHelper = {
  applyCustomerSpecificFeeIfAvailable
};

export default wrapLogs(feeBatchHelper);
