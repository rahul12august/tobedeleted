import { rundeck } from './config';
import logger from '../src/logger';
import { Rundeck, RundeckConfig } from '@dk/module-common';
import { context } from '../src/context';

const getAccessToken = (): string =>
  process.env.RUNDECK_API_ACCESS_TOKEN || rundeck.apiAccessToken;

const rundeckConfig: RundeckConfig = {
  apiURL: rundeck.apiURL,
  apiVersion: rundeck.apiVersion,
  token: getAccessToken()
};

const rundeckClient = new Rundeck(rundeckConfig, logger, context);

logger.info(`Setting up rundeck-client: ${rundeck.apiURL}`);

export default rundeckClient;
