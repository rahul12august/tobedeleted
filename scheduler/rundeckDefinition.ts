import { config, rundeck } from './config';
import { RunDeckDefinition } from '@dk/module-common';

const ms = config.get('ms');
const autoUpdateMambuBlockingCounter = {
  filepath: `${__dirname}/jobDefinitions/put.yaml`,
  options: {
    jobId: 'update-mambu-blocking-counter',
    jobDescription:
      'This job is to update mambu blocking counter with days code',
    group: 'ms-transfer',
    name: 'Update mambu blocking counter',
    data: '',
    api: `${ms.transfer}/private/jobs-execution/update-mambu-blocking-days`,
    scheduleDay: '"*"',
    scheduleMonth: '"*"',
    scheduleHour: '"0"',
    scheduleMinute: '"0"',
    scheduleSeconds: '"0"',
    scheduleYear: '"*"',
    slackWebhookURL: rundeck.slackWebhookURL
  }
};

const runDeckDefinitions: RunDeckDefinition[] = [
  {
    project: {
      name: 'ms-transfer',
      configs: {}
    },
    jobs: [
      {
        filepath: `${__dirname}/jobDefinitions/put.yaml`,
        options: {
          jobId: 'update-status-gobills',
          jobDescription:
            'This job is to update all pending transaction status from gobills',
          group: 'ms-transfer',
          name: 'Update Status from gobills',
          data: '',
          api: `${ms.transfer}/private/jobs/bill-payment/status-update`,
          scheduleDay: '"*"',
          scheduleMonth: '"*"',
          scheduleHour: '"*"',
          scheduleMinute: '"*/5"',
          scheduleSeconds: '"0"',
          scheduleYear: '"*"',
          slackWebhookURL: rundeck.slackWebhookURL
        }
      },
      autoUpdateMambuBlockingCounter
    ]
  }
];

export default runDeckDefinitions;
