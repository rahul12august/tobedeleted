import logger from '../src/logger';
import runDeckDefinitions from './rundeckDefinition';
import rundeckClient from './rundeck.client';

const main = async () => {
  await rundeckClient.setUpRundeckDefinitions(runDeckDefinitions);
};

main()
  .then(() => {
    logger.info('Jobs successfully created');
  })
  .catch(error => {
    logger.error(`Error: ${error.message}`, { error: error.stack });
  });
