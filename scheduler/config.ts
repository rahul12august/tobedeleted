import { merge } from 'lodash';
import { config, loadConfig } from '@dk/module-config';

const defaultConf = require('../configs/default.json');
const env = process.env.NODE_ENV;
const envConf = env ? require(`../configs/${env}.json`) : {};

loadConfig(merge(defaultConf, envConf));

const rundeck = config.get('rundeck');

export { config, rundeck };
