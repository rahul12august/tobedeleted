import { collectionName } from '../src/billPayment/billPaymentTransaction/billPaymentTransaction.model';
import { Db } from 'mongodb';

module.exports = {
  up(db: Db) {
    return db
      .collection(collectionName)
      .createIndex({ orderId: 1 }, { name: 'orderId_Idx_1', background: true })
      .catch(e => e);
  },

  down(db: Db) {
    return db
      .collection(collectionName)
      .dropIndex('orderId_Idx_1')
      .catch(e => e);
  }
};
