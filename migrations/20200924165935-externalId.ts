import { collectionName } from '../src/transaction/transaction.model';
import { Db } from 'mongodb';

module.exports = {
  up(db: Db) {
    db.collection(collectionName)
      .dropIndex('externalIdx_1')
      .catch(e => e);
    return db
      .collection(collectionName)
      .createIndex(
        { externalId: 1, paymentServiceType: -1 },
        { name: 'externalIdx_paymentServiceTypex', unique: true, sparse: true }
      );
  },

  down(db: Db) {
    return db
      .collection(collectionName)
      .dropIndex('externalIdx_paymentServiceTypex');
  }
};
