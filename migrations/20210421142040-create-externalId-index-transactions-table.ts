import { collectionName } from '../src/transaction/transaction.model';
import { Db } from 'mongodb';

module.exports = {
  up(db: Db) {
    return db
      .collection(collectionName)
      .createIndex(
        { externalId: 1 },
        { name: 'externalId_Idx_1', background: true }
      )
      .catch(e => e);
  },

  down(db: Db) {
    return db
      .collection(collectionName)
      .dropIndex('externalId_Idx_1')
      .catch(e => e);
  }
};
