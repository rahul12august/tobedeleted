import { collectionName } from '../src/transaction/transaction.model';
import { Db } from 'mongodb';
import logger from '../src/logger';

module.exports = {
  async up(db: Db) {
    try {
      await db.collection(collectionName).updateMany(
        { paymentServiceCode: 'IRIS_JAGO_TO_OTH', fees: null },
        {
          $set: {
            fees: []
          }
        }
      );
    } catch (e) {
      logger.error(
        `Error when migrating up script 20210409082140-update-fees-to-array-IRIS-transactions ${e}`
      );
    }
  },

  async down(db: Db) {
    try {
      await db.collection(collectionName).updateMany(
        { paymentServiceCode: 'IRIS_JAGO_TO_OTH', fees: [] },
        {
          $set: {
            fees: null
          }
        }
      );
    } catch (e) {
      logger.error(
        `Error when migrating down script 20210409082140-update-fees-to-array-IRIS-transactions ${e}`
      );
    }
  }
};
