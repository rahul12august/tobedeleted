import { collectionName } from '../src/transaction/transaction.model';
import { Db } from 'mongodb';

module.exports = {
  up(db: Db) {
    return db
      .collection(collectionName)
      .createIndex(
        { thirdPartyOutgoingId: 1 },
        { name: 'thirdPartyOutgoingId_Idx_1', background: true }
      )
      .catch(e => e);
  },

  down(db: Db) {
    return db
      .collection(collectionName)
      .dropIndex('thirdPartyOutgoingId_Idx_1')
      .catch(e => e);
  }
};
