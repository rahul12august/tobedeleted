import { collectionName } from '../src/transaction/transaction.model';
import { Db } from 'mongodb';

module.exports = {
  up(db: Db) {
    db.collection(collectionName)
      .dropIndex('externalId_1')
      .catch(e => e);
    return db
      .collection(collectionName)
      .dropIndex('secondaryExternalId_1')
      .catch(e => e);
  },

  down(db: Db) {
    db.collection(collectionName)
      .createIndex(
        { externalId: 1 },
        { name: 'externalId_1', unique: true, sparse: true }
      )
      .catch(e => e);
    return db
      .collection(collectionName)
      .createIndex(
        { secondaryExternalId: 1 },
        { name: 'secondaryExternalId_1', unique: true, sparse: true }
      )
      .catch(e => e);
  }
};
