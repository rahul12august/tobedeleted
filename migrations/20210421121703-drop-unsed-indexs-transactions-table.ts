import { collectionName } from '../src/transaction/transaction.model';
import { Db } from 'mongodb';

module.exports = {
  up(db: Db) {
    db.collection(collectionName)
      .dropIndex('offerClaimId_1')
      .catch(e => e);
    db.collection(collectionName)
      .dropIndex('secondaryExternalIdx_paymentServiceTypex')
      .catch(e => e);
    return db
      .collection(collectionName)
      .dropIndex('externalIdx_paymentServiceTypex')
      .catch(e => e);
  },

  down(db: Db) {
    db.collection(collectionName)
      .createIndex({ offerClaimId: 1 }, { name: 'offerClaimId_1' })
      .catch(e => e);
    db.collection(collectionName)
      .createIndex(
        { externalId: 1, secondaryExternalId: 1, paymentServiceType: -1 },
        { name: 'secondaryExternalIdx_paymentServiceTypex', unique: true }
      )
      .catch(e => e);
    return db
      .collection(collectionName)
      .createIndex(
        { externalId: 1, paymentServiceType: -1 },
        { name: 'externalIdx_paymentServiceTypex', unique: true }
      )
      .catch(e => e);
  }
};
