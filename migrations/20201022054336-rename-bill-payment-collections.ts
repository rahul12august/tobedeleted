import { Db } from 'mongodb';
import logger from '../src/logger';
const gobillsPollingCollection = 'bill_payment_status_polling_configs';
const goBillsTransactionsCollection = 'bill_payment_transactions';
const gobillsPollingCollectionNewName = 'gobills_polling_states';
const goBillsTransactionsCollectionNewName = 'gobills_transactions';
module.exports = {
  async up(db: Db) {
    try {
      await Promise.all([
        db.renameCollection(
          gobillsPollingCollection,
          gobillsPollingCollectionNewName
        ),
        db.renameCollection(
          goBillsTransactionsCollection,
          goBillsTransactionsCollectionNewName
        )
      ]);
    } catch (e) {
      logger.error(
        `Migration: Error while renaming ${gobillsPollingCollection} and ${goBillsTransactionsCollection}`
      );
    }
  },
  async down(db: Db) {
    try {
      await Promise.all([
        db.renameCollection(
          gobillsPollingCollectionNewName,
          gobillsPollingCollection
        ),
        db.renameCollection(
          goBillsTransactionsCollectionNewName,
          goBillsTransactionsCollection
        )
      ]);
    } catch (e) {
      logger.error(
        `Migration: Error while renaming ${gobillsPollingCollectionNewName} and ${goBillsTransactionsCollection}`
      );
    }
  }
};
